<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
    xmlns:html="http://www.w3.org/TR/REC-html40"
    xmlns:sitemap="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Image Sitemap</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<style type="text/css">
			body {
				background: #fff;
				margin: 0;
				padding: 0; }
			
			body, td, th {
				font: 11px "Lucida Grande", "Lucida Sans Unicode", Tahoma, Verdana, sans-serif; }
			
			#wrap {
				background: #fff;
				clear: both;
				padding: 15px 30px;
				width: 980px;
				margin: 0 auto;
			}
			
			h1 {
				border-bottom-width: 1px;
				border-bottom-style: solid;
				border-bottom-color: #dadada;
				color: #666;
				clear: both;
				font: 24px Georgia, "Times New Roman", Times, serif;
				margin: 5px 0 0;
				padding: 0;
				padding-bottom: 7px;
				padding-right: 280px; }
			
			#about {
				margin: 20px; }
		
			#footer	{
				border-top: #CCC 1px solid;
				color: #999;
				clear: both;
				height: 30px;
				padding: 10px 0 0;
				text-align: center;
				margin: 30px 0 0;
				font-size: 12px;
			}
				
			#footer a {
				color: #333; }
				
			#footer a:hover {
				text-decoration: underline; }
			
			table {
				border-color: #ccc;
				border-width: 1px;
				border-style: solid;
				border-collapse: collapse;
				width: 100%;
				clear: both;
				margin: 0; }
			
			thead {
				background-color: #BBDDEE;
				color: #000; }
			
			td {
				border-bottom-width: 1px;
				border-bottom-style: solid;
				border-bottom-color: #ccc;
				padding: 7px 15px 9px 10px;
				line-height: 20px;
				color: #444;
				vertical-align: top; }
			
			th {
				font-size: 13px;
				border-bottom: #9AA 1px solid;
				text-align: left;
				line-height: 25px;
				padding: 3px 6px;
				color: #000;
				font-weight: normal;
			}
			
			tr.odd {
				background-color: #f9f9f9; }
			
			img {
				padding: 2px;
				border: 1px solid #bbb; }
			
			a {
				color: #2583ad;
				text-decoration: none; }

			a:hover {
				color: #d54e21; }
		</style>
	</head>
	<body>
		<div id="wrap">
			<h1>Image Sitemap</h1>
			
			<div id="content">
				<table>
				<thead>
					<tr>
						<th>Image</th>
						<th>Landing Page</th>
					</tr>
				</thead>
				<tbody>
					<xsl:for-each select="sitemap:urlset/sitemap:url">
						<tr>
							<xsl:if test="position() mod 2 = 1">
								<xsl:attribute name="class">odd</xsl:attribute>
							</xsl:if>

							<td>
								<xsl:variable name="imageURL">
									<xsl:value-of select="image:image/image:loc"/>
								</xsl:variable>
								<xsl:variable name="itemURL">
									<xsl:value-of select="sitemap:loc"/>
								</xsl:variable>
								
								<a href="{$imageURL}"><img src="{$imageURL}" style="width: 120px;" /></a>
							</td>
							
							<td>
								<xsl:variable name="itemURL">
									<xsl:value-of select="sitemap:loc"/>
								</xsl:variable>
								<a href="{$itemURL}">
									<xsl:value-of select="sitemap:loc"/>
								</a>
							</td>
						</tr>
					</xsl:for-each>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>
</xsl:template>

</xsl:stylesheet>
