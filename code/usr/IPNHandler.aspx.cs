﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Xml;
using System.Xml.Schema;

public partial class IPNHandler : System.Web.UI.Page
{
    #region "Properties"
    protected string strCurrency = "";
    protected DataSet dsResponse;
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        new Paypal().IPN();
    }
    #endregion

}