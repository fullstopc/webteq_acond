﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/public_sub_one_column.master" AutoEventWireup="true" CodeFile="gallery.aspx.cs" Inherits="usr_gallery" ValidateRequest="false" %>
<%@ MasterType VirtualPath="~/mst/public_sub_one_column.master" %>
<%@ Register Src="~/ctrl/ucUsrCMS.ascx" TagName="UsrCMS" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrEvent.ascx" TagName="UsrEvent" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrIndEvent.ascx" TagName="UsrIndEvent" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <asp:Panel ID="pnlCMS" runat="server" CssClass="divCMSContent">
        <uc:UsrCMS ID="ucUsrCMS" runat="server" />
    </asp:Panel>
    <asp:Panel ID="pnlEvent" runat="server" CssClass="wrapper padding gap">
        <uc:UsrEvent ID="ucUsrEvent" runat="server" />
    </asp:Panel>
    <asp:Panel ID="pnlIndEvent" runat="server" Visible="false" CssClass="wrapper padding gap">
        <uc:UsrIndEvent ID="ucUsrIndEvent" runat="server" />
    </asp:Panel>
</asp:Content>