﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class usr_addCMS : System.Web.UI.Page
{
    #region "Properties"
    protected int _pageId;
    protected int _customId;
    protected int _customId2;
    protected int _blockId;
    protected int _cmsType;
    protected string _lang = "";
    protected int _adminView = 0;
    protected int _cmsGroup = 1;
    #endregion


    #region "Property Methods"
    protected string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int pageId
    {
        get
        {
            if (ViewState["PAGEID"] == null)
            {
                return _pageId;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGEID"]);
            }
        }
        set { ViewState["PAGEID"] = value; }
    }

    public int customId
    {
        get
        {
            if (ViewState["CUSTOMID"] == null)
            {
                return _customId;
            }
            else
            {
                return Convert.ToInt16(ViewState["CUSTOMID"]);
            }
        }
        set { ViewState["CUSTOMID"] = value; }
    }

    public int customId2
    {
        get
        {
            if (ViewState["CUSTOMID2"] == null)
            {
                return _customId2;
            }
            else
            {
                return Convert.ToInt16(ViewState["CUSTOMID2"]);
            }
        }
        set { ViewState["CUSTOMID2"] = value; }
    }

    public int blockId
    {
        get
        {
            if (ViewState["BLOCKID"] == null)
            {
                return _blockId;
            }
            else
            {
                return Convert.ToInt16(ViewState["BLOCKID"]);
            }
        }
        set { ViewState["BLOCKID"] = value; }
    }

    public int cmsType
    {
        get
        {
            if (ViewState["CMSTYPE"] == null)
            {
                return _cmsType;
            }
            else
            {
                return Convert.ToInt16(ViewState["CMSTYPE"]);
            }
        }
        set { ViewState["CMSTYPE"] = value; }
    }

    public string lang
    {
        get { return ViewState["LANG"] as string ?? _lang; }
        set { ViewState["LANG"] = value; }
    }

    public int adminView
    {
        get
        {
            if (ViewState["ADMINVIEW"] == null)
            {
                return _adminView;
            }
            else
            {
                return Convert.ToInt16(ViewState["ADMINVIEW"]);
            }
        }
        set { ViewState["ADMINVIEW"] = value; }
    }

    public int cmsGroup
    {
        get
        {
            if (ViewState["CMSGROUP"] == null)
            {
                return _cmsGroup;
            }
            else
            {
                return Convert.ToInt16(ViewState["CMSGROUP"]);
            }
        }
        set { ViewState["CMSGROUP"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "addcms.css' rel='Stylesheet' type='text/css' />";
        this.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["pgid"]))
            {
                int intTryParse;
                if (int.TryParse(Request["pgid"].Trim(), out intTryParse))
                {
                    pageId = intTryParse;
                }
                else
                {
                    pageId = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["cid"]))
            {
                int intTryParse;
                if (int.TryParse(Request["cid"].Trim(), out intTryParse))
                {
                    customId = intTryParse;
                }
                else
                {
                    customId = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["cid2"]))
            {
                int intTryParse;
                if (int.TryParse(Request["cid2"].Trim(), out intTryParse))
                {
                    customId2 = intTryParse;
                }
                else
                {
                    customId2 = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["bid"]))
            {
                int intTryParse;
                if (int.TryParse(Request["bid"].Trim(), out intTryParse))
                {
                    blockId = intTryParse;
                }
                else
                {
                    blockId = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["type"]))
            {
                int intTryParse;
                if (int.TryParse(Request["type"].Trim(), out intTryParse))
                {
                    clsMis mis = new clsMis();
                    if (mis.isListValueValid(intTryParse.ToString(), "CMS TYPE"))
                    {
                        cmsType = intTryParse;
                    }
                }
                else
                {
                    cmsType = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["grp"]))
            {
                int intTryParse;
                if (int.TryParse(Request["grp"].Trim(), out intTryParse))
                {
                    clsMis mis = new clsMis();
                    if (mis.isListValueValid(intTryParse.ToString(), "CMS GROUP"))
                    {
                        cmsGroup = intTryParse;
                    }
                }
            }

            if (!string.IsNullOrEmpty(Request["lang"]))
            {
                string strLang = Request["lang"].Trim();

                switch (strLang)
                {
                    case "zh":
                    case "ms":
                        lang = strLang;
                        break;
                }
            }

            if (!string.IsNullOrEmpty(Request["adm"]))
            {
                int intTryParse;
                if (int.TryParse(Request["adm"].Trim(), out intTryParse))
                {
                    if (intTryParse > 0) { adminView = 1; }
                }
                else
                {
                    adminView = 0;
                }
            }

            setPageProperties();
        }
    }

    //protected void validatechklstCMSList_server(object source, ServerValidateEventArgs args)
    //{
    //    Boolean boolValid = false;

    //    if (!string.IsNullOrEmpty(chklstCMSList.SelectedValue))
    //    {
    //        boolValid = true;
    //    }
    //    else
    //    {
    //        boolValid = false;

    //        //cvCMSList.ErrorMessage = "Please select one.";
    //    }

    //    args.IsValid = boolValid;
    //}

    protected void rptGroup_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intValue = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "LIST_VALUE"));

            HyperLink hypItem = (HyperLink)e.Item.FindControl("hypItem");

            string strCurQuery = Request.QueryString.ToString();
            string strNewQuery = clsMis.getNewQueryString(strCurQuery, "grp", intValue.ToString());

            hypItem.NavigateUrl = currentPageName + strNewQuery;

            if (intValue == cmsGroup)
            {
                hypItem.CssClass = "addCMSGroupLinkSel";

                Panel pnlGroupItemInner = (Panel)e.Item.FindControl("pnlGroupItemInner");
                pnlGroupItemInner.CssClass = "divAddCMSGroupItemInnerSel";
            }
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            clsCMSObject co = new clsCMSObject();
            clsPageObject po = new clsPageObject();
            int intRecordAffected = 0;
            Boolean boolSuccess = true;
            Boolean boolUpdated = false;

            string strDeletingCMSId = "";
            string strTempDeletingCMSId = "";
            string[] strTempDeletingCMSIdSplit;
            string strSelectedCMSId = "";

            if (!string.IsNullOrEmpty(hdnSectCMSId.Value)) { strTempDeletingCMSId = hdnSectCMSId.Value; }

            //get selected Ids
            for (int i = 0; i < chklstCMSList.Items.Count; i++)
            {
                if (chklstCMSList.Items[i].Selected)
                {
                    int intCMSId = Convert.ToInt16(chklstCMSList.Items[i].Value);

                    strSelectedCMSId += intCMSId + ",";
                }
            }

            string[] strSelectedCMSIdSplit = new string[]{};

            if (!string.IsNullOrEmpty(strSelectedCMSId))
            {
                strSelectedCMSId = strSelectedCMSId.Substring(0, strSelectedCMSId.Length - 1);
                strSelectedCMSIdSplit = strSelectedCMSId.Split((char)',');
            }
            //strSelectedCMSId = strSelectedCMSId.Substring(0, strSelectedCMSId.Length - 1);
            //string[] strSelectedCMSIdSplit = strSelectedCMSId.Split((char)',');

            //get deleting Ids
            if (!string.IsNullOrEmpty(strTempDeletingCMSId))
            {
                strTempDeletingCMSId = strTempDeletingCMSId.Substring(0, strTempDeletingCMSId.Length - 1);
                strTempDeletingCMSIdSplit = strTempDeletingCMSId.Split((char)',');

                for (int l = 0; l < strTempDeletingCMSIdSplit.Length; l++)
                {
                    int intTempDelCMSId = Convert.ToInt16(strTempDeletingCMSIdSplit[l]);
                    Boolean boolDeleting = true;

                    for (int m = 0; m < strSelectedCMSIdSplit.Length; m++)
                    {
                        if (intTempDelCMSId == Convert.ToInt16(strSelectedCMSIdSplit[m]))
                        {
                            boolDeleting = false;
                            break;
                        }
                    }

                    if (boolDeleting)
                    {
                        strDeletingCMSId += intTempDelCMSId + ",";
                    }
                }
            }

            if (!string.IsNullOrEmpty(strDeletingCMSId))
            {
                strDeletingCMSId = strDeletingCMSId.Substring(0, strDeletingCMSId.Length - 1);
                string[] strDeletingCMSIdSplit = strDeletingCMSId.Split((char)',');

                for (int k = 0; k < strDeletingCMSIdSplit.Length; k++)
                {
                    int intCMSId = Convert.ToInt16(strDeletingCMSIdSplit[k]);

                    if (!string.IsNullOrEmpty(lang)) { intRecordAffected = po.deleteItemById(pageId, customId, customId2, blockId, intCMSId, cmsType, lang, adminView, cmsGroup); }
                    else { intRecordAffected = po.deleteItemById(pageId, customId, customId2, blockId, intCMSId, cmsType, adminView, cmsGroup); }
                }
            }

            for (int j = 0; j < strSelectedCMSIdSplit.Length; j++)
            {
                int intCMSId = Convert.ToInt16(strSelectedCMSIdSplit[j]);

                Boolean boolExist = true;

                if (!string.IsNullOrEmpty(lang)) { boolExist = po.isItemExist(pageId, customId, customId2, blockId, intCMSId, cmsType, lang, adminView, cmsGroup); }
                else { boolExist = po.isItemExist(pageId, customId, customId2, blockId, intCMSId, cmsType, adminView, cmsGroup); }

                if (!boolExist)
                {
                    if (!string.IsNullOrEmpty(lang)) { intRecordAffected = po.addItem(pageId, customId, customId2, blockId, intCMSId, cmsType, lang, adminView, cmsGroup, Convert.ToInt16(Session["ADMID"])); }
                    else { intRecordAffected = po.addItem(pageId, customId, customId2, blockId, intCMSId, cmsType, adminView, cmsGroup, Convert.ToInt16(Session["ADMID"])); }
                    
                    if (intRecordAffected == 1)
                    {
                        boolUpdated = true;
                    }
                    else
                    {
                        boolSuccess = false;
                        break;
                    }
                }
            }

            if (!boolSuccess)
            {
                pnlAck.Visible = true;

                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "userErrHdr.Text") + "<br />" + GetGlobalResourceObject("GlobalResource", "userErrUpdateContent.Text") + "</div>";
            }
            else
            {
                clsMis.resetListing();

                if (!Page.ClientScript.IsStartupScriptRegistered("REFRESHPARENT"))
                {
                    string strJS = null;
                    strJS = "<script type=\"text/javascript\">";
                    strJS += "tb_close(true);";
                    strJS += "</script>";

                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "REFRESHPARENT", strJS, false);
                }
            }
        }
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        lnkbtnCancel.OnClientClick = "javascript:self.parent.tb_remove(); return false;";

        if (cmsType > 0)
        {
            if (Session["ADMID"] != null && Session["ADMID"] != "")
            {
                bindRptData();
                fillForm();

                pnlAddCMSContainerInnerIn.Visible = true;
                pnlAck.Visible = false;
            }
            else
            {
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "userErrUnauthorized.Text").ToString() + "</div>";

                pnlAddCMSContainerInnerIn.Visible = false;
                pnlAck.Visible = true;
            }
        }
        else
        {
            litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";

            pnlAddCMSContainerInnerIn.Visible = false;
            pnlAck.Visible = true;
        }
    }

    protected void bindRptData()
    {
        clsMis mis = new clsMis();
        DataSet ds = new DataSet();
        ds = mis.getListByListGrp("CMS GROUP", 1);
        DataView dv = new DataView(ds.Tables[0]);
        dv.Sort = "LIST_ORDER ASC";

        rptGroup.DataSource = dv;
        rptGroup.DataBind();
    }

    protected void fillForm()
    {
        DataSet ds = new DataSet();
        DataView dv = new DataView();

        if (cmsGroup == clsAdmin.CONSTCMSGROUPCONTENT)
        {
            if (Application["CMSLIST"] == null)
            {
                clsCMSObject co = new clsCMSObject();
                Application["CMSLIST"] = co.getCMSList(1);
            }

            ds = (DataSet)Application["CMSLIST"];
            dv = new DataView(ds.Tables[0]);

            dv.RowFilter = "CMS_TYPE = " + cmsType;
            dv.Sort = "CMS_TITLE ASC";

            chklstCMSList.DataSource = dv;
            chklstCMSList.DataTextField = "CMS_TITLE";
            chklstCMSList.DataValueField = "CMS_ID";
            chklstCMSList.DataBind();
        }
        else if (cmsGroup == clsAdmin.CONSTCMSGROUPSLIDESHOW)
        {
            if (Application["SLIDEGROUP"] == null)
            {
                clsMasthead mast = new clsMasthead();
                Application["SLIDEGROUP"] = mast.getGrpList(1);
            }

            ds = (DataSet)Application["SLIDEGROUP"];
            dv = new DataView(ds.Tables[0]);

            dv.Sort = "GRP_SHOWTOP DESC, GRP_NAME ASC";

            chklstCMSList.DataSource = dv;
            chklstCMSList.DataTextField = "GRP_NAME";
            chklstCMSList.DataValueField = "GRP_ID";
            chklstCMSList.DataBind();
        }

        if (dv.Count <= 0)
        {
            lnkbtnSave.Enabled = false;
            lnkbtnSave.CssClass = "btnAddCMSDisabled";

            pnlAddCMSListItems.Visible = false;
            pnlAddCMSListItemsNoItem.Visible = true;
        }

        if (Application["PAGEOBJECTLIST"] == null)
        {
            clsPageObject po = new clsPageObject();
            Application["PAGEOBJECTLIST"] = po.getItemList();
        }

        ds = new DataSet();
        ds = (DataSet)Application["PAGEOBJECTLIST"];
        DataTable dt = new DataTable();
        dt = ds.Tables[0];
        DataRow[] foundRow;

        foundRow = dt.Select("PAGE_ID = " + pageId + " AND CUSTOM_ID = " + customId + " AND CUSTOM_ID_2 = " + customId2 + " AND BLOCK_ID = " + blockId + " AND PAGECMS_TYPE = " + cmsType + " AND PAGECMS_ADMINVIEW = " + adminView + " AND PAGECMS_GROUP = " + cmsGroup + (!string.IsNullOrEmpty(lang) ? (" AND PAGECMS_LANG = '" + lang + "'") : ""));
        
        foreach (DataRow row in foundRow)
        {
            for (int i = 0; i < chklstCMSList.Items.Count; i++)
            {
                if (Convert.ToInt16(row["CMS_ID"]) == Convert.ToInt16(chklstCMSList.Items[i].Value))
                {
                    chklstCMSList.Items[i].Selected = true;
                    hdnSectCMSId.Value += row["CMS_ID"].ToString() + ",";
                }
            }
        }
    }
    #endregion
}
