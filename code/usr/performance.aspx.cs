﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Newtonsoft.Json;
public partial class usr_company : PersistViewStateToFileSystem
{
    #region "Properties"
    public int _sectId = 2;
    public int _pageId = 2;
    #endregion

    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["pgid"]))
        {
            int intTryParse;
            if (int.TryParse(Request["pgid"], out intTryParse))
            {
                pageId = intTryParse;
            }
        }

        Master.pageId = pageId;

        if (!IsPostBack)
        {
            clsPage page = new clsPage();
            if (page.extractPageById(pageId, 1))
            {
                if (page.parent == 0 || page.parent == -1)
                {
                    Master.parent = pageId;
                    Master.parentName = page.displayName;
                    Master.parentTemplate = page.template;
                }
                else
                {
                    Master.parent = page.parent;

                    page.extractPageById(page.parent, 1);
                    Master.parentName = page.displayName;
                    Master.parentTemplate = page.template;
                }
            }
            
            Master.sectId = sectId;
            ucUsrCMS.pageId = pageId;
            ucUsrCMS.styleSetFile = "ckeditorStylesSet.js";
            ucUsrCMS.styleSetName = "set1";
            ucUsrCMS.toolBarName = "User3";
            ucUsrCMS.cmsWidth = 720;
            ucUsrCMS.cmsCSS = "public.css";

            ucUsrCompany.fill();

            ucUsrChart.isInfoBoxHide = true;
            ucUsrChart.fill();

            ucUsrPerformance.fill();
        }
    }
    #endregion
}
