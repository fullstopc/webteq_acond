﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/mobile/publicmb_blank.master" AutoEventWireup="true" CodeFile="loginmb.aspx.cs" Inherits="usr_pagesub" ValidateRequest="false" %>
<%@ MasterType VirtualPath="~/mst/mobile/publicmb_blank.master" %>
<%@ Register Src="~/ctrl/mobile/ucUsrContent.ascx" TagName="UsrContent" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrMemberLogin.ascx" TagName="ucUsrMemberLogin" TagPrefix="uc" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
   <uc:UsrContent ID="ucUsrContent" runat="server"/>
   <div class="wrapper">
       <uc:ucUsrMemberLogin ID="ucUsrMemberLogin" runat="server" />
   </div>
</asp:Content>
