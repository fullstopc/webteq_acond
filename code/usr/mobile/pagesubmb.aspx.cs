﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using System.Web.Caching;

public partial class usr_pagesub : PersistViewStateToFileSystem
{
    #region "Properties"
    public int _sectId = 2;
    public int _pageId = 0;
    protected string _lang = "";
    #endregion

    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["pgid"]))
        {
            int intTryParse;
            if (int.TryParse(Request["pgid"], out intTryParse))
            {
                pageId = intTryParse;
            }
        }

        int intParentId = 0;

        if (!IsPostBack)
        {
            Master.sectId = sectId;
            Master.pageId = pageId;
            ucUsrContent.pageId = pageId;
            ucUsrContent.fill();
        }
        
    }

    protected override void InitializeCulture()
    {
        if (!string.IsNullOrEmpty(Request["lang"]))
        {
            lang = Request["lang"];
        }

        String selectedLanguage = "";

        switch (lang)
        {
            case "zh":
                selectedLanguage = "zh-CN";
                break;
            default:
                selectedLanguage = "en-GB";
                break;
        }

        UICulture = selectedLanguage;
        Culture = selectedLanguage;

        Thread.CurrentThread.CurrentCulture =
            CultureInfo.CreateSpecificCulture(selectedLanguage);
        Thread.CurrentThread.CurrentUICulture = new
            CultureInfo(selectedLanguage);

        base.InitializeCulture();
    }
    #endregion
}
