﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/mobile/publicmb_sub.master" AutoEventWireup="true" CodeFile="bookingmb.aspx.cs" Inherits="usr_pagesub" ValidateRequest="false" %>
<%@ MasterType VirtualPath="~/mst/mobile/publicmb_sub.master" %>
<%@ Register Src="~/ctrl/mobile/ucUsrContent.ascx" TagName="UsrContent" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrBookingStep1.ascx" TagName="UsrBookingStep1" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrBookingStep2.ascx" TagName="UsrBookingStep2" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrBookingStep3.ascx" TagName="UsrBookingStep3" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrBookingStep4.ascx" TagName="UsrBookingStep4" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <uc:UsrContent ID="ucUsrContent" runat="server"/>
   
    <div class="row ack-container" runat="server" id="pnlAck" visible="false">
        <div class="col-sm-6">
            <asp:Literal runat="server" ID="litAck"></asp:Literal>
        </div>
    </div>
    <div class="bookingform">
        <asp:Panel Visible="false" runat="server" ID="pnlBookingStep1" CssClass="container-fluid bookingform__step1">
            <uc:UsrBookingStep1 runat="server" ID="ucUsrBookingStep1" />
        </asp:Panel>
        <asp:Panel Visible="false" runat="server" ID="pnlBookingStep2" CssClass="container-fluid bookingform__step2">
            <uc:UsrBookingStep2 runat="server" ID="ucUsrBookingStep2" />
        </asp:Panel>
        <asp:Panel Visible="false" runat="server" ID="pnlBookingStep3" CssClass="container-fluid bookingform__step3">
            <uc:UsrBookingStep3 runat="server" ID="ucUsrBookingStep3" />
        </asp:Panel>
        <asp:Panel Visible="false" runat="server" ID="pnlBookingStep4" CssClass="container-fluid bookingform__step3">
            <uc:UsrBookingStep4 runat="server" ID="ucUsrBookingStep4" />
        </asp:Panel>
    </div>
    <script>
    </script>
</asp:Content>
