﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/mobile/publicmb_sub.master" AutoEventWireup="true" CodeFile="contactusmb.aspx.cs" Inherits="usr_contactus" ValidateRequest="false" MaintainScrollPositionOnPostback="false" %>
<%@ MasterType VirtualPath="~/mst/mobile/publicmb_sub.master" %>
<%@ Register Src="~/ctrl/mobile/ucUsrContent.ascx" TagName="UsrContent" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/mobile/ucUsrEnquirymb.ascx" TagName="UsrEnquiry" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <asp:Panel ID="pnlCMS" runat="server" CssClass="divCMSContent">
        <uc:UsrContent ID="ucUsrContent" runat="server"/>
    </asp:Panel>
    <div class="wrapper padding gap">
        <asp:Panel ID="pnlEnquiryForm" runat="server" CssClass="divEnquiryForm">
            <uc:UsrEnquiry ID="ucUsrEnquiry" runat="server" />
        </asp:Panel>
    </div>
</asp:Content>
