﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using System.Web.Caching;
using System.Configuration;

public partial class usr_pagesub : PersistViewStateToFileSystem
{
    #region "Properties"
    public int _sectId = 2;
    public int _pageId = 0;
    protected int _memId;
    protected Dictionary<string, object> dictReturn;
    #endregion 

    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }
    public int memId
    {
        get
        {
            if (ViewState["MEMID"] == null)
            {
                return _memId;
            }
            else
            {
                return Convert.ToInt16(ViewState["MEMID"]);
            }
        }
        set { ViewState["MEMID"] = value; }
    }

    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["pgid"]))
        {
            int intTryParse;
            if (int.TryParse(Request["pgid"], out intTryParse))
            {
                pageId = intTryParse;
            }
        }

        if (!IsPostBack)
        {
            
            Master.headerText = "";
            Master.sectId = sectId;
            Master.pageId = pageId;

            if (Session[clsKey.SESSION_MEMBER_ID] != null && Session[clsKey.SESSION_MEMBER_ID] != "")
            {
                try
                {
                    memId = Convert.ToInt16(Session[clsKey.SESSION_MEMBER_ID]);
                }
                catch
                {
                    memId = 0;
                }
            }
            else
            {
                Session["PREVURL"] = Request.Url.ToString();
                Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY  + "loginmb.aspx");
            }

            if (Session["ERROR"] != null)
            {
                litAck.Text = Session["ERROR"].ToString();
                pnlAck.Visible = true;
            }
            else if (Session["ADDED"] != null)
            {
                litAck.Text = Session["ADDED"].ToString();
                pnlAck.Visible = true;
                pnlAck.Attributes["class"] = pnlAck.Attributes["class"] + " success";
            }
            else if (Session["EDITED"] != null)
            {
                litAck.Text = Session["EDITED"].ToString();
                pnlAck.Visible = true;
                pnlAck.Attributes["class"] = pnlAck.Attributes["class"] + " success";
            }
            else if (Session["NOCHANGE"] != null)
            {
                litAck.Text = Session["NOCHANGE"].ToString();
                pnlAck.Visible = true;
            }

            fill();
        }
    }
    
    private void fill()
    {
        clsReservation reserve = new clsReservation();
        List<clsReservation> reserves = reserve.getDataTable()
                                                .AsEnumerable()
                                                .Select(x => new clsReservation() { row = x })
                                                .Where(x => x.memID == memId)
                                                .OrderByDescending(x => x.creation)
                                                .ToList();

        dictReturn = new Dictionary<string, object>() {
            {"historys", reserves },
        };

    }
    #endregion
}
