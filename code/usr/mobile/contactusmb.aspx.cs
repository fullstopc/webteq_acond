﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using System.Web.Caching;

public partial class usr_contactus : PersistViewStateToFileSystem
{
    #region "Properties"
    public int _sectId = 0;
    public int _pageId = 0;
    protected string _lang = "";
    public string _subject;
    #endregion
    
    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }

    public string subject
    {
        get { return _subject; }
        set { _subject = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["pgid"]))
        {
            int intTryParse;
            if (int.TryParse(Request["pgid"], out intTryParse))
            {
                pageId = intTryParse;
                ucUsrEnquiry.pgid = intTryParse;
            }
        }

        if (!string.IsNullOrEmpty(Request["subject"]))
        {
            subject = Request["subject"];
            ucUsrEnquiry.subject = subject;
        }

        if (!IsPostBack)
        {
            Master.sectId = sectId;
            Master.pageId = pageId;
            Master.lang = lang;

            ucUsrContent.pageId = pageId;
            ucUsrContent.fill();
        }

        ucUsrEnquiry.lang = lang;
    }

    protected override void InitializeCulture()
    {
        if (!string.IsNullOrEmpty(Request["lang"]))
        {
            lang = Request["lang"];
        }

        String selectedLanguage = "";

        switch (lang)
        {
            case "zh":
                selectedLanguage = "zh-CN";
                break;
            default:
                selectedLanguage = "en-GB";
                break;
        }

        UICulture = selectedLanguage;
        Culture = selectedLanguage;

        Thread.CurrentThread.CurrentCulture =
            CultureInfo.CreateSpecificCulture(selectedLanguage);
        Thread.CurrentThread.CurrentUICulture = new
            CultureInfo(selectedLanguage);

        base.InitializeCulture();
    }
    #endregion
}
