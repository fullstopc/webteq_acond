﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/mobile/publicmb_sub.master" AutoEventWireup="true" CodeFile="changepasswordmb.aspx.cs" Inherits="usr_pagesub" ValidateRequest="false" %>
<%@ MasterType VirtualPath="~/mst/mobile/publicmb_sub.master" %>
<%@ Register Src="~/ctrl/mobile/ucUsrContent.ascx" TagName="UsrContent" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrChangePassword.ascx" TagName="UsrChangePassword" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <uc:UsrContent ID="ucUsrContent" runat="server"/>
   
    <div class="row ack-container" runat="server" id="pnlAck" visible="false">
        <div class="col-sm-6">
            <asp:Literal runat="server" ID="litAck"></asp:Literal>
        </div>
    </div>
    <div class="student-result">
        <uc:UsrChangePassword ID="ucUsrChangePassword" runat="server" />
    </div>
</asp:Content>
