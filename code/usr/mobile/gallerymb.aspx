﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/mobile/publicmb_sub.master" AutoEventWireup="true" CodeFile="gallerymb.aspx.cs" Inherits="usr_gallery" ValidateRequest="false" %>
<%@ MasterType VirtualPath="~/mst/mobile/publicmb_sub.master" %>
<%@ Register Src="~/ctrl/mobile/ucUsrContent.ascx" TagName="UsrContent" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/mobile/ucUsrEventmb.ascx" TagName="UsrEvent" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/mobile/ucUsrIndEventmb.ascx" TagName="UsrIndEvent" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <asp:Panel ID="pnlCMS" runat="server" CssClass="divCMSContent">
         <uc:UsrContent ID="ucUsrContent" runat="server"/>
    </asp:Panel>
    <asp:Panel ID="pnlEvent" runat="server" CssClass="wrapper padding gap">
        <uc:UsrEvent ID="ucUsrEvent" runat="server" />
    </asp:Panel>
    <asp:Panel ID="pnlIndEvent" runat="server" CssClass="wrapper padding gap" Visible="false">
        <uc:UsrIndEvent ID="ucUsrIndEvent" runat="server" />
    </asp:Panel>
</asp:Content>