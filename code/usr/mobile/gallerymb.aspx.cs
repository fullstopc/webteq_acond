﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usr_gallery : PersistViewStateToFileSystem
{
    #region "Properties"
    public int _sectId = 2;
    public int _pageId = 2;
    public int _eid = 0;
    #endregion

    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public int eid
    {
        get { return _eid; }
        set { _eid = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["pgid"]))
        {
            int intTryParse;
            if (int.TryParse(Request["pgid"], out intTryParse))
            {
                pageId = intTryParse;
                ucUsrEvent.pgid = intTryParse;
                ucUsrIndEvent.pageId = intTryParse;
            }
        }

        if (!string.IsNullOrEmpty(Request["pg"]))
        {
            int intTryParse;
            if (int.TryParse(Request["pg"], out intTryParse))
            {
                ucUsrEvent.pageNo = intTryParse;
            }
        }

        if (!string.IsNullOrEmpty(Request["eid"]))
        {
            int intTryParse;
            if (int.TryParse(Request["eid"], out intTryParse))
            {
                ucUsrIndEvent.intEventId = intTryParse;
                eid = intTryParse;

                pnlEvent.Visible = false;
                pnlIndEvent.Visible = true;
            }
        }

        if (!IsPostBack)
        {
            Master.sectId = sectId;
            Master.pageId = pageId;

            ucUsrContent.pageId = pageId;
            ucUsrContent.fill();
        }
    }
    #endregion
}
