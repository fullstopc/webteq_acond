﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using System.Web.Caching;
using System.Configuration;

public partial class usr_pagesub : PersistViewStateToFileSystem
{
    #region "Properties"
    public int _sectId = 2;
    public int _pageId = 0;
    protected int _memId;
    #endregion 

    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }
    public int memId
    {
        get
        {
            if (ViewState["MEMID"] == null)
            {
                return _memId;
            }
            else
            {
                return Convert.ToInt16(ViewState["MEMID"]);
            }
        }
        set { ViewState["MEMID"] = value; }
    }

    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["pgid"]))
        {
            int intTryParse;
            if (int.TryParse(Request["pgid"], out intTryParse))
            {
                pageId = intTryParse;
            }
        }

        int intParentId = 0;

        if (pageId > 0)
        {
            clsPage page = new clsPage();
            int intCurPageId = pageId;

            while (intCurPageId != 0)
            {
                page.extractPageById(intCurPageId, 1);
                intCurPageId = page.parent;

                if (page.parent != 0)
                {
                    intParentId = page.parent;
                }
            }
        }

        if (!IsPostBack)
        {
            Master.sectId = sectId;
            Master.pageId = pageId;
            Master.parentId = intParentId;
            ucUsrContent.pageId = pageId;
            ucUsrContent.fill();

            if (Session["MEM_ID"] != null && Session["MEM_ID"] != "")
            {
                try
                {
                    memId = Convert.ToInt16(Session["MEMID"]);
                }
                catch
                {
                    memId = 0;
                }
            }
            else
            {
                Session["PREVURL"] = Request.Url.ToString();
                Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY  + "loginmb.aspx");
            }

            if (Session["ERROR"] != null)
            {
                litAck.Text = Session["ERROR"].ToString();
                pnlAck.Visible = true;
                pnlAck.Attributes["class"] = pnlAck.Attributes["class"] + " error";
            }
            else if (Session["ADDED"] != null)
            {
                litAck.Text = Session["ADDED"].ToString();
                pnlAck.Visible = true;
                pnlAck.Attributes["class"] = pnlAck.Attributes["class"] + " success";
            }
            else if (Session["EDITED"] != null)
            {
                litAck.Text = Session["EDITED"].ToString();
                pnlAck.Visible = true;
                pnlAck.Attributes["class"] = pnlAck.Attributes["class"] + " success";
            }
            else if (Session["NOCHANGE"] != null)
            {
                litAck.Text = Session["NOCHANGE"].ToString();
                pnlAck.Visible = true;
            }

            ucUsrChangePassword.isMobile = true;
            ucUsrChangePassword.fill();
        }

        Session["ADDED"] = null;
        Session["ERROR"] = null;
        Session["EDITED"] = null;
        Session["NOCHANGE"] = null;
    }
    
    #endregion
}
