﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using System.Web.Caching;

public partial class usr_pagesub : PersistViewStateToFileSystem
{
    #region "Properties"
    public int _sectId = 2;
    public int _pageId = 0;
    protected string _lang = "";
    #endregion

    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["pgid"]))
        {
            int intTryParse;
            if (int.TryParse(Request["pgid"], out intTryParse))
            {
                pageId = intTryParse;
            }
        }

        int intParentId = 0;

        if (pageId > 0)
        {
            clsPage page = new clsPage();
            int intCurPageId = pageId;

            while (intCurPageId != 0)
            {
                page.extractPageById(intCurPageId, 1);
                intCurPageId = page.parent;

                if (page.parent != 0)
                {
                    intParentId = page.parent;
                }
            }
        }

        if (!IsPostBack)
        {
            Master.sectId = sectId;
            Master.pageId = pageId;
            Master.parentId = intParentId;
            ucUsrContent.pageId = pageId;
            ucUsrContent.fill();

            if (Session["REDIRECT"] != null)
            {
                Session["REDIRECT"] = null;
                Response.Redirect("memberresultmb.aspx");
            }
            else
            {
                Session["MEM_ID"] = null;
                Session["MEM_EMAIL"] = null;
                Session["MEM_NAME"] = null;
                Session["MEM_COMP_NAME"] = null;
            }

            Session["PREVURL"] = null;

            if (Session["ERROR"] != null)
            {
                litAck.Text = Session["ERROR"].ToString();
                pnlAck.Visible = true;
                pnlAck.Attributes["class"] = pnlAck.Attributes["class"] + " error";
            }
            else if (Session["EDITED"] != null)
            {
                litAck.Text = Session["EDITED"].ToString();
                pnlAck.Visible = true;
                pnlAck.Attributes["class"] = pnlAck.Attributes["class"] + " success";
            }
            else if (Session["NOCHANGE"] != null)
            {
                litAck.Text = Session["NOCHANGE"].ToString();
                pnlAck.Visible = true;
            }

            ucUsrForgotPassword.isMobile = true;
            ucUsrForgotPassword.fill();
        }

        Master.lang = lang;
    }

    protected override void InitializeCulture()
    {
        if (!string.IsNullOrEmpty(Request["lang"]))
        {
            lang = Request["lang"];
        }

        String selectedLanguage = "";

        switch (lang)
        {
            case "zh":
                selectedLanguage = "zh-CN";
                break;
            default:
                selectedLanguage = "en-GB";
                break;
        }

        UICulture = selectedLanguage;
        Culture = selectedLanguage;

        Thread.CurrentThread.CurrentCulture =
            CultureInfo.CreateSpecificCulture(selectedLanguage);
        Thread.CurrentThread.CurrentUICulture = new
            CultureInfo(selectedLanguage);

        base.InitializeCulture();
    }
    #endregion
}
