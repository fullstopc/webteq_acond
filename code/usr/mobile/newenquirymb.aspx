﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/mobile/publicmb_sub.master" AutoEventWireup="true" CodeFile="newenquirymb.aspx.cs" Inherits="usr_newenquirymb" ValidateRequest="false" MaintainScrollPositionOnPostback="false" %>
<%@ MasterType VirtualPath="~/mst/mobile/publicmb_sub.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <asp:Panel ID="pnlEnquiryForm" runat="server" CssClass="divEnquiryForm">
         <table id="tblEnquiry" cellpadding="0" cellspacing="0" class="tblData">
             <tr>
                <td colspan="2" class="tdSectionHdr"><h1>New Enquiry</h1></td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Recipient Email:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litRecipientEmail" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="tdSplitter"></td>
                <td class="tdSplitter"></td>
            </tr>
         <%--   <tr>
                <td class="tdLabel nobr">Domain URL:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litDomainURL" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="tdSplitter"></td>
                <td class="tdSplitter"></td>
            </tr>--%>
            <tr>
                <td class="tdLabel">Received Date:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litSentDate" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="tdSplitter"></td>
                <td class="tdSplitter"></td>
            </tr>
            <tr>
                <td class="tdLabel">Salutation:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litSalutation" runat="server"></asp:Literal>
                </td>
            </tr>
           <tr>
                <td class="tdSplitter"></td>
                <td class="tdSplitter"></td>
            </tr>
            <tr>
                <td class="tdLabel">Name:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litName" runat="server"></asp:Literal>
                </td>
            </tr>
           <tr>
                <td class="tdSplitter"></td>
                <td class="tdSplitter"></td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Company Name:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litCompanyName" runat="server"></asp:Literal>
                </td>
            </tr>
          <tr>
                <td class="tdSplitter"></td>
                <td class="tdSplitter"></td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Contact:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litContact" runat="server"></asp:Literal>
                </td>
            </tr>
          <tr>
                <td class="tdSplitter"></td>
                <td class="tdSplitter"></td>
            </tr>
            <tr>
                <td class="tdLabel">Email:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litEmail" runat="server"></asp:Literal>
                </td>
            </tr>
          <tr>
                <td class="tdSplitter"></td>
                <td class="tdSplitter"></td>
            </tr>
            <tr>
                <td class="tdLabel">Subject:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litSubject" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="tdSplitter"></td>
                <td class="tdSplitter"></td>
            </tr>
            <tr>
                <td class="tdLabel">Message:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="tdSplitter"></td>
                <td class="tdSplitter"></td>
            </tr>
            <tr id="trLocation" visible="false" runat="server">
                <td colspan="2">
                    <div id="map" style="width:320px;height:400px;border:1px solid #ececec;"></div>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer2">&nbsp;</td>
                <td class="tdSpacer2">&nbsp;</td>
            </tr>
        </table>
    </asp:Panel>
    <% if (trLocation.Visible){ %>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>gomap-1.3.2.min.js" type="text/javascript"></script>
<script>
$(function() {
    $.getJSON( "http://maps.googleapis.com/maps/api/geocode/json?latlng=<%= strLat %>,<%= strLng %>&sensor=true", function( json ) {
        var addr = json.results[0].formatted_address;
        console.log(addr);
        $("#map").goMap({
            markers: [{
                latitude: <%= strLat %>,
                longitude: <%= strLng %>,
                html: { 
                    content: addr, 
                    popup: true 
                }
            }],
            zoom: 15,
            maptype: 'ROADMAP',
            scaleControl: true,
        });
     });
});
</script>
<% } %>
</asp:Content>
