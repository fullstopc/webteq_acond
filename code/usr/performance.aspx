﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/public_sub_one_column.master" AutoEventWireup="true" CodeFile="performance.aspx.cs" Inherits="usr_company" ValidateRequest="false" %>
<%@ MasterType VirtualPath="~/mst/public_sub_one_column.master" %>
<%@ Register Src="~/ctrl/ucUsrCMS.ascx" TagName="UsrCMS" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrCompany.ascx" TagName="UsrCompany" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrChart.ascx" TagName="UsrChart" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrPerformance.ascx" TagName="UsrPerformance" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
   <uc:UsrCMS ID="ucUsrCMS" runat="server" />
   <uc:UsrCompany ID="ucUsrCompany" runat="server" />
   <uc:UsrChart ID="ucUsrChart" runat="server" />
   <uc:UsrPerformance ID="ucUsrPerformance" runat="server" />
</asp:Content>
