﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/public_sub2.master" AutoEventWireup="true" CodeFile="contactus.aspx.cs" Inherits="usr_contactus" ValidateRequest="false" MaintainScrollPositionOnPostback="false" %>
<%@ MasterType VirtualPath="~/mst/public_sub2.master" %>
<%@ Register Src="~/ctrl/ucUsrCMS.ascx" TagName="UsrCMS" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrEnquiry.ascx" TagName="UsrEnquiry" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrCMSContainer.ascx" TagName="UsrCMSContainer" TagPrefix="uc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" Runat="Server">
   <asp:Panel ID="pnlCMS" runat="server" CssClass="divCMSContentContact">
        <uc:UsrCMS ID="ucUsrCMS" runat="server" />
    </asp:Panel>
    <div class="wrapper padding gap">
        <div class="contactus-container">
            <div class="contactus-info"><uc:UsrCMSContainer ID="ucUsrCMSContainer" runat="server" blockId="3" cmsType="3" /></div>
             <asp:Panel ID="pnlEnquiryForm" runat="server" CssClass="contactus-form">
                <div class="divEnquiryFormInner">
                <uc:UsrEnquiry ID="ucUsrEnquiry" runat="server" /></div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
