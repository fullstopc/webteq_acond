﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using System.Web.Caching;
using System.Configuration;
using System.Collections.Specialized;

public partial class usr_pagesub : PersistViewStateToFileSystem
{
    #region "Properties"
    public int _sectId = 2;
    public int _pageId = 0;
    protected int _memId;
    #endregion 

    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }
    public int memId
    {
        get
        {
            if (ViewState["MEMID"] == null)
            {
                return _memId;
            }
            else
            {
                return Convert.ToInt16(ViewState["MEMID"]);
            }
        }
        set { ViewState["MEMID"] = value; }
    }

    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["pgid"]))
        {
            int intTryParse;
            if (int.TryParse(Request["pgid"], out intTryParse))
            {
                pageId = intTryParse;
            }
        }

        if (!IsPostBack)
        {
            Master.headerText = "";
            Master.sectId = sectId;
            Master.pageId = pageId;
            ucUsrContent.pageId = pageId;
            ucUsrContent.fill();

            int step = 1;
            if (!string.IsNullOrEmpty(Request["step"]))
            {
                int.TryParse(Request["step"], out step);
            }

            if (!string.IsNullOrEmpty(Request["tx"]))
            {
                step = 4;
            }

            Reservation reserve = Session[clsKey.SESSION_RESERVATION] as Reservation;
            //if(reserve != null && step != reserve.step)
            //{
            //    NameValueCollection parameters = HttpUtility.ParseQueryString(Request.QueryString.ToString());
            //    parameters["step"] = reserve.step.ToString();
            //    string url = Request.Url.AbsolutePath + "?" + parameters.ToString();
            //    Response.Redirect(url);
            //}

            switch (step)
            {
                case 2:
                    pnlBookingStep2.Visible = true;
                    ucUsrBookingStep2.fill();
                    break;
                case 3:
                    pnlBookingStep3.Visible = true;
                    ucUsrBookingStep3.fill();
                    break;
                case 4:
                    pnlBookingStep4.Visible = true;
                    ucUsrBookingStep4.fill();
                    Session[clsKey.SESSION_RESERVATION] = null;
                    Session[clsKey.SESSION_RESERVATION_SUCCESS] = null;
                    break;
                default:
                    pnlBookingStep1.Visible = true;
                    ucUsrBookingStep1.fill();
                    break;

            }

            

            if (Session[clsKey.SESSION_MEMBER_ID] != null && Session[clsKey.SESSION_MEMBER_ID] != "")
            {
                try
                {
                    memId = Convert.ToInt16(Session["MEMID"]);
                }
                catch
                {
                    memId = 0;
                }
            }
            else
            {
                Session["PREVURL"] = Request.Url.ToString();
                Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY  + "loginmb.aspx");
            }

            if (Session["ERROR"] != null)
            {
                litAck.Text = Session["ERROR"].ToString();
                pnlAck.Visible = true;
            }
            else if (Session["ADDED"] != null)
            {
                litAck.Text = Session["ADDED"].ToString();
                pnlAck.Visible = true;
                pnlAck.Attributes["class"] = pnlAck.Attributes["class"] + " success";
            }
            else if (Session["EDITED"] != null)
            {
                litAck.Text = Session["EDITED"].ToString();
                pnlAck.Visible = true;
                pnlAck.Attributes["class"] = pnlAck.Attributes["class"] + " success";
            }
            else if (Session["NOCHANGE"] != null)
            {
                litAck.Text = Session["NOCHANGE"].ToString();
                pnlAck.Visible = true;
            }
        }
    }
    
    #endregion
}
