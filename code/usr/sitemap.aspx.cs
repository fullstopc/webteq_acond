﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Text;
using System.Configuration;
using System.Data;

public partial class usr_sitemap : System.Web.UI.Page
{
    #region "Properties"
    private Boolean boolLanguage = false;
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Cache.SetNoStore();
        Response.ContentType = "text/xml";
        generateSiteMap(Response.OutputStream);
        Response.End();
    }
    #endregion


    #region "Methods"
    private void addPage(XmlTextWriter xmlWriter)
    {
        clsXMLItem xi = new clsXMLItem();
        DataSet dsPage = new DataSet();
        dsPage = xi.getPageList(1);

        DataView dvPage = new DataView(dsPage.Tables[0]);
        dvPage.Sort = "PAGE_PARENT ASC, PAGE_ORDER ASC";

        string strQueryString = "";
        string strRelUrl = "";
        string strLastUpdate = ""; 
        foreach (DataRowView row in dvPage)
        {
            strQueryString = "?pgid=" + row["PAGE_ID"].ToString() + (boolLanguage ? "&lang=" + row["PAGE_LANGUAGE"].ToString() : "");
            strRelUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/" + row["PAGE_TEMPLATE"].ToString();
            strLastUpdate = (row["PAGE_LASTUPDATE"].ToString() == "" )? row["PAGE_CREATION"].ToString() : row["PAGE_LASTUPDATE"].ToString();
            
            addStaticPage(xmlWriter, strRelUrl, strQueryString, strLastUpdate, 0.5f);
        }
    }

    private void addGroup(XmlTextWriter xmlWriter)
    {
        clsXMLItem xi = new clsXMLItem();
        DataSet dsGroup = new DataSet();
        dsGroup = xi.getGrpList(1);

        DataView dvGroup = new DataView(dsGroup.Tables[0]);

        string strQueryString = "";
        string strLastUpdate = ""; 
        string strRelUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/" + clsSetting.CONSTUSRPRODUCTPAGE;
        
        foreach (DataRowView row in dvGroup)
        {
            strQueryString = "?grpid=" + row["GRP_ID"].ToString();
            strLastUpdate = (row["GRP_LASTUPDATE"].ToString() == "") ? row["GRP_CREATION"].ToString() : row["GRP_LASTUPDATE"].ToString();

            addStaticPage(xmlWriter, strRelUrl, strQueryString, strLastUpdate, 0.5f);
        }
    }

    private void addProduct(XmlTextWriter xmlWriter)
    {
        clsXMLItem xi = new clsXMLItem();
        DataSet dsProd = new DataSet();
        dsProd = xi.getProdList(1);

        DataView dvProd = new DataView(dsProd.Tables[0]);

        string strQueryString = "";
        string strLastUpdate = ""; 
        string strRelUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/" + clsSetting.CONSTUSRPRODUCTPAGE;

        foreach (DataRowView row in dvProd)
        {
            strQueryString = "?id=" + row["PROD_ID"].ToString();
            strLastUpdate = (row["PROD_LASTUPDATE"].ToString() == "") ? row["PROD_CREATION"].ToString() : row["PROD_LASTUPDATE"].ToString();

            addStaticPage(xmlWriter, strRelUrl, strQueryString, strLastUpdate, 0.5f);
        }
    }

    private void generateSiteMap(Stream stream)
    {
        XmlTextWriter siteMap = new XmlTextWriter(stream, Encoding.UTF8);

        siteMap.WriteStartDocument();

        string strPI = "type='text/xsl' href='" + ConfigurationManager.AppSettings["cssBase"] + "xml/sitemap_nocredits.xsl'";
        siteMap.WriteProcessingInstruction("xml-stylesheet", strPI);

        siteMap.WriteStartElement("urlset");
        siteMap.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
        siteMap.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        siteMap.WriteAttributeString("xsi:schemaLocation", "http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");

        addPage(siteMap);
        addGroup(siteMap);
        addProduct(siteMap);

        siteMap.WriteEndElement();
        siteMap.WriteEndDocument();
        siteMap.Close();
    }

    //Modified by Li Meng (29 DEC 2014)
    private void addStaticPage(XmlTextWriter xmlWriter, string strRelUrl, string strQueryString, string strLastUpdate, Single sigPriority)
    {
        if (File.Exists(@Server.MapPath(strRelUrl)))
        {
            string strUrl = ConfigurationManager.AppSettings["fullBase"] + strRelUrl + strQueryString;
            DateTime dtLastModified = Convert.ToDateTime(strLastUpdate); //Modified by Li Meng (29 DEC 2014)
            writeItemNode(xmlWriter, strUrl, dtLastModified, "daily", (decimal)sigPriority);
        }
    }
    //End Modified by Li Meng

    private void writeItemNode(XmlTextWriter xmlWriter, string strUrl, DateTime dtLastModified, string strChangeFrequency, decimal decPriority)
    {
        xmlWriter.WriteStartElement("url");
        xmlWriter.WriteElementString("loc", escapeUrl(strUrl));
        xmlWriter.WriteElementString("lastmod", dtLastModified.ToString("yyyy-MM-dd"));
        xmlWriter.WriteElementString("changefreq", strChangeFrequency);
        xmlWriter.WriteElementString("priority", decPriority.ToString("0.#"));
        xmlWriter.WriteEndElement();
    }

    private string escapeUrl(string strUrl)
    {
        string strUrlTemp = strUrl.Replace("&", "&amp;").Replace("'", "&apos").Replace(@"""", "&quot;").Replace(">", "&gt;").Replace("<", "&lt;");
        return strUrlTemp;
    }
    #endregion
}
