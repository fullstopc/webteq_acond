﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Text;
using System.Configuration;
using System.Data;

public partial class usr_image_sitemap : System.Web.UI.Page
{
    #region "Constants"
    const string CONSTSMNS = "http://www.sitemaps.org/schemas/sitemap/0.9";
    const string CONSTIMGNS = "http://www.google.com/schemas/sitemap-image/1.1";
    const string CONSTIMGPF = "image";
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Cache.SetNoStore();
        Response.ContentType = "text/xml";
        generateSiteMap(Response.OutputStream);
        Response.End();
    }
    #endregion


    #region "Methods"
    private void addProd(XmlTextWriter xmlWriter)
    {
        clsConfig config = new clsConfig();
        clsXMLItem xi = new clsXMLItem();
        DataSet dsProd = new DataSet();
        dsProd = xi.getProdList(1);

        DataView dvProd = new DataView(dsProd.Tables[0]);

        string strRelUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/" + clsSetting.CONSTUSRPRODUCTPAGE;
        string strQueryString = "";
        string strRelImageUrl = "";
        foreach (DataRowView row in dvProd)
        {
            strQueryString = "?id=" + row["PROD_ID"].ToString();
            string strImage = row["PROD_IMAGE"].ToString();
            if(!string.IsNullOrEmpty(strImage))
            {
                strRelImageUrl = ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMPRODFOLDER + "/" + strImage;
            }
            else
            {
                strRelImageUrl = ConfigurationManager.AppSettings["scriptBase"] + config.defaultGroupImg;
            }

            addStaticPage(xmlWriter, strRelUrl, strQueryString, strRelImageUrl);
        }
    }

    private void generateSiteMap(Stream stream)
    {
        XmlTextWriter siteMap = new XmlTextWriter(stream, Encoding.UTF8);

        siteMap.WriteStartDocument();

        string strPI = "type='text/xsl' href='" + ConfigurationManager.AppSettings["cssBase"] + "xml/image_sitemap_nocredits.xsl'";
        siteMap.WriteProcessingInstruction("xml-stylesheet", strPI);

        siteMap.WriteStartElement("urlset");
        siteMap.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        siteMap.WriteAttributeString("xsi:schemaLocation", "http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");
        siteMap.WriteAttributeString("xmlns", CONSTSMNS);
        siteMap.WriteAttributeString("xmlns:image", CONSTIMGNS);

        addProd(siteMap);

        siteMap.WriteEndElement();
        siteMap.WriteEndDocument();
        siteMap.Close();
    }

    private void addStaticPage(XmlTextWriter xmlWriter, string strRelUrl, string strQueryString, string strRelImageUrl)
    {
        if (File.Exists(@Server.MapPath(strRelUrl)))
        {
            string strUrl = ConfigurationManager.AppSettings["fullBase"] + strRelUrl + strQueryString;
            string strImageUrl = ConfigurationManager.AppSettings["fullBase"] + strRelImageUrl;
            writeItemNode(xmlWriter, strUrl, strImageUrl);
        }
    }

    private void writeItemNode(XmlTextWriter xmlWriter, string strUrl, string strImageUrl)
    {
        xmlWriter.WriteStartElement("url", CONSTSMNS);
        xmlWriter.WriteElementString("loc", CONSTSMNS, escapeUrl(strUrl));
        xmlWriter.WriteStartElement(CONSTIMGPF, "image", CONSTIMGNS);
        xmlWriter.WriteElementString(CONSTIMGPF, "loc", CONSTIMGNS, escapeUrl(strImageUrl));
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndElement();
    }

    private string escapeUrl(string strUrl)
    {
        string strUrlTemp = strUrl.Replace("&", "&amp;").Replace("'", "&apos").Replace(@"""", "&quot;").Replace(">", "&gt;").Replace("<", "&lt;");
        return strUrlTemp;
    }
    #endregion
}
