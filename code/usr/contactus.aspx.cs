﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usr_contactus : PersistViewStateToFileSystem
{
    #region "Properties"
    public int _sectId = 2;
    public int _pageId = 2;
    #endregion
    
    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["pgid"]))
        {
            int intTryParse;
            if (int.TryParse(Request["pgid"], out intTryParse))
            {
                pageId = intTryParse;
            }
        }

        Master.pageId = pageId;

        if (!IsPostBack)
        {
            Master.sectId = sectId;
            ucUsrCMS.pageId = pageId;
            ucUsrCMS.styleSetFile = "ckeditorStylesSet.js";
            ucUsrCMS.styleSetName = "set1";
            ucUsrCMS.toolBarName = "User3";
            ucUsrCMS.cmsWidth = 300;
            ucUsrCMS.cmsCSS = "public.css";

            ucUsrCMSContainer.pageId = pageId;
            ucUsrCMSContainer.fill();
        }

        
    }
    #endregion
}
