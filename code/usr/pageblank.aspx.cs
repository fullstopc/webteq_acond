﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usr_pageblank : System.Web.UI.Page
{
    #region "Properties"
    public int _sectId = 3;
    public int _pageId = 3;
    #endregion

    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["pgid"]))
        {
            int intTryParse;
            if (int.TryParse(Request["pgid"], out intTryParse))
            {
                pageId = intTryParse;
            }
        }

        if (!IsPostBack)
        {
            int intParentId = 0;

            if (pageId > 0)
            {
                clsPage page = new clsPage();
                int intCurPageId = pageId;

                while (intCurPageId != 0)
                {
                    page.extractPageById(intCurPageId, 1);
                    intCurPageId = page.parent;

                    if (page.parent != 0)
                    {
                        intParentId = page.parent;
                    }
                }
            }

            ucUsrCMS.id = pageId;
            //ucUsrCMS.sectTitle = GetLocalResourceObject("litPageTitle.Text").ToString();
            ucUsrCMS.styleSetFile = "ckeditorStylesSet.js";
            ucUsrCMS.styleSetName = "set1";
            ucUsrCMS.toolBarName = "User3";
            ucUsrCMS.cmsCSS = "public.css";
        }
    }
    #endregion
}
