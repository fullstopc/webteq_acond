﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

public partial class usr_map : System.Web.UI.Page
{
    #region "Properties"
    protected int mapNo = 0;
    #endregion


    #region "Property Methods"
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litJS = new Literal();
        litJS.Text = "<script src='http://maps.google.com/maps?file=api&amp;v=2&amp;key=" + ConfigurationManager.AppSettings["gmapkey"] + "' type='text/javascript'></script>";
        this.Page.Header.Controls.Add(litJS);

        Literal litJS2 = new Literal();
        litJS2.Text = "<script src='" + ConfigurationManager.AppSettings["jsBase"] + "loadmap.js' type='text/javascript'></script>";
        this.Page.Header.Controls.Add(litJS2);

        if (!IsPostBack)
        {
            double dblLat = 0.00;
            double dblLng = 0.00;
            double dblTryParse;

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;
                if (int.TryParse(Request["id"], out intTryParse))
                {
                    mapNo = intTryParse;
                }
            }

            if (!string.IsNullOrEmpty(Request["lat"]))
            {
                if (double.TryParse(Request["lat"], out dblTryParse))
                {
                    dblLat = dblTryParse;
                }
            }

            if (!string.IsNullOrEmpty(Request["lng"]))
            {
                if (double.TryParse(Request["lng"], out dblTryParse))
                {
                    dblLng = dblTryParse;
                }
            }

            pnlMap.Width = clsSetting.CONSTGMAPWIDTH;
            pnlMap.Height = clsSetting.CONSTGMAPHEIGHT;

            switch (mapNo)
            {
                case 1:
                    dblLat = 1.428643;
                    dblLng = 103.82656;
                    break;
                case 2:
                    dblLat = 1.336574;
                    dblLng = 103.7977;
                    break;
                case 3:
                    dblLat = 1.296083;
                    dblLng = 103.830358;
                    break;
                case 4:
                    dblLat = 1.313481;
                    dblLng = 103.901394;
                    break;
            }

            HtmlGenericControl MasterPageBodyTag = new HtmlGenericControl();
            MasterPageBodyTag = (HtmlGenericControl)Page.Master.FindControl("BodyID");
            MasterPageBodyTag.Attributes.Add("onunload", "GUnload()");
            MasterPageBodyTag.Attributes.Add("onload", "load('" + pnlMap.ClientID + "', '" + dblLat + "', '" + dblLng + "', " + clsSetting.CONSTGMAPZOOM + ")");
        }
    }
    #endregion
}
