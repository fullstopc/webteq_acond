﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/mobile/publicmb_sub.master" AutoEventWireup="true" CodeFile="historymb.aspx.cs" Inherits="usr_pagesub" ValidateRequest="false" %>
<%@ MasterType VirtualPath="~/mst/mobile/publicmb_sub.master" %>
<%@ Register Src="~/ctrl/ucUsrBookingStep1.ascx" TagName="UsrBookingStep1" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrBookingStep2.ascx" TagName="UsrBookingStep2" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrBookingStep3.ascx" TagName="UsrBookingStep3" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrBookingStep4.ascx" TagName="UsrBookingStep4" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="row ack-container" runat="server" id="pnlAck" visible="false">
        <div class="col-sm-6">
            <asp:Literal runat="server" ID="litAck"></asp:Literal>
        </div>
    </div>
    <div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="history__container">
                <div class="history__filter">
                    <div class="form-input">
                        <input type="text" placeholder="Enter Booking Code" />
                        <i class="material-icons">search</i>
                    </div>
                </div>
                <div class="history__list"></div>
            </div>
        </div>
    </div>
    </div>
    <script>
        var data = <%= Newtonsoft.Json.JsonConvert.SerializeObject(dictReturn) %>;
        var template = `
            <div class="history__item new">
                <div class="history__item__icon"><i class="material-icons">more_horiz</i></div>
                <div class="history__item__content">
                    <div class="history__item__info">
                        <div class="history__item__code">{{history.code}}</div>
                        <div class="history__item__addr">{{history.addrUnit}}</div>
                        <div class="history__item__status">{{history.status}}</div>
                    </div>
                    <div class="history__item__timestamp">
                        <div class="history__item__date">{{history.date}}</div>
                        <div class="history__item__time">{{history.time}}</div>
                    </div>
                </div>
            </div>`;
        var bindHistoryData = function(historys){
            var historyNew = JSON.parse(JSON.stringify(historys));
            var historyHTML = historyNew.map(function(history){
                //console.log(history);
                return template.replace("{{history.date}}", moment(history.serviceDatetimeFrom).format("D MMM YYYY"))
                               .replace("{{history.time}}", moment(history.serviceDatetimeFrom).format("h:mm A"))
                               .replace("{{history.code}}",history.code)
                               .replace("{{history.addrUnit}}", history.propertyUnitno)
                               .replace("{{history.status}}", "New");
            }).join("");

            if(!historyHTML){
                historyHTML = `
                    <div class="history__item new">
                        <div class="history__item__content">No Data Founded.</div>
                    </div>`;
            }


            $(".history__list").html("");
            $(".history__list").append(historyHTML);
        }
        $(function () {
            var historys = JSON.parse(JSON.stringify(data.historys));
            bindHistoryData(historys);
            
            $(".history__filter input[type='text']").keyup(function(){
                var value = $(this).val();
                var historyNew = JSON.parse(JSON.stringify(historys));

                if(value){
                    historyNew = historyNew.filter(function(history){
                        return history.code && (history.code.indexOf(value) > -1);
                    });
                }

                bindHistoryData(historyNew);
            });
        })
    </script>
</asp:Content>
