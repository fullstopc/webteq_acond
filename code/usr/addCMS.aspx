﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/public_blank.master" AutoEventWireup="true" CodeFile="addCMS.aspx.cs" Inherits="usr_addCMS" %>
<%@ MasterType VirtualPath="~/mst/public_blank.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <script type="text/javascript">
        var minNo = 1;

        function validatechklstCMSList_client(source, args) {
            var chklstCMSList = document.getElementById("<% =chklstCMSList.ClientID %>");
            var chkbox = chklstCMSList.getElementsByTagName("input");
            var count = 0;

            for (var i = 0; i < chkbox.length; i++) {
                if (chkbox[i].checked) {
                    count++;
                }
            }

            if (count >= minNo) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;

                source.errormessage = "Please select one.";
                source.innerHTML = "Please select one.";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <asp:Panel ID="pnlAddCMSContainer" runat="server" CssClass="divAddCMSContainer">
        <asp:Panel ID="pnlAddCMSContainerInner" runat="server" CssClass="divAddCMSContainerInner">
            <asp:Panel ID="pnlAck" runat="server" CssClass="divAck" Visible="false">
                <asp:Literal ID="litAck" runat="server"></asp:Literal>
            </asp:Panel>
            <asp:Panel ID="pnlAddCMSContainerInnerIn" runat="server" CssClass="divAddCMSContainerInnerIn">
                <div class="divAddCMSTitle"><asp:Literal ID="litAddCMSTitle" runat="server" meta:resourcekey="litAddCMSTitle"></asp:Literal></div>
                <asp:Panel ID="pnlAddCMSItems" runat="server" CssClass="divAddCMSItems">
                    <asp:Panel ID="pnlAddCMSItemsLeft" runat="server" CssClass="divAddCMSItemsLeft">
                        <asp:Panel ID="pnlAddCMSGroupList" runat="server" CssClass="divAddCMSGroupList">
                            <asp:Panel ID="pnlAddCMSGroupListHeader" runat="server" CssClass="divAddCMSGroupListHeader">
                                <asp:Literal ID="litGroupHeader" runat="server" meta:resourcekey="litGroupHeader"></asp:Literal>
                            </asp:Panel>
                            <asp:Panel ID="pnlAddCMSGroupListItems" runat="server" CssClass="divAddCMSGroupListItems">
                                <asp:Repeater ID="rptGroup" runat="server" OnItemDataBound="rptGroup_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:Panel ID="pnlGroupItem" runat="server" CssClass="divAddCMSGroupItem">
                                            <asp:Panel ID="pnlGroupItemInner" runat="server" CssClass="divAddCMSGroupItemInner">
                                                <asp:HyperLink ID="hypItem" runat="server" CssClass="addCMSGroupLink" Text='<%# DataBinder.Eval(Container.DataItem, "LIST_NAME") %>' ToolTip='<%# DataBinder.Eval(Container.DataItem, "LIST_NAME") %>'></asp:HyperLink>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlAddCMSItemsRight" runat="server" CssClass="divAddCMSItemsRight">
                        <asp:Panel ID="pnlCMSList" runat="server" ScrollBars="Vertical" CssClass="divCMSList">
                            <asp:Panel ID="pnlAddCMSListItems" runat="server" CssClass="divAddCMSListItems">
                                <asp:CheckBoxList ID="chklstCMSList" runat="server" ValidationGroup="vgAddCMS">
                                </asp:CheckBoxList>
                                <asp:HiddenField ID="hdnSectCMSId" runat="server" />
                                <%--<asp:CustomValidator ID="cvCMSList" runat="server" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validatechklstCMSList_client" OnServerValidate="validatechklstCMSList_server" ValidationGroup="vgAddCMS"></asp:CustomValidator>--%>
                            </asp:Panel>
                            <asp:Panel ID="pnlAddCMSListItemsNoItem" runat="server" CssClass="divAddCMSListItemsNoItem" Visible="false">
                                <asp:Literal ID="litNoItem" runat="server" meta:resourcekey="litNoItem"></asp:Literal>
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="pnlAddAction" runat="server" CssClass="divAddCMSBtn">
                    <asp:LinkButton ID="lnkbtnSave" runat="server" meta:resourcekey="lnkbtnSave" CssClass="btnAddCMS" ValidationGroup="vgAddCMS" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                    <asp:LinkButton ID="lnkbtnCancel" runat="server" meta:resourcekey="lnkbtnCancel" CssClass="btnAddCMS" CausesValidation="false" ValidationGroup="vgAddCMS"></asp:LinkButton>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>

