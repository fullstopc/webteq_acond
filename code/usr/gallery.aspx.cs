﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usr_gallery : PersistViewStateToFileSystem
{
    #region "Properties"
    public int _sectId = 2;
    public int _pageId = 2;
    public int _eid = 0;
    #endregion

    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public int eid
    {
        get { return _eid; }
        set { _eid = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["pgid"]))
        {
            int intTryParse;
            if (int.TryParse(Request["pgid"], out intTryParse))
            {
                pageId = intTryParse;
                ucUsrIndEvent.pageId = pageId;
            }
        }

        if (!string.IsNullOrEmpty(Request["pg"]))
        {
            int intTryParse;
            if (int.TryParse(Request["pg"], out intTryParse))
            {
                ucUsrEvent.pageNo = intTryParse;
            }
        }

        if (!string.IsNullOrEmpty(Request["eid"]))
        {
            int intTryParse;
            if (int.TryParse(Request["eid"], out intTryParse))
            {
                pnlIndEvent.Visible = true;
                pnlEvent.Visible = false;
                eid = intTryParse;

                ucUsrIndEvent.eid = intTryParse;
            }
        }

        if (!string.IsNullOrEmpty(Request["ppg"]))
        {
            int intTryParse;
            if (int.TryParse(Request["ppg"], out intTryParse))
            {
                ucUsrIndEvent.pageNoGall = intTryParse;
            }
        }

        if (!string.IsNullOrEmpty(Request["vpg"]))
        {
            int intTryParse;
            if (int.TryParse(Request["vpg"], out intTryParse))
            {
                ucUsrIndEvent.pageNoVid = intTryParse;
            }
        }

        if (!IsPostBack)
        {
            Master.sectId = sectId;
            Master.pageId = pageId;
            Master.eid = eid;
            ucUsrCMS.pageId = pageId;
            ucUsrCMS.styleSetFile = "ckeditorStylesSet.js";
            ucUsrCMS.styleSetName = "set1";
            ucUsrCMS.toolBarName = "User3";
            ucUsrCMS.cmsWidth = 690;
            ucUsrCMS.cmsCSS = "public.css";
        }
    }
    #endregion
}
