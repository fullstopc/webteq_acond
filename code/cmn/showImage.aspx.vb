﻿Imports System.Drawing.Imaging

Partial Class user_showImage
    Inherits System.Web.UI.Page

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim imageUrl As String = Request.QueryString("img")
        Dim imageTargetHeight As Integer = Request.QueryString("h")
        Dim imageTargetWidth As Integer = Request.QueryString("w")
        'Indicate the img is full path
        Dim imageFullPath As Boolean = IIf(Not String.IsNullOrEmpty(Request.QueryString("f")), True, False)

        'Make sure that the image URL does exist
        If String.IsNullOrEmpty(imageUrl) Then
            HttpContext.Current.Response.Write(imageUrl)
            Response.End()
        End If

        'Make sure that the image URL doesn't contain any /'s or \'s
        'If imageUrl.IndexOf("/") >= 0 Or imageUrl.IndexOf("\") >= 0 Then
        '    'We found a / or \
        '    Response.End()
        'End If

        'Add on the appropriate directory
        'imageUrl = "/images/" & imageUrl

        Dim fullSizeImg As System.Drawing.Image
        If imageFullPath Then
            fullSizeImg = System.Drawing.Image.FromFile(imageUrl)
        Else
            fullSizeImg = System.Drawing.Image.FromFile(Server.MapPath(imageUrl))
        End If


        'Do we need to create a thumbnail?
        Response.ContentType = "image/JPEG"
        If imageTargetHeight > 0 And imageTargetWidth > 0 Then
            Dim imageNewWidth As Integer
            Dim imageNewHeight As Integer
            Dim imageRatio As Double
            If fullSizeImg.Width <= imageTargetWidth And fullSizeImg.Height <= imageTargetHeight Then
                imageNewWidth = fullSizeImg.Width
                imageNewHeight = fullSizeImg.Height
            Else
                Dim imageWidthRatio As Double
                Dim imageHeightRatio As Double

                imageWidthRatio = fullSizeImg.Width / imageTargetWidth
                imageHeightRatio = fullSizeImg.Height / imageTargetHeight

                If imageWidthRatio >= imageHeightRatio Then
                    imageRatio = imageWidthRatio
                    imageNewWidth = imageTargetWidth
                    imageNewHeight = fullSizeImg.Height / imageRatio
                Else
                    imageRatio = imageHeightRatio
                    imageNewHeight = imageTargetHeight
                    imageNewWidth = fullSizeImg.Width / imageRatio
                End If
            End If

            Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
            dummyCallBack = New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)

            Dim thumbNailImg As System.Drawing.Image
            thumbNailImg = fullSizeImg.GetThumbnailImage(imageNewWidth, imageNewHeight, dummyCallBack, IntPtr.Zero)

            thumbNailImg.Save(Response.OutputStream, FileContentType(fullSizeImg))

            'Clean up / Dispose...
            thumbNailImg.Dispose()
        Else
            fullSizeImg.Save(Response.OutputStream, FileContentType(fullSizeImg))
        End If

        'Clean up / Dispose...
        fullSizeImg.Dispose()
    End Sub
#End Region

#Region "Protected Method"

    Protected Function FileContentType(ByVal imgfullSizeImg As System.Drawing.Image) As ImageFormat

        If (imgfullSizeImg.RawFormat.Equals(ImageFormat.Bmp)) Then
            Return ImageFormat.Bmp
        ElseIf (imgfullSizeImg.RawFormat.Equals(ImageFormat.Gif)) Then
            Return ImageFormat.Gif
        ElseIf (imgfullSizeImg.RawFormat.Equals(ImageFormat.Png)) Then
            Return ImageFormat.Png
        Else
            Return ImageFormat.Jpeg
        End If

    End Function



    Protected Function ThumbnailCallback() As Boolean
        Return False
    End Function
#End Region
End Class
