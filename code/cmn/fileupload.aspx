﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="fileupload.aspx.cs" Inherits="cmn_fileupload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
        <asp:Panel ID="pnlAckDesc" runat="server"><asp:Literal ID="litAck" runat="server"></asp:Literal></asp:Panel>                
    </asp:Panel>
    <asp:Panel ID="pnlForm" runat="server"> 
        <table id="tblFileUpload" cellpadding="0" cellspacing="0" class="formTbl tblData" style="padding:0px;">
            <tr>
                <td class="tdLabel nobr"><asp:Literal ID="litTitle" runat="server"></asp:Literal><span class="attention_compulsory">*</span>:</td>
                <td><asp:TextBox ID="txtFileTitle" runat="server" CssClass="text_big" MaxLength="250"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvFileTitle" runat="server" ErrorMessage="<br />Please enter the file title." ControlToValidate="txtFileTitle" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator></td>
            </tr>
            <tr id="trZH" runat="server" visible="false">
                <td class="tdLabel"><asp:Literal ID="litTitle_zh" runat="server"></asp:Literal><span class="attention_compulsory">*</span>:</td>
                <td><asp:TextBox ID="txtFileTitle_zh" runat="server" CssClass="text_big" MaxLength="250"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvFileTitle_zh" runat="server" ErrorMessage="<br />Please enter the file title (Chinese)." ControlToValidate="txtFileTitle_zh" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator></td>
            </tr>
            <tr id="trMS" runat="server" visible="false">
                <td class="tdLabel"><asp:Literal ID="litTitle_ms" runat="server"></asp:Literal><span class="attention_compulsory">*</span>:</td>
                <td><asp:TextBox ID="txtFileTitle_ms" runat="server" CssClass="text_big" MaxLength="250"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvFileTitle_ms" runat="server" ErrorMessage="<br />Please enter the file title (Malay)." ControlToValidate="txtFileTitle_ms" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Literal ID="litFile" runat="server"></asp:Literal><span class="attention_compulsory">*</span>:</td>
                <td class="tdWithFileUpload"><asp:FileUpload ID="fileUpload"  runat="server" name="upload" CssClass="fileUpload" />
                <asp:RequiredFieldValidator ID="rfvUpload" runat="server" ErrorMessage="<br />Please specify the file to be uploaded." ControlToValidate="fileUpload" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvUpload" runat="server" ControlToValidate="fileUpload" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateUpload_server" OnPreRender="cvUpload_PreRender"></asp:CustomValidator>
                <span class="spanTooltip">
                    <span class="icon"></span>
                    <span class="msg">
                        <asp:Literal ID="litFileRule" runat="server"></asp:Literal>
                    </span>
                </span>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><asp:LinkButton ID="lnkbtnUpload" runat="server" class="btn2 btnAdd" OnClick="lnkbtnUpload_Click"></asp:LinkButton></td>
            </tr>
        </table>
    </asp:Panel>                
</asp:Content>

