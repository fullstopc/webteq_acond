﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.IO;
using System.Configuration;

public partial class cmn_fileupload : System.Web.UI.Page
{
    #region "Properties"
    protected int _type = 0;
    protected int _size = 0;
    protected int _lang = 0;
    protected int _idealWidth = 0;
    protected int _idealHeight = 0;

    protected int intFileMaxLength;
    protected string strFileExt;
    #endregion


    #region "Property Methods"
    public int type
    {
        get
        {
            if (ViewState["TYPE"] == null)
            {
                return _type;
            }
            else
            {
                return Convert.ToInt16(ViewState["TYPE"]);
            }
        }
        set { ViewState["TYPE"] = value; }
    }

    public int size
    {
        get
        {
            if (ViewState["SIZE"] == null)
            {
                return _size;
            }
            else
            {
                return Convert.ToInt16(ViewState["SIZE"]);
            }
        }
        set { ViewState["SIZE"] = value; }
    }

    public int lang
    {
        get
        {
            if (ViewState["LANG"] == null)
            {
                return _lang;
            }
            else
            {
                return Convert.ToInt16(ViewState["LANG"]);
            }
        }
        set { ViewState["LANG"] = value; }
    }

    public int idealWidth
    {
        get
        {
            if (ViewState["IDEALWIDTH"] == null)
            {
                return _idealWidth;
            }
            else
            {
                return Convert.ToInt16(ViewState["IDEALWIDTH"]);
            }
        }
        set { ViewState["IDEALWIDTH"] = value; }
    }

    public int idealHeight
    {
        get
        {
            if (ViewState["IDEALHEIGHT"] == null)
            {
                return _idealHeight;
            }
            else
            {
                return Convert.ToInt16(ViewState["IDEALHEIGHT"]);
            }
        }
        set { ViewState["IDEALHEIGHT"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            if (!string.IsNullOrEmpty(Request["type"]))
            {
                int intTryParse;
                if (int.TryParse(Request["type"], out intTryParse))
                {
                    type = intTryParse;
                }
                else
                {
                    type = 0;
                }
            }
            else
            {
                type = 0;
            }

            if (!string.IsNullOrEmpty(Request["lang"]))
            {
                int intTryParse;
                if (int.TryParse(Request["lang"], out intTryParse))
                {
                    lang = intTryParse;
                }
                else
                {
                    lang = 0;
                }
            }
            else
            {
                lang = 0;
            }

            if (!string.IsNullOrEmpty(Request["size"]))
            {
                int intTryParse;
                if (int.TryParse(Request["size"], out intTryParse))
                {
                    size = intTryParse;
                }
                else { size = 0; }
            }

            if (!string.IsNullOrEmpty(Request["iw"]))
            {
                int intTryParse;
                if (int.TryParse(Request["iw"], out intTryParse))
                {
                    idealWidth = intTryParse;
                }
            }

            if (!string.IsNullOrEmpty(Request["ih"]))
            {
                int intTryParse;
                if (int.TryParse(Request["ih"], out intTryParse))
                {
                    idealHeight = intTryParse;
                }
            }

            setPageProperties();
        }
    }

    protected void lnkbtnUpload_Click(object sender, EventArgs e)
    {

        if (Page.IsValid)
        {
            Session["FILETITLE"] += txtFileTitle.Text.Trim() + clsAdmin.CONSTDEFAULTSEPERATOR;
            Session["FILETITLE_ZH"] += txtFileTitle_zh.Text.Trim() + clsAdmin.CONSTDEFAULTSEPERATOR;
            Session["FILETITLE_MS"] += txtFileTitle_ms.Text.Trim() + clsAdmin.CONSTDEFAULTSEPERATOR;

            if (fileUpload.PostedFile.FileName != "")
            {
                try
                {
                    HttpPostedFile postedFile = fileUpload.PostedFile;
                    string filename = Path.GetFileName(postedFile.FileName);
                    string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMTEMPFOLDER.ToString() + "/";
                    clsMis.createFolder(uploadPath);
                    string newFilename = clsMis.GetUniqueFilename(filename, uploadPath);
                    postedFile.SaveAs(uploadPath + newFilename);
                    Session["FILEUPLOAD"] += newFilename + clsAdmin.CONSTDEFAULTSEPERATOR;
                }
                catch (Exception ex)
                {
                    clsLog.logErroMsg(ex.Message.ToString());
                    pnlAck.Visible = true;
                    litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />" + ex.Message.ToString() + "</div>";

                    throw ex;
                }
            }
            else
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            }

            txtFileTitle.Text = string.Empty;
            txtFileTitle_zh.Text = string.Empty;
            txtFileTitle_ms.Text = string.Empty;
        }
    }

    protected void validateUpload_server(object source, ServerValidateEventArgs args)
    {
        if (fileUpload.HasFile)
        {
            int intFileMaxSize = 0;
            int intImgMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTFILESETTING] != null && Session[clsAdmin.CONSTPROJECTFILESETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTFILESETTING]) > 0)
                {
                    intFileMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTFILESETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intFileMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            switch (type)
            {
                case 1:
                    if (size > 0) { intFileMaxLength = intFileMaxSize; } //intFileMaxLength = clsAdmin.CONSTADMDOCALLOWEDMAXLENGTH;
                    else { intFileMaxLength = intImgMaxSize; } //intFileMaxLength = clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH; 

                    strFileExt = clsAdmin.CONSTADMIMGALLOWEDEXT;

                    break;
                case 2:
                    intFileMaxLength = intFileMaxSize;  //intFileMaxLength = clsAdmin.CONSTADMDOCALLOWEDMAXLENGTH;
                    strFileExt = clsAdmin.CONSTADMDOCALLOWEDEXT;

                    break;
                default:
                    intFileMaxLength = intFileMaxSize; //intFileMaxLength = clsAdmin.CONSTADMDOCALLOWEDMAXLENGTH;
                    strFileExt = clsAdmin.CONSTADMDOCIMGALLOWEDEXT;

                    break;
            }

            if (fileUpload.PostedFile.ContentLength > intFileMaxLength)
            {
                cvUpload.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(intFileMaxLength, "kb") + ".";
                args.IsValid = false;
            }
            else
            {
                Match matchImage = Regex.Match(fileUpload.FileName.ToLower(), strFileExt.ToLower(), RegexOptions.IgnoreCase);
                if (matchImage.Success)
                {
                    args.IsValid = true;
                }
                else
                {
                    cvUpload.ErrorMessage = "<br />Please enter valid file. (" + clsMis.formatAllowedFileExt(strFileExt) + ").";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cvUpload_PreRender(object sender, EventArgs e)
    {
        if ((!ClientScript.IsStartupScriptRegistered("Upload")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + fileUpload.ClientID + "', document.all['" + cvUpload.ClientID + "']);";
            strJS += "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "Upload", strJS, false);
        }
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        int intFileMaxSize = 0;
        int intImgMaxSize = 0;

        if (Session[clsAdmin.CONSTPROJECTFILESETTING] != null && Session[clsAdmin.CONSTPROJECTFILESETTING] != "")
        {
            if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTFILESETTING]) > 0)
            {
                intFileMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTFILESETTING]) * 1024;
            }
            else
            {
                if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                {
                    if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                    {
                        intFileMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                    }
                }
            }
        }

        if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
        {
            if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
            {
                intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
            }
            else
            {
                if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                {
                    if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                    {
                        intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                    }
                }
            }
        }

        switch (type)
        {
            case 1:
                if (size > 0) { intFileMaxLength = intFileMaxSize; } //intFileMaxLength = clsAdmin.CONSTADMDOCALLOWEDMAXLENGTH
                else { intFileMaxLength = intImgMaxSize; } //intFileMaxLength = clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH
                
                strFileExt = clsAdmin.CONSTADMIMGALLOWEDEXT;
                
                litTitle.Text = "Image Title";
                litTitle_zh.Text = "Image Title (Chinese)";
                litTitle_ms.Text = "Image Title (Malay)";
                litFile.Text = "Image File";
                lnkbtnUpload.Text = "Add";
                lnkbtnUpload.ToolTip = "Add";
                break;
            case 2:
                intFileMaxLength = intFileMaxSize; //intFileMaxLength = clsAdmin.CONSTADMDOCALLOWEDMAXLENGTH;
                strFileExt = clsAdmin.CONSTADMDOCALLOWEDEXT;
                
                litTitle.Text = "Document Title";
                litTitle_zh.Text = "Document Title (Chinese)";
                litTitle_ms.Text = "Document Title (Malay)";
                litFile.Text = "Document File";
                lnkbtnUpload.Text = "Add";
                lnkbtnUpload.ToolTip = "Add";
                break;
            default:
                intFileMaxLength = intFileMaxSize; //intFileMaxLength = clsAdmin.CONSTADMDOCALLOWEDMAXLENGTH;
                strFileExt = clsAdmin.CONSTADMDOCIMGALLOWEDEXT;
                
                litTitle.Text = "Article Title";
                litTitle_zh.Text = "Article Title (Chinese)";
                litTitle_ms.Text = "Article Title (Malay)";
                litFile.Text = "Article File";
                lnkbtnUpload.Text = "Add";
                lnkbtnUpload.ToolTip = "Add";
                break;
        }

        if (lang > 0)
        {
            trZH.Visible = true;
        }

        litFileRule.Text = clsMis.formatAllowedFileExt(strFileExt) + " | " + clsMis.formatAllowedFileSize(intFileMaxLength, "kb");

        if (idealWidth > 0 && idealHeight > 0)
        {
            litFileRule.Text += " | " + clsMis.formatAllowedFileDimension(idealWidth, idealHeight, "px");
        }
    }
    #endregion

}
