﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsVendor
/// </summary>
public class clsWorkingConfigWeek : Controller
{
    #region "Properties"
    public int ID { get; set; }
    public int dayChecked { get; set; }
    public int createdby { get; set; }
    public int updatedby { get; set; }

    public string day { get; set; }
    public string group { get; set; }
    #endregion

    private DataTable datatable { get; set; }

    public clsWorkingConfigWeek()
    {
        _tableName = "tb_working_config_week";
        _ID = "week_id";
        _columns = new List<Model>()
        {
            new Model(typeof(Int32)) { name = "ID", columnName = "week_id",  },
            new Model(typeof(Int16)) { name = "dayChecked", columnName = "week_checked", },
            new Model(typeof(Int32)) { name = "createdby", columnName = "week_createdby", },
            new Model(typeof(Int32)) { name = "updatedby", columnName = "week_updatedby", },

            new Model(typeof(string)) { name = "day", columnName = "week_day",  },
            new Model(typeof(string)) { name = "group", columnName = "week_group",  },
        };

        resetValue();
    }

    public List<DayTimeSlot> getDayTimeSlot()
    {
        if (datatable == null) { datatable = getDataTable(); }
        return datatable.AsEnumerable()
                        .Select(x => new DayTimeSlot {
                            WeekID = (int)x["week_id"], // Boxed use (int) implicit casting is fastest.
                            WeekGroup = Misc.FixDBNullTrim(x["week_group"].ToString())
                        })
                        .OrderBy(x => x.WeekID)
                        .ToList();
    }

}

public class DayTimeSlot 
{
    public int WeekID { get; set; }
    public string WeekGroup { get; set; }
}
