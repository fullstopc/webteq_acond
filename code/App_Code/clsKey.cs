﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsVendor
/// </summary>
public static class clsKey
{
    public static string SESSION_MEMBER_NAME = "session_member_name";
    public static string SESSION_MEMBER_EMAIL = "session_member_email";
    public static string SESSION_MEMBER_ID = "session_member_id";

    public static string SESSION_RESERVATION = "session_reservation";
    public static string SESSION_RESERVATION_SUCCESS = "session_reservation_success";

    public static string SESSION_ADDED = "session_added";
    public static string SESSION_EDITED = "session_edited";
    public static string SESSION_DELETED = "session_deleted";
    public static string SESSION_NOCHANGE = "session_nochange";
    public static string SESSION_ERROR = "session_error";
    public static string SESSION_MSG = "session_msg";

    public static int CONFIG__SERVICE_TYPE__CLEAN = 1;
    public static int CONFIG__SERVICE_TYPE__AIRCOND = 2;
    public static int CONFIG__TIMESLOT__DURATION = 30; // MINUTES

    // paypal
    public static string CONFIG__PAYPAL = "PAYPAL SETTINGS";
    public static string CONFIG__PAYPAL__NAME = "paypal";
    public static string CONFIG__PAYPAL__CMD = "cmd";
    public static string CONFIG__PAYPAL__BUSINESS = "business";
    public static string CONFIG__PAYPAL__USE_SAND_BOX = "useSandBox";
    public static string CONFIG__PAYPAL__SAND_BOX = "paypalSandBox";
    public static string CONFIG__PAYPAL__PDT_TOKEN = "PDTToken";
    public static string CONFIG__PAYPAL__CURRENCY = "currency_code";
    public static string CONFIG__PAYPAL__IPN = "usr/ipnhandler.aspx";
}
