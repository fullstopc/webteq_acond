﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for Extension
/// </summary>
public static class Misc
{
    public static string FixDBNullTrim(string value)
    {
        return string.IsNullOrEmpty(value) ? string.Empty : value.Trim();
    }

    public static string FixDBNullTrim(string value, string original)
    {
        if (string.IsNullOrEmpty(value))
        {
            return string.IsNullOrEmpty(original) ? string.Empty : original.Trim();
        }
        return value.Trim();
    }

    public static DateTime ConvertObjToDateTime(object o)
    {
        DateTime result;
        if (DateTime.TryParse((o ?? "").ToString(), out result))
        {
            return result;
        }
        else
        {
            return DateTime.MinValue;
        }
    }

    public static void AddDefault(this DropDownList dropdown, string value = "", string text = "")
    {
        dropdown.Items.Insert(0, new ListItem(text, value));
    }

    public static int ToInteger(this object value)
    {
        int result;
        if (int.TryParse((value ?? "").ToString(), out result))
        {
            return result;
        }
        return -1;
    }

    public static decimal ToDecimal(this object value)
    {
        decimal result;
        if (decimal.TryParse((value ?? "").ToString(), out result))
        {
            return result;
        }
        return -1;
    }
    public static DateTime ToDateTime(this object value)
    {
        DateTime result;
        if (DateTime.TryParse((value ?? "").ToString(), out result))
        {
            return result;
        }
        return Convert.ToDateTime("1900-01-01");
    }
    public static DateTime ToEarliestTime(this DateTime value)
    {
        return Convert.ToDateTime(value.ToString("dd MMM yyyy") + " 00:00:00");
    }
    public static DateTime ToLatestTime(this DateTime value)
    {
        return Convert.ToDateTime(value.ToString("dd MMM yyyy") + " 23:59:59");
    }
    public static string ToReadableDate(this DateTime value)
    {
        return value.ToString("dd MMM yyyy");
    }
}