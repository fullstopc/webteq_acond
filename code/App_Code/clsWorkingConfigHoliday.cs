﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsVendor
/// </summary>
public class clsWorkingConfigHoliday : Controller
{
    #region "Properties"
    public int ID { get; set; }
    public int active { get; set; }
    public int createdby { get; set; }
    public int updatedby { get; set; }

    public DateTime date { get; set; }
    public DateTime dateto { get; set; }
    public string remark { get; set; }
    #endregion

    private DataTable datatable { get; set; }

    public clsWorkingConfigHoliday()
    {
        _tableName = "tb_working_config_holiday";
        _ID = "holiday_id";
        _columns = new List<Model>()
        {
            new Model(typeof(Int32)) { name = "ID", columnName = "holiday_id",  },
            new Model(typeof(Int16)) { name = "active", columnName = "holiday_active", },
            new Model(typeof(Int32)) { name = "createdby", columnName = "holiday_createdby", },
            new Model(typeof(Int32)) { name = "updatedby", columnName = "holiday_updatedby", },

            new Model(typeof(string)) { name = "remark", columnName = "holiday_remark",  },
            new Model(typeof(DateTime)) { name = "date", columnName = "holiday_date",  },
            new Model(typeof(DateTime)) { name = "dateto", columnName = "holiday_date_to",  },
        };

        resetValue();
    }

    public bool isCrashDate()
    {
        if (datatable == null) { datatable = getDataTable(); }
        return datatable.AsEnumerable()
                        .Any(x => (date.TimeOfDay > Misc.ConvertObjToDateTime(x["holiday_date"]).TimeOfDay && date.TimeOfDay < Misc.ConvertObjToDateTime(x["holiday_date_to"]).TimeOfDay)
                               || (dateto.TimeOfDay > Misc.ConvertObjToDateTime(x["holiday_date"]).TimeOfDay && dateto.TimeOfDay < Misc.ConvertObjToDateTime(x["holiday_date_to"]).TimeOfDay));
    }

}
