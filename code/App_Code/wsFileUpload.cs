﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for wsFileUpload
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class wsFileUpload : System.Web.Services.WebService {

    public wsFileUpload () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string deleteEventGallery(string strId)
    {
        JavaScriptSerializer json = new JavaScriptSerializer();
        bool boolSuccess = false;


        if(HttpContext.Current.Session["EVENT_GALLERY"] != null)
        {
            Dictionary<string, EventGalleryDetail> dictEventGalleryDetail = json.Deserialize<Dictionary<string, EventGalleryDetail>>(HttpContext.Current.Session["EVENT_GALLERY"].ToString());

            EventGalleryDetail eventGalleryDetail = dictEventGalleryDetail[strId];
            eventGalleryDetail.isDelete = true;

            dictEventGalleryDetail[strId] = eventGalleryDetail;

            HttpContext.Current.Session["EVENT_GALLERY"] = json.Serialize(dictEventGalleryDetail);
            boolSuccess = true;
        }

        return "{\"success\": "+ (boolSuccess?"1":"0") + "}";
    }

    [WebMethod(EnableSession = true)]
    public string updateOrderEventGallery(string strId,int intOrder)
    {
        JavaScriptSerializer json = new JavaScriptSerializer();
        bool boolSuccess = false;
        
        if (HttpContext.Current.Session["EVENT_GALLERY_SORTING"] != null)
        {
            Dictionary<string, int> dictEventGalleryDetail = json.Deserialize<Dictionary<string, int>>(HttpContext.Current.Session["EVENT_GALLERY_SORTING"].ToString());

            dictEventGalleryDetail[strId] = intOrder;


            HttpContext.Current.Session["EVENT_GALLERY_SORTING"] = json.Serialize(dictEventGalleryDetail);
            boolSuccess = true;
        }

        return "{\"success\": " + (boolSuccess ? "1" : "0") + "}";
    }

    [WebMethod(EnableSession = true)]
    public string updateDescEventGallery(string strId, string strDesc)
    {
        JavaScriptSerializer json = new JavaScriptSerializer();
        bool boolSuccess = false;


        if (HttpContext.Current.Session["EVENT_GALLERY"] != null)
        {
            Dictionary<string, EventGalleryDetail> dictEventGalleryDetail = json.Deserialize<Dictionary<string, EventGalleryDetail>>(HttpContext.Current.Session["EVENT_GALLERY"].ToString());

            EventGalleryDetail eventGalleryDetail = dictEventGalleryDetail[strId];
            eventGalleryDetail.title = strDesc;

            dictEventGalleryDetail[strId] = eventGalleryDetail;

            HttpContext.Current.Session["EVENT_GALLERY"] = json.Serialize(dictEventGalleryDetail);
            boolSuccess = true;
        }

        return "{\"success\": " + (boolSuccess ? "1" : "0") + "}";
    }

    [WebMethod(EnableSession = true)]
    public string updateDescProdGallery(string strId, string strDesc)
    {
        JavaScriptSerializer json = new JavaScriptSerializer();
        bool boolSuccess = false;


        if (HttpContext.Current.Session["PROD_GALLERY"] != null)
        {
            Dictionary<string, ProdGalleryDetail> items = json.Deserialize<Dictionary<string, ProdGalleryDetail>>(HttpContext.Current.Session["PROD_GALLERY"].ToString());

            ProdGalleryDetail item = items[strId];
            item.desc = strDesc;

            items[strId] = item;

            HttpContext.Current.Session["PROD_GALLERY"] = json.Serialize(items);
            boolSuccess = true;
        }

        return "{\"success\": " + (boolSuccess ? "1" : "0") + "}";
    }

    [WebMethod(EnableSession = true)]
    public string deleteProdGallery(string strId)
    {
        JavaScriptSerializer json = new JavaScriptSerializer();
        bool boolSuccess = false;

        if (HttpContext.Current.Session["PROD_GALLERY"] != null)
        {
            Dictionary<string, ProdGalleryDetail> dictProdGalleryDetail = json.Deserialize<Dictionary<string, ProdGalleryDetail>>(HttpContext.Current.Session["PROD_GALLERY"].ToString());

            ProdGalleryDetail prodGalleryDetail = dictProdGalleryDetail[strId];
            prodGalleryDetail.isDelete = true;
            dictProdGalleryDetail[strId] = prodGalleryDetail;

            if (prodGalleryDetail.defaultImg == 1)
            {
                foreach (KeyValuePair<string, ProdGalleryDetail> entry in dictProdGalleryDetail)
                {
                    if (!entry.Value.isDelete) { updateDefaultProdGallery(entry.Value.filename); break; };
                }
            }
            
            HttpContext.Current.Session["PROD_GALLERY"] = json.Serialize(dictProdGalleryDetail);
            boolSuccess = true;
        }

        return "{\"success\": " + (boolSuccess ? "1" : "0") + "}";
    }

    [WebMethod(EnableSession = true)]
    public string updateOrderProdGallery(string strId, int intOrder)
    {
        JavaScriptSerializer json = new JavaScriptSerializer();
        bool boolSuccess = false;
        
        if (HttpContext.Current.Session["PROD_GALLERY_SORTING"] != null)
        {
            Dictionary<string, int> dictProdGalleryDetail = json.Deserialize<Dictionary<string, int>>(HttpContext.Current.Session["PROD_GALLERY_SORTING"].ToString());
            dictProdGalleryDetail[strId] = intOrder;
            
            HttpContext.Current.Session["PROD_GALLERY_SORTING"] = json.Serialize(dictProdGalleryDetail);
            boolSuccess = true;
        }

        return "{\"success\": " + (boolSuccess ? "1" : "0") + "}";
    }

    [WebMethod(EnableSession = true)]
    public string updateDefaultProdGallery(string strId)
    {
        JavaScriptSerializer json = new JavaScriptSerializer();
        bool boolSuccess = false;

        if (HttpContext.Current.Session["PROD_GALLERY_DEFAULT"] != null)
        {
            Dictionary<string, int> dictProdGalleryDetail = json.Deserialize<Dictionary<string, int>>(HttpContext.Current.Session["PROD_GALLERY_DEFAULT"].ToString());

            //Reset all Default Value to Zero
            var keys = new List<string>(dictProdGalleryDetail.Keys);
            foreach (string key in keys)
            {
                dictProdGalleryDetail[key] = 0;
            }
            
            //Assign New Default Value
            dictProdGalleryDetail[strId] = 1;
            
            HttpContext.Current.Session["PROD_GALLERY_DEFAULT"] = json.Serialize(dictProdGalleryDetail);
            boolSuccess = true;
        }

        return "{\"success\": " + (boolSuccess ? "1" : "0") + "}";
    }
}

