﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsPayment
/// </summary>
public class clsPayment : Controller
{
    #region "Properties"
    public int ID { get; set; }
    public int orderID { get; set; }
    public int gateway { get; set; }
    public string remarks { get; set; }
    public DateTime payDate { get; set; }
    public decimal total { get; set; }
    public int bank { get; set; }
    public string bankName { get; set; }
    public string bankAccount { get; set; }
    public string transID { get; set; }
    public DateTime bankDate { get; set; }
    public string email { get; set; }
    public int reject { get; set; }
    public string rejectRemarks { get; set; }
    public int active { get; set; }
    public int status { get; set; }
    #endregion

    private DataTable datatable { get; set; }

    public clsPayment()
    {
        _tableName = "tb_payment";
        _ID = "payment_id";
        _columns = new List<Model>()
        {
            new Model(typeof(Int32)) { name = "ID", columnName = "PAYMENT_ID",  },
            new Model(typeof(Int32)) { name = "orderID", columnName = "ORDER_ID", },
            new Model(typeof(Int32)) { name = "gateway", columnName = "PAYMENT_GATEWAY", },
            new Model(typeof(string)) { name = "remarks", columnName = "PAYMENT_REMARKS",  },
            new Model(typeof(DateTime)) { name = "payDate", columnName = "PAYMENT_DATE",  },
            new Model(typeof(decimal)) { name = "total", columnName = "PAYMENT_TOTAL",  },
            new Model(typeof(int)) { name = "bank", columnName = "PAYMENT_BANK",  },
            new Model(typeof(string)) { name = "bankName", columnName = "PAYMENT_BANKNAME",  },
            new Model(typeof(string)) { name = "bankAccount", columnName = "PAYMENT_BANKACCOUNT",  },
            new Model(typeof(string)) { name = "transID", columnName = "PAYMENT_TRANSID",  },
            new Model(typeof(DateTime)) { name = "bankDate", columnName = "PAYMENT_BANKDATE",  },
            new Model(typeof(string)) { name = "email", columnName = "PAYMENT_EMAIL",  },
            new Model(typeof(int)) { name = "reject", columnName = "PAYMENT_REJECT",  },
            new Model(typeof(string)) { name = "rejectRemarks", columnName = "PAYMENT_REJECTREMARKS",  },
            new Model(typeof(int)) { name = "active", columnName = "PAYMENT_ACTIVE",  },
            new Model(typeof(int)) { name = "status", columnName = "PAYMENT_STATUS",  },
        };

        resetValue();
    }

    public List<PaymentHistory> getPaymentHistoryByOrderId (int orderId)
    {
        if (datatable == null) { datatable = getDataTable(); }
        return datatable.AsEnumerable()
                        .Where(x => Convert.ToInt32(x["ORDER_ID"]) == orderId)
                        .Select(x => new PaymentHistory {
                            transID = Misc.FixDBNullTrim(x["PAYMENT_TRANSID"].ToString()),
                            payDate = Convert.ToDateTime(x["PAYMENT_DATE"]),
                            gateway = Convert.ToInt32(x["PAYMENT_GATEWAY"]),
                            total = Convert.ToDecimal(x["PAYMENT_TOTAL"]),
                            status = Convert.ToInt32(x["PAYMENT_STATUS"]),
                        }).ToList();
    }

}

public class PaymentHistory : clsPayment
{
    public string GatewayDescription 
    {
        get
        {
            var mis = new clsMis();
            var paymentType = mis.getListByListGrp("PAYMENT TYPE").Tables[0].AsEnumerable()
                             .Where(x => gateway == Convert.ToInt32(x["LIST_VALUE"]))
                             .Select(x => Misc.FixDBNullTrim(x["LIST_NAME"].ToString()));
            if (paymentType.Count() > 0)
            {
                return paymentType.FirstOrDefault();
            }
            else
            {
                return "";
            }
        }
    }

    public string StatusInText
    {
        get
        {
            var mis = new clsMis();
            var statusItems = mis.getListByListGrp("PAYMENT STATUS").Tables[0].AsEnumerable()
                             .Where(x => status == Convert.ToInt32(x["LIST_VALUE"]))
                             .Select(x => Misc.FixDBNullTrim(x["LIST_NAME"].ToString()));
            if (statusItems.Count() > 0)
            {
                return statusItems.FirstOrDefault();
            }
            else
            {
                return "";
            }
        }
    }

    public string Amount
    {
        get
        {
            var clsConfig = new clsConfig();
            return string.Format("{0} {1}", clsConfig.currency, total);
        }
    }

    public string PayDate
    {
        get
        {
            string longDateFormat = HttpContext.GetGlobalResourceObject("GlobalResource", "longDateFormat").ToString();
            return payDate.ToString(longDateFormat);
        }
    }
}