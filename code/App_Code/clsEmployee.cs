﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsVendor
/// </summary>
public class clsEmployee : Controller
{
    #region "Properties"
    public int ID { get; set; }
    public int active { get; set; }
    public int createdby { get; set; }
    public int updatedby { get; set; }
    public string name { get; set; }
    public string email { get; set; }
    public string team { get; set; }

    // contact info
    public string contactTel { get; set; }
    public string telPrefix { get; set; }

    // login info
    public string loginUsername { get; set; }
    public string loginPassword { get; set; }
    public DateTime loginTimestamp { get; set; }

    public DateTime creation { get; set; }
    public DateTime lastupdated { get; set; }
    #endregion

    private DataTable teams { get; set; }

    public clsEmployee ()
    {
        _tableName = "tb_employee";
        _ID = "employee_id";
        _columns = new List<Model>()
        {
            new Model(typeof(Int32)) { name = "ID", columnName = "employee_id",  },
            new Model(typeof(Int32)) { name = "createdby", columnName = "employee_createdby", },
            new Model(typeof(Int32)) { name = "updatedby", columnName = "employee_updatedby", },
            new Model(typeof(Int16)) { name = "active", columnName = "employee_active", },
            new Model(typeof(string)) { name = "name", columnName = "employee_name",  },
            new Model(typeof(string)) { name = "email", columnName = "employee_email",  },
            new Model(typeof(string)) { name = "contactTel", columnName = "employee_contact_tel",  },
            new Model(typeof(string)) { name = "telPrefix", columnName = "employee_tel_prefix",  },
            new Model(typeof(string)) { name = "team", columnName = "employee_team",  },
            new Model(typeof(string)) { name = "loginUsername", columnName = "employee_login_username",  },
            new Model(typeof(string)) { name = "loginPassword", columnName = "employee_login_password",  },
            new Model(typeof(DateTime)) { name = "loginTimestamp", columnName = "employee_login_timestamp",  },

            new Model(typeof(DateTime)) { name = "creation", columnName = "employee_creation",fillable = false  },
            new Model(typeof(DateTime)) { name = "lastupdated", columnName = "employee_lastupdated",fillable = false  },
        };

        resetValue();
    }

    public string[] getTeams()
    {
        if (teams == null) { teams = getDataTable(); }
        return teams.AsEnumerable()
                        .Select(x => Misc.FixDBNullTrim(x["employee_team"].ToString()))
                        .Distinct()
                        .OrderBy(x => x)
                        .ToArray();
    }
}
