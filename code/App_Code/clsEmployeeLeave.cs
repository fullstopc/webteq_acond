﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsVendor
/// </summary>
public class clsEmployeeLeave : Controller
{
    #region "Properties"
    public int ID { get; set; }
    public int type { get; set; }
    public int employeeID { get; set; }
    public int createdby { get; set; }
    public int updatedby { get; set; }
    public string reason { get; set; }
    public string group { get; set; }

    public DateTime creation { get; set; }
    public DateTime lastupdated { get; set; }
    public DateTime start { get; set; }
    public DateTime end { get; set; }

    #endregion
    public clsEmployeeLeave()
    {
        _tableName = "tb_employee_leave";
        _prefix = "leave_";
        _ID = _prefix + "id";
        _columns = new List<Model>()
        {
            new Model(typeof(Int32)) { name = "ID", columnName = _ID },
            new Model(typeof(Int32)) { name = "createdby", columnName = _prefix + "createdby", },
            new Model(typeof(Int32)) { name = "updatedby", columnName = _prefix + "updatedby", },
            new Model(typeof(Int16)) { name = "type", columnName = _prefix + "type", },
            new Model(typeof(Int16)) { name = "employeeID", columnName =  "employee_id", },

            new Model(typeof(string)) { name = "reason", columnName = _prefix + "reason",  },
            new Model(typeof(string)) { name = "group", columnName = _prefix + "group",  },

            new Model(typeof(DateTime)) { name = "start", columnName = _prefix + "start",  },
            new Model(typeof(DateTime)) { name = "end", columnName = _prefix + "end",  },
            new Model(typeof(DateTime)) { name = "creation", columnName = _prefix + "creation",fillable = false  },
            new Model(typeof(DateTime)) { name = "lastupdated", columnName = _prefix + "lastupdated",fillable = false  },
        };

        resetValue();
    }
}
