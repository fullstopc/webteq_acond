﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsVendor
/// </summary>
public class clsWorkingConfigTimeslot : Controller
{
    #region "Properties"
    public int ID { get; set; }
    public int active { get; set; }
    public int createdby { get; set; }
    public int updatedby { get; set; }

    public DateTime timeFrom { get; set; }
    public DateTime timeTo { get; set; }

    public string group { get; set; }
    #endregion

    private DataTable datatable { get; set; }

    public clsWorkingConfigTimeslot()
    {
        _tableName = "tb_working_config_time";
        _ID = "time_id";
        _columns = new List<Model>()
        {
            new Model(typeof(Int32)) { name = "ID", columnName = "time_id",  },
            new Model(typeof(Int16)) { name = "active", columnName = "time_active", },
            new Model(typeof(Int32)) { name = "createdby", columnName = "time_createdby", },
            new Model(typeof(Int32)) { name = "updatedby", columnName = "time_updatedby", },

            new Model(typeof(DateTime)) { name = "timeFrom", columnName = "time_from",  },
            new Model(typeof(DateTime)) { name = "timeTo", columnName = "time_to",  },

            new Model(typeof(string)) { name = "group", columnName = "time_group",  },
        };

        resetValue();
    }

    public int getTotalWorkingHour()
    {
        if(datatable == null) { datatable = getDataTable(); }
        return datatable.AsEnumerable()
            .Select(x => (int) (Convert.ToDateTime(x["time_from"]) - Convert.ToDateTime(x["time_to"])).TotalHours)
            .Sum();
    }

    public int getWorkingHour()
    {
        return (int)(getEarliestTimeslotStart() - getLatestTimeslotEnd()).TotalHours;
    }

    public int getTotalFreeHour()
    {
        return getWorkingHour() - getTotalWorkingHour();
    }

    public DateTime getBreaktimeStart()
    {
        return Convert.ToDateTime("1900-01-01 12:00");
    }

    public DateTime getBreaktimeEnd()
    {
        return Convert.ToDateTime("1900-01-01 14:00");
    }

    public int getBreaktimeHour()
    {
        return (int)(getBreaktimeEnd() - getBreaktimeStart()).TotalHours;
    }

    public DateTime getEarliestTimeslotStart()
    {
        if (datatable == null) { datatable = getDataTable(); }
        return datatable.AsEnumerable()
                        .Select(x => Convert.ToDateTime(x["time_from"]))
                        .OrderBy(x => x)
                        .FirstOrDefault();
    }

    public DateTime getLatestTimeslotEnd()
    {
        if (datatable == null) { datatable = getDataTable(); }
        return datatable.AsEnumerable()
                        .Select(x => Convert.ToDateTime(x["time_to"]))
                        .OrderByDescending(x => x)
                        .FirstOrDefault();
    }

    public string[] getGroupName()
    {
        if (datatable == null) { datatable = getDataTable(); }
        return datatable.AsEnumerable()
                        .Select(x => Misc.FixDBNullTrim(x["time_group"].ToString()))
                        .Distinct()
                        .OrderBy(x => x)
                        .ToArray();
    }

    public bool isCrashTime()
    {
        if (datatable == null) { datatable = getDataTable(); }
        return datatable.AsEnumerable()
                        .Any(x => (timeFrom.TimeOfDay > Misc.ConvertObjToDateTime(x["time_from"]).TimeOfDay && timeFrom.TimeOfDay < Misc.ConvertObjToDateTime(x["time_to"]).TimeOfDay) 
                               || (timeTo.TimeOfDay > Misc.ConvertObjToDateTime(x["time_from"]).TimeOfDay && timeTo.TimeOfDay < Misc.ConvertObjToDateTime(x["time_to"]).TimeOfDay));
    }
}

