﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Services;

/// <summary>
/// Summary description for recaptcha
/// </summary>
/// 
[System.Web.Script.Services.ScriptService()]
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class recaptcha : System.Web.Services.WebService {

    protected static string strReCaptcha_Secret; //System.Configuration.ConfigurationManager.AppSettings["ReCaptcha_Secret"];

    public recaptcha () {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string VerifyCaptcha(string response)
    {
        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMERECAPTCHASECRETKEY, clsConfig.CONSTGROUPGOOGLERECAPTCHASETTINGS, 1);
        strReCaptcha_Secret = config.value;

        string url = "https://www.google.com/recaptcha/api/siteverify?secret=" + strReCaptcha_Secret + "&response=" + response;
        return (new WebClient()).DownloadString(url);
    }
    
}
