﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsCurrency
/// </summary>
public class clsCurrency : dbconnBase
{
    #region "Properties"
    private int _currId;
    private string _curname;
    private string _curvalue;
    private string _curflag;
    private int _curorder;
    private int _curactive;
    private decimal _curConvRate;
    private DateTime _curCreation;
    private int _curCreatedBy;
    private DateTime _curLastUpdate;
    private int _curUpdatedBy;
    #endregion

    #region "Property Methods"
    public int currId
    {
        get { return _currId; }
        set { _currId = value; }
    }

    public string curname
    {
        get { return _curname; }
        set { _curname = value; }
    }

    public string curvalue
    {
        get { return _curvalue; }
        set { _curvalue = value; }
    }

    public string curflag
    {
        get { return _curflag; }
        set { _curflag = value; }
    }

    public int curorder
    {
        get { return _curorder; }
        set { _curorder = value; }
    }

    public int curactive
    {
        get { return _curactive; }
        set { _curactive = value; }
    }

    public decimal curConvRate
    {
        get { return _curConvRate; }
        set { _curConvRate = value; }
    }

    public DateTime curCreation
    {
        get { return _curCreation; }
        set { _curCreation = value; }
    }

    public int curCreatedBy
    {
        get { return _curCreatedBy; }
        set { _curCreatedBy = value; }
    }

    public DateTime curLastUpdate
    {
        get { return _curLastUpdate; }
        set { _curLastUpdate = value; }
    }

    public int curUpdatedBy
    {
        get { return _curUpdatedBy; }
        set { _curUpdatedBy = value; }
    }
    #endregion

    public clsCurrency()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet getCurrencyList()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CUR_ID, A.CUR_NAME, A.CUR_VALUE2, A.CUR_FLAG, A.CUR_ORDER, A.CUR_ACTIVE, A.CUR_CONVRATE," +
                     " A.CUR_CREATION, A.CUR_CREATEDBY, A.CUR_LASTUPDATE, A.CUR_UPDATEDBY" +
                     " FROM TB_CURRENCY A";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean isExactSameDataSet(int intCurId, string strCurrName, string strCurrValue, string strcurrflag, string strCurrOrder, int intActive, string strCurrConversionRate)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_CURRENCY" +
                     " WHERE CUR_ID = ?" +
                     " AND CUR_NAME = ?" +
                     " AND CUR_VALUE2 = ?" +
                     " AND CUR_FLAG = ?" +
                     " AND CUR_ORDER = ?" +
                     " AND CUR_ACTIVE = ?" +
                     " AND CUR_CONVRATE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_CUR_ID", OdbcType.BigInt, 9).Value = intCurId;
            cmd.Parameters.Add("p_CUR_NAME", OdbcType.VarChar, 150).Value = strCurrName;
            cmd.Parameters.Add("p_CUR_VALUE2", OdbcType.VarChar, 150).Value = strCurrValue;
            cmd.Parameters.Add("p_CUR_FLAG", OdbcType.VarChar, 250).Value = strcurrflag;
            cmd.Parameters.Add("p_CUR_ORDER", OdbcType.BigInt, 9).Value = strCurrOrder;
            cmd.Parameters.Add("p_CUR_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_CUR_CONVRATE", OdbcType.Decimal).Value = strCurrConversionRate;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateCurrencyById(int intCurrId, string strCurrName, string strCurrValue, string strcurrflag, string strCurrOrder, int intActive, string strCurrConversionRate, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_CURRENCY SET" +
                     " CUR_NAME = ?," +
                     " CUR_VALUE2 = ?," +
                     " CUR_FLAG = ?," +
                     " CUR_ORDER = ?," +
                     " CUR_ACTIVE = ?," +
                     " CUR_CONVRATE = ?," +
                     " CUR_UPDATEDBY = ?," +
                     " CUR_LASTUPDATE = SYSDATE()" +
                     " WHERE CUR_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CUR_NAME", OdbcType.VarChar, 150).Value = strCurrName;
            cmd.Parameters.Add("p_CUR_VALUE2", OdbcType.VarChar, 150).Value = strCurrValue;
            cmd.Parameters.Add("p_CUR_FLAG", OdbcType.VarChar, 250).Value = strcurrflag;
            cmd.Parameters.Add("p_CUR_ORDER", OdbcType.BigInt, 9).Value = strCurrOrder;
            cmd.Parameters.Add("p_CUR_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_CUR_CONVRATE", OdbcType.Decimal).Value = strCurrConversionRate;
            cmd.Parameters.Add("p_CUR_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_CUR_ID", OdbcType.BigInt, 9).Value = intCurrId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                currId = intCurrId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isCurrencyExist(string strCurrName, string strCurrValue)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT EXISTS(" +
                     "SELECT * FROM TB_CURRENCY" +
                     " WHERE CUR_NAME = ? AND CUR_VALUE2 = ?)";

            cmd.Parameters.Add("p_CUR_NAME", OdbcType.VarChar, 250).Value = strCurrName;
            cmd.Parameters.Add("p_CUR_VALUE2", OdbcType.VarChar, 250).Value = strCurrValue;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = int.Parse(cmd.ExecuteScalar().ToString());

            boolFound = intCount == 1 ? true : false;
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int addCurrency(string strCurrName, string strCurrValue, string strcurrflag, string strCurrOrder, int intActive, string strCurrConversionRate, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_CURRENCY (" +
                     "CUR_NAME," +
                     "CUR_VALUE2," +
                     "CUR_FLAG," +
                     "CUR_ORDER," +
                     "CUR_ACTIVE," +
                     "CUR_CONVRATE," +
                     "CUR_CREATION," +
                     "CUR_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CUR_NAME", OdbcType.VarChar, 150).Value = strCurrName;
            cmd.Parameters.Add("p_CUR_VALUE2", OdbcType.VarChar, 150).Value = strCurrValue;
            cmd.Parameters.Add("p_CUR_FLAG", OdbcType.VarChar, 250).Value = strcurrflag;
            cmd.Parameters.Add("p_CUR_ORDER", OdbcType.BigInt, 9).Value = strCurrOrder;
            cmd.Parameters.Add("p_CUR_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_CUR_CONVRATE", OdbcType.Decimal).Value = strCurrConversionRate;
            cmd.Parameters.Add("p_CUR_CREATEDBY", OdbcType.BigInt, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";

                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                currId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractCurById(int intId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CUR_ID, A.CUR_NAME, A.CUR_VALUE2, A.CUR_FLAG, A.CUR_ORDER, A.CUR_ACTIVE, A.CUR_CONVRATE, A.CUR_CREATION, A.CUR_CREATEDBY, A.CUR_LASTUPDATE, A.CUR_UPDATEDBY" +
                     " FROM TB_CURRENCY A" +
                     " WHERE A.CUR_ID = ?";

            cmd.Parameters.Add("p_CUR_ID", OdbcType.BigInt, 9).Value = intId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                currId = int.Parse(ds.Tables[0].Rows[0]["CUR_ID"].ToString());
                curname = ds.Tables[0].Rows[0]["CUR_NAME"].ToString();
                curvalue = ds.Tables[0].Rows[0]["CUR_VALUE2"].ToString();
                curflag = ds.Tables[0].Rows[0]["CUR_FLAG"].ToString();
                curorder = int.Parse(ds.Tables[0].Rows[0]["CUR_ORDER"].ToString());
                curactive = int.Parse(ds.Tables[0].Rows[0]["CUR_ACTIVE"].ToString());
                curConvRate = decimal.Parse(ds.Tables[0].Rows[0]["CUR_CONVRATE"].ToString());
                curCreation = ds.Tables[0].Rows[0]["CUR_CREATION"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["CUR_CREATION"].ToString()) : DateTime.MaxValue;
                curCreatedBy = int.Parse(ds.Tables[0].Rows[0]["CUR_CREATEDBY"].ToString());
                curLastUpdate = ds.Tables[0].Rows[0]["CUR_LASTUPDATE"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["CUR_LASTUPDATE"].ToString()) : DateTime.MaxValue;
                curUpdatedBy = int.Parse(ds.Tables[0].Rows[0]["CUR_UPDATEDBY"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int deleteCurById(int intId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_CURRENCY WHERE CUR_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_CUR_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateCurOrderById(int intId, int intOrder)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_CURRENCY SET" +
                     " CUR_ORDER = ?" +
                     " WHERE CUR_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CUR_ORDER", OdbcType.BigInt, 9).Value = intOrder;
            cmd.Parameters.Add("p_CUR_ID", OdbcType.BigInt, 9).Value = intId;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

}
