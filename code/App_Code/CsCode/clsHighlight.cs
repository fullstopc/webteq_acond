﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;
using System.Linq;
/// <summary>
/// Summary description for clsHighlight
/// </summary>
public class clsHighlight : dbconnBase
{
    #region "Properties"
    private int _highId;
    private string _highTitle;
    private string _highTitle_zh;
    private string _highTitle_jp;
    private string _highTitle_ms;
    private string _highTitleFURL;
    private string _highSnapshot;
    private string _highSnapshotJp;
    private string _highSnapshotMs;
    private string _highSnapshotZh;
    private string _highShortDesc;
    private string _highShortDesc_zh;
    private string _highShortDesc_jp;
    private string _highShortDesc_ms;
    private DateTime _highStartDate;
    private DateTime _highEndDate;
    private int _highGroup;
    private string _highGroupName;
    private string _highImage;
    private int _highOrder;
    private int _highShowTop;
    private int _highVisible;
    private int _highAllowComment;
    private string _highDesc;
    private string _highDesc_zh;
    private string _highDesc_jp;
    private string _highDesc_ms;
    private int _highActive;
    private int _highCreatedBy;
    private DateTime _highCreation;
    private int _highUpdatedBy;
    private DateTime _highLastUpdate;
    private int _vGroup;
    private string _vGroupName;
    private string _vGroupName_zh;
    private string _vGroupName_jp;
    private string _vGroupName_ms;
    private int _vNOC;
    private int _vNOP;
    private int _vNOV;
    private int _highGallId;
    private string _highGallThumb;
    private string _highSourceUrl;
    private string _highSourceTitle;

    // session name
    public static string CONSTEVENT_GALLERY = "EVENT_GALLERY";
    #endregion
    
    #region "Property Methods"
    public int highId
    {
        get { return _highId; }
        set { _highId = value; }
    }

    public string highTitle
    {
        get { return _highTitle; }
        set { _highTitle = value; }
    }

    public string highTitle_zh
    {
        get { return _highTitle_zh; }
        set { _highTitle_zh = value; }
    }

    public string highTitle_jp
    {
        get { return _highTitle_jp; }
        set { _highTitle_jp = value; }
    }

    public string highTitle_ms
    {
        get { return _highTitle_ms; }
        set { _highTitle_ms = value; }
    }

    public string highTitleFURL
    {
        get { return _highTitleFURL; }
        set { _highTitleFURL = value; }
    }

    public string highSnapshot
    {
        get { return _highSnapshot; }
        set { _highSnapshot = value; }
    }

    public string highSnapshotJp
    {
        get { return _highSnapshotJp; }
        set { _highSnapshotJp = value; }
    }

    public string highSnapshotMs
    {
        get { return _highSnapshotMs; }
        set { _highSnapshotMs = value; }
    }

    public string highSnapshotZh
    {
        get { return _highSnapshotZh; }
        set { _highSnapshotZh = value; }
    }
       
    public string highShortDesc
    {
        get { return _highShortDesc; }
        set { _highShortDesc = value; }
    }

    public string highShortDesc_zh
    {
        get { return _highShortDesc_zh; }
        set { _highShortDesc_zh = value; }
    }

    public string highShortDesc_jp
    {
        get { return _highShortDesc_jp; }
        set { _highShortDesc_jp = value; }
    }

    public string highShortDesc_ms
    {
        get { return _highShortDesc_ms; }
        set { _highShortDesc_ms = value; }
    }

    public DateTime highStartDate
    {
        get { return _highStartDate; }
        set { _highStartDate = value; }
    }

    public DateTime highEndDate
    {
        get { return _highEndDate; }
        set { _highEndDate = value; }
    }

    public int highGroup
    {
        get { return _highGroup; }
        set { _highGroup = value; }
    }

    public string highGroupName
    {
        get { return _highGroupName; }
        set { _highGroupName = value; }
    }

    public string highImage
    {
        get { return _highImage; }
        set { _highImage = value; }
    }

    public int highOrder
    {
        get { return _highOrder; }
        set { _highOrder = value; }
    }

    public int highShowTop
    {
        get { return _highShowTop; }
        set { _highShowTop = value; }
    }

    public int highVisible
    {
        get { return _highVisible; }
        set { _highVisible = value; }
    }

    public int highAllowComment
    {
        get { return _highAllowComment; }
        set { _highAllowComment = value; }
    }

    public string highDesc
    {
        get { return _highDesc; }
        set { _highDesc = value; }
    }

    public string highDesc_zh
    {
        get { return _highDesc_zh; }
        set { _highDesc_zh = value; }
    }

    public string highDesc_jp
    {
        get { return _highDesc_jp; }
        set { _highDesc_jp = value; }
    }

    public string highDesc_ms
    {
        get { return _highDesc_ms; }
        set { _highDesc_ms = value; }
    }

    public int highActive
    {
        get { return _highActive; }
        set { _highActive = value; }
    }

    public int highCreatedBy
    {
        get { return _highCreatedBy; }
        set { _highCreatedBy = value; }
    }

    public DateTime highCreation
    {
        get { return _highCreation; }
        set { _highCreation = value; }
    }

    public int highUpdatedBy
    {
        get { return _highUpdatedBy; }
        set { _highUpdatedBy = value; }
    }

    public DateTime highLastUpdate
    {
        get { return _highLastUpdate; }
        set { _highLastUpdate = value; }
    }

    public int vGroup
    {
        get { return _vGroup; }
        set { _vGroup = value; }
    }

    public string vGroupName
    {
        get { return _vGroupName; }
        set { _vGroupName = value; }
    }

    public string vGroupName_zh
    {
        get { return _vGroupName_zh; }
        set { _vGroupName_zh = value; }
    }

    public string vGroupName_jp
    {
        get { return _vGroupName_jp; }
        set { _vGroupName_jp = value; }
    }

    public string vGroupName_ms
    {
        get { return _vGroupName_ms; }
        set { _vGroupName_ms = value; }
    }

    public int vNOC
    {
        get { return _vNOC; }
        set { _vNOC = value; }
    }

    public int vNOP
    {
        get { return _vNOP; }
        set { _vNOP = value; }
    }

    public int vNOV
    {
        get { return _vNOV; }
        set { _vNOV = value; }
    }

    public int highGallId
    {
        get { return _highGallId; }
        set { _highGallId = value; }
    }

    public string highGallThumb
    {
        get { return _highGallThumb; }
        set { _highGallThumb = value; }
    }

    public string highSourceUrl
    {
        get { return _highSourceUrl; }
        set { _highSourceUrl = value; }
    }

    public string highSourceTitle
    {
        get { return _highSourceTitle; }
        set { _highSourceTitle = value; }
    }
    #endregion

    #region "Page Events"
    public clsHighlight()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public int addHighlightDetails(string strHighTitle, string strHighTitleJp, string strHighTitleMs, string strHighTitleZh, string strHighTitleFURL, string strHighSnapshot, string strHighSnapshotJp, string strHighSnapshotMs, string strHighSnapshotZh,
                                   string strHighShortDesc, string strHighShortDescJp, string strHighShortDescMs, string strHighShortDescZh, DateTime dtHighStartDate, DateTime dtHighEndDate, string strHighImage, int intHighOrder,
                                   int intHighShowTop, int intHighVisible, int intHighAllowComment,string strSourceUrl, string strSourceTitle, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_HIGHLIGHT (" +
                     "HIGH_TITLE, " +
                     "HIGH_TITLE_JP, " +
                     "HIGH_TITLE_MS, " +
                     "HIGH_TITLE_ZH, " +
                     "HIGH_TITLEFURL, " +
                     "HIGH_SNAPSHOT, " +
                     "HIGH_SNAPSHOT_JP, " +
                     "HIGH_SNAPSHOT_MS, " +
                     "HIGH_SNAPSHOT_ZH, " +
                     "HIGH_SHORTDESC, " +
                     "HIGH_SHORTDESC_JP, " +
                     "HIGH_SHORTDESC_MS, " +
                     "HIGH_SHORTDESC_ZH, " +
                     "HIGH_STARTDATE, " +
                     "HIGH_ENDDATE, " +
                     "HIGH_IMAGE, " +
                     "HIGH_ORDER, " +
                     "HIGH_SHOWTOP, " +
                     "HIGH_VISIBLE, " +
                     "HIGH_ALLOWCOMMENT, " +
                     "HIGH_SOURCE_URL, " +
                     "HIGH_SOURCE_TITLE, " +
                     "HIGH_CREATEDBY, " +
                     "HIGH_CREATION" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_TITLE", OdbcType.VarChar, 250).Value = strHighTitle;
            cmd.Parameters.Add("p_HIGH_TITLE_JP", OdbcType.VarChar, 250).Value = strHighTitleJp;
            cmd.Parameters.Add("p_HIGH_TITLE_MS", OdbcType.VarChar, 250).Value = strHighTitleMs;
            cmd.Parameters.Add("p_HIGH_TITLE_ZH", OdbcType.VarChar, 250).Value = strHighTitleZh;
            cmd.Parameters.Add("p_HIGH_TITLEFURL", OdbcType.VarChar, 250).Value = strHighTitleFURL;
            cmd.Parameters.Add("p_HIGH_SNAPSHOT", OdbcType.VarChar, 1000).Value = strHighSnapshot;
            cmd.Parameters.Add("p_HIGH_SNAPSHOT_JP", OdbcType.VarChar, 1000).Value = strHighSnapshotJp;
            cmd.Parameters.Add("p_HIGH_SNAPSHOT_MS", OdbcType.VarChar, 1000).Value = strHighSnapshotMs;
            cmd.Parameters.Add("p_HIGH_SNAPSHOT_ZH", OdbcType.VarChar, 1000).Value = strHighSnapshotZh;
            cmd.Parameters.Add("p_HIGH_SHORTDESC", OdbcType.Text).Value = strHighShortDesc;
            cmd.Parameters.Add("p_HIGH_SHORTDESC_JP", OdbcType.Text).Value = strHighShortDescJp;
            cmd.Parameters.Add("p_HIGH_SHORTDESC_MS", OdbcType.Text).Value = strHighShortDescMs;
            cmd.Parameters.Add("p_HIGH_SHORTDESC_ZH", OdbcType.Text).Value = strHighShortDescZh;
            cmd.Parameters.Add("p_HIGH_STARTDATE", OdbcType.DateTime).Value = dtHighStartDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_HIGH_ENDDATE", OdbcType.DateTime).Value = dtHighEndDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_HIGH_IMAGE", OdbcType.VarChar, 250).Value = strHighImage;
            cmd.Parameters.Add("p_HIGH_ORDER", OdbcType.Int, 9).Value = intHighOrder;
            cmd.Parameters.Add("p_HIGH_SHOWTOP", OdbcType.Int, 1).Value = intHighShowTop;
            cmd.Parameters.Add("p_HIGH_VISIBLE", OdbcType.Int, 1).Value = intHighVisible;
            cmd.Parameters.Add("p_HIGH_ALLOWCOMMENT", OdbcType.Int, 1).Value = intHighAllowComment;
            cmd.Parameters.Add("p_HIGH_SOURCE_URL", OdbcType.VarChar, 500).Value = strSourceUrl == null ? "" : strSourceUrl;
            cmd.Parameters.Add("p_HIGH_SOURCE_TITLE", OdbcType.VarChar, 500).Value = strSourceTitle == null ? "" : strSourceTitle;
            cmd.Parameters.Add("p_HIGH_CREATEDBY", OdbcType.Int, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                highId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addHighlightDetails(string strHighTitle, string strHighTitle_zh, string strHighShortDesc, string strHighShortDesc_zh, DateTime dtHighStartDate, DateTime dtHighEndDate, string strHighImage, int intHighShowTop, int intHighVisible, int intHighAllowComment, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_HIGHLIGHT (" +
                     "HIGH_TITLE, " +
                     "HIGH_TITLE_ZH, " +
                     "HIGH_SHORTDESC, " +
                     "HIGH_SHORTDESC_ZH, " +
                     "HIGH_STARTDATE, " +
                     "HIGH_ENDDATE, " +
                     "HIGH_IMAGE, " +
                     "HIGH_SHOWTOP, " +
                     "HIGH_VISIBLE, " +
                     "HIGH_ALLOWCOMMENT, " +
                     "HIGH_CREATEDBY, " +
                     "HIGH_CREATION" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_TITLE", OdbcType.VarChar, 250).Value = strHighTitle;
            cmd.Parameters.Add("p_HIGH_TITLE_ZH", OdbcType.VarChar, 250).Value = strHighTitle_zh;
            cmd.Parameters.Add("p_HIGH_SHORTDESC", OdbcType.Text).Value = strHighShortDesc;
            cmd.Parameters.Add("p_HIGH_SHORTDESC_ZH", OdbcType.Text).Value = strHighShortDesc_zh;
            cmd.Parameters.Add("p_HIGH_STARTDATE", OdbcType.DateTime).Value = dtHighStartDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_HIGH_ENDDATE", OdbcType.DateTime).Value = dtHighEndDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_HIGH_IMAGE", OdbcType.VarChar, 250).Value = strHighImage;
            cmd.Parameters.Add("p_HIGH_SHOWTOP", OdbcType.Int, 1).Value = intHighShowTop;
            cmd.Parameters.Add("p_HIGH_VISIBLE", OdbcType.Int, 1).Value = intHighVisible;
            cmd.Parameters.Add("p_HIGH_ALLOWCOMMENT", OdbcType.Int, 1).Value = intHighAllowComment;
            cmd.Parameters.Add("p_HIGH_CREATEDBY", OdbcType.Int, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                highId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int assignHighlightGroup(int intHighId, int intGroupId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_HIGHGROUP (" +
                     "HIGH_ID, " +
                     "GRP_ID" +
                     ") VALUES " +
                     "(?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGroupId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addHighlightGallery(int intHighId, string strHighImageTitle, string strHighImageTitleJp, string strHighImageTitleMs, string strHighImageTitleZh, string strHighImage, int intHighOrder)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_HIGHGALLERY (" +
                     "HIGH_ID, " +
                     "HIGHGALL_TITLE, " +
                     "HIGHGALL_TITLE_JP, " +
                     "HIGHGALL_TITLE_MS, " +
                     "HIGHGALL_TITLE_ZH, " +
                     "HIGHGALL_IMAGE," +
                     "HIGHGALL_ORDER" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHGALL_TITLE", OdbcType.VarChar, 250).Value = strHighImageTitle;
            cmd.Parameters.Add("p_HIGHGALL_TITLE_JP", OdbcType.VarChar, 250).Value = strHighImageTitleJp;
            cmd.Parameters.Add("p_HIGHGALL_TITLE_MS", OdbcType.VarChar, 250).Value = strHighImageTitleMs;
            cmd.Parameters.Add("p_HIGHGALL_TITLE_ZH", OdbcType.VarChar, 250).Value = strHighImageTitleZh;
            cmd.Parameters.Add("p_HIGHGALL_IMAGE", OdbcType.VarChar, 250).Value = strHighImage;
            cmd.Parameters.Add("p_HIGHGALL_ORDER", OdbcType.Int, 9).Value = intHighOrder;
            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                highGallId = Convert.ToInt32(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addHighlightGallery(int intHighId, string strHighImageTitle, string strHighImage)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_HIGHGALLERY (" +
                     "HIGH_ID, " +
                     "HIGHGALL_TITLE, " +
                     "HIGHGALL_IMAGE" +
                     ") VALUES " +
                     "(?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHGALL_TITLE", OdbcType.VarChar, 250).Value = strHighImageTitle;
            cmd.Parameters.Add("p_HIGHGALL_IMAGE", OdbcType.VarChar, 250).Value = strHighImage;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addHighlightGallery(int intHighId, string strHighImageTitle, string strTitle_zh, string strHighImage)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_HIGHGALLERY (" +
                     "HIGH_ID, " +
                     "HIGHGALL_TITLE, " +
                     "HIGHGALL_TITLE_ZH, " +
                     "HIGHGALL_IMAGE" +
                     ") VALUES " +
                     "(?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHGALL_TITLE", OdbcType.VarChar, 250).Value = strHighImageTitle;
            cmd.Parameters.Add("p_HIGHGALL_TITLE_ZH", OdbcType.VarChar, 250).Value = strTitle_zh;
            cmd.Parameters.Add("p_HIGHGALL_IMAGE", OdbcType.VarChar, 250).Value = strHighImage;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addHighlightVideo(int intHighId, string strHighVideo, string strHighVideoThumb)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_HIGHVIDEO (" +
                     "HIGH_ID, " +
                     "HIGHVIDEO_VIDEO," +
                     "HIGHVIDEO_THUMBNAIL" +
                     ") VALUES " +
                     "(?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHVIDEO_VIDEO", OdbcType.VarChar, 1000).Value = strHighVideo;
            cmd.Parameters.Add("p_HIGHVIDEO_THUMBNAIL", OdbcType.VarChar, 1000).Value = strHighVideoThumb;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addHighlightArticle(int intHighId, string strHighArtTitle, string strHighArtFile)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_HIGHARTICLE (" +
                     "HIGH_ID, " +
                     "HIGHART_TITLE, " +
                     "HIGHART_FILE" +
                     ") VALUES " +
                     "(?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHART_TITLE", OdbcType.VarChar, 250).Value = strHighArtTitle;
            cmd.Parameters.Add("p_HIGHART_FILE", OdbcType.VarChar, 250).Value = strHighArtFile;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addHighlightArticle(int intHighId, string strHighArtTitle, string strHighArtTitle_zh, string strHighArtFile)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_HIGHARTICLE (" +
                     "HIGH_ID, " +
                     "HIGHART_TITLE, " +
                     "HIGHART_TITLE_ZH, " +
                     "HIGHART_FILE" +
                     ") VALUES " +
                     "(?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHART_TITLE", OdbcType.VarChar, 250).Value = strHighArtTitle;
            cmd.Parameters.Add("p_HIGHART_TITLE_ZH", OdbcType.VarChar, 250).Value = strHighArtTitle_zh;
            cmd.Parameters.Add("p_HIGHART_FILE", OdbcType.VarChar, 250).Value = strHighArtFile;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addHighlightComment(int intHighId, string strHighComment, int intPostedBy, string strPostedBy, string strPostedEmail)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_HIGHCOMMENT (" +
                     "HIGH_ID, " +
                     "HIGHCOMM_COMMENT, " +
                     "HIGHCOMM_POSTEDBY, " +
                     "HIGHCOMM_POSTEDBYNAME, " +
                     "HIGHCOMM_EMAIL, " +
                     "HIGHCOMM_POSTEDDATE" +
                     ") VALUES " +
                     "(?,?,?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHCOMM_COMMENT", OdbcType.VarChar, 1000).Value = strHighComment;
            cmd.Parameters.Add("p_HIGHCOMM_POSTEDBY", OdbcType.Int, 9).Value = intPostedBy;
            cmd.Parameters.Add("p_HIGHCOMM_POSTEDBYNAME", OdbcType.VarChar, 250).Value = strPostedBy;
            cmd.Parameters.Add("p_HIGHCOMM_EMAIL", OdbcType.VarChar, 250).Value = strPostedEmail;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addHighlightReply(int intHighCommentId, int intHighId, string strHighReply, int intRepliedBy, string strRepliedBy, string strRepliedEmail)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_HIGHREPLY (" +
                     "HIGHCOMM_ID, " +
                     "HIGH_ID, " +
                     "HIGHREPLY_COMMENT, " +
                     "HIGHREPLY_REPLIEDBY, " +
                     "HIGHREPLY_REPLIEDBYNAME, " +
                     "HIGHREPLY_EMAIL," +
                     "HIGHREPLY_REPLIEDDATE" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGHCOMM_ID", OdbcType.Int, 9).Value = intHighCommentId;
            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHREPLY_COMMENT", OdbcType.VarChar, 1000).Value = strHighReply;
            cmd.Parameters.Add("p_HIGHREPLY_REPLIEDBY", OdbcType.Int, 9).Value = intRepliedBy;
            cmd.Parameters.Add("p_HIGHREPLY_REPLIEDBYNAME", OdbcType.VarChar, 250).Value = strRepliedBy;
            cmd.Parameters.Add("p_HIGHREPLY_EMAIL", OdbcType.VarChar, 250).Value = strRepliedEmail;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int countHighlightList()
    {
        int count = 0;
        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            DataSet ds = new DataSet();
            string strSql;

            strSql = "SELECT COUNT(*) AS TOTAL FROM TB_HIGHGROUP";


            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                count = int.Parse(ds.Tables[0].Rows[0]["TOTAL"].ToString());
            }


        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return count;
    }
    public Boolean extractHighlightById(int intHighId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            DataSet ds = new DataSet();
            string strSql;

            strSql = "SELECT A.HIGH_ID, A.HIGH_TITLE, A.HIGH_TITLE_ZH, A.HIGH_TITLE_JP, A.HIGH_TITLE_MS,  A.HIGH_TITLEFURL, A.HIGH_SNAPSHOT, A.HIGH_SNAPSHOT_JP, A.HIGH_SNAPSHOT_MS, A.HIGH_SNAPSHOT_ZH," +
                     " A.HIGH_SHORTDESC, A.HIGH_SHORTDESC_ZH, A.HIGH_SHORTDESC_JP, A.HIGH_SHORTDESC_MS, A.HIGH_STARTDATE, A.HIGH_ENDDATE, A.HIGH_IMAGE, A.HIGH_ORDER, A.HIGH_SHOWTOP, A.HIGH_VISIBLE," +
                     " A.HIGH_ALLOWCOMMENT, A.HIGH_DESC, A.HIGH_DESC_ZH, A.HIGH_DESC_JP, A.HIGH_DESC_MS, A.HIGH_ACTIVE, A.HIGH_CREATEDBY, A.HIGH_CREATION, A.HIGH_UPDATEDBY, A.HIGH_LASTUPDATE," +
                     " B.GRP_ID AS V_GRP, B.GRP_NAME AS V_GRPNAME, B.GRP_NAME_ZH AS V_GRPNAME_ZH, B.GRP_NAME_JP AS V_GRPNAME_JP, B.GRP_NAME_MS AS V_GRPNAME_MS," +
                     " A.HIGH_SOURCE_URL, A.HIGH_SOURCE_TITLE" +
                     " FROM (TB_HIGHLIGHT A LEFT JOIN VW_HIGHGROUP B ON (A.HIGH_ID = B.HIGH_ID))" +
                     " WHERE A.HIGH_ID = ?" +
                     " GROUP BY A.HIGH_ID";


            //strSql = "SELECT A.HIGH_ID, A.HIGH_TITLE, A.HIGH_TITLE_ZH, A.HIGH_TITLE_JP, A.HIGH_TITLE_MS, A.HIGH_SNAPSHOT, A.HIGH_SNAPSHOT_JP, A.HIGH_SNAPSHOT_MS, A.HIGH_SNAPSHOT_ZH," +
            //         " A.HIGH_SHORTDESC, A.HIGH_SHORTDESC_ZH, A.HIGH_SHORTDESC_JP, A.HIGH_SHORTDESC_MS, A.HIGH_STARTDATE, A.HIGH_ENDDATE, A.HIGH_IMAGE, A.HIGH_ORDER, A.HIGH_SHOWTOP, A.HIGH_VISIBLE," +
            //         " A.HIGH_ALLOWCOMMENT, A.HIGH_DESC, A.HIGH_DESC_ZH, A.HIGH_DESC_JP, A.HIGH_DESC_MS, A.HIGH_ACTIVE, A.HIGH_CREATEDBY, A.HIGH_CREATION, A.HIGH_UPDATEDBY, A.HIGH_LASTUPDATE," +
            //         " A.V_GRP, A.V_GRPNAME, A.V_GRPNAME_ZH, A.V_GRPNAME_JP, A.V_GRPNAME_MS" +
            //         " FROM VW_HIGHLIGHT A" +
            //         " WHERE A.HIGH_ID = ?";

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            if (intActive != 0)
            {
                strSql += " AND A.HIGH_ACTIVE = ?";

                cmd.Parameters.Add("p_HIGH_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                highId = int.Parse(ds.Tables[0].Rows[0]["HIGH_ID"].ToString());
                highTitle = ds.Tables[0].Rows[0]["HIGH_TITLE"].ToString();
                highTitle_zh = ds.Tables[0].Rows[0]["HIGH_TITLE_ZH"].ToString();
                highTitle_jp = ds.Tables[0].Rows[0]["HIGH_TITLE_JP"].ToString();
                highTitle_ms = ds.Tables[0].Rows[0]["HIGH_TITLE_MS"].ToString();
                highTitleFURL = ds.Tables[0].Rows[0]["HIGH_TITLEFURL"].ToString();
                highSnapshot = ds.Tables[0].Rows[0]["HIGH_SNAPSHOT"].ToString();
                highSnapshotJp = ds.Tables[0].Rows[0]["HIGH_SNAPSHOT_JP"].ToString();
                highSnapshotMs = ds.Tables[0].Rows[0]["HIGH_SNAPSHOT_MS"].ToString();
                highSnapshotZh = ds.Tables[0].Rows[0]["HIGH_SNAPSHOT_ZH"].ToString();
                highShortDesc = ds.Tables[0].Rows[0]["HIGH_SHORTDESC"].ToString();
                highShortDesc_zh = ds.Tables[0].Rows[0]["HIGH_SHORTDESC_ZH"].ToString();
                highShortDesc_jp = ds.Tables[0].Rows[0]["HIGH_SHORTDESC_JP"].ToString();
                highShortDesc_ms = ds.Tables[0].Rows[0]["HIGH_SHORTDESC_MS"].ToString(); 
                highStartDate = ds.Tables[0].Rows[0]["HIGH_STARTDATE"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["HIGH_STARTDATE"].ToString()) : DateTime.MaxValue;
                highEndDate = ds.Tables[0].Rows[0]["HIGH_ENDDATE"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["HIGH_ENDDATE"].ToString()) : DateTime.MaxValue;
                highImage = ds.Tables[0].Rows[0]["HIGH_IMAGE"].ToString();
                highOrder = int.Parse(ds.Tables[0].Rows[0]["HIGH_ORDER"].ToString());
                highShowTop = int.Parse(ds.Tables[0].Rows[0]["HIGH_SHOWTOP"].ToString());
                highVisible = int.Parse(ds.Tables[0].Rows[0]["HIGH_VISIBLE"].ToString());
                highAllowComment = int.Parse(ds.Tables[0].Rows[0]["HIGH_ALLOWCOMMENT"].ToString());
                highDesc = ds.Tables[0].Rows[0]["HIGH_DESC"].ToString();
                highDesc_zh = ds.Tables[0].Rows[0]["HIGH_DESC_ZH"].ToString();
                highDesc_jp = ds.Tables[0].Rows[0]["HIGH_DESC_JP"].ToString();
                highDesc_ms = ds.Tables[0].Rows[0]["HIGH_DESC_MS"].ToString(); 
                highActive = int.Parse(ds.Tables[0].Rows[0]["HIGH_ACTIVE"].ToString());
                highCreatedBy = int.Parse(ds.Tables[0].Rows[0]["HIGH_CREATEDBY"].ToString());
                highCreation = ds.Tables[0].Rows[0]["HIGH_CREATION"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["HIGH_CREATION"].ToString()) : DateTime.MaxValue;
                highUpdatedBy = int.Parse(ds.Tables[0].Rows[0]["HIGH_UPDATEDBY"].ToString());
                highLastUpdate = ds.Tables[0].Rows[0]["HIGH_LASTUPDATE"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["HIGH_LASTUPDATE"].ToString()) : DateTime.MaxValue;
                vGroup = ds.Tables[0].Rows[0]["V_GRP"] != DBNull.Value ? int.Parse(ds.Tables[0].Rows[0]["V_GRP"].ToString()) : 0;
                vGroupName = ds.Tables[0].Rows[0]["V_GRPNAME"].ToString();
                vGroupName_zh = ds.Tables[0].Rows[0]["V_GRPNAME_ZH"].ToString();
                vGroupName_jp = ds.Tables[0].Rows[0]["V_GRPNAME_JP"].ToString();
                vGroupName_ms = ds.Tables[0].Rows[0]["V_GRPNAME_MS"].ToString();
                highSourceTitle = ds.Tables[0].Rows[0]["HIGH_SOURCE_TITLE"].ToString();
                highSourceUrl = ds.Tables[0].Rows[0]["HIGH_SOURCE_URL"].ToString();
                //vNOC = int.Parse(ds.Tables[0].Rows[0]["V_NOC"].ToString());
                //vNOP = int.Parse(ds.Tables[0].Rows[0]["V_NOP"].ToString());
                //vNOV = int.Parse(ds.Tables[0].Rows[0]["V_NOV"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public DataSet getHighlightList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT A.HIGH_ID, A.HIGH_TITLE, A.HIGH_TITLE_ZH, A.HIGH_TITLE_JP, A.HIGH_TITLE_MS, A.HIGH_SHORTDESC, A.HIGH_SHORTDESC_ZH, A.HIGH_SHORTDESC_JP, A.HIGH_SHORTDESC_MS, A.HIGH_STARTDATE, A.HIGH_ENDDATE, A.HIGH_IMAGE, A.HIGH_SHOWTOP, A.HIGH_VISIBLE," +
                     " A.HIGH_DESC, A.HIGH_DESC_ZH, A.HIGH_DESC_JP, A.HIGH_DESC_MS, A.HIGH_ACTIVE, A.HIGH_CREATEDBY, A.HIGH_CREATION, A.HIGH_UPDATEDBY, A.HIGH_LASTUPDATE," +
                     " A.V_GRP, A.V_GRPNAME, A.V_GRPNAME_ZH, A.V_GRPNAME_JP, A.V_GRPNAME_MS," +
                     " (SELECT COUNT(*) FROM TB_HIGHGALLERY B WHERE B.HIGH_ID = A.HIGH_ID) AS HIGHGALL_COUNT" +
                     " FROM VW_HIGHLIGHT A";

            if (intActive != 0)
            {
                strSql += " WHERE HIGH_ACTIVE = ?";

                cmd.Parameters.Add("p_HIGH_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getHighlightList2(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT A.HIGH_ID, A.HIGH_TITLE, A.HIGH_TITLE_ZH, A.HIGH_SHORTDESC, A.HIGH_SHORTDESC_ZH, A.HIGH_STARTDATE, A.HIGH_ENDDATE, A.HIGH_IMAGE, A.HIGH_SHOWTOP, A.HIGH_VISIBLE," +
                     " A.HIGH_DESC, A.HIGH_DESC_ZH, A.HIGH_ACTIVE, A.HIGH_CREATEDBY, A.HIGH_CREATION, A.HIGH_UPDATEDBY, A.HIGH_LASTUPDATE," +
                     " A.V_GRP, A.V_GRPNAME, A.V_GRPNAME_ZH" +
                     " FROM VW_HIGHLIGHT A" +
                     " WHERE  A.HIGH_VISIBLE = 1 " +
                     " ORDER BY A.HIGH_LASTUPDATE DESC" +
                     " LIMIT 1";
            if (intActive != 0)
            {
                strSql += " WHERE HIGH_ACTIVE = ?";

                cmd.Parameters.Add("p_HIGH_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getHighlightGallery()
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT HIGH_ID, HIGHGALL_TITLE, HIGHGALL_TITLE_ZH, HIGHGALL_IMAGE" +
                     " FROM TB_HIGHGALLERY";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getHighlightGalleryById(int intHighId)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT HIGHGALL_ID, HIGH_ID, HIGHGALL_TITLE, HIGHGALL_TITLE_ZH, HIGHGALL_TITLE_JP, HIGHGALL_TITLE_MS, HIGHGALL_IMAGE,HIGHGALL_THUMBNAIL, HIGHGALL_ORDER" +
                     " FROM TB_HIGHGALLERY" +
                     " WHERE HIGH_ID = ?";

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getHighlightVideos()
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT HIGHVIDEO_ID, HIGH_ID, HIGHVIDEO_VIDEO, HIGHVIDEO_THUMBNAIL" +
                     " FROM TB_HIGHVIDEO";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getHighlightVideosById(int intHighId)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT HIGHVIDEO_ID, HIGH_ID, HIGHVIDEO_VIDEO, HIGHVIDEO_THUMBNAIL" +
                     " FROM TB_HIGHVIDEO" +
                     " WHERE HIGH_ID = ?";

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getHighlightArticles()
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT HIGHART_ID, HIGH_ID, HIGHART_TITLE, HIGHART_TITLE_ZH, HIGHART_FILE" +
                     " FROM TB_HIGHARTICLE";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getHighlightArticlesById(int intHighid)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT HIGHART_ID, HIGH_ID, HIGHART_TITLE, HIGHART_TITLE_ZH, HIGHART_FILE" +
                     " FROM TB_HIGHARTICLE" +
                     " WHERE HIGH_ID = ?";

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighid;

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getHighlightComments()
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT A.HIGHCOMM_ID, A.HIGH_ID, A.HIGHCOMM_COMMENT, A.HIGHCOMM_POSTEDBY, A.HIGHCOMM_POSTEDBYNAME, A.HIGHCOMM_EMAIL," +
                     "A.HIGHCOMM_REPLY, A.HIGHCOMM_ACTIVE, A.HIGHCOMM_POSTEDDATE, A.HIGHREPLY_COMMENT, A.HIGHREPLY_REPLIEDBY, A.HIGHREPLY_REPLIEDBYNAME, A.HIGHREPLY_EMAIL, A.HIGHREPLY_ACTIVE, A.HIGHREPLY_REPLIEDDATE" +
                     " FROM VW_HIGHCOMMENT A";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getHighlightCommentsById(int intHighId)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT A.HIGHCOMM_ID, A.HIGH_ID, A.HIGHCOMM_COMMENT, A.HIGHCOMM_POSTEDBY, A.HIGHCOMM_POSTEDBYNAME, A.HIGHCOMM_EMAIL," +
                     " A.HIGHCOMM_REPLY, A.HIGHCOMM_ACTIVE, A.HIGHCOMM_POSTEDDATE, B.HIGHREPLY_COMMENT, B.HIGHREPLY_REPLIEDBY, B.HIGHREPLY_REPLIEDBYNAME," +
                     " B.HIGHREPLY_EMAIL, B.HIGHREPLY_ACTIVE, B.HIGHREPLY_REPLIEDDATE" +
                     " FROM (TB_HIGHCOMMENT A LEFT JOIN TB_HIGHREPLY B ON (A.HIGHCOMM_ID = B.HIGHCOMM_ID) AND (A.HIGH_ID = B.HIGH_ID))" +
                     " WHERE A.HIGH_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public int getPreviousEventById(DateTime dtHighStartDate, int intEventId, /*int intGroupId,*/ int intActive)
    {
        int PreviousEvent = 0;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT HIGH_ID FROM VW_HIGHLIGHT" +
                     " WHERE ((HIGH_STARTDATE = ? AND HIGH_ID < ?)" +
                     " OR (HIGH_STARTDATE < ?))";

            cmd.Parameters.Add("HIGH_STARTDATE", OdbcType.DateTime).Value = dtHighStartDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("HIGH_ID", OdbcType.BigInt, 9).Value = intEventId;
            cmd.Parameters.Add("HIGH_STARTDATE_2", OdbcType.DateTime).Value = dtHighStartDate.ToString(clsAdmin.CONSTDATEFORMAT);

            //if (intGroupId != 0)
            //{
            //    strSql += " AND V_GRP = ?";

            //    cmd.Parameters.Add("V_GRP", OdbcType.Int, 9).Value = intGroupId;
            //}

            if (intActive != 0)
            {
                strSql += " AND HIGH_ACTIVE = ?";

                cmd.Parameters.Add("HIGH_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " AND HIGH_VISIBLE = 1" +
                      " ORDER BY HIGH_STARTDATE DESC, HIGH_ID DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            PreviousEvent = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return PreviousEvent;
    }

    public int getNextEventById(DateTime dtHighStartDate, int intEventId, /*int intGroupId,*/ int intActive)
    {
        int NextEvent = 0;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT HIGH_ID FROM VW_HIGHLIGHT" +
                     " WHERE ((HIGH_STARTDATE = ? AND HIGH_ID > ?)" +
                     " OR HIGH_STARTDATE > ?)";

            cmd.Parameters.Add("HIGH_STARTDATE", OdbcType.DateTime).Value = dtHighStartDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("HIGH_ID", OdbcType.BigInt, 9).Value = intEventId;
            cmd.Parameters.Add("HIGH_STARTDATE_2", OdbcType.DateTime).Value = dtHighStartDate.ToString(clsAdmin.CONSTDATEFORMAT);

            //if (intGroupId != 0)
            //{
            //    strSql += " AND V_GRP = ?";

            //    cmd.Parameters.Add("V_GRP", OdbcType.Int, 9).Value = intGroupId;
            //}

            if (intActive != 0)
            {
                strSql += " AND HIGH_ACTIVE = ?";

                cmd.Parameters.Add("HIGH_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " AND HIGH_VISIBLE = 1" +
                      " ORDER BY HIGH_STARTDATE ASC, HIGH_ID ASC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            NextEvent = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return NextEvent;
    }

    public Boolean isExactSameHighlightDetails(int intHighId, string strHighTitle, string strHighTitleJp, string strHighTitleMs, string strHighTitleZh, string strHighTitleFURL, string strHighSnapshot, string strHighSnapshotJp, string strHighSnapshoMs, string strHighSnapshotZh,
                                               string strHighShortDesc, string strHighShortDescJp, string strHighShortDescMs, string strHighShortDescZh, DateTime dtHighStartDate, DateTime dtHighEndDate, string strHighImage, int intHighOrder,
                                               int intHighShowTop, int intHighVisible, int intHighAllowComment, string strSourceUrl, string strSourceTitle)
    {
        Boolean boolSame = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_HIGHLIGHT" +
                     " WHERE HIGH_ID = ?" +
                     " AND BINARY HIGH_TITLE = ?" +
                     " AND BINARY HIGH_TITLE_JP = ?" +
                     " AND BINARY HIGH_TITLE_MS = ?" +
                     " AND BINARY HIGH_TITLE_ZH = ?" +
                     " AND BINARY HIGH_TITLEFURL = ?" +
                     " AND BINARY HIGH_SNAPSHOT = ?" +
                     " AND BINARY HIGH_SNAPSHOT_JP = ?" +
                     " AND BINARY HIGH_SNAPSHOT_MS = ?" +
                     " AND BINARY HIGH_SNAPSHOT_ZH = ?" +
                     " AND BINARY HIGH_SHORTDESC = ?" +
                     " AND BINARY HIGH_SHORTDESC_JP = ?" +
                     " AND BINARY HIGH_SHORTDESC_MS = ?" +
                     " AND BINARY HIGH_SHORTDESC_ZH = ?" +
                     " AND HIGH_STARTDATE = ?" +
                     " AND HIGH_ENDDATE = ?" +
                     " AND BINARY HIGH_IMAGE = ?" +
                     " AND HIGH_ORDER = ?" +
                     " AND HIGH_SHOWTOP = ?" +
                     " AND HIGH_VISIBLE = ?" +
                     " AND HIGH_ALLOWCOMMENT = ?" +
                     " AND HIGH_SOURCE_URL = ?" +
                     " AND HIGH_SOURCE_TITLE = ?";

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGH_TITLE", OdbcType.VarChar, 250).Value = strHighTitle;
            cmd.Parameters.Add("p_HIGH_TITLE_JP", OdbcType.VarChar, 250).Value = strHighTitleJp;
            cmd.Parameters.Add("p_HIGH_TITLE_MS", OdbcType.VarChar, 250).Value = strHighTitleMs;
            cmd.Parameters.Add("p_HIGH_TITLE_ZH", OdbcType.VarChar, 250).Value = strHighTitleZh;
            cmd.Parameters.Add("p_HIGH_TITLEFURL", OdbcType.VarChar, 250).Value = strHighTitleFURL;
            cmd.Parameters.Add("p_HIGH_SNAPSHOT", OdbcType.VarChar, 1000).Value = strHighSnapshot;
            cmd.Parameters.Add("p_HIGH_SNAPSHOT_JP", OdbcType.VarChar, 1000).Value = strHighSnapshotJp;
            cmd.Parameters.Add("p_HIGH_SNAPSHOT_MS", OdbcType.VarChar, 1000).Value = strHighSnapshoMs;
            cmd.Parameters.Add("p_HIGH_SNAPSHOT_ZH", OdbcType.VarChar, 1000).Value = strHighSnapshotZh;
            cmd.Parameters.Add("p_HIGH_SHORTDESC", OdbcType.Text).Value = strHighShortDesc;
            cmd.Parameters.Add("p_HIGH_SHORTDESC_JP", OdbcType.Text).Value = strHighShortDescJp;
            cmd.Parameters.Add("p_HIGH_SHORTDESC_MS", OdbcType.Text).Value = strHighShortDescMs;
            cmd.Parameters.Add("p_HIGH_SHORTDESC_ZH", OdbcType.Text).Value = strHighShortDescZh;
            cmd.Parameters.Add("p_HIGH_STARTDATE", OdbcType.DateTime).Value = dtHighStartDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_HIGH_ENDDATE", OdbcType.DateTime).Value = dtHighEndDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_HIGH_IMAGE", OdbcType.VarChar, 250).Value = strHighImage;
            cmd.Parameters.Add("p_HIGH_ORDER", OdbcType.Int, 9).Value = intHighOrder;
            cmd.Parameters.Add("p_HIGH_SHOWTOP", OdbcType.Int, 1).Value = intHighShowTop;
            cmd.Parameters.Add("p_HIGH_VISIBLE", OdbcType.Int, 1).Value = intHighVisible;
            cmd.Parameters.Add("p_HIGH_ALLOWCOMMENT", OdbcType.Int, 1).Value = intHighAllowComment;
            cmd.Parameters.Add("p_HIGH_SOURCE_URL", OdbcType.VarChar, 500).Value = strSourceUrl == null ? "" : strSourceUrl;
            cmd.Parameters.Add("p_HIGH_SOURCE_TITLE", OdbcType.VarChar, 500).Value = strSourceTitle == null ? "" : strSourceTitle;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public Boolean isExactSameHighlightDetails(int intHighId, string strHighTitle, string strHighTitle_zh, string strHighShortDesc, string strHighShortDesc_zh, DateTime dtHighStartDate, DateTime dtHighEndDate, string strHighImage, int intHighShowTop, int intHighVisible, int intHighAllowComment)
    {
        Boolean boolSame = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_HIGHLIGHT" +
                     " WHERE HIGH_ID = ?" +
                     " AND BINARY HIGH_TITLE = ?" +
                     " AND BINARY HIGH_TITLE_ZH = ?" +
                     " AND BINARY HIGH_SHORTDESC = ?" +
                     " AND BINARY HIGH_SHORTDESC_ZH = ?" +
                     " AND HIGH_STARTDATE = ?" +
                     " AND HIGH_ENDDATE = ?" +
                     " AND BINARY HIGH_IMAGE = ?" +
                     " AND HIGH_SHOWTOP = ?" +
                     " AND HIGH_VISIBLE = ?" +
                     " AND HIGH_ALLOWCOMMENT = ?";

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGH_TITLE", OdbcType.VarChar, 250).Value = strHighTitle;
            cmd.Parameters.Add("p_HIGH_TITLE_ZH", OdbcType.VarChar, 250).Value = strHighTitle_zh;
            cmd.Parameters.Add("p_HIGH_SHORTDESC", OdbcType.Text).Value = strHighShortDesc;
            cmd.Parameters.Add("p_HIGH_SHORTDESC_ZH", OdbcType.Text).Value = strHighShortDesc_zh;
            cmd.Parameters.Add("p_HIGH_STARTDATE", OdbcType.DateTime).Value = dtHighStartDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_HIGH_ENDDATE", OdbcType.DateTime).Value = dtHighEndDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_HIGH_IMAGE", OdbcType.VarChar, 250).Value = strHighImage;
            cmd.Parameters.Add("p_HIGH_SHOWTOP", OdbcType.Int, 1).Value = intHighShowTop;
            cmd.Parameters.Add("p_HIGH_VISIBLE", OdbcType.Int, 1).Value = intHighVisible;
            cmd.Parameters.Add("p_HIGH_ALLOWCOMMENT", OdbcType.Int, 1).Value = intHighAllowComment;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public Boolean isExactSameHighlightDesc(int intHighId, string strHighDesc)
    {
        Boolean boolSame = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_HIGHLIGHT" +
                     " WHERE HIGH_ID = ?" +
                     " AND BINARY HIGH_DESC = ?";

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGH_DESC", OdbcType.Text).Value = strHighDesc;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public Boolean isExactSameHighlightDesc(int intHighId, string strHighDesc, string strHighDesc_zh)
    {
        Boolean boolSame = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_HIGHLIGHT" +
                     " WHERE HIGH_ID = ?" +
                     " AND BINARY HIGH_DESC = ?" +
                     " AND BINARY HIGH_DESC_ZH = ?";

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGH_DESC", OdbcType.Text).Value = strHighDesc;
            cmd.Parameters.Add("p_HIGH_DESC_ZH", OdbcType.Text).Value = strHighDesc_zh;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public Boolean isExactSameHighlightGallery(int intHighId, string strHighImageTitle, string strHighImage)
    {
        Boolean boolSame = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_HIGHGALLERY" +
                     " WHERE HIGH_ID = ?" +
                     " AND BINARY HIGHGALL_TITLE = ?" +
                     " AND BINARY HIGHGALL_IMAGE = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHGALL_TITLE", OdbcType.VarChar, 250).Value = strHighImageTitle;
            cmd.Parameters.Add("p_HIGHGALL_IMAGE", OdbcType.VarChar, 250).Value = strHighImage;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public Boolean isExactSameHighlightVideo(int intHighId, string strHighVideo, string strHighVideoThumb)
    {
        Boolean boolSame = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_HIGHVIDEO" +
                     " WHERE HIGH_ID = ?" +
                     " AND BINARY HIGHVIDEO_VIDEO = ?" +
                     " AND BINARY HIGHVIDEO_THUMBNAIL = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHVIDEO_VIDEO", OdbcType.VarChar, 1000).Value = strHighVideo;
            cmd.Parameters.Add("p_HIGHVIDEP_THUMBNAIL", OdbcType.VarChar, 1000).Value = strHighVideoThumb;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public Boolean isExactSameHighlightArticle(int intHighId, string strHighArtTitle, string strHighArtFile)
    {
        Boolean boolSame = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_HIGHARTICLE" +
                     " WHERE HIGH_ID = ?" +
                     " AND BINARY HIGHART_TITLE = ?" +
                     " AND BINARY HIGHART_FILE = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHART_TITLE", OdbcType.VarChar, 250).Value = strHighArtTitle;
            cmd.Parameters.Add("p_HIGHART_FILE", OdbcType.VarChar, 250).Value = strHighArtFile;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int updateHighlightDetails(int intHighId, string strHighTitle, string strHighTitleJp, string strHighTitleMs, string strHighTitleZh, string strHighTitleFURL, string strHighSnapshot, string strHighSnapshotJp, string strHighSnapshotMs, string strHighSnapshotZh,
                                      string strHighShortDesc, string strHighShortDescJp, string strHighShortDescMs, string strHighShortDescZh, DateTime dtHighStartDate, DateTime dtHighEndDate, string strHighImage, int intHighOrder,
                                      int intHighShowTop, int intHighVisible, int intHighAllowComment, string strSourceUrl, string strSourceTitle, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_HIGHLIGHT SET " +
                     "HIGH_TITLE = ?, " +
                     "HIGH_TITLE_JP = ?, " +
                     "HIGH_TITLE_MS = ?, " +
                     "HIGH_TITLE_ZH = ?, " +
                     "HIGH_TITLEFURL = ?, " +
                     "HIGH_SNAPSHOT = ?," +
                     "HIGH_SNAPSHOT_JP = ?," +
                     "HIGH_SNAPSHOT_MS = ?," +
                     "HIGH_SNAPSHOT_ZH = ?," +
                     "HIGH_SHORTDESC = ?, " +
                     "HIGH_SHORTDESC_JP = ?, " +
                     "HIGH_SHORTDESC_MS = ?, " +
                     "HIGH_SHORTDESC_ZH = ?, " +
                     "HIGH_STARTDATE = ?, " +
                     "HIGH_ENDDATE = ?, " +
                     "HIGH_IMAGE = ?, " +
                     "HIGH_ORDER = ?, " +
                     "HIGH_SHOWTOP = ?, " +
                     "HIGH_VISIBLE = ?, " +
                     "HIGH_ALLOWCOMMENT = ?, " +
                     "HIGH_SOURCE_URL = ?, " +
                     "HIGH_SOURCE_TITLE = ?, " +
                     "HIGH_UPDATEDBY = ?, " +
                     "HIGH_LASTUPDATE = SYSDATE() " +
                     "WHERE HIGH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_TITLE", OdbcType.VarChar, 250).Value = strHighTitle;
            cmd.Parameters.Add("p_HIGH_TITLE_JP", OdbcType.VarChar, 250).Value = strHighTitleJp;
            cmd.Parameters.Add("p_HIGH_TITLE_MS", OdbcType.VarChar, 250).Value = strHighTitleMs;
            cmd.Parameters.Add("p_HIGH_TITLE_ZH", OdbcType.VarChar, 250).Value = strHighTitleZh;
            cmd.Parameters.Add("p_HIGH_TITLEFURL", OdbcType.VarChar, 250).Value = strHighTitleFURL;
            cmd.Parameters.Add("p_HIGH_SNAPSHOT", OdbcType.VarChar, 1000).Value = strHighSnapshot;
            cmd.Parameters.Add("p_HIGH_SNAPSHOT_JP", OdbcType.VarChar, 1000).Value = strHighSnapshotJp;
            cmd.Parameters.Add("p_HIGH_SNAPSHOT_MS", OdbcType.VarChar, 1000).Value = strHighSnapshotMs;
            cmd.Parameters.Add("p_HIGH_SNAPSHOT_ZH", OdbcType.VarChar, 1000).Value = strHighSnapshotZh;
            cmd.Parameters.Add("p_HIGH_SHORTDESC", OdbcType.Text).Value = strHighShortDesc;
            cmd.Parameters.Add("p_HIGH_SHORTDESC_JP", OdbcType.Text).Value = strHighShortDescJp;
            cmd.Parameters.Add("p_HIGH_SHORTDESC_MS", OdbcType.Text).Value = strHighShortDescMs;
            cmd.Parameters.Add("p_HIGH_SHORTDESC_ZH", OdbcType.Text).Value = strHighShortDescZh;
            cmd.Parameters.Add("p_HIGH_STARTDATE", OdbcType.DateTime).Value = dtHighStartDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_HIGH_ENDDATE", OdbcType.DateTime).Value = dtHighEndDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_HIGH_IMAGE", OdbcType.VarChar, 250).Value = strHighImage;
            cmd.Parameters.Add("p_HIGH_ORDER", OdbcType.Int, 9).Value = intHighOrder;
            cmd.Parameters.Add("p_HIGH_SHOWTOP", OdbcType.Int, 1).Value = intHighShowTop;
            cmd.Parameters.Add("p_HIGH_VISIBLE", OdbcType.Int, 1).Value = intHighVisible;
            cmd.Parameters.Add("p_HIGH_ALLOWCOMMENT", OdbcType.Int, 1).Value = intHighAllowComment;
            cmd.Parameters.Add("p_HIGH_SOURCE_URL", OdbcType.VarChar, 500).Value = strSourceUrl == null ? "" : strSourceUrl;
            cmd.Parameters.Add("p_HIGH_SOURCE_TITLE", OdbcType.VarChar, 500).Value = strSourceTitle == null ? "" : strSourceTitle;
            cmd.Parameters.Add("p_HIGH_UPDATEDBY", OdbcType.Int, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateHighlightDetails(int intHighId, string strHighTitle, string strHighTitle_zh, string strHighShortDesc, string strHighShortDesc_zh, DateTime dtHighStartDate, DateTime dtHighEndDate, string strHighImage, int intHighShowTop, int intHighVisible, int intHighAllowComment, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_HIGHLIGHT SET " +
                     "HIGH_TITLE = ?, " +
                     "HIGH_TITLE_ZH = ?, " +
                     "HIGH_SHORTDESC = ?, " +
                     "HIGH_SHORTDESC_ZH = ?, " +
                     "HIGH_STARTDATE = ?, " +
                     "HIGH_ENDDATE = ?, " +
                     "HIGH_IMAGE = ?, " +
                     "HIGH_SHOWTOP = ?, " +
                     "HIGH_VISIBLE = ?, " +
                     "HIGH_ALLOWCOMMENT = ?, " +
                     "HIGH_UPDATEDBY = ?, " +
                     "HIGH_LASTUPDATE = SYSDATE() " +
                     "WHERE HIGH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_TITLE", OdbcType.VarChar, 250).Value = strHighTitle;
            cmd.Parameters.Add("p_HIGH_TITLE_ZH", OdbcType.VarChar, 250).Value = strHighTitle_zh;
            cmd.Parameters.Add("p_HIGH_SHORTDESC", OdbcType.Text).Value = strHighShortDesc;
            cmd.Parameters.Add("p_HIGH_SHORTDESC_ZH", OdbcType.Text).Value = strHighShortDesc_zh;
            cmd.Parameters.Add("p_HIGH_STARTDATE", OdbcType.DateTime).Value = dtHighStartDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_HIGH_ENDDATE", OdbcType.DateTime).Value = dtHighEndDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_HIGH_IMAGE", OdbcType.VarChar, 250).Value = strHighImage;
            cmd.Parameters.Add("p_HIGH_SHOWTOP", OdbcType.Int, 1).Value = intHighShowTop;
            cmd.Parameters.Add("p_HIGH_VISIBLE", OdbcType.Int, 1).Value = intHighVisible;
            cmd.Parameters.Add("p_HIGH_ALLOWCOMMENT", OdbcType.Int, 1).Value = intHighAllowComment;
            cmd.Parameters.Add("p_HIGH_UPDATEDBY", OdbcType.Int, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateHighlightGroup(int intHighId, int intGroupId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;


            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_HIGHGROUP SET GRP_ID = ? WHERE HIGH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGroupId;
            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateHighlightDesc(int intHighId, string strHighDesc)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_HIGHLIGHT SET HIGH_DESC = ? WHERE HIGH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_DESC", OdbcType.Text).Value = strHighDesc;
            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateHighlightDesc(int intHighId, string strHighDesc, string strHighDesc_zh)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_HIGHLIGHT SET" +
                     " HIGH_DESC = ?," +
                     " HIGH_DESC_ZH = ?" +
                     " WHERE HIGH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_DESC", OdbcType.Text).Value = strHighDesc;
            cmd.Parameters.Add("p_HIGH_DESC_ZH", OdbcType.Text).Value = strHighDesc_zh;
            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int setHighlightCommentReplied(int intHighId, string strHighComment, int intPostedBy, string strPostedBy, string strPostedEmail)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_HIGHCOMMENT SET HIGHCOMM_REPLY = ?" +
                     " WHERE HIGH_ID = ?" +
                     " HIGHCOMM_COMMENT = ?" +
                     " HIGHCOMM_POSTEDBY = ?" +
                     " HIGHCOMM_POSTEDBYNAME = ?" +
                     " HIGHCOMM_EMAIL = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGHCOMM_REPLY", OdbcType.Int, 1).Value = 1;
            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHCOMM_COMMENT", OdbcType.VarChar, 1000).Value = strHighComment;
            cmd.Parameters.Add("p_HIGHCOMM_POSTEDBY", OdbcType.Int, 9).Value = intPostedBy;
            cmd.Parameters.Add("p_HIGHCOMM_POSTEDBYNAME", OdbcType.VarChar, 250).Value = strPostedBy;
            cmd.Parameters.Add("p_HIGHCOMM_EMAIL", OdbcType.VarChar, 250).Value = strPostedEmail;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int setHighlightCommentReplied(int intHighCommId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_HIGHCOMMENT SET" +
                     " HIGHCOMM_REPLY = ?" +
                     " WHERE HIGHCOMM_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGHCOMM_REPLY", OdbcType.Int, 1).Value = 1;
            cmd.Parameters.Add("p_HIGHCOMM_ID", OdbcType.Int, 9).Value = intHighCommId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int setHighlightCommentInactive(int intHighId, string strHighComment, int intPostedBy, string strPostedBy, string strPostedEmail)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_HIGHCOMMENT SET HIGHCOMM_ACTIVE = ?" +
                     " WHERE HIGH_ID = ?" +
                     " HIGHCOMM_COMMENT = ?" +
                     " HIGHCOMM_POSTEDBY = ?" +
                     " HIGHCOMM_POSTEDBYNAME = ?" +
                     " HIGHCOMM_EMAIL = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGHCOMM_ACTIVE", OdbcType.Int, 1).Value = 0;
            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHCOMM_COMMENT", OdbcType.VarChar, 1000).Value = strHighComment;
            cmd.Parameters.Add("p_HIGHCOMM_POSTEDBY", OdbcType.Int, 9).Value = intPostedBy;
            cmd.Parameters.Add("p_HIGHCOMM_POSTEDBYNAME", OdbcType.VarChar, 250).Value = strPostedBy;
            cmd.Parameters.Add("p_HIGHCOMM_EMAIL", OdbcType.VarChar, 250).Value = strPostedEmail;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int setHighlightReplyInactive(int intHighCommId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_HIGHREPLY SET HIGHREPLY_ACTIVE = ? WHERE HIGHCOMM_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGHREPLY_ACTIVE", OdbcType.Int, 1).Value = 0;
            cmd.Parameters.Add("p_HIGHCOMM_ID", OdbcType.Int, 9).Value = intHighCommId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int setHighlightInactive(int intHighId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_HIGHLIGHT SET HIGH_ACTIVE = ? WHERE HIGH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ACTIVE", OdbcType.Int, 1).Value = 0;
            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteHighlightGroup(int intHighId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_HIGHGROUP WHERE HIGH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteHighlight(int intHighId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_HIGHLIGHT WHERE HIGH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteHighlightGalleryImage(int intHighId, string strHighImageTitle, string strHighImage)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_HIGHGALLERY" +
                     " WHERE HIGH_ID = ?" +
                     " AND HIGHGALL_TITLE = ?" +
                     " AND HIGHGALL_IMAGE = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHGALL_TITLE", OdbcType.VarChar, 250).Value = strHighImageTitle;
            cmd.Parameters.Add("p_HIGHGALL_IMAGE", OdbcType.VarChar, 250).Value = strHighImage;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteHighlightGallery(int intHighId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_HIGHGALLERY WHERE HIGH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteHighlightVideo(int intHighVideoId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_HIGHVIDEO" +
                     " WHERE HIGHVIDEO_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGHVIDEO_ID", OdbcType.Int, 9).Value = intHighVideoId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteHighlightVideo(int intHighId, string strHighVideo, string strHighVideoThumb)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_HIGHVIDEO" +
                     " WHERE HIGH_ID = ?" +
                     " AND HIGHVIDEO_VIDEO = ?" +
                     " AND HIGHVIDEO_THUMBNAIL = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHVIDEO_VIDEO", OdbcType.VarChar, 1000).Value = strHighVideo;
            cmd.Parameters.Add("p_HIGHVIDEO_THUMBNAIL", OdbcType.VarChar, 1000).Value = strHighVideoThumb;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteHighlightVideos(int intHighId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_HIGHVIDEO WHERE HIGH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteHighlightArticle(int intHighId, string strHighArtTitle, string strHighArtFile)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_HIGHARTICLE" +
                     " WHERE HIGH_ID = ?" +
                     " AND HIGHART_TITLE = ?" +
                     " AND HIGHART_FILE = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHART_TITLE", OdbcType.VarChar, 250).Value = strHighArtTitle;
            cmd.Parameters.Add("p_HIGHART_FILE", OdbcType.VarChar, 250).Value = strHighArtFile;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteHighlightArticles(int intHighId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_HIGHARTICLE WHERE HIGH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteHighlightComment(int intHighId, string strHighComment, int intPostedBy, string strPostedBy, string strPostedEmail)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_HIGHCOMMENT" +
                     " WHERE HIGH_ID = ?" +
                     " AND HIGHCOMM_COMMENT = ?" +
                     " AND HIGHCOMM_POSTEDBY = ?" +
                     " AND HIGHCOMM_POSTEDBYNAME = ?" +
                     " AND HIGHCOMM_EMAIL = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHCOMM_COMMENT", OdbcType.VarChar, 1000).Value = strHighComment;
            cmd.Parameters.Add("p_HIGHCOMM_POSTEDBY", OdbcType.Int, 9).Value = intPostedBy;
            cmd.Parameters.Add("p_HIGHCOMM_POSTEDBYNAME", OdbcType.VarChar, 250).Value = strPostedBy;
            cmd.Parameters.Add("p_HIGHCOMM_EMAIL", OdbcType.VarChar, 250).Value = strPostedEmail;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteHighlightComment(int intHighCommId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_HIGHCOMMENT" +
                     " WHERE HIGHCOMM_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGHCOMM_ID", OdbcType.Int, 9).Value = intHighCommId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteHighlightComments(int intHighId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_HIGHCOMMENT WHERE HIGH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteHighlightReply(int intHighCommId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_HIGHREPLY WHERE HIGHCOMM_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGHCOMM_ID", OdbcType.Int, 9).Value = intHighCommId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteHighlightReplies(int intHighId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_HIGHREPLY WHERE HIGH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int getHighlightPhotoTotalByGrpId(int intGrpId, int intHighId)
    {
        int intTotal = 0;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT SUM(A.V_NOP) AS V_NOP" +
                     " FROM VW_HIGHLIGHT A" +
                     " WHERE A.HIGH_ID = ?";


            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;
            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            try
            {
                intTotal = Convert.ToInt16(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                intTotal = 0;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intTotal;
    }

    public int getPreviousEventByIdInd(DateTime dtHighStartDate, int intEventId, int intGroupId, int intActive)
    {
        int PreviousEvent = 0;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT HIGH_ID FROM VW_HIGHLIGHT" +
                     " WHERE ((HIGH_STARTDATE = ? AND HIGH_ID < ?)" +
                     " OR (HIGH_STARTDATE < ?))";

            cmd.Parameters.Add("HIGH_STARTDATE", OdbcType.DateTime).Value = dtHighStartDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("HIGH_ID", OdbcType.BigInt, 9).Value = intEventId;
            cmd.Parameters.Add("HIGH_STARTDATE_2", OdbcType.DateTime).Value = dtHighStartDate.ToString(clsAdmin.CONSTDATEFORMAT);

            if (intGroupId != 0)
            {
                strSql += " AND V_GRP = ?";

                cmd.Parameters.Add("V_GRP", OdbcType.Int, 9).Value = intGroupId;
            }

            if (intActive != 0)
            {
                strSql += " AND HIGH_ACTIVE = ?";

                cmd.Parameters.Add("HIGH_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " AND HIGH_VISIBLE = 1" +
                      " ORDER BY HIGH_STARTDATE DESC, HIGH_ID DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            PreviousEvent = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return PreviousEvent;
    }

    public int getNextEventByIdInd(DateTime dtHighStartDate, int intEventId, int intGroupId, int intActive)
    {
        int NextEvent = 0;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT HIGH_ID FROM VW_HIGHLIGHT" +
                     " WHERE ((HIGH_STARTDATE = ? AND HIGH_ID > ?)" +
                     " OR HIGH_STARTDATE > ?)";

            cmd.Parameters.Add("HIGH_STARTDATE", OdbcType.DateTime).Value = dtHighStartDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("HIGH_ID", OdbcType.BigInt, 9).Value = intEventId;
            cmd.Parameters.Add("HIGH_STARTDATE_2", OdbcType.DateTime).Value = dtHighStartDate.ToString(clsAdmin.CONSTDATEFORMAT);

            if (intGroupId != 0)
            {
                strSql += " AND V_GRP = ?";

                cmd.Parameters.Add("V_GRP", OdbcType.Int, 9).Value = intGroupId;
            }

            if (intActive != 0)
            {
                strSql += " AND HIGH_ACTIVE = ?";

                cmd.Parameters.Add("HIGH_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " AND HIGH_VISIBLE = 1" +
                      " ORDER BY HIGH_STARTDATE ASC, HIGH_ID ASC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            NextEvent = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return NextEvent;
    }


    public DataSet getHighlightVideosByVideoId(int intVideoId)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT A.HIGHVIDEO_ID, A.HIGH_ID, A.HIGHVIDEO_VIDEO, A.HIGHVIDEO_THUMBNAIL" +
                     " FROM TB_HIGHVIDEO A" +
                     " WHERE A.HIGHVIDEO_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Parameters.Add("p_HIGHVIDEO_ID", OdbcType.Int, 9).Value = intVideoId;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }


    /*add for gallery*/
    public int updateHighlightGalleryById(int intHighId, string strHighTitle, string strHighImage, int intHighlightUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_HIGHLIGHT SET" +
                     " HIGH_TITLE = ?," +
                     " HIGH_IMAGE = ?," +
                     " HIGH_UPDATEDBY = ?," +
                     " HIGH_LASTUPDATE = SYSDATE()" +
                     " WHERE HIGH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("HIGH_TITLE", OdbcType.VarChar, 250).Value = strHighTitle;
            cmd.Parameters.Add("HIGH_IMAGE", OdbcType.Text).Value = strHighImage;
            cmd.Parameters.Add("HIGH_UPDATEDBY", OdbcType.Int, 9).Value = intHighlightUpdatedBy;
            cmd.Parameters.Add("HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateGallery(int intID, string strImageTitle, string strImageTitleJp, string strImageTitleMs, string strImageTitleZh, string strImage, int intOrder)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_HIGHGALLERY SET" +
                " HIGHGALL_ORDER = ?, HIGHGALL_TITLE = ?" +
                " WHERE HIGHGALL_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGHGALL_ORDER", OdbcType.Int, 9).Value = intOrder;
            cmd.Parameters.Add("p_HIGHGALL_TITLE", OdbcType.VarChar, 250).Value = strImageTitle;
            cmd.Parameters.Add("p_HIGHGALL_ID", OdbcType.BigInt, 9).Value = intID;
            //cmd.Parameters.Add("p_HIGHGALL_TITLE_JP", OdbcType.VarChar, 250).Value = strImageTitleJp;
            //cmd.Parameters.Add("p_HIGHGALL_TITLE_MS", OdbcType.VarChar, 250).Value = strImageTitleMs;
            //cmd.Parameters.Add("p_HIGHGALL_TITLE_ZH", OdbcType.VarChar, 250).Value = strImageTitleZh;
            //cmd.Parameters.Add("p_HIGHGALL_IMAGE", OdbcType.VarChar, 250).Value = strImage;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    /*end gallery*/

    public int getTotalRecord(int intActive)
    {
        int intTotal = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_HIGHLIGHT A" +
                     " WHERE 1 = 1";

            if (intActive != 0)
            {
                strSql += " AND A.HIGH_ACTIVE = ?";
                cmd.Parameters.Add("p_HIGH_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            intTotal = int.Parse(cmd.ExecuteScalar().ToString());
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intTotal;
    }

    public int updateHighGalleryThumbnail(int intHighGallId, string strThumbnail)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;


            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_HIGHGALLERY SET HIGHGALL_THUMBNAIL = ? WHERE HIGHGALL_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGHGALL_THUMBNAIL", OdbcType.VarChar, 250).Value = strThumbnail;
            cmd.Parameters.Add("p_HIGHGALL_ID", OdbcType.Int, 9).Value = intHighGallId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractHighGall(int intHighId, string strImage)
    {
        Boolean boolFound = false;

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            DataSet ds = new DataSet();
            string strSql;

            strSql = "SELECT A.HIGHGALL_THUMBNAIL" +
                     " FROM TB_HIGHGALLERY A" +
                     " WHERE A.HIGH_ID = ?" +
                     " AND A.HIGHGALL_IMAGE = ?";

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGHGALL_IMAGE", OdbcType.VarChar, 250).Value = strImage;

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                highGallThumb = ds.Tables[0].Rows[0]["HIGHGALL_THUMBNAIL"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean isExactSameHighlightComment(int intHighId, int intAllowComment)
    {
        Boolean boolSame = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_HIGHLIGHT" +
                     " WHERE HIGH_ID = ?" +
                     " AND HIGH_ALLOWCOMMENT = ?";

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;
            cmd.Parameters.Add("p_HIGH_ALLOWCOMMENT", OdbcType.Int, 1).Value = intAllowComment;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int updateHighlightComment(int intHighId, int intAllowComment)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_HIGHLIGHT SET HIGH_ALLOWCOMMENT = ? WHERE HIGH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_HIGH_ALLOWCOMMENT", OdbcType.Int, 1).Value = intAllowComment;
            cmd.Parameters.Add("p_HIGH_ID", OdbcType.Int, 9).Value = intHighId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                highId = intHighId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    #region "Upload Image"
    public void addEventGallery(EventGalleryDetail eventGalleryDetail)
    {
        System.Web.Script.Serialization.JavaScriptSerializer json = new System.Web.Script.Serialization.JavaScriptSerializer();
        Dictionary<string,EventGalleryDetail> dictSession = new Dictionary<string, EventGalleryDetail>();
        if (HttpContext.Current.Session["EVENT_GALLERY"] != null)
        {
            dictSession = json.Deserialize<Dictionary<string, EventGalleryDetail>>(HttpContext.Current.Session["EVENT_GALLERY"].ToString());
        }
        dictSession.Add(eventGalleryDetail.filename,eventGalleryDetail);
        HttpContext.Current.Session["EVENT_GALLERY"] = json.Serialize(dictSession);
    }
    public void addEventGallerySort(EventGalleryDetail eventGalleryDetail)
    {
        System.Web.Script.Serialization.JavaScriptSerializer json = new System.Web.Script.Serialization.JavaScriptSerializer();
        Dictionary<string, int> dictSession = new Dictionary<string, int>();
        if (HttpContext.Current.Session["EVENT_GALLERY_SORTING"] != null)
        {
            dictSession = json.Deserialize<Dictionary<string, int>>(HttpContext.Current.Session["EVENT_GALLERY_SORTING"].ToString());
        }
        dictSession.Add(eventGalleryDetail.filename, 9999);
        HttpContext.Current.Session["EVENT_GALLERY_SORTING"] = json.Serialize(dictSession);
    }
    public string getEventGallery(int intId)
    {
        System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
        Dictionary<string, EventGalleryDetail> dictEventGalleryDetail = new Dictionary<string, EventGalleryDetail>();

        // bind from database
        DataSet ds = getHighlightGalleryById(intId);
        foreach (DataRow dw in ds.Tables[0].Rows)
        {
            EventGalleryDetail eventGalleryDetail = new EventGalleryDetail();
            eventGalleryDetail.id = Convert.ToInt32(dw["HIGHGALL_ID"]);
            eventGalleryDetail.filename = dw["HIGHGALL_IMAGE"].ToString();
            eventGalleryDetail.thumbname = dw["HIGHGALL_THUMBNAIL"].ToString();
            eventGalleryDetail.title = System.Uri.EscapeDataString(dw["HIGHGALL_TITLE"].ToString());
            eventGalleryDetail.type = "database";
            eventGalleryDetail.highId = int.Parse(dw["HIGH_ID"].ToString());
            eventGalleryDetail.order = int.Parse(dw["HIGHGALL_ORDER"].ToString());
            
            dictEventGalleryDetail.Add(dw["HIGHGALL_IMAGE"].ToString(), eventGalleryDetail);
        }
        return js.Serialize(dictEventGalleryDetail);
    }
    public string getEventGallerySorting()
    {

        System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
        Dictionary<string,int> dictEventGalleryDetail = new Dictionary<string, int>();

        if (HttpContext.Current.Session["EVENT_GALLERY"] != null)
        {
            Dictionary<string, EventGalleryDetail> dictEventGallery = js.Deserialize<Dictionary<string, EventGalleryDetail>>(HttpContext.Current.Session["EVENT_GALLERY"].ToString());
            foreach (KeyValuePair<string, EventGalleryDetail> entry in dictEventGallery)
            {
                dictEventGalleryDetail.Add(entry.Key,entry.Value.order);
            }
        }
        return js.Serialize(dictEventGalleryDetail);
    }
    public string getEventGalleryList()
    {

        System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
        List<EventGalleryDetail> listEventGalleryDetail = new List<EventGalleryDetail>();

        if (HttpContext.Current.Session["EVENT_GALLERY"] != null)
        {
            Dictionary<string, EventGalleryDetail> dictEventGallery = js.Deserialize<Dictionary<string, EventGalleryDetail>>(HttpContext.Current.Session["EVENT_GALLERY"].ToString());
            foreach (KeyValuePair<string, EventGalleryDetail> entry in dictEventGallery)
            {
                // do something with entry.Value or entry.Key
                listEventGalleryDetail.Add(entry.Value);
            }
        }
        return js.Serialize(listEventGalleryDetail);        
    }


    #endregion
    #endregion
}

public class EventGalleryDetail
{
    public int id { get; set; }
    public int highId { get; set; }
    public int order { get; set; }
    public string filename { get; set; }
    public string thumbname { get; set; }
    public string title { get; set; }
    public string type { get; set; }
    public bool isDelete { get; set; }

    public EventGalleryDetail()
    {
        id = -1;
        isDelete = false;
        order = 9999;
    }
}
