﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Configuration;
using System.Web.Hosting;

/// <summary>
/// Summary description for clsLog
/// </summary>
public class clsLog
{
    #region "Public Methods"
    
    public clsLog()
	{
	}

    public static void logErroMsg(string strErrorMsg)
    {
        string strLogPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["uplBase"] + clsAdmin.CONSTLOGFOLDER + "/");
        string strLogFullPath = strLogPath + "log.txt";
        File.AppendAllText(strLogFullPath, DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ": " + strErrorMsg + Environment.NewLine);
    }

    public static void error_email(string msg)
    {
        string strLogPath = HostingEnvironment.MapPath(ConfigurationManager.AppSettings["uplBase"] + clsAdmin.CONSTLOGFOLDER + "/");
        string strLogFullPath = strLogPath + "error.txt";
        //FileStream fs = new FileStream(strLogFullPath, FileMode.OpenOrCreate);
        File.AppendAllText(strLogFullPath, DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ": " + "[EMAIL] " + msg + Environment.NewLine);
    }

    public static void email(string message)
    {
        string strLogPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMLOGFOLDER + "/");
        string strLogFullPath = strLogPath + "email.txt";
        File.AppendAllText(strLogFullPath, DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ": " + message + Environment.NewLine);
    }
    public static void payment(string message)
    {
        string strLogPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMLOGFOLDER + "/");
        string strLogFullPath = strLogPath + "payment.txt";
        File.AppendAllText(strLogFullPath, DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ": " + message + Environment.NewLine);
    }
    #endregion
}
