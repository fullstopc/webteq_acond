﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Security.Cryptography;
using System.Text;

/// <summary>
/// Summary description for clsSHA1
/// </summary>
public class clsSHA1
{
    #region "Methods"
    public clsSHA1()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    public string getSignature(string strkey)
    {
        SHA1CryptoServiceProvider objSHA1 = new SHA1CryptoServiceProvider();
        objSHA1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(strkey.ToCharArray()));

        byte[] buffer = objSHA1.Hash;
        string signature = System.Convert.ToBase64String(buffer);

        return signature;
    }
    #endregion
}
