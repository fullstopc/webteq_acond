﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Web.UI.WebControls;
//using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Web.UI;

public class clsMis : dbconnBase
{
    #region "Properties"
    private string _countryId;
    private int _showState;
    private int _countryActive;


    private string _countryCode;
    private string _countryName;
    private int _countryOrder;
    private int _countryDefault;
    #endregion

    #region "Property Methods"
    public string countryId
    {
        get { return _countryId; }
        set { _countryId = value; }
    }

    public int showState
    {
        get { return _showState; }
        set { _showState = value; }
    }

    public int countryActive
    {
        get { return _countryActive; }
        set { _countryActive = value; }
    }





    public string countryCode
    {
        get { return _countryCode; }
        set { _countryCode = value; }
    }

    public string countryName
    {
        get { return _countryName; }
        set { _countryName = value; }
    }

    public int countryOrder
    {
        get { return _countryOrder; }
        set { _countryOrder = value; }
    }

    public int countryDefault
    {
        get { return _countryDefault; }
        set { _countryDefault = value; }
    }
    #endregion

    public clsMis()
    {

    }

    public static void resetListing()
    {
    }

    public static string getCurrentPageName()
    {
        string strPath = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path);

        return strPath;
    }

    public static string getCurrentPageName2(Boolean boolWithQuery)
    {
        string strPath = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path);

        if (boolWithQuery)
        {
            strPath += HttpContext.Current.Request.Url.Query;
        }

        return strPath;
    }

    public static string getPageName(string strPath)
    {
        System.IO.FileInfo oInfo = new System.IO.FileInfo(strPath);
        string strRet = oInfo.Name;
        return strRet;
    }

    public static string GetUniqueFilename(string filename, string uploadPath)
    {
        TimeSpan ts = DateTime.Now - Convert.ToDateTime("1/1/1970 8:00:00 AM");
        long file_append = (long)ts.TotalSeconds;
        string newFilename = file_append.ToString() + "_" + filename.ToLower();

        while (File.Exists(uploadPath + "/" + newFilename))
        {
            file_append++;
            newFilename = file_append.ToString() + "_" + filename;
        }

        return newFilename;
    }

    public static string getNewQueryString(string strCurQuery, string strTargetKey, string strTargetValue)
    {
        string strNewQuery = "";
        Boolean boolFound = false;
        string strQuery = strCurQuery;
        string strTempTargetKey = strTargetKey + "=";

        if (!string.IsNullOrEmpty(strQuery))
        {
            string[] strQuerySplit = strQuery.Split((char)'&');
            strQuery = "";

            for (int i = 0; i < strQuerySplit.Length; i++)
            {
                //find targetted query string key and assign new value
                if (strQuerySplit[i].IndexOf(strTempTargetKey) >= 0)
                {
                    boolFound = true;

                    if (i == 0) { strQuerySplit[i] = "?" + strTempTargetKey + strTargetValue; }
                    else { strQuerySplit[i] = strTempTargetKey + strTargetValue; }
                }

                //re-construct query string
                if (i == (strQuerySplit.Length - 1))
                {
                    strQuery += strQuerySplit[i];
                }
                else
                {
                    strQuery += strQuerySplit[i] + "&";
                }
            }
        }
        else
        {
            boolFound = true;
            strQuery = "?" + strTempTargetKey + strTargetValue;
        }

        if (!boolFound)
        {
            strQuery += "&" + strTempTargetKey + strTargetValue;
        }

        if (!strQuery.StartsWith("?")) { strQuery = "?" + strQuery; }

        strNewQuery = strQuery;

        return strNewQuery;
    }

    public static string formatAllowedFileExt(string strRegExp)
    {
        string strRegExpPrefix = "^.+\\.(";
        string strRegExpPostfix = ")$";
        int intStart = strRegExp.LastIndexOf(strRegExpPrefix) + strRegExpPrefix.Length;
        int intLength = strRegExp.Length - strRegExpPrefix.Length - strRegExpPostfix.Length;
        string strRet = strRegExp.Substring(intStart, intLength);
        strRet = strRet.Replace("|", ", ");
        strRet = strRet.Replace("(", "");
        strRet = strRet.Replace(")", "");

        return strRet;
    }

    public static string formatAllowedFileSize(int intFileSize, string strUnit)
    {
        string strRet = "";
        int intConvertedFileSize = intFileSize;
        string strConvertedUnit = "kb";

        switch (strUnit.ToLower())
        {
            case "mb":
                intConvertedFileSize = intFileSize / 1024 / 1024;
                strConvertedUnit = strUnit;
                break;
            case "kb":
                intConvertedFileSize = intFileSize / 1024;
                strConvertedUnit = strUnit;
                break;
            default:
                break;
        }

        strRet = intConvertedFileSize + " " + strConvertedUnit;

        return strRet;
    }

    public static string formatAllowedFileDimension(int intFileWidth, int intFileHeight, string strUnit)
    {
        string strRet = "";
        string strDimensionUnit = "px";

        if (!string.IsNullOrEmpty(strUnit)) { strDimensionUnit = strUnit; }

        strRet = intFileWidth + strDimensionUnit + " x " + intFileHeight + strDimensionUnit;

        return strRet;
    }

    public static string formatYesNo(int intInput)
    {
        string strRet = "";

        switch (intInput)
        {
            case 0:
                strRet = "No";
                break;
            case 1:
                strRet = "Yes";
                break;
            default:
                break;
        }

        return strRet;
    }

    public static string formatYesNo(Boolean boolInput)
    {
        string strRet = "";

        switch (boolInput)
        {
            case false:
                strRet = "No";
                break;
            case true:
                strRet = "Yes";
                break;
        }

        return strRet;
    }

    public static Boolean formatBooleanTrueFalse(int intInput)
    {
        Boolean boolRet = false;

        switch (intInput)
        {
            case 0:
                boolRet = false;
                break;
            case 1:
                boolRet = true;
                break;
            default:
                break;
        }

        return boolRet;
    }

    public static string formatDateDDMMtoMMDD(string dt)
    {
        string returnDt = "";

        if (dt != null && dt != "")
        {
            string[] dtItems = dt.Split(clsAdmin.CONSTDATESEPERATOR);
            returnDt = dtItems[1] + clsAdmin.CONSTDATESEPERATOR + dtItems[0] + clsAdmin.CONSTDATESEPERATOR + dtItems[2];
        }

        return returnDt;
    }

    public static string formatDateMMDDtoDDMM(string dt)
    {
        string returnDt = "";

        if (dt != null && dt != "")
        {
            string[] dtItems = dt.Split(clsAdmin.CONSTDATESEPERATOR);
            returnDt = dtItems[1] + clsAdmin.CONSTDATESEPERATOR + dtItems[0] + clsAdmin.CONSTDATESEPERATOR + dtItems[2];
        }

        return returnDt;
    }

    public static string formatPageTitle(string strTitle, Boolean boolAdmin)
    {
        System.Globalization.TextInfo textInfo = new System.Globalization.CultureInfo("en-US", false).TextInfo;
        string strRetPageTitle;

        clsConfig config = new clsConfig();
        string strPrefix = textInfo.ToTitleCase(config.prefix.ToLower());

        if (boolAdmin)
        {
            strRetPageTitle = (!string.IsNullOrEmpty(strPrefix) ? (strPrefix) : "") + (strPrefix == "Admin" ? "" : " :: " + "Admin") + (!string.IsNullOrEmpty(strTitle) ? (" :: " + strTitle) : "");
        }
        else
        {
            strRetPageTitle = (!string.IsNullOrEmpty(strPrefix) ? (!string.IsNullOrEmpty(strTitle) ? (strPrefix + " :: " + strTitle) : strPrefix) : "");
        }

        return strRetPageTitle;
    }

    public static string formatDatasetToString(DataSet ds, string strColName, string strSplitter)
    {
        string strRet = "";

        //try
        //{
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            if (strRet != "")
            {
                strRet += strSplitter;
            }
            strRet += dr[strColName].ToString();
        }
        //}
        //catch (Exception ex)
        //{ }

        return strRet;
    }

    public static string limitString(string strTarget, int intTargetLength)
    {
        string strRet = "";

        if (intTargetLength > 0)
        {
            if (intTargetLength >= strTarget.Length)
            {
                strRet = strTarget;
            }
            else
            {
                strRet = strTarget.Substring(0, intTargetLength);
                strRet += HttpContext.GetGlobalResourceObject("GlobalResource", "contentSummary.Text").ToString();
            }

        }
        else
        {
            strRet = strTarget;
        }

        return strRet;
    }

    public static string pageTitle(string strpageTitle)
    {
        string strRetPageTitle;
        char pageItemSplit = clsAdmin.CONSTDEFAULTSEPERATOR;

        strpageTitle = strpageTitle.Substring(0, strpageTitle.Length);
        string[] pageItem = strpageTitle.Split(pageItemSplit);

        if (pageItem.Length > 1)
        {
            strRetPageTitle = "<span class=\"divPageTitle\"> " + pageItem[0] + " </span> " + "<span class=\"divPageTitle2\"> " + pageItem[1] + "</span>";
            return strRetPageTitle;
        }

        else
        {
            strRetPageTitle = "<span class=\"divPageTitle\"> " + strpageTitle + " </span> ";
            return strRetPageTitle;
        }
    }

    public static string formatProcessingDone(int intInput)
    {
        string strRet = "";

        switch (intInput)
        {
            case 6:
                strRet = "PROCESSING";
                break;
            case 7:
                strRet = "DONE";
                break;
            case 8:
                strRet = "REJECTED";
                break;
            default:
                break;
        }

        return strRet;
    }

    public static void createFolder(string strPhysicalPath)
    {
        if (!Directory.Exists(strPhysicalPath))
        {
            Directory.CreateDirectory(strPhysicalPath);
        }
    }

    public static string getEncryptResMemCode(string strResCode, string strMemCode)
    {
        string strEncryptCode = "";

        string strResMemCode = strResCode + strMemCode;

        clsMD5 md5 = new clsMD5();
        strEncryptCode = md5.encrypt(strResMemCode);

        return strEncryptCode;
    }

    public static string getImageIcon(string strName)
    {
        string strCssClassName = null;
        switch (strName)
        {
            case "website":
                strCssClassName = "website";
                break;
            case "product":
                strCssClassName = "product";
                break;
            case "enquiry":
                strCssClassName = "enquiry";
                break;
            case "custom":
                strCssClassName = "custom";
                break;
            case "user":
                strCssClassName = "user";
                break;
            case "order":
                strCssClassName = "order";
                break;
            case "controller panel":
                strCssClassName = "controllerpanel";
                break;
            case "project manager":
                strCssClassName = "project";
                break;
            case "user manager":
                strCssClassName = "user";
                break;
            case "user manual":
                strCssClassName = "userinfo";
                break;
            case "faq":
                strCssClassName = "faq";
                break;
            case "training":
                strCssClassName = "training";
                break;

        }

        return strCssClassName;
    }

    public static string getSideMenuImageIcon(string strName)
    {
        string strCssClassName = "";
        switch (strName)
        {
            case "product detail":
                strCssClassName = "product-detail";
                break;
            case "product content":
                strCssClassName = "product-content";
                break;
            case "product pricing":
                strCssClassName = "product-pricing";
                break;
            case "related product":
                strCssClassName = "product-related";
                break;
            case "addon product":
                strCssClassName = "product-addon";
                break;

            case "page details":
                strCssClassName = "page-detail";
                break;
            case "sibling pages sorting":
                strCssClassName = "page-sibling";
                break;
            case "add new page":
                strCssClassName = "page-new";
                break;
            case "replicate page":
                strCssClassName = "page-replicate";
                break;
            case "mobile view":
                strCssClassName = "page-mobile";
                break;

            case "slide show group details":
                strCssClassName = "slideshowgroup-detail";
                break;

            case "slide show details":
                strCssClassName = "slideshow-detail";
                break;

            case "event details":
                strCssClassName = "event-detail";
                break;
            case "event gallery":
                strCssClassName = "event-gallery";
                break;
            case "event description":
                strCssClassName = "event-desc";
                break;
            case "event video":
                strCssClassName = "event-video";
                break;
            case "event comment":
                strCssClassName = "event-comment";
                break;

            case "object details":
                strCssClassName = "object-detail";
                break;
        }
        return strCssClassName;
    }

    public static Image getImageResize(Image imgTarget, int intWidthTarget, int intHeightTarget, int intWidth, int intHeight)
    {
        int imageNewWidth;
        int imageNewHeight;
        double imageRatio;

        if (intWidth <= intWidthTarget && intHeight <= intHeightTarget)
        {
            imageNewWidth = intWidth;
            imageNewHeight = intHeight;
        }
        else
        {
            double imageWidthRatio;
            double imageHeightRatio;

            imageWidthRatio = (Convert.ToDouble(intWidth) / Convert.ToDouble(intWidthTarget));
            imageHeightRatio = (Convert.ToDouble(intHeight) / Convert.ToDouble(intHeightTarget));

            if (imageWidthRatio >= imageHeightRatio)
            {
                imageRatio = imageWidthRatio;
                imageNewWidth = intWidthTarget;
                imageNewHeight = Convert.ToInt16(Math.Round((Convert.ToDouble(intHeight) / Convert.ToDouble(imageRatio)), 0));
            }
            else
            {
                imageRatio = imageHeightRatio;
                imageNewHeight = intHeightTarget;
                imageNewWidth = Convert.ToInt16(Math.Round((Convert.ToDouble(intWidth) / Convert.ToDouble(imageRatio)), 0));
            }
        }

        imgTarget.Width = imageNewWidth;
        imgTarget.Height = imageNewHeight;

        return imgTarget;
    }
    public static Image getImageResize2(Image imgTarget, int intWidthTarget, int intHeightTarget, int intWidth, int intHeight)
    {
        int imageNewWidth;
        int imageNewHeight;
        double imageRatio;

        double imageWidthRatio;
        double imageHeightRatio;

        imageWidthRatio = (Convert.ToDouble(intWidth) / Convert.ToDouble(intWidthTarget));
        imageHeightRatio = (Convert.ToDouble(intHeight) / Convert.ToDouble(intHeightTarget));

        if (imageWidthRatio >= imageHeightRatio)
        {
            imageRatio = imageHeightRatio;
            imageNewHeight = intHeightTarget;
            imageNewWidth = Convert.ToInt16(Math.Round((Convert.ToDouble(intWidth) / Convert.ToDouble(imageRatio)), 0));
        }
        else
        {
            imageRatio = imageWidthRatio;
            imageNewWidth = intWidthTarget;
            imageNewHeight = Convert.ToInt16(Math.Round((Convert.ToDouble(intHeight) / Convert.ToDouble(imageRatio)), 0));
        }

        imgTarget.Width = imageNewWidth;
        imgTarget.Height = imageNewHeight;

        return imgTarget;
    }
    #region "tb_list"
    public DataSet getListByListGrp(string strListGrp, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT LIST_ID, LIST_NAME, LIST_VALUE, LIST_GROUP, LIST_ORDER, LIST_ACTIVE, LIST_PARENTVALUE, LIST_PARENTGROUP, LIST_LANG, LIST_DEFAULT" +
                     " FROM TB_LIST" +
                     " WHERE LIST_GROUP = ?";

            cmd.Parameters.Add("p_LIST_GROUP", OdbcType.VarChar, 250).Value = strListGrp;

            if (intActive != 0)
            {
                strSql += " AND LIST_ACTIVE = ?";
                cmd.Parameters.Add("p_LIST_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " ORDER BY LIST_GROUP ASC";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getListByListGrp(string strListGrp)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT LIST_ID, LIST_NAME, LIST_VALUE, LIST_GROUP, LIST_ORDER, LIST_ACTIVE, LIST_PARENTVALUE, LIST_PARENTGROUP" +
                     " FROM TB_LIST" +
                     " WHERE LIST_ACTIVE = 1";

            if (strListGrp != "")
            {
                strSql += " AND LIST_GROUP = ?";
                cmd.Parameters.Add("p_LIST_GROUP", OdbcType.VarChar, 250).Value = strListGrp;
            }

            strSql += " ORDER BY LIST_GROUP ASC";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getListByListValue(string strListGrp, string strListValue)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT LIST_ID, LIST_NAME, LIST_VALUE, LIST_GROUP, LIST_ORDER, LIST_ACTIVE, LIST_PARENTVALUE, LIST_PARENTGROUP" +
                     " FROM TB_LIST" +
                     " WHERE LIST_VALUE = ?";

            cmd.Parameters.Add("p_LIST_VALUE", OdbcType.VarChar, 250).Value = strListValue;

            if (strListGrp != "")
            {
                strSql += " AND LIST_GROUP = ?";
                cmd.Parameters.Add("p_LIST_GROUP", OdbcType.VarChar, 250).Value = strListGrp;
            }

            strSql += " ORDER BY LIST_GROUP ASC";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getListByParentValue(string strParentValue)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT LIST_ID, LIST_NAME, LIST_VALUE, LIST_GROUP, LIST_ORDER, LIST_ACTIVE, LIST_PARENTVALUE, LIST_PARENTGROUP" +
                     " FROM TB_LIST" +
                     " WHERE LIST_PARENTVALUE = ?" +
                     " ORDER BY LIST_ORDER ASC";

            cmd.Parameters.Add("p_LIST_PARENTVALUE", OdbcType.VarChar, 250).Value = strParentValue;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean isListValueValid(string strListValue, string strListGroup)
    {
        Boolean boolValid = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_LIST" +
                     " WHERE LIST_VALUE = ?" +
                     " AND LIST_GROUP = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_LIST_VALUE", OdbcType.VarChar, 250).Value = strListValue;
            cmd.Parameters.Add("p_LIST_GROUP", OdbcType.VarChar, 250).Value = strListGroup;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1)
            {
                boolValid = true;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolValid;
    }

    public string getListNameByListValue(string strValue, string strGroup, int intDefault, int intActive)
    {
        string strName = "";

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            DataSet ds = new DataSet();

            strSql = "SELECT LIST_ID, LIST_NAME, LIST_VALUE, LIST_GROUP, LIST_ORDER, LIST_ACTIVE, LIST_PARENTVALUE, LIST_PARENTGROUP, LIST_LANG, LIST_DEFAULT" +
                     " FROM TB_LIST" +
                     " WHERE LIST_VALUE = ?" +
                     " AND LIST_GROUP = ?";

            cmd.Parameters.Add("p_LIST_VALUE", OdbcType.VarChar, 250).Value = strValue;
            cmd.Parameters.Add("p_LIST_GROUP", OdbcType.VarChar, 250).Value = strGroup;

            if (intDefault != 0)
            {
                strSql += " AND LIST_DEFAULT = ?";
                cmd.Parameters.Add("p_LIST_DEFAULT", OdbcType.Int, 1).Value = intDefault;
            }

            if (intActive != 0)
            {
                strSql += " AND LIST_ACTIVE = ?";
                cmd.Parameters.Add("p_LIST_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " ORDER BY LIST_ORDER ASC";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                strName = ds.Tables[0].Rows[0]["LIST_NAME"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return strName;
    }

    public int updateItem(string strGroup, string strName, string strValue,int intOrder,int intActive, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<string> listUpdate = new List<string>();

            if(strValue != null)
            {
                listUpdate.Add("LIST_VALUE = ?");
                cmd.Parameters.Add("p_LIST_VALUE", OdbcType.VarChar, 250).Value = strValue;
            }

            if (intOrder != -1)
            {
                listUpdate.Add("LIST_ORDER = ?");
                cmd.Parameters.Add("p_LIST_ORDER", OdbcType.Int).Value = intOrder;
            }

            if (intActive != -1)
            {
                listUpdate.Add("LIST_ACTIVE = ?");
                cmd.Parameters.Add("p_LIST_ACTIVE", OdbcType.Int).Value = intActive;
            }

            strSql = "UPDATE TB_LIST SET" +
                     " " + string.Join(",",listUpdate.ToArray()) + "" +
                     " WHERE LIST_GROUP = ?" +
                     " AND LIST_NAME = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion

    #region "Shipping Fee Calculation"
    public static decimal getShippingFee(string strCountry, string strState, int intWeight, decimal decOrdTotal)
    {
        decimal decTotalShipping = Convert.ToDecimal("0.00");

        decimal decShippingKgFirst = Convert.ToDecimal("0.00");
        decimal decShippingFeeFirst = Convert.ToDecimal("0.00");
        decimal decShippingKgSub = Convert.ToDecimal("0.00");
        decimal decShippingFeeSub = Convert.ToDecimal("0.00");
        decimal decShippingKgFree = Convert.ToDecimal("0.00");

        clsShipping ship = new clsShipping();
        clsWaiver waiver = new clsWaiver();
        DataSet dsShipping = new DataSet();
        DataTable dtShipping = new DataTable();
        DataRow[] foundRow;

        dsShipping = ship.getShippingList(1);
        dtShipping = dsShipping.Tables[0];

        foundRow = dtShipping.Select("SHIP_COUNTRY = '" + strCountry + "' AND SHIP_STATE = '" + strState + "'");
        
        if (foundRow.Length > 0)
        {
            decShippingKgFirst = Convert.ToDecimal(foundRow[0]["SHIP_KG_FIRST"]);
            decShippingFeeFirst = Convert.ToDecimal(foundRow[0]["SHIP_FEE_FIRST"]);
            decShippingKgSub = Convert.ToDecimal(foundRow[0]["SHIP_KG"]);
            decShippingFeeSub = Convert.ToDecimal(foundRow[0]["SHIP_FEE"]);
            decShippingKgFree = Convert.ToDecimal(foundRow[0]["SHIP_FEEFREE"]);
        }
        else
        {
            foundRow = dtShipping.Select("SHIP_COUNTRY = '" + strCountry + "' AND (SHIP_STATE IS NULL OR SHIP_STATE = '')");

            if (foundRow.Length > 0)
            {
                decShippingKgFirst = Convert.ToDecimal(foundRow[0]["SHIP_KG_FIRST"]);
                decShippingFeeFirst = Convert.ToDecimal(foundRow[0]["SHIP_FEE_FIRST"]);
                decShippingKgSub = Convert.ToDecimal(foundRow[0]["SHIP_KG"]);
                decShippingFeeSub = Convert.ToDecimal(foundRow[0]["SHIP_FEE"]);
                decShippingKgFree = Convert.ToDecimal(foundRow[0]["SHIP_FEEFREE"]);
            }
            else
            {
                foundRow = dtShipping.Select("SHIP_DEFAULT = 1");

                if (foundRow.Length > 0)
                {
                    decShippingKgFirst = Convert.ToDecimal(foundRow[0]["SHIP_KG_FIRST"]);
                    decShippingFeeFirst = Convert.ToDecimal(foundRow[0]["SHIP_FEE_FIRST"]);
                    decShippingKgSub = Convert.ToDecimal(foundRow[0]["SHIP_KG"]);
                    decShippingFeeSub = Convert.ToDecimal(foundRow[0]["SHIP_FEE"]);
                    decShippingKgFree = Convert.ToDecimal(foundRow[0]["SHIP_FEEFREE"]);
                }
            }
        }

        if (decShippingKgFree > 0 ? intWeight < (decShippingKgFree * 1000) : true)
        {
            int intWeightSub = intWeight - Convert.ToInt32(decShippingKgFirst * 1000);
            int intUnitOfWeightSub = decShippingKgSub > 0 ? Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(intWeightSub) / Convert.ToDecimal(decShippingKgSub * 1000))) : 0;
            intUnitOfWeightSub = intUnitOfWeightSub > 0 ? intUnitOfWeightSub : 0;
            decTotalShipping = decShippingFeeFirst + (intUnitOfWeightSub * decShippingFeeSub);
            decTotalShipping = Math.Round(decTotalShipping, 2);
            decimal decWaiver = waiver.getWaiverByCost(decOrdTotal, 1);
            decTotalShipping = decTotalShipping - decWaiver;
            decTotalShipping = decTotalShipping > 0 ? decTotalShipping : 0;
        }

        return decTotalShipping;
    }

    public static decimal getShippingFee2(string strCountry, string strState, int intWeight, decimal decOrdTotal)
    {
        decimal decTotalShipping = Convert.ToDecimal("0.00");

        decimal decShippingKgFirst = Convert.ToDecimal("0.00");
        decimal decShippingFeeFirst = Convert.ToDecimal("0.00");
        decimal decShippingKgSub = Convert.ToDecimal("0.00");
        decimal decShippingFeeSub = Convert.ToDecimal("0.00");
        decimal decShippingKgFree = Convert.ToDecimal("0.00");

        clsShipping ship = new clsShipping();
        DataSet dsShipping = new DataSet();
        DataTable dtShipping = new DataTable();
        DataRow[] foundRow;

        dsShipping = ship.getShippingList(1);
        dtShipping = dsShipping.Tables[0];

        foundRow = dtShipping.Select("SHIP_COUNTRY = '" + strCountry + "' AND SHIP_STATE = '" + strState + "'");

        if (foundRow.Length > 0)
        {
            decShippingKgFirst = Convert.ToDecimal(foundRow[0]["SHIP_KG_FIRST"]);
            decShippingFeeFirst = Convert.ToDecimal(foundRow[0]["SHIP_FEE_FIRST"]);
            decShippingKgSub = Convert.ToDecimal(foundRow[0]["SHIP_KG"]);
            decShippingFeeSub = Convert.ToDecimal(foundRow[0]["SHIP_FEE"]);
            decShippingKgFree = Convert.ToDecimal(foundRow[0]["SHIP_FEEFREE"]);
        }
        else
        {
            foundRow = dtShipping.Select("SHIP_COUNTRY = '" + strCountry + "' AND (SHIP_STATE IS NULL OR SHIP_STATE = '')");

            if (foundRow.Length > 0)
            {
                decShippingKgFirst = Convert.ToDecimal(foundRow[0]["SHIP_KG_FIRST"]);
                decShippingFeeFirst = Convert.ToDecimal(foundRow[0]["SHIP_FEE_FIRST"]);
                decShippingKgSub = Convert.ToDecimal(foundRow[0]["SHIP_KG"]);
                decShippingFeeSub = Convert.ToDecimal(foundRow[0]["SHIP_FEE"]);
                decShippingKgFree = Convert.ToDecimal(foundRow[0]["SHIP_FEEFREE"]);
            }
            else
            {
                foundRow = dtShipping.Select("SHIP_DEFAULT = 1");

                if (foundRow.Length > 0)
                {
                    decShippingKgFirst = Convert.ToDecimal(foundRow[0]["SHIP_KG_FIRST"]);
                    decShippingFeeFirst = Convert.ToDecimal(foundRow[0]["SHIP_FEE_FIRST"]);
                    decShippingKgSub = Convert.ToDecimal(foundRow[0]["SHIP_KG"]);
                    decShippingFeeSub = Convert.ToDecimal(foundRow[0]["SHIP_FEE"]);
                    decShippingKgFree = Convert.ToDecimal(foundRow[0]["SHIP_FEEFREE"]);
                }
            }
        }

        if (decShippingKgFree > 0 ? intWeight < (decShippingKgFree * 1000) : true)
        {
            int intWeightSub = intWeight - Convert.ToInt32(decShippingKgFirst * 1000);
            int intUnitOfWeightSub = decShippingKgSub > 0 ? Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(intWeightSub) / Convert.ToDecimal(decShippingKgSub * 1000))) : 0;
            intUnitOfWeightSub = intUnitOfWeightSub > 0 ? intUnitOfWeightSub : 0;
            decTotalShipping = decShippingFeeFirst + (intUnitOfWeightSub * decShippingFeeSub);
            decTotalShipping = Math.Round(decTotalShipping, 2);
            decTotalShipping = decTotalShipping > 0 ? decTotalShipping : 0;
        }

        return decTotalShipping;
    }

    public static decimal getShippingFee3(string strCountry, string strState, int intWeight, decimal decOrdTotal)
    {
        decimal decTotalShipping = Convert.ToDecimal("0.00");

        decimal decShippingKgFirst = Convert.ToDecimal("0.00");
        decimal decShippingFeeFirst = Convert.ToDecimal("0.00");
        decimal decShippingKgSub = Convert.ToDecimal("0.00");
        decimal decShippingFeeSub = Convert.ToDecimal("0.00");
        decimal decShippingKgFree = Convert.ToDecimal("0.00");

        clsShipping ship = new clsShipping();
        clsWaiver waiver = new clsWaiver();
        DataSet dsShipping = new DataSet();
        DataTable dtShipping = new DataTable();
        DataRow[] foundRow;

        dsShipping = ship.getShippingList(1);
        dtShipping = dsShipping.Tables[0];

        foundRow = dtShipping.Select("SHIP_COUNTRY = '" + strCountry + "' AND SHIP_STATE = '" + strState + "'");

        if (foundRow.Length > 0)
        {
            decShippingKgFirst = Convert.ToDecimal(foundRow[0]["SHIP_KG_FIRST"]);
            decShippingFeeFirst = Convert.ToDecimal(foundRow[0]["SHIP_FEE_FIRST"]);
            decShippingKgSub = Convert.ToDecimal(foundRow[0]["SHIP_KG"]);
            decShippingFeeSub = Convert.ToDecimal(foundRow[0]["SHIP_FEE"]);
            decShippingKgFree = Convert.ToDecimal(foundRow[0]["SHIP_FEEFREE"]);
        }
        else
        {
            foundRow = dtShipping.Select("SHIP_COUNTRY = '" + strCountry + "' AND (SHIP_STATE IS NULL OR SHIP_STATE = '')");

            if (foundRow.Length > 0)
            {
                decShippingKgFirst = Convert.ToDecimal(foundRow[0]["SHIP_KG_FIRST"]);
                decShippingFeeFirst = Convert.ToDecimal(foundRow[0]["SHIP_FEE_FIRST"]);
                decShippingKgSub = Convert.ToDecimal(foundRow[0]["SHIP_KG"]);
                decShippingFeeSub = Convert.ToDecimal(foundRow[0]["SHIP_FEE"]);
                decShippingKgFree = Convert.ToDecimal(foundRow[0]["SHIP_FEEFREE"]);
            }
            else
            {
                foundRow = dtShipping.Select("SHIP_DEFAULT = 1");

                if (foundRow.Length > 0)
                {
                    decShippingKgFirst = Convert.ToDecimal(foundRow[0]["SHIP_KG_FIRST"]);
                    decShippingFeeFirst = Convert.ToDecimal(foundRow[0]["SHIP_FEE_FIRST"]);
                    decShippingKgSub = Convert.ToDecimal(foundRow[0]["SHIP_KG"]);
                    decShippingFeeSub = Convert.ToDecimal(foundRow[0]["SHIP_FEE"]);
                    decShippingKgFree = Convert.ToDecimal(foundRow[0]["SHIP_FEEFREE"]);
                }
            }
        }

        if (decShippingKgFree > 0 ? decOrdTotal < decShippingKgFree : true)
        {
            int intWeightSub = intWeight - Convert.ToInt32(decShippingKgFirst * 1000);
            int intUnitOfWeightSub = decShippingKgSub > 0 ? Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(intWeightSub) / Convert.ToDecimal(decShippingKgSub * 1000))) : 0;
            intUnitOfWeightSub = intUnitOfWeightSub > 0 ? intUnitOfWeightSub : 0;
            decTotalShipping = decShippingFeeFirst + (intUnitOfWeightSub * decShippingFeeSub);
            decTotalShipping = Math.Round(decTotalShipping, 2);
            decTotalShipping = decTotalShipping > 0 ? decTotalShipping : 0;

            //decimal decPriceSub = decOrdTotal - decShippingKgFirst;
            //int intUnitOfPriceSub = decShippingKgSub > 0 ? Convert.ToInt16(Math.Ceiling(decPriceSub / decShippingKgSub)) : 0;
            //intUnitOfPriceSub = intUnitOfPriceSub > 0 ? intUnitOfPriceSub : 0;
            //decTotalShipping = decShippingFeeFirst + (intUnitOfPriceSub * decShippingFeeSub);
            //decTotalShipping = Math.Round(decTotalShipping, 2);
            //decTotalShipping = decTotalShipping > 0 ? decTotalShipping : 0;
        }

        return decTotalShipping;
    }

    //Cuctomization - Ys Hamper Project - start
    public static decimal getShippingFee1(string strCountry, string strState, string strCity, int intWeight, decimal decOrdTotal)
    {
        decimal decTotalShipping = Convert.ToDecimal("0.00");

        decimal decShippingKgFirst = Convert.ToDecimal("0.00");
        decimal decShippingFeeFirst = Convert.ToDecimal("0.00");
        decimal decShippingKgSub = Convert.ToDecimal("0.00");
        decimal decShippingFeeSub = Convert.ToDecimal("0.00");
        decimal decShippingKgFree = Convert.ToDecimal("0.00");

        clsShipping ship = new clsShipping();
        clsWaiver waiver = new clsWaiver();
        DataSet dsShipping = new DataSet();
        DataTable dtShipping = new DataTable();
        DataRow[] foundRow;

        dsShipping = ship.getShippingList3(1);
        dtShipping = dsShipping.Tables[0];

        foundRow = dtShipping.Select("SHIP_COUNTRY = '" + strCountry + "' AND SHIP_STATE = '" + strState + "' AND SHIP_CITY = '" + strCity + "'");

        if (foundRow.Length > 0)
        {
            //decShippingKgFirst = Convert.ToDecimal(foundRow[0]["SHIP_KG_FIRST"]);
            //decShippingFeeFirst = Convert.ToDecimal(foundRow[0]["SHIP_FEE_FIRST"]);
            //decShippingKgSub = Convert.ToDecimal(foundRow[0]["SHIP_KG"]);
            decShippingFeeSub = Convert.ToDecimal(foundRow[0]["SHIP_FEE"]);
            decShippingKgFree = Convert.ToDecimal(foundRow[0]["SHIP_FEEFREE"]);
        }
        else
        {
            foundRow = dtShipping.Select("SHIP_COUNTRY = '" + strCountry + "' AND (SHIP_STATE IS NULL OR SHIP_STATE = '')");

            if (foundRow.Length > 0)
            {
                //decShippingKgFirst = Convert.ToDecimal(foundRow[0]["SHIP_KG_FIRST"]);
                //decShippingFeeFirst = Convert.ToDecimal(foundRow[0]["SHIP_FEE_FIRST"]);
                //decShippingKgSub = Convert.ToDecimal(foundRow[0]["SHIP_KG"]);
                decShippingFeeSub = Convert.ToDecimal(foundRow[0]["SHIP_FEE"]);
                decShippingKgFree = Convert.ToDecimal(foundRow[0]["SHIP_FEEFREE"]);
            }
            else
            {
                foundRow = dtShipping.Select("SHIP_DEFAULT = 1");

                if (foundRow.Length > 0)
                {
                    //decShippingKgFirst = Convert.ToDecimal(foundRow[0]["SHIP_KG_FIRST"]);
                    //decShippingFeeFirst = Convert.ToDecimal(foundRow[0]["SHIP_FEE_FIRST"]);
                    //decShippingKgSub = Convert.ToDecimal(foundRow[0]["SHIP_KG"]);
                    decShippingFeeSub = Convert.ToDecimal(foundRow[0]["SHIP_FEE"]);
                    decShippingKgFree = Convert.ToDecimal(foundRow[0]["SHIP_FEEFREE"]);
                }
            }
        }

        if (decShippingKgFree > 0 ? intWeight < (decShippingKgFree * 1000) : true)
        {
            int intWeightSub = intWeight - Convert.ToInt32(decShippingKgFirst * 1000);
            int intUnitOfWeightSub = decShippingKgSub > 0 ? Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(intWeightSub) / Convert.ToDecimal(decShippingKgSub * 1000))) : 0;
            intUnitOfWeightSub = intUnitOfWeightSub > 0 ? intUnitOfWeightSub : 0;
            //decTotalShipping = decShippingFeeFirst + (intUnitOfWeightSub * decShippingFeeSub);        

            if (decShippingKgFree > 0)
            {
                if (decOrdTotal > decShippingKgFree)
                {
                    decTotalShipping = 0;
                }
                else
                {
                    decTotalShipping = decShippingFeeSub;
                }
            }
            else
            {
                decTotalShipping = decShippingFeeSub;
            }

            decTotalShipping = Math.Round(decTotalShipping, 2);
            decimal decWaiver = waiver.getWaiverByCost(decOrdTotal, 1);
            decTotalShipping = decTotalShipping - decWaiver;
            decTotalShipping = decTotalShipping > 0 ? decTotalShipping : 0;
        }

        return decTotalShipping;
    }
    //Cuctomization - Ys Hamper Project - end

    //public static decimal getShippingFee3(string strCountry, string strState, decimal decOrdTotal)
    //{
    //    decimal decTotalShipping = Convert.ToDecimal("0.00");

    //    decimal decShippingKgFirst = Convert.ToDecimal("0.00");
    //    decimal decShippingFeeFirst = Convert.ToDecimal("0.00");
    //    decimal decShippingKgSub = Convert.ToDecimal("0.00");
    //    decimal decShippingFeeSub = Convert.ToDecimal("0.00");
    //    decimal decShippingKgFree = Convert.ToDecimal("0.00");

    //    clsShipping ship = new clsShipping();
    //    clsWaiver waiver = new clsWaiver();
    //    DataSet dsShipping = new DataSet();
    //    DataTable dtShipping = new DataTable();
    //    DataRow[] foundRow;

    //    dsShipping = ship.getShippingList(1);
    //    dtShipping = dsShipping.Tables[0];

    //    foundRow = dtShipping.Select("SHIP_COUNTRY = '" + strCountry + "' AND SHIP_STATE = '" + strState + "'");

    //    if (foundRow.Length > 0)
    //    {
    //        decShippingKgFirst = Convert.ToDecimal(foundRow[0]["SHIP_KG_FIRST"]);
    //        decShippingFeeFirst = Convert.ToDecimal(foundRow[0]["SHIP_FEE_FIRST"]);
    //        decShippingKgSub = Convert.ToDecimal(foundRow[0]["SHIP_KG"]);
    //        decShippingFeeSub = Convert.ToDecimal(foundRow[0]["SHIP_FEE"]);
    //        decShippingKgFree = Convert.ToDecimal(foundRow[0]["SHIP_FEEFREE"]);
    //    }
    //    else
    //    {
    //        foundRow = dtShipping.Select("SHIP_COUNTRY = '" + strCountry + "' AND (SHIP_STATE IS NULL OR SHIP_STATE = '')");

    //        if (foundRow.Length > 0)
    //        {
    //            decShippingKgFirst = Convert.ToDecimal(foundRow[0]["SHIP_KG_FIRST"]);
    //            decShippingFeeFirst = Convert.ToDecimal(foundRow[0]["SHIP_FEE_FIRST"]);
    //            decShippingKgSub = Convert.ToDecimal(foundRow[0]["SHIP_KG"]);
    //            decShippingFeeSub = Convert.ToDecimal(foundRow[0]["SHIP_FEE"]);
    //            decShippingKgFree = Convert.ToDecimal(foundRow[0]["SHIP_FEEFREE"]);
    //        }
    //        else
    //        {
    //            foundRow = dtShipping.Select("SHIP_DEFAULT = 1");

    //            if (foundRow.Length > 0)
    //            {
    //                decShippingKgFirst = Convert.ToDecimal(foundRow[0]["SHIP_KG_FIRST"]);
    //                decShippingFeeFirst = Convert.ToDecimal(foundRow[0]["SHIP_FEE_FIRST"]);
    //                decShippingKgSub = Convert.ToDecimal(foundRow[0]["SHIP_KG"]);
    //                decShippingFeeSub = Convert.ToDecimal(foundRow[0]["SHIP_FEE"]);
    //                decShippingKgFree = Convert.ToDecimal(foundRow[0]["SHIP_FEEFREE"]);
    //            }
    //        }
    //    }

    //    if (decShippingKgFree > 0 ? decOrdTotal < decShippingKgFree : true)
    //    {
    //        decimal decPriceSub = decOrdTotal - decShippingKgFirst;
    //        int intUnitOfPriceSub = decShippingKgSub > 0 ? Convert.ToInt32(Math.Ceiling(decPriceSub / decShippingKgSub)) : 0;
    //        intUnitOfPriceSub = intUnitOfPriceSub > 0 ? intUnitOfPriceSub : 0;
    //        decTotalShipping = decShippingFeeFirst + (intUnitOfPriceSub * decShippingFeeSub);
    //        decTotalShipping = Math.Round(decTotalShipping, 2);
    //        decTotalShipping = decTotalShipping > 0 ? decTotalShipping : 0;
    //    }

    //    return decTotalShipping;
    //}
    #endregion

    #region "Admin Page"
    public static int getSubSectId(int intSectId, int intValue, int intActive)
    {
        int intSubSectId = 0;

        clsAdmin adm = new clsAdmin();
        adm.extractAdminPageByParent(intSectId, intValue, intActive);

        intSubSectId = adm.pageId;

        return intSubSectId;
    }

    public static Boolean isLimitReach(string strSessionKey, int intTotalRecord)
    {
        Boolean boolExceed = false;

        if (HttpContext.Current.Session[strSessionKey] != null && HttpContext.Current.Session[strSessionKey] != "")
        {
            int intTryParse;
            if (int.TryParse(HttpContext.Current.Session[strSessionKey].ToString(), out intTryParse)) { boolExceed = intTotalRecord >= intTryParse; }
        }

        return boolExceed;
    }
    #endregion

    #region "Language"
    public static Boolean isLangExist(string strLang)
    {
        Boolean boolFound = false;

        if (HttpContext.Current.Session[clsAdmin.CONSTPROJECTLANG] != null && HttpContext.Current.Session[clsAdmin.CONSTPROJECTLANG] != "")
        {
            string strItem = clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + strLang + clsAdmin.CONSTDEFAULTSEPERATOR.ToString();
            boolFound = HttpContext.Current.Session[clsAdmin.CONSTPROJECTLANG].ToString().IndexOf(strItem) >= 0;
        }

        return boolFound;
    }

    public static Boolean isLangExist()
    {
        Boolean boolFound = false;

        if (HttpContext.Current.Session[clsAdmin.CONSTPROJECTLANG] != null && HttpContext.Current.Session[clsAdmin.CONSTPROJECTLANG] != "")
        {
            boolFound = true;
        }

        return boolFound;
    }
    #endregion

    #region "Update Methods"
    public static int performUpdateOrderStatus(int intOrdId, int intStatusId, Boolean boolUpdateStatus)
    {
        int intRecordAffected = 0;

        try
        {
            clsOrder ord = new clsOrder();

            if (intOrdId > 0)
            {
                intRecordAffected = ord.addOrderStatus(intOrdId, intStatusId);

                if (intRecordAffected == 1)
                {
                    if (boolUpdateStatus == true)
                    {
                        intRecordAffected = ord.setOrderStatus();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
        }

        return intRecordAffected;
    }

    public static int performUpdateOrderStatus2(int intOrdId, int intStatusId, DateTime dtOrdDate)
    {
        int intRecordAffected = 0;

        try
        {
            clsOrder ord = new clsOrder();

            if (intOrdId > 0)
            {
                intRecordAffected = ord.addOrderStatus2(intOrdId, intStatusId, dtOrdDate);

                if (intRecordAffected == 1)
                {
                    intRecordAffected = ord.setOrderStatus();
                }
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
        }

        return intRecordAffected;
    }
    #endregion

    #region "Delete Methods"
    public static void performDeleteMasthead(int intMastId, string strUploadPath, ref string strDeletedMastName)
    {
        try
        {
            clsMasthead masthead = new clsMasthead();
            masthead.extractMastheadById(intMastId, 0);
            masthead.mastId = intMastId;
            int iRecordAffected = masthead.deleteMasthead(masthead.mastId);

            if (iRecordAffected == 1)
            {
                if (masthead.mastImage != "")
                {
                    if (File.Exists(strUploadPath + masthead.mastImage)) { File.Delete(strUploadPath + masthead.mastImage); }
                }
                if (masthead.mastThumb != "")
                {
                    if (File.Exists(strUploadPath + masthead.mastThumb)) { File.Delete(strUploadPath + masthead.mastThumb); }
                }
                strDeletedMastName = masthead.mastName;
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
        }
    }

    public static void performDeleteGroup2(int intGrpId, int intDestGrpId, ref int intMovedProdNo, ref string strDeletedGrpName)
    {
        try
        {
            clsMasthead grp = new clsMasthead();
            grp.extractGrpById(intGrpId, 0);

            int iRecordAffected = grp.deleteGroup(grp.grpId);

            if (iRecordAffected == 1)
            {
                clsMasthead mast = new clsMasthead();
                intMovedProdNo = mast.moveMastheadByGrpId(intGrpId, intDestGrpId);
                strDeletedGrpName = grp.grpName;

                clsPageObject po = new clsPageObject();
                po.deleteItemById(intGrpId, clsAdmin.CONSTCMSGROUPSLIDESHOW);
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message);
            throw ex;
        }
        finally
        {
        }
    }

    public static void performDeleteGroup(int intGrpId, int intDestGrpId, string strUploadPath, ref int intMovedProdNo, ref string strDeletedGrpName)
    {
        try
        {
            clsGroup grp = new clsGroup();
            grp.extractGrpById(intGrpId, 0);

            int iRecordAffected = grp.deleteGroup(grp.grpId);

            if (iRecordAffected == 1)
            {
                if (grp.grpImage != "")
                {
                    if (File.Exists(strUploadPath + grp.grpImage)) { File.Delete(strUploadPath + grp.grpImage); }
                }
                clsProduct prod = new clsProduct();
                //HttpContext.Current.Response.Write(grp.grpId + " " + intDestGrpId);
                intMovedProdNo = prod.moveProdByGrpId(grp.grpId, intDestGrpId);
                strDeletedGrpName = grp.grpName;
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message);
            throw ex;
        }
        finally
        {
        }
    }

    public static void performDeleteProgGroup(int intGrpId, int intDestGrpId, string strUploadPath, ref int intMovedProdNo, ref string strDeletedGrpName)
    {
        try
        {
            clsProgram prog = new clsProgram();
            prog.extractProgGrpById(intGrpId, 0);

            int iRecordAffected = prog.deleteProgGroup(prog.progGrpId);

            if (iRecordAffected == 1)
            {
                if (prog.progGrpImage != "")
                {
                    if (File.Exists(strUploadPath + prog.progGrpImage)) { File.Delete(strUploadPath + prog.progGrpImage); }
                }
                clsProduct prod = new clsProduct();
                //HttpContext.Current.Response.Write(grp.grpId + " " + intDestGrpId);
                intMovedProdNo = prod.moveProdByGrpId(prog.progGrpId, intDestGrpId);
                strDeletedGrpName = prog.progGrpName;
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message);
            throw ex;
        }
        finally
        {
        }
    }

    public static void performDeleteProduct(int intProdId, string strUploadPath, ref string strDeletedProdName)
    {
        try
        {
            clsProduct prod = new clsProduct();
            prod.extractProdById(intProdId, 0);

            int iRecordAffected = prod.deleteProduct(prod.prodId);

            if (iRecordAffected == 1)
            {
                prod.deleteRelatedProds(prod.prodId);
                prod.deleteRelatedProd(prod.prodId);
                prod.deleteProductGroup(prod.prodId);
                prod.deletePricing(prod.prodId);

                DataSet ds = prod.getGalleryById(prod.prodId);

                int intRecordAffected = prod.deleteGallery(prod.prodId);

                if (intRecordAffected >= 1)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        string strProdImage = row["PRODGALL_IMAGE"].ToString();

                        string strUploadPathGall = strUploadPath + clsAdmin.CONSTADMGALLERYFOLDER + "/";
                        if (File.Exists(strUploadPathGall + strProdImage)) { File.Delete(strUploadPathGall + strProdImage); }
                    }
                }

                if (prod.prodImage != "")
                {
                    if (File.Exists(strUploadPath + prod.prodImage)) { File.Delete(strUploadPath + prod.prodImage); }
                }

                strDeletedProdName = prod.prodName;
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message);
            throw ex;
        }
        finally
        {
        }
    }

    public static void performCancelRegistration(int intParId, ref string strCancelledParName)
    {
        try
        {
            clsParticipant participant = new clsParticipant();
            participant.extractParticipantById(intParId, 0);

            int intRecordAffected = participant.setParticipantInactive(participant.parId);

            if (intRecordAffected == 1)
            {
                participant.updateParticipantStatus(participant.parId, clsAdmin.CONSTTRANSSTATUSCANCEL);
                strCancelledParName = participant.parFirstName + " " + participant.parLastName;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
    }

    public static void performDeleteShipping(int intShipId, ref int intDeletedShipId)
    {
        try
        {
            clsShipping ship = new clsShipping();
            ship.extractShippingById(intShipId, 0);

            int intRecordAffected = ship.deleteShippingById(ship.shipId);

            if (intRecordAffected == 1)
            {
                intDeletedShipId = ship.shipId;
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
        }
    }

    public static void performDeleteWaiver(int intWaiverId, ref int intDeletedWaiverId)
    {
        try
        {
            clsWaiver waiver = new clsWaiver();
            waiver.extractWaiverById(intWaiverId, 0);

            int intRecordAffected = waiver.deleteWaiverById(waiver.waiverId);

            if (intRecordAffected == 1)
            {
                intDeletedWaiverId = waiver.waiverId;
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message.ToString());
        }
        finally
        {
        }
    }

    public static void performCancelOrder(int intOrdId, ref string strCanceledOrdNo)
    {
        try
        {
            clsOrder ord = new clsOrder();
            ord.extractOrderById(intOrdId);

            int intRecordAffected = ord.setOrderInactive(ord.ordId);

            if (intRecordAffected == 1)
            {
                ord.addOrderStatus(ord.ordId, clsAdmin.CONSTTRANSSTATUSCANCEL);
                ord.setOrderStatus();
                ord.setCancel(ord.ordId);

                strCanceledOrdNo = ord.ordNo;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
        }
    }

    public static void performDeleteTrans(int intId, ref string strDeletedName)
    {
        try
        {
            int intRecordAffected = 0;

            clsOrder ord = new clsOrder();
            ord.extractOrderById(intId);

            intRecordAffected = ord.deleteOrderDelivery(ord.ordId);

            if (intRecordAffected == 1)
            {
                ord.deleteOrderItems(intId);
                ord.deleteStatusById(intId);
                ord.deletePaymentById(intId);

                strDeletedName = ord.ordNo;
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message);
            throw ex;
        }
        finally
        {
        }
    }

    public static void performDeleteHighlight(int intId, string strUploadPath, ref string strDeletedName)
    {
        try
        {
            clsHighlight high = new clsHighlight();
            high.extractHighlightById(intId, 0);

            int intRecordAffected = high.deleteHighlight(high.highId);

            if (intRecordAffected == 1)
            {
                high.deleteHighlightGallery(high.highId);
                if (high.highImage != "")
                {
                    if (File.Exists(strUploadPath + high.highImage)) { File.Delete(strUploadPath + high.highImage); }
                }
                strDeletedName = high.highTitle;
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message);
            throw ex;
        }
        finally
        {
        }
    }


    public static void performDeleteProject(int intProjId, ref string strDeletedName)
    {
        try
        {
            clsProject proj = new clsProject();
            proj.extractProjectById2(intProjId, 0);

            int intRecordAffected = proj.deleteProjectById(proj.projId);

            if (intRecordAffected == 1)
            {
                proj.deleteLanguage(proj.projId);
                proj.deletePaymentGatewayByProjId(proj.projId);
                clsUser user = new clsUser();
                user.deleteUserByProject(proj.projId);

                strDeletedName = proj.projName;
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
        }
    }

    public static void performDeleteOrder(int intOrdId, ref string strDeletedSales)
    {
        try
        {
            clsOrder ord = new clsOrder();
            ord.extractOrderById(intOrdId);

            int intRecordAffected = ord.deleteOrder(intOrdId);

            if (intRecordAffected == 1)
            {
                strDeletedSales = ord.ordNo;
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
        }
    }

    public static void performDeleteCountry(int intCountryId, ref string strDeletedCountry)
    {
        try
        {
            clsCountry country = new clsCountry();
            country.extractCountryById(intCountryId, 0);

            int intRecordAffected = country.deleteCountry(intCountryId);

            if (intRecordAffected == 1)
            {
                strDeletedCountry = country.countryCode;
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
        }
    }


    public static void performDeletePageAuthorization(int intPgAuthId, ref string strDeletedPgAuth)
    {
        try
        {
            clsPage page = new clsPage();
            page.extractPageAuthById(intPgAuthId);

            int intRecordAffected = page.deleteIndPageAuth(page.pgAuthid);

            if (intRecordAffected == 1)
            {
                strDeletedPgAuth = page.IndCompName;
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message);
            throw ex;
        }
        finally
        {
        }
    }
    #endregion

    #region "Print"
    public void printInvoice(int intOrdId, int intType)
    {
        //@ write HTML from HTML file @

        clsOrder ord = new clsOrder();
        ord.extractOrderById4(intOrdId);

        clsConfig config = new clsConfig();
        string strCurrency = config.currency;

        Boolean boolPrint = false;
        int intLength = 0;
        string strItem = "";
        string strItemChar = "";
        string strReplaceItem = "";
        string strContent = "";
        string strContentBtm = "";

        #region "Style"
        string strNoItemStyle = " style=\"color:#fff;\"";
        string strNoItemStyle2 = " style=\"color:#ccc;\"";
        string strItemStyleNormal = " style=\"font-size:7pt; line-height:7pt\"";
        string strItemStyleSmall = " style=\"font-size:6pt; line-height:6pt\"";
        string strItemStyleBig = " style=\"font-size:7.5pt; line-height:7.5pt\"";
        string strItemStyleBig2 = " style=\"font-size:6.5pt; line-height:6.5pt\"";

        #endregion

        // read in the contents of the Receipt.htm HTML template file
        if (intType == 1)
        {
            strContent = File.ReadAllText(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["docBase"] + "invoice_form.htm"));
            strContentBtm = File.ReadAllText(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["docBase"] + "invoice_form_btm.htm"));
        }
        else if (intType == 2)
        {
            strContent = File.ReadAllText(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["docBase"] + "deliveryorder_form.htm"));
            strContentBtm = File.ReadAllText(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["docBase"] + "deliveryorder_form_btm.htm"));
        }

        // create a Document object
        var document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 5, 5, 10, 10);

        // create a new PdfWrite object, writing the output to a MemoryStream
        var output = new MemoryStream();
        var writer = PdfWriter.GetInstance(document, output);

        // open the Document for writing
        document.Open();

        #region "Header"
        #region "Invoice Logo"
        config.extractItemByNameGroup(clsConfig.CONSTNAMEINVOICELOGO, clsConfig.CONSTGROUPINVOICE, 1);
        string strLogo = config.value;

        string strImgPath = HttpContext.Current.Session[clsAdmin.CONSTPROJECTUPLOADPATH] + "/img/cmn/trans.gif";

        if (!string.IsNullOrEmpty(strLogo))
        {
            strImgPath = HttpContext.Current.Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCMSFOLDER + "/" + clsAdmin.CONSTIMAGESFOLDER + "/" + config.value;
            string strFilePath = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + strImgPath + "&w=" + clsAdmin.CONSTADMINVOICELOGOWIDTH + "&h=" + clsAdmin.CONSTADMINVOICELOGOHEIGHT + "&f=1";
        }

        strItem = strImgPath;

        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGLOGOPATH, strItem);
        #endregion

        #region "Company Details"
        /*Company Name*/
        config.extractItemByNameGroup(clsConfig.CONSTNAMECOMPANYNAME, clsConfig.CONSTGROUPINVOICE, 1);
        strItem = config.value;

        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCOMPANYNAME, strItem);
        /*End of Company Name*/

        /*ROC*/
        config.extractItemByNameGroup(clsConfig.CONSTNAMEROC, clsConfig.CONSTGROUPINVOICE, 1);
        strItem = " (" + config.value + ")";

        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCOMPANYROC, strItem);
        /*End of ROC*/

        /*Company Address*/
        config.extractItemByNameGroup(clsConfig.CONSTNAMECOMPANYADDRESS, clsConfig.CONSTGROUPINVOICE, 1);
        strItem = config.value;

        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCOMPANYADDRESS, strItem);
        /*End of Company Address*/

        /*Company Tel*/
        config.extractItemByNameGroup(clsConfig.CONSTNAMECOMPANYTEL, clsConfig.CONSTGROUPINVOICE, 1);
        strItem = "Tel : " + config.value;

        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCOMPANYTEL, strItem);
        /*End of Company Tel*/

        /*Company Email*/
        config.extractItemByNameGroup(clsConfig.CONSTNAMECOMPANYEMAIL, clsConfig.CONSTGROUPINVOICE, 1);
        strItem = "Email : " + config.value;

        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCOMPANYDETAILEMAIL, strItem);
        /*End of Company Email*/

        /*Company Website*/
        config.extractItemByNameGroup(clsConfig.CONSTNAMECOMPANYWEBSITE, clsConfig.CONSTGROUPINVOICE, 1);
        strItem = "Website : " + config.value;

        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCOMPANYWEBSITE, strItem);
        /*End of Company Website*/
        #endregion

        #region "Invoice Type"
        if (intType == 1) { strItem = "SALES ORDER"; }
        else if (intType == 2) { strItem = "DELIVERY ORDER"; }

        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGINVOICETYPE, strItem);
        #endregion
        #endregion

        #region "Top"
        #region "Billing Details"
        if (intType == 1)
        {
            /*Contact No*/
            strItem = ord.contactNo;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCONTACTNO, strItem);

            strItem = ord.contactNo2;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCONTACTNO2, strItem);

            strItem = ord.fax;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGFAX, strItem);
            /*End of Contact No*/

            /*Contact Person*/
            strItem = ord.name;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCONTACTPERSON, strItem);
            /*End of Contact Person*/

            /*Company Name / Email*/
            if (!string.IsNullOrEmpty(ord.company)) { strItem = ord.company; }
            else { strItem = ord.email; }

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCOMPANYEMAIL, strItem);
            /*End of Company Name / Email*/

            /*Address*/
            strItem = ord.add;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGADDRESS, strItem);
            /*End of Address*/

            /*Poscode*/
            strItem = ord.poscode;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGPOSCODE, strItem);
            /*End of Poscode*/

            /*City*/
            if (!string.IsNullOrEmpty(ord.city)) { strItem = ", " + ord.city; }

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCITY, strItem);
            /*End of City*/

            /*State*/
            strItem = ord.state;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGSTATE, strItem);
            /*End of State*/

            /*Country*/
            strItem = ord.countryName;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCOUNTRY, strItem);
            /*End of Country*/
        }
        #endregion

        #region "Delivery Details"
        else if (intType == 2)
        {
            /*Shipping Contact No*/
            strItem = ord.shippingContactNo;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCONTACTNO, strItem);

            strItem = ord.shippingContactNo2;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCONTACTNO2, strItem);

            strItem = ord.shippingFax;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGFAX, strItem);
            /*End of Shipping Contact No*/

            /*Shipping Contact Person*/
            strItem = ord.shippingName;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCONTACTPERSON, strItem);
            /*End of Shipping Contact Person*/

            /*Shipping Company Name / Email*/
            if (!string.IsNullOrEmpty(ord.shippingCustCompany)) { strItem = ord.shippingCustCompany; }
            else { strItem = ord.shippingEmail; }

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCOMPANYEMAIL, strItem);
            /*End of Shipping Company Name / Email*/

            /*Shipping Address*/
            strItem = ord.shippingAdd;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGADDRESS, strItem);
            /*End of Shipping Address*/

            /*Shipping Poscode*/
            strItem = ord.shippingPoscode;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGPOSCODE, strItem);
            /*End of Shipping Poscode*/

            /*Shipping City*/
            if (!string.IsNullOrEmpty(ord.shippingCity)) { strItem = ", " + ord.shippingCity; }

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCITY, strItem);
            /*End of Shipping City*/

            /*Shipping State*/
            strItem = ord.shippingState;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGSTATE, strItem);
            /*End of Shipping State*/

            /*Shipping Country*/
            strItem = ord.shippingCountryName;

            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGCOUNTRY, strItem);
            /*End of Shipping Country*/
        }
        #endregion

        #region "Sales No"
        strItem = intType == 1 ? "SALES ORDER NO." : "D/O NO.";
        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGINVOICEDONO, strItem);

        strItem = ord.ordNo;
        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGSALESNO, strItem);
        #endregion

        #region "Sales Date"
        /*Invoice Sales Date*/
        strItem = ord.ordCreation.ToString("dd MMM yyyy");

        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGINVOICESALESDATE, strItem);
        /*End of Invoice Sales Date*/

        /*DO Sales Date*/
        if (ord.shippingDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
        {
            strItem = ord.shippingDate.ToString("dd MMM yyyy");
        }
        else { strItem = ord.ordCreation.ToString("dd MMM yyyy"); }

        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGDOSALESDATE, strItem);
        /*End of DO Sales Date*/
        #endregion

        #region "Customer No"
        strItem = ord.customerNo;

        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGSALESCUSTOMERNO, strItem);
        #endregion
        #endregion

        #region "Middle"
        #region "Sales Items"
        DataSet ds = new DataSet();
        ds = ord.getOrderItemsByOrderId2(intOrdId);

        int intItemCount = 0;
        decimal decUnitPrice = Convert.ToDecimal("0.00");
        decimal decShipping = Convert.ToDecimal("0.00"); 
        int intQty = 0;
        decimal decSubTotalItem = Convert.ToDecimal("0.00");
        decimal decSubTotal = Convert.ToDecimal("0.00");
        decimal decTotal = Convert.ToDecimal("0.00");
        string strOrdItems = "";

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            intItemCount += 1;
            decUnitPrice = Convert.ToDecimal(row["ORDDETAIL_PRODPRICE"]);
            intQty = int.Parse(row["ORDDETAIL_PRODQTY"].ToString());

            if (intType == 1)
            {
                strOrdItems += "<tr>";

                strOrdItems += "<td width=\"5%\" style=\"text-align:center;\">" + intItemCount.ToString() + "</td>";
                strOrdItems += "<td width=\"65%\">" + row["ORDDETAIL_PRODNAME"].ToString() + "<br/>" + row["ORDDETAIL_PRODSNAPSHOT"].ToString() + "</td>";
                strOrdItems += "<td width=\"10%\" style=\"text-align:center;\">" + String.Format("{0:#,0}", intQty) + "</td>";
                strOrdItems += "<td width=\"10%\" style=\"text-align:center;\">" + String.Format("{0:N}", decUnitPrice) + "</td>";
                strOrdItems += "<td width=\"10%\" style=\"text-align:center;\">" + String.Format("{0:N}", row["ORDDETAIL_PRODTOTAL"]) + "</td>";

                strOrdItems += "</tr>";
            }
            else if (intType == 2)
            {
                strOrdItems += "<tr>";

                strOrdItems += "<td width=\"5%\" style=\"text-align:center;\">" + intItemCount.ToString() + "</td>";
                strOrdItems += "<td width=\"65%\">" + row["ORDDETAIL_PRODNAME"].ToString() + "<br/>" + row["ORDDETAIL_PRODSNAPSHOT"].ToString() + "</td>";
                strOrdItems += "<td width=\"10%\" style=\"text-align:center;\">" + String.Format("{0:#,0}", intQty) + "</td>";

                strOrdItems += "</tr>";
            }

            decSubTotalItem = decUnitPrice * intQty;
            decSubTotal += decSubTotalItem;
        }
        #endregion

        strItem = strOrdItems;

        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGSALESITEM, strItem);
        #endregion

        #region "Sales Item Footer"
        #region "Sales Currency"
        strItem = strCurrency;

        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGSALESCURRENCY, strItem);
        #endregion

        decShipping = ord.ordShipping;

        if (decShipping != 0 && intType == 1)
        {
            #region "Sales Subtotal"
            strItem = "SUBTOTAL (" + strCurrency + ")";
            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGSALESSUBTOTALTEXT, strItem);

            strItem = String.Format("{0:N}", decSubTotal); //decSubTotal.ToString();
            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGSALESSUBTOTAL, strItem);
            #endregion

            #region "Sales Shipping"
            strItem = "SHIPPING (" + strCurrency + ")";
            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGSALESSHIPPINGTEXT, strItem);

            strItem = String.Format("{0:N}", decShipping); //decShipping.ToString();
            strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGSALESSHIPPING, strItem);
            #endregion
        }

        #region "Sales Total"
        decTotal = decSubTotal + decShipping;
        strItem = String.Format("{0:N}", decTotal); //decTotal.ToString();

        strContent = strContent.Replace(clsAdmin.CONSTHTMLTAGSALESTOTAL, strItem);
        #endregion
        #endregion

        #region "Bottom"
        #region "Sales Remarks"
        strItem = ord.ordRemarks;

        strContentBtm = strContentBtm.Replace(clsAdmin.CONSTHTMLTAGSALESREMARKS, strItem);
        #endregion

        #region "Invoice Remarks"
        config.extractItemByNameGroup(clsConfig.CONSTNAMEINVOICEREMARKSPDF, clsConfig.CONSTGROUPINVOICE, 1);
        string strInvoiceRemarks = config.longValue;
        strItem = config.longValue;

        strContentBtm = strContentBtm.Replace(clsAdmin.CONSTHTMLTAGSALESINVOICEREMARKS, strItem);
        #endregion

        #region "Invoice Signature"
        config.extractItemByNameGroup(clsConfig.CONSTNAMEINVOICESIGNATUREPDF, clsConfig.CONSTGROUPINVOICE, 1);
        string strInvoiceSignature = config.longValue;
        strItem = config.longValue;

        strContentBtm = strContentBtm.Replace(clsAdmin.CONSTHTMLTAGSALESINVOICESIGNATURE, strItem);
        #endregion
        #endregion

        // parse the HTML string into a collection of elements
        var parsedHTMLElements = HTMLWorker.ParseToList(new StringReader(strContentBtm), null);

        PdfPTable tableMain = new PdfPTable(1);
        //tableMain.TotalHeight = document.PageSize.Height;
        tableMain.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;

        PdfPCell cellMain = new PdfPCell();
        cellMain.Border = 0;

        // enumerate the elements, adding each one to the Table
        foreach (iTextSharp.text.IElement htmlElement in parsedHTMLElements)
        {
            cellMain.AddElement(htmlElement);
        }

        tableMain.AddCell(cellMain);
        // set position table when display
        tableMain.WriteSelectedRows(0, -1, document.LeftMargin, document.BottomMargin + 150, writer.DirectContent);

        TextReader reader = new StringReader(strContent);

        HTMLWorker worker = new HTMLWorker(document);
        worker.StartDocument();
        worker.Parse(reader);
        worker.EndDocument();
        worker.Close();

        document.Close();

        HttpContext.Current.Response.ContentType = "application/pdf";

        if (intType == 1)
        {
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=invoice-{0}.pdf", ord.ordNo));
        }
        else if (intType == 2)
        {
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=Delivery Order-{0}.pdf", ord.ordNo));
        }
        HttpContext.Current.Response.BinaryWrite(output.ToArray());
    }
    #endregion

    #region "Sales Quick Done"
    public static int performSalesQuickDone(int intOrdId)
    {
        clsOrder ord = new clsOrder();
        clsConfig config = new clsConfig();
        int intRecordAffected = 0;

        int oldOrdStatus;

        ord.extractOrderById3(intOrdId);
        oldOrdStatus = ord.ordStatus;

        if (ord.ordDone == 0)
        {
            intRecordAffected = clsMis.performUpdateOrderStatus(intOrdId, clsAdmin.CONSTORDERSTATUSINVOICEDONE, true);

            if (intRecordAffected == 1)
            {
                ord.extractOrderById3(intOrdId);
                ord.setDone(intOrdId);

                if (oldOrdStatus == clsAdmin.CONSTORDERSTATUSINVOICENEW)
                {
                    DateTime dtPayDate = DateTime.Now;

                    config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYMENTTYPE, clsConfig.CONSTGROUPINVOICE, 1);
                    int payType = Convert.ToInt16(config.value);

                    if (!ord.isPaymentExist(intOrdId))
                    {
                        intRecordAffected = ord.addOrderPayment2(intOrdId, dtPayDate, payType, ord.ordTotal, "");

                        if (intRecordAffected == 1)
                        {
                            intRecordAffected = clsMis.performUpdateOrderStatus(intOrdId, clsAdmin.CONSTORDERSTATUSINVOICEPAY, false);

                            ord.setOrderPayment();
                        }
                    }

                    config.extractItemByNameGroup(clsConfig.CONSTNAMESHIPPINGCOMPANY, clsConfig.CONSTGROUPINVOICE, 1);
                    int shipCom = Convert.ToInt16(config.value);

                    clsMis mis = new clsMis();
                    string shipComName = mis.getListNameByListValue(shipCom.ToString(), clsAdmin.CONSTNAMESHIPPINGCOM, 1, 1);

                    if (!ord.isShippingExist(intOrdId))
                    {
                        intRecordAffected = ord.updateDeliveryDetails(intOrdId, ord.ordCreation, shipCom, shipComName, "");

                        if (intRecordAffected == 1)
                        {
                            intRecordAffected = clsMis.performUpdateOrderStatus(intOrdId, clsAdmin.CONSTORDERSTATUSINVOICEDELIVERY, false);
                        }
                    }
                }
                else if (oldOrdStatus == clsAdmin.CONSTORDERSTATUSINVOICEPAY)
                {
                    config.extractItemByNameGroup(clsConfig.CONSTNAMESHIPPINGCOMPANY, clsConfig.CONSTGROUPINVOICE, 1);
                    int shipCom = Convert.ToInt16(config.value);

                    clsMis mis = new clsMis();
                    string shipComName = mis.getListNameByListValue(shipCom.ToString(), clsAdmin.CONSTNAMESHIPPINGCOM, 1, 1);

                    if (!ord.isShippingExist(intOrdId))
                    {
                        intRecordAffected = ord.updateDeliveryDetails(intOrdId, ord.ordCreation, shipCom, shipComName, "");

                        if (intRecordAffected == 1)
                        {
                            intRecordAffected = clsMis.performUpdateOrderStatus(intOrdId, clsAdmin.CONSTORDERSTATUSINVOICEDELIVERY, false);
                        }
                    }
                }
            }
        }

        return intRecordAffected;
    }


    public DataSet getStateList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.STATE_ID, A.STATE_CODE, A.STATE_NAME, A.STATE_ORDER, A.STATE_COUNTRY, A.STATE_ACTIVE, A.STATE_DEFAULT" +
                     " FROM TB_STATE A";

            if (intActive != 0)
            {
                strSql += " WHERE A.STATE_ACTIVE = ?";
                cmd.Parameters.Add("p_STATE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getCountryList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.COUNTRY_ID, A.COUNTRY_CODE, A.COUNTRY_NAME, A.COUNTRY_ORDER, A.COUNTRY_ACTIVE, " +
                     "A.COUNTRY_DEFAULT, A.SHOW_STATE " +
                     "FROM TB_COUNTRY A";

            if (intActive != 0)
            {
                strSql += " WHERE COUNTRY_ACTIVE = ?";
                cmd.Parameters.Add("p_COUNTRY_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }


    public Boolean extractCountryById(string strCountryCode, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.COUNTRY_ID, A.COUNTRY_CODE, A.COUNTRY_NAME, A.COUNTRY_ORDER, A.COUNTRY_ACTIVE," +
                     " A.COUNTRY_DEFAULT, A.SHOW_STATE" +
                     " FROM TB_COUNTRY A" +
                     " WHERE A.COUNTRY_CODE = ?";

            cmd.Parameters.Add("p_COUNTRY_CODE", OdbcType.VarChar, 20).Value = strCountryCode;

            if (intActive != 0)
            {
                strSql += " AND A.COUNTRY_ACTIVE = ?";
                cmd.Parameters.Add("p_COUNTRY_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                countryId = ds.Tables[0].Rows[0]["COUNTRY_ID"].ToString();
                countryCode = ds.Tables[0].Rows[0]["COUNTRY_CODE"].ToString();
                countryName = ds.Tables[0].Rows[0]["COUNTRY_NAME"].ToString();
                countryOrder = int.Parse(ds.Tables[0].Rows[0]["COUNTRY_ORDER"].ToString());
                countryActive = int.Parse(ds.Tables[0].Rows[0]["COUNTRY_ACTIVE"].ToString());
                countryDefault = int.Parse(ds.Tables[0].Rows[0]["COUNTRY_DEFAULT"].ToString());
                showState = int.Parse(ds.Tables[0].Rows[0]["SHOW_STATE"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractCountryById(int intCountryId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.COUNTRY_ID, A.COUNTRY_CODE, A.COUNTRY_NAME, A.COUNTRY_ORDER, A.COUNTRY_ACTIVE," +
                     " A.COUNTRY_DEFAULT, A.SHOW_STATE" +
                     " FROM TB_COUNTRY A" +
                     " WHERE A.COUNTRY_ID = ?";

            cmd.Parameters.Add("p_COUNTRY_ID", OdbcType.BigInt, 9).Value = intCountryId;

            if (intActive != 0)
            {
                strSql += " AND A.COUNTRY_ACTIVE = ?";
                cmd.Parameters.Add("p_COUNTRY_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                countryId = ds.Tables[0].Rows[0]["COUNTRY_ID"].ToString();
                countryCode = ds.Tables[0].Rows[0]["COUNTRY_CODE"].ToString();
                countryName = ds.Tables[0].Rows[0]["COUNTRY_NAME"].ToString();
                countryOrder = int.Parse(ds.Tables[0].Rows[0]["COUNTRY_ORDER"].ToString());
                countryActive = int.Parse(ds.Tables[0].Rows[0]["COUNTRY_ACTIVE"].ToString());
                countryDefault = int.Parse(ds.Tables[0].Rows[0]["COUNTRY_DEFAULT"].ToString());
                showState = int.Parse(ds.Tables[0].Rows[0]["SHOW_STATE"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int deleteCountry(int intCountryId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_COUNTRY WHERE COUNTRY_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_COUNTRY_ID", OdbcType.BigInt, 9).Value = intCountryId;
            cmd.Parameters["p_COUNTRY_ID"].Value = intCountryId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    #endregion


    public static void saveThumbnail(string strImagePath, string strThumbPath, ref string strImageName, int intWidthTarget, int intHeightTarget)
    {
        // get ratio
        int imageNewWidth;
        int imageNewHeight;
        double imageRatio;

        FileStream fs = new FileStream(strImagePath, FileMode.Open, FileAccess.Read);
        System.Drawing.Image imgTempOri = System.Drawing.Image.FromStream(fs);

        int intWidth = imgTempOri.Width;
        int intHeight = imgTempOri.Height;

        if (intWidth <= intWidthTarget && intHeight <= intHeightTarget)
        {
            imageNewWidth = intWidth;
            imageNewHeight = intHeight;
        }
        else
        {
            double imageWidthRatio;
            double imageHeightRatio;

            imageWidthRatio = (Convert.ToDouble(intWidth) / Convert.ToDouble(intWidthTarget));
            imageHeightRatio = (Convert.ToDouble(intHeight) / Convert.ToDouble(intHeightTarget));

            if (imageWidthRatio <= imageHeightRatio)
            {
                imageRatio = imageWidthRatio;
                imageNewWidth = intWidthTarget;
                imageNewHeight = Convert.ToInt16(Math.Round((Convert.ToDouble(intHeight) / Convert.ToDouble(imageRatio)), 0));
            }
            else
            {
                imageRatio = imageHeightRatio;
                imageNewHeight = intHeightTarget;
                imageNewWidth = Convert.ToInt16(Math.Round((Convert.ToDouble(intWidth) / Convert.ToDouble(imageRatio)), 0));
            }
        }
        // end of get ratio

        System.Drawing.Graphics gTemp = default(System.Drawing.Graphics);
        System.Drawing.Bitmap bmpTemp = new System.Drawing.Bitmap(imageNewWidth, imageNewHeight);
        gTemp = System.Drawing.Graphics.FromImage(bmpTemp);
        gTemp.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
        gTemp.DrawImage(imgTempOri, 0, 0, bmpTemp.Width + 1, bmpTemp.Height + 1);

        strImageName = strImageName + "_n.jpg";
        System.Drawing.Image imageOut = bmpTemp;
        imageOut.Save(strThumbPath + "/" + strImageName, System.Drawing.Imaging.ImageFormat.Jpeg);

        imageOut.Dispose();
        gTemp.Dispose();
        bmpTemp.Dispose();
        imgTempOri.Dispose();
        fs.Dispose();
    }
    
    #region "Draw Water on Image Module"
    public System.Drawing.Bitmap setImageOpacity(System.Drawing.Image image, float opacity)
    {
        try
        {
            //create a Bitmap the size of the image provided  
            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(image.Width, image.Height);

            //create a graphics object from the image  
            using (System.Drawing.Graphics gfx = System.Drawing.Graphics.FromImage(bmp))
            {

                //create a color matrix object  
                System.Drawing.Imaging.ColorMatrix matrix = new System.Drawing.Imaging.ColorMatrix();

                //set the opacity  
                matrix.Matrix33 = opacity;

                //create image attributes  
                System.Drawing.Imaging.ImageAttributes attributes = new System.Drawing.Imaging.ImageAttributes();

                //set the color(opacity) of the image  
                attributes.SetColorMatrix(matrix, System.Drawing.Imaging.ColorMatrixFlag.Default, System.Drawing.Imaging.ColorAdjustType.Bitmap);

                //now draw the image  
                gfx.DrawImage(image, new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, image.Width, image.Height, System.Drawing.GraphicsUnit.Pixel, attributes);
            }
            return bmp;
        }
        catch (Exception ex)
        {

            return null;
        }
    }

    public System.Drawing.Bitmap drawWatermarkImage(System.Drawing.Bitmap bitOriginal, System.Drawing.Bitmap bitWatermark, float opacity)
    {
        double oriWidth = bitOriginal.Width;
        double oriHeight = bitOriginal.Height;

        double waterWidth = bitWatermark.Width;
        double waterHeight = bitWatermark.Height;

        bool oriWidthBigger = oriWidth > waterWidth;
        bool oriHeightBigger = oriHeight > waterHeight;

        double waterWidthNew = bitOriginal.Width / 2;
        double waterHeightNew = bitOriginal.Height / 2;

        double diffWidth = Math.Abs(oriWidth - waterWidthNew);
        double diffHeight = Math.Abs(oriHeight - waterHeightNew);

        bool oriLandscape = oriWidth > oriHeight;
        bool waterLandscape = waterWidth > waterHeight;

        bool oriBigger = oriWidthBigger && oriHeightBigger;
        double ratio = 0;
        if (!oriLandscape && waterLandscape || (oriLandscape && waterLandscape && diffWidth < diffHeight)) // follow width
        {
            ratio = oriWidth / waterWidth;
            waterWidthNew = waterWidth * ratio;
            waterHeightNew = waterHeight * ratio;
        }
        else if (oriLandscape && !waterLandscape || (oriLandscape && waterLandscape && diffWidth > diffHeight)) // follow height
        {
            ratio = oriHeight / waterHeight;
            waterWidthNew = waterWidth * ratio;
            waterHeightNew = waterHeight * ratio;
        }

        int waterPosX = (int) (oriWidth - waterWidthNew) / 2;
        int waterPosY = (int) (oriHeight - waterHeightNew) / 2;

        bitWatermark = new System.Drawing.Bitmap(bitWatermark, new System.Drawing.Size((int)waterWidthNew, (int)waterHeightNew));
        System.Drawing.Bitmap bitCombine = new System.Drawing.Bitmap(bitOriginal.Width, bitOriginal.Height);
        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitCombine);
        g.DrawImage(bitOriginal, 0, 0, bitOriginal.Width, bitOriginal.Height);
        g.DrawImageUnscaled(setImageOpacity(bitWatermark, opacity), waterPosX, waterPosY);

        return bitCombine;
    }
    public System.Drawing.Bitmap drawWatermarkText(System.Drawing.Bitmap bitOriginal,string strWatermark, int opacity)
    {
        bitOriginal = new System.Drawing.Bitmap(bitOriginal, new System.Drawing.Size(bitOriginal.Width, bitOriginal.Height));
        System.Drawing.Graphics graphicsObj = System.Drawing.Graphics.FromImage(bitOriginal);
        System.Drawing.Font font = new System.Drawing.Font("Tahoma", 50, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
        System.Drawing.Brush brush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(opacity, 0, 0, 0));
        System.Drawing.Point postionWaterMark = new System.Drawing.Point((bitOriginal.Width / 2), (bitOriginal.Height / 2));
        System.Drawing.StringFormat format = new System.Drawing.StringFormat();
        format.LineAlignment = System.Drawing.StringAlignment.Center;
        format.Alignment = System.Drawing.StringAlignment.Center;
        graphicsObj.DrawString(strWatermark, font, brush, postionWaterMark, format);
        return bitOriginal;
    }

    #endregion



    public void registerGAScript(Page page)
    {
        clsConfig config = new clsConfig();

        config.extractItemByNameGroup(clsConfig.CONSTNAMEGOOGLEANALYTIC, clsConfig.CONSTGROUPGOOGLE, 1);
        string strGA = config.longValue;

        config.extractItemByNameGroup("Auto Script Tag", clsConfig.CONSTGROUPGOOGLE, 1);
        string strAutoScript = config.value;

        if ((!string.IsNullOrEmpty(strGA)) && (!page.ClientScript.IsStartupScriptRegistered("GA")))
        {
            string strJS = null;
            if (strAutoScript == "True")
            {
                strJS = "<script type=\"text/javascript\">";
                strJS += strGA;
                strJS += "</script>";
            }
            else
            {
                strJS += strGA;
            }

            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "GA", strJS, false);
        }
    }


    //Website View
    public static bool isMobileBrowser()
    {
        //GETS THE CURRENT USER CONTEXT
        HttpContext context = HttpContext.Current;

        //FIRST TRY BUILT IN ASP.NT CHECK
        //if (context.Request.Browser.IsMobileDevice)
        //{
        //    return true;
        //}
        //THEN TRY CHECKING FOR THE HTTP_X_WAP_PROFILE HEADER
        if (context.Request.ServerVariables["HTTP_X_WAP_PROFILE"] != null)
        {
            return true;
        }
        //THEN TRY CHECKING THAT HTTP_ACCEPT EXISTS AND CONTAINS WAP
        if (context.Request.ServerVariables["HTTP_ACCEPT"] != null &&
            context.Request.ServerVariables["HTTP_ACCEPT"].ToLower().Contains("wap"))
        {
            return true;
        }
        //AND FINALLY CHECK THE HTTP_USER_AGENT 
        //HEADER VARIABLE FOR ANY ONE OF THE FOLLOWING
        if (context.Request.ServerVariables["HTTP_USER_AGENT"] != null)
        {
            //Create a list of all mobile types
            string[] mobiles = new[] {
                    "midp", "j2me", "avant", "docomo",
                    "palm", "palmos", "palmsource",
                    "240x320", "opwv", "chtml", "pda",
                    "windows ce", "windows phone os", "mmp/",
                    "blackberry", "mib/", "symbian",
                    "wireless", "nokia", "hand", "android",
                    "phone", "cdm", "up.b", "audio",
                    "SIE-", "SEC-", "samsung", "HTC", "eric",
                    "mot-", "mitsu", "sagem", "sony","alcatel",
                    "NEC", "philips", "voda", "panasonic", "sharp",
                    "rover", "pocket", "benq", "java",
                    "iemobile", "opera mobi", "moto",
                    "sany", "sendo", "ipod", "iphone"
            };

            //Loop through each item in the list created above 
            //and check if the header contains that text
            foreach (string s in mobiles)
            {
                if (context.Request.ServerVariables["HTTP_USER_AGENT"].ToLower().Contains(s.ToLower()))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public static string formatMobileURL(string strURL)
    {
        string returnMobileUrl = "";

        if (strURL != null && strURL != "")
        {
            string[] arrItems = strURL.Split(clsSetting.CONSTDATESEPERATOR);
            string[] arrAspx = arrItems[arrItems.Length - 1].Split(clsSetting.CONSTDOTSEPARATOR);

            if (arrAspx.Length == 2)
            {
                arrAspx[0] += "mb";
                arrItems[arrItems.Length - 1] = string.Join(".", arrAspx);
            }

            arrItems[arrItems.Length - 2] += "/mobile";

            returnMobileUrl = string.Join(clsSetting.CONSTDATESEPERATOR.ToString(), arrItems);
        }

        return returnMobileUrl;
    }

    public static string formatDesktopURL(string strURL)
    {
        string returnDesktopUrl = "";

        if (strURL != null && strURL != "")
        {
            List<string> arrItems = new List<string>(strURL.Split(clsSetting.CONSTDATESEPERATOR));
            string[] arrAspx = arrItems[arrItems.Count - 1].Split(clsSetting.CONSTDOTSEPARATOR);

            if (arrAspx.Length == 2)
            {
                arrAspx[0] = arrAspx[0].Remove(arrAspx[0].Length - 2);
                arrItems[arrItems.Count - 1] = string.Join(".", arrAspx);
            }

            arrItems.RemoveAt(arrItems.Count - 2);

            returnDesktopUrl = string.Join(clsSetting.CONSTDATESEPERATOR.ToString(), arrItems.ToArray());
        }
        return returnDesktopUrl;
    }

    public static string formatWebsiteViewURL(string strView, int intPgId)
    {
        string returnWebsiteViewUrl = "";
        string strNewTemplate = "";

        if (intPgId <= 0)
        {
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["pgid"]))
            {
                int intTryParse;
                if (int.TryParse(HttpContext.Current.Request["pgid"], out intTryParse))
                {
                    intPgId = intTryParse;
                }
            }
        }

        if (intPgId > 0)
        {
            clsPage page = new clsPage();
            if (strView == clsSetting.CONSTWEBSITEVIEWDESKTOP)
            {
                if (page.extractPageById(intPgId, 1))
                {
                    strNewTemplate = page.template;
                    returnWebsiteViewUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRDEFAULTPATH + strNewTemplate;
                }
            }
            else if (strView == clsSetting.CONSTWEBSITEVIEWMOBILE)
            {
                if (page.extractMobilePageById(intPgId, 1, 1))
                {
                    strNewTemplate = page.mobileTemplate;
                    returnWebsiteViewUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strNewTemplate;
                }
            }
        }
        else
        {
            string strCurPageUrl = clsMis.getCurrentPageName();
            if (strView == clsSetting.CONSTWEBSITEVIEWDESKTOP)
            {
                returnWebsiteViewUrl = formatDesktopURL(strCurPageUrl);
            }
            else if (strView == clsSetting.CONSTWEBSITEVIEWMOBILE)
            {
                returnWebsiteViewUrl = formatMobileURL(strCurPageUrl);
            }
        }

        return returnWebsiteViewUrl;
    }
    //End Website View
    public static bool isSingleLogin()
    {
        return HttpContext.Current.Session["ADMROOT"] == null && HttpContext.Current.Session["CTRLID"] != null;
    }

    public static string formatPrice(decimal value)
    {
        return string.Format("{0:n2}", value);
    }

    public static string formatCKEditor(string elementID, string width, string height)
    {
        return "CKEDITOR.replace('" + elementID + "',{" +
                    "width:'"+ width + "'," +
                    "height:'"+ height + "'," +
                    "toolbar: 'User3'," +
                    "baseHref: '" + HttpContext.Current.Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "'," + 
        "});";
    }
}
