﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsAdmPage
/// </summary>
public class clsAdmPage : dbconnBase
{


    #region "Public Methods"
    public clsAdmPage()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet getAdminPageList(bool boolAll, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.ADMPAGE_ID, A.ADMPAGE_NAME, A.ADMPAGE_VALUE, A.ADMPAGE_URL, A.ADMPAGE_PARENT, A.ADMPAGE_ORDER, A.ADMPAGE_ACCESS, A.ADMPAGE_SHOWINMENU, A.ADMPAGE_ACTIVE" +
                     " FROM TB_ADMINPAGE A" +
                     " WHERE 1 = 1";

            if (!boolAll)
            {
                strSql += " AND A.ADMPAGE_ACCESS = ?";

                cmd.Parameters.Add("p_ADMPAGE_ACCESS", OdbcType.Int, 1).Value = 1;
            }

            if (intActive != 0)
            {
                strSql += " AND A.ADMPAGE_ACTIVE = ?";

                cmd.Parameters.Add("p_ADMPAGE_ACTIVE", OdbcType.Int, 1).Value = 1;
            }

            strSql += " ORDER BY ADMPAGE_PARENT ASC, ADMPAGE_ORDER ASC";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    #endregion
}