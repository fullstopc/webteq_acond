﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;
using System.Configuration;

/// <summary>
/// Summary description for dbconnBase
/// </summary>

public class dbconnBase : clsLog
{
    #region "Properties"

    protected OdbcConnection dbConnection;

    #endregion

    #region "Public Methods"

    public dbconnBase()
	{
	}

    protected OdbcConnection openConn()
    {
        if (dbConnection != null) return dbConnection;

        dbConnection = new OdbcConnection();
        dbConnection.ConnectionString = ConfigurationManager.ConnectionStrings["MySqlConn"].ConnectionString;
        dbConnection.Open();
        return dbConnection;
    }

    protected void closeConn()
    {
        if (dbConnection != null)
        {
            dbConnection.Close();
            dbConnection = null;
        }
    }

    #endregion
}
