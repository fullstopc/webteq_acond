﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

public class Controller : dbconnBase
{
    protected string _prefix { get; set; }
    protected string _tableName { get; set; }
    protected string _createdAt { get; set; }
    protected string _updatedAt { get; set; }
    public DataRow row
    {
        set
        {
            foreach (Model model in _columns)
            {
                this.GetType().GetProperty(model.name).SetValue(this, value[model.columnName] == DBNull.Value ? null :  Convert.ChangeType(value[model.columnName], model.type), null);
            }

            if(_columnsForeign != null && _columnsForeign.Count > 0)
            {
                foreach (Model model in _columnsForeign)
                {
                    this.GetType().GetProperty(model.name).SetValue(this, value[model.columnName] == DBNull.Value ? null : Convert.ChangeType(value[model.columnName], model.type), null);
                }
            }
        }
    }

    public string _ID { get; set; }
    public List<Model> _columns { get; set; }
    public List<Model> _columnsForeign { get; set; }


    public DataView getDataView()
    {
        DataTable dt = getDataTable();
        if(dt!= null)
        {
            return new DataView(dt);
        }
        return null;
    }
    public virtual DataTable getDataTable()
    {
        DataSet ds = getDataSet();
        if (ds.Tables.Count == 1)
        {
            return ds.Tables[0];
        }
        return null;
    }
    public virtual DataSet getDataSet()
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            string strSql = string.Format("SELECT * FROM {0} WHERE 1 = 1", _tableName);

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public void setValue()
    {
        foreach (Model model in _columns)
        {
            model.value = this.GetType().GetProperty(model.name).GetValue(this, null);
        }
    }
    public void resetValue()
    {
        foreach (Model model in _columns)
        {
            this.GetType().GetProperty(model.name).SetValue(this, model.defaultValue == null ? null : Convert.ChangeType(model.defaultValue, model.type), null);
        }
    }

    public bool extractByID(int ID)
    {
        resetValue();

        DataTable dt = getDataTable();
        DataView dv = new DataView(dt);
        dv.RowFilter = _ID + " = " + ID;

        if (dv.Count == 1)
        {
            DataRowView row = dv[0];
            foreach (Model model in _columns)
            {
                object value = null;
                if (row[model.columnName] == DBNull.Value)
                {
                    value = model.defaultValue;
                }
                else
                {
                    value = Convert.ChangeType(row[model.columnName], model.type);
                }
                this.GetType().GetProperty(model.name).SetValue(this, value, null);
            }
            return true;
        }
        return false;
    }
    public bool extractData(string name, object value)
    {
        return extractData(new Dictionary<string, object>()
        {
            { name, value }
        });
    }
    public bool extractData(Dictionary<string, object> parameters)
    {
        resetValue();

        DataTable dt = getDataTable();
        DataView dv = new DataView(dt);
        dv.RowFilter = parameters.AsEnumerable().Select(x => x.Key + " = '" + x.Value + "'").Aggregate((current, next) => current + " AND " + next);

        if (dv.Count == 1)
        {
            DataRowView row = dv[0];
            foreach (Model model in _columns)
            {
                object value = null;
                if (row[model.columnName] == DBNull.Value)
                {
                    value = model.defaultValue;
                }
                else
                {
                    value = Convert.ChangeType(row[model.columnName], model.type);
                }
                this.GetType().GetProperty(model.name).SetValue(this, value, null);
            }
            return true;
        }
        return false;
    }
    public virtual bool add()
    {
        int intRecordAffected = 0;

        try
        {
            setValue();
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<Model> models = _columns.Where(x => !Object.Equals(x.defaultValue, x.value) && x.fillable).ToList();
            List<string> columns = models.Select(x => x.columnName).ToList();
            List<string> parameters = models.Select(x => "?").ToList();

            List<string> timestamp_columns = new List<string>();
            if (!string.IsNullOrEmpty(_createdAt)) { timestamp_columns.Add(_createdAt); }
            if (!string.IsNullOrEmpty(_updatedAt)) { timestamp_columns.Add(_updatedAt); }

            List<string> timestamp_parameters = timestamp_columns.Select(x => "NOW()").ToList();

            bool isTimestamp = (_createdAt != null || _updatedAt != null);

            string query = string.Format("INSERT INTO {0} ({1}" + (isTimestamp ? ",{3}" : "")+ ") VALUES ({2}" + (isTimestamp ? ",{4}" : "") + ")",
                                        _tableName,
                                        string.Join(",", columns.ToArray()),
                                        string.Join(",", parameters.ToArray()),
                                        string.Join(",", timestamp_columns.ToArray()),
                                        string.Join(",", timestamp_parameters.ToArray()));


            foreach (Model model in models)
            {
                cmd.Parameters.AddWithValue("@" + model.name, model.value);
            }

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;
            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                if(this.GetType().GetProperty("ID") != null)
                {
                    int tempID = Convert.ToInt16(cmd2.ExecuteScalar());
                    this.GetType().GetProperty("ID").SetValue(this, tempID, null);
                }
                //ID = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected == 1;
    }
    public virtual bool update()
    {
        int intRecordAffected = 0;

        try
        {
            setValue();
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<Model> models = _columns
                .Where(x => x.value != null && x.fillable && !x.value.Equals(x.defaultValue))
                .Where(x => x.name != _ID).ToList();
            List<string> columns = models.Select(x => x.columnName + " = " + "?").ToList();

            bool isTimestamp = (_updatedAt != null);

            string query = string.Format("UPDATE {0} SET {1}"+ (isTimestamp ? ",{3}" : "") + " WHERE {2}",
                                        _tableName,
                                        string.Join(",", columns.ToArray()),
                                        _ID + " = " + "?",
                                        _updatedAt + " = " + "NOW()");

            foreach (Model model in models)
            {
                cmd.Parameters.AddWithValue(model.columnName, model.value);
            }
            cmd.Parameters.AddWithValue(_ID, _columns.Where(x => x.columnName == _ID).First().value);

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;

            intRecordAffected = cmd.ExecuteNonQuery();


            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected == 1;
    }
    public Boolean isSame()
    {
        Boolean boolSame = false;

        try
        {
            setValue();
            OdbcCommand cmd = new OdbcCommand();

            List<Model> models = _columns.Where(x => x.value != null && x.fillable && !x.value.Equals(x.defaultValue))
                                         .ToList();
            List<string> columns = models.Select(x => x.columnName + " = " + "?").ToList();

            string query = string.Format("SELECT count(*) FROM {0} WHERE {1}", _tableName, string.Join(" AND ", columns.ToArray()));
            foreach (Model model in models)
            {
                cmd.Parameters.AddWithValue(model.columnName, model.value);
            }
            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;


            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }
    public virtual int delete()
    {
        int intRecordAffected = 0;

        try
        {
            setValue();
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            string query = string.Format("DELETE FROM {0} WHERE {1}",
                                        _tableName,
                                        _ID + " = " + "?");

            cmd.Parameters.AddWithValue(_ID, _columns.Where(x => x.name == "ID").First().value);

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;

            intRecordAffected = cmd.ExecuteNonQuery();


            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public virtual int delete(string name, object value)
    {
        int intRecordAffected = 0;

        try
        {
            setValue();
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            string query = string.Format("DELETE FROM {0} WHERE {1}",
                                        _tableName,
                                        name + " = " + "?");

            cmd.Parameters.AddWithValue(name, value);

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;

            intRecordAffected = cmd.ExecuteNonQuery();


            if (intRecordAffected > 0)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
}
public class Model
{
    public string name { get; set; }
    public string columnName { get; set; }

    public Type type { get; set; }
    public bool fillable { get; set; }
    public object value { get; set; }
    public object defaultValue { get; set; }

    public bool isTimestamp { get; set; }

    public Model() { }
    public Model(Type type)
    {
        this.type = type;
        if(type == typeof(Int16) || type == typeof(Int32))
        {
            this.defaultValue = -1;
        }
        else if (type == typeof(DateTime))
        {
            this.defaultValue = Convert.ToDateTime("1900-01-01 00:00:00");
        }

        fillable = true;
    }

}