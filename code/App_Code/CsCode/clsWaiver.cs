﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsWaiver
/// </summary>
public class clsWaiver : dbconnBase
{
    #region "Properties"
    private int _waiverId;
    private decimal _waiverFrom;
    private decimal _waiverTo;
    private decimal _waiverValue;
    private int _waiverActive;
    private DateTime _waiverCreation;
    private int _waiverCreatedBy;
    private DateTime _waiverLastUpdate;
    private int _waiverUpdatedBy;
    #endregion


    #region "Property Methods"
    public int waiverId
    {
        get { return _waiverId; }
        set { _waiverId = value; }
    }

    public decimal waiverFrom
    {
        get { return _waiverFrom; }
        set { _waiverFrom = value; }
    }

    public decimal waiverTo
    {
        get { return _waiverTo; }
        set { _waiverTo = value; }
    }

    public decimal waiverValue
    {
        get { return _waiverValue; }
        set { _waiverValue = value; }
    }

    public int waiverActive
    {
        get { return _waiverActive; }
        set { _waiverActive = value; }
    }

    public DateTime waiverCreation
    {
        get { return _waiverCreation; }
        set { _waiverCreation = value; }
    }

    public int waiverCreatedBy
    {
        get { return _waiverCreatedBy; }
        set { _waiverCreatedBy = value; }
    }

    public DateTime waiverLastUpdate
    {
        get { return _waiverLastUpdate; }
        set { _waiverLastUpdate = value; }
    }

    public int waiverUpdatedBy
    {
        get { return _waiverUpdatedBy; }
        set { _waiverUpdatedBy = value; }
    }
    #endregion


    #region "Methods"
    public clsWaiver()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    public int addWaiver(decimal decFrom, decimal decTo, decimal decWaiver, int intActive, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_WAIVER (" +
                     "WAIVER_FROM," +
                     "WAIVER_TO," +
                     "WAIVER_VALUE," +
                     "WAIVER_ACTIVE," +
                     "WAIVER_CREATION," +
                     "WAIVER_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_WAIVER_FROM", OdbcType.Decimal).Value = decFrom;
            cmd.Parameters.Add("p_WAIVER_TO", OdbcType.Decimal).Value = decTo;
            cmd.Parameters.Add("p_WAIVER_VALUE", OdbcType.Decimal).Value = decWaiver;
            cmd.Parameters.Add("p_WAIVER_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_WAIVER_CREATEDBY", OdbcType.Int, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd.Connection = openConn();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = strSql2;
                waiverId = Convert.ToInt16(cmd.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractWaiverById(int intWaiverId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT A.WAIVER_ID, A.WAIVER_FROM, A.WAIVER_TO, A.WAIVER_VALUE, A.WAIVER_ACTIVE," +
                     " A.WAIVER_CREATION, A.WAIVER_CREATEDBY, A.WAIVER_LASTUPDATE, A.WAIVER_UPDATEDBY" +
                     " FROM TB_WAIVER A" +
                     " WHERE A.WAIVER_ID = ?";

            cmd.Parameters.Add("p_WAIVER_ID", OdbcType.Int, 9).Value = intWaiverId;

            if (intActive != 0)
            {
                strSql += " AND A.WAIVER_ACTIVE = ?";

                cmd.Parameters.Add("p_WAIVER_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                waiverId = Convert.ToInt16(ds.Tables[0].Rows[0]["WAIVER_ID"]);
                waiverFrom = Convert.ToDecimal(ds.Tables[0].Rows[0]["WAIVER_FROM"]);
                waiverTo = Convert.ToDecimal(ds.Tables[0].Rows[0]["WAIVER_TO"]);
                waiverValue = Convert.ToDecimal(ds.Tables[0].Rows[0]["WAIVER_VALUE"]);
                waiverActive = Convert.ToInt16(ds.Tables[0].Rows[0]["WAIVER_ACTIVE"]);
                waiverCreation = ds.Tables[0].Rows[0]["WAIVER_CREATION"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["WAIVER_CREATION"]) : DateTime.MaxValue;
                waiverCreatedBy = Convert.ToInt16(ds.Tables[0].Rows[0]["WAIVER_CREATEDBY"]);
                waiverLastUpdate = ds.Tables[0].Rows[0]["WAIVER_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["WAIVER_LASTUPDATE"]) : DateTime.MaxValue;
                waiverUpdatedBy = Convert.ToInt16(ds.Tables[0].Rows[0]["WAIVER_UPDATEDBY"]);
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public DataSet getWaiverList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.WAIVER_ID, A.WAIVER_FROM, A.WAIVER_TO, A.WAIVER_VALUE, A.WAIVER_ACTIVE," +
                     " A.WAIVER_CREATION, A.WAIVER_CREATEDBY, A.WAIVER_LASTUPDATE, A.WAIVER_UPDATEDBY" +
                     " FROM TB_WAIVER A";

            if (intActive != 0)
            {
                strSql += " WHERE A.WAIVER_ACTIVE = ?";

                cmd.Parameters.Add("p_WAIVER_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public decimal getWaiverByCost(decimal decShipping, int intActive)
    {
        decimal decWaiver = Convert.ToDecimal("0.00");

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT WAIVER_VALUE FROM TB_WAIVER" +
                     " WHERE ? >= WAIVER_FROM" +
                     " AND ? <= WAIVER_TO";

            cmd.Parameters.Add("p_WAIVER_FROM", OdbcType.Decimal).Value = decShipping;
            cmd.Parameters.Add("p_WAIVER_TO", OdbcType.Decimal).Value = decShipping;

            if (intActive != 0)
            {
                strSql += " AND WAIVER_ACTIVE = ?";

                cmd.Parameters.Add("p_WAIVER_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            decWaiver = Convert.ToDecimal(cmd.ExecuteScalar());
            
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return decWaiver;
    }

    public Boolean isWaiverOverlap(decimal decFrom, decimal decTo, int intWaiverId)
    {
        Boolean boolOverlap = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_WAIVER" +
                     " WHERE ((? <= WAIVER_FROM AND ? >= WAIVER_FROM)" +
                     " OR (? <= WAIVER_TO AND ? >= WAIVER_TO)" +
                     " OR (? >= WAIVER_FROM AND ? <= WAIVER_TO))";

            cmd.Parameters.Add("p_WAIVER_FROM1", OdbcType.Decimal).Value = decFrom;
            cmd.Parameters.Add("p_WAIVER_TO1", OdbcType.Decimal).Value = decTo;
            cmd.Parameters.Add("p_WAIVER_FROM2", OdbcType.Decimal).Value = decFrom;
            cmd.Parameters.Add("p_WAIVER_TO2", OdbcType.Decimal).Value = decTo;
            cmd.Parameters.Add("p_WAIVER_FROM3", OdbcType.Decimal).Value = decFrom;
            cmd.Parameters.Add("p_WAIVER_TO3", OdbcType.Decimal).Value = decTo;

            if (intWaiverId != 0)
            {
                strSql += " AND WAIVER_ID != ?";

                cmd.Parameters.Add("p_WAIVER_ID", OdbcType.Int, 9).Value = intWaiverId;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolOverlap = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolOverlap;
    }

    public Boolean isExactSameSet(int intWaiverId, decimal decFrom, decimal decTo, decimal decWaiver, int intActive)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_WAIVER" +
                     " WHERE WAIVER_ID = ?" +
                     " AND WAIVER_FROM = ?" +
                     " AND WAIVER_TO = ?" +
                     " AND WAIVER_VALUE = ?" +
                     " AND WAIVER_ACTIVE = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_WAIVER_ID", OdbcType.Int, 9).Value = intWaiverId;
            cmd.Parameters.Add("p_WAIVER_FROM", OdbcType.Decimal).Value = decFrom;
            cmd.Parameters.Add("p_WAIVER_TO", OdbcType.Decimal).Value = decTo;
            cmd.Parameters.Add("p_WAIVER_VALUE", OdbcType.Decimal).Value = decWaiver;
            cmd.Parameters.Add("p_WAIVER_ACTIVE", OdbcType.Int, 1).Value = intActive;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int updateWaiverById(int intWaiverId, decimal decFrom, decimal decTo, decimal decWaiver, int intActive, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_WAIVER SET" +
                     " WAIVER_FROM = ?," +
                     " WAIVER_TO = ?," +
                     " WAIVER_VALUE = ?," +
                     " WAIVER_ACTIVE = ?," +
                     " WAIVER_LASTUPDATE = SYSDATE()," +
                     " WAIVER_UPDATEDBY = ?" +
                     " WHERE WAIVER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_WAIVER_FROM", OdbcType.Decimal).Value = decFrom;
            cmd.Parameters.Add("p_WAIVER_TO", OdbcType.Decimal).Value = decTo;
            cmd.Parameters.Add("p_WAIVER_VALUE", OdbcType.Decimal).Value = decWaiver;
            cmd.Parameters.Add("p_WAIVER_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_WAIVER_UPDATEDBY", OdbcType.Int, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_WAIVER_ID", OdbcType.Int, 9).Value = intWaiverId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                waiverId = intWaiverId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteWaiverById(int intWaiverId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_WAIVER WHERE WAIVER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_WAIVER_ID", OdbcType.Int, 9).Value = intWaiverId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion
}
