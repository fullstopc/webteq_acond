﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Configuration;
using System.Runtime.Remoting.Messaging;
using System.Data;
using System.Data.Odbc;

public class clsEmail
{
    #region "Public Methods"
    public clsEmail()
    {
    }

    public static Boolean send(string strSenderName, string strSender, string strRecipient, string strRecipientCC, string strRecipientBCC, string strReplyTo, string strSubject, string strBody, int intMailFormat)
    {
        return sendByGroup(strSenderName, strSender, strRecipient, strRecipientCC, strRecipientBCC, strReplyTo, strSubject, strBody, "", intMailFormat, clsConfig.CONSTGROUPEMAILSETTING);
    }
    public static Boolean sendByGroup(string strSenderName, string strSender, string strRecipient, string strRecipientCC, string strRecipientBCC, string strReplyTo, string strSubject, string strBody, string strAttachment, int intMailFormat, string strGroupSetting)
    {
        MailMessage mail = new MailMessage();
        SmtpClient smtpClient = new SmtpClient();
        Boolean boolOk = true;
        clsConfig config = new clsConfig();

        try
        {
            string defaultSender = config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDER, strGroupSetting, 1) ? config.value : null;
            string defaultSenderName = config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, strGroupSetting, 1) ? config.value : null;
            string defaultRecipient = config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTRECIPIENT, clsConfig.CONSTGROUPEMAILSETTING, 1) ? config.value : null;
            string defaultRecipientBCC = config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTRECIPIENTBCC, strGroupSetting, 1) ? config.value : null;
            string defaultSenderPwd = config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERPWD, strGroupSetting, 1) ? config.value : null;
            string smtpServer = config.extractItemByNameGroup(clsConfig.CONSTNAMESMTPSERVER, strGroupSetting, 1) ? config.value : null;
            string useGmailSmtp = config.extractItemByNameGroup(clsConfig.CONSTNAMEUSERGMAILSMTP, strGroupSetting, 1) ? config.value : null;
            string port = config.extractItemByNameGroup(clsConfig.CONSTNAMEPORT, strGroupSetting, 1) ? config.value : null;
            string enableSSL = config.extractItemByNameGroup(clsConfig.CONSTNAMEENABLESSL, strGroupSetting, 1) ? config.value : null;

            mail.To.Clear();
            mail.CC.Clear();
            mail.Bcc.Clear();

            string strFrom = "";
            if (defaultSenderName == null || defaultSenderName == "")
            {
                if (strSender == null || strSender == "") { strFrom = defaultSender; }
                else { strFrom = strSender; }
            }
            else
            {
                if (strSender == null || strSender == "") { strFrom = defaultSenderName + " <" + defaultSender + ">"; }
                else { strFrom = defaultSenderName + " <" + strSender + ">"; }
            }
            mail.From = new MailAddress(strFrom);

            string strTo = "";
            if (strRecipient == null || strRecipient == "") { strTo = defaultRecipient; }
            else { strTo = strRecipient; }
            mail.To.Add(new MailAddress(strTo));

            if (strReplyTo != null && strReplyTo != "") { mail.ReplyTo = new MailAddress(strReplyTo); }
            if (strRecipientCC != null && strRecipientCC != "") { mail.CC.Add(new MailAddress(strRecipientCC)); }

            if (strRecipientBCC != null && strRecipientBCC != "") { mail.Bcc.Add(new MailAddress(strRecipientBCC)); }
            else if (!string.IsNullOrEmpty(defaultRecipientBCC)) { mail.Bcc.Add(new MailAddress(defaultRecipientBCC)); }

            if (!string.IsNullOrEmpty(strAttachment)) { mail.Attachments.Add(new Attachment(strAttachment)); }

            mail.Subject = strSubject;
            mail.Body = strBody;

            if (intMailFormat == 0) { mail.IsBodyHtml = false; }
            else { mail.IsBodyHtml = true; }

            NetworkCredential basicCredential = new NetworkCredential(defaultSender, defaultSenderPwd);
            try
            {
                smtpClient.Host = smtpServer;

                Boolean boolUseGmailSmtp = Convert.ToBoolean(useGmailSmtp);

                if (boolUseGmailSmtp)
                {
                    int intPort = Convert.ToInt16(port);
                    Boolean boolEnableSsl = Convert.ToBoolean(enableSSL);

                    smtpClient.Port = intPort;
                    smtpClient.EnableSsl = boolEnableSsl;
                }

                smtpClient.Credentials = basicCredential;
                smtpClient.Send(mail);
            }
            catch (Exception ex)
            {
                boolOk = false;
            }
        }
        catch (Exception ex)
        {
            boolOk = false;
        }

        mail.Dispose();
        smtpClient = null;

        return boolOk;
    }

    #region Version 2
    public void SendThat(MailMessage message, string connection)
    {
        AsyncMethodCaller caller = new AsyncMethodCaller(SendMailInSeperateThread);
        AsyncCallback callbackHandler = new AsyncCallback(AsyncCallback);
        caller.BeginInvoke(message, connection, callbackHandler, null);
    }

    private delegate void AsyncMethodCaller(MailMessage message, string connection);

    private void SendMailInSeperateThread(MailMessage message, string connection)
    {
        try
        {
            string defaultSender = extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDER, clsConfig.CONSTGROUPEMAILSETTING, 1, connection).value;
            string defaultSenderPwd = extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERPWD, clsConfig.CONSTGROUPEMAILSETTING, 1, connection).value;
            string smtpServer = extractItemByNameGroup(clsConfig.CONSTNAMESMTPSERVER, clsConfig.CONSTGROUPEMAILSETTING, 1, connection).value;
            string useGmailSmtp = extractItemByNameGroup(clsConfig.CONSTNAMEUSERGMAILSMTP, clsConfig.CONSTGROUPEMAILSETTING, 1, connection).value;
            string port = extractItemByNameGroup(clsConfig.CONSTNAMEPORT, clsConfig.CONSTGROUPEMAILSETTING, 1, connection).value;
            string enableSSL = extractItemByNameGroup(clsConfig.CONSTNAMEENABLESSL, clsConfig.CONSTGROUPEMAILSETTING, 1, connection).value;

            SmtpClient client = new SmtpClient();
            NetworkCredential basicCredential = new NetworkCredential(defaultSender, defaultSenderPwd);
            Boolean boolUseGmailSmtp = Convert.ToBoolean(useGmailSmtp);

            if (boolUseGmailSmtp)
            {
                int intPort = Convert.ToInt16(port);
                Boolean boolEnableSsl = Convert.ToBoolean(enableSSL);
                client.Port = intPort;
                client.EnableSsl = boolEnableSsl;
            }
            client.Host = smtpServer;
            client.Credentials = basicCredential;
            client.Timeout = 20000;
            client.Send(message);

            client = null;
            message.Dispose();
        }
        catch (Exception ex)
        {
            clsLog.error_email(ex.Message);
        }
    }

    private void AsyncCallback(IAsyncResult ar)
    {
        try
        {
            AsyncResult result = (AsyncResult)ar;
            AsyncMethodCaller caller = (AsyncMethodCaller)result.AsyncDelegate;
            caller.EndInvoke(ar);
        }
        catch (Exception ex)
        {
            clsLog.error_email(ex.Message + "::" + "Emailer - This hacky asynccallback thing is puking, serves you right.");
        }
    }

    public void send_v2(string strSenderName, string strSender, string strRecipient, string strRecipientCC, string strRecipientBCC, string strReplyTo, string strSubject, string strBody, int intMailFormat)
    {
        MailMessage mail = new MailMessage();
        clsConfig config = new clsConfig();

        string defaultSender = config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDER, clsConfig.CONSTGROUPEMAILSETTING, 1) ? config.value : null;
        string defaultSenderName = config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1) ? config.value : null;
        string defaultRecipient = config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTRECIPIENT, clsConfig.CONSTGROUPEMAILSETTING, 1) ? config.value : null;
        string defaultSenderPwd = config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERPWD, clsConfig.CONSTGROUPEMAILSETTING, 1) ? config.value : null;
        string smtpServer = config.extractItemByNameGroup(clsConfig.CONSTNAMESMTPSERVER, clsConfig.CONSTGROUPEMAILSETTING, 1) ? config.value : null;
        string useGmailSmtp = config.extractItemByNameGroup(clsConfig.CONSTNAMEUSERGMAILSMTP, clsConfig.CONSTGROUPEMAILSETTING, 1) ? config.value : null;
        string port = config.extractItemByNameGroup(clsConfig.CONSTNAMEPORT, clsConfig.CONSTGROUPEMAILSETTING, 1) ? config.value : null;
        string enableSSL = config.extractItemByNameGroup(clsConfig.CONSTNAMEENABLESSL, clsConfig.CONSTGROUPEMAILSETTING, 1) ? config.value : null;

        mail.To.Clear();
        mail.CC.Clear();
        mail.Bcc.Clear();

        string strFrom = "";
        if (defaultSenderName == null || defaultSenderName == "")
        {
            if (strSender == null || strSender == "") { strFrom = defaultSender; }
            else { strFrom = strSender; }
        }
        else
        {
            if (strSender == null || strSender == "") { strFrom = defaultSenderName + " <" + defaultSender + ">"; }
            else { strFrom = defaultSenderName + " <" + strSender + ">"; }
        }
        mail.From = new MailAddress(strFrom);

        string strTo = "";
        if (strRecipient == null || strRecipient == "") { strTo = defaultRecipient; }
        else { strTo = strRecipient; }
        mail.To.Add(new MailAddress(strTo));


        if (strReplyTo != null && strReplyTo != "") { mail.ReplyTo = new MailAddress(strReplyTo); }
        if (strRecipientCC != null && strRecipientCC != "") { mail.CC.Add(new MailAddress(strRecipientCC)); }
        if (strRecipientBCC != null && strRecipientBCC != "") { mail.Bcc.Add(new MailAddress(strRecipientBCC)); }
        mail.Subject = strSubject;
        mail.Body = strBody;

        if (intMailFormat == 0) { mail.IsBodyHtml = false; }
        else { mail.IsBodyHtml = true; }

        string connection = "";
        if (HttpContext.Current.Session[clsAdmin.CONSTADMINCS] != null && HttpContext.Current.Session[clsAdmin.CONSTADMINCS] != "")
        {
            connection = ConfigurationManager.ConnectionStrings["MySqlDBConn"].ConnectionString;
        }
        else if ((HttpContext.Current.Session[clsAdmin.CONSTPROJECTDRIVER] != null && HttpContext.Current.Session[clsAdmin.CONSTPROJECTDRIVER] != "") && (HttpContext.Current.Session[clsAdmin.CONSTPROJECTSERVER] != null && HttpContext.Current.Session[clsAdmin.CONSTPROJECTSERVER] != "") && (HttpContext.Current.Session[clsAdmin.CONSTPROJECTUSERID] != null && HttpContext.Current.Session[clsAdmin.CONSTPROJECTUSERID] != "") && (HttpContext.Current.Session[clsAdmin.CONSTPROJECTPASSWORD] != null && HttpContext.Current.Session[clsAdmin.CONSTPROJECTPASSWORD] != "") && (HttpContext.Current.Session[clsAdmin.CONSTPROJECTDATABASE] != null && HttpContext.Current.Session[clsAdmin.CONSTPROJECTDATABASE] != "") && (HttpContext.Current.Session[clsAdmin.CONSTPROJECTOPTION] != null && HttpContext.Current.Session[clsAdmin.CONSTPROJECTOPTION] != "") && (HttpContext.Current.Session[clsAdmin.CONSTPROJECTPORT] != null && HttpContext.Current.Session[clsAdmin.CONSTPROJECTPORT] != ""))
        {
            connection = "Driver={" + HttpContext.Current.Session[clsAdmin.CONSTPROJECTDRIVER] + "}; " + "Server=" + HttpContext.Current.Session[clsAdmin.CONSTPROJECTSERVER] + "; " + "uid=" + HttpContext.Current.Session[clsAdmin.CONSTPROJECTUSERID] + "; " + "pwd=" + HttpContext.Current.Session[clsAdmin.CONSTPROJECTPASSWORD] + "; " + "database=" + HttpContext.Current.Session[clsAdmin.CONSTPROJECTDATABASE] + "; " + "option=" + Convert.ToInt16(HttpContext.Current.Session[clsAdmin.CONSTPROJECTOPTION]) + "; " + "port=" + HttpContext.Current.Session[clsAdmin.CONSTPROJECTPORT] + ";";
        }
        else
        {
            connection = ConfigurationManager.ConnectionStrings["MySqlDBConn"].ConnectionString;
        }
        SendThat(mail, connection);

    }

    public Config extractItemByNameGroup(string strName, string strGroup, int intActive, string connectionString)
    {
        Boolean boolFound = false;
        OdbcConnection dbConnection = new OdbcConnection();
        Config config = new Config();
        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CONF_ID, A.CONF_NAME, A.CONF_VALUE, A.CONF_LONGVALUE, A.CONF_GROUP, A.CONF_ORDER, A.CONF_OPERATOR, A.CONF_UNIT, A.CONF_TAG" +
                     " FROM TB_CONFIG A" +
                     " WHERE A.CONF_NAME = ?";

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;

            if (!string.IsNullOrEmpty(strGroup))
            {
                strSql += " AND A.CONF_GROUP = ?";
                cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            }

            if (intActive != 0)
            {
                strSql += " AND A.CONF_ACTIVE = ?";
                cmd.Parameters.Add("p_CONF_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }


            dbConnection.ConnectionString = connectionString;
            dbConnection.Open();

            cmd.Connection = dbConnection;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;
                config.ID = int.Parse(dataSet.Tables[0].Rows[0]["CONF_ID"].ToString());
                config.name = dataSet.Tables[0].Rows[0]["CONF_NAME"].ToString();
                config.value = dataSet.Tables[0].Rows[0]["CONF_VALUE"].ToString();
                config.longValue = dataSet.Tables[0].Rows[0]["CONF_LONGVALUE"].ToString();
            }
        }
        catch (Exception ex)
        {
            clsLog.error_email("[ERROR][TB_CONFIG] : " + ex.Message.ToString());
            throw ex;
        }
        finally
        {
            if (dbConnection != null)
            {
                dbConnection.Close();
                dbConnection = null;
            }
        }

        return config;
    }

    #endregion
    #endregion
}

public class Config
{
    public int ID { get; set; }
    public string name { get; set; }
    public string value { get; set; }
    public string longValue { get; set; }
}
