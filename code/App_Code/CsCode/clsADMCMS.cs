﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsADMCMS
/// </summary>
public class clsADMCMS : dbconnBase
{
    #region "Properties"
    private int _cmsId;
    private string _cmsTitle;
    private string _cmsContent;
    private int _cmsType;
    private string _cmsTypeName;
    private DateTime _cmsCreation;
    private int _cmsCreatedBy;
    private DateTime _cmsLastUpdate;
    private int _cmsUpdatedBy;
    private int _cmsActive;
    #endregion


    #region "Property Methods"
    public int cmsId
    {
        get { return _cmsId; }
        set { _cmsId = value; }
    }

    public string cmsTitle
    {
        get { return _cmsTitle; }
        set { _cmsTitle = value; }
    }

    public string cmsContent
    {
        get { return _cmsContent; }
        set { _cmsContent = value; }
    }

    public int cmsType
    {
        get { return _cmsType; }
        set { _cmsType = value; }
    }

    public string cmsTypeName
    {
        get { return _cmsTypeName; }
        set { _cmsTypeName = value; }
    }

    public DateTime cmsCreation
    {
        get { return _cmsCreation; }
        set { _cmsCreation = value; }
    }

    public int cmsCreatedBy
    {
        get { return _cmsCreatedBy; }
        set { _cmsCreatedBy = value; }
    }

    public DateTime cmsLastUpdate
    {
        get { return _cmsLastUpdate; }
        set { _cmsLastUpdate = value; }
    }

    public int cmsUpdatedBy
    {
        get { return _cmsUpdatedBy; }
        set { _cmsUpdatedBy = value; }
    }

    public int cmsActive
    {
        get { return _cmsActive; }
        set { _cmsActive = value; }
    }
    #endregion


    #region "Methods"
    public clsADMCMS()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    public int addCMS(string strCMSTitle, string strCMSContent, int intCMSCreatedBy, int intCMSType, int intActive)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_CMS (" +
                     "CMS_TITLE," +
                     "CMS_TYPE," +
                     "CMS_CONTENT," +
                     "CMS_CREATION," +
                     "CMS_CREATEDBY," +
                     "CMS_ACTIVE" +
                     ") VALUES " +
                     "(?,?,?,SYSDATE(),?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CMS_TITLE", OdbcType.VarChar, 250).Value = strCMSTitle;
            cmd.Parameters.Add("p_CMS_TYPE", OdbcType.Int, 9).Value = intCMSType;
            cmd.Parameters.Add("p_CMS_CONTENT", OdbcType.Text).Value = strCMSContent;
            cmd.Parameters.Add("p_CMS_CREATEDBY", OdbcType.Int, 9).Value = intCMSCreatedBy;
            cmd.Parameters.Add("p_CMS_ACTIVE", OdbcType.Int, 1).Value = intActive;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";

                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                cmsId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractCMSById(int intCMSId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CMS_ID, A.CMS_TITLE, A.CMS_CONTENT, A.CMS_TYPE, A.V_CMSTYPENAME, A.CMS_CREATION, A.CMS_CREATEDBY, A.CMS_LASTUPDATE, A.CMS_UPDATEDBY, A.CMS_ACTIVE" +
                     " FROM VW_CMS A" +
                     " WHERE A.CMS_ID = ?";

            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;

            if (intActive != 0)
            {
                strSql += " AND A.CMS_ACTIVE = ?";

                cmd.Parameters.Add("p_CMS_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                cmsId = int.Parse(ds.Tables[0].Rows[0]["CMS_ID"].ToString());
                cmsTitle = ds.Tables[0].Rows[0]["CMS_TITLE"].ToString();
                cmsContent = ds.Tables[0].Rows[0]["CMS_CONTENT"].ToString();
                cmsType = Convert.ToInt16(ds.Tables[0].Rows[0]["CMS_TYPE"]);
                cmsTypeName = ds.Tables[0].Rows[0]["V_CMSTYPENAME"].ToString();
                cmsCreation = ds.Tables[0].Rows[0]["CMS_CREATION"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["CMS_CREATION"]) : DateTime.MaxValue;
                cmsCreatedBy = int.Parse(ds.Tables[0].Rows[0]["CMS_CREATEDBY"].ToString());
                cmsLastUpdate = ds.Tables[0].Rows[0]["CMS_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["CMS_LASTUPDATE"]) : DateTime.MaxValue;
                cmsUpdatedBy = int.Parse(ds.Tables[0].Rows[0]["CMS_UPDATEDBY"].ToString());
                cmsActive = Convert.ToInt16(ds.Tables[0].Rows[0]["CMS_ACTIVE"]);
            }

            ds.Dispose();
            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public DataSet getCMSList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CMS_ID, A.CMS_TITLE, A.CMS_CONTENT, A.CMS_TYPE, A.V_CMSTYPENAME, A.CMS_CREATION, A.CMS_CREATEDBY, A.CMS_LASTUPDATE, A.CMS_UPDATEDBY, A.CMS_ACTIVE" +
                     " FROM VW_CMS A";

            if (intActive != 0)
            {
                strSql += " WHERE A.CMS_ACTIVE = ?";

                cmd.Parameters.Add("p_CMS_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean isExactSameSetCMS(int intCMSId, string strCMSTitle, string strCMSContent, int intCMSType, int intActive)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_CMS" +
                     " WHERE CMS_ID = ?" +
                     " AND BINARY CMS_TITLE = ?" +
                     " AND BINARY CMS_CONTENT = ?" +
                     " AND CMS_TYPE = ?" +
                     " AND CMS_ACTIVE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;
            cmd.Parameters.Add("p_CMS_TITLE", OdbcType.VarChar, 250).Value = strCMSTitle;
            cmd.Parameters.Add("p_CMS_CONTENT", OdbcType.Text).Value = strCMSContent;
            cmd.Parameters.Add("p_CMS_TYPE", OdbcType.Int, 9).Value = intCMSType;
            cmd.Parameters.Add("p_CMS_ACTIVE", OdbcType.Int, 1).Value = intActive;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int updateCMSById(int intCMSId, string strCMSTitle, string strCMSContent, int intCMSUpdatedBy, int intCMSType, int intActive)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_CMS SET" +
                     " CMS_TITLE = ?," +
                     " CMS_CONTENT = ?," +
                     " CMS_LASTUPDATE = SYSDATE()," +
                     " CMS_UPDATEDBY = ?," +
                     " CMS_TYPE = ?," +
                     " CMS_ACTIVE = ?" +
                     " WHERE CMS_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CMS_TITLE", OdbcType.VarChar, 250).Value = strCMSTitle;
            cmd.Parameters.Add("p_CMS_CONTENT", OdbcType.Text).Value = strCMSContent;
            cmd.Parameters.Add("p_CMS_UPDATEDBY", OdbcType.Int, 9).Value = intCMSUpdatedBy;
            cmd.Parameters.Add("p_CMS_TYPE", OdbcType.Int, 9).Value = intCMSType;
            cmd.Parameters.Add("p_CMS_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                cmsId = intCMSId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteCMSById(int intCMSId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_CMS WHERE CMS_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                cmsId = intCMSId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion
}
