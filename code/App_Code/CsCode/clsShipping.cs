﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsShipping
/// </summary>
public class clsShipping : dbconnBase
{
    #region "Properties"
    private int _shipId;
    private int _shipComp;
    private int _shipMethod;
    private string _shipCountry;
    private string _shipCountryName;
    private string _shipState;
    private string _shipCity;
    private decimal _shipKgFirst;
    private decimal _shipFeeFirst;
    private decimal _shipKg;
    private decimal _shipFee;
    private int _shipActive;
    private int _shipChkCountry;
    private int _shipChkState;
    private int _shipChkCity;
    private DateTime _shipCreation;
    private int _shipCreatedBy;
    private DateTime _shipLastUpdate;
    private int _shipUpdatedBy;
    private int _shipDefault;
    private decimal _shipFeeFree;
    private int _shipCountryTxt;
    private int _shipStateTxt;
    private int _shipCityTxt;
    private string _shipLeadTime;
    private int _shipAllowLiquid;
    #endregion

    #region "Property Methods"
    public int shipId
    {
        get { return _shipId; }
        set { _shipId = value; }
    }

    public int shipComp
    {
        get { return _shipComp; }
        set { _shipComp = value; }
    }

    public int shipMethod
    {
        get { return _shipMethod; }
        set { _shipMethod = value; }
    }

    public string shipCountry
    {
        get { return _shipCountry; }
        set { _shipCountry = value; }
    }

    public string shipCountryName
    {
        get { return _shipCountryName; }
        set { _shipCountryName = value; }
    }

    public string shipState
    {
        get { return _shipState; }
        set { _shipState = value; }
    }

    public decimal shipKgFirst
    {
        get { return _shipKgFirst; }
        set { _shipKgFirst = value; }
    }

    public decimal shipFeeFirst
    {
        get { return _shipFeeFirst; }
        set { _shipFeeFirst = value; }
    }

    public decimal shipKg
    {
        get { return _shipKg; }
        set { _shipKg = value; }
    }

    public decimal shipFee
    {
        get { return _shipFee; }
        set { _shipFee = value; }
    }

    public int shipActive
    {
        get { return _shipActive; }
        set { _shipActive = value; }
    }

    public int shipChkCountry
    {
        get { return _shipChkCountry; }
        set { _shipChkCountry = value; }
    }

    public int shipChkState
    {
        get { return _shipChkState; }
        set { _shipChkState = value; }
    }

    public int shipChkCity
    {
        get { return _shipChkCity; }
        set { _shipChkCity = value; }
    }

    public DateTime shipCreation
    {
        get { return _shipCreation; }
        set { _shipCreation = value; }
    }

    public int shipCreatedBy
    {
        get { return _shipCreatedBy; }
        set { _shipCreatedBy = value; }
    }

    public DateTime shipLastUpdate
    {
        get { return _shipLastUpdate; }
        set { _shipLastUpdate = value; }
    }

    public int shipUpdatedBy
    {
        get { return _shipUpdatedBy; }
        set { _shipUpdatedBy = value; }
    }

    public int shipDefault
    {
        get { return _shipDefault; }
        set { _shipDefault = value; }
    }

    public decimal shipFeeFree
    {
        get { return _shipFeeFree; }
        set { _shipFeeFree = value; }
    }

    public int shipCountryTxt
    {
        get { return _shipCountryTxt; }
        set { _shipCountryTxt = value; }
    }

    public int shipStateTxt
    {
        get { return _shipStateTxt; }
        set { _shipStateTxt = value; }
    }

    public int shipCityTxt
    {
        get { return _shipCityTxt; }
        set { _shipCityTxt = value; }
    }

    public string shipLeadTime
    {
        get { return _shipLeadTime; }
        set { _shipLeadTime = value; }
    }

    public int shipAllowLiquid
    {
        get { return _shipAllowLiquid; }
        set { _shipAllowLiquid = value; }
    }

    public string shipCity
    {
        get { return _shipCity; }
        set { _shipCity = value; }
    }
    #endregion


    #region "Methods"
    public clsShipping()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    public int addShipping(string strCountry, string strState, int intActive, decimal decShipKgFirst, decimal decShipFeeFirst, decimal decShipKg, decimal decShipFee, decimal decShipFeeFree, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_SHIPPING2 (" +
                     "SHIP_KG_FIRST," +
                     "SHIP_FEE_FIRST," +
                     "SHIP_KG," +
                     "SHIP_FEE," +
                     "SHIP_FEEFREE," +
                     "SHIP_COUNTRY," +
                     "SHIP_STATE," +
                     "SHIP_ACTIVE," +
                     "SHIP_CREATION," +
                     "SHIP_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SHIP_KG_FIRST", OdbcType.Decimal).Value = decShipKgFirst;
            cmd.Parameters.Add("p_SHIP_FEE_FIRST", OdbcType.Decimal).Value = decShipFeeFirst;
            cmd.Parameters.Add("p_SHIP_KG", OdbcType.Decimal).Value = decShipKg;
            cmd.Parameters.Add("p_SHIP_FEE", OdbcType.Decimal).Value = decShipFee;
            cmd.Parameters.Add("p_SHIP_FEEFREE", OdbcType.Decimal).Value = decShipFeeFree;
            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIP_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_SHIP_CREATEDBY", OdbcType.Int, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd.Connection = openConn();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = strSql2;
                shipId = Convert.ToInt16(cmd.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addShipping2(int intShipComp, int intShipMethod, string strCountry, string strState, decimal decShipWeightFirst, decimal decShipFeeFirst, decimal decShipWeightSub, decimal decShipFeeSub, int intActive, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_SHIPPING2 (" +
                     "SHIP_COMPANY," +
                     "SHIP_METHOD," +
                     "SHIP_COUNTRY," +
                     "SHIP_STATE," +
                     "SHIP_KG_FIRST," +
                     "SHIP_FEE_FIRST," +
                     "SHIP_KG," +
                     "SHIP_FEE," +
                     "SHIP_ACTIVE," +
                     "SHIP_CREATION," +
                     "SHIP_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SHIP_COMPANY", OdbcType.Int, 1).Value = intShipComp;
            cmd.Parameters.Add("p_SHIP_METHOD", OdbcType.Int, 1).Value = intShipMethod;
            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIP_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_SHIP_KG_FIRST", OdbcType.Decimal).Value = decShipWeightFirst;
            cmd.Parameters.Add("p_SHIP_FEE_FIRST", OdbcType.Decimal).Value = decShipFeeFirst;
            cmd.Parameters.Add("p_SHIP_KG", OdbcType.Decimal).Value = decShipWeightSub;
            cmd.Parameters.Add("p_SHIP_FEE", OdbcType.Decimal).Value = decShipFeeSub;
            cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_SHIP_CREATEDBY", OdbcType.BigInt, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd.Connection = openConn();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = strSql2;
                shipId = Convert.ToInt16(cmd.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addShipping3(string strCountry, string strState, string strCity, int intActive, decimal decShipKgFirst, decimal decShipFeeFirst, decimal decShipKg, decimal decShipFee, decimal decShipFeeFree,int intCountry, int intState, int intCity, string strLeadTime, int intAllowLiquid, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_SHIPPING2 (" +
                     "SHIP_KG_FIRST," +
                     "SHIP_FEE_FIRST," +
                     "SHIP_KG," +
                     "SHIP_FEE," +
                     "SHIP_FEEFREE," +
                     "SHIP_COUNTRY," +
                     "SHIP_STATE," +
                     "SHIP_CITY," +
                     "SHIP_COUNTRYTEXT," +
                     "SHIP_STATETEXT," +
                     "SHIP_CITYTEXT," +
                     "SHIP_LEADTIME," +
                     "SHIP_LIQUID," +
                     "SHIP_ACTIVE," +
                     "SHIP_CREATION," +
                     "SHIP_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SHIP_KG_FIRST", OdbcType.Decimal).Value = decShipKgFirst;
            cmd.Parameters.Add("p_SHIP_FEE_FIRST", OdbcType.Decimal).Value = decShipFeeFirst;
            cmd.Parameters.Add("p_SHIP_KG", OdbcType.Decimal).Value = decShipKg;
            cmd.Parameters.Add("p_SHIP_FEE", OdbcType.Decimal).Value = decShipFee;
            cmd.Parameters.Add("p_SHIP_FEEFREE", OdbcType.Decimal).Value = decShipFeeFree;
            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIP_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_SHIP_CITY", OdbcType.VarChar, 250).Value = strCity;
            cmd.Parameters.Add("p_SHIP_COUNTRYTEXT", OdbcType.Int, 1).Value = intCountry;
            cmd.Parameters.Add("p_SHIP_STATETEXT", OdbcType.Int, 1).Value = intState;
            cmd.Parameters.Add("p_SHIP_CITYTEXT", OdbcType.Int, 1).Value = intCity;
            cmd.Parameters.Add("p_SHIP_LEADTIME", OdbcType.VarChar, 10).Value = strLeadTime;
            cmd.Parameters.Add("p_SHIP_LIQUID", OdbcType.Int, 1).Value = intAllowLiquid;
            cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_SHIP_CREATEDBY", OdbcType.Int, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd.Connection = openConn();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = strSql2;
                shipId = Convert.ToInt16(cmd.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isShippingExist(string strCountry, string strState)
    {
        Boolean boolFound = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_SHIPPING2" +
                     " WHERE SHIP_COUNTRY = ?" +
                     " AND SHIP_STATE = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIP_STATE", OdbcType.VarChar, 250).Value = strState;            

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolFound = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean isShippingExist2(int intShipComp, int intShipMethod, string strCountry, string strState)
    {
        Boolean boolFound = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_SHIPPING2" +
                     " WHERE SHIP_COMPANY = ?" +
                     " AND SHIP_METHOD = ?" +
                     " AND SHIP_COUNTRY = ?" +
                     " AND SHIP_STATE = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SHIP_COMPANY", OdbcType.Int, 1).Value = intShipComp;
            cmd.Parameters.Add("p_SHIP_METHOD", OdbcType.Int, 1).Value = intShipMethod;
            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIP_STATE", OdbcType.VarChar, 250).Value = strState;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolFound = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean isShippingExist3(string strCountry, string strState, string strCity)
    {
        Boolean boolFound = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_SHIPPING2" +
                     " WHERE SHIP_COUNTRY = ?" +
                     " AND SHIP_STATE = ?" +
                     " AND SHIP_CITY = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIP_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_SHIP_CITY", OdbcType.VarChar, 250).Value = strCity;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolFound = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    
    public Boolean extractShippingById(int intShipId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT A.SHIP_ID, A.SHIP_FEE, A.SHIP_COMPANY, A.SHIP_METHOD, A.SHIP_COUNTRY, A.SHIP_STATE, A.SHIP_ACTIVE, A.SHIP_CREATION," +
                     " A.SHIP_CREATEDBY, A.SHIP_LASTUPDATE, A.SHIP_UPDATEDBY, A.SHIP_DEFAULT, A.SHIP_FEE_FIRST, A.SHIP_KG_FIRST, A.SHIP_KG," +
                     " A.SHIP_FEEFREE, (SELECT COUNTRY_NAME FROM TB_COUNTRY WHERE COUNTRY_CODE = A.SHIP_COUNTRY) AS V_SHIPCOUNTRYNAME," +
                     " A.SHIP_COUNTRYTEXT, A.SHIP_STATETEXT, A.SHIP_CITYTEXT, A.SHIP_LEADTIME, A.SHIP_LIQUID, A.SHIP_CITY" +
                     " FROM TB_SHIPPING2 A" +
                     " WHERE A.SHIP_ID = ?";

            //strSql = "SELECT A.SHIP_ID, A.SHIP_KG_FIRST, A.SHIP_FEE_FIRST, A.SHIP_KG, A.SHIP_FEE, A.SHIP_COUNTRY, A.V_SHIPCOUNTRYNAME, A.SHIP_STATE, A.SHIP_ACTIVE," +
            //         " A.SHIP_CREATION, A.SHIP_CREATEDBY, A.SHIP_LASTUPDATE, A.SHIP_UPDATEDBY, A.SHIP_DEFAULT, A.SHIP_FEEFREE" +
            //         " FROM VW_SHIPPING2 A" +
            //         " WHERE A.SHIP_ID = ?";

            cmd.Parameters.Add("p_SHIP_ID", OdbcType.Int, 9).Value = intShipId;

            if (intActive != 0)
            {
                strSql += " AND A.SHIP_ACTIVE = ?";

                cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                shipId = int.Parse(ds.Tables[0].Rows[0]["SHIP_ID"].ToString());
                shipKgFirst = Convert.ToDecimal(ds.Tables[0].Rows[0]["SHIP_KG_FIRST"]);
                shipFeeFirst = Convert.ToDecimal(ds.Tables[0].Rows[0]["SHIP_FEE_FIRST"]);
                shipKg = Convert.ToDecimal(ds.Tables[0].Rows[0]["SHIP_KG"]);
                shipFee = Convert.ToDecimal(ds.Tables[0].Rows[0]["SHIP_FEE"]);
                shipComp = ds.Tables[0].Rows[0]["SHIP_COMPANY"] != DBNull.Value ? int.Parse(ds.Tables[0].Rows[0]["SHIP_COMPANY"].ToString()) : 0;
                shipMethod = ds.Tables[0].Rows[0]["SHIP_METHOD"] != DBNull.Value ? int.Parse(ds.Tables[0].Rows[0]["SHIP_METHOD"].ToString()) : 0;
                shipCountry = ds.Tables[0].Rows[0]["SHIP_COUNTRY"].ToString();
                shipCountryName = ds.Tables[0].Rows[0]["V_SHIPCOUNTRYNAME"].ToString();
                shipState = ds.Tables[0].Rows[0]["SHIP_STATE"].ToString();
                shipActive = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIP_ACTIVE"]);
                shipCreation = ds.Tables[0].Rows[0]["SHIP_CREATION"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["SHIP_CREATION"]) : DateTime.MaxValue;
                shipCreatedBy = int.Parse(ds.Tables[0].Rows[0]["SHIP_CREATEDBY"].ToString());
                shipLastUpdate = ds.Tables[0].Rows[0]["SHIP_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["SHIP_LASTUPDATE"]) : DateTime.MaxValue;
                shipUpdatedBy = int.Parse(ds.Tables[0].Rows[0]["SHIP_UPDATEDBY"].ToString());
                shipDefault = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIP_DEFAULT"]);
                shipFeeFree = Convert.ToDecimal(ds.Tables[0].Rows[0]["SHIP_FEEFREE"]);
                shipCountryTxt = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIP_COUNTRYTEXT"]);
                shipStateTxt = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIP_STATETEXT"]);
                shipCityTxt = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIP_CITYTEXT"]);
                shipLeadTime = ds.Tables[0].Rows[0]["SHIP_LEADTIME"].ToString();
                shipAllowLiquid = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIP_LIQUID"]);
                shipCity = ds.Tables[0].Rows[0]["SHIP_CITY"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public DataSet getShippingList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SHIP_ID, A.SHIP_KG_FIRST, A.SHIP_FEE_FIRST, A.SHIP_KG, A.SHIP_FEE, A.SHIP_COUNTRY, A.V_SHIPCOUNTRYNAME, A.SHIP_STATE, A.SHIP_ACTIVE," +
                     " A.SHIP_CREATION, A.SHIP_CREATEDBY, A.SHIP_LASTUPDATE, A.SHIP_UPDATEDBY, A.SHIP_DEFAULT, A.SHIP_FEEFREE" +
                     " FROM VW_SHIPPING2 A";

            if (intActive != 0)
            {
                strSql += " WHERE A.SHIP_ACTIVE = ?";

                cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getShippingList2(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SHIP_ID, A.SHIP_FEE, A.SHIP_COMPANY, (SELECT CONF_NAME FROM TB_CONFIG WHERE CONF_ID = A.SHIP_COMPANY AND CONF_GROUP = 'SHIPPING COMPANY SETTINGS') AS V_SHIPCOMPNAME," +
                     " A.SHIP_METHOD, (SELECT CONF_NAME FROM TB_CONFIG WHERE CONF_ID = A.SHIP_METHOD AND CONF_GROUP = 'SHIPPING METHOD SETTINGS') AS V_SHIPMETHODNAME," +
                     " A.SHIP_COUNTRY, (SELECT COUNTRY_NAME FROM TB_COUNTRY WHERE COUNTRY_CODE = A.SHIP_COUNTRY) AS V_SHIPCOUNTRYNAME," +
                     " A.SHIP_STATE, A.SHIP_CITY, A.SHIP_ACTIVE, A.SHIP_CREATION, A.SHIP_CREATEDBY, A.SHIP_LASTUPDATE, A.SHIP_UPDATEDBY, A.SHIP_DEFAULT," +
                     " A.SHIP_FEE_FIRST, A.SHIP_KG_FIRST, A.SHIP_KG, A.SHIP_FEEFREE" +
                     " FROM TB_SHIPPING2 A";

            if (intActive != 0)
            {
                strSql += " WHERE A.SHIP_ACTIVE = ?";
                cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getShippingList3(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SHIP_ID, A.SHIP_KG_FIRST, A.SHIP_FEE_FIRST, A.SHIP_KG, A.SHIP_FEE, A.SHIP_COUNTRY, A.V_SHIPCOUNTRYNAME, A.SHIP_STATE, A.SHIP_ACTIVE," +
                     " A.SHIP_CREATION, A.SHIP_CREATEDBY, A.SHIP_LASTUPDATE, A.SHIP_UPDATEDBY, A.SHIP_DEFAULT, A.SHIP_FEEFREE, A.SHIP_CITY" +
                     " FROM VW_SHIPPING2 A";

            if (intActive != 0)
            {
                strSql += " WHERE A.SHIP_ACTIVE = ?";

                cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getShippingCountryList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SHIP_ID, A.SHIP_KG_FIRST, A.SHIP_FEE_FIRST, A.SHIP_KG, A.SHIP_FEE, A.SHIP_COUNTRY, A.V_SHIPCOUNTRYNAME, A.SHIP_STATE, A.SHIP_ACTIVE," +
                     " A.SHIP_CREATION, A.SHIP_CREATEDBY, A.SHIP_LASTUPDATE, A.SHIP_UPDATEDBY, A.SHIP_DEFAULT, A.SHIP_FEEFREE" +
                     " FROM VW_SHIPPING2 A";

            if (intActive != 0)
            {
                strSql += " WHERE A.SHIP_ACTIVE = ?";

                cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " GROUP BY A.V_SHIPCOUNTRYNAME";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getShippingCountryList2(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SHIP_ID, A.SHIP_KG_FIRST, A.SHIP_FEE_FIRST, A.SHIP_KG, A.SHIP_FEE, A.SHIP_COUNTRY, A.V_SHIPCOUNTRYNAME, A.SHIP_STATE, A.SHIP_CITY, A.SHIP_ACTIVE," +
                     " A.SHIP_CREATION, A.SHIP_CREATEDBY, A.SHIP_LASTUPDATE, A.SHIP_UPDATEDBY, A.SHIP_DEFAULT, A.SHIP_FEEFREE" +
                     " FROM VW_SHIPPING2 A";

            if (intActive != 0)
            {
                strSql += " WHERE A.SHIP_ACTIVE = ?";

                cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean isExactSameSet(int intShipId, string strCountry, string strState, decimal decShipKgFirst, decimal decShipFeeFirst, decimal decShipKg, decimal decShipFee, decimal decShipFeeFree, int intActive)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_SHIPPING2" +
                     " WHERE SHIP_ID = ?" +
                     " AND BINARY SHIP_COUNTRY = ?" +
                     " AND BINARY SHIP_STATE = ?" +
                     " AND SHIP_KG_FIRST = ?" +
                     " AND SHIP_FEE_FIRST = ?" +
                     " AND SHIP_KG = ?" +
                     " AND SHIP_FEE = ?" +
                     " AND SHIP_FEEFREE = ?" +
                     " AND SHIP_ACTIVE = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SHIP_ID", OdbcType.Int, 9).Value = intShipId;
            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIP_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_SHIP_KG_FIRST", OdbcType.Decimal).Value = decShipKgFirst;
            cmd.Parameters.Add("p_SHIP_FEE_FIRST", OdbcType.Decimal).Value = decShipFeeFirst;
            cmd.Parameters.Add("p_SHIP_KG", OdbcType.Decimal).Value = decShipKg;
            cmd.Parameters.Add("p_SHIP_FEE", OdbcType.Decimal).Value = decShipFee;
            cmd.Parameters.Add("p_SHIP_FEEFREE", OdbcType.Decimal).Value = decShipFeeFree;
            cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public Boolean isExactSameSet2(int intShipId, int intShipComp, int intShipMethod, string strCountry, string strState, decimal decShipWeightFirst, decimal decShipFeeFirst, decimal decShipWeightSub, decimal decShipFeeSub, int intActive)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_SHIPPING2" +
                     " WHERE SHIP_ID = ?" +
                     " AND SHIP_COMPANY = ?" +
                     " AND SHIP_METHOD = ?" +
                     " AND BINARY SHIP_COUNTRY = ?" +
                     " AND BINARY SHIP_METHOD = ?" +
                     " AND SHIP_KG_FIRST = ?" +
                     " AND SHIP_FEE_FIRST = ?" +
                     " AND SHIP_KG = ?" +
                     " AND SHIP_FEE = ?" +
                     " AND SHIP_ACTIVE = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SHIP_ID", OdbcType.BigInt, 9).Value = intShipId;
            cmd.Parameters.Add("p_SHIP_COMPANY", OdbcType.Int, 1).Value = intShipComp;
            cmd.Parameters.Add("p_SHIP_METHOD", OdbcType.Int, 1).Value = intShipMethod;
            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIP_METHOD", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_SHIP_KG_FIRST", OdbcType.Decimal).Value = decShipWeightFirst;
            cmd.Parameters.Add("p_SHIP_FEE_FIRST", OdbcType.Decimal).Value = decShipFeeFirst;
            cmd.Parameters.Add("p_SHIP_KG", OdbcType.Decimal).Value = decShipWeightSub;
            cmd.Parameters.Add("p_SHIP_FEE", OdbcType.Decimal).Value = decShipFeeSub;
            cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public Boolean isExactSameSet3(int intShipId, string strCountry, string strState, string strCity, decimal decShipKgFirst, decimal decShipFeeFirst, decimal decShipKg, decimal decShipFee, decimal decShipFeeFree, int intCountry, int intState, int intCity, string strLeadTime, int intAllowLiquid, int intActive)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_SHIPPING2" +
                     " WHERE SHIP_ID = ?" +
                     " AND BINARY SHIP_COUNTRY = ?" +
                     " AND BINARY SHIP_STATE = ?" +
                     " AND BINARY SHIP_CITY = ?" +
                     " AND SHIP_KG_FIRST = ?" +
                     " AND SHIP_FEE_FIRST = ?" +
                     " AND SHIP_KG = ?" +
                     " AND SHIP_FEE = ?" +
                     " AND SHIP_FEEFREE = ?" +
                     " AND SHIP_COUNTRYTEXT = ?" +
                     " AND SHIP_STATETEXT = ?" +
                     " AND SHIP_CITYTEXT = ?" +
                     " AND SHIP_LEADTIME = ?" +
                     " AND SHIP_LIQUID = ?" +
                     " AND SHIP_ACTIVE = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SHIP_ID", OdbcType.Int, 9).Value = intShipId;
            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIP_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_SHIP_CITY", OdbcType.VarChar, 250).Value = strCity;
            cmd.Parameters.Add("p_SHIP_KG_FIRST", OdbcType.Decimal).Value = decShipKgFirst;
            cmd.Parameters.Add("p_SHIP_FEE_FIRST", OdbcType.Decimal).Value = decShipFeeFirst;
            cmd.Parameters.Add("p_SHIP_KG", OdbcType.Decimal).Value = decShipKg;
            cmd.Parameters.Add("p_SHIP_FEE", OdbcType.Decimal).Value = decShipFee;
            cmd.Parameters.Add("p_SHIP_FEEFREE", OdbcType.Decimal).Value = decShipFeeFree;
            cmd.Parameters.Add("p_SHIP_COUNTRYTEXT", OdbcType.Int, 1).Value = intCountry;
            cmd.Parameters.Add("p_SHIP_STATETEXT", OdbcType.Int, 1).Value = intState;
            cmd.Parameters.Add("p_SHIP_CITYTEXT", OdbcType.Int, 1).Value = intCity;
            cmd.Parameters.Add("p_SHIP_LEADTIME", OdbcType.VarChar, 10).Value = strLeadTime;
            cmd.Parameters.Add("p_SHIP_LIQUID", OdbcType.Int, 1).Value = intAllowLiquid;
            cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int updateShippingById(int intShipId, string strCountry, string strState, decimal decShipKgFirst, decimal decShipFeeFirst, decimal decShipKg, decimal decShipFee, decimal decShipFeeFree, int intActive, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_SHIPPING2 SET" +
                     " SHIP_KG_FIRST = ?," +
                     " SHIP_FEE_FIRST = ?," +
                     " SHIP_KG = ?," +
                     " SHIP_FEE = ?," +
                     " SHIP_FEEFREE = ?," +
                     " SHIP_COUNTRY = ?," +
                     " SHIP_STATE = ?," +
                     " SHIP_ACTIVE = ?," +
                     " SHIP_LASTUPDATE = SYSDATE()," +
                     " SHIP_UPDATEDBY = ?" +
                     " WHERE SHIP_ID = ?";

            cmd.Parameters.Add("p_SHIP_KG_FIRST", OdbcType.Decimal).Value = decShipKgFirst;
            cmd.Parameters.Add("p_SHIP_FEE_FIRST", OdbcType.Decimal).Value = decShipFeeFirst;
            cmd.Parameters.Add("p_SHIP_KG", OdbcType.Decimal).Value = decShipKg;
            cmd.Parameters.Add("p_SHIP_FEE", OdbcType.Decimal).Value = decShipFee;
            cmd.Parameters.Add("p_SHIP_FEEFREE", OdbcType.Decimal).Value = decShipFeeFree;
            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIP_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_SHIP_UPDATEDBY", OdbcType.Int, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_SHIP_ID", OdbcType.Int, 9).Value = intShipId;

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                shipId = intShipId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateShippingById2(int intShipId, int intShipComp, int intShipMethod, string strCountry, string strState, decimal decShipWeightFirst, decimal decShipFeeFirst, decimal decShipWeightSub, decimal decShipFeeSub, int intActive, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_SHIPPING2 SET" +
                     " SHIP_COMPANY = ?," +
                     " SHIP_METHOD = ?," +
                     " SHIP_COUNTRY = ?," +
                     " SHIP_STATE = ?," +
                     " SHIP_KG_FIRST = ?," +
                     " SHIP_FEE_FIRST = ?," +
                     " SHIP_KG = ?," +
                     " SHIP_FEE = ?," +
                     " SHIP_ACTIVE = ?," +
                     " SHIP_LASTUPDATE = SYSDATE()," +
                     " SHIP_UPDATEDBY = ?" +
                     " WHERE SHIP_ID = ?";

            cmd.Parameters.Add("p_SHIP_COMPANY", OdbcType.Int, 1).Value = intShipComp;
            cmd.Parameters.Add("p_SHIP_METHOD", OdbcType.Int, 1).Value = intShipMethod;
            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIP_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_SHIP_KG_FIRST", OdbcType.Decimal).Value = decShipWeightFirst;
            cmd.Parameters.Add("p_SHIP_FEE_FIRST", OdbcType.Decimal).Value = decShipFeeFirst;
            cmd.Parameters.Add("p_SHIP_KG", OdbcType.Decimal).Value = decShipWeightSub;
            cmd.Parameters.Add("p_SHIP_FEE", OdbcType.Decimal).Value = decShipFeeSub;
            cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_SHIP_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_SHIP_ID", OdbcType.BigInt, 9).Value = intShipId;

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                shipId = intShipId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateShippingById3(int intShipId, string strCountry, string strState, string strCity, decimal decShipKgFirst, decimal decShipFeeFirst, decimal decShipKg, decimal decShipFee, decimal decShipFeeFree, int intCountry, int intState, int intCity, string strLeadTime, int intAllowLiquid, int intActive, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_SHIPPING2 SET" +
                     " SHIP_KG_FIRST = ?," +
                     " SHIP_FEE_FIRST = ?," +
                     " SHIP_KG = ?," +
                     " SHIP_FEE = ?," +
                     " SHIP_FEEFREE = ?," +
                     " SHIP_COUNTRY = ?," +
                     " SHIP_STATE = ?," +
                     " SHIP_CITY = ?," +
                     " SHIP_COUNTRYTEXT = ?," +
                     " SHIP_STATETEXT = ?," +
                     " SHIP_CITYTEXT = ?," +
                     " SHIP_LEADTIME = ?," +
                     " SHIP_LIQUID = ?," +
                     " SHIP_ACTIVE = ?," +
                     " SHIP_LASTUPDATE = SYSDATE()," +
                     " SHIP_UPDATEDBY = ?" +
                     " WHERE SHIP_ID = ?";

            cmd.Parameters.Add("p_SHIP_KG_FIRST", OdbcType.Decimal).Value = decShipKgFirst;
            cmd.Parameters.Add("p_SHIP_FEE_FIRST", OdbcType.Decimal).Value = decShipFeeFirst;
            cmd.Parameters.Add("p_SHIP_KG", OdbcType.Decimal).Value = decShipKg;
            cmd.Parameters.Add("p_SHIP_FEE", OdbcType.Decimal).Value = decShipFee;
            cmd.Parameters.Add("p_SHIP_FEEFREE", OdbcType.Decimal).Value = decShipFeeFree;
            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIP_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_SHIP_CITY", OdbcType.VarChar, 250).Value = strCity;
            cmd.Parameters.Add("p_SHIP_COUNTRYTEXT", OdbcType.Int, 1).Value = intCountry;
            cmd.Parameters.Add("p_SHIP_STATETEXT", OdbcType.Int, 1).Value = intState;
            cmd.Parameters.Add("p_SHIP_CITYTEXT", OdbcType.Int, 1).Value = intCity;
            cmd.Parameters.Add("p_SHIP_LEADTIME", OdbcType.VarChar, 10).Value = strLeadTime;
            cmd.Parameters.Add("p_SHIP_LIQUID", OdbcType.Int, 1).Value = intAllowLiquid;
            cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_SHIP_UPDATEDBY", OdbcType.Int, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_SHIP_ID", OdbcType.Int, 9).Value = intShipId;

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                shipId = intShipId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteShippingById(int intShipId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_SHIPPING2 WHERE SHIP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SHIP_ID", OdbcType.Int, 9).Value = intShipId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    #region "Customization - Ys Hamper Project"
    public DataSet getShippingStateList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SHIP_ID, A.SHIP_KG_FIRST, A.SHIP_FEE_FIRST, A.SHIP_KG, A.SHIP_FEE, A.SHIP_COUNTRY, A.V_SHIPCOUNTRYNAME, A.SHIP_STATE, A.SHIP_ACTIVE," +
                     " A.SHIP_CREATION, A.SHIP_CREATEDBY, A.SHIP_LASTUPDATE, A.SHIP_UPDATEDBY, A.SHIP_DEFAULT, A.SHIP_FEEFREE, A.SHIP_LIQUID" +
                     " FROM VW_SHIPPING2 A";

            if (intActive != 0)
            {
                strSql += " WHERE A.SHIP_ACTIVE = ?";

                cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " GROUP BY A.SHIP_STATE";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean extractShippingCountry(string strShipCountry, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT A.SHIP_ID, A.SHIP_FEE, A.SHIP_COUNTRY, A.V_SHIPCOUNTRYNAME, A.SHIP_STATE, A.SHIP_ACTIVE," +
                    " A.SHIP_CREATION, A.SHIP_CREATEDBY, A.SHIP_LASTUPDATE, A.SHIP_UPDATEDBY, A.SHIP_DEFAULT, A.SHIP_CITY, A.SHIP_COUNTRYTEXT " +
                    " FROM VW_SHIPPING2 A" +
                    " WHERE A.SHIP_COUNTRY = ?";

            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strShipCountry;

            if (intActive != 0)
            {
                strSql += " AND A.SHIP_ACTIVE = ?";

                cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                shipChkCountry = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIP_COUNTRYTEXT"]);
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractShippingStatebyCountry(string strShipCountry, string strShipState, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT A.SHIP_ID, A.SHIP_FEE, A.SHIP_COUNTRY, A.V_SHIPCOUNTRYNAME, A.SHIP_STATE, A.SHIP_CITY, A.SHIP_ACTIVE," +
                     " A.SHIP_CREATION, A.SHIP_CREATEDBY, A.SHIP_LASTUPDATE, A.SHIP_UPDATEDBY, A.SHIP_DEFAULT, A.SHIP_STATETEXT, A.SHIP_CITYTEXT, A.SHIP_FEE_FIRST" +
                     " FROM VW_SHIPPING2 A" +
                     " WHERE A.SHIP_COUNTRY = ? AND A.SHIP_STATE = ?";

            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strShipCountry;
            cmd.Parameters.Add("p_SHIP_STATE", OdbcType.VarChar, 250).Value = strShipState;

            if (intActive != 0)
            {
                strSql += " AND A.SHIP_ACTIVE = ?";

                cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                shipChkState = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIP_STATETEXT"]);
                shipChkCity = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIP_CITYTEXT"]);
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractShippingCitybyState(string strShipCountry, string strShipState, string strShipCity, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT A.SHIP_ID, A.SHIP_FEE, A.SHIP_COUNTRY, A.V_SHIPCOUNTRYNAME, A.SHIP_STATE, A.SHIP_CITY, A.SHIP_ACTIVE," +
                     " A.SHIP_CREATION, A.SHIP_CREATEDBY, A.SHIP_LASTUPDATE, A.SHIP_UPDATEDBY, A.SHIP_DEFAULT, A.SHIP_STATETEXT, A.SHIP_CITYTEXT, A.SHIP_FEE_FIRST" +
                     " FROM VW_SHIPPING2 A" +
                     " WHERE A.SHIP_COUNTRY = ? AND A.SHIP_STATE = ? AND A.SHIP_CITY = ?";

            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strShipCountry;
            cmd.Parameters.Add("p_SHIP_STATE", OdbcType.VarChar, 250).Value = strShipState;
            cmd.Parameters.Add("p_SHIP_CITY", OdbcType.VarChar, 250).Value = strShipCity;

            if (intActive != 0)
            {
                strSql += " AND A.SHIP_ACTIVE = ?";

                cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                shipChkState = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIP_STATETEXT"]);
                shipChkCity = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIP_CITYTEXT"]);
                shipFeeFirst = Convert.ToDecimal(ds.Tables[0].Rows[0]["SHIP_FEE_FIRST"]);

            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public DataSet getShippingCityByState(string strCountry, string strState, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SHIP_ID, A.SHIP_FEE, A.SHIP_COUNTRY, A.V_SHIPCOUNTRYNAME, A.SHIP_STATE, A.SHIP_CITY, A.SHIP_ACTIVE," +
                     " A.SHIP_CREATION, A.SHIP_CREATEDBY, A.SHIP_LASTUPDATE, A.SHIP_UPDATEDBY, A.SHIP_DEFAULT, A.SHIP_STATETEXT, A.SHIP_CITYTEXT" +
                     " FROM VW_SHIPPING2 A" +
                     " WHERE A.SHIP_COUNTRY = ? AND A.SHIP_STATE = ?";

            cmd.Parameters.Add("p_SHIP_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIP_STATE", OdbcType.VarChar, 250).Value = strState;

            if (intActive != 0)
            {
                strSql += " AND A.SHIP_ACTIVE = ?";

                cmd.Parameters.Add("p_SHIP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    #endregion
    #endregion
}
