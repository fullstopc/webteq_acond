﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsLogin
/// </summary>
public class clsLogin : dbconnBase
{
    #region "Constants"
    public const char CONSTDEFAULTSEPERATOR = '|';
    #endregion


    #region "Properties"
    private int _id;
    private int _userId;
    private string _userEmail;
    private int _userType;
    private string _userSettings;
    private string _code;
    private DateTime _creation;
    #endregion


    #region "Property Methods"
    public int id
    {
        get { return _id; }
        set { _id = value; }
    }

    public int userId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    public string userEmail
    {
        get { return _userEmail; }
        set { _userEmail = value; }
    }

    public int userType
    {
        get { return _userType; }
        set { _userType = value; }
    }

    public string userSettings
    {
        get { return _userSettings; }
        set { _userSettings = value; }
    }

    public string code
    {
        get { return _code; }
        set { _code = value; }
    }

    public DateTime creation
    {
        get { return _creation; }
        set { _creation = value; }
    }
    #endregion


    #region "Methods"
    public clsLogin()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    public int add(int intUserId, string strEmail, int intType, string strSettings, string strCode)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_LOGIN (" +
                     "LOGIN_USERID, " +
                     "LOGIN_USEREMAIL, " +
                     "LOGIN_USERTYPE, " +
                     "LOGIN_SETTINGS, " +
                     "LOGIN_CODE, " +
                     "LOGIN_CREATION" +
                     ") VALUES " +
                     "(?,?,?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_LOGIN_USERID", OdbcType.BigInt, 9).Value = intUserId;
            cmd.Parameters.Add("p_LOGIN_USEREMAIL", OdbcType.VarChar, 250).Value = strEmail;
            cmd.Parameters.Add("p_LOGIN_USERTYPE", OdbcType.Int, 9).Value = intType;
            cmd.Parameters.Add("p_LOGIN_SETTINGS", OdbcType.VarChar, 250).Value = strSettings;
            cmd.Parameters.Add("p_LOGIN_CODE", OdbcType.VarChar, 50).Value = strCode;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                id = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractByCode(string strCode)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.LOGIN_ID, A.LOGIN_USERID, A.LOGIN_USEREMAIL, A.LOGIN_USERTYPE, A.LOGIN_SETTINGS, A.LOGIN_CODE, A.LOGIN_CREATION" +
                     " FROM TB_LOGIN A" +
                     " WHERE A.LOGIN_CODE = ?";

            cmd.Parameters.Add("p_LOGIN_CODE", OdbcType.VarChar, 50).Value = strCode;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                id = int.Parse(dataSet.Tables[0].Rows[0]["LOGIN_ID"].ToString());
                userId = int.Parse(dataSet.Tables[0].Rows[0]["LOGIN_USERID"].ToString());
                userEmail = dataSet.Tables[0].Rows[0]["LOGIN_USEREMAIL"].ToString();
                userType = Convert.ToInt16(dataSet.Tables[0].Rows[0]["LOGIN_USERTYPE"]);
                userSettings = dataSet.Tables[0].Rows[0]["LOGIN_SETTINGS"].ToString();
                code = dataSet.Tables[0].Rows[0]["LOGIN_SETTINGS"].ToString();
                creation = Convert.ToDateTime(dataSet.Tables[0].Rows[0]["LOGIN_CREATION"]);
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int deleteByCode(string strCode)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_LOGIN WHERE LOGIN_CODE = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_LOGIN_CODE", OdbcType.VarChar, 50).Value = strCode;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int resetAutoIncrement()
    {
        int intRecordAffected = 0;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();

            strSql = "ALTER TABLE TB_LOGIN AUTO_INCREMENT = 1";

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion
}
