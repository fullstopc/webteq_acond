﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsSetting
/// </summary>
public class clsSetting
{
    #region "Properties"
    // adm
    public const string CONSTADMLOGIN = "admlogin.aspx";
    //@lex public const string CONSTADMLANDING = "adm00.aspx";
    public const string CONSTADMLISTPRODPAGE = "adm02.aspx";
    public const string CONSTADMLISTMEMBERPAGE = "adm04.aspx";
    public const string CONSTADMLISTSLIDEPAGE = "admSlide.aspx";
    public const char CONSTCOMMASEPARATOR = ',';

    public const string CONSTADMCATFOLDER = "cat";
    public const string CONSTADMBRANDFOLDER = "brand";
    public const string CONSTADMPRODFOLDER = "prod";
    public const string CONSTADMPRODARTFOLDER = "prodart";
    public const string CONSTADMPRODCMSFOLDER = "prodcms";
    public const string CONSTADMLOGFOLDER = "log";
    public const string CONSTADMTEMPFOLDER = "temp";
    public const string CONSTADMORDFOLDER = "order";
    public const string CONSTADMNEWSFOLDER = "news";
    public const string CONSTADMEVENTFOLDER = "event";
    public const string CONSTMASTHEADFOLDER = "masthead";
    public const string CONSTADMHIGHFOLDER = "highlight";
    public const string CONSTADMGALLERYFOLDER = "gallery";
    public const string CONSTADMHIGHGALLERYFOLDER = "highgallery";
    public const string CONSTADMHIGHARTICLEFOLDER = "highart";
    public const int CONSTEXTERNALLINK = 9999;  //SS's Maker
    public const string CONSTEXTERNALLINK2 = "External Link"; //SS's Maker

    public const int CONSTADMUSERNAMEMINLENGTH = 5;
    public const int CONSTADMPASSWORDMINLENGTH = 6;

    public const int CONSTADMPAGESIZE = 25;

    public const int CONSTRSVPAYGATEWAYONLINE = 1;
    public const int CONSTSUCCESSID = 1;

    public const int CONSTLISTGROUPINGCAT = 1; //Database view involved.
    public const int CONSTLISTGROUPINGBRAND = 2; //Database view involved.
    public const int CONSTPRODSELECTEDAREA = 2;
    public const int CONSTPRODFORRENT = 2;

    public const int CONSTADMDEFAULTIMGWIDTH = 128;
    public const int CONSTADMDEFAULTIMGHEIGHT = 128;

    public const int CONSTBRANDDEFAULTWIDTH = 180;
    public const int CONSTBRANDDEFAULTHEIGHT = 100;

    public const int CONSTUSREVENTIMGWIDTH = 300;
    public const int CONSTUSREVENTIMGHEIGHT = 225;

    public const int CONSTUSRPHOTOGALLERYIMGWIDTHINDE = 175;
    public const int CONSTUSRPHOTOGALLERYIMGHEIGHTINDE = 132;

    public const int CONSTCMSADMCMSVIEW = 4;

    // event manager Gall3 setup
    public const string CONSTUSRDEFAULTPATH = "usr/";
    public const string CONSTUSRGALLPAGE = "gallery.aspx";
    public const int CONSTUSRINDEVENTGALLWIDTH = 175;
    public const int CONSTUSRINDEVENTGALLHEIGHT = 132;
    public const int CONSTUSRINDEVENTVIDWIDTH = 225;
    public const int CONSTUSRINDEVENTVIDHEIGHT = 169;
    public const int CONSTUSRINDEVENTIMGWIDTH = 255;
    public const int CONSTUSRINDEVENTIMGHEIGHT = 190;
    public const int CONSTUSRINDEVENTGALLERYSIZE = 20;
    public const int CONSTUSRINDEVENTVIDEOSIZE = 6;

    // usr
    public const string CONSTUSRLOGIN = "usr/login.aspx";
    public const string CONSTUSRREGISTER = "usr/register.aspx";
    public const string CONSTUSRFORGOTPWDPAGE = "usr/forgotpassword.aspx";
    public const string CONSTUSRPROFILE = "usr/memberProfile.aspx";
    public const string CONSTUSRPRODUCTPAGE = "product.aspx";
    public const string CONSTUSRGALLERYPAGE = "gallery.aspx";
    public const string CONSTUSRCARTPAGE = "usr/cart.aspx";
    public const string CONSTUSRCHANGEPWD = "usr/changePassword.aspx";
    public const string CONSTUSRCHECKORDSTATUS = "usr/checkOrdStatus.aspx";
    public const string CONSTUSRDIRPAGE = "merchants.aspx";
    public const string CONSTUSRNEWSPAGE = "news.aspx";

    //mobile
    public const string CONSTUSRMOBILEDIRECTARY = "usr/mobile/";
    public const string CONSTUSRMOBILEPAGEPAGE = "pagemb.aspx";
    public const string CONSTUSRMOBILECONTACTPAGE = "contactusmb.aspx";
    public const string CONSTUSRMOBILEPAGESUBPAGE = "pagesubmb.aspx";
    public const string CONSTUSRMOBILEGALLERYPAGE = "gallerymb.aspx";

    public const string CONSTCMSFOLDER = "cms";
    public const string CONSTADMBNNFOLDER = "bnn";

    public const string CONSTADMIMGALLOWEDEXT = "^.+\\.((gif)|(jpg)|(jpeg))$";
    public const string CONSTADMDOCALLOWEDEXT = "^.+\\.((pdf)|(doc)|(docx))$";
    public const string CONSTADMFILEALLOWEDEXT = "^.+\\.((txt)|(csv))$";
    public const string CONSTADMDOCIMGALLOWEDEXT = "^.+\\.((pdf)|(doc)|(docx)|(gif)|(jpg)|(jpeg))$";
    public const int CONSTADMIMGALLOWEDMAXLENGTH = 512000; //500kb
    public const int CONSTADMDOCALLOWEDMAXLENGTH = 2098152; //2mb
    public const int CONSTADMIMGALLOWEDWIDTH = 940;
    public const int CONSTADMIMGALLOWEDHEIGHT = 390;

    public const int CONSTUSRHOMETOPCATEGORY = 8;
    public const int CONSTUSRNEWPRODUCTPAGESIZEHOME = 3;
    public const int CONSTUSRNEWPRODUCTPAGESIZESUB = 6;
    public const int CONSTUSRPRODUCTPAGESIZE = 12;
    public const int CONSTUSREVENTPAGESIZE = 4;
    public const int CONSTUSRRECOMMENDPAGESIZE = 12;
    public const int CONSTUSRRECOMMENDPAGESIZESUB = 3;
    public const int CONSTUSRNEWPRODUCTPAGESIZE = 2;
    public const int CONSTUSRBRANDPAGESIZESUB = 12;
    public const int CONSTUSRCATPAGESIZESUB = 12;
    public const int CONSTUSRCATBRANDPAGESIZEHOME = 3;
    public const int CONSTUSRSHOWTOPPAGESIZE = 2;
    public const int CONSTUSRORDER = 10;
    public const int CONSTUSRPRODNAMELIMIT = 60;
    public const int CONSTUSRCATMENUPAGESIZE = 10;
    public const int CONSTUSRRELPRDPAGESIZE = 5;
    public const int CONSTUSRPRODPAGESIZE = 3;
    public const int CONSTUSREVENTGALLERYSIZE = 15;
    public const int CONSTUSRDIRPAGESIZE = 10;

    //Mobile
    public const int CONSTUSRMOBILEEVENTGALLERYSIZE = 10;

    public const int CONSTUSRMOBILEPHOTOGALLERYIMGWIDTHINDE = 180;
    public const int CONSTUSRMOBILEPHOTOGALLERYIMGHEIGHTINDE = 135;
    //End Mobile

    public const int CONSTUSRCARTIMGWIDTH = 73;
    public const int CONSTUSRCARTIMGHEIGHT = 73;

    public const string CONSTUSRARTICLETHICKBOX = "KeepThis=true&TB_iframe=true&height=500&width=817&modal=false";
    public const string CONSTUSRPRODIMGTHICKBOX = "KeepThis=true&TB_iframe=true&height=500&width=700&modal=false";

    public const string CONSTSAMSUNGLINK = "";
    public const string CONSTDELLLINK = "";
    public const string CONSTHPLINK = "";

    public const int CONSTGMAPWIDTH = 700;
    public const int CONSTGMAPHEIGHT = 500;
    public const int CONSTGMAPZOOM = 16;

    public const string CONSTCARTDIV = "divCartImg";

    public const int CONSTUSRBESTPRODHEIGHT = 139;
    public const int CONSTUSRBESTPRODWIDTH = 185;

    // cmn
    public const string CONSTSORTASC = "ASC";
    public const string CONSTSORTDESC = "DESC";
    public const int CONSTOTHERGROUP = 1;


    public const int CONSTUSRRECPRODUCTIMGWIDTH = 250;
    public const int CONSTUSRRECPRODUCTIMGHEIGHT = 250;
    public const int CONSTUSRRECPRODUCTIMGWIDTH2 = 140;
    public const int CONSTUSRRECPRODUCTIMGHEIGHT2 = 190;

    public const int CONSTUSRNEWPRODUCTIMGWIDTH = 90;
    public const int CONSTUSRNEWPRODUCTIMGHEIGHT = 90;
    public const int CONSTUSRPRODUCTIMGWIDTH = 208;
    public const int CONSTUSRPRODUCTIMGHEIGHT = 202;
    public const int CONSTUSRINDPRODRELIMGWIDTH = 148;
    public const int CONSTUSRINDPRODRELIMGHEIGHT = 214;
    public const int CONSTUSRINDCATIMGWIDTH = 90;
    public const int CONSTUSRINDCATIMGHEIGHT = 90;
    public const int CONSTUSRINDPRODUCTIMGWIDTH = 278;
    public const int CONSTUSRINDPRODUCTIMGHEIGHT = 403;
    public const int CONSTUSRLARGEPRODUCTIMGWIDTH = 680;
    public const int CONSTUSRLARGEPRODUCTIMGHEIGHT = 463;
    public const int CONSTUSRSLIDESHOWTHUMBWIDTH = 90;
    public const int CONSTUSRSLIDESHOWTHUMBHEIGHT = 30;

    public const string CONSTADMCATCODERE = "^[a-zA-Z0-9_]*$";
    public const string CONSTADMPRODCODERE = "^[a-zA-Z0-9_]*$";
    public const string CONSTNUMERICRE = "^\\d+$";
    public const string CONSTNUMERICNOLEADINGZERORE = "^[1-9]\\d{0,}$";
    public const string CONSTCURRENCYRE = "^([0-9]+)(.[0-9][0-9])$";
    public const string CONSTBIGCURRENCYRE = "^([0-9]|[1-9][0-9]|[1-9][0-9][0-9]|[1-9][0-9][0-9][0-9])(.[0-9][0-9])$";
    public const string CONSTBIGSHIPPING = "^([0-9]|[1-9][0-9]|[1-9][0-9][0-9]|[1-9][0-9][0-9][0-9])$";
    public const char CONSTDATESEPERATOR = '/';
    public const char CONSTDEFAULTSEPERATOR = '|';
    public const char CONSTDOTSEPARATOR = '.';

    public const int CONSTCMSADMVIEW = 1;
    public const int CONSTCMSUSRVIEW = 2;

    public const int CONSTDELIVERYFIRSTBLOCK = 3000;
    public const int CONSTDELIVERYSUBSEQBLOCK = 1000;

    public const int CONSTPAGEPARENTROOT = 0;
    public const int CONSTPAGEPARENTROOT2 = -1;

    //Website View
    public const string CONSTWEBSITEVIEWDESKTOP = "Desktop View";
    public const string CONSTWEBSITEVIEWMOBILE = "Mobile View";


    public const int CONSTSLIDESHOWMANAGERIMGWIDTH = 325;
    public const int CONSTSLIDESHOWMANAGERIMGHEIGHT = 195;
    public const char CONSTEQUALSEPARATOR = '=';
    #endregion

    public clsSetting()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}
