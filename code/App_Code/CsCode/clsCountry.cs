﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsCountry
/// </summary>
public class clsCountry : dbconnBase
{
    #region "Properties"
    private int _countryId;
    private string _countryCode;
    private string _countryName;
    private int _countryOrder;
    private int _countryActive;
    private int _countryDefault;
    private int _showState;
    #endregion

    #region "Property Methods"
    public int countryId
    {
        get { return _countryId; }
        set { _countryId = value; }
    }

    public string countryCode
    {
        get { return _countryCode; }
        set { _countryCode = value; }
    }

    public string countryName
    {
        get { return _countryName; }
        set { _countryName = value; }
    }

    public int countryOrder
    {
        get { return _countryOrder; }
        set { _countryOrder = value; }
    }

    public int countryActive
    {
        get { return _countryActive; }
        set { _countryActive = value; }
    }

    public int countryDefault
    {
        get { return _countryDefault; }
        set { _countryDefault = value; }
    }

    public int showState
    {
        get { return _showState; }
        set { _showState = value; }
    }
    #endregion

    public clsCountry()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public Boolean isCountryExist(string strCountryCode, string strCountryName)
    {
        Boolean boolExist = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT * FROM TB_COUNTRY WHERE COUNTRY_CODE = ? AND COUNTRY_NAME = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_COUNTRY_CODE", OdbcType.VarChar, 20).Value = strCountryCode;
            cmd.Parameters.Add("p_COUNTRY_NAME", OdbcType.VarChar, 250).Value = strCountryName;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolExist = true;
            }

            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public int addCountry(string strCountryCode, string strCountryName, int intActive)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_COUNTRY (" +
                     "COUNTRY_CODE, " +
                     "COUNTRY_NAME, " +
                     "COUNTRY_ACTIVE " +
                     ") VALUES " +
                     "(?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_COUNTRY_CODE", OdbcType.VarChar, 20).Value = strCountryCode;
            cmd.Parameters.Add("p_COUNTRY_NAME", OdbcType.VarChar, 250).Value = strCountryName;
            cmd.Parameters.Add("p_COUNTRY_ACTIVE", OdbcType.Int, 1).Value = intActive;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                countryId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameCountryDataSet(int intCountryId, string strCountryCode, string strCountryName, int intActive)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_COUNTRY" +
                     " WHERE COUNTRY_ID = ?" +
                     " AND COUNTRY_CODE = ?" +
                     " AND COUNTRY_NAME = ?" +
                     " AND COUNTRY_ACTIVE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_COUNTRY_ID", OdbcType.BigInt, 9).Value = intCountryId;
            cmd.Parameters.Add("p_COUNTRY_CODE", OdbcType.VarChar, 20).Value = strCountryCode;
            cmd.Parameters.Add("p_COUNTRY_NAME", OdbcType.VarChar, 250).Value = strCountryName;
            cmd.Parameters.Add("p_COUNTRY_ACTIVE", OdbcType.Int, 1).Value = intActive;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateCountryById(int intCountryId, string strCountryCode, string strCountryName, int intActive)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_COUNTRY SET" +
                     " COUNTRY_CODE = ?," +
                     " COUNTRY_NAME = ?," +
                     " COUNTRY_ACTIVE = ?" +
                     " WHERE COUNTRY_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_COUNTRY_CODE", OdbcType.VarChar, 20).Value = strCountryCode;
            cmd.Parameters.Add("p_COUNTRY_NAME", OdbcType.VarChar, 250).Value = strCountryName;
            cmd.Parameters.Add("p_COUNTRY_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_COUNTRY_ID", OdbcType.BigInt, 9).Value = intCountryId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                countryId = intCountryId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateOrderById(int intCountryId, int intOrder)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_COUNTRY SET" +
                     " COUNTRY_ORDER = ?" +
                     " WHERE COUNTRY_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_COUNTRY_ORDER", OdbcType.Int, 9).Value = intOrder;
            cmd.Parameters.Add("p_COUNTRY_ID", OdbcType.BigInt, 9).Value = intCountryId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                countryId = intCountryId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractCountryById(string strCountryCode, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.COUNTRY_ID, A.COUNTRY_CODE, A.COUNTRY_NAME, A.COUNTRY_ORDER, A.COUNTRY_ACTIVE," +
                     " A.COUNTRY_DEFAULT, A.SHOW_STATE" +
                     " FROM TB_COUNTRY A" +
                     " WHERE A.COUNTRY_CODE = ?";

            cmd.Parameters.Add("p_COUNTRY_CODE", OdbcType.VarChar, 20).Value = strCountryCode;

            if (intActive != 0)
            {
                strSql += " AND A.COUNTRY_ACTIVE = ?";
                cmd.Parameters.Add("p_COUNTRY_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                countryId = int.Parse(ds.Tables[0].Rows[0]["COUNTRY_ID"].ToString());
                countryCode = ds.Tables[0].Rows[0]["COUNTRY_CODE"].ToString();
                countryName = ds.Tables[0].Rows[0]["COUNTRY_NAME"].ToString();
                countryOrder = int.Parse(ds.Tables[0].Rows[0]["COUNTRY_ORDER"].ToString());
                countryActive = int.Parse(ds.Tables[0].Rows[0]["COUNTRY_ACTIVE"].ToString());
                countryDefault = int.Parse(ds.Tables[0].Rows[0]["COUNTRY_DEFAULT"].ToString());
                showState = int.Parse(ds.Tables[0].Rows[0]["SHOW_STATE"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractCountryById(int intCountryId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.COUNTRY_ID, A.COUNTRY_CODE, A.COUNTRY_NAME, A.COUNTRY_ORDER, A.COUNTRY_ACTIVE," +
                     " A.COUNTRY_DEFAULT, A.SHOW_STATE" +
                     " FROM TB_COUNTRY A" +
                     " WHERE A.COUNTRY_ID = ?";

            cmd.Parameters.Add("p_COUNTRY_ID", OdbcType.BigInt, 9).Value = intCountryId;

            if (intActive != 0)
            {
                strSql += " AND A.COUNTRY_ACTIVE = ?";
                cmd.Parameters.Add("p_COUNTRY_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                countryId = int.Parse(ds.Tables[0].Rows[0]["COUNTRY_ID"].ToString());
                countryCode = ds.Tables[0].Rows[0]["COUNTRY_CODE"].ToString();
                countryName = ds.Tables[0].Rows[0]["COUNTRY_NAME"].ToString();
                countryOrder = int.Parse(ds.Tables[0].Rows[0]["COUNTRY_ORDER"].ToString());
                countryActive = int.Parse(ds.Tables[0].Rows[0]["COUNTRY_ACTIVE"].ToString());
                countryDefault = int.Parse(ds.Tables[0].Rows[0]["COUNTRY_DEFAULT"].ToString());
                showState = int.Parse(ds.Tables[0].Rows[0]["SHOW_STATE"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public DataSet getCountryList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.COUNTRY_ID, A.COUNTRY_CODE, A.COUNTRY_NAME, A.COUNTRY_ORDER, A.COUNTRY_ACTIVE, " +
                     "A.COUNTRY_DEFAULT, A.SHOW_STATE " +
                     "FROM TB_COUNTRY A";

            if (intActive != 0)
            {
                strSql += " WHERE COUNTRY_ACTIVE = ?";
                cmd.Parameters.Add("p_COUNTRY_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public int deleteCountry(int intCountryId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_COUNTRY WHERE COUNTRY_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_COUNTRY_ID", OdbcType.BigInt, 9).Value = intCountryId;
            cmd.Parameters["p_COUNTRY_ID"].Value = intCountryId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    #region "tb_state"
    public DataSet getStateList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.STATE_ID, A.STATE_CODE, A.STATE_NAME, A.STATE_ORDER, A.STATE_COUNTRY, A.STATE_ACTIVE, A.STATE_DEFAULT" +
                     " FROM TB_STATE A";

            if (intActive != 0)
            {
                strSql += " WHERE A.STATE_ACTIVE = ?";
                cmd.Parameters.Add("p_STATE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    #endregion
}
