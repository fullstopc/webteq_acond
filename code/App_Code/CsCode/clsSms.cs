﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Configuration;
using System.Net;

/// <summary>
/// Summary description for clsSms
/// </summary>
public class clsSms : dbconnBase
{
    #region "Properties"
    private string _smsPrefix;
    private string _smsStatusId;
    private int _smsActive;
    #endregion

    #region "Property Methods"
    public string smsPrefix
    {
        get { return _smsPrefix; }
        set { _smsPrefix = value; }
    }

    public string smsStatusId
    {
        get { return _smsStatusId; }
        set { _smsStatusId = value; }
    }

    public int smsActive
    {
        get { return _smsActive; }
        set { _smsActive = value; }
    }
    #endregion

    #region "Methods"
    public clsSms()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    public Boolean extractSmsFlagById(string strPrefix, string strStatusId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.SMS_MODULEPREFIX, A.SMS_STATUSID, A.SMS_ACTIVE " +
                     "FROM TB_SMS A " +
                     "WHERE A.SMS_MODULEPREFIX = ? AND A.SMS_STATUSID = ?";

            cmd.Parameters.Add("p_SMS_MODULEPREFIX", OdbcType.VarChar, 150).Value = strPrefix;
            cmd.Parameters.Add("p_SMS_STATUSID", OdbcType.VarChar, 150).Value = strStatusId;

            if (intActive != 0)
            {
                strSql += " AND A.SMS_ACTIVE = ?";
                cmd.Parameters.Add("p_SMS_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;
                smsPrefix = ds.Tables[0].Rows[0]["SMS_MODULEPREFIX"].ToString();
                smsStatusId = ds.Tables[0].Rows[0]["SMS_STATUSID"].ToString();
                smsActive = int.Parse(ds.Tables[0].Rows[0]["SMS_ACTIVE"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public static string sendSMS(string strMobile, string strMessage)
    {
        string strResult = "";

        clsSms sms = new clsSms();
        strResult = sms.sendTextSMS(strMobile, strMessage);

        return strResult;
    }

    public string sendTextSMS(string Number, string SMSMessage)
    {
        clsConfig config = new clsConfig();

        config.extractItemByNameGroup(clsConfig.CONSTNAMESMSSENDERURL, clsConfig.CONSTGROUPSMSSETTINGS, 1);
        string sURL = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMESMSSENDERUSERNAME, clsConfig.CONSTGROUPSMSSETTINGS, 1);
        string sUsername = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMESMSSENDERPWD, clsConfig.CONSTGROUPSMSSETTINGS, 1);
        string sPassword = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMESMSSENDERUSERID, clsConfig.CONSTGROUPSMSSETTINGS, 1);
        string sSenderId = config.value;

        string sDestinationNo = Number;
        string sContentType = "1";
        string sMsg = SMSMessage;
        string sResult = "";

        sResult = SendSMS(sURL, sUsername, sPassword, sSenderId, sDestinationNo, sContentType, sMsg);

        return sResult;
    }

    public string SendSMS(string p_sURL, string p_sPassName, string p_sPassword, string p_sSenderId, string p_sDestinationNo, string p_sContentType, string p_sMsg)
    {
        WebRequest myWebRequest = default(WebRequest);
        WebResponse myWebResponse = default(WebResponse);
        string sendResult = null;
        myWebRequest = System.Net.WebRequest.Create(p_sURL);
        myWebRequest.Headers.Add("trx_id", HttpUtility.UrlEncode("0"));
        myWebRequest.Headers.Add("passname", HttpUtility.UrlEncode(p_sPassName));
        myWebRequest.Headers.Add("password", HttpUtility.UrlEncode(p_sPassword));
        myWebRequest.Headers.Add("short_code", HttpUtility.UrlEncode("36828"));
        myWebRequest.Headers.Add("originating_no", HttpUtility.UrlEncode(p_sSenderId));
        myWebRequest.Headers.Add("destination_no", HttpUtility.UrlEncode(p_sDestinationNo));
        myWebRequest.Headers.Add("cp_ref_id", HttpUtility.UrlEncode("0"));
        myWebRequest.Headers.Add("bill_type", HttpUtility.UrlEncode("0"));
        myWebRequest.Headers.Add("bill_price", HttpUtility.UrlEncode("0"));
        myWebRequest.Headers.Add("content_type", HttpUtility.UrlEncode(p_sContentType));
        myWebRequest.Headers.Add("msg", HttpUtility.UrlEncode(p_sMsg));
        myWebRequest.Headers.Add("bulk_fg", HttpUtility.UrlEncode("1"));
        myWebResponse = myWebRequest.GetResponse();

        sendResult = myWebResponse.Headers["result"];

        clsLog.logErroMsg("sendResult : " + sendResult);

        myWebResponse.Close();

        return sendResult;
    }
    #endregion
}
