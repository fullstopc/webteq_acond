﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsCustom
/// </summary>
public class clsCustom : dbconnBase
{
    #region "Properties"
    #region "Details"
    private int _prodId;
    private string _prodCode;
    private string _prodName;
    private string _prodNameJp;
    private string _prodNameMs;
    private string _prodNameZh;
    private string _prodDName;
    private string _prodDNameJp;
    private string _prodDNameMs;
    private string _prodDNameZh;
    private string _prodPageTitleFUrl;
    private string _prodSnapshot;
    private string _prodSnapshotJp;
    private string _prodSnapshotMs;
    private string _prodSnapshotZh;
    private string _prodRemarks;
    private string _prodRemarksJp;
    private string _prodRemarksMs;
    private string _prodRemarksZh;
    private string _prodDesc;
    private string _prodDescJp;
    private string _prodDescMs;
    private string _prodDescZh;
    private int _prodType;
    private string _prodUOM;
    private int _prodWeight;
    private int _prodLength;
    private int _prodWidth;
    private int _prodHeight;
    private int _prodNew;
    private int _prodShowTop;
    private int _prodSold;
    private int _prodRent;
    private int _prodOutOfStock;
    private int _prodPreorder;
    private int _prodBest;
    private int _prodSalesActive;
    private int _prodWithGst;
    private int _prodActive;
    private string _prodImage;
    private DateTime _prodCreation;
    private int _prodCreatedBy;
    private DateTime _prodLastUpdate;
    private int _prodUpdatedBy;
    private int _prodNOV;
    private int _prodNOP;
    private decimal _prodCost = 0;
    private decimal _prodPrice = 0;
    private decimal _prodPromPrice = 0;
    private DateTime _prodPromStartDate = DateTime.MinValue;
    private DateTime _prodPromEndDate = DateTime.MinValue;
    private int _prodCat1;
    private string _prodCat1Name;
    private int _prodCat2;
    private string _prodCat2Name;
    private int _prodCat3;
    private string _prodCat3Name;
    private int _prodCat4;
    private string _prodCat4Name;
    private int _prodCat5;
    private string _prodCat5Name;
    private int _prodCat6;
    private string _prodCat6Name;
    private int _prodCat7;
    private string _prodCat7Name;
    private int _prodCat8;
    private string _prodCat8Name;
    private int _prodCat9;
    private string _prodCat9Name;
    private int _prodCat10;
    private string _prodCat10Name;
    private string _prodStockStatus;
    private int _prodContainLiquid;
    private int _grpId;
    private int _grpingId;
    private string _prodSupplier;
    private int _prodInvAllowNegative;
    private int _prodShowHome;
    private int _prodSmartChoice;
    private int _prodInstallation;
    private decimal _prodRating;
    private string _prodItemPrice;
    #endregion

    #region "Hot Sales"
    private int _salesId;
    private string _salesCode;
    private DateTime _salesStartDate;
    private DateTime _salesEndDate;
    private int _salesQuantity;
    private decimal _salesPrice;
    private DateTime _salesCreation;
    private int _salesCreatedBy;
    private DateTime _salesLastUpdate;
    private int _salesUpdatedBy;
    #endregion

    #region "Gallery"
    private int _prodGallId;
    private string _prodGallImage;
    #endregion

    #region "Specification"
    private int _prodSpecId;
    private int _specGroupId;
    private string _prodSpecType;

    #region "Specification Item"
    private int _siId;
    private string _siName;
    private string _siOperator;
    private decimal _siPrice;
    private string _siAttachment;
    private string _siDateTitle;
    private int _siDateActive;
    private int _siOrder;
    private int _siActive;
    #endregion


    #endregion

    #region "promo price"
    private int _promoID = -1; // PK
    private int _promoProdID = -1; // FK
    private int _promoCreatedBy = -1;
    private int _promoUpdatedBy = -1;
    private int _promoActive = -1;
    private int _promoOrder = -1;
    private decimal _promoPrice = -1;
    private string _promoName;
    private DateTime _promoFrom;
    private DateTime _promoTo;
    private DateTime _promoCreatedAt;
    private DateTime _promoUpdatedAt;
    #endregion
    #endregion

    #region "Property Methods"
    #region "Details"
    public int prodId
    {
        get { return _prodId; }
        set { _prodId = value; }
    }
    public string prodCode
    {
        get { return _prodCode; }
        set { _prodCode = value; }
    }
    public string prodName
    {
        get { return _prodName; }
        set { _prodName = value; }
    }
    public string prodNameJp
    {
        get { return _prodNameJp; }
        set { _prodNameJp = value; }
    }
    public string prodNameMs
    {
        get { return _prodNameMs; }
        set { _prodNameMs = value; }
    }
    public string prodNameZh
    {
        get { return _prodNameZh; }
        set { _prodNameZh = value; }
    }
    public string prodDName
    {
        get { return _prodDName; }
        set { _prodDName = value; }
    }
    public string prodDNameJp
    {
        get { return _prodDNameJp; }
        set { _prodDNameJp = value; }
    }
    public string prodDNameMs
    {
        get { return _prodDNameMs; }
        set { _prodDNameMs = value; }
    }
    public string prodDNameZh
    {
        get { return _prodDNameZh; }
        set { _prodDNameZh = value; }
    }
    public string prodPageTitleFUrl
    {
        get { return _prodPageTitleFUrl; }
        set { _prodPageTitleFUrl = value; }
    }
    public string prodSnapshot
    {
        get { return _prodSnapshot; }
        set { _prodSnapshot = value; }
    }
    public string prodSnapshotJp
    {
        get { return _prodSnapshotJp; }
        set { _prodSnapshotJp = value; }
    }
    public string prodSnapshotMs
    {
        get { return _prodSnapshotMs; }
        set { _prodSnapshotMs = value; }
    }
    public string prodSnapshotZh
    {
        get { return _prodSnapshotZh; }
        set { _prodSnapshotZh = value; }
    }
    public string prodRemarks
    {
        get { return _prodRemarks; }
        set { _prodRemarks = value; }
    }
    public string prodRemarksJp
    {
        get { return _prodRemarksJp; }
        set { _prodRemarksJp = value; }
    }
    public string prodRemarksMs
    {
        get { return _prodRemarksMs; }
        set { _prodRemarksMs = value; }
    }
    public string prodRemarksZh
    {
        get { return _prodRemarksZh; }
        set { _prodRemarksZh = value; }
    }
    public string prodDesc
    {
        get { return _prodDesc; }
        set { _prodDesc = value; }
    }
    public string prodDescJp
    {
        get { return _prodDescJp; }
        set { _prodDescJp = value; }
    }
    public string prodDescMs
    {
        get { return _prodDescMs; }
        set { _prodDescMs = value; }
    }
    public string prodDescZh
    {
        get { return _prodDescZh; }
        set { _prodDescZh = value; }
    }
    public int prodType
    {
        get { return _prodType; }
        set { _prodType = value; }
    }
    public string prodUOM
    {
        get { return _prodUOM; }
        set { _prodUOM = value; }
    }
    public int prodWeight
    {
        get { return _prodWeight; }
        set { _prodWeight = value; }
    }
    public int prodLength
    {
        get { return _prodLength; }
        set { _prodLength = value; }
    }
    public int prodWidth
    {
        get { return _prodWidth; }
        set { _prodWidth = value; }
    }
    public int prodHeight
    {
        get { return _prodHeight; }
        set { _prodHeight = value; }
    }
    public int prodNew
    {
        get { return _prodNew; }
        set { _prodNew = value; }
    }
    public int prodShowTop
    {
        get { return _prodShowTop; }
        set { _prodShowTop = value; }
    }
    public int prodSold
    {
        get { return _prodSold; }
        set { _prodSold = value; }
    }
    public int prodRent
    {
        get { return _prodRent; }
        set { _prodRent = value; }
    }
    public int prodOutOfStock
    {
        get { return _prodOutOfStock; }
        set { _prodOutOfStock = value; }
    }
    public int prodPreorder
    {
        get { return _prodPreorder; }
        set { _prodPreorder = value; }
    }
    public int prodWithGst
    {
        get { return _prodWithGst; }
        set { _prodWithGst = value; }
    }
    public int prodActive
    {
        get { return _prodActive; }
        set { _prodActive = value; }
    }
    public int prodBest
    {
        get { return _prodBest; }
        set { _prodBest = value; }
    }
    public int prodSalesActive
    {
        get { return _prodSalesActive; }
        set { _prodSalesActive = value; }
    }
    public string prodImage
    {
        get { return _prodImage; }
        set { _prodImage = value; }
    }
    public DateTime prodCreation
    {
        get { return _prodCreation; }
        set { _prodCreation = value; }
    }
    public int prodCreatedBy
    {
        get { return _prodCreatedBy; }
        set { _prodCreatedBy = value; }
    }
    public DateTime prodLastUpdate
    {
        get { return _prodLastUpdate; }
        set { _prodLastUpdate = value; }
    }
    public int prodUpdatedBy
    {
        get { return _prodUpdatedBy; }
        set { _prodUpdatedBy = value; }
    }
    public int prodNOV
    {
        get { return _prodNOV; }
        set { _prodNOV = value; }
    }
    public int prodNOP
    {
        get { return _prodNOP; }
        set { _prodNOP = value; }
    }
    public decimal prodCost
    {
        get { return _prodCost; }
        set { _prodCost = value; }
    }
    public decimal prodPrice
    {
        get { return _prodPrice; }
        set { _prodPrice = value; }
    }
    public decimal prodPromPrice
    {
        get { return _prodPromPrice; }
        set { _prodPromPrice = value; }
    }
    public DateTime prodPromStartDate
    {
        get { return _prodPromStartDate; }
        set { _prodPromStartDate = value; }
    }
    public DateTime prodPromEndDate
    {
        get { return _prodPromEndDate; }
        set { _prodPromEndDate = value; }
    }
    public int prodCat2
    {
        get { return _prodCat2; }
        set { _prodCat2 = value; }
    }
    public int prodCat1
    {
        get { return _prodCat1; }
        set { _prodCat1 = value; }
    }
    public int prodCat3
    {
        get { return _prodCat3; }
        set { _prodCat3 = value; }
    }
    public int prodCat4
    {
        get { return _prodCat4; }
        set { _prodCat4 = value; }
    }
    public int prodCat5
    {
        get { return _prodCat5; }
        set { _prodCat5 = value; }
    }
    public int prodCat6
    {
        get { return _prodCat6; }
        set { _prodCat6 = value; }
    }
    public int prodCat7
    {
        get { return _prodCat7; }
        set { _prodCat7 = value; }
    }
    public int prodCat8
    {
        get { return _prodCat8; }
        set { _prodCat8 = value; }
    }
    public int prodCat9
    {
        get { return _prodCat9; }
        set { _prodCat9 = value; }
    }
    public int prodCat10
    {
        get { return _prodCat10; }
        set { _prodCat10 = value; }
    }
    public string prodCat2Name
    {
        get { return _prodCat2Name; }
        set { _prodCat2Name = value; }
    }
    public string prodCat1Name
    {
        get { return _prodCat1Name; }
        set { _prodCat1Name = value; }
    }
    public string prodCat3Name
    {
        get { return _prodCat3Name; }
        set { _prodCat3Name = value; }
    }
    public string prodCat4Name
    {
        get { return _prodCat4Name; }
        set { _prodCat4Name = value; }
    }
    public string prodCat5Name
    {
        get { return _prodCat5Name; }
        set { _prodCat5Name = value; }
    }
    public string prodCat6Name
    {
        get { return _prodCat6Name; }
        set { _prodCat6Name = value; }
    }
    public string prodCat7Name
    {
        get { return _prodCat7Name; }
        set { _prodCat7Name = value; }
    }
    public string prodCat8Name
    {
        get { return _prodCat8Name; }
        set { _prodCat8Name = value; }
    }
    public string prodCat9Name
    {
        get { return _prodCat9Name; }
        set { _prodCat9Name = value; }
    }
    public string prodCat10Name
    {
        get { return _prodCat10Name; }
        set { _prodCat10Name = value; }
    }
    public string prodStockStatus
    {
        get { return _prodStockStatus; }
        set { _prodStockStatus = value; }
    }
    public int prodContainLiquid
    {
        get { return _prodContainLiquid; }
        set { _prodContainLiquid = value; }
    }
    public int grpId
    {
        get { return _grpId; }
        set { _grpId = value; }
    }
    public int grpingId
    {
        get { return _grpingId; }
        set { _grpingId = value; }
    }
    public string prodSupplier
    {
        get { return _prodSupplier; }
        set { _prodSupplier = value; }
    }
    public int prodInvAllowNegative
    {
        get { return _prodInvAllowNegative; }
        set { _prodInvAllowNegative = value; }
    }
    public int prodShowHome
    {
        get { return _prodShowHome; }
        set { _prodShowHome = value; }
    }
    public int prodSmartChoice
    {
        get { return _prodSmartChoice; }
        set { _prodSmartChoice = value; }
    }
    public int prodInstallation
    {
        get { return _prodInstallation; }
        set { _prodInstallation = value; }
    }

    #region "QQGirlSG"
    public decimal prodRating
    {
        get { return _prodRating; }
        set { _prodRating = value; }
    }

    public string prodItemPrice
    {
        get { return _prodItemPrice; }
        set { _prodItemPrice = value; }
    }
    #endregion
    #endregion

    #region "Hot Sales"
    public int salesId
    {
        get { return _salesId; }
        set { _salesId = value; }
    }

    public string salesCode
    {
        get { return _salesCode; }
        set { _salesCode = value; }
    }

    public DateTime salesStartDate
    {
        get { return _salesStartDate; }
        set { _salesStartDate = value; }
    }

    public DateTime salesEndDate
    {
        get { return _salesEndDate; }
        set { _salesEndDate = value; }
    }

    public int salesQuantity
    {
        get { return _salesQuantity; }
        set { _salesQuantity = value; }
    }

    public decimal salesPrice
    {
        get { return _salesPrice; }
        set { _salesPrice = value; }
    }

    public DateTime salesCreation
    {
        get { return _salesCreation; }
        set { _salesCreation = value; }
    }

    public int salesCreatedBy
    {
        get { return _salesCreatedBy; }
        set { _salesCreatedBy = value; }
    }

    public DateTime salesLastUpdate
    {
        get { return _salesLastUpdate; }
        set { _salesLastUpdate = value; }
    }

    public int salesUpdatedBy
    {
        get { return _salesUpdatedBy; }
        set { _salesUpdatedBy = value; }
    }
    #endregion

    #region "Gallery"
    public int prodGallId
    {
        get { return _prodGallId; }
        set { _prodGallId = value; }
    }

    public string prodGallImage
    {
        get { return _prodGallImage; }
        set { _prodGallImage = value; }
    }
    #endregion

    #region "Specification"
    public int prodSpecId
    {
        get { return _prodSpecId; }
        set { _prodSpecId = value; }
    }

    public int specGroupId
    {
        get { return _specGroupId; }
        set { _specGroupId = value; }
    }

    public string prodSpecType
    {
        get { return _prodSpecType; }
        set { _prodSpecType = value; }
    }

    #region "Specification Item"
    public int siId
    {
        get { return _siId; }
        set { _siId = value; }
    }

    public string siName
    {
        get { return _siName; }
        set { _siName = value; }
    }

    public string siOperator
    {
        get { return _siOperator; }
        set { _siOperator = value; }
    }

    public decimal siPrice
    {
        get { return _siPrice; }
        set { _siPrice = value; }
    }

    public string siAttachment
    {
        get { return _siAttachment; }
        set { _siAttachment = value; }
    }

    public string siDateTitle
    {
        get { return _siDateTitle; }
        set { _siDateTitle = value; }
    }

    public int siDateActive
    {
        get { return _siDateActive; }
        set { _siDateActive = value; }
    }

    public int siOrder
    {
        get { return _siOrder; }
        set { _siOrder = value; }
    }

    public int siActive
    {
        get { return _siActive; }
        set { _siActive = value; }
    }
    #endregion
    #endregion

    #region "promo price"
    public int promoID
    {
        get { return _promoID; }
        set { _promoID = value; }
    }
    public int promoProdID
    {
        get { return _promoProdID; }
        set { _promoProdID = value; }
    }
    public int promoCreatedBy
    {
        get { return _promoCreatedBy; }
        set { _promoCreatedBy = value; }
    }
    public int promoUpdatedBy
    {
        get { return _promoUpdatedBy; }
        set { _promoUpdatedBy = value; }
    }
    public int promoActive
    {
        get { return _promoActive; }
        set { _promoActive = value; }
    }
    public int promoOrder
    {
        get { return _promoOrder; }
        set { _promoOrder = value; }
    }
    public decimal promoPrice
    {
        get { return _promoPrice; }
        set { _promoPrice = value; }
    }
    public string promoName
    {
        get { return _promoName; }
        set { _promoName = value; }
    }
    public DateTime promoFrom
    {
        get { return _promoFrom; }
        set { _promoFrom = value; }
    }
    public DateTime promoTo
    {
        get { return _promoTo; }
        set { _promoTo = value; }
    }
    public DateTime promoCreatedAt
    {
        get { return _promoCreatedAt; }
        set { _promoCreatedAt = value; }
    }
    public DateTime promoUpdatedAt
    {
        get { return _promoUpdatedAt; }
        set { _promoUpdatedAt = value; }
    }
    #endregion
    #endregion

    public clsCustom()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    /*Customise for QQGril - Start*/
    #region "QQGirlSG"
    public Boolean isExactSameProdSetQQGirl(int intProdId, string strProdCode, string strProdName, string strProdNameJp, string strProdNameMs, string strProdNameZh, string strProdDName, string strProdDNameJp, string strProdDNameMs, string strProdDNameZh,
                                      string strPageTitleFUrl, string strProdUOM, int intProdWeight, int intProdLength, int intProdWidth, int intProdHeight, int intProdNew, int intProdBest, int intProdActive, int intProdShowTop, int intProdSold, int intProdRent,
                                      int intProdOutOfStock, int intProdPreorder, int intProdWithGst, string strProdImage, int intProdStockStatus, int intProdContainLiquid, decimal decProdRate, string strProdPrice)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODUCT" +
                " WHERE PROD_ID = ?" +
                " AND BINARY PROD_CODE = ?" +
                " AND BINARY PROD_NAME = ?" +
                " AND BINARY PROD_NAME_JP = ?" +
                " AND BINARY PROD_NAME_MS = ?" +
                " AND BINARY PROD_NAME_ZH = ?" +
                " AND BINARY PROD_DNAME = ?" +
                " AND BINARY PROD_DNAME_JP = ?" +
                " AND BINARY PROD_DNAME_MS = ?" +
                " AND BINARY PROD_DNAME_ZH = ?" +
                " AND BINARY PROD_PAGETITLEFURL = ?" +
                " AND BINARY PROD_UOM = ?" +
                " AND PROD_WEIGHT = ?" +
                " AND PROD_LENGTH = ?" +
                " AND PROD_WIDTH = ?" +
                " AND PROD_HEIGHT = ?" +
                " AND PROD_NEW = ?" +
                " AND PROD_BEST = ?" +
                " AND PROD_ACTIVE = ?" +
                " AND PROD_SHOWTOP = ?" +
                " AND PROD_SOLD = ?" +
                " AND PROD_RENT = ?" +
                " AND PROD_OUTOFSTOCK = ?" +
                " AND PROD_PREORDER = ?" +
                " AND PROD_WITH_GST = ?" +
                " AND BINARY PROD_IMAGE = ?" +
                " AND PROD_STOCKSTATUS = ?" +
                " AND PROD_CONTAINLIQUID = ?" +
                " AND PROD_RATING = ?" +
                " AND PROD_PRICE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 50).Value = strProdCode;
            cmd.Parameters.Add("p_PROD_NAME", OdbcType.VarChar, 250).Value = strProdName;
            cmd.Parameters.Add("p_PROD_NAME_JP", OdbcType.VarChar, 250).Value = strProdNameJp;
            cmd.Parameters.Add("p_PROD_NAME_MS", OdbcType.VarChar, 250).Value = strProdNameMs;
            cmd.Parameters.Add("p_PROD_NAME_ZH", OdbcType.VarChar, 250).Value = strProdNameZh;
            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_PROD_DNAME_JP", OdbcType.VarChar, 250).Value = strProdDNameJp;
            cmd.Parameters.Add("p_PROD_DNAME_MS", OdbcType.VarChar, 250).Value = strProdDNameMs;
            cmd.Parameters.Add("p_PROD_DNAME_ZH", OdbcType.VarChar, 250).Value = strProdDNameZh;
            cmd.Parameters.Add("p_PROD_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strPageTitleFUrl;
            cmd.Parameters.Add("p_PROD_UOM", OdbcType.VarChar, 250).Value = strProdUOM;
            cmd.Parameters.Add("p_PROD_WEIGHT", OdbcType.Int, 9).Value = intProdWeight;
            cmd.Parameters.Add("p_PROD_LENGTH", OdbcType.Int, 9).Value = intProdLength;
            cmd.Parameters.Add("p_PROD_WIDTH", OdbcType.Int, 9).Value = intProdWidth;
            cmd.Parameters.Add("p_PROD_HEIGHT", OdbcType.Int, 9).Value = intProdHeight;
            cmd.Parameters.Add("p_PROD_NEW", OdbcType.Int, 1).Value = intProdNew;
            cmd.Parameters.Add("p_PROD_BEST", OdbcType.Int, 1).Value = intProdBest;
            cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intProdActive;
            cmd.Parameters.Add("p_PROD_SHOWTOP", OdbcType.Int, 1).Value = intProdShowTop;
            cmd.Parameters.Add("p_PROD_SOLD", OdbcType.Int, 1).Value = intProdSold;
            cmd.Parameters.Add("p_PROD_RENT", OdbcType.Int, 1).Value = intProdRent;
            cmd.Parameters.Add("p_PROD_OUTOFSTOCK", OdbcType.Int, 1).Value = intProdOutOfStock;
            cmd.Parameters.Add("p_PROD_PREORDER", OdbcType.Int, 1).Value = intProdPreorder;
            cmd.Parameters.Add("p_PROD_WITH_GST", OdbcType.Int, 1).Value = intProdWithGst;
            cmd.Parameters.Add("p_PROD_IMAGE", OdbcType.VarChar, 250).Value = strProdImage;
            cmd.Parameters.Add("p_PROD_STOCKSTATUS", OdbcType.VarChar, 10).Value = intProdStockStatus;
            cmd.Parameters.Add("p_PROD_CONTAINLIQUID", OdbcType.Int, 1).Value = intProdContainLiquid;
            cmd.Parameters.Add("p_PROD_RATING", OdbcType.Decimal).Value = decProdRate;
            cmd.Parameters.Add("p_PROD_PRICE", OdbcType.VarChar, 250).Value = strProdPrice;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateProdByIdQQGirl(int intProdId, string strProdCode, string strProdName, string strProdNameJp, string strProdNameMs, string strProdNameZh, string strProdDName, string strProdDNameJp, string strProdDNameMs, string strProdDNameZh,
                              string strPageTitleFUrl, string strProdUOM, int intProdWeight, int intProdLength, int intProdWidth, int intProdHeight, int intProdNew, int intProdBest, int intProdActive, int intProdShowTop, int intProdSold, int intProdRent,
                              int intProdOutOfStock, int intProdPreorder, int intProdWithGst, string strProdImage, int intProdUpdatedBy, int intProdStockStatus, int intProdContainLiquid, decimal decProdRate, string strProdPrice)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODUCT SET" +
                     " PROD_CODE = ?," +
                     " PROD_NAME = ?," +
                     " PROD_NAME_JP = ?," +
                     " PROD_NAME_MS = ?," +
                     " PROD_NAME_ZH = ?," +
                     " PROD_DNAME = ?," +
                     " PROD_DNAME_JP = ?," +
                     " PROD_DNAME_MS = ?," +
                     " PROD_DNAME_ZH = ?," +
                     " PROD_PAGETITLEFURL = ?," +
                     " PROD_UOM = ?," +
                     " PROD_WEIGHT = ?," +
                     " PROD_LENGTH = ?," +
                     " PROD_WIDTH = ?," +
                     " PROD_HEIGHT = ?," +
                     " PROD_NEW = ?," +
                     " PROD_BEST = ?," +
                     " PROD_ACTIVE = ?," +
                     " PROD_SHOWTOP = ?," +
                     " PROD_SOLD = ?," +
                     " PROD_RENT = ?," +
                     " PROD_OUTOFSTOCK = ?," +
                     " PROD_PREORDER = ?," +
                     " PROD_WITH_GST = ?," +
                     " PROD_IMAGE = ?," +
                     " PROD_UPDATEDBY = ?," +
                     " PROD_LASTUPDATE = SYSDATE()," +
                     " PROD_STOCKSTATUS = ?," +
                     " PROD_CONTAINLIQUID = ?," +
                     " PROD_RATING = ?," +
                     " PROD_PRICE = ?" +
                     " WHERE PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 50).Value = strProdCode;
            cmd.Parameters.Add("p_PROD_NAME", OdbcType.VarChar, 250).Value = strProdName;
            cmd.Parameters.Add("p_PROD_NAME_JP", OdbcType.VarChar, 250).Value = strProdNameJp;
            cmd.Parameters.Add("p_PROD_NAME_MS", OdbcType.VarChar, 250).Value = strProdNameMs;
            cmd.Parameters.Add("p_PROD_NAME_ZH", OdbcType.VarChar, 250).Value = strProdNameZh;
            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_PROD_DNAME_JP", OdbcType.VarChar, 250).Value = strProdDNameJp;
            cmd.Parameters.Add("p_PROD_DNAME_MS", OdbcType.VarChar, 250).Value = strProdDNameMs;
            cmd.Parameters.Add("p_PROD_DNAME_ZH", OdbcType.VarChar, 250).Value = strProdDNameZh;
            cmd.Parameters.Add("p_PROD_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strPageTitleFUrl;
            cmd.Parameters.Add("p_PROD_UOM", OdbcType.VarChar, 250).Value = strProdUOM;
            cmd.Parameters.Add("p_PROD_WEIGHT", OdbcType.Int, 9).Value = intProdWeight;
            cmd.Parameters.Add("p_PROD_LENGTH", OdbcType.Int, 9).Value = intProdLength;
            cmd.Parameters.Add("p_PROD_WIDTH", OdbcType.Int, 9).Value = intProdWidth;
            cmd.Parameters.Add("p_PROD_HEIGHT", OdbcType.Int, 9).Value = intProdHeight;
            cmd.Parameters.Add("p_PROD_NEW", OdbcType.Int, 9).Value = intProdNew;
            cmd.Parameters.Add("p_PROD_BEST", OdbcType.Int, 9).Value = intProdBest;
            cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 9).Value = intProdActive;
            cmd.Parameters.Add("p_PROD_SHOWTOP", OdbcType.Int, 9).Value = intProdShowTop;
            cmd.Parameters.Add("p_PROD_SOLD", OdbcType.Int, 1).Value = intProdSold;
            cmd.Parameters.Add("p_PROD_RENT", OdbcType.Int, 1).Value = intProdRent;
            cmd.Parameters.Add("p_PROD_OUTOFSTOCK", OdbcType.Int, 1).Value = intProdOutOfStock;
            cmd.Parameters.Add("p_PROD_PREORDER", OdbcType.Int, 1).Value = intProdPreorder;
            cmd.Parameters.Add("p_PROD_WITH_GST", OdbcType.Int, 1).Value = intProdWithGst;
            cmd.Parameters.Add("p_PROD_IMAGE", OdbcType.VarChar, 250).Value = strProdImage;
            cmd.Parameters.Add("p_PROD_UPDATEDBY", OdbcType.Int, 9).Value = intProdUpdatedBy;
            cmd.Parameters.Add("p_PROD_STOCKSTATUS", OdbcType.VarChar, 10).Value = intProdStockStatus;
            cmd.Parameters.Add("p_PROD_CONTAINLIQUID", OdbcType.Int, 1).Value = intProdContainLiquid;
            cmd.Parameters.Add("p_PROD_RATING", OdbcType.Decimal).Value = decProdRate;
            cmd.Parameters.Add("p_PROD_PRICE", OdbcType.VarChar, 250).Value = strProdPrice;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }


    public int addProductQQGirl(string strProdCode, string strProdName, string strProdNameJp, string strProdNameMs, string strProdNameZh, string strProdDName, string strProdDNameJp, string strProdDNameMs, string strProdDNameZh,
                          string strPageTitleFURL, string strProdUOM, int intProdWeight, int intProdLength, int intProdWidth, int intProdHeight, int intProdNew, int intProdBest, int intProdActive, int intProdShowTop, int intProdSold, int intProdRent,
                          int intOutOfStock, int intPreorder, int intWithGst, string strProdImg, int intProdCreatedBy, int intProdStockStatus, int intProdContainLiquid, decimal decProdRate, string strProdPrice)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRODUCT (" +
                    "PROD_CODE, " +
                    "PROD_NAME, " +
                    "PROD_NAME_JP, " +
                    "PROD_NAME_MS, " +
                    "PROD_NAME_ZH, " +
                    "PROD_DNAME, " +
                    "PROD_DNAME_JP, " +
                    "PROD_DNAME_MS, " +
                    "PROD_DNAME_ZH, " +
                    "PROD_PAGETITLEFURL, " +
                    "PROD_UOM, " +
                    "PROD_WEIGHT, " +
                    "PROD_LENGTH, " +
                    "PROD_WIDTH, " +
                    "PROD_HEIGHT, " +
                    "PROD_NEW, " +
                    "PROD_BEST, " +
                    "PROD_ACTIVE, " +
                    "PROD_SHOWTOP, " +
                    "PROD_SOLD, " +
                    "PROD_RENT, " +
                    "PROD_OUTOFSTOCK, " +
                    "PROD_PREORDER, " +
                    "PROD_WITH_GST, " +
                    "PROD_IMAGE, " +
                    "PROD_CREATEDBY, " +
                    "PROD_CREATION, " +
                    "PROD_STOCKSTATUS, " +
                    "PROD_CONTAINLIQUID," +
                    "PROD_RATING, " +
                    "PROD_PRICE " +
                    ") VALUES " +
                   "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE(),?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 50).Value = strProdCode;
            cmd.Parameters.Add("p_PROD_NAME", OdbcType.VarChar, 250).Value = strProdName;
            cmd.Parameters.Add("p_PROD_NAME_JP", OdbcType.VarChar, 250).Value = strProdNameJp;
            cmd.Parameters.Add("p_PROD_NAME_MS", OdbcType.VarChar, 250).Value = strProdNameMs;
            cmd.Parameters.Add("p_PROD_NAME_ZH", OdbcType.VarChar, 250).Value = strProdNameZh;
            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_PROD_DNAME_JP", OdbcType.VarChar, 250).Value = strProdDNameJp;
            cmd.Parameters.Add("p_PROD_DNAME_MS", OdbcType.VarChar, 250).Value = strProdDNameMs;
            cmd.Parameters.Add("p_PROD_DNAME_ZH", OdbcType.VarChar, 250).Value = strProdDNameZh;
            cmd.Parameters.Add("p_PROD_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strPageTitleFURL;
            cmd.Parameters.Add("p_PROD_UOM", OdbcType.VarChar, 9).Value = strProdUOM;
            cmd.Parameters.Add("p_PROD_WEIGHT", OdbcType.Int, 9).Value = intProdWeight;
            cmd.Parameters.Add("p_PROD_LENGTH", OdbcType.Int, 9).Value = intProdLength;
            cmd.Parameters.Add("p_PROD_WIDTH", OdbcType.Int, 9).Value = intProdWidth;
            cmd.Parameters.Add("p_PROD_HEIGHT", OdbcType.Int, 9).Value = intProdHeight;
            cmd.Parameters.Add("p_PROD_NEW", OdbcType.Int, 1).Value = intProdNew;
            cmd.Parameters.Add("p_PROD_BEST", OdbcType.Int, 1).Value = intProdBest;
            cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intProdActive;
            cmd.Parameters.Add("p_PROD_SHOWTOP", OdbcType.Int, 1).Value = intProdShowTop;
            cmd.Parameters.Add("p_PROD_SOLD", OdbcType.Int, 1).Value = intProdSold;
            cmd.Parameters.Add("p_PROD_RENT", OdbcType.Int, 1).Value = intProdRent;
            cmd.Parameters.Add("p_PROD_OUTOFSTOCK", OdbcType.Int, 1).Value = intOutOfStock;
            cmd.Parameters.Add("p_PROD_PREORDER", OdbcType.Int, 1).Value = intPreorder;
            cmd.Parameters.Add("p_PROD_WITH_GST", OdbcType.Int, 1).Value = intWithGst;
            cmd.Parameters.Add("p_PROD_IMAGE", OdbcType.VarChar, 250).Value = strProdImg;
            cmd.Parameters.Add("p_PROD_CREATEDBY", OdbcType.Int, 9).Value = intProdCreatedBy;
            cmd.Parameters.Add("p_PROD_STOCKSTATUS", OdbcType.VarChar, 10).Value = intProdStockStatus;
            cmd.Parameters.Add("p_PROD_CONTAINLIQUID", OdbcType.Int, 1).Value = intProdContainLiquid;
            cmd.Parameters.Add("p_PROD_RATING", OdbcType.Decimal).Value = decProdRate;
            cmd.Parameters.Add("p_PROD_PRICE", OdbcType.VarChar, 250).Value = strProdPrice;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                prodId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractProdByIdQQGirl(int intProdId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_CODE, A.PROD_NAME, A.PROD_NAME_JP, A.PROD_NAME_MS, A.PROD_NAME_ZH, A.PROD_DNAME, A.PROD_DNAME_JP, A.PROD_DNAME_MS, A.PROD_DNAME_ZH," +
                     " A.PROD_PAGETITLEFURL, A.PROD_UOM, A.PROD_WEIGHT, A.PROD_LENGTH, A.PROD_WIDTH, A.PROD_HEIGHT, A.PROD_IMAGE, A.PROD_NEW, A.PROD_BEST, A.PROD_SHOWTOP, A.PROD_SOLD," +
                     " A.PROD_RENT, A.PROD_OUTOFSTOCK, A.PROD_PREORDER, A.PROD_WITH_GST, A.PROD_ACTIVE, A.PROD_STOCKSTATUS, A.PROD_CONTAINLIQUID, A.PROD_RATING, A.PROD_PRICE" +
                     " FROM TB_PRODUCT A" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            if (intActive != 0)
            {
                strSql += " AND A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                prodId = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ID"].ToString());
                prodCode = dataSet.Tables[0].Rows[0]["PROD_CODE"].ToString();
                prodName = dataSet.Tables[0].Rows[0]["PROD_NAME"].ToString();
                prodNameJp = dataSet.Tables[0].Rows[0]["PROD_NAME_JP"].ToString();
                prodNameMs = dataSet.Tables[0].Rows[0]["PROD_NAME_MS"].ToString();
                prodNameZh = dataSet.Tables[0].Rows[0]["PROD_NAME_ZH"].ToString();
                prodDName = dataSet.Tables[0].Rows[0]["PROD_DNAME"].ToString();
                prodDNameJp = dataSet.Tables[0].Rows[0]["PROD_DNAME_JP"].ToString();
                prodDNameMs = dataSet.Tables[0].Rows[0]["PROD_DNAME_MS"].ToString();
                prodDNameZh = dataSet.Tables[0].Rows[0]["PROD_DNAME_ZH"].ToString();
                prodPageTitleFUrl = dataSet.Tables[0].Rows[0]["PROD_PAGETITLEFURL"].ToString();
                prodUOM = dataSet.Tables[0].Rows[0]["PROD_UOM"].ToString();
                prodWeight = int.Parse(dataSet.Tables[0].Rows[0]["PROD_WEIGHT"].ToString());
                prodLength = int.Parse(dataSet.Tables[0].Rows[0]["PROD_LENGTH"].ToString());
                prodWidth = int.Parse(dataSet.Tables[0].Rows[0]["PROD_WIDTH"].ToString());
                prodHeight = int.Parse(dataSet.Tables[0].Rows[0]["PROD_HEIGHT"].ToString());
                prodNew = int.Parse(dataSet.Tables[0].Rows[0]["PROD_NEW"].ToString());
                prodBest = int.Parse(dataSet.Tables[0].Rows[0]["PROD_BEST"].ToString());
                //prodSalesActive = int.Parse(dataSet.Tables[0].Rows[0]["PROD_HOTSALES"].ToString());
                prodActive = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ACTIVE"].ToString());
                prodShowTop = int.Parse(dataSet.Tables[0].Rows[0]["PROD_SHOWTOP"].ToString());
                prodSold = int.Parse(dataSet.Tables[0].Rows[0]["PROD_SOLD"].ToString());
                prodRent = int.Parse(dataSet.Tables[0].Rows[0]["PROD_RENT"].ToString());
                prodOutOfStock = int.Parse(dataSet.Tables[0].Rows[0]["PROD_OUTOFSTOCK"].ToString());
                prodPreorder = int.Parse(dataSet.Tables[0].Rows[0]["PROD_PREORDER"].ToString());
                prodWithGst = int.Parse(dataSet.Tables[0].Rows[0]["PROD_WITH_GST"].ToString());
                prodImage = dataSet.Tables[0].Rows[0]["PROD_IMAGE"].ToString();
                prodStockStatus = dataSet.Tables[0].Rows[0]["PROD_STOCKSTATUS"].ToString();
                prodContainLiquid = int.Parse(dataSet.Tables[0].Rows[0]["PROD_CONTAINLIQUID"].ToString());
                prodRating = decimal.Parse(dataSet.Tables[0].Rows[0]["PROD_RATING"].ToString());
                prodItemPrice = dataSet.Tables[0].Rows[0]["PROD_PRICE"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int addProductItemQQGirl(int intProdId, string strProdItemTitle, string strProdItemValue, int intProdItemOrder, int intProdItemCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRODITEM (" +
                    "PROD_ID, " +
                    "PRODITEM_TITLE, " +
                    "PRODITEM_VALUE, " +
                    "PRODITEM_ORDER, " +
                    "PRODITEM_CREATION, " +
                    "PRODITEM_CREATEDBY " +
                    ") VALUES " +
                   "(?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRODITEM_TITLE", OdbcType.VarChar, 50).Value = strProdItemTitle;
            cmd.Parameters.Add("p_PRODITEM_VALUE", OdbcType.VarChar, 250).Value = strProdItemValue;
            cmd.Parameters.Add("p_PRODITEM_ORDER", OdbcType.Int, 9).Value = intProdItemOrder;
            cmd.Parameters.Add("p_PRODITEM_CREATEDBY", OdbcType.Int, 9).Value = intProdItemCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                prodId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getProdItemListByProdIdQQGirl(int intProdId)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PRODITEM_ID, A.PRODITEM_TITLE, A.PRODITEM_VALUE, A.PRODITEM_ORDER, A.PRODITEM_CREATION, A.PRODITEM_CREATEDBY, A.PRODITEM_LASTUPDATE, A.PRODITEM_UPDATEDBY" +
                     " FROM TB_PRODITEM A" +
                     " WHERE A.PROD_ID = ? ORDER BY A.PRODITEM_ORDER ASC";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean isExactSameProductItemQQGirl(int intProdItemId, int intProdId, string strProdItemTitle, string strProdItemValue, int intProdItemOrder)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODITEM" +
                " WHERE PROD_ID = ?" +
                " AND PRODITEM_ID = ?" +
                " AND PRODITEM_TITLE = ?" +
                " AND PRODITEM_VALUE = ?" +
                " AND PRODITEM_ORDER = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRODITEM_ID", OdbcType.Int, 9).Value = intProdItemId;
            cmd.Parameters.Add("p_PRODITEM_TITLE", OdbcType.VarChar, 250).Value = strProdItemTitle;
            cmd.Parameters.Add("p_PRODITEM_VALUE", OdbcType.VarChar, 250).Value = strProdItemValue;
            cmd.Parameters.Add("p_PRODITEM_ORDER", OdbcType.Int, 9).Value = intProdItemOrder;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateProductItemByIdQQGirl(int intProdItemId, int intProdId, string strProdItemTitle, string strProdItemValue, int intProdItemOrder, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODITEM SET" +
                     " PROD_ID = ?," +
                     " PRODITEM_TITLE = ?," +
                     " PRODITEM_VALUE = ?," +
                     " PRODITEM_ORDER = ?," +
                     " PRODITEM_LASTUPDATE = SYSDATE()," +
                     " PRODITEM_UPDATEDBY = ?" +
                     " WHERE PRODITEM_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRODITEM_TITLE", OdbcType.VarChar, 250).Value = strProdItemTitle;
            cmd.Parameters.Add("p_PRODITEM_VALUE", OdbcType.VarChar, 250).Value = strProdItemValue;
            cmd.Parameters.Add("p_PRODITEM_ORDER", OdbcType.BigInt, 9).Value = intProdItemOrder;
            cmd.Parameters.Add("p_PRODITEM_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_PRODITEM_ID", OdbcType.BigInt, 9).Value = intProdItemId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteProductItembyIdQQGirl(int intProdItemId, int intProdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRODITEM WHERE PROD_ID = ? AND PRODITEM_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRODITEM_ID", OdbcType.Int, 9).Value = intProdItemId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion
    /*Customise for QQGril - End*/
}