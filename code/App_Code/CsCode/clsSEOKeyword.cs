﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsVendor
/// </summary>
public class clsSeoKeyword : dbconnBase
{
    #region "Properties"
    public int ID { get; set; }
    public string keyword { get; set; }
    public string group { get; set; }
    public int order { get; set; }
    public int important { get; set; }
    public int createdBy { get; set; }
    public int updatedBy { get; set; }

    public DateTime creation { get; set; }
    public DateTime lastupdate { get; set; }
    #endregion

    public clsSeoKeyword()
    {
        resetValue();
    }

    #region "Methods"
    public DataTable getDataTable()
    {
        DataSet ds = getDataSet();
        if (ds != null)
        {
            return ds.Tables[0];
        }
        return null;
    }
    public DataSet getDataSet()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_SeoKeyword WHERE 1 = 1";

            if (ID != -1)
            {
                strSql += " AND SEO_ID = ?";
                cmd.Parameters.Add("p_SEO_ID", OdbcType.Int, 9).Value = ID;
            }
            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            ds = null;
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public bool extractByID(int ID)
    {
        resetValue();
        this.ID = ID;

        DataTable dt = getDataTable();
        if (dt != null && dt.Rows.Count == 1)
        {
            DataRow row = dt.Rows[0];
            //active = Convert.ToInt16(row["NEWS_ACTIVE"]);

            //content = Convert.ToString(row["NEWS_CONTENT"]);
            //title = Convert.ToString(row["NEWS_TITLE"]);
            //sourceUrl = Convert.ToString(row["NEWS_SOURCE_URL"]);
            //sourceTitle = Convert.ToString(row["NEWS_SOURCE_TITLE"]);

            //date = row["NEWS_DATE"] != DBNull.Value ? Convert.ToDateTime(row["NEWS_DATE"]) : date;
            //creation = row["NEWS_CREATION"] != DBNull.Value ? Convert.ToDateTime(row["NEWS_CREATION"]) : creation;
            //lastupdate = row["NEWS_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(row["NEWS_LASTUPDATE"]) : lastupdate;
            //return true;
        }
        return false;
    }

    public bool save(string companyName, string companyNameShort, List<clsSeoKeyword> products, List<clsSeoKeyword> areas, int updatedBy)
    {
        this.update(companyName, "Company Name", updatedBy);
        this.update(companyNameShort, "Company Short Name", updatedBy);

        this.deleteList("PRODUCT");
        if (products.Count > 0)
        {
            this.addList(products);
        }

        this.deleteList("AREA");
        if (areas.Count > 0)
        {
            this.addList(areas);
        }
        //this.addList(area);
        return true;
    }

    public int update(string keyword, string group, int updatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);


            cmd.Parameters.AddWithValue("p_KEYWORD", keyword);
            cmd.Parameters.AddWithValue("p_UPDATEDBY", updatedBy);
            cmd.Parameters.AddWithValue("p_GROUP", group);


            strSql = "UPDATE TB_SEOKEYWORD SET SEO_KEYWORD = ?,"+
                    " SEO_UPDATEDBY = ?," +
                    " SEO_LASTUPDATE = NOW()" +
                    " WHERE SEO_GROUP = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();


            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public bool addList(List<clsSeoKeyword> list)
    {
        bool boolSuccess = false;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql = ""; ;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            //strSql += "DELETE FROM TB_SEOKEYWORD WHERE SEO_GROUP = ?;";
            //cmd.Parameters.AddWithValue("p_GROUP", list[0].group);

            strSql += "INSERT INTO TB_SEOKEYWORD (" +
                     "SEO_GROUP, " +
                     "SEO_KEYWORD, " +
                     "SEO_IMPORTANT, " +
                     "SEO_ORDER," +
                     "SEO_CREATEDBY," +
                     "SEO_CREATION" +
                     ") VALUES " + string.Join(",", list.AsEnumerable().Select(x => "(?,?,?,?,?,NOW())").ToArray());
            strSql += ";";
            for (int i = 0; i < list.Count; i++)
            {
                clsSeoKeyword seo = list[i];

                cmd.Parameters.AddWithValue("p_GROUP_" + i, seo.group);
                cmd.Parameters.AddWithValue("p_KEYWORD_" + i, seo.keyword);
                cmd.Parameters.AddWithValue("p_IMPORTANT_" + i, seo.important);
                cmd.Parameters.AddWithValue("p_ORDER_" + i, seo.order);
                cmd.Parameters.AddWithValue("p_CREATEDBY_" + i, seo.createdBy);
            }


            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;



            int intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected > 0)
            {
                boolSuccess = true;
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolSuccess;
    }
    public bool deleteList(string group)
    {
        bool boolSuccess = false;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql = ""; ;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql += "DELETE FROM TB_SEOKEYWORD WHERE SEO_GROUP = ?;";
            cmd.Parameters.AddWithValue("p_GROUP", group);

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;



            int intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected > 0)
            {
                trans.Commit();
                boolSuccess = true;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolSuccess;
    }
    private void resetValue()
    {
        ID = -1;
        order = -1;
        createdBy = -1;
        updatedBy = -1;
        important = -1;

        keyword = null;
        group = null;

        creation = DateTime.MinValue;
        lastupdate = DateTime.MinValue;
    }
    #endregion
}

public class SeoKeyword
{

}