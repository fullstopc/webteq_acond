﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsProgram
/// </summary>
public class clsProgram : dbconnBase
{
    #region "Properties"
    #region "Program Manager"
    private int _programId;

    private string _programName;
    private string _programNameZh;
    private string _programSnapshot;
    private string _programSnapshotZh;
    private string _programDesc;
    private string _programDescZh;
    private string _programImage;
    
    private DateTime _programPublishDate;
    private decimal _programFee;
    private int _programParticipant;
    private int _programNoOfHours;
    private int _programActive;
    private int _onlineRegistration;
    private int _progArea;
    private DateTime _programCreation;
    private int _programCreatedBy;
    private int _programOther;

    private int _dateId;
    private DateTime _programStartDate;
    private DateTime _programEndDate;
    private int _deleted;
    private int _vNOP;

    private int _typeId;
    private string _typeName;
    #endregion

    #region "Program Group Manager"
    private int _progGrpId;
    private string _progGrpCode;
    private string _progGrpName;
    private string _progGrpDName;
    private string _progGrpDesc;
    private string _progGrpSnapshot;
    private string _progGrpImage;
    private int _progGrpShowTop;
    private int _progGrpActive;
    private int _progGrpOrder;
    private DateTime _progGrpCreation;
    private int _progGrpCreatedBy;
    private DateTime _progGrpLastUpdate;
    private int _progGrpUpdatedBy;
    private int _progGrpOther;
    private int _progGrpShowDesc;
    private int _progGrpParent;
    #endregion
    #endregion

    #region "Property Methods"
    #region "Program Manager"
    public int programId
    {
        get { return _programId; }
        set { _programId = value; }
    }

    public string programName
    {
        get { return _programName; }
        set { _programName = value; }
    }

    public string programNameZh
    {
        get { return _programNameZh; }
        set { _programNameZh = value; }
    }

    public string programSnapshot
    {
        get { return _programSnapshot; }
        set { _programSnapshot = value; }
    }

    public string programSnapshotZh
    {
        get { return _programSnapshotZh; }
        set { _programSnapshotZh = value; }
    }

    public string programDesc
    {
        get { return _programDesc; }
        set { _programDesc = value; }
    }

    public string programDescZh
    {
        get { return _programDescZh; }
        set { _programDescZh = value; }
    }

    public string programImage
    {
        get { return _programImage; }
        set { _programImage = value; }
    }

    public DateTime programStartDate
    {
        get { return _programStartDate; }
        set { _programStartDate = value; }
    }

    public DateTime programEndDate
    {
        get { return _programEndDate; }
        set { _programEndDate = value; }
    }

    public DateTime programPublishDate
    {
        get { return _programPublishDate; }
        set { _programPublishDate = value; }
    }

    public decimal programFee
    {
        get { return _programFee; }
        set { _programFee = value; }
    }

    public int programParticipant
    {
        get { return _programParticipant; }
        set { _programParticipant = value; }
    }

    public int programNoOfHours
    {
        get { return _programNoOfHours; }
        set { _programNoOfHours = value; }
    }

    public int programActive
    {
        get { return _programActive; }
        set { _programActive = value; }
    }

    public int programOnlineRegistration
    {
        get { return _onlineRegistration; }
        set { _onlineRegistration = value; }
    }

    public int programArea
    {
        get { return _progArea; }
        set { _progArea = value; }
    }

    public DateTime programCreation
    {
        get { return _programCreation; }
        set { _programCreation = value; }
    }

    public int programCreatedBy
    {
        get { return _programCreatedBy; }
        set { _programCreatedBy = value; }
    }

    public int programOther
    {
        get { return _programOther; }
        set { _programOther = value; }
    }

    public int dateId
    {
        get { return _dateId; }
        set { _dateId = value; }
    }

    public int typeId
    {
        get { return _typeId; }
        set { _typeId = value; }
    }

    public string typeName
    {
        get { return _typeName; }
        set { _typeName = value; }
    }

    public int deleted
    {
        get { return _deleted; }
        set { _deleted = value; }
    }

    public int vNOP
    {
        get { return _vNOP; }
        set { _vNOP = value; }
    }
    #endregion

    #region "Program Group Manager"
    public int progGrpId
    {
        get { return _progGrpId; }
        set { _progGrpId = value; }
    }

    public string progGrpCode
    {
        get { return _progGrpCode; }
        set { _progGrpCode = value; }
    }

    public string progGrpName
    {
        get { return _progGrpName; }
        set { _progGrpName = value; }
    }

    public string progGrpDName
    {
        get { return _progGrpDName; }
        set { _progGrpDName = value; }
    }

    public string progGrpDesc
    {
        get { return _progGrpDesc; }
        set { _progGrpDesc = value; }
    }

    public string progGrpSnapshot
    {
        get { return _progGrpSnapshot; }
        set { _progGrpSnapshot = value; }
    }

    public string progGrpImage
    {
        get { return _progGrpImage; }
        set { _progGrpImage = value; }
    }

    public int progGrpShowTop
    {
        get { return _progGrpShowTop; }
        set { _progGrpShowTop = value; }
    }

    public int progGrpActive
    {
        get { return _progGrpActive; }
        set { _progGrpActive = value; }
    }

    public int progGrpOrder
    {
        get { return _progGrpOrder; }
        set { _progGrpOrder = value; }
    }

    public DateTime progGrpCreation
    {
        get { return _progGrpCreation; }
        set { _progGrpCreation = value; }
    }

    public int progGrpCreatedBy
    {
        get { return _progGrpCreatedBy; }
        set { _progGrpCreatedBy = value; }
    }

    public DateTime progGrpLastUpdate
    {
        get { return _progGrpLastUpdate; }
        set { _progGrpLastUpdate = value; }
    }

    public int progGrpUpdatedBy
    {
        get { return _progGrpUpdatedBy; }
        set { _progGrpUpdatedBy = value; }
    }

    public int progGrpOther
    {
        get { return _progGrpOther; }
        set { _progGrpOther = value; }
    }

    public int progGrpShowDesc
    {
        get { return _progGrpShowDesc; }
        set { _progGrpShowDesc = value; }
    }

    public int progGrpParent
    {
        get { return _progGrpParent; }
        set { _progGrpParent = value; }
    }
    #endregion
    #endregion

    public clsProgram()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    #region "Program Manager"
    public int addProgram(string strProgramName, string strProgramNameZh, string strProgramSnapshot, string strProgramSnapshotZh, string strProgramDesc, string strProgramDescZh, string strProgramImage, decimal dcProgramFee, DateTime dtPublishDate, int intParticipant, int intNoOfHours, int intActive, int intOnlineRegistration, int intArea, int intProgramCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PROGRAM (" +
                        "PRO_NAME, " +
                        "PRO_NAME_ZH, " +
                        "PRO_SNAPSHOT, " +
                        "PRO_SNAPSHOT_ZH, " +
                        "PRO_DESC, " +
                        "PRO_DESC_ZH, " +
                        "PRO_IMAGE, " +
                        "PRO_FEE, " +
                        "PRO_PUBLISHDATE, " +
                        "PRO_PARTICIPANT, " +
                        "PRO_NOOFHOURS, " +
                        "PRO_ACTIVE, " +
                        "PRO_ONLINEREG, " +
                        "PRO_AREA, " +
                        "PRO_CREATION, " +
                        "PRO_CREATEDBY" +
                        ") VALUES " +
                        "(?,?,?,?,?,?,?,?,SYSDATE(),?,?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PRO_NAME", OdbcType.VarChar, 250).Value = strProgramName;
            cmd.Parameters.Add("p_PRO_NAME", OdbcType.VarChar, 250).Value = strProgramNameZh;
            cmd.Parameters.Add("p_PRO_SNAPSHOT", OdbcType.VarChar, 250).Value = strProgramSnapshot;
            cmd.Parameters.Add("p_PRO_SNAPSHOT_ZH", OdbcType.VarChar, 250).Value = strProgramSnapshotZh;
            cmd.Parameters.Add("p_PRO_DESC", OdbcType.Text).Value = strProgramDesc;
            cmd.Parameters.Add("p_PRO_DESC_ZH", OdbcType.Text).Value = strProgramDescZh;
            cmd.Parameters.Add("p_PRO_IMAGE", OdbcType.VarChar, 250).Value = strProgramImage;
            //cmd.Parameters.Add("p_PRO_STARTDATE", OdbcType.DateTime).Value = dtProgStartDate;
            //cmd.Parameters.Add("p_PRO_ENDDATE", OdbcType.DateTime).Value = dtProgEndDate;
            cmd.Parameters.Add("p_PRO_FEE", OdbcType.Decimal).Value = dcProgramFee;
            //cmd.Parameters.Add("p_PRO_PUBLISHDATE", OdbcType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("p_PRO_PARTICIPANT", OdbcType.Int, 50).Value = intParticipant;
            cmd.Parameters.Add("p_PRO_NOOFHOURS", OdbcType.Int, 50).Value = intNoOfHours;
            cmd.Parameters.Add("p_PRO_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PRO_ONLINEREG", OdbcType.Int, 1).Value = intOnlineRegistration;
            cmd.Parameters.Add("p_PRO_AREA", OdbcType.Int, 1).Value = intArea;
            cmd.Parameters.Add("p_PRO_CREATEDBY", OdbcType.BigInt, 9).Value = intProgramCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                programId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addProgramDate(int intDateId, DateTime dtStartDate, DateTime dtEndDate)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PROGRAMDATE (" +
                     "DATE_ID, " +
                     "PROG_STARTDATE, " +
                     "PROG_ENDDATE" +
                     ") VALUES " +
                     "(?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_DATE_ID", OdbcType.Int, 9).Value = intDateId;
            cmd.Parameters.Add("p_PROG_STARTDATE", OdbcType.DateTime).Value = dtStartDate;
            cmd.Parameters.Add("p_PROG_ENDDATE", OdbcType.DateTime).Value = dtEndDate;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                dateId = intDateId;
                programId = intDateId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int assignGrpToProg(int intProgId, int intGrpId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PROGGROUP (" +
                     "PROG_ID, " +
                     "GRP_ID " +
                     ") VALUES " +
                     "(?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROG_ID", OdbcType.BigInt, 9).Value = intProgId;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                programId = intProgId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteProgramDate(int intDateId, DateTime dtStartDate, DateTime dtEndDate)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PROGRAMDATE" +
                     " WHERE DATE_ID = ?" +
                     " AND PROG_STARTDATE = ?" +
                     " AND PROG_ENDDATE = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_DATE_ID", OdbcType.Int, 9).Value = intDateId;
            cmd.Parameters.Add("p_PROG_STARTDATE", OdbcType.DateTime).Value = dtStartDate;
            cmd.Parameters.Add("p_PROG_ENDDATE", OdbcType.DateTime).Value = dtEndDate;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int assignProgramGroup(int intProgId, int intGroupId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PROGGROUP (" +
                     "PRO_ID, " +
                     "TYPE_ID" +
                     ") VALUES " +
                     "(?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PRO_ID", OdbcType.BigInt, 9).Value = intProgId;
            cmd.Parameters.Add("p_TYPE_ID", OdbcType.BigInt, 9).Value = intGroupId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                programId = intProgId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateProgramGroup(int intProgId, int intTypeId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PROGGROUP SET TYPE_ID = ? WHERE PRO_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_TYPE_ID", OdbcType.BigInt, 9).Value = intTypeId;
            cmd.Parameters.Add("p_PRO_ID", OdbcType.BigInt, 9).Value = intProgId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                programId = intProgId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateGrpToProg(int intProgId, int intGrpId, int intOldGrpId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PROGGROUP SET" +
                     " GRP_ID = ?" +
                     " WHERE PROG_ID = ?" +
                     " AND GRP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intGrpId;
            cmd.Parameters.Add("p_PROG_ID", OdbcType.BigInt, 9).Value = intProgId;
            cmd.Parameters.Add("p_OLD_GRP_ID", OdbcType.BigInt, 9).Value = intOldGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                programId = intProgId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getProgramDateListByProgId(int intProgId)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROG_ID, A.DATE_ID, A.PROG_STARTDATE, A.PROG_ENDDATE, CONCAT(DATE_FORMAT(A.PROG_STARTDATE, '%d %b %Y'), ' - ', DATE_FORMAT(A.PROG_ENDDATE, '%d %b %Y')) AS PROG_PERIOD FROM TB_PROGRAMDATE A WHERE DATE_ID = ?";

            cmd.Parameters.Add("p_DATE_ID", OdbcType.BigInt, 9).Value = intProgId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getProgramDateList()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROG_ID, A.DATE_ID, A.PROG_STARTDATE, A.PROG_ENDDATE, CONCAT(DATE_FORMAT(A.PROG_STARTDATE, '%d %b %Y'), ' - ', DATE_FORMAT(A.PROG_ENDDATE, '%d %b %Y')) AS PROG_PERIOD FROM TB_PROGRAMDATE A";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    //public DataSet getProgGroupListById(int intProgId)
    //{
    //    DataSet ds = new DataSet();

    //    try
    //    {
    //        string strSql;

    //        OdbcCommand cmd = new OdbcCommand();
    //        OdbcDataAdapter da = new OdbcDataAdapter();

    //        strSql = "SELECT A.PROG_ID, A.GRP_ID, B.GRPING_ID" +
    //                 " FROM (TB_PROGGROUP A LEFT JOIN TB_GROUP B ON (A.GRP_ID = B.GRP_ID))" +
    //                 " WHERE A.PROG_ID = ?";

    //        cmd.Parameters.Add("p_PROG_ID", OdbcType.BigInt, 9).Value = intProgId;

    //        cmd.Connection = openConn();
    //        cmd.CommandText = strSql;
    //        cmd.CommandType = CommandType.Text;
    //        da.SelectCommand = cmd;
    //        da.Fill(ds);
    //    }
    //    catch (Exception ex)
    //    {
    //        logErroMsg(ex.Message.ToString());
    //        throw ex;
    //    }
    //    finally
    //    {
    //        closeConn();
    //    }

    //    return ds;
    //}

    public Boolean extractProgramById(int intProgramId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PRO_ID, A.PRO_NAME, A.PRO_NAME_ZH, A.PRO_SNAPSHOT, A.PRO_SNAPSHOT_ZH, A.PRO_DESC, A.PRO_DESC_ZH," +
                     " A.PRO_IMAGE, A.PRO_FEE, A.PRO_PUBLISHDATE, A.PRO_PARTICIPANT, A.PRO_NOOFHOURS, A.PRO_ACTIVE, A.PRO_ONLINEREG," +
                     " A.PRO_AREA, A.PRO_CREATION, A.PRO_CREATEDBY, A.PRO_LASTUPDATE, A.PRO_UPDATEDBY, A.PRO_DELETED" +
                     " FROM TB_PROGRAM A" +
                     " WHERE A.PRO_ID = ?";

            //strSql = "SELECT A.PRO_ID, A.PRO_NAME, A.PRO_NAME_ZH, A.PRO_SNAPSHOT, A.PRO_SNAPSHOT_ZH, A.PRO_DESC, A.PRO_DESC_ZH, A.PRO_IMAGE, A.PRO_FEE, A.PRO_PUBLISHDATE," +
            //         "A.PRO_PARTICIPANT, A.PRO_NOOFHOURS, A.PRO_ACTIVE, A.PRO_ONLINEREG, A.PRO_AREA, A.PRO_CREATION, A.PRO_CREATEDBY, A.PRO_DELETED, A.V_NOP" +
            //         " FROM VW_PROGRAM A" +
            //         " WHERE A.PRO_ID = ?";

            cmd.Parameters.Add("p_PRO_ID", OdbcType.BigInt, 9).Value = intProgramId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                programId = int.Parse(dataSet.Tables[0].Rows[0]["PRO_ID"].ToString());
                programName = dataSet.Tables[0].Rows[0]["PRO_NAME"].ToString();
                programNameZh = dataSet.Tables[0].Rows[0]["PRO_NAME_ZH"].ToString();
                programSnapshot = dataSet.Tables[0].Rows[0]["PRO_SNAPSHOT"].ToString();
                programSnapshotZh = dataSet.Tables[0].Rows[0]["PRO_SNAPSHOT_ZH"].ToString();
                programDesc = dataSet.Tables[0].Rows[0]["PRO_DESC"].ToString();
                programDescZh = dataSet.Tables[0].Rows[0]["PRO_DESC_ZH"].ToString();
                programImage = dataSet.Tables[0].Rows[0]["PRO_IMAGE"].ToString();
                programFee = dataSet.Tables[0].Rows[0]["PRO_FEE"] != DBNull.Value ? decimal.Parse(dataSet.Tables[0].Rows[0]["PRO_FEE"].ToString()) : Convert.ToDecimal("0.00");
                programPublishDate = dataSet.Tables[0].Rows[0]["PRO_PUBLISHDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PRO_PUBLISHDATE"].ToString()) : DateTime.MaxValue;
                programParticipant = int.Parse(dataSet.Tables[0].Rows[0]["PRO_PARTICIPANT"].ToString());
                programNoOfHours = int.Parse(dataSet.Tables[0].Rows[0]["PRO_NOOFHOURS"].ToString());
                programActive = int.Parse(dataSet.Tables[0].Rows[0]["PRO_ACTIVE"].ToString());
                programOnlineRegistration = int.Parse(dataSet.Tables[0].Rows[0]["PRO_ONLINEREG"].ToString());
                programArea = int.Parse(dataSet.Tables[0].Rows[0]["PRO_AREA"].ToString());
                programCreation = dataSet.Tables[0].Rows[0]["PRO_CREATION"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PRO_CREATION"].ToString()) : DateTime.MaxValue;
                programCreatedBy = int.Parse(dataSet.Tables[0].Rows[0]["PRO_CREATEDBY"].ToString());
                //typeId = dataSet.Tables[0].Rows[0]["V_TYPE"] != DBNull.Value ? int.Parse(dataSet.Tables[0].Rows[0]["V_TYPE"].ToString()) : 0;
                //typeName = dataSet.Tables[0].Rows[0]["V_TYPENAME"].ToString();
                deleted = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PRO_DELETED"]);
                //vNOP = Convert.ToInt16(dataSet.Tables[0].Rows[0]["V_NOP"]);
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractProgramDateById(int intDateId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_PROGRAMDATE A WHERE A.DATE_ID = ?";

            cmd.Parameters.Add("p_DATE_ID", OdbcType.Int, 9).Value = intDateId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                dateId = int.Parse(dataSet.Tables[0].Rows[0]["DATE_ID"].ToString());
                programStartDate = dataSet.Tables[0].Rows[0]["PROG_STARTDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PROG_STARTDATE"].ToString()) : DateTime.MaxValue;
                programEndDate = dataSet.Tables[0].Rows[0]["PROG_ENDDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PROG_ENDDATE"].ToString()) : DateTime.MaxValue;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractProgramDateByProgDateId(int intProgDateId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_PROGRAMDATE A WHERE A.PROG_ID = ?";

            cmd.Parameters.Add("p_PROG_ID", OdbcType.Int, 9).Value = intProgDateId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                dateId = int.Parse(dataSet.Tables[0].Rows[0]["DATE_ID"].ToString());
                programStartDate = dataSet.Tables[0].Rows[0]["PROG_STARTDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PROG_STARTDATE"].ToString()) : DateTime.MaxValue;
                programEndDate = dataSet.Tables[0].Rows[0]["PROG_ENDDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PROG_ENDDATE"].ToString()) : DateTime.MaxValue;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean isExactSameDataSet(int intProgId, string strProgName, string strProgNameZh, string strProgSnapshot, string strProgSnapshotZh, string strProgDesc, string strProgDescZh, string strProgImage, decimal dcProgFee, DateTime dtPublishDate, int intParticipant, int intNoOfHours, int intActive, int intOnlineReg, int intArea)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PROGRAM" +
                " WHERE PRO_ID = ? " +
                " AND PRO_NAME = ? " +
                " AND PRO_NAME_ZH = ?" +
                " AND PRO_SNAPSHOT = ? " +
                " AND PRO_SNAPSHOT_ZH = ?" +
                " AND PRO_DESC = ? " +
                " AND PRO_DESC_ZH = ?" +
                " AND PRO_IMAGE = ?" +
                " AND PRO_FEE = ? " +
                " AND PRO_PUBLISHDATE = ? " +
                " AND PRO_PARTICIPANT = ? " +
                " AND PRO_NOOFHOURS = ? " +
                " AND PRO_ACTIVE = ? " +
                " AND PRO_ONLINEREG = ?" +
                " AND PRO_AREA = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PRO_ID", OdbcType.Int, 1).Value = intProgId;
            cmd.Parameters.Add("p_PRO_NAME", OdbcType.VarChar, 250).Value = strProgName;
            cmd.Parameters.Add("p_PRO_NAME_ZH", OdbcType.VarChar, 250).Value = strProgNameZh;
            cmd.Parameters.Add("p_PRO_SNAPSHOT", OdbcType.VarChar, 250).Value = strProgSnapshot;
            cmd.Parameters.Add("p_PRO_SNAPSHOT_ZH", OdbcType.VarChar, 250).Value = strProgSnapshotZh;
            cmd.Parameters.Add("p_PRO_DESC", OdbcType.Text).Value = strProgDesc;
            cmd.Parameters.Add("p_PRO_DESC_ZH", OdbcType.Text).Value = strProgDescZh;
            cmd.Parameters.Add("p_PRO_IMAGE", OdbcType.VarChar, 250).Value = strProgImage;
            cmd.Parameters.Add("p_PRO_FEE", OdbcType.Decimal).Value = dcProgFee;
            cmd.Parameters.Add("p_PRO_PUBLISHDATE", OdbcType.DateTime).Value = dtPublishDate;
            cmd.Parameters.Add("p_PRO_PARTICIPANT", OdbcType.Int, 50).Value = intParticipant;
            cmd.Parameters.Add("p_PRO_NOOFHOURS", OdbcType.Int, 50).Value = intNoOfHours;
            cmd.Parameters.Add("p_PRO_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PRO_ONLINEREG", OdbcType.Int, 1).Value = intOnlineReg;
            cmd.Parameters.Add("p_PRO_AREA", OdbcType.Int, 1).Value = intArea;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateProgramById(int intProgId, string strProgName, string strProgNameZh, string strProgSnapshot, string strProgSnapshotZh, string strProgDesc, string strProgDescZh, string strProgImage, decimal dcProgFee, DateTime dtPublishDate, int intParticipant, int intNoOfHours, int intActive, int intOnlineReg, int intArea, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PROGRAM SET" +
                     " PRO_NAME = ?," +
                     " PRO_NAME_ZH = ?," +
                     " PRO_SNAPSHOT = ?," +
                     " PRO_SNAPSHOT_ZH = ?," +
                     " PRO_DESC = ?," +
                     " PRO_DESC_ZH = ?," +
                     " PRO_IMAGE = ?," +
                     " PRO_FEE = ?," +
                     " PRO_PUBLISHDATE = ?," +
                     " PRO_PARTICIPANT = ?," +
                     " PRO_NOOFHOURS = ?," +
                     " PRO_ACTIVE = ?," +
                     " PRO_ONLINEREG = ?," +
                     " PRO_AREA = ?," +
                     " PRO_LASTUPDATE = SYSDATE()," +
                     " PRO_UPDATEDBY = ?" +
                     " WHERE PRO_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PRO_NAME", OdbcType.VarChar, 250).Value = strProgName;
            cmd.Parameters.Add("p_PRO_NAME_ZH", OdbcType.VarChar, 250).Value = strProgNameZh;
            cmd.Parameters.Add("p_PRO_SNAPSHOT", OdbcType.VarChar, 250).Value = strProgSnapshot;
            cmd.Parameters.Add("p_PRO_SNAPSHOT_ZH", OdbcType.VarChar, 250).Value = strProgSnapshotZh;
            cmd.Parameters.Add("p_PRO_DESC", OdbcType.Text).Value = strProgDesc;
            cmd.Parameters.Add("p_PRO_DESC_ZH", OdbcType.Text).Value = strProgDescZh;
            cmd.Parameters.Add("p_PRO_IMAGE", OdbcType.VarChar, 250).Value = strProgImage;
            cmd.Parameters.Add("p_PRO_FEE", OdbcType.Decimal).Value = dcProgFee;
            cmd.Parameters.Add("p_PRO_PUBLISHDATE", OdbcType.DateTime).Value = dtPublishDate;
            cmd.Parameters.Add("p_PRO_PARTICIPANT", OdbcType.Int, 50).Value = intParticipant;
            cmd.Parameters.Add("p_PRO_NOOFHOURS", OdbcType.Int, 50).Value = intNoOfHours;
            cmd.Parameters.Add("p_PRO_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PRO_ONLINEREG", OdbcType.Int, 1).Value = intOnlineReg;
            cmd.Parameters.Add("p_PRO_AREA", OdbcType.Int, 1).Value = intArea;
            cmd.Parameters.Add("p_PRO_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_PRO_ID", OdbcType.BigInt, 9).Value = intProgId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                programId = intProgId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteProgram(int intProgramId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PROGRAM WHERE PRO_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PRO_ID", OdbcType.Int, 9);
            cmd.Parameters["p_PRO_ID"].Value = intProgramId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getProgramByProgramId(int intProgramId)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PRO_ID, A.PRO_NAME, A.PRO_SNAPSHOT, A.PRO_DESC, " +
                     "A.PRO_FEE, A.PRO_PUBLISHDATE, A.PRO_PARTICIPANT, A.PRO_ACTIVE, A.PRO_ONLINEREG, A.PRO_CREATION, A.PRO_CREATEDBY, A.PRO_DELETED, A.V_TYPE, A.V_TYPENAME, A.V_NOP" +
                     " FROM VW_PROGRAM A" +
                     " WHERE A.PRO_ID = ?";

            cmd.Parameters.Add("p_PRO_ID", OdbcType.Int, 1).Value = intProgramId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getProgramList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT A.PRO_ID, A.PRO_NAME, A.PRO_NAME_ZH, A.PRO_SNAPSHOT, A.PRO_SNAPSHOT_ZH, A.PRO_DESC, A.PRO_DESC_ZH, A.PRO_IMAGE," +
                     " A.PRO_FEE, A.PRO_PUBLISHDATE, A.PRO_PARTICIPANT, A.PRO_ACTIVE, A.PRO_ONLINEREG, A.PRO_CREATION," +
                     " A.PRO_CREATEDBY, A.PRO_LASTUPDATE, A.PRO_UPDATEDBY, A.PRO_DELETED, B.GRP_ID," +
                     " (SELECT COUNT(DISTINCT PAR_ID) FROM TB_PARTICIPANT WHERE A.PRO_ID = PAR_PROGRAM) AS V_NOP," +
                     " (SELECT PROGGRP_DNAME FROM TB_PROGRAMGROUP WHERE B.GRP_ID = PROGGRP_ID) AS V_PROGGRP_DNAME" +
                     " FROM (TB_PROGRAM A LEFT JOIN TB_PROGGROUP B ON (A.PRO_ID = B.PROG_ID))";
           
            if (intActive != 0)
            {
                strSql += " WHERE PRO_ACTIVE = ?";

                cmd.Parameters.Add("p_PRO_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    //public DataSet getProgramList2(int intActive)
    //{
    //    DataSet ds = new DataSet();

    //    try
    //    {
    //        OdbcDataAdapter da = new OdbcDataAdapter();
    //        OdbcCommand cmd = new OdbcCommand();
    //        string strSql;

    //        strSql = "SELECT A.PRO_ID, A.PRO_NAME, A.PRO_NAME_ZH, A.PRO_SNAPSHOT, A.PRO_SNAPSHOT_ZH, A.PRO_DESC, A.PRO_DESC_ZH, A.PRO_IMAGE," +
    //                 " A.PRO_FEE, A.PRO_PUBLISHDATE, A.PRO_PARTICIPANT, A.PRO_ACTIVE, A.PRO_ONLINEREG, A.PRO_CREATION," +
    //                 " A.PRO_CREATEDBY, A.PRO_LASTUPDATE, A.PRO_UPDATEDBY, A.PRO_DELETED, B.GRP_ID" +
    //                 " FROM (TB_PROGRAM A LEFT JOIN TB_PROGGROUP B ON (A.PRO_ID = B.PROG_ID))";

    //        //cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intGrpId;

    //        if (intActive != 0)
    //        {
    //            strSql += " WHERE PRO_ACTIVE = ?";

    //            cmd.Parameters.Add("p_PRO_ACTIVE", OdbcType.Int, 1).Value = intActive;
    //        }

    //        cmd.Connection = openConn();
    //        cmd.CommandType = CommandType.Text;
    //        cmd.CommandText = strSql;

    //        da.SelectCommand = cmd;
    //        da.Fill(ds);
    //    }
    //    catch (Exception ex)
    //    {
    //        logErroMsg(ex.Message.ToString());
    //        throw ex;
    //    }
    //    finally
    //    {
    //        closeConn();
    //    }

    //    return ds;
    //}

    public DataSet getProgramListByParticipant(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            
            strSql = "SELECT A.PRO_ID, A.PRO_NAME, A.PRO_SNAPSHOT, A.PRO_DESC, A.PRO_FEE, A.PRO_PUBLISHDATE, A.PRO_PARTICIPANT, A.PRO_ACTIVE," +
                     " A.PRO_ONLINEREG, A.PRO_CREATION, A.PRO_CREATEDBY, A.V_TYPE, A.V_TYPENAME, A.V_NOP" +
                     " FROM VW_PROGRAM A";

            if (intActive != 0)
            {
                strSql += " WHERE A.PRO_ACTIVE = ?";

                cmd.Parameters.Add("p_PRO_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " GROUP BY A.PRO_PARTICIPANT";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean isExactSameProgramDateSet(int intDateId, DateTime dtStartDate, DateTime dtEndDate)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PROGRAMDATE" +
                     " WHERE DATE_ID = ? " +
                     " AND PROG_STARTDATE = ? " +
                     " AND PROG_ENDDATE = ? ";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_DATE_ID", OdbcType.BigInt, 9).Value = intDateId;
            cmd.Parameters.Add("p_PROG_STARTDATE", OdbcType.DateTime).Value = dtStartDate;
            cmd.Parameters.Add("p_PROG_ENDDATE", OdbcType.DateTime).Value = dtEndDate;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateProgramDate(int intDateId, DateTime dtStartDate, DateTime dtEndDate)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PROGRAMDATE SET" +
                     " PROG_STARTDATE = ?," +
                     " PROG_ENDDATE = ?" +
                     " WHERE DATE_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_DATE_ID", OdbcType.BigInt, 9).Value = intDateId;
            cmd.Parameters.Add("p_PROG_STARTDATE", OdbcType.DateTime).Value = dtStartDate;
            cmd.Parameters.Add("p_PROG_ENDDATE", OdbcType.DateTime).Value = dtEndDate;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                dateId = intDateId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int setDeleted(int intProgId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PROGRAM SET PRO_DELETED = ? WHERE PRO_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PRO_DELETED", OdbcType.Int, 1).Value = 1;
            cmd.Parameters.Add("p_PRO_ID", OdbcType.Int, 9).Value = intProgId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getProgTypeList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_PROGRANTYPE A";

            if (intActive != 0)
            {
                strSql += " AND A.TYPE_ACTIVE = ?";
                cmd.Parameters.Add("p_TYPE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    #endregion

    #region "Program Group Manager"
    public int addProgGroup(string strProgGrpCode, string strProgGrpName, string strProgGrpDName, string strProgGrpImage, int intProgGrpShowTop, int intProgGrpActive, int intProgGrpCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            int intProgGrpOrder = generateProgGroupOrderNo();

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PROGRAMGROUP (" +
                     " PROGGRP_CODE, " +
                     " PROGGRP_NAME, " +
                     " PROGGRP_DNAME, " +
                     " PROGGRP_IMAGE, " +
                     " PROGGRP_SHOWTOP, " +
                     " PROGGRP_ACTIVE, " +
                     " PROGGRP_CREATION, " +
                     " PROGGRP_CREATEDBY " +
                     ") VALUES " +
                     "(?,?,?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROGGRP_CODE", OdbcType.VarChar, 20).Value = strProgGrpCode;
            cmd.Parameters.Add("p_PROGGRP_NAME", OdbcType.VarChar, 250).Value = strProgGrpName;
            cmd.Parameters.Add("p_PROGGRP_DNAME", OdbcType.VarChar, 250).Value = strProgGrpDName;
            cmd.Parameters.Add("p_PROGGRP_IMAGE", OdbcType.VarChar, 250).Value = strProgGrpImage;
            cmd.Parameters.Add("p_PROGGRP_SHOWTOP", OdbcType.Int, 1).Value = intProgGrpShowTop;
            cmd.Parameters.Add("p_PROGGRP_ACTIVE", OdbcType.Int, 1).Value = intProgGrpActive;
            cmd.Parameters.Add("p_PROGGRP_CREATEDBY", OdbcType.BigInt, 1).Value = intProgGrpCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                progGrpId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int generateProgGroupOrderNo()
    {
        int progGrpOrderNo = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT PROGGRP_ORDER FROM TB_PROGRAMGROUP ORDER BY PROGGRP_ORDER DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            progGrpOrderNo = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return progGrpOrderNo + 1;
    }

    public int getTotalRecord(int intOther, int intActive)
    {
        int intTotal = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PROGRAMGROUP A" +
                     " WHERE 1 = 1";

            if (intOther == 0)
            {
                strSql += " AND A.PROGGRP_OTHER = ?";
                cmd.Parameters.Add("p_PROGGRP_OTHER", OdbcType.Int, 1).Value = intOther;
            }

            if (intActive != 0)
            {
                strSql += " AND A.PROGGRP_ACTIVE = ?";
                cmd.Parameters.Add("p_PROGGRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            intTotal = int.Parse(cmd.ExecuteScalar().ToString());
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intTotal;
    }

    public Boolean isExactSameProgGrp(int intProgGrpId, string strProgGrpCode, string strProgGrpName, string strProgGrpDName, string strProgGrpImage, int intProgGrpShowTop, int intProgGrpActive)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PROGRAMGROUP" +
                     " WHERE PROGGRP_ID = ?" +
                     " AND BINARY PROGGRP_CODE = ?" +
                     " AND BINARY PROGGRP_NAME = ?" +
                     " AND BINARY PROGGRP_DNAME = ?" +
                     " AND BINARY PROGGRP_IMAGE = ?" +
                     " AND PROGGRP_SHOWTOP = ?" +
                     " AND PROGGRP_ACTIVE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROGGRP_ID", OdbcType.BigInt, 9).Value = intProgGrpId;
            cmd.Parameters.Add("p_PROGGRP_CODE", OdbcType.VarChar, 20).Value = strProgGrpCode;
            cmd.Parameters.Add("p_PROGGRP_NAME", OdbcType.VarChar, 250).Value = strProgGrpName;
            cmd.Parameters.Add("p_PROGGRP_DNAME", OdbcType.VarChar, 250).Value = strProgGrpDName;
            cmd.Parameters.Add("p_PROGGRP_IMAGE", OdbcType.VarChar, 250).Value = strProgGrpImage;
            cmd.Parameters.Add("p_PROGGRP_SHOWTOP", OdbcType.Int, 1).Value = intProgGrpShowTop;
            cmd.Parameters.Add("p_PROGGRP_ACTIVE", OdbcType.Int, 1).Value = intProgGrpActive;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public Boolean isExactSameProgGrpDescDataSet(int intProgGrpId, string strProgGrpDesc)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PROGRAMGROUP" +
                     " WHERE PROGGRP_ID = ?" +
                     " AND PROGGRP_DESC = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROGGRP_ID", OdbcType.BigInt, 9).Value = intProgGrpId;
            cmd.Parameters.Add("p_PROGGRP_DESC", OdbcType.Text).Value = strProgGrpDesc;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateProgGroupById(int intProgGrpId, string strProgGrpName, string strProgGrpDName, string strProgGrpImage, int intProgGrpShowTop, int intProgGrpActive, int intProgGrpUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PROGRAMGROUP SET" +
                     " PROGGRP_NAME = ?," +
                     " PROGGRP_DNAME = ?," +
                     " PROGGRP_IMAGE = ?," +
                     " PROGGRP_SHOWTOP = ?," +
                     " PROGGRP_ACTIVE = ?," +
                     " PROGGRP_LASTUPDATE = SYSDATE()," +
                     " PROGGRP_UPDATEDBY = ?" +
                     " WHERE PROGGRP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROGGRP_NAME", OdbcType.VarChar, 250).Value = strProgGrpName;
            cmd.Parameters.Add("p_PROGGRP_DNAME", OdbcType.VarChar, 250).Value = strProgGrpDName;
            cmd.Parameters.Add("p_PROGGRP_IMAGE", OdbcType.VarChar, 250).Value = strProgGrpImage;
            cmd.Parameters.Add("p_PROGGRP_SHOWTOP", OdbcType.Int, 1).Value = intProgGrpShowTop;
            cmd.Parameters.Add("p_PROGGRP_ACTIVE", OdbcType.Int, 1).Value = intProgGrpActive;
            cmd.Parameters.Add("p_PROGGRP_UPDATEDBY", OdbcType.BigInt, 9).Value = intProgGrpUpdatedBy;
            cmd.Parameters.Add("p_PROGGRP_ID", OdbcType.BigInt, 9).Value = intProgGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                progGrpId = intProgGrpId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateProgGrpDescById(int intProgGrpId, string strProgGrpDesc, int intProgGrpUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PROGRAMGROUP SET" +
                     " PROGGRP_DESC = ?," +
                     " PROGGRP_LASTUPDATE = SYSDATE()," +
                     " PROGGRP_UPDATEDBY = ?" +
                     " WHERE PROGGRP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROGGRP_DESC", OdbcType.Text).Value = strProgGrpDesc;
            cmd.Parameters.Add("p_PROGGRP_UPDATEDBY", OdbcType.BigInt, 9).Value = intProgGrpUpdatedBy;
            cmd.Parameters.Add("p_PROGGRP_ID", OdbcType.BigInt, 9).Value = intProgGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                progGrpId = intProgGrpId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractProgGrpById(int intProgGrpId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROGGRP_ID, A.PROGGRP_CODE, A.PROGGRP_NAME, A.PROGGRP_NAME_JP, A.PROGGRP_NAME_MS, A.PROGGRP_NAME_ZH," +
                     " A.PROGGRP_DNAME, A.PROGGRP_DNAME_JP, A.PROGGRP_DNAME_MS, A.PROGGRP_DNAME_ZH, A.PROGGRP_DESC, A.PROGGRP_DESC_ZH," +
                     " A.PROGGRP_SNAPSHOT, PROGGRP_IMAGE, A.PROGGRP_SHOWTOP, A.PROGGRP_ACTIVE, A.PROGGRP_ORDER, A.PROGGRP_CREATION," +
                     " A.PROGGRP_CREATEDBY, A.PROGGRP_LASTUPDATE, A.PROGGRP_UPDATEDBY, A.PROGGRP_OTHER, A.PROGGRP_SHOWDESC, A.PROGGRP_PARENT" +
                     " FROM TB_PROGRAMGROUP A" +
                     " WHERE A.PROGGRP_ID = ?";

            cmd.Parameters.Add("p_PROGGRP_ID", OdbcType.Int, 9).Value = intProgGrpId;

            if (intActive != 0)
            {
                strSql += " AND A.PROGGRP_ACTIVE = ?";
                cmd.Parameters.Add("p_PROGGRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                progGrpId = int.Parse(dataSet.Tables[0].Rows[0]["PROGGRP_ID"].ToString());
                progGrpCode = dataSet.Tables[0].Rows[0]["PROGGRP_CODE"].ToString();
                progGrpName = dataSet.Tables[0].Rows[0]["PROGGRP_NAME"].ToString();
                progGrpDName = dataSet.Tables[0].Rows[0]["PROGGRP_DNAME"].ToString();
                progGrpDesc = dataSet.Tables[0].Rows[0]["PROGGRP_DESC"].ToString();
                progGrpSnapshot = dataSet.Tables[0].Rows[0]["PROGGRP_SNAPSHOT"].ToString();
                progGrpImage = dataSet.Tables[0].Rows[0]["PROGGRP_IMAGE"].ToString();
                progGrpShowTop = int.Parse(dataSet.Tables[0].Rows[0]["PROGGRP_SHOWTOP"].ToString());
                progGrpActive = int.Parse(dataSet.Tables[0].Rows[0]["PROGGRP_ACTIVE"].ToString());
                progGrpOrder = int.Parse(dataSet.Tables[0].Rows[0]["PROGGRP_ORDER"].ToString());
                progGrpCreation = dataSet.Tables[0].Rows[0]["PROGGRP_CREATION"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PROGGRP_CREATION"].ToString()) : DateTime.MaxValue;
                progGrpCreatedBy = int.Parse(dataSet.Tables[0].Rows[0]["PROGGRP_CREATEDBY"].ToString());
                progGrpLastUpdate = dataSet.Tables[0].Rows[0]["PROGGRP_LASTUPDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PROGGRP_LASTUPDATE"].ToString()) : DateTime.MaxValue;
                progGrpUpdatedBy = int.Parse(dataSet.Tables[0].Rows[0]["PROGGRP_UPDATEDBY"].ToString());
                progGrpOther = int.Parse(dataSet.Tables[0].Rows[0]["PROGGRP_OTHER"].ToString());
                progGrpShowDesc = int.Parse(dataSet.Tables[0].Rows[0]["PROGGRP_SHOWDESC"].ToString());
                progGrpParent = int.Parse(dataSet.Tables[0].Rows[0]["PROGGRP_PARENT"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean isProgGrpExist(string strProgGrpCode, int intProgGrpId)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_PROGRAMGROUP" +
                     " WHERE PROGGRP_CODE = ?";

            cmd.Parameters.Add("p_PROGGRP_CODE", OdbcType.VarChar, 20).Value = strProgGrpCode;

            if (intProgGrpId > 0)
            {
                strSql += " AND PROGGRP_ID != ?";
                cmd.Parameters.Add("p_PROGGRP_ID", OdbcType.Int, 9).Value = intProgGrpId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExist;
    }

    public Boolean isProgGrpNameExist(string strProgGrpName, int intProgGrpId)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_PROGRAMGROUP" +
                     " WHERE PROGGRP_NAME = ?";

            cmd.Parameters.Add("p_PROGGRP_NAME", OdbcType.VarChar, 250).Value = strProgGrpName;

            if (intProgGrpId > 0)
            {
                strSql += " AND PROGGRP_ID != ?";
                cmd.Parameters.Add("p_PROGGRP_ID", OdbcType.Int, 9).Value = intProgGrpId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExist;
    }

    public Boolean isProgGrpNameZhExist(string strProgGrpName, int intProgGrpId)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_PROGRAMGROUP" +
                     " WHERE PROGGRP_NAME_ZH = ?";

            cmd.Parameters.Add("p_PROGGRP_NAME_ZH", OdbcType.VarChar, 250).Value = strProgGrpName;

            if (intProgGrpId > 0)
            {
                strSql += " AND PROGGRP_ID != ?";
                cmd.Parameters.Add("p_PROGGRP_ID", OdbcType.Int, 9).Value = intProgGrpId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExist;
    }

    public int deleteProgGroup(int intProgGrpId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PROGRAMGROUP WHERE PROGGRP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROGGRP_ID", OdbcType.BigInt, 9);
            cmd.Parameters["p_PROGGRP_ID"].Value = intProgGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion

    #region "Program Group"
    public DataSet getProgGrpList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROGGRP_ID, A.PROGGRP_CODE, A.PROGGRP_NAME, A.PROGGRP_DNAME, A.PROGGRP_DESC, A.PROGGRP_SNAPSHOT," +
                     " A.PROGGRP_IMAGE, A.PROGGRP_SHOWTOP, A.PROGGRP_ACTIVE, A.PROGGRP_ORDER, A.PROGGRP_CREATION, A.PROGGRP_CREATEDBY," +
                     " A.PROGGRP_LASTUPDATE, A.PROGGRP_UPDATEDBY, A.PROGGRP_OTHER, A.PROGGRP_SHOWDESC, A.PROGGRP_PARENT" +
                     " FROM TB_PROGRAMGROUP A";

            if (intActive != 0)
            {
                strSql += " WHERE A.PROGGRP_ACTIVE = ?";
                cmd.Parameters.Add("p_PROGGRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getProgGroupListById()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROG_ID, A.GRP_ID, B.PROGGRP_DNAME" +
                     " FROM (TB_PROGGROUP A LEFT JOIN TB_PROGRAMGROUP B ON (A.GRP_ID = B.PROGGRP_ID))";// +
                     //" WHERE A.PROG_ID = ?";

            //cmd.Parameters.Add("p_PROG_ID", OdbcType.BigInt, 9).Value = intProgId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    #endregion
}
