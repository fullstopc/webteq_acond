﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;

public class clsGroup : dbconnBase
{
    #region "Properties"
    private int _grpId;
    private string _grpCode;
    private string _grpName;
    private string _grpNameJp;
    private string _grpNameMs;
    private string _grpNameZh;
    private string _grpDName;
    private string _grpDNameJp;
    private string _grpDNameMs;
    private string _grpDNameZh;
    private string _grpPageTitleFriendlyUrl;
    private string _grpDesc;
    private string _grpSnapshot;
    private int _grpingId;
    private string _grpingName;
    private int _grpingOrder;
    private int _grpingActive;
    private string _grpImage;
    private int _grpShowTop;
    private int _grpActive;
    private int _grpShowDesc;
    private string _grpPageTitleFURL;
    private int _grpOrder;
    private DateTime _grpCreation;
    private int _grpCreatedBy;
    private DateTime _grpLastUpdate;
    private int _grpUpdatedBy;
    private int _delete; //not in use
    private DateTime _deletion; //not in use
    private int _deletedBy; //not in use
    private int _otherGrpId;
    private int _parentId;
    private string _parentName;

    #region "HTM Pharmacy"
    private int _promo;
    private decimal _promoPrice;
    private DateTime _promoStart;
    private DateTime _promoEnd;
    #endregion
    #endregion

    #region "Property Methods"
    public int grpId
    {
        get { return _grpId; }
        set { _grpId = value; }
    }

    public string grpCode
    {
        get { return _grpCode; }
        set { _grpCode = value; }
    }

    public string grpName
    {
        get { return _grpName; }
        set { _grpName = value; }
    }

    public string grpNameJp
    {
        get { return _grpNameJp; }
        set { _grpNameJp = value; }
    }

    public string grpNameMs
    {
        get { return _grpNameMs; }
        set { _grpNameMs = value; }
    }

    public string grpNameZh
    {
        get { return _grpNameZh; }
        set { _grpNameZh = value; }
    }

    public string grpDName
    {
        get { return _grpDName; }
        set { _grpDName = value; }
    }

    public string grpDNameJp
    {
        get { return _grpDNameJp; }
        set { _grpDNameJp = value; }
    }

    public string grpDNameMs
    {
        get { return _grpDNameMs; }
        set { _grpDNameMs = value; }
    }

    public string grpDNameZh
    {
        get { return _grpDNameZh; }
        set { _grpDNameZh = value; }
    }

    public string grpPageTitleFriendlyUrl
    {
        get { return _grpPageTitleFriendlyUrl; }
        set { _grpPageTitleFriendlyUrl = value; }
    }

    public string grpDesc
    {
        get { return _grpDesc; }
        set { _grpDesc = value; }
    }

    public string grpSnapshot
    {
        get { return _grpSnapshot; }
        set { _grpSnapshot = value; }
    }

    public int grpingId
    {
        get { return _grpingId; }
        set { _grpingId = value; }
    }

    public string grpingName
    {
        get { return _grpingName; }
        set { _grpingName = value; }
    }

    public int grpingOrder
    {
        get { return _grpingOrder; }
        set { _grpingOrder = value; }
    }

    public int grpingActive
    {
        get { return _grpingActive; }
        set { _grpingActive = value; }
    }

    public string grpImage
    {
        get { return _grpImage; }
        set { _grpImage = value; }
    }

    public int grpShowTop
    {
        get { return _grpShowTop; }
        set { _grpShowTop = value; }
    }

    public int grpActive
    {
        get { return _grpActive; }
        set { _grpActive = value; }
    }

    public int grpShowDesc
    {
        get { return _grpShowDesc; }
        set { _grpShowDesc = value; }
    }

    public string grpPageTitleFURL
    {
        get { return _grpPageTitleFURL; }
        set { _grpPageTitleFURL = value; }
    }
    
    public int grpOrder
    {
        get { return _grpOrder; }
        set { _grpOrder = value; }
    }

    public DateTime grpCreation
    {
        get { return _grpCreation; }
        set { _grpCreation = value; }
    }

    public int grpCreatedBy
    {
        get { return _grpCreatedBy; }
        set { _grpCreatedBy = value; }
    }

    public DateTime grpLastUpdate
    {
        get { return _grpLastUpdate; }
        set { _grpLastUpdate = value; }
    }

    public int grpUpdatedBy
    {
        get { return _grpUpdatedBy; }
        set { _grpUpdatedBy = value; }
    }

    public int delete //not in use
    {
        get { return _delete; }
        set { _delete = value; }
    }

    public DateTime deletion //not in use
    {
        get { return _deletion; }
        set { _deletion = value; }
    }

    public int deletedBy //not in use
    {
        get { return _deletedBy; }
        set { _deletedBy = value; }
    }

    public int otherGrpId
    {
        get 
        {                
            if (HttpContext.Current.Cache["OTHERGRPID" + grpingId] as string == null || HttpContext.Current.Cache["OTHERGRPID" + grpingId] as string == "")
            {
                HttpContext.Current.Cache["OTHERGRPID" + grpingId] = getOtherGrpIdByGrpingId();             
            }
            else
            {
                HttpContext.Current.Cache["OTHERGRPID" + grpingId] = -1;                    
            }
            return Convert.ToInt16(HttpContext.Current.Cache["OTHERGRPID" + grpingId]); 
        }
    }

    public int parentId
    {
        get { return _parentId; }
        set { _parentId = value; }
    }

    public string parentName
    {
        get { return _parentName; }
        set { _parentName = value; }
    }

    #region "HTM Pharmacy"
    public int promo
    {
        get { return _promo; }
        set { _promo = value; }
    }
    public decimal promoPrice
    {
        get { return _promoPrice; }
        set { _promoPrice = value; }
    }
    public DateTime promoStart
    {
        get { return _promoStart; }
        set { _promoStart = value; }
    }
    public DateTime promoEnd
    {
        get { return _promoEnd; }
        set { _promoEnd = value; }
    }
    #endregion
    #endregion

    #region "Public Methods"
    public clsGroup()
	{
	}

    public int addGroup(string strGrpCode, string strGrpName, string strGrpDName, string strGrpDesc, string strGrpSnapshot, int intGrpingId, string strGrpImg, int intGrpShowTop, int intGrpActive, int intGrpCreatedBy)    
    {
        int intRecordAffected = 0;

        try
        {
            int intGrpOrder = generateGroupOrderNo();

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);
              
            strSql = "INSERT INTO TB_GROUP (" +
                            "GRP_CODE, " +
                            "GRP_NAME, " +
                            "GRP_DNAME, " +
                            "GRP_DESC, " +
                            "GRP_SNAPSHOT, " +
                            "GRPING_ID, " +
                            "GRP_IMAGE, " +
                            "GRP_SHOWTOP, " +
                            "GRP_ACTIVE, " +
                            "GRP_ORDER, " +
                            "GRP_CREATEDBY, " +
                            "GRP_CREATION " +
                            ") VALUES " +
                           "(?,?,?,?,?,?,?,?,?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_CODE", OdbcType.VarChar, 20).Value = strGrpCode;
            cmd.Parameters.Add("p_GRP_NAME", OdbcType.VarChar, 250).Value = strGrpName;
            cmd.Parameters.Add("p_GRP_DNAME", OdbcType.VarChar, 250).Value = strGrpDName;
            cmd.Parameters.Add("p_GRP_DESC", OdbcType.Text).Value = strGrpDesc;
            cmd.Parameters.Add("p_GRP_SNAPSHOT", OdbcType.VarChar, 1000).Value = strGrpSnapshot;
            cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;
            cmd.Parameters.Add("p_GRP_IMAGE", OdbcType.VarChar, 250).Value = strGrpImg;
            cmd.Parameters.Add("p_GRP_SHOWTOP", OdbcType.Int, 1).Value = intGrpShowTop;
            cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intGrpActive;
            cmd.Parameters.Add("p_GRP_ORDER", OdbcType.Int, 9).Value = intGrpOrder;
            cmd.Parameters.Add("p_GRP_CREATEDBY", OdbcType.Int, 9).Value = intGrpCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                grpId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addGroup2(string strGrpCode, string strGrpName, string strGrpNameJp, string strGrpNameMs, string strGrpNameZh, string strGrpDName, string strGrpDNameJp, string strGrpDNameMs, string strGrpDNameZh,
                         string strGrpPageTitleFriendlyUrl, int intGrpingId, string strGrpImg, int intGrpShowTop, int intGrpActive, int intGrpParent, int intGrpCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            int intGrpOrder = generateGroupOrderNo(intGrpParent, intGrpingId);

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_GROUP (" +
                     "GRP_CODE, " +
                     "GRP_NAME, " +
                     "GRP_NAME_JP, " +
                     "GRP_NAME_MS, " +
                     "GRP_NAME_ZH, " +
                     "GRP_DNAME, " +
                     "GRP_DNAME_JP, " +
                     "GRP_DNAME_MS, " +
                     "GRP_DNAME_ZH, " +
                     "GRP_PAGETITLEFURL, " +
                     "GRPING_ID, " +
                     "GRP_IMAGE, " +
                     "GRP_SHOWTOP, " +
                     "GRP_ACTIVE, " +
                     "GRP_ORDER, " +
                     "GRP_PARENT," +
                     "GRP_CREATEDBY, " +
                     "GRP_CREATION " +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_CODE", OdbcType.VarChar, 20).Value = strGrpCode;
            cmd.Parameters.Add("p_GRP_NAME", OdbcType.VarChar, 250).Value = strGrpName;
            cmd.Parameters.Add("p_GRP_NAME_JP", OdbcType.VarChar, 250).Value = strGrpNameJp;
            cmd.Parameters.Add("p_GRP_NAME_MS", OdbcType.VarChar, 250).Value = strGrpNameMs;
            cmd.Parameters.Add("p_GRP_NAME_ZH", OdbcType.VarChar, 250).Value = strGrpNameZh;
            cmd.Parameters.Add("p_GRP_DNAME", OdbcType.VarChar, 250).Value = strGrpDName;
            cmd.Parameters.Add("p_GRP_DNAME_JP", OdbcType.VarChar, 250).Value = strGrpDNameJp;
            cmd.Parameters.Add("p_GRP_DNAME_MS", OdbcType.VarChar, 250).Value = strGrpDNameMs;
            cmd.Parameters.Add("p_GRP_DNAME_ZH", OdbcType.VarChar, 250).Value = strGrpDNameZh;
            cmd.Parameters.Add("p_GRP_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strGrpPageTitleFriendlyUrl;
            cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;
            cmd.Parameters.Add("p_GRP_IMAGE", OdbcType.VarChar, 250).Value = strGrpImg;
            cmd.Parameters.Add("p_GRP_SHOWTOP", OdbcType.Int, 1).Value = intGrpShowTop;
            cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intGrpActive;
            cmd.Parameters.Add("p_GRP_ORDER", OdbcType.Int, 9).Value = intGrpOrder;
            cmd.Parameters.Add("p_GRP_PARENT", OdbcType.Int, 9).Value = intGrpParent;
            cmd.Parameters.Add("p_GRP_CREATEDBY", OdbcType.Int, 9).Value = intGrpCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                grpId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int generateGroupOrderNo()
    {
        int grpOrderNo = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT GRP_ORDER FROM TB_GROUP ORDER BY GRP_ORDER DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            grpOrderNo = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return grpOrderNo + 1;
    }

    public int generateGroupOrderNo(int intGrpParent, int intGrpingId)
    {
        int grpOrderNo = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT GRP_ORDER FROM TB_GROUP WHERE GRP_PARENT = ? AND GRPING_ID = ? ORDER BY GRP_ORDER DESC LIMIT 1";

            cmd.Parameters.Add("p_GRP_PARENT", OdbcType.Int, 9).Value = intGrpParent;
            cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            grpOrderNo = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return grpOrderNo + 1;
    }

    public Boolean isGrpCodeExist(string strGrpCode, int intGrpingId, int intGrpId)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_GROUP" +
                " WHERE GRP_CODE = ?" +
                " AND GRPING_ID = ?";

            cmd.Parameters.Add("p_GRP_CODE", OdbcType.VarChar, 20).Value = strGrpCode;
            cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;

            if (intGrpId > 0)
            { 
                strSql += " AND GRP_ID != ?";
                cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExist;
    }

    public Boolean isGrpNameExist(string strGrpName, int intGrpingId, int intGrpId)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_GROUP" +
                " WHERE GRP_NAME = ?" +
                " AND GRPING_ID = ?";

            cmd.Parameters.Add("p_GRP_NAME", OdbcType.VarChar, 250).Value = strGrpName;
            cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;
            
            if (intGrpId > 0)
            {
                strSql += " AND GRP_ID != ?";
                cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExist;
    }

    public Boolean isGrpNameJpExist(string strGrpName, int intGrpingId, int intGrpId)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_GROUP" +
                " WHERE GRP_NAME_JP = ?" +
                " AND GRPING_ID = ?";

            cmd.Parameters.Add("p_GRP_NAME_JP", OdbcType.VarChar, 250).Value = strGrpName;
            cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;

            if (intGrpId > 0)
            {
                strSql += " AND GRP_ID != ?";
                cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExist;
    }

    public Boolean isGrpNameMsExist(string strGrpName, int intGrpingId, int intGrpId)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_GROUP" +
                " WHERE GRP_NAME_MS = ?" +
                " AND GRPING_ID = ?";

            cmd.Parameters.Add("p_GRP_NAME_MS", OdbcType.VarChar, 250).Value = strGrpName;
            cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;

            if (intGrpId > 0)
            {
                strSql += " AND GRP_ID != ?";
                cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExist;
    }

    public Boolean isGrpNameZhExist(string strGrpName, int intGrpingId, int intGrpId)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_GROUP" +
                " WHERE GRP_NAME_ZH = ?" +
                " AND GRPING_ID = ?";

            cmd.Parameters.Add("p_GRP_NAME_ZH", OdbcType.VarChar, 250).Value = strGrpName;
            cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;

            if (intGrpId > 0)
            {
                strSql += " AND GRP_ID != ?";
                cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExist;
    }

    public Boolean isGrpPageTitleFriendlyUrlExist(string strGrpPageTitleFriendlyUrl, int intGrpingId, int intGrpId)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_GROUP" +
                     " WHERE GRP_PAGETITLEFURL = ?" +
                     " AND GRPING_ID = ?";

            cmd.Parameters.Add("p_GRP_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strGrpPageTitleFriendlyUrl;
            cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;

            if (intGrpId > 0)
            {
                strSql += " AND GRP_ID != ?";
                cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExist;
    }

    public int getFirstGroupId()
    {
        int intGrpId = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT GRP_ID FROM TB_GROUP WHERE GRP_ACTIVE = 1 ORDER BY GRP_DNAME ASC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intGrpId = Convert.ToInt16(cmd.ExecuteScalar());

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intGrpId;
    }

    public int getProdTotalByGrpId(int intGrpId)
    {
        int intTotal = 0;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT SUM(A.V_NOP) AS V_NOP" +
                     " FROM VW_GROUP A" +
                     " WHERE A.GRP_ID = ?";
                     //" AND (A.PROD_COUNTRY IS NULL OR A.PROD_COUNTRY = 0 OR A.PROD_COUNTRY = ?)";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;
            //cmd.Parameters.Add("p_PROD_COUNTRY", OdbcType.Int, 9).Value = intCountryId;

            try
            {
                intTotal = Convert.ToInt16(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                intTotal = 0;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intTotal;
    }

    public Boolean isExactSameGrp(int intGrpId, string strGrpCode, string strGrpName, string strGrpNameJp, string strGrpNameMs, string strGrpNameZh, string strGrpDName, string strGrpDNameJp, string strGrpDNameMs, string strGrpDNameZh,
                                  string strGrpPageTitleFriendlyUrl, string strGrpDesc, string strGrpSnapshot, int intGrpingId, string strGrpImg, int intGrpShowTop, int intGrpActive)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_GROUP" +
                " WHERE GRP_ID = ?" +
                " AND BINARY GRP_CODE = ?" +
                " AND BINARY GRP_NAME = ?" +
                " AND BINARY GRP_NAME_JP = ?" +
                " AND BINARY GRP_NAME_MS = ?" +
                " AND BINARY GRP_NAME_ZH = ?" +
                " AND BINARY GRP_DNAME = ?" +
                " AND BINARY GRP_DNAME_JP = ?" +
                " AND BINARY GRP_DNAME_MS = ?" +
                " AND BINARY GRP_DNAME_ZH = ?" +
                " AND BINARY GRP_PAGETITLEFURL = ?" +
                " AND BINARY GRP_DESC = ?" +
                " AND BINARY GRP_SNAPSHOT = ?" +
                " AND GRPING_ID = ?" +
                " AND BINARY GRP_IMAGE = ?" +
                " AND GRP_SHOWTOP = ?" +
                " AND GRP_ACTIVE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;
            cmd.Parameters.Add("p_GRP_CODE", OdbcType.VarChar, 20).Value = strGrpCode;
            cmd.Parameters.Add("p_GRP_NAME", OdbcType.VarChar, 250).Value = strGrpName;
            cmd.Parameters.Add("p_GRP_NAME_JP", OdbcType.VarChar, 250).Value = strGrpNameJp;
            cmd.Parameters.Add("p_GRP_NAME_MS", OdbcType.VarChar, 250).Value = strGrpNameMs;
            cmd.Parameters.Add("p_GRP_NAME_ZH", OdbcType.VarChar, 250).Value = strGrpNameZh;
            cmd.Parameters.Add("p_GRP_DNAME", OdbcType.VarChar, 250).Value = strGrpDName;
            cmd.Parameters.Add("p_GRP_DNAME_JP", OdbcType.VarChar, 250).Value = strGrpDNameJp;
            cmd.Parameters.Add("p_GRP_DNAME_MS", OdbcType.VarChar, 250).Value = strGrpDNameMs;
            cmd.Parameters.Add("p_GRP_DNAME_ZH", OdbcType.VarChar, 250).Value = strGrpDNameZh;
            cmd.Parameters.Add("p_GRP_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strGrpPageTitleFriendlyUrl;
            cmd.Parameters.Add("p_GRP_DESC", OdbcType.Text).Value = strGrpDesc;
            cmd.Parameters.Add("p_GRP_SNAPSHOT", OdbcType.VarChar, 1000).Value = strGrpSnapshot;
            cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;
            cmd.Parameters.Add("p_GRP_IMAGE", OdbcType.VarChar, 250).Value = strGrpImg;
            cmd.Parameters.Add("p_GRP_SHOWTOP", OdbcType.Int, 1).Value = intGrpShowTop;
            cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intGrpActive;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public Boolean isExactSameGrp2(int intGrpId, string strGrpCode, string strGrpName, string strGrpNameJp, string strGrpNameMs, string strGrpNameZh, string strGrpDName, string strGrpDNameJp, string strGrpDNameMs, string strGrpDNameZh,
                                   string strGrpPageTitleFriendlyUrl, int intGrpingId, string strGrpImg, int intGrpShowTop, int intGrpActive)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_GROUP" +
                " WHERE GRP_ID = ?" +
                " AND BINARY GRP_CODE = ?" +
                " AND BINARY GRP_NAME = ?" +
                " AND BINARY GRP_NAME_JP = ?" +
                " AND BINARY GRP_NAME_MS = ?" +
                " AND BINARY GRP_NAME_ZH = ?" +
                " AND BINARY GRP_DNAME = ?" +
                " AND BINARY GRP_DNAME_JP = ?" +
                " AND BINARY GRP_DNAME_MS = ?" +
                " AND BINARY GRP_DNAME_ZH = ?" +
                " AND BINARY GRP_PAGETITLEFURL = ?" +
                " AND GRPING_ID = ?" +
                " AND BINARY GRP_IMAGE = ?" +
                " AND GRP_SHOWTOP = ?" +
                " AND GRP_ACTIVE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;
            cmd.Parameters.Add("p_GRP_CODE", OdbcType.VarChar, 20).Value = strGrpCode;
            cmd.Parameters.Add("p_GRP_NAME", OdbcType.VarChar, 250).Value = strGrpName;
            cmd.Parameters.Add("p_GRP_NAME_JP", OdbcType.VarChar, 250).Value = strGrpNameJp;
            cmd.Parameters.Add("p_GRP_NAME_MS", OdbcType.VarChar, 250).Value = strGrpNameMs;
            cmd.Parameters.Add("p_GRP_NAME_ZH", OdbcType.VarChar, 250).Value = strGrpNameZh;
            cmd.Parameters.Add("p_GRP_DNAME", OdbcType.VarChar, 250).Value = strGrpDName;
            cmd.Parameters.Add("p_GRP_DNAME_JP", OdbcType.VarChar, 250).Value = strGrpDNameJp;
            cmd.Parameters.Add("p_GRP_DNAME_MS", OdbcType.VarChar, 250).Value = strGrpDNameMs;
            cmd.Parameters.Add("p_GRP_DNAME_ZH", OdbcType.VarChar, 250).Value = strGrpDNameZh;
            cmd.Parameters.Add("p_GRP_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strGrpPageTitleFriendlyUrl;
            cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;
            cmd.Parameters.Add("p_GRP_IMAGE", OdbcType.VarChar, 250).Value = strGrpImg;
            cmd.Parameters.Add("p_GRP_SHOWTOP", OdbcType.Int, 1).Value = intGrpShowTop;
            cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intGrpActive;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public Boolean isExactSameGrpSet(int intGrpId, string strGrpDesc)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_GROUP" +
                     " WHERE GRP_ID = ?" +
                     " AND GRP_DESC = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intGrpId;
            cmd.Parameters.Add("p_GRP_DESC", OdbcType.Text).Value = strGrpDesc;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public Boolean extractGrpById(int intGrpId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.GRP_ID, A.GRP_CODE, A.GRP_NAME, A.GRP_NAME_JP, A.GRP_DNAME, A.GRP_DNAME_JP, A.GRP_PAGETITLEFURL, A.GRP_DESC, A.GRP_SNAPSHOT, A.GRPING_ID, A.GRP_IMAGE, A.GRP_SHOWTOP," +
                     " A.GRP_NAME_MS, A.GRP_NAME_ZH, A.GRP_DNAME_MS, A.GRP_DNAME_ZH, A.GRP_ACTIVE, A.GRP_SHOWDESC,A.GRP_PAGETITLEFURL, B.LIST_NAME" +
                     " FROM TB_GROUP A, TB_LIST B" +
                     " WHERE A.GRP_ID = ?" +
                     " AND A.GRPING_ID = B.LIST_VALUE" +
                     " AND B.LIST_GROUP = 'GROUPING'";

            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;

            if (intActive != 0)
            {
                strSql += " AND A.GRP_ACTIVE = ?";
                cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                grpId = int.Parse(dataSet.Tables[0].Rows[0]["GRP_ID"].ToString());
                grpCode = dataSet.Tables[0].Rows[0]["GRP_CODE"].ToString();
                grpName = dataSet.Tables[0].Rows[0]["GRP_NAME"].ToString();
                grpNameJp = dataSet.Tables[0].Rows[0]["GRP_NAME_JP"].ToString();
                grpNameMs = dataSet.Tables[0].Rows[0]["GRP_NAME_MS"].ToString();
                grpNameZh = dataSet.Tables[0].Rows[0]["GRP_NAME_ZH"].ToString();
                grpDName = dataSet.Tables[0].Rows[0]["GRP_DNAME"].ToString();
                grpDNameJp = dataSet.Tables[0].Rows[0]["GRP_DNAME_JP"].ToString();
                grpDNameMs = dataSet.Tables[0].Rows[0]["GRP_DNAME_MS"].ToString();
                grpDNameZh = dataSet.Tables[0].Rows[0]["GRP_DNAME_ZH"].ToString();
                grpPageTitleFriendlyUrl = dataSet.Tables[0].Rows[0]["GRP_PAGETITLEFURL"].ToString();
                grpDesc = dataSet.Tables[0].Rows[0]["GRP_DESC"].ToString();
                grpSnapshot = dataSet.Tables[0].Rows[0]["GRP_SNAPSHOT"].ToString();
                grpingId = int.Parse(dataSet.Tables[0].Rows[0]["GRPING_ID"].ToString());
                grpingName = dataSet.Tables[0].Rows[0]["LIST_NAME"].ToString();
                grpImage = dataSet.Tables[0].Rows[0]["GRP_IMAGE"].ToString();
                grpShowTop = int.Parse(dataSet.Tables[0].Rows[0]["GRP_SHOWTOP"].ToString());
                grpActive = int.Parse(dataSet.Tables[0].Rows[0]["GRP_ACTIVE"].ToString());
                grpShowDesc = int.Parse(dataSet.Tables[0].Rows[0]["GRP_SHOWDESC"].ToString());
                grpPageTitleFURL = dataSet.Tables[0].Rows[0]["GRP_PAGETITLEFURL"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractGrpById2(int intGrpId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.GRP_ID, A.GRP_CODE, A.GRP_NAME, A.GRP_DNAME, A.GRP_SNAPSHOT, A.GRP_DESC, A.GRPING_ID, A.GRP_IMAGE, A.GRP_SHOWTOP," +
                     " A.GRP_ACTIVE, A.GRP_ORDER, A.GRP_CREATEDBY, A.GRP_CREATION, A.GRP_UPDATEDBY, A.GRP_LASTUPDATE, " +
                     " A.GRP_PARENT, B.LIST_NAME" +
                     " FROM TB_GROUP A, TB_LIST B" +
                     " WHERE A.GRP_ID = ?" +
                     " AND A.GRPING_ID = B.LIST_VALUE" +
                     " AND B.LIST_GROUP = 'GROUPING'";

            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;

            if (intActive != 0)
            {
                strSql += " AND A.GRP_ACTIVE = ?";
                cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                grpId = int.Parse(dataSet.Tables[0].Rows[0]["GRP_ID"].ToString());
                grpCode = dataSet.Tables[0].Rows[0]["GRP_CODE"].ToString();
                grpName = dataSet.Tables[0].Rows[0]["GRP_NAME"].ToString();
                grpDName = dataSet.Tables[0].Rows[0]["GRP_DNAME"].ToString();
                grpingId = int.Parse(dataSet.Tables[0].Rows[0]["GRPING_ID"].ToString());
                grpingName = dataSet.Tables[0].Rows[0]["LIST_NAME"].ToString();
                grpImage = dataSet.Tables[0].Rows[0]["GRP_IMAGE"].ToString();
                grpShowTop = int.Parse(dataSet.Tables[0].Rows[0]["GRP_SHOWTOP"].ToString());
                grpActive = int.Parse(dataSet.Tables[0].Rows[0]["GRP_ACTIVE"].ToString());
                grpOrder = int.Parse(dataSet.Tables[0].Rows[0]["GRP_ORDER"].ToString());
                grpSnapshot = dataSet.Tables[0].Rows[0]["GRP_SNAPSHOT"].ToString();
                grpDesc = dataSet.Tables[0].Rows[0]["GRP_DESC"].ToString();
                grpCreatedBy = int.Parse(dataSet.Tables[0].Rows[0]["GRP_CREATEDBY"].ToString());
                grpCreation = dataSet.Tables[0].Rows[0]["GRP_CREATION"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["GRP_CREATION"].ToString()) : DateTime.MaxValue;
                grpUpdatedBy = int.Parse(dataSet.Tables[0].Rows[0]["GRP_UPDATEDBY"].ToString());
                grpLastUpdate = dataSet.Tables[0].Rows[0]["GRP_LASTUPDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["GRP_LASTUPDATE"].ToString()) : DateTime.MaxValue;
                //grpOther = Convert.ToInt16(dataSet.Tables[0].Rows[0]["GRP_OTHER"]);
                parentId = Convert.ToInt16(dataSet.Tables[0].Rows[0]["GRP_PARENT"]);
            }

        }

        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractPrevGroup(int intOrder, int intParent, int intGrpingId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_GROUP WHERE GRP_PARENT = ? AND GRPING_ID = ? AND GRP_ORDER < ?";

            cmd.Parameters.Add("p_GRP_PARENT", OdbcType.Int, 9).Value = intParent;
            cmd.Parameters.Add("p_GRPING_ID", OdbcType.BigInt, 9).Value = intGrpingId;
            cmd.Parameters.Add("p_GRP_ORDER", OdbcType.Int, 9).Value = intOrder;

            if (intActive != 0)
            {
                strSql += " AND GRP_ACTIVE = ?";
                cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " ORDER BY GRP_ORDER DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                grpId = Convert.ToInt16(dataSet.Tables[0].Rows[0]["GRP_ID"]);
                grpName = dataSet.Tables[0].Rows[0]["GRP_NAME"].ToString();
                grpOrder = Convert.ToInt16(dataSet.Tables[0].Rows[0]["GRP_ORDER"]);
            }

        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractNextGroup(int intOrder, int intParent, int intGrpingId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_GROUP WHERE GRP_PARENT = ? AND GRPING_ID = ? AND GRP_ORDER > ?";

            cmd.Parameters.Add("p_GRP_PARENT", OdbcType.Int, 9).Value = intParent;
            cmd.Parameters.Add("p_GRPING_ID", OdbcType.BigInt, 9).Value = intGrpingId;
            cmd.Parameters.Add("p_GRP_ORDER", OdbcType.Int, 9).Value = intOrder;

            if (intActive != 0)
            {
                strSql += " AND GRP_ACTIVE = ?";
                cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " ORDER BY GRP_ORDER ASC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                grpId = Convert.ToInt16(dataSet.Tables[0].Rows[0]["GRP_ID"]);
                grpName = dataSet.Tables[0].Rows[0]["GRP_NAME"].ToString();
                grpOrder = Convert.ToInt16(dataSet.Tables[0].Rows[0]["GRP_ORDER"]);
            }

        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int updateGrpOrderById(int intId, int intOrder, int intParent)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_GROUP SET" +
                     " GRP_ORDER = ?" +
                     " WHERE GRP_PARENT = ? AND GRP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_ORDER", OdbcType.BigInt, 9).Value = intOrder;
            cmd.Parameters.Add("p_GRP_PARENT", OdbcType.BigInt, 9).Value = intParent;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intId;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getGrpListByGrpingId(int intGrpingId, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.GRP_ID, A.GRP_CODE, A.GRP_NAME, A.GRP_NAME_JP, A.GRP_DNAME, A.GRP_DNAME_JP, A.GRPING_ID, A.GRP_SHOWTOP, A.GRP_ACTIVE," +
                     " A.GRP_NAME_ZH, A.GRP_NAME_MS, A.GRP_DNAME_ZH, A.GRP_DNAME_MS, A.GRP_PARENT" +
                     " FROM TB_GROUP A" +
                     " WHERE 1 = 1";

            if (intGrpingId != 0)
            {
                strSql += " AND A.GRPING_ID = ?";
                cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;
            }

            if (intActive != 0)
            {
                strSql += " AND A.GRP_ACTIVE = ?";
                cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }
            
            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getGrpListByGrpingId2(int intGrpingId, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.GRP_ID, A.GRP_CODE, A.GRP_NAME, A.GRP_DNAME, A.GRP_NAME_JP, A.GRP_DNAME, A.GRP_DNAME_JP, A.GRPING_ID, A.GRP_SNAPSHOT, A.GRP_DESC, A.GRPING_ID, A.GRP_IMAGE, A.GRP_SHOWTOP," +
                     " A.GRP_NAME_ZH, A.GRP_NAME_MS, A.GRP_DNAME_ZH, A.GRP_DNAME_MS, A.GRP_ACTIVE, A.GRP_ORDER, A.GRP_CREATEDBY, A.GRP_CREATION, A.GRP_UPDATEDBY, A.GRP_LASTUPDATE, A.GRP_OTHER, A.GRP_PARENT" +
                     " FROM TB_GROUP A, TB_LIST B" +
                     " WHERE A.GRPING_ID = B.LIST_VALUE" +
                     " AND B.LIST_GROUP = 'GROUPING'";
            //strSql = "SELECT A.GRP_ID, A.GRP_NAME, A.GRPING_ID" +
            //         " FROM TB_GROUP A" +
            //         " WHERE 1 = 1";

            if (intGrpingId != 0)
            {
                strSql += " AND A.GRPING_ID = ?";
                cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;
            }

            if (intActive != 0)
            {
                strSql += " AND A.GRP_ACTIVE = ?";
                cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    
    public string getGrpIdsByGrpingId(int intGrpingId, int intActive)
    {
        string strIds = "";

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT GROUP_CONCAT(CAST(A.GRP_ID AS CHAR))" +
                     " FROM TB_GROUP A" +
                     " WHERE 1 = 1";

            if (intGrpingId != 0)
            {
                strSql += " AND A.GRPING_ID = ?";
                cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;
            }

            if (intActive != 0)
            {
                strSql += " AND A.GRP_ACTIVE = ?";
                cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            strIds = cmd.ExecuteScalar().ToString();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return strIds;
    }

    public int deleteGroup(int intGrpId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_GROUP WHERE GRP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_ID", OdbcType.VarChar, 10);
            cmd.Parameters["p_GRP_ID"].Value = intGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateGroupById(int intGrpId, string strGrpCode, string strGrpName, string strGrpNameJp, string strGrpNameMs, string strGrpNameZh, string strGrpDName, string strGrpDNameJp, string strGrpDNameMs, string strGrpDNameZh,
                               string strGrpPageTitleFriendlyUrl, string strGrpDesc, string strGrpSnapshot, string strGrpImg, int intGrpShowTop, int intGrpActive, int intGrpUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_GROUP SET" +
                     " GRP_CODE = ?," +
                     " GRP_NAME = ?," +
                     " GRP_NAME_JP = ?," +
                     " GRP_NAME_MS = ?," +
                     " GRP_NAME_ZH = ?," +
                     " GRP_DNAME = ?," +
                     " GRP_DNAME_JP = ?," +
                     " GRP_DNAME_MS = ?," +
                     " GRP_DNAME_ZH = ?," +
                     " GRP_PAGETITLEFURL = ?," +
                     " GRP_DESC = ?," +
                     " GRP_SNAPSHOT = ?," +
                     " GRP_IMAGE = ?," +
                     " GRP_SHOWTOP = ?," +
                     " GRP_ACTIVE = ?," +
                     " GRP_UPDATEDBY = ?," +
                     " GRP_LASTUPDATE = SYSDATE()" +
                     " WHERE GRP_ID = ?";                           

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_CODE", OdbcType.VarChar, 20).Value = strGrpCode;
            cmd.Parameters.Add("p_GRP_NAME", OdbcType.VarChar, 250);
            cmd.Parameters["p_GRP_NAME"].Value = strGrpName;
            cmd.Parameters.Add("p_GRP_NAME_JP", OdbcType.VarChar, 250);
            cmd.Parameters["p_GRP_NAME_JP"].Value = strGrpNameJp;
            cmd.Parameters.Add("p_GRP_NAME_MS", OdbcType.VarChar, 250).Value = strGrpNameMs;
            cmd.Parameters.Add("p_GRP_NAME_ZH", OdbcType.VarChar, 250).Value = strGrpNameZh;
            cmd.Parameters.Add("p_GRP_DNAME", OdbcType.VarChar, 250);
            cmd.Parameters["p_GRP_DNAME"].Value = strGrpDName;
            cmd.Parameters.Add("p_GRP_DNAME_JP", OdbcType.VarChar, 250);
            cmd.Parameters["p_GRP_DNAME_JP"].Value = strGrpDNameJp;
            cmd.Parameters.Add("p_GRP_DNAME_MS", OdbcType.VarChar, 250).Value = strGrpDNameMs;
            cmd.Parameters.Add("p_GRP_DNAME_ZH", OdbcType.VarChar, 250).Value = strGrpDNameZh;
            cmd.Parameters.Add("p_GRP_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strGrpPageTitleFriendlyUrl;
            cmd.Parameters.Add("p_GRP_DESC", OdbcType.Text);
            cmd.Parameters["p_GRP_DESC"].Value = strGrpDesc;
            cmd.Parameters.Add("p_GRP_SNAPSHOT", OdbcType.VarChar, 1000);
            cmd.Parameters["p_GRP_SNAPSHOT"].Value = strGrpSnapshot;
            cmd.Parameters.Add("p_GRP_IMAGE", OdbcType.VarChar, 250);
            cmd.Parameters["p_GRP_IMAGE"].Value = strGrpImg;
            cmd.Parameters.Add("p_GRP_SHOWTOP", OdbcType.Int, 1);
            cmd.Parameters["p_GRP_SHOWTOP"].Value = intGrpShowTop;
            cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1);
            cmd.Parameters["p_GRP_ACTIVE"].Value = intGrpActive;
            cmd.Parameters.Add("p_GRP_UPDATEDBY", OdbcType.Int, 9);
            cmd.Parameters["p_GRP_UPDATEDBY"].Value = intGrpUpdatedBy;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9);
            cmd.Parameters["p_GRP_ID"].Value = intGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                grpId = intGrpId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateGroupById2(int intGrpId, string strGrpCode, string strGrpName, string strGrpNameJp, string strGrpNameMs, string strGrpNameZh, string strGrpDName, string strGrpDNameJp, string strGrpDNameMs, string strGrpDNameZh,
                                string strGrpPageTitleFriendlyUrl, string strGrpImg, int intGrpShowTop, int intGrpActive, int intGrpUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_GROUP SET" +
                     " GRP_CODE = ?," +
                     " GRP_NAME = ?," +
                     " GRP_NAME_JP = ?," +
                     " GRP_NAME_MS = ?," +
                     " GRP_NAME_ZH = ?," +
                     " GRP_DNAME = ?," +
                     " GRP_DNAME_JP = ?," +
                     " GRP_DNAME_MS = ?," +
                     " GRP_DNAME_ZH = ?," +
                     " GRP_PAGETITLEFURL = ?," +
                     " GRP_IMAGE = ?," +
                     " GRP_SHOWTOP = ?," +
                     " GRP_ACTIVE = ?," +
                     " GRP_UPDATEDBY = ?," +
                     " GRP_LASTUPDATE = SYSDATE()" +
                     " WHERE GRP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_CODE", OdbcType.VarChar, 20).Value = strGrpCode;
            cmd.Parameters.Add("p_GRP_NAME", OdbcType.VarChar, 250).Value = strGrpName;
            cmd.Parameters.Add("p_GRP_NAME_JP", OdbcType.VarChar, 250).Value = strGrpNameJp;
            cmd.Parameters.Add("p_GRP_NAME_MS", OdbcType.VarChar, 250).Value = strGrpNameMs;
            cmd.Parameters.Add("p_GRP_NAME_ZH", OdbcType.VarChar, 250).Value = strGrpNameZh;
            cmd.Parameters.Add("p_GRP_DNAME", OdbcType.VarChar, 250).Value = strGrpDName;
            cmd.Parameters.Add("p_GRP_DNAME_JP", OdbcType.VarChar, 250).Value = strGrpDNameJp;
            cmd.Parameters.Add("p_GRP_DNAME_MS", OdbcType.VarChar, 250).Value = strGrpDNameMs;
            cmd.Parameters.Add("p_GRP_DNAME_ZH", OdbcType.VarChar, 250).Value = strGrpDNameZh;
            cmd.Parameters.Add("p_GRP_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strGrpPageTitleFriendlyUrl;
            cmd.Parameters.Add("p_GRP_IMAGE", OdbcType.VarChar, 250).Value = strGrpImg;
            cmd.Parameters.Add("p_GRP_SHOWTOP", OdbcType.Int, 1).Value = intGrpShowTop;
            cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intGrpActive;
            cmd.Parameters.Add("p_GRP_UPDATEDBY", OdbcType.Int, 9).Value = intGrpUpdatedBy;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                grpId = intGrpId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateGroupById3(int intGrpId, int intParent)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_GROUP SET" +
                     " GRP_PARENT = ?" +
                     " WHERE GRP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_PARENT", OdbcType.Int, 9).Value = intParent;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                grpId = intGrpId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateGrpDescById(int intGrpId, string strGrpDesc, int intGrpUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_GROUP SET" +
                     " GRP_DESC = ?," +
                     " GRP_LASTUPDATE = SYSDATE()," +
                     " GRP_UPDATEDBY = ?" +
                     " WHERE GRP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_DESC", OdbcType.Text).Value = strGrpDesc;
            cmd.Parameters.Add("p_GRP_UPDATEDBY", OdbcType.Int, 9).Value = intGrpUpdatedBy;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                grpId = intGrpId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    private int getOtherGrpIdByGrpingId()
    {
        int iOtherGrpId = -1;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT A.GRP_ID" +
                     " FROM TB_GROUP A, TB_LIST B" +
                     " WHERE A.GRPING_ID = ?" +
                     " AND A.GRP_OTHER = 1";

            cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = grpingId;
            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            iOtherGrpId = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return iOtherGrpId;    
    }

    public DataSet getGrpingList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT *" +
                     " FROM tb_list A";

            if (intActive != 0)
            {
                strSql += " WHERE LIST_ACTIVE = ?";
                cmd.Parameters.Add("p_LIST_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getGrpList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT *" +
                     " FROM tb_group A";

            if (intActive != 0)
            {
                strSql += " WHERE GRP_ACTIVE = ?";
                cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public int getTotalRecord(int intOther, int intActive)
    {
        int intTotal = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_GROUP A" +
                     " WHERE 1 = 1";

            if (intOther == 0)
            {
                strSql += " AND A.GRP_OTHER = ?";
                cmd.Parameters.Add("p_GRP_OTHER", OdbcType.Int, 1).Value = intOther;
            }

            if (intActive != 0)
            {
                strSql += " AND A.GRP_ACTIVE = ?";
                cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            intTotal = int.Parse(cmd.ExecuteScalar().ToString());
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intTotal;
    }

    public int getGrpNOC(int intGrpingId, int intParent, int intActive)
    {
        int intNOC = 0;

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) AS NOC" +
                     " FROM TB_GROUP A, TB_LIST B" +
                     " WHERE A.GRPING_ID = B.LIST_VALUE" +
                     " AND B.LIST_GROUP = 'GROUPING'" +
                     " AND A.GRPING_ID = ?" +
                     " AND A.GRP_PARENT = ?";

            cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;
            cmd.Parameters.Add("p_GRP_PARENT", OdbcType.Int, 9).Value = intParent;

            if (intActive != 0)
            {
                strSql += " AND A.GRP_ACTIVE = ?";
                cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            intNOC = Convert.ToInt16(cmd.ExecuteScalar());

        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intNOC;
    }
    public int getGrpNOP(int intGrpId, int intActive)
    {
        int intNOC = 0;

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) AS NOP" +
                     " FROM TB_PRODGROUP A, TB_PRODUCT B" +
                     " WHERE A.PROD_ID = B.PROD_ID" +
                     " AND A.GRP_ID = ?";

            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intGrpId;

            if (intActive != 0)
            {
                strSql += " AND B.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            intNOC = Convert.ToInt16(cmd.ExecuteScalar());

        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intNOC;
    }

    #region "Program"
    public DataSet getProgGrpListByGrpingId(int intGrpingId, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROGGRP_ID, A.PROGGRP_CODE, A.PROGGRP_NAME, A.PROGGRP_DNAME, A.PROGGRP_NAME_JP, A.PROGGRP_DNAME, A.PROGGRP_DNAME_JP, A.PROGGRP_SNAPSHOT, A.PROGGRP_DESC, A.PROGGRP_IMAGE, A.PROGGRP_SHOWTOP," +
                     " A.PROGGRP_NAME_ZH, A.PROGGRP_NAME_MS, A.PROGGRP_DNAME_ZH, A.PROGGRP_DNAME_MS, A.PROGGRP_ACTIVE, A.PROGGRP_ORDER, A.PROGGRP_CREATEDBY, A.PROGGRP_CREATION, A.PROGGRP_UPDATEDBY, A.PROGGRP_LASTUPDATE, A.PROGGRP_OTHER, A.PROGGRP_PARENT" +
                     " FROM TB_PROGRAMGROUP A";
                     //" WHERE A.GRPING_ID = B.LIST_VALUE" +
                     //" AND B.LIST_GROUP = 'GROUPING'";
            //strSql = "SELECT A.GRP_ID, A.GRP_NAME, A.GRPING_ID" +
            //         " FROM TB_GROUP A" +
            //         " WHERE 1 = 1";

            if (intGrpingId != 0)
            {
                //strSql += " AND A.GRPING_ID = ?";
                //cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;
            }

            if (intActive != 0)
            {
                strSql += " AND A.PROGGRP_ACTIVE = ?";
                cmd.Parameters.Add("p_PROGGRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    #endregion

    #region "Grouping"
    public int addGrouping(string strGrpingName, string strGroup, int intActive)
    {
        int intRecordAffected = 0;

        try
        {
            int intGrpingId = generateGroupingId();

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_LIST (" +
                            "LIST_NAME, " +
                            "LIST_VALUE, " +
                            "LIST_GROUP, " +
                            "LIST_ORDER, " +
                            "LIST_ACTIVE " +
                            ") VALUES " +
                           "(?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_LIST_NAME", OdbcType.VarChar, 20).Value = strGrpingName;
            cmd.Parameters.Add("p_LIST_VALUE", OdbcType.VarChar, 250).Value = intGrpingId;
            cmd.Parameters.Add("p_LIST_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_LIST_ORDER", OdbcType.Text).Value = intGrpingId;
            cmd.Parameters.Add("p_LISt_ACTIVE", OdbcType.VarChar, 1000).Value = intActive;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                grpingId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateGroupingById(int intGrpingId, string strGrpingName, int intGrpingActive)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_LIST SET" +
                     " LIST_NAME = ?," +
                     " LIST_ACTIVE = ?" +
                     " WHERE LIST_ID = ?";   

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_LIST_NAME", OdbcType.VarChar, 250).Value = strGrpingName;
            cmd.Parameters.Add("p_LIST_ACTIVE", OdbcType.Int, 1).Value = intGrpingActive;
            cmd.Parameters.Add("p_LIST_ID", OdbcType.Int, 9).Value = intGrpingId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                grpingId = intGrpingId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getGrpingList()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT *" +
                     " FROM TB_LIST" + 
                     " WHERE LIST_GROUP = 'GROUPING' AND LIST_VALUE NOT IN(1,2,3,4,5,6,7,8,9,10)";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean extractGrpingById(int intGrpingId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT *" +
                     " FROM TB_LIST" +
                     " WHERE LIST_ID = ?";

            cmd.Parameters.Add("p_LIST_ID", OdbcType.Int, 9).Value = intGrpingId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                grpingId = int.Parse(dataSet.Tables[0].Rows[0]["LIST_ID"].ToString());
                grpingName = dataSet.Tables[0].Rows[0]["LIST_NAME"].ToString();
                grpingActive = int.Parse(dataSet.Tables[0].Rows[0]["LIST_ACTIVE"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int deleteGrouping(int intGrpingId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_LIST WHERE LIST_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_LIST_ID", OdbcType.VarChar, 10).Value = intGrpingId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int generateGroupingId()
    {
        int intGrpingId = 0;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT LIST_VALUE FROM TB_LIST"+
                     " WHERE LIST_GROUP = 'GROUPING'" +
                     " ORDER BY LIST_ID DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            intGrpingId = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }


        return intGrpingId + 1;
    }

    public Boolean extractPrevGrouping(int intOrder, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT *" +
                     " FROM TB_LIST" +
                     " WHERE LIST_GROUP = 'GROUPING' AND LIST_ORDER < ?";

            cmd.Parameters.Add("p_LIST_ORDER", OdbcType.Int, 9).Value = intOrder;

            if (intActive != 0)
            {
                strSql += " AND LIST_ACTIVE = ?";
                cmd.Parameters.Add("p_LIST_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " ORDER BY LIST_ORDER DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                grpingId = Convert.ToInt16(dataSet.Tables[0].Rows[0]["LIST_ID"]);
                grpingName = dataSet.Tables[0].Rows[0]["LIST_NAME"].ToString();
                grpingOrder = Convert.ToInt16(dataSet.Tables[0].Rows[0]["LIST_ORDER"]);
            }

        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractNextGrouping(int intOrder, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT *" +
                      " FROM TB_LIST" +
                      " WHERE LIST_GROUP = 'GROUPING' AND LIST_ORDER > ?";

            cmd.Parameters.Add("p_LIST_ORDER", OdbcType.Int, 9).Value = intOrder;

            if (intActive != 0)
            {
                strSql += " AND LIST_ACTIVE = ?";
                cmd.Parameters.Add("p_LIST_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " ORDER BY LIST_ORDER ASC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                grpingId = Convert.ToInt16(dataSet.Tables[0].Rows[0]["LIST_ID"]);
                grpingName = dataSet.Tables[0].Rows[0]["LIST_NAME"].ToString();
                grpingOrder = Convert.ToInt16(dataSet.Tables[0].Rows[0]["LIST_ORDER"]);
            }

        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int updateListOrderById(int intId, int intOrder)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_LIST SET" +
                     " LIST_ORDER = ?" +
                     " WHERE LIST_GROUP = 'GROUPING' AND LIST_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_LIST_ORDER", OdbcType.BigInt, 9).Value = intOrder;
            cmd.Parameters.Add("p_LIST_ID", OdbcType.BigInt, 9).Value = intId;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isGrpingExist(string strGrpingName, int intGrpingId)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_LIST" +
                " WHERE LIST_GROUP = 'GROUPING'";

            if (intGrpingId > 0)
            {
                strSql += " AND LIST_ID = ?";
                cmd.Parameters.Add("p_LIST_ID", OdbcType.BigInt, 9).Value = intGrpingId;
            }

            else if (!string.IsNullOrEmpty(strGrpingName))
            {
                strSql += " AND LIST_NAME = ?";
                cmd.Parameters.Add("p_LIST_NAME", OdbcType.VarChar, 250).Value = strGrpingName;
            }
            
            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExist;
    }
    #endregion

    #region "Customized Project"
    #region "HTM Pharmacy"
    public int addGroup(string strGrpCode, string strGrpName, string strGrpNameJp, string strGrpNameMs, string strGrpNameZh, string strGrpDName, string strGrpDNameJp, string strGrpDNameMs, string strGrpDNameZh,
                         string strGrpPageTitleFriendlyUrl, int intGrpingId, string strGrpImg, int intGrpShowTop, int intGrpActive, int intGrpParent,
                         int intPromo, decimal decPromoPrice, DateTime dtPromoStart, DateTime dtPromoEnd, int intGrpCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            int intGrpOrder = generateGroupOrderNo(intGrpParent, intGrpingId);

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_GROUP (" +
                     "GRP_CODE, " +
                     "GRP_NAME, " +
                     "GRP_NAME_JP, " +
                     "GRP_NAME_MS, " +
                     "GRP_NAME_ZH, " +
                     "GRP_DNAME, " +
                     "GRP_DNAME_JP, " +
                     "GRP_DNAME_MS, " +
                     "GRP_DNAME_ZH, " +
                     "GRP_PAGETITLEFURL, " +
                     "GRPING_ID, " +
                     "GRP_IMAGE, " +
                     "GRP_SHOWTOP, " +
                     "GRP_ACTIVE, " +
                     "GRP_ORDER, " +
                     "GRP_PARENT," +
                     "GRP_PROMO, " +
                     "GRP_PROMOPRICE, " +
                     "GRP_PROMOSTART, " +
                     "GRP_PROMOEND, " +
                     "GRP_CREATEDBY, " +
                     "GRP_CREATION " +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_CODE", OdbcType.VarChar, 20).Value = strGrpCode;
            cmd.Parameters.Add("p_GRP_NAME", OdbcType.VarChar, 250).Value = strGrpName;
            cmd.Parameters.Add("p_GRP_NAME_JP", OdbcType.VarChar, 250).Value = strGrpNameJp;
            cmd.Parameters.Add("p_GRP_NAME_MS", OdbcType.VarChar, 250).Value = strGrpNameMs;
            cmd.Parameters.Add("p_GRP_NAME_ZH", OdbcType.VarChar, 250).Value = strGrpNameZh;
            cmd.Parameters.Add("p_GRP_DNAME", OdbcType.VarChar, 250).Value = strGrpDName;
            cmd.Parameters.Add("p_GRP_DNAME_JP", OdbcType.VarChar, 250).Value = strGrpDNameJp;
            cmd.Parameters.Add("p_GRP_DNAME_MS", OdbcType.VarChar, 250).Value = strGrpDNameMs;
            cmd.Parameters.Add("p_GRP_DNAME_ZH", OdbcType.VarChar, 250).Value = strGrpDNameZh;
            cmd.Parameters.Add("p_GRP_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strGrpPageTitleFriendlyUrl;
            cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;
            cmd.Parameters.Add("p_GRP_IMAGE", OdbcType.VarChar, 250).Value = strGrpImg;
            cmd.Parameters.Add("p_GRP_SHOWTOP", OdbcType.Int, 1).Value = intGrpShowTop;
            cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intGrpActive;
            cmd.Parameters.Add("p_GRP_ORDER", OdbcType.Int, 9).Value = intGrpOrder;
            cmd.Parameters.Add("p_GRP_PARENT", OdbcType.Int, 9).Value = intGrpParent;
            cmd.Parameters.Add("p_GRP_PROMO", OdbcType.Int, 1).Value = intPromo;
            cmd.Parameters.Add("p_GRP_PROMOPRICE", OdbcType.Decimal).Value = decPromoPrice;
            cmd.Parameters.Add("p_GRP_PROMOSTART", OdbcType.DateTime).Value = dtPromoStart;
            cmd.Parameters.Add("p_GRP_PROMOEND", OdbcType.DateTime).Value = dtPromoEnd;
            cmd.Parameters.Add("p_GRP_CREATEDBY", OdbcType.Int, 9).Value = intGrpCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                grpId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameGrp(int intGrpId, string strGrpCode, string strGrpName, string strGrpNameJp, string strGrpNameMs, string strGrpNameZh, string strGrpDName, string strGrpDNameJp, string strGrpDNameMs, string strGrpDNameZh,
                                   string strGrpPageTitleFriendlyUrl, int intGrpingId, string strGrpImg, int intGrpShowTop, int intGrpActive, 
                                   int intPromo, decimal decPromoPrice, DateTime dtPromoStart, DateTime dtPromoEnd)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_GROUP" +
                " WHERE GRP_ID = ?" +
                " AND BINARY GRP_CODE = ?" +
                " AND BINARY GRP_NAME = ?" +
                " AND BINARY GRP_NAME_JP = ?" +
                " AND BINARY GRP_NAME_MS = ?" +
                " AND BINARY GRP_NAME_ZH = ?" +
                " AND BINARY GRP_DNAME = ?" +
                " AND BINARY GRP_DNAME_JP = ?" +
                " AND BINARY GRP_DNAME_MS = ?" +
                " AND BINARY GRP_DNAME_ZH = ?" +
                " AND BINARY GRP_PAGETITLEFURL = ?" +
                " AND GRPING_ID = ?" +
                " AND BINARY GRP_IMAGE = ?" +
                " AND GRP_SHOWTOP = ?" +
                " AND GRP_ACTIVE = ?" +
                " AND GRP_PROMO = ?" +
                " AND GRP_PROMOPRICE = ?" +
                " AND GRP_PROMOSTART = ?" +
                " AND GRP_PROMOEND = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;
            cmd.Parameters.Add("p_GRP_CODE", OdbcType.VarChar, 20).Value = strGrpCode;
            cmd.Parameters.Add("p_GRP_NAME", OdbcType.VarChar, 250).Value = strGrpName;
            cmd.Parameters.Add("p_GRP_NAME_JP", OdbcType.VarChar, 250).Value = strGrpNameJp;
            cmd.Parameters.Add("p_GRP_NAME_MS", OdbcType.VarChar, 250).Value = strGrpNameMs;
            cmd.Parameters.Add("p_GRP_NAME_ZH", OdbcType.VarChar, 250).Value = strGrpNameZh;
            cmd.Parameters.Add("p_GRP_DNAME", OdbcType.VarChar, 250).Value = strGrpDName;
            cmd.Parameters.Add("p_GRP_DNAME_JP", OdbcType.VarChar, 250).Value = strGrpDNameJp;
            cmd.Parameters.Add("p_GRP_DNAME_MS", OdbcType.VarChar, 250).Value = strGrpDNameMs;
            cmd.Parameters.Add("p_GRP_DNAME_ZH", OdbcType.VarChar, 250).Value = strGrpDNameZh;
            cmd.Parameters.Add("p_GRP_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strGrpPageTitleFriendlyUrl;
            cmd.Parameters.Add("p_GRPING_ID", OdbcType.Int, 9).Value = intGrpingId;
            cmd.Parameters.Add("p_GRP_IMAGE", OdbcType.VarChar, 250).Value = strGrpImg;
            cmd.Parameters.Add("p_GRP_SHOWTOP", OdbcType.Int, 1).Value = intGrpShowTop;
            cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intGrpActive;
            cmd.Parameters.Add("p_GRP_PROMO", OdbcType.Int, 1).Value = intPromo;
            cmd.Parameters.Add("p_GRP_PROMOPRICE", OdbcType.Decimal).Value = decPromoPrice;
            cmd.Parameters.Add("p_GRP_PROMOSTART", OdbcType.DateTime).Value = dtPromoStart;
            cmd.Parameters.Add("p_GRP_PROMOEND", OdbcType.DateTime).Value = dtPromoEnd;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateGroupById(int intGrpId, string strGrpCode, string strGrpName, string strGrpNameJp, string strGrpNameMs, string strGrpNameZh, string strGrpDName, string strGrpDNameJp, string strGrpDNameMs, string strGrpDNameZh,
                                string strGrpPageTitleFriendlyUrl, string strGrpImg, int intGrpShowTop, int intGrpActive,
                                int intPromo, decimal decPromoPrice, DateTime dtPromoStart, DateTime dtPromoEnd, int intGrpUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_GROUP SET" +
                     " GRP_CODE = ?," +
                     " GRP_NAME = ?," +
                     " GRP_NAME_JP = ?," +
                     " GRP_NAME_MS = ?," +
                     " GRP_NAME_ZH = ?," +
                     " GRP_DNAME = ?," +
                     " GRP_DNAME_JP = ?," +
                     " GRP_DNAME_MS = ?," +
                     " GRP_DNAME_ZH = ?," +
                     " GRP_PAGETITLEFURL = ?," +
                     " GRP_IMAGE = ?," +
                     " GRP_SHOWTOP = ?," +
                     " GRP_ACTIVE = ?," +
                     " GRP_PROMO = ?," +
                     " GRP_PROMOPRICE = ?," +
                     " GRP_PROMOSTART = ?," +
                     " GRP_PROMOEND = ?," +
                     " GRP_UPDATEDBY = ?," +
                     " GRP_LASTUPDATE = SYSDATE()" +
                     " WHERE GRP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_CODE", OdbcType.VarChar, 20).Value = strGrpCode;
            cmd.Parameters.Add("p_GRP_NAME", OdbcType.VarChar, 250).Value = strGrpName;
            cmd.Parameters.Add("p_GRP_NAME_JP", OdbcType.VarChar, 250).Value = strGrpNameJp;
            cmd.Parameters.Add("p_GRP_NAME_MS", OdbcType.VarChar, 250).Value = strGrpNameMs;
            cmd.Parameters.Add("p_GRP_NAME_ZH", OdbcType.VarChar, 250).Value = strGrpNameZh;
            cmd.Parameters.Add("p_GRP_DNAME", OdbcType.VarChar, 250).Value = strGrpDName;
            cmd.Parameters.Add("p_GRP_DNAME_JP", OdbcType.VarChar, 250).Value = strGrpDNameJp;
            cmd.Parameters.Add("p_GRP_DNAME_MS", OdbcType.VarChar, 250).Value = strGrpDNameMs;
            cmd.Parameters.Add("p_GRP_DNAME_ZH", OdbcType.VarChar, 250).Value = strGrpDNameZh;
            cmd.Parameters.Add("p_GRP_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strGrpPageTitleFriendlyUrl;
            cmd.Parameters.Add("p_GRP_IMAGE", OdbcType.VarChar, 250).Value = strGrpImg;
            cmd.Parameters.Add("p_GRP_SHOWTOP", OdbcType.Int, 1).Value = intGrpShowTop;
            cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intGrpActive;
            cmd.Parameters.Add("p_GRP_PROMO", OdbcType.Int, 1).Value = intPromo;
            cmd.Parameters.Add("p_GRP_PROMOPRICE", OdbcType.Decimal).Value = decPromoPrice;
            cmd.Parameters.Add("p_GRP_PROMOSTART", OdbcType.DateTime).Value = dtPromoStart;
            cmd.Parameters.Add("p_GRP_PROMOEND", OdbcType.DateTime).Value = dtPromoEnd;
            cmd.Parameters.Add("p_GRP_UPDATEDBY", OdbcType.Int, 9).Value = intGrpUpdatedBy;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                grpId = intGrpId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractHTMGrpById(int intGrpId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.GRP_ID, A.GRP_CODE, A.GRP_NAME, A.GRP_NAME_JP, A.GRP_DNAME, A.GRP_DNAME_JP, A.GRP_PAGETITLEFURL, A.GRP_DESC, A.GRP_SNAPSHOT, A.GRPING_ID, A.GRP_IMAGE, A.GRP_SHOWTOP," +
                     " A.GRP_NAME_MS, A.GRP_NAME_ZH, A.GRP_DNAME_MS, A.GRP_DNAME_ZH, A.GRP_ACTIVE, A.GRP_SHOWDESC,A.GRP_PAGETITLEFURL, B.LIST_NAME, A.GRP_PROMO, A.GRP_PROMOPRICE, A.GRP_PROMOSTART, GRP_PROMOEND" +
                     " FROM TB_GROUP A, TB_LIST B" +
                     " WHERE A.GRP_ID = ?" +
                     " AND A.GRPING_ID = B.LIST_VALUE" +
                     " AND B.LIST_GROUP = 'GROUPING'";

            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;

            if (intActive != 0)
            {
                strSql += " AND A.GRP_ACTIVE = ?";
                cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                grpId = int.Parse(dataSet.Tables[0].Rows[0]["GRP_ID"].ToString());
                grpCode = dataSet.Tables[0].Rows[0]["GRP_CODE"].ToString();
                grpName = dataSet.Tables[0].Rows[0]["GRP_NAME"].ToString();
                grpNameJp = dataSet.Tables[0].Rows[0]["GRP_NAME_JP"].ToString();
                grpNameMs = dataSet.Tables[0].Rows[0]["GRP_NAME_MS"].ToString();
                grpNameZh = dataSet.Tables[0].Rows[0]["GRP_NAME_ZH"].ToString();
                grpDName = dataSet.Tables[0].Rows[0]["GRP_DNAME"].ToString();
                grpDNameJp = dataSet.Tables[0].Rows[0]["GRP_DNAME_JP"].ToString();
                grpDNameMs = dataSet.Tables[0].Rows[0]["GRP_DNAME_MS"].ToString();
                grpDNameZh = dataSet.Tables[0].Rows[0]["GRP_DNAME_ZH"].ToString();
                grpPageTitleFriendlyUrl = dataSet.Tables[0].Rows[0]["GRP_PAGETITLEFURL"].ToString();
                grpDesc = dataSet.Tables[0].Rows[0]["GRP_DESC"].ToString();
                grpSnapshot = dataSet.Tables[0].Rows[0]["GRP_SNAPSHOT"].ToString();
                grpingId = int.Parse(dataSet.Tables[0].Rows[0]["GRPING_ID"].ToString());
                grpingName = dataSet.Tables[0].Rows[0]["LIST_NAME"].ToString();
                grpImage = dataSet.Tables[0].Rows[0]["GRP_IMAGE"].ToString();
                grpShowTop = int.Parse(dataSet.Tables[0].Rows[0]["GRP_SHOWTOP"].ToString());
                grpActive = int.Parse(dataSet.Tables[0].Rows[0]["GRP_ACTIVE"].ToString());
                grpShowDesc = int.Parse(dataSet.Tables[0].Rows[0]["GRP_SHOWDESC"].ToString());
                grpPageTitleFURL = dataSet.Tables[0].Rows[0]["GRP_PAGETITLEFURL"].ToString();
                promo = int.Parse(dataSet.Tables[0].Rows[0]["GRP_PROMO"].ToString());
                promoPrice = dataSet.Tables[0].Rows[0]["GRP_PROMOPRICE"] != DBNull.Value ? decimal.Parse(dataSet.Tables[0].Rows[0]["GRP_PROMOPRICE"].ToString()) : 0;
                promoStart = dataSet.Tables[0].Rows[0]["GRP_PROMOSTART"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["GRP_PROMOSTART"].ToString()) : DateTime.MaxValue;
                promoEnd = dataSet.Tables[0].Rows[0]["GRP_PROMOEND"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["GRP_PROMOEND"].ToString()) : DateTime.MaxValue;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    #endregion
    #endregion

    #endregion
}
