﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsUser
/// </summary>
public class clsUser : dbconnBase
{
    #region "Properties"
    private int _userId;
    private string _userEmail;
    private string _userImage;
    private string _userPwd;
    private int _userActive;
    private int _userType;
    private string _userTypeName;
    private int _userProject;
    private string _userProjectName;
    private int _userCreatedBy;
    private DateTime _userCreation;
    private int _userUpdatedBy;
    private DateTime _userLastUpdate;
    private string _userProjServer;
    private string _userProjUserId;
    private string _userProjPwd;
    private string _userProjDatabase;
    private string _userProjOption;
    private string _userProjPort;
    private int _userProjDriver;
    private string _userProjDriverName;
    private string _userProjUploadPath;
    private string _userProjUserviewUrl;
    private int _userProjType;
    private string _userProjTypeName;
    #endregion

    #region "Property Methods"
    public int userId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    public string userEmail
    {
        get { return _userEmail; }
        set { _userEmail = value; }
    }

    public string userImage
    {
        get { return _userImage; }
        set { _userImage = value; }
    }

    public string userPwd
    {
        get { return _userPwd; }
        set { _userPwd = value; }
    }

    public int userActive
    {
        get { return _userActive; }
        set { _userActive = value; }
    }

    public int userType
    {
        get { return _userType; }
        set { _userType = value; }
    }

    public string userTypeName
    {
        get { return _userTypeName; }
        set { _userTypeName = value; }
    }

    public int userProject
    {
        get { return _userProject; }
        set { _userProject = value; }
    }

    public string userProjectName
    {
        get { return _userProjectName; }
        set { _userProjectName = value; }
    }

    public int userCreatedBy
    {
        get { return _userCreatedBy; }
        set { _userCreatedBy = value; }
    }

    public DateTime userCreation
    {
        get { return _userCreation; }
        set { _userCreation = value; }
    }

    public int userUpdatedBy
    {
        get { return _userUpdatedBy; }
        set { _userUpdatedBy = value; }
    }

    public DateTime userLastUpdate
    {
        get { return _userLastUpdate; }
        set { _userLastUpdate = value; }
    }

    public string userProjServer
    {
        get { return _userProjServer; }
        set { _userProjServer = value; }
    }

    public string userProjUserId
    {
        get { return _userProjUserId; }
        set { _userProjUserId = value; }
    }

    public string userProjPwd
    {
        get { return _userProjPwd; }
        set { _userProjPwd = value; }
    }

    public string userProjDatabase
    {
        get { return _userProjDatabase; }
        set { _userProjDatabase = value; }
    }

    public string userProjOption
    {
        get { return _userProjOption; }
        set { _userProjOption = value; }
    }

    public string userProjPort
    {
        get { return _userProjPort; }
        set { _userProjPort = value; }
    }

    public int userProjDriver
    {
        get { return _userProjDriver; }
        set { _userProjDriver = value; }
    }

    public string userProjDriverName
    {
        get { return _userProjDriverName; }
        set { _userProjDriverName = value; }
    }

    public string userProjUploadPath
    {
        get { return _userProjUploadPath; }
        set { _userProjUploadPath = value; }
    }

    public string userProjUserviewUrl
    {
        get { return _userProjUserviewUrl; }
        set { _userProjUserviewUrl = value; }
    }

    public int userProjType
    {
        get { return _userProjType; }
        set { _userProjType = value; }
    }

    public string userProjTypeName
    {
        get { return _userProjTypeName; }
        set { _userProjTypeName = value; }
    }
    #endregion

    #region "Methods"
    public clsUser()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    public int addUser(string strUserEmail, int intUserType, string strUserPwd, int intUserProject, int intActive, int intUserCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_USER (" +
                     "USR_EMAIL," +
                     "USR_TYPE," +
                     "USR_PWD," +
                     "USR_PROJ," +
                     "USR_ACTIVE," +
                     "USR_CREATEDBY," +
                     "USR_CREATION" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_USR_EMAIL", OdbcType.VarChar, 250).Value = strUserEmail;
            cmd.Parameters.Add("p_USR_TYPE", OdbcType.Int, 1).Value = intUserType;
            cmd.Parameters.Add("p_USR_PWD", OdbcType.VarChar, 50).Value = strUserPwd;
            cmd.Parameters.Add("p_USR_PROJ", OdbcType.Int, 9).Value = intUserProject;
            cmd.Parameters.Add("p_USR_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_USR_CREATEDBY", OdbcType.BigInt, 9).Value = intUserCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";

                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                userId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractUserById(int intUserId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.USR_ID, A.USR_EMAIL, A.USR_PWD,A.USR_IMAGE, A.USR_ACTIVE, A.USR_TYPE, A.V_USRTYPENAME, A.USR_PROJ," +
                     " A.V_USRPROJNAME, A.USR_CREATEDBY, A.USR_CREATION, A.USR_UPDATEDBY, A.USR_LASTUPDATE, A.V_PROJSERVER," +
                     " A.V_PROJUSERID, A.V_PROJPWD, A.V_PROJDATABASE, A.V_PROJOPTION, A.V_PROJPORT, A.V_PROJDRIVER, A.V_PROJDRIVERNAME," +
                     " A.V_PROJUPLOADPATH, A.V_PROJUSERVIEWURL, A.V_PROJTYPE, A.V_PROJTYPENAME" +
                     " FROM VW_USER A" +
                     " WHERE A.USR_ID = ?";

            cmd.Parameters.Add("p_USR_ID", OdbcType.BigInt, 9).Value = intUserId;

            if (intActive != 0)
            {
                strSql += " AND A.USR_ACTIVE = ?";

                cmd.Parameters.Add("p_USR_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                userId = int.Parse(ds.Tables[0].Rows[0]["USR_ID"].ToString());
                userEmail = ds.Tables[0].Rows[0]["USR_EMAIL"].ToString();
                userPwd = ds.Tables[0].Rows[0]["USR_PWD"].ToString();
                userActive = Convert.ToInt16(ds.Tables[0].Rows[0]["USR_ACTIVE"]);
                userType = Convert.ToInt16(ds.Tables[0].Rows[0]["USR_TYPE"]);
                userImage = Convert.ToString(ds.Tables[0].Rows[0]["USR_IMAGE"]);
                userTypeName = ds.Tables[0].Rows[0]["V_USRTYPENAME"].ToString();
                userProject = int.Parse(ds.Tables[0].Rows[0]["USR_PROJ"].ToString());
                userProjectName = ds.Tables[0].Rows[0]["V_USRPROJNAME"].ToString();
                userCreatedBy = int.Parse(ds.Tables[0].Rows[0]["USR_CREATEDBY"].ToString());
                userCreation = ds.Tables[0].Rows[0]["USR_CREATION"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["USR_CREATION"]) : DateTime.MaxValue;
                userUpdatedBy = int.Parse(ds.Tables[0].Rows[0]["USR_UPDATEDBY"].ToString());
                userLastUpdate = ds.Tables[0].Rows[0]["USR_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["USR_LASTUPDATE"]) : DateTime.MaxValue;
                userProjServer = ds.Tables[0].Rows[0]["V_PROJSERVER"].ToString();
                userProjUserId = ds.Tables[0].Rows[0]["V_PROJUSERID"].ToString();
                userProjPwd = ds.Tables[0].Rows[0]["V_PROJPWD"].ToString();
                userProjDatabase = ds.Tables[0].Rows[0]["V_PROJDATABASE"].ToString();
                userProjOption = ds.Tables[0].Rows[0]["V_PROJOPTION"].ToString();
                userProjPort = ds.Tables[0].Rows[0]["V_PROJPORT"].ToString();
                userProjDriver = Convert.ToInt16(ds.Tables[0].Rows[0]["V_PROJDRIVER"]);
                userProjDriverName = ds.Tables[0].Rows[0]["V_PROJDRIVERNAME"].ToString();
                userProjUploadPath = ds.Tables[0].Rows[0]["V_PROJUPLOADPATH"].ToString();
                userProjUserviewUrl = ds.Tables[0].Rows[0]["V_PROJUSERVIEWURL"].ToString();
                userProjType = int.Parse(ds.Tables[0].Rows[0]["V_PROJTYPE"].ToString());
                userProjTypeName = ds.Tables[0].Rows[0]["V_PROJTYPENAME"].ToString();
            }

            ds.Dispose();
            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractUserById2(int intUserId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.USR_ID, A.USR_EMAIL, A.USR_TYPE, A.USR_PROJ" +
                     " FROM TB_USER A" +
                     " WHERE A.USR_ID = ?";

            cmd.Parameters.Add("p_USR_ID", OdbcType.BigInt, 9).Value = intUserId;

            if (intActive != 0)
            {
                strSql += " AND A.USR_ACTIVE = ?";

                cmd.Parameters.Add("p_USR_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                userId = int.Parse(ds.Tables[0].Rows[0]["USR_ID"].ToString());
                userEmail = ds.Tables[0].Rows[0]["USR_EMAIL"].ToString();
                userType = Convert.ToInt16(ds.Tables[0].Rows[0]["USR_TYPE"]);
                userProject = int.Parse(ds.Tables[0].Rows[0]["USR_PROJ"].ToString());
            }

            ds.Dispose();
            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public DataSet getUserList(int intActive)
    {
        return getUserList(intActive, -1, -1);
    }

    public DataSet getUserList(int intActive, int start, int length)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.USR_ID, A.USR_EMAIL, A.USR_PWD, A.USR_ACTIVE, A.USR_TYPE, A.V_USRTYPENAME, A.USR_PROJ," +
                     " A.V_USRPROJNAME, A.USR_CREATEDBY, A.USR_CREATION, A.USR_UPDATEDBY, A.USR_LASTUPDATE, A.V_PROJSERVER," +
                     " A.V_PROJUSERID, A.V_PROJPWD, A.V_PROJDATABASE, A.V_PROJOPTION, A.V_PROJPORT, A.V_PROJDRIVER, A.V_PROJDRIVERNAME," +
                     " A.V_PROJUPLOADPATH, A.V_PROJUSERVIEWURL, A.V_PROJTYPE, A.V_PROJTYPENAME" +
                     " FROM VW_USER A";

            if (intActive != 0)
            {
                strSql += " WHERE A.USR_ACTIVE = ?";
                cmd.Parameters.Add("p_USR_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            if(length > 0)
            {
                strSql += " LIMIT " + start + "," + length;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public int countUserList(int intActive)
    {
        int count = 0;
        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM VW_USER A";

            if (intActive != 0)
            {
                strSql += " WHERE A.USR_ACTIVE = ?";
                cmd.Parameters.Add("p_USR_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            count = Convert.ToInt16(cmd.ExecuteScalar());
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return count;
    }
    public Boolean isUserExist(int intUserId, string strUserEmail)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_USER" +
                     " WHERE USR_EMAIL = ?";

            cmd.Parameters.Add("p_USR_EMAIL", OdbcType.VarChar, 250).Value = strUserEmail;

            if (intUserId != 0)
            {
                strSql += " AND USR_ID != ?";
                cmd.Parameters.Add("p_USR_ID", OdbcType.BigInt, 9).Value = intUserId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public Boolean isUserTypeValid(string strUsername, string strPassword, int intType)
    {
        Boolean boolValid = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT COUNT(*) FROM TB_USER WHERE USR_EMAIL = ? AND USR_PWD = ? AND USR_TYPE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_USR_EMAIL", OdbcType.VarChar, 250).Value = strUsername;
            cmd.Parameters.Add("p_USR_PWD", OdbcType.VarChar, 50).Value = strPassword;
            cmd.Parameters.Add("p_USR_TYPE", OdbcType.Int, 9).Value = intType;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolValid = true; }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolValid;
    }

    public Boolean isExactSameSetUser(int intUserId, string strUserEmail, int intUserType, int intUserProject, int intActive)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_USER" +
                     " WHERE USR_ID = ?" +
                     " AND BINARY USR_EMAIL = ?" +
                     " AND USR_TYPE = ?" +
                     " AND USR_PROJ = ?" +
                     " AND USR_ACTIVE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_USR_ID", OdbcType.BigInt, 9).Value = intUserId;
            cmd.Parameters.Add("p_USR_EMAIL", OdbcType.VarChar, 250).Value = strUserEmail;
            cmd.Parameters.Add("p_USR_TYPE", OdbcType.Int, 1).Value = intUserType;
            cmd.Parameters.Add("p_USR_PROJ", OdbcType.Int, 9).Value = intUserProject;
            cmd.Parameters.Add("p_USR_ACTIVE", OdbcType.Int, 1).Value = intActive;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int updateUserById(int intUserId, string strUserEmail, int intUserType, int intUserProject, int intActive, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_USER SET" +
                     " USR_EMAIL = ?," +
                     " USR_TYPE = ?," +
                     " USR_PROJ = ?," +
                     " USR_ACTIVE = ?," +
                     " USR_UPDATEDBY = ?," +
                     " USR_LASTUPDATE = SYSDATE()" +
                     " WHERE USR_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_USR_EMAIL", OdbcType.VarChar, 250).Value = strUserEmail;
            cmd.Parameters.Add("p_USR_TYPE", OdbcType.Int, 1).Value = intUserType;
            cmd.Parameters.Add("p_USR_PROJ", OdbcType.Int, 9).Value = intUserProject;
            cmd.Parameters.Add("p_USR_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_USR_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_USR_ID", OdbcType.BigInt, 9).Value = intUserId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                userId = intUserId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateUserById(int intUserId, string strUserPwd, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_USER SET" +
                     " USR_PWD = ?," +
                     " USR_UPDATEDBY = ?," +
                     " USR_LASTUPDATE = SYSDATE()" +
                     " WHERE USR_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("P_USR_PWD", OdbcType.VarChar, 50).Value = strUserPwd;
            cmd.Parameters.Add("p_USR_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_USR_ID", OdbcType.BigInt, 9).Value = intUserId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                userId = intUserId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updatePasswordByEmail(string strEmail, string strUserPwd)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_USER SET" +
                     " USR_PWD = ?," +
                     " USR_LASTUPDATE = SYSDATE()" +
                     " WHERE USR_EMAIL = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("P_USR_PWD", OdbcType.VarChar, 50).Value = strUserPwd;
            cmd.Parameters.Add("p_USR_EMAIL", OdbcType.VarChar, 250).Value = strEmail;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
             }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int changeUserPwd(int intId, string strCurrentPwd, string strNewPwd)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_USER SET USR_PWD = ? WHERE USR_ID = ? AND USR_PWD = ?";
            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_USR_PWD", OdbcType.VarChar, 50).Value = strNewPwd;
            cmd.Parameters.Add("p_USR_ID", OdbcType.BigInt, 9).Value = intId;
            cmd.Parameters.Add("p_USR_PWD", OdbcType.VarChar, 50).Value = strCurrentPwd;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                userId = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int changeProfileImage(int intUserID, string strImage)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_USER SET USR_IMAGE = ? WHERE USR_ID = ?";
            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_USR_IMAGE", OdbcType.VarChar, 250).Value = strImage;
            cmd.Parameters.Add("p_USR_ID", OdbcType.BigInt, 9).Value = intUserID;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int deleteUserById(int intUserId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_USER WHERE USR_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_USR_ID", OdbcType.BigInt, 9).Value = intUserId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if(intRecordAffected == 1)
            {
                trans.Commit();
                userId = intUserId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteUserByProject(int intProjId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_USER WHERE USR_PROJ = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_USR_PROJ", OdbcType.BigInt, 9).Value = intProjId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean login(int intActive)
    {
        Boolean bValidLogin = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_USER WHERE USR_EMAIL = ? AND USR_PWD = ?";

            cmd.Parameters.Add("p_USR_EMAIL", OdbcType.VarChar, 250).Value = userEmail;
            cmd.Parameters.Add("p_USR_PWD", OdbcType.VarChar, 50).Value = userPwd;

            if (intActive != 0)
            {
                strSql += " AND USR_ACTIVE = ?";
                cmd.Parameters.Add("p_USR_ACTIVE", OdbcType.Int, 1).Value = 1;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                userId = int.Parse(ds.Tables[0].Rows[0]["USR_ID"].ToString());
                userEmail = ds.Tables[0].Rows[0]["USR_TYPE"].ToString();
                userType = Convert.ToInt16(ds.Tables[0].Rows[0]["USR_TYPE"]);
                bValidLogin = true;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bValidLogin;
    }

    public Boolean isEmailExist(int intActive)
    {
        Boolean bValidLogin = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_USER WHERE USR_EMAIL = ?";

            cmd.Parameters.Add("p_USR_EMAIL", OdbcType.VarChar, 250).Value = userEmail;

            if (intActive != 0)
            {
                strSql += " AND USR_ACTIVE = ?";
                cmd.Parameters.Add("p_USR_ACTIVE", OdbcType.Int, 1).Value = 1;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                userId = int.Parse(ds.Tables[0].Rows[0]["USR_ID"].ToString());
                userEmail = ds.Tables[0].Rows[0]["USR_TYPE"].ToString();
                userType = Convert.ToInt16(ds.Tables[0].Rows[0]["USR_TYPE"]);
                bValidLogin = true;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bValidLogin;
    }

    public int setLastLogin()
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_USER SET USR_LASTLOGIN = SYSDATE() WHERE USR_ID = ?";
            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_USR_ID", OdbcType.BigInt, 9).Value = userId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion
}
