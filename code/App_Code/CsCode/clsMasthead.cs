﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;

public class clsMasthead : dbconnBase
{
    #region "Properties"


    #region "Masthead Slide Show"
    private int _mastId;
    private string _mastName;
    private int _mastActive;
    private string _mastImage;
    private string _mastThumb;
    private string _mastShortDesc;
    private string _mastTagline1;
    private string _mastTagline2;
    private int _mastOrder;
    private int _mastClickable;
    private string _mastUrl;
    private string _mastTarget;
    #endregion


    #region "Slide Group"
    private int _grpId;
    private string _grpName;
    private string _grpDName;
    private int _grpShowTop;
    private int _grpActive;
    private int _grpCreatedBy;
    private DateTime _grpCreation;
    private int _grpUpdatedBy;
    private DateTime _grpLastUpdate;
    private int _grpOther;

    //sldier setting
    private string _grpSliderType;
    private string _grpSliderOption;
    private string _grpSliderTrans;

    //slider Mobile option
    public const string CONSTSLIDER_MOBILE_SwingInsideInStairs = "{$Duration:[$duration],x:0.2,y:-0.1,$Delay:20,$Cols:[$cols],$Rows:[$rows],$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Easing:{$Left:$JssorEasing$.$EaseInWave,$Top:$JssorEasing$.$EaseInWave,$Clip:$JssorEasing$.$EaseOutQuad},$Assembly:260,$Round:{$Left:1.3,$Top:2.5}}";
    public const string CONSTSLIDER_MOBILE_DodgeDanceInStairs = "{$Duration:[$duration],x:0.3,y:-0.3,$Delay:20,$Cols:[$cols],$Rows:[$rows],$Clip:15,$During:{$Left:[0.2,0.8],$Top:[0.2,0.8]},$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Easing:{$Left:$JssorEasing$.$EaseInJump,$Top:$JssorEasing$.$EaseInJump,$Clip:$JssorEasing$.$EaseOutQuad},$Assembly:260,$Round:{$Left:0.8,$Top:2.5}}";
    public const string CONSTSLIDER_MOBILE_DodgePetInsideInStairs = "{$Duration:[$duration],x:0.2,y:-0.1,$Delay:20,$Cols:[$cols],$Rows:[$rows],$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Easing:{$Left:$JssorEasing$.$EaseInWave,$Top:$JssorEasing$.$EaseInWave,$Clip:$JssorEasing$.$EaseOutQuad},$Assembly:260,$Round:{$Left:0.8,$Top:2.5}}";
    public const string CONSTSLIDER_MOBILE_DodgeInsideInStairs = "{$Duration:[$duration],x:0.3,y:-0.3,$Delay:20,$Cols:[$cols],$Rows:[$rows],$Clip:15,$During:{$Left:[0.2,0.8],$Top:[0.2,0.8]},$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Easing:{$Left:$JssorEasing$.$EaseInJump,$Top:$JssorEasing$.$EaseInJump,$Clip:$JssorEasing$.$EaseSwing},$Assembly:260,$Round:{$Left:0.8,$Top:0.8}}";
    public const string CONSTSLIDER_MOBILE_FlutterInsideIn = "{$Duration:[$duration],x:1,$Delay:30,$Cols:[$cols],$Rows:[$rows],$Clip:15,$During:{$Left:[0.3,0.7]},$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Easing:{$Left:$JssorEasing$.$EaseInOutExpo,$Clip:$JssorEasing$.$EaseInOutQuad},$Assembly:260,$Round:{$Top:0.8}}";
    public const string CONSTSLIDER_MOBILE_RotateVDoubleIn = "{$Duration:[$duration],x:-1,y:2,$Rows:[$rows],$Zoom:11,$Rotate:1,$ChessMode:{$Row:15},$Easing:{$Left:$JssorEasing$.$EaseInCubic,$Top:$JssorEasing$.$EaseInCubic,$Zoom:$JssorEasing$.$EaseInCubic,$Opacity:$JssorEasing$.$EaseOutQuad,$Rotate:$JssorEasing$.$EaseInCubic},$Assembly:2049,$Opacity:2,$Round:{$Rotate:0.8}}";
    public const string CONSTSLIDER_MOBILE_ZoomVDoubleIn = "{$Duration:[$duration],y:2,$Rows:[$rows],$Zoom:11,$ChessMode:{$Row:15},$Easing:{$Top:$JssorEasing$.$EaseInCubic,$Zoom:$JssorEasing$.$EaseInCubic,$Opacity:$JssorEasing$.$EaseOutQuad},$Assembly:2049,$Opacity:2}";
    public const string CONSTSLIDER_MOBILE_CollapseStairs = "{$Duration:[$duration],$Delay:30,$Cols:[$cols],$Rows:[$rows],$Clip:15,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Easing:$JssorEasing$.$EaseOutQuad,$Assembly:2049}";
    public const string CONSTSLIDER_MOBILE_ClipAndChessIn = "{$Duration:[$duration],y:-1,$Cols:[$cols],$Rows:[$rows],$Clip:15,$During:{$Top:[0.5,0.5],$Clip:[0,0.5]},$Formation:$JssorSlideshowFormations$.$FormationStraight,$ChessMode:{$Column:12},$ScaleClip:0.5}";
    public const string CONSTSLIDER_MOBILE_ExpandStairs = "{$Duration:[$duration],$Delay:30,$Cols:[$cols],$Rows:[$rows],$Clip:15,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Easing:$JssorEasing$.$EaseInQuad,$Assembly:2050}";
    public const string CONSTSLIDER_MOBILE_DominoesStripe = "{$Duration:[$duration],y:-1,$Delay:60,$Cols:[$cols],$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Easing:$JssorEasing$.$EaseOutJump,$Round:{$Top:1.5}}";
    public const string CONSTSLIDER_MOBILE_WaveOut = "{$Duration:[$duration],y:-0.5,$Delay:60,$Cols:[$cols],$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Easing:$JssorEasing$.$EaseInWave,$Round:{$Top:1.5}}";
    public const string CONSTSLIDER_MOBILE_WaveIn = "{$Duration:[$duration],y:-0.5,$Delay:60,$Cols:[$cols],$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Easing:$JssorEasing$.$EaseInWave,$Round:{$Top:1.5}}";

    public static Dictionary<string, Dictionary<string, string>>  dictMobileTransition = new Dictionary<string, Dictionary<string, string>>{
        { "1", new Dictionary<string, string> { { "option", CONSTSLIDER_MOBILE_SwingInsideInStairs } } },
        { "2", new Dictionary<string, string> { { "option", CONSTSLIDER_MOBILE_DodgeDanceInStairs } } },
        { "3", new Dictionary<string, string> { { "option", CONSTSLIDER_MOBILE_DodgePetInsideInStairs } } },
        { "4", new Dictionary<string, string> { { "option", CONSTSLIDER_MOBILE_DodgeInsideInStairs } } },
        { "5", new Dictionary<string, string> { { "option", CONSTSLIDER_MOBILE_FlutterInsideIn } } },
        { "6", new Dictionary<string, string> { { "option", CONSTSLIDER_MOBILE_RotateVDoubleIn } } },
        { "7", new Dictionary<string, string> { { "option", CONSTSLIDER_MOBILE_ZoomVDoubleIn } } },
        { "8", new Dictionary<string, string> { { "option", CONSTSLIDER_MOBILE_CollapseStairs } } },
        { "9", new Dictionary<string, string> { { "option", CONSTSLIDER_MOBILE_ClipAndChessIn } } },
        { "10", new Dictionary<string, string> { { "option", CONSTSLIDER_MOBILE_ExpandStairs } } },
        { "11", new Dictionary<string, string> { { "option", CONSTSLIDER_MOBILE_DominoesStripe } } },
        { "12", new Dictionary<string, string> { { "option", CONSTSLIDER_MOBILE_WaveOut } } },
        { "13", new Dictionary<string, string> { { "option", CONSTSLIDER_MOBILE_WaveIn } } },
    };

    #endregion


    #endregion


    #region "Property Methods"


    #region "Masthead Slide Show"
    public int mastId
    {
        get { return _mastId; }
        set { _mastId = value; }
    }

    public int mastActive
    {
        get { return _mastId; }
        set { _mastId = value; }
    }

    public string mastName
    {
        get { return _mastName; }
        set { _mastName = value; }
    }

    public string mastImage
    {
        get { return _mastImage; }
        set { _mastImage = value; }
    }

    public string mastThumb
    {
        get { return _mastThumb; }
        set { _mastThumb = value; }
    }

    public string mastShortDesc
    {
        get { return _mastShortDesc; }
        set { _mastShortDesc = value; }
    }

    public string mastTagline1
    {
        get { return _mastTagline1; }
        set { _mastTagline1 = value; }
    }

    public string mastTagline2
    {
        get { return _mastTagline2; }
        set { _mastTagline2 = value; }
    }

    public int mastOrder
    {
        get { return _mastOrder; }
        set { _mastOrder = value; }
    }

    public int mastClickable
    {
        get { return _mastClickable; }
        set { _mastClickable = value; }
    }

    public string mastUrl
    {
        get { return _mastUrl; }
        set { _mastUrl = value; }
    }

    public string mastTarget
    {
        get { return _mastTarget; }
        set { _mastTarget = value; }
    }
    #endregion


    #region "Slide Group"
    public int grpId
    {
        get { return _grpId; }
        set { _grpId = value; }
    }

    public string grpName
    {
        get { return _grpName; }
        set { _grpName = value; }
    }

    public string grpDName
    {
        get { return _grpDName; }
        set { _grpDName = value; }
    }

    public int grpShowTop
    {
        get { return _grpShowTop; }
        set { _grpShowTop = value; }
    }

    public int grpActive
    {
        get { return _grpActive; }
        set { _grpActive = value; }
    }

    public DateTime grpCreation
    {
        get { return _grpCreation; }
        set { _grpCreation = value; }
    }

    public int grpCreatedBy
    {
        get { return _grpCreatedBy; }
        set { _grpCreatedBy = value; }
    }

    public DateTime grpLastUpdate
    {
        get { return _grpLastUpdate; }
        set { _grpLastUpdate = value; }
    }

    public int grpUpdatedBy
    {
        get { return _grpUpdatedBy; }
        set { _grpUpdatedBy = value; }
    }

    public int grpOther
    {
        get { return _grpOther; }
        set { _grpOther = value; }
    }

    public string grpSliderType
    {
        get { return _grpSliderType; }
        set { _grpSliderType = value; }
    }

    public string grpSliderOption
    {
        get { return _grpSliderOption; }
        set { _grpSliderOption = value; }
    }
    public string grpSliderTrans
    {
        get { return _grpSliderTrans; }
        set { _grpSliderTrans = value; }
    }
    
    public int otherGrpId
    {
        get
        {
            if (HttpContext.Current.Session["SLIDEOTHERGRPID"] as string == null || HttpContext.Current.Session["SLIDEOTHERGRPID"] as string == "")
            {
                HttpContext.Current.Session["SLIDEOTHERGRPID"] = getOtherGrpId();
            }
            else
            {
                HttpContext.Current.Session["SLIDEOTHERGRPID"] = 13;
            }
            return Convert.ToInt16(HttpContext.Current.Session["SLIDEOTHERGRPID"]);
        }
    }
    #endregion


    #endregion


    #region "Public Methods"
    public clsMasthead()
    {
    }


    #region "Masthead Slide Show"
    public DataSet getMastheadImage()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT *" +
                     " FROM VW_MASTHEAD";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public DataSet getMastheadImage2()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT *" +
                     " FROM TB_MASTHEAD";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public DataSet getMastheadImage3()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.*,B.GRP_NAME AS V_GRPNAME,B.GRP_DNAME AS V_GRPDNAME FROM TB_MASTHEAD A " +
                     "LEFT JOIN TB_SLIDEGROUP B ON A.GRP_ID = B.GRP_ID";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public int deleteMasthead(int intMastheadId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_MASTHEAD WHERE MASTHEAD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_MASTHEAD_ID", OdbcType.Int, 9).Value = intMastheadId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractMastheadById(int intMastId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.MASTHEAD_ID, A.MASTHEAD_NAME, A.MASTHEAD_IMAGE, A.MASTHEAD_ACTIVE, A.MASTHEAD_THUMB, A.MASTHEAD_SHORTDESC, " +
                     "A.MASTHEAD_TAGLINE1, A.MASTHEAD_TAGLINE2, A.GRP_ID, A.MASTHEAD_ORDER, A.MASTHEAD_CLICKABLE, A.MASTHEAD_TARGET, A.MASTHEAD_URL " +
                     "FROM TB_MASTHEAD A " +
                     "WHERE MASTHEAD_ID = ?";
            //strSql = "SELECT *" +
            //         " FROM VW_MASTHEAD" +
            //         " WHERE MASTHEAD_ID = ?";

            cmd.Parameters.Add("p_MASTHEAD_ID", OdbcType.Int, 9).Value = intMastId;

            if (intActive != 0)
            {
                strSql += " AND MASTHEAD_ACTIVE = ?";
                cmd.Parameters.Add("p_MASTHEAD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                mastId = int.Parse(dataSet.Tables[0].Rows[0]["MASTHEAD_ID"].ToString());
                mastName = dataSet.Tables[0].Rows[0]["MASTHEAD_NAME"].ToString();
                mastImage = dataSet.Tables[0].Rows[0]["MASTHEAD_IMAGE"].ToString();
                mastActive = Convert.ToInt16(dataSet.Tables[0].Rows[0]["MASTHEAD_ACTIVE"]);
                mastThumb = dataSet.Tables[0].Rows[0]["MASTHEAD_THUMB"].ToString();
                mastShortDesc = dataSet.Tables[0].Rows[0]["MASTHEAD_SHORTDESC"].ToString();
                mastTagline1 = dataSet.Tables[0].Rows[0]["MASTHEAD_TAGLINE1"].ToString();
                mastTagline2 = dataSet.Tables[0].Rows[0]["MASTHEAD_TAGLINE2"].ToString();
                mastClickable = Convert.ToInt16(dataSet.Tables[0].Rows[0]["MASTHEAD_CLICKABLE"]);
                mastUrl = dataSet.Tables[0].Rows[0]["MASTHEAD_URL"].ToString();
                mastTarget = dataSet.Tables[0].Rows[0]["MASTHEAD_TARGET"].ToString();
                mastOrder = Convert.ToInt16(dataSet.Tables[0].Rows[0]["MASTHEAD_ORDER"]);
                grpId = int.Parse(dataSet.Tables[0].Rows[0]["GRP_ID"].ToString());
                //grpName = dataSet.Tables[0].Rows[0]["V_GRPNAME"].ToString();
                //grpDName = dataSet.Tables[0].Rows[0]["V_GRPDNAME"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int addMasthead(string strMastName, string strMastImage, int intMastActive, string strMastThumb, string strMastShortDesc, string strMastTagline1, string strMastTagline2, int intGrpId, int intMastOrder, int intMastClick, string strMastUrl, string strMastTarget)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_MASTHEAD (" +
                     "MASTHEAD_NAME, " +
                     "MASTHEAD_IMAGE, " +
                     "MASTHEAD_ACTIVE, " +
                     "MASTHEAD_THUMB," +
                     "MASTHEAD_SHORTDESC," +
                     "MASTHEAD_TAGLINE1," +
                     "MASTHEAD_TAGLINE2," +
                     "GRP_ID," +
                     "MASTHEAD_ORDER," +
                     "MASTHEAD_CLICKABLE," +
                     "MASTHEAD_URL," +
                     "MASTHEAD_TARGET" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_MASTHEAD_NAME", OdbcType.VarChar, 250).Value = strMastName;
            cmd.Parameters.Add("p_MASTHEAD_IMAGE", OdbcType.VarChar, 250).Value = strMastImage;
            cmd.Parameters.Add("p_MASTHEAD_ACTIVE", OdbcType.Int, 1).Value = intMastActive;
            cmd.Parameters.Add("p_MASTHEAD_THUMB", OdbcType.VarChar, 250).Value = strMastThumb;
            cmd.Parameters.Add("p_MASTHEAD_SHORTDESC", OdbcType.VarChar, 250).Value = strMastShortDesc;
            cmd.Parameters.Add("p_MASTHEAD_TAGLINE1", OdbcType.Text).Value = strMastTagline1;
            cmd.Parameters.Add("p_MASTHEAD_TAGLINE2", OdbcType.Text).Value = strMastTagline2;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intGrpId;
            cmd.Parameters.Add("p_MASTHEAD_ORDER", OdbcType.Int, 9).Value = intMastOrder;
            cmd.Parameters.Add("p_MASTHEAD_CLICKABLE", OdbcType.Int, 1).Value = intMastClick;
            cmd.Parameters.Add("p_MASTHEAD_URL", OdbcType.VarChar, 250).Value = strMastUrl;
            cmd.Parameters.Add("p_MASTHEAD_TARGET", OdbcType.VarChar, 20).Value = strMastTarget;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                mastId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameMastheadSet(int intMastId, string strMastName, string strMastImg, int intMastActive, string strMastThumb, string strMastShortDesc, string strMastTagline1, string strMastTagline2, int intGrpId, int intMastOrder, int intMastClick, string strMastUrl, string strMastTarget)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_MASTHEAD" +
                     " WHERE MASTHEAD_ID = ?" +
                     " AND BINARY MASTHEAD_NAME = ?" +
                     " AND BINARY MASTHEAD_IMAGE = ?" +
                     " AND MASTHEAD_ACTIVE = ?" +
                     " AND BINARY MASTHEAD_THUMB = ?" +
                     " AND BINARY MASTHEAD_SHORTDESC = ?" +
                     " AND BINARY MASTHEAD_TAGLINE1 = ?" +
                     " AND BINARY MASTHEAD_TAGLINE2 = ?" +
                     " AND GRP_ID = ?" +
                     " AND MASTHEAD_ORDER = ?" +
                     " AND MASTHEAD_CLICKABLE = ?" +
                     " AND BINARY MASTHEAD_URL = ?" +
                     " AND MASTHEAD_TARGET = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_MASTHEAD_ID", OdbcType.Int, 9).Value = intMastId;
            cmd.Parameters.Add("p_MASTHEAD_NAME", OdbcType.VarChar, 250).Value = strMastName;
            cmd.Parameters.Add("p_MASTHEAD_IMAGE", OdbcType.VarChar, 250).Value = strMastImg;
            cmd.Parameters.Add("p_MASTHEAD_ACTIVE", OdbcType.Int, 1).Value = intMastActive;
            cmd.Parameters.Add("p_MASTHEAD_THUMB", OdbcType.VarChar, 250).Value = strMastThumb;
            cmd.Parameters.Add("p_MASTHEAD_SHORTDESC", OdbcType.VarChar, 250).Value = strMastShortDesc;
            cmd.Parameters.Add("p_MASTHEAD_TAGLINE1", OdbcType.Text).Value = strMastTagline1;
            cmd.Parameters.Add("p_MASTHEAD_TAGLINE2", OdbcType.Text).Value = strMastTagline2;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intGrpId;
            cmd.Parameters.Add("p_MASTHEAD_ORDER", OdbcType.Int, 9).Value = intMastOrder;
            cmd.Parameters.Add("p_MASTHEAD_CLICKABLE", OdbcType.Int, 1).Value = intMastClick;
            cmd.Parameters.Add("p_MASTHEAD_URL", OdbcType.VarChar, 250).Value = strMastUrl;
            cmd.Parameters.Add("p_MASTHEAD_TARGET", OdbcType.VarChar, 20).Value = strMastTarget;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateMastheadById(int intMastId, string strMastName, string strMastImg, int intMastActive, string strMastThumb, string strMastShortDesc, string strMastTagline1, string strMastTagline2, int intGrpId, int intMastOrder, int intMastClick, string strMastUrl, string strMastTarget)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_MASTHEAD SET" +
                     " MASTHEAD_NAME = ?," +
                     " MASTHEAD_IMAGE = ?," +
                     " MASTHEAD_ACTIVE = ?," +
                     " MASTHEAD_THUMB = ?," +
                     " MASTHEAD_SHORTDESC= ?," +
                     " MASTHEAD_TAGLINE1 = ?," +
                     " MASTHEAD_TAGLINE2 = ?," +
                     " GRP_ID = ?," +
                     " MASTHEAD_ORDER = ?," +
                     " MASTHEAD_CLICKABLE = ?," +
                     " MASTHEAD_URL = ?," +
                     " MASTHEAD_TARGET = ?," +
                     " MASTHEAD_LASTUPDATED = NOW()" +
                     " WHERE MASTHEAD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_MASTHEAD_NAME", OdbcType.VarChar, 250).Value = strMastName;
            cmd.Parameters.Add("p_MASTHEAD_IMAGE", OdbcType.VarChar, 250).Value = strMastImg;
            cmd.Parameters.Add("p_MASTHEAD_ACTIVE", OdbcType.Int, 1).Value = intMastActive;
            cmd.Parameters.Add("p_MASTHEAD_THUMB", OdbcType.VarChar, 250).Value = strMastThumb;
            cmd.Parameters.Add("p_MASTHEAD_SHORTDESC", OdbcType.VarChar, 250).Value = strMastShortDesc;
            cmd.Parameters.Add("p_MASTHEAD_TAGLINE1", OdbcType.Text).Value = strMastTagline1;
            cmd.Parameters.Add("p_MASTHEAD_TAGLINE2", OdbcType.Text).Value = strMastTagline2;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intGrpId;
            cmd.Parameters.Add("p_MASTHEAD_ORDER", OdbcType.Int, 9).Value = intMastOrder;
            cmd.Parameters.Add("p_MASTHEAD_CLICKABLE", OdbcType.Int, 1).Value = intMastClick;
            cmd.Parameters.Add("p_MASTHEAD_URL", OdbcType.VarChar, 250).Value = strMastUrl;
            cmd.Parameters.Add("p_MASTHEAD_TARGET", OdbcType.VarChar, 20).Value = strMastTarget;
            cmd.Parameters.Add("p_MASTHEAD_ID", OdbcType.Int, 9).Value = intMastId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                mastId = intMastId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int moveMastheadByGrpId(int intGrpId, int intNewGrpId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_MASTHEAD SET GRP_ID = ?" +
                     " WHERE GRP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_NEW_GRP_ID", OdbcType.Int, 9).Value = intNewGrpId;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int getTotalRecord(int intGroupId, int intActive)
    {
        int intTotal = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_MASTHEAD A" +
                     " WHERE 1 = 1";

            if (intGroupId != 0)
            {
                strSql += " AND A.GRP_ID = ?";
                cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intGroupId;
            }

            if (intActive != 0)
            {
                strSql += " AND A.MASTHEAD_ACTIVE = ?";
                cmd.Parameters.Add("p_MASTHEAD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            intTotal = int.Parse(cmd.ExecuteScalar().ToString());
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intTotal;
    }
    public DataSet getMastheadImageFromGoupID(int intGroupId)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT MASTHEAD_NAME, MASTHEAD_TAGLINE1, MASTHEAD_TAGLINE2, MASTHEAD_IMAGE, MASTHEAD_ORDER, MASTHEAD_ACTIVE" +
                     " FROM TB_MASTHEAD WHERE GRP_ID = ? AND MASTHEAD_ACTIVE = 1";
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intGroupId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    #endregion


    #region "Slide Group"
    public Boolean isGrpNameExist(string strGrpName, int intGrpId)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_SLIDEGROUP" +
                     " WHERE GRP_NAME = ?";

            cmd.Parameters.Add("p_GRP_NAME", OdbcType.VarChar, 250).Value = strGrpName;

            if (intGrpId > 0)
            {
                strSql += " AND GRP_ID != ?";
                cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExist;
    }

    public int addGroup(string strGrpName, string strGrpDName, string strSliderType,string strSliderOptions, string strSliderTrans,
                         int intGrpShowTop, int intGrpActive,int intGrpCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_SLIDEGROUP (" +
                     "GRP_NAME, " +
                     "GRP_DNAME, " +
                     "GRP_SLIDERTYPE, " +
                     "GRP_SLIDERTRANS, " +
                     "GRP_SLIDEROPTION, " +
                     "GRP_SHOWTOP, " +
                     "GRP_ACTIVE, " +
                     "GRP_CREATEDBY, " +
                     "GRP_CREATION " +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_NAME", OdbcType.VarChar, 250).Value = strGrpName;
            cmd.Parameters.Add("p_GRP_DNAME", OdbcType.VarChar, 250).Value = strGrpDName;
            cmd.Parameters.Add("p_GRP_SLIDERTYPE", OdbcType.VarChar, 250).Value = strSliderType;
            cmd.Parameters.Add("p_GRP_SLIDERTRANS", OdbcType.VarChar, 250).Value = strSliderTrans;
            cmd.Parameters.Add("p_GRP_SLIDEROPTION", OdbcType.VarChar, 250).Value = strSliderOptions;
            cmd.Parameters.Add("p_GRP_SHOWTOP", OdbcType.Int, 1).Value = intGrpShowTop;
            cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intGrpActive;
            cmd.Parameters.Add("p_GRP_CREATEDBY", OdbcType.Int, 9).Value = intGrpCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                grpId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameGrp(int intGrpId, string strGrpName, string strGrpDName,string strSliderType, string strSliderTrans,
                                  string strSliderOptions, int intGrpShowTop,int intGrpActive)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_SLIDEGROUP" +
                     " WHERE GRP_ID = ?" +
                     " AND BINARY GRP_NAME = ?" +
                     " AND BINARY GRP_DNAME = ?" +
                     " AND BINARY GRP_SLIDERTYPE = ?" +
                     " AND BINARY GRP_SLIDERTRANS = ?" +
                     " AND BINARY GRP_SLIDEROPTION = ?" +
                     " AND GRP_SHOWTOP = ?" +
                     " AND GRP_ACTIVE = ?";


            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;
            cmd.Parameters.Add("p_GRP_NAME", OdbcType.VarChar, 250).Value = strGrpName;
            cmd.Parameters.Add("p_GRP_DNAME", OdbcType.VarChar, 250).Value = strGrpDName;
            cmd.Parameters.Add("p_GRP_SLIDERTYPE", OdbcType.VarChar, 250).Value = strSliderType;
            cmd.Parameters.Add("p_GRP_SLIDERTRANS", OdbcType.VarChar, 250).Value = strSliderTrans;
            cmd.Parameters.Add("p_GRP_SLIDEROPTION", OdbcType.VarChar, 250).Value = strSliderOptions;
            cmd.Parameters.Add("p_GRP_SHOWTOP", OdbcType.Int, 1).Value = intGrpShowTop;
            cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intGrpActive;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateGroupById(int intGrpId, string strGrpName, string strGrpDName,string strSliderType,string strSliderTrans,
                               string strSliderOptions, int intGrpShowTop,int intGrpActive, int intGrpUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_SLIDEGROUP SET" +
                     " GRP_NAME = ?," +
                     " GRP_DNAME = ?," +
                     " GRP_SLIDERTYPE = ?," +
                     " GRP_SLIDERTRANS = ?," +
                     " GRP_SLIDEROPTION = ?," +
                     " GRP_SHOWTOP = ?," +
                     " GRP_ACTIVE = ?," +
                     " GRP_UPDATEDBY = ?," +
                     " GRP_LASTUPDATE = SYSDATE()" +
                     " WHERE GRP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_NAME", OdbcType.VarChar, 250).Value = strGrpName;
            cmd.Parameters.Add("p_GRP_DNAME", OdbcType.VarChar, 250).Value = strGrpDName;
            cmd.Parameters.Add("p_GRP_SLIDERTYPE", OdbcType.VarChar, 250).Value = strSliderType;
            cmd.Parameters.Add("p_GRP_SLIDERTRANS", OdbcType.VarChar, 250).Value = strSliderTrans;
            cmd.Parameters.Add("p_GRP_SLIDEROPTION", OdbcType.VarChar, 250).Value = strSliderOptions;
            cmd.Parameters.Add("p_GRP_SHOWTOP", OdbcType.Int, 1).Value = intGrpShowTop;
            cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intGrpActive;
            cmd.Parameters.Add("p_GRP_UPDATEDBY", OdbcType.Int, 9).Value = intGrpUpdatedBy;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                grpId = intGrpId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractGrpById(int intGrpId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.GRP_ID, A.GRP_NAME, A.GRP_DNAME, A.GRP_SHOWTOP, A.GRP_ACTIVE, A.GRP_CREATEDBY, A.GRP_CREATION, A.GRP_UPDATEDBY, A.GRP_LASTUPDATE, A.GRP_OTHER,A.GRP_SLIDERTYPE,A.GRP_SLIDERTRANS,A.GRP_SLIDEROPTION" +
                     " FROM TB_SLIDEGROUP A" +
                     " WHERE A.GRP_ID = ?";

            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;

            if (intActive != 0)
            {
                strSql += " AND A.GRP_ACTIVE = ?";
                cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                grpId = int.Parse(dataSet.Tables[0].Rows[0]["GRP_ID"].ToString());
                grpName = dataSet.Tables[0].Rows[0]["GRP_NAME"].ToString();
                grpDName = dataSet.Tables[0].Rows[0]["GRP_DNAME"].ToString();
                grpShowTop = Convert.ToInt16(dataSet.Tables[0].Rows[0]["GRP_SHOWTOP"]);
                grpActive = Convert.ToInt16(dataSet.Tables[0].Rows[0]["GRP_ACTIVE"]);
                grpCreatedBy = int.Parse(dataSet.Tables[0].Rows[0]["GRP_CREATEDBY"].ToString());
                grpCreation = dataSet.Tables[0].Rows[0]["GRP_CREATION"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["GRP_CREATION"].ToString()) : DateTime.MaxValue;
                grpUpdatedBy = int.Parse(dataSet.Tables[0].Rows[0]["GRP_UPDATEDBY"].ToString());
                grpLastUpdate = dataSet.Tables[0].Rows[0]["GRP_LASTUPDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["GRP_LASTUPDATE"].ToString()) : DateTime.MaxValue;
                grpOther = Convert.ToInt16(dataSet.Tables[0].Rows[0]["GRP_OTHER"]);

                // for slider setting - sh.chong 18 Sep 2015
                grpSliderOption = dataSet.Tables[0].Rows[0]["GRP_SLIDEROPTION"].ToString();
                grpSliderTrans = dataSet.Tables[0].Rows[0]["GRP_SLIDERTRANS"].ToString();
                grpSliderType = dataSet.Tables[0].Rows[0]["GRP_SLIDERTYPE"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public DataSet getGrpList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.GRP_ID, A.GRP_NAME, A.GRP_DNAME, A.GRP_SHOWTOP, A.GRP_ACTIVE, A.GRP_CREATEDBY, A.GRP_CREATION, A.GRP_UPDATEDBY, A.GRP_LASTUPDATE, A.GRP_OTHER" +
                     " FROM TB_SLIDEGROUP A";

            if (intActive != 0)
            {
                strSql += " WHERE A.GRP_ACTIVE = ?";
                cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    private int getOtherGrpId()
    {
        int iOtherGrpId = 10;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT A.GRP_ID" +
                     " FROM TB_SLIDEGROUP A" +
                     " WHERE A.GRP_OTHER = 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            iOtherGrpId = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return iOtherGrpId;
    }

    public int deleteGroup(int intGrpId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_SLIDEGROUP WHERE GRP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_ID", OdbcType.VarChar, 10).Value = intGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int getTotalRecordGroup(int intOther, int intActive)
    {
        int intTotal = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_SLIDEGROUP A" +
                     " WHERE 1 = 1";

            if (intOther == 0)
            {
                strSql += " AND A.GRP_OTHER = ?";
                cmd.Parameters.Add("p_GRP_OTHER", OdbcType.Int, 1).Value = intOther;
            }

            if (intActive != 0)
            {
                strSql += " AND A.GRP_ACTIVE = ?";
                cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            intTotal = int.Parse(cmd.ExecuteScalar().ToString());
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intTotal;
    }
    #endregion


    #endregion
}
