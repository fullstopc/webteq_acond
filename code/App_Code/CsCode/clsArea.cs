﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsArea
/// </summary>
public class clsArea : dbconnBase
{
    #region "Properties"
    private int _areaId;
    private string _areaName;
    private int _areaOrder;
    private DateTime _areaCreation;
    private int _areaCreatedBy;
    private DateTime _areaLastUpdate;
    private int _areaUpdatedBy;
    #endregion

    #region "Property Methods"
    public int areaId
    {
        get { return _areaId; }
        set { _areaId = value; }
    }

    public string areaName
    {
        get { return _areaName; }
        set { _areaName = value; }
    }

    public int areaOrder
    {
        get { return _areaOrder; }
        set { _areaOrder = value; }
    }

    public DateTime areaCreation
    {
        get { return _areaCreation; }
        set { _areaCreation = value; }
    }

    public int areaCreatedBy
    {
        get { return _areaCreatedBy; }
        set { _areaCreatedBy = value; }
    }

    public DateTime areaLastUpdate
    {
        get { return _areaLastUpdate; }
        set { _areaLastUpdate = value; }
    }

    public int areaUpdatedBy
    {
        get { return _areaUpdatedBy; }
        set { _areaUpdatedBy = value; }
    }
    #endregion

    #region "Methods"
    public clsArea()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    public int getOrder(int intId)
    {
        int intOrder = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT AREA_ORDER FROM TB_AREA WHERE 1=1";

            if (intId != 0)
            {
                strSql += " AND AREA_ID != ?";
                cmd.Parameters.Add("p_AREA_ID", OdbcType.Int, 9).Value = intId;
            }

            strSql += " ORDER BY AREA_ORDER DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            intOrder = Convert.ToInt16(cmd.ExecuteScalar()) + 1;

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intOrder;
    }

    public int addArea(string strName, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            int intOrder = getOrder(0);

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_AREA (" +
                     "AREA_NAME," +
                     "AREA_ORDER, " +
                     "AREA_CREATION," +
                     "AREA_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_AREA_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_AREA_ORDER", OdbcType.BigInt, 9).Value = intOrder;
            cmd.Parameters.Add("p_AREA_CREATEDBY", OdbcType.BigInt, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";

                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                areaId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isAreaExist(string strName)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT EXISTS(" +
                     "SELECT * FROM TB_AREA" +
                     " WHERE AREA_NAME = ?)";

            cmd.Parameters.Add("p_AREA_NAME", OdbcType.VarChar, 250).Value = strName;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = int.Parse(cmd.ExecuteScalar().ToString());

            boolFound = intCount == 1 ? true : false;
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean isExactSameSetData(string strName)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_AREA" +
                     " WHERE BINARY AREA_NAME = ?";

            cmd.Parameters.Add("p_AREA_NAME", OdbcType.VarChar, 250).Value = strName;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int updateArea(int intId, string strName, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_AREA SET" +
                     " AREA_NAME = ?," +
                     " AREA_LASTUPDATE = SYSDATE()," +
                     " AREA_UPDATEDBY" +
                     " WHERE AREA_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_AREA_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_AREA_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_AREA_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractAreaById(int intId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.AREA_ID, A.AREA_NAME, A.AREA_CREATION, A.AREA_CREATEDBY, A.AREA_LASTUPDATE, A.AREA_UPDATEDBY" +
                     " FROM TB_AREA A" +
                     " WHERE A.AREA_ID = ?";

            cmd.Parameters.Add("p_AREA_ID", OdbcType.BigInt, 9).Value = intId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                areaId = int.Parse(ds.Tables[0].Rows[0]["AREA_ID"].ToString());
                areaName = ds.Tables[0].Rows[0]["AREA_NAME"].ToString();
                areaCreation = ds.Tables[0].Rows[0]["AREA_CREATION"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["AREA_CREATION"].ToString()) : DateTime.MaxValue;
                areaCreatedBy = int.Parse(ds.Tables[0].Rows[0]["AREA_CREATEDBY"].ToString());
                areaLastUpdate = ds.Tables[0].Rows[0]["AREA_LASTUPDATE"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["AREA_LASTUPDATE"].ToString()) : DateTime.MaxValue;
                areaUpdatedBy = int.Parse(ds.Tables[0].Rows[0]["AREA_UPDATEDBY"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractPrevNextArea(int intOrder, string strPosition)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.AREA_ID, A.AREA_ORDER " +
                     " FROM TB_AREA A WHERE 1=1 ";

            switch (strPosition)
            { 
                case "Prev":
                    strSql += " AND A.AREA_ORDER < ? ORDER BY A.AREA_ORDER DESC LIMIT 1";
                    cmd.Parameters.Add("p_AREA_ORDER", OdbcType.BigInt, 9).Value = intOrder;
                    break;
                case "Next":
                    strSql += " AND A.AREA_ORDER > ? ORDER BY A.AREA_ORDER ASC LIMIT 1";
                    cmd.Parameters.Add("p_AREA_ORDER", OdbcType.BigInt, 9).Value = intOrder;
                    break;
            }
            
            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                areaId = int.Parse(dataSet.Tables[0].Rows[0]["AREA_ID"].ToString());
                areaOrder = int.Parse(dataSet.Tables[0].Rows[0]["AREA_ORDER"].ToString());
            }

        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int updateAreaOrderById(int intId, int intOrder)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_AREA SET" +
                     " AREA_ORDER = ?" +
                     " WHERE AREA_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_AREA_ORDER", OdbcType.BigInt, 9).Value = intOrder;
            cmd.Parameters.Add("p_AREA_ID", OdbcType.BigInt, 9).Value = intId;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteAreaById(int intId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_AREA WHERE AREA_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_AREA_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getAreaList()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.AREA_ID, A.AREA_NAME, A.AREA_ORDER, A.AREA_CREATION, A.AREA_CREATEDBY, A.AREA_LASTUPDATE, A.AREA_UPDATEDBY" +
                     " FROM TB_AREA A";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    #endregion
}
