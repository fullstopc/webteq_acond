﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;
using System.Linq;

/// <summary>
/// Summary description for clsCMS
/// </summary>
public class clsPage : dbconnBase
{
    #region "Properties"
    private int _id;
    private string _name;
    private string _displayName;
    private string _title;
    private string _titleFriendlyUrl;
    private string _content;
    private int _showPageTitle;
    private int _showInMenu;
    private int _showTop;
    private int _showBottom;
    private int _showInBreadcrumb;
    private int _userEditable;
    private int _enableLink;
    private string _cssClass;
    private int _order;
    private string _lang;
    private string _langName;
    private string _template;
    private int _templateParent;
    private string _templateParentName;
    private int _parent;
    private string _parentName;
    private string _parentCSSClass;
    private string _keyword;
    private int _defaultKeyword;
    private string _desc;
    private int _defaultDesc;
    private int _redirect;
    private string _redirectName;
    private int _active;
    private int _other;
    private int _createdBy;
    private DateTime _creation;
    private int _updatedBy;
    private DateTime _lastUpdate;
    private string _redirectLink;
    private string _redirectTarget;
    private int _otherId;
    private string _topMenuContent;
    private int _slideshowGroup;
    private int _showMobileView;
    private string _mobileTemplate;
    private string _mobileContent;
    private int _mobileShowInMenu;
    private int _mobileShowTop;
    private int _mobileShowBottom;
    private string _mobileCssClass;
    private int _showInSitemap;
    private int _mobileRedirect;
    private string _mobileRedirectName;
    private string _mobileExternalLink;
    private string _mobileTarget;
    private int _pageAuthorize;
    private int _pgAuthid;
    private string _uniLogin;
    private string _uniPwd;
    private string _IndCompName;
    private string _IndContactPerson;
    private string _IndEmail;
    private string _IndContactNo;
    private string _IndFaxNo;
    private string _IndMobileNo;
    private string _IndCountry;
    private string _IndCity;
    private string _IndAddress;
    private string _IndMemSince;
    private string _IndRemarks;
    private string _IndLogin;
    private string _IndPwd;
    private int _contentParent;
    private int _grpId;
    #endregion


    #region "Property Methods"
    public int id
    {
        get { return _id; }
        set { _id = value; }
    }

    public string name
    {
        get { return _name; }
        set { _name = value; }
    }

    public string displayName
    {
        get { return _displayName; }
        set { _displayName = value; }
    }

    public string title
    {
        get { return _title; }
        set { _title = value; }
    }

    public string titleFriendlyUrl
    {
        get { return _titleFriendlyUrl; }
        set { _titleFriendlyUrl = value; }
    }

    public string content
    {
        get { return _content; }
        set { _content = value; }
    }

    public int showPageTitle
    {
        get { return _showPageTitle; }
        set { _showPageTitle = value; }
    }

    public int showInMenu
    {
        get { return _showInMenu; }
        set { _showInMenu = value; }
    }

    public int showTop
    {
        get { return _showTop; }
        set { _showTop = value; }
    }

    public int showBottom
    {
        get { return _showBottom; }
        set { _showBottom = value; }
    }

    public int showInBreadcrumb
    {
        get { return _showInBreadcrumb; }
        set { _showInBreadcrumb = value; }
    }

    public int userEditable
    {
        get { return _userEditable; }
        set { _userEditable = value; }
    }

    public int enableLink
    {
        get { return _enableLink; }
        set { _enableLink = value; }
    }

    public string cssClass
    {
        get { return _cssClass; }
        set { _cssClass = value; }
    }
    public int order
    {
        get { return _order; }
        set { _order = value; }
    }

    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }

    public string langName
    {
        get { return _langName; }
        set { _langName = value; }
    }

    public string template
    {
        get { return _template; }
        set { _template = value; }
    }

    public int templateParent
    {
        get { return _templateParent; }
        set { _templateParent = value; }
    }

    public string templateParentName
    {
        get { return _templateParentName; }
        set { _templateParentName = value; }
    }

    public int parent
    {
        get { return _parent; }
        set { _parent = value; }
    }

    public string parentName
    {
        get { return _parentName; }
        set { _parentName = value; }
    }

    public string parentCSSClass
    {
        get { return _parentCSSClass; }
        set { _parentCSSClass = value; }
    }

    public string keyword
    {
        get { return _keyword; }
        set { _keyword = value; }
    }

    public int defaultKeyword
    {
        get { return _defaultKeyword; }
        set { _defaultKeyword = value; }
    }

    public string desc
    {
        get { return _desc; }
        set { _desc = value; }
    }

    public int defaultDesc
    {
        get { return _defaultDesc; }
        set { _defaultDesc = value; }
    }

    public int redirect
    {
        get { return _redirect; }
        set { _redirect = value; }
    }

    public string redirectName
    {
        get { return _redirectName; }
        set { _redirectName = value; }
    }

    public int active
    {
        get { return _active; }
        set { _active = value; }
    }

    public int other
    {
        get { return _other; }
        set { _other = value; }
    }

    public int createdBy
    {
        get { return _createdBy; }
        set { _createdBy = value; }
    }

    public DateTime creation
    {
        get { return _creation; }
        set { _creation = value; }
    }

    public int updatedBy
    {
        get { return _updatedBy; }
        set { _updatedBy = value; }
    }

    public DateTime lastUpdate
    {
        get { return _lastUpdate; }
        set { _lastUpdate = value; }
    }

    public string redirectLink
    {
        get { return _redirectLink; }
        set { _redirectLink = value; }
    }

    public string redirectTarget
    {
        get { return _redirectTarget; }
        set { _redirectTarget = value; }
    }

    public int otherId
    {
        get { return getOtherId(); }
    }

    public string topMenuContent
    {
        get { return _topMenuContent; }
        set { _topMenuContent = value; }
    }

    public int slideshowGroup
    {
        get { return _slideshowGroup; }
        set { _slideshowGroup = value; }
    }

    public int showMobileView
    {
        get { return _showMobileView; }
        set { _showMobileView = value; }
    }

    public string mobileTemplate
    {
        get { return _mobileTemplate; }
        set { _mobileTemplate = value; }
    }

    public string mobileContent
    {
        get { return _mobileContent; }
        set { _mobileContent = value; }
    }

    public int mobileShowInMenu
    {
        get { return _mobileShowInMenu; }
        set { _mobileShowInMenu = value; }
    }

    public int mobileShowTop
    {
        get { return _mobileShowTop; }
        set { _mobileShowTop = value; }
    }

    public int mobileShowBottom
    {
        get { return _mobileShowBottom; }
        set { _mobileShowBottom = value; }
    }

    public string mobileCssClass
    {
        get { return _mobileCssClass; }
        set { _mobileCssClass = value; }
    }

    public int showInSitemap
    {
        get { return _showInSitemap; }
        set { _showInSitemap = value; }
    }

    public int mobileRedirect
    {
        get { return _mobileRedirect; }
        set { _mobileRedirect = value; }
    }

    public string mobileRedirectName
    {
        get { return _mobileRedirectName; }
        set { _mobileRedirectName = value; }
    }

    public string mobileExternalLink
    {
        get { return _mobileExternalLink; }
        set { _mobileExternalLink = value; }
    }

    public string mobileTarget
    {
        get { return _mobileTarget; }
        set { _mobileTarget = value; }
    }

    public int pageAuthorize
    {
        get { return _pageAuthorize; }
        set { _pageAuthorize = value; }
    }
    
    public int pgAuthid
    {
        get { return _pgAuthid; }
        set { _pgAuthid = value; }
    }

    public string uniLogin
    {
        get { return _uniLogin; }
        set { _uniLogin = value; }
    }

    public string uniPwd
    {
        get { return _uniPwd; }
        set { _uniPwd = value; }
    }

    public string IndCompName
    {
        get { return _IndCompName; }
        set { _IndCompName = value; }
    }

    public string IndContactPerson
    {
        get { return _IndContactPerson; }
        set { _IndContactPerson = value; }
    }

    public string IndEmail
    {
        get { return _IndEmail; }
        set { _IndEmail = value; }
    }

    public string IndContactNo
    {
        get { return _IndContactNo; }
        set { _IndContactNo = value; }
    }

    public string IndFaxNo
    {
        get { return _IndFaxNo; }
        set { _IndFaxNo = value; }
    }

    public string IndMobileNo
    {
        get { return _IndMobileNo; }
        set { _IndMobileNo = value; }
    }

    public string IndCountry
    {
        get { return _IndCountry; }
        set { _IndCountry = value; }
    }

    public string IndCity
    {
        get { return _IndCity; }
        set { _IndCity = value; }
    }

    public string IndAddress
    {
        get { return _IndAddress; }
        set { _IndAddress = value; }
    }

    public string IndMemSince
    {
        get { return _IndMemSince; }
        set { _IndMemSince = value; }
    }

    public string IndRemarks
    {
        get { return _IndRemarks; }
        set { _IndRemarks = value; }
    }

    public string IndLogin
    {
        get { return _IndLogin; }
        set { _IndLogin = value; }
    }

    public string IndPwd
    {
        get { return _IndPwd; }
        set { _IndPwd = value; }
    }

    public int contentParent
    {
        get { return _contentParent; }
        set { _contentParent = value; }
    }
    public int grpid
    {
        get { return _grpId; }
        set { _grpId = value; }
    }
    #endregion


    #region "Public Methods"
    public clsPage()
    { }

    private int getOtherId()
    {
        int intOtherId = 10;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT A.PAGE_ID" +
                     " FROM TB_PAGE A" +
                     " WHERE A.PAGE_OTHER = 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            intOtherId = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intOtherId;
    }

    public int getTotalPagesByParent(int intParent)
    {
        int intTotal = 0;

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            strSql = "SELECT COUNT(*) AS TOTAL FROM TB_PAGE" +
                     " WHERE PAGE_PARENT = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.BigInt, 9).Value = intParent;

            intTotal = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intTotal;
    }

    public int getTotalPages(int intOther, int intActive)
    {
        int intTotal = 0;

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            strSql = "SELECT COUNT(*) AS TOTAL FROM TB_PAGE" +
                     " WHERE 1 = 1";

            if (intOther == 0)
            {
                strSql += " AND PAGE_OTHER = ?";
                cmd.Parameters.Add("p_PAGE_OTHER", OdbcType.Int, 1).Value = intOther;
            }

            if (intActive != 0)
            {
                strSql += " AND PAGE_ACTIVE = ?";
                cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            intTotal = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intTotal;
    }

    public int addPage(string strName, string strDisplayName, string strTitle, string strTitleFriendlyUrl, string strTemplate, 
        int intTemplateParent, int intParent, string strLanguage, int intShowPageTitle, int intShowInMenu, 
        int intShowTop, int intShowBottom, int intShowInBreadcrumb, int intUserEditable, int intEnableLink, 
        int intShowInSitemap, string strCSSClass, string strKeyword, int intDefaultKeyword, string strDesc, 
        int intDefaultDesc, int intRedirect, string strRedirectLink, string strTarget, int intActive, 
        string strContent, int intCreatedBy, string strTopMenuContent, int intSlideShowGroup, int intPageAuthorize,
        int intContentParent)
    {
        int intRecordAffected = 0;

        try
        {
            int intOrder = getOrder(intParent, 0);

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<string> columns = new List<string>()
            {
                "PAGE_NAME","PAGE_DISPLAYNAME","PAGE_TITLE","PAGE_TITLEFURL","PAGE_CONTENT",
                "PAGE_SHOWPAGETITLE","PAGE_SHOWINMENU","PAGE_SHOWTOP","PAGE_SHOWBOTTOM","PAGE_SHOWINBREADCRUMB",
                "PAGE_USEREDITABLE","PAGE_ENABLELINK","PAGE_SHOWINSITEMAP","PAGE_CSSCLASS","PAGE_ORDER",
                "PAGE_LANG","PAGE_TEMPLATE","PAGE_TEMPLATEPARENT","PAGE_PARENT","PAGE_KEYWORD",
                "PAGE_DEFAULTKEYWORD","PAGE_DESC","PAGE_DEFAULTDESC","PAGE_REDIRECT","PAGE_REDIRECTLINK",
                "PAGE_REDIRECTTARGET","PAGE_AUTHORIZE","PAGE_ACTIVE","PAGE_CREATEDBY","PAGE_TOPMENUCONTENT",
                "GRP_ID","PAGE_CONTENTPARENT",

                "PAGE_CREATION"
            };
            

            cmd.Parameters.Add("p_PAGE_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_PAGE_DISPLAYNAME", OdbcType.VarChar, 250).Value = strDisplayName;
            cmd.Parameters.Add("p_PAGE_TITLE", OdbcType.VarChar, 250).Value = strTitle;
            cmd.Parameters.Add("p_PAGE_TITLEFURL", OdbcType.VarChar, 250).Value = strTitleFriendlyUrl;
            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strContent;
            cmd.Parameters.Add("p_PAGE_SHOWPAGETITLE", OdbcType.Int, 1).Value = intShowPageTitle;
            cmd.Parameters.Add("p_PAGE_SHOWINMENU", OdbcType.Int, 1).Value = intShowInMenu;
            cmd.Parameters.Add("p_PAGE_SHOWTOP", OdbcType.Int, 1).Value = intShowTop;
            cmd.Parameters.Add("p_PAGE_SHOWBOTTOM", OdbcType.Int, 1).Value = intShowBottom;
            cmd.Parameters.Add("p_PAGE_SHOWINBREADCRUMB", OdbcType.Int, 1).Value = intShowInBreadcrumb;
            cmd.Parameters.Add("p_PAGE_USEREDITABLE", OdbcType.Int, 1).Value = intUserEditable;
            cmd.Parameters.Add("p_PAGE_ENABLELINK", OdbcType.Int, 1).Value = intEnableLink;
            cmd.Parameters.Add("p_PAGE_SHOWINSITEMAP", OdbcType.Int, 1).Value = intShowInSitemap;
            cmd.Parameters.Add("p_PAGE_CSSCLASS", OdbcType.VarChar, 250).Value = strCSSClass;
            cmd.Parameters.Add("p_PAGE_ORDER", OdbcType.Int, 9).Value = intOrder;
            cmd.Parameters.Add("p_PAGE_LANG", OdbcType.VarChar, 250).Value = strLanguage;
            cmd.Parameters.Add("p_PAGE_TEMPLATE", OdbcType.VarChar, 250).Value = strTemplate;
            cmd.Parameters.Add("p_PAGE_TEMPLATEPARENT", OdbcType.Int, 1).Value = intTemplateParent;
            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.BigInt, 9).Value = intParent;
            cmd.Parameters.Add("p_PAGE_KEYWORD", OdbcType.VarChar, 1000).Value = strKeyword;
            cmd.Parameters.Add("p_PAGE_DEFAULTKEYWORD", OdbcType.Int, 1).Value = intDefaultKeyword;
            cmd.Parameters.Add("p_PAGE_DESC", OdbcType.VarChar, 1000).Value = strDesc;
            cmd.Parameters.Add("p_PAGE_DEFAULTDESC", OdbcType.Int, 1).Value = intDefaultDesc;
            cmd.Parameters.Add("p_PAGE_REDIRECT", OdbcType.BigInt, 9).Value = intRedirect;
            cmd.Parameters.Add("p_PAGE_REDIRECTLINK", OdbcType.VarChar, 1000).Value = strRedirectLink;
            cmd.Parameters.Add("p_PAGE_REDIRECTTARGET", OdbcType.VarChar, 250).Value = strTarget;
            cmd.Parameters.Add("p_PAGE_AUTHORIZE", OdbcType.Int, 1).Value = intPageAuthorize;
            cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PAGE_CREATEDBY", OdbcType.Int, 9).Value = intCreatedBy;
            cmd.Parameters.Add("p_PAGE_TOPMENUCONTENT", OdbcType.Text).Value = strTopMenuContent;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intSlideShowGroup;
            cmd.Parameters.Add("p_PAGE_CONTENTPARENT", OdbcType.Int, 1).Value = intContentParent;

            if(intContentParent == 1)
            {
                columns.Insert(columns.Count - 1, "PAGE_MOBILECONTENT");
                cmd.Parameters.Add("p_PAGE_MOBILECONTENT", OdbcType.Text).Value = strContent;
            }

            List<string> parameters = columns.Select((value, index) => (index == columns.Count - 1 ? "NOW()" : "?")).ToList();

            strSql = "INSERT INTO TB_PAGE (" + (string.Join(",", columns.ToArray())) + ")" +
                     " VALUES (" + (string.Join(",", parameters.ToArray())) + ")";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;



            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                id = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addPage(string strTitle, string strTemplate, int intTemplateParent, int intParent, int intShowPageTitle, int intShowInMenu, string strCSSClass, string strKeyword, int intDefaultKeyword, string strDesc, int intDefaultDesc, int intRedirect, int intActive, string strContent, int intCreatedBy, string strTopMenuContent, int intSlideShowGroup)
    {
        int intRecordAffected = 0;

        try
        {
            int intOrder = getOrder(intParent, 0);

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PAGE (" +
                    "PAGE_TITLE, " +
                    "PAGE_CONTENT, " +
                    "PAGE_SHOWPAGETITLE, " +
                    "PAGE_SHOWINMENU, " +
                    "PAGE_CSSCLASS, " +
                    "PAGE_ORDER, " +
                    "PAGE_TEMPLATE, " +
                    "PAGE_TEMPLATEPARENT, " +
                    "PAGE_PARENT, " +
                    "PAGE_KEYWORD, " +
                    "PAGE_DEFAULTKEYWORD, " +
                    "PAGE_DESC, " +
                    "PAGE_DEFAULTDESC, " +
                    "PAGE_REDIRECT, " +
                    "PAGE_ACTIVE, " +
                    "PAGE_CREATEDBY, " +
                    "PAGE_CREATION, " +
                    "PAGE_TOPMENUCONTENT, " +
                    "GRP_ID " +
                    ") VALUES " +
                   "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE(),?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;


            cmd.Parameters.Add("p_PAGE_TITLE", OdbcType.VarChar, 250).Value = strTitle;
            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strContent;
            cmd.Parameters.Add("p_PAGE_SHOWPAGETITLE", OdbcType.Int, 1).Value = intShowPageTitle;
            cmd.Parameters.Add("p_PAGE_SHOWINMENU", OdbcType.Int, 1).Value = intShowInMenu;
            cmd.Parameters.Add("p_PAGE_CSSCLASS", OdbcType.VarChar, 250).Value = strCSSClass;
            cmd.Parameters.Add("p_PAGE_ORDER", OdbcType.Int, 9).Value = intOrder;
            cmd.Parameters.Add("p_PAGE_TEMPLATE", OdbcType.VarChar, 250).Value = strTemplate;
            cmd.Parameters.Add("p_PAGE_TEMPLATEPARENT", OdbcType.Int, 1).Value = intTemplateParent;
            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.BigInt, 9).Value = intParent;
            cmd.Parameters.Add("p_PAGE_KEYWORD", OdbcType.VarChar, 1000).Value = strKeyword;
            cmd.Parameters.Add("p_PAGE_DEFAULTKEYWORD", OdbcType.Int, 1).Value = intDefaultKeyword;
            cmd.Parameters.Add("p_PAGE_DESC", OdbcType.VarChar, 1000).Value = strDesc;
            cmd.Parameters.Add("p_PAGE_DEFAULTDESC", OdbcType.Int, 1).Value = intDefaultDesc;
            cmd.Parameters.Add("p_PAGE_REDIRECT", OdbcType.BigInt, 9).Value = intRedirect;
            cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PAGE_CREATEDBY", OdbcType.Int, 9).Value = intCreatedBy;
            cmd.Parameters.Add("p_PAGE_TOPMENUCONTENT", OdbcType.Text).Value = strTopMenuContent;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intSlideShowGroup;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                id = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addPage2(string strName, string strDisplayName, string strTitle, string strTitleFriendlyUrl, string strTemplate, 
        int intTemplateParent, int intParent, int intShowPageTitle, int intShowInMenu, int intShowTop, 
        int intShowBottom, int intShowInBreadcrumb, int intUserEditable, int intEnableLink, int intShowInSitemap, 
        string strCSSClass, string strKeyword, int intDefaultKeyword, string strDesc, int intDefaultDesc, 
        int intRedirect, string strRedirectLink, string strTarget, int intActive, string strContent, 
        int intCreatedBy, string strTopMenuContent, int intSlideShowGroup, int intPageAuthorize, int intContentParent)
    {
        int intRecordAffected = 0;

        try
        {
            int intOrder = getOrder(intParent, 0);

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<string> columns = new List<string>()
            {
                "PAGE_NAME","PAGE_DISPLAYNAME","PAGE_TITLE","PAGE_TITLEFURL","PAGE_CONTENT",
                "PAGE_SHOWPAGETITLE","PAGE_SHOWINMENU","PAGE_SHOWTOP","PAGE_SHOWBOTTOM","PAGE_SHOWINBREADCRUMB",
                "PAGE_USEREDITABLE","PAGE_ENABLELINK","PAGE_SHOWINSITEMAP","PAGE_CSSCLASS","PAGE_ORDER",
                "PAGE_TEMPLATE","PAGE_TEMPLATEPARENT","PAGE_PARENT","PAGE_KEYWORD","PAGE_DEFAULTKEYWORD",
                "PAGE_DESC","PAGE_DEFAULTDESC","PAGE_REDIRECT","PAGE_REDIRECTLINK","PAGE_REDIRECTTARGET",
                "PAGE_AUTHORIZE","PAGE_ACTIVE","PAGE_CREATEDBY","PAGE_TOPMENUCONTENT","GRP_ID",
                "PAGE_CONTENTPARENT",

                "PAGE_CREATION",
            };

            cmd.Parameters.Add("p_PAGE_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_PAGE_DISPLAYNAME", OdbcType.VarChar, 250).Value = strDisplayName;
            cmd.Parameters.Add("p_PAGE_TITLE", OdbcType.VarChar, 250).Value = strTitle;
            cmd.Parameters.Add("p_PAGE_TITLEFURL", OdbcType.VarChar, 250).Value = strTitleFriendlyUrl;
            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strContent;
            cmd.Parameters.Add("p_PAGE_SHOWPAGETITLE", OdbcType.Int, 1).Value = intShowPageTitle;
            cmd.Parameters.Add("p_PAGE_SHOWINMENU", OdbcType.Int, 1).Value = intShowInMenu;
            cmd.Parameters.Add("p_PAGE_SHOWTOP", OdbcType.Int, 1).Value = intShowTop;
            cmd.Parameters.Add("p_PAGE_SHOWBOTTOM", OdbcType.Int, 1).Value = intShowBottom;
            cmd.Parameters.Add("p_PAGE_SHOWINBREADCRUMB", OdbcType.Int, 1).Value = intShowInBreadcrumb;
            cmd.Parameters.Add("p_PAGE_USEREDITABLE", OdbcType.Int, 1).Value = intUserEditable;
            cmd.Parameters.Add("p_PAGE_ENABLELINK", OdbcType.Int, 1).Value = intEnableLink;
            cmd.Parameters.Add("p_PAGE_SHOWINSITEMAP", OdbcType.Int, 1).Value = intShowInSitemap;
            cmd.Parameters.Add("p_PAGE_CSSCLASS", OdbcType.VarChar, 250).Value = strCSSClass;
            cmd.Parameters.Add("p_PAGE_ORDER", OdbcType.Int, 9).Value = intOrder;
            cmd.Parameters.Add("p_PAGE_TEMPLATE", OdbcType.VarChar, 250).Value = strTemplate;
            cmd.Parameters.Add("p_PAGE_TEMPLATEPARENT", OdbcType.Int, 1).Value = intTemplateParent;
            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.BigInt, 9).Value = intParent;
            cmd.Parameters.Add("p_PAGE_KEYWORD", OdbcType.VarChar, 1000).Value = strKeyword;
            cmd.Parameters.Add("p_PAGE_DEFAULTKEYWORD", OdbcType.Int, 1).Value = intDefaultKeyword;
            cmd.Parameters.Add("p_PAGE_DESC", OdbcType.VarChar, 1000).Value = strDesc;
            cmd.Parameters.Add("p_PAGE_DEFAULTDESC", OdbcType.Int, 1).Value = intDefaultDesc;
            cmd.Parameters.Add("p_PAGE_REDIRECT", OdbcType.BigInt, 9).Value = intRedirect;
            cmd.Parameters.Add("p_PAGE_REDIRECTLINK", OdbcType.VarChar, 1000).Value = strRedirectLink;
            cmd.Parameters.Add("p_PAGE_REDIRECTTARGET", OdbcType.VarChar, 250).Value = strTarget;
            cmd.Parameters.Add("p_PAGE_AUTHORIZE", OdbcType.Int, 1).Value = intPageAuthorize;
            cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PAGE_CREATEDBY", OdbcType.Int, 9).Value = intCreatedBy;
            cmd.Parameters.Add("p_PAGE_TOPMENUCONTENT", OdbcType.Text).Value = strTopMenuContent;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intSlideShowGroup;
            cmd.Parameters.Add("p_PAGE_CONTENTPARENT", OdbcType.Int, 1).Value = intContentParent;

            if (intContentParent == 1)
            {
                columns.Insert(columns.Count - 1, "PAGE_MOBILECONTENT");
                cmd.Parameters.Add("p_PAGE_MOBILECONTENT", OdbcType.Text).Value = strContent;
            }

            List<string> parameters = columns.Select((value, index) => (index == columns.Count - 1 ? "NOW()" : "?")).ToList();

            strSql = "INSERT INTO TB_PAGE (" + (string.Join(",", columns.ToArray())) + ")" +
                     " VALUES (" + (string.Join(",", parameters.ToArray())) + ")";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;



            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                id = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addPage(int intId, int intSelId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PAGECORRESPOND (" +
                    "PAGE_ID, " +
                    "PC_PAGEID" +
                    ") VALUES " +
                   "(?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;


            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;
            cmd.Parameters.Add("p_PC_PAGEID", OdbcType.BigInt, 9).Value = intSelId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                id = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int getOrder(int intParent, int intId)
    {
        int intOrder = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT PAGE_ORDER FROM TB_PAGE WHERE PAGE_PARENT = ?";

            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.Int, 9).Value = intParent;

            if (intId != 0)
            {
                strSql += " AND PAGE_ID != ?";
                cmd.Parameters.Add("p_PAGE_ID", OdbcType.Int, 9).Value = intId;
            }

            strSql += " ORDER BY PAGE_ORDER DESC LIMIT 1";

            //strSql = "SELECT PAGE_ORDER FROM TB_PAGE WHERE PAGE_PARENT = ? ORDER BY PAGE_ORDER DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.Int, 9).Value = intParent;
            intOrder = Convert.ToInt16(cmd.ExecuteScalar()) + 1;

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intOrder;
    }

    public Boolean isOther(int intId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT COUNT(*) FROM TB_PAGE WHERE PAGE_ID = ? AND PAGE_OTHER = 1";

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolFound = true; }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractPageById(int intId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PAGE_ID, A.PAGE_NAME, A.PAGE_DISPLAYNAME, A.PAGE_TITLE, A.PAGE_TITLEFURL, A.PAGE_CONTENT, A.PAGE_SHOWPAGETITLE, A.PAGE_SHOWINMENU, A.PAGE_SHOWTOP, A.PAGE_SHOWBOTTOM," +
                     " A.PAGE_USEREDITABLE, A.PAGE_ENABLELINK, A.PAGE_SHOWINSITEMAP, A.PAGE_CSSCLASS, A.PAGE_ORDER, A.PAGE_LANG," +
                     " (SELECT B.LIST_NAME FROM TB_LIST B WHERE A.PAGE_LANG = B.LIST_VALUE AND B.LIST_DEFAULT = 1 AND B.LIST_GROUP = 'LANGUAGE') AS V_LANGNAME, A.PAGE_SHOWINBREADCRUMB," +
                     " A.PAGE_TEMPLATE, A.PAGE_TEMPLATEPARENT, C.PAGE_TEMPLATE AS V_TEMPLATEPARENT, A.PAGE_PARENT, C.PAGE_TITLE AS V_PARENTNAME, C.PAGE_CSSCLASS AS V_PARENTCSSCLASS, A.PAGE_KEYWORD," +
                     " A.PAGE_DEFAULTKEYWORD, A.PAGE_DESC, A.PAGE_DEFAULTDESC, A.PAGE_REDIRECT, (SELECT D.PAGE_NAME FROM TB_PAGE D WHERE A.PAGE_REDIRECT = D.PAGE_ID) AS V_REDIRECTNAME," +
                     " A.PAGE_ACTIVE, A.PAGE_OTHER, A.PAGE_CREATEDBY, A.PAGE_CREATION, A.PAGE_UPDATEDBY, A.PAGE_LASTUPDATE, A.PAGE_REDIRECTLINK, A.PAGE_REDIRECTTARGET, A.PAGE_TOPMENUCONTENT, A.GRP_ID," +
                     " A.PAGE_SHOWMOBILEVIEW,A.PAGE_MOBILESHOWINMENU,A.PAGE_MOBILESHOWTOP,A.PAGE_MOBILESHOWBOTTOM,A.PAGE_MOBILECSSCLASS, A.PAGE_MOBILETEMPLATE, A.PAGE_MOBILECONTENT, A.PAGE_MOBILEREDIRECT," +
                     " (SELECT E.PAGE_NAME FROM TB_PAGE E WHERE A.PAGE_SHOWMOBILEVIEW = 1 AND A.PAGE_MOBILEREDIRECT = E.PAGE_ID) AS V_MOBILEREDIRECTNAME, A.PAGE_MOBILEREDIRECTLINK," +
                     " A.PAGE_MOBILEREDIRECTTARGET, A.PAGE_AUTHORIZE, A.PAGE_CONTENTPARENT" +
                     " FROM (TB_PAGE A LEFT JOIN TB_PAGE C ON (A.PAGE_PARENT = C.PAGE_ID))" +
                     " WHERE A.PAGE_ID = ? ";

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;

            if (intActive != 0)
            {
                strSql += " AND A.PAGE_ACTIVE = ?";
                cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                id = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_ID"].ToString());
                name = dataSet.Tables[0].Rows[0]["PAGE_NAME"].ToString();
                displayName = dataSet.Tables[0].Rows[0]["PAGE_DISPLAYNAME"].ToString();
                title = dataSet.Tables[0].Rows[0]["PAGE_TITLE"].ToString();
                titleFriendlyUrl = dataSet.Tables[0].Rows[0]["PAGE_TITLEFURL"].ToString();
                content = dataSet.Tables[0].Rows[0]["PAGE_CONTENT"].ToString();
                showPageTitle = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWPAGETITLE"]);
                showInMenu = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWINMENU"]);
                showTop = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWTOP"]);
                showBottom = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWBOTTOM"]);
                showInBreadcrumb = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWINBREADCRUMB"]);
                userEditable = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_USEREDITABLE"]);
                enableLink = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_ENABLELINK"]);
                showInSitemap = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWINSITEMAP"]);
                cssClass = dataSet.Tables[0].Rows[0]["PAGE_CSSCLASS"].ToString();
                order = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_ORDER"]);
                lang = dataSet.Tables[0].Rows[0]["PAGE_LANG"].ToString();
                langName = dataSet.Tables[0].Rows[0]["V_LANGNAME"].ToString();
                template = dataSet.Tables[0].Rows[0]["PAGE_TEMPLATE"].ToString();
                templateParent = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_TEMPLATEPARENT"].ToString());
                templateParentName = dataSet.Tables[0].Rows[0]["V_TEMPLATEPARENT"].ToString();
                parent = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_PARENT"].ToString());
                parentName = dataSet.Tables[0].Rows[0]["V_PARENTNAME"].ToString();
                parentCSSClass = dataSet.Tables[0].Rows[0]["V_PARENTCSSCLASS"].ToString();
                keyword = dataSet.Tables[0].Rows[0]["PAGE_KEYWORD"].ToString();
                defaultKeyword = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_DEFAULTKEYWORD"]);
                desc = dataSet.Tables[0].Rows[0]["PAGE_DESC"].ToString();
                defaultDesc = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_DEFAULTDESC"]);
                redirect = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_REDIRECT"].ToString());
                redirectName = dataSet.Tables[0].Rows[0]["V_REDIRECTNAME"].ToString();
                active = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_ACTIVE"]);
                other = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_OTHER"]);
                createdBy = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_CREATEDBY"].ToString());
                creation = dataSet.Tables[0].Rows[0]["PAGE_CREATION"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PAGE_CREATION"].ToString()) : DateTime.MaxValue; ;
                updatedBy = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_UPDATEDBY"].ToString());
                lastUpdate = dataSet.Tables[0].Rows[0]["PAGE_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PAGE_LASTUPDATE"].ToString()) : DateTime.MaxValue;
                redirectLink = dataSet.Tables[0].Rows[0]["PAGE_REDIRECTLINK"].ToString();
                redirectTarget = dataSet.Tables[0].Rows[0]["PAGE_REDIRECTTARGET"].ToString();
                topMenuContent = dataSet.Tables[0].Rows[0]["PAGE_TOPMENUCONTENT"].ToString();
                slideshowGroup = Convert.ToInt16(dataSet.Tables[0].Rows[0]["GRP_ID"]);
                showMobileView = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_SHOWMOBILEVIEW"].ToString());
                mobileTemplate = dataSet.Tables[0].Rows[0]["PAGE_MOBILETEMPLATE"].ToString();
                mobileContent = dataSet.Tables[0].Rows[0]["PAGE_MOBILECONTENT"].ToString();
                mobileShowInMenu = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_MOBILESHOWINMENU"]);
                mobileShowTop = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_MOBILESHOWTOP"]);
                mobileShowBottom = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_MOBILESHOWBOTTOM"]);
                mobileCssClass = dataSet.Tables[0].Rows[0]["PAGE_MOBILECSSCLASS"].ToString();
                mobileRedirect = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_MOBILEREDIRECT"].ToString());
                mobileRedirectName = dataSet.Tables[0].Rows[0]["V_MOBILEREDIRECTNAME"].ToString();
                mobileExternalLink = dataSet.Tables[0].Rows[0]["PAGE_MOBILEREDIRECTLINK"].ToString();
                mobileTarget = dataSet.Tables[0].Rows[0]["PAGE_MOBILEREDIRECTTARGET"].ToString();
                pageAuthorize = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_AUTHORIZE"]);
                contentParent = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_CONTENTPARENT"]);
            }
           
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    public Boolean extractPageById_Page(int intId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PAGE_ID, A.PAGE_NAME, A.PAGE_DISPLAYNAME, A.PAGE_TITLE, A.PAGE_SHOWPAGETITLE, A.PAGE_ACTIVE, " +
                     " A.PAGE_PARENT, A.PAGE_KEYWORD, A.PAGE_DEFAULTKEYWORD, A.PAGE_DESC, A.PAGE_DEFAULTDESC, A.GRP_ID " +
                     " FROM TB_PAGE A" +
                     " WHERE A.PAGE_ID = ? ";

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;

            if (intActive != 0)
            {
                strSql += " AND A.PAGE_ACTIVE = ?";
                cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                id = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_ID"].ToString());
                name = dataSet.Tables[0].Rows[0]["PAGE_NAME"].ToString();
                displayName = dataSet.Tables[0].Rows[0]["PAGE_DISPLAYNAME"].ToString();
                title = dataSet.Tables[0].Rows[0]["PAGE_TITLE"].ToString();
                showPageTitle = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWPAGETITLE"]);
                parent = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_PARENT"].ToString());
                keyword = dataSet.Tables[0].Rows[0]["PAGE_KEYWORD"].ToString();
                defaultKeyword = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_DEFAULTKEYWORD"]);
                desc = dataSet.Tables[0].Rows[0]["PAGE_DESC"].ToString();
                defaultDesc = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_DEFAULTDESC"]);
                active = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_ACTIVE"]);
                grpid = int.Parse(dataSet.Tables[0].Rows[0]["GRP_ID"].ToString());
            }

        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    public Boolean extractFirstPage(int intParent, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PAGE_ID, A.PAGE_NAME, A.PAGE_DISPLAYNAME, A.PAGE_TITLE, A.PAGE_CONTENT, A.PAGE_SHOWPAGETITLE, A.PAGE_SHOWINMENU, A.PAGE_SHOWTOP, A.PAGE_SHOWBOTTOM, A.PAGE_USEREDITABLE, A.PAGE_ENABLELINK, A.PAGE_CSSCLASS, A.PAGE_ORDER," +
                     " A.PAGE_LANG, (SELECT B.LIST_NAME FROM TB_LIST B WHERE A.PAGE_LANG = B.LIST_VALUE AND B.LIST_DEFAULT = 1 AND B.LIST_GROUP = 'LANGUAGE') AS V_LANGNAME," +
                     " A.PAGE_TEMPLATE, A.PAGE_TEMPLATEPARENT, C.PAGE_TEMPLATE AS V_TEMPLATEPARENT, A.PAGE_PARENT, C.PAGE_TITLE AS V_PARENTNAME, C.PAGE_CSSCLASS AS V_PARENTCSSCLASS, A.PAGE_KEYWORD, A.PAGE_DEFAULTKEYWORD, A.PAGE_DESC, A.PAGE_DEFAULTDESC," +
                     " A.PAGE_REDIRECT, (SELECT D.PAGE_NAME FROM TB_PAGE D WHERE A.PAGE_REDIRECT = D.PAGE_ID) AS V_REDIRECTNAME, A.PAGE_ACTIVE, A.PAGE_OTHER, A.PAGE_CREATEDBY, A.PAGE_CREATION, A.PAGE_UPDATEDBY, A.PAGE_LASTUPDATE, A.PAGE_REDIRECTLINK, A.PAGE_REDIRECTTARGET" +
                     " FROM (TB_PAGE A LEFT JOIN TB_PAGE C ON (A.PAGE_PARENT = C.PAGE_ID))" + 
                     " WHERE A.PAGE_PARENT = ?";

            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.BigInt, 9).Value = intParent;

            if (intActive != 0)
            {
                strSql += " AND A.PAGE_ACTIVE = ?";
                cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " ORDER BY A.PAGE_ORDER ASC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                id = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_ID"].ToString());
                title = dataSet.Tables[0].Rows[0]["PAGE_TITLE"].ToString();
                content = dataSet.Tables[0].Rows[0]["PAGE_CONTENT"].ToString();
                showPageTitle = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWPAGETITLE"]);
                showInMenu = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWINMENU"]);
                cssClass = dataSet.Tables[0].Rows[0]["PAGE_CSSCLASS"].ToString();
                order = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_ORDER"]);
                lang = dataSet.Tables[0].Rows[0]["PAGE_LANG"].ToString();
                langName = dataSet.Tables[0].Rows[0]["V_LANGNAME"].ToString();
                template = dataSet.Tables[0].Rows[0]["PAGE_TEMPLATE"].ToString();
                templateParent = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_TEMPLATEPARENT"].ToString());
                templateParentName = dataSet.Tables[0].Rows[0]["V_TEMPLATEPARENT"].ToString();
                parent = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_PARENT"].ToString());
                parentName = dataSet.Tables[0].Rows[0]["V_PARENTNAME"].ToString();
                parentCSSClass = dataSet.Tables[0].Rows[0]["V_PARENTCSSCLASS"].ToString();
                keyword = dataSet.Tables[0].Rows[0]["PAGE_KEYWORD"].ToString();
                defaultKeyword = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_DEFAULTKEYWORD"]);
                desc = dataSet.Tables[0].Rows[0]["PAGE_DESC"].ToString();
                defaultDesc = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_DEFAULTDESC"]);
                redirect = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_REDIRECT"].ToString());
                redirectName = dataSet.Tables[0].Rows[0]["V_REDIRECTNAME"].ToString();
                active = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_ACTIVE"]);
                other = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_OTHER"]);
                createdBy = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_CREATEDBY"].ToString());
                creation = dataSet.Tables[0].Rows[0]["PAGE_CREATION"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PAGE_CREATION"].ToString()) : DateTime.MaxValue; ;
                updatedBy = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_UPDATEDBY"].ToString());
                lastUpdate = dataSet.Tables[0].Rows[0]["PAGE_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PAGE_LASTUPDATE"].ToString()) : DateTime.MaxValue;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractPrevPage(int intParent, int intOrder, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PAGE_ID, A.PAGE_NAME, A.PAGE_DISPLAYNAME, A.PAGE_TITLE, A.PAGE_CONTENT, A.PAGE_SHOWPAGETITLE, A.PAGE_SHOWINMENU, A.PAGE_SHOWTOP, A.PAGE_SHOWBOTTOM, A.PAGE_USEREDITABLE, A.PAGE_ENABLELINK, A.PAGE_CSSCLASS, A.PAGE_ORDER," +
                     " A.PAGE_LANG, (SELECT B.LIST_NAME FROM TB_LIST B WHERE A.PAGE_LANG = B.LIST_VALUE AND B.LIST_DEFAULT = 1 AND B.LIST_GROUP = 'LANGUAGE') AS V_LANGNAME, A.PAGE_SHOWINBREADCRUMB," +
                     " A.PAGE_TEMPLATE, A.PAGE_TEMPLATEPARENT, C.PAGE_TEMPLATE AS V_TEMPLATEPARENT, A.PAGE_PARENT, C.PAGE_TITLE AS V_PARENTNAME, C.PAGE_CSSCLASS AS V_PARENTCSSCLASS, A.PAGE_KEYWORD, A.PAGE_DEFAULTKEYWORD, A.PAGE_DESC, A.PAGE_DEFAULTDESC," +
                     " A.PAGE_REDIRECT, (SELECT D.PAGE_NAME FROM TB_PAGE D WHERE A.PAGE_REDIRECT = D.PAGE_ID) AS V_REDIRECTNAME, A.PAGE_ACTIVE, A.PAGE_OTHER, A.PAGE_CREATEDBY, A.PAGE_CREATION, A.PAGE_UPDATEDBY, A.PAGE_LASTUPDATE, A.PAGE_REDIRECTLINK, A.PAGE_REDIRECTTARGET" +
                     " FROM (TB_PAGE A LEFT JOIN TB_PAGE C ON (A.PAGE_PARENT = C.PAGE_ID))" + 
                     " WHERE A.PAGE_PARENT = ? AND A.PAGE_ORDER < ?";

            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.BigInt, 9).Value = intParent;
            cmd.Parameters.Add("p_PAGE_ORDER", OdbcType.Int, 9).Value = intOrder;

            if (intActive != 0)
            {
                strSql += " AND A.PAGE_ACTIVE = ?";
                cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " ORDER BY A.PAGE_ORDER DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                id = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_ID"]);
                name = dataSet.Tables[0].Rows[0]["PAGE_NAME"].ToString();
                displayName = dataSet.Tables[0].Rows[0]["PAGE_DISPLAYNAME"].ToString();
                title = dataSet.Tables[0].Rows[0]["PAGE_TITLE"].ToString();
                content = dataSet.Tables[0].Rows[0]["PAGE_CONTENT"].ToString();
                showPageTitle = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWPAGETITLE"]);
                showInMenu = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWINMENU"]);
                showInBreadcrumb = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWINBREADCRUMB"]);
                cssClass = dataSet.Tables[0].Rows[0]["PAGE_CSSCLASS"].ToString();
                order = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_ORDER"]);
                lang = dataSet.Tables[0].Rows[0]["PAGE_LANG"].ToString();
                langName = dataSet.Tables[0].Rows[0]["V_LANGNAME"].ToString();
                template = dataSet.Tables[0].Rows[0]["PAGE_TEMPLATE"].ToString();
                templateParent = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_TEMPLATEPARENT"]);
                templateParentName = dataSet.Tables[0].Rows[0]["V_TEMPLATEPARENT"].ToString();
                parent = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_PARENT"]);
                parentName = dataSet.Tables[0].Rows[0]["V_PARENTNAME"].ToString();
                parentCSSClass = dataSet.Tables[0].Rows[0]["V_PARENTCSSCLASS"].ToString();
                keyword = dataSet.Tables[0].Rows[0]["PAGE_KEYWORD"].ToString();
                defaultKeyword = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_DEFAULTKEYWORD"]);
                desc = dataSet.Tables[0].Rows[0]["PAGE_DESC"].ToString();
                defaultDesc = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_DEFAULTDESC"]);
                redirect = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_REDIRECT"]);
                redirectName = dataSet.Tables[0].Rows[0]["V_REDIRECTNAME"].ToString();
                active = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_ACTIVE"]);
                other = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_OTHER"]);
                createdBy = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_CREATEDBY"]);
                creation = dataSet.Tables[0].Rows[0]["PAGE_CREATION"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PAGE_CREATION"].ToString()) : DateTime.MaxValue; ;
                updatedBy = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_UPDATEDBY"]);
                lastUpdate = dataSet.Tables[0].Rows[0]["PAGE_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PAGE_LASTUPDATE"].ToString()) : DateTime.MaxValue;
                redirectLink = dataSet.Tables[0].Rows[0]["PAGE_REDIRECTLINK"].ToString();
                redirectTarget = dataSet.Tables[0].Rows[0]["PAGE_REDIRECTTARGET"].ToString();
            }

        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractNextPage(int intParent, int intOrder, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PAGE_ID, A.PAGE_NAME, A.PAGE_DISPLAYNAME, A.PAGE_TITLE, A.PAGE_CONTENT, A.PAGE_SHOWPAGETITLE, A.PAGE_SHOWINMENU, A.PAGE_SHOWTOP, A.PAGE_SHOWBOTTOM, A.PAGE_USEREDITABLE, A.PAGE_ENABLELINK, A.PAGE_CSSCLASS, A.PAGE_ORDER," +
                     " A.PAGE_LANG, (SELECT B.LIST_NAME FROM TB_LIST B WHERE A.PAGE_LANG = B.LIST_VALUE AND B.LIST_DEFAULT = 1 AND B.LIST_GROUP = 'LANGUAGE') AS V_LANGNAME, A.PAGE_SHOWINBREADCRUMB," +
                     " A.PAGE_TEMPLATE, A.PAGE_TEMPLATEPARENT, C.PAGE_TEMPLATE AS V_TEMPLATEPARENT, A.PAGE_PARENT, C.PAGE_TITLE AS V_PARENTNAME, C.PAGE_CSSCLASS AS V_PARENTCSSCLASS, A.PAGE_KEYWORD, A.PAGE_DEFAULTKEYWORD, A.PAGE_DESC, A.PAGE_DEFAULTDESC," +
                     " A.PAGE_REDIRECT, (SELECT D.PAGE_NAME FROM TB_PAGE D WHERE A.PAGE_REDIRECT = D.PAGE_ID) AS V_REDIRECTNAME, A.PAGE_ACTIVE, A.PAGE_OTHER, A.PAGE_CREATEDBY, A.PAGE_CREATION, A.PAGE_UPDATEDBY, A.PAGE_LASTUPDATE, A.PAGE_REDIRECTLINK, A.PAGE_REDIRECTTARGET" +
                     " FROM (TB_PAGE A LEFT JOIN TB_PAGE C ON (A.PAGE_PARENT = C.PAGE_ID))" +
                     " WHERE A.PAGE_PARENT = ? AND A.PAGE_ORDER > ?";

            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.BigInt, 9).Value = intParent;
            cmd.Parameters.Add("p_PAGE_ORDER", OdbcType.Int, 9).Value = intOrder;

            if (intActive != 0)
            {
                strSql += " AND A.PAGE_ACTIVE = ?";
                cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " ORDER BY A.PAGE_ORDER ASC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                id = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_ID"]);
                name = dataSet.Tables[0].Rows[0]["PAGE_NAME"].ToString();
                displayName = dataSet.Tables[0].Rows[0]["PAGE_DISPLAYNAME"].ToString();
                title = dataSet.Tables[0].Rows[0]["PAGE_TITLE"].ToString();
                content = dataSet.Tables[0].Rows[0]["PAGE_CONTENT"].ToString();
                showPageTitle = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWPAGETITLE"]);
                showInMenu = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWINMENU"]);
                showInBreadcrumb = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWINBREADCRUMB"]);
                cssClass = dataSet.Tables[0].Rows[0]["PAGE_CSSCLASS"].ToString();
                order = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_ORDER"]);
                lang = dataSet.Tables[0].Rows[0]["PAGE_LANG"].ToString();
                langName = dataSet.Tables[0].Rows[0]["V_LANGNAME"].ToString();
                template = dataSet.Tables[0].Rows[0]["PAGE_TEMPLATE"].ToString();
                templateParent = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_TEMPLATEPARENT"]);
                templateParentName = dataSet.Tables[0].Rows[0]["V_TEMPLATEPARENT"].ToString();
                parent = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_PARENT"]);
                parentName = dataSet.Tables[0].Rows[0]["V_PARENTNAME"].ToString();
                parentCSSClass = dataSet.Tables[0].Rows[0]["V_PARENTCSSCLASS"].ToString();
                keyword = dataSet.Tables[0].Rows[0]["PAGE_KEYWORD"].ToString();
                defaultKeyword = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_DEFAULTKEYWORD"]);
                desc = dataSet.Tables[0].Rows[0]["PAGE_DESC"].ToString();
                defaultDesc = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_DEFAULTDESC"]);
                redirect = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_REDIRECT"]);
                redirectName = dataSet.Tables[0].Rows[0]["V_REDIRECTNAME"].ToString();
                active = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_ACTIVE"]);
                other = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_OTHER"]);
                createdBy = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_CREATEDBY"]);
                creation = dataSet.Tables[0].Rows[0]["PAGE_CREATION"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PAGE_CREATION"].ToString()) : DateTime.MaxValue; ;
                updatedBy = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_UPDATEDBY"]);
                lastUpdate = dataSet.Tables[0].Rows[0]["PAGE_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PAGE_LASTUPDATE"].ToString()) : DateTime.MaxValue;
                redirectLink = dataSet.Tables[0].Rows[0]["PAGE_REDIRECTLINK"].ToString();
                redirectTarget = dataSet.Tables[0].Rows[0]["PAGE_REDIRECTTARGET"].ToString();
            }

        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    public void getNextPrevPageId(int intId,out int prevId,out int nextId)
    {
        DataSet ds = new DataSet();
        prevId = nextId = 0;
        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_PAGE ORDER BY PAGE_ORDER ASC";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            if(ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for(int i=0;i< ds.Tables[0].Rows.Count; i++)
                {
                    DataRow dw = ds.Tables[0].Rows[i];
                    if (int.Parse(dw["PAGE_ID"].ToString()) == intId)
                    {
                        if(i!=0) { prevId = int.Parse(ds.Tables[0].Rows[i-1]["PAGE_ID"].ToString()); }
                        if (i != ds.Tables[0].Rows.Count - 1) { nextId = int.Parse(ds.Tables[0].Rows[i+1]["PAGE_ID"].ToString()); }
                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }
    }

    public DataSet getPageList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PAGE_ID, A.PAGE_NAME, A.PAGE_DISPLAYNAME, A.PAGE_TITLE, A.PAGE_CONTENT, A.PAGE_SHOWPAGETITLE, A.PAGE_SHOWINMENU, A.PAGE_SHOWTOP, A.PAGE_SHOWBOTTOM, A.PAGE_USEREDITABLE, A.PAGE_ENABLELINK, A.PAGE_SHOWINSITEMAP, A.PAGE_CSSCLASS, A.PAGE_ORDER," +
                     " A.PAGE_LANG, (SELECT B.LIST_NAME FROM TB_LIST B WHERE A.PAGE_LANG = B.LIST_VALUE AND B.LIST_DEFAULT = 1 AND B.LIST_GROUP = 'LANGUAGE') AS V_LANGNAME," +
                     " A.PAGE_TEMPLATE, A.PAGE_TEMPLATEPARENT, C.PAGE_TEMPLATE AS V_TEMPLATEPARENT, A.PAGE_PARENT, C.PAGE_TITLE AS V_PARENTNAME, C.PAGE_CSSCLASS AS V_PARENTCSSCLASS, A.PAGE_KEYWORD, A.PAGE_DEFAULTKEYWORD, A.PAGE_DESC, A.PAGE_DEFAULTDESC," +
                     " A.PAGE_REDIRECT, (SELECT D.PAGE_NAME FROM TB_PAGE D WHERE A.PAGE_REDIRECT = D.PAGE_ID) AS V_REDIRECTNAME, A.PAGE_ACTIVE, A.PAGE_OTHER, A.PAGE_CREATEDBY, A.PAGE_CREATION, A.PAGE_UPDATEDBY, A.PAGE_LASTUPDATE, A.PAGE_REDIRECTLINK, A.PAGE_REDIRECTTARGET, A.PAGE_SHOWMOBILEVIEW" +
                     " FROM (TB_PAGE A LEFT JOIN TB_PAGE C ON (A.PAGE_PARENT = C.PAGE_ID))";

            if (intActive != 0)
            {
                strSql += " WHERE A.PAGE_ACTIVE = ?";
                cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    //Dictionary<string, string> keyword, Dictionary<string, string> filterLike,Dictionary<string, string> filterAnd,string sorting, int start, int length
    public DataSet getPageDataTables()
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            List<string> columns = new List<string>()
            {
                "A.PAGE_ID","A.PAGE_NAME", "A.PAGE_DISPLAYNAME", "A.PAGE_TITLE", "A.PAGE_CONTENT",
                "A.PAGE_SHOWPAGETITLE", "A.PAGE_SHOWINMENU","A.PAGE_SHOWTOP", "A.PAGE_SHOWBOTTOM", "A.PAGE_USEREDITABLE",
                "A.PAGE_ENABLELINK", "A.PAGE_SHOWINSITEMAP", "A.PAGE_CSSCLASS", "A.PAGE_ORDER","A.PAGE_LANG",
                "A.PAGE_TEMPLATE", "A.PAGE_TEMPLATEPARENT","A.PAGE_REDIRECT", "A.PAGE_KEYWORD", "A.PAGE_DEFAULTKEYWORD",
                "A.PAGE_DESC", "A.PAGE_DEFAULTDESC","A.PAGE_ACTIVE","A.PAGE_OTHER", "A.PAGE_CREATEDBY",
                "A.PAGE_CREATION", "A.PAGE_UPDATEDBY", "A.PAGE_LASTUPDATE", "A.PAGE_REDIRECTLINK", "A.PAGE_REDIRECTTARGET",
                "A.PAGE_SHOWMOBILEVIEW", "A.PAGE_PARENT",

                "C.PAGE_TEMPLATE AS V_TEMPLATEPARENT", "C.PAGE_TITLE AS V_PARENTNAME", "C.PAGE_CSSCLASS AS V_PARENTCSSCLASS",

                "(SELECT B.LIST_NAME FROM TB_LIST B WHERE A.PAGE_LANG = B.LIST_VALUE AND B.LIST_DEFAULT = 1 AND B.LIST_GROUP = 'LANGUAGE') AS V_LANGNAME",
                "(SELECT D.PAGE_NAME FROM TB_PAGE D WHERE A.PAGE_REDIRECT = D.PAGE_ID) AS V_REDIRECTNAME",   
            };

            string strSql = "SELECT " + string.Join(",",columns.ToArray()) + " FROM TB_PAGE A LEFT JOIN TB_PAGE C ON A.PAGE_PARENT = C.PAGE_ID WHERE 1=1";

            //if (keyword != null)
            //{
            //    List<string> temp = new List<string>();
            //    foreach (KeyValuePair<string, string> entry in keyword)
            //    {
            //        temp.Add(entry.Key + " LIKE ?");
            //        cmd.Parameters.AddWithValue("k_" + entry.Key, "%" + entry.Value + "%");
            //    }
            //    strSql += " AND (" + string.Join(" OR ", temp.ToArray()) + ")";
            //}

            //if (filterLike != null)
            //{
            //    foreach (KeyValuePair<string, string> entry in filterLike)
            //    {
            //        strSql += " AND " + entry.Key + " LIKE ?";
            //        cmd.Parameters.AddWithValue("fl_" + entry.Key, "%" + entry.Value + "%");
            //    }
                
            //}

            //if (filterAnd != null)
            //{
            //    foreach (KeyValuePair<string, string> entry in filterAnd)
            //    {
            //        strSql += " AND " + entry.Key + " = ?";
            //        cmd.Parameters.AddWithValue("fa_" + entry.Key, "" + entry.Value + "");
            //    }
            //}

            //if (sorting != null)
            //{
            //    strSql += " ORDER BY " + sorting;
            //}
            //if (length > 0)
            //{
            //    strSql += " LIMIT " + start + "," + length + "";
            //}


            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public int getPageDataTablesCount(Dictionary<string, string> keyword, 
                                     Dictionary<string, string> filterLike,
                                     Dictionary<string, string> filterAnd)
    {
        int count = 0;
        try
        {
            OdbcCommand cmd = new OdbcCommand();
            List<string> columns = new List<string>()
            {
                "A.PAGE_ID","A.PAGE_NAME", "A.PAGE_DISPLAYNAME", "A.PAGE_TITLE", "A.PAGE_CONTENT",
                "A.PAGE_SHOWPAGETITLE", "A.PAGE_SHOWINMENU","A.PAGE_SHOWTOP", "A.PAGE_SHOWBOTTOM", "A.PAGE_USEREDITABLE",
                "A.PAGE_ENABLELINK", "A.PAGE_SHOWINSITEMAP", "A.PAGE_CSSCLASS", "A.PAGE_ORDER","A.PAGE_LANG",
                "A.PAGE_TEMPLATE", "A.PAGE_TEMPLATEPARENT","A.PAGE_REDIRECT", "A.PAGE_KEYWORD", "A.PAGE_DEFAULTKEYWORD",
                "A.PAGE_DESC", "A.PAGE_DEFAULTDESC","A.PAGE_ACTIVE","A.PAGE_OTHER", "A.PAGE_CREATEDBY",
                "A.PAGE_CREATION", "A.PAGE_UPDATEDBY", "A.PAGE_LASTUPDATE", "A.PAGE_REDIRECTLINK", "A.PAGE_REDIRECTTARGET",
                "A.PAGE_SHOWMOBILEVIEW", "A.PAGE_PARENT",

                "C.PAGE_TEMPLATE AS V_TEMPLATEPARENT", "C.PAGE_TITLE AS V_PARENTNAME", "C.PAGE_CSSCLASS AS V_PARENTCSSCLASS",

                "(SELECT B.LIST_NAME FROM TB_LIST B WHERE A.PAGE_LANG = B.LIST_VALUE AND B.LIST_DEFAULT = 1 AND B.LIST_GROUP = 'LANGUAGE') AS V_LANGNAME",
                "(SELECT D.PAGE_NAME FROM TB_PAGE D WHERE A.PAGE_REDIRECT = D.PAGE_ID) AS V_REDIRECTNAME",
            };

            string strSql = "SELECT COUNT(*) FROM (SELECT " + string.Join(",", columns.ToArray()) + " FROM (TB_PAGE A LEFT JOIN TB_PAGE C ON (A.PAGE_PARENT = C.PAGE_ID))) TB_PAGE WHERE 1=1";

            if (keyword != null)
            {
                List<string> temp = new List<string>();
                foreach (KeyValuePair<string, string> entry in keyword)
                {
                    temp.Add(entry.Key + " LIKE ?");
                    cmd.Parameters.AddWithValue("k_" + entry.Key, "%" + entry.Value + "%");
                }
                strSql += " AND (" + string.Join(" OR ", temp.ToArray()) + ")";
            }

            if (filterLike != null)
            {
                foreach (KeyValuePair<string, string> entry in filterLike)
                {
                    strSql += " AND " + entry.Key + " LIKE ?";
                    cmd.Parameters.AddWithValue("fl_" + entry.Key, "%" + entry.Value + "%");
                }

            }

            if (filterAnd != null)
            {
                foreach (KeyValuePair<string, string> entry in filterAnd)
                {
                    strSql += " AND " + entry.Key + " = ?";
                    cmd.Parameters.AddWithValue("fa_" + entry.Key, "" + entry.Value + "");
                }

            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            count = Convert.ToInt16(cmd.ExecuteScalar());
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return count;
    }
    public DataSet getPagePageListById(int intId)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PAGE_ID, A.PC_PAGEID, A.V_PAGETITLE, A.V_PAGEORDER, A.V_PAGELANG, A.V_PAGETEMPLATE" +
                     " FROM VW_PAGECORRESPOND A" +
                     " WHERE A.PAGE_ID = ?";

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean isItemExist(int intId, int intSelId)
    {
        Boolean boolExist = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PAGECORRESPOND" +
                     " WHERE PAGE_ID = ?" +
                     " AND PC_PAGEID = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;
            cmd.Parameters.Add("p_PC_PAGEID", OdbcType.BigInt, 9).Value = intSelId;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1)
            {
                boolExist = true;
            }

            cmd.Dispose();
        }
        catch (ExecutionEngineException ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public Boolean isPageTitleFriendlyUrlExist(string strPageTitleFriendlyUrl, int intPageId)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_PAGE" +
                     " WHERE PAGE_TITLEFURL = ?";

            cmd.Parameters.Add("p_PAGE_TITLEFURL", OdbcType.VarChar, 250).Value = strPageTitleFriendlyUrl;

            if (intPageId > 0)
            {
                strSql += " AND PAGE_ID != ?";
                cmd.Parameters.Add("p_PAGE_ID", OdbcType.Int, 9).Value = intPageId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExist;
    }

    public Boolean isExactSameDataSet(int intId, string strName, string strDisplayName, string strTitle, string strTitleFriendlyUrl, 
        string strContent, int intShowPageTitle, int intShowInMenu, int intShowTop, int intShowBottom, 
        int intShowInBreadcrumb, int intUserEditable, int intEnableLink, int intShowInSitemap, string strCSSClass, 
        string strLang, string strTemplate, int intTemplateParent, int intParent, string strKeyword, 
        int intDefaultKeyword, string strDesc, int intDefaultDesc, int intRedirect, string strRedirectLink, 
        string strTarget, int intActive, string strTopMenuContent, int intSlideShowGroup, int intPageAuthorize,
        int intContentParent)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PAGE" +
                     " WHERE PAGE_ID = ?" +
                     " AND BINARY PAGE_NAME = ?" +
                     " AND BINARY PAGE_DISPLAYNAME = ?" +
                     " AND BINARY PAGE_TITLE = ?" +
                     " AND BINARY PAGE_TITLEFURL = ?" +
                     " AND BINARY PAGE_CONTENT = ?" +
                     " AND PAGE_SHOWPAGETITLE = ?" +
                     " AND PAGE_SHOWINMENU = ?" +
                     " AND PAGE_SHOWTOP = ?" +
                     " AND PAGE_SHOWBOTTOM = ?" +
                     " AND PAGE_SHOWINBREADCRUMB = ?" +
                     " AND PAGE_USEREDITABLE = ?" +
                     " AND PAGE_ENABLELINK = ?" +
                     " AND PAGE_SHOWINSITEMAP = ?" +
                     " AND BINARY PAGE_CSSCLASS = ?" +
                     " AND BINARY PAGE_LANG = ?" +
                     " AND BINARY PAGE_TEMPLATE = ?" +
                     " AND PAGE_TEMPLATEPARENT = ?" +
                     " AND PAGE_PARENT = ?" +
                     " AND BINARY PAGE_KEYWORD = ?" +
                     " AND PAGE_DEFAULTKEYWORD = ?" +
                     " AND BINARY PAGE_DESC = ?" +
                     " AND PAGE_DEFAULTDESC = ?" +
                     " AND PAGE_REDIRECT = ?" +
                     " AND BINARY PAGE_REDIRECTLINK = ?" +
                     " AND PAGE_REDIRECTTARGET = ?" +
                     " AND PAGE_AUTHORIZE = ?" +
                     " AND PAGE_ACTIVE = ?" +
                     " AND PAGE_TOPMENUCONTENT = ?" +
                     " AND GRP_ID = ?" + 
                     " AND PAGE_CONTENTPARENT = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;
            cmd.Parameters.Add("p_PAGE_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_PAGE_DISPLAYNAME", OdbcType.VarChar, 250).Value = strDisplayName;
            cmd.Parameters.Add("p_PAGE_TITLE", OdbcType.VarChar, 250).Value = strTitle;
            cmd.Parameters.Add("p_PAGE_TITLEFURL", OdbcType.VarChar, 250).Value = strTitleFriendlyUrl;
            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strContent;
            cmd.Parameters.Add("p_PAGE_SHOWPAGETITLE", OdbcType.Int, 1).Value = intShowPageTitle;
            cmd.Parameters.Add("p_PAGE_SHOWINMENU", OdbcType.Int, 1).Value = intShowInMenu;
            cmd.Parameters.Add("p_PAGE_SHOWTOP", OdbcType.Int, 1).Value = intShowTop;
            cmd.Parameters.Add("p_PAGE_SHOWBOTTOM", OdbcType.Int, 1).Value = intShowBottom;
            cmd.Parameters.Add("p_PAGE_SHOWINBREADCRUMB", OdbcType.Int, 1).Value = intShowInBreadcrumb;
            cmd.Parameters.Add("P_PAGE_USEREDITABLE", OdbcType.Int, 1).Value = intUserEditable;
            cmd.Parameters.Add("p_PAGE_ENABLELINK", OdbcType.Int, 1).Value = intEnableLink;
            cmd.Parameters.Add("p_PAGE_SHOWINSITEMAP", OdbcType.Int, 1).Value = intShowInSitemap;
            cmd.Parameters.Add("p_PAGE_CSSCLASS", OdbcType.VarChar, 250).Value = strCSSClass;
            cmd.Parameters.Add("p_PAGE_LANG", OdbcType.VarChar, 250).Value = strLang;
            cmd.Parameters.Add("p_PAGE_TEMPLATE", OdbcType.VarChar, 250).Value = strTemplate;
            cmd.Parameters.Add("p_PAGE_TEMPLATEPARENT", OdbcType.Int, 1).Value = intTemplateParent;
            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.BigInt, 9).Value = intParent;
            cmd.Parameters.Add("p_PAGE_KEYWORD", OdbcType.VarChar, 1000).Value = strKeyword;
            cmd.Parameters.Add("p_PAGE_DEFAULTKEYWORD", OdbcType.Int, 1).Value = intDefaultKeyword;
            cmd.Parameters.Add("p_PAGE_DESC", OdbcType.VarChar, 1000).Value = strDesc;
            cmd.Parameters.Add("p_PAGE_DEFAULTDESC", OdbcType.Int, 1).Value = intDefaultDesc;
            cmd.Parameters.Add("p_PAGE_REDIRECT", OdbcType.BigInt, 9).Value = intRedirect;
            cmd.Parameters.Add("p_PAGE_REDIRECTLINK", OdbcType.VarChar, 1000).Value = strRedirectLink;
            cmd.Parameters.Add("p_PAGE_REDIRECTTARGET", OdbcType.VarChar, 250).Value = strTarget;
            cmd.Parameters.Add("p_PAGE_AUTHORIZE", OdbcType.Int, 1).Value = intPageAuthorize;
            cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PAGE_TOPMENUCONTENT", OdbcType.Text).Value = strTopMenuContent;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intSlideShowGroup;
            cmd.Parameters.Add("p_PAGE_CONTENTPARENT", OdbcType.Int, 1).Value = intContentParent;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public Boolean isExactSameDataSet3(int intPageId, int intShowMobileView, int intShowInMenu, int intShowTop, int intShowBottom, string strCSSClass, string strTemplate, string strPageContent, int intRedirect, string strRedirectLink, string strTarget)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PAGE" +
                     " WHERE PAGE_ID = ?" +
                     " AND PAGE_SHOWMOBILEVIEW = ?" +
                     " AND PAGE_MOBILESHOWINMENU = ?" +
                     " AND PAGE_MOBILESHOWTOP = ?" +
                     " AND PAGE_MOBILESHOWBOTTOM = ?" +
                     " AND PAGE_MOBILECSSCLASS = ?" +
                     " AND PAGE_MOBILETEMPLATE = ?" +
                     " AND BINARY PAGE_MOBILECONTENT = ?" +
                     " AND PAGE_MOBILEREDIRECT = ?" +
                     " AND BINARY PAGE_MOBILEREDIRECTLINK = ?" +
                     " AND PAGE_MOBILEREDIRECTTARGET = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.Int, 9).Value = intPageId;
            cmd.Parameters.Add("p_PAGE_SHOWMOBILEVIEW", OdbcType.Int, 1).Value = intShowMobileView;
            cmd.Parameters.Add("p_PAGE_MOBILESHOWINMENU", OdbcType.Int, 1).Value = intShowInMenu;
            cmd.Parameters.Add("p_PAGE_MOBILESHOWTOP", OdbcType.Int, 1).Value = intShowTop;
            cmd.Parameters.Add("p_PAGE_MOBILESHOWBOTTOM", OdbcType.Int, 1).Value = intShowBottom;
            cmd.Parameters.Add("p_PAGE_MOBILECSSCLASS", OdbcType.VarChar, 250).Value = strCSSClass;
            cmd.Parameters.Add("p_PAGE_MOBILETEMPLATE", OdbcType.VarChar, 250).Value = strTemplate;
            cmd.Parameters.Add("p_PAGE_MOBILECONTENT", OdbcType.Text).Value = strPageContent;
            cmd.Parameters.Add("p_PAGE_REDIRECT", OdbcType.BigInt, 9).Value = intRedirect;
            cmd.Parameters.Add("p_PAGE_REDIRECTLINK", OdbcType.VarChar, 1000).Value = strRedirectLink;
            cmd.Parameters.Add("p_PAGE_REDIRECTTARGET", OdbcType.VarChar, 250).Value = strTarget;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }
    
    public Boolean isExactSameDataSetUserAdm_NoUse(int intId, string strName, string strDisplayName, string strTitle, string strContent, int intShowPageTitle, int intShowInMenu, int intShowTop, int intShowBottom, int intShowInBreadcrumb, int intUserEditable, int intEnableLink, string strCSSClass, string strLang, string strTemplate, int intTemplateParent, string strKeyword, int intDefaultKeyword, string strDesc, int intDefaultDesc, int intRedirect, string strRedirectLink, string strTarget, int intActive, string strTopMenuContent, int intSlideShowGroup)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PAGE" +
                     " WHERE PAGE_ID = ?" +
                     " AND BINARY PAGE_NAME = ?" +
                     " AND BINARY PAGE_DISPLAYNAME = ?" +
                     " AND BINARY PAGE_TITLE = ?" +
                     " AND BINARY PAGE_CONTENT = ?" +
                     " AND PAGE_SHOWPAGETITLE = ?" +
                     " AND PAGE_SHOWINMENU = ?" +
                     " AND PAGE_SHOWTOP = ?" +
                     " AND PAGE_SHOWBOTTOM = ?" +
                     " AND PAGE_SHOWINBREADCRUMB = ?" +
                     " AND PAGE_USEREDITABLE = ?" +
                     " AND PAGE_ENABLELINK = ?" +
                     " AND BINARY PAGE_CSSCLASS = ?" +
                     " AND BINARY PAGE_LANG = ?" +
                     " AND BINARY PAGE_TEMPLATE = ?" +
                     " AND PAGE_TEMPLATEPARENT = ?" +
                     " AND BINARY PAGE_KEYWORD = ?" +
                     " AND PAGE_DEFAULTKEYWORD = ?" +
                     " AND BINARY PAGE_DESC = ?" +
                     " AND PAGE_DEFAULTDESC = ?" +
                     " AND PAGE_REDIRECT = ?" +
                     " AND BINARY PAGE_REDIRECTLINK = ?" +
                     " AND PAGE_REDIRECTTARGET = ?" +
                     " AND PAGE_ACTIVE = ?" +
                     " AND BINARY PAGE_TOPMENUCONTENT = ?" +
                     " AND GRP_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;
            cmd.Parameters.Add("p_PAGE_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_PAGE_DISPLAYNAME", OdbcType.VarChar, 250).Value = strDisplayName;
            cmd.Parameters.Add("p_PAGE_TITLE", OdbcType.VarChar, 250).Value = strTitle;
            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strContent;
            cmd.Parameters.Add("p_PAGE_SHOWPAGETITLE", OdbcType.Int, 1).Value = intShowPageTitle;
            cmd.Parameters.Add("p_PAGE_SHOWINMENU", OdbcType.Int, 1).Value = intShowInMenu;
            cmd.Parameters.Add("p_PAGE_SHOWTOP", OdbcType.Int, 1).Value = intShowTop;
            cmd.Parameters.Add("p_PAGE_SHOWBOTTOM", OdbcType.Int, 1).Value = intShowBottom;
            cmd.Parameters.Add("p_PAGE_SHOWINBREADCRUMB", OdbcType.Int, 1).Value = intShowInBreadcrumb;
            cmd.Parameters.Add("P_PAGE_USEREDITABLE", OdbcType.Int, 1).Value = intUserEditable;
            cmd.Parameters.Add("p_PAGE_ENABLELINK", OdbcType.Int, 1).Value = intEnableLink;
            cmd.Parameters.Add("p_PAGE_CSSCLASS", OdbcType.VarChar, 250).Value = strCSSClass;
            cmd.Parameters.Add("p_PAGE_LANG", OdbcType.VarChar, 250).Value = strLang;
            cmd.Parameters.Add("p_PAGE_TEMPLATE", OdbcType.VarChar, 250).Value = strTemplate;
            cmd.Parameters.Add("p_PAGE_TEMPLATEPARENT", OdbcType.Int, 1).Value = intTemplateParent;
            cmd.Parameters.Add("p_PAGE_KEYWORD", OdbcType.VarChar, 1000).Value = strKeyword;
            cmd.Parameters.Add("p_PAGE_DEFAULTKEYWORD", OdbcType.Int, 1).Value = intDefaultKeyword;
            cmd.Parameters.Add("p_PAGE_DESC", OdbcType.VarChar, 1000).Value = strDesc;
            cmd.Parameters.Add("p_PAGE_DEFAULTDESC", OdbcType.Int, 1).Value = intDefaultDesc;
            cmd.Parameters.Add("p_PAGE_REDIRECT", OdbcType.BigInt, 9).Value = intRedirect;
            cmd.Parameters.Add("p_PAGE_REDIRECTLINK", OdbcType.VarChar, 1000).Value = strRedirectLink;
            cmd.Parameters.Add("p_PAGE_REDIRECTTARGET", OdbcType.VarChar, 250).Value = strTarget;
            cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PAGE_TOPMENUCONTENT", OdbcType.Text).Value = strTopMenuContent;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intSlideShowGroup;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public Boolean isExactSameDataSetUserAdm(int intId, string strName, string strDisplayName, string strTitle, string strTitleFriendlyUrl, string strContent, int intShowPageTitle, int intShowInMenu, int intShowTop, int intShowBottom, int intShowInBreadcrumb, int intUserEditable, int intEnableLink, int intShowInSitemap, string strCSSClass, string strLang, string strTemplate, int intTemplateParent, int intParent, string strKeyword, int intDefaultKeyword, string strDesc, int intDefaultDesc, int intRedirect, string strRedirectLink, string strTarget, int intActive, string strTopMenuContent, int intSlideShowGroup)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PAGE" +
                     " WHERE PAGE_ID = ?" +
                     " AND BINARY PAGE_NAME = ?" +
                     " AND BINARY PAGE_DISPLAYNAME = ?" +
                     " AND BINARY PAGE_TITLE = ?" +
                     " AND BINARY PAGE_TITLEFURL = ?" +
                     " AND BINARY PAGE_CONTENT = ?" +
                     " AND PAGE_SHOWPAGETITLE = ?" +
                     " AND PAGE_SHOWINMENU = ?" +
                     " AND PAGE_SHOWTOP = ?" +
                     " AND PAGE_SHOWBOTTOM = ?" +
                     " AND PAGE_SHOWINBREADCRUMB = ?" +
                     " AND PAGE_USEREDITABLE = ?" +
                     " AND PAGE_ENABLELINK = ?" +
                     " AND PAGE_SHOWINSITEMAP = ?" +
                     " AND BINARY PAGE_CSSCLASS = ?" +
                     " AND BINARY PAGE_LANG = ?" +
                     " AND BINARY PAGE_TEMPLATE = ?" +
                     " AND PAGE_TEMPLATEPARENT = ?" +
                     " AND PAGE_PARENT = ?" +
                     " AND BINARY PAGE_KEYWORD = ?" +
                     " AND PAGE_DEFAULTKEYWORD = ?" +
                     " AND BINARY PAGE_DESC = ?" +
                     " AND PAGE_DEFAULTDESC = ?" +
                     " AND PAGE_REDIRECT = ?" +
                     " AND BINARY PAGE_REDIRECTLINK = ?" +
                     " AND PAGE_REDIRECTTARGET = ?" +
                     " AND PAGE_ACTIVE = ?" +
                     " AND BINARY PAGE_TOPMENUCONTENT = ?" +
                     " AND GRP_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;
            cmd.Parameters.Add("p_PAGE_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_PAGE_DISPLAYNAME", OdbcType.VarChar, 250).Value = strDisplayName;
            cmd.Parameters.Add("p_PAGE_TITLE", OdbcType.VarChar, 250).Value = strTitle;
            cmd.Parameters.Add("P_PAGE_TITLEFURL", OdbcType.VarChar, 250).Value = strTitleFriendlyUrl;
            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strContent;
            cmd.Parameters.Add("p_PAGE_SHOWPAGETITLE", OdbcType.Int, 1).Value = intShowPageTitle;
            cmd.Parameters.Add("p_PAGE_SHOWINMENU", OdbcType.Int, 1).Value = intShowInMenu;
            cmd.Parameters.Add("p_PAGE_SHOWTOP", OdbcType.Int, 1).Value = intShowTop;
            cmd.Parameters.Add("p_PAGE_SHOWBOTTOM", OdbcType.Int, 1).Value = intShowBottom;
            cmd.Parameters.Add("p_PAGE_SHOWINBREADCRUMB", OdbcType.Int, 1).Value = intShowInBreadcrumb;
            cmd.Parameters.Add("P_PAGE_USEREDITABLE", OdbcType.Int, 1).Value = intUserEditable;
            cmd.Parameters.Add("p_PAGE_ENABLELINK", OdbcType.Int, 1).Value = intEnableLink;
            cmd.Parameters.Add("p_PAGE_SHOWINSITEMAP", OdbcType.Int, 1).Value = intShowInSitemap;
            cmd.Parameters.Add("p_PAGE_CSSCLASS", OdbcType.VarChar, 250).Value = strCSSClass;
            cmd.Parameters.Add("p_PAGE_LANG", OdbcType.VarChar, 250).Value = strLang;
            cmd.Parameters.Add("p_PAGE_TEMPLATE", OdbcType.VarChar, 250).Value = strTemplate;
            cmd.Parameters.Add("p_PAGE_TEMPLATEPARENT", OdbcType.Int, 1).Value = intTemplateParent;
            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.Int, 1).Value = intParent;
            cmd.Parameters.Add("p_PAGE_KEYWORD", OdbcType.VarChar, 1000).Value = strKeyword;
            cmd.Parameters.Add("p_PAGE_DEFAULTKEYWORD", OdbcType.Int, 1).Value = intDefaultKeyword;
            cmd.Parameters.Add("p_PAGE_DESC", OdbcType.VarChar, 1000).Value = strDesc;
            cmd.Parameters.Add("p_PAGE_DEFAULTDESC", OdbcType.Int, 1).Value = intDefaultDesc;
            cmd.Parameters.Add("p_PAGE_REDIRECT", OdbcType.BigInt, 9).Value = intRedirect;
            cmd.Parameters.Add("p_PAGE_REDIRECTLINK", OdbcType.VarChar, 1000).Value = strRedirectLink;
            cmd.Parameters.Add("p_PAGE_REDIRECTTARGET", OdbcType.VarChar, 250).Value = strTarget;
            cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PAGE_TOPMENUCONTENT", OdbcType.Text).Value = strTopMenuContent;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intSlideShowGroup;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public Boolean isExactSameDataSet(int intId, string strName, string strDisplayName, string strTitle, string strTitleFriendlyUrl, 
        string strContent, int intShowPageTitle, int intShowInMenu, int intShowTop, int intShowBottom, 
        int intShowInBreadcrumb, int intUserEditable, int intEnableLink, int intShowInSitemap, string strCSSClass, 
        string strTemplate, int intTemplateParent, int intParent, string strKeyword, int intDefaultKeyword, 
        string strDesc, int intDefaultDesc, int intRedirect, string strRedirectLink, string strTarget, 
        int intActive, string strTopMenuContent, int intSlideShowGroup, int intPageAuthorize, int intContentParent)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PAGE" +
                     " WHERE PAGE_ID = ?" +
                     " AND BINARY PAGE_NAME = ?" +
                     " AND BINARY PAGE_DISPLAYNAME = ?" +
                     " AND BINARY PAGE_TITLE = ?" +
                     " AND BINARY PAGE_TITLEFURL = ?" +
                     " AND BINARY PAGE_CONTENT = ?" +
                     " AND PAGE_SHOWPAGETITLE = ?" +
                     " AND PAGE_SHOWINMENU = ?" +
                     " AND PAGE_SHOWTOP = ?" +
                     " AND PAGE_SHOWBOTTOM = ?" +
                     " AND PAGE_SHOWINBREADCRUMB = ?" +
                     " AND PAGE_USEREDITABLE = ?" +
                     " AND PAGE_ENABLELINK = ?" +
                     " AND PAGE_SHOWINSITEMAP = ?" +
                     " AND BINARY PAGE_CSSCLASS = ?" +
                     " AND BINARY PAGE_TEMPLATE = ?" +
                     " AND PAGE_TEMPLATEPARENT = ?" +
                     " AND PAGE_PARENT = ?" +
                     " AND BINARY PAGE_KEYWORD = ?" +
                     " AND PAGE_DEFAULTKEYWORD = ?" +
                     " AND BINARY PAGE_DESC = ?" +
                     " AND PAGE_DEFAULTDESC = ?" +
                     " AND PAGE_REDIRECT = ?" +
                     " AND BINARY PAGE_REDIRECTLINK = ?" +
                     " AND PAGE_REDIRECTTARGET = ?" +
                     " AND PAGE_AUTHORIZE = ?" +
                     " AND PAGE_ACTIVE = ?" +
                     " AND BINARY PAGE_TOPMENUCONTENT = ?" +
                     " AND GRP_ID = ?" + 
                     " AND PAGE_CONTENTPARENT = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;
            cmd.Parameters.Add("p_PAGE_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_PAGE_DISPLAYNAME", OdbcType.VarChar, 250).Value = strDisplayName;
            cmd.Parameters.Add("p_PAGE_TITLE", OdbcType.VarChar, 250).Value = strTitle;
            cmd.Parameters.Add("p_PAGE_TITLEFURL", OdbcType.VarChar, 250).Value = strTitleFriendlyUrl;
            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strContent;
            cmd.Parameters.Add("p_PAGE_SHOWPAGETITLE", OdbcType.Int, 1).Value = intShowPageTitle;
            cmd.Parameters.Add("p_PAGE_SHOWINMENU", OdbcType.Int, 1).Value = intShowInMenu;
            cmd.Parameters.Add("p_PAGE_SHOWTOP", OdbcType.Int, 1).Value = intShowTop;
            cmd.Parameters.Add("p_PAGE_SHOWBOTTOM", OdbcType.Int, 1).Value = intShowBottom;
            cmd.Parameters.Add("p_PAGE_SHOWINBREADCRUMB", OdbcType.Int, 1).Value = intShowInBreadcrumb;
            cmd.Parameters.Add("p_PAGE_USEREDITABLE", OdbcType.Int, 1).Value = intUserEditable;
            cmd.Parameters.Add("p_PAGE_ENABLELINK", OdbcType.Int, 1).Value = intEnableLink;
            cmd.Parameters.Add("p_PAGE_SHOWINSITEMAP", OdbcType.Int, 1).Value = intShowInSitemap;
            cmd.Parameters.Add("p_PAGE_CSSCLASS", OdbcType.VarChar, 250).Value = strCSSClass;
            cmd.Parameters.Add("p_PAGE_TEMPLATE", OdbcType.VarChar, 250).Value = strTemplate;
            cmd.Parameters.Add("p_PAGE_TEMPLATEPARENT", OdbcType.Int, 1).Value = intTemplateParent;
            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.BigInt, 9).Value = intParent;
            cmd.Parameters.Add("p_PAGE_KEYWORD", OdbcType.VarChar, 1000).Value = strKeyword;
            cmd.Parameters.Add("p_PAGE_DEFAULTKEYWORD", OdbcType.Int, 1).Value = intDefaultKeyword;
            cmd.Parameters.Add("p_PAGE_DESC", OdbcType.VarChar, 1000).Value = strDesc;
            cmd.Parameters.Add("p_PAGE_DEFAULTDESC", OdbcType.Int, 1).Value = intDefaultDesc;
            cmd.Parameters.Add("p_PAGE_REDIRECT", OdbcType.BigInt, 9).Value = intRedirect;
            cmd.Parameters.Add("p_PAGE_REDIRECTLINK", OdbcType.VarChar, 1000).Value = strRedirectLink;
            cmd.Parameters.Add("p_PAGE_REDIRECTTARGET", OdbcType.VarChar, 250).Value = strTarget;
            cmd.Parameters.Add("p_PAGE_AUTHORIZE", OdbcType.Int, 1).Value = intPageAuthorize;
            cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PAGE_TOPMENUCONTENT", OdbcType.Text).Value = strTopMenuContent;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intSlideShowGroup;
            cmd.Parameters.Add("p_PAGE_CONTENTPARENT", OdbcType.Int, 1).Value = intContentParent;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updatePageById(int intId, string strName, string strDisplayName, string strTitle, string strTitleFriendlyUrl, 
        int intShowPageTitle, int intShowInMenu, int intShowTop, int intShowBottom, int intShowInBreadcrumb,
        int intUserEditable, int intEnableLink, int intShowInSitemap, string strCSSClass, string strLang, 
        string strTemplate, int intTemplateParent, int intParent, string strKeyword, int intDefaultKeyword, 
        string strDesc, int intDefaultDesc, int intRedirect, string strRedirectLink, string strTarget, 
        int intActive, string strContent, int intUpdatedBy, string strTopMenuContent, int intSlideShowGroup, 
        int intPageAuthorize, int intContentParent)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<string> columns = new List<string>()
            {
                "PAGE_NAME = ?","PAGE_DISPLAYNAME = ?","PAGE_TITLE = ?","PAGE_TITLEFURL = ?","PAGE_CONTENT = ?",
                "PAGE_SHOWPAGETITLE = ?","PAGE_SHOWINMENU = ?","PAGE_SHOWTOP = ?","PAGE_SHOWBOTTOM = ?","PAGE_SHOWINBREADCRUMB = ?",
                "PAGE_USEREDITABLE = ?","PAGE_ENABLELINK = ?","PAGE_SHOWINSITEMAP = ?","PAGE_CSSCLASS = ?","PAGE_LANG = ?",
                "PAGE_TEMPLATE = ?","PAGE_TEMPLATEPARENT = ?","PAGE_PARENT = ?","PAGE_KEYWORD = ?","PAGE_DEFAULTKEYWORD = ?",
                "PAGE_DESC = ?","PAGE_DEFAULTDESC = ?","PAGE_REDIRECT = ?","PAGE_REDIRECTLINK = ?","PAGE_REDIRECTTARGET = ?",
                "PAGE_AUTHORIZE = ?","PAGE_ACTIVE = ?","PAGE_UPDATEDBY = ?","PAGE_TOPMENUCONTENT = ?","GRP_ID = ?",
                "PAGE_CONTENTPARENT = ?",

                "PAGE_LASTUPDATE = SYSDATE()",
            };

            cmd.Parameters.Add("p_PAGE_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_PAGE_DISPLAYNAME", OdbcType.VarChar, 250).Value = strDisplayName;
            cmd.Parameters.Add("p_PAGE_TITLE", OdbcType.VarChar, 250).Value = strTitle;
            cmd.Parameters.Add("p_PAGE_TITLEFURL", OdbcType.VarChar, 250).Value = strTitleFriendlyUrl;
            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strContent;
            cmd.Parameters.Add("p_PAGE_SHOWPAGETITLE", OdbcType.Int, 1).Value = intShowPageTitle;
            cmd.Parameters.Add("p_PAGE_SHOWINMENU", OdbcType.Int, 1).Value = intShowInMenu;
            cmd.Parameters.Add("p_PAGE_SHOWTOP", OdbcType.Int, 1).Value = intShowTop;
            cmd.Parameters.Add("p_PAGE_SHOWBOTTOM", OdbcType.Int, 1).Value = intShowBottom;
            cmd.Parameters.Add("p_PAGE_SHOWINBREADCRUMB", OdbcType.Int, 1).Value = intShowInBreadcrumb;
            cmd.Parameters.Add("p_PAGE_USEREDITABLE", OdbcType.Int, 1).Value = intUserEditable;
            cmd.Parameters.Add("p_PAGE_ENABLELINK", OdbcType.Int, 1).Value = intEnableLink;
            cmd.Parameters.Add("p_PAGE_SHOWINSITEMAP", OdbcType.Int, 1).Value = intShowInSitemap;
            cmd.Parameters.Add("p_PAGE_CSSCLASS", OdbcType.VarChar, 250).Value = strCSSClass;
            cmd.Parameters.Add("p_PAGE_LANG", OdbcType.VarChar, 10).Value = strLang;
            cmd.Parameters.Add("p_PAGE_TEMPLATE", OdbcType.VarChar, 250).Value = strTemplate;
            cmd.Parameters.Add("p_PAGE_TEMPLATEPARENT", OdbcType.Int, 1).Value = intTemplateParent;
            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.BigInt, 9).Value = intParent;
            cmd.Parameters.Add("p_PAGE_KEYWORD", OdbcType.VarChar, 1000).Value = strKeyword;
            cmd.Parameters.Add("p_PAGE_DEFAULTKEYWORD", OdbcType.Int, 1).Value = intDefaultKeyword;
            cmd.Parameters.Add("p_PAGE_DESC", OdbcType.VarChar, 1000).Value = strDesc;
            cmd.Parameters.Add("p_PAGE_DEFAULTDESC", OdbcType.Int, 1).Value = intDefaultDesc;
            cmd.Parameters.Add("p_PAGE_REDIRECT", OdbcType.BigInt, 9).Value = intRedirect;
            cmd.Parameters.Add("p_PAGE_REDIRECTLINK", OdbcType.VarChar, 1000).Value = strRedirectLink;
            cmd.Parameters.Add("p_PAGE_REDIRECTTARGET", OdbcType.VarChar, 250).Value = strTarget;
            cmd.Parameters.Add("p_PAGE_AUTHORIZE", OdbcType.Int, 1).Value = intPageAuthorize;
            cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PAGE_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_PAGE_TOPMENUCONTENT", OdbcType.Text).Value = strTopMenuContent;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intSlideShowGroup;
            cmd.Parameters.Add("p_PAGE_CONTENTPARENT", OdbcType.Int, 1).Value = intContentParent;

            if (intContentParent == 1)
            {
                columns.Insert(columns.Count - 1, "PAGE_MOBILECONTENT = ?");
                cmd.Parameters.Add("p_PAGE_MOBILECONTENT", OdbcType.Text).Value = strContent;
            }

            strSql = "UPDATE TB_PAGE SET " + string.Join(",", columns.ToArray()) + " WHERE PAGE_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;



            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                id = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updatePageById3(int intPageId, int intShowMobileView, int intShowInMenu, int intShowTop, int intShowBottom, string strCSSClass, string strTemplate, string strPageContent, int intRedirect, string strRedirectLink, string strTarget, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGE SET" +
                     " PAGE_SHOWMOBILEVIEW = ?," +
                     " PAGE_MOBILESHOWINMENU = ?," +
                     " PAGE_MOBILESHOWTOP = ?," +
                     " PAGE_MOBILESHOWBOTTOM = ?," +
                     " PAGE_MOBILECSSCLASS = ?," +
                     " PAGE_MOBILETEMPLATE = ?," +
                     " PAGE_MOBILECONTENT = ?," +
                     " PAGE_MOBILEREDIRECT = ?," +
                     " PAGE_MOBILEREDIRECTLINK = ?," +
                     " PAGE_MOBILEREDIRECTTARGET = ?," +
                     " PAGE_LASTUPDATE = SYSDATE()," +
                     " PAGE_UPDATEDBY = ?" +
                     " WHERE PAGE_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_SHOWMOBILEVIEW", OdbcType.Int, 1).Value = intShowMobileView;
            cmd.Parameters.Add("p_PAGE_MOBILESHOWINMENU", OdbcType.Int, 1).Value = intShowInMenu;
            cmd.Parameters.Add("p_PAGE_MOBILESHOWTOP", OdbcType.Int, 1).Value = intShowTop;
            cmd.Parameters.Add("p_PAGE_MOBILESHOWBOTTOM", OdbcType.Int, 1).Value = intShowBottom;
            cmd.Parameters.Add("p_PAGE_MOBILECSSCLASS", OdbcType.VarChar, 250).Value = strCSSClass;
            cmd.Parameters.Add("p_PAGE_MOBILETEMPLATE", OdbcType.VarChar, 250).Value = strTemplate;
            cmd.Parameters.Add("p_PAGE_MOBILECONTENT", OdbcType.Text).Value = strPageContent;
            cmd.Parameters.Add("p_PAGE_MOBILEREDIRECT", OdbcType.BigInt, 9).Value = intRedirect;
            cmd.Parameters.Add("p_PAGE_MOBILEREDIRECTLINK", OdbcType.VarChar, 1000).Value = strRedirectLink;
            cmd.Parameters.Add("p_PAGE_MOBILEREDIRECTTARGET", OdbcType.VarChar, 250).Value = strTarget;
            cmd.Parameters.Add("p_PAGE_UPDATEDBY", OdbcType.Int, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_PAGE_ID", OdbcType.Int, 9).Value = intPageId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                id = intPageId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updatePageByIdUserAdm_NoUse(int intId, string strName, string strDisplayName, string strTitle, int intShowPageTitle, int intShowInMenu, int intShowTop, int intShowBottom, int intShowInBreadcrumb, int intUserEditable, int intEnableLink, string strCSSClass, string strLang, string strTemplate, int intTemplateParent, string strKeyword, int intDefaultKeyword, string strDesc, int intDefaultDesc, int intRedirect, string strRedirectLink, string strTarget, int intActive, string strContent, int intUpdatedBy, string strTopMenuContent, int intSlideShowGroup)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGE SET" +
                     " PAGE_NAME = ?," +
                     " PAGE_DISPLAYNAME = ?," +
                     " PAGE_TITLE = ?," +
                     " PAGE_CONTENT = ?," +
                     " PAGE_SHOWPAGETITLE = ?," +
                     " PAGE_SHOWINMENU = ?," +
                     " PAGE_SHOWTOP = ?," +
                     " PAGE_SHOWBOTTOM = ?," +
                     " PAGE_SHOWINBREADCRUMB = ?," +
                     " PAGE_USEREDITABLE = ?," +
                     " PAGE_ENABLELINK = ?," +
                     " PAGE_CSSCLASS = ?," +
                     " PAGE_LANG = ?," +
                     " PAGE_TEMPLATE = ?," +
                     " PAGE_TEMPLATEPARENT = ?," +
                     " PAGE_KEYWORD = ?," +
                     " PAGE_DEFAULTKEYWORD = ?," +
                     " PAGE_DESC = ?," +
                     " PAGE_DEFAULTDESC = ?," +
                     " PAGE_REDIRECT = ?," +
                     " PAGE_REDIRECTLINK = ?," +
                     " PAGE_REDIRECTTARGET = ?," +
                     " PAGE_ACTIVE = ?," +
                     " PAGE_UPDATEDBY = ?," +
                     " PAGE_LASTUPDATE = SYSDATE()," +
                     " PAGE_TOPMENUCONTENT = ?," +
                     " GRP_ID = ?" +
                     " WHERE PAGE_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_PAGE_DISPLAYNAME", OdbcType.VarChar, 250).Value = strDisplayName;
            cmd.Parameters.Add("p_PAGE_TITLE", OdbcType.VarChar, 250).Value = strTitle;
            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strContent;
            cmd.Parameters.Add("p_PAGE_SHOWPAGETITLE", OdbcType.Int, 1).Value = intShowPageTitle;
            cmd.Parameters.Add("p_PAGE_SHOWINMENU", OdbcType.Int, 1).Value = intShowInMenu;
            cmd.Parameters.Add("p_PAGE_SHOWTOP", OdbcType.Int, 1).Value = intShowTop;
            cmd.Parameters.Add("p_PAGE_SHOWBOTTOM", OdbcType.Int, 1).Value = intShowBottom;
            cmd.Parameters.Add("p_PAGE_SHOWINBREADCRUMB", OdbcType.Int, 1).Value = intShowInBreadcrumb;
            cmd.Parameters.Add("p_PAGE_USEREDITABLE", OdbcType.Int, 1).Value = intUserEditable;
            cmd.Parameters.Add("p_PAGE_ENABLELINK", OdbcType.Int, 1).Value = intEnableLink;
            cmd.Parameters.Add("p_PAGE_CSSCLASS", OdbcType.VarChar, 250).Value = strCSSClass;
            cmd.Parameters.Add("p_PAGE_LANG", OdbcType.VarChar, 10).Value = strLang;
            cmd.Parameters.Add("p_PAGE_TEMPLATE", OdbcType.VarChar, 250).Value = strTemplate;
            cmd.Parameters.Add("p_PAGE_TEMPLATEPARENT", OdbcType.Int, 1).Value = intTemplateParent;
            cmd.Parameters.Add("p_PAGE_KEYWORD", OdbcType.VarChar, 1000).Value = strKeyword;
            cmd.Parameters.Add("p_PAGE_DEFAULTKEYWORD", OdbcType.Int, 1).Value = intDefaultKeyword;
            cmd.Parameters.Add("p_PAGE_DESC", OdbcType.VarChar, 1000).Value = strDesc;
            cmd.Parameters.Add("p_PAGE_DEFAULTDESC", OdbcType.Int, 1).Value = intDefaultDesc;
            cmd.Parameters.Add("p_PAGE_REDIRECT", OdbcType.BigInt, 9).Value = intRedirect;
            cmd.Parameters.Add("p_PAGE_REDIRECTLINK", OdbcType.VarChar, 1000).Value = strRedirectLink;
            cmd.Parameters.Add("p_PAGE_REDIRECTTARGET", OdbcType.VarChar, 250).Value = strTarget;
            cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PAGE_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_PAGE_TOPMENUCONTENT", OdbcType.Text).Value = strTopMenuContent;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intSlideShowGroup;
            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                id = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updatePageByIdUserAdm(int intId, string strName, string strDisplayName, string strTitle, string strTitleFriendlyUrl, int intShowPageTitle, int intShowInMenu, int intShowTop, int intShowBottom, int intShowInBreadcrumb, int intUserEditable, int intEnableLink, int intShowInSitemap, string strCSSClass, string strLang, string strTemplate, int intTemplateParent, int intParent, string strKeyword, int intDefaultKeyword, string strDesc, int intDefaultDesc, int intRedirect, string strRedirectLink, string strTarget, int intActive, string strContent, int intUpdatedBy, string strTopMenuContent, int intSlideShowGroup)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGE SET" +
                     " PAGE_NAME = ?," +
                     " PAGE_DISPLAYNAME = ?," +
                     " PAGE_TITLE = ?," +
                     " PAGE_TITLEFURL = ?," +
                     " PAGE_CONTENT = ?," +
                     " PAGE_SHOWPAGETITLE = ?," +
                     " PAGE_SHOWINMENU = ?," +
                     " PAGE_SHOWTOP = ?," +
                     " PAGE_SHOWBOTTOM = ?," +
                     " PAGE_SHOWINBREADCRUMB = ?," +
                     " PAGE_USEREDITABLE = ?," +
                     " PAGE_ENABLELINK = ?," +
                     " PAGE_SHOWINSITEMAP = ?," +
                     " PAGE_CSSCLASS = ?," +
                     " PAGE_LANG = ?," +
                     " PAGE_TEMPLATE = ?," +
                     " PAGE_TEMPLATEPARENT = ?," +
                     " PAGE_PARENT = ?," +
                     " PAGE_KEYWORD = ?," +
                     " PAGE_DEFAULTKEYWORD = ?," +
                     " PAGE_DESC = ?," +
                     " PAGE_DEFAULTDESC = ?," +
                     " PAGE_REDIRECT = ?," +
                     " PAGE_REDIRECTLINK = ?," +
                     " PAGE_REDIRECTTARGET = ?," +
                     " PAGE_ACTIVE = ?," +
                     " PAGE_UPDATEDBY = ?," +
                     " PAGE_LASTUPDATE = SYSDATE()," +
                     " PAGE_TOPMENUCONTENT = ?," +
                     " GRP_ID = ?" +
                     " WHERE PAGE_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_PAGE_DISPLAYNAME", OdbcType.VarChar, 250).Value = strDisplayName;
            cmd.Parameters.Add("p_PAGE_TITLE", OdbcType.VarChar, 250).Value = strTitle;
            cmd.Parameters.Add("p_PAGE_TITLEFURL", OdbcType.VarChar, 250).Value = strTitleFriendlyUrl;
            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strContent;
            cmd.Parameters.Add("p_PAGE_SHOWPAGETITLE", OdbcType.Int, 1).Value = intShowPageTitle;
            cmd.Parameters.Add("p_PAGE_SHOWINMENU", OdbcType.Int, 1).Value = intShowInMenu;
            cmd.Parameters.Add("p_PAGE_SHOWTOP", OdbcType.Int, 1).Value = intShowTop;
            cmd.Parameters.Add("p_PAGE_SHOWBOTTOM", OdbcType.Int, 1).Value = intShowBottom;
            cmd.Parameters.Add("p_PAGE_SHOWINBREADCRUMB", OdbcType.Int, 1).Value = intShowInBreadcrumb;
            cmd.Parameters.Add("p_PAGE_USEREDITABLE", OdbcType.Int, 1).Value = intUserEditable;
            cmd.Parameters.Add("p_PAGE_ENABLELINK", OdbcType.Int, 1).Value = intEnableLink;
            cmd.Parameters.Add("p_PAGE_SHOWINSITEMAP", OdbcType.Int, 1).Value = intShowInSitemap;
            cmd.Parameters.Add("p_PAGE_CSSCLASS", OdbcType.VarChar, 250).Value = strCSSClass;
            cmd.Parameters.Add("p_PAGE_LANG", OdbcType.VarChar, 10).Value = strLang;
            cmd.Parameters.Add("p_PAGE_TEMPLATE", OdbcType.VarChar, 250).Value = strTemplate;
            cmd.Parameters.Add("p_PAGE_TEMPLATEPARENT", OdbcType.Int, 1).Value = intTemplateParent;
            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.Int, 1).Value = intParent;
            cmd.Parameters.Add("p_PAGE_KEYWORD", OdbcType.VarChar, 1000).Value = strKeyword;
            cmd.Parameters.Add("p_PAGE_DEFAULTKEYWORD", OdbcType.Int, 1).Value = intDefaultKeyword;
            cmd.Parameters.Add("p_PAGE_DESC", OdbcType.VarChar, 1000).Value = strDesc;
            cmd.Parameters.Add("p_PAGE_DEFAULTDESC", OdbcType.Int, 1).Value = intDefaultDesc;
            cmd.Parameters.Add("p_PAGE_REDIRECT", OdbcType.BigInt, 9).Value = intRedirect;
            cmd.Parameters.Add("p_PAGE_REDIRECTLINK", OdbcType.VarChar, 1000).Value = strRedirectLink;
            cmd.Parameters.Add("p_PAGE_REDIRECTTARGET", OdbcType.VarChar, 250).Value = strTarget;
            cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PAGE_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_PAGE_TOPMENUCONTENT", OdbcType.Text).Value = strTopMenuContent;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intSlideShowGroup;
            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                id = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updatePageById(int intId, string strName, string strDisplayName, string strTitle, string strTitleFriendlyUrl, 
        int intShowPageTitle, int intShowInMenu, int intShowTop, int intShowBottom, int intShowInBreadcrumb, 
        int intUserEditable, int intEnableLink, int intShowInSitemap, string strCSSClass, string strTemplate, 
        int intTemplateParent, int intParent, string strKeyword, int intDefaultKeyword, string strDesc, 
        int intDefaultDesc, int intRedirect, string strRedirectLink, string strTarget, int intActive, 
        string strContent, int intUpdatedBy, string strTopMenuContent, int intSlideShowGroup, int intPageAuthorize,
        int intContentParent)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<string> columns = new List<string>()
            {
                "PAGE_NAME = ?","PAGE_DISPLAYNAME = ?","PAGE_TITLE = ?","PAGE_TITLEFURL= ?","PAGE_CONTENT = ?",
                "PAGE_SHOWPAGETITLE = ?","PAGE_SHOWINMENU = ?","PAGE_SHOWTOP = ?","PAGE_SHOWBOTTOM = ?","PAGE_SHOWINBREADCRUMB = ?",
                "PAGE_USEREDITABLE = ?","PAGE_ENABLELINK = ?","PAGE_SHOWINSITEMAP = ?","PAGE_CSSCLASS = ?","PAGE_TEMPLATE = ?",
                "PAGE_TEMPLATEPARENT = ?","PAGE_PARENT = ?","PAGE_KEYWORD = ?","PAGE_DEFAULTKEYWORD = ?","PAGE_DESC = ?",
                "PAGE_DEFAULTDESC = ?","PAGE_REDIRECT = ?","PAGE_REDIRECTLINK = ?","PAGE_REDIRECTTARGET = ?","PAGE_AUTHORIZE = ?",
                "PAGE_ACTIVE = ?","PAGE_UPDATEDBY = ?","PAGE_TOPMENUCONTENT = ?","GRP_ID = ?","PAGE_CONTENTPARENT = ?",

                "PAGE_LASTUPDATE = SYSDATE()",
            };

            cmd.Parameters.Add("p_PAGE_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_APGE_DISPLAYNAME", OdbcType.VarChar, 250).Value = strDisplayName;
            cmd.Parameters.Add("p_PAGE_TITLE", OdbcType.VarChar, 250).Value = strTitle;
            cmd.Parameters.Add("p_PAGE_TITLEFURL", OdbcType.VarChar, 250).Value = strTitleFriendlyUrl;
            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strContent;
            cmd.Parameters.Add("p_PAGE_SHOWPAGETITLE", OdbcType.Int, 1).Value = intShowPageTitle;
            cmd.Parameters.Add("p_PAGE_SHOWINMENU", OdbcType.Int, 1).Value = intShowInMenu;
            cmd.Parameters.Add("p_PAGE_SHOWTOP", OdbcType.Int, 1).Value = intShowTop;
            cmd.Parameters.Add("p_PAGE_SHOWBOTTOM", OdbcType.Int, 1).Value = intShowBottom;
            cmd.Parameters.Add("p_PAGE_SHOWINBREADCRUMB", OdbcType.Int, 1).Value = intShowInBreadcrumb;
            cmd.Parameters.Add("p_PAGE_USEREDITABLE", OdbcType.Int, 1).Value = intUserEditable;
            cmd.Parameters.Add("p_PAGE_ENABLELINK", OdbcType.Int, 1).Value = intEnableLink;
            cmd.Parameters.Add("p_PAGE_SHOWINSITEMAP", OdbcType.Int, 1).Value = intShowInSitemap;
            cmd.Parameters.Add("p_PAGE_CSSCLASS", OdbcType.VarChar, 250).Value = strCSSClass;
            cmd.Parameters.Add("p_PAGE_TEMPLATE", OdbcType.VarChar, 250).Value = strTemplate;
            cmd.Parameters.Add("p_PAGE_TEMPLATEPARENT", OdbcType.Int, 1).Value = intTemplateParent;
            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.BigInt, 9).Value = intParent;
            cmd.Parameters.Add("p_PAGE_KEYWORD", OdbcType.VarChar, 1000).Value = strKeyword;
            cmd.Parameters.Add("p_PAGE_DEFAULTKEYWORD", OdbcType.Int, 1).Value = intDefaultKeyword;
            cmd.Parameters.Add("p_PAGE_DESC", OdbcType.VarChar, 1000).Value = strDesc;
            cmd.Parameters.Add("p_PAGE_DEFAULTDESC", OdbcType.Int, 1).Value = intDefaultDesc;
            cmd.Parameters.Add("p_PAGE_REDIRECT", OdbcType.BigInt, 9).Value = intRedirect;
            cmd.Parameters.Add("p_PAGE_REDIRECTLINK", OdbcType.VarChar, 1000).Value = strRedirectLink;
            cmd.Parameters.Add("p_PAGE_REDIRECTTARGET", OdbcType.VarChar, 250).Value = strTarget;
            cmd.Parameters.Add("p_PAGE_AUTHORIZE", OdbcType.Int, 1).Value = intPageAuthorize;
            cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PAGE_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_PAGE_TOPMENUCONTENT", OdbcType.Text).Value = strTopMenuContent;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intSlideShowGroup;
            cmd.Parameters.Add("p_PAGE_CONTENTPARENT", OdbcType.Int, 1).Value = intContentParent;

            if (intContentParent == 1)
            {
                columns.Insert(columns.Count - 1, "PAGE_MOBILECONTENT = ?");
                cmd.Parameters.Add("p_PAGE_MOBILECONTENT", OdbcType.Text).Value = strContent;
            }

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;
            strSql = "UPDATE TB_PAGE SET " + string.Join(",", columns.ToArray()) + " WHERE PAGE_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;



            


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                id = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updatePage(int intNewParent, int intOldParent, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGE SET" +
                     " PAGE_PARENT = ?," +
                     " PAGE_ORDER = 0," +
                     " PAGE_UPDATEDBY = ?," +
                     " PAGE_LASTUPDATE = SYSDATE()" +
                     " WHERE PAGE_PARENT = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_NEW_PARENT", OdbcType.BigInt, 9).Value = intNewParent;
            cmd.Parameters.Add("p_PAGE_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_PAGE_OLD_PARENT", OdbcType.BigInt, 9).Value = intOldParent;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updatePage(int intOldId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGE SET" +
                     " PAGE_REDIRECT = 0" +
                     " WHERE PAGE_REDIRECT = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_REDIRECT", OdbcType.BigInt, 9).Value = intOldId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateOrder(int intParent, int intOldOrder)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGE SET" +
                     " PAGE_ORDER = PAGE_ORDER - 1" +
                     " WHERE PAGE_PARENT = ?" + 
                     " AND PAGE_ORDER > ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_PARENT", OdbcType.BigInt, 9).Value = intParent;
            cmd.Parameters.Add("p_PAGE_ORDER", OdbcType.BigInt, 9).Value = intOldOrder;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateOrderById(int intId, int intParent)
    {
        int intRecordAffected = 0;

        try
        {
            int intOrder = getOrder(intParent, 0);
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGE SET" +
                     " PAGE_ORDER = ?" +
                     " WHERE PAGE_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            
            cmd.Parameters.Add("p_PAGE_ORDER", OdbcType.BigInt, 9).Value = intOrder;
            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateOrderById2(int intId, int intOrder)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGE SET" +
                     " PAGE_ORDER = ?" +
                     " WHERE PAGE_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_ORDER", OdbcType.BigInt, 9).Value = intOrder;
            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateContentById(int intId, string strContent)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGE SET" +
                     " PAGE_CONTENT = ?" +
                     " WHERE PAGE_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strContent;
            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                id = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int updateMobileContentById(int intId, string strContent)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGE SET" +
                     " PAGE_MOBILECONTENT = ?" +
                     " WHERE PAGE_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strContent;
            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                id = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deletePageById(int intId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PAGE WHERE PAGE_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deletePageById(int intId, int intSelId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PAGECORRESPOND WHERE PAGE_ID = ? AND PC_PAGEID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;
            cmd.Parameters.Add("p_PC_PAGEID", OdbcType.BigInt, 9).Value = intSelId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                id = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    //Mobile View
    public Boolean extractMobilePageById(int intId, int intShowInMobile, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PAGE_ID, A.PAGE_DISPLAYNAME, A.PAGE_TITLE, A.PAGE_SHOWPAGETITLE, A.PAGE_MOBILETEMPLATE, A.PAGE_MOBILECONTENT" +
                     " FROM TB_PAGE A" +
                     " WHERE A.PAGE_ID = ? ";

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intId;

            if (intShowInMobile != 0)
            {
                strSql += " AND A.PAGE_SHOWMOBILEVIEW = ?";
                cmd.Parameters.Add("p_PAGE_SHOWMOBILEVIEW", OdbcType.Int, 1).Value = intShowInMobile;
            }

            if (intActive != 0)
            {
                strSql += " AND A.PAGE_ACTIVE = ?";
                cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                id = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_ID"].ToString());
                displayName = dataSet.Tables[0].Rows[0]["PAGE_DISPLAYNAME"].ToString();
                title = dataSet.Tables[0].Rows[0]["PAGE_TITLE"].ToString();
                showPageTitle = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAGE_SHOWPAGETITLE"]);
                mobileTemplate = dataSet.Tables[0].Rows[0]["PAGE_MOBILETEMPLATE"].ToString();
                mobileContent = dataSet.Tables[0].Rows[0]["PAGE_MOBILECONTENT"].ToString();
            }

        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int getMobileFirstPageByTemplate(string strTemplate, int intShowInMobile, int intActive)
    {
        int intId = 0;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PAGE_ID" +
                     " FROM TB_PAGE A" +
                     " WHERE A.PAGE_MOBILETEMPLATE = ? ";

            cmd.Parameters.Add("p_PAGE_MOBILETEMPLATE", OdbcType.VarChar, 250).Value = strTemplate;

            if (intShowInMobile != 0)
            {
                strSql += " AND A.PAGE_SHOWMOBILEVIEW = ?";
                cmd.Parameters.Add("p_PAGE_SHOWMOBILEVIEW", OdbcType.Int, 1).Value = intShowInMobile;
            }

            if (intActive != 0)
            {
                strSql += " AND A.PAGE_ACTIVE = ?";
                cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            intId = Convert.ToInt32(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intId;
    }

    public DataSet getMobilePageList(int intShowInMobile, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            //strSql = "SELECT A.PAGE_ID, A.PAGE_DISPLAYNAME, A.PAGE_TITLEFURL, A.PAGE_MOBILETEMPLATE, A.PAGE_ORDER, A.PAGE_LANG, A.PAGE_TEMPLATE, A.PAGE_PARENT, A.PAGE_MOBILECONTENT, A.PAGE_MOBILESHOWINMENU, A.PAGE_MOBILESHOWTOP, A.PAGE_MOBILESHOWBOTTOM, A.PAGE_MOBILECSSCLASS, A.PAGE_ORDER" +
            //         " FROM TB_PAGE A" +
            //         " WHERE 1 = 1";
            strSql = "SELECT A.PAGE_ID, A.PAGE_DISPLAYNAME, A.PAGE_TITLEFURL, A.PAGE_ENABLELINK, A.PAGE_ORDER, A.PAGE_LANG, A.PAGE_TEMPLATE, A.PAGE_PARENT, " +
                   "C.PAGE_MOBILETEMPLATE AS V_MOBILETEMPLATEPARENT, A.PAGE_REDIRECT, A.PAGE_REDIRECTLINK, A.PAGE_REDIRECTTARGET, A.PAGE_TEMPLATEPARENT, A.PAGE_OTHER, A.PAGE_MOBILETEMPLATE, A.PAGE_MOBILECONTENT, " +
                   "A.PAGE_MOBILESHOWINMENU, A.PAGE_MOBILESHOWTOP, A.PAGE_MOBILESHOWBOTTOM, A.PAGE_MOBILECSSCLASS, A.PAGE_ORDER, " +
                   "A.PAGE_MOBILEREDIRECT, A.PAGE_MOBILEREDIRECTLINK, A.PAGE_MOBILEREDIRECTTARGET" +
                   " FROM TB_PAGE A LEFT JOIN TB_PAGE C ON (A.PAGE_PARENT = C.PAGE_ID)" +
                   " WHERE 1 = 1";

            if (intShowInMobile != 0)
            {
                strSql += " AND A.PAGE_SHOWMOBILEVIEW = ?";
                cmd.Parameters.Add("p_PAGE_SHOWMOBILEVIEW", OdbcType.Int, 1).Value = intShowInMobile;
            }

            if (intActive != 0)
            {
                strSql += " AND A.PAGE_ACTIVE = ?";
                cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    //End Mobile View

    public bool getPageTemplateById(string strId)
    {
        string strTemplate = "";

        if (HttpContext.Current.Application["PAGELIST"] == null)
        {
            clsPage page = new clsPage();
            HttpContext.Current.Application["PAGELIST"] = page.getPageList(1);
        }

        DataSet ds = new DataSet();
        ds = (DataSet)HttpContext.Current.Application["PAGELIST"];

        DataRow[] foundRow;

        foundRow = ds.Tables[0].Select("PAGE_ID = '" + strId + "'");

        if (foundRow.Length > 0) { template = foundRow[0]["PAGE_TEMPLATE"].ToString(); return true; }

        return false;
    }


    //Backup Page
    public int addPageBackup(int intPageId, string strContent, int intPageType, int intBackupBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PAGEBACKUP (" +
                    "PAGE_ID, " +
                    "PAGEBACKUP_CONTENT, " +
                    "PAGEBACKUP_TYPE, " +
                    "PAGEBACKUP_DATE, " +
                    "PAGEBACKUP_BY " +
                    ") VALUES " +
                   "(?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;
            cmd.Parameters.Add("p_PAGEBACKUP_CONTENT", OdbcType.Text).Value = strContent;
            cmd.Parameters.Add("p_PAGEBACKUP_TYPE", OdbcType.Int, 1).Value = intPageType;
            cmd.Parameters.Add("p_PAGEBACKUP_BY", OdbcType.Int, 9).Value = intBackupBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                id = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteOldPageBackup(int intPageId, int intPageType)
    {
        int intRecordAffected = 0;
        int intPageBackupId = getOldPageBackupId(intPageId, intPageType);

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PAGEBACKUP WHERE PAGEBACKUP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGEBACKUP_ID", OdbcType.BigInt, 9).Value = intPageBackupId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getPageBackupList(int intPageId, int intPageType)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_PAGEBACKUP WHERE 1 = 1";

            if (intPageId != 0)
            {
                strSql += " AND PAGE_ID = ?";
                cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;
            }

            if (intPageType != 0)
            {
                strSql += " AND PAGEBACKUP_TYPE = ?";
                cmd.Parameters.Add("p_PAGEBACKUP_TYPE", OdbcType.Int, 1).Value = intPageType;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public string getPageBackupContentById(int intBackupId)
    {
        string strBackupCont = "";

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT PAGEBACKUP_CONTENT FROM TB_PAGEBACKUP" +
                     " WHERE 1 = 1";

            if (intBackupId != 0)
            {
                strSql += " AND PAGEBACKUP_ID = ?";
                cmd.Parameters.Add("p_PAGEBACKUP_ID", OdbcType.BigInt, 9).Value = intBackupId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            strBackupCont = cmd.ExecuteScalar().ToString();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return strBackupCont;
    }

    public string getLatestPageContent(int intPageId, int intPageType)
    {
        string strLatestCont = "";

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT PAGEBACKUP_CONTENT FROM TB_PAGEBACKUP"+
                     " WHERE 1 = 1";

            if (intPageId != 0)
            {
                strSql += " AND PAGE_ID = ?";
                cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;
            }

            if (intPageType != 0)
            {
                strSql += " AND PAGEBACKUP_TYPE = ?";
                cmd.Parameters.Add("p_PAGEBACKUP_TYPE", OdbcType.Int, 1).Value = intPageType;
            }

            strSql += " ORDER BY PAGEBACKUP_ID DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            strLatestCont = cmd.ExecuteScalar().ToString();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return strLatestCont;
    }

    public int getTotalBackupPages(int intPageId, int intPageType)
    {
        int intTotal = 0;

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            strSql = "SELECT COUNT(*) AS TOTAL FROM TB_PAGEBACKUP" +
                     " WHERE 1 = 1";

            if (intPageId != 0)
            {
                strSql += " AND PAGE_ID = ?";
                cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;
            }

            if (intPageType != 0)
            {
                strSql += " AND PAGEBACKUP_TYPE = ?";
                cmd.Parameters.Add("p_PAGEBACKUP_TYPE", OdbcType.Int, 1).Value = intPageType;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            intTotal = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intTotal;
    }

    public int getOldPageBackupId(int intPageId, int intPageType)
    {
        int intPageBackupId = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT PAGEBACKUP_ID FROM TB_PAGEBACKUP" +
                     " WHERE 1 = 1";

            if (intPageId != 0)
            {
                strSql += " AND PAGE_ID = ?";
                cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;
            }

            if (intPageType != 0)
            {
                strSql += " AND PAGEBACKUP_TYPE = ?";
                cmd.Parameters.Add("p_PAGEBACKUP_TYPE", OdbcType.Int, 1).Value = intPageType;
            }

            strSql += " LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            intPageBackupId = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intPageBackupId;
    }

    //End Backup Page

    #region "Page Authorization"
    public int addUniPageAuthorization(string strGroup, string strUniLogin, string strUniPwd)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PAGEAUTHORIZATION (" +
                    "PGAUTH_GROUP, " +
                    "PGAUTH_UNILOGIN, " +
                    "PGAUTH_UNIPWD" +
                    ") VALUES " +
                   "(?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PGAUTH_GROUP", OdbcType.VarChar, 50).Value = strGroup;
            cmd.Parameters.Add("p_PGAUTH_UNILOGIN", OdbcType.VarChar, 250).Value = strUniLogin;
            cmd.Parameters.Add("p_PGAUTH_UNIPWD", OdbcType.VarChar, 250).Value = strUniPwd;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                pgAuthid = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addIndPageAuthorization(string strIndGroup, string strIndCompName, string strIndContactPerson, string strIndEmail, string strIndContactNo, string strIndRemarks, string strIndLogin, string strIndPwd)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PAGEAUTHORIZATION (" +
                    "PGAUTH_GROUP, " +
                    "PGAUTH_INDCOMPANYNAME, " +
                    "PGAUTH_INDCONTACTPERSON, " +
                    "PGAUTH_INDEMAIL, " +
                    "PGAUTH_INDCONTACTNO, " +
                    "PGAUTH_INDREMARKS, " +
                    "PGAUTH_INDLOGIN, " +
                    "PGAUTH_INDPWD" +
                    ") VALUES " +
                   "(?,?,?,?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PGAUTH_GROUP", OdbcType.VarChar, 50).Value = strIndGroup;
            cmd.Parameters.Add("p_PGAUTH_INDCOMPANYNAME", OdbcType.VarChar, 250).Value = strIndCompName;
            cmd.Parameters.Add("p_PGAUTH_INDCONTACTPERSON", OdbcType.VarChar, 250).Value = strIndContactPerson;
            cmd.Parameters.Add("p_PGAUTH_INDEMAIL", OdbcType.VarChar, 250).Value = strIndEmail;
            cmd.Parameters.Add("p_PGAUTH_INDCONTACTNO", OdbcType.VarChar, 20).Value = strIndContactNo;
            cmd.Parameters.Add("p_PGAUTH_INDREMARKS", OdbcType.VarChar, 1000).Value = strIndRemarks;
            cmd.Parameters.Add("p_PGAUTH_INDLOGIN", OdbcType.VarChar, 250).Value = strIndLogin;
            cmd.Parameters.Add("p_PGAUTH_INDPWD", OdbcType.VarChar, 250).Value = strIndPwd;
 
            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                pgAuthid = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addIndPageAuthorization(string strIndGroup, string strIndCompName, string strIndContactPerson, string strIndEmail, string strIndContactNo, string strIndFaxNo, string strIndMobileNo,
                                       string strIndCountry, string strIndCity, string strIndAddress, string strIndMemberSince, string strIndRemarks, string strIndLogin, string strIndPwd)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PAGEAUTHORIZATION (" +
                    "PGAUTH_GROUP, " +
                    "PGAUTH_INDCOMPANYNAME, " +
                    "PGAUTH_INDCONTACTPERSON, " +
                    "PGAUTH_INDEMAIL, " +
                    "PGAUTH_INDCONTACTNO, " +
                    "PGAUTH_INDFAXNO, " +
                    "PGAUTH_INDMOBILENO, " +
                    "PGAUTH_INDCOUNTRY, " +
                    "PGAUTH_INDCITY, " +
                    "PGAUTH_INDADDRESS, " +
                    "PGAUTH_INDMEMBERSINCE, " +
                    "PGAUTH_INDREMARKS, " +
                    "PGAUTH_INDLOGIN, " +
                    "PGAUTH_INDPWD" +
                    ") VALUES " +
                   "(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PGAUTH_GROUP", OdbcType.VarChar, 50).Value = strIndGroup;
            cmd.Parameters.Add("p_PGAUTH_INDCOMPANYNAME", OdbcType.VarChar, 250).Value = strIndCompName;
            cmd.Parameters.Add("p_PGAUTH_INDCONTACTPERSON", OdbcType.VarChar, 250).Value = strIndContactPerson;
            cmd.Parameters.Add("p_PGAUTH_INDEMAIL", OdbcType.VarChar, 250).Value = strIndEmail;
            cmd.Parameters.Add("p_PGAUTH_INDCONTACTNO", OdbcType.VarChar, 20).Value = strIndContactNo;
            cmd.Parameters.Add("p_PGAUTH_INDFAXNO", OdbcType.VarChar, 20).Value = strIndFaxNo;
            cmd.Parameters.Add("p_PGAUTH_INDMOBILENO", OdbcType.VarChar, 20).Value = strIndMobileNo;
            cmd.Parameters.Add("p_PGAUTH_INDCOUNTRY", OdbcType.VarChar, 10).Value = strIndCountry;
            cmd.Parameters.Add("p_PGAUTH_INDCITY", OdbcType.VarChar, 50).Value = strIndCity;
            cmd.Parameters.Add("p_PGAUTH_INDADDRESS", OdbcType.VarChar, 250).Value = strIndAddress;
            cmd.Parameters.Add("p_PGAUTH_INDMEMBERSINCE", OdbcType.VarChar, 10).Value = strIndMemberSince;
            cmd.Parameters.Add("p_PGAUTH_INDREMARKS", OdbcType.VarChar, 1000).Value = strIndRemarks;
            cmd.Parameters.Add("p_PGAUTH_INDLOGIN", OdbcType.VarChar, 250).Value = strIndLogin;
            cmd.Parameters.Add("p_PGAUTH_INDPWD", OdbcType.VarChar, 250).Value = strIndPwd;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                pgAuthid = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameUniPageAuthDataSet(string strGroup, string strUniLogin)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PAGEAUTHORIZATION" +
                     " WHERE PGAUTH_GROUP = ?" +
                     " AND BINARY PGAUTH_UNILOGIN = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PGAUTH_GROUP", OdbcType.VarChar, 50).Value = strGroup;
            cmd.Parameters.Add("p_PGAUTH_UNILOGIN", OdbcType.VarChar, 250).Value = strUniLogin;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public Boolean isExactSameIndPageAuthDataSet(int intId, string strIndCompName, string strIndContactPerson, string strIndEmail, string strIndContactNo, string strIndRemarks, string strIndLogin)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PAGEAUTHORIZATION" +
                     " WHERE PGAUTH_ID = ?" +
                     " AND BINARY PGAUTH_INDCOMPANYNAME = ?" +
                     " AND BINARY PGAUTH_INDCONTACTPERSON = ?" +
                     " AND BINARY PGAUTH_INDEMAIL = ?" +
                     " AND BINARY PGAUTH_INDCONTACTNO = ?" +
                     " AND BINARY PGAUTH_INDREMARKS = ?" +
                     " AND BINARY PGAUTH_INDLOGIN = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PGAUTH_ID", OdbcType.BigInt, 9).Value = intId;
            cmd.Parameters.Add("p_PGAUTH_INDCOMPANYNAME", OdbcType.VarChar, 250).Value = strIndCompName;
            cmd.Parameters.Add("p_PGAUTH_INDCONTACTPERSON", OdbcType.VarChar, 250).Value = strIndContactPerson;
            cmd.Parameters.Add("p_PGAUTH_INDEMAIL", OdbcType.VarChar, 250).Value = strIndEmail;
            cmd.Parameters.Add("p_PGAUTH_INDCONTACTNO", OdbcType.VarChar, 20).Value = strIndContactNo;
            cmd.Parameters.Add("p_PGAUTH_INDREMARKS", OdbcType.VarChar, 1000).Value = strIndRemarks;
            cmd.Parameters.Add("p_PGAUTH_INDLOGIN", OdbcType.VarChar, 250).Value = strIndLogin;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public Boolean isExactSameIndPageAuthDataSet(int intId, string strIndCompName, string strIndContactPerson, string strIndEmail, string strIndContactNo, string strIndFaxNo, string strIndMobileNo,
                                                 string strIndCountry, string strIndCity, string strIndAddress, string strIndMemberSince, string strIndRemarks, string strIndLogin)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PAGEAUTHORIZATION" +
                     " WHERE PGAUTH_ID = ?" +
                     " AND BINARY PGAUTH_INDCOMPANYNAME = ?" +
                     " AND BINARY PGAUTH_INDCONTACTPERSON = ?" +
                     " AND BINARY PGAUTH_INDEMAIL = ?" +
                     " AND BINARY PGAUTH_INDCONTACTNO = ?" +
                     " AND BINARY PGAUTH_INDFAXNO = ?" +
                     " AND BINARY PGAUTH_INDMOBILENO = ?" +
                     " AND BINARY PGAUTH_INDCOUNTRY = ?" +
                     " AND BINARY PGAUTH_INDCITY = ?" +
                     " AND BINARY PGAUTH_INDADDRESS = ?" +
                     " AND BINARY PGAUTH_INDMEMBERSINCE = ?" +
                     " AND BINARY PGAUTH_INDREMARKS = ?" +
                     " AND BINARY PGAUTH_INDLOGIN = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PGAUTH_ID", OdbcType.BigInt, 9).Value = intId;
            cmd.Parameters.Add("p_PGAUTH_INDCOMPANYNAME", OdbcType.VarChar, 250).Value = strIndCompName;
            cmd.Parameters.Add("p_PGAUTH_INDCONTACTPERSON", OdbcType.VarChar, 250).Value = strIndContactPerson;
            cmd.Parameters.Add("p_PGAUTH_INDEMAIL", OdbcType.VarChar, 250).Value = strIndEmail;
            cmd.Parameters.Add("p_PGAUTH_INDCONTACTNO", OdbcType.VarChar, 20).Value = strIndContactNo;
            cmd.Parameters.Add("p_PGAUTH_INDFAXNO", OdbcType.VarChar, 20).Value = strIndFaxNo;
            cmd.Parameters.Add("p_PGAUTH_INDMOBILENO", OdbcType.VarChar, 20).Value = strIndMobileNo;
            cmd.Parameters.Add("p_PGAUTH_INDCOUNTRY", OdbcType.VarChar, 10).Value = strIndCountry;
            cmd.Parameters.Add("p_PGAUTH_INDCITY", OdbcType.VarChar, 50).Value = strIndCity;
            cmd.Parameters.Add("p_PGAUTH_INDADDRESS", OdbcType.VarChar, 250).Value = strIndAddress;
            cmd.Parameters.Add("p_PGAUTH_INDMEMBERSINCE", OdbcType.VarChar, 10).Value = strIndMemberSince;
            cmd.Parameters.Add("p_PGAUTH_INDREMARKS", OdbcType.VarChar, 1000).Value = strIndRemarks;
            cmd.Parameters.Add("p_PGAUTH_INDLOGIN", OdbcType.VarChar, 250).Value = strIndLogin;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateUniPageAuthById(string strGroup, string strUniLogin)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGEAUTHORIZATION SET" +
                     " PGAUTH_UNILOGIN = ?" +
                     " WHERE PGAUTH_GROUP = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PGAUTH_UNILOGIN", OdbcType.VarChar, 250).Value = strUniLogin;
            cmd.Parameters.Add("p_PGAUTH_GROUP", OdbcType.VarChar, 50).Value = strGroup;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                //pgAuthid = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateIndPageAuthById(int intId, string strIndCompName, string strIndContactPerson, string strIndEmail, string strIndContactNo, string strIndRemarks, string strIndLogin)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGEAUTHORIZATION SET" +
                     " PGAUTH_INDCOMPANYNAME = ?," +
                     " PGAUTH_INDCONTACTPERSON = ?," +
                     " PGAUTH_INDEMAIL = ?," +
                     " PGAUTH_INDCONTACTNO = ?," +
                     " PGAUTH_INDREMARKS = ?," +
                     " PGAUTH_INDLOGIN = ?" +
                     " WHERE PGAUTH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PGAUTH_INDCOMPANYNAME", OdbcType.VarChar, 250).Value = strIndCompName;
            cmd.Parameters.Add("p_PGAUTH_INDCONTACTPERSON", OdbcType.VarChar, 250).Value = strIndContactPerson;
            cmd.Parameters.Add("p_PGAUTH_INDEMAIL", OdbcType.VarChar, 250).Value = strIndEmail;
            cmd.Parameters.Add("p_PGAUTH_INDCONTACTNO", OdbcType.VarChar, 20).Value = strIndContactNo;
            cmd.Parameters.Add("p_PGAUTH_INDREMARKS", OdbcType.VarChar, 1000).Value = strIndRemarks;
            cmd.Parameters.Add("p_PGAUTH_INDLOGIN", OdbcType.VarChar, 250).Value = strIndLogin;
            cmd.Parameters.Add("p_PGAUTH_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                pgAuthid = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateIndPageAuthById(int intId, string strIndCompName, string strIndContactPerson, string strIndEmail, string strIndContactNo, string strIndFaxNo, string strIndMobileNo,
                                       string strIndCountry, string strIndCity, string strIndAddress, string strIndMemberSince, string strIndRemarks, string strIndLogin)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGEAUTHORIZATION SET" +
                     " PGAUTH_INDCOMPANYNAME = ?," +
                     " PGAUTH_INDCONTACTPERSON = ?," +
                     " PGAUTH_INDEMAIL = ?," +
                     " PGAUTH_INDCONTACTNO = ?," +
                     " PGAUTH_INDFAXNO = ?," +
                     " PGAUTH_INDMOBILENO = ?," +
                     " PGAUTH_INDCOUNTRY = ?," +
                     " PGAUTH_INDCITY = ?," +
                     " PGAUTH_INDADDRESS = ?," +
                     " PGAUTH_INDMEMBERSINCE = ?," +
                     " PGAUTH_INDREMARKS = ?," +
                     " PGAUTH_INDLOGIN = ?" +
                     " WHERE PGAUTH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PGAUTH_INDCOMPANYNAME", OdbcType.VarChar, 250).Value = strIndCompName;
            cmd.Parameters.Add("p_PGAUTH_INDCONTACTPERSON", OdbcType.VarChar, 250).Value = strIndContactPerson;
            cmd.Parameters.Add("p_PGAUTH_INDEMAIL", OdbcType.VarChar, 250).Value = strIndEmail;
            cmd.Parameters.Add("p_PGAUTH_INDCONTACTNO", OdbcType.VarChar, 20).Value = strIndContactNo;
            cmd.Parameters.Add("p_PGAUTH_INDFAXNO", OdbcType.VarChar, 20).Value = strIndFaxNo;
            cmd.Parameters.Add("p_PGAUTH_INDMOBILENO", OdbcType.VarChar, 20).Value = strIndMobileNo;
            cmd.Parameters.Add("p_PGAUTH_INDCOUNTRY", OdbcType.VarChar, 10).Value = strIndCountry;
            cmd.Parameters.Add("p_PGAUTH_INDCITY", OdbcType.VarChar, 50).Value = strIndCity;
            cmd.Parameters.Add("p_PGAUTH_INDADDRESS", OdbcType.VarChar, 250).Value = strIndAddress;
            cmd.Parameters.Add("p_PGAUTH_INDMEMBERSINCE", OdbcType.VarChar, 10).Value = strIndMemberSince;
            cmd.Parameters.Add("p_PGAUTH_INDREMARKS", OdbcType.VarChar, 1000).Value = strIndRemarks;
            cmd.Parameters.Add("p_PGAUTH_INDLOGIN", OdbcType.VarChar, 250).Value = strIndLogin;
            cmd.Parameters.Add("p_PGAUTH_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                pgAuthid = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateUniPasswordById(string strGroup, string strPassword)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGEAUTHORIZATION SET PGAUTH_UNIPWD = ? WHERE PGAUTH_GROUP = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PGAUTH_UNIPWD", OdbcType.VarChar, 250).Value = strPassword;
            cmd.Parameters.Add("p_PGAUTH_GROUP", OdbcType.VarChar, 50).Value = strGroup;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                //pgAuthid = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateIndPasswordById(int intId, string strPassword)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGEAUTHORIZATION SET PGAUTH_INDPWD = ? WHERE PGAUTH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PGAUTH_INDPWD", OdbcType.VarChar, 250).Value = strPassword;
            cmd.Parameters.Add("p_PGAUTH_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                pgAuthid = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getPageAuthList()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PGAUTH_ID, A.PGAUTH_GROUP, A.PGAUTH_UNILOGIN, A.PGAUTH_UNIPWD, A.PGAUTH_INDCOMPANYNAME, A.PGAUTH_INDCONTACTPERSON, A.PGAUTH_INDEMAIL," +
                     " A.PGAUTH_INDCONTACTNO, A.PGAUTH_INDREMARKS, A.PGAUTH_INDLOGIN, A.PGAUTH_INDPWD" +
                     " FROM TB_PAGEAUTHORIZATION A";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean extractPageAuthById(int intId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PGAUTH_ID, A.PGAUTH_UNILOGIN, A.PGAUTH_UNIPWD, A.PGAUTH_INDCOMPANYNAME, A.PGAUTH_INDCONTACTPERSON, A.PGAUTH_INDEMAIL," +
                     " A.PGAUTH_INDCONTACTNO, A.PGAUTH_INDREMARKS, A.PGAUTH_INDLOGIN, A.PGAUTH_INDPWD" +
                     " FROM TB_PAGEAUTHORIZATION A" +
                     " WHERE A.PGAUTH_ID = ? ";

            cmd.Parameters.Add("p_PGAUTH_ID", OdbcType.BigInt, 9).Value = intId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                pgAuthid = int.Parse(dataSet.Tables[0].Rows[0]["PGAUTH_ID"].ToString());
                uniLogin = dataSet.Tables[0].Rows[0]["PGAUTH_UNILOGIN"].ToString();
                uniPwd = dataSet.Tables[0].Rows[0]["PGAUTH_UNIPWD"].ToString();
                IndCompName = dataSet.Tables[0].Rows[0]["PGAUTH_INDCOMPANYNAME"].ToString();
                IndContactPerson = dataSet.Tables[0].Rows[0]["PGAUTH_INDCONTACTPERSON"].ToString();
                IndEmail = dataSet.Tables[0].Rows[0]["PGAUTH_INDEMAIL"].ToString();
                IndContactNo = dataSet.Tables[0].Rows[0]["PGAUTH_INDCONTACTNO"].ToString();
                IndRemarks = dataSet.Tables[0].Rows[0]["PGAUTH_INDREMARKS"].ToString();
                IndLogin = dataSet.Tables[0].Rows[0]["PGAUTH_INDLOGIN"].ToString();
                IndPwd = dataSet.Tables[0].Rows[0]["PGAUTH_INDPWD"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractPageAuthById2(int intId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PGAUTH_ID, A.PGAUTH_UNILOGIN, A.PGAUTH_UNIPWD, A.PGAUTH_INDCOMPANYNAME, A.PGAUTH_INDCONTACTPERSON, A.PGAUTH_INDEMAIL," +
                     " A.PGAUTH_INDCONTACTNO, A.PGAUTH_INDFAXNO, A.PGAUTH_INDMOBILENO, A.PGAUTH_INDCOUNTRY, A.PGAUTH_INDCITY, A.PGAUTH_INDADDRESS," +
                     " A.PGAUTH_INDMEMBERSINCE, A.PGAUTH_INDREMARKS, A.PGAUTH_INDLOGIN, A.PGAUTH_INDPWD" +
                     " FROM TB_PAGEAUTHORIZATION A" +
                     " WHERE A.PGAUTH_ID = ? ";

            cmd.Parameters.Add("p_PGAUTH_ID", OdbcType.BigInt, 9).Value = intId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                pgAuthid = int.Parse(dataSet.Tables[0].Rows[0]["PGAUTH_ID"].ToString());
                uniLogin = dataSet.Tables[0].Rows[0]["PGAUTH_UNILOGIN"].ToString();
                uniPwd = dataSet.Tables[0].Rows[0]["PGAUTH_UNIPWD"].ToString();
                IndCompName = dataSet.Tables[0].Rows[0]["PGAUTH_INDCOMPANYNAME"].ToString();
                IndContactPerson = dataSet.Tables[0].Rows[0]["PGAUTH_INDCONTACTPERSON"].ToString();
                IndEmail = dataSet.Tables[0].Rows[0]["PGAUTH_INDEMAIL"].ToString();
                IndContactNo = dataSet.Tables[0].Rows[0]["PGAUTH_INDCONTACTNO"].ToString();
                IndFaxNo = dataSet.Tables[0].Rows[0]["PGAUTH_INDFAXNO"].ToString();
                IndMobileNo = dataSet.Tables[0].Rows[0]["PGAUTH_INDMOBILENO"].ToString();
                IndCountry = dataSet.Tables[0].Rows[0]["PGAUTH_INDCOUNTRY"].ToString();
                IndCity = dataSet.Tables[0].Rows[0]["PGAUTH_INDCITY"].ToString();
                IndAddress = dataSet.Tables[0].Rows[0]["PGAUTH_INDADDRESS"].ToString();
                IndMemSince = dataSet.Tables[0].Rows[0]["PGAUTH_INDMEMBERSINCE"].ToString();
                IndRemarks = dataSet.Tables[0].Rows[0]["PGAUTH_INDREMARKS"].ToString();
                IndLogin = dataSet.Tables[0].Rows[0]["PGAUTH_INDLOGIN"].ToString();
                IndPwd = dataSet.Tables[0].Rows[0]["PGAUTH_INDPWD"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean isUniLoginExist(string strGroup)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_PAGEAUTHORIZATION" +
                     " WHERE PGAUTH_GROUP = ?";

            cmd.Parameters.Add("p_PGAUTH_GROUP", OdbcType.VarChar, 50).Value = strGroup;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExist;
    }

    public int deleteIndPageAuth(int intPgAuthId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PAGEAUTHORIZATION WHERE PGAUTH_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PGAUTH_ID", OdbcType.Int, 9).Value = intPgAuthId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isIndLoginPageAuthExist(string strIndPageLogin, int intPageAuthId)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_PAGEAUTHORIZATION" +
                     " WHERE PGAUTH_INDLOGIN = ?";

            cmd.Parameters.Add("p_PGAUTH_INDLOGIN", OdbcType.VarChar, 250).Value = strIndPageLogin;

            if (intPageAuthId > 0)
            {
                strSql += " AND PGAUTH_ID != ?";
                cmd.Parameters.Add("p_PGAUTH_ID", OdbcType.Int, 9).Value = intPageAuthId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExist;
    }
    #endregion

    #endregion


    #region "Static Methods"
    public static int getPageIdByTemplate(string strTemplate, string strLanguage, int intOrder)
    {
        int intId = 0;

        intOrder = intOrder > 0 ? intOrder - 1 : 0;

        if (HttpContext.Current.Application["PAGELIST"] == null)
        {
            clsPage page = new clsPage();
            HttpContext.Current.Application["PAGELIST"] = page.getPageList(1);
        }

        DataSet ds = new DataSet();
        ds = (DataSet)HttpContext.Current.Application["PAGELIST"];

        DataRow[] foundRow;

        if (!string.IsNullOrEmpty(strLanguage)) { foundRow = ds.Tables[0].Select("PAGE_TEMPLATE = '" + strTemplate + "' AND PAGE_LANG = '" + strLanguage + "'", "PAGE_ORDER ASC"); }
        else { foundRow = ds.Tables[0].Select("PAGE_TEMPLATE = '" + strTemplate + "'", "PAGE_ORDER ASC"); }

        if (foundRow.Length > 0) { intId = Convert.ToInt16(foundRow[intOrder]["PAGE_ID"]); }

        return intId;
    }

    
    #endregion
}
