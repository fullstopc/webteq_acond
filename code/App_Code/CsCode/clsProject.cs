﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;
using System.Linq;

/// <summary>
/// Summary description for clsProject
/// </summary>
public class clsProject : dbconnBase
{
    #region "Constants"


    // change accordingly to db
    #region "Config"
    #region "Group"
    public const string CONSTGROUPLIMIT = "LIMIT";
    #endregion


    #region "Name"
    public const string CONSTNAMEPAGELIMIT = "Page";
    public const string CONSTNAMEOBJECTLIMIT = "Object";
    public const string CONSTNAMESLIDESHOWLIMIT = "Slide Show";
    public const string CONSTNAMESLIDESHOWIMAGELIMIT = "Slide Show Image";
    public const string CONSTNAMEEVENTLIMIT = "Event";
    public const string CONSTNAMECATEGORYLIMIT = "Category";
    public const string CONSTNAMEPRODUCTLIMIT = "Product";
    public const string CONSTNAMEPROGGROUPLIMIT = "Program Group";
    public const string CONSTNAMEPROGRAMLIMIT = "Program";
    public const string CONSTNAMEFAQLIMIT = "FAQ";
    #endregion
    #endregion


    #endregion


    #region "Properties"
    private int _projId;
    private string _projName;
    private string _projServer;
    private string _projUserId;
    private string _projPwd;
    private string _projDatabase;
    private string _projOption;
    private string _projPort;
    private int _projActive;
    private int _projDriver;
    private string _projDriverName;
    private string _projUploadPath;
    private string _projUserviewUrl;
    private int _projType;
    private string _projTypeName;
    private DateTime _projCreation;
    private int _projCreatedBy;
    private DateTime _projLastUpdate;
    private int _projUpdatedBy;
    private int _projFullPath;
    private string _projFullPathValue;
    private int _projGroupMenu;
    private int _projGroupParentChild;
    private int _projGroupDesc;
    private int _projFAQ;
    private int _projProgManager;
    private int _projEventManager;
    private int _projProdDesc;
    private int _projProdGall;
    private int _projCouponManager;
    private int _projCouponManagerPackage;
    private int _projMultipleCurr;
    private int _projInventory;
    private int _projInventoryType;
    private int _projMobileView;
    private int _projGst;
    private int _projGstNotAvailable;
    private int _projGstInclusive;
    private int _projGstApplied;
    private int _projFbLogin;
    private int _projWatermark;
    private int _projFriendlyURL;
    private int _projSliderActive;
    private int _projSliderDefault;
    private int _projSliderFullScreen;
    private int _projSliderMobile;
    private string _ErrorMsg;
    private int _projImgSetting;
    private int _projFileSetting;
    private int _projCkeditorFileSetting;
    private int _projCkeditoImgSetting;
    private int _projDefaultSetting;
    private int _projEventThumbSetting;
    private int _projEventGallSetting;
    private int _projProdThumbSetting;
    private int _projProdGallSetting;
    private int _projTraining;
    private int _projProdPromo;
    private int _projPgAuthorization;
    private int _projUniversalLogin;
    private int _projIndividualLogin;
    private int _projDeliveryMsg;
    private int _projEventMaxImageUpload;
    private int _projRightClick;
    private int _projWebteqLogo;
    private int _projCkeditorTopmenu;
    private int _projSMSSetting;
    private int _projTopmenuRoot;
    private int _projDeliveryDatetime;
    private int _projProdAddon;
    private int _projFbCatalogActive;
    private DateTime _projFbCatalogExpired;
    private int _projEnquiryField;
    private string _projRandId;

    private int _configId;
    private string _configName;
    private string _configValue;
    private string _configLongValue;
    private string _configGroup;
    private int _configOrder;
    private string _configOperator;
    private string _configUnit;
    private string _configTag;
    private int _configActive;
    private DateTime _configCreation;
    private int _configCreatedBy;
    private DateTime _configLastUpdate;
    private int _configUpdatedBy;
    #endregion

    #region "Property Methods"
    public int projId
    {
        get { return _projId; }
        set { _projId = value; }
    }

    public string projName
    {
        get { return _projName; }
        set { _projName = value; }
    }

    public string projServer
    {
        get { return _projServer; }
        set { _projServer = value; }
    }

    public string projUserId
    {
        get { return _projUserId; }
        set { _projUserId = value; }
    }

    public string projPwd
    {
        get { return _projPwd; }
        set { _projPwd = value; }
    }

    public string projDatabase
    {
        get { return _projDatabase; }
        set { _projDatabase = value; }
    }

    public string projOption
    {
        get { return _projOption; }
        set { _projOption = value; }
    }

    public string projPort
    {
        get { return _projPort; }
        set { _projPort = value; }
    }

    public int projActive
    {
        get { return _projActive; }
        set { _projActive = value; }
    }

    public int projDriver
    {
        get { return _projDriver; }
        set { _projDriver = value; }
    }

    public string projDriverName
    {
        get { return _projDriverName; }
        set { _projDriverName = value; }
    }

    public string projUploadPath
    {
        get { return _projUploadPath; }
        set { _projUploadPath = value; }
    }

    public string projUserviewUrl
    {
        get { return _projUserviewUrl; }
        set { _projUserviewUrl = value; }
    }

    public int projType
    {
        get { return _projType; }
        set { _projType = value; }
    }

    public string projTypeName
    {
        get { return _projTypeName; }
        set { _projTypeName = value; }
    }

    public DateTime projCreation
    {
        get { return _projCreation; }
        set { _projCreation = value; }
    }

    public int projCreatedBy
    {
        get { return _projCreatedBy; }
        set { _projCreatedBy = value; }
    }

    public DateTime projLastUpdate
    {
        get { return _projLastUpdate; }
        set { _projLastUpdate = value; }
    }

    public int projUpdatedBy
    {
        get { return _projUpdatedBy; }
        set { _projUpdatedBy = value; }
    }

    public int projFullPath
    {
        get { return _projFullPath; }
        set { _projFullPath = value; }
    }

    public string projFullPathValue
    {
        get { return _projFullPathValue; }
        set { _projFullPathValue = value; }
    }

    public int projGroupMenu
    {
        get { return _projGroupMenu; }
        set { _projGroupMenu = value; }
    }

    public int projGroupParentChild
    {
        get { return _projGroupParentChild; }
        set { _projGroupParentChild = value; }
    }

    public int projGroupDesc
    {
        get { return _projGroupDesc; }
        set { _projGroupDesc = value; }
    }

    public int projFAQ
    {
        get { return _projFAQ; }
        set { _projFAQ = value; }
    }

    public int projProgManager
    {
        get { return _projProgManager; }
        set { _projProgManager = value; }
    }

    public int projEventManager
    {
        get { return _projEventManager; }
        set { _projEventManager = value; }
    }

    public int projProdDesc
    {
        get { return _projProdDesc; }
        set { _projProdDesc = value; }
    }

    public int projProdGall
    {
        get { return _projProdGall; }
        set { _projProdGall = value; }
    }

    public int projCouponManager
    {
        get { return _projCouponManager; }
        set { _projCouponManager = value; }
    }
    public int projCouponManagerPackage
    {
        get { return _projCouponManagerPackage; }
        set { _projCouponManagerPackage = value; }
    }
    public int projMultipleCurr
    {
        get { return _projMultipleCurr; }
        set { _projMultipleCurr = value; }
    }

    public int projInventory
    {
        get { return _projInventory; }
        set { _projInventory = value; }
    }

    public int projInventoryType
    {
        get { return _projInventoryType; }
        set { _projInventoryType = value; }
    }

    public int projMobileView
    {
        get { return _projMobileView; }
        set { _projMobileView = value; }
    }

    public int projGst
    {
        get { return _projGst; }
        set { _projGst = value; }
    }

    public int projGstNotAvailable
    {
        get { return _projGstNotAvailable; }
        set { _projGstNotAvailable = value; }
    }

    public int projGstInclusive
    {
        get { return _projGstInclusive; }
        set { _projGstInclusive = value; }
    }

    public int projGstApplied
    {
        get { return _projGstApplied; }
        set { _projGstApplied = value; }
    }
    public int projSliderActive
    {
        get { return _projSliderActive; }
        set { _projSliderActive = value; }
    }
    public int projSliderDefault
    {
        get { return _projSliderDefault; }
        set { _projSliderDefault = value; }
    }
    public int projSliderFullScreen
    {
        get { return _projSliderFullScreen; }
        set { _projSliderFullScreen = value; }
    }
    public int projSliderMobile
    {
        get { return _projSliderMobile; }
        set { _projSliderMobile = value; }
    }

    public int projFbLogin
    {
        get { return _projFbLogin; }
        set { _projFbLogin = value; }
    }

    public int projWatermark
    {
        get { return _projWatermark; }
        set { _projWatermark = value; }
    }

    public int projFriendlyURL
    {
        get { return _projFriendlyURL; }
        set { _projFriendlyURL = value; }
    }

    public string ErrorMsg
    {
        get { return _ErrorMsg; }
        set { _ErrorMsg = value; }
    }

    public int projFbCatalogActive
    {
        get { return _projFbCatalogActive; }
        set { _projFbCatalogActive = value; }
    }
    public DateTime projFbCatalogExpired
    {
        get { return _projFbCatalogExpired; }
        set { _projFbCatalogExpired = value; }
    }
    public int projEnquiryField
    {
        get { return _projEnquiryField; }
        set { _projEnquiryField = value; }
    }
    public string projRandId
    {
        get { return _projRandId; }
        set { _projRandId = value; }
    }
    #region "Config"
    public int configId
    {
        get { return _configId; }
        set { _configId = value; }
    }

    public string configName
    {
        get { return _configName; }
        set { _configName = value; }
    }

    public string configValue
    {
        get { return _configValue; }
        set { _configValue = value; }
    }

    public string configLongValue
    {
        get { return _configLongValue; }
        set { _configLongValue = value; }
    }

    public string configGroup
    {
        get { return _configGroup; }
        set { _configGroup = value; }
    }

    public int configOrder
    {
        get { return _configOrder; }
        set { _configOrder = value; }
    }

    public string configOperator
    {
        get { return _configOperator; }
        set { _configOperator = value; }
    }

    public string configUnit
    {
        get { return _configUnit; }
        set { _configUnit = value; }
    }

    public string configTag
    {
        get { return _configTag; }
        set { _configTag = value; }
    }

    public int configActive
    {
        get { return _configActive; }
        set { _configActive = value; }
    }

    public DateTime configCreation
    {
        get { return _configCreation; }
        set { _configCreation = value; }
    }

    public int configCreatedBy
    {
        get { return _configCreatedBy; }
        set { _configCreatedBy = value; }
    }

    public DateTime configLastUpdate
    {
        get { return _configLastUpdate; }
        set { _configLastUpdate = value; }
    }

    public int configUpdatedBy
    {
        get { return _configUpdatedBy; }
        set { _configUpdatedBy = value; }
    }

    public int projImgSetting
    {
        get { return _projImgSetting; }
        set { _projImgSetting = value; }
    }

    public int projFileSetting
    {
        get { return _projFileSetting; }
        set { _projFileSetting = value; }
    }

    public int projCkeditorFileSetting
    {
        get { return _projCkeditorFileSetting; }
        set { _projCkeditorFileSetting = value; }
    }

    public int projCkeditoImgSetting
    {
        get { return _projCkeditoImgSetting; }
        set { _projCkeditoImgSetting = value; }
    }

    public int projDefaultSetting
    {
        get { return _projDefaultSetting; }
        set { _projDefaultSetting = value; }
    }

    public int projEventThumbSetting
    {
        get { return _projEventThumbSetting; }
        set { _projEventThumbSetting = value; }
    }

    public int projEventGallSetting
    {
        get { return _projEventGallSetting; }
        set { _projEventGallSetting = value; }
    }

    public int projProdThumbSetting
    {
        get { return _projProdThumbSetting; }
        set { _projProdThumbSetting = value; }
    }

    public int projProdGallSetting
    {
        get { return _projProdGallSetting; }
        set { _projProdGallSetting = value; }
    }

    public int projTraining
    {
        get { return _projTraining; }
        set { _projTraining = value; }
    }

    public int projProdPromo
    {
        get { return _projProdPromo; }
        set { _projProdPromo = value; }
    }

    public int projPgAuthorization
    {
        get { return _projPgAuthorization; }
        set { _projPgAuthorization = value; }
    }

    public int projUniversalLogin
    {
        get { return _projUniversalLogin; }
        set { _projUniversalLogin = value; }
    }

    public int projIndividualLogin
    {
        get { return _projIndividualLogin; }
        set { _projIndividualLogin = value; }
    }

    public int projEventMaxImageUpload
    {
        get { return _projEventMaxImageUpload; }
        set { _projEventMaxImageUpload = value; }
    }

    public int projDeliveryMsg
    {
        get { return _projDeliveryMsg; }
        set { _projDeliveryMsg = value; }
    }

    public int projRightClick
    {
        get { return _projRightClick; }
        set { _projRightClick = value; }
    }

    public int projWebteqLogo
    {
        get { return _projWebteqLogo; }
        set { _projWebteqLogo = value; }
    }

    public int projCkeditorTopmenu
    {
        get { return _projCkeditorTopmenu; }
        set { _projCkeditorTopmenu = value; }
    }

    public int projSMSSetting
    {
        get { return _projSMSSetting; }
        set { _projSMSSetting = value; }
    }

    public int projTopmenuRoot
    {
        get { return _projTopmenuRoot; }
        set { _projTopmenuRoot = value; }
    }

    public int projDeliveryDatetime
    {
        get { return _projDeliveryDatetime; }
        set { _projDeliveryDatetime = value; }
    }
    public int projProdAddon
    {
        get { return _projProdAddon; }
        set { _projProdAddon = value; }
    }
    
    #endregion


    #endregion

    #region "Public Methods"
    public clsProject()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    #region "Details"
    public int addProject(clsProject input)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<string> columns = new List<string>()
            {
                "PROJ_NAME","PROJ_DRIVER","PROJ_SERVER","PROJ_USERID","PROJ_PWD",
                "PROJ_DATABASE","PROJ_OPTION","PROJ_PORT","PROJ_UPLOADPATH","PROJ_USERVIEWURL",
                "PROJ_TYPE","PROJ_PROGMANAGER","PROJ_EVENTMANAGER","PROJ_PRODDESC","PROJ_PRODGALL",
                "PROJ_ACTIVE","PROJ_CREATEDBY","PROJ_FULLPATH","PROJ_FULLPATHVALUE","PROJ_GROUPMENU",
                "PROJ_GROUPPARENTCHILD","PROJ_GROUPDESC","PROJ_FAQ","PROJ_COUPONMANAGER","PROJ_MULTIPLECURR",
                "PROJ_INVENTORY","PROJ_INVENTORYTYPE","PROJ_MOBILEVIEW","PROJ_FBLOGIN","PROJ_WATERMARK",
                "PROJ_FRIENDLYURL","PROJ_GST","PROJ_GST_NOTAVAILABLE","PROJ_GST_INCLUSIVE","PROJ_GST_APPLIED",
                "PROJ_IMGSETTING","PROJ_FILESETTING","PROJ_CKEDITORIMGSETTING","PROJ_CKEDITORFILESETTING","PROJ_DEFAULTSETTING",
                "PROJ_EVENTTHUMBSETTING","PROJ_EVENTGALLSETTING","PROJ_PRODTHUMBSETTING","PROJ_PRODGALLSETTING","PROJ_TRAINING",
                "PROJ_SLIDER_ACTIVE","PROJ_SLIDER_DEFAULT","PROJ_SLIDER_FULLSCREEN","PROJ_SLIDER_MOBILE","PROJ_DELIVERYMSG",
                "PROJ_PRODMULTIPLEPROMO","PROJ_PGAUTHORIZATION","PROJ_UNIVERSALLOGIN","PROJ_INDIVIDUALLOGIN","PROJ_RIGHTCLICK",
                "PROJ_WEBTEQLOGO","PROJ_CKEDITORTOPMENU","PROJ_SMSSETTING","PROJ_TOPMENUROOT","PROJ_DELIVERYDATETIME",
                "PROJ_PRODADDON", "PROJ_FBCATALOG_ACTIVE" , "PROJ_FBCATALOG_EXPIRED", "PROJ_ENQUIRY_FIELD","PROJ_COUPONMANAGER_PACKAGE",

                "PROJ_CREATION"
            };

            List<string> parameters = columns.Select((value, index) => (index == columns.Count - 1) ? "NOW()" : "?").ToList();

            strSql = "INSERT INTO TB_PROJECT (" + string.Join(",", columns.ToArray()) + ") VALUES (" + string.Join(",", parameters.ToArray()) + ")";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROJ_NAME", OdbcType.VarChar, 250).Value = input.projName;
            cmd.Parameters.Add("p_PROJ_DRIVER", OdbcType.Int, 1).Value = input.projDriver;
            cmd.Parameters.Add("p_PROJ_SERVER", OdbcType.VarChar, 50).Value = input.projServer;
            cmd.Parameters.Add("p_PROJ_USERID", OdbcType.VarChar, 50).Value = input.projUserId;
            cmd.Parameters.Add("p_PROJ_PWD", OdbcType.VarChar, 50).Value = input.projPwd;
            cmd.Parameters.Add("p_PROJ_DATABASE", OdbcType.VarChar, 50).Value = input.projDatabase;
            cmd.Parameters.Add("p_PROJ_OPTION", OdbcType.VarChar, 10).Value = input.projOption;
            cmd.Parameters.Add("p_PROJ_PORT", OdbcType.VarChar, 10).Value = input.projPort;
            cmd.Parameters.Add("p_PROJ_UPLOADPATH", OdbcType.VarChar, 1000).Value = input.projUploadPath;
            cmd.Parameters.Add("p_PROJ_USERVIEWURL", OdbcType.VarChar, 1000).Value = input.projUserviewUrl;
            cmd.Parameters.Add("p_PROJ_TYPE", OdbcType.Int, 9).Value = input.projType;
            cmd.Parameters.Add("p_PROJ_PROGMANAGER", OdbcType.Int, 1).Value = input.projProgManager;
            cmd.Parameters.Add("p_PROJ_EVENTMANAGER", OdbcType.Int, 1).Value = input.projEventManager;
            cmd.Parameters.Add("p_PROJ_PRODDESC", OdbcType.Int, 1).Value = input.projProdDesc;
            cmd.Parameters.Add("p_PROJ_PRODGALL", OdbcType.Int, 1).Value = input.projProdGall;
            cmd.Parameters.Add("p_PROJ_ACTIVE", OdbcType.Int, 1).Value = input.projActive;
            cmd.Parameters.Add("p_PROJ_CREATEDBY", OdbcType.BigInt, 9).Value = input.projCreatedBy;
            cmd.Parameters.Add("p_PROJ_FULLPATH", OdbcType.Int, 1).Value = input.projFullPath;
            cmd.Parameters.Add("p_PROJ_FULLPATHVALUE", OdbcType.VarChar, 250).Value = input.projFullPath;
            cmd.Parameters.Add("p_PROJ_GROUPMENU", OdbcType.Int, 1).Value = input.projGroupMenu;
            cmd.Parameters.Add("p_PROJ_GROUPPARENTCHILD", OdbcType.Int, 1).Value = input.projGroupParentChild;
            cmd.Parameters.Add("p_PROJ_GROUPDESC", OdbcType.Int, 1).Value = input.projGroupDesc;
            cmd.Parameters.Add("p_PROJ_FAQ", OdbcType.Int, 1).Value = input.projFAQ;
            cmd.Parameters.Add("p_PROJ_COUPONMANAGER", OdbcType.Int, 1).Value = input.projCouponManager;
            cmd.Parameters.Add("p_PROJ_MULTIPLECURR", OdbcType.Int, 1).Value = input.projMultipleCurr;
            cmd.Parameters.Add("p_PROJ_INVENTORY", OdbcType.Int, 1).Value = input.projInventory;
            cmd.Parameters.Add("p_PROJ_INVENTORYTYPE", OdbcType.Int, 1).Value = input.projInventoryType;
            cmd.Parameters.Add("p_PROJ_MOBILEVIEW", OdbcType.Int, 1).Value = input.projMobileView;
            cmd.Parameters.Add("p_PROJ_FBLOGIN", OdbcType.Int, 1).Value = input.projFbLogin;
            cmd.Parameters.Add("p_PROJ_WATERMARK", OdbcType.Int, 1).Value = input.projWatermark;
            cmd.Parameters.Add("p_PROJ_FRIENDLYURL", OdbcType.Int, 1).Value = input.projFriendlyURL;
            cmd.Parameters.Add("p_PROJ_GST", OdbcType.Int, 1).Value = input.projGst;
            cmd.Parameters.Add("p_PROJ_GST_NOTAVAILABLE", OdbcType.Int, 1).Value = input.projGstNotAvailable;
            cmd.Parameters.Add("p_PROJ_GST_INCLUSIVE", OdbcType.Int, 1).Value = input.projGstInclusive;
            cmd.Parameters.Add("p_PROJ_GST_APPLIED", OdbcType.Int, 1).Value = input.projGstApplied;
            cmd.Parameters.Add("p_PROJ_IMGSETTING", OdbcType.Int, 9).Value = input.projImgSetting;
            cmd.Parameters.Add("p_PROJ_FILESETTING", OdbcType.Int, 9).Value = input.projFileSetting;
            cmd.Parameters.Add("p_PROJ_CKEDITORIMGSETTING", OdbcType.Int, 9).Value = input.projCkeditoImgSetting;
            cmd.Parameters.Add("p_PROJ_CKEDITORFILESETTING", OdbcType.Int, 9).Value = input.projCkeditorFileSetting;
            cmd.Parameters.Add("p_PROJ_DEFAULTSETTING", OdbcType.Int, 9).Value = input.projDefaultSetting;
            cmd.Parameters.Add("p_PROJ_EVENTTHUMBSETTING", OdbcType.Int, 9).Value = input.projEventThumbSetting;
            cmd.Parameters.Add("p_PROJ_EVENTGALLSETTING", OdbcType.Int, 9).Value = input.projEventGallSetting;
            cmd.Parameters.Add("p_PROJ_PRODTHUMBSETTING", OdbcType.Int, 9).Value = input.projProdThumbSetting;
            cmd.Parameters.Add("p_PROJ_PRODGALLSETTING", OdbcType.Int, 9).Value = input.projProdGallSetting;
            cmd.Parameters.Add("p_PROJ_TRAINING", OdbcType.Int, 1).Value = input.projTraining;
            cmd.Parameters.Add("p_PROJ_SLIDER_ACTIVE", OdbcType.Int, 1).Value = input.projSliderActive;
            cmd.Parameters.Add("p_PROJ_SLIDER_DEFAULT", OdbcType.Int, 1).Value = input.projSliderDefault;
            cmd.Parameters.Add("p_PROJ_SLIDER_FULLSCREEN", OdbcType.Int, 1).Value = input.projSliderFullScreen;
            cmd.Parameters.Add("p_PROJ_SLIDER_MOBILE", OdbcType.Int, 1).Value = input.projSliderMobile;
            cmd.Parameters.Add("p_PROJ_DELIVERYMSG", OdbcType.Int, 1).Value = input.projDeliveryMsg;
            cmd.Parameters.Add("p_PROJ_PRODMULTIPLEPROMO", OdbcType.Int, 1).Value = input.projProdPromo;
            cmd.Parameters.Add("p_PROJ_PGAUTHORIZATION", OdbcType.Int, 1).Value = input.projPgAuthorization;
            cmd.Parameters.Add("p_PROJ_UNIVERSALLOGIN", OdbcType.Int, 1).Value = input.projUniversalLogin;
            cmd.Parameters.Add("p_PROJ_INDIVIDUALLOGIN", OdbcType.Int, 1).Value = input.projIndividualLogin;
            cmd.Parameters.Add("p_PROJ_RIGHTCLICK", OdbcType.Int, 1).Value = input.projRightClick;
            cmd.Parameters.Add("p_PROJ_WEBTEQLOGO", OdbcType.Int, 1).Value = input.projWebteqLogo;
            cmd.Parameters.Add("p_PROJ_CKEDITORTOPMENU", OdbcType.Int, 1).Value = input.projCkeditorTopmenu;
            cmd.Parameters.Add("p_PROJ_SMSSETTING", OdbcType.Int, 1).Value = input.projSMSSetting;
            cmd.Parameters.Add("p_PROJ_TOPMENUROOT", OdbcType.Int, 9).Value = input.projTopmenuRoot;
            cmd.Parameters.Add("p_PROJ_DELIVERYDATETIME", OdbcType.Int, 1).Value = input.projDeliveryDatetime;
            cmd.Parameters.Add("p_PROJ_PRODADDON", OdbcType.Int, 1).Value = input.projProdAddon;
            cmd.Parameters.Add("p_PROJ_FBCATALOG_ACTIVE", OdbcType.Int, 1).Value = input.projFbCatalogActive;
            cmd.Parameters.Add("p_PROJ_FBCATALOG_EXPIRED", OdbcType.DateTime).Value = input.projFbCatalogExpired;
            cmd.Parameters.Add("p_PROJ_ENQUIRY_FIELD", OdbcType.Int, 1).Value = input.projEnquiryField;
            cmd.Parameters.Add("p_PROJ_COUPONMANAGER_PACKAGE", OdbcType.Int, 1).Value = input.projCouponManagerPackage;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                projId = int.Parse(cmd2.ExecuteScalar().ToString());

                if(input.projFbCatalogActive == 1 && input.projFbCatalogExpired != DateTime.MinValue)
                {
                    this.updateProjRandomID(projId);
                }
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractProjectById(int intProjId, int intActive)
    {
        return extractProj(intProjId, intActive);
    }

    public Boolean extractProjectById2(int intProjId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROJ_ID, A.PROJ_NAME" +
                     " FROM TB_PROJECT A" +
                     " WHERE A.PROJ_ID = ?";

            cmd.Parameters.Add("p_PROJ_ID", OdbcType.Int, 9).Value = intProjId;

            if (intActive != 0)
            {
                strSql += " AND A.PROJ_ACTIVE = ?";

                cmd.Parameters.Add("p_PROJ_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                projId = int.Parse(ds.Tables[0].Rows[0]["PROJ_ID"].ToString());
                projName = ds.Tables[0].Rows[0]["PROJ_NAME"].ToString();
            }

            ds.Dispose();
            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractProjectById3(int intProjId, int intActive)
    {
        return extractProj(intProjId, intActive);
    }

    public Boolean extractProj(int intProjId, int intActive)
    {
        Boolean boolFound = false;

        try
        {

            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            List<string> columns = new List<string>()
            {
                "A.PROJ_ID","A.PROJ_NAME","A.PROJ_SERVER","A.PROJ_USERID","A.PROJ_PWD","A.PROJ_DATABASE","A.PROJ_OPTION",
                "A.PROJ_PORT","A.PROJ_PROGMANAGER","A.PROJ_EVENTMANAGER","A.PROJ_PRODDESC","A.PROJ_PRODGALL","A.PROJ_COUPONMANAGER",
                "A.PROJ_MULTIPLECURR","A.PROJ_INVENTORY","A.PROJ_INVENTORYTYPE","A.PROJ_ACTIVE","A.PROJ_DRIVER","A.PROJ_UPLOADPATH",
                "A.PROJ_USERVIEWURL","A.PROJ_TYPE","A.PROJ_FULLPATH","A.PROJ_FULLPATHVALUE","A.PROJ_GROUPMENU","A.PROJ_GROUPPARENTCHILD",
                "A.PROJ_GROUPDESC","A.PROJ_FAQ","A.PROJ_MOBILEVIEW","A.PROJ_GST","A.PROJ_GST_NOTAVAILABLE",
                "A.PROJ_GST_INCLUSIVE","A.PROJ_GST_APPLIED","A.PROJ_FBLOGIN","A.PROJ_WATERMARK","A.PROJ_FRIENDLYURL",
                "A.PROJ_IMGSETTING","A.PROJ_FILESETTING","A.PROJ_CKEDITORIMGSETTING","A.PROJ_CKEDITORFILESETTING","A.PROJ_DEFAULTSETTING",
                "A.PROJ_EVENTTHUMBSETTING","A.PROJ_EVENTGALLSETTING","A.PROJ_PRODTHUMBSETTING","A.PROJ_PRODGALLSETTING","A.PROJ_TRAINING",
                "A.PROJ_SLIDER_ACTIVE","A.PROJ_SLIDER_FULLSCREEN","A.PROJ_SLIDER_DEFAULT","A.PROJ_SLIDER_MOBILE","A.PROJ_DELIVERYMSG",""+
                "A.PROJ_PRODMULTIPLEPROMO","A.PROJ_PGAUTHORIZATION","A.PROJ_UNIVERSALLOGIN","A.PROJ_INDIVIDUALLOGIN","A.PROJ_RIGHTCLICK",
                "A.PROJ_WEBTEQLOGO","A.PROJ_CKEDITORTOPMENU","A.PROJ_SMSSETTING","A.PROJ_TOPMENUROOT","A.PROJ_DELIVERYDATETIME",
                "A.PROJ_PRODADDON", "A.PROJ_FBCATALOG_ACTIVE" , "PROJ_FBCATALOG_EXPIRED", "A.PROJ_ENQUIRY_FIELD", "PROJ_RANDID","PROJ_COUPONMANAGER_PACKAGE",

                "(SELECT B.LIST_NAME FROM TB_LIST B WHERE A.PROJ_DRIVER = B.LIST_VALUE AND B.LIST_GROUP = 'DRIVER TYPE' AND B.LIST_DEFAULT = 1) AS V_DRIVERNAME"
            };


            string strSql = "SELECT " + string.Join(",",columns.ToArray()) + 
                     " FROM TB_PROJECT A" +
                     " WHERE A.PROJ_ID = ?";

            cmd.Parameters.Add("p_PROJ_ID", OdbcType.Int, 9).Value = intProjId;

            if (intActive != 0)
            {
                strSql += " AND A.PROJ_ACTIVE = ?";
                cmd.Parameters.Add("p_PROJ_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                DataRow row = ds.Tables[0].Rows[0];

                projId = int.Parse(row["PROJ_ID"].ToString());
                projName = row["PROJ_NAME"].ToString();
                projServer = row["PROJ_SERVER"].ToString();
                projUserId = row["PROJ_USERID"].ToString();
                projPwd = row["PROJ_PWD"].ToString();
                projDatabase = row["PROJ_DATABASE"].ToString();
                projOption = row["PROJ_OPTION"].ToString();
                projPort = row["PROJ_PORT"].ToString();
                projActive = Convert.ToInt16(row["PROJ_ACTIVE"]);
                projDriver = Convert.ToInt16(row["PROJ_DRIVER"]);
                projDriverName = row["V_DRIVERNAME"].ToString();
                projUploadPath = row["PROJ_UPLOADPATH"].ToString();
                projUserviewUrl = row["PROJ_USERVIEWURL"].ToString();
                projType = int.Parse(row["PROJ_TYPE"].ToString());
                projFullPath = int.Parse(row["PROJ_FULLPATH"].ToString());
                projFullPathValue = row["PROJ_FULLPATHVALUE"].ToString();
                projGroupMenu = Convert.ToInt16(row["PROJ_GROUPMENU"].ToString());
                projGroupParentChild = Convert.ToInt16(row["PROJ_GROUPPARENTCHILD"].ToString());
                projGroupDesc = Convert.ToInt16(row["PROJ_GROUPDESC"].ToString());
                projFAQ = Convert.ToInt16(row["PROJ_FAQ"]);
                projProgManager = int.Parse(row["PROJ_PROGMANAGER"].ToString());
                projEventManager = int.Parse(row["PROJ_EVENTMANAGER"].ToString());
                projProdDesc = int.Parse(row["PROJ_PRODDESC"].ToString());
                projProdGall = int.Parse(row["PROJ_PRODGALL"].ToString());
                projCouponManager = int.Parse(row["PROJ_COUPONMANAGER"].ToString());
                projCouponManagerPackage = int.Parse(row["PROJ_COUPONMANAGER_PACKAGE"].ToString());
                projMultipleCurr = int.Parse(row["PROJ_MULTIPLECURR"].ToString());
                projInventory = int.Parse(row["PROJ_INVENTORY"].ToString());
                projInventoryType = int.Parse(row["PROJ_INVENTORYTYPE"].ToString());
                projMobileView = int.Parse(row["PROJ_MOBILEVIEW"].ToString());
                projFbLogin = int.Parse(row["PROJ_FBLOGIN"].ToString());
                projWatermark = int.Parse(row["PROJ_WATERMARK"].ToString());
                projFriendlyURL = int.Parse(row["PROJ_FRIENDLYURL"].ToString());
                projGst = int.Parse(row["PROJ_GST"].ToString());
                projGstNotAvailable = int.Parse(row["PROJ_GST_NOTAVAILABLE"].ToString());
                projGstInclusive = int.Parse(row["PROJ_GST_INCLUSIVE"].ToString());
                projGstApplied = int.Parse(row["PROJ_GST_APPLIED"].ToString());
                projImgSetting = int.Parse(row["PROJ_IMGSETTING"].ToString());
                projFileSetting = int.Parse(row["PROJ_FILESETTING"].ToString());
                projCkeditorFileSetting = int.Parse(row["PROJ_CKEDITORFILESETTING"].ToString());
                projCkeditoImgSetting = int.Parse(row["PROJ_CKEDITORIMGSETTING"].ToString());
                projDefaultSetting = int.Parse(row["PROJ_DEFAULTSETTING"].ToString());
                projEventThumbSetting = int.Parse(row["PROJ_EVENTTHUMBSETTING"].ToString());
                projEventGallSetting = int.Parse(row["PROJ_EVENTGALLSETTING"].ToString());
                projProdThumbSetting = int.Parse(row["PROJ_PRODTHUMBSETTING"].ToString());
                projProdGallSetting = int.Parse(row["PROJ_PRODGALLSETTING"].ToString());
                projTraining = int.Parse(row["PROJ_TRAINING"].ToString());
                projSliderActive = int.Parse(row["PROJ_SLIDER_ACTIVE"].ToString());
                projSliderDefault = int.Parse(row["PROJ_SLIDER_DEFAULT"].ToString());
                projSliderFullScreen = int.Parse(row["PROJ_SLIDER_FULLSCREEN"].ToString());
                projSliderMobile = int.Parse(row["PROJ_SLIDER_MOBILE"].ToString());
                projDeliveryMsg = int.Parse(row["PROJ_DELIVERYMSG"].ToString());
                projProdPromo = int.Parse(row["PROJ_PRODMULTIPLEPROMO"].ToString()); // add by sh.chong 18 Feb 2016
                projPgAuthorization = int.Parse(row["PROJ_PGAUTHORIZATION"].ToString());
                projUniversalLogin = int.Parse(row["PROJ_UNIVERSALLOGIN"].ToString());
                projIndividualLogin = int.Parse(row["PROJ_INDIVIDUALLOGIN"].ToString());
                projSMSSetting = int.Parse(row["PROJ_SMSSETTING"].ToString());

                projRightClick = int.Parse(row["PROJ_RIGHTCLICK"].ToString());
                projWebteqLogo = int.Parse(row["PROJ_WEBTEQLOGO"].ToString());
                projCkeditorTopmenu = int.Parse(row["PROJ_CKEDITORTOPMENU"].ToString());
                projTopmenuRoot = int.Parse(row["PROJ_TOPMENUROOT"].ToString());
                projDeliveryDatetime = int.Parse(row["PROJ_DELIVERYDATETIME"].ToString());
                projProdAddon = Convert.ToInt16(row["PROJ_PRODADDON"]);

                projFbCatalogActive = int.Parse(row["PROJ_FBCATALOG_ACTIVE"].ToString());
                projFbCatalogExpired = row["PROJ_FBCATALOG_EXPIRED"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(row["PROJ_FBCATALOG_EXPIRED"]);
                projRandId = row["PROJ_RANDID"].ToString();

                projEnquiryField = int.Parse(row["PROJ_ENQUIRY_FIELD"].ToString());

            }

            ds.Dispose();
            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name + "  #  " + System.Reflection.MethodBase.GetCurrentMethod().Name + "  #  " + ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    public DataSet getProjectList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROJ_ID, A.PROJ_NAME, A.PROJ_SERVER, A.PROJ_USERID, A.PROJ_PWD, A.PROJ_DATABASE, A.PROJ_OPTION," +
                     " A.PROJ_PORT, A.PROJ_ACTIVE, A.PROJ_DRIVER, A.V_PROJDRIVERNAME, A.PROJ_UPLOADPATH, A.PROJ_USERVIEWURL," +
                     " A.PROJ_TYPE, A.V_TYPENAME, A.PROJ_MOBILEVIEW," +
                     " A.PROJ_CREATEDBY, A.PROJ_CREATION, A.PROJ_UPDATEDBY, A.PROJ_LASTUPDATE, A.PROJ_FULLPATH, A.PROJ_FULLPATHVALUE, A.PROJ_GROUPPARENTCHILD," +
                     " CONCAT(PROJ_NAME, ' (wt', PROJ_DATABASE, ') ') AS V_PROJNAME" +
                     " FROM VW_PROJECT A";

            if (intActive != 0)
            {
                strSql += " WHERE A.PROJ_ACTIVE = ?";
                cmd.Parameters.Add("p_PROJ_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean isNameExist(string strName, int intId)
    {
        Boolean boolExist = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_PROJECT WHERE PROJ_NAME = ?";

            cmd.Parameters.Add("p_PROJ_NAME", OdbcType.VarChar, 250).Value = strName;

            if (intId > 0)
            {
                strSql += " AND PROJ_ID != ?";
                cmd.Parameters.Add("p_PROJ_ID", OdbcType.BigInt, 9).Value = intId;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolExist = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public Boolean isExactSameSetProject(clsProject input)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PROJECT" +
                     " WHERE PROJ_ID = ?" +
                     " AND BINARY PROJ_NAME = ?" +
                     " AND PROJ_DRIVER = ?" +
                     " AND BINARY PROJ_SERVER = ?" +
                     " AND BINARY PROJ_USERID = ?" +
                     " AND BINARY PROJ_PWD = ?" +
                     " AND BINARY PROJ_DATABASE = ?" +
                     " AND BINARY PROJ_OPTION = ?" +
                     " AND BINARY PROJ_PORT = ?" +
                     " AND BINARY PROJ_UPLOADPATH = ?" +
                     " AND BINARY PROJ_USERVIEWURL = ?" +
                     " AND PROJ_TYPE = ?" +
                     " AND PROJ_PROGMANAGER = ?" +
                     " AND PROJ_EVENTMANAGER = ?" +
                     " AND PROJ_PRODDESC = ?" +
                     " AND PROJ_PRODGALL = ?" +
                     " AND PROJ_ACTIVE = ?" +
                     " AND PROJ_FULLPATH = ?" +
                     " AND PROJ_FULLPATHVALUE = ?" +
                     " AND PROJ_GROUPMENU = ?" +
                     " AND PROJ_GROUPPARENTCHILD = ?" +
                     " AND PROJ_GROUPDESC = ?" +
                     " AND PROJ_FAQ = ?" +
                     " AND PROJ_COUPONMANAGER = ?" +
                     " AND PROJ_MULTIPLECURR = ?" +
                     " AND PROJ_INVENTORY = ?" +
                     " AND PROJ_INVENTORYTYPE = ?" +
                     " AND PROJ_MOBILEVIEW = ?" +
                     " AND PROJ_FBLOGIN = ?" +
                     " AND PROJ_WATERMARK = ?" +
                     " AND PROJ_FRIENDLYURL = ?" +
                     " AND PROJ_GST = ?" +
                     " AND PROJ_GST_NOTAVAILABLE = ?" +
                     " AND PROJ_GST_INCLUSIVE = ?" +
                     " AND PROJ_GST_APPLIED = ?" +
                     " AND PROJ_IMGSETTING = ?" +
                     " AND PROJ_FILESETTING = ?" +
                     " AND PROJ_CKEDITORIMGSETTING = ?" +
                     " AND PROJ_CKEDITORFILESETTING = ?" +
                     " AND PROJ_DEFAULTSETTING = ?" +
                     " AND PROJ_EVENTTHUMBSETTING = ?" +
                     " AND PROJ_EVENTGALLSETTING = ?" +
                     " AND PROJ_PRODTHUMBSETTING = ?" +
                     " AND PROJ_PRODGALLSETTING = ?" +
                     " AND PROJ_TRAINING = ?" +
                     " AND PROJ_SLIDER_ACTIVE = ?" +
                     " AND PROJ_SLIDER_DEFAULT = ?" +
                     " AND PROJ_SLIDER_FULLSCREEN = ?" +
                     " AND PROJ_SLIDER_MOBILE = ?" +
                     " AND PROJ_DELIVERYMSG = ?" +
                     " AND PROJ_PRODMULTIPLEPROMO = ?" +
                     " AND PROJ_PGAUTHORIZATION = ?" +
                     " AND PROJ_UNIVERSALLOGIN = ?" +
                     " AND PROJ_INDIVIDUALLOGIN = ?" +
                     " AND PROJ_RIGHTCLICK = ?" +
                     " AND PROJ_WEBTEQLOGO = ?" +
                     " AND PROJ_CKEDITORTOPMENU = ?" +
                     " AND PROJ_SMSSETTING = ?" +
                     " AND PROJ_TOPMENUROOT = ?" +
                     " AND PROJ_DELIVERYDATETIME = ?" +
                     " AND PROJ_PRODADDON = ?" +
                     " AND PROJ_FBCATALOG_ACTIVE = ?" +
                     " AND PROJ_FBCATALOG_EXPIRED = ?" + 
                     " AND PROJ_ENQUIRY_FIELD = ?" +
                     " AND PROJ_COUPONMANAGER_PACKAGE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROJ_ID", OdbcType.BigInt, 9).Value = input.projId;
            cmd.Parameters.Add("p_PROJ_NAME", OdbcType.VarChar, 250).Value = input.projName;
            cmd.Parameters.Add("p_PROJ_DRIVER", OdbcType.Int, 1).Value = input.projDriver;
            cmd.Parameters.Add("p_PROJ_SERVER", OdbcType.VarChar, 50).Value = input.projServer;
            cmd.Parameters.Add("p_PROJ_USERID", OdbcType.VarChar, 50).Value = input.projUserId;
            cmd.Parameters.Add("p_PROJ_PWD", OdbcType.VarChar, 50).Value = input.projPwd;
            cmd.Parameters.Add("p_PROJ_DATABASE", OdbcType.VarChar, 50).Value = input.projDatabase;
            cmd.Parameters.Add("p_PROJ_OPTION", OdbcType.VarChar, 10).Value = input.projOption;
            cmd.Parameters.Add("p_PROJ_PORT", OdbcType.VarChar, 10).Value = input.projPort;
            cmd.Parameters.Add("p_PROJ_UPLOADPATH", OdbcType.VarChar, 1000).Value = input.projUploadPath;
            cmd.Parameters.Add("p_PROJ_USERVIEWURL", OdbcType.VarChar, 1000).Value = input.projUserviewUrl;
            cmd.Parameters.Add("p_PROJ_TYPE", OdbcType.BigInt, 9).Value = input.projType;
            cmd.Parameters.Add("p_PROJ_PROGMANAGER", OdbcType.Int, 1).Value = input.projProgManager;
            cmd.Parameters.Add("p_PROJ_EVENTMANAGER", OdbcType.Int, 1).Value = input.projEventManager;
            cmd.Parameters.Add("p_PROJ_PRODDESC", OdbcType.Int, 1).Value = input.projProdDesc;
            cmd.Parameters.Add("P_PROJ_PRODGALL", OdbcType.Int, 1).Value = input.projProdGall;
            cmd.Parameters.Add("p_PROJ_ACTIVE", OdbcType.Int, 1).Value = input.projActive;
            cmd.Parameters.Add("p_PROJ_FULLPATH", OdbcType.Int, 1).Value = input.projFullPath;
            cmd.Parameters.Add("p_PROJ_FULLPATHVALUE", OdbcType.VarChar, 1000).Value = input.projFullPath;
            cmd.Parameters.Add("p_PROJ_GROUPMENU", OdbcType.Int, 1).Value = input.projGroupMenu;
            cmd.Parameters.Add("p_PROJ_GROUPPARENTCHILD", OdbcType.Int, 1).Value = input.projGroupParentChild;
            cmd.Parameters.Add("P_PROJ_GROUPDESC", OdbcType.Int, 1).Value = input.projGroupDesc;
            cmd.Parameters.Add("p_PROJ_FAQ", OdbcType.Int, 1).Value = input.projFAQ;
            cmd.Parameters.Add("P_PROJ_COUPONMANAGER", OdbcType.Int, 1).Value = input.projCouponManager;
            cmd.Parameters.Add("P_PROJ_MULTIPLECURR", OdbcType.Int, 1).Value = input.projMultipleCurr;
            cmd.Parameters.Add("P_PROJ_INVENTORY", OdbcType.Int, 1).Value = input.projInventory;
            cmd.Parameters.Add("P_PROJ_INVENTORYTYPE", OdbcType.Int, 1).Value = input.projInventoryType;
            cmd.Parameters.Add("P_PROJ_MOBILEVIEW", OdbcType.Int, 1).Value = input.projMobileView;
            cmd.Parameters.Add("p_PROJ_FBLOGIN", OdbcType.Int, 1).Value = input.projFbLogin;
            cmd.Parameters.Add("p_PROJ_WATERMARK", OdbcType.Int, 1).Value = input.projWatermark;
            cmd.Parameters.Add("p_PROJ_FRIENDLYURL", OdbcType.Int, 1).Value = input.projFriendlyURL;
            cmd.Parameters.Add("p_PROJ_GST", OdbcType.Int, 1).Value = input.projGst;
            cmd.Parameters.Add("p_PROJ_GST_NOTAVAILABLE", OdbcType.Int, 1).Value = input.projGstNotAvailable;
            cmd.Parameters.Add("p_PROJ_GST_INCLUSIVE", OdbcType.Int, 1).Value = input.projGstInclusive;
            cmd.Parameters.Add("p_PROJ_GST_APPLIED", OdbcType.Int, 1).Value = input.projGstApplied;
            cmd.Parameters.Add("p_PROJ_IMGSETTING", OdbcType.Int, 9).Value = input.projImgSetting;
            cmd.Parameters.Add("p_PROJ_FILESETTING", OdbcType.Int, 9).Value = input.projFileSetting;
            cmd.Parameters.Add("p_PROJ_CKEDITORIMGSETTING", OdbcType.Int, 9).Value = input.projCkeditoImgSetting;
            cmd.Parameters.Add("p_PROJ_CKEDITORFILESETTING", OdbcType.Int, 9).Value = input.projCkeditorFileSetting;
            cmd.Parameters.Add("p_PROJ_DEFAULTSETTING", OdbcType.Int, 9).Value = input.projDefaultSetting;
            cmd.Parameters.Add("p_PROJ_EVENTTHUMBSETTING", OdbcType.Int, 9).Value = input.projEventThumbSetting;
            cmd.Parameters.Add("p_PROJ_EVENTGALLSETTING", OdbcType.Int, 9).Value = input.projEventGallSetting;
            cmd.Parameters.Add("p_PROJ_PRODTHUMBSETTING", OdbcType.Int, 9).Value = input.projProdThumbSetting;
            cmd.Parameters.Add("p_PROJ_PRODGALLSETTING", OdbcType.Int, 9).Value = input.projProdGallSetting;
            cmd.Parameters.Add("p_PROJ_TRAINING", OdbcType.Int, 1).Value = input.projTraining;
            cmd.Parameters.Add("p_PROJ_SLIDER_ACTIVE", OdbcType.Int, 1).Value = input.projSliderActive;
            cmd.Parameters.Add("p_PROJ_SLIDER_DEFAULT", OdbcType.Int, 1).Value = input.projSliderDefault;
            cmd.Parameters.Add("p_PROJ_SLIDER_FULLSCREEN", OdbcType.Int, 1).Value = input.projSliderFullScreen;
            cmd.Parameters.Add("p_PROJ_SLIDER_MOBILE", OdbcType.Int, 1).Value = input.projSliderMobile;
            cmd.Parameters.Add("p_PROJ_DELIVERYMSG", OdbcType.Int, 1).Value = input.projDeliveryMsg;
            cmd.Parameters.Add("p_PROJ_PRODMULTIPLEPROMO", OdbcType.Int, 1).Value = input.projProdPromo;
            cmd.Parameters.Add("p_PROJ_PGAUTHORIZATION", OdbcType.Int, 1).Value = input.projPgAuthorization;
            cmd.Parameters.Add("p_PROJ_UNIVERSALLOGIN", OdbcType.Int, 1).Value = input.projUniversalLogin;
            cmd.Parameters.Add("p_PROJ_INDIVIDUALLOGIN", OdbcType.Int, 1).Value = input.projIndividualLogin;

            cmd.Parameters.Add("p_PROJ_RIGHTCLICK", OdbcType.Int, 1).Value = input.projRightClick;
            cmd.Parameters.Add("p_PROJ_WEBTEQLOGO", OdbcType.Int, 1).Value = input.projWebteqLogo;
            cmd.Parameters.Add("p_PROJ_CKEDITORTOPMENU", OdbcType.Int, 1).Value = input.projCkeditorTopmenu;
            cmd.Parameters.Add("p_PROJ_SMSSETTING", OdbcType.Int, 1).Value = input.projSMSSetting;
            cmd.Parameters.Add("p_PROJ_TOPMENUROOT", OdbcType.Int, 9).Value = input.projTopmenuRoot;
            cmd.Parameters.Add("p_PROJ_DELIVERYDATETIME", OdbcType.Int, 1).Value = input.projDeliveryDatetime;
            cmd.Parameters.Add("p_PROJ_PRODADDON", OdbcType.Int, 1).Value = input.projProdAddon;
            cmd.Parameters.Add("p_PROJ_FBCATALOG_ACTIVE", OdbcType.Int, 1).Value = input.projFbCatalogActive;
            cmd.Parameters.Add("p_PROJ_FBCATALOG_EXPIRED", OdbcType.DateTime).Value = input.projFbCatalogExpired;
            cmd.Parameters.Add("p_PROJ_ENQUIRY_FIELD", OdbcType.Int, 1).Value = input.projEnquiryField;
            cmd.Parameters.Add("p_PROJ_COUPONMANAGER_PACKAGE", OdbcType.Int, 1).Value = input.projCouponManagerPackage;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int updateProjectById(clsProject input)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PROJECT SET" +
                     " PROJ_NAME = ?," +
                     " PROJ_DRIVER = ?," +
                     " PROJ_SERVER = ?," +
                     " PROJ_USERID = ?," +
                     " PROJ_PWD = ?," +
                     " PROJ_DATABASE = ?," +
                     " PROJ_OPTION = ?," +
                     " PROJ_PORT = ?," +
                     " PROJ_UPLOADPATH = ?," +
                     " PROJ_USERVIEWURL = ?," +
                     " PROJ_TYPE = ?," +
                     " PROJ_PROGMANAGER = ?," +
                     " PROJ_EVENTMANAGER = ?," +
                     " PROJ_PRODDESC = ?," +
                     " PROJ_PRODGALL = ?," +
                     " PROJ_ACTIVE = ?," +
                     " PROJ_FULLPATH = ?," +
                     " PROJ_FULLPATHVALUE = ?," +
                     " PROJ_GROUPMENU = ?," +
                     " PROJ_GROUPPARENTCHILD = ?," +
                     " PROJ_GROUPDESC = ?," +
                     " PROJ_FAQ = ?," +
                     " PROJ_COUPONMANAGER = ?," +
                     " PROJ_MULTIPLECURR = ?, " +
                     " PROJ_INVENTORY = ?, " +
                     " PROJ_INVENTORYTYPE = ?, " +
                     " PROJ_MOBILEVIEW = ?," +
                     " PROJ_FBLOGIN = ?," +
                     " PROJ_WATERMARK = ?," +
                     " PROJ_FRIENDLYURL = ?," +
                     " PROJ_GST = ?," +
                     " PROJ_GST_NOTAVAILABLE = ?," +
                     " PROJ_GST_INCLUSIVE = ?," +
                     " PROJ_GST_APPLIED = ?," +
                     " PROJ_IMGSETTING = ?," +
                     " PROJ_FILESETTING = ?," +
                     " PROJ_CKEDITORIMGSETTING = ?," +
                     " PROJ_CKEDITORFILESETTING = ?," +
                     " PROJ_DEFAULTSETTING = ?," +
                     " PROJ_EVENTTHUMBSETTING = ?," +
                     " PROJ_EVENTGALLSETTING = ?," +
                     " PROJ_PRODTHUMBSETTING = ?," +
                     " PROJ_PRODGALLSETTING = ?," +
                     " PROJ_TRAINING = ?," +
                     " PROJ_SLIDER_ACTIVE = ?," +
                     " PROJ_SLIDER_DEFAULT = ?," +
                     " PROJ_SLIDER_FULLSCREEN = ?," +
                     " PROJ_SLIDER_MOBILE = ?," +
                     " PROJ_DELIVERYMSG = ?," +
                     " PROJ_PRODMULTIPLEPROMO = ?," +
                     " PROJ_PGAUTHORIZATION = ?," +
                     " PROJ_UNIVERSALLOGIN = ?," +
                     " PROJ_INDIVIDUALLOGIN = ?," +
                     " PROJ_RIGHTCLICK = ?," +
                     " PROJ_WEBTEQLOGO = ?," +
                     " PROJ_CKEDITORTOPMENU = ?," +
                     " PROJ_SMSSETTING = ?," +
                     " PROJ_TOPMENUROOT = ?," +
                     " PROJ_DELIVERYDATETIME = ?," +
                     " PROJ_PRODADDON = ?," +
                     " PROJ_FBCATALOG_ACTIVE = ?," +
                     " PROJ_FBCATALOG_EXPIRED = ?," +
                     " PROJ_ENQUIRY_FIELD = ?," +
                     " PROJ_COUPONMANAGER_PACKAGE = ?" +
                     " WHERE PROJ_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROJ_NAME", OdbcType.VarChar, 250).Value = input.projName;
            cmd.Parameters.Add("p_PROJ_DRIVER", OdbcType.Int, 1).Value = input.projDriver;
            cmd.Parameters.Add("p_PROJ_SERVER", OdbcType.VarChar, 50).Value = input.projServer;
            cmd.Parameters.Add("p_PROJ_USERID", OdbcType.VarChar, 50).Value = input.projUserId;
            cmd.Parameters.Add("p_PROJ_PWD", OdbcType.VarChar, 50).Value = input.projPwd;
            cmd.Parameters.Add("p_PROJ_DATABASE", OdbcType.VarChar, 50).Value = input.projDatabase;
            cmd.Parameters.Add("p_PROJ_OPTION", OdbcType.VarChar, 10).Value = input.projOption;
            cmd.Parameters.Add("p_PROJ_PORT", OdbcType.VarChar, 10).Value = input.projPort;
            cmd.Parameters.Add("p_PROJ_UPLOADPATH", OdbcType.VarChar, 1000).Value = input.projUploadPath;
            cmd.Parameters.Add("p_PROJ_USERVIEWURL", OdbcType.VarChar, 1000).Value = input.projUserviewUrl;
            cmd.Parameters.Add("p_PROJ_TYPE", OdbcType.BigInt, 9).Value = input.projType;
            cmd.Parameters.Add("p_PROJ_PROGMANAGER", OdbcType.Int, 1).Value = input.projProgManager;
            cmd.Parameters.Add("p_PROJ_EVENTMANAGER", OdbcType.Int, 1).Value = input.projEventManager;
            cmd.Parameters.Add("p_PROJ_PRODDESC", OdbcType.Int, 1).Value = input.projProdDesc;
            cmd.Parameters.Add("p_PROJ_PRODGALL", OdbcType.Int, 1).Value = input.projProdGall;
            cmd.Parameters.Add("p_PROJ_ACTIVE", OdbcType.Int, 1).Value = input.projActive;
            cmd.Parameters.Add("p_PROJ_FULLPATH", OdbcType.Int, 1).Value = input.projFullPath;
            cmd.Parameters.Add("p_PROJ_FULLPATHVALUE", OdbcType.VarChar, 250).Value = input.projFullPath;
            cmd.Parameters.Add("p_PROJ_GROUPMENU", OdbcType.Int, 1).Value = input.projGroupMenu;
            cmd.Parameters.Add("p_PROJ_GROUPPARENTCHILD", OdbcType.Int, 1).Value = input.projGroupParentChild;
            cmd.Parameters.Add("p_PROJ_GROUPDESC", OdbcType.Int, 1).Value = input.projGroupDesc;
            cmd.Parameters.Add("p_PROJ_FAQ", OdbcType.Int, 1).Value = input.projFAQ;
            cmd.Parameters.Add("p_PROJ_COUPONMANAGER", OdbcType.Int, 1).Value = input.projCouponManager;
            cmd.Parameters.Add("p_PROJ_MULTIPLECURR", OdbcType.Int, 1).Value = input.projMultipleCurr;
            cmd.Parameters.Add("p_PROJ_INVENTORY", OdbcType.Int, 1).Value = input.projInventory;
            cmd.Parameters.Add("p_PROJ_INVENTORYTYPE", OdbcType.Int, 1).Value = input.projInventoryType;
            cmd.Parameters.Add("p_PROJ_MOBILEVIEW", OdbcType.Int, 1).Value = input.projMobileView;
            cmd.Parameters.Add("p_PROJ_FBLOGIN", OdbcType.Int, 1).Value = input.projFbLogin;
            cmd.Parameters.Add("p_PROJ_WATERMARK", OdbcType.Int, 1).Value = input.projWatermark;
            cmd.Parameters.Add("p_PROJ_FRIENDLYURL", OdbcType.Int, 1).Value = input.projFriendlyURL;
            cmd.Parameters.Add("p_PROJ_GST", OdbcType.Int, 1).Value = input.projGst;
            cmd.Parameters.Add("p_PROJ_GST_NOTAVAILABLE", OdbcType.Int, 1).Value = input.projGstNotAvailable;
            cmd.Parameters.Add("p_PROJ_GST_INCLUSIVE", OdbcType.Int, 1).Value = input.projGstInclusive;
            cmd.Parameters.Add("p_PROJ_GST_APPLIED", OdbcType.Int, 1).Value = input.projGstApplied;
            cmd.Parameters.Add("p_PROJ_IMGSETTING", OdbcType.Int, 9).Value = input.projImgSetting;
            cmd.Parameters.Add("p_PROJ_FILESETTING", OdbcType.Int, 9).Value = input.projFileSetting;
            cmd.Parameters.Add("p_PROJ_CKEDITORIMGSETTING", OdbcType.Int, 9).Value = input.projCkeditoImgSetting;
            cmd.Parameters.Add("p_PROJ_CKEDITORFILESETTING", OdbcType.Int, 9).Value = input.projCkeditorFileSetting;
            cmd.Parameters.Add("p_PROJ_DEFAULTSETTING", OdbcType.Int, 9).Value = input.projDefaultSetting;
            cmd.Parameters.Add("p_PROJ_EVENTTHUMBSETTING", OdbcType.Int, 9).Value = input.projEventThumbSetting;
            cmd.Parameters.Add("p_PROJ_EVENTGALLSETTING", OdbcType.Int, 9).Value = input.projEventGallSetting;
            cmd.Parameters.Add("p_PROJ_PRODTHUMBSETTING", OdbcType.Int, 9).Value = input.projProdThumbSetting;
            cmd.Parameters.Add("p_PROJ_PRODGALLSETTING", OdbcType.Int, 9).Value = input.projProdGallSetting;
            cmd.Parameters.Add("p_PROJ_TRAINING", OdbcType.Int, 1).Value = input.projTraining;
            cmd.Parameters.Add("p_PROJ_SLIDER_ACTIVE", OdbcType.Int, 1).Value = input.projSliderActive;
            cmd.Parameters.Add("p_PROJ_SLIDER_DEFAULT", OdbcType.Int, 1).Value = input.projSliderDefault;
            cmd.Parameters.Add("p_PROJ_SLIDER_FULLSCREEN", OdbcType.Int, 1).Value = input.projSliderFullScreen;
            cmd.Parameters.Add("p_PROJ_SLIDER_MOBILE", OdbcType.Int, 1).Value = input.projSliderMobile;
            cmd.Parameters.Add("p_PROJ_DELIVERYMSG", OdbcType.Int, 1).Value = input.projDeliveryMsg;
            cmd.Parameters.Add("p_PROJ_PRODMULTIPLEPROMO", OdbcType.Int, 1).Value = input.projProdPromo;
            cmd.Parameters.Add("p_PROJ_PGAUTHORIZATION", OdbcType.Int, 1).Value = input.projPgAuthorization;
            cmd.Parameters.Add("p_PROJ_UNIVERSALLOGIN", OdbcType.Int, 1).Value = input.projUniversalLogin;
            cmd.Parameters.Add("p_PROJ_INDIVIDUALLOGIN", OdbcType.Int, 1).Value = input.projIndividualLogin;
            cmd.Parameters.Add("p_PROJ_RIGHTCLICK", OdbcType.Int, 1).Value = input.projRightClick;
            cmd.Parameters.Add("p_PROJ_WEBTEQLOGO", OdbcType.Int, 1).Value = input.projWebteqLogo;
            cmd.Parameters.Add("p_PROJ_CKEDITORTOPMENU", OdbcType.Int, 1).Value = input.projCkeditorTopmenu;
            cmd.Parameters.Add("p_PROJ_SMSSETTING", OdbcType.Int, 1).Value = input.projSMSSetting;
            cmd.Parameters.Add("p_PROJ_TOPMENUROOT", OdbcType.Int, 9).Value = input.projTopmenuRoot;
            cmd.Parameters.Add("p_PROJ_DELIVERYDATETIME", OdbcType.Int, 1).Value = input.projDeliveryDatetime;
            cmd.Parameters.Add("p_PROJ_PRODADDON", OdbcType.Int, 1).Value = input.projProdAddon;
            cmd.Parameters.Add("p_PROJ_FBCATALOG_ACTIVE", OdbcType.Int, 1).Value = input.projFbCatalogActive;
            cmd.Parameters.Add("p_PROJ_FBCATALOG_EXPIRED", OdbcType.DateTime).Value = input.projFbCatalogExpired;
            cmd.Parameters.Add("p_PROJ_ENQUIRY_FIELD", OdbcType.Int, 1).Value = input.projEnquiryField;
            cmd.Parameters.Add("p_PROJ_COUPONMANAGER_PACKAGE", OdbcType.Int, 1).Value = input.projCouponManagerPackage;

            cmd.Parameters.Add("p_PROJ_ID", OdbcType.BigInt, 9).Value = input.projId;

            

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                if (input.projFbCatalogActive == 1 && input.projFbCatalogExpired != DateTime.MinValue)
                {
                    this.updateProjRandomID(input.projId);
                }
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteProjectById(int intProjId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PROJECT WHERE PROJ_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROJ_ID", OdbcType.Int, 9).Value = intProjId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                projId = intProjId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int updateProjRandomID(int intProjId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PROJECT SET PROJ_RANDID = Func_ProjRandomID(PROJ_ID) WHERE PROJ_ID = ?;";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROJ_ID", OdbcType.Int, 9).Value = intProjId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name + "  #  " + System.Reflection.MethodBase.GetCurrentMethod().Name + "  #  " + ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion


    #region "Type"
    public DataSet getTypeList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.TYPE_ID, A.TYPE_NAME, A.TYPE_ORDER,A.TYPE_PAGELIMIT, A.TYPE_ACTIVE" +
                     " FROM TB_PROJECTTYPE A";

            if (intActive != 0)
            {
                strSql += " WHERE A.TYPE_ACTIVE = ?";
                cmd.Parameters.Add("p_TYPE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    #endregion


    #region "Config"
    public int generateOrderNo(string strGroup, int intProjId)
    {
        int orderNo = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT CONF_ORDER FROM TB_PROJECTCONFIG WHERE CONF_GROUP = ? AND PROJ_ID = ?" +
                     " ORDER BY CONF_ORDER DESC LIMIT 1";

            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_PROJ_ID", OdbcType.BigInt, 9).Value = intProjId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            orderNo = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return orderNo + 1;
    }

    public int addConfig(int intProjId, string strName, string strValue, string strGroup, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            int intOrder = generateOrderNo(strGroup, intProjId);

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PROJECTCONFIG (" +
                     "CONF_NAME," +
                     "CONF_VALUE," +
                     "CONF_GROUP," +
                     "CONF_ORDER," +
                     "CONF_CREATION," +
                     "CONF_CREATEDBY," +
                     "PROJ_ID" +
                     ") VALUES " +
                     "(?,?,?,?,SYSDATE(),?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_CONF_VALUE", OdbcType.VarChar, 250).Value = strValue;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_ORDER", OdbcType.Int, 9).Value = intOrder;
            cmd.Parameters.Add("p_CONF_CREATEDBY", OdbcType.BigInt, 9).Value = intCreatedBy;
            cmd.Parameters.Add("p_PROJ_ID", OdbcType.BigInt, 9).Value = intProjId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";

                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                configId = int.Parse(cmd2.ExecuteScalar().ToString());
                projId = intProjId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getConfigList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CONF_ID, A.CONF_NAME, A.CONF_VALUE, A.CONF_LONGVALUE, A.CONF_GROUP, A.CONF_ORDER, A.CONF_OPERATOR, A.CONF_UNIT, A.CONF_TAG, A.CONF_ACTIVE," +
                     " A.CONF_CREATION, A.CONF_CREATEDBY, A.CONF_LASTUPDATE, A.CONF_UPDATEDBY, A.PROJ_ID" +
                     " FROM TB_PROJECTCONFIG A";

            if (intActive != 0)
            {
                strSql += " WHERE A.CONF_ACTIVE = ?";
                cmd.Parameters.Add("p_CONF_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getConfigByProject(int intProjId, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CONF_ID, A.CONF_NAME, A.CONF_VALUE, A.CONF_LONGVALUE, A.CONF_GROUP, A.CONF_ORDER, A.CONF_OPERATOR, A.CONF_UNIT, A.CONF_TAG, A.CONF_ACTIVE," +
                     " A.CONF_CREATION, A.CONF_CREATEDBY, A.CONF_LASTUPDATE, A.CONF_UPDATEDBY, A.PROJ_ID" +
                     " FROM TB_PROJECTCONFIG A" +
                     " WHERE A.PROJ_ID = ?";

            cmd.Parameters.Add("p_PROJ_ID", OdbcType.BigInt, 9).Value = intProjId;

            if (intActive != 0)
            {
                strSql += " AND A.CONF_ACTIVE = ?";
                cmd.Parameters.Add("p_CONF_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean isConfigExist(string strName, string strGroup, int intProjId)
    {
        Boolean boolValid = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_PROJECTCONFIG" +
                     " WHERE CONF_NAME = ?" +
                     " AND CONF_GROUP = ?" +
                     " AND PROJ_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_PROJ_ID", OdbcType.BigInt, 9).Value = intProjId;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1) { boolValid = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolValid;
    }

    public Boolean isExactSameSetData(string strName, string strValue, string strGroup, int intProjId)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PROJECTCONFIG" +
                     " WHERE CONF_NAME = ?" +
                     " AND BINARY CONF_VALUE = ?" +
                     " AND BINARY CONF_GROUP = ?" +
                     " AND PROJ_ID = ?";

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_CONF_VALUE", OdbcType.VarChar, 250).Value = strValue;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_PROJ_ID", OdbcType.BigInt, 9).Value = intProjId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int updateConfig(string strGroup, string strName, string strValue, int intUpdatedBy, int intProjId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PROJECTCONFIG SET" +
                     " CONF_VALUE = ?," +
                     " CONF_LASTUPDATE = SYSDATE()," +
                     " CONF_UPDATEDBY = ?" +
                     " WHERE CONF_GROUP = ?" +
                     " AND CONF_NAME = ?" +
                     " AND PROJ_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CONF_VALUE", OdbcType.VarChar, 250).Value = strValue;
            cmd.Parameters.Add("p_CONF_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_PROJ_ID", OdbcType.BigInt, 9).Value = intProjId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                projId = intProjId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion


    #region "Language"
    public int addLanguage(int intProjId, string strLang)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PROJECTLANG (" +
                    "PROJ_ID, " +
                    "LANG_LANG " +
                    ") VALUES " +
                   "(?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROJ_ID", OdbcType.BigInt, 9).Value = intProjId;
            cmd.Parameters.Add("p_LANG_LANG", OdbcType.VarChar, 10).Value = strLang;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                projId = intProjId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteLanguage(int intProjId, string strLang)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PROJECTLANG" +
                     " WHERE PROJ_ID = ?" +
                     " AND LANG_LANG = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROJ_ID", OdbcType.BigInt, 9).Value = intProjId;
            cmd.Parameters.Add("p_LANG_LANG", OdbcType.VarChar, 10).Value = strLang;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                projId = intProjId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteLanguage(int intProjId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PROJECTLANG" +
                     " WHERE PROJ_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROJ_ID", OdbcType.BigInt, 9).Value = intProjId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
                projId = intProjId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getLanguageById(int intProjId)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROJ_ID, A.LANG_LANG," +
                     " (SELECT B.LIST_NAME FROM TB_LIST B WHERE A.LANG_LANG = B.LIST_VALUE AND B.LIST_GROUP = 'LANGUAGE' AND B.LIST_DEFAULT = 1) AS V_LANGNAME" +
                     " FROM TB_PROJECTLANG A" +
                     " WHERE A.PROJ_ID = ?";

            cmd.Parameters.Add("p_PROJ_ID", OdbcType.BigInt, 9).Value = intProjId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    #endregion

    #region "Payment Gateway"
    public DataSet getPaymentGatewayList()
    {
        return getPaymentGatewayList(0, 0);
    }
    public DataSet getPaymentGatewayList(int intProjId)
    {
        return getPaymentGatewayList(intProjId, 0);
    }
    public DataSet getPaymentGatewayList(int intProjId, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT PROJ_ID,PG_NAME,PG_ACTIVE FROM TB_PROJECTPAYMENTGATEWAY WHERE 1 = 1";
            if (intProjId > 0)
            {
                strSql += " AND PROJ_ID = ?";
                cmd.Parameters.Add("p_PROJ_ID", OdbcType.Int, 9).Value = intProjId;
            }

            if (intActive > 0)
            {
                strSql += " AND PG_ACTIVE = ?";
                cmd.Parameters.Add("p_PG_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }
            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public int addPaymentGateway(int intProjId, string strName, int intActive, int intCreatedBy)
    {
        int intRecordAffected = 0;
        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PROJECTPAYMENTGATEWAY (" +
                     "PROJ_ID," +
                     "PG_NAME," +
                     "PG_ACTIVE," +
                     "PG_CREATION," +
                     "PG_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROJ_ID", OdbcType.Int, 9).Value = intProjId;
            cmd.Parameters.Add("p_PG_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_PG_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_BT_CREATEDBY", OdbcType.Int, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }
        return intRecordAffected;
    }
    public int updatePaymentGateway(int intProjId, string strName, int intActive, int intUpdatedBy)
    {
        int intRecordAffected = 0;
        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PROJECTPAYMENTGATEWAY SET " +
                     "PG_ACTIVE = ?," +
                     "PG_LASTUPDATE = SYSDATE()," +
                     "PG_UPDATEDBY = ? " +
                     "WHERE PROJ_ID = ? AND PG_NAME = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PG_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PG_UPDATEDBY", OdbcType.Int, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_PROJ_ID", OdbcType.Int, 9).Value = intProjId;
            cmd.Parameters.Add("p_PG_NAME", OdbcType.VarChar, 250).Value = strName;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }
        return intRecordAffected;


    }
    public int deletePaymentGatewayByProjId(int intId)
    {
        int intRecordAffected = 0;
        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PROJECTPAYMENTGATEWAY WHERE PROJ_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROJ_ID", OdbcType.Int, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected > 0)
            {
                trans.Commit();
                projId = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion

    #region "DB"
    public int SQLScript(string strSQLScript)
    {
        int intRecordAffected = 0;
        ErrorMsg = "";

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);
            strSql = strSQLScript;

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(GetType().Name+"  #  "+System.Reflection.MethodBase.GetCurrentMethod().Name+"  #  "+ex.Message.ToString());
            ErrorMsg = ex.Message.ToString();
            //throw ex;
            //return ex.Message.ToString();
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion

    #region Session
    public static void resetSession()
    {
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTNAME] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTDRIVER] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTSERVER] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTUSERID] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTPASSWORD] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTDATABASE] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTOPTION] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTPORT] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTUPLOADPATH] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTUSERVIEWURL] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTTYPE] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTLANG] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTFULLPATH] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTFULLPATHVALUE] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTGROUPPARENTCHILD] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTFAQ] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTPROGRAMMANAGER] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTEVENTMANAGER] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTCOUPONMANAGER] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTMULTIPLECURRENCY] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTINVENTORY] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTMOBILEVIEW] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTPRODUCTDESCRIPTION] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTPAYMENTGATEWAY] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTGST] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTGSTNOTAVAILABLE] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTGSTINCLUSIVE] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTGSTAPPLIED] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTFBLOGIN] = null;

        HttpContext.Current.Session[clsAdmin.CONSTPROJECTIMGSETTING] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTFILESETTING] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTCKEDITORFILE] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTCKEDITORIMG] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTEVENTTHUMBSETTING] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTEVENTGALLSETTING] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTPRODGALLSETTING] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTTRAINING] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTEVENTMAXIMAGEUPLOAD] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTPRODPROMO] = null;

        HttpContext.Current.Session[clsAdmin.CONSTPROJECTRIGHTCLICK] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTWEBTEQLOGO] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTCKEDITORTOPMENU] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTTOPMENUROOT] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTDELIVERYDATETIME] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTPRODADDON] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTENQUIRYFIELD] = null;

        // config
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTCONFIGLIMITPAGE] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTCONFIGLIMITOBJECT] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTCONFIGLIMITSLIDESHOW] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTCONFIGLIMITSLIDESHOWIMAGE] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTCONFIGLIMITEVENT] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTCONFIGLIMITCATEGORY] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTCONFIGLIMITPRODUCT] = null;
        HttpContext.Current.Session[clsAdmin.CONSTGOOGLEANALYTICSCLIENTID] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTWATERMARK] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTFRIENDLYURL] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTDELIVERYMSG] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTGROUPMENU] = null;

        // slider
        HttpContext.Current.Session[clsAdmin.CONSTPROJECT_SLIDER_ACTIVE] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECT_SLIDER_DEFAULT] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECT_SLIDER_FULLSCREEN] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECT_SLIDER_MOBILE] = null;

        // Page Authorization
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTPGAUTHORIZATION] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTUNIVERSALLOGIN] = null;
        HttpContext.Current.Session[clsAdmin.CONSTPROJECTINDIVIDUALLOGIN] = null;
    }
    #endregion Session

    #endregion
}
