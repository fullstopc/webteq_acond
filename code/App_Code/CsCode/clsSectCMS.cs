﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsSectCMS
/// </summary>
public class clsSectCMS : dbconnBase
{
    #region "Properties"
    private int _sectCMSId;
    private int _cmsId;
    private string _cmsTitle;
    private string _cmsContent;
    private DateTime _cmsCreation;
    private int _cmsCreatedBy;
    private DateTime _cmsLastUpdate;
    private int _cmsUpdatedBy;
    private int _cmsActive;
    private string _sectCMSType;
    private DateTime _sectCMSCreation;
    private int _sectCMSCreatedBy;
    private DateTime _sectCMSLastUpdate;
    private int _sectCMSUpdatedBy;
    #endregion


    #region "Property Methods"
    public int sectCMSId
    {
        get { return _sectCMSId; }
        set { _sectCMSId = value; }
    }

    public int cmsId
    {
        get { return _cmsId; }
        set { _cmsId = value; }
    }

    public string cmsTitle
    {
        get { return _cmsTitle; }
        set { _cmsTitle = value; }
    }

    public string cmsContent
    {
        get { return _cmsContent; }
        set { _cmsContent = value; }
    }

    public DateTime cmsCreation
    {
        get { return _cmsCreation; }
        set { _cmsCreation = value; }
    }

    public int cmsCreatedBy
    {
        get { return _cmsCreatedBy; }
        set { _cmsCreatedBy = value; }
    }

    public DateTime cmsLastUpdate
    {
        get { return _cmsLastUpdate; }
        set { _cmsLastUpdate = value; }
    }

    public int cmsUpdatedBy
    {
        get { return _cmsUpdatedBy; }
        set { _cmsUpdatedBy = value; }
    }

    public int cmsActive
    {
        get { return _cmsActive; }
        set { _cmsActive = value; }
    }

    public string sectCMSType
    {
        get { return _sectCMSType; }
        set { _sectCMSType = value; }
    }

    public DateTime sectCMSCreation
    {
        get { return _sectCMSCreation; }
        set { _sectCMSCreation = value; }
    }

    public int sectCMSCreatedBy
    {
        get { return _sectCMSCreatedBy; }
        set { _sectCMSCreatedBy = value; }
    }

    public DateTime sectCMSLastUpdate
    {
        get { return _sectCMSLastUpdate; }
        set { _sectCMSLastUpdate = value; }
    }

    public int sectCMSUpdatedBy
    {
        get { return _sectCMSUpdatedBy; }
        set { _sectCMSUpdatedBy = value; }
    }
    #endregion


    #region "Methods"
    public clsSectCMS()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    public int addSectCMS(int intSectId, int pageId, int intCMSId, int intSectCMSType, int intSectCMSCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_SECTCMS (" +
                     "SECT_ID," +
                     "PAGE_ID," +
                     "CMS_ID," +
                     "SECTCMS_TYPE," +
                     "SECTCMS_CREATION," +
                     "SECTCMS_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SECT_ID", OdbcType.Int, 9).Value = intSectId;
            cmd.Parameters.Add("p_PAGE_ID", OdbcType.Int, 9).Value = pageId;
            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;
            cmd.Parameters.Add("p_SECTCMS_TYPE", OdbcType.Int, 9).Value = intSectCMSType;
            cmd.Parameters.Add("p_SECTCMS_CREATION", OdbcType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("p_SECTCMS_CREATEDBY", OdbcType.Int, 9).Value = intSectCMSCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";

                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                sectCMSId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getSectCMSList()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SECTCMS_ID, A.SECT_ID, A.PAGE_ID, A.CMS_ID, A.V_CMSTITLE, A.V_CMSCONTENT, A.V_CMSCREATION, A.V_CMSCREATEDBY, A.V_CMSLASTUPDATE, A.V_CMSUPDATEDBY, A.V_CMSACTIVE," +
                     " A.SECTCMS_TYPE, A.V_SECTCMSTYPENAME, A.SECTCMS_CREATION, A.SECTCMS_CREATEDBY, A.SECTCMS_LASTUPDATE, A.SECTCMS_UPDATEDBY" +
                     " FROM VW_SECTCMS A";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean isSectCMSExist(int intSectId, int intPageId, int intCMSId, int intSectCMSType)
    {
        Boolean boolExist = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_SECTCMS" +
                     " WHERE SECT_ID = ?" +
                     " AND PAGE_ID = ?" +
                     " AND CMS_ID = ?" +
                     " AND SECTCMS_TYPE = ?";

            cmd.Parameters.Add("p_SECT_ID", OdbcType.Int, 9).Value = intSectId;
            cmd.Parameters.Add("p_PAGE_ID", OdbcType.Int, 9).Value = intPageId;
            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;
            cmd.Parameters.Add("p_SECTCMS_TYPE", OdbcType.Int, 9).Value = intSectCMSType;
            
            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolExist = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public int deleteSectCMSById(int intSectId, int intPageId, int intCMSId, int intCMSType)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_SECTCMS WHERE SECT_ID = ? AND PAGE_ID = ? AND CMS_ID = ? AND SECTCMS_TYPE= ?";
            cmd.Parameters.Add("p_SECT_ID", OdbcType.Int, 9).Value = intSectId;
            cmd.Parameters.Add("p_PAGE_ID", OdbcType.Int, 9).Value = intPageId;
            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;
            cmd.Parameters.Add("p_SECTCMS_TYPE", OdbcType.Int, 9).Value = intCMSType;

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteSectCMSByCMSId(int intCMSId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_SECTCMS WHERE CMS_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion
}
