﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsAdjust
/// </summary>
public class clsPaymentGateway : dbconnBase
{
    #region "Properties"

    #endregion


    #region "Property Methods"

    #endregion


    #region "Methods"
    public clsPaymentGateway()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    
    public DataSet getBankTransferList()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT BT_ID,LIST_NAME,BT_NAME,BT_BANK,BT_BANKACC_NAME,BT_BANKACC_NUM,BT_ORDER,BT_ACTIVE,BT_CREATION,BT_CREATEDBY,BT_LASTUPDATE,BT_UPDATEDBY " +
                     "FROM TB_BANKTRANSFER A " +
                     "LEFT JOIN TB_LIST B ON A.BT_BANK = B.LIST_ID";


            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public int addBankTrans(string strBankSel,string strBankName, string strBankAccName, string strBankAccNum, int intCreatedBy)
    {
        int intRecordAffected = 0;
        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_BANKTRANSFER (" +
                     "BT_NAME," +
                     "BT_BANK," +
                     "BT_BANKACC_NAME," +
                     "BT_BANKACC_NUM," +
                     "BT_CREATION," +
                     "BT_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_BT_NAME", OdbcType.VarChar, 250).Value = strBankName;
            cmd.Parameters.Add("p_BT_BANK", OdbcType.VarChar, 250).Value = strBankSel;
            cmd.Parameters.Add("p_BT_BANKACC_NAME", OdbcType.VarChar, 250).Value = strBankAccName;
            cmd.Parameters.Add("p_BT_BANKACC_NUM", OdbcType.VarChar, 250).Value = strBankAccNum;
            cmd.Parameters.Add("p_BT_CREATEDBY", OdbcType.Int, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                //string strSql2;
                //OdbcCommand cmd2 = new OdbcCommand();
                //strSql2 = "SELECT LAST_INSERT_ID()";
                //cmd2.Connection = openConn();
                //cmd2.CommandType = CommandType.Text;
                //cmd2.CommandText = strSql2;
                //areaId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }
        return intRecordAffected;
    }
    public int deleteBankTransId(int intId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_BANKTRANSFER WHERE BT_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_BT_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }


 
   

    #endregion
}
