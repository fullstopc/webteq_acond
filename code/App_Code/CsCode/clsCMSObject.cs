﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsADMCMS
/// </summary>
public class clsCMSObject : dbconnBase
{
    #region "Properties"
    private int _cmsId;
    private string _cmsTitle;
    private string _cmsContent;
    private int _cmsType;
    private string _cmsTypeName;
    private DateTime _cmsCreation;
    private int _cmsCreatedBy;
    private DateTime _cmsLastUpdate;
    private int _cmsUpdatedBy;
    private int _cmsActive;
    private int _cmsAllowDelete;

    // cms backup
    private int _cmsBackupId;
    private int _cmsBackupType;
    private int _cmsBackupBy;
    private string _cmsBackupContent;
    private DateTime _cmsBackupDate;

    #endregion


    #region "Property Methods"
    public int cmsId
    {
        get { return _cmsId; }
        set { _cmsId = value; }
    }

    public string cmsTitle
    {
        get { return _cmsTitle; }
        set { _cmsTitle = value; }
    }

    public string cmsContent
    {
        get { return _cmsContent; }
        set { _cmsContent = value; }
    }

    public int cmsType
    {
        get { return _cmsType; }
        set { _cmsType = value; }
    }

    public string cmsTypeName
    {
        get { return _cmsTypeName; }
        set { _cmsTypeName = value; }
    }

    public DateTime cmsCreation
    {
        get { return _cmsCreation; }
        set { _cmsCreation = value; }
    }

    public int cmsCreatedBy
    {
        get { return _cmsCreatedBy; }
        set { _cmsCreatedBy = value; }
    }

    public DateTime cmsLastUpdate
    {
        get { return _cmsLastUpdate; }
        set { _cmsLastUpdate = value; }
    }

    public int cmsUpdatedBy
    {
        get { return _cmsUpdatedBy; }
        set { _cmsUpdatedBy = value; }
    }

    public int cmsActive
    {
        get { return _cmsActive; }
        set { _cmsActive = value; }
    }

    public int cmsAllowDelete
    {
        get { return _cmsAllowDelete; }
        set { _cmsAllowDelete = value; }
    }

    // CMS Backup
    public int cmsBackupId
    {
        get { return _cmsBackupId; }
        set { _cmsBackupId = value; }
    }
    public int cmsBackupType
    {
        get { return _cmsBackupType; }
        set { _cmsBackupType = value; }
    }
    public int cmsBackupBy
    {
        get { return _cmsBackupBy; }
        set { _cmsBackupBy = value; }
    }
    public string cmsBackupContent
    {
        get { return _cmsBackupContent; }
        set { _cmsBackupContent = value; }
    }
    public DateTime cmsBackupDate
    {
        get { return _cmsBackupDate; }
        set { _cmsBackupDate = value; }
    }
    #endregion


    #region "Methods"
    public clsCMSObject()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    public int addCMS(string strCMSTitle, string strCMSContent, int intCMSCreatedBy, int intCMSType, int intActive)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_CMS (" +
                     "CMS_TITLE," +
                     "CMS_TYPE," +
                     "CMS_CONTENT," +
                     "CMS_CREATION," +
                     "CMS_CREATEDBY," +
                     "CMS_ACTIVE" +
                     ") VALUES " +
                     "(?,?,?,SYSDATE(),?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CMS_TITLE", OdbcType.VarChar, 250).Value = strCMSTitle;
            cmd.Parameters.Add("p_CMS_TYPE", OdbcType.Int, 9).Value = intCMSType;
            cmd.Parameters.Add("p_CMS_CONTENT", OdbcType.Text).Value = strCMSContent;
            cmd.Parameters.Add("p_CMS_CREATEDBY", OdbcType.Int, 9).Value = intCMSCreatedBy;
            cmd.Parameters.Add("p_CMS_ACTIVE", OdbcType.Int, 1).Value = intActive;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";

                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                cmsId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int addCMS(string strCMSTitle, string strCMSContent, int intCMSCreatedBy, int intCMSType, int intActive, int intAllowDelete)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_CMS (" +
                     "CMS_TITLE," +
                     "CMS_TYPE," +
                     "CMS_CONTENT," +
                     "CMS_CREATION," +
                     "CMS_CREATEDBY," +
                     "CMS_ACTIVE, " +
                     "CMS_ALLOWDELETE" +
                     ") VALUES " +
                     "(?,?,?,SYSDATE(),?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CMS_TITLE", OdbcType.VarChar, 250).Value = strCMSTitle;
            cmd.Parameters.Add("p_CMS_TYPE", OdbcType.Int, 9).Value = intCMSType;
            cmd.Parameters.Add("p_CMS_CONTENT", OdbcType.Text).Value = strCMSContent;
            cmd.Parameters.Add("p_CMS_CREATEDBY", OdbcType.Int, 9).Value = intCMSCreatedBy;
            cmd.Parameters.Add("p_CMS_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_CMS_ALLOWDELETE", OdbcType.Int, 1).Value = intAllowDelete;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";

                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                cmsId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public Boolean extractCMSById(int intCMSId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CMS_ID, A.CMS_TITLE, A.CMS_CONTENT, A.CMS_TYPE, A.V_CMSTYPENAME, A.CMS_CREATION, A.CMS_CREATEDBY, A.CMS_LASTUPDATE, A.CMS_UPDATEDBY, A.CMS_ACTIVE, A.CMS_ALLOWDELETE" +
                     " FROM VW_CMS A" +
                     " WHERE A.CMS_ID = ?";

            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;

            if (intActive != 0)
            {
                strSql += " AND A.CMS_ACTIVE = ?";

                cmd.Parameters.Add("p_CMS_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                cmsId = int.Parse(ds.Tables[0].Rows[0]["CMS_ID"].ToString());
                cmsTitle = ds.Tables[0].Rows[0]["CMS_TITLE"].ToString();
                cmsContent = ds.Tables[0].Rows[0]["CMS_CONTENT"].ToString();
                cmsType = Convert.ToInt16(ds.Tables[0].Rows[0]["CMS_TYPE"]);
                cmsTypeName = ds.Tables[0].Rows[0]["V_CMSTYPENAME"].ToString();
                cmsCreation = ds.Tables[0].Rows[0]["CMS_CREATION"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["CMS_CREATION"]) : DateTime.MaxValue;
                cmsCreatedBy = int.Parse(ds.Tables[0].Rows[0]["CMS_CREATEDBY"].ToString());
                cmsLastUpdate = ds.Tables[0].Rows[0]["CMS_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["CMS_LASTUPDATE"]) : DateTime.MaxValue;
                cmsUpdatedBy = int.Parse(ds.Tables[0].Rows[0]["CMS_UPDATEDBY"].ToString());
                cmsActive = Convert.ToInt16(ds.Tables[0].Rows[0]["CMS_ACTIVE"]);
                cmsAllowDelete = Convert.ToInt16(ds.Tables[0].Rows[0]["CMS_ALLOWDELETE"]);
            }

            ds.Dispose();
            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    public DataSet getCMSList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CMS_ID, A.CMS_TITLE, A.CMS_CONTENT, A.CMS_TYPE, A.V_CMSTYPENAME, A.CMS_CREATION, A.CMS_CREATEDBY, A.CMS_LASTUPDATE, A.CMS_UPDATEDBY, A.CMS_ACTIVE, A.CMS_ALLOWDELETE" +
                     " FROM VW_CMS A";

            if (intActive != 0)
            {
                strSql += " WHERE A.CMS_ACTIVE = ?";

                cmd.Parameters.Add("p_CMS_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public DataSet getCMSDataTables(Dictionary<string, string> keyword,
                                 Dictionary<string, string> filterLike,
                                 Dictionary<string, string> filterAnd,
                                 string sorting, int start, int length)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            List<string> columns = new List<string>()
            {
                "A.CMS_ID", "A.CMS_TITLE", "A.CMS_CONTENT", "A.CMS_TYPE", "A.V_CMSTYPENAME",
                "A.CMS_CREATION", "A.CMS_CREATEDBY", "A.CMS_LASTUPDATE", "A.CMS_UPDATEDBY", "A.CMS_ACTIVE",
                "A.CMS_ALLOWDELETE"
            };

            string strSql = "SELECT * FROM (SELECT " + string.Join(",", columns.ToArray()) + " FROM VW_CMS A) VW_CMS WHERE 1=1";

            if (keyword != null)
            {
                List<string> temp = new List<string>();
                foreach (KeyValuePair<string, string> entry in keyword)
                {
                    temp.Add(entry.Key + " LIKE ?");
                    cmd.Parameters.AddWithValue("k_" + entry.Key, "%" + entry.Value + "%");
                }
                strSql += " AND (" + string.Join(" OR ", temp.ToArray()) + ")";
            }

            if (filterLike != null)
            {
                foreach (KeyValuePair<string, string> entry in filterLike)
                {
                    strSql += " AND " + entry.Key + " LIKE ?";
                    cmd.Parameters.AddWithValue("fl_" + entry.Key, "%" + entry.Value + "%");
                }

            }

            if (filterAnd != null)
            {
                foreach (KeyValuePair<string, string> entry in filterAnd)
                {
                    strSql += " AND " + entry.Key + " = ?";
                    cmd.Parameters.AddWithValue("fa_" + entry.Key, "" + entry.Value + "");
                }

            }

            if (sorting != null)
            {
                strSql += " ORDER BY " + sorting;
            }
            if (length > 0)
            {
                strSql += " LIMIT " + start + "," + length + "";
            }


            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public int getCMSDataTablesCount(Dictionary<string, string> keyword,
                                     Dictionary<string, string> filterLike,
                                     Dictionary<string, string> filterAnd)
    {
        int count = 0;
        try
        {
            OdbcCommand cmd = new OdbcCommand();
            List<string> columns = new List<string>()
            {
                "A.CMS_ID", "A.CMS_TITLE", "A.CMS_CONTENT", "A.CMS_TYPE", "A.V_CMSTYPENAME",
                "A.CMS_CREATION", "A.CMS_CREATEDBY", "A.CMS_LASTUPDATE", "A.CMS_UPDATEDBY", "A.CMS_ACTIVE",
                "A.CMS_ALLOWDELETE"
            };

            string strSql = "SELECT COUNT(*) FROM (SELECT " + string.Join(",", columns.ToArray()) + " FROM VW_CMS A) VW_CMS WHERE 1=1";

            if (keyword != null)
            {
                List<string> temp = new List<string>();
                foreach (KeyValuePair<string, string> entry in keyword)
                {
                    temp.Add(entry.Key + " LIKE ?");
                    cmd.Parameters.AddWithValue("k_" + entry.Key, "%" + entry.Value + "%");
                }
                strSql += " AND (" + string.Join(" OR ", temp.ToArray()) + ")";
            }

            if (filterLike != null)
            {
                foreach (KeyValuePair<string, string> entry in filterLike)
                {
                    strSql += " AND " + entry.Key + " LIKE ?";
                    cmd.Parameters.AddWithValue("fl_" + entry.Key, "%" + entry.Value + "%");
                }

            }

            if (filterAnd != null)
            {
                foreach (KeyValuePair<string, string> entry in filterAnd)
                {
                    strSql += " AND " + entry.Key + " = ?";
                    cmd.Parameters.AddWithValue("fa_" + entry.Key, "" + entry.Value + "");
                }

            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            count = Convert.ToInt16(cmd.ExecuteScalar());
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return count;
    }

    public Boolean isExactSameSetCMS(int intCMSId, string strCMSTitle, string strCMSContent, int intCMSType, int intActive)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_CMS" +
                     " WHERE CMS_ID = ?" +
                     " AND BINARY CMS_TITLE = ?" +
                     " AND BINARY CMS_CONTENT = ?" +
                     " AND CMS_TYPE = ?" +
                     " AND CMS_ACTIVE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;
            cmd.Parameters.Add("p_CMS_TITLE", OdbcType.VarChar, 250).Value = strCMSTitle;
            cmd.Parameters.Add("p_CMS_CONTENT", OdbcType.Text).Value = strCMSContent;
            cmd.Parameters.Add("p_CMS_TYPE", OdbcType.Int, 9).Value = intCMSType;
            cmd.Parameters.Add("p_CMS_ACTIVE", OdbcType.Int, 1).Value = intActive;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }
    public int updateCMSById(int intCMSId, string strCMSTitle, string strCMSContent, int intCMSUpdatedBy, int intCMSType, int intActive)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_CMS SET" +
                     " CMS_TITLE = ?," +
                     " CMS_CONTENT = ?," +
                     " CMS_LASTUPDATE = SYSDATE()," +
                     " CMS_UPDATEDBY = ?," +
                     " CMS_TYPE = ?," +
                     " CMS_ACTIVE = ?" +
                     " WHERE CMS_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CMS_TITLE", OdbcType.VarChar, 250).Value = strCMSTitle;
            cmd.Parameters.Add("p_CMS_CONTENT", OdbcType.Text).Value = strCMSContent;
            cmd.Parameters.Add("p_CMS_UPDATEDBY", OdbcType.Int, 9).Value = intCMSUpdatedBy;
            cmd.Parameters.Add("p_CMS_TYPE", OdbcType.Int, 9).Value = intCMSType;
            cmd.Parameters.Add("p_CMS_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                cmsId = intCMSId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int updateCMSContentById(int intCMSId, string strCMSContent)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_CMS SET" +
                     " CMS_CONTENT = ?," +
                     " CMS_LASTUPDATE = SYSDATE()" +
                     " WHERE CMS_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            
            cmd.Parameters.Add("p_CMS_CONTENT", OdbcType.Text).Value = strCMSContent;
            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                cmsId = intCMSId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteCMSById(int intCMSId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_CMS WHERE CMS_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                cmsId = intCMSId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int getTotalRecord(int intActive)
    {
        int intTotal = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_CMS A";

            if (intActive != 0)
            {
                strSql += " WHERE A.CMS_ACTIVE = ?";
                cmd.Parameters.Add("p_CMS_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            intTotal = int.Parse(cmd.ExecuteScalar().ToString());
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intTotal;
    }
    #endregion

    #region CMS Backup
    public DataSet getCMSBackupDataset()
    {
        return getCMSBackupDataset(0);
    }
    public DataSet getCMSBackupDataset(int intCMSId)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_CMSBACKUP WHERE 1 = 1";

            if (intCMSId != 0)
            {
                strSql += " AND CMS_ID = ?";
                cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            ds = null;
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }
        return ds;
    }
    public string getLastCMSBackupContent(int intCMSId)
    {
        string strLatestCont = "";

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT CMSBACKUP_CONTENT FROM TB_CMSBACKUP" +
                     " WHERE 1 = 1";

            if (intCMSId != 0)
            {
                strSql += " AND CMS_ID = ?";
                cmd.Parameters.Add("p_CMS_ID", OdbcType.BigInt, 9).Value = intCMSId;
            }

            strSql += " ORDER BY CMSBACKUP_ID DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            strLatestCont = cmd.ExecuteScalar().ToString();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return strLatestCont;
    }
    public int getTotalCMSBackup(int intCMSId)
    {
        int intTotal = 0;

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            strSql = "SELECT COUNT(*) AS TOTAL FROM TB_CMSBACKUP" +
                     " WHERE 1 = 1";

            if (intCMSId != 0)
            {
                strSql += " AND CMS_ID = ?";
                cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;
            }


            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            intTotal = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intTotal;
    }

    public Boolean extractCMSBackupByBackupId(int intCMSBackupId)
    {
        return extractCMSBackup(intCMSBackupId);
    }
    public Boolean extractCMSBackup(int intCMSBackupId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_CMSBACKUP A" +
                     " WHERE 1 = 1";

            
           
            if (intCMSBackupId != 0)
            {
                strSql += " AND  A.CMSBACKUP_ID = ?";
                cmd.Parameters.Add("p_CMSBACKUP_ID", OdbcType.Int, 9).Value = intCMSBackupId;
            }


            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                cmsId = Convert.ToInt32(ds.Tables[0].Rows[0]["CMS_ID"]);
                cmsBackupId = Convert.ToInt32(ds.Tables[0].Rows[0]["CMSBACKUP_ID"]);
                cmsBackupType = Convert.ToInt16(ds.Tables[0].Rows[0]["CMSBACKUP_TYPE"]);
                cmsBackupDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["CMSBACKUP_DATE"]);
                cmsBackupContent = Convert.ToString(ds.Tables[0].Rows[0]["CMSBACKUP_CONTENT"]);
            }

            ds.Dispose();
            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    public int addCMSBackup(int intCmdId, string strContent, int intBackupBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_CMSBACKUP (" +
                     "CMS_ID," +
                     "CMSBACKUP_CONTENT," +
                     "CMSBACKUP_TYPE," +
                     "CMSBACKUP_BY," +
                     "CMSBACKUP_DATE" +
                     ") VALUES " +
                     "(?,?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCmdId;
            cmd.Parameters.Add("p_CMSBACKUP_CONTENT", OdbcType.Text).Value = strContent;
            cmd.Parameters.Add("p_CMSBACKUP_TYPE", OdbcType.Int, 1).Value = 1;
            cmd.Parameters.Add("p_CMSBACKUP_BY", OdbcType.Int, 9).Value = intBackupBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";

                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                cmsId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteOldCMSBackup(int intCmsId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_CMSBACKUP WHERE CMS_ID = ? ORDER BY CMSBACKUP_DATE DESC LIMIT 1";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCmsId;

            intRecordAffected = cmd.ExecuteNonQuery();
            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    #endregion
}
