﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsAdmin
/// </summary>

public class clsAdmin : dbconnBase
{
    #region "Constants"

    #region "Session Keys"
    public const string CONSTPROJECTNAME = "PROJECTNAME";
    public const string CONSTPROJECTDRIVER = "PROJECTDRIVER";
    public const string CONSTPROJECTSERVER = "PROJECTSERVER";
    public const string CONSTPROJECTUSERID = "PROJECTUSERID";
    public const string CONSTPROJECTPASSWORD = "PROJECTPASSWORD";
    public const string CONSTPROJECTDATABASE = "PROJECTDATABASE";
    public const string CONSTPROJECTOPTION = "PROJECTOPTION";
    public const string CONSTPROJECTPORT = "PROJECTPORT";
    public const string CONSTPROJECTUPLOADPATH = "PROJECTUPLOADPATH";
    public const string CONSTPROJECTUSERVIEWURL = "PROJECTUSERVIEWURL";
    public const string CONSTPROJECTTYPE = "PROJECTTYPE";
    public const string CONSTPROJECTLANG = "PROJECTLANG";
    public const string CONSTPROJECTFULLPATH = "PROJECTFULLPATH";
    public const string CONSTPROJECTFULLPATHVALUE = "PROJECTFULLPATHVALUE";
    public const string CONSTPROJECTGROUPPARENTCHILD = "PROJECTGROUPPARENTCHILD";
    public const string CONSTPROJECTFAQ = "PROJECTFAQ";
    public const string CONSTPROJECTPROGRAMMANAGER = "PROJECTPROGRAMMANAGER";
    public const string CONSTPROJECTEVENTMANAGER = "PROJECTEVENTMANAGER";
    public const string CONSTPROJECTPRODUCTDESCRIPTION = "PROJECTDESCRIPTION";
    public const string CONSTPROJECTCOUPONMANAGER = "PROJECTCOUPONMANAGER";
    public const string CONSTPROJECTCOUPONMANAGERPACKAGE = "PROJECTCOUPONMANAGERPACKAGE";
    public const string CONSTPROJECTMULTIPLECURRENCY = "PROJECTMULTIPLECURRENCY";
    public const string CONSTPROJECTINVENTORY = "PROJECTINVENTORY";
    public const string CONSTPROJECTMOBILEVIEW = "PROJECTMOBILEVIEW";
    public const string CONSTPROJECTDELIVERYMSG = "PROJECTDELIVERYMSG";
    public const string CONSTPROJECTPAYMENTGATEWAY = "PROJECTPAYMENTGATEWAY";
    public const string CONSTPROJECTGST = "PROJECTGST";
    public const string CONSTPROJECTGSTNOTAVAILABLE = "PROJECTGSTNOTAVAILABLE";
    public const string CONSTPROJECTGSTINCLUSIVE = "PROJECTINCLUSIVE";
    public const string CONSTPROJECTGSTAPPLIED = "PROJECTAPPLIED";
    public const string CONSTPROJECTFBLOGIN = "PROJECTFBLOGIN";
    public const string CONSTPROJECTWATERMARK = "PROJECTWATERMARK";
    public const string CONSTPROJECTFRIENDLYURL = "PROJECTFRIENDLYURL";
    public const string CONSTPROJECTGROUPMENU = "PROJECTGROUPMENU";
    public const string CONSTPROJECTIMGSETTING = "PROJECTIMGSETTING";
    public const string CONSTPROJECTFILESETTING = "PROJECTFILESETTING";
    public const string CONSTPROJECTCKEDITORFILE = "PROJECTCKEDITORFILE";
    public const string CONSTPROJECTCKEDITORIMG = "PROJECTCKEDITORIMG";
    public const string CONSTPROJECTDEFAULTSETTING = "PROJECTDEFAULTSETTING";
    public const string CONSTPROJECTEVENTTHUMBSETTING = "PROJECTEVENTTHUMBSETTING";
    public const string CONSTPROJECTEVENTGALLSETTING = "PROJECTEVENTGALLSETTING";
    public const string CONSTPROJECTPRODTHUMBSETTING = "PROJECTPRODTHUMBSETTING";
    public const string CONSTPROJECTPRODGALLSETTING = "PROJECTPRODGALLSETTING";
    public const string CONSTPROJECTTRAINING = "PROJECTTRAINING";
    public const string CONSTPROJECTEVENTMAXIMAGEUPLOAD = "PROJECTEVENTMAXIMAGEUPLOAD";
    public const string CONSTPROJECTPRODPROMO = "PROJECTPRODPROMO";
    public const string CONSTPROJECTENQUIRYFIELD = "PROJECTENQUIRYFIELD";
    public const string CONSTPROJECTRIGHTCLICK = "PROJ_RIGHTCLICK";
    public const string CONSTPROJECTWEBTEQLOGO = "PROJ_WEBTEQLOGO";
    public const string CONSTPROJECTCKEDITORTOPMENU = "PROJ_CKEDITORTOPMENU";
    public const string CONSTPROJECTTOPMENUROOT = "PROJ_TOPMENUROOT";
    public const string CONSTPROJECTDELIVERYDATETIME = "PROJ_DELIVERYDATETIME";
    public const string CONSTPROJECTPRODADDON = "PROJ_PRODADDON";

    // slider setting
    public const string CONSTPROJECT_SLIDER_ACTIVE = "PROJECTSLIDERACTIVE";
    public const string CONSTPROJECT_SLIDER_DEFAULT = "PROJECTSLIDERDEFAULT";
    public const string CONSTPROJECT_SLIDER_FULLSCREEN = "PROJECTSLIDERFULLSCREEN"; 
    public const string CONSTPROJECT_SLIDER_MOBILE = "PROJECTSLIDERMOBILE";

    //page authorization
    public const string CONSTPROJECTPGAUTHORIZATION = "PROJECTPGAUTHORIZATION";
    public const string CONSTPROJECTUNIVERSALLOGIN = "PROJECTUNIVERSALLOGIN";
    public const string CONSTPROJECTINDIVIDUALLOGIN = "PROJECTINDIVIDUALLOGIN";

    #region "Config"
    public const string CONSTPROJECTCONFIGLIMITPAGE = "PROJECTCONFIGLIMITPAGE";
    public const string CONSTPROJECTCONFIGLIMITOBJECT = "PROJECTCONFIGLIMITOBJECT";
    public const string CONSTPROJECTCONFIGLIMITSLIDESHOW = "PROJECTCONFIGLIMITSLIDESHOW";
    public const string CONSTPROJECTCONFIGLIMITSLIDESHOWIMAGE = "PROJECTCONFIGLIMITSLIDESHOWIMAGE";
    public const string CONSTPROJECTCONFIGLIMITEVENT = "PROJECTCONFIGLIMITEVENT";
    public const string CONSTPROJECTCONFIGLIMITCATEGORY = "PROJECTCONFIGLIMITCATEGORY";
    public const string CONSTPROJECTCONFIGLIMITPRODUCT = "PROJECTCONFIGLIMITPRODUCT";
    public const string CONSTPROJECTCONFIGLIMITPROGRAMGROUP = "PROJECTCONFIGLIMITPROGRAMGROUP";
    public const string CONSTPROJECTCONFIGLIMITPROGRAM = "PROJECTCONFIGLIMITPROGRAM";
    public const string CONSTGOOGLEANALYTICSCLIENTID = "GOOGLEANALYTICS_CLIENTID";
    #endregion

    public const string CONSTADMINCS = "ADMINCONNECTIONSTRING";
    #endregion


    #region "Path"
    public const string CONSTADMDEFAULTPATH = "adm/";
    public const string CONSTADMLANDINGPAGE = "";
    public const string CONSTADMLOGIN = "admlogin.aspx";
    public const string CONSTADMCHANGEPWD = "admProfile.aspx";
    public const string CONSTADMACCESSDENIEDPAGE = "adm00.aspx";
    public const string CONSTADMFORGOTPASSWORD = "admForgotPwd.aspx";
    public const string CONSTADMCONTROLPANEL = "admCMSControlPanel01.aspx";
    public const string CONSTADMUNAUTHORIZED = "unauthorized.htm";
    public const string CONSTADMPRODUCTDETAIL = "admProduct0101.aspx";
    public const string CONSTADMPRODUCTLIST = "admProduct01.aspx";
    public const string CONSTADMPAGEDETAIL = "admPage0101.aspx";
    public const string CONSTADMEVENTDETAIL = "admHighlight0101.aspx";

    /*Mobile*/
    public const string CONSTADMCHANGEPWDMOBILE = "admProfilemb.aspx";
    #endregion


    #region "Admin Page"
    public const string CONSTADMUSERMANAGERNAME = "USER MANAGER";

    public const string CONSTADMCONTROLPANELNAME = "CONTROL PANEL";
    public const string CONSTADMEMAILSETUPNAME = "EMAIL SETUP";
    public const string CONSTADMIMAGESETTINGS = "IMAGE SETTING";
    public const string CONSTADMPAYMENTGATEWAY = "PAYMENT GATEWAY";
    public const string CONSTADMORDERSLIPSETUP = "ORDER SLIP SETUP";
    public const string CONSTADMOBJECTMANAGERNAME = "OBJECT MANAGER";
    public const string CONSTADMPAGEMANAGERNAME = "PAGE CONTENT MANAGER";
    public const string CONSTADMSLIDESHOWGROUPNAME = "SLIDE SHOW GROUP";
    public const string CONSTADMSLIDESHOWMANAGERNAME = "SLIDE SHOW MANAGER";
    public const string CONSTADMEVENTMANAGERNAME = "EVENT MANAGER";
    public const string CONSTADMEVENTDESCRIPTIONNAME = "EVENT DESCRIPTION";
    public const string CONSTADMEVENTVIDEONAME = "EVENT VIDEO";
    public const string CONSTADMEVENTARTICLENAME = "EVENT ARTICLE";
    public const string CONSTADMEVENTCOMMENTNAME = "EVENT COMMENT";
    public const string CONSTADMCATEGORYMANAGERNAME = "CATEGORY MANAGER";
    public const string CONSTADMPRODUCTMANAGERNAME = "PRODUCT MANAGER";
    public const string CONSTADMMEMBERMANAGERNAME = "MEMBER MANAGER";
    public const string CONSTADMSALESMANAGERNAME = "SALES MANAGER";
    public const string CONSTADMADDSALESNAME = "ADD SALES";
    public const string CONSTADMSHIPPINGMANAGERNAME = "SHIPPING MANAGER";
    public const string CONSTADMCOUNTRYMANAGERNAME = "COUNTRY MANAGER";
    public const string CONSTADMFAQNAME = "FAQ";
    public const string CONSTADMFAQMANAGERNAME = "FAQ MANAGER";
    public const string CONSTADMPROGRAMNAME = "PROGRAM";
    public const string CONSTADMPROGRAMGROUPNAME = "PROGRAM GROUP MANAGER";
    public const string CONSTADMPROGRAMMANAGERNAME = "PROGRAM MANAGER";
    public const string CONSTADMMERCHANTNAMAGERNAME = "MERCHANT MANAGER";
    public const string CONSTADMDEALSMANAGERNAME = "DEALS MANAGER";
    public const string CONSTADMCOUPONMANAGERNAME = "COUPON MANAGER";
    public const string CONSTADMCUTOFFDATE = "CUTOFF DATE";
    public const string CONSTADMGSTSETTINGS = "GST SETTINGS";
    public const string CONSTADMPAGEMANAGER_ADDNEWPAGE = "ADD NEW PAGE";
    public const string CONSTADMPAGEMANAGER_REPLICATEPAGE = "REPLICATE PAGE";
    public const string CONSTADMMOBILEVIEW = "MOBILE VIEW";
    public const string CONSTADMSOCIALMEDIA = "SOCIAL MEDIA";
    public const string CONSTADMAREASETTING = "AREA SETTING";
    public const string CONSTADMMERCHANTEVENTNAME = "EVENT";
    public const string CONSTADMUPLOADINVENTORY = "UPLOAD INVENTORY";
    public const string CONSTADMUPLOADMETHOD = "INVENTORY UPDATE METHOD";
    public const string CONSTADMPRODINVENTORY = "PRODUCT INVENTORY";
    public const string CONSTADMGROUPMENU = "GROUP MENU";
    public const string CONSTADMTRAINING = "TRAINING";
    public const string CONSTADMTRAININGMANAGERNAME = "TRAINING MANAGER";
    public const string CONSTADMGOOGLESETTINGS = "GOOGLE SETTINGS";
    public const string CONSTADMPGAUTHORIZATION = "PAGE AUTHORIZATION";
    public const string CONSTADMUNILOGIN = "UNIVERSAL LOGIN";
    public const string CONSTADMINDLOGIN = "INDIVIDUAL LOGIN";
    public const string CONSTADMSMSSETUPNAME = "SMS SETUP";
    public const string CONSTADMPRODGALLERY = "PRODUCT GALLERY";
    public const string CONSTADMPRODADDON = "addon product";


    // start of Inventory module
    public const string CONSTADMINVENTORYNAME = "INVENTORY";
    public const string CONSTADMVENDORMANAGERNAME = "VENDOR MANAGER";
    public const string CONSTADMGOODSRECEIVEMANAGERNAME = "INVENTORY RECEIVABLE";
    public const string CONSTADMADJUSTMENTMANAGERNAME = "INVENTORY ADJUSTMENT";
    public const string CONSTADMPRODUCTBALANCENAME = "INVENTORY BALANCE";
    // end of inventory module

    // ADMIN PAGE ID
    public const int CONSTADMPAGEMANAGERID = 5;

    // Website type
    public const int CONSTDESKTOPVIEW = 1;
    public const int CONSTMOBILEVIEW = 2;
    
    #endregion


    #region "User View"
    public const string CONSTUSERVIEWMANAGEPAGE = "adm/admUserView.aspx";
    public const string CONSTUSERVIEWADMINLOGINPAGE = "adm/userview.aspx";
    #endregion


    #region "Ids"

    #region "AdminPage id"
    public const int CONSTADMPAGEID_SMSSETTING = 134;
    public const int CONSTADMPAGEID_PRODADDON = 135;
    #endregion

    #region "Project Type"
    public const int CONSTTYPEePROFILE = 1;
    public const int CONSTTYPEePROFILELITE = 2;
    public const int CONSTTYPEeSHOP = 3;
    public const int CONSTTYPEeSHOPLITE = 4;
    public const int CONSTTYPEeCATALOG = 5;
    public const int CONSTTYPEeCATALOGLITE = 6;
    public const int CONSTTYPETAILORED = 7;
    public const int CONSTTYPEeCATALOGLITEEVENTMANAGER = 8;
    public const int CONSTTYPEeDIRECTORYADMANAGER = 9;
    public const int CONSTTYPEESHOPINVOICE = 10;
    public const int CONSTTYPEESHOPLITEINVOICE = 11;
	public const int CONSTTYPEeSHOPEVENTMANAGER = 12;
	public const int CONSTTYPEeCATALOGEVENTMANAGER = 13;
    #endregion


    #region "Admin Type"
    public const int CONSTADMTYPE = 1;
    public const int CONSTUSERADMTYPE = 2;
    #endregion


    #region "Grouping"
    public const string CONSTLISTGROUPINGGROUP = "GROUPING";
    public const int CONSTLISTGROUPINGCAT = 1;
    public const string CONSTLISRGROUPINGCATNAME = "CATEGORY";

    public const int CONSTLISTGROUPINGBIZTYPE = 4; //@ reserve 2 and 3 for Brand and Type.
    public const string CONSTLISTGROUPINGBIZTYPENAME = "Business Type";    
        
    #endregion

    #region "SMS"
    public const string CONSTSMSORDERSTATUS = "ORDSTATUS";
    public const string CONSTSMSORDERSTATUSDELIVERY = "4";
    #endregion

    /*Customized for YSHamper Project*/
    #region "Stock Status"
    public const string CONSTLISTSTOCKSTATUS = "STOCK STATUS";
    #endregion
    /*End of Customized for YSHamper Project*/

    #region "Order Status"
    public const int CONSTTRANSSTATUSCANCEL = -1;

    /*Invoice Manager*/
    public const int CONSTORDERSTATUSINVOICENEW = 1;
    public const int CONSTORDERSTATUSINVOICEPAY = 2;
    //public const int CONSTORDERSTATUSPROCESSING = 3;
    public const int CONSTORDERSTATUSINVOICEDELIVERY = 3;
    public const int CONSTORDERSTATUSINVOICEDONE = 4;
    /*End of Invoice Manager*/

    public const int CONSTORDERSTATUSNEW = 1;
    public const int CONSTORDERSTATUSPAY = 2;
    public const int CONSTORDERSTATUSPROCESSING = 3;
    public const int CONSTORDERSTATUSDELIVERY = 4;
    public const int CONSTORDERSTATUSDONE = 5;
    #endregion


    #region "Page"
    public const int CONSTSAMETABID = 1;
    public const int CONSTNEWTABID = 2;
    #endregion


    #region "CMS Group"
    public const int CONSTCMSGROUPCONTENT = 1;
    public const int CONSTCMSGROUPSLIDESHOW = 2;
    #endregion

    #region "Sales Report Type"
    public const int CONSTTYPEDETAILED = 1;
    public const int CONSTTYPESUMMARY = 2;
    #endregion

    #region "Shipping Type"
    public const int CONSTSHIPPINGTYPE1 = 1;
    public const int CONSTSHIPPINGTYPE2 = 2;
    public const int CONSTSHIPPINGTYPE3 = 3;
    public const int CONSTSHIPPINGTYPE4 = 4;
    public const int CONSTSHIPPINGTYPE5 = 5;
    public const int CONSTSHIPPINGTYPE6 = 6;
    #endregion

    #region "Language"
    public const string CONSTLANGJAPANESE = "ja";
    public const string CONSTLANGMALAY = "ms";
    public const string CONSTLANGCHINESE = "zh";
    #endregion
    #endregion

    #region "Miscellaneous Settings"
    public const int CONSTADMUSERNAMEMINLENGTH = 5;
    public const int CONSTADMPASSWORDMINLENGTH = 6;
    public const char CONSTDATESEPERATOR = '/';
    public const char CONSTDEFAULTSEPERATOR = '|';
    public const char CONSTCOMMASEPARATOR = ',';
    public const char CONSTSYMBOLSEPARATOR = '`';
    public const char CONSTCOMMASPACE = ' ';
    public const string CONSTDEFAULTSEPERATOR2 = "^*^";
    public const string CONSTDBCHAR1 = "<|*";
    public const string CONSTDBCHAR2 = "*|>";
    public const string CONSTDBCHAR3 = ";;;";

    public const string CONSTCOMMASEPERATORSTRING = ",";

    public const string CONSTSORTASC = "ASC";
    public const string CONSTSORTDESC = "DESC";

    public const int CONSTEXTERNALLINK = 9999;
    public const int CONSTMAXORDERGALLERY = 9999;

    public const string CONSTADMCATCODERE = "^[a-zA-Z0-9_]*$";
    public const string CONSTADMPRODCODERE = "^[a-zA-Z\\-!$%^&*()_+|~=`{}\\[\\]:\";'<>?,.\\/#@0-9_]*$";
    public const string CONSTNUMERICRE = "^\\d+$";
    public const string CONSTNUMERICRE2 = "^-?\\d+$";
    public const string CONSTNUMERICNOLEADINGZERORE = "^[1-9]\\d{0,}$";
    public const string CONSTCURRENCYRE = "^(\\+)?\\d{1,13}(\\.\\d{1,2})?$";
    public const string CONSTCURRENCYRE2 = "^(\\+)?\\d{1,4}(\\.\\d{1,6})?$";
    public const string CONSTCURRENCYRE3 = "^(\\+)?\\d{1,13}(\\.\\d{1,2})?$";
    public const string CONSTCURRENCYRE4 = "^([0-9]+)(.[0-9][0-9])$";
    public const string CONSTBIGCURRENCYRE = "^([0-9]|[1-9][0-9]|[1-9][0-9][0-9]|[1-9][0-9][0-9][0-9])(.[0-9][0-9])$";
    public const string CONSTPERCENTAGECURRECCYRE = "^(100(?:\\.0{1,2})?|0*?\\.\\d{1,2}|\\d{1,2}(?:\\.\\d{1,2})?)$";
    public const string CONSTADMIMGALLOWEDEXT = "^.+\\.((gif)|(jpg)|(jpeg)|(ico)|(png))$";
    public const string CONSTADMDOCALLOWEDEXT = "^.+\\.((pdf)|(doc)|(docx)|(png))$";
    public const string CONSTADMDOCIMGALLOWEDEXT = "^.+\\.((pdf)|(doc)|(docx)|(gif)|(jpg)|(jpeg)|(png))$";
    public const string CONSTADMPAGETITLEFRIENDLYURL = "^[a-zA-Z0-9_-]*$";
    public const string CONSTALPHANUMERICRE = "^[a-zA-Z0-9_]*$";
    public const string CONSTSIGNEDCURRENCYRE = "^(\\+|-)?\\d{1,5}(\\.\\d{1,2})?$";
    public const string CONSTADMINVENTORYALLOWEDEXT = "^.+\\.(csv)$";

    public const int CONSTADMIMGALLOWEDMAXLENGTH = 512000; //500kb
    public const int CONSTADMDOCALLOWEDMAXLENGTH = 2098152; //2mb
    public const int CONSTADMIMGALLOWEDWIDTH = 500;
    public const int CONSTADMIMGALLOWEDHEIGHT = 500;
    public const int CONSTADMCURIMGALLOWEDWIDTH = 30;
    public const int CONSTADMCURIMGALLOWEDHEIGHT = 30;

    public const int CONSTADMDEFAULTIMGWIDTH = 230;
    public const int CONSTADMDEFAULTIMGHEIGHT = 230;

    public const int CONSTADMPAGESIZE = 100;

    public const int CONSTMAXPRODUCTRELATED = 20;
    public const int CONSTMAXDEALRELATED = 10;

    public const int CONSTADMINVOICELOGOWIDTH = 120;
    public const int CONSTADMINVOICELOGOHEIGHT = 90;

    public const int CONSTORDMEMTYPE1 = 1;
    public const int CONSTORDMEMTYPE2 = 2;

    public const string CONSTTRUE = "True";
    public const string CONSTFALSE = "False";

    public const string CONSTPARENTROOT2 = "-1";
    public const int CONSTMAXORDERHIGHGALLERY = 9999;
    public const string CONSTNAMESHIPPINGCOM = "SHIPPING COMPANY";

    public const string CONSTDATEFORMAT = "yyyy-M-d h:m:s";
    public const string CONSTDATEFORMAT2 = "yyyy-M-d hh:mm:ss";
    public const string CONSTDATEFORMAT3 = "yyyy-M-d H:m:s";

    public const int CONSTFULLPATHACTIVE = 3;
    public const int CONSTFULLPATHDEFAULT = 2;

    public const int CONSTUSRINDPRODUCTIMGWIDTH = 330;
    public const int CONSTUSRINDPRODUCTIMGHEIGHT = 330;

    // Watermark 
    public const string CONSTWATERMARKTYPETEXT = "1";
    public const string CONSTWATERMARKTYPEIMAGE = "2";

    public const int CONSTADMIMGALLOWEDMAXLENGTHINKB = 500;
    public const int CONSTADMDOCALLOWEDMAXLENGTHINKB = 2048;
    #endregion


    #region "Editor"
    public const string CONSTADMSTYLESET = "set1";
    public const string CONSTADMSTYLESETFILE = "ckeditorStylesSet.js";
    public const string CONSTADMCMSCSS = "public.css";

    public const string CONSTCMSADMCMSVIEW = "admcms";
    public const string CONSTADMCTRLPNLVIEW = "admctrlpnl";
    public const string CONSTCMSGRPVIEW = "grp";
    public const string CONSTCMSPRODVIEW = "prod";
    public const string CONSTCMSHIGHVIEW = "high";
    public const string CONSTCMSNEWSVIEW = "news";
    public const string CONSTCMSCMSVIEW = "cms";
    public const string CONSTCMSMEMVIEW = "mem";
    public const string CONSTDIRVIEW = "dir";
    public const string CONSTADMADFOLDER = "ad";
    public const string CONSTADMDEALVIEW = "deal";
    
    #endregion

    #region "tb_list naming"
    #region "LIST_GROUP"
    public const string CONSTLISTGROUP_SLIDERTYPE = "SLIDER TYPE";
    public const string CONSTLISTGROUP_TRANSITIONTYPEFULLSCREEN = "TRANSITION TYPE FULLSCREEN";
    public const string CONSTLISTGROUP_TRANSITIONTYPEDEFAULT = "TRANSITION TYPE DEFAULT";
    public const string CONSTLISTGROUP_TRANSITIONTYPEMOBILE = "TRANSITION TYPE MOBILE";
    #endregion

    #region "LIST_NAME"
    #endregion

    #region "LIST_VALUE"
    public const string CONSTLISTVALUE_DEFAULT = "1";
    public const string CONSTLISTVALUE_FULLSCREEN = "2";
    public const string CONSTLISTVALUE_MOBILE = "3";

    public const string CONSTLIST_COUPONMANAGERPACKAGE_LITE = "1";
    public const string CONSTLIST_COUPONMANAGERPACKAGE_ADV = "2";
    #endregion
    #endregion
    
    public const int CONSTUSRSLIDESHOWTHUMBWIDTH = 90;
    public const int CONSTUSRSLIDESHOWTHUMBHEIGHT = 30;
    public const int CONSTUSRDIRIMGWIDTH = 220;
    public const int CONSTUSRDIRIMGHEIGHT = 165;
    public const int CONSTUSRADBANNERWIDTH = 158;
    public const int CONSTUSRADBANNERHEIGHT = 119;
    /* Editor
    

    public const string CONSTCMSGRPFOLDER = "grpcms";
    public const string CONSTCMSPRODFOLDER = "prodcms";
    public const string CONSTCMSMEMFOLDER = "memcms";
    public const string CONSTCMSADMFOLDER = "admcms";
    public const string CONSTADMHIGHCMSFOLDER = "highcms";
    public const string CONSTADMREMARKSCMSFOLDER = "remarkscms";
    public const string CONSTCMSFOLDER = "cms";
    public const string CONSTADMROOMCMSFOLDER = "roomcms";
    public const string CONSTADMNEWSCMSFOLDER = "newscms";
    public const string CONSTADMMASTHEADFOLDER = "masthead";

    public const string CONSTFLASHFOLDER = "flash";

    public const string CONSTFLASHXMLFILE = "category.xml";
    public const string CONSTFLASHXMLFILE_ZH = "category_zh.xml";
    /* End of Editor */


    #region "Folders"
    public const string CONSTLOGFOLDER = "log";
    public const string CONSTADMTEMPFOLDER = "temp";
    public const string CONSTCMSFOLDER = "cms";
    public const string CONSTARTICLEFOLDER = "article";
    public const string CONSTGALLERYFOLDER = "gallery";
    public const string CONSTVIDEOFOLDER = "video";

    public const string CONSTADMPRODARTICLEFOLDER = "prodart";
    public const string CONSTADMPRODCMSFOLDER = "prodcms";

    public const string CONSTCMSADMFOLDER = "admcms";
    public const string CONSTCTRLPNLADMFOLDER = "admctrlpnl";
    public const string CONSTADMGRPFOLDER = "cat";
    public const string CONSTADMPRODFOLDER = "prod";
    public const string CONSTADMMEMFOLDER = "mem";
    public const string CONSTADMHIGHFOLDER = "highlight";
    public const string CONSTMASTHEADFOLDER = "masthead";
    public const string CONSTADMORDFOLDER = "order";
    public const string CONSTADMDIRFOLDER = "dir";
    public const string CONSTIMAGESFOLDER = "images";
    public const string CONSTIADMPROGGRPFOLDER = "progcat";
    public const string CONSTADMGALLERYFOLDER = "gallery";
    public const string CONSTADMDEALFOLDER = "deal";
    public const string CONSTADMPRODSPECFOLDER = "prodspec";

    public const string CONSTADMHIGHARTFOLDER = "highart";
    public const string CONSTADMHIGHGALLERYFOLDER = "highgallery";
    public const string CONSTHIGHGALLERYFOLDER = "highgallery";
    public const string CONSTADMHIGHVIDEO = "highvideo";
    public const string CONSTADMMERCHANTGALLERYFOLDER = "dirgallery";
    public const string CONSTADMDIRHIGHFOLDER = "dirHighlight";
    public const string CONSTADMDIRHIGHGALLERYFOLDER = "dirHighGallery";

    public const string CONSTADMPROGRAMFOLDER = "program";
    public const string CONSTTHUMBNAILFOLDER = "thumb";
    public const string CONSTINVENTORYFOLDER = "inventory";
    #endregion


    #region "Thickbox"
    public const string CONSTUSRARTICLETHICKBOX = "KeepThis=true&TB_iframe=true&height=400&width=817&modal=false";
    public const string CONSTUSRSALEORDERTHICKBOX = "KeepThis=true&TB_iframe=true&height=400&width=817&modal=false";
    public const string CONSTUSRMASTHEADPREVIEWTHICKBOX = "KeepThis=true&TB_iframe=true&height=400&width=960&modal=false";

    #endregion


    /* Page Size */    
    public const int CONSTADMADBANNERSIZE = 10;
    /* End of Page Size */


    #region "Tags"
    public const string CONSTYEARTAG = "<!YEAR!>";
    public const string CONSTCOMPANYNAMETAG = "<!COMPANYNAME!>";
    public const string CONSTIDTAG = "<!ID!>";
    public const string CONSTID2TAG = "<!ID2!>";
    public const string CONSTEIDTAG = "<!EID!>";
    public const string CONSTGROUPTAG = "<!GROUP!>";
    public const string CONSTTOTALTAG = "<!TOTAL!>";
    public const string CONSTDIRGROUP = "<!DIRGROUP!>";    

    public const string CONSTTAGREDIRECT = "[REDIRECT]";

    #region "HTML Form Replace Tags"
    public const string CONSTHTMLTAGLOGOPATH = "<!INVOICELOGOPATH!>";
    public const string CONSTHTMLTAGCOMPANYNAME = "<!COMPANYNAMESTAG!>";
    public const string CONSTHTMLTAGCOMPANYROC = "<!COMPANYROCTAG!>";
    public const string CONSTHTMLTAGCOMPANYADDRESS = "<!COMPANYADRRESTAG!>";
    public const string CONSTHTMLTAGCOMPANYTEL = "<!COMPANYTELTAG!>";
    public const string CONSTHTMLTAGCOMPANYDETAILEMAIL = "<!COMPANYEMAILTAG!>";
    public const string CONSTHTMLTAGCOMPANYWEBSITE = "<!COMPANYWEBSITETAG !>";
    public const string CONSTHTMLTAGINVOICETYPE = "<!INVOICETYPETAG!>";
    public const string CONSTHTMLTAGCONTACTNO = "<!CONTACTNOTAG!>";
    public const string CONSTHTMLTAGCONTACTNO2 = "<!CONTACTNO2TAG!>";
    public const string CONSTHTMLTAGFAX = "<!FAXTAG!>";
    public const string CONSTHTMLTAGINVOICEDONO = "<!SALESINVOICEDOTAG!>";
    public const string CONSTHTMLTAGSALESNO = "<!SALESNOTAG!>";
    public const string CONSTHTMLTAGINVOICESALESDATE = "<!INVOICESALESDATETAG!>";
    public const string CONSTHTMLTAGDOSALESDATE = "<!DOSALESDATETAG!>";
    public const string CONSTHTMLTAGSALESCUSTOMERNO = "<!SALESCUSTOMERPONOTAG!>";
    public const string CONSTHTMLTAGSALESREMARKS = "<!SALESREMARKSTAG!>";
    public const string CONSTHTMLTAGSALESINVOICEREMARKS = "<!SALESINVOICEREMARKSTAG!>";
    public const string CONSTHTMLTAGSALESINVOICESIGNATURE = "<!SALESINVOICESIGNATURE!>";

    public const string CONSTHTMLTAGSALESCURRENCY = "<!SALESCURRENCYTAG!>";
    public const string CONSTHTMLTAGSALESITEM = "<!SALESITEMTAG!>";
    public const string CONSTHTMLTAGSALESSUBTOTALTEXT = "<!SALESSUBTOTALCURRENCYTAG!>";
    public const string CONSTHTMLTAGSALESSUBTOTAL = "<!SALESSUBTOTALTAG!>";
    public const string CONSTHTMLTAGSALESSHIPPINGTEXT = "<!SALESSHIPPINGCURRENCYTAG!>";
    public const string CONSTHTMLTAGSALESSHIPPING = "<!SALESSHIPPINGTAG!>";
    public const string CONSTHTMLTAGSALESTOTAL = "<!SALESTOTALTAG!>";

    public const string CONSTHTMLTAGCONTACTPERSON = "<!CONTACTPERSONTAG!>";
    public const string CONSTHTMLTAGCOMPANYEMAIL = "<!COMPANYNAMETAG!>";
    public const string CONSTHTMLTAGADDRESS = "<!ADDRESSTAG!>";
    public const string CONSTHTMLTAGPOSCODE = "<!POSCODETAG!>";
    public const string CONSTHTMLTAGCITY = "<!CITY!>";
    public const string CONSTHTMLTAGSTATE = "<!STATETAG!>";
    public const string CONSTHTMLTAGCOUNTRY = "<!COUNTRYTAG!>";
    #endregion
    #endregion


    /* Tags
    

    
    public const string CONSTUSRPASSWORDLENGTHTAG = "<!PASSWORDLENGTH!>";
    public const string CONSTUSRPROFILEPAGETAG = "<!PROFILEPAGE!>";
    public const string CONSTUSRCSSCLASSTAG = "<!CSSCLASS!>";
    public const string CONSTUSRCSSCLASS2TAG = "<!CSSCLASS2!>";
    public const string CONSTUSRURLTAG = "<!URL!>";
    public const string CONSTUSRUSERNAMETAG = "<!USERNAME!>";
    public const string CONSTUSRDATESTARTTAG = "<!DATESTART!>";
    public const string CONSTUSRDATEENDTAG = "<!DATEEND!>";
    public const string CONSTUSRDATETAG = "<!DATE!>";

    public const string CONSTUSRCREATIONTAG = "<!CREATION!>";
    public const string CONSTUSRLASTUPDATEDTAG = "<!LASTUPDATE!>";
    public const string CONSTUSRLASTLOGINTAG = "<!LASTLOGIN!>";

    public const string CONSTUSRSALUTATIONTAG = "<!SALUTATION!>";
    public const string CONSTUSRNAMETAG = "<!NAME!>";
    /* End of Tags */

    #region "Currency"
    public const string CONSTACTIVEYES = "Yes";
    public const string CONSTACTIVENO = "No";
    #endregion

    #region "Participant"
    public const string CONSTADMPARYES = "Yes";
    public const string CONSTADMPARNO = "No";

    public const string CONSTADMGENDERMALE = "Male";
    public const string CONSTADMGENDERFEMALE = "Female";

    #region "Malaysian Type"
    public const int CONSTMALAYSIANYES = 1;
    public const int CONSTMALAYSIANNO = 2;
    #endregion

    #region "Gender"
    public const int CONSTMALE = 1;
    public const int CONSTFEMALE = 2;
    #endregion
    #endregion

    #region "Spec"
    public const int CONSTSPECTOTAL = 5;
    public const string CONSTSPECNAMEDEFAULT = "Specification";
    #endregion
    #endregion


    #region "Properties"
    private int _admId;
    private string _email;
    private string _password;
    private string _name;
    private string _image;
    private int _type;
    private int _active;
    private int _createdBy;
    private DateTime _creation;
    private int _updatedBy;
    private DateTime _lastUpdate;
    private DateTime _lastLogin;

    private int _admAccId;
    private int _pageId;
    private string _pageName;
    private string _pageUrl;

    public const string CONSTRP = "aDm1n";
    #endregion


    #region "Property Methods"
    public int admId
    {
        get { return _admId; }
        set { _admId = value; }
    }

    public string email
    {
        get { return _email; }
        set { _email = value; }
    }

    public string password
    {
        get { return _password; }
        set { _password = value; }
    }

    public string name
    {
        get { return _name; }
        set { _name = value; }
    }
    public string image
    {
        get { return _image; }
        set { _image = value; }
    }
    public int type
    {
        get { return _type; }
        set { _type = value; }
    }

    public int active
    {
        get { return _active; }
        set { _active = value; }
    }

    public DateTime creation
    {
        get { return _creation; }
        set { _creation = value; }
    }

    public int createdBy
    {
        get { return _createdBy; }
        set { _createdBy = value; }
    }

    public DateTime lastUpdate
    {
        get { return _lastUpdate; }
        set { _lastUpdate = value; }
    }

    public int updatedBy
    {
        get { return _updatedBy; }
        set { _updatedBy = value; }
    }

    public DateTime lastLogin
    {
        get { return _lastLogin; }
        set { _lastLogin = value; }
    }

    public int admAccId
    {
        get { return _admAccId; }
        set { _admAccId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public string pageName
    {
        get { return _pageName; }
        set { _pageName = value; }
    }

    public string pageUrl
    {
        get { return _pageUrl; }
        set { _pageUrl = value; }
    }
    #endregion


    #region "Public Methods"
    public clsAdmin()
	{
    }

    public int addAdmin(string strEmail, string strPassword)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ADMIN (" +
                     "ADM_EMAIL, " +
                     "ADM_PWD, " +
                     "ADM_ACTIVE, " +
                     "ADM_CREATION " +
                     ") VALUES " +
                     "(?,?,1,SYSDATE())";
            
            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ADM_EMAIL", OdbcType.VarChar, 250);
            cmd.Parameters["p_ADM_EMAIL"].Value = strEmail;
            cmd.Parameters.Add("p_ADM_PWD", OdbcType.VarChar, 50);
            cmd.Parameters["p_ADM_PWD"].Value = strPassword;

            intRecordAffected = cmd.ExecuteNonQuery();
            
            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                admId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {            
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addAdmin(string strEmail, string strPassword, int intType)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ADMIN (" +
                     "ADM_EMAIL, " +
                     "ADM_PWD, " +
                     "ADM_TYPE, " +
                     "ADM_ACTIVE, " +
                     "ADM_CREATION " +
                     ") VALUES " +
                     "(?,?,?,1,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ADM_EMAIL", OdbcType.VarChar, 250).Value = strEmail;
            cmd.Parameters.Add("p_ADM_PWD", OdbcType.VarChar, 50).Value = strPassword;
            cmd.Parameters.Add("p_ADM_TYPE", OdbcType.Int, 9).Value = intType;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                admId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addAdmin(string strEmail, string strPassword, string strName, int intActive, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ADMIN (" +
                     "ADM_EMAIL, " +
                     "ADM_PWD, " +
                     "ADM_NAME, " +
                     "ADM_ACTIVE, " +
                     "ADM_CREATION, " +
                     "ADM_CREATEDBY " +
                     ") VALUES " +
                     "(?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ADM_EMAIL", OdbcType.VarChar, 250).Value = strEmail;
            cmd.Parameters.Add("p_ADM_PWD", OdbcType.VarChar, 50).Value = strPassword;
            cmd.Parameters.Add("p_ADM_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_ADM_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_ADM_CREATEDBY", OdbcType.BigInt, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                admId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addAdminAccess(int intAdmId, int intPageId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ADMINACCESS (" +
                     "ADM_ID, " +
                     "PAGE_ID" +
                     ") VALUES " +
                     "(?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ADM_ID", OdbcType.BigInt, 9).Value = intAdmId;
            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                admId = intAdmId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isAdminExist(string strEmail)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_ADMIN WHERE ADM_EMAIL = ?";
            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("p_ADM_EMAIL", OdbcType.VarChar, 250).Value = strEmail;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {                
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {            
            closeConn();
        }

        return bExist;
    }

    public Boolean extractAdminById(int intAdmId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_ADMIN WHERE ADM_ID = ?";
            cmd.Parameters.Add("p_ADM_ID", OdbcType.Int, 9).Value = intAdmId;

            if (intActive != 0)
            {
                strSql += " AND ADM_ACTIVE = ?";
                cmd.Parameters.Add("p_ADM_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }            

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                admId = int.Parse(dataSet.Tables[0].Rows[0]["ADM_ID"].ToString());
                email = dataSet.Tables[0].Rows[0]["ADM_EMAIL"].ToString();
                password = dataSet.Tables[0].Rows[0]["ADM_PWD"].ToString();
                name = dataSet.Tables[0].Rows[0]["ADM_NAME"].ToString();
                image = Convert.ToString(dataSet.Tables[0].Rows[0]["ADM_IMAGE"]);
                active = int.Parse(dataSet.Tables[0].Rows[0]["ADM_ACTIVE"].ToString());
                creation = dataSet.Tables[0].Rows[0]["ADM_CREATION"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["ADM_CREATION"].ToString()) : DateTime.MaxValue;
                createdBy = Convert.ToInt16(dataSet.Tables[0].Rows[0]["ADM_CREATEDBY"]);
                lastUpdate = dataSet.Tables[0].Rows[0]["ADM_LASTUPDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["ADM_LASTUPDATE"].ToString()) : DateTime.MaxValue;
                updatedBy = Convert.ToInt16(dataSet.Tables[0].Rows[0]["ADM_UPDATEDBY"]);
                lastLogin = dataSet.Tables[0].Rows[0]["ADM_LASTLOGIN"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["ADM_LASTLOGIN"].ToString()) : DateTime.MaxValue;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public DataSet getAdminList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_ADMIN";

            if (intActive != 0)
            {
                strSql += " WHERE ADM_ACTIVE = ?";
                cmd.Parameters.Add("p_ADM_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getAdminAccessList(int intAdmId)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.ADMACC_ID, A.PAGE_ID, A.ADM_ID, A.V_PAGENAME" +
                     " FROM VW_ADMINACCESS A" +
                     " WHERE A.ADM_ID = ?";

            cmd.Parameters.Add("p_ADM_ID", OdbcType.BigInt, 9).Value = intAdmId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean login(int intActive)
    {
        Boolean bValidLogin = false;

        try 
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_ADMIN WHERE ADM_EMAIL = ? AND ADM_PWD = ?";

            cmd.Parameters.Add("p_ADM_EMAIL", OdbcType.VarChar, 250).Value = email;
            cmd.Parameters.Add("p_ADM_PWD", OdbcType.VarChar, 50).Value = password;

            if (intActive != 0)
            {
                strSql += " AND ADM_ACTIVE = ?";
                cmd.Parameters.Add("p_ADM_ACTIVE", OdbcType.Int, 1).Value = 1;
            }
            
            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                admId = int.Parse(dataSet.Tables[0].Rows[0]["ADM_ID"].ToString());
                type = Convert.ToInt16(dataSet.Tables[0].Rows[0]["ADM_TYPE"]);
                bValidLogin = true;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bValidLogin;
    }

    public int setLastLogin()
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ADMIN SET ADM_LASTLOGIN = SYSDATE() WHERE ADM_ID = ?";
            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_ADM_ID", OdbcType.VarChar, 10).Value = admId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isValidAdmin(int intAdmId, int intPageId)
    {
        Boolean boolValid = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_ADMINACCESS" +
                     " WHERE ADM_ID = ?" +
                     " AND PAGE_ID = ?";

            cmd.Parameters.Add("p_ADM_ID", OdbcType.BigInt, 9).Value = intAdmId;
            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1)
            {
                boolValid = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolValid;
    }

    public Boolean isExactSameData(int intAdmId, string strName, int intActive)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_ADMIN" +
                     " WHERE ADM_ID = ?" +
                     " AND BINARY ADM_NAME = ?" +
                     " AND ADM_ACTIVE = ?";

            cmd.Parameters.Add("p_ADM_ID", OdbcType.BigInt, 9).Value = intAdmId;
            cmd.Parameters.Add("p_ADM_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_ADM_ACTIVE", OdbcType.Int, 1).Value = intActive;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public Boolean isExactSameData2(int intAdmId, int intPageId)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_ADMINACCESS" +
                     " WHERE ADM_ID = ?" +
                     " AND PAGE_ID = ?";

            cmd.Parameters.Add("p_ADM_ID", OdbcType.BigInt, 9).Value = intAdmId;
            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public Boolean isAdminAccessExist(int intAdmId, int intPageId)
    {
        Boolean boolExist = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_ADMINACCESS" +
                     " WHERE ADM_ID = ?" +
                     " AND PAGE_ID = ?";

            cmd.Parameters.Add("p_ADM_ID", OdbcType.BigInt, 9).Value = intAdmId;
            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1)
            {
                boolExist = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public Boolean isAdminTypeValid(string strEmail, string strPassword, int intType)
    {
        Boolean boolValid = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT COUNT(*) FROM TB_ADMIN WHERE ADM_EMAIL = ? AND ADM_PWD = ? AND ADM_TYPE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_ADM_EMAIL", OdbcType.VarChar, 250).Value = strEmail;
            cmd.Parameters.Add("p_ADM_PWD", OdbcType.VarChar, 50).Value = strPassword;
            cmd.Parameters.Add("p_ADM_TYPE", OdbcType.Int, 9).Value = intType;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolValid = true; }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolValid;
    }

    public int updateAdminById(int intAdmId, string strName, int intActive, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ADMIN SET" +
                     " ADM_NAME = ?," +
                     " ADM_ACTIVE = ?" +
                     " WHERE ADM_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_ADM_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_ADM_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_ADM_ID", OdbcType.BigInt, 9).Value = intAdmId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                admId = intAdmId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateAdminPageActive(int intAdmPageId, string strName, int intActive, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ADMINPAGE SET" +
                     " ADMPAGE_ACTIVE = ?" +
                     " WHERE ADMPAGE_ID = ? AND ADMPAGE_NAME = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_ADMPAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_ADMPAGE_ID", OdbcType.BigInt, 9).Value = intAdmPageId;
            cmd.Parameters.Add("p_ADMPAGE_NAME", OdbcType.VarChar, 250).Value = strName;
            

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateAdminPageActive(string url, string name, int active)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ADMINPAGE SET" +
                     " ADMPAGE_ACTIVE = ?" +
                     " WHERE ADMPAGE_URL = ? AND ADMPAGE_NAME = ?";
            
            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_ADMPAGE_ACTIVE", OdbcType.Int, 1).Value = active;
            cmd.Parameters.Add("p_ADMPAGE_URL", OdbcType.VarChar, 250).Value = url;
            cmd.Parameters.Add("p_ADMPAGE_NAME", OdbcType.VarChar, 250).Value = name;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }


    public int updatePasswordById(int intAdmId, string strPassword)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ADMIN SET ADM_PWD = ? WHERE ADM_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_ADM_PWD", OdbcType.VarChar, 50).Value = strPassword;
            cmd.Parameters.Add("p_ADM_ID", OdbcType.BigInt, 9).Value = intAdmId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                admId = intAdmId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updatePasswordByEmail(string strEmail, string strPassword)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ADMIN SET ADM_PWD = ? WHERE ADM_EMAIL = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ADM_PWD", OdbcType.VarChar, 50).Value = strPassword;
            cmd.Parameters.Add("p_ADM_EMAIL", OdbcType.VarChar, 250).Value = strEmail;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int changeAdminPwd(int intAdmId, string strCurrentPwd, string strNewPwd)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ADMIN SET ADM_PWD = ? WHERE ADM_ID = ? AND ADM_PWD = ?";
            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_ADM_PWD", OdbcType.VarChar, 50).Value = strNewPwd;
            cmd.Parameters.Add("p_ADM_ID", OdbcType.BigInt, 9).Value = intAdmId;
            cmd.Parameters.Add("p_ADM_PWD", OdbcType.VarChar, 50).Value = strCurrentPwd;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int changeProfileImage(int intUserID,string strImage)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ADMIN SET ADM_IMAGE = ? WHERE ADM_ID = ?";
            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_ADM_IMAGE", OdbcType.VarChar, 250).Value = strImage;
            cmd.Parameters.Add("p_ADM_ID", OdbcType.BigInt, 9).Value = intUserID;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteAdminAccessById(int intAdmId, int intPageId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_ADMINACCESS WHERE PAGE_ID = ? AND ADM_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;
            cmd.Parameters.Add("p_ADM_ID", OdbcType.BigInt, 9).Value = intAdmId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteAdminAccessByAdmId(int intAdmId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_ADMINACCESS WHERE ADM_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_ADM_ID", OdbcType.BigInt, 9).Value = intAdmId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteAdminById(int intAdmId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_ADMIN WHERE ADM_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_ADM_ID", OdbcType.BigInt, 9).Value = intAdmId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int _r_p(string strEmail, string strPassword)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ADMIN SET ADM_PWD = ? WHERE ADM_EMAIL = ?";
            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_ADM_PWD", OdbcType.VarChar, 50).Value = strPassword;
            cmd.Parameters.Add("p_ADM_EMAIL", OdbcType.VarChar, 250).Value = strEmail;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }


    #region "tb_adminpage"
    public Boolean extractAdminPageByName(string strAdmPageName, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_ADMINPAGE WHERE ADMPAGE_NAME = ?";
            cmd.Parameters.Add("p_ADMPAGE_NAME", OdbcType.VarChar, 250).Value = strAdmPageName;

            if (intActive != 0)
            {
                strSql += " AND ADMPAGE_ACTIVE = ?";
                cmd.Parameters.Add("p_ADMPAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                pageId = int.Parse(dataSet.Tables[0].Rows[0]["ADMPAGE_ID"].ToString());
                pageUrl = dataSet.Tables[0].Rows[0]["ADMPAGE_URL"].ToString();
                active = int.Parse(dataSet.Tables[0].Rows[0]["ADMPAGE_ACTIVE"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractAdminPageByParent(int intParent, int intValue, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_ADMINPAGE" +
                     " WHERE ADMPAGE_PARENT = ?" + 
                     " AND ADMPAGE_VALUE = ?";

            cmd.Parameters.Add("p_ADMPAGE_PARENT", OdbcType.BigInt, 9).Value = intParent;
            cmd.Parameters.Add("p_ADMPAGE_VALUE", OdbcType.Int, 9).Value = intValue;

            if (intActive != 0)
            {
                strSql += " AND ADMPAGE_ACTIVE = ?";
                cmd.Parameters.Add("p_ADMPAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                pageId = int.Parse(dataSet.Tables[0].Rows[0]["ADMPAGE_ID"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public DataSet getAdminPageList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.ADMPAGE_ID, A.ADMPAGE_NAME, A.ADMPAGE_VALUE, A.ADMPAGE_URL, A.ADMPAGE_PARENT, A.ADMPAGE_ORDER, A.ADMPAGE_ACCESS, A.ADMPAGE_MOBILE," +
                     " A.ADMPAGE_SHOWINMENU, A.ADMPAGE_SHOWINTOPMENU, A.ADMPAGE_SHOWINSIDEMENU, A.ADMPAGE_ACTIVE, B.TYPE_ID AS V_TYPE" +
                     " FROM (TB_ADMINPAGE A LEFT JOIN TB_ADMINPAGETYPE B ON (A.ADMPAGE_ID = B.ADMPAGE_ID))" +
                     " WHERE 1 = 1";

            if (intActive != 0)
            {
                strSql += " AND A.ADMPAGE_ACTIVE = ?";

                cmd.Parameters.Add("p_ADMPAGE_ACTIVE", OdbcType.Int, 1).Value = 1;
            }

            strSql += " ORDER BY ADMPAGE_PARENT ASC, ADMPAGE_ORDER ASC";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getAdminPageList2(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.ADMPAGE_ID, A.ADMPAGE_NAME, A.ADMPAGE_VALUE, A.ADMPAGE_URL, B.TYPE_ID AS V_TYPE" +
                     " FROM (TB_ADMINPAGE A LEFT JOIN TB_ADMINPAGETYPE B ON (A.ADMPAGE_ID = B.ADMPAGE_ID))" +
                     " WHERE 1 = 1";

            if (intActive != 0)
            {
                strSql += " AND A.ADMPAGE_ACTIVE = ?";

                cmd.Parameters.Add("p_ADMPAGE_ACTIVE", OdbcType.Int, 1).Value = 1;
            }

            strSql += " ORDER BY ADMPAGE_PARENT ASC, ADMPAGE_ORDER ASC";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getAdminPageList2(string strUrl, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.ADMPAGE_ID, A.ADMPAGE_NAME, A.ADMPAGE_VALUE, A.ADMPAGE_URL, B.TYPE_ID AS V_TYPE" +
                     " FROM (TB_ADMINPAGE A LEFT JOIN TB_ADMINPAGETYPE B ON (A.ADMPAGE_ID = B.ADMPAGE_ID))" +
                     " WHERE 1 = 1" +
                     " AND A.ADMPAGE_URL LIKE ?";

            cmd.Parameters.Add("p_ADMPAGE_URL", OdbcType.VarChar, 1000).Value = "%" + strUrl + "%";

            if (intActive != 0)
            {
                strSql += " AND A.ADMPAGE_ACTIVE = ?";

                cmd.Parameters.Add("p_ADMPAGE_ACTIVE", OdbcType.Int, 1).Value = 1;
            }

            strSql += " ORDER BY ADMPAGE_PARENT ASC, ADMPAGE_ORDER ASC";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public int activeAdminPageById(int intId,int intActive)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ADMINPAGE SET" +
                     " ADMPAGE_ACTIVE = ?" +
                     " WHERE ADMPAGE_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("p_ADMPAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_ADMPAGE_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected > 0)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion


    #endregion
}
