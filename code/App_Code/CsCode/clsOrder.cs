﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsOrder
/// </summary>
public class clsOrder : dbconnBase
{
    #region "Properties"
    private int _memId;
    private int _ordId;
    private string _memCode;
    private string _memName;
    private string _memEmail;
    private int _memType;
    private string _ordNo;
    private DateTime _ordCreation;
    private string _customerNo;
    private DateTime _ordDueDate;
    private string _ordRemarks;
    private int _ordRead;
    private int _ordType;
    private string _ordTypeName;
    private int _ordStatusId;
    private int _ordStatus;
    private string _ordStatusName;
    private DateTime _ordStatusDate;
    private int _ordStatusActive;
    private int _ordBillShipSame;
    private string _ordFrom;
    private string _ordTo;
    private string _ordMessage;
    private string _ordMsgInstruction;
    private string _ordMsgBuyer;
    private string _company;
    private string _name;
    private string _add;
    private string _poscode;
    private string _city;
    private string _cityOther;
    private string _country;
    private string _countryOther;
    private string _countryName;
    private string _state;
    private string _stateOther;
    private string _contactNo;
    private string _contactNo2;
    private string _fax;
    private string _email;

    private string _shippingCustCompany;
    private string _shippingName;
    private string _shippingAdd;
    private string _shippingPoscode;
    private string _shippingCity;
    private string _shippingCityOther;
    private string _shippingCountry;
    private string _shippingCountryOther;
    private string _shippingCountryName;
    private string _shippingState;
    private string _shippingStateOther;
    private string _shippingContactNo;
    private string _shippingContactNo2;
    private string _shippingFax;
    private string _shippingEmail;
    private string _shippingRemarks;
    private DateTime _shippingDate;
    private string _shippingTrackCode;
    private int _shippingCompany;
    private string _shippingCompanyName;
    private int _pickupPoint;
    private string _pickupPointName;
    private DateTime _shippingCollectionDate;

    private DateTime _deliveryDate;
    private string _deliveryRecipient;
    private string _deliverySender;
    private string _deliveryMessage;
    private string _deliveryTime;

    private int _ordShippingCheck;
    private decimal _ordShipping;
    private int _ordDlvService;
    private int _ordDone;
    private int _ordCancel;
    private int _ordActive;
    private int _ordUpdatedBy;
    private DateTime _ordLastUpdate;

    private int _ordPaymentId;
    private int _ordPayGatewayId;
    private string _ordPayGateway;
    private string _ordPayRemarks;
    private DateTime _ordPayment;
    private decimal _ordPayTotal;
    private int _ordPayBank;
    private string _ordPayBankName;
    private string _ordPayBankAccount;
    private string _ordPayTransId;
    private DateTime _ordPayBankDate;
    private int _ordPayReject;
    private string _ordPayRejectRemarks;

    private int _ordTotalItem;
    private int _ordTotalWeight;
    private decimal _ordSubtotal;
    private decimal _ordTotal;
    private int _ordDetailId;
    private string _couponCode;
    private string _disOperator;
    private decimal _disValue;
    private decimal _ordConvRate;
    private string _ordConvUnit;
    private int _ordGST;
    private decimal _ordTotalGST;
    #endregion

    #region "Property Methods"
    public int ordId
    {
        get { return _ordId; }
        set { _ordId = value; }
    }

    public int memId
    {
        get { return _memId; }
        set { _memId = value; }
    }

    public string memCode
    {
        get { return _memCode; }
        set { _memCode = value; }
    }

    public string memName
    {
        get { return _memName; }
        set { _memName = value; }
    }

    public string memEmail
    {
        get { return _memEmail; }
        set { _memEmail = value; }
    }

    public int memType
    {
        get { return _memType; }
        set { _memType = value; }
    }

    public string ordNo
    {
        get { return _ordNo; }
        set { _ordNo = value; }
    }

    public DateTime ordCreation
    {
        get { return _ordCreation; }
        set { _ordCreation = value; }
    }

    public string customerNo
    {
        get { return _customerNo; }
        set { _customerNo = value; }
    }

    public DateTime ordDueDate
    {
        get { return _ordDueDate; }
        set { _ordDueDate = value; }
    }

    public string ordRemarks
    {
        get { return _ordRemarks; }
        set { _ordRemarks = value; }
    }

    public int ordRead
    {
        get { return _ordRead; }
        set { _ordRead = value; }
    }

    public int ordType
    {
        get { return _ordType; }
        set { _ordType = value; }
    }

    public string ordTypeName
    {
        get { return _ordTypeName; }
        set { _ordTypeName = value; }
    }

    public int ordStatusId
    {
        get { return _ordStatusId; }
        set { _ordStatusId = value; }
    }

    public int ordStatus
    {
        get { return _ordStatus; }
        set { _ordStatus = value; }
    }

    public string ordStatusName
    {
        get { return _ordStatusName; }
        set { _ordStatusName = value; }
    }

    public DateTime ordStatusDate
    {
        get { return _ordStatusDate; }
        set { _ordStatusDate = value; }
    }

    public int ordStatusActive
    {
        get { return _ordStatusActive; }
        set { _ordStatusActive = value; }
    }

    public int ordBillShipSame
    {
        get { return _ordBillShipSame; }
        set { _ordBillShipSame = value; }
    }

    public string ordFrom
    {
        get { return _ordFrom; }
        set { _ordFrom = value; }
    }

    public string ordTo
    {
        get { return _ordTo; }
        set { _ordTo = value; }
    }
    public string ordMessage
    {
        get { return _ordMessage; }
        set { _ordMessage = value; }
    }

    public string ordMsgInstruction
    {
        get { return _ordMsgInstruction; }
        set { _ordMsgInstruction = value; }
    }
    public string ordMsgBuyer
    {
        get { return _ordMsgBuyer; }
        set { _ordMsgBuyer = value; }
    }
    public string company
    {
        get { return _company; }
        set { _company = value; }
    }

    public string name
    {
        get { return _name; }
        set { _name = value; }
    }

    public string add
    {
        get { return _add; }
        set { _add = value; }
    }

    public string poscode
    {
        get { return _poscode; }
        set { _poscode = value; }
    }

    public string city
    {
        get { return _city; }
        set { _city = value; }
    }

    public string cityOther
    {
        get { return _cityOther; }
        set { _cityOther = value; }
    }

    public string country
    {
        get { return _country; }
        set { _country = value; }
    }

    public string countryOther
    {
        get { return _countryOther; }
        set { _countryOther = value; }
    }

    public string countryName
    {
        get { return _countryName; }
        set { _countryName = value; }
    }

    public string state
    {
        get { return _state; }
        set { _state = value; }
    }

    public string stateOther
    {
        get { return _stateOther; }
        set { _stateOther = value; }
    }

    public string contactNo
    {
        get { return _contactNo; }
        set { _contactNo = value; }
    }

    public string contactNo2
    {
        get { return _contactNo2; }
        set { _contactNo2 = value; }
    }

    public string fax
    {
        get { return _fax; }
        set { _fax = value; }
    }

    public string email
    {
        get { return _email; }
        set { _email = value; }
    }

    public string shippingCustCompany
    {
        get { return _shippingCustCompany; }
        set { _shippingCustCompany = value; }
    }

    public string shippingName
    {
        get { return _shippingName; }
        set { _shippingName = value; }
    }

    public string shippingAdd
    {
        get { return _shippingAdd; }
        set { _shippingAdd = value; }
    }

    public string shippingPoscode
    {
        get { return _shippingPoscode; }
        set { _shippingPoscode = value; }
    }

    public string shippingCity
    {
        get { return _shippingCity; }
        set { _shippingCity = value; }
    }

    public string shippingCityOther
    {
        get { return _shippingCityOther; }
        set { _shippingCityOther = value; }
    }

    public string shippingCountry
    {
        get { return _shippingCountry; }
        set { _shippingCountry = value; }
    }

    public string shippingCountryOther
    {
        get { return _shippingCountryOther; }
        set { _shippingCountryOther = value; }
    }

    public string shippingCountryName
    {
        get { return _shippingCountryName; }
        set { _shippingCountryName = value; }
    }

    public string shippingState
    {
        get { return _shippingState; }
        set { _shippingState = value; }
    }

    public string shippingStateOther
    {
        get { return _shippingStateOther; }
        set { _shippingStateOther = value; }
    }

    public string shippingContactNo
    {
        get { return _shippingContactNo; }
        set { _shippingContactNo = value; }
    }

    public string shippingContactNo2
    {
        get { return _shippingContactNo2; }
        set { _shippingContactNo2 = value; }
    }

    public string shippingFax
    {
        get { return _shippingFax; }
        set { _shippingFax = value; }
    }

    public string shippingEmail
    {
        get { return _shippingEmail; }
        set { _shippingEmail = value; }
    }

    public string shippingRemarks
    {
        get { return _shippingRemarks; }
        set { _shippingRemarks = value; }
    }

    public DateTime shippingDate
    {
        get { return _shippingDate; }
        set { _shippingDate = value; }
    }

    public string shippingTrackCode
    {
        get { return _shippingTrackCode; }
        set { _shippingTrackCode = value; }
    }

    public int shippingCompany
    {
        get { return _shippingCompany; }
        set { _shippingCompany = value; }
    }

    public string shippingCompanyName
    {
        get { return _shippingCompanyName; }
        set { _shippingCompanyName = value; }
    }

    public DateTime deliveryDate
    {
        get { return _deliveryDate; }
        set { _deliveryDate = value; }
    }
    public string deliveryTime
    {
        get { return _deliveryTime; }
        set { _deliveryTime = value; }
    }
    public string deliveryRecipient
    {
        get { return _deliveryRecipient; }
        set { _deliveryRecipient = value; }
    }

    public string deliverySender
    {
        get { return _deliverySender; }
        set { _deliverySender = value; }
    }

    public string deliveryMessage
    {
        get { return _deliveryMessage; }
        set { _deliveryMessage = value; }
    }

    public int pickupPoint
    {
        get { return _pickupPoint; }
        set { _pickupPoint = value; }
    }

    public string pickupPointName
    {
        get { return _pickupPointName; }
        set { _pickupPointName = value; }
    }

    public DateTime shippingCollectionDate
    {
        get { return _shippingCollectionDate; }
        set { _shippingCollectionDate = value; }
    }

    public int ordShippingCheck
    {
        get { return _ordShippingCheck; }
        set { _ordShippingCheck = value; }
    }

    public decimal ordShipping
    {
        get { return _ordShipping; }
        set { _ordShipping = value; }
    }

    public int ordPaymentId
    {
        get { return _ordPaymentId; }
        set { _ordPaymentId = value; }
    }

    public int ordPayGatewayId
    {
        get { return _ordPayGatewayId; }
        set { _ordPayGatewayId = value; }
    }

    public string ordPayGateway
    {
        get { return _ordPayGateway; }
        set { _ordPayGateway = value; }
    }

    public string ordPayRemarks
    {
        get { return _ordPayRemarks; }
        set { _ordPayRemarks = value; }
    }

    public DateTime ordPayment
    {
        get { return _ordPayment; }
        set { _ordPayment = value; }
    }

    public decimal ordPayTotal
    {
        get { return _ordPayTotal; }
        set { _ordPayTotal = value; }
    }

    public int ordPayBank
    {
        get { return _ordPayBank; }
        set { _ordPayBank = value; }
    }

    public string ordPayBankName
    {
        get { return _ordPayBankName; }
        set { _ordPayBankName = value; }
    }

    public string ordPayBankAccount
    {
        get { return _ordPayBankAccount; }
        set { _ordPayBankAccount = value; }
    }

    public string ordPayTransId
    {
        get { return _ordPayTransId; }
        set { _ordPayTransId = value; }
    }

    public DateTime ordPayBankDate
    {
        get { return _ordPayBankDate; }
        set { _ordPayBankDate = value; }
    }

    public int ordPayReject
    {
        get { return _ordPayReject; }
        set { _ordPayReject = value; }
    }

    public string ordPayRejectRemarks
    {
        get { return _ordPayRejectRemarks; }
        set { _ordPayRejectRemarks = value; }
    }

    public int ordDlvService
    {
        get { return _ordDlvService; }
        set { _ordDlvService = value; }
    }

    public int ordDone
    {
        get { return _ordDone; }
        set { _ordDone = value; }
    }

    public int ordCancel
    {
        get { return _ordCancel; }
        set { _ordCancel = value; }
    }

    public int ordActive
    {
        get { return _ordActive; }
        set { _ordActive = value; }
    }

    public int ordUpdatedBy
    {
        get { return _ordUpdatedBy; }
        set { _ordUpdatedBy = value; }
    }

    public DateTime ordLastUpdate
    {
        get { return _ordLastUpdate; }
        set { _ordLastUpdate = value; }
    }

    public int ordTotalItem
    {
        get { return _ordTotalItem; }
        set { _ordTotalItem = value; }
    }

    public int ordTotalWeight
    {
        get { return _ordTotalWeight; }
        set { _ordTotalWeight = value; }
    }

    public decimal ordSubtotal
    {
        get { return _ordSubtotal; }
        set { _ordSubtotal = value; }
    }

    public decimal ordTotal
    {
        get { return _ordTotal; }
        set { _ordTotal = value; }
    }

    public int ordDetailId
    {
        get { return _ordDetailId; }
        set { _ordDetailId = value; }
    }

    public string couponCode
    {
        get { return _couponCode; }
        set { _couponCode = value; }
    }

    public string disOperator
    {
        get { return _disOperator; }
        set { _disOperator = value; }
    }

    public decimal disValue
    {
        get { return _disValue; }
        set { _disValue = value; }
    }

    public decimal ordConvRate
    {
        get { return _ordConvRate; }
        set { _ordConvRate = value; }
    }

    public string ordConvUnit
    {
        get { return _ordConvUnit; }
        set { _ordConvUnit = value; }
    }

    public int ordGST
    {
        get { return _ordGST; }
        set { _ordGST = value; }
    }

    public decimal ordTotalGST
    {
        get { return _ordTotalGST; }
        set { _ordTotalGST = value; }
    }
    #endregion

    #region "Public Methods"
    public clsOrder()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    public int addOrderDelivery(int intMemId, string strOrdNo, string strName, string strAdd, string strPoscode, string strCity, string strState, string strCountry, string strContactNo, string strShippingName, string strShippingAdd, string strShippingPoscode, string strShippingCity, string strShippingState, string strShippingCountry, string strShippingContactNo, string strPickupPointName)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ORDER (" +
                     "MEM_ID, " +
                     "ORDER_NO, " +
                     "ORDER_CREATION, " +
                     "ORDER_NAME, " +
                     "ORDER_ADD, " +
                     "ORDER_POSCODE, " +
                     "ORDER_CITY, " +
                     "ORDER_STATE, " +
                     "ORDER_COUNTRY, " +
                     "ORDER_CONTACTNO, " +
                     "SHIPPING_NAME, " +
                     "SHIPPING_ADD, " +
                     "SHIPPING_POSCODE, " +
                     "SHIPPING_CITY, " +
                     "SHIPPING_STATE, " +
                     "SHIPPING_COUNTRY, " +
                     "SHIPPING_CONTACTNO, " +
                     "ORDER_PICKUPPOINTNAME" +
                     ") VALUES " +
                     "(?,?,SYSDATE(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_MEM_ID", OdbcType.Int, 9).Value = intMemId;
            cmd.Parameters.Add("p_ORDER_NO", OdbcType.VarChar, 20).Value = strOrdNo;
            cmd.Parameters.Add("p_ORDER_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_ORDER_ADD", OdbcType.VarChar, 250).Value = strAdd;
            cmd.Parameters.Add("p_ORDER_POSCODE", OdbcType.VarChar, 20).Value = strPoscode;
            cmd.Parameters.Add("p_ORDER_CITY", OdbcType.VarChar, 50).Value = strCity;
            cmd.Parameters.Add("p_ORDER_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_ORDER_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_ORDER_CONTACTNO", OdbcType.VarChar, 20).Value = strContactNo;
            cmd.Parameters.Add("p_SHIPPING_NAME", OdbcType.VarChar, 250).Value = strShippingName;
            cmd.Parameters.Add("p_SHIPPING_ADD", OdbcType.VarChar, 250).Value = strShippingAdd;
            cmd.Parameters.Add("p_SHIPPING_POSCODE", OdbcType.VarChar, 20).Value = strShippingPoscode;
            cmd.Parameters.Add("p_SHIPPING_CITY", OdbcType.VarChar, 50).Value = strShippingCity;
            cmd.Parameters.Add("p_SHIPPING_STATE", OdbcType.VarChar, 250).Value = strShippingState;
            cmd.Parameters.Add("p_SHIPPING_COUNTRY", OdbcType.VarChar, 10).Value = strShippingCountry;
            cmd.Parameters.Add("p_SHIPPING_CONTACTNO", OdbcType.VarChar, 20).Value = strShippingContactNo;
            cmd.Parameters.Add("p_ORDER_PICKUPPOINTNAME", OdbcType.VarChar, 100).Value = strPickupPointName;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                ordId = int.Parse(cmd2.ExecuteScalar().ToString());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addOrderDelivery(int intMemId, string strMemCode, string strMemName, string strMemEmail, string strOrdNo, string strName, string strAdd, string strPoscode, string strCity, string strState, string strCountry, string strContactNo, string strShippingName, string strShippingAdd, string strShippingPoscode, string strShippingCity, string strShippingState, string strShippingCountry, string strShippingContactNo, string strPickupPointName)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ORDER (" +
                     "MEM_ID, " +
                     "MEM_CODE, " +
                     "MEM_NAME, " +
                     "MEM_EMAIL, " +
                     "ORDER_NO, " +
                     "ORDER_CREATION, " +
                     "ORDER_NAME, " +
                     "ORDER_ADD, " +
                     "ORDER_POSCODE, " +
                     "ORDER_CITY, " +
                     "ORDER_STATE, " +
                     "ORDER_COUNTRY, " +
                     "ORDER_CONTACTNO, " +
                     "SHIPPING_NAME, " +
                     "SHIPPING_ADD, " +
                     "SHIPPING_POSCODE, " +
                     "SHIPPING_CITY, " +
                     "SHIPPING_STATE, " +
                     "SHIPPING_COUNTRY, " +
                     "SHIPPING_CONTACTNO, " +
                     "ORDER_PICKUPPOINTNAME" +
                     ") VALUES " +
                     "(?,?,?,?,?,SYSDATE(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_MEM_ID", OdbcType.Int, 9).Value = intMemId;
            cmd.Parameters.Add("p_MEM_CODE", OdbcType.VarChar, 20).Value = strMemCode;
            cmd.Parameters.Add("p_MEM_NAME", OdbcType.VarChar, 250).Value = strMemName;
            cmd.Parameters.Add("p_MEM_EMAIL", OdbcType.VarChar, 250).Value = strMemEmail;
            cmd.Parameters.Add("p_ORDER_NO", OdbcType.VarChar, 20).Value = strOrdNo;
            cmd.Parameters.Add("p_ORDER_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_ORDER_ADD", OdbcType.VarChar, 250).Value = strAdd;
            cmd.Parameters.Add("p_ORDER_POSCODE", OdbcType.VarChar, 20).Value = strPoscode;
            cmd.Parameters.Add("p_ORDER_CITY", OdbcType.VarChar, 50).Value = strCity;
            cmd.Parameters.Add("p_ORDER_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_ORDER_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_ORDER_CONTACTNO", OdbcType.VarChar, 20).Value = strContactNo;
            cmd.Parameters.Add("p_SHIPPING_NAME", OdbcType.VarChar, 250).Value = strShippingName;
            cmd.Parameters.Add("p_SHIPPING_ADD", OdbcType.VarChar, 250).Value = strShippingAdd;
            cmd.Parameters.Add("p_SHIPPING_POSCODE", OdbcType.VarChar, 20).Value = strShippingPoscode;
            cmd.Parameters.Add("p_SHIPPING_CITY", OdbcType.VarChar, 50).Value = strShippingCity;
            cmd.Parameters.Add("p_SHIPPING_STATE", OdbcType.VarChar, 250).Value = strShippingState;
            cmd.Parameters.Add("p_SHIPPING_COUNTRY", OdbcType.VarChar, 10).Value = strShippingCountry;
            cmd.Parameters.Add("p_SHIPPING_CONTACTNO", OdbcType.VarChar, 20).Value = strShippingContactNo;
            cmd.Parameters.Add("p_ORDER_PICKUPPOINTNAME", OdbcType.VarChar, 100).Value = strPickupPointName;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                ordId = int.Parse(cmd2.ExecuteScalar().ToString());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addOrderDelivery2(int intMemId, string strOrdNo, string strName, string strAdd, string strPoscode, string strCity, string strState, string strCountry, string strContactNo, string strShippingName, string strShippingAdd, string strShippingPoscode, string strShippingCity, string strShippingState, string strShippingCountry, string strShippingContactNo, int intPickupPoint, string strPickupPointName)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ORDER (" +
                     "MEM_ID, " +
                     "ORDER_NO, " +
                     "ORDER_CREATION, " +
                     "ORDER_NAME, " +
                     "ORDER_ADD, " +
                     "ORDER_POSCODE, " +
                     "ORDER_CITY, " +
                     "ORDER_STATE, " +
                     "ORDER_COUNTRY, " +
                     "ORDER_CONTACTNO, " +
                     "SHIPPING_NAME, " +
                     "SHIPPING_ADD, " +
                     "SHIPPING_POSCODE, " +
                     "SHIPPING_CITY, " +
                     "SHIPPING_STATE, " +
                     "SHIPPING_COUNTRY, " +
                     "SHIPPING_CONTACTNO, " +
                     "ORDER_PICKUPPOINT, " +
                     "ORDER_PICKUPPOINTNAME" +
                     ") VALUES " +
                     "(?,?,SYSDATE(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_MEM_ID", OdbcType.Int, 9).Value = intMemId;
            cmd.Parameters.Add("p_ORDER_NO", OdbcType.VarChar, 20).Value = strOrdNo;
            cmd.Parameters.Add("p_ORDER_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_ORDER_ADD", OdbcType.VarChar, 250).Value = strAdd;
            cmd.Parameters.Add("p_ORDER_POSCODE", OdbcType.VarChar, 20).Value = strPoscode;
            cmd.Parameters.Add("p_ORDER_CITY", OdbcType.VarChar, 50).Value = strCity;
            cmd.Parameters.Add("p_ORDER_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_ORDER_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_ORDER_CONTACTNO", OdbcType.VarChar, 20).Value = strContactNo;
            cmd.Parameters.Add("p_SHIPPING_NAME", OdbcType.VarChar, 250).Value = strShippingName;
            cmd.Parameters.Add("p_SHIPPING_ADD", OdbcType.VarChar, 250).Value = strShippingAdd;
            cmd.Parameters.Add("p_SHIPPING_POSCODE", OdbcType.VarChar, 20).Value = strShippingPoscode;
            cmd.Parameters.Add("p_SHIPPING_CITY", OdbcType.VarChar, 50).Value = strShippingCity;
            cmd.Parameters.Add("p_SHIPPING_STATE", OdbcType.VarChar, 250).Value = strShippingState;
            cmd.Parameters.Add("p_SHIPPING_COUNTRY", OdbcType.VarChar, 10).Value = strShippingCountry;
            cmd.Parameters.Add("p_SHIPPING_CONTACTNO", OdbcType.VarChar, 20).Value = strShippingContactNo;
            cmd.Parameters.Add("p_ORDER_PICKUPPOINT", OdbcType.Int, 1).Value = intPickupPoint;
            cmd.Parameters.Add("p_ORDER_PICKUPPOINTNAME", OdbcType.VarChar, 100).Value = strPickupPointName;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                ordId = int.Parse(cmd2.ExecuteScalar().ToString());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addOrderDelivery2(int intMemId, string strMemCode, string strMemName, string strMemEmail, string strOrdNo, string strName, string strAdd, string strPoscode, string strCity, string strState, string strCountry, string strContactNo, string strShippingName, string strShippingAdd, string strShippingPoscode, string strShippingCity, string strShippingState, string strShippingCountry, string strShippingContactNo, int intPickupPoint, string strPickupPointName)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ORDER (" +
                     "MEM_ID, " +
                     "MEM_CODE, " +
                     "MEM_NAME, " +
                     "MEM_EMAIL, " +
                     "ORDER_NO, " +
                     "ORDER_CREATION, " +
                     "ORDER_NAME, " +
                     "ORDER_ADD, " +
                     "ORDER_POSCODE, " +
                     "ORDER_CITY, " +
                     "ORDER_STATE, " +
                     "ORDER_COUNTRY, " +
                     "ORDER_CONTACTNO, " +
                     "SHIPPING_NAME, " +
                     "SHIPPING_ADD, " +
                     "SHIPPING_POSCODE, " +
                     "SHIPPING_CITY, " +
                     "SHIPPING_STATE, " +
                     "SHIPPING_COUNTRY, " +
                     "SHIPPING_CONTACTNO, " +
                     "ORDER_PICKUPPOINT, " +
                     "ORDER_PICKUPPOINTNAME" +
                     ") VALUES " +
                     "(?,?,?,?,?,SYSDATE(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_MEM_ID", OdbcType.Int, 9).Value = intMemId;
            cmd.Parameters.Add("p_MEM_CODE", OdbcType.VarChar, 20).Value = strMemCode;
            cmd.Parameters.Add("p_MEM_NAME", OdbcType.VarChar, 250).Value = strMemName;
            cmd.Parameters.Add("p_MEM_EMAIL", OdbcType.VarChar, 250).Value = strMemEmail;
            cmd.Parameters.Add("p_ORDER_NO", OdbcType.VarChar, 20).Value = strOrdNo;
            cmd.Parameters.Add("p_ORDER_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_ORDER_ADD", OdbcType.VarChar, 250).Value = strAdd;
            cmd.Parameters.Add("p_ORDER_POSCODE", OdbcType.VarChar, 20).Value = strPoscode;
            cmd.Parameters.Add("p_ORDER_CITY", OdbcType.VarChar, 50).Value = strCity;
            cmd.Parameters.Add("p_ORDER_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_ORDER_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_ORDER_CONTACTNO", OdbcType.VarChar, 20).Value = strContactNo;
            cmd.Parameters.Add("p_SHIPPING_NAME", OdbcType.VarChar, 250).Value = strShippingName;
            cmd.Parameters.Add("p_SHIPPING_ADD", OdbcType.VarChar, 250).Value = strShippingAdd;
            cmd.Parameters.Add("p_SHIPPING_POSCODE", OdbcType.VarChar, 20).Value = strShippingPoscode;
            cmd.Parameters.Add("p_SHIPPING_CITY", OdbcType.VarChar, 50).Value = strShippingCity;
            cmd.Parameters.Add("p_SHIPPING_STATE", OdbcType.VarChar, 250).Value = strShippingState;
            cmd.Parameters.Add("p_SHIPPING_COUNTRY", OdbcType.VarChar, 10).Value = strShippingCountry;
            cmd.Parameters.Add("p_SHIPPING_CONTACTNO", OdbcType.VarChar, 20).Value = strShippingContactNo;
            cmd.Parameters.Add("p_ORDER_PICKUPPOINT", OdbcType.Int, 1).Value = intPickupPoint;
            cmd.Parameters.Add("p_ORDER_PICKUPPOINTNAME", OdbcType.VarChar, 100).Value = strPickupPointName;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                ordId = int.Parse(cmd2.ExecuteScalar().ToString());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addOrderItems(int intOrdId, int intProdId, string strProdCode, string strProdName, string strProdDName, string strProdImage, decimal decProdPrice, int intProdQty, string strProdUOM, int intProdWeight, int intProdLength, int intProdWidth, int intProdHeight, decimal decProdTotal, string strProdCat1, string strProdCat2)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ORDERDETAIL (" +
                     "ORDER_ID, " +
                     "ORDDETAIL_PRODID, " +
                     "ORDDETAIL_PRODCODE, " +
                     "ORDDETAIL_PRODNAME, " +
                     "ORDDETAIL_PRODDNAME, " +
                     "ORDDETAIL_PRODUOM, " +
                     "ORDDETAIL_PRODWEIGHT, " +
                     "ORDDETAIL_PRODLENGTH, " +
                     "ORDDETAIL_PRODWIDTH, " +
                     "ORDDETAIL_PRODHEIGHT, " +
                     "ORDDETAIL_PRODIMAGE, " +
                     "ORDDETAIL_PRODPRICE, " +
                     "ORDDETAIL_PRODQTY, " +
                     "ORDDETAIL_PRODTOTAL, " +
                     "ORDDETAIL_PRODCAT1," +
                     "ORDDETAIL_PRODCAT2" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;
            cmd.Parameters.Add("p_ORDDETAIL_PRODID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_ORDDETAIL_PRODCODE", OdbcType.VarChar, 20).Value = strProdCode;
            cmd.Parameters.Add("p_ORDDETAIL_PRODNAME", OdbcType.VarChar, 250).Value = strProdName;
            cmd.Parameters.Add("p_ORDDETAIL_PRODDNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_ORDDETAIL_PRODUOM", OdbcType.VarChar, 20).Value = strProdUOM;
            cmd.Parameters.Add("p_ORDDETAIL_PRODWEIGHT", OdbcType.Int, 9).Value = intProdWeight;
            cmd.Parameters.Add("p_ORDDETAIL_PRODLENGTH", OdbcType.Int, 9).Value = intProdLength;
            cmd.Parameters.Add("p_ORDDETAIL_PRODWIDTH", OdbcType.Int, 9).Value = intProdWidth;
            cmd.Parameters.Add("p_ORDDETAIL_PRODHEIGHT", OdbcType.Int, 9).Value = intProdHeight;
            cmd.Parameters.Add("p_ORDDETAIL_PRODIMAGE", OdbcType.VarChar, 250).Value = strProdImage;
            cmd.Parameters.Add("p_ORDDETAIL_PRODPRICE", OdbcType.Decimal).Value = decProdPrice;
            cmd.Parameters.Add("p_ORDDETAIL_PRODQTY", OdbcType.Int, 9).Value = intProdQty;
            cmd.Parameters.Add("p_ORDDETAIL_PRODTOTAL", OdbcType.Decimal).Value = decProdTotal;
            cmd.Parameters.Add("p_ORDDETAIL_PRODCAT1", OdbcType.VarChar, 250).Value = strProdCat1;
            cmd.Parameters.Add("p_ORDDETAIL_PRODCAT2", OdbcType.VarChar, 250).Value = strProdCat2;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int generateSequenceNo()
    {
        int intRecordAffected = 0;
        int intSequence = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_SEQUENCE (SEQ_DATEGET) VALUES (SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                intSequence = int.Parse(cmd2.ExecuteScalar().ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intSequence;
    }

    public Boolean extractOrderById(int intOrdId)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            //strSql = "SELECT A.ORDER_ID, A.V_MEM, A.V_MEMCODE, A.V_MEMNAME, A.V_MEMEMAIL, A.ORDER_NO, A.ORDER_CREATION, A.ORDER_DUEDATE, A.ORDER_REMARKS, " +
            //         "A.ORDER_READ, A.V_ORDERTYPE, A.V_ORDERTYPENAME, A.ORDER_STATUSID, A.ORDER_STATUS, A.V_ORDERSTATUS, A.V_ORDERSTATUSNAME, A.V_STATUSDATE, " +
            //         "A.ORDER_NAME, A.ORDER_ADD, A.ORDER_POSCODE, A.ORDER_CITY, A.ORDER_STATE, A.ORDER_COUNTRY, A.V_COUNTRYNAME, A.ORDER_CONTACTNO, A.ORDER_EMAIL, " +
            //         "A.SHIPPING_NAME, A.SHIPPING_ADD, A.SHIPPING_POSCODE, A.SHIPPING_CITY, A.SHIPPING_STATE, A.SHIPPING_COUNTRY, A.V_SHIPPINGCOUNTRYNAME, A.SHIPPING_CONTACTNO, A.SHIPPING_EMAIL, " +
            //         "A.SHIPPING_REMARKS, A.SHIPPING_TRACKCODE, A.SHIPPING_COMPANY, A.SHIPPING_COMPANYNAME, A.ORDER_PICKUPPOINT, A.ORDER_PICKUPPOINTNAME, A.ORDER_SHIPPING, " +
            //         "A.ORDER_PAYMENT, A.PAYMENT_GATEWAY, A.PAYMENT_GATEWAYNAME, A.PAYMENT_REMARKS, A.PAYMENT_DATE, A.PAYMENT_TOTAL, " +
            //         "A.PAYMENT_BANK, A.PAYMENT_BANKNAME, A.PAYMENT_BANKACCOUNT, A.PAYMENT_TRANSID, A.PAYMENT_BANKDATE, A.PAYMENT_REJECT, A.PAYMENT_REJECTREMARKS, " +
            //         "A.ORDER_DONE, A.ORDER_CANCEL, A.ORDER_ACTIVE, A.ORDER_UPDATEDBY, A.ORDER_LASTUPDATE, A.ORDER_TOTALITEM, A.ORDER_TOTALWEIGHT, A.ORDER_SUBTOTAL, A.ORDER_TOTAL, A.ORDER_GST, A.ORDER_TOTALGST " +
            //         "FROM VW_ORDER A " +
            //         "WHERE A.ORDER_ID = ?";

            strSql = "SELECT Z.ORDER_ID, Z.V_MEM, Z.V_MEMCODE, Z.V_MEMNAME, Z.V_MEMEMAIL, Z.ORDER_NO, Z.ORDER_CREATION, Z.ORDER_DUEDATE, Z.ORDER_REMARKS, " +
                     "Z.ORDER_READ, Z.V_ORDERTYPE, Z.V_ORDERTYPENAME, Z.ORDER_STATUSID, Z.ORDER_STATUS, Z.V_ORDERSTATUS, Z.V_ORDERSTATUSNAME, Z.V_STATUSDATE,  " + 
                     "Z.ORDER_NAME, Z.ORDER_ADD, Z.ORDER_POSCODE, Z.ORDER_CITY, Z.ORDER_STATE, Z.ORDER_COUNTRY, Z.V_COUNTRYNAME, Z.ORDER_CONTACTNO, Z.ORDER_EMAIL, " +
                     "Z.SHIPPING_NAME, Z.SHIPPING_ADD, Z.SHIPPING_POSCODE, Z.SHIPPING_CITY, Z.SHIPPING_STATE, Z.SHIPPING_COUNTRY, Z.V_SHIPPINGCOUNTRYNAME, Z.SHIPPING_CONTACTNO, Z.SHIPPING_EMAIL,   " +
                     "Z.SHIPPING_REMARKS, Z.SHIPPING_TRACKCODE, Z.SHIPPING_COMPANY, Z.SHIPPING_COMPANYNAME, Z.ORDER_PICKUPPOINT, Z.ORDER_PICKUPPOINTNAME, Z.ORDER_SHIPPING,   " +
                     "Z.ORDER_PAYMENT, Z.PAYMENT_GATEWAY, Z.PAYMENT_GATEWAYNAME, Z.PAYMENT_REMARKS, Z.PAYMENT_DATE, Z.PAYMENT_TOTAL,   " +
                     "Z.PAYMENT_BANK, Z.PAYMENT_BANKNAME, Z.PAYMENT_BANKACCOUNT, Z.PAYMENT_TRANSID, Z.PAYMENT_BANKDATE, Z.PAYMENT_REJECT, Z.PAYMENT_REJECTREMARKS,   " +
                     "Z.ORDER_DONE, Z.ORDER_CANCEL, Z.ORDER_ACTIVE, Z.ORDER_UPDATEDBY, Z.ORDER_LASTUPDATE, Z.ORDER_TOTALITEM, Z.ORDER_TOTALWEIGHT, Z.ORDER_SUBTOTAL, Z.ORDER_TOTAL, Z.ORDER_GST, Z.ORDER_TOTALGST   " +
                     "FROM  " +
                        "   ( " +
                        "   select a.ORDER_ID AS ORDER_ID,a.order_gst, a.ORDER_TOTALGST, a.MEM_ID AS V_MEM,a.MEM_CODE AS V_MEMCODE,a.MEM_NAME AS V_MEMNAME,a.MEM_EMAIL AS V_MEMEMAIL,a.ORDER_NO AS ORDER_NO,a.ORDER_CREATION AS ORDER_CREATION,a.ORDER_DUEDATE AS ORDER_DUEDATE,a.ORDER_REMARKS AS ORDER_REMARKS,a.ORDER_READ AS ORDER_READ,a.ORDER_TYPE AS V_ORDERTYPE,d.ORDERTYPE_NAME AS V_ORDERTYPENAME,a.ORDER_STATUSID AS ORDER_STATUSID,a.ORDER_STATUS AS ORDER_STATUS,f.STATUS_ID AS V_ORDERSTATUS,f.V_STATUSNAME AS V_ORDERSTATUSNAME,f.STATUS_DATE AS V_STATUSDATE,a.ORDER_NAME AS ORDER_NAME,a.ORDER_ADD AS ORDER_ADD,a.ORDER_POSCODE AS ORDER_POSCODE,a.ORDER_CITY AS ORDER_CITY,a.ORDER_STATE AS ORDER_STATE,a.ORDER_COUNTRY AS ORDER_COUNTRY,g.COUNTRY_NAME AS V_COUNTRYNAME,a.ORDER_CONTACTNO AS ORDER_CONTACTNO,a.ORDER_EMAIL AS ORDER_EMAIL,a.SHIPPING_NAME AS SHIPPING_NAME,a.SHIPPING_ADD AS SHIPPING_ADD,a.SHIPPING_POSCODE AS SHIPPING_POSCODE,a.SHIPPING_CITY AS SHIPPING_CITY,a.SHIPPING_STATE AS SHIPPING_STATE,a.SHIPPING_COUNTRY AS SHIPPING_COUNTRY,h.COUNTRY_NAME AS V_SHIPPINGCOUNTRYNAME,a.SHIPPING_CONTACTNO AS SHIPPING_CONTACTNO,a.SHIPPING_EMAIL AS SHIPPING_EMAIL,a.SHIPPING_REMARKS AS SHIPPING_REMARKS,a.SHIPPING_TRACKCODE AS SHIPPING_TRACKCODE,a.SHIPPING_COMPANY AS SHIPPING_COMPANY,i.COMPANY_NAME AS V_COMPANYNAME,a.SHIPPING_COMPANYNAME AS SHIPPING_COMPANYNAME,a.ORDER_SHIPPING AS ORDER_SHIPPING,a.ORDER_PAYMENT AS ORDER_PAYMENT,e.PAYMENT_GATEWAY AS PAYMENT_GATEWAY,e.PAYMENT_GATEWAYNAME AS PAYMENT_GATEWAYNAME,e.PAYMENT_REMARKS AS PAYMENT_REMARKS,e.PAYMENT_DATE AS PAYMENT_DATE,e.PAYMENT_TOTAL AS PAYMENT_TOTAL,e.PAYMENT_BANK AS PAYMENT_BANK,e.PAYMENT_BANKNAME AS PAYMENT_BANKNAME,e.PAYMENT_BANKACCOUNT AS PAYMENT_BANKACCOUNT,e.PAYMENT_TRANSID AS PAYMENT_TRANSID,e.PAYMENT_BANKDATE AS PAYMENT_BANKDATE,e.PAYMENT_REJECT AS PAYMENT_REJECT,e.PAYMENT_REJECTREMARKS AS PAYMENT_REJECTREMARKS,a.ORDER_DONE AS ORDER_DONE,a.ORDER_CANCEL AS ORDER_CANCEL,a.ORDER_DLVFLAG AS ORDER_DLVFLAG,a.ORDER_ACTIVE AS ORDER_ACTIVE,a.ORDER_UPDATEDBY AS ORDER_UPDATEDBY,a.ORDER_LASTUPDATE AS ORDER_LASTUPDATE,sum(c.ORDDETAIL_PRODQTY) AS ORDER_TOTALITEM,sum((c.ORDDETAIL_PRODWEIGHT * c.ORDDETAIL_PRODQTY)) AS ORDER_TOTALWEIGHT,sum(c.ORDDETAIL_PRODTOTAL) AS ORDER_SUBTOTAL,(sum(c.ORDDETAIL_PRODTOTAL) + a.ORDER_SHIPPING + a.ORDER_TOTALGST) AS ORDER_TOTAL,a.ORDER_PICKUPPOINT AS ORDER_PICKUPPOINT,a.ORDER_PICKUPPOINTNAME AS ORDER_PICKUPPOINTNAME " +
                        "   from (((((((tb_order a left join tb_orderdetail c on((a.ORDER_ID = c.ORDER_ID)))  " +
                        "   left join vw_ordertype d on((a.ORDER_TYPE = d.ORDERTYPE_ID)))  " +
                        "   left join vw_payment e on(((a.ORDER_PAYMENT = e.PAYMENT_ID) or ((a.ORDER_ID = e.ORDER_ID) and (e.PAYMENT_ACTIVE = 1)))))  " +
                        "   left join vw_orderstatus f on(((a.ORDER_STATUSID = f.ORDSTATUS_ID) or ((convert(a.ORDER_STATUS using utf8) = convert(f.STATUS_ID using utf8)) and (a.ORDER_ID = f.ORDER_ID) and (f.STATUS_ACTIVE = 1)))))  " +
                        "   left join tb_country g on convert(a.ORDER_COUNTRY using utf8) = g.COUNTRY_CODE) " +
                        "   left join tb_country h on convert(a.SHIPPING_COUNTRY using utf8) = h.COUNTRY_CODE)  " +
                        "   left join vw_shippingcompany i on a.SHIPPING_COMPANY = i.COMPANY_ID)  " +
                        "   group by a.ORDER_ID " +
                        "   ) " +
                     " Z " +
                     "WHERE Z.ORDER_ID = ?";

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;
                ordId = int.Parse(ds.Tables[0].Rows[0]["ORDER_ID"].ToString());
                memId = int.Parse(ds.Tables[0].Rows[0]["V_MEM"].ToString());
                memCode = ds.Tables[0].Rows[0]["V_MEMCODE"].ToString();
                memName = ds.Tables[0].Rows[0]["V_MEMNAME"].ToString();
                memEmail = ds.Tables[0].Rows[0]["V_MEMEMAIL"].ToString();
                ordNo = ds.Tables[0].Rows[0]["ORDER_NO"].ToString();
                ordCreation = ds.Tables[0].Rows[0]["ORDER_CREATION"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["ORDER_CREATION"]) : DateTime.MaxValue;
                ordDueDate = ds.Tables[0].Rows[0]["ORDER_DUEDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["ORDER_DUEDATE"]) : DateTime.MaxValue;
                ordRemarks = ds.Tables[0].Rows[0]["ORDER_REMARKS"].ToString();
                ordRead = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_READ"]);
                ordType = Convert.ToInt16(ds.Tables[0].Rows[0]["V_ORDERTYPE"]);
                ordTypeName = ds.Tables[0].Rows[0]["V_ORDERTYPENAME"].ToString();
                ordStatusId = int.Parse(ds.Tables[0].Rows[0]["ORDER_STATUSID"].ToString());
                ordStatus = ds.Tables[0].Rows[0]["V_ORDERSTATUS"] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0]["V_ORDERSTATUS"]) : 0;
                ordStatusName = ds.Tables[0].Rows[0]["V_ORDERSTATUSNAME"].ToString();
                ordStatusDate = ds.Tables[0].Rows[0]["V_STATUSDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["V_STATUSDATE"]) : DateTime.MaxValue;

                name = ds.Tables[0].Rows[0]["ORDER_NAME"].ToString();
                add = ds.Tables[0].Rows[0]["ORDER_ADD"].ToString();
                poscode = ds.Tables[0].Rows[0]["ORDER_POSCODE"].ToString();
                city = ds.Tables[0].Rows[0]["ORDER_CITY"].ToString();
                state = ds.Tables[0].Rows[0]["ORDER_STATE"].ToString();
                country = ds.Tables[0].Rows[0]["ORDER_COUNTRY"].ToString();
                countryName = ds.Tables[0].Rows[0]["V_COUNTRYNAME"].ToString();
                contactNo = ds.Tables[0].Rows[0]["ORDER_CONTACTNO"].ToString();
                email = ds.Tables[0].Rows[0]["ORDER_EMAIL"].ToString();

                shippingName = ds.Tables[0].Rows[0]["SHIPPING_NAME"].ToString();
                shippingAdd = ds.Tables[0].Rows[0]["SHIPPING_ADD"].ToString();
                shippingPoscode = ds.Tables[0].Rows[0]["SHIPPING_POSCODE"].ToString();
                shippingCity = ds.Tables[0].Rows[0]["SHIPPING_CITY"].ToString();
                shippingState = ds.Tables[0].Rows[0]["SHIPPING_STATE"].ToString();
                shippingCountry = ds.Tables[0].Rows[0]["SHIPPING_COUNTRY"].ToString();
                shippingCountryName = ds.Tables[0].Rows[0]["V_SHIPPINGCOUNTRYNAME"].ToString();
                shippingContactNo = ds.Tables[0].Rows[0]["SHIPPING_CONTACTNO"].ToString();
                shippingEmail = ds.Tables[0].Rows[0]["SHIPPING_EMAIL"].ToString();
                shippingRemarks = ds.Tables[0].Rows[0]["SHIPPING_REMARKS"].ToString();
                shippingTrackCode = ds.Tables[0].Rows[0]["SHIPPING_TRACKCODE"].ToString();
                shippingCompany = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIPPING_COMPANY"]);
                shippingCompanyName = ds.Tables[0].Rows[0]["SHIPPING_COMPANYNAME"].ToString();
                pickupPoint = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_PICKUPPOINT"]);
                pickupPointName = ds.Tables[0].Rows[0]["ORDER_PICKUPPOINTNAME"].ToString();

                ordShipping = Convert.ToDecimal(ds.Tables[0].Rows[0]["ORDER_SHIPPING"]);
                ordPaymentId = int.Parse(ds.Tables[0].Rows[0]["ORDER_PAYMENT"].ToString());
                ordPayGatewayId = ds.Tables[0].Rows[0]["PAYMENT_GATEWAY"] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0]["PAYMENT_GATEWAY"]) : 0;
                ordPayGateway = ds.Tables[0].Rows[0]["PAYMENT_GATEWAYNAME"].ToString();
                ordPayRemarks = ds.Tables[0].Rows[0]["PAYMENT_REMARKS"].ToString();
                ordPayment = ds.Tables[0].Rows[0]["PAYMENT_DATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["PAYMENT_DATE"]) : DateTime.MaxValue;
                ordPayTotal = ds.Tables[0].Rows[0]["PAYMENT_TOTAL"] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0]["PAYMENT_TOTAL"]) : Convert.ToDecimal("0.00");
                ordPayBank = ds.Tables[0].Rows[0]["PAYMENT_BANK"] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0]["PAYMENT_BANK"]) : 0;
                ordPayBankName = ds.Tables[0].Rows[0]["PAYMENT_BANKNAME"].ToString();
                ordPayBankAccount = ds.Tables[0].Rows[0]["PAYMENT_BANKACCOUNT"].ToString();
                ordPayTransId = ds.Tables[0].Rows[0]["PAYMENT_TRANSID"].ToString();
                ordPayBankDate = ds.Tables[0].Rows[0]["PAYMENT_BANKDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["PAYMENT_BANKDATE"]) : DateTime.MaxValue;
                ordPayReject = ds.Tables[0].Rows[0]["PAYMENT_REJECT"] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0]["PAYMENT_REJECT"]) : 0;
                ordPayRejectRemarks = ds.Tables[0].Rows[0]["PAYMENT_REJECTREMARKS"].ToString();

                ordDone = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_DONE"]);
                ordCancel = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_CANCEL"]);
                ordActive = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_ACTIVE"]);
                ordUpdatedBy = int.Parse(ds.Tables[0].Rows[0]["ORDER_UPDATEDBY"].ToString());
                ordLastUpdate = ds.Tables[0].Rows[0]["ORDER_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["ORDER_LASTUPDATE"]) : DateTime.MaxValue;
                
                ordTotalItem = ds.Tables[0].Rows[0]["ORDER_TOTALITEM"] != DBNull.Value ? int.Parse(ds.Tables[0].Rows[0]["ORDER_TOTALITEM"].ToString()) : 0;
                ordTotalWeight = ds.Tables[0].Rows[0]["ORDER_TOTALWEIGHT"] != DBNull.Value ? int.Parse(ds.Tables[0].Rows[0]["ORDER_TOTALWEIGHT"].ToString()) : 0;
                ordSubtotal = ds.Tables[0].Rows[0]["ORDER_SUBTOTAL"] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0]["ORDER_SUBTOTAL"]) : Convert.ToDecimal("0.00");
                ordTotal = ds.Tables[0].Rows[0]["ORDER_TOTAL"] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0]["ORDER_TOTAL"]) : Convert.ToDecimal("0.00");
                ordGST = int.Parse(ds.Tables[0].Rows[0]["ORDER_GST"].ToString());
                ordTotalGST = ds.Tables[0].Rows[0]["ORDER_TOTALGST"] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0]["ORDER_TOTALGST"]) : Convert.ToDecimal("0.00");
            }
        }
        catch(Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    public Boolean extractOrderByIdFromTB(int intOrdId)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            List<string> columns = new List<string>()
            {
                "`a`.`ORDER_ID` AS `ORDER_ID`",
                "`a`.`MEM_ID` AS `V_MEM`",
                "`a`.`MEM_CODE` AS `V_MEMCODE`",
                "`a`.`MEM_NAME` AS `V_MEMNAME`",
                "`a`.`MEM_EMAIL` AS `V_MEMEMAIL`",
                "`a`.`ORDER_NO` AS `ORDER_NO`",
                "`a`.`ORDER_GST` AS `order_gst`",
                "`a`.`ORDER_TOTALGST` AS `ORDER_TOTALGST`",
                "`a`.`ORDER_CREATION` AS `ORDER_CREATION`",
                "`a`.`ORDER_DUEDATE` AS `ORDER_DUEDATE`",
                "`a`.`ORDER_REMARKS` AS `ORDER_REMARKS`",
                "`a`.`ORDER_READ` AS `ORDER_READ`",
                "`a`.`ORDER_TYPE` AS `V_ORDERTYPE`",
                "`a`.`ORDER_STATUSID` AS `ORDER_STATUSID`",
                "`a`.`ORDER_STATUS` AS `ORDER_STATUS`",
                "`a`.`ORDER_NAME` AS `ORDER_NAME`",
                "`a`.`ORDER_ADD` AS `ORDER_ADD`",
                "`a`.`ORDER_POSCODE` AS `ORDER_POSCODE`",
                "`a`.`ORDER_CITY` AS `ORDER_CITY`",
                "`a`.`ORDER_STATE` AS `ORDER_STATE`",
                "`a`.`ORDER_COUNTRY` AS `ORDER_COUNTRY`",
                "`a`.`ORDER_CONTACTNO` AS `ORDER_CONTACTNO`",
                "`a`.`ORDER_EMAIL` AS `ORDER_EMAIL`",
                "`a`.`SHIPPING_NAME` AS `SHIPPING_NAME`",
                "`a`.`SHIPPING_ADD` AS `SHIPPING_ADD`",
                "`a`.`SHIPPING_POSCODE` AS `SHIPPING_POSCODE`",
                "`a`.`SHIPPING_CITY` AS `SHIPPING_CITY`",
                "`a`.`SHIPPING_STATE` AS `SHIPPING_STATE`",
                "`a`.`SHIPPING_COUNTRY` AS `SHIPPING_COUNTRY`",
                "`a`.`SHIPPING_CONTACTNO` AS `SHIPPING_CONTACTNO`",
                "`a`.`SHIPPING_EMAIL` AS `SHIPPING_EMAIL`",
                "`a`.`SHIPPING_REMARKS` AS `SHIPPING_REMARKS`",
                "`a`.`SHIPPING_TRACKCODE` AS `SHIPPING_TRACKCODE`",
                "`a`.`SHIPPING_COMPANY` AS `SHIPPING_COMPANY`",
                "`a`.`SHIPPING_COMPANYNAME` AS `SHIPPING_COMPANYNAME`",
                "`a`.`ORDER_SHIPPING` AS `ORDER_SHIPPING`",
                "`a`.`ORDER_PAYMENT` AS `ORDER_PAYMENT`",
                "`a`.`ORDER_DONE` AS `ORDER_DONE`",
                "`a`.`ORDER_CANCEL` AS `ORDER_CANCEL`",
                "`a`.`ORDER_DLVFLAG` AS `ORDER_DLVFLAG`",
                "`a`.`ORDER_ACTIVE` AS `ORDER_ACTIVE`",
                "`a`.`ORDER_UPDATEDBY` AS `ORDER_UPDATEDBY`",
                "`a`.`ORDER_LASTUPDATE` AS `ORDER_LASTUPDATE`",
                "`a`.`ORDER_DISCOUPONCODE`",
                "`a`.`ORDER_DISOPERATOR`",
                "`a`.`ORDER_DISVALUE`",
                "`a`.`ORDER_PICKUPPOINT` AS `ORDER_PICKUPPOINT`",
                "`a`.`ORDER_PICKUPPOINTNAME` AS `ORDER_PICKUPPOINTNAME`",
                "`a`.`ORDER_MSG_BUYER`",

                "`a`.`DELIVERY_DATE`",
                "`a`.`DELIVERY_TIME`",
                "`a`.`DELIVERY_RECIPIENT`",
                "`a`.`DELIVERY_SENDER`",
                "`a`.`DELIVERY_MESSAGE`",

                "`d`.`ORDERTYPE_NAME` AS `V_ORDERTYPENAME`",
                "`e`.`PAYMENT_GATEWAY` AS `PAYMENT_GATEWAY`",
                "`e`.`PAYMENT_GATEWAYNAME` AS `PAYMENT_GATEWAYNAME`",
                "`e`.`PAYMENT_REMARKS` AS `PAYMENT_REMARKS`",
                "`e`.`PAYMENT_DATE` AS `PAYMENT_DATE`",
                "`e`.`PAYMENT_TOTAL` AS `PAYMENT_TOTAL`",
                "`e`.`PAYMENT_BANK` AS `PAYMENT_BANK`",
                "`e`.`PAYMENT_BANKNAME` AS `PAYMENT_BANKNAME`",
                "`e`.`PAYMENT_BANKACCOUNT` AS `PAYMENT_BANKACCOUNT`",
                "`e`.`PAYMENT_TRANSID` AS `PAYMENT_TRANSID`",
                "`e`.`PAYMENT_BANKDATE` AS `PAYMENT_BANKDATE`",
                "`e`.`PAYMENT_REJECT` AS `PAYMENT_REJECT`",
                "`e`.`PAYMENT_REJECTREMARKS` AS `PAYMENT_REJECTREMARKS`",
                "`f`.`STATUS_ID` AS `V_ORDERSTATUS`",
                "`f`.`V_STATUSNAME` AS `V_ORDERSTATUSNAME`",
                "`f`.`STATUS_DATE` AS `V_STATUSDATE`",
                "`g`.`COUNTRY_NAME` AS `V_COUNTRYNAME`",
                "`h`.`COUNTRY_NAME` AS `V_SHIPPINGCOUNTRYNAME`",
                "`i`.`COMPANY_NAME` AS `V_COMPANYNAME`",

                "sum(`c`.`ORDDETAIL_PRODQTY`) AS `ORDER_TOTALITEM`",
                "sum(`c`.`ORDDETAIL_PRODWEIGHT` * `c`.`ORDDETAIL_PRODQTY`) AS `ORDER_TOTALWEIGHT`",
                "sum(`c`.`ORDDETAIL_PRODTOTAL`) AS `ORDER_SUBTOTAL`",
                "(sum(`c`.`ORDDETAIL_PRODTOTAL`) + `a`.`ORDER_SHIPPING` + `a`.`ORDER_TOTALGST`) AS `ORDER_TOTAL`",  
            };

            string strSql = "SELECT " + string.Join(",",columns.ToArray()) + 
                            " FROM `tb_order` `a` " +
                            " LEFT JOIN `tb_orderdetail` `c` on `a`.`ORDER_ID` = `c`.`ORDER_ID` " +
                            " LEFT JOIN `vw_ordertype` `d` on `a`.`ORDER_TYPE` = `d`.`ORDERTYPE_ID`" +
                            " LEFT JOIN `vw_payment` `e` on `a`.`ORDER_PAYMENT` = `e`.`PAYMENT_ID` OR(`a`.`ORDER_ID` = `e`.`ORDER_ID`  AND `e`.`PAYMENT_ACTIVE` = 1)" +
                            " LEFT JOIN `vw_orderstatus` `f` on `a`.`ORDER_STATUSID` = `f`.`ORDSTATUS_ID` OR(convert(`a`.`ORDER_STATUS` USING utf8) = convert(`f`.`STATUS_ID` USING utf8) AND `a`.`ORDER_ID` = `f`.`ORDER_ID` AND `f`.`STATUS_ACTIVE` = 1)" +
                            " LEFT JOIN `tb_country` `g` on convert(`a`.`ORDER_COUNTRY` USING utf8) = `g`.`COUNTRY_CODE` " +
                            " LEFT JOIN `tb_country` `h` on convert(`a`.`SHIPPING_COUNTRY` USING utf8) = `h`.`COUNTRY_CODE` " +
                            " LEFT JOIN `vw_shippingcompany` `i` on `a`.`SHIPPING_COMPANY` = `i`.`COMPANY_ID`" +
                            " WHERE `a`.`ORDER_ID` = ? " +
                            " GROUP BY `a`.`ORDER_ID`;";

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = ds.Tables[0].Rows[0];

                boolFound = true;
                ordId = int.Parse(row["ORDER_ID"].ToString());
                memId = int.Parse(row["V_MEM"].ToString());
                memCode = row["V_MEMCODE"].ToString();
                memName = row["V_MEMNAME"].ToString();
                memEmail = row["V_MEMEMAIL"].ToString();
                ordNo = row["ORDER_NO"].ToString();
                ordCreation = row["ORDER_CREATION"] != DBNull.Value ? Convert.ToDateTime(row["ORDER_CREATION"]) : DateTime.MaxValue;
                ordDueDate = row["ORDER_DUEDATE"] != DBNull.Value ? Convert.ToDateTime(row["ORDER_DUEDATE"]) : DateTime.MaxValue;
                ordRemarks = row["ORDER_REMARKS"].ToString();
                ordRead = Convert.ToInt16(row["ORDER_READ"]);
                ordType = Convert.ToInt16(row["V_ORDERTYPE"]);
                ordTypeName = row["V_ORDERTYPENAME"].ToString();
                ordStatusId = int.Parse(row["ORDER_STATUSID"].ToString());
                ordStatus = row["V_ORDERSTATUS"] != DBNull.Value ? Convert.ToInt16(row["V_ORDERSTATUS"]) : 0;
                ordStatusName = row["V_ORDERSTATUSNAME"].ToString();
                ordStatusDate = row["V_STATUSDATE"] != DBNull.Value ? Convert.ToDateTime(row["V_STATUSDATE"]) : DateTime.MaxValue;
                ordMsgBuyer = row["ORDER_MSG_BUYER"].ToString();

                name = row["ORDER_NAME"].ToString();
                add = row["ORDER_ADD"].ToString();
                poscode = row["ORDER_POSCODE"].ToString();
                city = row["ORDER_CITY"].ToString();
                state = row["ORDER_STATE"].ToString();
                country = row["ORDER_COUNTRY"].ToString();
                countryName = row["V_COUNTRYNAME"].ToString();
                contactNo = row["ORDER_CONTACTNO"].ToString();
                email = row["ORDER_EMAIL"].ToString();

                shippingName = row["SHIPPING_NAME"].ToString();
                shippingAdd = row["SHIPPING_ADD"].ToString();
                shippingPoscode = row["SHIPPING_POSCODE"].ToString();
                shippingCity = row["SHIPPING_CITY"].ToString();
                shippingState = row["SHIPPING_STATE"].ToString();
                shippingCountry = row["SHIPPING_COUNTRY"].ToString();
                shippingCountryName = row["V_SHIPPINGCOUNTRYNAME"].ToString();
                shippingContactNo = row["SHIPPING_CONTACTNO"].ToString();
                shippingEmail = row["SHIPPING_EMAIL"].ToString();
                shippingRemarks = row["SHIPPING_REMARKS"].ToString();
                shippingTrackCode = row["SHIPPING_TRACKCODE"].ToString();
                shippingCompany = Convert.ToInt16(row["SHIPPING_COMPANY"]);
                shippingCompanyName = row["SHIPPING_COMPANYNAME"].ToString();
                pickupPoint = Convert.ToInt16(row["ORDER_PICKUPPOINT"]);
                pickupPointName = row["ORDER_PICKUPPOINTNAME"].ToString();

                ordShipping = Convert.ToDecimal(row["ORDER_SHIPPING"]);
                ordPaymentId = int.Parse(row["ORDER_PAYMENT"].ToString());
                ordPayGatewayId = row["PAYMENT_GATEWAY"] != DBNull.Value ? Convert.ToInt16(row["PAYMENT_GATEWAY"]) : 0;
                ordPayGateway = row["PAYMENT_GATEWAYNAME"].ToString();
                ordPayRemarks = row["PAYMENT_REMARKS"].ToString();
                ordPayment = row["PAYMENT_DATE"] != DBNull.Value ? Convert.ToDateTime(row["PAYMENT_DATE"]) : DateTime.MaxValue;
                ordPayTotal = row["PAYMENT_TOTAL"] != DBNull.Value ? Convert.ToDecimal(row["PAYMENT_TOTAL"]) : Convert.ToDecimal("0.00");
                ordPayBank = row["PAYMENT_BANK"] != DBNull.Value ? Convert.ToInt16(row["PAYMENT_BANK"]) : 0;
                ordPayBankName = row["PAYMENT_BANKNAME"].ToString();
                ordPayBankAccount = row["PAYMENT_BANKACCOUNT"].ToString();
                ordPayTransId = row["PAYMENT_TRANSID"].ToString();
                ordPayBankDate = row["PAYMENT_BANKDATE"] != DBNull.Value ? Convert.ToDateTime(row["PAYMENT_BANKDATE"]) : DateTime.MaxValue;
                ordPayReject = row["PAYMENT_REJECT"] != DBNull.Value ? Convert.ToInt16(row["PAYMENT_REJECT"]) : 0;
                ordPayRejectRemarks = row["PAYMENT_REJECTREMARKS"].ToString();

                ordDone = Convert.ToInt16(row["ORDER_DONE"]);
                ordCancel = Convert.ToInt16(row["ORDER_CANCEL"]);
                ordActive = Convert.ToInt16(row["ORDER_ACTIVE"]);
                ordUpdatedBy = int.Parse(row["ORDER_UPDATEDBY"].ToString());
                ordLastUpdate = row["ORDER_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(row["ORDER_LASTUPDATE"]) : DateTime.MaxValue;

                ordTotalItem = row["ORDER_TOTALITEM"] != DBNull.Value ? int.Parse(row["ORDER_TOTALITEM"].ToString()) : 0;
                ordTotalWeight = row["ORDER_TOTALWEIGHT"] != DBNull.Value ? int.Parse(row["ORDER_TOTALWEIGHT"].ToString()) : 0;
                ordSubtotal = row["ORDER_SUBTOTAL"] != DBNull.Value ? Convert.ToDecimal(row["ORDER_SUBTOTAL"]) : Convert.ToDecimal("0.00");
                ordTotal = row["ORDER_TOTAL"] != DBNull.Value ? Convert.ToDecimal(row["ORDER_TOTAL"]) : Convert.ToDecimal("0.00");
                ordGST = int.Parse(row["ORDER_GST"].ToString());
                ordTotalGST = row["ORDER_TOTALGST"] != DBNull.Value ? Convert.ToDecimal(row["ORDER_TOTALGST"]) : Convert.ToDecimal("0.00");

                // ========= Coupon Feature========
                couponCode = Convert.ToString(row["ORDER_DISCOUPONCODE"]);
                disOperator = Convert.ToString(row["ORDER_DISOPERATOR"]);
                disValue = Convert.ToDecimal(row["ORDER_DISVALUE"]);

                // ========== Delivery Info ===========
                deliveryDate = row["DELIVERY_DATE"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["DELIVERY_DATE"].ToString()) : DateTime.MaxValue;
                deliveryTime = row["DELIVERY_TIME"].ToString();
                deliveryRecipient = row["DELIVERY_RECIPIENT"].ToString();
                deliverySender = row["DELIVERY_SENDER"].ToString();
                deliveryMessage = row["DELIVERY_MESSAGE"].ToString();

                // ========= Format Data ==========
                ordTotal = ordTotal - disValue;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractConvertionRateUnitById(int intOrdId)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDER_ID, A.ORDER_CONVERTIONRATE, A.ORDER_CONVERTIONUNIT " +
                     "FROM TB_ORDER A " +
                     "WHERE A.ORDER_ID = ?";

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;
                ordId = int.Parse(ds.Tables[0].Rows[0]["ORDER_ID"].ToString());
                ordConvRate = ds.Tables[0].Rows[0]["ORDER_CONVERTIONRATE"] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0]["ORDER_CONVERTIONRATE"]) : Convert.ToDecimal("0.00");
                ordConvUnit = ds.Tables[0].Rows[0]["ORDER_CONVERTIONUNIT"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractOrderDiscountById(int intOrdId)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDER_DISCOUPONCODE, A.ORDER_DISOPERATOR, A.ORDER_DISVALUE " +
                     "FROM TB_ORDER A " +
                     "WHERE A.ORDER_ID = ?";

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;
                couponCode = ds.Tables[0].Rows[0]["ORDER_DISCOUPONCODE"].ToString();
                disOperator = ds.Tables[0].Rows[0]["ORDER_DISOPERATOR"].ToString();
                disValue = ds.Tables[0].Rows[0]["ORDER_DISVALUE"] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0]["ORDER_DISVALUE"]) : Convert.ToDecimal("0.00");

            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public DataSet getOrderList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDER_ID, A.V_MEM, A.V_MEMCODE, A.V_MEMNAME, A.V_MEMEMAIL, A.ORDER_NO, A.ORDER_CREATION, A.ORDER_DUEDATE, A.ORDER_REMARKS, " +
                     "A.ORDER_READ, A.V_ORDERTYPE, A.V_ORDERTYPENAME, A.ORDER_STATUSID, A.ORDER_STATUS, A.V_ORDERSTATUS, A.V_ORDERSTATUSNAME, A.V_STATUSDATE, " +
                     "A.ORDER_NAME, A.ORDER_ADD, A.ORDER_POSCODE, A.ORDER_CITY, A.ORDER_STATE, A.ORDER_COUNTRY, A.V_COUNTRYNAME, A.ORDER_CONTACTNO, A.ORDER_EMAIL, " +
                     "A.SHIPPING_NAME, A.SHIPPING_ADD, A.SHIPPING_POSCODE, A.SHIPPING_CITY, A.SHIPPING_STATE, A.SHIPPING_COUNTRY, A.V_SHIPPINGCOUNTRYNAME, A.SHIPPING_CONTACTNO, A.SHIPPING_EMAIL, " +
                     "A.SHIPPING_REMARKS, A.SHIPPING_TRACKCODE, A.SHIPPING_COMPANY, A.SHIPPING_COMPANYNAME, A.ORDER_PICKUPPOINT, A.ORDER_PICKUPPOINTNAME, A.ORDER_SHIPPING, " +
                     "A.ORDER_PAYMENT, A.PAYMENT_GATEWAY, A.PAYMENT_GATEWAYNAME, A.PAYMENT_REMARKS, A.PAYMENT_DATE, A.PAYMENT_TOTAL, " +
                     "A.PAYMENT_BANK, A.PAYMENT_BANKNAME, A.PAYMENT_BANKACCOUNT, A.PAYMENT_TRANSID, A.PAYMENT_BANKDATE, A.PAYMENT_REJECT, A.PAYMENT_REJECTREMARKS, " +
                     "A.ORDER_DONE, A.ORDER_CANCEL, A.ORDER_ACTIVE, A.ORDER_UPDATEDBY, A.ORDER_LASTUPDATE, A.ORDER_TOTALITEM, A.ORDER_TOTALWEIGHT, A.ORDER_SUBTOTAL, A.ORDER_TOTAL " +
                     "FROM VW_ORDER A";

            if (intActive != 0)
            {
                strSql += " WHERE A.ORDER_ACTIVE = ?";
                cmd.Parameters.Add("p_ORDER_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getOrderList2(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            //strSql = "SELECT A.ORDER_ID, A.MEM_ID, A.MEM_CODE, A.MEM_NAME, A.MEM_COMPANYNAME, A.MEM_EMAIL, A.MEM_TYPE, A.ORDER_NO, A.ORDER_CREATION, A.ORDER_REMARKS, A.ORDER_READ, " +
            //         "A.ORDER_TYPE, A.ORDER_STATUSID, A.ORDER_STATUS, A.ORDER_COMPANY, A.ORDER_NAME, A.ORDER_ADD, A.ORDER_POSCODE, A.ORDER_CITY, A.ORDER_STATE, " +
            //         "A.ORDER_COUNTRY, A.ORDER_CONTACTNO, A.ORDER_CONTACTNO2, A.ORDER_FAX, A.ORDER_EMAIL, A.SHIPPING_CUSTCOMPANY, A.SHIPPING_NAME, " +
            //         "A.SHIPPING_ADD, A.SHIPPING_POSCODE, A.SHIPPING_CITY, A.SHIPPING_STATE, A.SHIPPING_COUNTRY, A.SHIPPING_CONTACTNO, A.SHIPPING_CONTACTNO2, " +
            //         "A.SHIPPING_FAX, A.SHIPPING_EMAIL, A.SHIPPING_REMARKS, A.SHIPPING_TRACKCODE, A.SHIPPING_COMPANY, A.SHIPPING_COMPANYNAME, A.ORDER_BILLSHIPSAME, " +
            //         "A.ORDER_SHIPPING, A.ORDER_PAYMENT, A.ORDER_DONE, A.ORDER_CANCEL, A.ORDER_DLVFLAG, A.ORDER_ACTIVE, A.ORDER_PICKUPPOINT, A.ORDER_PICKUPPOINTNAME, A.ORDER_DISVALUE, " +
            //         "A.ORDER_UPDATEDBY, A.ORDER_LASTUPDATE, A.ORDER_CONVERTIONRATE, A.ORDER_CONVERTIONUNIT, " +
            //         "(SUM(B.ORDDETAIL_PRODTOTAL) + A.ORDER_SHIPPING + A.ORDER_TOTALGST) AS ORDER_TOTAL, SUM(B.ORDDETAIL_PRODQTY) AS ORDER_TOTALITEM, SUM(B.ORDDETAIL_PRODTOTAL) AS ORDER_SUBTOTAL, " +
            //         "(SELECT LIST_NAME FROM TB_LIST WHERE A.ORDER_STATUS = LIST_VALUE AND LIST_GROUP = 'ORDER STATUS' AND LIST_DEFAULT = 1) AS V_STATUSNAME, " +
            //         "(SELECT LIST_NAME FROM TB_LIST WHERE A.ORDER_TYPE = LIST_VALUE AND LIST_GROUP = 'ORDER TYPE') AS ORDERTYPE_NAME, " +
            //         "CASE WHEN MEM_TYPE = 1 THEN MEM_EMAIL ELSE CASE WHEN MEM_COMPANYNAME = '' THEN MEM_EMAIL ELSE MEM_COMPANYNAME END END AS V_MEMCUSTNAME " +
            //         "FROM TB_ORDER A LEFT JOIN TB_ORDERDETAIL B ON (A.ORDER_ID = B.ORDER_ID) " +
            //         "GROUP BY A.ORDER_ID";

            string[] columns = {
                                   "A.ORDER_ID","A.MEM_ID","A.MEM_CODE","A.MEM_NAME","A.MEM_COMPANYNAME","A.MEM_EMAIL",
                                   "A.MEM_TYPE","A.ORDER_NO","A.ORDER_CREATION","A.ORDER_REMARKS","A.ORDER_READ",
                                   "A.ORDER_TYPE","A.ORDER_STATUSID","A.ORDER_STATUS","A.ORDER_COMPANY","A.ORDER_NAME","A.ORDER_ADD",
                                   "A.ORDER_POSCODE","A.ORDER_CITY", "A.ORDER_STATE","A.ORDER_COUNTRY","A.ORDER_CONTACTNO","A.ORDER_CONTACTNO2",
                                   "A.ORDER_FAX","A.ORDER_EMAIL","A.SHIPPING_CUSTCOMPANY","A.SHIPPING_NAME","A.SHIPPING_ADD","A.SHIPPING_POSCODE",
                                   "A.SHIPPING_CITY","A.SHIPPING_STATE","A.SHIPPING_COUNTRY","A.SHIPPING_CONTACTNO","A.SHIPPING_CONTACTNO2","A.SHIPPING_FAX",
                                   "A.SHIPPING_EMAIL","A.SHIPPING_REMARKS","A.SHIPPING_TRACKCODE","A.SHIPPING_COMPANY","A.SHIPPING_COMPANYNAME","A.ORDER_BILLSHIPSAME",
                                   "A.ORDER_SHIPPING","A.ORDER_PAYMENT","A.ORDER_DONE","A.ORDER_CANCEL","A.ORDER_DLVFLAG","A.ORDER_ACTIVE",
                                   "A.ORDER_PICKUPPOINT","A.ORDER_PICKUPPOINTNAME", "A.ORDER_DISVALUE","A.ORDER_DISCOUPONCODE", "A.ORDER_UPDATEDBY","A.ORDER_LASTUPDATE",
                                   "A.ORDER_CONVERTIONRATE","A.ORDER_CONVERTIONUNIT",
                                   "(SUM(B.ORDDETAIL_PRODTOTAL) + A.ORDER_SHIPPING + A.ORDER_TOTALGST) AS ORDER_TOTAL",
                                   "SUM(B.ORDDETAIL_PRODQTY) AS ORDER_TOTALITEM","SUM(B.ORDDETAIL_PRODTOTAL) AS ORDER_SUBTOTAL",
                                   "LIST_A.LIST_NAME AS V_STATUSNAME","LIST_B.LIST_NAME AS ORDERTYPE_NAME",
                                   "CASE WHEN MEM_TYPE = 1 THEN MEM_EMAIL ELSE CASE WHEN MEM_COMPANYNAME = '' THEN MEM_EMAIL ELSE MEM_COMPANYNAME END END AS V_MEMCUSTNAME"
                               };

            strSql = "SELECT " + string.Join(",",columns) +
                    " FROM TB_ORDERDETAIL B" +
                    " LEFT JOIN TB_ORDER A ON (A.ORDER_ID = B.ORDER_ID)" +
                    " LEFT JOIN TB_LIST LIST_A ON A.ORDER_STATUS = LIST_A.LIST_VALUE" +
                    " LEFT JOIN TB_LIST LIST_B ON A.ORDER_TYPE = LIST_B.LIST_VALUE" +
                    " WHERE (LIST_A.LIST_GROUP = 'ORDER STATUS' AND LIST_A.LIST_DEFAULT = 1)" +
                    " AND (LIST_B.LIST_GROUP = 'ORDER TYPE')" +
                    " GROUP BY B.ORDER_ID";

            if (intActive != 0)
            {
                strSql += " WHERE A.ORDER_ACTIVE = ?";
                cmd.Parameters.Add("p_ORDER_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataTable getTable(int intActive)
    {
        DataSet ds = new DataSet();
        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql = "SELECT * FROM TB_ORDER A";

            if (intActive != 0)
            {
                strSql += " WHERE A.ORDER_ACTIVE = ?";
                cmd.Parameters.Add("p_ORDER_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            ds = null;
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        if(ds!= null)
        {
            return ds.Tables[0];
        }
        return null;
    }


    public DataSet getOrderItems(string strStart, string strEnd, int intOrdType, int intOrdStatus, int intProdId)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDER_ID, A.ORDER_TYPE, A.ORDER_STATUS,A.ORDER_CREATION, B.ORDDETAIL_PRODID, B.ORDDETAIL_PRODCODE, B.ORDDETAIL_PRODNAME, B.ORDDETAIL_PRODDNAME, B.ORDDETAIL_PRODIMAGE, B.ORDDETAIL_PRODPRICE," +
                     " B.ORDDETAIL_PRODQTY, B.ORDDETAIL_PRODUOM, B.ORDDETAIL_PRODWEIGHT, B.ORDDETAIL_PRODLENGTH, B.ORDDETAIL_PRODWIDTH, B.ORDDETAIL_PRODHEIGHT, B.ORDDETAIL_PRODTOTAL," +
                     " SUM(B.ORDDETAIL_PRODQTY) AS ORDDETAIL_PRODTOTALQTY, SUM(B.ORDDETAIL_PRODTOTAL) AS ORDDETAIL_PRODTOTALAMOUNT" +
                     " FROM TB_ORDER A, TB_ORDERDETAIL B" +
                     " WHERE A.ORDER_ID = B.ORDER_ID";

            if (!string.IsNullOrEmpty(strStart) && !string.IsNullOrEmpty(strEnd))
            {
                strSql += " AND A.ORDER_CREATION >= ? AND A.ORDER_CREATION <= ?";

                cmd.Parameters.Add("p_START", OdbcType.VarChar, 250).Value = strStart;
                cmd.Parameters.Add("p_END", OdbcType.VarChar, 250).Value = strEnd;
            }

            if (intOrdType != 0)
            {
                strSql += " AND A.ORDER_TYPE = ?";

                cmd.Parameters.Add("p_ORDER_TYPE", OdbcType.Int, 9).Value = intOrdType;
            }

            if (intOrdStatus != 0)
            {
                strSql += " AND A.ORDER_STATUS = ?";

                cmd.Parameters.Add("p_ORDER_STATUS", OdbcType.Int, 9).Value = intOrdStatus;
            }

            if (intProdId != 0)
            {
                strSql += " AND B.ORDDETAIL_PRODID = ?";

                cmd.Parameters.Add("p_ORDDETAIL_PRODID", OdbcType.Int, 9).Value = intProdId;
            }

            strSql += " GROUP BY B.ORDDETAIL_PRODID";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getOrderItems()
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDER_ID, B.ORDDETAIL_PRODID, B.ORDDETAIL_PRODCODE, B.ORDDETAIL_PRODNAME, B.ORDDETAIL_PRODDNAME, B.ORDDETAIL_PRODIMAGE, B.ORDDETAIL_PRODPRICE, " +
                     "B.ORDDETAIL_PRODQTY, B.ORDDETAIL_PRODUOM, B.ORDDETAIL_PRODWEIGHT, B.ORDDETAIL_PRODLENGTH, B.ORDDETAIL_PRODWIDTH, B.ORDDETAIL_PRODHEIGHT, B.ORDDETAIL_PRODTOTAL, " +
                     "SUM(B.ORDDETAIL_PRODTOTAL) AS ORDDETAIL_PRODTOTALAMOUNT " +
                     "FROM TB_ORDER A, TB_ORDERDETAIL B " +
                     "WHERE A.ORDER_ID = B.ORDER_ID " +
                     "GROUP BY B.ORDDETAIL_PRODID";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getOrderItemsByOrderId(int intOrdId)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDDETAIL_ID, A.ORDDETAIL_PRODID, A.ORDDETAIL_PRODCODE, A.ORDDETAIL_PRODNAME, A.ORDDETAIL_PRODDNAME, A.ORDDETAIL_PRODIMAGE, A.ORDDETAIL_PRODPRICE," +
                     " A.ORDDETAIL_PRODQTY, A.ORDDETAIL_PRODUOM, A.ORDDETAIL_PRODWEIGHT, A.ORDDETAIL_PRODLENGTH, A.ORDDETAIL_PRODWIDTH, A.ORDDETAIL_PRODHEIGHT, A.ORDDETAIL_PRODTOTAL," +
                     " A.ORDDETAIL_PRODPRICING, A.ORDDETAIL_PRICINGNAME1, A.ORDDETAIL_PRICINGNAME2" +
                     " FROM TB_ORDERDETAIL A" +
                     " WHERE ORDER_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getOrderItemsByOrderId3(int intOrdId)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDDETAIL_ID, A.ORDDETAIL_PRODID, A.ORDDETAIL_PRODCODE, A.ORDDETAIL_PRODNAME, A.ORDDETAIL_PRODDNAME, A.ORDDETAIL_PRODIMAGE, A.ORDDETAIL_PRODPRICE," +
                     " A.ORDDETAIL_PRODQTY, A.ORDDETAIL_PRODUOM, A.ORDDETAIL_PRODWEIGHT, A.ORDDETAIL_PRODLENGTH, A.ORDDETAIL_PRODWIDTH, A.ORDDETAIL_PRODHEIGHT, A.ORDDETAIL_PRODTOTAL," +
                     " A.ORDDETAIL_PRODPRICING, A.ORDDETAIL_PRICINGNAME1, A.ORDDETAIL_PRICINGNAME2, A.ORDDETAIL_PRODGST" +
                     " FROM TB_ORDERDETAIL A" +
                     " WHERE ORDER_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getTbOrderByOrderId(string strColumnName, int intOrdId)
    {
        return getTbOrderByOrderId(new string[1] { strColumnName }, intOrdId);
    }

    public DataSet getTbOrderByOrderId(string[] strColumnName, int intOrdId)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT " + string.Join(",",strColumnName) +
                     " FROM TB_ORDER A" +
                     " WHERE ORDER_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public int addOrderFile(int intOrdId, string strOrderFileTitle, string strOrderFile)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ORDERFILE (" +
                     "ORDER_ID, " +
                     "ORDFILE_TITLE, " +
                     "ORDFILE_FILE" +
                     ") VALUES " +
                     "(?,?,?)";

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;
            cmd.Parameters.Add("p_ORDFILE_TITLE", OdbcType.VarChar, 250).Value = strOrderFileTitle;
            cmd.Parameters.Add("p_ORDFILE_FILE", OdbcType.VarChar, 250).Value = strOrderFile;

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateOrderType(int intOrdId, int intOrdType)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET ORDER_TYPE = ? WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_TYPE", OdbcType.Int, 9).Value = intOrdType;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateOrderRead(int intOrdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET ORDER_READ = 1 WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateOrderRemarks(int intOrdId, string strRemarks)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET ORDER_REMARKS = ? WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_REMARKS", OdbcType.Text, 1000).Value = strRemarks;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isMemberOrderValid(int intOrdid, int intMemId)
    {
        Boolean boolValid = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_ORDER" +
                     " WHERE ORDER_ID = ?" +
                     " AND MEM_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdid;
            cmd.Parameters.Add("p_MEM_ID", OdbcType.Int, 9).Value = intMemId;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1)
            {
                boolValid = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolValid;
    }

    public Boolean isOrderCodeExist(int intOrdId, string strOrdCode)
    {
        Boolean boolExist = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_ORDER " +
                     "WHERE ORDER_NO = ?";

            cmd.Parameters.Add("p_ORDER_NO", OdbcType.VarChar, 20).Value = strOrdCode;

            if (intOrdId != 0)
            {
                strSql += " AND ORDER_ID != ?";

                cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intOrdId;
            }

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolExist = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public Boolean isExactSameOrderSet(int intOrdId, string strNameBill, string strAddBill, string strPoscodeBill, string strCityBill, string strStateBill, string strCountryBill, string strContactNoBill, string strName, string strAdd, string strPoscode, string strCity, string strState, string strCountry, string strContactNo, DateTime dtCollectionDate)
    {
        Boolean boolSame = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_ORDER" +
                     " WHERE ORDER_ID = ?" +
                     " AND BINARY ORDER_NAME = ?" +
                     " AND BINARY ORDER_ADD = ?" +
                     " AND BINARY ORDER_POSCODE = ?" +
                     " AND BINARY ORDER_CITY = ?" +
                     " AND BINARY ORDER_STATE = ?" +
                     " AND ORDER_COUNTRY = ?" +
                     " AND BINARY ORDER_CONTACTNO = ?" +
                     " AND BINARY SHIPPING_NAME = ?" +
                     " AND BINARY SHIPPING_ADD = ?" +
                     " AND BINARY SHIPPING_POSCODE = ?" +
                     " AND BINARY SHIPPING_CITY = ?" +
                     " AND BINARY SHIPPING_STATE = ?" +
                     " AND SHIPPING_COUNTRY = ?" +
                     " AND BINARY SHIPPING_CONTACTNO = ?" +
                     " AND SHIPPING_COLLECTIONDATE = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;
            cmd.Parameters.Add("p_ORDER_NAME", OdbcType.VarChar, 250).Value = strNameBill;
            cmd.Parameters.Add("p_ORDER_ADD", OdbcType.VarChar, 250).Value = strAddBill;
            cmd.Parameters.Add("p_ORDER_POSCODE", OdbcType.VarChar, 20).Value = strPoscodeBill;
            cmd.Parameters.Add("p_ORDER_CITY", OdbcType.VarChar, 50).Value = strCityBill;
            cmd.Parameters.Add("p_ORDER_STATE", OdbcType.VarChar, 250).Value = strStateBill;
            cmd.Parameters.Add("p_ORDER_COUNTRY", OdbcType.VarChar, 10).Value = strCountryBill;
            cmd.Parameters.Add("p_ORDER_CONTACTNO", OdbcType.VarChar, 20).Value = strContactNoBill;
            cmd.Parameters.Add("p_SHIPPING_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_SHIPPING_ADD", OdbcType.VarChar, 250).Value = strAdd;
            cmd.Parameters.Add("p_SHIPPING_POSCODE", OdbcType.VarChar, 20).Value = strPoscode;
            cmd.Parameters.Add("p_SHIPPING_CITY", OdbcType.VarChar, 50).Value = strCity;
            cmd.Parameters.Add("p_SHIPPING_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_SHIPPING_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIPPING_CONTACTNO", OdbcType.VarChar, 20).Value = strContactNo;
            cmd.Parameters.Add("p_SHIPPING_COLLECTIONDATE", OdbcType.DateTime).Value = dtCollectionDate;
            
            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int updateOrderDelivery(int intOrdId, string strNameBill, string strAddBill, string strPoscodeBill, string strCityBill, string strStateBill, string strCountryBill, string strContactNoBill, string strName, string strAdd, string strPoscode, string strCity, string strState, string strCountry, string strContactNo, DateTime dtCollectionDate, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET" +
                     " ORDER_NAME = ?," +
                     " ORDER_ADD = ?," +
                     " ORDER_POSCODE = ?," +
                     " ORDER_CITY = ?," +
                     " ORDER_STATE = ?," +
                     " ORDER_COUNTRY = ?," +
                     " ORDER_CONTACTNO = ?," +
                     " SHIPPING_NAME = ?," +
                     " SHIPPING_ADD = ?," +
                     " SHIPPING_POSCODE = ?," +
                     " SHIPPING_CITY = ?," +
                     " SHIPPING_STATE = ?," +
                     " SHIPPING_COUNTRY = ?," +
                     " SHIPPING_CONTACTNO = ?,"+
                     " SHIPPING_COLLECTIONDATE = ?," +
                     " ORDER_UPDATEDBY = ?" +
                     " WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_NAME", OdbcType.VarChar, 250).Value = strNameBill;
            cmd.Parameters.Add("p_ORDER_ADD", OdbcType.VarChar, 250).Value = strAddBill;
            cmd.Parameters.Add("p_ORDER_POSCODE", OdbcType.VarChar, 20).Value = strPoscodeBill;
            cmd.Parameters.Add("p_ORDER_CITY", OdbcType.VarChar, 50).Value = strCityBill;
            cmd.Parameters.Add("p_ORDER_STATE", OdbcType.VarChar, 250).Value = strStateBill;
            cmd.Parameters.Add("p_ORDER_COUNTRY", OdbcType.VarChar, 10).Value = strCountryBill;
            cmd.Parameters.Add("p_ORDER_CONTACTNO", OdbcType.VarChar, 20).Value = strContactNoBill;
            cmd.Parameters.Add("p_SHIPPING_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_SHIPPING_ADD", OdbcType.VarChar, 250).Value = strAdd;
            cmd.Parameters.Add("p_SHIPPING_POSCODE", OdbcType.VarChar, 20).Value = strPoscode;
            cmd.Parameters.Add("p_SHIPPING_CITY", OdbcType.VarChar, 50).Value = strCity;
            cmd.Parameters.Add("p_SHIPPING_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_SHIPPING_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIPPING_CONTACTNO", OdbcType.VarChar, 20).Value = strContactNo;
            cmd.Parameters.Add("p_SHIPPING_COLLECTIONDATE", OdbcType.DateTime).Value = dtCollectionDate.ToString(clsAdmin.CONSTDATEFORMAT3);
            cmd.Parameters.Add("p_ORDER_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateOrderItem(int intOrdId, int intDetailId, int intProdId, int intProdQty, decimal decProdTotal)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDERDETAIL SET" +
                     " ORDDETAIL_PRODQTY = ?," +
                     " ORDDETAIL_PRODTOTAL = ?" +
                     " WHERE ORDER_ID = ?" +
                     " AND ORDDETAIL_ID = ?" +
                     " AND ORDDETAIL_PRODID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDDETAIL_PRODQTY", OdbcType.Int, 9).Value = intProdQty;
            cmd.Parameters.Add("p_ORDDETAIL_PRODTOTAL", OdbcType.Decimal).Value = decProdTotal;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intOrdId;
            cmd.Parameters.Add("p_ORDDETAIL_ID", OdbcType.BigInt, 9).Value = intDetailId;
            cmd.Parameters.Add("p_ORDDETAIL_PRODID", OdbcType.BigInt, 9).Value = intProdId;
            
            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
                ordDetailId = intDetailId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateOrderItem2(int intOrdId, int intDetailId, int intProdId, int intProdQty, decimal decProdTotal, decimal decProdGST)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDERDETAIL SET" +
                     " ORDDETAIL_PRODQTY = ?," +
                     " ORDDETAIL_PRODGST = ?," +
                     " ORDDETAIL_PRODTOTAL = ?" +
                     " WHERE ORDER_ID = ?" +
                     " AND ORDDETAIL_ID = ?" +
                     " AND ORDDETAIL_PRODID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDDETAIL_PRODQTY", OdbcType.Int, 9).Value = intProdQty;
            cmd.Parameters.Add("p_ORDDETAIL_PRODGST", OdbcType.Decimal).Value = decProdGST;
            cmd.Parameters.Add("p_ORDDETAIL_PRODTOTAL", OdbcType.Decimal).Value = decProdTotal;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intOrdId;
            cmd.Parameters.Add("p_ORDDETAIL_ID", OdbcType.BigInt, 9).Value = intDetailId;
            cmd.Parameters.Add("p_ORDDETAIL_PRODID", OdbcType.BigInt, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
                ordDetailId = intDetailId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateDeliveryDetails(int intId, string strCourier, string strConsignmentNo)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET" +
                     " SHIPPING_COMPANYNAME = ?," +
                     " SHIPPING_TRACKCODE = ?" +
                     " WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SHIPPING_COMPANYNAME", OdbcType.VarChar, 250).Value = strCourier;
            cmd.Parameters.Add("p_SHIPPING_TRACKCODE", OdbcType.VarChar, 250).Value = strConsignmentNo;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateDeliveryDetails(int intId, DateTime dtDate, int intCourier, string strCourier, string strConsignmentNo)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET" +
                     " SHIPPING_DATE = ?," +
                     " SHIPPING_COMPANY = ?," +
                     " SHIPPING_COMPANYNAME = ?," +
                     " SHIPPING_TRACKCODE = ?" +
                     " WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SHIPPING_DATE", OdbcType.DateTime).Value = dtDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_SHIPPING_COMPANY", OdbcType.Int, 9).Value = intCourier;
            cmd.Parameters.Add("p_SHIPPING_COMPANYNAME", OdbcType.VarChar, 250).Value = strCourier;
            cmd.Parameters.Add("p_SHIPPING_TRACKCODE", OdbcType.VarChar, 250).Value = strConsignmentNo;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateDeliveryDetails(int intId, int intCourier, string strCourier, string strConsignmentNo)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET" +
                     " SHIPPING_COMPANY = ?," +
                     " SHIPPING_COMPANYNAME = ?," +
                     " SHIPPING_TRACKCODE = ?" +
                     " WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SHIPPING_COMPANY", OdbcType.Int, 9).Value = intCourier;
            cmd.Parameters.Add("p_SHIPPING_COMPANYNAME", OdbcType.VarChar, 250).Value = strCourier;
            cmd.Parameters.Add("p_SHIPPING_TRACKCODE", OdbcType.VarChar, 250).Value = strConsignmentNo;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateOrderShipping(int intOrdId, decimal decShipping)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET ORDER_SHIPPING = ? " +
                     " WHERE ORDER_ID = ?";

            cmd.Parameters.Add("p_ORDER_SHIPPING", OdbcType.Decimal).Value = decShipping;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateOrderShipping2(int intOrdId, decimal decShipping, int intShippingCheck)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET " +
                     " ORDER_SHIPPING = ?, " +
                     " ORDER_SHIPPINGCHECK  = ?" +
                     " WHERE ORDER_ID = ?";

            cmd.Parameters.Add("p_ORDER_SHIPPING", OdbcType.Decimal).Value = decShipping;
            cmd.Parameters.Add("p_ORDER_SHIPPINGCHECK", OdbcType.Int, 1).Value = intShippingCheck;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int setOrderStatus()
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET" +
                     " ORDER_STATUSID = ?," + 
                     " ORDER_STATUS = ?" +
                     " WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_STATUSID", OdbcType.BigInt, 9).Value = ordStatusId;
            cmd.Parameters.Add("p_ORDER_STATUS", OdbcType.Int, 9).Value = ordStatus;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = ordId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int setOrderStatus(int intId, int intStatusId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET ORDER_STATUS = ? WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_STATUS", OdbcType.Int, 9).Value = intStatusId;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intId;
                ordStatusId = intStatusId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int setOrderStatus(int intId, int intStatusId, int intStatus)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET" +
                     " ORDER_STATUSID = ?," +
                     " ORDER_STATUS = ?" + 
                     " WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_STATUSID", OdbcType.BigInt, 9).Value = intStatusId;
            cmd.Parameters.Add("p_ORDER_STATUS", OdbcType.Int, 9).Value = intStatus;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intId;
                ordStatusId = intStatusId;
                ordStatus = intStatus;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int setOrderPayment()
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET ORDER_PAYMENT = ? WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_PAYMENT", OdbcType.Int, 9).Value = ordPaymentId;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = ordId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int setOrderPayment(int intId, int intPayId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET ORDER_PAYMENT = ? WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_PAYMENT", OdbcType.Int, 9).Value = intPayId;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intId;
                ordPaymentId = intPayId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int setDone(int intId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET ORDER_DONE = 1 WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int setCancel(int intId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET ORDER_CANCEL = 1 WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int setOrderInactive(int intOrdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET ORDER_ACTIVE = 0 WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteOrderDelivery(int intOrdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_ORDER WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteOrderItem(int intOrdId, int intDetailId, int intProdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_ORDERDETAIL WHERE ORDER_ID = ? AND ORDDETAIL_ID = ? AND ORDDETAIL_PRODID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intOrdId;
            cmd.Parameters.Add("p_ORDDETAIL_ID", OdbcType.BigInt, 9).Value = intDetailId;
            cmd.Parameters.Add("p_ORDDETAIL_PRODID", OdbcType.BigInt, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
                ordDetailId = intDetailId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteOrderItems(int intOrdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_ORDERDETAIL WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    #region "Order Status"
    public int addOrderStatus(int intOrdId, int intStatusId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ORDERSTATUS (" +
                     "ORDER_ID, " +
                     "STATUS_ID, " +
                     "STATUS_DATE" +
                     ") VALUES " +
                     "(?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intOrdId;
            cmd.Parameters.Add("p_STATUS_ID", OdbcType.Int, 9).Value = intStatusId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
                ordStatus = intStatusId;

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                ordStatusId = int.Parse(cmd2.ExecuteScalar().ToString());
            }
            else
            {
                trans.Rollback();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addOrderStatus2(int intOrdId, int intStatusId, DateTime dtOrdDate)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ORDERSTATUS (" +
                     "ORDER_ID, " +
                     "STATUS_ID, " +
                     "STATUS_DATE" +
                     ") VALUES " +
                     "(?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intOrdId;
            cmd.Parameters.Add("p_STATUS_ID", OdbcType.Int, 9).Value = intStatusId;
            cmd.Parameters.Add("p_STATUS_DATE", OdbcType.DateTime).Value = dtOrdDate;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
                ordStatus = intStatusId;

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                ordStatusId = int.Parse(cmd2.ExecuteScalar().ToString());
            }
            else
            {
                trans.Rollback();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isLastStatus(int intType, int intStatusOrder)
    {
        Boolean boolLast = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            DataSet ds = new DataSet();
            string strSql;

            strSql = "SELECT MAX(LIST_ORDER) FROM TB_LIST " +
                     "WHERE LIST_GROUP = 'ORDER STATUS' " +
                     "AND LIST_PARENTVALUE = ?";

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            cmd.Parameters.Add("p_LIST_PARENTVALUE", OdbcType.Int, 9).Value = intType;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count >= 1)
            {
                int intMax = int.Parse(ds.Tables[0].Rows[0][0].ToString());

                if (intStatusOrder == intMax)
                {
                    boolLast = true;
                }
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolLast;
    }

    public Boolean extractStatusById(int intId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDSTATUS_ID, A.ORDER_ID, A.STATUS_ID, A.V_STATUSNAME, A.STATUS_DATE, A.STATUS_ACTIVE " +
                     "FROM VW_ORDERSTATUS A " +
                     "WHERE A.ORDSTATUS_ID = ?";

            cmd.Parameters.Add("p_ORDSTATUS_ID", OdbcType.BigInt, 9).Value = intId;

            if (intActive != 0)
            {
                strSql += " AND A.STATUS_ACTIVE = ?";
                cmd.Parameters.Add("p_STATUS_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;
                ordStatusId = int.Parse(ds.Tables[0].Rows[0]["ORDSTATUS_ID"].ToString());
                ordId = int.Parse(ds.Tables[0].Rows[0]["ORDER_ID"].ToString());
                ordStatus = Convert.ToInt16(ds.Tables[0].Rows[0]["STATUS_ID"]);
                ordStatusName = ds.Tables[0].Rows[0]["V_STATUSNAME"].ToString();
                ordStatusDate = ds.Tables[0].Rows[0]["STATUS_DATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["STATUS_DATE"]) : DateTime.MaxValue;
                ordStatusActive = Convert.ToInt16(ds.Tables[0].Rows[0]["STATUS_ACTIVE"]);
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public DataSet getOrderStatus(int intOrdId)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDERSTATUS_NAME, A.ORDERSTATUS_ID, A.ORDERSTATUS_ORDER, A.ORDERSTATUS_GROUP," +
                     " A.ORDERSTATUS_PARENTVALUE, A.ORDERSTATUS_PARENTGROUP, A.ORDERSTATUS_ACTIVE," +
                     " B.ORDSTATUS_ID, B.ORDER_ID, B.STATUS_ID, B.V_STATUSNAME, B.STATUS_DATE, B.STATUS_ACTIVE" +
                     " FROM VW_ORDERSTATUSLIST A LEFT JOIN" +
                     " (SELECT * FROM VW_ORDERSTATUS" +
                     " WHERE ORDER_ID = ?" +
                     " AND STATUS_ACTIVE = 1) B" +
                     " ON A.ORDERSTATUS_ID = B.STATUS_ID";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public int getPrevStatus(int intId, int intCurStatusValue)
    {
        int intPrevStatus = 0;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            DataSet ds = new DataSet();
            string strSql;

            strSql = "SELECT ORDSTATUS_ID FROM VW_ORDERSTATUS" +
                     " WHERE STATUS_ID < ?" +
                     " AND STATUS_ACTIVE = 1" +
                     " AND ORDER_ID = ?";

            cmd.Parameters.Add("p_STATUS_ID", OdbcType.Int, 9).Value = intCurStatusValue;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intId;

            strSql += " ORDER BY STATUS_ID DESC LIMIT 1";

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            intPrevStatus = int.Parse(cmd.ExecuteScalar().ToString());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intPrevStatus;
    }

    public int setStatusInactive(int intId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDERSTATUS SET STATUS_ACTIVE = ?" +
                     " WHERE ORDSTATUS_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_STATUS_ACTIVE", OdbcType.Int, 1).Value = 0;
            cmd.Parameters.Add("p_ORDSTATUS_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordStatusId = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteStatusById(int intOrdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_ORDERSTATUS WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion

    #region "Payment"
    public int addOrderPayment(int intOrdId, int intPayGateway, decimal decPayTotal, string strPayRemarks)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PAYMENT (" +
                     "ORDER_ID, " +
                     "PAYMENT_GATEWAY, " +
                     "PAYMENT_REMARKS, " +
                     "PAYMENT_DATE, " +
                     "PAYMENT_TOTAL" +
                     ") VALUES " +
                     "(?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;
            cmd.Parameters.Add("p_PAYMENT_GATEWAY", OdbcType.Int, 9).Value = intPayGateway;
            cmd.Parameters.Add("p_PAYMENT_REMARKS", OdbcType.VarChar, 1000).Value = strPayRemarks;
            cmd.Parameters.Add("p_PAYMENT_TOTAL", OdbcType.Decimal).Value = decPayTotal;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                ordPaymentId = int.Parse(cmd2.ExecuteScalar().ToString());
            }
            else
            {
                trans.Rollback();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addOrderPayment(int intOrdId, int intPayGateway, decimal decPayAmount, int intBank, string strBankName, string strAccount, string strTransId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PAYMENT (" +
                     "ORDER_ID, " +
                     "PAYMENT_GATEWAY, " +
                     "PAYMENT_TOTAL, " +
                     "PAYMENT_BANK, " +
                     "PAYMENT_BANKNAME, " +
                     "PAYMENT_BANKACCOUNT, " +
                     "PAYMENT_TRANSID, " +
                     "PAYMENT_BANKDATE" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;
            cmd.Parameters.Add("p_PAYMENT_GATEWAY", OdbcType.Int, 9).Value = intPayGateway;
            cmd.Parameters.Add("p_PAYMENT_TOTAL", OdbcType.Decimal).Value = decPayAmount;
            cmd.Parameters.Add("p_PAYMENT_BANK", OdbcType.Int, 9).Value = intBank;
            cmd.Parameters.Add("p_PAYMENT_BANKNAME", OdbcType.VarChar, 250).Value = strBankName;
            cmd.Parameters.Add("p_PAYMENT_BANKACCOUNT", OdbcType.VarChar, 50).Value = strAccount;
            cmd.Parameters.Add("p_PAYMENT_TRANSID", OdbcType.VarChar, 250).Value = strTransId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                ordPaymentId = int.Parse(cmd2.ExecuteScalar().ToString());
            }
            else
            {
                trans.Rollback();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addOrderPayment2(int intOrdId, DateTime dtPayDate, int intPayGateway, decimal decPayTotal, string strPayDesc)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PAYMENT (" +
                     "ORDER_ID, " +
                     "PAYMENT_DATE, " +
                     "PAYMENT_GATEWAY, " +
                     "PAYMENT_REMARKS, " +
                     "PAYMENT_TOTAL" +
                     ") VALUES " +
                     "(?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;
            cmd.Parameters.Add("p_PAYMENT_DATE", OdbcType.DateTime).Value = dtPayDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_PAYMENT_GATEWAY", OdbcType.Int, 9).Value = intPayGateway;
            cmd.Parameters.Add("p_PAYMENT_REMARKS", OdbcType.VarChar, 1000).Value = strPayDesc;
            cmd.Parameters.Add("p_PAYMENT_TOTAL", OdbcType.Decimal).Value = decPayTotal;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                ordPaymentId = int.Parse(cmd2.ExecuteScalar().ToString());
            }
            else
            {
                trans.Rollback();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameDataSet(int intId, int intPayId, int intPayGateway, decimal decPayAmount, int intBank, string strBankName, string strBankAccount, string strTransId)
    {
        Boolean boolSame = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_PAYMENT" +
                     " WHERE ORDER_ID = ?" +
                     " AND PAYMENT_ID = ?" +
                     " AND PAYMENT_GATEWAY = ?" +
                     " AND PAYMENT_TOTAL = ?" +
                     " AND PAYMENT_BANK = ?" +
                     " AND BINARY PAYMENT_BANKNAME = ?" +
                     " AND BINARY PAYMENT_BANKACCOUNT = ?" +
                     " AND BINARY PAYMENT_TRANSID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intId;
            cmd.Parameters.Add("p_PAYMENT_ID", OdbcType.BigInt, 9).Value = intPayId;
            cmd.Parameters.Add("p_PAYMENT_GATEWAY", OdbcType.Int, 9).Value = intPayGateway;
            cmd.Parameters.Add("p_PAYMENT_TOTAL", OdbcType.Decimal).Value = decPayAmount;
            cmd.Parameters.Add("p_PAYMENT_BANK", OdbcType.Int, 9).Value = intBank;
            cmd.Parameters.Add("p_PAYMENT_BANKNAME", OdbcType.VarChar, 250).Value = strBankName;
            cmd.Parameters.Add("p_PAYMENT_BANKACCOUNT", OdbcType.VarChar, 50).Value = strBankAccount;
            cmd.Parameters.Add("p_PAYMENT_TRANSID", OdbcType.VarChar, 20).Value = strTransId;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int updatePayment(int intId, int intPayId, int intPayGateway, decimal decAmount, int intBank, string strBankName, string strAccount, string strTransId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAYMENT SET" +
                     " PAYMENT_GATEWAY = ?," +
                     " PAYMENT_TOTAL = ?," +
                     " PAYMENT_BANK = ?," +
                     " PAYMENT_BANKNAME = ?," +
                     " PAYMENT_BANKACCOUNT = ?," +
                     " PAYMENT_TRANSID = ?," +
                     " PAYMENT_BANKDATE = SYSDATE()" +
                     " WHERE ORDER_ID = ?" +
                     " AND PAYMENT_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAYMENT_GATEWAY", OdbcType.Int, 9).Value = intPayGateway;
            cmd.Parameters.Add("p_PAYMENT_TOTAL", OdbcType.Decimal).Value = decAmount;
            cmd.Parameters.Add("p_PAYMENT_BANK", OdbcType.Int, 9).Value = intBank;
            cmd.Parameters.Add("p_PAYMENT_BANKNAME", OdbcType.VarChar, 250).Value = strBankName;
            cmd.Parameters.Add("p_PAYMENT_BANKACCOUNT", OdbcType.VarChar, 50).Value = strAccount;
            cmd.Parameters.Add("p_PAYMENT_TRANSID", OdbcType.VarChar, 20).Value = strTransId;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intId;
            cmd.Parameters.Add("p_PAYMENT_ID", OdbcType.BigInt, 9).Value = intPayId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intId;
                ordPaymentId = intPayId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updatePaymentReject(int intId, int intPayId, string strRemarks)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAYMENT SET " +
                     "PAYMENT_REJECTREMARKS = ?, " +
                     "PAYMENT_REJECT = ? " +
                     "WHERE ORDER_ID = ? " +
                     "AND PAYMENT_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAYMENT_REJECTREMARKS", OdbcType.VarChar, 1000).Value = strRemarks;
            cmd.Parameters.Add("p_PAYMENT_REJECT", OdbcType.Int, 1).Value = 1;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intId;
            cmd.Parameters.Add("p_PAYMENT_ID", OdbcType.BigInt, 9).Value = intPayId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intId;
                ordPaymentId = intPayId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int setPaymentInactive(int intId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAYMENT SET PAYMENT_ACTIVE = ?" +
                     " WHERE PAYMENT_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAYMENT_ACTIVE", OdbcType.Int, 1).Value = 0;
            cmd.Parameters.Add("p_PAYMENT_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordPaymentId = intId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deletePaymentById(int intOrdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PAYMENT WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion

    #region "Sales"
    public int addSales(string strSalesNo, DateTime dtSalesDate, string strCustomerNo, int intMemId, string strMemCode, string strSalesCustomer, string strSalesCustomerCompName, string strSalesCustomerEmail, int intMemType, string strInvoiceRemarks, int intOrderType, string strSalesCompany, string strSalesName, string strSalesAdd, string strSalesEmail, string strSalesPoscode, string strSalesCity, string strSalesCountry, string strSalesState, string strSalesContact1, string strSalesContact2, string strSalesFax, string strShippingCompany, string strShippingName, string strShippingAdd, string strShippingEmail, string strShippingPoscode, string strShippingCity, string strShippingCountry, string strShippingState, string strShippingContact1, string strShippingContact2, string strShippingFax, int intBillShipSame, decimal decShipping)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ORDER (" +
                     "ORDER_NO, " +
                     "ORDER_CREATION, " +
                     "ORDER_CUSTOMERNO, " +
                     "MEM_ID, " +
                     "MEM_CODE, " +
                     "MEM_NAME, " +
                     "MEM_COMPANYNAME, " +
                     "MEM_EMAIL, " +
                     "MEM_TYPE, " +
                     "ORDER_REMARKS, " +
                     "ORDER_TYPE, " +
                     "ORDER_COMPANY, " +
                     "ORDER_NAME, " +
                     "ORDER_ADD, " +
                     "ORDER_EMAIL, " +
                     "ORDER_POSCODE, " +
                     "ORDER_CITY, " +
                     "ORDER_COUNTRY, " +
                     "ORDER_STATE, " +
                     "ORDER_CONTACTNO, " +
                     "ORDER_CONTACTNO2, " +
                     "ORDER_FAX, " +
                     "SHIPPING_CUSTCOMPANY, " +
                     "SHIPPING_NAME, " +
                     "SHIPPING_ADD, " +
                     "SHIPPING_EMAIL, " +
                     "SHIPPING_POSCODE, " +
                     "SHIPPING_CITY, " +
                     "SHIPPING_COUNTRY, " +
                     "SHIPPING_STATE, " +
                     "SHIPPING_CONTACTNO, " +
                     "SHIPPING_CONTACTNO2, " +
                     "SHIPPING_FAX, " +
                     "ORDER_BILLSHIPSAME, " +
                     "ORDER_SHIPPING" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_NO", OdbcType.VarChar, 20).Value = strSalesNo;
            cmd.Parameters.Add("p_ORDER_CREATION", OdbcType.DateTime).Value = dtSalesDate;
            cmd.Parameters.Add("p_ORDER_CUSTOMERNO", OdbcType.VarChar, 250).Value = strCustomerNo;
            cmd.Parameters.Add("p_MEM_ID", OdbcType.BigInt, 9).Value = intMemId;
            cmd.Parameters.Add("p_MEM_CODE", OdbcType.VarChar, 20).Value = strMemCode;
            cmd.Parameters.Add("p_MEM_NAME", OdbcType.VarChar, 250).Value = strSalesCustomer;
            cmd.Parameters.Add("p_MEM_COMPANYNAME", OdbcType.VarChar, 250).Value = strSalesCustomerCompName;
            cmd.Parameters.Add("p_MEM_EMAIL", OdbcType.VarChar, 250).Value = strSalesCustomerEmail;
            cmd.Parameters.Add("p_MEM_TYPE", OdbcType.Int, 1).Value = intMemType;
            cmd.Parameters.Add("p_ORDER_REMARKS", OdbcType.Text).Value = strInvoiceRemarks;
            cmd.Parameters.Add("p_ORDER_TYPE", OdbcType.BigInt, 9).Value = intOrderType;

            cmd.Parameters.Add("p_ORDER_COMPANY", OdbcType.VarChar, 250).Value = strSalesCompany;
            cmd.Parameters.Add("p_ORDER_NAME", OdbcType.VarChar, 250).Value = strSalesName;
            cmd.Parameters.Add("p_ORDER_ADD", OdbcType.VarChar, 500).Value = strSalesAdd;
            cmd.Parameters.Add("p_ORDER_EMAIL", OdbcType.VarChar, 250).Value = strSalesEmail;
            cmd.Parameters.Add("p_ORDER_POSCODE", OdbcType.VarChar, 20).Value = strSalesPoscode;
            cmd.Parameters.Add("p_ORDER_CITY", OdbcType.VarChar, 50).Value = strSalesCity;
            cmd.Parameters.Add("p_ORDER_COUNTRY", OdbcType.VarChar, 10).Value = strSalesCountry;
            cmd.Parameters.Add("p_ORDER_STATE", OdbcType.VarChar, 250).Value = strSalesState;
            cmd.Parameters.Add("p_ORDER_CONTACTNO", OdbcType.VarChar, 20).Value = strSalesContact1;
            cmd.Parameters.Add("p_ORDER_CONTACTNO2", OdbcType.VarChar, 20).Value = strSalesContact2;
            cmd.Parameters.Add("p_ORDER_FAX", OdbcType.VarChar, 20).Value = strSalesFax;

            cmd.Parameters.Add("p_SHIPPING_CUSTCOMPANY", OdbcType.VarChar, 250).Value = strShippingCompany;
            cmd.Parameters.Add("p_SHIPPING_NAME", OdbcType.VarChar, 250).Value = strShippingName;
            cmd.Parameters.Add("p_SHIPPING_ADD", OdbcType.VarChar, 500).Value = strShippingAdd;
            cmd.Parameters.Add("p_SHIPPING_EMAIL", OdbcType.VarChar, 250).Value = strShippingEmail;
            cmd.Parameters.Add("p_SHIPPING_POSCODE", OdbcType.VarChar, 20).Value = strShippingPoscode;
            cmd.Parameters.Add("p_SHIPPING_CITY", OdbcType.VarChar, 50).Value = strShippingCity;
            cmd.Parameters.Add("p_SHIPPING_COUNTRY", OdbcType.VarChar, 10).Value = strShippingCountry;
            cmd.Parameters.Add("p_SHIPPING_STATE", OdbcType.VarChar, 250).Value = strShippingState;
            cmd.Parameters.Add("p_SHIPPING_CONTACTNO", OdbcType.VarChar, 20).Value = strShippingContact1;
            cmd.Parameters.Add("p_SHIPPING_CONTACTNO2", OdbcType.VarChar, 20).Value = strShippingContact2;
            cmd.Parameters.Add("p_SHIPPING_FAX", OdbcType.VarChar, 20).Value = strShippingFax;

            cmd.Parameters.Add("p_ORDER_BILLSHIPSAME", OdbcType.Int, 1).Value = intBillShipSame;
            cmd.Parameters.Add("p_ORDER_SHIPPING", OdbcType.Decimal).Value = decShipping;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                ordId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addOrdItems(int intOrdId, int intProdId, string strProdCode, string strProdName, string strProdDesc, int intProdWeight, decimal decProdPrice, int intProdQty, decimal decProdTotal)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ORDERDETAIL (" +
                     "ORDER_ID, " +
                     "ORDDETAIL_PRODID, " +
                     "ORDDETAIL_PRODCODE, " +
                     "ORDDETAIL_PRODNAME, " +
                     "ORDDETAIL_PRODSNAPSHOT, " +
                     "ORDDETAIL_PRODWEIGHT, " +
                     "ORDDETAIL_PRODPRICE, " +
                     "ORDDETAIL_PRODQTY, " +
                     "ORDDETAIL_PRODTOTAL" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intOrdId;
            cmd.Parameters.Add("p_ORDDETAIL_PRODID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_ORDDETAIL_PRODCODE", OdbcType.VarChar, 20).Value = strProdCode;
            cmd.Parameters.Add("p_ORDDETAIL_PRODNAME", OdbcType.VarChar, 250).Value = strProdName;
            cmd.Parameters.Add("p_ORDDETAIL_PRODSNAPSHOT", OdbcType.VarChar, 1000).Value = strProdDesc;
            cmd.Parameters.Add("p_ORDDETAIL_PRODWEIGHT", OdbcType.Int, 9).Value = intProdWeight;
            cmd.Parameters.Add("p_ORDDETAIL_PRODPRICE", OdbcType.Decimal).Value = decProdPrice;
            cmd.Parameters.Add("p_ORDDETAIL_PRODQTY", OdbcType.Int, 9).Value = intProdQty;
            cmd.Parameters.Add("p_ORDDETAIL_PRODTOTAL", OdbcType.Decimal).Value = decProdTotal;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addOrderRemarks(int intOrdId, string strRemarks)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ORDERREMARKS (" +
                     "ORDER_ID, " +
                     "ORDREMARKS_REMARK" +
                     ") VALUES " +
                     "(?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intOrdId;
            cmd.Parameters.Add("p_ORDREMARKS_REMARK", OdbcType.VarChar, 1000).Value = strRemarks;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameDataSet(int intOrdDetailId, int intProdId, int intProdQty)
    {
        Boolean boolSame = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_ORDERDETAIL" +
                     " WHERE ORDDETAIL_ID = ?" +
                     " AND ORDDETAIL_PRODID = ?" +
                     " AND ORDDETAIL_PRODQTY = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDDETAIL_ID", OdbcType.BigInt, 9).Value = intOrdDetailId;
            cmd.Parameters.Add("p_ORDDETAIL_PRODID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_ORDDETAIL_PRODQTY", OdbcType.Int, 9).Value = intProdQty;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int updateOrdItems(int intOrdDetailId, int intProdQty, decimal decProdTotal)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDERDETAIL SET" +
                     " ORDDETAIL_PRODQTY = ?," +
                     " ORDDETAIL_PRODTOTAL = ?" +
                     " WHERE ORDDETAIL_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDDETAIL_PRODQTY", OdbcType.Int, 9).Value = intProdQty;
            cmd.Parameters.Add("p_ORDDETAIL_PRODTOTAL", OdbcType.Decimal).Value = decProdTotal;
            cmd.Parameters.Add("p_ORDDETAIL_ID", OdbcType.BigInt, 9).Value = intOrdDetailId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                //ordId = intOrdDetailId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameOrderSet2(int intOrdId, string strSalesNo, DateTime dtSalesDate, string strCustomerNo, int intMemId, string strMemCode, string strMemName, string strMemCompName, string strMemEmail, string strInvoiceRemarks, 
                                        string strBillCompany, string strBillName, string strBillAdd, string strBillEmail, string strBillPoscode, string strBillCity, string strBillCountry, string strBillState, string strBillTel1, string strBillTel2, string strBillFax,
                                        string strShipCompany, string strShipName, string strShipAdd, string strShipEmail, string strShipPoscode, string strShipCity, string strShipCountry, string strShipState, string strShipTel1, string strShipTel2, string strShipFax, int intBillShipSame, decimal decShipping)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_ORDER" +
                     " WHERE ORDER_ID = ? " +
                     " AND ORDER_NO = ? " +
                     " AND ORDER_CREATION = ? " +
                     " AND ORDER_CUSTOMERNO = ? " +
                     " AND MEM_ID = ? " +
                     " AND MEM_CODE = ? " +
                     " AND MEM_NAME = ? " +
                     " AND MEM_COMPANYNAME = ? " +
                     " AND MEM_EMAIL = ? " +
                     " AND ORDER_REMARKS = ? " +
                     " AND ORDER_COMPANY = ? " +
                     " AND ORDER_NAME = ? " +
                     " AND ORDER_ADD = ? " +
                     " AND ORDER_EMAIL = ? " +
                     " AND ORDER_POSCODE = ? " +
                     " AND ORDER_CITY = ? " +
                     " AND ORDER_COUNTRY = ? " +
                     " AND ORDER_STATE = ? " +
                     " AND ORDER_CONTACTNO = ? " +
                     " AND ORDER_CONTACTNO2 = ? " +
                     " AND ORDER_FAX = ? " +
                     " AND SHIPPING_CUSTCOMPANY = ? " +
                     " AND SHIPPING_NAME = ? " +
                     " AND SHIPPING_ADD = ? " +
                     " AND SHIPPING_EMAIL = ? " +
                     " AND SHIPPING_POSCODE = ? " +
                     " AND SHIPPING_CITY = ? " +
                     " AND SHIPPING_COUNTRY = ? " +
                     " AND SHIPPING_STATE = ? " +
                     " AND SHIPPING_CONTACTNO = ? " +
                     " AND SHIPPING_CONTACTNO2 = ? " +
                     " AND SHIPPING_FAX = ? " +
                     " AND ORDER_BILLSHIPSAME = ? " +
                     " AND ORDER_SHIPPING = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value  = intOrdId;
            cmd.Parameters.Add("p_ORDER_NO", OdbcType.VarChar, 20).Value = strSalesNo;
            cmd.Parameters.Add("p_ORDER_CREATION", OdbcType.DateTime).Value = dtSalesDate;
            cmd.Parameters.Add("p_ORDER_CUSTOMERNO", OdbcType.VarChar, 250).Value = strCustomerNo;
            cmd.Parameters.Add("p_MEM_ID", OdbcType.BigInt, 9).Value = intMemId;
            cmd.Parameters.Add("p_MEM_CODE", OdbcType.VarChar, 20).Value = strMemCode;
            cmd.Parameters.Add("p_MEM_NAME", OdbcType.VarChar, 250).Value = strMemName;
            cmd.Parameters.Add("p_MEM_COMPANYNAME", OdbcType.VarChar, 250).Value = strMemCompName;
            cmd.Parameters.Add("p_MEM_EMAIL", OdbcType.VarChar, 250).Value = strMemEmail;
            cmd.Parameters.Add("p_ORDER_REMARKS", OdbcType.VarChar, 1000).Value = strInvoiceRemarks;
            cmd.Parameters.Add("p_ORDER_COMPANY", OdbcType.VarChar, 250).Value = strBillCompany;
            cmd.Parameters.Add("p_ORDER_NAME", OdbcType.VarChar, 250).Value = strBillName;
            cmd.Parameters.Add("p_ORDER_ADD", OdbcType.VarChar, 500).Value = strBillAdd;
            cmd.Parameters.Add("p_ORDER_EMAIL", OdbcType.VarChar, 250).Value = strBillEmail;
            cmd.Parameters.Add("p_ORDER_POSCODE", OdbcType.VarChar, 20).Value = strBillPoscode;
            cmd.Parameters.Add("p_ORDER_CITY", OdbcType.VarChar, 50).Value = strBillCity;
            cmd.Parameters.Add("p_ORDER_COUNTRY", OdbcType.VarChar, 10).Value = strBillCountry;
            cmd.Parameters.Add("p_ORDER_STATE", OdbcType.VarChar, 250).Value = strBillState;
            cmd.Parameters.Add("p_ORDER_CONTACTNO", OdbcType.VarChar, 20).Value = strBillTel1;
            cmd.Parameters.Add("p_ORDER_CONTACTNO2", OdbcType.VarChar, 20).Value = strBillTel2;
            cmd.Parameters.Add("p_ORDER_FAX", OdbcType.VarChar, 20).Value = strBillFax;
            cmd.Parameters.Add("p_SHIPPING_CUSTCOMPANY", OdbcType.VarChar, 250).Value = strShipCompany;
            cmd.Parameters.Add("p_SHIPPING_NAME", OdbcType.VarChar, 250).Value = strShipName;
            cmd.Parameters.Add("p_SHIPPING_ADD", OdbcType.VarChar, 250).Value = strShipAdd;
            cmd.Parameters.Add("p_SHIPPING_EMAIL", OdbcType.VarChar, 250).Value = strShipEmail;
            cmd.Parameters.Add("p_SHIPPING_POSCODE", OdbcType.VarChar, 20).Value = strShipPoscode;
            cmd.Parameters.Add("p_SHIPPING_CITY", OdbcType.VarChar, 50).Value = strShipCity;
            cmd.Parameters.Add("p_SHIPPING_COUNTRY", OdbcType.VarChar, 10).Value = strShipCountry;
            cmd.Parameters.Add("p_SHIPPING_STATE", OdbcType.VarChar, 250).Value = strShipState;
            cmd.Parameters.Add("p_SHIPPING_CONTACTNO", OdbcType.VarChar, 20).Value = strShipTel1;
            cmd.Parameters.Add("p_SHIPPING_CONTACTNO2", OdbcType.VarChar, 20).Value = strShipTel2;
            cmd.Parameters.Add("p_SHIPPING_FAX", OdbcType.VarChar, 20).Value = strShipFax;
            cmd.Parameters.Add("p_ORDER_BILLSHIPSAME", OdbcType.Int, 1).Value = intBillShipSame;
            cmd.Parameters.Add("p_ORDER_SHIPPING", OdbcType.Decimal).Value = decShipping;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateOrderById(int intOrdId, string strSalesNo, DateTime dtSalesDate, string strCustomerNo, int intMemId, string strMemCode, string strMemName, string strMemCompName, string strMemEmail, string strInvoiceRemarks,
                               string strBillCompany, string strBillName, string strBillAdd, string strBillEmail, string strBillPoscode, string strBillCity, string strBillCountry, string strBillState, string strBillTel1, string strBillTel2, string strBillFax,
                               string strShipCompany, string strShipName, string strShipAdd, string strShipEmail, string strShipPoscode, string strShipCity, string strShipCountry, string strShipState, string strShipTel1, string strShipTel2, string strShipFax, int intBillShipSame, decimal decShipping, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET" +
                     " ORDER_NO = ?, " +
                     " ORDER_CREATION = ?, " +
                     " ORDER_CUSTOMERNO = ?, " +
                     " MEM_ID = ?, " +
                     " MEM_CODE = ?, " +
                     " MEM_NAME = ?, " +
                     " MEM_COMPANYNAME = ?, " +
                     " MEM_EMAIL = ?, " +
                     " ORDER_REMARKS = ?, " +
                     " ORDER_COMPANY = ?, " +
                     " ORDER_NAME = ?, " +
                     " ORDER_ADD = ?, " +
                     " ORDER_EMAIL = ?, " +
                     " ORDER_POSCODE = ?, " +
                     " ORDER_CITY = ?, " +
                     " ORDER_COUNTRY = ?, " +
                     " ORDER_STATE = ?, " +
                     " ORDER_CONTACTNO = ?, " +
                     " ORDER_CONTACTNO2 = ?, " +
                     " ORDER_FAX = ?, " +
                     " SHIPPING_CUSTCOMPANY = ?, " +
                     " SHIPPING_NAME = ?, " +
                     " SHIPPING_ADD = ?, " +
                     " SHIPPING_EMAIL = ?, " +
                     " SHIPPING_POSCODE = ?, " +
                     " SHIPPING_CITY = ?, " +
                     " SHIPPING_COUNTRY = ?, " +
                     " SHIPPING_STATE = ?, " +
                     " SHIPPING_CONTACTNO = ?, " +
                     " SHIPPING_CONTACTNO2 = ?, " +
                     " SHIPPING_FAX = ?, " +
                     " ORDER_BILLSHIPSAME = ?, " +
                     " ORDER_SHIPPING = ?, " +
                     " ORDER_UPDATEDBY = ?," +
                     " ORDER_LASTUPDATE = SYSDATE()" +
                     " WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_NO", OdbcType.VarChar, 20).Value = strSalesNo;
            cmd.Parameters.Add("p_ORDER_CREATION", OdbcType.DateTime).Value = dtSalesDate;
            cmd.Parameters.Add("p_ORDER_CUSTOMERNO", OdbcType.VarChar, 250).Value = strCustomerNo;
            cmd.Parameters.Add("p_MEM_ID", OdbcType.BigInt, 9).Value = intMemId;
            cmd.Parameters.Add("p_MEM_CODE", OdbcType.VarChar, 20).Value = strMemCode;
            cmd.Parameters.Add("p_MEM_NAME", OdbcType.VarChar, 250).Value = strMemName;
            cmd.Parameters.Add("p_MEM_COMPANYNAME", OdbcType.VarChar, 250).Value = strMemCompName;
            cmd.Parameters.Add("p_MEM_EMAIL", OdbcType.VarChar, 250).Value = strMemEmail;
            cmd.Parameters.Add("p_ORDER_REMARKS", OdbcType.VarChar, 1000).Value = strInvoiceRemarks;
            cmd.Parameters.Add("p_ORDER_COMPANY", OdbcType.VarChar, 250).Value = strBillCompany;
            cmd.Parameters.Add("p_ORDER_NAME", OdbcType.VarChar, 250).Value = strBillName;
            cmd.Parameters.Add("p_ORDER_ADD", OdbcType.VarChar, 500).Value = strBillAdd;
            cmd.Parameters.Add("p_ORDER_EMAIL", OdbcType.VarChar, 250).Value = strBillEmail;
            cmd.Parameters.Add("p_ORDER_POSCODE", OdbcType.VarChar, 20).Value = strBillPoscode;
            cmd.Parameters.Add("p_ORDER_CITY", OdbcType.VarChar, 50).Value = strBillCity;
            cmd.Parameters.Add("p_ORDER_COUNTRY", OdbcType.VarChar, 10).Value = strBillCountry;
            cmd.Parameters.Add("p_ORDER_STATE", OdbcType.VarChar, 250).Value = strBillState;
            cmd.Parameters.Add("p_ORDER_CONTACTNO", OdbcType.VarChar, 20).Value = strBillTel1;
            cmd.Parameters.Add("p_ORDER_CONTACTNO2", OdbcType.VarChar, 20).Value = strBillTel2;
            cmd.Parameters.Add("p_ORDER_FAX", OdbcType.VarChar, 20).Value = strBillFax;
            cmd.Parameters.Add("p_SHIPPING_CUSTCOMPANY", OdbcType.VarChar, 250).Value = strShipCompany;
            cmd.Parameters.Add("p_SHIPPING_NAME", OdbcType.VarChar, 250).Value = strShipName;
            cmd.Parameters.Add("p_SHIPPING_ADD", OdbcType.VarChar, 250).Value = strShipAdd;
            cmd.Parameters.Add("p_SHIPPING_EMAIL", OdbcType.VarChar, 250).Value = strShipEmail;
            cmd.Parameters.Add("p_SHIPPING_POSCODE", OdbcType.VarChar, 20).Value = strShipPoscode;
            cmd.Parameters.Add("p_SHIPPING_CITY", OdbcType.VarChar, 50).Value = strShipCity;
            cmd.Parameters.Add("p_SHIPPING_COUNTRY", OdbcType.VarChar, 10).Value = strShipCountry;
            cmd.Parameters.Add("p_SHIPPING_STATE", OdbcType.VarChar, 250).Value = strShipState;
            cmd.Parameters.Add("p_SHIPPING_CONTACTNO", OdbcType.VarChar, 20).Value = strShipTel1;
            cmd.Parameters.Add("p_SHIPPING_CONTACTNO2", OdbcType.VarChar, 20).Value = strShipTel2;
            cmd.Parameters.Add("p_SHIPPING_FAX", OdbcType.VarChar, 20).Value = strShipFax;
            cmd.Parameters.Add("p_ORDER_BILLSHIPSAME", OdbcType.Int, 1).Value = intBillShipSame;
            cmd.Parameters.Add("p_ORDER_SHIPPING", OdbcType.Decimal).Value = decShipping;
            cmd.Parameters.Add("p_ORDER_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intOrdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractOrderById2(int intOrdId)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDER_ID, A.ORDER_NO " +
                     "FROM TB_ORDER A " +
                     "WHERE A.ORDER_ID = ?";

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;
                ordId = int.Parse(ds.Tables[0].Rows[0]["ORDER_ID"].ToString());
                ordNo = ds.Tables[0].Rows[0]["ORDER_NO"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int deleteOrderRemarks(int intOrdId, string strRemarks)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_ORDERREMARKS" +
                     " WHERE ORDER_ID = ?" +
                     " AND ORDREMARKS_REMARK = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intOrdId;
            cmd.Parameters.Add("p_ORDREMARKS_REMARK", OdbcType.VarChar, 1000).Value = strRemarks;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteOrder(int intOrdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_ORDER WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getOrdRemarksList(int intOrdId)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.ORDREMARKS_ID, A.ORDER_ID, A.ORDREMARKS_REMARK" +
                     " FROM TB_ORDERREMARKS A WHERE A.ORDER_ID = ?";

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intOrdId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean extractOrderById3(int intOrdId)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDER_ID, A.MEM_ID, A.MEM_CODE, A.MEM_NAME, A.MEM_EMAIL, A.MEM_TYPE, A.ORDER_NO, A.ORDER_CREATION, A.ORDER_CUSTOMERNO, A.ORDER_DUEDATE, A.ORDER_REMARKS, " +
                     "A.ORDER_READ, A.ORDER_TYPE, A.ORDER_STATUSID, A.ORDER_STATUS, " +
                     "A.ORDER_COMPANY, A.ORDER_NAME, A.ORDER_ADD, A.ORDER_POSCODE, A.ORDER_CITY, A.ORDER_STATE, A.ORDER_COUNTRY, A.ORDER_CONTACTNO, A.ORDER_CONTACTNO2, A.ORDER_FAX, A.ORDER_EMAIL, " +
                     "A.SHIPPING_CUSTCOMPANY, A.SHIPPING_NAME, A.SHIPPING_ADD, A.SHIPPING_POSCODE, A.SHIPPING_CITY, A.SHIPPING_STATE, A.SHIPPING_COUNTRY, A.SHIPPING_CONTACTNO, A.SHIPPING_CONTACTNO2, A.SHIPPING_FAX, A.SHIPPING_EMAIL, " +
                     "A.SHIPPING_REMARKS, A.SHIPPING_DATE, A.SHIPPING_TRACKCODE, A.SHIPPING_COMPANY, A.SHIPPING_COMPANYNAME, A.ORDER_PICKUPPOINT, A.ORDER_PICKUPPOINTNAME, A.ORDER_SHIPPINGCHECK, A.ORDER_SHIPPING, " +
                     "A.ORDER_BILLSHIPSAME, A.ORDER_PAYMENT, A.ORDER_DONE, A.ORDER_CANCEL, A.ORDER_ACTIVE, A.ORDER_UPDATEDBY, A.ORDER_LASTUPDATE, " +
                     "((SELECT SUM(ORDDETAIL_PRODTOTAL) FROM TB_ORDERDETAIL WHERE ORDER_ID = A.ORDER_ID) + A.ORDER_SHIPPING) AS V_ORDERTOTAL, (SELECT PAYMENT_ID FROM TB_PAYMENT WHERE ORDER_ID = A.ORDER_ID  ORDER BY PAYMENT_ID DESC LIMIT 1) AS V_PAYMENTID " +
                     "FROM TB_ORDER A " +
                     "WHERE A.ORDER_ID = ?";

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;
                ordId = int.Parse(ds.Tables[0].Rows[0]["ORDER_ID"].ToString());
                memId = int.Parse(ds.Tables[0].Rows[0]["MEM_ID"].ToString());
                memCode = ds.Tables[0].Rows[0]["MEM_CODE"].ToString();
                memName = ds.Tables[0].Rows[0]["MEM_NAME"].ToString();
                memEmail = ds.Tables[0].Rows[0]["MEM_EMAIL"].ToString();
                memType = int.Parse(ds.Tables[0].Rows[0]["MEM_TYPE"].ToString());
                ordNo = ds.Tables[0].Rows[0]["ORDER_NO"].ToString();
                ordCreation = ds.Tables[0].Rows[0]["ORDER_CREATION"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["ORDER_CREATION"]) : DateTime.MaxValue;
                customerNo = ds.Tables[0].Rows[0]["ORDER_CUSTOMERNO"].ToString();
                ordDueDate = ds.Tables[0].Rows[0]["ORDER_DUEDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["ORDER_DUEDATE"]) : DateTime.MaxValue;
                ordRemarks = ds.Tables[0].Rows[0]["ORDER_REMARKS"].ToString();
                ordRead = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_READ"]);
                ordType = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_TYPE"]);
                ordStatusId = int.Parse(ds.Tables[0].Rows[0]["ORDER_STATUSID"].ToString());
                ordStatus = int.Parse(ds.Tables[0].Rows[0]["ORDER_STATUS"].ToString());
                ordBillShipSame = int.Parse(ds.Tables[0].Rows[0]["ORDER_BILLSHIPSAME"].ToString());

                company = ds.Tables[0].Rows[0]["ORDER_COMPANY"].ToString();
                name = ds.Tables[0].Rows[0]["ORDER_NAME"].ToString();
                add = ds.Tables[0].Rows[0]["ORDER_ADD"].ToString();
                poscode = ds.Tables[0].Rows[0]["ORDER_POSCODE"].ToString();
                city = ds.Tables[0].Rows[0]["ORDER_CITY"].ToString();
                state = ds.Tables[0].Rows[0]["ORDER_STATE"].ToString();
                country = ds.Tables[0].Rows[0]["ORDER_COUNTRY"].ToString();
                contactNo = ds.Tables[0].Rows[0]["ORDER_CONTACTNO"].ToString();
                contactNo2 = ds.Tables[0].Rows[0]["ORDER_CONTACTNO2"].ToString();
                fax = ds.Tables[0].Rows[0]["ORDER_FAX"].ToString();
                email = ds.Tables[0].Rows[0]["ORDER_EMAIL"].ToString();

                shippingCustCompany = ds.Tables[0].Rows[0]["SHIPPING_CUSTCOMPANY"].ToString();
                shippingName = ds.Tables[0].Rows[0]["SHIPPING_NAME"].ToString();
                shippingAdd = ds.Tables[0].Rows[0]["SHIPPING_ADD"].ToString();
                shippingPoscode = ds.Tables[0].Rows[0]["SHIPPING_POSCODE"].ToString();
                shippingCity = ds.Tables[0].Rows[0]["SHIPPING_CITY"].ToString();
                shippingState = ds.Tables[0].Rows[0]["SHIPPING_STATE"].ToString();
                shippingCountry = ds.Tables[0].Rows[0]["SHIPPING_COUNTRY"].ToString();
                shippingContactNo = ds.Tables[0].Rows[0]["SHIPPING_CONTACTNO"].ToString();
                shippingContactNo2 = ds.Tables[0].Rows[0]["SHIPPING_CONTACTNO2"].ToString();
                shippingFax = ds.Tables[0].Rows[0]["SHIPPING_FAX"].ToString();
                shippingEmail = ds.Tables[0].Rows[0]["SHIPPING_EMAIL"].ToString();
                shippingRemarks = ds.Tables[0].Rows[0]["SHIPPING_REMARKS"].ToString();
                shippingTrackCode = ds.Tables[0].Rows[0]["SHIPPING_TRACKCODE"].ToString();
                shippingDate = ds.Tables[0].Rows[0]["SHIPPING_DATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["SHIPPING_DATE"]) : DateTime.MaxValue;
                shippingCompany = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIPPING_COMPANY"]);
                shippingCompanyName = ds.Tables[0].Rows[0]["SHIPPING_COMPANYNAME"].ToString();
                pickupPoint = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_PICKUPPOINT"]);
                pickupPointName = ds.Tables[0].Rows[0]["ORDER_PICKUPPOINTNAME"].ToString();

                ordShippingCheck = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_SHIPPINGCHECK"]);
                ordShipping = Convert.ToDecimal(ds.Tables[0].Rows[0]["ORDER_SHIPPING"]);
                ordTotal = ds.Tables[0].Rows[0]["V_ORDERTOTAL"] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0]["V_ORDERTOTAL"]) : 0;

                //ordPaymentId = int.Parse(ds.Tables[0].Rows[0]["V_PAYMENTID"].ToString());
                ordPaymentId = ds.Tables[0].Rows[0]["V_PAYMENTID"] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0]["V_PAYMENTID"]) : 0;
                ordDone = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_DONE"]);
                ordCancel = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_CANCEL"]);
                ordActive = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_ACTIVE"]);
                ordUpdatedBy = int.Parse(ds.Tables[0].Rows[0]["ORDER_UPDATEDBY"].ToString());
                ordLastUpdate = ds.Tables[0].Rows[0]["ORDER_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["ORDER_LASTUPDATE"]) : DateTime.MaxValue;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractOrderById4(int intOrdId)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDER_ID, A.ORDER_NO, A.ORDER_CREATION, A.ORDER_CUSTOMERNO, A.ORDER_REMARKS, A.ORDER_TYPE, " +
                     "A.ORDER_COMPANY, A.ORDER_NAME, A.ORDER_ADD, A.ORDER_POSCODE, A.ORDER_CITY, A.ORDER_STATE, A.ORDER_COUNTRY, " +
                     "(SELECT COUNTRY_NAME FROM TB_COUNTRY WHERE COUNTRY_CODE = A.ORDER_COUNTRY) AS V_BILLCOUNTRYNAME, " +
                     "A.ORDER_CONTACTNO, A.ORDER_CONTACTNO2, A.ORDER_FAX, A.ORDER_EMAIL, " +
                     "A.SHIPPING_CUSTCOMPANY, A.SHIPPING_NAME, A.SHIPPING_ADD, A.SHIPPING_POSCODE, A.SHIPPING_CITY, A.SHIPPING_STATE, A.SHIPPING_COUNTRY, " +
                     "(SELECT COUNTRY_NAME FROM TB_COUNTRY WHERE COUNTRY_CODE = A.SHIPPING_COUNTRY) AS V_SHIPCOUNTRYNAME, " +
                     "A.SHIPPING_CONTACTNO, A.SHIPPING_CONTACTNO2, A.SHIPPING_FAX, A.SHIPPING_EMAIL, " +
                     "A.SHIPPING_REMARKS, A.SHIPPING_DATE, A.SHIPPING_TRACKCODE, A.SHIPPING_COMPANY, A.SHIPPING_COMPANYNAME, A.ORDER_SHIPPING, A.ORDER_ACTIVE, " +
                     "((SELECT SUM(ORDDETAIL_PRODTOTAL) FROM TB_ORDERDETAIL WHERE ORDER_ID = A.ORDER_ID) + A.ORDER_SHIPPING) AS V_ORDERTOTAL, " +
                     "A.ORDER_DLVSERVICE, A.SHIPPING_COLLECTIONDATE, A.DELIVERY_DATE,A.DELIVERY_TIME, A.DELIVERY_RECIPIENT, A.DELIVERY_SENDER, A.DELIVERY_MESSAGE,A.ORDER_STATUS " +
                     "FROM TB_ORDER A " +
                     "WHERE A.ORDER_ID = ?";

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;
                ordId = int.Parse(ds.Tables[0].Rows[0]["ORDER_ID"].ToString());
                ordNo = ds.Tables[0].Rows[0]["ORDER_NO"].ToString();
                ordCreation = ds.Tables[0].Rows[0]["ORDER_CREATION"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["ORDER_CREATION"]) : DateTime.MaxValue;
                customerNo = ds.Tables[0].Rows[0]["ORDER_CUSTOMERNO"].ToString();
                ordRemarks = ds.Tables[0].Rows[0]["ORDER_REMARKS"].ToString();
                ordType = int.Parse(ds.Tables[0].Rows[0]["ORDER_TYPE"].ToString());
                ordStatus = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_STATUS"]);

                company = ds.Tables[0].Rows[0]["ORDER_COMPANY"].ToString();
                name = ds.Tables[0].Rows[0]["ORDER_NAME"].ToString();
                add = ds.Tables[0].Rows[0]["ORDER_ADD"].ToString();
                poscode = ds.Tables[0].Rows[0]["ORDER_POSCODE"].ToString();
                city = ds.Tables[0].Rows[0]["ORDER_CITY"].ToString();
                state = ds.Tables[0].Rows[0]["ORDER_STATE"].ToString();
                country = ds.Tables[0].Rows[0]["ORDER_COUNTRY"].ToString();
                countryName = ds.Tables[0].Rows[0]["V_BILLCOUNTRYNAME"].ToString();
                contactNo = ds.Tables[0].Rows[0]["ORDER_CONTACTNO"].ToString();
                contactNo2 = ds.Tables[0].Rows[0]["ORDER_CONTACTNO2"].ToString();
                fax = ds.Tables[0].Rows[0]["ORDER_FAX"].ToString();
                email = ds.Tables[0].Rows[0]["ORDER_EMAIL"].ToString();

                shippingCustCompany = ds.Tables[0].Rows[0]["SHIPPING_CUSTCOMPANY"].ToString();
                shippingName = ds.Tables[0].Rows[0]["SHIPPING_NAME"].ToString();
                shippingAdd = ds.Tables[0].Rows[0]["SHIPPING_ADD"].ToString();
                shippingPoscode = ds.Tables[0].Rows[0]["SHIPPING_POSCODE"].ToString();
                shippingCity = ds.Tables[0].Rows[0]["SHIPPING_CITY"].ToString();
                shippingState = ds.Tables[0].Rows[0]["SHIPPING_STATE"].ToString();
                shippingCountry = ds.Tables[0].Rows[0]["SHIPPING_COUNTRY"].ToString();
                shippingCountryName = ds.Tables[0].Rows[0]["V_SHIPCOUNTRYNAME"].ToString();
                shippingContactNo = ds.Tables[0].Rows[0]["SHIPPING_CONTACTNO"].ToString();
                shippingContactNo2 = ds.Tables[0].Rows[0]["SHIPPING_CONTACTNO2"].ToString();
                shippingFax = ds.Tables[0].Rows[0]["SHIPPING_FAX"].ToString();
                shippingEmail = ds.Tables[0].Rows[0]["SHIPPING_EMAIL"].ToString();
                shippingRemarks = ds.Tables[0].Rows[0]["SHIPPING_REMARKS"].ToString();
                shippingDate = ds.Tables[0].Rows[0]["SHIPPING_DATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["SHIPPING_DATE"]) : DateTime.MaxValue;
                shippingTrackCode = ds.Tables[0].Rows[0]["SHIPPING_TRACKCODE"].ToString();
                shippingCompany = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIPPING_COMPANY"]);
                shippingCompanyName = ds.Tables[0].Rows[0]["SHIPPING_COMPANYNAME"].ToString();
                shippingCollectionDate = ds.Tables[0].Rows[0]["SHIPPING_COLLECTIONDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["SHIPPING_COLLECTIONDATE"]) : DateTime.MaxValue;

                ordDlvService = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_DLVSERVICE"]);
                ordShipping = Convert.ToDecimal(ds.Tables[0].Rows[0]["ORDER_SHIPPING"]);
                ordTotal = ds.Tables[0].Rows[0]["V_ORDERTOTAL"] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0]["V_ORDERTOTAL"]) : 0;

                deliveryDate = ds.Tables[0].Rows[0]["DELIVERY_DATE"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["DELIVERY_DATE"].ToString()) : DateTime.MaxValue;
                deliveryTime = ds.Tables[0].Rows[0]["DELIVERY_TIME"].ToString();
                deliveryRecipient = ds.Tables[0].Rows[0]["DELIVERY_RECIPIENT"].ToString();
                deliverySender = ds.Tables[0].Rows[0]["DELIVERY_SENDER"].ToString();
                deliveryMessage = ds.Tables[0].Rows[0]["DELIVERY_MESSAGE"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractOrderById6(int intOrdId)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDER_ID, A.ORDER_NO, A.ORDER_CREATION, A.ORDER_CUSTOMERNO, A.ORDER_REMARKS, A.ORDER_TYPE, " +
                     "A.ORDER_COMPANY, A.ORDER_NAME, A.ORDER_ADD, A.ORDER_POSCODE, A.ORDER_CITY, A.ORDER_STATE, A.ORDER_COUNTRY, " +
                     "(SELECT COUNTRY_NAME FROM TB_COUNTRY WHERE COUNTRY_CODE = A.ORDER_COUNTRY) AS V_BILLCOUNTRYNAME, " +
                     "A.ORDER_CONTACTNO, A.ORDER_CONTACTNO2, A.ORDER_FAX, A.ORDER_EMAIL, " +
                     "A.SHIPPING_CUSTCOMPANY, A.SHIPPING_NAME, A.SHIPPING_ADD, A.SHIPPING_POSCODE, A.SHIPPING_CITY, A.SHIPPING_STATE, A.SHIPPING_COUNTRY, " +
                     "(SELECT COUNTRY_NAME FROM TB_COUNTRY WHERE COUNTRY_CODE = A.SHIPPING_COUNTRY) AS V_SHIPCOUNTRYNAME, " +
                     "A.SHIPPING_CONTACTNO, A.SHIPPING_CONTACTNO2, A.SHIPPING_FAX, A.SHIPPING_EMAIL, " +
                     "A.SHIPPING_REMARKS, A.SHIPPING_DATE, A.SHIPPING_TRACKCODE, A.SHIPPING_COMPANY, A.SHIPPING_COMPANYNAME, A.ORDER_SHIPPING, A.ORDER_ACTIVE, " +
                     "((SELECT SUM(ORDDETAIL_PRODTOTAL) FROM TB_ORDERDETAIL WHERE ORDER_ID = A.ORDER_ID) + A.ORDER_SHIPPING + A.ORDER_TOTALGST) AS V_ORDERTOTAL, A.ORDER_GST, A.ORDER_TOTALGST, " +
                     "A.ORDER_DLVSERVICE, A.SHIPPING_COLLECTIONDATE " +
                     "FROM TB_ORDER A " +
                     "WHERE A.ORDER_ID = ?";

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;
                ordId = int.Parse(ds.Tables[0].Rows[0]["ORDER_ID"].ToString());
                ordNo = ds.Tables[0].Rows[0]["ORDER_NO"].ToString();
                ordCreation = ds.Tables[0].Rows[0]["ORDER_CREATION"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["ORDER_CREATION"]) : DateTime.MaxValue;
                customerNo = ds.Tables[0].Rows[0]["ORDER_CUSTOMERNO"].ToString();
                ordRemarks = ds.Tables[0].Rows[0]["ORDER_REMARKS"].ToString();
                ordType = int.Parse(ds.Tables[0].Rows[0]["ORDER_TYPE"].ToString());

                company = ds.Tables[0].Rows[0]["ORDER_COMPANY"].ToString();
                name = ds.Tables[0].Rows[0]["ORDER_NAME"].ToString();
                add = ds.Tables[0].Rows[0]["ORDER_ADD"].ToString();
                poscode = ds.Tables[0].Rows[0]["ORDER_POSCODE"].ToString();
                city = ds.Tables[0].Rows[0]["ORDER_CITY"].ToString();
                state = ds.Tables[0].Rows[0]["ORDER_STATE"].ToString();
                country = ds.Tables[0].Rows[0]["ORDER_COUNTRY"].ToString();
                countryName = ds.Tables[0].Rows[0]["V_BILLCOUNTRYNAME"].ToString();
                contactNo = ds.Tables[0].Rows[0]["ORDER_CONTACTNO"].ToString();
                contactNo2 = ds.Tables[0].Rows[0]["ORDER_CONTACTNO2"].ToString();
                fax = ds.Tables[0].Rows[0]["ORDER_FAX"].ToString();
                email = ds.Tables[0].Rows[0]["ORDER_EMAIL"].ToString();

                shippingCustCompany = ds.Tables[0].Rows[0]["SHIPPING_CUSTCOMPANY"].ToString();
                shippingName = ds.Tables[0].Rows[0]["SHIPPING_NAME"].ToString();
                shippingAdd = ds.Tables[0].Rows[0]["SHIPPING_ADD"].ToString();
                shippingPoscode = ds.Tables[0].Rows[0]["SHIPPING_POSCODE"].ToString();
                shippingCity = ds.Tables[0].Rows[0]["SHIPPING_CITY"].ToString();
                shippingState = ds.Tables[0].Rows[0]["SHIPPING_STATE"].ToString();
                shippingCountry = ds.Tables[0].Rows[0]["SHIPPING_COUNTRY"].ToString();
                shippingCountryName = ds.Tables[0].Rows[0]["V_SHIPCOUNTRYNAME"].ToString();
                shippingContactNo = ds.Tables[0].Rows[0]["SHIPPING_CONTACTNO"].ToString();
                shippingContactNo2 = ds.Tables[0].Rows[0]["SHIPPING_CONTACTNO2"].ToString();
                shippingFax = ds.Tables[0].Rows[0]["SHIPPING_FAX"].ToString();
                shippingEmail = ds.Tables[0].Rows[0]["SHIPPING_EMAIL"].ToString();
                shippingRemarks = ds.Tables[0].Rows[0]["SHIPPING_REMARKS"].ToString();
                shippingDate = ds.Tables[0].Rows[0]["SHIPPING_DATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["SHIPPING_DATE"]) : DateTime.MaxValue;
                shippingTrackCode = ds.Tables[0].Rows[0]["SHIPPING_TRACKCODE"].ToString();
                shippingCompany = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIPPING_COMPANY"]);
                shippingCompanyName = ds.Tables[0].Rows[0]["SHIPPING_COMPANYNAME"].ToString();
                shippingCollectionDate = ds.Tables[0].Rows[0]["SHIPPING_COLLECTIONDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["SHIPPING_COLLECTIONDATE"]) : DateTime.MaxValue;

                ordDlvService = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_DLVSERVICE"]);
                ordShipping = Convert.ToDecimal(ds.Tables[0].Rows[0]["ORDER_SHIPPING"]);
                ordTotal = ds.Tables[0].Rows[0]["V_ORDERTOTAL"] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0]["V_ORDERTOTAL"]) : 0;
                ordGST = int.Parse(ds.Tables[0].Rows[0]["ORDER_GST"].ToString());
                ordTotalGST = ds.Tables[0].Rows[0]["ORDER_TOTALGST"] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0]["ORDER_TOTALGST"]) : Convert.ToDecimal("0.00");
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractPaymentById(int intPaymentId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDER_ID, A.PAYMENT_GATEWAY, A.PAYMENT_REMARKS, A.PAYMENT_DATE, A.PAYMENT_TOTAL, " +
                     "(SELECT LIST_NAME FROM TB_LIST WHERE LIST_GROUP = 'PAYMENT TYPE' AND LIST_VALUE = A.PAYMENT_GATEWAY AND LIST_DEFAULT = 1) AS V_PAYMENTGATEWAYNAME " +
                     "FROM TB_PAYMENT A " +
                     "WHERE A.PAYMENT_ID = ?";

            cmd.Parameters.Add("p_PAYMENT_ID", OdbcType.BigInt, 9).Value = intPaymentId;

            if (intActive != 0)
            {
                strSql += " AND A.PAYMENT_ACTIVE = ?";
                cmd.Parameters.Add("p_PAYMENT_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;
                ordId = int.Parse(ds.Tables[0].Rows[0]["ORDER_ID"].ToString());
                ordPayGatewayId = int.Parse(ds.Tables[0].Rows[0]["PAYMENT_GATEWAY"].ToString());
                ordPayRemarks = ds.Tables[0].Rows[0]["PAYMENT_REMARKS"].ToString();
                ordPayment = ds.Tables[0].Rows[0]["PAYMENT_DATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["PAYMENT_DATE"]) : DateTime.MaxValue;
                ordPayTotal = Convert.ToDecimal(ds.Tables[0].Rows[0]["PAYMENT_TOTAL"]);
                ordPayGateway = ds.Tables[0].Rows[0]["V_PAYMENTGATEWAYNAME"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int getProdWeightById(int intOrdId)
    {
        int intProdWeight = 0;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT SUM(ORDDETAIL_PRODWEIGHT * ORDDETAIL_PRODQTY) AS V_PRODTOTALWEIGHT FROM TB_ORDERDETAIL" +
                     " WHERE ORDER_ID = ?";

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intOrdId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            object objResult = cmd.ExecuteScalar();
            if (objResult != null)
            {
                intProdWeight = int.Parse(objResult.ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intProdWeight;
    }

    public decimal getProdTotalById(int intOrdId)
    {
        decimal decProdTotal = 0;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT SUM(ORDDETAIL_PRODTOTAL) AS V_PRODTOTAL FROM TB_ORDERDETAIL" +
                     " WHERE ORDER_ID = ?";

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intOrdId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            decProdTotal = Convert.ToDecimal(cmd.ExecuteScalar().ToString());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return decProdTotal;
    }

    public DataSet getOrderItemsByOrderId2(int intOrdId)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDDETAIL_ID, A.ORDDETAIL_PRODID, A.ORDDETAIL_PRODCODE, A.ORDDETAIL_PRODNAME, A.ORDDETAIL_PRODDNAME, A.ORDDETAIL_PRODSNAPSHOT, A.ORDDETAIL_PRODIMAGE, A.ORDDETAIL_PRODPRICE, " +
                     "A.ORDDETAIL_PRODQTY, A.ORDDETAIL_PRODUOM, A.ORDDETAIL_PRODWEIGHT, A.ORDDETAIL_PRODLENGTH, A.ORDDETAIL_PRODWIDTH, A.ORDDETAIL_PRODHEIGHT, A.ORDDETAIL_PRODTOTAL " +
                     "FROM TB_ORDERDETAIL A " +
                     "WHERE ORDER_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public string getLastOfflineOrd()
    {
        string strLastOrdNo = "";

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT ORDER_NO FROM TB_ORDER WHERE ORDER_TYPE = 2 " +
                     "ORDER BY ORDER_ID DESC LIMIT 1";

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            strLastOrdNo = cmd.ExecuteScalar() != null ? cmd.ExecuteScalar().ToString() : "";
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return strLastOrdNo;
    }

    public Boolean isPaymentExist(int intOrdId)
    {
        Boolean boolValid = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_PAYMENT" +
                     " WHERE ORDER_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intOrdId;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1) { boolValid = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolValid;
    }

    public Boolean isShippingExist(int intOrdId)
    {
        Boolean boolValid = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT SHIPPING_COMPANYNAME FROM TB_ORDER" +
                     " WHERE ORDER_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.BigInt, 9).Value = intOrdId;

            string strCompName = cmd.ExecuteScalar().ToString();

            if (!string.IsNullOrEmpty(strCompName)) { boolValid = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolValid;
    }
    #endregion

    #region "Specification"
    public DataSet getOrderSpecItem(int intOrdDetailId)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDDETAIL_ID, A.ORDPRODSPEC_ID, A.SI_ID, A.SPEC_ID, A.PROD_ID, A.SPEC_GROUPID, " +
                     "A.SI_NAME, A.SI_OPERATOR, A.SI_PRICE, A.SI_ATTACHMENT, A.SI_DATETITLE, A.SI_DATE, A.SI_DATEACTIVE, A.SI_ORDER, A.SI_ACTIVE,  " +
                     "(SELECT B.SPEC_TYPE FROM TB_ORDERPRODSPEC B WHERE B.ORDPRODSPEC_ID = A.ORDPRODSPEC_ID) AS SPEC_TYPE " +
                     "FROM TB_ORDERPRODSPECITEM A " +
                     "WHERE ORDDETAIL_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDDETAIL_ID", OdbcType.Int, 9).Value = intOrdDetailId;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    #endregion

    #region "Customization - Ys Hamper Project"
    public Boolean extractOrderById5(int intOrdId)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.ORDER_ID, A.ORDER_NO, A.ORDER_CREATION, A.ORDER_CUSTOMERNO, A.ORDER_REMARKS, A.ORDER_TYPE, " +
                     "A.ORDER_COMPANY, A.ORDER_NAME, A.ORDER_ADD, A.ORDER_POSCODE, A.ORDER_CITY, A.ORDER_CITYOTHER, A.ORDER_STATE, A.ORDER_STATEOTHER, A.ORDER_COUNTRY, A.ORDER_COUNTRYOTHER, " +
                     "(SELECT COUNTRY_NAME FROM TB_COUNTRY WHERE COUNTRY_CODE = A.ORDER_COUNTRY) AS V_BILLCOUNTRYNAME, " +
                     "A.ORDER_CONTACTNO, A.ORDER_CONTACTNO2, A.ORDER_FAX, A.ORDER_EMAIL, " +
                     "A.SHIPPING_CUSTCOMPANY, A.SHIPPING_NAME, A.SHIPPING_ADD, A.SHIPPING_POSCODE, A.SHIPPING_CITY, A.SHIPPING_CITYOTHER, A.SHIPPING_STATE, A.SHIPPING_STATEOTHER, A.SHIPPING_COUNTRY, A.SHIPPING_COUNTRYOTHER, " +
                     "(SELECT COUNTRY_NAME FROM TB_COUNTRY WHERE COUNTRY_CODE = A.SHIPPING_COUNTRY) AS V_SHIPCOUNTRYNAME, " +
                     "A.SHIPPING_CONTACTNO, A.SHIPPING_CONTACTNO2, A.SHIPPING_FAX, A.SHIPPING_EMAIL, " +
                     "A.SHIPPING_REMARKS, A.SHIPPING_DATE, A.SHIPPING_TRACKCODE, A.SHIPPING_COMPANY, A.SHIPPING_COMPANYNAME, A.ORDER_SHIPPING, A.ORDER_ACTIVE, " +
                     "((SELECT SUM(ORDDETAIL_PRODTOTAL) FROM TB_ORDERDETAIL WHERE ORDER_ID = A.ORDER_ID) + A.ORDER_SHIPPING) AS V_ORDERTOTAL, " +
                     "A.ORDER_DLVSERVICE, A.SHIPPING_COLLECTIONDATE, A.ORDER_RECIPIENTNAME, A.ORDER_RECIPIENTMSG, A.ORDER_SENDERNAME, A.ORDER_MSGINSTRUCTION, A.ORDER_STATUS" +
                     " FROM TB_ORDER A " +
                     " WHERE A.ORDER_ID = ?";

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;
                ordId = int.Parse(ds.Tables[0].Rows[0]["ORDER_ID"].ToString());
                ordNo = ds.Tables[0].Rows[0]["ORDER_NO"].ToString();
                ordCreation = ds.Tables[0].Rows[0]["ORDER_CREATION"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["ORDER_CREATION"]) : DateTime.MaxValue;
                customerNo = ds.Tables[0].Rows[0]["ORDER_CUSTOMERNO"].ToString();
                ordRemarks = ds.Tables[0].Rows[0]["ORDER_REMARKS"].ToString();
                ordType = int.Parse(ds.Tables[0].Rows[0]["ORDER_TYPE"].ToString());

                company = ds.Tables[0].Rows[0]["ORDER_COMPANY"].ToString();
                name = ds.Tables[0].Rows[0]["ORDER_NAME"].ToString();
                add = ds.Tables[0].Rows[0]["ORDER_ADD"].ToString();
                poscode = ds.Tables[0].Rows[0]["ORDER_POSCODE"].ToString();
                city = ds.Tables[0].Rows[0]["ORDER_CITY"].ToString();
                cityOther = ds.Tables[0].Rows[0]["ORDER_CITYOTHER"].ToString();
                state = ds.Tables[0].Rows[0]["ORDER_STATE"].ToString();
                stateOther = ds.Tables[0].Rows[0]["ORDER_STATEOTHER"].ToString();
                country = ds.Tables[0].Rows[0]["ORDER_COUNTRY"].ToString();
                countryOther = ds.Tables[0].Rows[0]["ORDER_COUNTRYOTHER"].ToString();
                countryName = ds.Tables[0].Rows[0]["V_BILLCOUNTRYNAME"].ToString();
                contactNo = ds.Tables[0].Rows[0]["ORDER_CONTACTNO"].ToString();
                contactNo2 = ds.Tables[0].Rows[0]["ORDER_CONTACTNO2"].ToString();
                fax = ds.Tables[0].Rows[0]["ORDER_FAX"].ToString();
                email = ds.Tables[0].Rows[0]["ORDER_EMAIL"].ToString();

                shippingCustCompany = ds.Tables[0].Rows[0]["SHIPPING_CUSTCOMPANY"].ToString();
                shippingName = ds.Tables[0].Rows[0]["SHIPPING_NAME"].ToString();
                shippingAdd = ds.Tables[0].Rows[0]["SHIPPING_ADD"].ToString();
                shippingPoscode = ds.Tables[0].Rows[0]["SHIPPING_POSCODE"].ToString();
                shippingCity = ds.Tables[0].Rows[0]["SHIPPING_CITY"].ToString();
                shippingCityOther = ds.Tables[0].Rows[0]["SHIPPING_CITYOTHER"].ToString();
                shippingState = ds.Tables[0].Rows[0]["SHIPPING_STATE"].ToString();
                shippingStateOther = ds.Tables[0].Rows[0]["SHIPPING_STATEOTHER"].ToString();
                shippingCountry = ds.Tables[0].Rows[0]["SHIPPING_COUNTRY"].ToString();
                shippingCountryOther = ds.Tables[0].Rows[0]["SHIPPING_COUNTRYOTHER"].ToString();
                shippingCountryName = ds.Tables[0].Rows[0]["V_SHIPCOUNTRYNAME"].ToString();
                shippingContactNo = ds.Tables[0].Rows[0]["SHIPPING_CONTACTNO"].ToString();
                shippingContactNo2 = ds.Tables[0].Rows[0]["SHIPPING_CONTACTNO2"].ToString();
                shippingFax = ds.Tables[0].Rows[0]["SHIPPING_FAX"].ToString();
                shippingEmail = ds.Tables[0].Rows[0]["SHIPPING_EMAIL"].ToString();
                shippingRemarks = ds.Tables[0].Rows[0]["SHIPPING_REMARKS"].ToString();
                shippingDate = ds.Tables[0].Rows[0]["SHIPPING_DATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["SHIPPING_DATE"]) : DateTime.MaxValue;
                shippingTrackCode = ds.Tables[0].Rows[0]["SHIPPING_TRACKCODE"].ToString();
                shippingCompany = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIPPING_COMPANY"]);
                shippingCompanyName = ds.Tables[0].Rows[0]["SHIPPING_COMPANYNAME"].ToString();
                shippingCollectionDate = ds.Tables[0].Rows[0]["SHIPPING_COLLECTIONDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["SHIPPING_COLLECTIONDATE"]) : DateTime.MaxValue;

                ordStatus = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_STATUS"]);
                ordDlvService = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_DLVSERVICE"]);
                ordShipping = Convert.ToDecimal(ds.Tables[0].Rows[0]["ORDER_SHIPPING"]);
                ordTotal = ds.Tables[0].Rows[0]["V_ORDERTOTAL"] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0]["V_ORDERTOTAL"]) : 0;
                ordFrom = ds.Tables[0].Rows[0]["ORDER_SENDERNAME"].ToString();
                ordTo = ds.Tables[0].Rows[0]["ORDER_RECIPIENTNAME"].ToString();
                ordMessage = ds.Tables[0].Rows[0]["ORDER_RECIPIENTMSG"].ToString();
                ordMsgInstruction = ds.Tables[0].Rows[0]["ORDER_MSGINSTRUCTION"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean isExactSameOrderSet3(int intOrdId, string strCompNameBill, string strNameBill, string strAddBill, string strPoscodeBill, string strCityBill, string strCityBillOther, string strStateBill, string strStateBillOther, string strCountryBill, string strCountryBillOther, string strContactNoBill, string strEmailBill, string strCompName, string strName, string strAdd, string strPoscode, string strCity, string strCityOther, string strState, string strStateOther, string strCountry, string strCountryOther, string strContactNo, DateTime dtCollectionDate, string strEmail)
    {
        Boolean boolSame = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_ORDER" +
                     " WHERE ORDER_ID = ?" +
                     " AND BINARY ORDER_COMPANY = ?" +
                     " AND BINARY ORDER_NAME = ?" +
                     " AND BINARY ORDER_ADD = ?" +
                     " AND BINARY ORDER_POSCODE = ?" +
                     " AND BINARY ORDER_CITY = ?" +
                     " AND BINARY ORDER_CITYOTHER = ?" +
                     " AND BINARY ORDER_STATE = ?" +
                     " AND BINARY ORDER_STATEOTHER = ?" +
                     " AND ORDER_COUNTRY = ?" +
                     " AND ORDER_COUNTRYOTHER = ?" +
                     " AND BINARY ORDER_CONTACTNO = ?" +
                     " AND BINARY ORDER_EMAIL = ?" +
                     " AND BINARY SHIPPING_CUSTCOMPANY = ?" +
                     " AND BINARY SHIPPING_NAME = ?" +
                     " AND BINARY SHIPPING_ADD = ?" +
                     " AND BINARY SHIPPING_POSCODE = ?" +
                     " AND BINARY SHIPPING_CITY = ?" +
                     " AND BINARY SHIPPING_CITYOTHER = ?" +
                     " AND BINARY SHIPPING_STATE = ?" +
                     " AND BINARY SHIPPING_STATEOTHER = ?" +
                     " AND SHIPPING_COUNTRY = ?" +
                     " AND SHIPPING_COUNTRYOTHER = ?" +
                     " AND BINARY SHIPPING_CONTACTNO = ?" +
                     " AND SHIPPING_COLLECTIONDATE = ?" +
                     " AND BINARY SHIPPING_EMAIL = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;
            cmd.Parameters.Add("p_ORDER_COMPANY", OdbcType.VarChar, 250).Value = strCompNameBill;
            cmd.Parameters.Add("p_ORDER_NAME", OdbcType.VarChar, 250).Value = strNameBill;
            cmd.Parameters.Add("p_ORDER_ADD", OdbcType.VarChar, 250).Value = strAddBill;
            cmd.Parameters.Add("p_ORDER_POSCODE", OdbcType.VarChar, 20).Value = strPoscodeBill;
            cmd.Parameters.Add("p_ORDER_CITY", OdbcType.VarChar, 50).Value = strCityBill;
            cmd.Parameters.Add("p_ORDER_CITYOTHER", OdbcType.VarChar, 250).Value = strCityBillOther;
            cmd.Parameters.Add("p_ORDER_STATE", OdbcType.VarChar, 250).Value = strStateBill;
            cmd.Parameters.Add("p_ORDER_STATEOTHER", OdbcType.VarChar, 250).Value = strStateBillOther;
            cmd.Parameters.Add("p_ORDER_COUNTRY", OdbcType.VarChar, 10).Value = strCountryBill;
            cmd.Parameters.Add("p_ORDER_COUNTRYOTHER", OdbcType.VarChar, 250).Value = strCountryBillOther;
            cmd.Parameters.Add("p_ORDER_CONTACTNO", OdbcType.VarChar, 20).Value = strContactNoBill;
            cmd.Parameters.Add("p_ORDER_EMAIL", OdbcType.VarChar, 250).Value = strEmailBill;
            cmd.Parameters.Add("p_SHIPPING_CUSTCOMPANY", OdbcType.VarChar, 250).Value = strCompName;
            cmd.Parameters.Add("p_SHIPPING_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_SHIPPING_ADD", OdbcType.VarChar, 250).Value = strAdd;
            cmd.Parameters.Add("p_SHIPPING_POSCODE", OdbcType.VarChar, 20).Value = strPoscode;
            cmd.Parameters.Add("p_SHIPPING_CITY", OdbcType.VarChar, 50).Value = strCity;
            cmd.Parameters.Add("p_SHIPPING_CITYOTHER", OdbcType.VarChar, 250).Value = strCityOther;
            cmd.Parameters.Add("p_SHIPPING_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_SHIPPING_STATEOTHER", OdbcType.VarChar, 250).Value = strStateOther;
            cmd.Parameters.Add("p_SHIPPING_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIPPING_COUNTRYOTHER", OdbcType.VarChar, 250).Value = strCountryOther;
            cmd.Parameters.Add("p_SHIPPING_CONTACTNO", OdbcType.VarChar, 20).Value = strContactNo;
            cmd.Parameters.Add("p_SHIPPING_COLLECTIONDATE", OdbcType.DateTime).Value = dtCollectionDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_SHIPPING_EMAIL", OdbcType.VarChar, 250).Value = strEmail;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int updateOrderDelivery2(int intOrdId, string strCompNameBill, string strNameBill, string strAddBill, string strPoscodeBill, string strCityBill, string strCityBillOther, string strStateBill, string strStateBillOther, string strCountryBill, string strCountryBillOther, string strContactNoBill, string strEmailBill, string strCompName,string strName, string strAdd, string strPoscode, string strCity, string strCityOther, string strState, string strStateOther, string strCountry, string strCountryOther, string strContactNo, DateTime dtCollectionDate, string strEmail, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ORDER SET" +
                     " ORDER_COMPANY = ?," +
                     " ORDER_NAME = ?," +
                     " ORDER_ADD = ?," +
                     " ORDER_POSCODE = ?," +
                     " ORDER_CITY = ?," +
                     " ORDER_CITYOTHER = ?," +
                     " ORDER_STATE = ?," +
                     " ORDER_STATEOTHER = ?," +
                     " ORDER_COUNTRY = ?," +
                     " ORDER_COUNTRYOTHER = ?," +
                     " ORDER_CONTACTNO = ?," +
                     " ORDER_EMAIL = ?," +
                     " SHIPPING_CUSTCOMPANY = ?," +
                     " SHIPPING_NAME = ?," +
                     " SHIPPING_ADD = ?," +
                     " SHIPPING_POSCODE = ?," +
                     " SHIPPING_CITY = ?," +
                     " SHIPPING_CITYOTHER = ?," +
                     " SHIPPING_STATE = ?," +
                     " SHIPPING_STATEOTHER = ?," +
                     " SHIPPING_COUNTRY = ?," +
                     " SHIPPING_COUNTRYOTHER = ?," +
                     " SHIPPING_CONTACTNO = ?," +
                     " SHIPPING_COLLECTIONDATE = ?," +
                     " SHIPPING_EMAIL = ?," +
                     " ORDER_UPDATEDBY = ?" +
                     " WHERE ORDER_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ORDER_COMPANY", OdbcType.VarChar, 250).Value = strCompNameBill;
            cmd.Parameters.Add("p_ORDER_NAME", OdbcType.VarChar, 250).Value = strNameBill;
            cmd.Parameters.Add("p_ORDER_ADD", OdbcType.VarChar, 250).Value = strAddBill;
            cmd.Parameters.Add("p_ORDER_POSCODE", OdbcType.VarChar, 20).Value = strPoscodeBill;
            cmd.Parameters.Add("p_ORDER_CITY", OdbcType.VarChar, 50).Value = strCityBill;
            cmd.Parameters.Add("p_ORDER_CITYOTHER", OdbcType.VarChar, 250).Value = strCityBillOther;
            cmd.Parameters.Add("p_ORDER_STATE", OdbcType.VarChar, 250).Value = strStateBill;
            cmd.Parameters.Add("p_ORDER_STATEOTHER", OdbcType.VarChar, 250).Value = strStateBillOther;
            cmd.Parameters.Add("p_ORDER_COUNTRY", OdbcType.VarChar, 10).Value = strCountryBill;
            cmd.Parameters.Add("p_ORDER_COUNTRYOTHER", OdbcType.VarChar, 250).Value = strCountryBillOther;
            cmd.Parameters.Add("p_ORDER_CONTACTNO", OdbcType.VarChar, 20).Value = strContactNoBill;
            cmd.Parameters.Add("p_ORDER_EMAIL", OdbcType.VarChar, 250).Value = strEmailBill;
            cmd.Parameters.Add("p_SHIPPING_CUSTCOMPANY", OdbcType.VarChar, 250).Value = strCompName;
            cmd.Parameters.Add("p_SHIPPING_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_SHIPPING_ADD", OdbcType.VarChar, 250).Value = strAdd;
            cmd.Parameters.Add("p_SHIPPING_POSCODE", OdbcType.VarChar, 20).Value = strPoscode;
            cmd.Parameters.Add("p_SHIPPING_CITY", OdbcType.VarChar, 50).Value = strCity;
            cmd.Parameters.Add("p_SHIPPING_CITYOTHER", OdbcType.VarChar, 250).Value = strCityOther;
            cmd.Parameters.Add("p_SHIPPING_STATE", OdbcType.VarChar, 250).Value = strState;
            cmd.Parameters.Add("p_SHIPPING_STATEOTHER", OdbcType.VarChar, 250).Value = strStateOther;
            cmd.Parameters.Add("p_SHIPPING_COUNTRY", OdbcType.VarChar, 10).Value = strCountry;
            cmd.Parameters.Add("p_SHIPPING_COUNTRYOTHER", OdbcType.VarChar, 250).Value = strCountryOther;
            cmd.Parameters.Add("p_SHIPPING_CONTACTNO", OdbcType.VarChar, 20).Value = strContactNo;
            cmd.Parameters.Add("p_SHIPPING_COLLECTIONDATE", OdbcType.DateTime).Value = dtCollectionDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_SHIPPING_EMAIL", OdbcType.VarChar, 250).Value = strEmail;
            cmd.Parameters.Add("p_ORDER_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                ordId = intOrdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractOrder(int intOrdId)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            List<string> columns = new List<string>() {
                "`a`.`ORDER_ID` AS `ORDER_ID`",
                "`a`.`MEM_ID` AS `V_MEM`",
                "`a`.`MEM_CODE` AS `V_MEMCODE`",
                "`a`.`MEM_NAME` AS `V_MEMNAME`",
                "`a`.`MEM_EMAIL` AS `V_MEMEMAIL`",
                "`a`.`ORDER_NO` AS `ORDER_NO`",
                "`a`.`ORDER_CREATION` AS `ORDER_CREATION`",
                "`a`.`ORDER_DUEDATE` AS `ORDER_DUEDATE`",
                "`a`.`ORDER_REMARKS` AS `ORDER_REMARKS`",
                "`a`.`ORDER_READ` AS `ORDER_READ`",
                "`a`.`ORDER_TYPE` AS `V_ORDERTYPE`",
                "`a`.`ORDER_STATUSID` AS `ORDER_STATUSID`",
                "`a`.`ORDER_STATUS` AS `ORDER_STATUS`",
                "`a`.`ORDER_COMPANY` AS `ORDER_COMPANY`",
                "`a`.`ORDER_NAME` AS `ORDER_NAME`",
                "`a`.`ORDER_ADD` AS `ORDER_ADD`",
                "`a`.`ORDER_POSCODE` AS `ORDER_POSCODE`",
                "`a`.`ORDER_CITY` AS `ORDER_CITY`",
                "`a`.`ORDER_CITYOTHER` AS `ORDER_CITYOTHER`",
                "`a`.`ORDER_STATE` AS `ORDER_STATE`",
                "`a`.`ORDER_STATEOTHER` AS `ORDER_STATEOTHER`",
                "`a`.`ORDER_COUNTRY` AS `ORDER_COUNTRY`",
                "`a`.`ORDER_COUNTRYOTHER` AS `ORDER_COUNTRYOTHER`",
                "`a`.`ORDER_CONTACTNO` AS `ORDER_CONTACTNO`",
                "`a`.`ORDER_EMAIL` AS `ORDER_EMAIL`",
                "`a`.`SHIPPING_CUSTCOMPANY` AS `SHIPPING_CUSTCOMPANY`",
                "`a`.`SHIPPING_NAME` AS `SHIPPING_NAME`",
                "`a`.`SHIPPING_ADD` AS `SHIPPING_ADD`",
                "`a`.`SHIPPING_POSCODE` AS `SHIPPING_POSCODE`",
                "`a`.`SHIPPING_CITY` AS `SHIPPING_CITY`",
                "`a`.`SHIPPING_CITYOTHER` AS `SHIPPING_CITYOTHER`",
                "`a`.`SHIPPING_STATE` AS `SHIPPING_STATE`",
                "`a`.`SHIPPING_STATEOTHER` AS `SHIPPING_STATEOTHER`",
                "`a`.`SHIPPING_COUNTRY` AS `SHIPPING_COUNTRY`",
                "`a`.`SHIPPING_COUNTRYOTHER` AS `SHIPPING_COUNTRYOTHER`",
                "`a`.`SHIPPING_CONTACTNO` AS `SHIPPING_CONTACTNO`",
                "`a`.`SHIPPING_EMAIL` AS `SHIPPING_EMAIL`",
                "`a`.`SHIPPING_REMARKS` AS `SHIPPING_REMARKS`",
                "`a`.`SHIPPING_TRACKCODE` AS `SHIPPING_TRACKCODE`",
                "`a`.`SHIPPING_COMPANY` AS `SHIPPING_COMPANY`",
                "`a`.`SHIPPING_COMPANYNAME` AS `SHIPPING_COMPANYNAME`",
                "`a`.`ORDER_SHIPPING` AS `ORDER_SHIPPING`",
                "`a`.`ORDER_PAYMENT` AS `ORDER_PAYMENT`",
                "`a`.`ORDER_DONE` AS `ORDER_DONE`",
                "`a`.`ORDER_CANCEL` AS `ORDER_CANCEL`",
                "`a`.`ORDER_DLVFLAG` AS `ORDER_DLVFLAG`",
                "`a`.`ORDER_ACTIVE` AS `ORDER_ACTIVE`",
                "`a`.`ORDER_UPDATEDBY` AS `ORDER_UPDATEDBY`",
                "`a`.`ORDER_LASTUPDATE` AS `ORDER_LASTUPDATE`",
                "`a`.`ORDER_PICKUPPOINT` AS `ORDER_PICKUPPOINT`",
                "`a`.`ORDER_PICKUPPOINTNAME` AS `ORDER_PICKUPPOINTNAME`",
                "`a`.`ORDER_RECIPIENTNAME` AS `ORDER_RECIPIENTNAME`",
                "`a`.`ORDER_RECIPIENTMSG` AS `ORDER_RECIPIENTMSG`",
                "`a`.`ORDER_SENDERNAME` AS `ORDER_SENDERNAME`",
                "`a`.`ORDER_MSGINSTRUCTION` AS `ORDER_MSGINSTRUCTION`",
                "`a`.`ORDER_MSG_BUYER`",
                "sum(`c`.`ORDDETAIL_PRODQTY`) AS `ORDER_TOTALITEM`",
                "sum((`c`.`ORDDETAIL_PRODWEIGHT` * `c`.`ORDDETAIL_PRODQTY`)) AS `ORDER_TOTALWEIGHT`",
                "sum(`c`.`ORDDETAIL_PRODTOTAL`) AS `ORDER_SUBTOTAL`",
                "sum(`c`.`ORDDETAIL_PRODTOTAL`) + `a`.`ORDER_SHIPPING` AS `ORDER_TOTAL`",

            };
            strSql = "select " + string.Join(",", columns.ToArray()) + " from `tb_orderdetail` `c` ";
            strSql += " left join `tb_order` `a` on `a`.`ORDER_ID` = `c`.`ORDER_ID`";
            strSql += " where c.ORDER_ID = ?";
            strSql += " group by c.ORDER_ID"; ;

            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;
                ordId = int.Parse(ds.Tables[0].Rows[0]["ORDER_ID"].ToString());
                memId = int.Parse(ds.Tables[0].Rows[0]["V_MEM"].ToString());
                memCode = ds.Tables[0].Rows[0]["V_MEMCODE"].ToString();
                memName = ds.Tables[0].Rows[0]["V_MEMNAME"].ToString();
                memEmail = ds.Tables[0].Rows[0]["V_MEMEMAIL"].ToString();
                ordNo = ds.Tables[0].Rows[0]["ORDER_NO"].ToString();
                ordCreation = ds.Tables[0].Rows[0]["ORDER_CREATION"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["ORDER_CREATION"]) : DateTime.MaxValue;
                ordDueDate = ds.Tables[0].Rows[0]["ORDER_DUEDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["ORDER_DUEDATE"]) : DateTime.MaxValue;
                ordRemarks = ds.Tables[0].Rows[0]["ORDER_REMARKS"].ToString();
                ordRead = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_READ"]);
                ordStatus = int.Parse(ds.Tables[0].Rows[0]["ORDER_STATUS"].ToString());
                ordStatusId = int.Parse(ds.Tables[0].Rows[0]["ORDER_STATUSID"].ToString());
                ordMsgBuyer = ds.Tables[0].Rows[0]["ORDER_MSG_BUYER"].ToString();

                name = ds.Tables[0].Rows[0]["ORDER_NAME"].ToString();
                add = ds.Tables[0].Rows[0]["ORDER_ADD"].ToString();
                poscode = ds.Tables[0].Rows[0]["ORDER_POSCODE"].ToString();
                city = ds.Tables[0].Rows[0]["ORDER_CITY"].ToString();
                cityOther = ds.Tables[0].Rows[0]["ORDER_CITYOTHER"].ToString();
                state = ds.Tables[0].Rows[0]["ORDER_STATE"].ToString();
                stateOther = ds.Tables[0].Rows[0]["ORDER_STATEOTHER"].ToString();
                country = ds.Tables[0].Rows[0]["ORDER_COUNTRY"].ToString();
                countryOther = ds.Tables[0].Rows[0]["ORDER_COUNTRYOTHER"].ToString();
                contactNo = ds.Tables[0].Rows[0]["ORDER_CONTACTNO"].ToString();
                email = ds.Tables[0].Rows[0]["ORDER_EMAIL"].ToString();
                company = ds.Tables[0].Rows[0]["ORDER_COMPANY"].ToString();

                shippingName = ds.Tables[0].Rows[0]["SHIPPING_NAME"].ToString();
                shippingAdd = ds.Tables[0].Rows[0]["SHIPPING_ADD"].ToString();
                shippingPoscode = ds.Tables[0].Rows[0]["SHIPPING_POSCODE"].ToString();
                shippingCity = ds.Tables[0].Rows[0]["SHIPPING_CITY"].ToString();
                shippingCityOther = ds.Tables[0].Rows[0]["SHIPPING_CITYOTHER"].ToString();
                shippingState = ds.Tables[0].Rows[0]["SHIPPING_STATE"].ToString();
                shippingStateOther = ds.Tables[0].Rows[0]["SHIPPING_STATEOTHER"].ToString();
                shippingCountry = ds.Tables[0].Rows[0]["SHIPPING_COUNTRY"].ToString();
                shippingCountryOther = ds.Tables[0].Rows[0]["SHIPPING_COUNTRYOTHER"].ToString();
                shippingContactNo = ds.Tables[0].Rows[0]["SHIPPING_CONTACTNO"].ToString();
                shippingEmail = ds.Tables[0].Rows[0]["SHIPPING_EMAIL"].ToString();
                shippingRemarks = ds.Tables[0].Rows[0]["SHIPPING_REMARKS"].ToString();
                shippingTrackCode = ds.Tables[0].Rows[0]["SHIPPING_TRACKCODE"].ToString();
                shippingCompany = Convert.ToInt16(ds.Tables[0].Rows[0]["SHIPPING_COMPANY"]);
                shippingCompanyName = ds.Tables[0].Rows[0]["SHIPPING_COMPANYNAME"].ToString();
                shippingCustCompany = ds.Tables[0].Rows[0]["SHIPPING_CUSTCOMPANY"].ToString();
                pickupPoint = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_PICKUPPOINT"]);
                pickupPointName = ds.Tables[0].Rows[0]["ORDER_PICKUPPOINTNAME"].ToString();

                ordShipping = Convert.ToDecimal(ds.Tables[0].Rows[0]["ORDER_SHIPPING"]);
                ordPaymentId = int.Parse(ds.Tables[0].Rows[0]["ORDER_PAYMENT"].ToString());


                ordDone = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_DONE"]);
                ordCancel = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_CANCEL"]);
                ordActive = Convert.ToInt16(ds.Tables[0].Rows[0]["ORDER_ACTIVE"]);
                ordUpdatedBy = int.Parse(ds.Tables[0].Rows[0]["ORDER_UPDATEDBY"].ToString());
                ordLastUpdate = ds.Tables[0].Rows[0]["ORDER_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["ORDER_LASTUPDATE"]) : DateTime.MaxValue;

                ordTotalItem = ds.Tables[0].Rows[0]["ORDER_TOTALITEM"] != DBNull.Value ? int.Parse(ds.Tables[0].Rows[0]["ORDER_TOTALITEM"].ToString()) : 0;
                ordTotalWeight = ds.Tables[0].Rows[0]["ORDER_TOTALWEIGHT"] != DBNull.Value ? int.Parse(ds.Tables[0].Rows[0]["ORDER_TOTALWEIGHT"].ToString()) : 0;
                ordSubtotal = ds.Tables[0].Rows[0]["ORDER_SUBTOTAL"] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0]["ORDER_SUBTOTAL"]) : Convert.ToDecimal("0.00");
                ordTotal = ds.Tables[0].Rows[0]["ORDER_TOTAL"] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0]["ORDER_TOTAL"]) : Convert.ToDecimal("0.00");
                ordFrom = ds.Tables[0].Rows[0]["ORDER_SENDERNAME"].ToString();
                ordTo = ds.Tables[0].Rows[0]["ORDER_RECIPIENTNAME"].ToString();
                ordMessage = ds.Tables[0].Rows[0]["ORDER_RECIPIENTMSG"].ToString();
                ordMsgInstruction = ds.Tables[0].Rows[0]["ORDER_MSGINSTRUCTION"].ToString();
                ordType = Convert.ToInt16(ds.Tables[0].Rows[0]["V_ORDERTYPE"]);

                //countryName = ds.Tables[0].Rows[0]["V_COUNTRYNAME"].ToString();
                //shippingCountryName = ds.Tables[0].Rows[0]["V_SHIPPINGCOUNTRYNAME"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    public Boolean extractOrderPayment(int intOrdId, int intPayId)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            List<string> columns = new List<string>() { "*" };
            strSql = "SELECT * FROM vw_payment WHERE PAYMENT_ID = ? OR (ORDER_ID = ? AND PAYMENT_ACTIVE = 1)";

            cmd.Parameters.Add("p_PAYMENT_ID", OdbcType.Int, 9).Value = intPayId;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                ordPayGatewayId = ds.Tables[0].Rows[0]["PAYMENT_GATEWAY"] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0]["PAYMENT_GATEWAY"]) : 0;
                ordPayGateway = ds.Tables[0].Rows[0]["PAYMENT_GATEWAYNAME"].ToString();
                ordPayRemarks = ds.Tables[0].Rows[0]["PAYMENT_REMARKS"].ToString();
                ordPayment = ds.Tables[0].Rows[0]["PAYMENT_DATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["PAYMENT_DATE"]) : DateTime.MaxValue;
                ordPayTotal = ds.Tables[0].Rows[0]["PAYMENT_TOTAL"] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0]["PAYMENT_TOTAL"]) : Convert.ToDecimal("0.00");
                ordPayBank = ds.Tables[0].Rows[0]["PAYMENT_BANK"] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0]["PAYMENT_BANK"]) : 0;
                ordPayBankName = ds.Tables[0].Rows[0]["PAYMENT_BANKNAME"].ToString();
                ordPayBankAccount = ds.Tables[0].Rows[0]["PAYMENT_BANKACCOUNT"].ToString();
                ordPayTransId = ds.Tables[0].Rows[0]["PAYMENT_TRANSID"].ToString();
                ordPayBankDate = ds.Tables[0].Rows[0]["PAYMENT_BANKDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["PAYMENT_BANKDATE"]) : DateTime.MaxValue;
                ordPayReject = ds.Tables[0].Rows[0]["PAYMENT_REJECT"] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0]["PAYMENT_REJECT"]) : 0;
                ordPayRejectRemarks = ds.Tables[0].Rows[0]["PAYMENT_REJECTREMARKS"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    public Boolean extractOrderStatus(int intOrdId, int intOrdStatus, int intOrdStatusId)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            List<string> columns = new List<string>()
            {
                "`STATUS_ID` AS `V_ORDERSTATUS`",
                "`V_STATUSNAME` AS `V_ORDERSTATUSNAME`",
                "`STATUS_DATE` AS `V_STATUSDATE`",
            };
            strSql = "SELECT "+string.Join(",",columns.ToArray())+" FROM VW_ORDERSTATUS WHERE ORDSTATUS_ID = ? or (STATUS_ID = ? and ORDER_ID = ? and STATUS_ACTIVE = 1)";

            cmd.Parameters.Add("p_ORDSTATUS_ID", OdbcType.Int, 9).Value = intOrdStatusId;
            cmd.Parameters.Add("p_STATUS_ID", OdbcType.Int, 9).Value = intOrdStatus;
            cmd.Parameters.Add("p_ORDER_ID", OdbcType.Int, 9).Value = intOrdId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                
                ordStatus = ds.Tables[0].Rows[0]["V_ORDERSTATUS"] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0]["V_ORDERSTATUS"]) : 0;
                ordStatusName = ds.Tables[0].Rows[0]["V_ORDERSTATUSNAME"].ToString();
                ordStatusDate = ds.Tables[0].Rows[0]["V_STATUSDATE"] != DBNull.Value ? Convert.ToDateTime(ds.Tables[0].Rows[0]["V_STATUSDATE"]) : DateTime.MaxValue;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    public Boolean extractOrderType(int intOrdTypeId)
    {
        Boolean boolFound = false;

        try
        {
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            List<string> columns = new List<string>()
            {
                "`ORDERTYPE_NAME` AS `V_ORDERTYPENAME`",
            };
            strSql = "SELECT " + string.Join(",", columns.ToArray()) + " FROM vw_ordertype WHERE ORDERTYPE_ID = ?";

            cmd.Parameters.Add("p_ORDERTYPE_ID", OdbcType.Int, 9).Value = intOrdTypeId;

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                ordTypeName = ds.Tables[0].Rows[0]["V_ORDERTYPENAME"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public bool extractOrderYsHamper(int intOrdId)
    {
        bool isOrderExists = extractOrder(intOrdId);
        if (isOrderExists)
        {
            extractOrderPayment(intOrdId, ordPaymentId);
            extractOrderStatus(intOrdId, ordStatus, ordStatusId);
            extractOrderType(ordType);

            DataView countries = new DataView(new clsMis().getCountryList(1).Tables[0]);

            countries.RowFilter = "country_code = '" + country + "'";
            if (countries.Count > 0) { countryName = countries[0]["country_name"].ToString(); }

            countries.RowFilter = "country_code = '" + shippingCompany + "'";
            if (countries.Count > 0) { shippingCompanyName = countries[0]["country_name"].ToString(); }
        }
        return isOrderExists;
    }
    #endregion
    #endregion
}
