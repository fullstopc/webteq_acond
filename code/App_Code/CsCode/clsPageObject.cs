﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsSectCMS
/// </summary>
public class clsPageObject : dbconnBase
{
    #region "Properties"
    private int _pageCMSId;
    private int _pageId;
    private int _customId;
    private int _customId2;
    private int _blockId;
    private int _cmsId;
    private string _cmsTitle;
    private string _cmsContent;
    private DateTime _cmsCreation;
    private int _cmsCreatedBy;
    private DateTime _cmsLastUpdate;
    private int _cmsUpdatedBy;
    private int _cmsActive;
    private string _pageCMSType;
    private string _pageCMSTypeName;
    private string _pageCMSLang;
    private int _pageCMSAdminView;
    private DateTime _pageCMSCreation;
    private int _pageCMSCreatedBy;
    private DateTime _pageCMSLastUpdate;
    private int _pageCMSUpdatedBy;
    #endregion


    #region "Property Methods"
    public int pageCMSId
    {
        get { return _pageCMSId; }
        set { _pageCMSId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public int customId
    {
        get { return _customId; }
        set { _customId = value; }
    }

    public int customId2
    {
        get { return _customId2; }
        set { _customId2 = value; }
    }

    public int blockId
    {
        get { return _blockId; }
        set { _blockId = value; }
    }

    public int cmsId
    {
        get { return _cmsId; }
        set { _cmsId = value; }
    }

    public string cmsTitle
    {
        get { return _cmsTitle; }
        set { _cmsTitle = value; }
    }

    public string cmsContent
    {
        get { return _cmsContent; }
        set { _cmsContent = value; }
    }

    public DateTime cmsCreation
    {
        get { return _cmsCreation; }
        set { _cmsCreation = value; }
    }

    public int cmsCreatedBy
    {
        get { return _cmsCreatedBy; }
        set { _cmsCreatedBy = value; }
    }

    public DateTime cmsLastUpdate
    {
        get { return _cmsLastUpdate; }
        set { _cmsLastUpdate = value; }
    }

    public int cmsUpdatedBy
    {
        get { return _cmsUpdatedBy; }
        set { _cmsUpdatedBy = value; }
    }

    public int cmsActive
    {
        get { return _cmsActive; }
        set { _cmsActive = value; }
    }

    public string pageCMSType
    {
        get { return _pageCMSType; }
        set { _pageCMSType = value; }
    }

    public string pageCMSTypeName
    {
        get { return _pageCMSTypeName; }
        set { _pageCMSTypeName = value; }
    }

    public string pageCMSLang
    {
        get { return _pageCMSLang; }
        set { _pageCMSLang = value; }
    }

    public int pageCMSAdminView
    {
        get { return _pageCMSAdminView; }
        set { _pageCMSAdminView = value; }
    }

    public DateTime pageCMSCreation
    {
        get { return _pageCMSCreation; }
        set { _pageCMSCreation = value; }
    }

    public int pageCMSCreatedBy
    {
        get { return _pageCMSCreatedBy; }
        set { _pageCMSCreatedBy = value; }
    }

    public DateTime pageCMSLastUpdate
    {
        get { return _pageCMSLastUpdate; }
        set { _pageCMSLastUpdate = value; }
    }

    public int pageCMSUpdatedBy
    {
        get { return _pageCMSUpdatedBy; }
        set { _pageCMSUpdatedBy = value; }
    }
    #endregion


    #region "Methods"
    public clsPageObject()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    public int addItem(int intPageId, int intCustomId, int intCMSId, int intType, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PAGECMS (" +
                     "PAGE_ID," +
                     "CUSTOM_ID," +
                     "CMS_ID," +
                     "PAGECMS_TYPE," +
                     "PAGECMS_CREATION," +
                     "PAGECMS_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;
            cmd.Parameters.Add("p_CUSTOM_ID", OdbcType.BigInt, 9).Value = intCustomId;
            cmd.Parameters.Add("p_CMS_ID", OdbcType.Int, 9).Value = intCMSId;
            cmd.Parameters.Add("p_PAGECMS_TYPE", OdbcType.Int, 9).Value = intType;
            cmd.Parameters.Add("p_PAGECMS_CREATEDBY", OdbcType.Int, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";

                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                pageCMSId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addItem(int intPageId, int intCustomId, int intCustomId2, int intBlockId, int intCMSId, int intType, int intAdminView, int intGroup, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PAGECMS (" +
                     "PAGE_ID, " +
                     "CUSTOM_ID, " +
                     "CUSTOM_ID_2, " +
                     "BLOCK_ID, " +
                     "CMS_ID, " +
                     "PAGECMS_TYPE, " +
                     "PAGECMS_GROUP, " +
                     "PAGECMS_ADMINVIEW, " +
                     "PAGECMS_CREATION, " +
                     "PAGECMS_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;
            cmd.Parameters.Add("p_CUSTOM_ID", OdbcType.BigInt, 9).Value = intCustomId;
            cmd.Parameters.Add("p_CUSTOM_ID_2", OdbcType.BigInt, 9).Value = intCustomId2;
            cmd.Parameters.Add("p_BLOCK_ID", OdbcType.BigInt, 9).Value = intBlockId;
            cmd.Parameters.Add("p_CMS_ID", OdbcType.BigInt, 9).Value = intCMSId;
            cmd.Parameters.Add("p_PAGECMS_TYPE", OdbcType.Int, 9).Value = intType;
            cmd.Parameters.Add("p_PAGECMS_GROUP", OdbcType.Int, 9).Value = intGroup;
            cmd.Parameters.Add("p_PAGECMS_ADMINVIEW", OdbcType.Int, 1).Value = intAdminView;
            cmd.Parameters.Add("p_PAGECMS_CREATEDBY", OdbcType.Int, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";

                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                pageCMSId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addItem(int intPageId, int intCustomId, int intCustomId2, int intBlockId, int intCMSId, int intType, string strLang, int intAdminView, int intGroup, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PAGECMS (" +
                     "PAGE_ID, " +
                     "CUSTOM_ID, " +
                     "CUSTOM_ID_2, " +
                     "BLOCK_ID, " +
                     "CMS_ID, " +
                     "PAGECMS_TYPE, " +
                     "PAGECMS_GROUP, " +
                     "PAGECMS_LANG, " +
                     "PAGECMS_ADMINVIEW, " +
                     "PAGECMS_CREATION, " +
                     "PAGECMS_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;
            cmd.Parameters.Add("p_CUSTOM_ID", OdbcType.BigInt, 9).Value = intCustomId;
            cmd.Parameters.Add("p_CUSTOM_ID_2", OdbcType.BigInt, 9).Value = intCustomId2;
            cmd.Parameters.Add("p_BLOCK_ID", OdbcType.BigInt, 9).Value = intBlockId;
            cmd.Parameters.Add("p_CMS_ID", OdbcType.BigInt, 9).Value = intCMSId;
            cmd.Parameters.Add("p_PAGECMS_TYPE", OdbcType.Int, 9).Value = intType;
            cmd.Parameters.Add("p_PAGECMS_GROUP", OdbcType.Int, 9).Value = intGroup;
            cmd.Parameters.Add("p_PAGECMS_LANG", OdbcType.VarChar, 10).Value = strLang;
            cmd.Parameters.Add("p_PAGECMS_ADMINVIEW", OdbcType.Int, 1).Value = intAdminView;
            cmd.Parameters.Add("p_PAGECMS_CREATEDBY", OdbcType.BigInt, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";

                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                pageCMSId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getItemList()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PAGECMS_ID, A.PAGE_ID, A.CUSTOM_ID, A.CUSTOM_ID_2, A.BLOCK_ID, A.CMS_ID, A.V_CMSTITLE, A.V_CMSCONTENT, A.V_CMSCREATION, A.V_CMSCREATEDBY, A.V_CMSLASTUPDATE, A.V_CMSUPDATEDBY, A.V_CMSACTIVE," +
                     " A.PAGECMS_TYPE, A.V_TYPENAME, A.PAGECMS_GROUP, A.V_GROUPNAME, A.PAGECMS_LANG, A.PAGECMS_ADMINVIEW, A.PAGECMS_CREATION, A.PAGECMS_CREATEDBY, A.PAGECMS_LASTUPDATE, A.PAGECMS_UPDATEDBY" +
                     " FROM VW_PAGECMS A";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public DataSet getItemList2()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "select a.PAGECMS_ID AS PAGECMS_ID, a.PAGE_ID AS PAGE_ID,a.CUSTOM_ID AS CUSTOM_ID,a.CUSTOM_ID_2 AS CUSTOM_ID_2,a.BLOCK_ID AS BLOCK_ID,a.CMS_ID AS CMS_ID, " +
                        " b.CMS_TITLE AS V_CMSTITLE,b.CMS_CONTENT AS V_CMSCONTENT,b.CMS_CREATION AS V_CMSCREATION,b.CMS_CREATEDBY AS V_CMSCREATEDBY, " +
                        " b.CMS_LASTUPDATE AS V_CMSLASTUPDATE,b.CMS_UPDATEDBY AS V_CMSUPDATEDBY,e.GRP_ACTIVE AS V_GRP_ACTIVE,b.CMS_ACTIVE AS V_CMSACTIVE, " +
                        " a.PAGECMS_TYPE AS PAGECMS_TYPE,c.CMSTYPE_NAME AS V_TYPENAME,a.PAGECMS_GROUP AS PAGECMS_GROUP, " +
                        " d.CMSGROUP_NAME AS V_GROUPNAME,a.PAGECMS_LANG AS PAGECMS_LANG, a.PAGECMS_ADMINVIEW AS PAGECMS_ADMINVIEW, " +
                        " a.PAGECMS_CREATION AS PAGECMS_CREATION,a.PAGECMS_CREATEDBY AS PAGECMS_CREATEDBY,a.PAGECMS_LASTUPDATE AS PAGECMS_LASTUPDATE," +
                        " a.PAGECMS_UPDATEDBY AS PAGECMS_UPDATEDBY, B.CMS_ACTIVE AS CMS_ACTIVE, " +
                        " (SELECT B.GRP_DNAME FROM TB_SLIDEGROUP B WHERE A.CMS_ID = B.GRP_ID AND A.PAGECMS_GROUP = 2) AS SLIDEGRP_DNAME, " +
                        " (SELECT B.GRP_ACTIVE FROM TB_SLIDEGROUP B WHERE A.CMS_ID = B.GRP_ID AND A.PAGECMS_GROUP = 2) AS GRP_ACTIVE " +
                        " from ( " +
                        " ( " +
                        " 	((tb_pagecms a left join tb_cms b on a.CMS_ID = b.CMS_ID and a.PAGECMS_GROUP = 1)  " +
                        " 	left join  " +
                        " 		(select a.LIST_NAME AS CMSTYPE_NAME,a.LIST_VALUE AS CMSTYPE_ID,a.LIST_GROUP AS CMSTYPE_GROUP,a.LIST_ORDER AS CMSTYPE_ORDER,a.LIST_ACTIVE AS CMSTYPE_ACTIVE,a.LIST_PARENTVALUE AS CMSTYPE_PARENTVALUE,a.LIST_PARENTGROUP AS CMSTYPE_PARENTGROUP from tb_list a where a.LIST_GROUP = 'CMS TYPE')  " +
                        " 		as c on a.PAGECMS_TYPE = c.CMSTYPE_ID)  " +
                        " 	left join  " +
                        " 		(select a.LIST_NAME AS CMSGROUP_NAME,a.LIST_VALUE AS CMSGROUP_ID,a.LIST_GROUP AS CMSGROUP_GROUP,a.LIST_ORDER AS CMSGROUP_ORDER,a.LIST_ACTIVE AS CMSGROUP_ACTIVE,a.LIST_PARENTVALUE AS CMSGROUP_PARENTVALUE,a.LIST_PARENTGROUP AS CMSGROUP_PARENTGROUP from tb_list a where a.LIST_GROUP = 'CMS GROUP')  " +
                        " 		as d on a.PAGECMS_GROUP = d.CMSGROUP_ID " +
                        " ) left join tb_slidegroup e on a.CMS_ID = e.GRP_ID); ";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }


    public Boolean isItemExist(int intPageId, int intCustomId, int intCustomId2, int intBlockId, int intCMSId, int intType, int intAdminView, int intGroup)
    {
        Boolean boolExist = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PAGECMS" +
                     " WHERE PAGE_ID = ?" +
                     " AND CUSTOM_ID = ?" +
                     " AND CUSTOM_ID_2 = ?" +
                     " AND BLOCK_ID = ?" +
                     " AND CMS_ID = ?" +
                     " AND PAGECMS_TYPE = ?" +
                     " AND PAGECMS_ADMINVIEW = ?" +
                     " AND PAGECMS_GROUP = ?";

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;
            cmd.Parameters.Add("p_CUSTOM_ID", OdbcType.BigInt, 9).Value = intCustomId;
            cmd.Parameters.Add("p_CUSTOM_ID_2", OdbcType.BigInt, 9).Value = intCustomId2;
            cmd.Parameters.Add("p_BLOCK_ID", OdbcType.BigInt, 9).Value = intBlockId;
            cmd.Parameters.Add("p_CMS_ID", OdbcType.BigInt, 9).Value = intCMSId;
            cmd.Parameters.Add("p_PAGECMS_TYPE", OdbcType.Int, 9).Value = intType;
            cmd.Parameters.Add("p_PAGECMS_ADMINVIEW", OdbcType.Int, 1).Value = intAdminView;
            cmd.Parameters.Add("p_PAGECMS_GROUP", OdbcType.Int, 9).Value = intGroup;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolExist = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public Boolean isItemExist(int intPageId, int intCustomId, int intCustomId2, int intBlockId, int intCMSId, int intType, string strLang, int intAdminView, int intGroup)
    {
        Boolean boolExist = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PAGECMS" +
                     " WHERE PAGE_ID = ?" +
                     " AND CUSTOM_ID = ?" +
                     " AND CUSTOM_ID_2 = ?" +
                     " AND BLOCK_ID = ?" +
                     " AND CMS_ID = ?" +
                     " AND PAGECMS_TYPE = ?" +
                     " AND PAGECMS_LANG = ?" +
                     " AND PAGECMS_ADMINVIEW = ?" +
                     " AND PAGECMS_GROUP = ?";

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;
            cmd.Parameters.Add("p_CUSTOM_ID", OdbcType.BigInt, 9).Value = intCustomId;
            cmd.Parameters.Add("p_CUSTOM_ID_2", OdbcType.BigInt, 9).Value = intCustomId2;
            cmd.Parameters.Add("p_BLOCK_ID", OdbcType.BigInt, 9).Value = intBlockId;
            cmd.Parameters.Add("p_CMS_ID", OdbcType.BigInt, 9).Value = intCMSId;
            cmd.Parameters.Add("p_PAGECMS_TYPE", OdbcType.Int, 9).Value = intType;
            cmd.Parameters.Add("p_PAGECMS_LANG", OdbcType.VarChar, 10).Value = strLang;
            cmd.Parameters.Add("p_PAGECMS_ADMINVIEW", OdbcType.Int, 1).Value = intAdminView;
            cmd.Parameters.Add("p_PAGECMS_GROUP", OdbcType.Int, 9).Value = intGroup;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolExist = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public int deleteItemById(int intPageId, int intCustomId, int intCustomId2, int intBlockId, int intCMSId, int intType, int intAdminView, int intGroup)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PAGECMS" +
                     " WHERE PAGE_ID = ?" +
                     " AND CUSTOM_ID = ?" +
                     " AND CUSTOM_ID_2 = ?" +
                     " AND BLOCK_ID = ?" +
                     " AND CMS_ID = ?" +
                     " AND PAGECMS_TYPE= ?" +
                     " AND PAGECMS_ADMINVIEW = ?" +
                     " AND PAGECMS_GROUP = ?";

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;
            cmd.Parameters.Add("p_CUSTOM_ID", OdbcType.BigInt, 9).Value = intCustomId;
            cmd.Parameters.Add("p_CUSTOM_ID_2", OdbcType.BigInt, 9).Value = intCustomId2;
            cmd.Parameters.Add("p_BLOCK_ID", OdbcType.BigInt, 9).Value = intBlockId;
            cmd.Parameters.Add("p_CMS_ID", OdbcType.BigInt, 9).Value = intCMSId;
            cmd.Parameters.Add("p_PAGECMS_TYPE", OdbcType.Int, 9).Value = intType;
            cmd.Parameters.Add("p_PAGECMS_ADMINVIEW", OdbcType.Int, 1).Value = intAdminView;
            cmd.Parameters.Add("p_PAGECMS_GROUP", OdbcType.Int, 9).Value = intGroup;

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteItemById(int intPageId, int intCustomId, int intCustomId2, int intBlockId, int intCMSId, int intType, string strLang, int intAdminView, int intGroup)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PAGECMS" +
                     " WHERE PAGE_ID = ?" +
                     " AND CUSTOM_ID = ?" +
                     " AND CUSTOM_ID_2 = ?" +
                     " AND BLOCK_ID = ?" +
                     " AND CMS_ID = ?" +
                     " AND PAGECMS_TYPE= ?" +
                     " AND PAGECMS_LANG = ?" +
                     " AND PAGECMS_ADMINVIEW = ?" +
                     " AND PAGECMS_GROUP = ?";

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPageId;
            cmd.Parameters.Add("p_CUSTOM_ID", OdbcType.BigInt, 9).Value = intCustomId;
            cmd.Parameters.Add("p_CUSTOM_ID_2", OdbcType.BigInt, 9).Value = intCustomId2;
            cmd.Parameters.Add("p_BLOCK_ID", OdbcType.BigInt, 9).Value = intBlockId;
            cmd.Parameters.Add("p_CMS_ID", OdbcType.BigInt, 9).Value = intCMSId;
            cmd.Parameters.Add("p_PAGECMS_TYPE", OdbcType.Int, 9).Value = intType;
            cmd.Parameters.Add("p_PAGECMS_LANG", OdbcType.VarChar, 10).Value = strLang;
            cmd.Parameters.Add("p_PAGECMS_ADMINVIEW", OdbcType.Int, 1).Value = intAdminView;
            cmd.Parameters.Add("p_PAGECMS_GROUP", OdbcType.Int, 9).Value = intGroup;

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteItemById(int intCMSId, int intGroup)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PAGECMS WHERE CMS_ID = ? AND PAGECMS_GROUP = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CMS_ID", OdbcType.BigInt, 9).Value = intCMSId;
            cmd.Parameters.Add("p_PAGECMS_GROUP", OdbcType.Int, 9).Value = intGroup;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion
}
