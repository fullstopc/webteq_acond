﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsXMLItem
/// </summary>
public class clsXMLItem : dbconnBase
{
    #region "Methods"
    public clsXMLItem()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    public DataSet getPageList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PAGE_ID, A.PAGE_ORDER, A.PAGE_LANG, A.PAGE_TEMPLATE, A.PAGE_PARENT, A.PAGE_CREATION, A.PAGE_LASTUPDATE" +
                     " FROM TB_PAGE A  WHERE PAGE_SHOWINSITEMAP = ?";

            cmd.Parameters.Add("p_PAGE_SHOWINSITEMAP", OdbcType.Int, 1).Value = 1;

            if (intActive != 0)
            {
                strSql += " AND A.PAGE_ACTIVE = ?";
                cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getGrpList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.GRP_ID, A.GRP_ORDER, A.GRP_CREATION, A.GRP_LASTUPDATE" +
                    " FROM TB_GROUP A";

            if (intActive != 0)
            {
                strSql += " WHERE A.GRP_ACTIVE = ?";
                cmd.Parameters.Add("p_GRP_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getProdList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_IMAGE, A.PROD_CREATION, A.PROD_LASTUPDATE" +
                     " FROM TB_PRODUCT A";

            if (intActive != 0)
            {
                strSql += " WHERE A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    #endregion
}
