﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsParticipant
/// </summary>
public class clsParticipant : dbconnBase
{
    #region "Properties"
    private int _parId;
    private string _parFirstName;
    private string _parLastName;
    private string _parEmail;
    private string _parContactNo;
    private int _parNationalityId;
    private string _parNationalityName;
    private int _parGenderId;
    private string _parGenderName;
    private DateTime _parDOB;
    private int _parEduLevelId;
    private string _parEduLevelName;
    private int _parEngSpeakLevelId;
    private string _parEngSpeakLevelName;
    private int _parEngWriteLevelId;
    private string _parEngWriteLevelName;
    private string _parSuitableReason;
    private string _parResumeName;
    private string _parResume;
    private int _parLearnAboutId;
    private string _parLearnAboutName;
    private int _parProgGrpId;
    private string _parProgGrpName;
    private int _parProgramId;
    private string _parProgramName;
    private int _parProgDateId;
    private DateTime _parProgStartDate;
    private DateTime _parProgEndDate;
    private int _parRecentAppliedId;
    private string _parRecentAppliedName;
    private int _parSubscribe;
    private DateTime _parRegisteredDate;
    private int _parActive;
    private string _parOrderStatus;

    private int _parProgramDate;
    private string _parPaymentGatewayName;
    private string _vGatewayName;
    private int _parStatus;
    private string _parRandomCode;
    private int _proDeleted;

    private int _stdStatus;
    private int _stdStatusFor;
    #endregion

    #region "Property Methods"
    public int parId
    {
        get { return _parId; }
        set { _parId = value; }
    }

    public string parFirstName
    {
        get { return _parFirstName; }
        set { _parFirstName = value; }
    }

    public string parLastName
    {
        get { return _parLastName; }
        set { _parLastName = value; }
    }

    public string parEmail
    {
        get { return _parEmail; }
        set { _parEmail = value; }
    }

    public string parContactNo
    {
        get { return _parContactNo; }
        set { _parContactNo = value; }
    }

    public int parNationalityId
    {
        get { return _parNationalityId; }
        set { _parNationalityId = value; }
    }

    public string parNationalityName
    {
        get { return _parNationalityName; }
        set { _parNationalityName = value; }
    }

    public int parGenderId
    {
        get { return _parGenderId; }
        set { _parGenderId = value; }
    }

    public string parGenderName
    {
        get { return _parGenderName; }
        set { _parGenderName = value; }
    }

    public DateTime parDOB
    {
        get { return _parDOB; }
        set { _parDOB = value; }
    }

    public int parEduLevelId
    {
        get { return _parEduLevelId; }
        set { _parEduLevelId = value; }
    }

    public string parEduLevelName
    {
        get { return _parEduLevelName; }
        set { _parEduLevelName = value; }
    }

    public int parEngSpeakLevelId
    {
        get { return _parEngSpeakLevelId; }
        set { _parEngSpeakLevelId = value; }
    }

    public string parEngSpeakLevelName
    {
        get { return _parEngSpeakLevelName; }
        set { _parEngSpeakLevelName = value; }
    }

    public int parEngWriteLevelId
    {
        get { return _parEngWriteLevelId; }
        set { _parEngWriteLevelId = value; }
    }

    public string parEngWriteLevelName
    {
        get { return _parEngWriteLevelName; }
        set { _parEngWriteLevelName = value; }
    }

    public string parSuitableReason
    {
        get { return _parSuitableReason; }
        set { _parSuitableReason = value; }
    }

    public string parResumeName
    {
        get { return _parResumeName; }
        set { _parResumeName = value; }
    }

    public string parResume
    {
        get { return _parResume; }
        set { _parResume = value; }
    }

    public int parLearnAboutId
    {
        get { return _parLearnAboutId; }
        set { _parLearnAboutId = value; }
    }

    public string parLearnAboutName
    {
        get { return _parLearnAboutName; }
        set { _parLearnAboutName = value; }
    }

    public int parProgGrpId
    {
        get { return _parProgGrpId; }
        set { _parProgGrpId = value; }
    }

    public string parProgGrpName
    {
        get { return _parProgGrpName; }
        set { _parProgGrpName = value; }
    }

    public int parProgramId
    {
        get { return _parProgramId; }
        set { _parProgramId = value; }
    }

    public string parProgramName
    {
        get { return _parProgramName; }
        set { _parProgramName = value; }
    }

    public int parProgDateId
    {
        get { return _parProgDateId; }
        set { _parProgDateId = value; }
    }

    public DateTime parProgStartDate
    {
        get { return _parProgStartDate; }
        set { _parProgStartDate = value; }
    }

    public DateTime parProgEndDate
    {
        get { return _parProgEndDate; }
        set { _parProgEndDate = value; }
    }

    public int parRecentAppliedId
    {
        get { return _parRecentAppliedId; }
        set { _parRecentAppliedId = value; }
    }

    public string parRecentAppliedName
    {
        get { return _parRecentAppliedName; }
        set { _parRecentAppliedName = value; }
    }

    public int parSubscribe
    {
        get { return _parSubscribe; }
        set { _parSubscribe = value; }
    }

    public DateTime parRegisteredDate
    {
        get { return _parRegisteredDate; }
        set { _parRegisteredDate = value; }
    }

    public int parActive
    {
        get { return _parActive; }
        set { _parActive = value; }
    }

    public string parOrderStatus
    {
        get { return _parOrderStatus; }
        set { _parOrderStatus = value; }
    }

    public int stdStatus
    {
        get { return _stdStatus; }
        set { _stdStatus = value; }
    }

    public int stdStatusFor
    {
        get { return _stdStatusFor; }
        set { _stdStatusFor = value; }
    }

    public int parProgramDate
    {
        get { return _parProgramDate; }
        set { _parProgramDate = value; }
    }

    public string parPaymentGatewayName
    {
        get { return _parPaymentGatewayName; }
        set { _parPaymentGatewayName = value; }
    }

    public string vGatewayName
    {
        get { return _vGatewayName; }
        set { _vGatewayName = value; }
    }

    public int parStatus
    {
        get { return _parStatus; }
        set { _parStatus = value; }
    }

    public string parRandomCode
    {
        get { return _parRandomCode; }
        set { _parRandomCode = value; }
    }

    public int proDeleted
    {
        get { return _proDeleted; }
        set { _proDeleted = value; }
    }
    #endregion

    public clsParticipant()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int addParticipant(string strName, string strIcNo, string strNationality, DateTime dtDOB, string strAddress, string strContactNo, string strEmail, string strOtherCourses, int intProgId, int intProgDate, string strPromotionCode)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PARTICIPANT (" +
                     "PAR_NAME, " +
                     "PAR_IC, " +
                     "PAR_NATIONALITY, " +
                     "PAR_DOB, " +
                     "PAR_ADDRESS, " +
                     "PAR_CONTNO, " +
                     "PAR_EMAIL, " +
                     "PAR_OTHERCOURSES, " +
                     "PAR_PROGRAM, " +
                     "PAR_PROGRAMDATE, " +
                     "PAR_PROMOTIONCODE, " +
                     "PAR_REGISTEREDDATE" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAR_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_PAR_IC", OdbcType.VarChar, 250).Value = strIcNo;
            cmd.Parameters.Add("p_PAR_NATIONALITY", OdbcType.VarChar, 250).Value = strNationality;
            cmd.Parameters.Add("p_PAR_DOB", OdbcType.DateTime).Value = dtDOB;
            cmd.Parameters.Add("p_PAR_ADDRESS", OdbcType.VarChar, 250).Value = strAddress;
            cmd.Parameters.Add("p_PAR_CONTNO", OdbcType.VarChar, 50).Value = strContactNo;
            cmd.Parameters.Add("p_PAR_EMAIL", OdbcType.VarChar, 250).Value = strEmail;
            cmd.Parameters.Add("p_PAR_OTHERCOURSES", OdbcType.VarChar, 250).Value = strOtherCourses;
            cmd.Parameters.Add("P_PAR_PROGRAM", OdbcType.Int, 9).Value = intProgId;
            cmd.Parameters.Add("p_PAR_PROGRAMDATE", OdbcType.Int, 9).Value = intProgDate;
            cmd.Parameters.Add("p_PAR_PROMOTIONCODE", OdbcType.VarChar, 250).Value = strPromotionCode;
            cmd.Parameters.Add("p_PAR_REGISTEREDDATE", OdbcType.VarChar, 250).Value = DateTime.Now;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd.Connection = openConn();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = strSql2;
                parId = Convert.ToInt16(cmd.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addParticipantPayment(int intParId, int intPayGateway, decimal decPayTotal, string strPayRemarks)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PAYMENT (" +
                     "PAR_ID, " +
                     "PAYMENT_GATEWAY, " +
                     "PAYMENT_REMARKS, " +
                     "PAYMENT_DATE, " +
                     "PAYMENT_TOTAL" +
                     ") VALUES " +
                     "(?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAR_ID", OdbcType.Int, 9).Value = intParId;
            cmd.Parameters.Add("p_PAYMENT_GATEWAY", OdbcType.Int, 9).Value = intPayGateway;
            cmd.Parameters.Add("p_PAYMENT_REMARKS", OdbcType.VarChar, 1000).Value = strPayRemarks;
            cmd.Parameters.Add("p_PAYMENT_DATE", OdbcType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("p_PAYMENT_TOTAL", OdbcType.Decimal).Value = decPayTotal;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                parId = intParId;
            }
            else
            {
                trans.Rollback();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addParticipantPayment(int intParId, string strPayGateway, decimal decPayTotal, string strPayRemarks)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PAYMENT (" +
                     "PAR_ID, " +
                     "PAYMENT_GATEWAYNAME, " +
                     "PAYMENT_REMARKS, " +
                     "PAYMENT_DATE, " +
                     "PAYMENT_TOTAL" +
                     ") VALUES " +
                     "(?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAR_ID", OdbcType.Int, 9).Value = intParId;
            cmd.Parameters.Add("p_PAYMENT_GATEWAYNAME", OdbcType.VarChar, 250).Value = strPayGateway;
            cmd.Parameters.Add("p_PAYMENT_REMARKS", OdbcType.VarChar, 1000).Value = strPayRemarks;
            cmd.Parameters.Add("p_PAYMENT_DATE", OdbcType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("p_PAYMENT_TOTAL", OdbcType.Decimal).Value = decPayTotal;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                parId = intParId;
            }
            else
            {
                trans.Rollback();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractParticipantById(int intParId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.PAR_ID, A.PAR_FIRSTNAME, A.PAR_LASTNAME, A.PAR_EMAIL, A.PAR_CONTACTNO, A.PAR_NATIONALITY," +
                     " (SELECT LIST_NAME FROM TB_LIST WHERE A.PAR_NATIONALITY = LIST_VALUE AND LIST_GROUP = 'MALAYSIAN TYPE') AS V_NATIONALITY," +
                     " (SELECT LIST_NAME FROM TB_LIST WHERE A.PAR_GENDER = LIST_VALUE AND LIST_GROUP = 'GENDER') AS V_GENDER, A.PAR_GENDER," +
                     " A.PAR_DOB, A.PAR_EDULEVEL, (SELECT LIST_NAME FROM TB_LIST WHERE A.PAR_EDULEVEL = LIST_VALUE AND LIST_GROUP = 'EDUCATION LEVEL') AS V_EDULEVELNAME," +
                     " A.PAR_ENGSPEAKLEVEL, (SELECT LIST_NAME FROM TB_LIST WHERE A.PAR_ENGSPEAKLEVEL = LIST_VALUE AND LIST_GROUP = 'ENGLISH LEVEL') AS V_ENGSPEAKLEVELNAME," +
                     " A.PAR_ENGWRITELEVEL, (SELECT LIST_NAME FROM TB_LIST WHERE A.PAR_ENGWRITELEVEL = LIST_VALUE AND LIST_GROUP = 'ENGLISH LEVEL') AS V_ENGWRITELEVELNAME," +
                     " A.PAR_SUITABLEREASON, A.PAR_RESUMENAME, A.PAR_RESUME, A.PAR_LEARNABOUT, (SELECT LIST_NAME FROM TB_LIST WHERE A.PAR_LEARNABOUT = LIST_VALUE AND LIST_GROUP = 'OPTIONS LEARN PROGRAM') AS V_LEARNABOUTNAME," +
                     " A.PAR_PROGGRP, (SELECT PROGGRP_DNAME FROM TB_PROGRAMGROUP WHERE A.PAR_PROGGRP = PROGGRP_ID) AS V_PROGGRPNAME," +
                     " A.PAR_PROGRAM, (SELECT PRO_NAME FROM TB_PROGRAM WHERE A.PAR_PROGRAM = PRO_ID) AS V_PROGRAMNAME," +
                     " A.PAR_PROGRAMDATE, (SELECT PROG_STARTDATE FROM TB_PROGRAMDATE WHERE A.PAR_PROGRAMDATE = PROG_ID) AS V_PROGSTARTDATE," +
                     " (SELECT PROG_ENDDATE FROM TB_PROGRAMDATE WHERE A.PAR_PROGRAMDATE = PROG_ID) AS V_PROGENDDATE," +
                     " A.PAR_RECENTAPPLIED, (SELECT PRO_NAME FROM TB_PROGRAM WHERE A.PAR_RECENTAPPLIED = PRO_ID) AS V_RECENTAPPLIEDNAME," +
                     " A.PAR_SUBSCRIBE, A.PAR_REGISTEREDDATE, A.PAR_ACTIVE, (SELECT PRO_DELETED FROM TB_PROGRAM WHERE A.PAR_PROGRAM = PRO_ID) AS V_PROGDELETED," +
                     " A.PARTICIPANT_STATUS" +
                     " FROM TB_PARTICIPANT A" +
                     " WHERE PAR_ID = ?";

            //strSql = "SELECT * FROM TB_PARTICIPANT WHERE PAR_ID = ?";

            cmd.Parameters.Add("p_PAR_ID", OdbcType.Int, 9).Value = intParId;

            if (intActive != 0)
            {
                strSql += " AND PAR_ACTIVE = ?";
                cmd.Parameters.Add("p_PAR_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            cmd.Connection = openConn();

            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                parId = int.Parse(dataSet.Tables[0].Rows[0]["PAR_ID"].ToString());
                parFirstName = dataSet.Tables[0].Rows[0]["PAR_FIRSTNAME"].ToString();
                parLastName = dataSet.Tables[0].Rows[0]["PAR_LASTNAME"].ToString();
                parEmail = dataSet.Tables[0].Rows[0]["PAR_EMAIL"].ToString();
                parContactNo = dataSet.Tables[0].Rows[0]["PAR_CONTACTNO"].ToString();
                parNationalityId = int.Parse(dataSet.Tables[0].Rows[0]["PAR_NATIONALITY"].ToString());
                parNationalityName = dataSet.Tables[0].Rows[0]["V_NATIONALITY"].ToString();
                parGenderId = int.Parse(dataSet.Tables[0].Rows[0]["PAR_GENDER"].ToString());
                parGenderName = dataSet.Tables[0].Rows[0]["V_GENDER"].ToString();
                parDOB = dataSet.Tables[0].Rows[0]["PAR_DOB"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PAR_DOB"]) : DateTime.MaxValue;
                parEduLevelId = int.Parse(dataSet.Tables[0].Rows[0]["PAR_EDULEVEL"].ToString());
                parEduLevelName = dataSet.Tables[0].Rows[0]["V_EDULEVELNAME"].ToString();
                parEngSpeakLevelId = int.Parse(dataSet.Tables[0].Rows[0]["PAR_ENGSPEAKLEVEL"].ToString());
                parEngSpeakLevelName = dataSet.Tables[0].Rows[0]["V_ENGSPEAKLEVELNAME"].ToString();
                parEngWriteLevelId = int.Parse(dataSet.Tables[0].Rows[0]["PAR_ENGWRITELEVEL"].ToString());
                parEngWriteLevelName = dataSet.Tables[0].Rows[0]["V_ENGWRITELEVELNAME"].ToString();
                parSuitableReason = dataSet.Tables[0].Rows[0]["PAR_SUITABLEREASON"].ToString();
                parResumeName = dataSet.Tables[0].Rows[0]["PAR_RESUMENAME"].ToString();
                parResume = dataSet.Tables[0].Rows[0]["PAR_RESUME"].ToString();
                parLearnAboutId = int.Parse(dataSet.Tables[0].Rows[0]["PAR_LEARNABOUT"].ToString());
                parLearnAboutName = dataSet.Tables[0].Rows[0]["V_LEARNABOUTNAME"].ToString();
                parProgGrpId = int.Parse(dataSet.Tables[0].Rows[0]["PAR_PROGGRP"].ToString());
                parProgGrpName = dataSet.Tables[0].Rows[0]["V_PROGGRPNAME"].ToString();
                parProgramId = int.Parse(dataSet.Tables[0].Rows[0]["PAR_PROGRAM"].ToString());
                parProgramName = dataSet.Tables[0].Rows[0]["V_PROGRAMNAME"].ToString();
                parProgDateId = int.Parse(dataSet.Tables[0].Rows[0]["PAR_PROGRAMDATE"].ToString());
                parProgStartDate = dataSet.Tables[0].Rows[0]["V_PROGSTARTDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["V_PROGSTARTDATE"]) : DateTime.MaxValue;
                parProgEndDate = dataSet.Tables[0].Rows[0]["V_PROGENDDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["V_PROGENDDATE"]) : DateTime.MaxValue;
                parRecentAppliedId = int.Parse(dataSet.Tables[0].Rows[0]["PAR_RECENTAPPLIED"].ToString());
                parRecentAppliedName = dataSet.Tables[0].Rows[0]["V_RECENTAPPLIEDNAME"].ToString();
                parSubscribe = int.Parse(dataSet.Tables[0].Rows[0]["PAR_SUBSCRIBE"].ToString());
                parRegisteredDate = dataSet.Tables[0].Rows[0]["PAR_REGISTEREDDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PAR_REGISTEREDDATE"]) : DateTime.MaxValue;
                parActive = int.Parse(dataSet.Tables[0].Rows[0]["PAR_ACTIVE"].ToString());
                proDeleted = dataSet.Tables[0].Rows[0]["V_PROGDELETED"] != DBNull.Value ? Convert.ToInt16(dataSet.Tables[0].Rows[0]["V_PROGDELETED"]) : 0;
                parStatus = int.Parse(dataSet.Tables[0].Rows[0]["PARTICIPANT_STATUS"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    //public Boolean extractParticipantByRandCode(string strRandCode)
    //{
    //    Boolean boolFound = false;

    //    try
    //    {
    //        DataSet dataSet = new DataSet();
    //        OdbcCommand cmd = new OdbcCommand();
    //        OdbcDataAdapter da = new OdbcDataAdapter();
    //        string strSql;

    //        strSql = "SELECT * FROM VW_PARTICIPANT WHERE PAR_RANDOMCODE = ?";

    //        cmd.Parameters.Add("p_PAR_RANDOMCODE", OdbcType.VarChar, 250).Value = strRandCode;

    //        cmd.CommandType = CommandType.Text;
    //        cmd.CommandText = strSql;
    //        cmd.Connection = openConn();

    //        da.SelectCommand = cmd;
    //        da.Fill(dataSet);

    //        if (dataSet.Tables[0].Rows.Count > 0)
    //        {
    //            boolFound = true;

    //            parId = int.Parse(dataSet.Tables[0].Rows[0]["PAR_ID"].ToString());
    //            parName = dataSet.Tables[0].Rows[0]["PAR_NAME"].ToString();
    //            parDOB = dataSet.Tables[0].Rows[0]["PAR_DOB"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PAR_DOB"]) : DateTime.MaxValue;
    //            parAdd = dataSet.Tables[0].Rows[0]["PAR_ADDR"].ToString();
    //            parContact = dataSet.Tables[0].Rows[0]["PAR_CONTNO"].ToString();
    //            parEmail = dataSet.Tables[0].Rows[0]["PAR_EMAIL"].ToString();
    //            parProgram = int.Parse(dataSet.Tables[0].Rows[0]["PAR_PROGRAM"].ToString());
    //            parProgramName = dataSet.Tables[0].Rows[0]["PAR_PROGRAMNAME"].ToString();
    //            programStartDate = dataSet.Tables[0].Rows[0]["PROG_STARTDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PROG_STARTDATE"].ToString()) : DateTime.MaxValue;
    //            programEndDate = dataSet.Tables[0].Rows[0]["PROG_ENDDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PROG_ENDDATE"].ToString()) : DateTime.MaxValue;
    //            programFee = decimal.Parse(dataSet.Tables[0].Rows[0]["PRO_FEE"].ToString());
    //            parPaymentType = dataSet.Tables[0].Rows[0]["PAR_PAYMENTTYPE"] != DBNull.Value ? int.Parse(dataSet.Tables[0].Rows[0]["PAR_PAYMENTTYPE"].ToString()) : 0;
    //            parPaymentTypeName = dataSet.Tables[0].Rows[0]["PAR_PAYMENTTYPENAME"].ToString();
    //            parPaymentGatewayName = dataSet.Tables[0].Rows[0]["PAYMENT_GATEWAYNAME"].ToString();
    //            vGatewayName = dataSet.Tables[0].Rows[0]["V_GATEWAYNAME"].ToString();
    //            paymentRemarks = dataSet.Tables[0].Rows[0]["PAYMENT_REMARKS"].ToString();
    //            paymentTotal = dataSet.Tables[0].Rows[0]["PAYMENT_TOTAL"] != DBNull.Value ? decimal.Parse(dataSet.Tables[0].Rows[0]["PAYMENT_TOTAL"].ToString()) : Convert.ToDecimal("0.00");
    //            paymentDate = dataSet.Tables[0].Rows[0]["PAYMENT_DATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PAYMENT_DATE"].ToString()) : DateTime.MaxValue;
    //            parRegisterDate = dataSet.Tables[0].Rows[0]["PAR_REGISTEREDDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PAR_REGISTEREDDATE"].ToString()) : DateTime.MaxValue;
    //            parActive = int.Parse(dataSet.Tables[0].Rows[0]["PAR_ACTIVE"].ToString());
    //            parStatus = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAR_STATUS"]);
    //            parOrderStatus = dataSet.Tables[0].Rows[0]["V_ORDERSTATUSNAME"].ToString();
    //            parProgramDate = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PAR_PROGRAMDATE"]);
    //            parRandomCode = dataSet.Tables[0].Rows[0]["PAR_RANDOMCODE"].ToString();
    //            proDeleted = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PRO_DELETED"]);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        logErroMsg(ex.Message.ToString());
    //        throw ex;
    //    }
    //    finally
    //    {
    //        closeConn();
    //    }

    //    return boolFound;
    //}

    //public Boolean extractParticipantByParId(int intParId)
    //{
    //    Boolean boolFound = false;

    //    try
    //    {
    //        DataSet dataSet = new DataSet();
    //        OdbcCommand cmd = new OdbcCommand();
    //        OdbcDataAdapter da = new OdbcDataAdapter();
    //        string strSql;

    //        strSql = "SELECT * FROM VW_PARTICIPANT WHERE PAR_ID = ?";

    //        cmd.Parameters.Add("p_PAR_ID", OdbcType.Int, 9).Value = intParId;

    //        cmd.CommandType = CommandType.Text;
    //        cmd.CommandText = strSql;
    //        cmd.Connection = openConn();

    //        da.SelectCommand = cmd;
    //        da.Fill(dataSet);

    //        if (dataSet.Tables[0].Rows.Count > 0)
    //        {
    //            boolFound = true;

    //            parId = int.Parse(dataSet.Tables[0].Rows[0]["PAR_ID"].ToString());
    //            parName = dataSet.Tables[0].Rows[0]["PAR_NAME"].ToString();
    //            parCode = dataSet.Tables[0].Rows[0]["PAR_CODE"].ToString();
    //            parParentName = dataSet.Tables[0].Rows[0]["PAR_PARENTNAME"].ToString();
    //            parChildName = dataSet.Tables[0].Rows[0]["PAR_CHILDNAME"].ToString();
    //            parAdd = dataSet.Tables[0].Rows[0]["PAR_ADDR"].ToString();
    //            parContact = dataSet.Tables[0].Rows[0]["PAR_CONTNO"].ToString();
    //            parEmail = dataSet.Tables[0].Rows[0]["PAR_EMAIL"].ToString();
    //            parProgram = int.Parse(dataSet.Tables[0].Rows[0]["PAR_PROGRAM"].ToString());
    //            parProgramName = dataSet.Tables[0].Rows[0]["PAR_PROGRAMNAME"].ToString();
    //            programStartDate = dataSet.Tables[0].Rows[0]["PROG_STARTDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PROG_STARTDATE"].ToString()) : DateTime.MaxValue;
    //            programEndDate = dataSet.Tables[0].Rows[0]["PROG_ENDDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PROG_ENDDATE"].ToString()) : DateTime.MaxValue;
    //            programFee = decimal.Parse(dataSet.Tables[0].Rows[0]["PRO_FEE"].ToString());
    //            //parPaymentType = int.Parse(dataSet.Tables[0].Rows[0]["PAR_PAYMENTTYPE"].ToString());
    //            parPaymentTypeName = dataSet.Tables[0].Rows[0]["PAR_PAYMENTTYPENAME"].ToString();
    //            paymentRemarks = dataSet.Tables[0].Rows[0]["PAYMENT_REMARKS"].ToString();
    //            paymentTotal = dataSet.Tables[0].Rows[0]["PAYMENT_TOTAL"] != DBNull.Value ? decimal.Parse(dataSet.Tables[0].Rows[0]["PAYMENT_TOTAL"].ToString()) : Convert.ToDecimal("0.00");
    //            paymentDate = dataSet.Tables[0].Rows[0]["PAYMENT_DATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PAYMENT_DATE"].ToString()) : DateTime.MaxValue;
    //            parRegisterDate = dataSet.Tables[0].Rows[0]["PAR_REGISTEREDDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PAR_REGISTEREDDATE"].ToString()) : DateTime.MaxValue;
    //            parActive = int.Parse(dataSet.Tables[0].Rows[0]["PAR_ACTIVE"].ToString());
    //            parOrderStatus = dataSet.Tables[0].Rows[0]["V_ORDERSTATUSNAME"].ToString();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        logErroMsg(ex.Message.ToString());
    //        throw ex;
    //    }
    //    finally
    //    {
    //        closeConn();
    //    }

    //    return boolFound;
    //}

    public int setParticipantInactive(int intParId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PARTICIPANT SET PAR_ACTIVE = 0 WHERE PAR_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAR_ID", OdbcType.Int, 9).Value = intParId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                parId = intParId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateParticipantStatus(int intParId, int intStatusId, int intStatusFor)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PARTICIPANTSTATUS (" +
                     "PAR_ID, " +
                     "STATUS_ID, " +
                     "STATUS_FOR," +
                     "STATUS_DATE" +
                     ") VALUES " +
                     "(?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_STATUS_ID", OdbcType.VarChar, 10).Value = intStatusId;
            cmd.Parameters.Add("p_STATUS_FOR", OdbcType.VarChar, 10).Value = intStatusFor;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                parId = intParId;
                stdStatus = intStatusId;
                stdStatusFor = intStatusFor;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateParticipantStatus(int intParId, int intStatusId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PARTICIPANT SET PARTICIPANT_STATUS = ? WHERE PAR_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PARTICIPANT_STATUS", OdbcType.VarChar, 10).Value = intStatusId;
            cmd.Parameters.Add("p_PAR_ID", OdbcType.Int, 9).Value = intParId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                parId = intParId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateParticipantPaymentType(int intParId, int intType)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PARTICIPANT SET PAR_PAYMENTTYPE = ? WHERE PAR_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAR_PAYMENTTYPE", OdbcType.Int, 9).Value = intType;
            cmd.Parameters.Add("p_PAR_ID", OdbcType.Int, 9).Value = intParId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                parId = intParId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateRandomCode(int intParId, string strRandomCode)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PARTICIPANT SET PAR_RANDOMCODE = ? WHERE PAR_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAR_RANDOMCODE", OdbcType.VarChar, 250).Value = strRandomCode;
            cmd.Parameters.Add("p_PAR_ID", OdbcType.Int, 9).Value = intParId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                parId = intParId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getParticipantList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            string strSql;

            strSql = "SELECT A.PAR_ID, A.PAR_FIRSTNAME, A.PAR_LASTNAME, A.PAR_EMAIL, A.PAR_CONTACTNO, A.PAR_NATIONALITY, A.PAR_GENDER," +
                     " A.PAR_DOB, A.PAR_EDULEVEL, " +
                     " A.PAR_ENGSPEAKLEVEL, A.PAR_ENGWRITELEVEL, A.PAR_SUITABLEREASON, A.PAR_RESUMENAME, A.PAR_RESUME, A.PAR_LEARNABOUT," +
                     " A.PAR_PROGGRP, A.PAR_PROGRAM, " +
                     " A.PAR_PROGRAMDATE, (SELECT PROG_STARTDATE FROM TB_PROGRAMDATE WHERE A.PAR_PROGRAMDATE = PROG_ID) AS V_PROGSTARTDATE," +
                     " (SELECT PROG_ENDDATE FROM TB_PROGRAMDATE WHERE A.PAR_PROGRAMDATE = PROG_ID) AS V_PROGENDDATE," +
                     " A.PAR_RECENTAPPLIED, A.PAR_SUBSCRIBE, A.PAR_REGISTEREDDATE, A.PAR_ACTIVE" +
                     " FROM TB_PARTICIPANT A";

            //strSql = "SELECT * FROM TB_PARTICIPANT A";

            if (intActive != 0)
            {
                strSql += " WHERE PAR_ACTIVE = ?";
                cmd.Parameters.Add("p_PAR_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public int generateSequenceNo()
    {
        int intRecordAffected = 0;
        int intSequence = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PARSEQUENCE (PARSEQ_DATEGET) VALUES (SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                intSequence = Convert.ToInt16(cmd2.ExecuteScalar());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intSequence;
    }

    public Boolean isExactSameDataSet(int intParId, string strFirstName, string strLastName, string strEmail, string strContactNo, int intMalaysian, int intGender, DateTime dtDOB,
                                     int intHighestEdu, int intEngSpeak, int intEngWrite, int intProgDate)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PARTICIPANT" +
                     " WHERE PAR_ID = ? " +
                     " AND PAR_FIRSTNAME = ?" +
                     " AND PAR_LASTNAME = ?" +
                     " AND PAR_EMAIL = ?" +
                     " AND PAR_CONTACTNO = ?" +
                     " AND PAR_NATIONALITY = ?" +
                     " AND PAR_GENDER = ?" +
                     " AND PAR_DOB = ?" +
                     " AND PAR_EDULEVEL = ?" +
                     " AND PAR_ENGSPEAKLEVEL = ?" +
                     " AND PAR_ENGWRITELEVEL = ?" +
                     " AND PAR_PROGRAMDATE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PAR_ID", OdbcType.Int, 9).Value = intParId;
            cmd.Parameters.Add("p_PAR_FIRSTNAME", OdbcType.VarChar, 250).Value = strFirstName;
            cmd.Parameters.Add("p_PAR_LASTNAME", OdbcType.VarChar, 250).Value = strLastName;
            cmd.Parameters.Add("p_PAR_EMAIL", OdbcType.VarChar, 250).Value = strEmail;
            cmd.Parameters.Add("p_PAR_CONTACTNO", OdbcType.VarChar, 50).Value = strContactNo;
            cmd.Parameters.Add("p_PAR_NATIONALITY", OdbcType.Int, 1).Value = intMalaysian;
            cmd.Parameters.Add("p_PAR_GENDER", OdbcType.Int, 1).Value = intGender;
            cmd.Parameters.Add("p_PAR_DOB", OdbcType.DateTime).Value = dtDOB;
            cmd.Parameters.Add("p_PAR_EDULEVEL", OdbcType.Int, 9).Value = intHighestEdu;
            cmd.Parameters.Add("p_PAR_ENGSPEAKLEVEL", OdbcType.Int, 9).Value = intEngSpeak;
            cmd.Parameters.Add("p_PAR_ENGWRITELEVEL", OdbcType.Int, 9).Value = intEngWrite;
            cmd.Parameters.Add("p_PAR_PROGRAMDATE", OdbcType.Int, 9).Value = intProgDate;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    //public Boolean isExactSameDataSet(int intParId, string strName, string strIcNo, string strNationality, DateTime dtDOB, string strAddress, string strContactNo, string strEmail, string strOtherCourses, int intProgDate, string strPromotionCode)
    //{
    //    Boolean bExactSame = false;

    //    try
    //    {
    //        string strSql;
    //        OdbcCommand cmd = new OdbcCommand();

    //        strSql = "SELECT COUNT(*) FROM TB_PARTICIPANT" +
    //            " WHERE PAR_ID = ? " +
    //            " AND PAR_NAME = ?" +
    //            " AND PAR_IC = ?" +
    //            " AND PAR_NATIONALITY = ?" +
    //            " AND PAR_DOB = ?" +
    //            " AND PAR_ADDRESS = ?" +
    //            " AND PAR_CONTNO = ?" +
    //            " AND PAR_EMAIL = ?" +
    //            " AND PAR_OTHERCOURSES = ?" +
    //            " AND PAR_PROGRAMDATE = ?" +
    //            " AND PAR_PROMOTIONCODE = ?";

    //        cmd.Connection = openConn();
    //        cmd.CommandText = strSql;
    //        cmd.CommandType = CommandType.Text;

    //        cmd.Parameters.Add("p_PAR_ID", OdbcType.Int, 9).Value = intParId;
    //        cmd.Parameters.Add("p_PAR_NAME", OdbcType.VarChar, 250).Value = strName;
    //        cmd.Parameters.Add("p_PAR_IC", OdbcType.VarChar, 250).Value = strIcNo;
    //        cmd.Parameters.Add("p_PAR_NATIONALITY", OdbcType.VarChar, 250).Value = strNationality;
    //        cmd.Parameters.Add("p_PAR_DOB", OdbcType.DateTime).Value = dtDOB;
    //        cmd.Parameters.Add("p_PAR_ADDRESS", OdbcType.VarChar, 250).Value = strAddress;
    //        cmd.Parameters.Add("p_PAR_CONTNO", OdbcType.VarChar, 20).Value = strContactNo;
    //        cmd.Parameters.Add("p_PAR_EMAIL", OdbcType.VarChar, 250).Value = strEmail;
    //        cmd.Parameters.Add("p_PAR_OTHERCOURSES", OdbcType.VarChar, 250).Value = strOtherCourses;
    //        cmd.Parameters.Add("p_PAR_PROGRAMDATE", OdbcType.Int, 9).Value = intProgDate;
    //        cmd.Parameters.Add("p_PAR_PROMOTIONCODE", OdbcType.VarChar, 250).Value = strPromotionCode;

    //        int iCount = Convert.ToInt16(cmd.ExecuteScalar());

    //        if (iCount >= 1)
    //        {
    //            bExactSame = true;
    //        }

    //        cmd.Dispose();
    //    }
    //    catch (Exception ex)
    //    {
    //        logErroMsg(ex.Message.ToString());
    //        throw ex;
    //    }
    //    finally
    //    {
    //        closeConn();
    //    }

    //    return bExactSame;
    //}

    public int updateParticipantById(int intParId, string strFirstName, string strLastName, string strEmail, string strContactNo, int intMalaysian, int intGender,
                                     DateTime dtDOB, int intHighestEdu, int intEngSpeak, int intEngWrite, int intProgDate)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PARTICIPANT SET" +
                     " PAR_FIRSTNAME = ?," +
                     " PAR_LASTNAME = ?," +
                     " PAR_EMAIL = ?," +
                     " PAR_CONTACTNO = ?," +
                     " PAR_NATIONALITY = ?," +
                     " PAR_GENDER = ?," +
                     " PAR_DOB = ?," +
                     " PAR_EDULEVEL = ?," +
                     " PAR_ENGSPEAKLEVEL = ?," +
                     " PAR_ENGWRITELEVEL = ?," +
                     " PAR_PROGRAMDATE = ?" +
                     " WHERE PAR_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAR_FIRSTNAME", OdbcType.VarChar, 250).Value = strFirstName;
            cmd.Parameters.Add("p_PAR_LASTNAME", OdbcType.VarChar, 250).Value = strLastName;
            cmd.Parameters.Add("p_PAR_EMAIL", OdbcType.VarChar, 250).Value = strEmail;
            cmd.Parameters.Add("p_PAR_CONTACTNO", OdbcType.VarChar, 50).Value = strContactNo;
            cmd.Parameters.Add("p_PAR_NATIONALITY", OdbcType.Int, 1).Value = intMalaysian;
            cmd.Parameters.Add("p_PAR_GENDER", OdbcType.Int, 1).Value = intGender;
            cmd.Parameters.Add("p_PAR_DOB", OdbcType.DateTime).Value = dtDOB;
            cmd.Parameters.Add("p_PAR_EDULEVEL", OdbcType.Int, 9).Value = intHighestEdu;
            cmd.Parameters.Add("p_PAR_ENGSPEAKLEVEL", OdbcType.Int, 9).Value = intEngSpeak;
            cmd.Parameters.Add("p_PAR_ENGWRITELEVEL", OdbcType.Int, 9).Value = intEngWrite;
            cmd.Parameters.Add("p_PAR_PROGRAMDATE", OdbcType.Int, 9).Value = intProgDate;
            cmd.Parameters.Add("p_PAR_ID", OdbcType.Int, 9).Value = intParId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                parId = intParId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    //public int updateParticipantById(int intParId, string strName, string strIcNo, string strNationality, DateTime dtDOB, string strAddress, string strContactNo, string strEmail, string strOtherCourses, int intProgDate, string strPromotionCode)
    //{
    //    int intRecordAffected = 0;

    //    try
    //    {
    //        OdbcTransaction trans;
    //        OdbcCommand cmd = new OdbcCommand();
    //        string strSql;

    //        cmd.Connection = openConn();
    //        trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

    //        strSql = "UPDATE TB_PARTICIPANT SET" +
    //                 " PAR_NAME = ?," +
    //                 " PAR_IC = ?," +
    //                 " PAR_NATIONALITY = ?," +
    //                 " PAR_DOB = ?," +
    //                 " PAR_ADDRESS = ?," +
    //                 " PAR_CONTNO = ?," +
    //                 " PAR_EMAIL = ?," +
    //                 " PAR_OTHERCOURSES = ?," +
    //                 " PAR_PROGRAMDATE = ?," +
    //                 " PAR_PROMOTIONCODE = ?" +
    //                 " WHERE PAR_ID = ?";

    //        cmd.Transaction = trans;
    //        cmd.CommandType = CommandType.Text;
    //        cmd.CommandText = strSql;

    //        cmd.Parameters.Add("p_PAR_NAME", OdbcType.VarChar, 250).Value = strName;
    //        cmd.Parameters.Add("p_PAR_IC", OdbcType.VarChar, 250).Value = strIcNo;
    //        cmd.Parameters.Add("p_PAR_NATIONALITY", OdbcType.VarChar, 250).Value = strNationality;
    //        cmd.Parameters.Add("p_PAR_DOB", OdbcType.DateTime).Value = dtDOB;
    //        cmd.Parameters.Add("p_PAR_ADDRESS", OdbcType.VarChar, 250).Value = strAddress;
    //        cmd.Parameters.Add("p_PAR_CONTNO", OdbcType.VarChar, 20).Value = strContactNo;
    //        cmd.Parameters.Add("p_PAR_EMAIL", OdbcType.VarChar, 250).Value = strEmail;
    //        cmd.Parameters.Add("p_PAR_OTHERCOURSES", OdbcType.VarChar, 250).Value = strOtherCourses;
    //        cmd.Parameters.Add("p_PAR_PROGRAMDATE", OdbcType.Int, 9).Value = intProgDate;
    //        cmd.Parameters.Add("p_PAR_PROMOTIONCODE", OdbcType.VarChar, 250).Value = strPromotionCode;
    //        cmd.Parameters.Add("p_PAR_ID", OdbcType.Int, 9).Value = intParId;

    //        intRecordAffected = cmd.ExecuteNonQuery();

    //        if (intRecordAffected == 1)
    //        {
    //            trans.Commit();
    //            parId = intParId;
    //        }
    //        else
    //        {
    //            trans.Rollback();
    //        }

    //        cmd.Dispose();
    //        trans.Dispose();
    //    }
    //    catch (Exception ex)
    //    {
    //        logErroMsg(ex.Message.ToString());
    //        throw ex;
    //    }
    //    finally
    //    {
    //        closeConn();
    //    }

    //    return intRecordAffected;
    //}
}

