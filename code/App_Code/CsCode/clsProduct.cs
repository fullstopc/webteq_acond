﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;

public class clsProduct : dbconnBase
{
    #region "Properties"
    #region "Details"
    private int _prodId;
    private string _prodCode;
    private string _prodName;
    private string _prodNameJp;
    private string _prodNameMs;
    private string _prodNameZh;
    private string _prodDName;
    private string _prodDNameJp;
    private string _prodDNameMs;
    private string _prodDNameZh;
    private string _prodPageTitleFUrl;
    private string _prodSnapshot;
    private string _prodSnapshotJp;
    private string _prodSnapshotMs;
    private string _prodSnapshotZh;
    private string _prodRemarks;
    private string _prodRemarksJp;
    private string _prodRemarksMs;
    private string _prodRemarksZh;
    private string _prodDesc;
    private string _prodDescJp;
    private string _prodDescMs;
    private string _prodDescZh;
    private int _prodType;
    private string _prodUOM;
    private int _prodWeight;
    private int _prodLength;
    private int _prodWidth;
    private int _prodHeight;
    private int _prodNew;
    private int _prodShowTop;
    private int _prodSold;
    private int _prodRent;
    private int _prodOutOfStock;
    private int _prodPreorder;
    private int _prodBest;
    private int _prodSalesActive;
    private int _prodWithGst; 
    private int _prodActive;
    private string _prodImage;
    private DateTime _prodCreation;
    private int _prodCreatedBy;
    private DateTime _prodLastUpdate;
    private int _prodUpdatedBy;
    private int _prodNOV;
    private int _prodNOP;
    private decimal _prodCost = 0;
    private decimal _prodPrice = 0;
    private decimal _prodPromPrice = 0;
    private DateTime _prodPromStartDate = DateTime.MinValue;
    private DateTime _prodPromEndDate = DateTime.MinValue;
    private int _prodCat1;
    private string _prodCat1Name;
    private int _prodCat2;
    private string _prodCat2Name;
    private int _prodCat3;
    private string _prodCat3Name;
    private int _prodCat4;
    private string _prodCat4Name;
    private int _prodCat5;
    private string _prodCat5Name;
    private int _prodCat6;
    private string _prodCat6Name;
    private int _prodCat7;
    private string _prodCat7Name;
    private int _prodCat8;
    private string _prodCat8Name;
    private int _prodCat9;
    private string _prodCat9Name;
    private int _prodCat10;
    private string _prodCat10Name;
    private string _prodStockStatus;
    private int _prodContainLiquid;
    private int _prodSales;
    private int _prodAddon;
    private int _grpId;
    private int _grpingId;
    private string _prodSupplier;
    private int _prodInvAllowNegative;
    private int _prodShowHome;
    private int _prodSmartChoice;
    private int _prodInstallation;

    #endregion

    #region "Hot Sales"
    private int _salesId;
    private string _salesCode;
    private DateTime _salesStartDate;
    private DateTime _salesEndDate;
    private int _salesQuantity;
    private decimal _salesPrice;
    private DateTime _salesCreation;
    private int _salesCreatedBy;
    private DateTime _salesLastUpdate;
    private int _salesUpdatedBy;
    #endregion

    #region "Gallery"
    private int _prodGallId;
    private string _prodGallImage;
    #endregion

    #region "Specification"
    private int _prodSpecId;
    private int _specGroupId;
    private string _prodSpecType;

    #region "Specification Item"
    private int _siId;
    private string _siName;
    private string _siOperator;
    private decimal _siPrice;
    private string _siAttachment;
    private string _siThumbnail;
    private string _siDateTitle;
    private int _siDateActive;
    private int _siOrder;
    private int _siActive;
    #endregion


    #endregion

    #region "promo price"
    private int _promoID = -1; // PK
    private int _promoProdID = -1; // FK
    private int _promoCreatedBy = -1;
    private int _promoUpdatedBy = -1;
    private int _promoActive = -1;
    private int _promoOrder = -1;
    private decimal _promoPrice = -1;
    private string _promoName;
    private DateTime _promoFrom;
    private DateTime _promoTo;
    private DateTime _promoCreatedAt;
    private DateTime _promoUpdatedAt;
    #endregion
    #endregion

    #region "Property Methods"
    #region "Details"
    public int prodId
    {
        get { return _prodId; }
        set { _prodId = value; }
    }
    public string prodCode
    {
        get { return _prodCode; }
        set { _prodCode = value; }
    }
    public string prodName
    {
        get { return _prodName; }
        set { _prodName = value; }
    }
    public string prodNameJp
    {
        get { return _prodNameJp; }
        set { _prodNameJp = value; }
    }
    public string prodNameMs
    {
        get { return _prodNameMs; }
        set { _prodNameMs = value; }
    }
    public string prodNameZh
    {
        get { return _prodNameZh; }
        set { _prodNameZh = value; }
    }
    public string prodDName
    {
        get { return _prodDName; }
        set { _prodDName = value; }
    }
    public string prodDNameJp
    {
        get { return _prodDNameJp; }
        set { _prodDNameJp = value; }
    }
    public string prodDNameMs
    {
        get { return _prodDNameMs; }
        set { _prodDNameMs = value; }
    }
    public string prodDNameZh
    {
        get { return _prodDNameZh; }
        set { _prodDNameZh = value; }
    }
    public string prodPageTitleFUrl
    {
        get { return _prodPageTitleFUrl; }
        set { _prodPageTitleFUrl = value; }
    }
    public string prodSnapshot
    {
        get { return _prodSnapshot; }
        set { _prodSnapshot = value; }
    }
    public string prodSnapshotJp
    {
        get { return _prodSnapshotJp; }
        set { _prodSnapshotJp = value; }
    }
    public string prodSnapshotMs
    {
        get { return _prodSnapshotMs; }
        set { _prodSnapshotMs = value; }
    }
    public string prodSnapshotZh
    {
        get { return _prodSnapshotZh; }
        set { _prodSnapshotZh = value; }
    }
    public string prodRemarks
    {
        get { return _prodRemarks; }
        set { _prodRemarks = value; }
    }
    public string prodRemarksJp
    {
        get { return _prodRemarksJp; }
        set { _prodRemarksJp = value; }
    }
    public string prodRemarksMs
    {
        get { return _prodRemarksMs; }
        set { _prodRemarksMs = value; }
    }
    public string prodRemarksZh
    {
        get { return _prodRemarksZh; }
        set { _prodRemarksZh = value; }
    }
    public string prodDesc
    {
        get { return _prodDesc; }
        set { _prodDesc = value; }
    }
    public string prodDescJp
    {
        get { return _prodDescJp; }
        set { _prodDescJp = value; }
    }
    public string prodDescMs
    {
        get { return _prodDescMs; }
        set { _prodDescMs = value; }
    }
    public string prodDescZh
    {
        get { return _prodDescZh; }
        set { _prodDescZh = value; }
    }
    public int prodType
    {
        get { return _prodType; }
        set { _prodType = value; }
    }
    public string prodUOM
    {
        get { return _prodUOM; }
        set { _prodUOM = value; }
    }
    public int prodWeight
    {
        get { return _prodWeight; }
        set { _prodWeight = value; }
    }
    public int prodLength
    {
        get { return _prodLength; }
        set { _prodLength = value; }
    }
    public int prodWidth
    {
        get { return _prodWidth; }
        set { _prodWidth = value; }
    }
    public int prodHeight
    {
        get { return _prodHeight; }
        set { _prodHeight = value; }
    }
    public int prodNew
    {
        get { return _prodNew; }
        set { _prodNew = value; }
    }
    public int prodShowTop
    {
        get { return _prodShowTop; }
        set { _prodShowTop = value; }
    }
    public int prodSold
    {
        get { return _prodSold; }
        set { _prodSold = value; }
    }
    public int prodRent
    {
        get { return _prodRent; }
        set { _prodRent = value; }
    }
    public int prodOutOfStock
    {
        get { return _prodOutOfStock; }
        set { _prodOutOfStock = value; }
    }
    public int prodPreorder
    {
        get { return _prodPreorder; }
        set { _prodPreorder = value; }
    }
    public int prodWithGst
    {
        get { return _prodWithGst; }
        set { _prodWithGst = value; }
    }
    public int prodActive
    {
        get { return _prodActive; }
        set { _prodActive = value; }
    }
    public int prodBest
    {
        get { return _prodBest; }
        set { _prodBest = value; }
    }
    public int prodSalesActive
    {
        get { return _prodSalesActive; }
        set { _prodSalesActive = value; }
    }
    public string prodImage
    {
        get { return _prodImage; }
        set { _prodImage = value; }
    }
    public DateTime prodCreation
    {
        get { return _prodCreation; }
        set { _prodCreation = value; }
    }
    public int prodCreatedBy
    {
        get { return _prodCreatedBy; }
        set { _prodCreatedBy = value; }
    }
    public DateTime prodLastUpdate
    {
        get { return _prodLastUpdate; }
        set { _prodLastUpdate = value; }
    }
    public int prodUpdatedBy
    {
        get { return _prodUpdatedBy; }
        set { _prodUpdatedBy = value; }
    }
    public int prodNOV
    {
        get { return _prodNOV; }
        set { _prodNOV = value; }
    }
    public int prodNOP
    {
        get { return _prodNOP; }
        set { _prodNOP = value; }
    }
    public decimal prodCost
    {
        get { return _prodCost; }
        set { _prodCost = value; }
    }
    public decimal prodPrice
    {
        get { return _prodPrice; }
        set { _prodPrice = value; }
    }
    public decimal prodPromPrice
    {
        get { return _prodPromPrice; }
        set { _prodPromPrice = value; }
    }
    public DateTime prodPromStartDate
    {
        get { return _prodPromStartDate; }
        set { _prodPromStartDate = value; }
    }
    public DateTime prodPromEndDate
    {
        get { return _prodPromEndDate; }
        set { _prodPromEndDate = value; }
    }
    public int prodCat2
    {
        get { return _prodCat2; }
        set { _prodCat2 = value; }
    }
    public int prodCat1
    {
        get { return _prodCat1; }
        set { _prodCat1 = value; }
    }
    public int prodCat3
    {
        get { return _prodCat3; }
        set { _prodCat3 = value; }
    }
    public int prodCat4
    {
        get { return _prodCat4; }
        set { _prodCat4 = value; }
    }
    public int prodCat5
    {
        get { return _prodCat5; }
        set { _prodCat5 = value; }
    }
    public int prodCat6
    {
        get { return _prodCat6; }
        set { _prodCat6 = value; }
    }
    public int prodCat7
    {
        get { return _prodCat7; }
        set { _prodCat7 = value; }
    }
    public int prodCat8
    {
        get { return _prodCat8; }
        set { _prodCat8 = value; }
    }
    public int prodCat9
    {
        get { return _prodCat9; }
        set { _prodCat9 = value; }
    }
    public int prodCat10
    {
        get { return _prodCat10; }
        set { _prodCat10 = value; }
    }
    public string prodCat2Name
    {
        get { return _prodCat2Name; }
        set { _prodCat2Name = value; }
    }
    public string prodCat1Name
    {
        get { return _prodCat1Name; }
        set { _prodCat1Name = value; }
    }
    public string prodCat3Name
    {
        get { return _prodCat3Name; }
        set { _prodCat3Name = value; }
    }
    public string prodCat4Name
    {
        get { return _prodCat4Name; }
        set { _prodCat4Name = value; }
    }
    public string prodCat5Name
    {
        get { return _prodCat5Name; }
        set { _prodCat5Name = value; }
    }
    public string prodCat6Name
    {
        get { return _prodCat6Name; }
        set { _prodCat6Name = value; }
    }
    public string prodCat7Name
    {
        get { return _prodCat7Name; }
        set { _prodCat7Name = value; }
    }
    public string prodCat8Name
    {
        get { return _prodCat8Name; }
        set { _prodCat8Name = value; }
    }
    public string prodCat9Name
    {
        get { return _prodCat9Name; }
        set { _prodCat9Name = value; }
    }
    public string prodCat10Name
    {
        get { return _prodCat10Name; }
        set { _prodCat10Name = value; }
    }
    public string prodStockStatus
    {
        get { return _prodStockStatus; }
        set { _prodStockStatus = value; }
    }
    public int prodContainLiquid
    {
        get { return _prodContainLiquid; }
        set { _prodContainLiquid  = value; }
    }
    public int prodSales
    {
        get { return _prodSales; }
        set { _prodSales = value; }
    }
    public int prodAddon
    {
        get { return _prodAddon; }
        set { _prodAddon = value; }
    }
    public int grpId
    {
        get { return _grpId; }
        set { _grpId = value; }
    }
    public int grpingId
    {
        get { return _grpingId; }
        set { _grpingId = value; }
    }
    public string prodSupplier
    {
        get { return _prodSupplier; }
        set { _prodSupplier = value; }
    }
    public int prodInvAllowNegative
    {
        get { return _prodInvAllowNegative; }
        set { _prodInvAllowNegative = value; }
    }
    public int prodShowHome
    {
        get { return _prodShowHome; }
        set { _prodShowHome = value; }
    }
    public int prodSmartChoice
    {
        get { return _prodSmartChoice; }
        set { _prodSmartChoice = value; }
    }
    public int prodInstallation
    {
        get { return _prodInstallation; }
        set { _prodInstallation = value; }
    }
    #endregion

    #region "Hot Sales"
    public int salesId
    {
        get { return _salesId; }
        set { _salesId = value; }
    }

    public string salesCode
    {
        get { return _salesCode; }
        set { _salesCode = value; }
    }

    public DateTime salesStartDate
    {
        get { return _salesStartDate; }
        set { _salesStartDate = value; }
    }

    public DateTime salesEndDate
    {
        get { return _salesEndDate; }
        set { _salesEndDate = value; }
    }

    public int salesQuantity
    {
        get { return _salesQuantity; }
        set { _salesQuantity = value; }
    }

    public decimal salesPrice
    {
        get { return _salesPrice; }
        set { _salesPrice = value; }
    }

    public DateTime salesCreation
    {
        get { return _salesCreation; }
        set { _salesCreation = value; }
    }

    public int salesCreatedBy
    {
        get { return _salesCreatedBy; }
        set { _salesCreatedBy = value; }
    }

    public DateTime salesLastUpdate
    {
        get { return _salesLastUpdate; }
        set { _salesLastUpdate = value; }
    }

    public int salesUpdatedBy
    {
        get { return _salesUpdatedBy; }
        set { _salesUpdatedBy = value; }
    }
    #endregion

    #region "Gallery"
    public int prodGallId
    {
        get { return _prodGallId; }
        set { _prodGallId = value; }
    }

    public string prodGallImage
    {
        get { return _prodGallImage; }
        set { _prodGallImage = value; }
    }
    #endregion

    #region "Specification"
    public int prodSpecId
    {
        get { return _prodSpecId; }
        set { _prodSpecId = value; }
    }

    public int specGroupId
    {
        get { return _specGroupId; }
        set { _specGroupId = value; }
    }

    public string prodSpecType
    {
        get { return _prodSpecType; }
        set { _prodSpecType = value; }
    }

    #region "Specification Item"
    public int siId
    {
        get { return _siId; }
        set { _siId = value; }
    }

    public string siName
    {
        get { return _siName; }
        set { _siName = value; }
    }

    public string siOperator
    {
        get { return _siOperator; }
        set { _siOperator = value; }
    }

    public decimal siPrice
    {
        get { return _siPrice; }
        set { _siPrice = value; }
    }

    public string siAttachment
    {
        get { return _siAttachment; }
        set { _siAttachment = value; }
    }
    public string siThumbnail
    {
        get { return _siThumbnail; }
        set { _siThumbnail = value; }
    }
    public string siDateTitle
    {
        get { return _siDateTitle; }
        set { _siDateTitle = value; }
    }

    public int siDateActive
    {
        get { return _siDateActive; }
        set { _siDateActive = value; }
    }

    public int siOrder
    {
        get { return _siOrder; }
        set { _siOrder = value; }
    }

    public int siActive
    {
        get { return _siActive; }
        set { _siActive = value; }
    }
    #endregion
    #endregion

    #region "promo price"
    public int promoID
    {
        get { return _promoID; }
        set { _promoID = value; }
    }
    public int promoProdID
    {
        get { return _promoProdID; }
        set { _promoProdID = value; }
    }
    public int promoCreatedBy
    {
        get { return _promoCreatedBy; }
        set { _promoCreatedBy = value; }
    }
    public int promoUpdatedBy
    {
        get { return _promoUpdatedBy; }
        set { _promoUpdatedBy = value; }
    }
    public int promoActive
    {
        get { return _promoActive; }
        set { _promoActive = value; }
    }
    public int promoOrder
    {
        get { return _promoOrder; }
        set { _promoOrder = value; }
    }
    public decimal promoPrice
    {
        get { return _promoPrice; }
        set { _promoPrice = value; }
    }
    public string promoName
    {
        get { return _promoName; }
        set { _promoName = value; }
    }
    public DateTime promoFrom
    {
        get { return _promoFrom; }
        set { _promoFrom = value; }
    }
    public DateTime promoTo
    {
        get { return _promoTo; }
        set { _promoTo = value; }
    }
    public DateTime promoCreatedAt
    {
        get { return _promoCreatedAt; }
        set { _promoCreatedAt = value; }
    }
    public DateTime promoUpdatedAt
    {
        get { return _promoUpdatedAt; }
        set { _promoUpdatedAt = value; }
    }
    #endregion
    #endregion

    #region "Public Methods"
    public clsProduct()
	{
    }

    public int moveProdByGrpId(int intGrpId, int intNewGrpId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODGROUP SET GRP_ID = ?" +
                     " WHERE GRP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_NEW_GRP_ID", OdbcType.Int, 9).Value = intNewGrpId;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int assignGrpToProd(int intProdId, int intGrpId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRODGROUP (" +
                    "PROD_ID, " +
                    "GRP_ID " +
                    ") VALUES " +
                   "(?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isProdCodeExist(string strProdCode, int intProdId)
    {
        Boolean boolExist = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODUCT" +
                     " WHERE PROD_CODE = ?";

            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 50).Value = strProdCode;

            if (intProdId > 0)
            {
                strSql += " AND PROD_ID != ?";
                cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            }
            
            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount > 0)
            {
                boolExist = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public Boolean isPageTitleFUrlExist(string strPageTitleFURL, int intProdId)
    {
        Boolean bExist = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_PRODUCT" +
                     " WHERE PROD_PAGETITLEFURL = ?";

            cmd.Parameters.Add("p_PROD_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strPageTitleFURL;

            if (intProdId > 0)
            {
                strSql += " AND PROD_ID != ?";
                cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                bExist = true;
            }

            cmd.Dispose();
            da.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExist;
    }

    public int addProduct(string strProdCode, string strProdName, string strProdNameJp, string strProdNameMs, string strProdNameZh, string strProdDName, string strProdDNameJp, string strProdDNameMs, string strProdDNameZh,
                          string strPageTitleFURL, string strProdUOM, int intProdWeight, int intProdLength, int intProdWidth, int intProdHeight, int intProdNew, int intProdBest, int intProdActive, int intProdShowTop, int intProdSold, int intProdRent,
                          int intOutOfStock, int intPreorder, int intWithGst, string strProdImg, int intProdCreatedBy, int intProdStockStatus, int intProdContainLiquid, int intProdAddon, int intProdSales)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRODUCT (" +
                    "PROD_CODE, " +
                    "PROD_NAME, " +
                    "PROD_NAME_JP, " +
                    "PROD_NAME_MS, " +
                    "PROD_NAME_ZH, " +
                    "PROD_DNAME, " +
                    "PROD_DNAME_JP, " +
                    "PROD_DNAME_MS, " +
                    "PROD_DNAME_ZH, " +
                    "PROD_PAGETITLEFURL, " +
                    "PROD_UOM, " +
                    "PROD_WEIGHT, " +
                    "PROD_LENGTH, " +
                    "PROD_WIDTH, " +
                    "PROD_HEIGHT, " +
                    "PROD_NEW, " +
                    "PROD_BEST, " +
                    "PROD_ACTIVE, " +  
                    "PROD_SHOWTOP, " +
                    "PROD_SOLD, " +
                    "PROD_RENT, " +  
                    "PROD_OUTOFSTOCK, " +
                    "PROD_PREORDER, " +
                    "PROD_WITH_GST, " + 
                    "PROD_IMAGE, " +
                    "PROD_CREATEDBY, " +
                    "PROD_CREATION, " +
                    "PROD_STOCKSTATUS, " +
                    "PROD_CONTAINLIQUID, " +
                    "PROD_ADDON, " +
                    "PROD_SALES " +
                    ") VALUES " +
                   "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE(),?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 50).Value = strProdCode;
            cmd.Parameters.Add("p_PROD_NAME", OdbcType.VarChar, 250).Value = strProdName;
            cmd.Parameters.Add("p_PROD_NAME_JP", OdbcType.VarChar, 250).Value = strProdNameJp;
            cmd.Parameters.Add("p_PROD_NAME_MS", OdbcType.VarChar, 250).Value = strProdNameMs;
            cmd.Parameters.Add("p_PROD_NAME_ZH", OdbcType.VarChar, 250).Value = strProdNameZh;
            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_PROD_DNAME_JP", OdbcType.VarChar, 250).Value = strProdDNameJp;
            cmd.Parameters.Add("p_PROD_DNAME_MS", OdbcType.VarChar, 250).Value = strProdDNameMs;
            cmd.Parameters.Add("p_PROD_DNAME_ZH", OdbcType.VarChar, 250).Value = strProdDNameZh;
            cmd.Parameters.Add("p_PROD_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strPageTitleFURL;
            cmd.Parameters.Add("p_PROD_UOM", OdbcType.VarChar, 9).Value = strProdUOM;
            cmd.Parameters.Add("p_PROD_WEIGHT", OdbcType.Int, 9).Value = intProdWeight;
            cmd.Parameters.Add("p_PROD_LENGTH", OdbcType.Int, 9).Value = intProdLength;
            cmd.Parameters.Add("p_PROD_WIDTH", OdbcType.Int, 9).Value = intProdWidth;
            cmd.Parameters.Add("p_PROD_HEIGHT", OdbcType.Int, 9).Value = intProdHeight;
            cmd.Parameters.Add("p_PROD_NEW", OdbcType.Int, 1).Value = intProdNew;
            cmd.Parameters.Add("p_PROD_BEST", OdbcType.Int, 1).Value = intProdBest;
            cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intProdActive;
            cmd.Parameters.Add("p_PROD_SHOWTOP", OdbcType.Int, 1).Value = intProdShowTop;
            cmd.Parameters.Add("p_PROD_SOLD", OdbcType.Int, 1).Value = intProdSold;
            cmd.Parameters.Add("p_PROD_RENT", OdbcType.Int, 1).Value = intProdRent;
            cmd.Parameters.Add("p_PROD_OUTOFSTOCK", OdbcType.Int, 1).Value = intOutOfStock;
            cmd.Parameters.Add("p_PROD_PREORDER", OdbcType.Int, 1).Value = intPreorder;
            cmd.Parameters.Add("p_PROD_WITH_GST", OdbcType.Int, 1).Value = intWithGst;
            cmd.Parameters.Add("p_PROD_IMAGE", OdbcType.VarChar, 250).Value = strProdImg;
            cmd.Parameters.Add("p_PROD_CREATEDBY", OdbcType.Int, 9).Value = intProdCreatedBy;
            cmd.Parameters.Add("p_PROD_STOCKSTATUS", OdbcType.VarChar, 10).Value = intProdStockStatus;
            cmd.Parameters.Add("p_PROD_CONTAINLIQUID", OdbcType.Int, 1).Value = intProdContainLiquid;
            cmd.Parameters.Add("p_PROD_ADDON", OdbcType.Int, 1).Value = intProdAddon;
            cmd.Parameters.Add("p_PROD_SALES", OdbcType.Int, 1).Value = intProdSales;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                prodId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    /*Customized for SeaAuto - Start*/
    public int addProduct2(string strProdCode, string strProdName, string strProdNameJp, string strProdNameMs, string strProdNameZh, string strProdDName, string strProdDNameJp, string strProdDNameMs, string strProdDNameZh,
                          string strPageTitleFURL, int intprodType, string strProdUOM, int intProdWeight, int intProdLength, int intProdWidth, int intProdHeight, int intProdNew, int intProdBest, int intProdActive, int intProdShowTop, int intProdSold, int intProdRent,
                          int intOutOfStock, int intPreorder, int intWithGst, string strProdImg, int intProdCreatedBy, int intProdStockStatus, int intProdContainLiquid)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRODUCT (" +
                    "PROD_CODE, " +
                    "PROD_NAME, " +
                    "PROD_NAME_JP, " +
                    "PROD_NAME_MS, " +
                    "PROD_NAME_ZH, " +
                    "PROD_DNAME, " +
                    "PROD_DNAME_JP, " +
                    "PROD_DNAME_MS, " +
                    "PROD_DNAME_ZH, " +
                    "PROD_PAGETITLEFURL, " +
                    "PROD_TYPE, " +
                    "PROD_UOM, " +
                    "PROD_WEIGHT, " +
                    "PROD_LENGTH, " +
                    "PROD_WIDTH, " +
                    "PROD_HEIGHT, " +
                    "PROD_NEW, " +
                    "PROD_BEST, " +
                    "PROD_ACTIVE, " +
                    "PROD_SHOWTOP, " +
                    "PROD_SOLD, " +
                    "PROD_RENT, " +
                    "PROD_OUTOFSTOCK, " +
                    "PROD_PREORDER, " +
                    "PROD_WITH_GST, " +
                    "PROD_IMAGE, " +
                    "PROD_CREATEDBY, " +
                    "PROD_CREATION, " +
                    "PROD_STOCKSTATUS, " +
                    "PROD_CONTAINLIQUID " +
                    ") VALUES " +
                   "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE(),?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 50).Value = strProdCode;
            cmd.Parameters.Add("p_PROD_NAME", OdbcType.VarChar, 250).Value = strProdName;
            cmd.Parameters.Add("p_PROD_NAME_JP", OdbcType.VarChar, 250).Value = strProdNameJp;
            cmd.Parameters.Add("p_PROD_NAME_MS", OdbcType.VarChar, 250).Value = strProdNameMs;
            cmd.Parameters.Add("p_PROD_NAME_ZH", OdbcType.VarChar, 250).Value = strProdNameZh;
            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_PROD_DNAME_JP", OdbcType.VarChar, 250).Value = strProdDNameJp;
            cmd.Parameters.Add("p_PROD_DNAME_MS", OdbcType.VarChar, 250).Value = strProdDNameMs;
            cmd.Parameters.Add("p_PROD_DNAME_ZH", OdbcType.VarChar, 250).Value = strProdDNameZh;
            cmd.Parameters.Add("p_PROD_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strPageTitleFURL;
            cmd.Parameters.Add("p_PROD_TYPE", OdbcType.Int, 1).Value = intprodType;
            cmd.Parameters.Add("p_PROD_UOM", OdbcType.VarChar, 9).Value = strProdUOM;
            cmd.Parameters.Add("p_PROD_WEIGHT", OdbcType.Int, 9).Value = intProdWeight;
            cmd.Parameters.Add("p_PROD_LENGTH", OdbcType.Int, 9).Value = intProdLength;
            cmd.Parameters.Add("p_PROD_WIDTH", OdbcType.Int, 9).Value = intProdWidth;
            cmd.Parameters.Add("p_PROD_HEIGHT", OdbcType.Int, 9).Value = intProdHeight;
            cmd.Parameters.Add("p_PROD_NEW", OdbcType.Int, 1).Value = intProdNew;
            cmd.Parameters.Add("p_PROD_BEST", OdbcType.Int, 1).Value = intProdBest;
            cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intProdActive;
            cmd.Parameters.Add("p_PROD_SHOWTOP", OdbcType.Int, 1).Value = intProdShowTop;
            cmd.Parameters.Add("p_PROD_SOLD", OdbcType.Int, 1).Value = intProdSold;
            cmd.Parameters.Add("p_PROD_RENT", OdbcType.Int, 1).Value = intProdRent;
            cmd.Parameters.Add("p_PROD_OUTOFSTOCK", OdbcType.Int, 1).Value = intOutOfStock;
            cmd.Parameters.Add("p_PROD_PREORDER", OdbcType.Int, 1).Value = intPreorder;
            cmd.Parameters.Add("p_PROD_WITH_GST", OdbcType.Int, 1).Value = intWithGst;
            cmd.Parameters.Add("p_PROD_IMAGE", OdbcType.VarChar, 250).Value = strProdImg;
            cmd.Parameters.Add("p_PROD_CREATEDBY", OdbcType.Int, 9).Value = intProdCreatedBy;
            cmd.Parameters.Add("p_PROD_STOCKSTATUS", OdbcType.VarChar, 10).Value = intProdStockStatus;
            cmd.Parameters.Add("p_PROD_CONTAINLIQUID", OdbcType.Int, 1).Value = intProdContainLiquid;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                prodId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameProdSet4(int intProdId, string strProdCode, string strProdName, string strProdNameJp, string strProdNameMs, string strProdNameZh, string strProdDName, string strProdDNameJp, string strProdDNameMs, string strProdDNameZh,
                                       string strPageTitleFUrl, int intProdType, string strProdUOM, int intProdWeight, int intProdLength, int intProdWidth, int intProdHeight, int intProdNew, int intProdBest, int intProdActive, int intProdShowTop, int intProdSold, int intProdRent,
                                       int intProdOutOfStock, int intProdPreorder, int intProdWithGst, string strProdImage, int intProdStockStatus, int intProdContainLiquid)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODUCT" +
                " WHERE PROD_ID = ?" +
                " AND BINARY PROD_CODE = ?" +
                " AND BINARY PROD_NAME = ?" +
                " AND BINARY PROD_NAME_JP = ?" +
                " AND BINARY PROD_NAME_MS = ?" +
                " AND BINARY PROD_NAME_ZH = ?" +
                " AND BINARY PROD_DNAME = ?" +
                " AND BINARY PROD_DNAME_JP = ?" +
                " AND BINARY PROD_DNAME_MS = ?" +
                " AND BINARY PROD_DNAME_ZH = ?" +
                " AND BINARY PROD_PAGETITLEFURL = ?" +
                " AND PROD_TYPE = ?" +
                " AND BINARY PROD_UOM = ?" +
                " AND PROD_WEIGHT = ?" +
                " AND PROD_LENGTH = ?" +
                " AND PROD_WIDTH = ?" +
                " AND PROD_HEIGHT = ?" +
                " AND PROD_NEW = ?" +
                " AND PROD_BEST = ?" +
                " AND PROD_ACTIVE = ?" +
                " AND PROD_SHOWTOP = ?" +
                " AND PROD_SOLD = ?" +
                " AND PROD_RENT = ?" +
                " AND PROD_OUTOFSTOCK = ?" +
                " AND PROD_PREORDER = ?" +
                " AND PROD_WITH_GST = ?" +
                " AND BINARY PROD_IMAGE = ?" +
                " AND PROD_STOCKSTATUS = ?" +
                " AND PROD_CONTAINLIQUID = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 50).Value = strProdCode;
            cmd.Parameters.Add("p_PROD_NAME", OdbcType.VarChar, 250).Value = strProdName;
            cmd.Parameters.Add("p_PROD_NAME_JP", OdbcType.VarChar, 250).Value = strProdNameJp;
            cmd.Parameters.Add("p_PROD_NAME_MS", OdbcType.VarChar, 250).Value = strProdNameMs;
            cmd.Parameters.Add("p_PROD_NAME_ZH", OdbcType.VarChar, 250).Value = strProdNameZh;
            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_PROD_DNAME_JP", OdbcType.VarChar, 250).Value = strProdDNameJp;
            cmd.Parameters.Add("p_PROD_DNAME_MS", OdbcType.VarChar, 250).Value = strProdDNameMs;
            cmd.Parameters.Add("p_PROD_DNAME_ZH", OdbcType.VarChar, 250).Value = strProdDNameZh;
            cmd.Parameters.Add("p_PROD_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strPageTitleFUrl;
            cmd.Parameters.Add("p_PROD_TYPE", OdbcType.Int, 1).Value = intProdType;
            cmd.Parameters.Add("p_PROD_UOM", OdbcType.VarChar, 250).Value = strProdUOM;
            cmd.Parameters.Add("p_PROD_WEIGHT", OdbcType.Int, 9).Value = intProdWeight;
            cmd.Parameters.Add("p_PROD_LENGTH", OdbcType.Int, 9).Value = intProdLength;
            cmd.Parameters.Add("p_PROD_WIDTH", OdbcType.Int, 9).Value = intProdWidth;
            cmd.Parameters.Add("p_PROD_HEIGHT", OdbcType.Int, 9).Value = intProdHeight;
            cmd.Parameters.Add("p_PROD_NEW", OdbcType.Int, 1).Value = intProdNew;
            cmd.Parameters.Add("p_PROD_BEST", OdbcType.Int, 1).Value = intProdBest;
            cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intProdActive;
            cmd.Parameters.Add("p_PROD_SHOWTOP", OdbcType.Int, 1).Value = intProdShowTop;
            cmd.Parameters.Add("p_PROD_SOLD", OdbcType.Int, 1).Value = intProdSold;
            cmd.Parameters.Add("p_PROD_RENT", OdbcType.Int, 1).Value = intProdRent;
            cmd.Parameters.Add("p_PROD_OUTOFSTOCK", OdbcType.Int, 1).Value = intProdOutOfStock;
            cmd.Parameters.Add("p_PROD_PREORDER", OdbcType.Int, 1).Value = intProdPreorder;
            cmd.Parameters.Add("p_PROD_WITH_GST", OdbcType.Int, 1).Value = intProdWithGst;
            cmd.Parameters.Add("p_PROD_IMAGE", OdbcType.VarChar, 250).Value = strProdImage;
            cmd.Parameters.Add("p_PROD_STOCKSTATUS", OdbcType.VarChar, 10).Value = intProdStockStatus;
            cmd.Parameters.Add("p_PROD_CONTAINLIQUID", OdbcType.Int, 1).Value = intProdContainLiquid;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateProdById2(int intProdId, string strProdCode, string strProdName, string strProdNameJp, string strProdNameMs, string strProdNameZh, string strProdDName, string strProdDNameJp, string strProdDNameMs, string strProdDNameZh,
                              string strPageTitleFUrl, int intProdType, string strProdUOM, int intProdWeight, int intProdLength, int intProdWidth, int intProdHeight, int intProdNew, int intProdBest, int intProdActive, int intProdShowTop, int intProdSold, int intProdRent,
                              int intProdOutOfStock, int intProdPreorder, int intProdWithGst, string strProdImage, int intProdUpdatedBy, int intProdStockStatus, int intProdContainLiquid)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODUCT SET" +
                     " PROD_CODE = ?," +
                     " PROD_NAME = ?," +
                     " PROD_NAME_JP = ?," +
                     " PROD_NAME_MS = ?," +
                     " PROD_NAME_ZH = ?," +
                     " PROD_DNAME = ?," +
                     " PROD_DNAME_JP = ?," +
                     " PROD_DNAME_MS = ?," +
                     " PROD_DNAME_ZH = ?," +
                     " PROD_PAGETITLEFURL = ?," +
                     " PROD_TYPE = ?," +
                     " PROD_UOM = ?," +
                     " PROD_WEIGHT = ?," +
                     " PROD_LENGTH = ?," +
                     " PROD_WIDTH = ?," +
                     " PROD_HEIGHT = ?," +
                     " PROD_NEW = ?," +
                     " PROD_BEST = ?," +
                     " PROD_ACTIVE = ?," +
                     " PROD_SHOWTOP = ?," +
                     " PROD_SOLD = ?," +
                     " PROD_RENT = ?," +
                     " PROD_OUTOFSTOCK = ?," +
                     " PROD_PREORDER = ?," +
                     " PROD_WITH_GST = ?," +
                     " PROD_IMAGE = ?," +
                     " PROD_UPDATEDBY = ?," +
                     " PROD_LASTUPDATE = SYSDATE()," +
                     " PROD_STOCKSTATUS = ?," +
                     " PROD_CONTAINLIQUID = ?" +
                     " WHERE PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 50).Value = strProdCode;
            cmd.Parameters.Add("p_PROD_NAME", OdbcType.VarChar, 250).Value = strProdName;
            cmd.Parameters.Add("p_PROD_NAME_JP", OdbcType.VarChar, 250).Value = strProdNameJp;
            cmd.Parameters.Add("p_PROD_NAME_MS", OdbcType.VarChar, 250).Value = strProdNameMs;
            cmd.Parameters.Add("p_PROD_NAME_ZH", OdbcType.VarChar, 250).Value = strProdNameZh;
            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_PROD_DNAME_JP", OdbcType.VarChar, 250).Value = strProdDNameJp;
            cmd.Parameters.Add("p_PROD_DNAME_MS", OdbcType.VarChar, 250).Value = strProdDNameMs;
            cmd.Parameters.Add("p_PROD_DNAME_ZH", OdbcType.VarChar, 250).Value = strProdDNameZh;
            cmd.Parameters.Add("p_PROD_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strPageTitleFUrl;
            cmd.Parameters.Add("p_PROD_TYPE", OdbcType.Int, 1).Value = intProdType;
            cmd.Parameters.Add("p_PROD_UOM", OdbcType.VarChar, 250).Value = strProdUOM;
            cmd.Parameters.Add("p_PROD_WEIGHT", OdbcType.Int, 9).Value = intProdWeight;
            cmd.Parameters.Add("p_PROD_LENGTH", OdbcType.Int, 9).Value = intProdLength;
            cmd.Parameters.Add("p_PROD_WIDTH", OdbcType.Int, 9).Value = intProdWidth;
            cmd.Parameters.Add("p_PROD_HEIGHT", OdbcType.Int, 9).Value = intProdHeight;
            cmd.Parameters.Add("p_PROD_NEW", OdbcType.Int, 9).Value = intProdNew;
            cmd.Parameters.Add("p_PROD_BEST", OdbcType.Int, 9).Value = intProdBest;
            cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 9).Value = intProdActive;
            cmd.Parameters.Add("p_PROD_SHOWTOP", OdbcType.Int, 9).Value = intProdShowTop;
            cmd.Parameters.Add("p_PROD_SOLD", OdbcType.Int, 1).Value = intProdSold;
            cmd.Parameters.Add("p_PROD_RENT", OdbcType.Int, 1).Value = intProdRent;
            cmd.Parameters.Add("p_PROD_OUTOFSTOCK", OdbcType.Int, 1).Value = intProdOutOfStock;
            cmd.Parameters.Add("p_PROD_PREORDER", OdbcType.Int, 1).Value = intProdPreorder;
            cmd.Parameters.Add("p_PROD_WITH_GST", OdbcType.Int, 1).Value = intProdWithGst;
            cmd.Parameters.Add("p_PROD_IMAGE", OdbcType.VarChar, 250).Value = strProdImage;
            cmd.Parameters.Add("p_PROD_UPDATEDBY", OdbcType.Int, 9).Value = intProdUpdatedBy;
            cmd.Parameters.Add("p_PROD_STOCKSTATUS", OdbcType.VarChar, 10).Value = intProdStockStatus;
            cmd.Parameters.Add("p_PROD_CONTAINLIQUID", OdbcType.Int, 1).Value = intProdContainLiquid;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractProdById5(int intProdId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_CODE, A.PROD_NAME, A.PROD_NAME_JP, A.PROD_NAME_MS, A.PROD_NAME_ZH, A.PROD_DNAME, A.PROD_DNAME_JP, A.PROD_DNAME_MS, A.PROD_DNAME_ZH," +
                     " A.PROD_PAGETITLEFURL, A.PROD_TYPE, A.PROD_UOM, A.PROD_WEIGHT, A.PROD_LENGTH, A.PROD_WIDTH, A.PROD_HEIGHT, A.PROD_IMAGE," +
                     " A.PROD_NEW, A.PROD_BEST, A.PROD_SHOWTOP, A.PROD_SOLD, A.PROD_RENT, A.PROD_OUTOFSTOCK, A.PROD_PREORDER, A.PROD_WITH_GST, A.PROD_ACTIVE, A.PROD_STOCKSTATUS, A.PROD_CONTAINLIQUID" +
                     " FROM TB_PRODUCT A" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            if (intActive != 0)
            {
                strSql += " AND A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                prodId = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ID"].ToString());
                prodCode = dataSet.Tables[0].Rows[0]["PROD_CODE"].ToString();
                prodName = dataSet.Tables[0].Rows[0]["PROD_NAME"].ToString();
                prodNameJp = dataSet.Tables[0].Rows[0]["PROD_NAME_JP"].ToString();
                prodNameMs = dataSet.Tables[0].Rows[0]["PROD_NAME_MS"].ToString();
                prodNameZh = dataSet.Tables[0].Rows[0]["PROD_NAME_ZH"].ToString();
                prodDName = dataSet.Tables[0].Rows[0]["PROD_DNAME"].ToString();
                prodDNameJp = dataSet.Tables[0].Rows[0]["PROD_DNAME_JP"].ToString();
                prodDNameMs = dataSet.Tables[0].Rows[0]["PROD_DNAME_MS"].ToString();
                prodDNameZh = dataSet.Tables[0].Rows[0]["PROD_DNAME_ZH"].ToString();
                prodPageTitleFUrl = dataSet.Tables[0].Rows[0]["PROD_PAGETITLEFURL"].ToString();
                prodType = int.Parse(dataSet.Tables[0].Rows[0]["PROD_TYPE"].ToString());
                prodUOM = dataSet.Tables[0].Rows[0]["PROD_UOM"].ToString();
                prodWeight = int.Parse(dataSet.Tables[0].Rows[0]["PROD_WEIGHT"].ToString());
                prodLength = int.Parse(dataSet.Tables[0].Rows[0]["PROD_LENGTH"].ToString());
                prodWidth = int.Parse(dataSet.Tables[0].Rows[0]["PROD_WIDTH"].ToString());
                prodHeight = int.Parse(dataSet.Tables[0].Rows[0]["PROD_HEIGHT"].ToString());
                prodNew = int.Parse(dataSet.Tables[0].Rows[0]["PROD_NEW"].ToString());
                prodBest = int.Parse(dataSet.Tables[0].Rows[0]["PROD_BEST"].ToString());
                //prodSalesActive = int.Parse(dataSet.Tables[0].Rows[0]["PROD_HOTSALES"].ToString());
                prodActive = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ACTIVE"].ToString());
                prodShowTop = int.Parse(dataSet.Tables[0].Rows[0]["PROD_SHOWTOP"].ToString());
                prodSold = int.Parse(dataSet.Tables[0].Rows[0]["PROD_SOLD"].ToString());
                prodRent = int.Parse(dataSet.Tables[0].Rows[0]["PROD_RENT"].ToString());
                prodOutOfStock = int.Parse(dataSet.Tables[0].Rows[0]["PROD_OUTOFSTOCK"].ToString());
                prodPreorder = int.Parse(dataSet.Tables[0].Rows[0]["PROD_PREORDER"].ToString());
                prodWithGst = int.Parse(dataSet.Tables[0].Rows[0]["PROD_WITH_GST"].ToString());
                prodImage = dataSet.Tables[0].Rows[0]["PROD_IMAGE"].ToString();
                prodStockStatus = dataSet.Tables[0].Rows[0]["PROD_STOCKSTATUS"].ToString();
                prodContainLiquid = int.Parse(dataSet.Tables[0].Rows[0]["PROD_CONTAINLIQUID"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    /*Customized for SeaAuto - End*/

    public Boolean extractProdById(int intProdId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_CODE, A.PROD_NAME, A.PROD_NAME_JP, A.PROD_NAME_MS, A.PROD_NAME_ZH, A.PROD_DNAME, A.PROD_DNAME_JP, A.PROD_DNAME_MS, A.PROD_DNAME_ZH," +
                     " A.PROD_PAGETITLEFURL, A.PROD_UOM, A.PROD_WEIGHT, A.PROD_LENGTH, A.PROD_WIDTH, A.PROD_HEIGHT, A.PROD_IMAGE," +
                     " A.PROD_NEW, A.PROD_BEST, A.PROD_SHOWTOP, A.PROD_SOLD, A.PROD_RENT, A.PROD_OUTOFSTOCK, A.PROD_PREORDER, A.PROD_WITH_GST, A.PROD_ACTIVE, A.PROD_STOCKSTATUS, A.PROD_CONTAINLIQUID," +
                     " A.PROD_SALES, A.PROD_ADDON" +
                     " FROM TB_PRODUCT A" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            if (intActive != 0)
            {
                strSql += " AND A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                prodId = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ID"].ToString());
                prodCode = dataSet.Tables[0].Rows[0]["PROD_CODE"].ToString();
                prodName = dataSet.Tables[0].Rows[0]["PROD_NAME"].ToString();
                prodNameJp = dataSet.Tables[0].Rows[0]["PROD_NAME_JP"].ToString();
                prodNameMs = dataSet.Tables[0].Rows[0]["PROD_NAME_MS"].ToString();
                prodNameZh = dataSet.Tables[0].Rows[0]["PROD_NAME_ZH"].ToString();
                prodDName = dataSet.Tables[0].Rows[0]["PROD_DNAME"].ToString();
                prodDNameJp = dataSet.Tables[0].Rows[0]["PROD_DNAME_JP"].ToString();
                prodDNameMs = dataSet.Tables[0].Rows[0]["PROD_DNAME_MS"].ToString();
                prodDNameZh = dataSet.Tables[0].Rows[0]["PROD_DNAME_ZH"].ToString();
                prodPageTitleFUrl = dataSet.Tables[0].Rows[0]["PROD_PAGETITLEFURL"].ToString();
                prodUOM = dataSet.Tables[0].Rows[0]["PROD_UOM"].ToString();
                prodWeight = int.Parse(dataSet.Tables[0].Rows[0]["PROD_WEIGHT"].ToString());
                prodLength = int.Parse(dataSet.Tables[0].Rows[0]["PROD_LENGTH"].ToString());
                prodWidth = int.Parse(dataSet.Tables[0].Rows[0]["PROD_WIDTH"].ToString());
                prodHeight = int.Parse(dataSet.Tables[0].Rows[0]["PROD_HEIGHT"].ToString());
                prodNew = int.Parse(dataSet.Tables[0].Rows[0]["PROD_NEW"].ToString());
                prodBest = int.Parse(dataSet.Tables[0].Rows[0]["PROD_BEST"].ToString());
                //prodSalesActive = int.Parse(dataSet.Tables[0].Rows[0]["PROD_HOTSALES"].ToString());
                prodActive = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ACTIVE"].ToString());
                prodShowTop = int.Parse(dataSet.Tables[0].Rows[0]["PROD_SHOWTOP"].ToString());
                prodSold = int.Parse(dataSet.Tables[0].Rows[0]["PROD_SOLD"].ToString());
                prodRent = int.Parse(dataSet.Tables[0].Rows[0]["PROD_RENT"].ToString());
                prodOutOfStock = int.Parse(dataSet.Tables[0].Rows[0]["PROD_OUTOFSTOCK"].ToString());
                prodPreorder = int.Parse(dataSet.Tables[0].Rows[0]["PROD_PREORDER"].ToString());
                prodWithGst = int.Parse(dataSet.Tables[0].Rows[0]["PROD_WITH_GST"].ToString());
                prodImage = dataSet.Tables[0].Rows[0]["PROD_IMAGE"].ToString();
                prodStockStatus = dataSet.Tables[0].Rows[0]["PROD_STOCKSTATUS"].ToString();
                prodContainLiquid = int.Parse(dataSet.Tables[0].Rows[0]["PROD_CONTAINLIQUID"].ToString());
                prodSales = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PROD_SALES"]);
                prodAddon = Convert.ToInt16(dataSet.Tables[0].Rows[0]["PROD_ADDON"]);
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractProdById2(int intProdId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_SNAPSHOT, A.PROD_SNAPSHOT_JP, A.PROD_SNAPSHOT_MS, A.PROD_SNAPSHOT_ZH," +
                     " A.PROD_REMARKS, A.PROD_REMARKS_JP, A.PROD_REMARKS_MS, A.PROD_REMARKS_ZH," +
                     " A.PROD_DESC, A.PROD_DESC_JP, A.PROD_DESC_MS, A.PROD_DESC_ZH" +
                     " FROM TB_PRODUCT A" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            if (intActive != 0)
            {
                strSql += " AND A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                prodId = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ID"].ToString());
                prodSnapshotJp = dataSet.Tables[0].Rows[0]["PROD_SNAPSHOT_JP"].ToString();
                prodSnapshotMs = dataSet.Tables[0].Rows[0]["PROD_SNAPSHOT_MS"].ToString();
                prodSnapshotZh = dataSet.Tables[0].Rows[0]["PROD_SNAPSHOT_ZH"].ToString();
                prodSnapshot = dataSet.Tables[0].Rows[0]["PROD_SNAPSHOT"].ToString();
                prodRemarks = dataSet.Tables[0].Rows[0]["PROD_REMARKS"].ToString();
                prodRemarksJp = dataSet.Tables[0].Rows[0]["PROD_REMARKS_JP"].ToString();
                prodRemarksMs = dataSet.Tables[0].Rows[0]["PROD_REMARKS_MS"].ToString();
                prodRemarksZh = dataSet.Tables[0].Rows[0]["PROD_REMARKS_ZH"].ToString();
                prodDesc = dataSet.Tables[0].Rows[0]["PROD_DESC"].ToString();
                prodDescJp = dataSet.Tables[0].Rows[0]["PROD_DESC_JP"].ToString();
                prodDescMs = dataSet.Tables[0].Rows[0]["PROD_DESC_MS"].ToString();
                prodDescZh = dataSet.Tables[0].Rows[0]["PROD_DESC_ZH"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractProdById3(int intProdId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, B.PRODPRC_COST, B.PRODPRC_PRICE, B.PRODPRC_PROMPRICE, B.PRODPRC_PROMSTARTDATE, B.PRODPRC_PROMENDDATE" +
                     " FROM (TB_PRODUCT A LEFT JOIN TB_PRODPRICE B ON (A.PROD_ID = B.PROD_ID))" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            if (intActive != 0)
            {
                strSql += " AND A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                prodId = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ID"].ToString());
                prodCost = dataSet.Tables[0].Rows[0]["PRODPRC_COST"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[0]["PRODPRC_COST"]) : 0;
                prodPrice = dataSet.Tables[0].Rows[0]["PRODPRC_PRICE"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[0]["PRODPRC_PRICE"]) : 0;
                prodPromPrice = dataSet.Tables[0].Rows[0]["PRODPRC_PROMPRICE"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[0]["PRODPRC_PROMPRICE"]) : 0;
                prodPromStartDate = dataSet.Tables[0].Rows[0]["PRODPRC_PROMSTARTDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PRODPRC_PROMSTARTDATE"]) : DateTime.MaxValue;
                prodPromEndDate = dataSet.Tables[0].Rows[0]["PRODPRC_PROMENDDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PRODPRC_PROMENDDATE"]) : DateTime.MaxValue;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractProdById6(int intProdId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_SUPPLIER, A.PROD_INVALLOWNEGATIVE" +
                     " FROM TB_PRODUCT A" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            if (intActive != 0)
            {
                strSql += " AND A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                prodId = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ID"].ToString());
                prodSupplier = dataSet.Tables[0].Rows[0]["PROD_SUPPLIER"].ToString();
                prodInvAllowNegative = int.Parse(dataSet.Tables[0].Rows[0]["PROD_INVALLOWNEGATIVE"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractProdByCode(string strCode, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_NAME, A.PROD_DNAME, A.PROD_CODE,A.PROD_UOM," +
                     " B.PRODPRC_PRICE, B.PRODPRC_PROMPRICE, B.PRODPRC_PROMSTARTDATE, B.PRODPRC_PROMENDDATE" +
                     " FROM (TB_PRODUCT A LEFT JOIN TB_PRODPRICE B ON (A.PROD_ID = B.PROD_ID))" +
                     " WHERE A.PROD_CODE = ?";

            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 20).Value = strCode;

            if (intActive != 0)
            {
                strSql += " AND A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                prodId = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ID"].ToString());
                prodCode = dataSet.Tables[0].Rows[0]["PROD_CODE"].ToString();
                prodName = dataSet.Tables[0].Rows[0]["PROD_NAME"].ToString();
                prodDName = dataSet.Tables[0].Rows[0]["PROD_DNAME"].ToString();
                prodUOM = dataSet.Tables[0].Rows[0]["PROD_UOM"].ToString();
                prodPrice = dataSet.Tables[0].Rows[0]["PRODPRC_PRICE"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[0]["PRODPRC_PRICE"]) : 0;
                prodPromPrice = dataSet.Tables[0].Rows[0]["PRODPRC_PROMPRICE"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[0]["PRODPRC_PROMPRICE"]) : 0;
                prodPromStartDate = dataSet.Tables[0].Rows[0]["PRODPRC_PROMSTARTDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PRODPRC_PROMSTARTDATE"]) : DateTime.MaxValue;
                prodPromEndDate = dataSet.Tables[0].Rows[0]["PRODPRC_PROMENDDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PRODPRC_PROMENDDATE"]) : DateTime.MaxValue;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractProdByCode(string strCode,string strName, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_NAME, A.PROD_DNAME, A.PROD_CODE,A.PROD_UOM," +
                     " B.PRODPRC_PRICE, B.PRODPRC_PROMPRICE, B.PRODPRC_PROMSTARTDATE, B.PRODPRC_PROMENDDATE" +
                     " FROM (TB_PRODUCT A LEFT JOIN TB_PRODPRICE B ON (A.PROD_ID = B.PROD_ID))" +
                     " WHERE A.PROD_CODE = ? AND A.PROD_DNAME = ?";

            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 20).Value = strCode;
            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 250).Value = strName;
            if (intActive != 0)
            {
                strSql += " AND A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                prodId = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ID"].ToString());
                prodCode = dataSet.Tables[0].Rows[0]["PROD_CODE"].ToString();
                prodName = dataSet.Tables[0].Rows[0]["PROD_NAME"].ToString();
                prodDName = dataSet.Tables[0].Rows[0]["PROD_DNAME"].ToString();
                prodUOM = dataSet.Tables[0].Rows[0]["PROD_UOM"].ToString();
                prodPrice = dataSet.Tables[0].Rows[0]["PRODPRC_PRICE"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[0]["PRODPRC_PRICE"]) : 0;
                prodPromPrice = dataSet.Tables[0].Rows[0]["PRODPRC_PROMPRICE"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[0]["PRODPRC_PROMPRICE"]) : 0;
                prodPromStartDate = dataSet.Tables[0].Rows[0]["PRODPRC_PROMSTARTDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PRODPRC_PROMSTARTDATE"]) : DateTime.MaxValue;
                prodPromEndDate = dataSet.Tables[0].Rows[0]["PRODPRC_PROMENDDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PRODPRC_PROMENDDATE"]) : DateTime.MaxValue;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractProdByCode2(string strCode, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_NAME, A.PROD_DNAME, A.PROD_CODE, A.PROD_UOM" +
                     " FROM TB_PRODUCT A" +
                     " WHERE A.PROD_CODE = ?";

            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 20).Value = strCode;

            if (intActive != 0)
            {
                strSql += " AND A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                prodId = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ID"].ToString());
                prodCode = dataSet.Tables[0].Rows[0]["PROD_CODE"].ToString();
                prodName = dataSet.Tables[0].Rows[0]["PROD_NAME"].ToString();
                prodDName = dataSet.Tables[0].Rows[0]["PROD_DNAME"].ToString();
                prodUOM = dataSet.Tables[0].Rows[0]["PROD_UOM"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractProdByCode2(string strCode,string strName, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_NAME, A.PROD_DNAME, A.PROD_CODE, A.PROD_UOM" +
                     " FROM TB_PRODUCT A" +
                     " WHERE A.PROD_CODE = ? AND A.PROD_DNAME = ?";

            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 20).Value = strCode;
            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strName;
            if (intActive != 0)
            {
                strSql += " AND A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                prodId = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ID"].ToString());
                prodCode = dataSet.Tables[0].Rows[0]["PROD_CODE"].ToString();
                prodName = dataSet.Tables[0].Rows[0]["PROD_NAME"].ToString();
                prodDName = dataSet.Tables[0].Rows[0]["PROD_DNAME"].ToString();
                prodUOM = dataSet.Tables[0].Rows[0]["PROD_UOM"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }


    public int getPrevProdId(string strProdDName, int intProdId, int intActive)
    {
        int intPrevProdId = 0;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT PROD_ID FROM TB_PRODUCT" +
                     " WHERE (PROD_DNAME = ? AND PROD_ID < ?)" +
                     " OR PROD_DNAME < ?";

            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_PROD_DNAME2", OdbcType.VarChar, 250).Value = strProdDName;

            if (intActive != 0)
            {
                strSql += " AND PROD_ACTIVE = ?";

                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " ORDER BY PROD_DNAME DESC, PROD_ID DESC LIMIT 1";
            
            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intPrevProdId = Convert.ToInt16(cmd.ExecuteScalar());

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intPrevProdId;
    }

    public int getNextProdId(string strProdDName, int intProdId, int intActive)
    {
        int intNextProdId = 0;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT PROD_ID FROM TB_PRODUCT" +
                     " WHERE (PROD_DNAME = ? AND PROD_ID > ?)" +
                     " OR PROD_DNAME > ?";

            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_PROD_DNAME2", OdbcType.VarChar, 250).Value = strProdDName;

            if (intActive != 0)
            {
                strSql += " AND PROD_ACTIVE = ?";

                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            strSql += " ORDER BY PROD_DNAME ASC, PROD_ID ASC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intNextProdId = Convert.ToInt16(cmd.ExecuteScalar());

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intNextProdId;
    }

    public DataSet getProdList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_CODE, A.PROD_NAME, A.PROD_NAME_JP, A.PROD_DNAME, A.PROD_DNAME_JP, A.PROD_UOM, A.PROD_WEIGHT, A.PROD_LENGTH," +
                     " A.PROD_WIDTH, A.PROD_HEIGHT, A.PROD_NEW, A.PROD_BEST, A.PROD_SHOWTOP, A.PROD_ACTIVE,A.PROD_CREATION,A.PROD_LASTUPDATE," +
                     " A.PROD_NAME_MS, A.PROD_NAME_ZH, A.PROD_DNAME_MS, A.PROD_DNAME_ZH," +
                     " B.PRODPRC_COST AS PROD_COST, B.PRODPRC_PRICE AS PROD_PRICE, B.PRODPRC_PROMPRICE AS PROD_PROMPRICE," +
                     " B.PRODPRC_PROMSTARTDATE AS PROD_PROMSTARTDATE, B.PRODPRC_PROMENDDATE AS PROD_PROMENDDATE, C.INV_QTY AS PROD_INVQTY" +
                     " FROM ((TB_PRODUCT A LEFT JOIN TB_PRODPRICE B ON (A.PROD_ID = B.PROD_ID)) LEFT JOIN TB_INVENTORY C ON (A.PROD_ID = C.PROD_ID))";

            if (intActive != 0)
            {
                strSql += " WHERE A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getProdList2(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_CODE, A.PROD_NAME, A.PROD_NAME_JP, A.PROD_DNAME, A.PROD_DNAME_JP," +
                     " A.PROD_IMAGE, A.PROD_NEW, A.PROD_BEST, A.PROD_SHOWTOP, A.PROD_ACTIVE," +
                     " A.PROD_NAME_MS, A.PROD_NAME_ZH, A.PROD_DNAME_MS, A.PROD_DNAME_ZH, A.PROD_ADDON" +
                     " FROM TB_PRODUCT A";

            if (intActive != 0)
            {
                strSql += " WHERE A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getProdList3(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_CODE, A.PROD_NAME," +
                     " (SELECT SUM(B.CS_TOTAL) FROM TB_COLORSIZE B WHERE B.PROD_ID = A.PROD_ID) AS V_TOTAL" +
                     " FROM TB_PRODUCT A";

            if (intActive != 0)
            {
                strSql += " WHERE A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getProdList3(int intCatId, int intGrpId, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_CODE, A.PROD_NAME," +
                     " (SELECT SUM(B.CS_TOTAL) FROM TB_COLORSIZE B WHERE B.PROD_ID = A.PROD_ID) AS V_TOTAL" +
                     " FROM TB_PRODUCT A" +
                     " WHERE 1 = 1";

            if (intActive != 0)
            {
                strSql += " AND A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            if (intCatId != 0)
            {
                strSql += " AND A.PROD_ID IN (SELECT B.PROD_ID FROM TB_PRODGROUP B WHERE B.GRP_ID = ?)";
                cmd.Parameters.Add("p_GRP_ID", OdbcType.Int, 9).Value = intCatId;
            }
            else if (intGrpId != 0)
            {
                clsGroup grp = new clsGroup();
                string strCatIds = grp.getGrpIdsByGrpingId(intGrpId, 0);

                strSql += " AND A.PROD_ID IN (SELECT B.PROD_ID FROM TB_PRODGROUP B WHERE B.GRP_ID IN (" + strCatIds + "))";
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }


    public string getProdIdsByGrpId(int intGrpId)
    {
        string strIds = "";

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT GROUP_CONCAT(CAST(A.PROD_ID AS CHAR))" +
                     " FROM TB_PRODGROUP A" +
                     " WHERE A.GRP_ID = ?";

            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intGrpId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            strIds = cmd.ExecuteScalar().ToString();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return strIds;
    }

    public DataSet getProdGroupListById(int intProdId)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.GRP_ID, B.GRPING_ID" +
                     " FROM (TB_PRODGROUP A LEFT JOIN TB_GROUP B ON (A.GRP_ID = B.GRP_ID))" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getProdGroupListById2(int intId)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT A.PRODGRP_ID, A.PROD_ID, A.GRP_ID, B.GRP_NAME, B.GRPING_ID, B.GRP_PARENT, C.GRP_NAME AS GRP_PARENTNAME, C.GRP_PARENT AS GRP_PARENTPARENT, D.GRP_NAME AS GRP_PARENTPARENTNAME" +
                     " FROM ((TB_PRODGROUP A LEFT JOIN TB_GROUP B ON (A.GRP_ID = B.GRP_ID)) LEFT JOIN TB_GROUP C ON (B.GRP_PARENT = C.GRP_ID)) LEFT JOIN TB_GROUP D ON (C.GRP_PARENT = D.GRP_ID)" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intId;

            //HttpContext.Current.Response.Write("getProdGroupListById2<br />");
            //HttpContext.Current.Response.Write(DateTime.Now + "<br />");
            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
            //HttpContext.Current.Response.Write(DateTime.Now + "<br />");
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    
    public string getProdCodeList(int intActive)
    {
        string strCodeList = "";

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT GROUP_CONCAT(CONCAT(A.PROD_CODE,' | ',A.PROD_DNAME))" +
                     " FROM TB_PRODUCT A";

            if (intActive != 0)
            {
                strSql += " WHERE A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            strCodeList = cmd.ExecuteScalar().ToString();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return strCodeList;
    }

    public DataSet getRelatedProdList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT PROD_ID, RELPROD_ID, RELPROD_CODE, RELPROD_NAME, RELPROD_DNAME, RELPROD_DESC, RELPROD_SNAPSHOT, RELPROD_UOM, " +
                     "RELPROD_WEIGHT, RELPROD_LENGTH, RELPROD_WIDTH, RELPROD_HEIGHT, RELPROD_NEW, RELPROD_SHOWTOP, RELPROD_ACTIVE, " +
                     "RELPROD_IMAGE, RELPROD_NOV, RELPROD_NOP, RELPROD_CREATEDBY, RELPROD_CREATION, RELPROD_UPDATEDBY, RELPROD_LASTUPDATE, " +
                     "RELPROD_CAT1, RELPROD_CAT1NAME, RELPROD_CAT2, RELPROD_CAT2NAME, RELPROD_COST, RELPROD_PRICE, RELPROD_PROMPRICE, " +
                     "RELPROD_PROMSTARTDATE, RELPROD_PROMENDDATE" +
                     " FROM VW_RELPRODUCT";

            if (intActive != 0)
            {
                strSql += " WHERE RELPROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getRelatedProdListById(int intProdId, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.RELPRODLIST_RELPRODID AS RELPROD_ID, B.PROD_NAME AS RELPROD_NAME, B.PROD_IMAGE AS RELPROD_IMAGE" +
                     " FROM TB_RELATEDPRODLIST A, TB_PRODUCT B" +
                     " WHERE A.RELPRODLIST_RELPRODID = B.PROD_ID" +
                     " AND A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            if (intActive != 0)
            {
                strSql += " AND B.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getAddonProducts(int intProdId, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_ADDON_ID AS ADDON_ID, B.PROD_NAME AS ADDON_NAME, B.PROD_IMAGE AS ADDON_IMAGE" +
                     " FROM TB_PRODADDON A, TB_PRODUCT B" +
                     " WHERE A.PROD_ADDON_ID = B.PROD_ID" +
                     " AND A.PROD_ID = ? AND B.PROD_ADDON = 1";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            if (intActive != 0)
            {
                strSql += " AND B.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public int deleteProduct(int intProdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRODUCT WHERE PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.VarChar, 10);
            cmd.Parameters["p_PROD_ID"].Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteProductGroup(int intProdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRODGROUP WHERE PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.VarChar, 10);
            cmd.Parameters["p_PROD_ID"].Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteProductGroup(int intProdId, int intGrpId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRODGROUP WHERE PROD_ID = ? AND GRP_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteProductGroup2(int intProdId, string strGrpId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRODGROUP WHERE PROD_ID = ? AND GRP_ID NOT IN (?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.VarChar, 250).Value = strGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }


    public Boolean isExactSameProdSet1(int intProdId, string strProdCode, string strProdName, string strProdNameJp, string strProdNameMs, string strProdNameZh, string strProdDName, string strProdDNameJp, string strProdDNameMs, string strProdDNameZh,
                                       string strPageTitleFUrl, string strProdUOM, int intProdWeight, int intProdLength, int intProdWidth, int intProdHeight, int intProdNew, int intProdBest, int intProdActive, int intProdShowTop, int intProdSold, int intProdRent,
                                       int intProdOutOfStock, int intProdPreorder, int intProdWithGst, string strProdImage, int intProdStockStatus, int intProdContainLiquid, int intProdAddon, int intProdSales)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODUCT" +
                " WHERE PROD_ID = ?" +
                " AND BINARY PROD_CODE = ?" +
                " AND BINARY PROD_NAME = ?" +
                " AND BINARY PROD_NAME_JP = ?" +
                " AND BINARY PROD_NAME_MS = ?" +
                " AND BINARY PROD_NAME_ZH = ?" +
                " AND BINARY PROD_DNAME = ?" +
                " AND BINARY PROD_DNAME_JP = ?" +
                " AND BINARY PROD_DNAME_MS = ?" +
                " AND BINARY PROD_DNAME_ZH = ?" +
                " AND BINARY PROD_PAGETITLEFURL = ?" +
                " AND BINARY PROD_UOM = ?" +
                " AND PROD_WEIGHT = ?" +
                " AND PROD_LENGTH = ?" +
                " AND PROD_WIDTH = ?" +
                " AND PROD_HEIGHT = ?" +
                " AND PROD_NEW = ?" +
                " AND PROD_BEST = ?" +
                " AND PROD_ACTIVE = ?" +
                " AND PROD_SHOWTOP = ?" +
                " AND PROD_SOLD = ?" +
                " AND PROD_RENT = ?" +
                " AND PROD_OUTOFSTOCK = ?" +
                " AND PROD_PREORDER = ?" +
                " AND PROD_WITH_GST = ?" +
                " AND BINARY PROD_IMAGE = ?" +
                " AND PROD_STOCKSTATUS = ?" +
                " AND PROD_CONTAINLIQUID = ?" +
                " AND PROD_ADDON = ?" +
                " AND PROD_SALES = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 50).Value = strProdCode;
            cmd.Parameters.Add("p_PROD_NAME", OdbcType.VarChar, 250).Value = strProdName;
            cmd.Parameters.Add("p_PROD_NAME_JP", OdbcType.VarChar, 250).Value = strProdNameJp;
            cmd.Parameters.Add("p_PROD_NAME_MS", OdbcType.VarChar, 250).Value = strProdNameMs;
            cmd.Parameters.Add("p_PROD_NAME_ZH", OdbcType.VarChar, 250).Value = strProdNameZh;
            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_PROD_DNAME_JP", OdbcType.VarChar, 250).Value = strProdDNameJp;
            cmd.Parameters.Add("p_PROD_DNAME_MS", OdbcType.VarChar, 250).Value = strProdDNameMs;
            cmd.Parameters.Add("p_PROD_DNAME_ZH", OdbcType.VarChar, 250).Value = strProdDNameZh;
            cmd.Parameters.Add("p_PROD_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strPageTitleFUrl;
            cmd.Parameters.Add("p_PROD_UOM", OdbcType.VarChar, 250).Value = strProdUOM;
            cmd.Parameters.Add("p_PROD_WEIGHT", OdbcType.Int, 9).Value = intProdWeight;
            cmd.Parameters.Add("p_PROD_LENGTH", OdbcType.Int, 9).Value = intProdLength;
            cmd.Parameters.Add("p_PROD_WIDTH", OdbcType.Int, 9).Value = intProdWidth;
            cmd.Parameters.Add("p_PROD_HEIGHT", OdbcType.Int, 9).Value = intProdHeight;
            cmd.Parameters.Add("p_PROD_NEW", OdbcType.Int, 1).Value = intProdNew;
            cmd.Parameters.Add("p_PROD_BEST", OdbcType.Int, 1).Value = intProdBest;
            cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intProdActive;
            cmd.Parameters.Add("p_PROD_SHOWTOP", OdbcType.Int, 1).Value = intProdShowTop;
            cmd.Parameters.Add("p_PROD_SOLD", OdbcType.Int, 1).Value = intProdSold;
            cmd.Parameters.Add("p_PROD_RENT", OdbcType.Int, 1).Value = intProdRent;
            cmd.Parameters.Add("p_PROD_OUTOFSTOCK", OdbcType.Int, 1).Value = intProdOutOfStock;
            cmd.Parameters.Add("p_PROD_PREORDER", OdbcType.Int, 1).Value = intProdPreorder;
            cmd.Parameters.Add("p_PROD_WITH_GST", OdbcType.Int, 1).Value = intProdWithGst;
            cmd.Parameters.Add("p_PROD_IMAGE", OdbcType.VarChar, 250).Value = strProdImage;
            cmd.Parameters.Add("p_PROD_STOCKSTATUS", OdbcType.VarChar, 10).Value = intProdStockStatus;
            cmd.Parameters.Add("p_PROD_CONTAINLIQUID", OdbcType.Int, 1).Value = intProdContainLiquid;
            cmd.Parameters.Add("p_PROD_ADDON", OdbcType.Int, 1).Value = intProdAddon;
            cmd.Parameters.Add("p_PROD_SALES", OdbcType.Int, 1).Value = intProdSales;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateProdById(int intProdId, string strProdCode, string strProdName, string strProdNameJp, string strProdNameMs, string strProdNameZh, string strProdDName, string strProdDNameJp, string strProdDNameMs, string strProdDNameZh,
                              string strPageTitleFUrl, string strProdUOM, int intProdWeight, int intProdLength, int intProdWidth, int intProdHeight, int intProdNew, int intProdBest, int intProdActive, int intProdShowTop, int intProdSold, int intProdRent,
                              int intProdOutOfStock, int intProdPreorder, int intProdWithGst, string strProdImage, int intProdUpdatedBy, int intProdStockStatus, int intProdContainLiquid, int intProdAddon, int intProdSales)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODUCT SET" +   
                     " PROD_CODE = ?," +
                     " PROD_NAME = ?," +
                     " PROD_NAME_JP = ?," +
                     " PROD_NAME_MS = ?," +
                     " PROD_NAME_ZH = ?," +
                     " PROD_DNAME = ?," +
                     " PROD_DNAME_JP = ?," +
                     " PROD_DNAME_MS = ?," +
                     " PROD_DNAME_ZH = ?," +
                     " PROD_PAGETITLEFURL = ?," +
                     " PROD_UOM = ?," +
                     " PROD_WEIGHT = ?," +
                     " PROD_LENGTH = ?," +
                     " PROD_WIDTH = ?," +
                     " PROD_HEIGHT = ?," +
                     " PROD_NEW = ?," +
                     " PROD_BEST = ?," +
                     " PROD_ACTIVE = ?," +
                     " PROD_SHOWTOP = ?," +
                     " PROD_SOLD = ?," +
                     " PROD_RENT = ?," +
                     " PROD_OUTOFSTOCK = ?," +
                     " PROD_PREORDER = ?," +
                     " PROD_WITH_GST = ?," +
                     " PROD_IMAGE = ?," +                
                     " PROD_UPDATEDBY = ?," +
                     " PROD_LASTUPDATE = SYSDATE()," +
                     " PROD_STOCKSTATUS = ?," +
                     " PROD_CONTAINLIQUID = ?," +
                     " PROD_ADDON = ?," +
                     " PROD_SALES = ?" +
                     " WHERE PROD_ID = ?";     
            
            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 50).Value = strProdCode;
            cmd.Parameters.Add("p_PROD_NAME", OdbcType.VarChar, 250).Value = strProdName;
            cmd.Parameters.Add("p_PROD_NAME_JP", OdbcType.VarChar, 250).Value = strProdNameJp;
            cmd.Parameters.Add("p_PROD_NAME_MS", OdbcType.VarChar, 250).Value = strProdNameMs;
            cmd.Parameters.Add("p_PROD_NAME_ZH", OdbcType.VarChar, 250).Value = strProdNameZh;
            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_PROD_DNAME_JP", OdbcType.VarChar, 250).Value = strProdDNameJp;
            cmd.Parameters.Add("p_PROD_DNAME_MS", OdbcType.VarChar, 250).Value = strProdDNameMs;
            cmd.Parameters.Add("p_PROD_DNAME_ZH", OdbcType.VarChar, 250).Value = strProdDNameZh;
            cmd.Parameters.Add("p_PROD_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strPageTitleFUrl;
            cmd.Parameters.Add("p_PROD_UOM", OdbcType.VarChar, 250).Value = strProdUOM;
            cmd.Parameters.Add("p_PROD_WEIGHT", OdbcType.Int, 9).Value = intProdWeight;
            cmd.Parameters.Add("p_PROD_LENGTH", OdbcType.Int, 9).Value = intProdLength;
            cmd.Parameters.Add("p_PROD_WIDTH", OdbcType.Int, 9).Value = intProdWidth;
            cmd.Parameters.Add("p_PROD_HEIGHT", OdbcType.Int, 9).Value = intProdHeight;
            cmd.Parameters.Add("p_PROD_NEW", OdbcType.Int, 9).Value = intProdNew;
            cmd.Parameters.Add("p_PROD_BEST", OdbcType.Int, 9).Value = intProdBest;
            cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 9).Value = intProdActive;
            cmd.Parameters.Add("p_PROD_SHOWTOP", OdbcType.Int, 9).Value = intProdShowTop;
            cmd.Parameters.Add("p_PROD_SOLD", OdbcType.Int, 1).Value = intProdSold;
            cmd.Parameters.Add("p_PROD_RENT", OdbcType.Int, 1).Value = intProdRent;
            cmd.Parameters.Add("p_PROD_OUTOFSTOCK", OdbcType.Int, 1).Value = intProdOutOfStock;
            cmd.Parameters.Add("p_PROD_PREORDER", OdbcType.Int, 1).Value = intProdPreorder;
            cmd.Parameters.Add("p_PROD_WITH_GST", OdbcType.Int, 1).Value = intProdWithGst;
            cmd.Parameters.Add("p_PROD_IMAGE", OdbcType.VarChar, 250).Value = strProdImage;
            cmd.Parameters.Add("p_PROD_UPDATEDBY", OdbcType.Int, 9).Value = intProdUpdatedBy;
            cmd.Parameters.Add("p_PROD_STOCKSTATUS", OdbcType.VarChar, 10).Value = intProdStockStatus;
            cmd.Parameters.Add("p_PROD_CONTAINLIQUID", OdbcType.Int, 1).Value = intProdContainLiquid;
            cmd.Parameters.Add("p_PROD_ADDON", OdbcType.Int, 1).Value = intProdAddon;
            cmd.Parameters.Add("p_PROD_SALES", OdbcType.Int, 1).Value = intProdSales;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateListProdById(int intProdId, string strProdCode, string strProdName, string strProdDName, int intProdNew, int intProdBest, int intProdActive, int intProdShowTop)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODUCT SET" +
                     " PROD_CODE = ?," +
                     " PROD_NAME = ?," +
                     " PROD_DNAME = ?," +
                     " PROD_NEW = ?," +
                     " PROD_BEST = ?," +
                     " PROD_ACTIVE = ?," +
                     " PROD_SHOWTOP = ?" +
                     " WHERE PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 50).Value = strProdCode;
            cmd.Parameters.Add("p_PROD_NAME", OdbcType.VarChar, 250).Value = strProdName;
            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_PROD_NEW", OdbcType.Int, 9).Value = intProdNew;
            cmd.Parameters.Add("p_PROD_BEST", OdbcType.Int, 9).Value = intProdBest;
            cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 9).Value = intProdActive;
            cmd.Parameters.Add("p_PROD_SHOWTOP", OdbcType.Int, 9).Value = intProdShowTop;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameProdHotSalesDataSet(int intProdId, int intHotSalesActive)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODUCT" +
                     " WHERE PROD_ID = ?" +
                     " AND PROD_HOTSALES = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_PROD_HOTSALES", OdbcType.Int, 1).Value = intHotSalesActive;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateProdHotSalesById(int intProdId, int intHotSalesActive, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODUCT SET" +
                     " PROD_HOTSALES = ?," +
                     " PROD_LASTUPDATE = SYSDATE()," +
                     " PROD_UPDATEDBY = ?" +
                     " WHERE PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_HOTSALES", OdbcType.Int, 1).Value = intHotSalesActive;
            cmd.Parameters.Add("p_PROD_UPDATEDBY", OdbcType.Int, 1).Value = intUpdatedBy;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addProdPrice(int intProdId, decimal decProdCost, decimal decProdPrice, decimal decProdPromPrice, DateTime dtProdPromStartDate, DateTime dtProdPromEndDate)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRODPRICE (" +
                    "PROD_ID, " +
                    "PRODPRC_COST, " +
                    "PRODPRC_PRICE, " +
                    "PRODPRC_PROMPRICE, " +
                    "PRODPRC_PROMSTARTDATE, " +
                    "PRODPRC_PROMENDDATE " +
                    ") VALUES " +
                   "(?,?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRODPRC_COST", OdbcType.Double).Value = Convert.ToDouble(decProdCost);
            cmd.Parameters.Add("p_PRODPRC_PRICE", OdbcType.Double).Value = Convert.ToDouble(decProdPrice);
            cmd.Parameters.Add("p_PRODPRC_PROMPRICE", OdbcType.Double).Value = Convert.ToDouble(decProdPromPrice);
            cmd.Parameters.Add("p_PRODPRC_PROMSTARTDATE", OdbcType.DateTime).Value = dtProdPromStartDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_PRODPRC_PROMENDDATE", OdbcType.DateTime).Value = dtProdPromEndDate.ToString(clsAdmin.CONSTDATEFORMAT);

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isProdPriceExist(int intProdId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT *" +
                     " FROM TB_PRODPRICE" +
                     " WHERE PROD_ID = ?";                     

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 1).Value = intProdId;            

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean isProdGroupExist(int intProdId, int intCatId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT *" +
                     " FROM TB_PRODGROUP" +
                     " WHERE PROD_ID = ? AND GRP_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intCatId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean isExactSameProdSet3(int intProdId, decimal decProdCost, decimal decProdPrice, decimal decProdPromPrice, DateTime dtProdPromStartDate, DateTime dtProdPromEndDate)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODPRICE" +
                " WHERE PROD_ID = ?" +
                " AND PRODPRC_COST = ?" +
                " AND PRODPRC_PRICE = ?" +
                " AND PRODPRC_PROMPRICE = ?" +
                " AND PRODPRC_PROMSTARTDATE = ?" +
                " AND PRODPRC_PROMENDDATE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRODPRC_COST", OdbcType.Double).Value = decProdCost;
            cmd.Parameters.Add("p_PRODPRC_PRICE", OdbcType.Double).Value = decProdPrice;
            cmd.Parameters.Add("p_PRODPRC_PROMPRICE", OdbcType.Double).Value = decProdPromPrice;
            cmd.Parameters.Add("p_PRODPRC_PROMSTARTDATE", OdbcType.DateTime).Value = dtProdPromStartDate.ToString(clsAdmin.CONSTDATEFORMAT);             
            cmd.Parameters.Add("p_PRODPRC_PROMENDDATE", OdbcType.DateTime).Value = dtProdPromEndDate.ToString(clsAdmin.CONSTDATEFORMAT); 

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateProdPrice(int intProdId, decimal decProdCost, decimal decProdPrice, decimal decProdPromPrice, DateTime dtProdPromStartDate, DateTime dtProdPromEndDate)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODPRICE SET" +
                " PRODPRC_COST = ?," +
                " PRODPRC_PRICE = ?," +
                " PRODPRC_PROMPRICE = ?," +
                " PRODPRC_PROMSTARTDATE = ?," +
                " PRODPRC_PROMENDDATE = ?" +
                " WHERE PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PRODPRC_COST", OdbcType.Double).Value = decProdCost;
            cmd.Parameters.Add("p_PRODPRC_PRICE", OdbcType.Double).Value = decProdPrice;
            cmd.Parameters.Add("p_PRODPRC_PROMPRICE", OdbcType.Double).Value = decProdPromPrice;
            cmd.Parameters.Add("p_PRODPRC_PROMSTARTDATE", OdbcType.DateTime).Value = dtProdPromStartDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_PRODPRC_PROMENDDATE", OdbcType.DateTime).Value = dtProdPromEndDate.ToString(clsAdmin.CONSTDATEFORMAT);
            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
                                    
            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addProdArticle(int intProdId, string strArtTitle, string strArtFile)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRODARTICLE (" +
                    "PROD_ID, " +
                    "PRODART_TITLE, " +
                    "PRODART_FILE " +
                    ") VALUES " +
                   "(?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRODART_TITLE", OdbcType.VarChar, 250).Value = strArtTitle;
            cmd.Parameters.Add("p_PRODART_FILE", OdbcType.VarChar, 250).Value = strArtFile;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteProdArticle(int intProdId, string strArtTitle, string strArtFile)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRODARTICLE" +
                " WHERE PROD_ID = ?" +
                " AND PRODART_TITLE = ?" +
                " AND PRODART_FILE = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 10).Value = intProdId;
            cmd.Parameters.Add("p_PRODART_TITLE", OdbcType.VarChar, 250).Value = strArtTitle;
            cmd.Parameters.Add("p_PRODART_FILE", OdbcType.VarChar, 250).Value = strArtFile;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameProdSet2(int intProdId, string strProdSnapshot, string strProdSnapshotJp, string strProdSnapshotMs, string strProdSnapshotZh, string strProdRemarks, string strProdRemarksJp, string strProdRemarksMs, string strProdRemarksZh, string strProdDesc, string strProdDescJp, string strProdDescMs, string strProdDescZh)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODUCT" +
                " WHERE PROD_ID = ?" +
                " AND BINARY PROD_SNAPSHOT = ?" +
                " AND BINARY PROD_SNAPSHOT_JP = ?" +
                " AND BINARY PROD_SNAPSHOT_MS = ?" +
                " AND BINARY PROD_SNAPSHOT_ZH = ?" +
                " AND BINARY PROD_REMARKS = ?" +
                " AND BINARY PROD_REMARKS_JP = ?" +
                " AND BINARY PROD_REMARKS_MS = ?" +
                " AND BINARY PROD_REMARKS_ZH = ?" +
                " AND BINARY PROD_DESC = ?" +
                " AND BINARY PROD_DESC_JP = ?" +
                " AND BINARY PROD_DESC_MS = ?" +
                " AND BINARY PROD_DESC_ZH = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_PROD_SNAPSHOT", OdbcType.VarChar, 1000).Value = strProdSnapshot;
            cmd.Parameters.Add("p_PROD_SNAPSHOT_JP", OdbcType.VarChar, 1000).Value = strProdSnapshotJp;
            cmd.Parameters.Add("p_PROD_SNAPSHOT_MS", OdbcType.VarChar, 1000).Value = strProdSnapshotMs;
            cmd.Parameters.Add("p_PROD_SNAPSHOT_ZH", OdbcType.VarChar, 1000).Value = strProdSnapshotZh;
            cmd.Parameters.Add("p_PROD_REMARKS", OdbcType.Text).Value = strProdRemarks;
            cmd.Parameters.Add("p_PROD_REMARKS_JP", OdbcType.Text).Value = strProdRemarksJp;
            cmd.Parameters.Add("p_PROD_REMARKS_MS", OdbcType.Text).Value = strProdRemarksMs;
            cmd.Parameters.Add("p_PROD_REMARKS_ZH", OdbcType.Text).Value = strProdRemarksZh;
            cmd.Parameters.Add("p_PROD_DESC", OdbcType.Text).Value = strProdDesc;
            cmd.Parameters.Add("p_PROD_DESC_JP", OdbcType.Text).Value = strProdDescJp;
            cmd.Parameters.Add("p_PROD_DESC_MS", OdbcType.Text).Value = strProdDescMs;
            cmd.Parameters.Add("p_PROD_DESC_ZH", OdbcType.Text).Value = strProdDescZh;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public Boolean isExactSameProdSet5(int intProdId, string strSupplierName, int intInvAllowNegative)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODUCT" +
                    " WHERE PROD_ID = ?" +
                    " AND PROD_SUPPLIER = ?" +
                    " AND PROD_INVALLOWNEGATIVE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_PROD_SUPPLIER", OdbcType.VarChar, 250).Value = strSupplierName;
            cmd.Parameters.Add("p_PROD_INVALLOWNEGATIVE", OdbcType.Int, 1).Value = intInvAllowNegative;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateGrpToProd(int intProdId, int intGrpId, int intOldGrpId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODGROUP SET" +
                    " GRP_ID = ?" +
                    " WHERE PROD_ID = ?" +
                    " AND GRP_ID =?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_GRP_ID", OdbcType.BigInt, 9).Value = intGrpId;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_OLD_GRP_ID", OdbcType.BigInt, 9).Value = intOldGrpId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateProdDescById(int intProdId, string strProdSnapshot, string strProdSnapshotJp, string strProdSnapshotMs, string strProdSnapshotZh, string strProdRemarks, string strProdRemarksJp, string strProdRemarksMs, string strProdRemarksZh, string strProdDesc, string strProdDescJp, string strProdDescMs, string strProdDescZh, int intProdUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODUCT SET" +
                " PROD_DESC = ?," +
                " PROD_DESC_JP = ?," +
                " PROD_DESC_MS = ?," +
                " PROD_DESC_ZH = ?," +
                " PROD_SNAPSHOT = ?," +
                " PROD_SNAPSHOT_JP = ?," +
                " PROD_SNAPSHOT_MS = ?," +
                " PROD_SNAPSHOT_ZH = ?," +
                " PROD_REMARKS = ?," +
                " PROD_REMARKS_JP = ?," +
                " PROD_REMARKS_MS = ?," +
                " PROD_REMARKS_ZH = ?," +
                " PROD_LASTUPDATE = SYSDATE()," +
                " PROD_UPDATEDBY = ?" +
                " WHERE PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_DESC", OdbcType.Text).Value = strProdDesc;
            cmd.Parameters.Add("p_PROD_DESC_JP", OdbcType.Text).Value = strProdDescJp;
            cmd.Parameters.Add("p_PROD_DESC_MS", OdbcType.Text).Value = strProdDescMs;
            cmd.Parameters.Add("p_PROD_DESC_ZH", OdbcType.Text).Value = strProdDescZh;
            cmd.Parameters.Add("p_PROD_SNAPSHOT", OdbcType.VarChar, 1000).Value = strProdSnapshot;
            cmd.Parameters.Add("p_PROD_SNAPSHOT_JP", OdbcType.VarChar, 1000).Value = strProdSnapshotJp;
            cmd.Parameters.Add("p_PROD_SNAPSHOT_MS", OdbcType.VarChar, 1000).Value = strProdSnapshotMs;
            cmd.Parameters.Add("p_PROD_SNAPSHOT_ZH", OdbcType.VarChar, 1000).Value = strProdSnapshotZh;
            cmd.Parameters.Add("P_PROD_REMARKS", OdbcType.Text).Value = strProdRemarks;
            cmd.Parameters.Add("p_PROD_REMARKS_JP", OdbcType.Text).Value = strProdRemarksJp;
            cmd.Parameters.Add("p_PROD_REMARKS_MS", OdbcType.Text).Value = strProdRemarksMs;
            cmd.Parameters.Add("p_PROD_REMARKS_ZH", OdbcType.Text).Value = strProdRemarksZh;
            cmd.Parameters.Add("p_PROD_UPDATEDBY", OdbcType.Int, 9).Value = intProdUpdatedBy;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateProdInventoryById(int intProdId, string strSupplierName, int intInvAllowNegative, int intProdUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODUCT SET" +
                    " PROD_SUPPLIER = ?," +
                    " PROD_INVALLOWNEGATIVE = ?," +
                    " PROD_LASTUPDATE = SYSDATE()," +
                    " PROD_UPDATEDBY = ?" +
                    " WHERE PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_SUPPLIER", OdbcType.VarChar, 250).Value = strSupplierName;
            cmd.Parameters.Add("p_PROD_INVALLOWNEGATIVE", OdbcType.Int, 1).Value = intInvAllowNegative;
            cmd.Parameters.Add("p_PROD_UPDATEDBY", OdbcType.Int, 9).Value = intProdUpdatedBy;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getProdArticleByProdId(int intProdId)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PRODART_TITLE, A.PRODART_FILE" +
                     " FROM TB_PRODARTICLE A" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 1).Value = intProdId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public int addRelatedProd(int intProdId, int intRelProdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_RELATEDPRODLIST (" +
                     "PROD_ID, " +
                     "RELPRODLIST_RELPRODID " +
                     ") VALUES " + 
                     "(?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_RELPROD_ID", OdbcType.Int, 9).Value = intRelProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteRelatedProd(int intProdId, int intRelProdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_RELATEDPRODLIST" +
                     " WHERE PROD_ID = ?" +
                     " AND RELPRODLIST_RELPRODID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_RELPROD_ID", OdbcType.Int, 9).Value = intRelProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteRelatedProds(int intProdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_RELATEDPRODLIST WHERE PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteRelatedProd(int intProdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_RELATEDPRODLIST WHERE RELPRODLIST_RELPRODID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_RELPRODLIST_RELPRODID", OdbcType.Int, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameRelProdSet(int intProdid, int intRelProdId)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_RELATEDPRODLIST" +
                     " WHERE PROD_ID = ?" +
                     " AND RELPRODLIST_RELPRODID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdid;
            cmd.Parameters.Add("p_RELPROD_ID", OdbcType.Int, 9).Value = intRelProdId;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public DataSet getRecProdList()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM VW_PRODUCT A" +
                     " WHERE A.PROD_BEST=1 AND A.PROD_ACTIVE=1" +
                     " ORDER BY PROD_LASTUPDATE ASC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public int getTotalRecord(int intActive)
    {
        int intTotal = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODUCT A" +
                     " WHERE 1 = 1";

            if (intActive != 0)
            {
                strSql += " AND A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            intTotal = int.Parse(cmd.ExecuteScalar().ToString());
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intTotal;
    }

    public Boolean isExactSameSetData(int intProdId, int intColorId, int intSizeId, int intTotal)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_COLORSIZE" +
                     " WHERE PROD_ID = ?" +
                     " AND COLOR_ID = ?" +
                     " AND SIZE_ID = ?" +
                     " AND CS_TOTAL = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_COLOR_ID", OdbcType.BigInt, 9).Value = intColorId;
            cmd.Parameters.Add("p_SIZE_ID", OdbcType.Int, 9).Value = intSizeId;
            cmd.Parameters.Add("p_CS_TOTAL", OdbcType.Decimal).Value = intTotal;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int addAddonProd(int intProdId, int intAddonProdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRODADDON (" +
                     "PROD_ID, " +
                     "PROD_ADDON_ID " +
                     ") VALUES " +
                     "(?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_PROD_ADDON_ID", OdbcType.Int, 9).Value = intAddonProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteAddonProd(int intProdId, int intAddonProdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRODADDON" +
                     " WHERE PROD_ID = ?" +
                     " AND PROD_ADDON_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_PROD_ADDON_ID", OdbcType.Int, 9).Value = intAddonProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameAddonProd(int intProdid, int intAddonProdId)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODADDON" +
                     " WHERE PROD_ID = ?" +
                     " AND PROD_ADDON_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdid;
            cmd.Parameters.Add("p_PROD_ADDON_ID", OdbcType.Int, 9).Value = intAddonProdId;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    #region "Product Hot Sales"
    public int addProdSales(int intProdId, string strSalesCode, DateTime dtSalesStartDate, DateTime dtSalesEndDate, int intSalesQuantity, decimal decSalesPrice, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRODSALES (" +
                     "PROD_ID, " +
                     "PRODSALES_CODE, " +
                     "PRODSALES_STARTDATE, " +
                     "PRODSALES_ENDDATE, " +
                     "PRODSALES_QUANTITY, " +
                     "PRODSALES_PRICE, " +
                     "PRODSALES_CREATION, " +
                     "PRODSALES_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRODSALES_CODE", OdbcType.VarChar, 20).Value = strSalesCode;
            cmd.Parameters.Add("p_PRODSALES_STARTDATE", OdbcType.DateTime).Value = dtSalesStartDate;
            cmd.Parameters.Add("p_PRODSALES_ENDDATE", OdbcType.DateTime).Value = dtSalesEndDate;
            cmd.Parameters.Add("p_PRODSALES_QUANTITY", OdbcType.Int, 9).Value = intSalesQuantity;
            cmd.Parameters.Add("p_PRODSALES_PRICE", OdbcType.Decimal).Value = decSalesPrice;
            cmd.Parameters.Add("p_PRODSALES_CREATEDBY", OdbcType.BigInt, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isProdSalesCodeExist(string strProdSalesCode, int intProdSalesId)
    {
        Boolean boolExist = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODSALES" +
                     " WHERE PRODSALES_CODE = ?";

            cmd.Parameters.Add("p_PRODSALES_CODE", OdbcType.VarChar, 20).Value = strProdSalesCode;

            if (intProdSalesId > 0)
            {
                strSql += " AND PRODSALES_ID != ?";
                cmd.Parameters.Add("p_PRODSALES_ID", OdbcType.BigInt, 9).Value = intProdSalesId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount > 0)
            {
                boolExist = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public Boolean isExactSameProdSalesDataSet(int intSalesId, DateTime dtSalesStartDate, DateTime dtSalesEndDate, int intSalesQuantity, decimal decSalesPrice)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODSALES" + 
                     " WHERE PRODSALES_ID = ?" +
                     " AND PRODSALES_STARTDATE = ?" +
                     " AND PRODSALES_ENDDATE = ?" +
                     " AND PRODSALES_QUANTITY = ?" +
                     " AND PRODSALES_PRICE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PRODSALES_ID", OdbcType.BigInt, 9).Value = intSalesId;
            cmd.Parameters.Add("p_PRODSALES_STARTDATE", OdbcType.DateTime).Value = dtSalesStartDate;
            cmd.Parameters.Add("p_PRODSALES_ENDDATE", OdbcType.DateTime).Value = dtSalesEndDate;
            cmd.Parameters.Add("p_PRODSALES_QUANTITY", OdbcType.Int, 9).Value = intSalesQuantity;
            cmd.Parameters.Add("p_PRODSALES_PRICE", OdbcType.Decimal).Value = decSalesPrice;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateProdSalesById(int intSalesId, DateTime dtSalesStartDate, DateTime dtSalesEndDate, int intSalesQuantity, decimal decSalesPrice, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODSALES SET" +
                     " PRODSALES_STARTDATE = ?," +
                     " PRODSALES_ENDDATE = ?," +
                     " PRODSALES_QUANTITY = ?," +
                     " PRODSALES_PRICE = ?," +
                     " PRODSALES_LASTUPDATE = SYSDATE()," +
                     " PRODSALES_UPDATEDBY = ?" +
                     " WHERE PRODSALES_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PRODSALES_STARTDATE", OdbcType.DateTime).Value = dtSalesStartDate;
            cmd.Parameters.Add("p_PRODSALES_ENDDATE", OdbcType.DateTime).Value = dtSalesEndDate;
            cmd.Parameters.Add("p_PRODSALES_QUANTITY", OdbcType.Int, 9).Value = intSalesQuantity;
            cmd.Parameters.Add("p_PRODSALES_PRICE", OdbcType.Decimal).Value = decSalesPrice;
            cmd.Parameters.Add("p_PRODSALES_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_PRODSALES_ID", OdbcType.BigInt, 9).Value = intSalesId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                salesId = intSalesId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    //public DataSet getProdSalesList()
    //{
    //    DataSet ds = new DataSet();

    //    try
    //    {
    //        string strSql;

    //        OdbcCommand cmd = new OdbcCommand();
    //        OdbcDataAdapter da = new OdbcDataAdapter();

    //        strSql = "SELECT A.PRODSALES_ID, A.PROD_ID, A.PRODSALES_CODE, A.PRODSALES_STARTDATE, A.PRODSALES_ENDDATE," +
    //                 " A.PRODSALES_QUANTITY, A.PRODSALES_PRICE" +
    //                 " FROM TB_PRODSALES A";

    //        cmd.Connection = openConn();
    //        cmd.CommandText = strSql;
    //        cmd.CommandType = CommandType.Text;
    //        da.SelectCommand = cmd;
    //        da.Fill(ds);
    //    }
    //    catch (Exception ex)
    //    {
    //        logErroMsg(ex.Message.ToString());
    //        throw ex;
    //    }
    //    finally
    //    {
    //        closeConn();
    //    }

    //    return ds;
    //}

    public Boolean extractProdSalesById(int intSalesId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PRODSALES_ID, A.PROD_ID, A.PRODSALES_CODE, A.PRODSALES_STARTDATE, A.PRODSALES_ENDDATE," +
                     " A.PRODSALES_QUANTITY, A.PRODSALES_PRICE, A.PRODSALES_CREATION, A.PRODSALES_CREATEDBY," +
                     " A.PRODSALES_LASTUPDATE, A.PRODSALES_UPDATEDBY" +
                     " FROM TB_PRODSALES A" +
                     " WHERE A.PRODSALES_ID = ?";

            cmd.Parameters.Add("p_PRODSALES_ID", OdbcType.BigInt, 9).Value = intSalesId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                salesId = int.Parse(ds.Tables[0].Rows[0]["PRODSALES_ID"].ToString());
                salesCode = ds.Tables[0].Rows[0]["PRODSALES_CODE"].ToString();
                salesStartDate = ds.Tables[0].Rows[0]["PRODSALES_STARTDATE"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["PRODSALES_STARTDATE"].ToString()) : DateTime.MaxValue;
                salesEndDate = ds.Tables[0].Rows[0]["PRODSALES_ENDDATE"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["PRODSALES_ENDDATE"].ToString()) : DateTime.MaxValue;
                salesQuantity = int.Parse(ds.Tables[0].Rows[0]["PRODSALES_QUANTITY"].ToString());
                salesPrice = ds.Tables[0].Rows[0]["PRODSALES_PRICE"] != DBNull.Value ? decimal.Parse(ds.Tables[0].Rows[0]["PRODSALES_PRICE"].ToString()) : Convert.ToDecimal("0.00");
                salesCreation = ds.Tables[0].Rows[0]["PRODSALES_CREATION"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["PRODSALES_CREATION"].ToString()) : DateTime.MaxValue;
                salesCreatedBy = int.Parse(ds.Tables[0].Rows[0]["PRODSALES_CREATEDBY"].ToString());
                salesLastUpdate = ds.Tables[0].Rows[0]["PRODSALES_LASTUPDATE"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["PRODSALES_LASTUPDATE"].ToString()) : DateTime.MaxValue;
                salesUpdatedBy = int.Parse(ds.Tables[0].Rows[0]["PRODSALES_UPDATEDBY"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int deleteProdSales(int intSalesId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRODSALES WHERE PRODSALES_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PRODSALES_ID", OdbcType.BigInt, 9).Value = intSalesId;
            cmd.Parameters["p_PRODSALES_ID"].Value = intSalesId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion

    #region "Add Sales"
    public string getProdNameIdList(int intActive)
    {
        string strNameList = "";

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT GROUP_CONCAT(A.PROD_NAME)" +
                     " FROM TB_PRODUCT A";

            if (intActive != 0)
            {
                strSql += " WHERE A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            strNameList = cmd.ExecuteScalar().ToString();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return strNameList;
    }

    public Boolean extractProdByName(string strName, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_CODE, A.PROD_NAME, A.PROD_SNAPSHOT," +
                     " B.PRODPRC_PRICE, B.PRODPRC_PROMPRICE, B.PRODPRC_PROMSTARTDATE, B.PRODPRC_PROMENDDATE" +
                     " FROM (TB_PRODUCT A LEFT JOIN TB_PRODPRICE B ON (A.PROD_ID = B.PROD_ID))" +
                     " WHERE A.PROD_NAME = ?";

            cmd.Parameters.Add("p_PROD_NAME", OdbcType.VarChar, 250).Value = strName;

            if (intActive != 0)
            {
                strSql += " AND A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                prodId = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ID"].ToString());
                prodCode = dataSet.Tables[0].Rows[0]["PROD_CODE"].ToString();
                prodName = dataSet.Tables[0].Rows[0]["PROD_NAME"].ToString();
                prodSnapshot = dataSet.Tables[0].Rows[0]["PROD_SNAPSHOT"].ToString();
                prodPrice = dataSet.Tables[0].Rows[0]["PRODPRC_PRICE"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[0]["PRODPRC_PRICE"]) : 0;
                prodPromPrice = dataSet.Tables[0].Rows[0]["PRODPRC_PROMPRICE"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[0]["PRODPRC_PROMPRICE"]) : 0;
                prodPromStartDate = dataSet.Tables[0].Rows[0]["PRODPRC_PROMSTARTDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PRODPRC_PROMSTARTDATE"]) : DateTime.MaxValue;
                prodPromEndDate = dataSet.Tables[0].Rows[0]["PRODPRC_PROMENDDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["PRODPRC_PROMENDDATE"]) : DateTime.MaxValue;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractProdById4(int intProdId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_CODE, A.PROD_WEIGHT" +
                     " FROM TB_PRODUCT A" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;

            if (intActive != 0)
            {
                strSql += " AND A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                prodId = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ID"].ToString());
                prodCode = dataSet.Tables[0].Rows[0]["PROD_CODE"].ToString();
                prodWeight = int.Parse(dataSet.Tables[0].Rows[0]["PROD_WEIGHT"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    #endregion

    #region "Pricing Set"
    public int getPricingOrder(int intProdId)
    {
        int intOrder = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT PRICING_ORDER FROM TB_PRICING WHERE PROD_ID = ? ORDER BY PRICING_ORDER DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            intOrder = Convert.ToInt16(cmd.ExecuteScalar()) + 1;

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intOrder;
    }

    public Boolean isPricingExist(int intProdId, string strName1, string strName2, decimal decPrice, int intEquivalent)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*)" +
                     " FROM TB_PRICING" +
                     " WHERE PROD_ID = ?" +
                     " AND PRICING_NAME1 = ?" +
                     " AND PRICING_NAME2 = ?" +
                     " AND PRICING_PRICE = ?" +
                     " AND PRICING_EQUIVALENT = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRICING_NAME1", OdbcType.VarChar, 250).Value = strName1;
            cmd.Parameters.Add("p_PRICING_NAME2", OdbcType.VarChar, 250).Value = strName2;
            cmd.Parameters.Add("p_PRICING_PRICE", OdbcType.Decimal).Value = decPrice;
            cmd.Parameters.Add("p_PRICING_EQUIVALENT", OdbcType.Int,9).Value = intEquivalent;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = int.Parse(cmd.ExecuteScalar().ToString());

            boolFound = intCount > 0 ? true : false;
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int addPricing(int intProdId, string strName1, string strName2, decimal decPrice,int intEquivalent, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            int intOrder = getPricingOrder(intProdId);

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRICING (" +
                     "PROD_ID, " +
                     "PRICING_NAME1, " +
                     "PRICING_NAME2, " +
                     "PRICING_PRICE, " +
                     "PRICING_EQUIVALENT, " +
                     "PRICING_ORDER, " +
                     "PRICING_CREATION, " +
                     "PRICING_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRICING_NAME1", OdbcType.VarChar, 250).Value = strName1;
            cmd.Parameters.Add("p_PRICING_NAME2", OdbcType.VarChar, 250).Value = strName2;
            cmd.Parameters.Add("p_PRICING_PRICE", OdbcType.Decimal).Value = decPrice;
            cmd.Parameters.Add("p_PRICING_EQUIVALENT", OdbcType.Int,9).Value = intEquivalent;
            cmd.Parameters.Add("p_PRICING_ORDER", OdbcType.Int, 9).Value = intOrder;
            cmd.Parameters.Add("p_PRICING_CREATEDBY", OdbcType.BigInt, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getPricingListById(int intProdId, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PRICING_ID, A.PROD_ID, A.PRICING_NAME1, A.PRICING_NAME2, A.PRICING_PRICE, A.PRICING_EQUIVALENT, A.PRICING_ORDER, A.PRICING_ACTIVE," +
                     " A.PRICING_CREATION, A.PRICING_CREATEDBY, A.PRICING_LASTUPDATE, A.PRICING_UPDATEDBY" +
                     " FROM TB_PRICING A" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;

            if (intActive != 0)
            {
                strSql += " AND A.PRICING_ACTIVE = ?";
                cmd.Parameters.Add("p_PRICING_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public int updatePricingById(int intPricingId, string strName1, string strName2, decimal decPrice, int intEquivalent, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRICING SET" +
                     " pricing_name1 = ?," +
                     " pricing_name2 = ?," +
                     " pricing_price = ?," +
                     " pricing_equivalent = ?," +
                     " pricing_updatedby = ?," +
                     " PRICING_LASTUPDATE = NOW()" +
                     " WHERE PRICING_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PRICING_NAME1", OdbcType.VarChar, 250).Value = strName1;
            cmd.Parameters.Add("p_PRICING_NAME2", OdbcType.VarChar, 250).Value = strName2;
            cmd.Parameters.Add("p_PRICING_PRICE", OdbcType.Decimal).Value = decPrice;
            cmd.Parameters.Add("p_PRICING_EQUIVALENT", OdbcType.Int, 9).Value = intEquivalent;
            cmd.Parameters.Add("p_PRICING_UPDATEDBY", OdbcType.Int, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_PRICING_ID", OdbcType.BigInt, 9).Value = intPricingId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int updatePricingOrderById(int intId, int intOrder)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRICING SET" +
                     " PRICING_ORDER = ?" +
                     " WHERE PRICING_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PRICING_ORDER", OdbcType.BigInt, 9).Value = intOrder;
            cmd.Parameters.Add("p_PRICING_ID", OdbcType.BigInt, 9).Value = intId;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updatePricingActiveById(int intId, int intActive)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRICING SET" +
                     " PRICING_ACTIVE = ?" +
                     " WHERE PRICING_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PRICING_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_PRICING_ID", OdbcType.BigInt, 9).Value = intId;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deletePricingById(int intPricingId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRICING" +
                     " WHERE PRICING_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PRICING_ID", OdbcType.Int, 9).Value = intPricingId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updatePricingOrder(int intProdId, int intOldOrder)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRICING SET" +
                     " PRICING_ORDER = PRICING_ORDER - 1" +
                     " WHERE PROD_ID = ?" +
                     " AND PRICING_ORDER > ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRICING_ORDER", OdbcType.BigInt, 9).Value = intOldOrder;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deletePricing(int intProdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRICING WHERE PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    #endregion

    #region "Product Description List"
    public int addProdDescItem(int intProdId, string strProdDescTitle, string strProdDescContent)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT TB_PRODDESCRIPTION (" +
                     "PROD_ID, " +
                     "PRODDESC_TITLE, " +
                     "PRODDESC_DESC" +
                     ") VALUES " +
                     "(?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRODDESC_TITLE", OdbcType.Text).Value = strProdDescTitle;
            cmd.Parameters.Add("p_PRODDESC_DESC", OdbcType.Text).Value = strProdDescContent;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            //closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactProdDescSameData(int intProdDescId, int intProdId, string strProdDescTitle, string strProdDescContent)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODDESCRIPTION" +
                     " WHERE PRODDESC_ID = ?" +
                     " AND PROD_ID = ?" +
                     " AND BINARY PRODDESC_TITLE = ?" +
                     " AND BINARY PRODDESC_DESC = ?";

            cmd.Parameters.Add("p_PRODDESC_ID", OdbcType.BigInt, 9).Value = intProdDescId;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRODDESC_TITLE", OdbcType.Text).Value = strProdDescTitle;
            cmd.Parameters.Add("p_PRODDESC_DESC", OdbcType.Text).Value = strProdDescContent;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            //closeConn();
        }

        return boolSame;
    }

    public int updateProdDescItemById(int intProdDescId, int intProdId, string strProdDescTitle, string strProdDescContent)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODDESCRIPTION SET" +
                     " PROD_ID = ?," +
                     " PRODDESC_TITLE = ?," +
                     " PRODDESC_DESC = ?" +
                     " WHERE PRODDESC_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            
            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRODDESC_TITLE", OdbcType.Text).Value = strProdDescTitle;
            cmd.Parameters.Add("p_PRODDESC_DESC", OdbcType.Text).Value = strProdDescContent;
            cmd.Parameters.Add("p_PRODDESC_ID", OdbcType.BigInt, 9).Value = intProdDescId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            //closeConn();
        }

        return intRecordAffected;
    }

    public int deleteProdDescItemById(int intProdDescId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRODDESCRIPTION WHERE PRODDESC_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PRODDESC_ID", OdbcType.BigInt, 9).Value = intProdDescId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            //closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getProdDescItemListByProdId(int intProdId)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PRODDESC_ID, A.PRODDESC_TITLE, A.PRODDESC_DESC" +
                     " FROM TB_PRODDESCRIPTION A" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            //closeConn();
        }

        return ds;
    }
    #endregion

    #region "Supplier"
    //public int addSupplier(string strCompany, string strAdd, string strContactPerson, string strTel, string strFax, string strEmail, string strShopNo)
    //{
    //    int intRecordAffected = 0;

    //    try
    //    {
    //        OdbcTransaction trans;
    //        OdbcCommand cmd = new OdbcCommand();
    //        string strSql;

    //        cmd.Connection = openConn();
    //        trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

    //        strSql = "INSERT INTO TB_SUPPLIER (" +
    //                 "SUPPLIER_COMPANYNAME, " +
    //                 "SUPPLIER_ADD, " +
    //                 "SUPPLIER_CONTACTPERSON, " +
    //                 "SUPPLIER_TEL, " +
    //                 "SUPPLIER_FAX, " +
    //                 "SUPPLIER_EMAIL, " +
    //                 "SUPPLIER_SHOPNO " +
    //                 ") VALUES ";
    //    }
    //    catch (Exception ex)
    //    {
    //        logErroMsg(ex.Message.ToString());
    //        throw ex;
    //    }
    //    finally
    //    {
    //        closeConn();
    //    }

    //    return intRecordAffected;
    //}
    #endregion

    #region "Gallery"
    public DataSet getGallery()
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT PROD_ID, PRODGALL_TITLE, PRODGALL_IMAGE, PRODGALL_DEFAULT, PRODGALL_ORDER" +
                     " FROM TB_PRODGALLERY";

            //HttpContext.Current.Response.Write(DateTime.Now + " addGallery<br />");
            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            //HttpContext.Current.Response.Write(DateTime.Now + " addGallery<br />");
            da.SelectCommand = cmd;
            da.Fill(ds);

        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getGalleryById(int intId)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT PROD_ID,PRODGALL_ID, PRODGALL_TITLE, PRODGALL_IMAGE, PRODGALL_DESC, PRODGALL_DEFAULT, PRODGALL_ORDER" +
                     " FROM TB_PRODGALLERY" +
                     " WHERE PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intId;

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean isExactSameGallery(int prodGallID, ProdGalleryDetail item, int intDefault, int intOrder)
    {
        Boolean boolSame = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            List<string> columns = new List<string>();

            cmd.Parameters.Add("p_PRODGALL_ID", OdbcType.Int, 9).Value = prodGallID;
            if (item.filename != null) { columns.Add("PRODGALL_IMAGE = ?"); cmd.Parameters.AddWithValue("p_PRODGALL_IMAGE", item.filename); }
            if (item.desc != null) { columns.Add("PRODGALL_DESC = ?"); cmd.Parameters.AddWithValue("p_PRODGALL_DESC", item.desc); }
            if (item.title != null) { columns.Add("PRODGALL_TITLE = ?"); cmd.Parameters.AddWithValue("p_PRODGALL_TITLE", item.title); }

            if (intOrder != -1) { columns.Add("PRODGALL_ORDER = ?"); cmd.Parameters.AddWithValue("p_PRODGALL_ORDER", intOrder); }
            if (intDefault != -1) { columns.Add("PRODGALL_DEFAULT = ?"); cmd.Parameters.AddWithValue("p_PRODGALL_DEFAULT", intDefault); }


            strSql = "SELECT COUNT(*) FROM TB_PRODGALLERY" +
                     " WHERE PRODGALL_ID = ? AND " + string.Join(" AND ",columns.ToArray());

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
           

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1)
            {
                boolSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int addGallery(ProdGalleryDetail item, int intDefault, int intOrder, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRODGALLERY (" +
                     "PROD_ID, " +
                     "PRODGALL_TITLE, " +
                     "PRODGALL_DESC, " +
                     "PRODGALL_IMAGE," +
                     "PRODGALL_DEFAULT," +
                     "PRODGALL_ORDER," +
                     "PRODGALL_CREATEDBY," +
                     "PRODGALL_CREATION" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,NOW())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = item.prodId;
            cmd.Parameters.Add("p_PRODGALL_TITLE", OdbcType.VarChar, 250).Value = item.title;
            cmd.Parameters.Add("p_PRODGALL_DESC", OdbcType.VarChar, 250).Value = (!string.IsNullOrEmpty(item.desc) ? item.desc : "");
            cmd.Parameters.Add("p_PRODGALL_IMAGE", OdbcType.VarChar, 250).Value = item.filename;
            cmd.Parameters.Add("p_PRODGALL_DEFAULT", OdbcType.Int, 1).Value = intDefault;
            cmd.Parameters.Add("p_PRODGALL_ORDER", OdbcType.Int, 9).Value = intOrder;
            cmd.Parameters.Add("p_PRODGALL_CREATEDBY", OdbcType.Int, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                prodGallId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateGalleryImage(int prodGallID, string image, int updatedBy)
    {
        ProdGalleryDetail item = new ProdGalleryDetail();
        item.filename = image;
        return updateGallery(prodGallID, item, -1, -1, updatedBy);
    }
    public int updateGallery(int prodGallID, ProdGalleryDetail item, int intDefault, int intOrder, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<string> columns = new List<string>();

            if (item.filename != null) { columns.Add("PRODGALL_IMAGE = ?"); cmd.Parameters.AddWithValue("p_PRODGALL_IMAGE", item.filename); }
            if (item.desc != null) { columns.Add("PRODGALL_DESC = ?"); cmd.Parameters.AddWithValue("p_PRODGALL_DESC", item.desc); }
            if (item.title != null) { columns.Add("PRODGALL_TITLE = ?"); cmd.Parameters.AddWithValue("p_PRODGALL_TITLE", item.title); }

            if (intOrder != -1) { columns.Add("PRODGALL_ORDER = ?"); cmd.Parameters.AddWithValue("p_PRODGALL_ORDER", intOrder); }
            if (intDefault != -1) { columns.Add("PRODGALL_DEFAULT = ?"); cmd.Parameters.AddWithValue("p_PRODGALL_DEFAULT", intDefault); }


            cmd.Parameters.Add("p_PRODGALL_UPDATEDBY", OdbcType.Int, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_PRODGALL_ID", OdbcType.Int, 9).Value = prodGallID;


            strSql = "UPDATE TB_PRODGALLERY SET " + string.Join(",", columns.ToArray()) + "," +
                    " PRODGALL_UPDATEDBY = ?," +
                    " PRODGALL_LASTUPDATE = NOW()" +
                    " WHERE PRODGALL_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteGalleryImage(int prodGallID)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRODGALLERY" +
                     " WHERE PRODGALL_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PRODGALL_ID", OdbcType.Int, 9).Value = prodGallID;
    
            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteGallery(int intProdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRODGALLERY WHERE PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.VarChar, 10);
            cmd.Parameters["p_PROD_ID"].Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion

    #region "Customized"
    #region "Specification"
    public int generateSpecOrderNo(int intProdId, int intSpecGroup)
    {
        int specOrderNo = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT SPEC_ORDER FROM TB_PRODSPEC WHERE PROD_ID = ? AND SPEC_GROUPID = ? ORDER BY SPEC_ORDER DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.Int, 9).Value = intSpecGroup;

            specOrderNo = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return specOrderNo + 1;
    }

    public int addSpec(int intProdId, string strSpecType, int intSpecGroup, int intActive)
    {
        int intRecordAffected = 0;

        try
        {
            int intSpecOrder = generateSpecOrderNo(intProdId, intSpecGroup);

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRODSPEC (" +
                     "PROD_ID, " +
                     "SPEC_TYPE, " +
                     "SPEC_GROUPID, " +
                     "SPEC_ORDER, " +
                     "SPEC_ACTIVE " +
                     ") VALUES " +
                     "(?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_TYPE", OdbcType.VarChar, 250).Value = strSpecType;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.Int, 9).Value = intSpecGroup;
            cmd.Parameters.Add("p_SPEC_ORDER", OdbcType.Int, 9).Value = intSpecOrder;
            cmd.Parameters.Add("p_SPEC_ACTIVE", OdbcType.Int, 1).Value = intActive;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
                prodId = intProdId;


            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameSetSpec(int intSpecId, int intProdId, string strSpecType, int intSpecGroup, int intActive)
    {
        Boolean boolExist = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODSPEC" +
                     " WHERE SPEC_ID = ?" +
                     " AND PROD_ID = ?" +
                     " AND SPEC_TYPE = ?" +
                     " AND SPEC_GROUPID = ?" +
                     " AND SPEC_ACTIVE = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SPEC_ID", OdbcType.BigInt, 9).Value = intSpecId;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_TYPE", OdbcType.VarChar, 250).Value = strSpecType;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.Int, 9).Value = intSpecGroup;
            cmd.Parameters.Add("p_SPEC_ACTIVE", OdbcType.Int, 1).Value = intActive;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolExist = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public int updateSpecById(int intSpecId, int intProdId, int intSpecGroup, string strSpecType, int intActive)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODSPEC SET" +
                     " SPEC_TYPE = ?," +
                     " SPEC_ACTIVE = ?" +
                     " WHERE SPEC_ID = ?" +
                     " AND PROD_ID = ?" +
                     " AND SPEC_GROUPID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SPEC_TYPE", OdbcType.VarChar, 250).Value = strSpecType;
            cmd.Parameters.Add("p_SPEC_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_SPEC_ID", OdbcType.BigInt, 9).Value = intSpecId;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.Int, 9).Value = intSpecGroup;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getSpecList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SPEC_ID, A.PROD_ID, A.SPEC_TYPE, A.SPEC_TYPEOPTION, A.SPEC_GROUPID, A.SPEC_GROUP, A.SPEC_VALUE, A.SPEC_OPERATOR, A.SPEC_ORDER, A.SPEC_UNIT, A.SPEC_PERPAGE, A.SPEC_ACTIVE, A.SPEC_FILETITLE, A.SPEC_FILE," +
                //" A.SPEC_VALUE2, A.SPEC_UNIT2, A.SPEC_MESSAGE" +
                     " A.SPEC_VALUE2 " +
                     " FROM TB_PRODSPEC A";

            if (intActive != 0)
            {
                strSql += " WHERE A.SPEC_ACTIVE = ?";
                cmd.Parameters.Add("p_SPEC_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);

            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    
    public DataSet getSpecList2(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SPEC_ID, A.PROD_ID, A.SPEC_TYPE, A.SPEC_GROUPID, A.SPEC_ORDER" +
                     " FROM TB_PRODSPEC A";

            if (intActive != 0)
            {
                strSql += " WHERE A.SPEC_ACTIVE = ?";
                cmd.Parameters.Add("p_SPEC_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);

            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean isSpecExist(int intProdId, string strSpecName, int intGroupId)
    {
        Boolean boolExist = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODSPECITEM" +
                     " WHERE PROD_ID = ?" +
                     " AND SPEC_NAME = ?" +
                     " AND SPEC_GROUPID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_NAME", OdbcType.VarChar, 250).Value = strSpecName;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.Int, 9).Value = intGroupId;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolExist = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public int addProdSpecItem(int intSpecId, int intProdId, int intGroupId, string strName, decimal decPrice, string strAttachment, int intActive)
    {
        int intRecordAffected = 0;

        try
        {
            //int intSpecOrder = generateSpecOrderNo(intProdId, intSpecGroup);

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRODSPECITEM (" +
                     "SPEC_ID, " +
                     "PROD_ID, " +
                     "SPEC_GROUPID, " +
                     "SI_NAME, " +
                     "SI_PRICE, " +
                     "SI_ATTACHMENT, " +
                     "SI_ACTIVE" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SPEC_ID", OdbcType.BigInt, 9).Value = intSpecId;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.Int, 9).Value = intGroupId;
            cmd.Parameters.Add("p_SI_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_SI_PRICE", OdbcType.Decimal).Value = decPrice;
            cmd.Parameters.Add("p_SI_ATTACHMENT", OdbcType.VarChar, 250).Value = strAttachment;
            cmd.Parameters.Add("p_SI_ACTIVE", OdbcType.Int, 1).Value = intActive;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameSpec(int intSpecId, int intProdId, string strSpecType, int intSpecGroup, int intActive)
    {
        Boolean boolExist = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODSPEC" +
                     " WHERE SPEC_ID = ?" +
                     " AND PROD_ID = ?" +
                     " AND SPEC_TYPE = ?" +
                     " AND SPEC_GROUPID = ?" +
                     " AND SPEC_ACTIVE = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SPEC_ID", OdbcType.BigInt, 9).Value = intSpecId;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_TYPE", OdbcType.VarChar, 250).Value = strSpecType;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.Int, 9).Value = intSpecGroup;
            cmd.Parameters.Add("p_SPEC_ACTIVE", OdbcType.Int, 1).Value = intActive;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolExist = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public Boolean extractSpecDetails(int intSpecId, int intProdId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SPEC_ID, A.PROD_ID, A.SPEC_TYPE, A.SPEC_GROUPID" +
                     " FROM TB_PRODSPEC A" +
                     " WHERE A.SPEC_ID = ?" +
                     " AND A.PROD_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SPEC_ID", OdbcType.BigInt, 9).Value = intSpecId;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                prodSpecId = int.Parse(ds.Tables[0].Rows[0]["SPEC_ID"].ToString());
                prodSpecType = ds.Tables[0].Rows[0]["SPEC_TYPE"].ToString();
                specGroupId = int.Parse(ds.Tables[0].Rows[0]["SPEC_GROUPID"].ToString());
            }

            ds.Dispose();
            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int deleteSpecByProdId(int intProdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRODSPEC WHERE PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    #region "Specification Item"
    public int generateSpecItemOrderNo(int intProdId, int intSpecGroup)
    {
        int specItemOrderNo = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT SI_ORDER FROM TB_PRODSPECITEM WHERE PROD_ID = ? AND SPEC_GROUPID = ? ORDER BY SI_ORDER DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.Int, 9).Value = intSpecGroup;

            specItemOrderNo = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return specItemOrderNo + 1;
    }

    public int addSpecItem(int intSpecId, int intProdId, int intSpecGroup, string strSpecItemName, string strOperator, Decimal decPrice, string strAttachment,string strThumbnail, int intActive)
    {
        int intRecordAffected = 0;

        try
        {
            int intSpecItemOrder = generateSpecItemOrderNo(intProdId, intSpecGroup);

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRODSPECITEM (" +
                     "SPEC_ID, " +
                     "PROD_ID, " +
                     "SPEC_GROUPID, " +
                     "SI_NAME, " +
                     "SI_OPERATOR, " +
                     "SI_PRICE, " +
                     "SI_ATTACHMENT, " +
                     "SI_THUMBNAIL, " +
                     "SI_ORDER, " +
                     "SI_ACTIVE " +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SPEC_ID", OdbcType.BigInt, 9).Value = intSpecId;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.Int, 9).Value = intSpecGroup;
            cmd.Parameters.Add("p_SI_NAME", OdbcType.VarChar, 250).Value = strSpecItemName;
            cmd.Parameters.Add("p_SI_OPERATOR", OdbcType.VarChar, 45).Value = strOperator;
            cmd.Parameters.Add("p_SI_PRICE", OdbcType.Decimal).Value = decPrice;
            cmd.Parameters.Add("p_SI_ATTACHMENT", OdbcType.VarChar, 250).Value = strAttachment;
            cmd.Parameters.Add("p_SI_THUMBNAIL", OdbcType.VarChar, 250).Value = strThumbnail;
            //cmd.Parameters.Add("p_SI_DATETITLE", OdbcType.VarChar, 250).Value = strDateTitle;
            //cmd.Parameters.Add("p_SI_DATEACTIVE", OdbcType.Int, 1).Value = intDateActive;
            cmd.Parameters.Add("p_SI_ORDER", OdbcType.BigInt, 9).Value = intSpecItemOrder;
            cmd.Parameters.Add("p_SI_ACTIVE", OdbcType.Int, 1).Value = intActive;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateSpecItem(int intSpecItemId, int intSpecId, int intProdId, int intGroupId, string strName, string strOperator, decimal decPrice, string strAttachment,string strThumbnail, int intActive)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODSPECITEM SET" +
                     " SI_NAME = ?," +
                     " SI_OPERATOR = ?," +
                     " SI_PRICE = ?," +
                     " SI_ATTACHMENT = ?," +
                     " SI_THUMBNAIL = ?," +
                     " SI_ACTIVE = ?" +
                     " WHERE SI_ID = ?" +
                     " AND SPEC_ID = ?" +
                     " AND PROD_ID = ?" +
                     " AND SPEC_GROUPID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SI_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_SI_OPERATOR", OdbcType.VarChar, 45).Value = strOperator;
            cmd.Parameters.Add("p_SI_PRICE", OdbcType.Decimal).Value = decPrice;
            cmd.Parameters.Add("p_SI_ATTACHMENT", OdbcType.VarChar, 250).Value = strAttachment;
            cmd.Parameters.Add("p_SI_THUMBNAIL", OdbcType.VarChar, 250).Value = strThumbnail;
            //cmd.Parameters.Add("p_SI_DATETITLE", OdbcType.VarChar, 250).Value = strDateTitle;
            //cmd.Parameters.Add("p_SI_DATEACTIVE", OdbcType.Int, 1).Value = intDateActive;
            cmd.Parameters.Add("p_SI_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_SI_ID", OdbcType.BigInt, 9).Value = intSpecItemId;
            cmd.Parameters.Add("p_SPEC_ID", OdbcType.BigInt, 9).Value = intSpecId;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.BigInt, 9).Value = intGroupId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getSpecItemList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SI_ID, A.SPEC_ID, A.PROD_ID, A.SPEC_GROUPID, A.SI_NAME, A.SI_OPERATOR, A.SI_PRICE, A.SI_ATTACHMENT,A.SI_THUMBNAIL, A.SI_DATETITLE, A.SI_DATEACTIVE, A.SI_ORDER, A.SI_ACTIVE" +
                     " FROM TB_PRODSPECITEM A";

            if (intActive != 0)
            {
                strSql += " WHERE A.SI_ACTIVE = ?";
                cmd.Parameters.Add("p_SI_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);

            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public int getSpecItemOrderMin(int intProdId, int intSpecGroup)
    {
        int intSpecOrderMin = 0;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT MIN(SI_ORDER) AS SI_ORDERMIN FROM TB_PRODSPECITEM WHERE PROD_ID = ? AND SPEC_GROUPID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.Int, 9).Value = intSpecGroup;

            intSpecOrderMin = Convert.ToInt16(cmd.ExecuteScalar());

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intSpecOrderMin;
    }

    public int getSpecItemOrderMax(int intProdId, int intSpecGroup)
    {
        int intSpecOrderMax = 0;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT MAX(SI_ORDER) AS SI_ORDERMAX FROM TB_PRODSPECITEM WHERE PROD_ID = ? AND SPEC_GROUPID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.Int, 9).Value = intSpecGroup;

            intSpecOrderMax = Convert.ToInt16(cmd.ExecuteScalar());

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intSpecOrderMax;
    }

    public int getPrevSpecItemId(int intSpecItemOrder, int intProdId, int intSpecGroup, int intActive)
    {
        int intPrevSpecId = 0;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT SI_ID FROM TB_PRODSPECITEM" +
                     " WHERE SI_ORDER < ?" +
                     " AND PROD_ID = ?" +
                     " AND SPEC_GROUPID = ?";

            cmd.Parameters.Add("p_SI_ORDER", OdbcType.Int, 9).Value = intSpecItemOrder;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.Int, 9).Value = intSpecGroup;

            if (intActive != 0)
            {
                strSql += " AND SI_ACTIVE = ?";

                cmd.Parameters.Add("p_SI_ACTIVE", OdbcType.Int, 9).Value = intActive;
            }

            strSql += " ORDER BY SI_ORDER DESC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intPrevSpecId = Convert.ToInt16(cmd.ExecuteScalar());

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intPrevSpecId;
    }

    public int getNextSpecItemId(int intSpecItemOrder, int intProdId, int intSpecGroup, int intActive)
    {
        int intNextSpecId = 0;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT SI_ID FROM TB_PRODSPECITEM" +
                     " WHERE SI_ORDER > ?" +
                     " AND PROD_ID = ?" +
                     " AND SPEC_GROUPID = ?";

            cmd.Parameters.Add("p_SI_ORDER", OdbcType.Int, 9).Value = intSpecItemOrder;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.Int, 9).Value = intSpecGroup;

            if (intActive != 0)
            {
                strSql += " AND SPEC_ACTIVE = ?";

                cmd.Parameters.Add("p_SPEC_ACTIVE", OdbcType.Int, 9).Value = intActive;
            }

            strSql += " ORDER BY SI_ORDER ASC LIMIT 1";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intNextSpecId = Convert.ToInt16(cmd.ExecuteScalar());

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intNextSpecId;
    }

    public Boolean extractSpecItemDetailsById(int intSpecItemId, int intProdId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SI_ID, A.SPEC_ID, A.PROD_ID, A.SPEC_GROUPID, A.SI_NAME, A.SI_OPERATOR, A.SI_PRICE, A.SI_ATTACHMENT, A.SI_THUMBNAIL, A.SI_DATETITLE, A.SI_DATEACTIVE, A.SI_ORDER, A.SI_ACTIVE " +
                     "FROM TB_PRODSPECITEM A" +
                     " WHERE A.SI_ID = ?" +
                     " AND A.PROD_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SI_ID", OdbcType.BigInt, 9).Value = intSpecItemId;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                prodSpecId = Convert.ToInt16(ds.Tables[0].Rows[0]["SPEC_ID"]);
                prodId = Convert.ToInt16(ds.Tables[0].Rows[0]["PROD_ID"]);
                specGroupId = Convert.ToInt16(ds.Tables[0].Rows[0]["SPEC_GROUPID"]);
                siName = ds.Tables[0].Rows[0]["SI_NAME"].ToString();
                siOperator = ds.Tables[0].Rows[0]["SI_OPERATOR"].ToString();
                siPrice = Convert.ToDecimal(ds.Tables[0].Rows[0]["SI_PRICE"]);
                siAttachment = ds.Tables[0].Rows[0]["SI_ATTACHMENT"].ToString();
                siThumbnail = ds.Tables[0].Rows[0]["SI_THUMBNAIL"].ToString();
                siDateTitle = ds.Tables[0].Rows[0]["SI_DATETITLE"].ToString();
                siDateActive = Convert.ToInt16(ds.Tables[0].Rows[0]["SI_DATEACTIVE"]);
                siOrder = Convert.ToInt16(ds.Tables[0].Rows[0]["SI_ORDER"]);
                siActive = Convert.ToInt16(ds.Tables[0].Rows[0]["SI_ACTIVE"]);
            }

            ds.Dispose();
            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int updateSpecItemOrderById(int intSpecItemId, int intProdId, int intSpecItemOrder)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODSPECITEM SET" +
                     " SI_ORDER = ?" +
                     " WHERE SI_ID = ?" +
                     " AND PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SI_ORDER", OdbcType.Int, 9).Value = intSpecItemOrder;
            cmd.Parameters.Add("p_SI_ID", OdbcType.BigInt, 9).Value = intSpecItemId;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateSpecItemActiveById(int intSpecItemId, int intProdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODSPECITEM SET" +
                     " SI_ATTACHMENT = ?" +
                     " WHERE PROD_ID = ?" +
                     " AND SI_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SI_ATTACHMENT", OdbcType.VarChar, 250).Value = "";
            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_SI_ID", OdbcType.Int, 9).Value = intSpecItemId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteSpecItemById(int intSpecItemId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRODSPECITEM WHERE SI_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SI_ID", OdbcType.BigInt, 9).Value = intSpecItemId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int deleteSpecItemByProdId(int intProdId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_PRODSPECITEM WHERE PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected >= 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isSpecItemExist(int intProdId, int intSpecItemId)
    {
        Boolean boolExist = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODSPECITEM" +
                     " WHERE PROD_ID = ?" +
                     " AND SI_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_SI_ID", OdbcType.BigInt, 9).Value = intSpecItemId;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolExist = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public Boolean isSpecItemExist(int intSpecItemId)
    {
        Boolean boolExist = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODSPECITEM" +
                     " WHERE SI_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SI_ID", OdbcType.BigInt, 9).Value = intSpecItemId;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolExist = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public Boolean isExactSameSetSpecItem(int intSpecItemId, int intSpecId, int intProdId, int intGroupId, string strName, string strOperator, decimal decPrice, string strAttachment, string strThumbnail)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODSPECITEM" +
                     " WHERE SI_ID = ?" +
                     " AND SPEC_ID = ?" +
                     " AND PROD_ID = ?" +
                     " AND SPEC_GROUPID = ?" +
                     " AND SI_NAME = ?" +
                     " AND SI_OPERATOR = ?" +
                     " AND SI_PRICE = ?" +
                     " AND SI_ATTACHMENT = ?" +
                     " AND SI_THUMBNAIL = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_SI_ID", OdbcType.BigInt, 9).Value = intSpecItemId;
            cmd.Parameters.Add("p_SPEC_ID", OdbcType.BigInt, 9).Value = intSpecId;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.BigInt, 9).Value = intGroupId;
            cmd.Parameters.Add("p_SI_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_SI_OPERATOR", OdbcType.VarChar, 45).Value = strOperator;
            cmd.Parameters.Add("p_SI_PRICE", OdbcType.Decimal).Value = decPrice;
            cmd.Parameters.Add("p_SI_ATTACHMENT", OdbcType.VarChar, 250).Value = strAttachment;
            cmd.Parameters.Add("p_SI_THUMBNAIL", OdbcType.VarChar, 250).Value = strThumbnail;
            //cmd.Parameters.Add("p_SI_DATETITLE", OdbcType.VarChar, 250).Value = strDateTitle;
            //cmd.Parameters.Add("p_SI_DATEACTIVE", OdbcType.BigInt, 1).Value = intDateActive;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    #endregion
    #endregion

    #region "manager user view"
    public bool getGrpingIdGrpIdByProdId(int intProdId)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT B.GRPING_ID,A.GRP_ID " +
                     " FROM TB_PRODGROUP A " +
                     " JOIN TB_GROUP B ON A.GRP_ID = B.GRP_ID " +
                     " WHERE PROD_ID = ? " +
                     " LIMIT 1;";

           cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;


            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                grpingId = int.Parse(dataSet.Tables[0].Rows[0]["GRPING_ID"].ToString());
                grpId = int.Parse(dataSet.Tables[0].Rows[0]["GRP_ID"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    #endregion
    
    #endregion


    #region "Spec"
    public DataSet getSpecListByProdId(int intProdId, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SPEC_ID, A.SPEC_TYPE, A.SPEC_GROUPID" +
                     " FROM TB_PRODSPEC A" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;

            if (intActive != 0)
            {
                strSql += " AND A.SPEC_ACTIVE = ?";
                cmd.Parameters.Add("p_SPEC_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);

            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public DataSet getSpecListByProdId3(int intProdId, int intGroupId, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SPEC_ID, A.SPEC_TYPE, CONCAT(CAST(A.SPEC_ID AS CHAR), '" + clsSetting.CONSTDEFAULTSEPERATOR.ToString() + "', A.SPEC_TYPE) AS SPEC_IDTYPE, A.SPEC_ORDER" +
                     " FROM TB_PRODSPEC A" +
                     " WHERE A.PROD_ID = ?" +
                     " AND A.SPEC_GROUPID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_SPEC_GROUPID", OdbcType.Int, 9).Value = intGroupId;

            if (intActive != 0)
            {
                strSql += " AND A.SPEC_ACTIVE = ?";
                cmd.Parameters.Add("p_SPEC_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);

            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    // Color Size
    public DataSet getColorSizeByProdId(int intProdId)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CS_ID, A.COLOR_ID, A.SIZE_ID, A.CS_TOTAL" +
                     " FROM TB_COLORSIZE A" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);

            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getColorSize()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CS_ID, A.COLOR_ID, A.SIZE_ID, A.CS_TOTAL" +
                     " FROM TB_COLORSIZE A";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);

            da.Dispose();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    #endregion

    #region "Promo Price"
    public void resetPromoValue()
    {
        promoID = -1;
        promoProdID = -1;
        promoActive = -1;
        promoOrder = -1;
        promoCreatedBy = -1;
        promoUpdatedBy = -1;
        promoPrice = -1;
        promoName = null;
        promoFrom = DateTime.MinValue;
        promoTo = DateTime.MinValue;
        promoCreatedAt = DateTime.MinValue;
        promoUpdatedAt = DateTime.MinValue;
    }
    public int addPromo()
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);
            string[] columns =
            {
                "PROMO_PRICE",
                "PROMO_NAME",
                "PROD_ID",
                "PROMO_FROM",
                "PROMO_TO",
                "PROMO_CREATEDBY"
            };
            string[] parameters = new string[columns.Length];
            for(int i=0;i<columns.Length;i++) { parameters[i] = "?"; }

            strSql = "INSERT INTO TB_PRODPROMO (" + string.Join(",", columns) +" , PROMO_CREATEDAT) VALUES " +
                     "("+string.Join(",",parameters)+", SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROMO_PRICE", OdbcType.Decimal).Value = promoPrice;
            cmd.Parameters.Add("p_PROMO_NAME", OdbcType.NVarChar, 250).Value = promoName;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int).Value = promoProdID;
            cmd.Parameters.Add("p_PROMO_FROM", OdbcType.DateTime).Value = promoFrom;
            cmd.Parameters.Add("p_PROMO_TO", OdbcType.DateTime).Value = promoTo;
            cmd.Parameters.Add("p_PROMO_CREATEDBY", OdbcType.Int).Value = prodCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int updatePromo()
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            
            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);
            string strSql = "UPDATE TB_PRODPROMO SET " +
                            "PROMO_PRICE = ?," +
                            "PROMO_FROM = ?," +
                            "PROMO_TO = ?," +
                            "PROMO_UPDATEDBY = ?," +
                            "PROMO_UPDATEDAT = NOW() " +
                            " WHERE PROMO_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROMO_PRICE", OdbcType.Decimal).Value = promoPrice;
            cmd.Parameters.Add("p_PROMO_FROM", OdbcType.DateTime).Value = promoFrom;
            cmd.Parameters.Add("p_PROMO_TO", OdbcType.DateTime).Value = promoTo;
            cmd.Parameters.Add("p_PROMO_UPDATEDBY", OdbcType.Int).Value = prodUpdatedBy;
            cmd.Parameters.Add("p_PROMO_ID", OdbcType.Int).Value = promoID;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int deletePromo(int intPromoID)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);
            strSql = "DELETE FROM TB_PRODPROMO WHERE PROMO_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROMO_ID", OdbcType.Decimal).Value = intPromoID;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public bool extractPromoByPromoID(int intPromoID)
    {
        bool boolSuccess = false;
        resetPromoValue();
        promoID = intPromoID;

        DataSet ds = getPromoDataSet();
        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            boolSuccess = true;

            promoID = Convert.ToInt32(ds.Tables[0].Rows[0]["PROMO_ID"]);
            promoProdID = Convert.ToInt32(ds.Tables[0].Rows[0]["PROD_ID"]);
            promoActive = Convert.ToInt16(ds.Tables[0].Rows[0]["PROMO_ACTIVE"]);
            promoOrder = Convert.ToInt32(ds.Tables[0].Rows[0]["PROMO_ORDER"]);
            promoCreatedBy = Convert.ToInt32(ds.Tables[0].Rows[0]["PROMO_CREATEDBY"]);
            promoUpdatedBy = Convert.ToInt32(ds.Tables[0].Rows[0]["PROMO_UPDATEDBY"]);

            promoPrice = Convert.ToDecimal(ds.Tables[0].Rows[0]["PROMO_PRICE"]);
            promoName = Convert.ToString(ds.Tables[0].Rows[0]["PROMO_NAME"]);

            promoFrom = Convert.ToDateTime(ds.Tables[0].Rows[0]["PROMO_FROM"]);
            promoTo = Convert.ToDateTime(ds.Tables[0].Rows[0]["PROMO_TO"]);
            promoCreatedAt = Convert.ToDateTime(ds.Tables[0].Rows[0]["PROMO_CREATEDAT"]);
            promoUpdatedAt = Convert.ToDateTime(ds.Tables[0].Rows[0]["PROMO_UPDATEDAT"]);
        }
        return boolSuccess;
    }
    public DataTable getPromoDataTableByProdID(int intProdID)
    {
        resetPromoValue();
        promoProdID = intProdID;

        return getPromoDataTable();
    }
    public DataTable getPromoDataTable()
    {
        DataSet ds = getPromoDataSet();
        if(ds.Tables.Count > 0)
        {
            return ds.Tables[0];
        }
        return null;
    }
    public DataSet getPromoDataSet()
    {
        DataSet dataSet = new DataSet();
        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_PRODPROMO A WHERE 1=1";
            if (promoID != -1)
            {
                strSql += " AND A.PROMO_ID = ?";
                cmd.Parameters.Add("p_PROMO_ID", OdbcType.Int, 9).Value = promoID;
            }
            if (promoProdID != -1)
            {
                strSql += " AND A.PROD_ID = ?";
                cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = promoProdID;
            }
            if (promoActive != -1)
            {
                strSql += " AND A.PROMO_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = promoActive;
            }
            if (promoOrder != -1)
            {
                strSql += " AND A.PROMO_ORDER = ?";
                cmd.Parameters.Add("p_PROMO_ORDER", OdbcType.Int, 9).Value = promoOrder;
            }
            if (promoCreatedBy != -1)
            {
                strSql += " AND A.PROMO_CREATEDBY = ?";
                cmd.Parameters.Add("p_PROMO_CREATEDBY", OdbcType.Int, 9).Value = promoCreatedBy;
            }
            if (promoUpdatedBy != -1)
            {
                strSql += " AND A.PROMO_UPDATEDBY = ?";
                cmd.Parameters.Add("p_PROMO_UPDATEDBY", OdbcType.Int, 9).Value = promoUpdatedBy;
            }
            if (promoPrice != -1)
            {
                strSql += " AND A.PROMO_PRICE = ?";
                cmd.Parameters.Add("p_PROMO_PRICE", OdbcType.Decimal).Value = promoPrice;
            }
            if (!string.IsNullOrEmpty(prodName))
            {
                strSql += " AND A.PROMO_NAME = ?";
                cmd.Parameters.Add("p_PROMO_NAME", OdbcType.NVarChar).Value = prodName;
            }
            if (promoFrom != DateTime.MinValue)
            {
                strSql += " AND A.PROMO_FROM = ?";
                cmd.Parameters.Add("p_PROMO_FROM", OdbcType.DateTime).Value = promoFrom;
            }
            if (promoTo != DateTime.MinValue)
            {
                strSql += " AND A.PROMO_TO = ?";
                cmd.Parameters.Add("p_PROMO_TO", OdbcType.DateTime).Value = promoTo;
            }
            if (promoCreatedAt != DateTime.MinValue)
            {
                strSql += " AND A.PROMO_CREATEDAT = ?";
                cmd.Parameters.Add("p_PROMO_CREATEDAT", OdbcType.DateTime).Value = promoCreatedAt;
            }
            if (promoUpdatedAt != DateTime.MinValue)
            {
                strSql += " AND A.PROMO_UPDATEDAT = ?";
                cmd.Parameters.Add("p_PROMO_UPDATEDAT", OdbcType.DateTime).Value = promoUpdatedAt;
            }
            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return dataSet;
    }
    #endregion
    public Boolean isProdCodeItemCodeExist(string strProdCode, string strItemCode)
    {
        Boolean boolExist = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT PROD_ID FROM TB_PRODUCT" +
                    " WHERE PROD_CODE in ('" + strProdCode + "','" + strItemCode + "')";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            prodId = Convert.ToInt32(cmd.ExecuteScalar());

            if (prodId > 0)
            {
                boolExist = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    #region "Jubin BMS"
    /*Customized for JubinBMS - Start*/
    public int addProductJubinBMS(string strProdCode, string strProdName, string strProdNameJp, string strProdNameMs, string strProdNameZh, string strProdDName, string strProdDNameJp, string strProdDNameMs, string strProdDNameZh,
                          string strPageTitleFURL, string strProdUOM, int intProdWeight, int intProdLength, int intProdWidth, int intProdHeight, int intProdNew, int intProdBest, int intProdActive, int intProdShowTop, int intProdSold, int intProdRent,
                          int intOutOfStock, int intPreorder, int intShowHome, int intSmartChoice, int intInstallation, int intWithGst, string strProdImg, int intProdCreatedBy, int intProdStockStatus, int intProdContainLiquid)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRODUCT (" +
                    "PROD_CODE, " +
                    "PROD_NAME, " +
                    "PROD_NAME_JP, " +
                    "PROD_NAME_MS, " +
                    "PROD_NAME_ZH, " +
                    "PROD_DNAME, " +
                    "PROD_DNAME_JP, " +
                    "PROD_DNAME_MS, " +
                    "PROD_DNAME_ZH, " +
                    "PROD_PAGETITLEFURL, " +
                    "PROD_UOM, " +
                    "PROD_WEIGHT, " +
                    "PROD_LENGTH, " +
                    "PROD_WIDTH, " +
                    "PROD_HEIGHT, " +
                    "PROD_NEW, " +
                    "PROD_BEST, " +
                    "PROD_ACTIVE, " +
                    "PROD_SHOWTOP, " +
                    "PROD_SOLD, " +
                    "PROD_RENT, " +
                    "PROD_OUTOFSTOCK, " +
                    "PROD_PREORDER, " +
                    "PROD_SHOWHOME, " +
                    "PROD_SMARTCHOICE, " +
                    "PROD_INSTALLATION, " +
                    "PROD_WITH_GST, " +
                    "PROD_IMAGE, " +
                    "PROD_CREATEDBY, " +
                    "PROD_CREATION, " +
                    "PROD_STOCKSTATUS, " +
                    "PROD_CONTAINLIQUID " +
                    ") VALUES " +
                   "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE(),?,?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 50).Value = strProdCode;
            cmd.Parameters.Add("p_PROD_NAME", OdbcType.VarChar, 250).Value = strProdName;
            cmd.Parameters.Add("p_PROD_NAME_JP", OdbcType.VarChar, 250).Value = strProdNameJp;
            cmd.Parameters.Add("p_PROD_NAME_MS", OdbcType.VarChar, 250).Value = strProdNameMs;
            cmd.Parameters.Add("p_PROD_NAME_ZH", OdbcType.VarChar, 250).Value = strProdNameZh;
            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_PROD_DNAME_JP", OdbcType.VarChar, 250).Value = strProdDNameJp;
            cmd.Parameters.Add("p_PROD_DNAME_MS", OdbcType.VarChar, 250).Value = strProdDNameMs;
            cmd.Parameters.Add("p_PROD_DNAME_ZH", OdbcType.VarChar, 250).Value = strProdDNameZh;
            cmd.Parameters.Add("p_PROD_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strPageTitleFURL;
            cmd.Parameters.Add("p_PROD_UOM", OdbcType.VarChar, 9).Value = strProdUOM;
            cmd.Parameters.Add("p_PROD_WEIGHT", OdbcType.Int, 9).Value = intProdWeight;
            cmd.Parameters.Add("p_PROD_LENGTH", OdbcType.Int, 9).Value = intProdLength;
            cmd.Parameters.Add("p_PROD_WIDTH", OdbcType.Int, 9).Value = intProdWidth;
            cmd.Parameters.Add("p_PROD_HEIGHT", OdbcType.Int, 9).Value = intProdHeight;
            cmd.Parameters.Add("p_PROD_NEW", OdbcType.Int, 1).Value = intProdNew;
            cmd.Parameters.Add("p_PROD_BEST", OdbcType.Int, 1).Value = intProdBest;
            cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intProdActive;
            cmd.Parameters.Add("p_PROD_SHOWTOP", OdbcType.Int, 1).Value = intProdShowTop;
            cmd.Parameters.Add("p_PROD_SOLD", OdbcType.Int, 1).Value = intProdSold;
            cmd.Parameters.Add("p_PROD_RENT", OdbcType.Int, 1).Value = intProdRent;
            cmd.Parameters.Add("p_PROD_OUTOFSTOCK", OdbcType.Int, 1).Value = intOutOfStock;
            cmd.Parameters.Add("p_PROD_PREORDER", OdbcType.Int, 1).Value = intPreorder;
            cmd.Parameters.Add("p_PROD_SHOWHOME", OdbcType.Int, 1).Value = intShowHome;
            cmd.Parameters.Add("p_PROD_SMARTCHOICE", OdbcType.Int, 1).Value = intSmartChoice;
            cmd.Parameters.Add("p_PROD_INSTALLATION", OdbcType.Int, 1).Value = intInstallation;
            cmd.Parameters.Add("p_PROD_WITH_GST", OdbcType.Int, 1).Value = intWithGst;
            cmd.Parameters.Add("p_PROD_IMAGE", OdbcType.VarChar, 250).Value = strProdImg;
            cmd.Parameters.Add("p_PROD_CREATEDBY", OdbcType.Int, 9).Value = intProdCreatedBy;
            cmd.Parameters.Add("p_PROD_STOCKSTATUS", OdbcType.VarChar, 10).Value = intProdStockStatus;
            cmd.Parameters.Add("p_PROD_CONTAINLIQUID", OdbcType.Int, 1).Value = intProdContainLiquid;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                prodId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean isExactSameProdSetJubinBMS(int intProdId, string strProdCode, string strProdName, string strProdNameJp, string strProdNameMs, string strProdNameZh, string strProdDName, string strProdDNameJp, string strProdDNameMs, string strProdDNameZh,
                                       string strPageTitleFUrl, string strProdUOM, int intProdWeight, int intProdLength, int intProdWidth, int intProdHeight, int intProdNew, int intProdBest, int intProdActive, int intProdShowTop, int intProdSold, int intProdRent,
                                       int intProdOutOfStock, int intProdPreorder, int intShowHome, int intSmartChoice, int intInstallation, int intProdWithGst, string strProdImage, int intProdStockStatus, int intProdContainLiquid)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRODUCT" +
                " WHERE PROD_ID = ?" +
                " AND BINARY PROD_CODE = ?" +
                " AND BINARY PROD_NAME = ?" +
                " AND BINARY PROD_NAME_JP = ?" +
                " AND BINARY PROD_NAME_MS = ?" +
                " AND BINARY PROD_NAME_ZH = ?" +
                " AND BINARY PROD_DNAME = ?" +
                " AND BINARY PROD_DNAME_JP = ?" +
                " AND BINARY PROD_DNAME_MS = ?" +
                " AND BINARY PROD_DNAME_ZH = ?" +
                " AND BINARY PROD_PAGETITLEFURL = ?" +
                " AND BINARY PROD_UOM = ?" +
                " AND PROD_WEIGHT = ?" +
                " AND PROD_LENGTH = ?" +
                " AND PROD_WIDTH = ?" +
                " AND PROD_HEIGHT = ?" +
                " AND PROD_NEW = ?" +
                " AND PROD_BEST = ?" +
                " AND PROD_ACTIVE = ?" +
                " AND PROD_SHOWTOP = ?" +
                " AND PROD_SOLD = ?" +
                " AND PROD_RENT = ?" +
                " AND PROD_OUTOFSTOCK = ?" +
                " AND PROD_PREORDER = ?" +
                " AND PROD_SHOWHOME = ?" +
                " AND PROD_SMARTCHOICE = ?" +
                " AND PROD_INSTALLATION = ?" +
                " AND PROD_WITH_GST = ?" +
                " AND BINARY PROD_IMAGE = ?" +
                " AND PROD_STOCKSTATUS = ?" +
                " AND PROD_CONTAINLIQUID = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 50).Value = strProdCode;
            cmd.Parameters.Add("p_PROD_NAME", OdbcType.VarChar, 250).Value = strProdName;
            cmd.Parameters.Add("p_PROD_NAME_JP", OdbcType.VarChar, 250).Value = strProdNameJp;
            cmd.Parameters.Add("p_PROD_NAME_MS", OdbcType.VarChar, 250).Value = strProdNameMs;
            cmd.Parameters.Add("p_PROD_NAME_ZH", OdbcType.VarChar, 250).Value = strProdNameZh;
            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_PROD_DNAME_JP", OdbcType.VarChar, 250).Value = strProdDNameJp;
            cmd.Parameters.Add("p_PROD_DNAME_MS", OdbcType.VarChar, 250).Value = strProdDNameMs;
            cmd.Parameters.Add("p_PROD_DNAME_ZH", OdbcType.VarChar, 250).Value = strProdDNameZh;
            cmd.Parameters.Add("p_PROD_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strPageTitleFUrl;
            cmd.Parameters.Add("p_PROD_UOM", OdbcType.VarChar, 250).Value = strProdUOM;
            cmd.Parameters.Add("p_PROD_WEIGHT", OdbcType.Int, 9).Value = intProdWeight;
            cmd.Parameters.Add("p_PROD_LENGTH", OdbcType.Int, 9).Value = intProdLength;
            cmd.Parameters.Add("p_PROD_WIDTH", OdbcType.Int, 9).Value = intProdWidth;
            cmd.Parameters.Add("p_PROD_HEIGHT", OdbcType.Int, 9).Value = intProdHeight;
            cmd.Parameters.Add("p_PROD_NEW", OdbcType.Int, 1).Value = intProdNew;
            cmd.Parameters.Add("p_PROD_BEST", OdbcType.Int, 1).Value = intProdBest;
            cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intProdActive;
            cmd.Parameters.Add("p_PROD_SHOWTOP", OdbcType.Int, 1).Value = intProdShowTop;
            cmd.Parameters.Add("p_PROD_SOLD", OdbcType.Int, 1).Value = intProdSold;
            cmd.Parameters.Add("p_PROD_RENT", OdbcType.Int, 1).Value = intProdRent;
            cmd.Parameters.Add("p_PROD_OUTOFSTOCK", OdbcType.Int, 1).Value = intProdOutOfStock;
            cmd.Parameters.Add("p_PROD_PREORDER", OdbcType.Int, 1).Value = intProdPreorder;
            cmd.Parameters.Add("p_PROD_SHOWHOME", OdbcType.Int, 1).Value = intShowHome;
            cmd.Parameters.Add("p_PROD_SMARTCHOICE", OdbcType.Int, 1).Value = intSmartChoice;
            cmd.Parameters.Add("p_PROD_INSTALLATION", OdbcType.Int, 1).Value = intInstallation;
            cmd.Parameters.Add("p_PROD_WITH_GST", OdbcType.Int, 1).Value = intProdWithGst;
            cmd.Parameters.Add("p_PROD_IMAGE", OdbcType.VarChar, 250).Value = strProdImage;
            cmd.Parameters.Add("p_PROD_STOCKSTATUS", OdbcType.VarChar, 10).Value = intProdStockStatus;
            cmd.Parameters.Add("p_PROD_CONTAINLIQUID", OdbcType.Int, 1).Value = intProdContainLiquid;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateProdByIdJubinBMS(int intProdId, string strProdCode, string strProdName, string strProdNameJp, string strProdNameMs, string strProdNameZh, string strProdDName, string strProdDNameJp, string strProdDNameMs, string strProdDNameZh,
                              string strPageTitleFUrl, string strProdUOM, int intProdWeight, int intProdLength, int intProdWidth, int intProdHeight, int intProdNew, int intProdBest, int intProdActive, int intProdShowTop, int intProdSold, int intProdRent,
                              int intProdOutOfStock, int intProdPreorder, int intProdShowHome, int intProdSmartChoice, int intProdInstallation, int intProdWithGst, string strProdImage, int intProdUpdatedBy, int intProdStockStatus, int intProdContainLiquid)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRODUCT SET" +
                     " PROD_CODE = ?," +
                     " PROD_NAME = ?," +
                     " PROD_NAME_JP = ?," +
                     " PROD_NAME_MS = ?," +
                     " PROD_NAME_ZH = ?," +
                     " PROD_DNAME = ?," +
                     " PROD_DNAME_JP = ?," +
                     " PROD_DNAME_MS = ?," +
                     " PROD_DNAME_ZH = ?," +
                     " PROD_PAGETITLEFURL = ?," +
                     " PROD_UOM = ?," +
                     " PROD_WEIGHT = ?," +
                     " PROD_LENGTH = ?," +
                     " PROD_WIDTH = ?," +
                     " PROD_HEIGHT = ?," +
                     " PROD_NEW = ?," +
                     " PROD_BEST = ?," +
                     " PROD_ACTIVE = ?," +
                     " PROD_SHOWTOP = ?," +
                     " PROD_SOLD = ?," +
                     " PROD_RENT = ?," +
                     " PROD_OUTOFSTOCK = ?," +
                     " PROD_PREORDER = ?," +
                     " PROD_SHOWHOME = ?," +
                     " PROD_SMARTCHOICE = ?," +
                     " PROD_INSTALLATION = ?," +
                     " PROD_WITH_GST = ?," +
                     " PROD_IMAGE = ?," +
                     " PROD_UPDATEDBY = ?," +
                     " PROD_LASTUPDATE = SYSDATE()," +
                     " PROD_STOCKSTATUS = ?," +
                     " PROD_CONTAINLIQUID = ?" +
                     " WHERE PROD_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_CODE", OdbcType.VarChar, 50).Value = strProdCode;
            cmd.Parameters.Add("p_PROD_NAME", OdbcType.VarChar, 250).Value = strProdName;
            cmd.Parameters.Add("p_PROD_NAME_JP", OdbcType.VarChar, 250).Value = strProdNameJp;
            cmd.Parameters.Add("p_PROD_NAME_MS", OdbcType.VarChar, 250).Value = strProdNameMs;
            cmd.Parameters.Add("p_PROD_NAME_ZH", OdbcType.VarChar, 250).Value = strProdNameZh;
            cmd.Parameters.Add("p_PROD_DNAME", OdbcType.VarChar, 250).Value = strProdDName;
            cmd.Parameters.Add("p_PROD_DNAME_JP", OdbcType.VarChar, 250).Value = strProdDNameJp;
            cmd.Parameters.Add("p_PROD_DNAME_MS", OdbcType.VarChar, 250).Value = strProdDNameMs;
            cmd.Parameters.Add("p_PROD_DNAME_ZH", OdbcType.VarChar, 250).Value = strProdDNameZh;
            cmd.Parameters.Add("p_PROD_PAGETITLEFURL", OdbcType.VarChar, 250).Value = strPageTitleFUrl;
            cmd.Parameters.Add("p_PROD_UOM", OdbcType.VarChar, 250).Value = strProdUOM;
            cmd.Parameters.Add("p_PROD_WEIGHT", OdbcType.Int, 9).Value = intProdWeight;
            cmd.Parameters.Add("p_PROD_LENGTH", OdbcType.Int, 9).Value = intProdLength;
            cmd.Parameters.Add("p_PROD_WIDTH", OdbcType.Int, 9).Value = intProdWidth;
            cmd.Parameters.Add("p_PROD_HEIGHT", OdbcType.Int, 9).Value = intProdHeight;
            cmd.Parameters.Add("p_PROD_NEW", OdbcType.Int, 9).Value = intProdNew;
            cmd.Parameters.Add("p_PROD_BEST", OdbcType.Int, 9).Value = intProdBest;
            cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 9).Value = intProdActive;
            cmd.Parameters.Add("p_PROD_SHOWTOP", OdbcType.Int, 9).Value = intProdShowTop;
            cmd.Parameters.Add("p_PROD_SOLD", OdbcType.Int, 1).Value = intProdSold;
            cmd.Parameters.Add("p_PROD_RENT", OdbcType.Int, 1).Value = intProdRent;
            cmd.Parameters.Add("p_PROD_OUTOFSTOCK", OdbcType.Int, 1).Value = intProdOutOfStock;
            cmd.Parameters.Add("p_PROD_PREORDER", OdbcType.Int, 1).Value = intProdPreorder;
            cmd.Parameters.Add("p_PROD_SHOWHOME", OdbcType.Int, 1).Value = intProdShowHome;
            cmd.Parameters.Add("p_PROD_SMARTCHOICE", OdbcType.Int, 1).Value = intProdSmartChoice;
            cmd.Parameters.Add("p_PROD_INSTALLATION", OdbcType.Int, 1).Value = intProdInstallation;
            cmd.Parameters.Add("p_PROD_WITH_GST", OdbcType.Int, 1).Value = intProdWithGst;
            cmd.Parameters.Add("p_PROD_IMAGE", OdbcType.VarChar, 250).Value = strProdImage;
            cmd.Parameters.Add("p_PROD_UPDATEDBY", OdbcType.Int, 9).Value = intProdUpdatedBy;
            cmd.Parameters.Add("p_PROD_STOCKSTATUS", OdbcType.VarChar, 10).Value = intProdStockStatus;
            cmd.Parameters.Add("p_PROD_CONTAINLIQUID", OdbcType.Int, 1).Value = intProdContainLiquid;
            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                prodId = intProdId;
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractProdByIdJubinBMS(int intProdId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PROD_ID, A.PROD_CODE, A.PROD_NAME, A.PROD_NAME_JP, A.PROD_NAME_MS, A.PROD_NAME_ZH, A.PROD_DNAME, A.PROD_DNAME_JP, A.PROD_DNAME_MS, A.PROD_DNAME_ZH," +
                     " A.PROD_PAGETITLEFURL, A.PROD_UOM, A.PROD_WEIGHT, A.PROD_LENGTH, A.PROD_WIDTH, A.PROD_HEIGHT, A.PROD_IMAGE," +
                     " A.PROD_NEW, A.PROD_BEST, A.PROD_SHOWTOP, A.PROD_SOLD, A.PROD_RENT, A.PROD_OUTOFSTOCK, A.PROD_PREORDER, A.PROD_SHOWHOME, A.PROD_SMARTCHOICE, A.PROD_INSTALLATION," +
                     " A.PROD_WITH_GST, A.PROD_ACTIVE, A.PROD_STOCKSTATUS, A.PROD_CONTAINLIQUID" +
                     " FROM TB_PRODUCT A" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;

            if (intActive != 0)
            {
                strSql += " AND A.PROD_ACTIVE = ?";
                cmd.Parameters.Add("p_PROD_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                prodId = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ID"].ToString());
                prodCode = dataSet.Tables[0].Rows[0]["PROD_CODE"].ToString();
                prodName = dataSet.Tables[0].Rows[0]["PROD_NAME"].ToString();
                prodNameJp = dataSet.Tables[0].Rows[0]["PROD_NAME_JP"].ToString();
                prodNameMs = dataSet.Tables[0].Rows[0]["PROD_NAME_MS"].ToString();
                prodNameZh = dataSet.Tables[0].Rows[0]["PROD_NAME_ZH"].ToString();
                prodDName = dataSet.Tables[0].Rows[0]["PROD_DNAME"].ToString();
                prodDNameJp = dataSet.Tables[0].Rows[0]["PROD_DNAME_JP"].ToString();
                prodDNameMs = dataSet.Tables[0].Rows[0]["PROD_DNAME_MS"].ToString();
                prodDNameZh = dataSet.Tables[0].Rows[0]["PROD_DNAME_ZH"].ToString();
                prodPageTitleFUrl = dataSet.Tables[0].Rows[0]["PROD_PAGETITLEFURL"].ToString();
                prodUOM = dataSet.Tables[0].Rows[0]["PROD_UOM"].ToString();
                prodWeight = int.Parse(dataSet.Tables[0].Rows[0]["PROD_WEIGHT"].ToString());
                prodLength = int.Parse(dataSet.Tables[0].Rows[0]["PROD_LENGTH"].ToString());
                prodWidth = int.Parse(dataSet.Tables[0].Rows[0]["PROD_WIDTH"].ToString());
                prodHeight = int.Parse(dataSet.Tables[0].Rows[0]["PROD_HEIGHT"].ToString());
                prodNew = int.Parse(dataSet.Tables[0].Rows[0]["PROD_NEW"].ToString());
                prodBest = int.Parse(dataSet.Tables[0].Rows[0]["PROD_BEST"].ToString());
                //prodSalesActive = int.Parse(dataSet.Tables[0].Rows[0]["PROD_HOTSALES"].ToString());
                prodActive = int.Parse(dataSet.Tables[0].Rows[0]["PROD_ACTIVE"].ToString());
                prodShowTop = int.Parse(dataSet.Tables[0].Rows[0]["PROD_SHOWTOP"].ToString());
                prodSold = int.Parse(dataSet.Tables[0].Rows[0]["PROD_SOLD"].ToString());
                prodRent = int.Parse(dataSet.Tables[0].Rows[0]["PROD_RENT"].ToString());
                prodOutOfStock = int.Parse(dataSet.Tables[0].Rows[0]["PROD_OUTOFSTOCK"].ToString());
                prodPreorder = int.Parse(dataSet.Tables[0].Rows[0]["PROD_PREORDER"].ToString());
                prodShowHome = int.Parse(dataSet.Tables[0].Rows[0]["PROD_SHOWHOME"].ToString());
                prodSmartChoice = int.Parse(dataSet.Tables[0].Rows[0]["PROD_SMARTCHOICE"].ToString());
                prodInstallation = int.Parse(dataSet.Tables[0].Rows[0]["PROD_INSTALLATION"].ToString());
                prodWithGst = int.Parse(dataSet.Tables[0].Rows[0]["PROD_WITH_GST"].ToString());
                prodImage = dataSet.Tables[0].Rows[0]["PROD_IMAGE"].ToString();
                prodStockStatus = dataSet.Tables[0].Rows[0]["PROD_STOCKSTATUS"].ToString();
                prodContainLiquid = int.Parse(dataSet.Tables[0].Rows[0]["PROD_CONTAINLIQUID"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    /*Customized for JubinBMS - End*/
    #endregion

    #region "HTM Pharmacy"
    /*Customized for HTM Pharmacy - Start*/
    public Boolean isPriceCodeExist(string strPriceCode, int intPriceId, int intProdId)
    {
        Boolean boolExist = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PRICING" +
                     " WHERE PRICING_CODE = ?";

            cmd.Parameters.Add("p_PRICING_CODE", OdbcType.VarChar, 50).Value = strPriceCode;

            if (intPriceId > 0)
            {
                strSql += " AND PRICING_ID != ?";
                cmd.Parameters.Add("p_PRICING_ID", OdbcType.Int, 9).Value = intPriceId;
            }

            if (intProdId > 0)
            {
                strSql += " AND PROD_ID != ?";
                cmd.Parameters.Add("p_PROD_ID", OdbcType.Int, 9).Value = intProdId;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount > 0)
            {
                boolExist = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolExist;
    }

    public Boolean isPricingExist(int intProdId, string strCode, string strName1, string strName2, decimal decPrice, int intEquivalent, int intBalQty, int intBest)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*)" +
                     " FROM TB_PRICING" +
                     " WHERE PROD_ID = ?" +
                     " AND PRICING_CODE = ?" +
                     " AND PRICING_NAME1 = ?" +
                     " AND PRICING_NAME2 = ?" +
                     " AND PRICING_PRICE = ?" +
                     " AND PRICING_EQUIVALENT = ?" +
                     " AND PRICING_BALQUANTITY = ?" +
                     " AND PRICING_BESTPRICE = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRICING_CODE", OdbcType.VarChar, 50).Value = strCode;
            cmd.Parameters.Add("p_PRICING_NAME1", OdbcType.VarChar, 250).Value = strName1;
            cmd.Parameters.Add("p_PRICING_NAME2", OdbcType.VarChar, 250).Value = strName2;
            cmd.Parameters.Add("p_PRICING_PRICE", OdbcType.Decimal).Value = decPrice;
            cmd.Parameters.Add("p_PRICING_EQUIVALENT", OdbcType.BigInt, 9).Value = intEquivalent;
            cmd.Parameters.Add("p_PRICING_BALQUANTITY", OdbcType.BigInt, 9).Value = intBalQty;
            cmd.Parameters.Add("p_PRICING_BESTPRICE", OdbcType.Int, 1).Value = intBest;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = int.Parse(cmd.ExecuteScalar().ToString());

            boolFound = intCount > 0 ? true : false;
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int addPricing(int intProdId, string strCode, string strName1, string strName2, decimal decPrice, int intEquivalent, int intBalQty, int intBest, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            int intOrder = getPricingOrder(intProdId);

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PRICING (" +
                     "PROD_ID, " +
                     "PRICING_CODE, " + 
                     "PRICING_NAME1, " +
                     "PRICING_NAME2, " +
                     "PRICING_PRICE, " +
                     "PRICING_EQUIVALENT, " +
                     "PRICING_BALQUANTITY, " +
                     "PRICING_BESTPRICE, " + 
                     "PRICING_ORDER, " +
                     "PRICING_CREATION, " +
                     "PRICING_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;
            cmd.Parameters.Add("p_PRICING_CODE", OdbcType.VarChar, 50).Value = strCode;
            cmd.Parameters.Add("p_PRICING_NAME1", OdbcType.VarChar, 250).Value = strName1;
            cmd.Parameters.Add("p_PRICING_NAME2", OdbcType.VarChar, 250).Value = strName2;
            cmd.Parameters.Add("p_PRICING_PRICE", OdbcType.Decimal).Value = decPrice;
            cmd.Parameters.Add("p_PRICING_EQUIVALENT", OdbcType.BigInt, 9).Value = intEquivalent;
            cmd.Parameters.Add("p_PRICING_BALQUANTITY", OdbcType.BigInt, 9).Value = intBalQty;
            cmd.Parameters.Add("p_PRICING_BESTPRICE", OdbcType.Int, 1).Value = intBest;
            cmd.Parameters.Add("p_PRICING_ORDER", OdbcType.Int, 9).Value = intOrder;
            cmd.Parameters.Add("p_PRICING_CREATEDBY", OdbcType.BigInt, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updatePricingById(int intPricingId, string strCode, string strName1, string strName2, decimal decPrice, int intEquivalent, int intBalQty, int intBest, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PRICING SET" +
                     " PRICING_CODE = ?," +
                     " pricing_name1 = ?," +
                     " pricing_name2 = ?," +
                     " pricing_price = ?," +
                     " pricing_equivalent = ?," +
                     " PRICING_BALQUANTITY = ?," +
                     " PRICING_BESTPRICE = ?," + 
                     " pricing_updatedby = ?," +
                     " PRICING_LASTUPDATE = NOW()" +
                     " WHERE PRICING_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PRICING_CODE", OdbcType.VarChar, 50).Value = strCode;
            cmd.Parameters.Add("p_PRICING_NAME1", OdbcType.VarChar, 250).Value = strName1;
            cmd.Parameters.Add("p_PRICING_NAME2", OdbcType.VarChar, 250).Value = strName2;
            cmd.Parameters.Add("p_PRICING_PRICE", OdbcType.Decimal).Value = decPrice;
            cmd.Parameters.Add("p_PRICING_EQUIVALENT", OdbcType.BigInt, 9).Value = intEquivalent;
            cmd.Parameters.Add("p_PRICING_BALQUANTITY", OdbcType.BigInt, 9).Value = intBalQty;
            cmd.Parameters.Add("p_PRICING_BESTPRICE", OdbcType.Int, 1).Value = intBest;
            cmd.Parameters.Add("p_PRICING_UPDATEDBY", OdbcType.Int, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_PRICING_ID", OdbcType.BigInt, 9).Value = intPricingId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getHTMPricingListById(int intProdId, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.PRICING_ID, A.PROD_ID, A.PRICING_CODE, A.PRICING_NAME1, A.PRICING_NAME2, A.PRICING_PRICE, A.PRICING_EQUIVALENT, A.PRICING_ORDER, A.PRICING_ACTIVE," +
                     " A.PRICING_BESTPRICE, A.PRICING_BALQUANTITY, A.PRICING_CREATION, A.PRICING_CREATEDBY, A.PRICING_LASTUPDATE, A.PRICING_UPDATEDBY" +
                     " FROM TB_PRICING A" +
                     " WHERE A.PROD_ID = ?";

            cmd.Parameters.Add("p_PROD_ID", OdbcType.BigInt, 9).Value = intProdId;

            if (intActive != 0)
            {
                strSql += " AND A.PRICING_ACTIVE = ?";
                cmd.Parameters.Add("p_PRICING_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    /*Customized for HTM Pharmacy - End*/
    #endregion

    #endregion

    #region "Upload Image"
    public void addProdGallery(ProdGalleryDetail prodGalleryDetail)
    {
        System.Web.Script.Serialization.JavaScriptSerializer json = new System.Web.Script.Serialization.JavaScriptSerializer();
        Dictionary<string, ProdGalleryDetail> dictSession = new Dictionary<string, ProdGalleryDetail>();
        if (HttpContext.Current.Session["PROD_GALLERY"] != null)
        {
            dictSession = json.Deserialize<Dictionary<string, ProdGalleryDetail>>(HttpContext.Current.Session["PROD_GALLERY"].ToString());
        }
        
        dictSession.Add(prodGalleryDetail.filenameEncode, prodGalleryDetail);
        HttpContext.Current.Session["PROD_GALLERY"] = json.Serialize(dictSession);
    }
    public void addProdGallerySort(ProdGalleryDetail prodGalleryDetail)
    {
        System.Web.Script.Serialization.JavaScriptSerializer json = new System.Web.Script.Serialization.JavaScriptSerializer();
        Dictionary<string, int> dictSession = new Dictionary<string, int>();
        if (HttpContext.Current.Session["PROD_GALLERY_SORTING"] != null)
        {
            dictSession = json.Deserialize<Dictionary<string, int>>(HttpContext.Current.Session["PROD_GALLERY_SORTING"].ToString());
        }
        dictSession.Add(prodGalleryDetail.filenameEncode, 9999);
        HttpContext.Current.Session["PROD_GALLERY_SORTING"] = json.Serialize(dictSession);
    }
    public void addProdGalleryDefault(ProdGalleryDetail prodGalleryDetail)
    {
        System.Web.Script.Serialization.JavaScriptSerializer json = new System.Web.Script.Serialization.JavaScriptSerializer();
        Dictionary<string, int> dictSession = new Dictionary<string, int>();
        bool boolDefault = false;
        int intDefault = 0;

        if (HttpContext.Current.Session["PROD_GALLERY_DEFAULT"] != null)
        {
            dictSession = json.Deserialize<Dictionary<string, int>>(HttpContext.Current.Session["PROD_GALLERY_DEFAULT"].ToString());

            foreach (KeyValuePair<string, int> entry in dictSession)
            {
                if (entry.Value == 1)
                {
                    boolDefault = true;
                    break;
                }
            }
        }

        if (!boolDefault)
        {
            intDefault = 1;
        }
        
        dictSession.Add(prodGalleryDetail.filenameEncode, intDefault);
        HttpContext.Current.Session["PROD_GALLERY_DEFAULT"] = json.Serialize(dictSession);
    }
    public string getProdGallery(int intId)
    {
        System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
        Dictionary<string, ProdGalleryDetail> dictProdGalleryDetail = new Dictionary<string, ProdGalleryDetail>();

        // bind from database
        DataSet ds = getGalleryById(intId);
        foreach (DataRow dw in ds.Tables[0].Rows)
        {
            ProdGalleryDetail prodGalleryDetail = new ProdGalleryDetail();
            prodGalleryDetail.id = Convert.ToInt32(dw["PRODGALL_ID"]);
            prodGalleryDetail.prodId = int.Parse(dw["PROD_ID"].ToString());
            prodGalleryDetail.title = dw["PRODGALL_TITLE"].ToString();
            prodGalleryDetail.filename = dw["PRODGALL_IMAGE"].ToString();
            prodGalleryDetail.filenameEncode = System.Uri.EscapeDataString(prodGalleryDetail.filename);
            prodGalleryDetail.desc = Convert.ToString(dw["PRODGALL_DESC"]);
            prodGalleryDetail.defaultImg = int.Parse(dw["PRODGALL_DEFAULT"].ToString());
            prodGalleryDetail.order = int.Parse(dw["PRODGALL_ORDER"].ToString());
            prodGalleryDetail.type = "database";
            
            dictProdGalleryDetail.Add(prodGalleryDetail.filenameEncode, prodGalleryDetail);
        }
        return js.Serialize(dictProdGalleryDetail);
    }
    public string getProdGallerySorting()
    {
        System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
        Dictionary<string, int> dictProdGalleryDetail = new Dictionary<string, int>();

        if (HttpContext.Current.Session["PROD_GALLERY"] != null)
        {
            Dictionary<string, ProdGalleryDetail> dictProdGallery = js.Deserialize<Dictionary<string, ProdGalleryDetail>>(HttpContext.Current.Session["PROD_GALLERY"].ToString());
            foreach (KeyValuePair<string, ProdGalleryDetail> entry in dictProdGallery)
            {
                dictProdGalleryDetail.Add(entry.Key, entry.Value.order);
            }
        }
        return js.Serialize(dictProdGalleryDetail);
    }
    public string getProdGalleryDefault()
    {
        System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
        Dictionary<string, int> dictProdGalleryDetail = new Dictionary<string, int>();

        if (HttpContext.Current.Session["PROD_GALLERY"] != null)
        {
            Dictionary<string, ProdGalleryDetail> dictProdGallery = js.Deserialize<Dictionary<string, ProdGalleryDetail>>(HttpContext.Current.Session["PROD_GALLERY"].ToString());
            foreach (KeyValuePair<string, ProdGalleryDetail> entry in dictProdGallery)
            {
                dictProdGalleryDetail.Add(entry.Key, entry.Value.defaultImg);
            }
        }
        return js.Serialize(dictProdGalleryDetail);
    }
    public string getProdGalleryList()
    {
        System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
        List<ProdGalleryDetail> listProdGalleryDetail = new List<ProdGalleryDetail>();

        if (HttpContext.Current.Session["PROD_GALLERY"] != null)
        {
            Dictionary<string, ProdGalleryDetail> dictEventGallery = js.Deserialize<Dictionary<string, ProdGalleryDetail>>(HttpContext.Current.Session["PROD_GALLERY"].ToString());
            foreach (KeyValuePair<string, ProdGalleryDetail> entry in dictEventGallery)
            {
                // do something with entry.Value or entry.Key
                listProdGalleryDetail.Add(entry.Value);
            }
        }
        return js.Serialize(listProdGalleryDetail);
    }
    #endregion
}

public class ProdGalleryDetail
{
    public int id { get; set; }
    public int prodId { get; set; }
    public int order { get; set; }
    public string filename { get; set; }
    public string filenameEncode { get; set; }
    public string desc { get; set; }
    public int defaultImg { get; set; }
    public string title { get; set; }
    public string type { get; set; }
    public bool isDelete { get; set; }

    public ProdGalleryDetail()
    {
        isDelete = false;
        order = 9999;

        id = -1;
        prodId = -1;
    }
}