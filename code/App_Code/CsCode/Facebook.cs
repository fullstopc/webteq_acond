﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for BorderlessImage
/// </summary>
public class Facebook
{
    public string pageUrl { get; set; }
    public string pageName { get; set; }
    public string imageUrl { get; set; }
    public string websiteUrl { get; set; }
    public string title { get; set; }
    public string type { get; set; }
    public string desc { get; set; }

    public void setState()
    {
        clsConfig config = new clsConfig();

        pageUrl = getConfigValue(config, clsConfig.CONSTNAMEFBPAGEURL, clsConfig.CONSTGROUPFB);
        pageName = getConfigValue(config, clsConfig.CONSTNAMEFBLIKESITENAME, clsConfig.CONSTGROUPFB);
        title = getConfigValue(config, clsConfig.CONSTNAMEFBLIKETITLE, clsConfig.CONSTGROUPFB);
        type = getConfigValue(config, clsConfig.CONSTNAMEFBLIKETYPE, clsConfig.CONSTGROUPFB);
        websiteUrl = getConfigValue(config, clsConfig.CONSTNAMEFBLIKEURL, clsConfig.CONSTGROUPFB);
        pageUrl = getConfigValue(config, clsConfig.CONSTNAMEFBPAGEURL, clsConfig.CONSTGROUPFB);
        imageUrl = getConfigValue(config, clsConfig.CONSTNAMEFBLIKEIMAGE, clsConfig.CONSTGROUPFB);
        desc = getConfigValue(config, clsConfig.CONSTNAMEFBLIKEDESC, clsConfig.CONSTGROUPFB);
        config.extractItemByNameGroup(clsConfig.CONSTNAMEFBLIKEIMAGE, clsConfig.CONSTGROUPFB, 1);
    }

    public string getConfigValue(clsConfig config, string name, string group)
    {
        return config.extractItemByNameGroup(name, group, 1) ? config.value : null;
    }
}
