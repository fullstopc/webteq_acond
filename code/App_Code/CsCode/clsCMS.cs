﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsCMS
/// </summary>
public class clsCMS : dbconnBase
{
    #region "Properties"

    private int _PageId;
    private string _PageTitle;
    private string _PageContent;
    private int _PageShowPageTitle;
    private int _PageActive;
    private int _PageCreatedBy;
    private DateTime _PageCreation;
    private int _PageUpdateBy;
    private DateTime _PageLastUpdate;

    private int _SectId;
    private string _SectName;
    private int _SectCreatedBy;
    private DateTime _SectCreation;
    private int _SectUpdateBy;
    private DateTime _SectLastUpdate;

    private int _intCMSId;
    private string _strCMSTitle;
    private string _strCMSContent;
    private int _intCMSType;
    private DateTime _CMSCreation;
    private DateTime _CMSLastUpdate;
    private int _intCMSActive;
    private int _intCMSCreatedBy;
    private int _intCMSUpdatedBy;

    private string _PageValue;
    private string _SectValue;
    #endregion

    #region "Property Methods"
    public int PageId
    {
        get { return _PageId; }
        set { _PageId = value; }
    }

    public string PageTitle
    {
        get { return _PageTitle; }
        set { _PageTitle = value; }
    }

    public string PageContent
    {
        get { return _PageContent; }
        set { _PageContent = value; }
    }

    public int PageShowPageTitle
    {
        get { return _PageShowPageTitle; }
        set { _PageShowPageTitle = value; }
    }

    public int PageActive
    {
        get { return _PageActive; }
        set { _PageActive = value; }
    }

    public int PageCreatedBy
    {
        get { return _PageCreatedBy; }
        set { _PageCreatedBy = value; }
    }

    public DateTime PageCreation
    {
        get { return _PageCreation; }
        set { _PageCreation = value; }
    }

    public int PageUpdateBy
    {
        get { return _PageUpdateBy; }
        set { _PageUpdateBy = value; }
    }

    public DateTime PageLastUpdate
    {
        get { return _PageLastUpdate; }
        set { _PageLastUpdate = value; }
    }


    public int SectId
    {
        get { return _SectId; }
        set { _SectId = value; }
    }

    public string SectName
    {
        get { return _SectName; }
        set { _SectName = value; }
    }

    public int SectCreatedBy
    {
        get { return _SectCreatedBy; }
        set { _SectCreatedBy = value; }
    }

    public DateTime SectCreation
    {
        get { return _SectCreation; }
        set { _SectCreation = value; }
    }

    public int SectUpdateBy
    {
        get { return _SectUpdateBy; }
        set { _SectUpdateBy = value; }
    }

    public DateTime SectLastUpdate
    {
        get { return _SectLastUpdate; }
        set { _SectLastUpdate = value; }
    }

    public string PageValue
    {
        get { return _PageValue; }
        set { _PageValue = value; }
    }

    public string SectValue
    {
        get { return _SectValue; }
        set { _SectValue = value; }
    }

    public int cmsId
    {
        get { return _intCMSId; }
        set { _intCMSId = value; }
    }

    public string cmsTitle
    {
        get { return _strCMSTitle; }
        set { _strCMSTitle = value; }
    }

    public string cmsContent
    {
        get { return _strCMSContent; }
        set { _strCMSContent = value; }
    }

    public int cmsType
    {
        get { return _intCMSType; }
        set { _intCMSType = value; }
    }

    public DateTime cmsCreation
    {
        get { return _CMSCreation; }
        set { _CMSCreation = value; }
    }

    public DateTime cmsLastUpdate
    {
        get { return _CMSLastUpdate; }
        set { _CMSLastUpdate = value; }
    }

    public int cmsActive
    {
        get { return _intCMSActive; }
        set { _intCMSActive = value; }
    }

    public int cmsCreatedBy
    {
        get { return _intCMSCreatedBy; }
        set { _intCMSCreatedBy = value; }
    }

    public int cmsUpdatedBy
    {
        get { return _intCMSUpdatedBy; }
        set { _intCMSUpdatedBy = value; }
    }
    #endregion

    #region "Public Methods"
    public clsCMS()
    { }

    public int getTotalPages(int intSectValue)
    {
        int intTotal = 0;

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            strSql = "SELECT COUNT(*) AS TOTAL FROM VW_PAGE" +
                     " WHERE SECT_VALUE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_SECT_VALUE", OdbcType.Int, 9).Value = intSectValue;
            intTotal = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intTotal;
    }

    public int addCMSContent(int intPageValue, int intSectValue, string strPageTitle, int intPageShowPageTitle, int intPageActive, string strPageContent, int intPageCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_PAGE (" +
                    "PAGE_TITLE, " +
                    "PAGE_CONTENT, " +
                    "PAGE_SHOWPAGETITLE, " +
                    "PAGE_ACTIVE, " +
                    "PAGE_VALUE," +
                    "SECT_VALUE," +
                    "PAGE_CREATEDBY, " +
                    "PAGE_CREATION" +
                    ") VALUES " +
                   "(?,?,?,?,?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;


            cmd.Parameters.Add("p_PAGE_TITLE", OdbcType.VarChar, 50).Value = strPageTitle;
            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strPageContent;
            cmd.Parameters.Add("p_PAGE_SHOWPAGETITLE", OdbcType.Int, 1).Value = intPageShowPageTitle;
            cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intPageActive;
            cmd.Parameters.Add("p_PAGE_VALUE", OdbcType.VarChar, 20).Value = intPageValue;
            cmd.Parameters.Add("p_SECT_VALUE", OdbcType.VarChar, 20).Value = intSectValue;
            cmd.Parameters.Add("p_PAGE_CREATEDBY", OdbcType.Int, 9).Value = intPageCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                PageId = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractCMSContentById(int intPageValue, int intSectValue, int intPageActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM VW_PAGE WHERE PAGE_VALUE = ? " +
                     "AND SECT_VALUE = ?";

            cmd.Parameters.Add("p_PAGE_VALUE", OdbcType.VarChar, 20).Value = intPageValue;
            cmd.Parameters.Add("p_SECT_VALUE", OdbcType.VarChar, 20).Value = intSectValue;

            if (intPageActive != 0)
            {
                strSql += " AND PAGE_ACTIVE = ?";
                cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intPageActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                PageId = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_ID"].ToString());
                PageTitle = dataSet.Tables[0].Rows[0]["PAGE_TITLE"].ToString();
                PageContent = dataSet.Tables[0].Rows[0]["PAGE_CONTENT"].ToString();
                PageShowPageTitle = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_SHOWPAGETITLE"].ToString());
                PageActive = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_ACTIVE"].ToString());
                PageValue = dataSet.Tables[0].Rows[0]["PAGE_VALUE"].ToString();
                SectValue = dataSet.Tables[0].Rows[0]["SECT_VALUE"].ToString();
                PageCreatedBy = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_CREATEDBY"].ToString());
                PageCreation = dataSet.Tables[0].Rows[0]["PAGE_CREATION"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PAGE_CREATION"].ToString()) : DateTime.MaxValue; ;
                PageUpdateBy = int.Parse(dataSet.Tables[0].Rows[0]["PAGE_UPDATEBY"].ToString());
                PageLastUpdate = dataSet.Tables[0].Rows[0]["PAGE_LASTUPDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["PAGE_LASTUPDATE"].ToString()) : DateTime.MaxValue;
                SectName = dataSet.Tables[0].Rows[0]["SECT_NAME"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public Boolean extractCMSContent(int intCMSId, int intCMSActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM VW_CMS WHERE CMS_ID = ? ";

            cmd.Parameters.Add("p_CMS_ID", OdbcType.BigInt, 9).Value = intCMSId;

            if (intCMSActive != 0)
            {
                strSql += " AND CMS_ACTIVE = ?";
                cmd.Parameters.Add("p_CMS_ACTIVE", OdbcType.Int, 1).Value = intCMSActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                cmsId = int.Parse(dataSet.Tables[0].Rows[0]["CMS_ID"].ToString());
                cmsTitle = dataSet.Tables[0].Rows[0]["CMS_TITLE"].ToString();
                cmsContent = dataSet.Tables[0].Rows[0]["CMS_CONTENT"].ToString();
                cmsType = int.Parse(dataSet.Tables[0].Rows[0]["CMS_TYPE"].ToString());
                cmsCreation = PageCreation = dataSet.Tables[0].Rows[0]["CMS_CREATION"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["CMS_CREATION"].ToString()) : DateTime.MaxValue; ;
                cmsCreatedBy = int.Parse(dataSet.Tables[0].Rows[0]["CMS_CREATEDBY"].ToString());
                cmsLastUpdate = PageCreation = dataSet.Tables[0].Rows[0]["CMS_LASTUPDATE"] != DBNull.Value ? DateTime.Parse(dataSet.Tables[0].Rows[0]["CMS_LASTUPDATE"].ToString()) : DateTime.MaxValue; ;
                cmsUpdatedBy = int.Parse(dataSet.Tables[0].Rows[0]["CMS_UPDATEDBY"].ToString());
                cmsActive = int.Parse(dataSet.Tables[0].Rows[0]["CMS_ACTIVE"].ToString());
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public DataSet getSectionList()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.SECT_ID, A.SECT_NAME, A.SECT_VALUE, A.SECT_CREATEDBY, A.SECT_CREATION,  A.SECT_UPDATEBY, A.SECT_LASTUPDATE" +
                     " FROM TB_SECTION A";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getPageList()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM VW_PAGE A";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean isExactSameCMSContent(int intPageValue, int intSectValue, string strPageTitle, int intPageShowPageTitle, int intPageActive, string strPageContent)
    {
        Boolean bExactSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_PAGE" +
                " WHERE PAGE_TITLE = ?" +
                " AND PAGE_CONTENT = ?" +
                " AND PAGE_SHOWPAGETITLE = ?" +
                " AND PAGE_ACTIVE = ?" +
                " AND PAGE_VALUE = ?" +
                " AND SECT_VALUE = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;


            cmd.Parameters.Add("p_PAGE_TITLE", OdbcType.VarChar, 50).Value = strPageTitle;
            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strPageContent;
            cmd.Parameters.Add("p_PAGE_SHOWPAGETITLE", OdbcType.Int, 1).Value = intPageShowPageTitle;
            cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intPageActive;
            cmd.Parameters.Add("p_PAGE_VALUE", OdbcType.VarChar, 20).Value = intPageValue;
            cmd.Parameters.Add("p_SECT_VALUE", OdbcType.VarChar, 20).Value = intSectValue;

            int iCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (iCount >= 1)
            {
                bExactSame = true;
            }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return bExactSame;
    }

    public int updateCMSContentById(int intPageValue, int intSectValue, string strPageTitle, int intPageShowPageTitle, int intPageActive, string strPageContent, int intPageUpdateBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_PAGE SET" +
                " PAGE_TITLE = ?," +
                " PAGE_SHOWPAGETITLE = ?," +
                " PAGE_ACTIVE = ?," +
                " PAGE_CONTENT = ?," +
                " PAGE_UPDATEBY = ?," +
                " PAGE_LASTUPDATE = SYSDATE()" +
                " WHERE PAGE_VALUE = ?" +
                " AND SECT_VALUE = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_PAGE_TITLE", OdbcType.VarChar, 50).Value = strPageTitle;
            cmd.Parameters.Add("p_PAGE_SHOWPAGETITLE", OdbcType.Int, 1).Value = intPageShowPageTitle;
            cmd.Parameters.Add("p_PAGE_ACTIVE", OdbcType.Int, 1).Value = intPageActive;
            cmd.Parameters.Add("p_PAGE_CONTENT", OdbcType.Text).Value = strPageContent;
            cmd.Parameters.Add("p_PAGE_UPDATEBY", OdbcType.Int, 9).Value = intPageUpdateBy;
            cmd.Parameters.Add("p_PAGE_VALUE", OdbcType.VarChar, 20).Value = intPageValue;
            cmd.Parameters.Add("p_SECT_VALUE", OdbcType.VarChar, 20).Value = intSectValue;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
                PageValue = intPageValue.ToString();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getCMSList()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();
            strSql = "SELECT * FROM VW_PAGE A";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);

        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public int getSectIdByPageId(int intPgId)
    {
        int intSectId = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT SECT_ID FROM VW_PAGE WHERE PAGE_ID = ?";

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("p_PAGE_ID", OdbcType.BigInt, 9).Value = intPgId;

            intSectId = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intSectId;
    }
    #endregion
}
