﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Linq;

/// <summary>
/// Summary description for clsConfig
/// </summary>
public class clsConfig : dbconnBase
{
    // change accordingly to db
    #region "Constants"
    // group
    public const string CONSTGROUPPAGEDETAILS = "PAGE DETAILS";
    public const string CONSTGROUPPAGEDETAILSLANG = "PAGE DETAILS LANGUAGE";
    public const string CONSTGROUPEMAILCONTENT = "EMAIL CONTENT";
    public const string CONSTGROUPSTFEMAILCONTENT = "SHARE TO FRIEND EMAIL CONTENT";
    public const string CONSTGROUPASKEMAILCONTENT = "ASK EMAIL CONTENT";
    public const string CONSTGROUPNEWORDEREMAILCONTENT = "NEW ORDER EMAIL CONTENT";
    public const string CONSTGROUPPAYRECBTEMAILCONTENT = "PAYMENT RECEIVE (BT) EMAIL CONTENT";
    public const string CONSTGROUPPAYRECOPEMAILCONTENT = "PAYMENT RECEIVE (OP) EMAIL CONTENT";
    public const string CONSTGROUPPAYERROREMAILCONTENT = "PAYMENT ERROR EMAIL CONTENT";
    public const string CONSTGROUPPAYREJECTEMAILCONTENT = "PAYMENT REJECT EMAIL CONTENT";
    public const string CONSTGROUPREFUNDEMAILCONTENT = "REFUND EMAIL CONTENT";
    public const string CONSTGROUPORDPROCESSINGEMAILCONTENT = "ORDER PROCESSING EMAIL CONTENT";
    public const string CONSTGROUPORDDELIVERYEMAILCONTENT = "ORDER DELIVERY EMAIL CONTENT";
    public const string CONSTGROUPORDDONEEMAILCONTENT = "ORDER DONE EMAIL CONTENT";
    public const string CONSTGROUPFORGOTPWDEMAILCONTENT = "FORGOT PASSWORD EMAIL CONTENT";
    public const string CONSTGROUPORDCANCELEMAILCONTENT = "ORDER CANCEL EMAIL CONTENT";
    public const string CONSTGROUPPGAUTHORIZATIONEMAILCONTENT = "PAGE AUTHORIZATION EMAIL CONTENT";
    public const string CONSTGROUPEMAILSETTING = "EMAIL SETTING";
    public const string CONSTGROUPWEBTEQEMAILSETTING = "WEBTEQ EMAIL SETTING";
    public const string CONSTGROUPDEFAULT = "DEFAULT SETTINGS";
    public const string CONSTGROUPSHIPPING = "SHIPPING";
    public const string CONSTGROUPQUICKCONTACT = "QUICK CONTACT";
    public const string CONSTGROUPFB = "FACEBOOK";
    public const string CONSTGROUPWEBSITE = "WEBSITE SETTINGS";
    public const string CONSTGROUPGOOGLE = "GOOGLE SETTINGS";
    public const string CONSTGROUPGOOGLEPLUS = "GOOGLEPLUS SETTINGS";
    public const string CONSTGROUPLINKEDIN = "LINKEDIN SETTINGS";
    public const string CONSTGROUPINSTAGRAM = "INSTAGRAM SETTINGS";
    public const string CONSTGROUPTWITTER = "TWITTER SETTINGS";
    public const string CONSTGROUPYOUTUBE = "YOUTUBE SETTINGS";
    public const string CONSTGROUPFAVICON = "FAVICON SETTINGS";
    public const string CONSTGROUPDEFAULTPAGE = "DEFAULT PAGE SETTINGS";
    public const string CONSTGROUPPRODUCT = "PRODUCT SETTINGS";
    public const string CONSTGROUPSHIPPINGCOMPANY = "SHIPPING COMPANY SETTINGS";
    public const string CONSTGROUPSHIPPINGMETHOD = "SHIPPING METHOD SETTINGS";
    public const string CONSTGROUPINVOICE = "INVOICE SETTINGS";
    public const string CONSTGROUPUSERMANUAL = "USER MANUAL";
    public const string CONSTGROUPOTHERSETTINGS = "OTHER SETTINGS";
    public const string CONSTGROUPSMSCONTENT = "SMS CONTENT";
    public const string CONSTGROUPSMSSETTINGS = "SMS SETTINGS";
    public const string CONSTGROUPCUTOFFDATEDETAILS = "CUTOFF DATE DETAILS";
    public const string CONSTGROUPGSTSETTING = "GST SETTING";
    public const string CONSTGROUPLOGOANDICONSETTINGS = "LOGO AND ICON SETTINGS";
    public const string CONSTGROUPWATERMARKSETTINGS = "WATERMARK SETTINGS";
    public const string CONSTGROUPPAYPALSETTINGS = "PAYPAL SETTINGS";
    public const string CONSTGROUPMOLPAYSETTINGS = "MOLPAY SETTINGS";
    public const string CONSTGROUPIPAY88SETTINGS = "IPAY88 SETTINGS";
    public const string CONSTGROUPIPAY88ENETSSETTINGS = "IPAY88_ENETS SETTINGS";
    public const string CONSTGROUPEGHLSETTINGS = "EGHL SETTINGS";
    public const string CONSTGROUPORDERSLIPSETUP = "ORDER SLIP SETUP";
    public const string CONSTGROUPFBLOGIN = "FACEBOOK LOGIN";
    public const string CONSTGROUPGOOGLEANALYTICS = "Google Analytics";
    public const string CONSTGROUPPRODUCTDIMENSION = "Product Dimension";
    public const string CONSTGROUPPRODUCTWEIGHT = "Product Weight";
    public const string CONSTGROUPCROPIMAGESETTINGS = "CROP IMAGE SETTINGS";
    public const string CONSTGROUPENQUIRYSETTINGS = "Enquiry Settings";
    public const string CONSTGROUPGOOGLERECAPTCHASETTINGS = "GOOGLE RECAPTCHA SETTINGS";

    // name
    public const string CONSTNAMEMAXTOTALPAGE = "Max Total Page";
    public const string CONSTNAMEPAGETITLEPREFIX = "Page Title Prefix";
    public const string CONSTNAMEPAGETITLE = "Default Page Title";
    public const string CONSTNAMEKEYWORD = "Default Keyword";
    public const string CONSTNAMEDESCRIPTION = "Default Description";
    public const string CONSTNAMESMTPSERVER = "Smtp Server";
    public const string CONSTNAMEDEFAULTSENDERNAME = "Default Sender Name";
    public const string CONSTNAMEDEFAULTSENDER = "Default Sender";
    public const string CONSTNAMEDEFAULTSENDERPWD = "Default Sender Password";
    public const string CONSTNAMEDEFAULTRECIPIENT = "Default Recipient";
    public const string CONSTNAMEDEFAULTRECIPIENTBCC = "Default Recipient BCC";
    public const string CONSTNAMEUSERGMAILSMTP = "Use Gmail Smtp";
    public const string CONSTNAMEPORT = "Port";
    public const string CONSTNAMEENABLESSL = "Enable SSL";
    public const string CONSTNAMEEMAILPREFIX = "Email Subject Prefix";
    public const string CONSTNAMEEMAILSIGNATURE = "Email Signature";
    public const string CONSTNAMEADMINEMAILSUBJECT = "Admin Email Subject";
    public const string CONSTNAMEADMINEMAILCONTENT = "Admin Email Content";
    public const string CONSTNAMEUSEREMAILSUBJECT = "User Email Subject";
    public const string CONSTNAMEUSEREMAILCONTENT = "User Email Content";
    public const string CONSTNAMESTFEMAILSUBJECT = "Share to Friend Email Subject";
    public const string CONSTNAMESTFUSEREMAILCONTENT = "Share to Friend User Email Content";
    public const string CONSTNAMEASKEMAILSUBJECT = "Ask Email Subject";
    public const string CONSTNAMEASKADMINEMAILCONTENT = "Ask Admin Email Content";
    public const string CONSTNAMEASKUSEREMAILCONTENT = "Ask User Email Content";
    public const string CONSTNAMENEWORDEMAILSUBJECT = "New Order Email Subject";
    public const string CONSTNAMENEWORDADMINEMAILCONTENT = "New Order Admin Email Content";
    public const string CONSTNAMENEWORDUSEREMAILCONTENT = "New Order User Email Content";
    public const string CONSTNAMEPAYRECBTEMAILSUBJECT = "Payment Receive(BT) Email Subject";
    public const string CONSTNAMEPAYRECBTADMINEMAILCONTENT = "Payment Receive(BT) Admin Email Content";
    public const string CONSTNAMEPAYRECBTUSEREMAILCONTENT = "Payment Receive(BT) User Email Content";
    public const string CONSTNAMEPAYRECOPEMAILSUBJECT = "Payment Receive(OP) Email Subject";
    public const string CONSTNAMEPAYRECOPADMINEMAILCONTENT = "Payment Receive(OP) Admin Email Content";
    public const string CONSTNAMEPAYRECOPUSEREMAILCONTENT = "Payment Receive(OP) User Email Content";
    public const string CONSTNAMEPAYERROREMAILSUBJECT = "Payment Error Email Subject";
    public const string CONSTNAMEPAYERRORADMINEMAILCONTENT = "Payment Error Admin Email Content";
    public const string CONSTNAMEPAYERRORUSEREMAILCONTENT = "Payment Error User Email Content";
    public const string CONSTNAMEPAYREJECTEMAILSUBJECT = "Payment Reject Email Subject";
    public const string CONSTNAMEPAYREJECTADMINEMAILCONTENT = "Payment Reject Admin Email Content";
    public const string CONSTNAMEPAYREJECTUSEREMAILCONTENT = "Payment Reject User Email Content";
    public const string CONSTNAMEREFUNDEMAILSUBJECT = "Refund Email Subject";
    public const string CONSTNAMEREFUNDADMINEMAILCONTENT = "Refund Admin Email Content";
    public const string CONSTNAMEREFUNDUSEREMAILCONTENT = "Refund User Email Content";
    public const string CONSTNAMEORDPROEMAILSUBJECT = "Order Processing Email Subject";
    public const string CONSTNAMEORDPROADMINEMAILCONTENT = "Order Processing Admin Email Content";
    public const string CONSTNAMEORDPROUSEREMAILCONTENT = "Order Processing User Email Content";
    public const string CONSTNAMEORDDELEMAILSUBJECT = "Order Delivering Email Subject";
    public const string CONSTNAMEORDDELADMINEMAILCONTENT = "Order Delivering Admin Email Content";
    public const string CONSTNAMEORDDELUSEREMAILCONTENT = "Order Delivering User Email Content";
    public const string CONSTNAMEORDDONEEMAILSUBJECT = "Order Done Email Subject";
    public const string CONSTNAMEORDDONEADMINEMAILCONTENT = "Order Done Admin Email Content";
    public const string CONSTNAMEORDDONEUSEREMAILCONTENT = "Order Done User Email Content";
    public const string CONSTNAMEFORGOTPWDEMAILSUBJECT = "Forgot Password Email Subject";
    public const string CONSTNAMEFORGOTPWDUSEREMAILCONTENT = "Forgot Password User Email Content";
    public const string CONSTNAMEORDCANCELEMAILSUBJECT = "Order Cancel Email Subject";
    public const string CONSTNAMEORDCANCELADMINEMAILCONTENT = "Order Cancel Admin Email Content";
    public const string CONSTNAMEORDCANCELUSEREMAILCONTENT = "Order Cancel User Email Content";
    public const string CONSTNAMEPGAUTHORIZATIONADMINEMAILSUBJECT = "Individual Login Admin Email Subject";
    public const string CONSTNAMEPGAUTHORIZATIONADMINEMAILCONTENT = "Individual Login Admin Email Content";
    public const string CONSTNAMEPGAUTHORIZATIONCOMPEMAILSUBJECT = "Individual Login Company Email Subject";
    public const string CONSTNAMEPGAUTHORIZATIONCOMPEMAILCONTENT = "Individual Login Company Email Content";

    public const string CONSTNAMEUSERMANUALCONTENT = "User Manual Content";
    public const string CONSTNAMEGROUPIMG = "Group Image";
    public const string CONSTNAMECURRENCY = "Currency";
    public const string CONSTNAMEDIMENSIONUNIT = "Dimension Unit";
    public const string CONSTNAMEWEIGHTUNIT = "Weight Unit";
    public const string CONSTNAMESHIPPINGUNIT = "Shipping Unit";
    public const string CONSTNAMESHIPPINGTYPE = "Shipping Type";
    public const string CONSTNAMEADMLANDING = "Admin Landing";
    public const string CONSTNAMEADMMOBILELANDING = "Admin Mobile Landing";
    public const string CONSTNAMEADMPROFILE = "Admin Profile";
    public const string CONSTNAMEADMMOBILEPROFILE = "Admin Mobile Profile";
    public const string CONSTNAMEQUICKCONTACTTEL = "Quick Contact Tel";
    public const string CONSTNAMEQUICKCONTACTEML = "Quick Contact Email";
    public const string CONSTNAMEFBPAGEURL = "Facebook Page URL";
    public const string CONSTNAMEFBLIKETITLE = "Facebook Like Title";
    public const string CONSTNAMEFBLIKETYPE = "Facebook Like Type";
    public const string CONSTNAMEFBLIKEURL = "Facebook Like URL";
    public const string CONSTNAMEFBLIKEIMAGE = "Facebook Like Image";
    public const string CONSTNAMEFBLIKESITENAME = "Facebook Like Website Name";
    public const string CONSTNAMEFBLIKEDESC = "Facebook Like Description";
    public const string CONSTNAMECOPYRIGHT = "Copyright";
    public const string CONSTNAMEEDITORSTYLE = "Editor Style";
    public const string CONSTNAMEGOOGLEANALYTIC = "Google Analytic";
    public const string CONSTNAMEGOOGLEANALYTICVIEWID = "Google Analytic View ID";
    public const string CONSTNAMEAUTOSCRIPTTAG = "Auto Script Tag";
    public const string CONSTNAMECONVERSIONCODEENQUIRY = "Conversion Code (Enquiry)";
    public const string CONSTNAMECONVERSIONCODEORDER = "Conversion Code (Order)";
    public const string CONSTNAMECONVERSIONCODEASK = "Conversion Code (Ask)";
    public const string CONSTNAMECONVERSIONCODESHARE = "Conversion Code (Share)";
    public const string CONSTNAMECONVERSIONCODECALL = "Conversion Code (Call)";

    public const string CONSTNAMERECAPTCHASITEKEY = "Recaptcha Site Key";
    public const string CONSTNAMERECAPTCHASECRETKEY = "Recaptcha Secret Key";
    public const string CONSTNAMEENABLEENQUIRYRECIPIENT = "Enable Enquiry Recipient";

    public const string CONSTNAMEGOOGLEPLUSURL = "Google+ URL";
    public const string CONSTNAMEGPLIKEURL = "Google+ Like URL";
    public const string CONSTNAMELINKEDINURL = "LinkedIn URL";
    public const string CONSTNAMELISHAREURL = "LinkedIn Share URL";
    public const string CONSTNAMEINSTAGRAMURL = "Instagram URL";
    public const string CONSTNAMETWITTERURL = "Twitter URL";
    public const string CONSTNAMEYOUTUBEURL = "Youtube URL";
    public const string CONSTNAMEFAVICONIMAGE = "FAVICON Image";
    public const string CONSTNAMEPRODUCTPAGE = "Product Page";
    public const string CONSTNAMEEVENTPAGE = "Event Page";
    public const string CONSTNAMEPRODDIMENSION = "Dimension Unit";
    public const string CONSTNAMEPRODINVENTORY = "Inventory";
    public const string CONSTNAMEPRODDNAME = "Display Name";
    public const string CONSTNAMEPRODCODE = "Product Code";
    public const string CONSTNAMEPRODSNAPSHOT = "Product Snapshot";
    public const string CONSTNAMEPRODPRICE = "Product Price";
    public const string CONSTNAMEPRODIMAGERATIO = "Image Aspect Ratio";
    public const string CONSTNAMERANDOMRELATEDPRODENABLED = "Random Related Product Enabled";
    public const string CONSTNAMELISTINGSIZE = "Listing Size (row)";
    public const string CONSTNAMESESSIONTIMEOUT = "Session Timeout";
    public const string CONSTNAMEFRIENDLYURL = "Friendly URL";
    public const string CONSTNAMEMOBILEVIEW = "Mobile View";
    public const string CONSTNAMEDELIVERYMSG = "Order Manager Delivery Message";
    public const string CONSTNAMEINVOICELOGO = "Invoice Logo";
    public const string CONSTNAMECOMPANYNAME = "Company Name";
    public const string CONSTNAMEROC = "ROC";
    public const string CONSTNAMECOMPANYADDRESS = "Company Address";
    public const string CONSTNAMECOMPANYTEL = "Company Telephone";
    public const string CONSTNAMECOMPANYEMAIL = "Company Email";
    public const string CONSTNAMECOMPANYWEBSITE = "Company Website";
    public const string CONSTNAMEINVOICEREMARKS = "Invoice Remarks";
    public const string CONSTNAMEINVOICEREMARKSPDF = "Invoice Remarks (PDF)";
    public const string CONSTNAMEINVOICESIGNATURE = "DO Signature";
    public const string CONSTNAMEINVOICESIGNATUREPDF = "DO Signature (PDF)";
    public const string CONSTNAMEPAYMENTTYPE = "Payment Type";
    public const string CONSTNAMESHIPPINGCOMPANY = "Shipping Company";
    public const string CONSTNAMEINVOICEPAYMENTEMAIL = "Send Payment Email";
    public const string CONSTNAMEINVOICEPROCESSINGEMAIL = "Send Processing Email";
    public const string CONSTNAMEINVOICEDELIVERYEMAIL = "Send Delivery Email";
    public const string CONSTNAMESMSSENDERURL = "SMS Sender URL";
    public const string CONSTNAMESMSSENDERAPIKEY = "SMS Sender API Key";
    public const string CONSTNAMESMSSENDERUSERNAME = "SMS Sender UserName";
    public const string CONSTNAMESMSSENDERPWD = "SMS Sender Password";
    public const string CONSTNAMESMSSENDERUSERID = "SMS Sender UserId";
    public const string CONSTNAMESMSDEFAULTRECIPIENT = "SMS Default Recipient";
    public const string CONSTNAMESENDENQUIRYSMS = "Send Enquiry SMS";
    public const string CONSTNAMEENQUIRYSMSCONTENT = "Enquiry SMS";
    public const string CONSTNAMESENDNEWORDERSMS = "Send New Order SMS";
    public const string CONSTNAMENEWORDERSMSCONTENT = "New Order SMS";
    public const string CONSTNAMESENDPAYMENTSMS = "Send Payment SMS";
    public const string CONSTNAMEPAYMENTSMSCONTENT = "Payment SMS";

    public const string CONSTNAMEYSHAMPERFLAG = "YSHamper Flag";
    public const string CONSTNAMEGOODYFLAG = "Goody Flag";
    public const string CONSTNAMENEXTFESTIVALNAME = "Next Festival";
    public const string CONSTNAMENEXTFESTIVALDATE = "Next Festival Date";
    public const string CONSTNAMECUTOFFDATE = "Cutoff Date";
    public const string CONSTNAMEGSTACTIVATION = "GST Activation";
    public const string CONSTNAMEGSTINCLUSIVEICON = "GST Inclusive Icon"; 	 	
    public const string CONSTNAMEGSTPERCENTAGE = "GST %"; 	 	
    public const string CONSTNAMEGSTPRODUCTWITHGST = "Product with GST"; 	 	
    public const string CONSTNAMEGSTSHOWITEMIZED = "Show Itemized";
    public const string CONSTNAMEGSTAPPLIED = "3";
    public const string CONSTNAMEGSTPARTIALLYAPPLIED = "2";
    public const string CONSTNAMEGSTSHOWITEMIZEDCHECKED = "1";
    public const string CONSTNAMEGOOGLEANALYTICS_CLIENTID = "Client ID";
    public const string CONSTNAMEINVENTORY = "Inventory";
    public const string CONSTNAMEUSEVOLUMETRICWEIGHT = "Use Volumetric Weight";
    public const string CONSTNAMEENQUIRYTITLE = "Enquiry Title";
    public const string CONSTNAMEENQUIRYDESC = "Enquiry Description";
    public const string CONSTNAMEENQUIRYRECAPTCHA = "Enquiry Recaptcha";
    public const string CONSTNAMERIGHTCLICKENABLE = "Stop Right Click";
    public const string CONSTNAMEMAXQTYPERPRODUCT = "Max. Quantity for Single Product";
    public const string CONSTNAMEMAXQTYPERPROMOPRODUCT = "Max. Quantity for Single Promotion Product";
    public const string CONSTNAMEPRODUCTDETAILDISPLAYTYPE = "Product Detail Display Type";
    public const string CONSTNAMEPRODUCTPRICERANGEFILTER = "Product Price Range Filter";
    public const string CONSTNAMEWEBSITENAME = "Website Name";

    // value
    public const string CONSTVALUEVOLUMETRICWEIGHT = "1";

    // Logo & Icon
    public const string CONSTNAMELOGOCOMPANYLOGO = "Company Logo";
    public const string CONSTNAMELOGORECICON = "Recommend Icon";
    public const string CONSTNAMELOGONEWICON = "New Icon";
    public const string CONSTNAMELOGOCALOGO = "CA Logo";
    public const string CONSTNAMELOGOWEBTEQLOGO = "Webteq Logo";

    public const string CONSTNAMETHUMBNAILWIDTH = "Thumbnail Width";
    public const string CONSTNAMETHUMBNAILHEIGHT = "Thumbnail Height";

    public const string CONSTNAMEASPECTRATIO = "aspect ratio";
    #region "Payment Gateway Name"
    // paypal
    public const string CONSTNAMEPAYPAL_USESANDBOX = "useSandBox";
    public const string CONSTNAMEPAYPAL_PAYPALSANDBOX = "paypalSandBox";
    public const string CONSTNAMEPAYPAL_PAYPAL = "paypal";
    public const string CONSTNAMEPAYPAL_CURRENCYCODE = "currency_code";
    public const string CONSTNAMEPAYPAL_CMD = "cmd";
    public const string CONSTNAMEPAYPAL_BUSINESS = "business";
    public const string CONSTNAMEPAYPAL_CANCELRETURN = "cancel_return";
    public const string CONSTNAMEPAYPAL_PDTTOKEN = "PDTToken";

    // molpay
    public const string CONSTNAMEMOLPAY_MOLPAY = "molpay";
    public const string CONSTNAMEMOLPAY_MERCHANTID = "merchantID";
    public const string CONSTNAMEMOLPAY_VERIFYKEY = "verifykey";
    public const string CONSTNAMEMOLPAY_CUR = "cur";
    public const string CONSTNAMEMOLPAY_COUNTRY = "country";
    public const string CONSTNAMEMOLPAY_LANGCODE = "langcode";
    public const string CONSTNAMEMOLPAY_RETURN = "return";
    public const string CONSTNAMEMOLPAY_RETURNPIN = "returnipn";

    // ipay88
    public const string CONSTNAMEIPAY88_IPAY88ENTRYPAGE = "iPay88EntryPage";
    public const string CONSTNAMEIPAY88_IPAY88ENQUIRYPAGE = "iPay88EnquiryPage";
    public const string CONSTNAMEIPAY88_RESPONSEURL = "responseUrl";
    public const string CONSTNAMEIPAY88_REQUESTURL = "requestUrl";
    public const string CONSTNAMEIPAY88_MERCHANTKEY = "merchantKey";
    public const string CONSTNAMEIPAY88_MERCHANTCODE = "merchantCode";
    public const string CONSTNAMEIPAY88_PAYMENTID = "paymentId";
    public const string CONSTNAMEIPAY88_CURRENCY = "currency";
    public const string CONSTNAMEIPAY88_PRODDESC = "prodDesc";
    public const string CONSTNAMEIPAY88_BACKENDURL = "backendUrl";

    // eGhl
    public const string CONSTNAMEEGHL_USETESTURL = "useTestUrl";
    public const string CONSTNAMEEGHL_TESTPAYMENTURL = "testPaymentUrl";
    public const string CONSTNAMEEGHL_PAYMENTURL = "paymentUrl";
    public const string CONSTNAMEEGHL_TRANSACTIONTYPE = "transactionType";
    public const string CONSTNAMEEGHL_PAYMENTMETHOD = "paymentMethod";
    public const string CONSTNAMEEGHL_MERCHANTID = "merchantID";
    public const string CONSTNAMEEGHL_MERCHANTPWD = "merchantPwd";
    public const string CONSTNAMEEGHL_MERCHANTNAME = "merchantName";
    public const string CONSTNAMEEGHL_RETURNURL = "returnUrl";
    public const string CONSTNAMEEGHL_CALLBACKURL = "callBackUrl";
    public const string CONSTNAMEEGHL_CURRENCYCODE = "currencyCode";
    public const string CONSTNAMEEGHL_PAGETIMEOUT = "pageTimeout";

    // PAYMENT GATEWAY NAME
    public const string CONSTNAMEPAYMENTGATEWAY_NAME = "name";
    public const string CONSTNAMEPAYMENTGATEWAY_VALUE = "value";
    public const string CONSTNAMEPAYMENTGATEWAY_ACTIVE = "active";

    // PAYMENT TYPE NAME VALUE
    public const string CONSTNAMEPAYMENTTYPENAME_PAYPAL = "1";
    public const string CONSTNAMEPAYMENTTYPENAME_BANKTRANSFER = "2";
    public const string CONSTNAMEPAYMENTTYPENAME_IPAY88 = "3";
    public const string CONSTNAMEPAYMENTTYPENAME_MOLPAY = "4";
    public const string CONSTNAMEPAYMENTTYPENAME_IPAY88ENETS = "5";
    public const string CONSTNAMEPAYMENTTYPENAME_EGHL = "6";
    #endregion
    #region "Order Slip Setup Name"
    public const string CONSTNAMEORDERSLIP_COMPANYLOGO = "Company Logo";
    public const string CONSTNAMEORDERSLIP_COMPANYNAME = "Company Name";
    public const string CONSTNAMEORDERSLIP_ADDR1 = "Address 1";
    public const string CONSTNAMEORDERSLIP_ADDR2 = "Address 2";
    public const string CONSTNAMEORDERSLIP_TEL = "Tel";
    public const string CONSTNAMEORDERSLIP_FAX = "Fax";
    public const string CONSTNAMEORDERSLIP_EMAIL = "Email";
    public const string CONSTNAMEORDERSLIP_WEBSITE = "Website";
    public const string CONSTNAMEORDERSLIP_COMPANYLREGNO = "Co Reg No";
    public const string CONSTNAMEORDERSLIP_GSTREGNO = "GST Reg No";
    public const string CONSTNAMEORDERSLIP_LINE1 = "Line 1";
    public const string CONSTNAMEORDERSLIP_LINE2 = "Line 2";
    public const string CONSTNAMEORDERSLIP_LINE3 = "Line 3";
    #endregion
    #region "Facebook Login Name"
    public const string CONSTNAMEFBLOGINAPIKEY = "API Key";
    public const string CONSTNAMEFBLOGINSECRET = "Secret";
    public const string CONSTNAMEFBLOGINACTIVE = "Active";
    public const string CONSTNAMEFBLOGINCALLBACK = "Callback";
    public const string CONSTNAMEFBLOGINSUFFIX = "Suffix";

    #endregion

    //water mark
    public const string CONSTNAMEWATERMARKTYPE = "Watermark Type";
    public const string CONSTNAMEWATERMARKOPACITY = "Watermark Opacity";
    public const string CONSTNAMEWATERMARKVALUE = "Watermark Value";

    // default value
    public const string CONSTDEFAULTVALUEPORT = "587";

    public const string CONSTTRUE = "True";
    public const string CONSTFALSE = "False";

    public const string CONSTDEFAULTTRUEVALUE = "1";
    public const string CONSTDEFAULTFALSEVALUE = "0";

    // website prefix
    public const string CONSTWEBSITEPREFIX = "http://";

    // facebook setttings
    public const string CONSTFACEBOOKLIKETYPE = "Website";

    // manage user view
    public const string CONSTMANAGEUSERVIEW_URL = "url";
    public const string CONSTMANAGEUSERVIEW_TEMPLATE = "template";
    public const string CONSTMANAGEUSERVIEW_PARAMS = "params";
    public const string CONSTSPLITTER_SEMICOLON = ";";
    public const string CONSTSPLITTER_COLON = ":";

    // LIST GROUP
    public const string CONSTLISTGRPBANKNAME = "BANK SELECTION";


    // Security Key
    public const string CONSTSECURITYKEY = "WEBTEQ";
    #endregion


    #region "properties"
    private int _id;
    private string _name;
    private string _value;
    private string _longValue;
    private string _group;
    private string _lang;
    private int _order;
    private string _operator;
    private string _unit;
    private string _tag;
    private int _active;
    private DateTime _creation;
    private int _createdBy;
    private DateTime _lastUpdate;
    private int _updatedBy;


    private string _prefix;
    private string _defaultTitle;
    private int _maxTotalPage;
    private string _defaultKeyword;
    private string _defaultDesc;
    #endregion


    #region "property methods"
    public int id
    {
        get { return _id; }
        set { _id = value; }
    }

    public string name
    {
        get {return _name;}
        set {_name = value;}
    }

    public string value
    {
        get {return _value;}
        set {_value = value;}
    }

    public string longValue
    {
        get { return _longValue; }
        set { _longValue = value; }
    }

    public string group
    {
        get {return _group;}
        set {_group = value;}
    }
    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }
    public int order
    {
        get {return _order;}
        set {_order = value;}
    }

    public string c_operator
    {
        get { return _operator; }
        set { _operator = value; }
    }

    public string unit
    {
        get { return _unit; }
        set { _unit = value; }
    }

    public string tag
    {
        get { return _tag; }
        set { _tag = value; }
    }

    public int active
    {
        get {return _active;}
        set {_active = value;}
    }

    public DateTime creation
    {
        get { return _creation; }
        set { _creation = value; }
    }

    public int createdBy
    {
        get { return _createdBy; }
        set { _createdBy = value; }
    }

    public DateTime lastUpdate
    {
        get { return _lastUpdate; }
        set { _lastUpdate = value; }
    }

    public int updatedBy
    {
        get { return _updatedBy; }
        set { _updatedBy = value; }
    }


    public string prefix
    {
        get { return getPrefix(); }
    }

    public string defaultTitle
    {
        get { return getDefaultTitle(); }
    }

    public int maxTotalPage
    {
        get { return getMaxTotalPage(); }
    }

    public string defaultKeyword
    {
        get { return getDefaultKeyword(); }
    }

    public string defaultDescription
    {
        get { return getDefaultDesc(); }
    }

    public string adminEmailContent
    {
        get { return getAdminEmailContent(); }
    }

    public string userEmailContent
    {
        get { return getUserEmailContent(); }
    }

    public string defaultGroupImg
    {
        get { return getDefaultGroupImg(); }
    }

    public string currency
    {
        get { return getCurrency(); }
    }

    public string currencyTag
    {
        get { return getCurrencyTag(); }
    }

    public string shippingUnit
    {
        get { return getShippingUnit(); }
    }

    public string shippingType
    {
        get { return getShippingType(); }
    }

    public string shippingUnitTag
    {
        get { return getShippingUnitTag(); }
    }

    public string dimensionUnit
    {
        get { return getDimensionUnit(); }
    }

    public string weightUnit
    {
        get { return getWeightUnit(); }
    }

    public string admLandingPage
    {
        get { return getAdminLandingPage(); }
    }

    public string admMobileLandingPage
    {
        get { return getAdminMobileLandingPage(); }
    }

    public string admProfilePage
    {
        get { return getAdminProfilePage(); }
    }

    public string admMobileProfilePage
    {
        get { return getAdminMobileProfilePage(); }
    }

    public string admSessionTimeout
    {
        get { return getSessionTimeout(); }
    }

    public string userManualContent
    {
        get { return getUserManualContent(); }
    }

    public string aspectRatio
    {
        get { return getAspectRatio(); }
    }

    public int thumbnailWidth
    {
        get
        {
            string strCONSTNAMETHUMBNAILWIDTH = getItemByGroupName(clsConfig.CONSTGROUPWEBSITE, clsConfig.CONSTNAMETHUMBNAILWIDTH) ;
            return Convert.ToInt32(strCONSTNAMETHUMBNAILWIDTH ==""? "0" : strCONSTNAMETHUMBNAILWIDTH);
        }
    }

    public int thumbnailHeight
    {
        get
        {
            string strCONSTNAMETHUMBNAILHEIGHT = getItemByGroupName(clsConfig.CONSTGROUPWEBSITE, clsConfig.CONSTNAMETHUMBNAILHEIGHT);
            return Convert.ToInt32(strCONSTNAMETHUMBNAILHEIGHT == "" ? "0" : strCONSTNAMETHUMBNAILHEIGHT);
        }
    }

    public string defaultFavIconImg
    {
        get { return getFavIconImg(); }
    }

    #endregion


    #region "Methods"
    public clsConfig()
	{
	}

    public int addItem(string strName, string strValue, string strGroup, int intActive, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            int intOrder = generateOrderNo(strGroup);

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_CONFIG (" +
                     "CONF_NAME," +
                     "CONF_VALUE," +
                     "CONF_GROUP," +
                     "CONF_ORDER," +
                     "CONF_ACTIVE," +
                     "CONF_CREATION," +
                     "CONF_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_CONF_VALUE", OdbcType.VarChar, 1000).Value = strValue;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_ORDER", OdbcType.Int, 9).Value = intOrder;
            cmd.Parameters.Add("p_CONF_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_CONF_CREATEDBY", OdbcType.BigInt, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";

                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                id = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addItem2(string strName, string strLongValue, string strGroup, int intActive, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            int intOrder = generateOrderNo(strGroup);

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_CONFIG (" +
                     "CONF_NAME," +
                     "CONF_LONGVALUE," +
                     "CONF_GROUP," +
                     "CONF_ORDER," +
                     "CONF_ACTIVE," +
                     "CONF_CREATION," +
                     "CONF_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_CONF_LONGVALUE", OdbcType.Text).Value = strLongValue;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_ORDER", OdbcType.Int, 9).Value = intOrder;
            cmd.Parameters.Add("p_CONF_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_CONF_CREATEDBY", OdbcType.BigInt, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";

                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                id = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int addItem(string strName, string strValue, string strGroup, string strOperator, string strUnit, int intActive, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            int intOrder = generateOrderNo(strGroup);

            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_CONFIG (" +
                     "CONF_NAME," +
                     "CONF_VALUE," +
                     "CONF_GROUP," +
                     "CONF_ORDER," +
                     "CONF_OPERATOR," +
                     "CONF_UNIT," +
                     "CONF_ACTIVE," +
                     "CONF_CREATION," +
                     "CONF_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_CONF_LONGVALUE", OdbcType.Text).Value = strValue;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_ORDER", OdbcType.Int, 9).Value = intOrder;
            cmd.Parameters.Add("p_CONF_OPERATOR", OdbcType.VarChar, 10).Value = strOperator;
            cmd.Parameters.Add("p_CONF_UNIT", OdbcType.VarChar, 10).Value = strUnit;
            cmd.Parameters.Add("p_CONF_ACTIVE", OdbcType.Int, 1).Value = intActive;
            cmd.Parameters.Add("p_CONF_CREATEDBY", OdbcType.BigInt, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();

                string strSql2;
                OdbcCommand cmd2 = new OdbcCommand();

                strSql2 = "SELECT LAST_INSERT_ID()";

                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;

                id = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int generateOrderNo(string strGroup)
    {
        int orderNo = 0;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT CONF_ORDER FROM TB_CONFIG WHERE CONF_GROUP = ?" +
                     " ORDER BY CONF_ORDER DESC LIMIT 1";

            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            orderNo = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return orderNo + 1;
    }

    public Boolean extractItemByNameGroup(string strName, string strGroup, int intActive)
    {
        Boolean boolFound = false;
        
        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CONF_ID, A.CONF_NAME, A.CONF_VALUE, A.CONF_LONGVALUE, A.CONF_GROUP, A.CONF_ORDER, A.CONF_OPERATOR, A.CONF_UNIT, A.CONF_TAG" +
                     " FROM TB_CONFIG A" +
                     " WHERE A.CONF_NAME = ?";

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;

            if (!string.IsNullOrEmpty(strGroup))
            {
                strSql += " AND A.CONF_GROUP = ?";
                cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            }

            if (intActive != 0)
            {
                strSql += " AND A.CONF_ACTIVE = ?";
                cmd.Parameters.Add("p_CONF_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                id = int.Parse(dataSet.Tables[0].Rows[0]["CONF_ID"].ToString());
                name = dataSet.Tables[0].Rows[0]["CONF_NAME"].ToString();
                value = dataSet.Tables[0].Rows[0]["CONF_VALUE"].ToString();
                longValue = dataSet.Tables[0].Rows[0]["CONF_LONGVALUE"].ToString();
                group = dataSet.Tables[0].Rows[0]["CONF_GROUP"].ToString();
                order = Convert.ToInt16(dataSet.Tables[0].Rows[0]["CONF_ORDER"]);
                c_operator = dataSet.Tables[0].Rows[0]["CONF_OPERATOR"].ToString();
                unit = dataSet.Tables[0].Rows[0]["CONF_UNIT"].ToString();
                tag = dataSet.Tables[0].Rows[0]["CONF_TAG"].ToString();
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    
    public DataSet getItemList(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CONF_ID, A.CONF_NAME, A.CONF_VALUE, A.CONF_LONGVALUE, A.CONF_GROUP, A.CONF_LANG, A.CONF_ORDER, A.CONF_OPERATOR, A.CONF_UNIT, A.CONF_TAG, A.CONF_ACTIVE," +
                     " A.CONF_CREATION, A.CONF_CREATEDBY, A.CONF_LASTUPDATE, A.CONF_UPDATEDBY" +
                     " FROM TB_CONFIG A";

            if (intActive != 0)
            {
                strSql += " WHERE A.CONF_ACTIVE = ?";
                cmd.Parameters.Add("p_CONF_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public DataSet getItemsByGroup(string strGroup, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CONF_ID, A.CONF_NAME, A.CONF_VALUE, A.CONF_LONGVALUE, A.CONF_GROUP, A.CONF_ORDER, A.CONF_OPERATOR, A.CONF_UNIT, A.CONF_TAG, A.CONF_ACTIVE," +
                     " A.CONF_CREATION, A.CONF_CREATEDBY, A.CONF_LASTUPDATE, A.CONF_UPDATEDBY" +
                     " FROM TB_CONFIG A" +
                     " WHERE A.CONF_GROUP = ?";

            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;

            if (intActive != 0)
            {
                strSql += " AND A.CONF_ACTIVE = ?";
                cmd.Parameters.Add("p_CONF_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean isItemExist(string strName, string strGroup)
    {
        Boolean boolValid = false;

        try
        {
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT COUNT(*) FROM TB_CONFIG" +
                     " WHERE CONF_NAME = ?" +
                     " AND CONF_GROUP = ?";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount == 1) { boolValid = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolValid;
    }
    public Boolean isExactSameSetData(string strName, string strValue, string strGroup)
    {
        return isExactSameSetData(strName, strValue, strGroup, null);
    }
    public Boolean isExactSameSetData(string strName, string strValue, string strGroup, string strLang)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            List<string> columns = new List<string>()
            {
                "BINARY CONF_VALUE = ?", "BINARY CONF_GROUP = ?", "CONF_NAME = ?"
            };

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_CONF_VALUE", OdbcType.VarChar, 1000).Value = strValue;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;

            if(strLang != null)
            {
                columns.Add("CONF_LANG = ?");
                cmd.Parameters.Add("p_CONF_LANG", OdbcType.VarChar, 250).Value = strLang;
            }


            strSql = "SELECT COUNT(*) FROM TB_CONFIG" +
                     " WHERE " + string.Join(" AND ", columns.ToArray());

            

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public Boolean isExactSameSetData()
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            List<string> columns = new List<string>()
            {
                "BINARY CONF_GROUP = ?", "CONF_NAME = ?"
            };

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = name;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = group;

            if (lang != null)
            {
                columns.Add("CONF_LANG = ?");
                cmd.Parameters.Add("p_CONF_LANG", OdbcType.VarChar).Value = lang;
            }
            if (value != null)
            {
                columns.Add("BINARY CONF_VALUE = ?");
                cmd.Parameters.Add("p_CONF_LANG", OdbcType.VarChar).Value = value;
            }
            if (longValue != null)
            {
                columns.Add("BINARY CONF_LONGVALUE = ?");
                cmd.Parameters.Add("p_CONF_LANG", OdbcType.Text).Value = longValue;
            }

            strSql = "SELECT COUNT(*) FROM TB_CONFIG" +
                     " WHERE " + string.Join(" AND ", columns.ToArray());



            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public Boolean isExactSameSetData2(string strName, string strLongValue, string strGroup)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_CONFIG" +
                     " WHERE BINARY CONF_NAME = ?" +
                     " AND BINARY CONF_LONGVALUE = ?" +
                     " AND BINARY CONF_GROUP = ?";

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_CONF_LONGVALUE", OdbcType.Text).Value = strLongValue;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public Boolean isExactSameSetData3(string strName, string strValue, string strGroup, int intOrder)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_CONFIG" +
                     " WHERE CONF_NAME = ?" +
                     " AND BINARY CONF_VALUE = ?" +
                     " AND BINARY CONF_GROUP = ?" +
                     " AND CONF_ORDER = ?";

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_CONF_VALUE", OdbcType.VarChar, 1000).Value = strValue;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_ORDER", OdbcType.Int, 1).Value = intOrder;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public Boolean isExactSameSetData(string strName, string strValue, string strGroup, string strOperator, string strUnit)
    {
        Boolean boolSame = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*) FROM TB_CONFIG" +
                     " WHERE BINARY CONF_NAME = ?" +
                     " AND BINARY CONF_VALUE = ?" +
                     " AND BINARY CONF_GROUP = ?" +
                     " AND CONF_OPERATOR = ?" +
                     " AND BINARY CONF_UNIT = ?";

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_CONF_VALUE", OdbcType.VarChar, 1000).Value = strValue;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_OPERATOR", OdbcType.VarChar, 10).Value = strOperator;
            cmd.Parameters.Add("p_CONF_UNIT", OdbcType.VarChar, 10).Value = strUnit;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = Convert.ToInt16(cmd.ExecuteScalar());

            if (intCount >= 1) { boolSame = true; }

            cmd.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return boolSame;
    }

    public int updateItem(string strGroup, string strName, string strValue, int intUpdatedBy)
    {
        return updateItem(strGroup, strName, strValue, null, intUpdatedBy);
    }

    public int updateItem(string strGroup, string strName, string strValue,string strLang, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<string> columns = new List<string>()
            {
                "CONF_VALUE = ?", "CONF_UPDATEDBY = ?", 
            };

            cmd.Parameters.Add("p_CONF_VALUE", OdbcType.VarChar, 1000).Value = strValue;
            cmd.Parameters.Add("p_CONF_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;

            strSql = "UPDATE TB_CONFIG SET " + string.Join(",", columns.ToArray()) + "," + 
                     " CONF_LASTUPDATE = SYSDATE() " +
                     " WHERE CONF_GROUP = ?" +
                     " AND CONF_NAME = ?";


            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;

            if (strLang != null)
            {
                strSql += " AND CONF_LANG = ?";
                cmd.Parameters.Add("p_CONF_LANG", OdbcType.VarChar, 250).Value = strLang;
            }

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;



            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int updateItem()
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<string> columns = new List<string>()
            {
                 "CONF_UPDATEDBY = ?",
            };
            cmd.Parameters.Add("p_CONF_UPDATEDBY", OdbcType.BigInt, 9).Value = updatedBy;

            if (value != null)
            {
                columns.Add("CONF_VALUE = ?");
                cmd.Parameters.Add("p_CONF_VALUE", OdbcType.VarChar, 1000).Value = value;
            }
            if (longValue != null)
            {
                columns.Add("CONF_LONGVALUE = ?");
                cmd.Parameters.Add("p_CONF_VALUE", OdbcType.Text).Value = longValue;
            }


            strSql = "UPDATE TB_CONFIG SET " + string.Join(",", columns.ToArray()) + "," +
                     " CONF_LASTUPDATE = SYSDATE() " +
                     " WHERE CONF_GROUP = ?" +
                     " AND CONF_NAME = ?";


            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = group;
            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = name;

            if (lang != null)
            {
                strSql += " AND CONF_LANG = ?";
                cmd.Parameters.Add("p_CONF_LANG", OdbcType.VarChar, 250).Value = lang;
            }

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;



            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int updateItemActive(string strGroup, string strName, int intActive, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_CONFIG SET" +
                     " CONF_ACTIVE = ?," +
                     " CONF_LASTUPDATE = SYSDATE()," +
                     " CONF_UPDATEDBY = ?" +
                     " WHERE CONF_GROUP = ?" +
                     " AND CONF_NAME = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CONF_ACTIVE", OdbcType.VarChar, 1000).Value = intActive;
            cmd.Parameters.Add("p_CONF_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int updateItem2(string strGroup, string strName, string strLongValue, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_CONFIG SET" +
                     " CONF_LONGVALUE = ?," +
                     " CONF_LASTUPDATE = SYSDATE()," +
                     " CONF_UPDATEDBY = ?" +
                     " WHERE CONF_GROUP = ?" +
                     " AND CONF_NAME = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CONF_LONGVALUE", OdbcType.Text).Value = strLongValue;
            cmd.Parameters.Add("p_CONF_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateItemById(string strGroup, string strName, string strValue, string strOperator, string strUnit, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_CONFIG SET" +
                     " CONF_VALUE = ?," +
                     " CONF_OPERATOR = ?," +
                     " CONF_UNIT = ?," +
                     " CONF_LASTUPDATE = SYSDATE()," +
                     " CONF_UPDATEDBY = ?" +
                     " WHERE CONF_GROUP = ?" +
                     " AND CONF_NAME = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CONF_VALUE", OdbcType.VarChar, 1000).Value = strValue;
            cmd.Parameters.Add("p_CONF_OPERATOR", OdbcType.VarChar, 10).Value = strOperator;
            cmd.Parameters.Add("p_CONF_UNIT", OdbcType.VarChar, 10).Value = strUnit;
            cmd.Parameters.Add("p_CONF_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateItemById2(int intId, string strGroup, string strName, string strValue, int intOrder, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_CONFIG SET" +
                     " CONF_NAME = ?," +
                     " CONF_VALUE = ?," +
                     " CONF_ORDER = ?," +
                     " CONF_LASTUPDATE = SYSDATE()," +
                     " CONF_UPDATEDBY = ?" +
                     " WHERE CONF_GROUP = ?" +
                     " AND CONF_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;
            cmd.Parameters.Add("p_CONF_VALUE", OdbcType.VarChar, 1000).Value = strValue;
            cmd.Parameters.Add("p_CONF_ORDER", OdbcType.Int, 9).Value = intOrder;
            cmd.Parameters.Add("p_CONF_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_ID", OdbcType.BigInt, 9).Value = intId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public int updateUserManualById(string strGroup, string strName, string strLongValue, int intUpdatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_CONFIG SET" +
                     " CONF_LONGVALUE = ?," +
                     " CONF_LASTUPDATE = SYSDATE()," +
                     " CONF_UPDATEDBY = ?" +
                     " WHERE CONF_GROUP = ?" +
                     " AND CONF_NAME = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CONF_LONGVALUE", OdbcType.VarChar, 1000).Value = strLongValue;
            cmd.Parameters.Add("p_CONF_UPDATEDBY", OdbcType.BigInt, 9).Value = intUpdatedBy;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public Boolean extractConfigByConfigName(string strConfigName, int intConfigActive)
    {
        Boolean boolFound = false;
        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM TB_CONFIG WHERE CONF_NAME = ? ";

            cmd.Parameters.Add("CONF_NAME", OdbcType.VarChar, 20).Value = strConfigName;

            if (intConfigActive != 0)
            {
                strSql += " AND CONF_ACTIVE = ?";
                cmd.Parameters.Add("CONF_ACTIVE", OdbcType.Int, 1).Value = intConfigActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                id = int.Parse(dataSet.Tables[0].Rows[0]["CONF_ID"].ToString());
                name = dataSet.Tables[0].Rows[0]["CONF_NAME"].ToString();
                value = dataSet.Tables[0].Rows[0]["CONF_VALUE"].ToString();
                group = dataSet.Tables[0].Rows[0]["CONF_GROUP"].ToString();
                order = Convert.ToInt16(dataSet.Tables[0].Rows[0]["CONF_ORDER"]);
                active = Convert.ToInt16(dataSet.Tables[0].Rows[0]["CONF_ACTIVE"]);
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    protected string getItemByGroupName(string strGroup, string strName)
    {
        string strValue = "";
        extractItemByNameGroup(strName, strGroup, 1);
        strValue = value;
        return strValue;
    }

    protected string getItemByGroupName2(string strGroup, string strName)
    {
        string strValue = "";
        extractItemByNameGroup(strName, strGroup, 1);
        strValue = longValue;
        return strValue;
    }

    protected string getTagByGroupName(string strGroup, string strName)
    {
        string strTag = "";
        extractItemByNameGroup(strName, strGroup, 1);
        strTag = tag;
        return strTag;
    }

    #region "Page Details"
    protected string getPrefix()
    {
        string strPrefix = getItemByGroupName(clsConfig.CONSTGROUPPAGEDETAILS, clsConfig.CONSTNAMEPAGETITLEPREFIX);

        return strPrefix;
    }

    protected string getDefaultTitle()
    {
        string strTitle = getItemByGroupName(clsConfig.CONSTGROUPPAGEDETAILS, clsConfig.CONSTNAMEPAGETITLE);

        return strTitle;
    }

    protected int getMaxTotalPage()
    {
        string strMaxTotalPage = getItemByGroupName(clsConfig.CONSTGROUPPAGEDETAILS, clsConfig.CONSTNAMEMAXTOTALPAGE);
        int intMaxTotalPage = !string.IsNullOrEmpty(strMaxTotalPage) ? Convert.ToInt16(strMaxTotalPage) : 0;

        return intMaxTotalPage;
    }

    protected string getDefaultKeyword()
    {
        string strKeyword = getItemByGroupName(clsConfig.CONSTGROUPPAGEDETAILS, clsConfig.CONSTNAMEKEYWORD);

        return strKeyword;
    }

    protected string getDefaultDesc()
    {
        string strDesc = getItemByGroupName(clsConfig.CONSTGROUPPAGEDETAILS, clsConfig.CONSTNAMEDESCRIPTION);

        return strDesc;
    }

    protected string getFavIconImg()
    {
        string strFavIconImg = getItemByGroupName(CONSTGROUPFAVICON, CONSTNAMEFAVICONIMAGE);
        return strFavIconImg;
    }

    public string[,] getDeliveryWeek()
    {
        if(extractItemByNameGroup("Delivery Week", "Shipping", 1))
        {
            string[] array = longValue.Split('|');
            string[,] array2D = new string[7, 2];

            for (int i = 0; i < array.Length; i++)
            {
                string weekName = "";
                switch (i)
                {
                    case 0: weekName = "Sunday"; break;
                    case 1: weekName = "Monday"; break;
                    case 2: weekName = "Tuesday"; break;
                    case 3: weekName = "Wednesday"; break;
                    case 4: weekName = "Thursday"; break;
                    case 5: weekName = "Friday"; break;
                    case 6: weekName = "Saturday"; break;
                }

                array2D[i, 0] = weekName;
                array2D[i, 1] = array[i];
            }
            return array2D;
        }
        return null;
    }
    public string[] getDeliveryTime()
    {
        if (extractItemByNameGroup("Delivery Time", "Shipping", 1))
        {
            if (!string.IsNullOrEmpty(longValue))
            {
                return longValue.Split('|');
            }
        }
        return null;
    }

    public bool saveDeliveryWeek(string[] week, int intAdmID)
    {
        // [0] = active, [1] = sunday ... [7] = monday
        if(week.Length == 8)
        {
            string confValue = week[0];
            string confLongValue = week.Select((value, index) => new { value, index })
                                        .Where(x => x.index > 0)
                                        .Select(x => x.value)
                                        .Aggregate((current, next) => current + "|" + next);
            string confName = "Delivery Week";
            string confGroup = clsConfig.CONSTGROUPSHIPPING;

            this.value = confValue;
            this.longValue = confLongValue;
            this.group = confGroup;
            this.name = confName;
            this.updatedBy = intAdmID;

            if (!isExactSameSetData())
            {
                return updateItem() == 1;
            }
        }
        return false;
    }
    public bool saveDeliveryTime(string times,string timesActive, int intAdmID)
    {
        string confValue = timesActive;
        string confLongValue = times;
        string confName = "Delivery Time";
        string confGroup = clsConfig.CONSTGROUPSHIPPING;

        this.value = confValue;
        this.longValue = confLongValue;
        this.group = confGroup;
        this.name = confName;
        this.updatedBy = intAdmID;

        if (!isExactSameSetData())
        {
            return updateItem() == 1;
        }

        return false;
    }
    #endregion

    #region "Email Content"
    protected string getAdminEmailContent()
    {
        string strAdminEmailContent = getItemByGroupName(clsConfig.CONSTGROUPEMAILCONTENT, clsConfig.CONSTNAMEADMINEMAILCONTENT);

        return strAdminEmailContent;
    }

    protected string getUserEmailContent()
    {
        string strUserEmailContent = getItemByGroupName(clsConfig.CONSTGROUPEMAILCONTENT, clsConfig.CONSTNAMEUSEREMAILCONTENT);

        return strUserEmailContent;
    }
    #endregion

    #region "Group Default Image"
    protected string getDefaultGroupImg()
    {
        string strDefaultGroupImg = getItemByGroupName("", CONSTNAMEGROUPIMG);

        return strDefaultGroupImg;
    }
    #endregion

    #region "Default Settings"
    protected string getCurrency()
    {
        string strCurrency = getItemByGroupName(clsConfig.CONSTGROUPDEFAULT, clsConfig.CONSTNAMECURRENCY);

        return strCurrency;
    }

    protected string getCurrencyTag()
    {
        string strTag = getTagByGroupName(clsConfig.CONSTGROUPDEFAULT, clsConfig.CONSTNAMECURRENCY);

        return strTag;
    }
    #endregion

    #region "Shipping"
    protected string getShippingUnit()
    {
        string strUnit = getItemByGroupName(clsConfig.CONSTGROUPSHIPPING, clsConfig.CONSTNAMESHIPPINGUNIT);

        return strUnit;
    }

    protected string getShippingUnitTag()
    {
        string strTag = getTagByGroupName(clsConfig.CONSTGROUPSHIPPING, clsConfig.CONSTNAMESHIPPINGUNIT);

        return strTag;
    }

    protected string getShippingType()
    {
        string strType = getItemByGroupName(clsConfig.CONSTGROUPSHIPPING, clsConfig.CONSTNAMESHIPPINGTYPE);

        return strType;
    }
    #endregion

    #region "Shipping Company/Shipping Method"
    public Boolean isShippingSettingExist(string strName, string strGroup)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*)" + 
                     " FROM TB_CONFIG" +
                     " WHERE CONF_GROUP = ?" +
                     " AND CONF_NAME = ?";

            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = int.Parse(cmd.ExecuteScalar().ToString());

            boolFound = intCount > 0 ? true : false;
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int addShippingSetting(string strShipName, string strShipValue, int intShipOrder, string strShipGroup, int intCreatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_CONFIG (" +
                     "CONF_NAME, " +
                     "CONF_VALUE, " +
                     "CONF_ORDER, " +
                     "CONF_GROUP, " +
                     "CONF_CREATION, " +
                     "CONF_CREATEDBY" +
                     ") VALUES " +
                     "(?,?,?,?,SYSDATE(),?)";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strShipName;
            cmd.Parameters.Add("p_CONF_VALUE", OdbcType.VarChar, 1000).Value = strShipValue;
            cmd.Parameters.Add("p_CONF_ORDER", OdbcType.Int, 9).Value = intShipOrder;
            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strShipGroup;
            cmd.Parameters.Add("p_CONF_CREATEDBY", OdbcType.BigInt, 9).Value = intCreatedBy;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public DataSet getShippingCompanyByGroup(string strShipCompGroup, int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CONF_ID, A.CONF_NAME, A.CONF_VALUE, A.CONF_LONGVALUE, A.CONF_GROUP, A.CONF_ORDER, " +
                     "A.CONF_OPERATOR, A.CONF_UNIT, A.CONF_TAG, A.CONF_ACTIVE, A.CONF_CREATION, A.CONF_CREATEDBY, " +
                     "A.CONF_LASTUPDATE, A.CONF_UPDATEDBY " +
                     "FROM TB_CONFIG A " +
                     "WHERE A.CONF_GROUP = ?";

            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strShipCompGroup;

            if (intActive != 0)
            {
                strSql += " AND A.CONF_ACTIVE = ?";
                cmd.Parameters.Add("p_CONF_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean extractConfigByConfigId(int intConfId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet ds = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT A.CONF_ID, A.CONF_NAME, A.CONF_VALUE, A.CONF_LONGVALUE, A.CONF_GROUP," +
                     " A.CONF_ORDER, A.CONF_OPERATOR, A.CONF_UNIT, A.CONF_TAG, A.CONF_ACTIVE, A.CONF_CREATION," +
                     " A.CONF_CREATEDBY, A.CONF_LASTUPDATE, A.CONF_UPDATEDBY" +
                     " FROM TB_CONFIG A" +
                     " WHERE A.CONF_ID = ?";

            cmd.Parameters.Add("p_CONF_ID",OdbcType.BigInt, 9).Value = intConfId;

            if (intActive != 0)
            {
                strSql += " AND A.CONF_ACTIVE = ?";
                cmd.Parameters.Add("p_CONF_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            da.SelectCommand = cmd;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                id = int.Parse(ds.Tables[0].Rows[0]["CONF_ID"].ToString());
                name = ds.Tables[0].Rows[0]["CONF_NAME"].ToString();
                value = ds.Tables[0].Rows[0]["CONF_VALUE"].ToString();
                longValue = ds.Tables[0].Rows[0]["CONF_LONGVALUE"].ToString();
                group = ds.Tables[0].Rows[0]["CONF_GROUP"].ToString();
                order = int.Parse(ds.Tables[0].Rows[0]["CONF_ORDER"].ToString());
                c_operator = ds.Tables[0].Rows[0]["CONF_OPERATOR"].ToString();
                unit = ds.Tables[0].Rows[0]["CONF_UNIT"].ToString();
                tag = ds.Tables[0].Rows[0]["CONF_TAG"].ToString();
                active = int.Parse(ds.Tables[0].Rows[0]["CONF_ACTIVE"].ToString());
                creation = ds.Tables[0].Rows[0]["CONF_CREATION"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["CONF_CREATION"].ToString()) : DateTime.MaxValue;
                createdBy = int.Parse(ds.Tables[0].Rows[0]["CONF_CREATEDBY"].ToString());
                lastUpdate = ds.Tables[0].Rows[0]["CONF_LASTUPDATE"] != DBNull.Value ? DateTime.Parse(ds.Tables[0].Rows[0]["CONF_LASTUPDATE"].ToString()) : DateTime.MaxValue;
                updatedBy = ds.Tables[0].Rows[0]["CONF_UPDATEDBY"] != DBNull.Value ? int.Parse(ds.Tables[0].Rows[0]["CONF_UPDATEDBY"].ToString()) : 0;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }

    public int deleteShippingSettingById(int intShipId)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "DELETE FROM TB_CONFIG" +
                     " WHERE CONF_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_CONF_ID", OdbcType.BigInt, 9).Value = intShipId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion

    #region "Product"
    protected string getDimensionUnit()
    {
        string strUnit = getItemByGroupName(clsConfig.CONSTGROUPPRODUCT, clsConfig.CONSTNAMEDIMENSIONUNIT);

        return strUnit;
    }

    protected string getWeightUnit()
    {
        string strUnit = getItemByGroupName(clsConfig.CONSTGROUPPRODUCT, clsConfig.CONSTNAMEWEIGHTUNIT);

        return strUnit;
    }
    #endregion

    #region "User Manual"
    protected string getUserManualContent()
    {
        string strAdminUserManualContent = getItemByGroupName2(clsConfig.CONSTGROUPUSERMANUAL, clsConfig.CONSTNAMEUSERMANUALCONTENT);

        return strAdminUserManualContent;
    }
    #endregion

    #region "Crop Image"
    protected string getAspectRatio()
    {
        string strAspectRatio = getItemByGroupName(clsConfig.CONSTGROUPCROPIMAGESETTINGS, clsConfig.CONSTNAMEASPECTRATIO);

        return strAspectRatio;
    }
    
    #endregion

    #region "Others"
    protected string getAdminLandingPage()
    {
        string strAdminLandingPage = getItemByGroupName("", CONSTNAMEADMLANDING);

        return strAdminLandingPage;
    }

    protected string getAdminMobileLandingPage()
    {
        string strAdminMobileLandingPage = getItemByGroupName("", CONSTNAMEADMMOBILELANDING);

        return strAdminMobileLandingPage;
    }

    protected string getAdminProfilePage()
    {
        string strAdminProfilePage = getItemByGroupName("", CONSTNAMEADMPROFILE);

        return strAdminProfilePage;
    }

    protected string getAdminMobileProfilePage()
    {
        string strAdminMobileProfilePage = getItemByGroupName("", CONSTNAMEADMMOBILEPROFILE);

        return strAdminMobileProfilePage;
    }

    protected string getSessionTimeout()
    {
        string strAdminMobileProfilePage = getItemByGroupName(CONSTGROUPOTHERSETTINGS, CONSTNAMESESSIONTIMEOUT);

        return strAdminMobileProfilePage;
    }
    //Customization for YSHamper Project - start    
    public Boolean isYsHamperFlagExist(string strName, string strGroup)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*)" +
                     " FROM TB_CONFIG" +
                     " WHERE CONF_GROUP = ?" +
                     " AND CONF_NAME = ?";

            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = int.Parse(cmd.ExecuteScalar().ToString());

            boolFound = intCount > 0 ? true : false;
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    //Customization for YSHamper Project - end

    //Customization for Goody Project - start    
    public Boolean isGoodyFlagExist(string strName, string strGroup)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();

            strSql = "SELECT COUNT(*)" +
                     " FROM TB_CONFIG" +
                     " WHERE CONF_GROUP = ?" +
                     " AND CONF_NAME = ?";

            cmd.Parameters.Add("p_CONF_GROUP", OdbcType.VarChar, 250).Value = strGroup;
            cmd.Parameters.Add("p_CONF_NAME", OdbcType.VarChar, 250).Value = strName;

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;

            int intCount = int.Parse(cmd.ExecuteScalar().ToString());

            boolFound = intCount > 0 ? true : false;
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    //Customization for YSHamper Project - end

    #endregion

    #endregion
}
