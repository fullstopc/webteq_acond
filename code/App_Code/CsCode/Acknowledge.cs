﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public class Acknowledge
{
    public Panel container { get; set; }
    public Literal msg { get; set; }
    public Literal title { get; set; }
    public Page page { get; set; }

    public Acknowledge(Page page)
    {
        this.page = page;

    }

    public void init()
    {

        if (!page.IsPostBack)
        {
            if (HttpContext.Current.Session[clsKey.SESSION_NOCHANGE] != null)
            {
                container.Visible = true;
                container.CssClass = container.CssClass + " info";
                msg.Text = HttpContext.Current.Session[clsKey.SESSION_MSG].ToString();
            }
            else if (HttpContext.Current.Session[clsKey.SESSION_ADDED] != null)
            {
                container.Visible = true;
                container.CssClass = container.CssClass + " success";
                msg.Text = HttpContext.Current.Session[clsKey.SESSION_MSG].ToString();
            }
            else if (HttpContext.Current.Session[clsKey.SESSION_EDITED] != null)
            {
                container.Visible = true;
                container.CssClass = container.CssClass + " success";
                msg.Text = HttpContext.Current.Session[clsKey.SESSION_MSG].ToString();
            }
            else if (HttpContext.Current.Session[clsKey.SESSION_DELETED] != null)
            {
                container.Visible = true;
                container.CssClass = container.CssClass + " success";
                msg.Text = HttpContext.Current.Session[clsKey.SESSION_MSG].ToString();
            }
            else if (HttpContext.Current.Session[clsKey.SESSION_ERROR] != null)
            {
                container.Visible = true;
                container.CssClass = container.CssClass + " error";
                msg.Text = HttpContext.Current.Session[clsKey.SESSION_MSG].ToString();
            }


        }

        HttpContext.Current.Session[clsKey.SESSION_NOCHANGE] = null;
        HttpContext.Current.Session[clsKey.SESSION_ADDED] = null;
        HttpContext.Current.Session[clsKey.SESSION_EDITED] = null;
        HttpContext.Current.Session[clsKey.SESSION_DELETED] = null;
        HttpContext.Current.Session[clsKey.SESSION_ERROR] = null;
        HttpContext.Current.Session[clsKey.SESSION_MSG] = null;
    }


}