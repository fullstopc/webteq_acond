﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsEnquiry
/// </summary>
public class clsEnquiry : dbconnBase
{
    #region "Properties"
    #region "Enquiry Details"
    private int _enqId;
    private int _enqSalutation;
    private int _enqActive;
    private int _enqResendCount;
    private string _enqEmailRecipient;
    private string _enqURL;
    private string _enqName;
    private string _enqCompanyName;
    private string _enqContactNo;
    private string _enqEmailSender;
    private string _enqSubject;
    private string _enqMessage;
    private string _engGeoLocation;
    private string _enqRemark1;
    private string _enqRemark2;
    private string _enqRemark3;
    private string _enqRemark4;
    private string _enqRemark5;
    private DateTime _enqCreation;
    private DateTime _enqLastUpDate;
    private DateTime _enqLastResend;
    #endregion
    #endregion

    #region "Property Methods"
    public int enqId
    {
        get { return _enqId; }
        set { _enqId = value; }
    }

    public string enqEmailRecipient
    {
        get { return _enqEmailRecipient; }
        set { _enqEmailRecipient = value; }
    }

    public string enqURL
    {
        get { return _enqURL; }
        set { _enqURL = value; }
    }

    public int enqSalutation
    {
        get { return _enqSalutation; }
        set { _enqSalutation = value; }
    }

    public string enqName
    {
        get { return _enqName; }
        set { _enqName = value; }
    }

    public string enqCompanyName
    {
        get { return _enqCompanyName; }
        set { _enqCompanyName = value; }
    }

    public string enqContactNo
    {
        get { return _enqContactNo; }
        set { _enqContactNo = value; }
    }

    public string enqEmailSender
    {
        get { return _enqEmailSender; }
        set { _enqEmailSender = value; }
    }

    public string enqSubject
    {
        get { return _enqSubject; }
        set { _enqSubject = value; }
    }

    public string enqMessage
    {
        get { return _enqMessage; }
        set { _enqMessage = value; }
    }

    public string engGeoLocation
    {
        get { return _engGeoLocation; }
        set { _engGeoLocation = value; }
    }

    public string enqRemark1
    {
        get { return _enqRemark1; }
        set { _enqRemark1 = value; }
    }

    public string enqRemark2
    {
        get { return _enqRemark2; }
        set { _enqRemark2 = value; }
    }

    public string enqRemark3
    {
        get { return _enqRemark3; }
        set { _enqRemark3 = value; }
    }

    public string enqRemark4
    {
        get { return _enqRemark4; }
        set { _enqRemark4 = value; }
    }

    public string enqRemark5
    {
        get { return _enqRemark5; }
        set { _enqRemark5 = value; }
    }

    public int enqActive
    {
        get { return _enqActive; }
        set { _enqActive = value; }
    }
    public int enqResendCount
    {
        get { return _enqResendCount; }
        set { _enqResendCount = value; }
    }
    public DateTime enqCreation
    {
        get { return _enqCreation; }
        set { _enqCreation = value; }
    }

    public DateTime enqLastUpDate
    {
        get { return _enqLastUpDate; }
        set { _enqLastUpDate = value; }
    }

    public DateTime enqLastResend
    {
        get { return _enqLastResend; }
        set { _enqLastResend = value; }
    }
    #endregion

    #region "Request"
    public int addEnquiry(string strEnqEmailRecipient, string strEnqURL, int intEnqSalutation, string strEnqName, string strEnqComName, string strEnqContactNo, string strEnqEmailSender, string strEnqSubject, string strEnqMessage)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "INSERT INTO TB_ENQUIRY (" +
                     "ENQ_EMAILRECIPIENT, " +
                     "ENQ_URL, " +
                     "ENQ_SALUTATION, " +
                     "ENQ_NAME, " +
                     "ENQ_COMPANYNAME, " +
                     "ENQ_CONTACTNO, " +
                     "ENQ_EMAILSENDER, " +
                     "ENQ_SUBJECT, " +
                     "ENQ_MESSAGE, " +
                     "ENQ_CREATION" +
                     ") VALUES " +
                     "(?,?,?,?,?,?,?,?,?,SYSDATE())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ENQ_EMAILRECIPIENT", OdbcType.VarChar, 250).Value = strEnqEmailRecipient;
            cmd.Parameters.Add("p_ENQ_URL", OdbcType.VarChar, 250).Value = strEnqURL;
            cmd.Parameters.Add("p_ENQ_SALUTATION", OdbcType.Int, 9).Value = intEnqSalutation;
            cmd.Parameters.Add("p_ENQ_NAME", OdbcType.VarChar, 250).Value = strEnqName;
            cmd.Parameters.Add("p_ENQ_COMPANYNAME", OdbcType.VarChar, 250).Value = strEnqComName;
            cmd.Parameters.Add("p_ENQ_CONTACTNO", OdbcType.VarChar, 20).Value = strEnqContactNo;
            cmd.Parameters.Add("p_ENQ_EMAILSENDER", OdbcType.VarChar, 250).Value = strEnqEmailSender;
            cmd.Parameters.Add("p_ENQ_SUBJECT", OdbcType.VarChar, 500).Value = strEnqSubject;
            cmd.Parameters.Add("p_ENQ_MESSAGE", OdbcType.VarChar, 1000).Value = strEnqMessage;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
   
    public DataSet getUserEnquiry(int intActive)
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT A.ENQ_ID, A.ENQ_EMAILRECIPIENT, A.ENQ_URL, A.ENQ_SALUTATION, A.ENQ_NAME, A.ENQ_COMPANYNAME, A.ENQ_CONTACTNO, A.ENQ_EMAILSENDER," +
                     " A.ENQ_SUBJECT, A.ENQ_MESSAGE, A.ENQ_REMARK1, A.ENQ_REMARK2, A.ENQ_REMARK3, A.ENQ_REMARK4, A.ENQ_REMARK5, A.ENQ_ACTIVE, A.ENQ_CREATION, A.ENQ_LASTUPDATE" +
                     " FROM TB_ENQUIRY A";

            if (intActive != 0)
            {
                strSql += " WHERE A.ENQ_ACTIVE = ?";
                cmd.Parameters.Add("p_ENQ_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public Boolean extractEnquiryById(int intId, int intActive)
    {
        Boolean boolFound = false;

        try
        {
            string strSql;
            DataSet dataSet = new DataSet();
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * " +
                      " FROM TB_ENQUIRY A " +
                      " WHERE A.ENQ_ID = ?"; 

            cmd.Parameters.Add("p_ENQ_ID", OdbcType.BigInt, 9).Value = intId;

            if (intActive != 0)
            {
                strSql += " AND A.ENQ_ACTIVE = ?";
                cmd.Parameters.Add("p_ENQ_ACTIVE", OdbcType.Int, 1).Value = intActive;
            }

            cmd.Connection = openConn();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dataSet);

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                boolFound = true;

                enqId = int.Parse(dataSet.Tables[0].Rows[0]["ENQ_ID"].ToString());
                enqResendCount = Convert.ToInt32(dataSet.Tables[0].Rows[0]["ENQ_RESENDCOUNT"]);
                enqEmailRecipient = dataSet.Tables[0].Rows[0]["ENQ_EMAILRECIPIENT"].ToString();
                enqURL = dataSet.Tables[0].Rows[0]["ENQ_URL"].ToString();
                enqSalutation = Convert.ToInt16(dataSet.Tables[0].Rows[0]["ENQ_SALUTATION"]);
                enqName = dataSet.Tables[0].Rows[0]["ENQ_NAME"].ToString();
                enqCompanyName = dataSet.Tables[0].Rows[0]["ENQ_COMPANYNAME"].ToString();
                enqContactNo = dataSet.Tables[0].Rows[0]["ENQ_CONTACTNO"].ToString();
                enqEmailSender = dataSet.Tables[0].Rows[0]["ENQ_EMAILSENDER"].ToString();
                enqMessage = dataSet.Tables[0].Rows[0]["ENQ_MESSAGE"].ToString();
                engGeoLocation = dataSet.Tables[0].Rows[0]["ENQ_GEOLOCATION"].ToString(); 
                enqSubject = dataSet.Tables[0].Rows[0]["ENQ_SUBJECT"].ToString();
                enqRemark1 = dataSet.Tables[0].Rows[0]["ENQ_REMARK1"].ToString();
                enqRemark2 = dataSet.Tables[0].Rows[0]["ENQ_REMARK2"].ToString();
                enqRemark3 = dataSet.Tables[0].Rows[0]["ENQ_REMARK3"].ToString();
                enqRemark4 = dataSet.Tables[0].Rows[0]["ENQ_REMARK4"].ToString();
                enqRemark5 = dataSet.Tables[0].Rows[0]["ENQ_REMARK5"].ToString();
                enqActive = Convert.ToInt16(dataSet.Tables[0].Rows[0]["ENQ_ACTIVE"]);
                enqCreation = dataSet.Tables[0].Rows[0]["ENQ_CREATION"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["ENQ_CREATION"].ToString()) : DateTime.MaxValue;
                enqLastUpDate = dataSet.Tables[0].Rows[0]["ENQ_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["ENQ_LASTUPDATE"].ToString()) : DateTime.MaxValue;
                enqLastResend = dataSet.Tables[0].Rows[0]["ENQ_LASTRESEND"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[0]["ENQ_LASTRESEND"].ToString()) : DateTime.MaxValue;
            }
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return boolFound;
    }
    public int updateEnquiryResend()
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            strSql = "UPDATE TB_ENQUIRY SET ENQ_LASTRESEND = NOW(), ENQ_RESENDCOUNT = ENQ_RESENDCOUNT + 1 WHERE ENQ_ID = ?";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            cmd.Parameters.Add("p_ENQ_ID", OdbcType.VarChar, 250).Value = enqId;

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    #endregion
}

public class clsEnquiryRecipient : dbconnBase
{
    #region "Properties"
    private int _id;
    private int _order;
    private int _active;
    private int _createdBy;
    private int _updatedBy;

    private string _name;
    private string _email;

    private DateTime _creation;
    private DateTime _lastUpdate;
    #endregion

    #region "Property Methods"
    public int id
    {
        get { return _id; }
        set { _id = value; }
    }
    public int order
    {
        get { return _order; }
        set { _order = value; }
    }
    public int active
    {
        get { return _active; }
        set { _active = value; }
    }
    public int createdBy
    {
        get { return _createdBy; }
        set { _createdBy = value; }
    }
    public int updatedBy
    {
        get { return _updatedBy; }
        set { _updatedBy = value; }
    }

    public string name
    {
        get { return _name; }
        set { _name = value; }
    }
    public string email
    {
        get { return _email; }
        set { _email = value; }
    }

    public DateTime creation
    {
        get { return _creation; }
        set { _creation = value; }
    }
    public DateTime lastUpdate
    {
        get { return _lastUpdate; }
        set { _lastUpdate = value; }
    }
    #endregion

    #region "Request"
    public DataTable getDataTable()
    {
        DataSet ds = getDataSet();
        if(ds != null)
        {
            return ds.Tables[0];
        }
        return null;
    }
    public DataSet getDataSet()
    {
        DataSet ds = new DataSet();

        try
        {
            OdbcDataAdapter da = new OdbcDataAdapter();
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            strSql = "SELECT * FROM tb_enquiry_recipient WHERE 1=1";
            strSql += " ORDER BY enqrcpt_order";
            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            ds = null;
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }

    public int add()
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<string> columns = new List<string>()
            {
                "enqrcpt_name","enqrcpt_email","enqrcpt_order","enqrcpt_active","enqrcpt_createdby",
                "enqrcpt_creation"
            };
            List<string> parameters = columns.Select((value, index) => (index == columns.Count - 1) ? "NOW()" : "?").ToList();

            cmd.Parameters.AddWithValue("p_enqrcpt_name", name);
            cmd.Parameters.AddWithValue("p_enqrcpt_email", email);
            cmd.Parameters.AddWithValue("p_enqrcpt_order", order);
            cmd.Parameters.AddWithValue("p_enqrcpt_active", active);
            cmd.Parameters.AddWithValue("p_enqrcpt_createdby", createdBy);

            string strSql = "INSERT INTO tb_enquiry_recipient (" + string.Join(",", columns.ToArray()) + ")" +
                            " VALUES (" + string.Join(",", parameters.ToArray()) + ")";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int update()
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<string> columns = new List<string>()
            {
                "enqrcpt_order = ?","enqrcpt_updatedby = ?",
                "enqrcpt_lastupdate = NOW()"
            };

            string strSql = "UPDATE tb_enquiry_recipient SET " + string.Join(",", columns.ToArray()) + 
                            " WHERE enqrcpt_id = ?";

            cmd.Parameters.AddWithValue("p_enqrcpt_order", order);
            cmd.Parameters.AddWithValue("p_enqrcpt_updatedby", updatedBy);

            cmd.Parameters.AddWithValue("p_enqrcpt_id", id);

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int delete()
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            string strSql = "DELETE FROM tb_enquiry_recipient WHERE enqrcpt_id = ?";

            cmd.Parameters.AddWithValue("p_enqrcpt_id", id);

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public void reset()
    {
            id = 0;
        order = 0;
        active = 0;
        createdBy = 0;
        updatedBy = 0;

        name = null;
        email = null;

        creation = DateTime.MinValue;
        lastUpdate = DateTime.MinValue;
    }
    #endregion
}

public class clsEnquiryField : dbconnBase
{

    #region "Properties"
    public int ID { get; set; }
    public int order { get; set; }
    public int active { get; set; }
    public int parent { get; set; }
    public int required { get; set; }
    public int createdBy { get; set; }
    public int updatedBy { get; set; }

    public string type { get; set; }
    public string posX { get; set; }
    public string posY { get; set; }
    public string sizeX { get; set; }
    public string sizeY { get; set; }
    public string validate { get; set; }

    public DateTime creation { get; set; }
    public DateTime lastupdate { get; set; }

    public List<clsEnquiryFieldAttr> attrs { get; set; }
    public List<clsEnquiryFieldLabel> labels { get; set; }
    #endregion

    public clsEnquiryField()
    {
        resetValue();
    }

    #region "Methods"
    public DataTable getDataTable()
    {
        DataSet ds = getDataSet();
        if (ds != null)
        {
            return ds.Tables[0];
        }
        return null;
    }
    public DataSet getDataSet()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM tb_enquiry_field t1 " +
                     " LEFT JOIN tb_enquiry_field_label t2 ON t1.enqfield_id = t2.enqfield_id and t2.enqlabel_lang = 'en'" +
                     " ORDER BY enqfield_order asc";

            if (ID != -1)
            {
                strSql += " AND enqfield_id = ?";
                cmd.Parameters.Add("p_enqfield_id", OdbcType.Int, 9).Value = ID;
            }
            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            ds = null;
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public bool extractByID(int ID)
    {
        resetValue();
        this.ID = ID;

        DataTable dt = getDataTable();
        if (dt != null && dt.Rows.Count == 1)
        {
            DataRow row = dt.Rows[0];
            //active = Convert.ToInt16(row["NEWS_ACTIVE"]);

            //content = Convert.ToString(row["NEWS_CONTENT"]);
            //title = Convert.ToString(row["NEWS_TITLE"]);
            //sourceUrl = Convert.ToString(row["NEWS_SOURCE_URL"]);
            //sourceTitle = Convert.ToString(row["NEWS_SOURCE_TITLE"]);

            //date = row["NEWS_DATE"] != DBNull.Value ? Convert.ToDateTime(row["NEWS_DATE"]) : date;
            //creation = row["NEWS_CREATION"] != DBNull.Value ? Convert.ToDateTime(row["NEWS_CREATION"]) : creation;
            //lastupdate = row["NEWS_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(row["NEWS_LASTUPDATE"]) : lastupdate;
            //return true;
        }
        return false;
    }

    public DataTable getOptions()
    {
        DataSet ds = new DataSet();
        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = " SELECT * FROM tb_enquiry_field t1 " +
                     " LEFT JOIN tb_enquiry_field_label t2 ON t1.enqfield_id = t2.enqfield_id and t2.enqlabel_lang = 'en'" +
                     " LEFT JOIN tb_enquiry_field_attribute t3 on t1.enqfield_id = t3.enqfield_id" +
                     " WHERE enqfield_type = 'option'" +
                     " ORDER BY enqfield_order asc";


            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            ds = null;
            throw ex;
        }
        finally
        {
            closeConn();
        }

        if(ds!= null)
        {
            return ds.Tables[0];
        }
        return null;
    }

    public bool addList(List<clsEnquiryField> fields)
    {
        foreach(clsEnquiryField field in fields)
        {
            bool fieldSuccess = false;
            this.parent = field.parent;
            this.required = field.required;
            this.type = field.type;
            this.createdBy = field.createdBy;
            this.validate = field.validate;
            this.order = field.order;
            this.active = field.active;
            this.posX = field.posX;
            this.posY = field.posY;
            this.sizeX = field.sizeX;
            this.sizeY = field.sizeY;

            fieldSuccess = this.add() == 1;

            // update parent for other 
            if(fields.Where(x => x.parent == field.ID).Count() > 0)
            {
                fields.ForEach(x => x.parent = (x.parent == field.ID) ? this.ID : x.parent);
            }


            bool labelSuccess = true;
            if (field.labels.Count > 0)
            {
                field.labels.ForEach(x => x.fieldID = this.ID);
                labelSuccess = new clsEnquiryFieldLabel().addList(field.labels);
            }

            bool attrSuccess = true;
            if (field.attrs != null && field.attrs.Count > 0)
            {
                field.attrs.ForEach(x => x.fieldID = this.ID);
                attrSuccess = new clsEnquiryFieldAttr().addList(field.attrs);
            }

        }

        return true;
    }
    public int add()
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            // ========= columns ========
            List<string> columns = new List<string>();
            if (parent != -1) { columns.Add("enqfield_parent");cmd.Parameters.AddWithValue("enqfield_parent",parent); }
            if (required != -1) { columns.Add("enqfield_required"); cmd.Parameters.AddWithValue("enqfield_required", required); }
            if (order != -1) { columns.Add("enqfield_order"); cmd.Parameters.AddWithValue("enqfield_order", order); }
            if (active != -1) { columns.Add("enqfield_active"); cmd.Parameters.AddWithValue("enqfield_active", active); }
            if (type != null) { columns.Add("enqfield_type"); cmd.Parameters.AddWithValue("enqfield_type", type); }
            if (validate != null) { columns.Add("enqfield_validate"); cmd.Parameters.AddWithValue("enqfield_validate", validate); }
            if (posX != null) { columns.Add("enqfield_position_x"); cmd.Parameters.AddWithValue("enqfield_position_x", posX); }
            if (posY != null) { columns.Add("enqfield_position_y"); cmd.Parameters.AddWithValue("enqfield_position_y", posY); }
            if (sizeX != null) { columns.Add("enqfield_size_x"); cmd.Parameters.AddWithValue("enqfield_size_x", sizeX); }
            if (sizeY != null) { columns.Add("enqfield_size_y"); cmd.Parameters.AddWithValue("enqfield_size_y", sizeY); }
            columns.Add("enqfield_createdby"); cmd.Parameters.AddWithValue("enqfield_createdby", createdBy);

            // ======== parameters ========
            List<string> parameters = columns.Select(x => "?").ToList();

            strSql = "INSERT INTO TB_ENQUIRY_FIELD ("+string.Join(",",columns.ToArray())+ ",enqfield_creation) VALUES (" + string.Join(",", parameters.ToArray()) + ",NOW())";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();


            if (intRecordAffected == 1)
            {
                trans.Commit();

                OdbcCommand cmd2 = new OdbcCommand();
                string strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                ID = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public bool delete(int ID)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            string strSql = "DELETE FROM tb_enquiry_field WHERE enqfield_id = ?";

            cmd.Parameters.AddWithValue("p_value", ID);

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;


            new clsEnquiryFieldAttr().deleteByFieldID(ID);
            new clsEnquiryFieldLabel().deleteByFieldID(ID);

            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected > 0)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected > 0;
    }

    public int updateOrderByID(int order, int ID, int updatedBy)
    {
        return update(
            new Dictionary<string, object>() { { "enqfield_order", order } }, 
            new Dictionary<string, object>() { { "enqfield_id", ID } }, 
            updatedBy);
    }
    public int update(Dictionary<string, object> paramUpdate, Dictionary<string, object> paramWhere, int updatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<string> columns = new List<string>();
            List<string> wheres = new List<string>();

            foreach(KeyValuePair<string,object> entry in paramUpdate)
            {
                columns.Add(entry.Key + " = " + "?");
                cmd.Parameters.AddWithValue("p_" + entry.Key, entry.Value);
            }

            cmd.Parameters.AddWithValue("p_UPDATEDBY", updatedBy);

            foreach (KeyValuePair<string, object> entry in paramWhere)
            {
                wheres.Add(entry.Key + " = " + "?");
                cmd.Parameters.AddWithValue("p_" + entry.Key, entry.Value);
            }

            strSql = "UPDATE TB_ENQUIRY_FIELD SET " + string.Join(",", columns.ToArray()) + "," +
                    " ENQFIELD_UPDATEDBY = ?," +
                    " ENQFIELD_LASTUPDATE = NOW()" +
                    " WHERE " + string.Join("AND ",wheres.ToArray());

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();


            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    private void resetValue()
    {
        ID = -1;
        order = -1;
        createdBy = -1;
        updatedBy = -1;
        parent = -1;
        required = -1;
        active = -1;

        creation = DateTime.MinValue;
        lastupdate = DateTime.MinValue;

        type = null;
        posX = null;
        posY = null;
        sizeX = null;
        sizeY = null;
        validate = null;
        attrs = null;
    }
    #endregion
}
public class clsEnquiryFieldAttr : dbconnBase
{

    #region "Properties"
    public int ID { get; set; }
    public int fieldID { get; set; }
    public string name { get; set; }
    public string value { get; set; }
    #endregion

    public clsEnquiryFieldAttr()
    {
        resetValue();
    }

    #region "Methods"
    public DataTable getDataTable()
    {
        DataSet ds = getDataSet();
        if (ds != null)
        {
            return ds.Tables[0];
        }
        return null;
    }
    public DataSet getDataSet()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM tb_enquiry_field_attribute t1 ";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            ds = null;
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public int add()
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            // ========= columns ========
            List<string> columns = new List<string>();
            if (fieldID != -1) { columns.Add("enqfield_id"); cmd.Parameters.AddWithValue("enqfield_id", fieldID); }
            if (name != null) { columns.Add("enqattr_name"); cmd.Parameters.AddWithValue("enqattr_name", name); }
            if (value != null) { columns.Add("enqattr_value"); cmd.Parameters.AddWithValue("enqattr_value", value); }

            // ======== parameters ========
            List<string> parameters = columns.Select(x => "?").ToList();

            strSql = "INSERT INTO TB_ENQUIRY_FIELD_ATTRIBUTE (" + string.Join(",", columns.ToArray()) + ") VALUES (" + string.Join(",", parameters.ToArray()) + ")";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();


            if (intRecordAffected == 1)
            {
                trans.Commit();

                OdbcCommand cmd2 = new OdbcCommand();
                string strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                ID = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public bool addList(List<clsEnquiryFieldAttr> attrs)
    {
        foreach (clsEnquiryFieldAttr attr in attrs)
        {
            bool success = false;
            this.fieldID = attr.fieldID;
            this.name = attr.name;
            this.value = attr.value;
            success = this.add() == 1;

            if (!success)
            {
                this.deleteByFieldID(this.fieldID);
                return false;
            }
        }
        return true;
    }
    public int update(Dictionary<string, object> paramUpdate, Dictionary<string, object> paramWhere, int updatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<string> columns = new List<string>();
            List<string> wheres = new List<string>();

            foreach (KeyValuePair<string, object> entry in paramUpdate)
            {
                columns.Add(entry.Key + " = " + "?");
                cmd.Parameters.AddWithValue("p_" + entry.Key, entry.Value);
            }

            foreach (KeyValuePair<string, object> entry in paramWhere)
            {
                wheres.Add(entry.Key + " = " + "?");
                cmd.Parameters.AddWithValue("p_" + entry.Key, entry.Value);
            }

            strSql = "UPDATE TB_ENQUIRY_FIELD_ATTRIBUTE SET " + string.Join(",", columns.ToArray()) + "" +
                    " WHERE " + string.Join("AND ", wheres.ToArray());

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();


            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }

    public bool deleteByFieldID(int fieldID)
    {
        return this.delete("enqfield_id", fieldID);
    }
    public bool delete(string name, object value)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            string strSql = "DELETE FROM tb_enquiry_field_label WHERE " + name + " = ?";

            cmd.Parameters.AddWithValue("p_value", value);

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected > 0)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected > 0;
    }


    private void resetValue()
    {
        ID = -1;
        fieldID = -1;

        name = null;
        value = null;
    }
    #endregion
}
public class clsEnquiryFieldLabel : dbconnBase
{

    #region "Properties"
    public int ID { get; set; }
    public int fieldID { get; set; }
    public string value { get; set; }
    public string lang { get; set; }
    #endregion

    public clsEnquiryFieldLabel()
    {
        resetValue();
    }

    #region "Methods"
    public DataTable getDataTable()
    {
        DataSet ds = getDataSet();
        if (ds != null)
        {
            return ds.Tables[0];
        }
        return null;
    }
    public DataSet getDataSet()
    {
        DataSet ds = new DataSet();

        try
        {
            string strSql;
            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter da = new OdbcDataAdapter();

            strSql = "SELECT * FROM tb_enquiry_field_attribute t1 ";

            cmd.Connection = openConn();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            ds = null;
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return ds;
    }
    public bool extractByID(int ID)
    {
        resetValue();
        this.ID = ID;

        DataTable dt = getDataTable();
        if (dt != null && dt.Rows.Count == 1)
        {
            DataRow row = dt.Rows[0];
            //active = Convert.ToInt16(row["NEWS_ACTIVE"]);

            //content = Convert.ToString(row["NEWS_CONTENT"]);
            //title = Convert.ToString(row["NEWS_TITLE"]);
            //sourceUrl = Convert.ToString(row["NEWS_SOURCE_URL"]);
            //sourceTitle = Convert.ToString(row["NEWS_SOURCE_TITLE"]);

            //date = row["NEWS_DATE"] != DBNull.Value ? Convert.ToDateTime(row["NEWS_DATE"]) : date;
            //creation = row["NEWS_CREATION"] != DBNull.Value ? Convert.ToDateTime(row["NEWS_CREATION"]) : creation;
            //lastupdate = row["NEWS_LASTUPDATE"] != DBNull.Value ? Convert.ToDateTime(row["NEWS_LASTUPDATE"]) : lastupdate;
            //return true;
        }
        return false;
    }
    public int add()
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            // ========= columns ========
            List<string> columns = new List<string>();
            if (fieldID != -1) { columns.Add("enqfield_id"); cmd.Parameters.AddWithValue("enqfield_id", fieldID); }
            if (value != null) { columns.Add("enqlabel_value"); cmd.Parameters.AddWithValue("enqlabel_value", value); }
            if (lang != null) { columns.Add("enqlabel_lang"); cmd.Parameters.AddWithValue("enqlabel_lang", lang); }

            // ======== parameters ========
            List<string> parameters = columns.Select(x => "?").ToList();

            strSql = "INSERT INTO TB_ENQUIRY_FIELD_LABEL (" + string.Join(",", columns.ToArray()) + ") VALUES (" + string.Join(",", parameters.ToArray()) + ")";

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();


            if (intRecordAffected == 1)
            {
                trans.Commit();

                OdbcCommand cmd2 = new OdbcCommand();
                string strSql2 = "SELECT LAST_INSERT_ID()";
                cmd2.Connection = openConn();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = strSql2;
                ID = Convert.ToInt16(cmd2.ExecuteScalar());
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public int update(Dictionary<string, object> paramUpdate, Dictionary<string, object> paramWhere, int updatedBy)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();
            string strSql;

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            List<string> columns = new List<string>();
            List<string> wheres = new List<string>();

            foreach (KeyValuePair<string, object> entry in paramUpdate)
            {
                columns.Add(entry.Key + " = " + "?");
                cmd.Parameters.AddWithValue("p_" + entry.Key, entry.Value);
            }

            foreach (KeyValuePair<string, object> entry in paramWhere)
            {
                wheres.Add(entry.Key + " = " + "?");
                cmd.Parameters.AddWithValue("p_" + entry.Key, entry.Value);
            }

            strSql = "UPDATE TB_ENQUIRY_FIELD_LABEL SET " + string.Join(",", columns.ToArray()) + "" +
                    " WHERE " + string.Join("AND ", wheres.ToArray());

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;

            intRecordAffected = cmd.ExecuteNonQuery();


            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected;
    }
    public bool addList(List<clsEnquiryFieldLabel> labels)
    {
        foreach(clsEnquiryFieldLabel label in labels)
        {
            bool success = false;
            this.fieldID = label.fieldID;
            this.lang = label.lang;
            this.value = label.value;
            success = this.add() == 1;

            if (!success)
            {
                this.deleteByFieldID(this.fieldID);
                return false;
            }
        }
        return true;
    }
    public bool deleteByFieldID(int fieldID)
    {
        return this.delete("enqfield_id",fieldID);
    }
    public bool delete(string name, object value)
    {
        int intRecordAffected = 0;

        try
        {
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            string strSql = "DELETE FROM tb_enquiry_field_label WHERE " + name + " = ?";

            cmd.Parameters.AddWithValue("p_value", value);

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strSql;


            intRecordAffected = cmd.ExecuteNonQuery();

            if (intRecordAffected > 0)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected > 0;
    }
    private void resetValue()
    {
        ID = -1;
        fieldID = -1;

        lang = null;
        value = null;
    }
    #endregion
}
