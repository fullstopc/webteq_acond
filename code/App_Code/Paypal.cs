﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Data;
using System.Web;
using System.Configuration;
using System.Xml;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
/// <summary>
/// Summary description for Booking
/// </summary>
public class Paypal
{
    public string cmd { get; set; }
    public string business { get; set; }
    public bool isSandbox { get; set; }
    public string sandBoxUrl { get; set; }
    public string paypalUrl { get; set; }
    public string notifyUrl { get; set; }
    public string PDTToken { get; set; }
    public string websiteName { get; set; }
    public string currency { get; set; }
    public string finalUrl
    {
        get
        {
            return isSandbox ? sandBoxUrl : paypalUrl;
        }
    }

    public void init()
    {
        clsConfig config = new clsConfig();
        cmd = config.extractItemByNameGroup(clsKey.CONFIG__PAYPAL__CMD, clsKey.CONFIG__PAYPAL, 1) ? config.value : null;
        business = config.extractItemByNameGroup(clsKey.CONFIG__PAYPAL__BUSINESS, clsKey.CONFIG__PAYPAL, 1) ? config.value : null;
        websiteName = config.extractItemByNameGroup(clsConfig.CONSTNAMEWEBSITENAME, clsConfig.CONSTGROUPWEBSITE, 1) ? config.value : null;
        isSandbox = config.extractItemByNameGroup(clsKey.CONFIG__PAYPAL__USE_SAND_BOX, clsKey.CONFIG__PAYPAL, 1) ? string.Compare(config.value, "true", false) == 0 ? true : false : false;
        sandBoxUrl = config.extractItemByNameGroup(clsKey.CONFIG__PAYPAL__SAND_BOX, clsKey.CONFIG__PAYPAL, 1) ? config.value : null;
        paypalUrl = config.extractItemByNameGroup(clsKey.CONFIG__PAYPAL__NAME, clsKey.CONFIG__PAYPAL, 1) ? config.value : null;
        PDTToken = config.extractItemByNameGroup(clsKey.CONFIG__PAYPAL__PDT_TOKEN, clsKey.CONFIG__PAYPAL, 1) ? config.value : null;
        currency = config.extractItemByNameGroup(clsKey.CONFIG__PAYPAL__CURRENCY, clsKey.CONFIG__PAYPAL, 1) ? config.value : null;
        notifyUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsKey.CONFIG__PAYPAL__IPN;
    }

    // Payment Data Trasfer
    public bool PDTValidate(string token)
    {
        init();
        string query = string.Format("cmd=_notify-synch&tx={0}&at={1}", token, PDTToken);

        //create the request back
        
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072; //TLS 1.2 = 3072
        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(finalUrl);

        //set values for the request back
        req.Method = "POST";
        req.UserAgent = websiteName;
        req.ContentType = "application/x-www-form-urlencoded";
        req.ContentLength = query.Length;


        //write the request back IPN strings
        StreamWriter stOut = new StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII);
        stOut.Write(query);
        stOut.Close();

        //do the request to Paypal and get the response
        StreamReader stIn = new StreamReader(req.GetResponse().GetResponseStream());
        string strResponse = stIn.ReadToEnd();
        stIn.Close();

        if (strResponse.StartsWith("SUCCESS"))
        {
            string[] strParts = strResponse.Split((char)'\n');
            string strFirstName = "";
            string strLastName = "";
            string strEmail = "";
            string strOrderNo = "";
            string strAmount = "";
            string strCurrency = "";
            string strCustom = "";

            for (int i = 0; i <= strParts.GetUpperBound(0); i++)
            {
                string[] strPart = strParts[i].Split((char)'=');

                switch (strPart[0])
                {
                    case "first_name":
                        strFirstName = strPart[1];
                        break;
                    case "last_name":
                        strLastName = strPart[1];
                        break;
                    case "payer_email":
                        strEmail = strPart[1].Replace("%40", "@");
                        break;
                    case "item_name":
                        strOrderNo = strPart[1];
                        break;
                    case "mc_gross":
                        strAmount = strPart[1];
                        break;
                    case "mc_currency":
                        strCurrency = strPart[1];
                        break;
                    case "custom":
                        strCustom = strPart[1];
                        break;
                }
            }

            return true;
        }
        return false;
    }

    // Payment
    public string payment(clsReservation order)
    {
        init();

        string url = finalUrl;
        url += "?cmd=" + cmd;
        url += "&business=" + HttpUtility.UrlEncode(business);
        url += "&item_name=" + HttpUtility.UrlEncode(order.code);
        url += "&amount=" + order.serviceChargeAmount;
        url += "&currency_code=" + currency;
        url += "&cancel_return=" + HttpUtility.UrlEncode(HttpContext.Current.Request.Url.ToString());
        url += "&custom=" + HttpUtility.UrlEncode(order.getParameters());
        url += "&address_override=1";
        url += "&first_name=" + order.memName;
        url += "&address1=address1";
        url += "&city=city";
        //strUrl += "&state=" + ord.shippingState;
        url += "&zip=";
        url += "&country=MY";
        url += "&return=" + HttpUtility.UrlEncode(HttpContext.Current.Request.Url.ToString());
        url += "&notify_url=" + notifyUrl;

        clsLog.payment("[PALPAY][URL] : " + url);
        return url;
    }

    // IPN Handler
    #region IPN Handler
    public bool IPN()
    {
        init();

        try
        {

            // get All request
            var request = HttpContext.Current.Request;
            string requestCode = request["item_name"];
            string requestCustom = request["custom"];

            clsLog.payment("[PAYPAL][Request][custom] :" + requestCustom);

            clsReservation order = new clsReservation();
            if (!string.IsNullOrEmpty(requestCustom))
            {
                //order.setParameters(requestCustom);
            }

            DataSet dsResponse = new DataSet();
            string responseFile = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["uplBase"] + "PaymentResponses.xml");
            if (File.Exists(responseFile))
            {
                dsResponse.ReadXml(responseFile);
            }
            else
            {
                createXML(responseFile, "Responses");
                dsResponse.ReadXml(responseFile);
            }


            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072; //TLS 1.2 = 3072
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(finalUrl);

            //set values fot the request back
            req.Method = "POST";
            req.UserAgent = websiteName; //GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString();
            req.ContentType = "application/x-www-form-urlencoded";
            byte[] param = HttpContext.Current.Request.BinaryRead(HttpContext.Current.Request.ContentLength);
            string query = Encoding.ASCII.GetString(param);
            string ipnPost = query;
            query += "&cmd=_notify-validate";
            req.ContentLength = query.Length;

            //clsLog.logErroMsg("IPN POST:" + ipnPost);
            clsLog.payment("[PAYPAL][" + requestCode + "] IPN Query:" + query);

            // send the request to PayPal and get the response
            StreamWriter stOut = new StreamWriter(req.GetRequestStream(), Encoding.ASCII);
            stOut.Write(query);
            stOut.Close();


            //send the request, read the response
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            Stream responseStream = res.GetResponseStream();
            StreamReader stIn = new StreamReader(responseStream);
            string strResponse = stIn.ReadToEnd();
            stIn.Close();



            // if no verified
            if (string.Compare(strResponse, "VERIFIED", false) != 0) return false;

            bool isRefund = string.Compare(request["payment_status"], "Refunded", true) == 0;
            bool isDuplicate = isDuplicateTxnId(request["txn_id"], dsResponse);
            bool isEmailSame = string.Compare(request["receiver_email"], business, false) != 0;
            bool isUnComplete = string.Compare(request["payment_status"], "Completed", false) != 0;

            // error message
            string msg = "";
            if (isRefund) { msg = "Payment Status is refunded.|"; }
            if (isDuplicate) { msg = "Duplicate txn_id found.|"; }
            if (isUnComplete) { msg = "Payment Status is not completed.|"; }
            if (isEmailSame) { msg = "receiver_email is different with our Primary PayPal email:" + request["receiver_email"] + ".|"; }

            if (isDuplicate || isUnComplete || isEmailSame || isRefund)
            {
                addPaymentResponse(requestCode,
                    request["txn_id"],
                    DateTime.Now,
                    Convert.ToDecimal(request["mc_gross"]),
                    request["mc_currency"],
                    request["payment_status"],
                    request["payer_email"],
                    request["first_name"],
                    request["last_name"],
                    "", "", "", "", "", false, msg);

                if (isRefund)
                {
                    PaymentRefund(order);
                }
            }
            else
            {
                addPaymentResponse(requestCode,
                    request["txn_id"],
                    DateTime.Now,
                    Convert.ToDecimal(request["mc_gross"]),
                    request["mc_currency"],
                    request["payment_status"],
                    request["payer_email"],
                    request["first_name"],
                    request["last_name"],
                    "", "", "", "", "", true, "Payment Process success.");

                PaymentReceive(order);
            }
        }
        catch(Exception ex)
        {
            clsLog.payment("[PAYPAL] Error in IPNHandler: " + ex.Message);
        }
        return false;
    }

    private void createXML(string xmlFile, string element)
    {
        //create the XMLDocument
        XmlDocument doc = new XmlDocument();
        XmlNode declaration = doc.CreateNode(XmlNodeType.XmlDeclaration, null, null);
        doc.AppendChild(declaration);

        string xmlData = "<" + element + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"PaymentResponse.xsd\">" +
                         "</" + element + ">";

        doc.Load(new StringReader(xmlData));

        try
        {
            doc.Save(xmlFile);
        }
        catch (Exception ex)
        {
            clsLog.payment("[PAYPAL] Error in Creating XML: " + ex.Message);
        }
    }
    private Boolean isDuplicateTxnId(string strTxnId, DataSet ds)
    {
        Boolean boolSame = false;

        try
        {
            string strExpression = "txn_id = '" + strTxnId + "' AND is_success = " + true;

            if (ds.Tables.Count > 0)
            {
                DataRow[] foundRow = ds.Tables[0].Select(strExpression);

                if (foundRow.Length > 0)
                {
                    boolSame = true;
                }
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg("[PAYPAL] Error in IPNHandler:" + ex.Message);
        }

        return boolSame;
    }
    private void addPaymentResponse(string strOrderNo, string strTxnId, DateTime dtPaymentDate, decimal decAmount, string strCurrencyCode, string strPaymentStatus, string strEmail, string strFirstName, string strLastName, string strStreet, string strCity, string strState, string strZip, string strCountry, Boolean boolIsSuccess, string strRemarks)
    {
        try
        {
            string xmlFile = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["uplBase"] + "PaymentResponses.xml");
            XmlDocument doc = new XmlDocument();
            XmlTextReader reader;

            if (File.Exists(xmlFile))
            {
                reader = new XmlTextReader(xmlFile);
                reader.Read();
            }
            else
            {
                createXML(xmlFile, "Responses");
                reader = new XmlTextReader(xmlFile);
                reader.Read();
            }

            doc.Load(reader);
            reader.Close();

            DateTime dtTryParse;
            decimal decTryParse;
            Boolean boolTryParse;

            if (DateTime.TryParse(dtPaymentDate.ToString(), out dtTryParse))
            {
                dtPaymentDate = dtTryParse;
            }
            else
            {
                clsLog.logErroMsg("XML Validation: payment_date is not in correct format.");
            }

            if (decimal.TryParse(decAmount.ToString(), out decTryParse))
            {
                decAmount = decTryParse;
            }
            else
            {
                clsLog.logErroMsg("XML Validation: payment_price is not in correct format.");
            }

            string pattern = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

            Regex check = new Regex(pattern, RegexOptions.IgnorePatternWhitespace);

            if (!string.IsNullOrEmpty(strEmail))
            {
                if (!check.IsMatch(strEmail))
                {
                    clsLog.logErroMsg("XML Validation: email is not in correct format.");
                }
            }

            if (Boolean.TryParse(boolIsSuccess.ToString(), out boolTryParse))
            {
                boolIsSuccess = boolTryParse;
            }
            else
            {
                clsLog.logErroMsg("XML Validation: is_success is not in correct format.");
            }

            XmlElement myResponse = doc.CreateElement("Response");
            myResponse.SetAttribute("order_no", strOrderNo);
            myResponse.SetAttribute("txn_id", strTxnId);
            myResponse.SetAttribute("payment_date", dtPaymentDate.ToString());
            myResponse.SetAttribute("currency_code", strCurrencyCode);
            myResponse.SetAttribute("payment_price", decAmount.ToString());
            myResponse.SetAttribute("payment_status", strPaymentStatus);
            myResponse.SetAttribute("email", strEmail);
            myResponse.SetAttribute("first_name", strFirstName);
            myResponse.SetAttribute("last_name", strLastName);
            myResponse.SetAttribute("street", strStreet);
            myResponse.SetAttribute("city", strCity);
            myResponse.SetAttribute("state", strState);
            myResponse.SetAttribute("zip", strZip);
            myResponse.SetAttribute("country", strCountry);
            myResponse.SetAttribute("is_success", boolIsSuccess.ToString());
            myResponse.SetAttribute("remarks", strRemarks);

            doc.DocumentElement.AppendChild(myResponse);

            doc.Save(xmlFile);
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg("Error in adding Payment Response:" + ex.Message);
        }
    }
    private void PaymentError(clsReservation order)
    {
        clsConfig config = new clsConfig();
        string subjectPrefix = config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1) ? config.value : "";
        string signature = config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1) ? config.longValue : "";
        string senderName = config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1) ? config.value : "";
        string websiteName = config.extractItemByNameGroup(clsConfig.CONSTNAMEWEBSITENAME, clsConfig.CONSTGROUPWEBSITE, 1) ? config.value : null;

        // custom
        string subject = config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYERROREMAILSUBJECT, clsConfig.CONSTGROUPPAYERROREMAILCONTENT, 1) ? config.value : "";
        string content = config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYERRORUSEREMAILCONTENT, clsConfig.CONSTGROUPPAYERROREMAILCONTENT, 1) ? config.longValue : "";
        string subjectAdmin = config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYERROREMAILSUBJECT, clsConfig.CONSTGROUPPAYERROREMAILCONTENT, 1) ? config.value : "";
        string contentAdmin = config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYERRORADMINEMAILCONTENT, clsConfig.CONSTGROUPPAYERROREMAILCONTENT, 1) ? config.longValue : "";

        subject = sendEmailReplaceContent(order, subject, signature);
        subjectAdmin = sendEmailReplaceContent(order, subjectAdmin, signature);

        content = sendEmailReplaceContent(order, content, signature);
        contentAdmin = sendEmailReplaceContent(order, contentAdmin, signature);

        if (!string.IsNullOrEmpty(subjectPrefix))
        {
            subject = subjectPrefix + " " + subjectPrefix;
        }

        // sent to admin
        bool isSuccess = clsEmail.send(senderName, "", "", "", "", order.memEmail, subjectAdmin, contentAdmin, 1);
        clsLog.email("[USER][" + order.code + "][PaymentError] email send " + (isSuccess ? "success" : "fail"));

        // send to user
        bool isSuccessAdmin = clsEmail.send(senderName, "", order.memEmail, "", "", "", subject, content, 1);
        clsLog.email("[ADMIN][" + order.code + "][PaymentError] email send " + (isSuccess ? "success" : "fail"));

    }
    private void PaymentReceive(clsReservation order)
    {
        clsConfig config = new clsConfig();
        string subjectPrefix = config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1) ? config.value : "";
        string signature = config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1) ? config.longValue : "";
        string senderName = config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1) ? config.value : "";
        string websiteName = config.extractItemByNameGroup(clsConfig.CONSTNAMEWEBSITENAME, clsConfig.CONSTGROUPWEBSITE, 1) ? config.value : null;

        // custom
        string subject = config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYRECOPEMAILSUBJECT, clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT, 1) ? config.value : "";
        string content = config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYRECOPUSEREMAILCONTENT, clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT, 1) ? config.longValue : "";
        string subjectAdmin = config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYRECOPEMAILSUBJECT, clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT, 1) ? config.value : "";
        string contentAdmin = config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYRECOPADMINEMAILCONTENT, clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT, 1) ? config.longValue : "";

        subject = sendEmailReplaceContent(order, subject, signature);
        subjectAdmin = sendEmailReplaceContent(order, subjectAdmin, signature);

        content = sendEmailReplaceContent(order, content, signature);
        contentAdmin = sendEmailReplaceContent(order, contentAdmin, signature);

        //if (!string.IsNullOrEmpty(subjectPrefix))
        //{
        //    subject = subjectPrefix + " " + subjectPrefix;
        //}

        // sent to admin
        bool isSuccess = clsEmail.send(senderName, "", "", "", "", order.memEmail, subject, contentAdmin, 1);
        clsLog.email("[USER][" + order.code + "][PaymentReceive] email send " + (isSuccess ? "success" : "fail"));

        // send to user
        bool isSuccessAdmin = clsEmail.send(senderName, "", order.memEmail, "", "", "", subject, content, 1);
        clsLog.email("[ADMIN][" + order.code + "][PaymentReceive] email send " + (isSuccess ? "success" : "fail"));

    }
    private void PaymentRefund(clsReservation order)
    {
        clsConfig config = new clsConfig();
        string subjectPrefix = config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1) ? config.value : "";
        string signature = config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1) ? config.longValue : "";
        string senderName = config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1) ? config.value : "";
        string websiteName = config.extractItemByNameGroup(clsConfig.CONSTNAMEWEBSITENAME, clsConfig.CONSTGROUPWEBSITE, 1) ? config.value : null;

        // custom
        string subject = config.extractItemByNameGroup(clsConfig.CONSTNAMEREFUNDEMAILSUBJECT, clsConfig.CONSTGROUPREFUNDEMAILCONTENT, 1) ? config.value : "";
        string content = config.extractItemByNameGroup(clsConfig.CONSTNAMEREFUNDUSEREMAILCONTENT, clsConfig.CONSTGROUPREFUNDEMAILCONTENT, 1) ? config.longValue : "";
        string subjectAdmin = config.extractItemByNameGroup(clsConfig.CONSTNAMEREFUNDEMAILSUBJECT, clsConfig.CONSTGROUPREFUNDEMAILCONTENT, 1) ? config.value : "";
        string contentAdmin = config.extractItemByNameGroup(clsConfig.CONSTNAMEREFUNDADMINEMAILCONTENT, clsConfig.CONSTGROUPREFUNDEMAILCONTENT, 1) ? config.longValue : "";

        subject = sendEmailReplaceContent(order, subject, signature);
        subjectAdmin = sendEmailReplaceContent(order, subjectAdmin, signature);

        content = sendEmailReplaceContent(order, content, signature);
        contentAdmin = sendEmailReplaceContent(order, contentAdmin, signature);

        if (!string.IsNullOrEmpty(subjectPrefix))
        {
            subject = subjectPrefix + " " + subjectPrefix;
        }

        // sent to admin
        bool isSuccess = clsEmail.send(senderName, "", "", "", "", order.memEmail, subjectAdmin, contentAdmin, 1);
        clsLog.email("[USER][" + order.code + "][PaymentRefund] email send " + (isSuccess ? "success" : "fail"));

        // send to user
        bool isSuccessAdmin = clsEmail.send(senderName, "", order.memEmail, "", "", "", subject, content, 1);
        clsLog.email("[ADMIN][" + order.code + "][PaymentRefund] email send " + (isSuccess ? "success" : "fail"));

    }
    private string sendEmailReplaceContent(clsReservation order, string content, string signature)
    {
        // FYI Only
        List<string> tags = new List<string>()
        {
            "[BOOKINGID]", "[ROUTE]", "[TRANSPORT]", "[PICKUPDATETIME]", "[REMARK]",
            "[CONTACT_NAME]", "[CONTACT_TEL]", "[CONTACT_EMAIL]",
            "[BILL_NAME]", "[BILL_TEL]", "[BILL_EMAIL]",
        };


        return content.Replace("[BOOKINGID]", order.code)
                        .Replace("[ORDNO]", order.code)
                        .Replace("[REMARK]", order.serviceRemark)

                        .Replace("[NAME]", order.memName)
                        .Replace("[CONTACT_NAME]", order.memName)
                        .Replace("[CONTACT_TEL]", order.memContactTel)
                        .Replace("[CONTACT_EMAIL]", order.memEmail)
                        .Replace("[SIGNATURE]", signature);
    }
    #endregion IPN Handler

}
