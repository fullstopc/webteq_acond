﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsVendor
/// </summary>
public class clsConfigPropertyType : Controller
{
    #region "Properties"
    public int ID { get; set; }
    public int aircondQty { get; set; }
    public int createdby { get; set; }
    public int updatedby { get; set; }
    
    public string name { get; set; }

    public DateTime creation { get; set; }
    public DateTime lastupdated { get; set; }

    #endregion

    public List<clsConfigPropertyTypeAircond> airconds { get; set; }
    public clsConfigPropertyType()
    {
        _tableName = "tb_config_property_type";
        _prefix = "type_";
        _ID = _prefix + "id";
        _columns = new List<Model>()
        {
            new Model(typeof(Int32)) { name = "ID", columnName = _ID },
            new Model(typeof(Int32)) { name = "aircondQty", columnName = _prefix + "aircond_qty", },
            new Model(typeof(Int32)) { name = "createdby", columnName = _prefix + "createdby", },
            new Model(typeof(Int32)) { name = "updatedby", columnName = _prefix + "updatedby", },
            
            new Model(typeof(string)) { name = "name", columnName = _prefix + "name",  },

            new Model(typeof(DateTime)) { name = "creation", columnName = _prefix + "creation",fillable = false  },
            new Model(typeof(DateTime)) { name = "lastupdated", columnName = _prefix + "lastupdated",fillable = false  },
        };

        resetValue();
    }

    public override bool add()
    {
        var isSuccess = base.add();
        foreach(var aircond in airconds)
        {
            aircond.typeID = this.ID;
            aircond.add();
        }
        return isSuccess;
    }

    public override bool update()
    {
        var isSucess = base.update();
        var aircondObj = new clsConfigPropertyTypeAircond();
        aircondObj.delete("type_id",this.ID);
        foreach (var aircond in airconds)
        {
            aircond.typeID = this.ID;
            aircond.add();
        }
        return isSucess;
    }
}
