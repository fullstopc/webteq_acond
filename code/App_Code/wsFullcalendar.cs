﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

/// <summary>
/// Summary description for wsFullcalendar
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class wsFullcalendar : System.Web.Services.WebService {

    public wsFullcalendar () {}

    [WebMethod(EnableSession = true)]
    public string getReservedList()
    {
        return JsonConvert.SerializeObject(new clsReservation()
            .getDataTable()
            .AsEnumerable()
            .Select(x => new clsReservation() { row = x })
            .ToList());
    }

    [WebMethod(EnableSession = true)]
    public string getEmployeeLeaveList()
    {
        var leaves = new clsEmployeeLeave()
            .getDataTable()
            .AsEnumerable()
            .Select(x => new clsEmployeeLeave() { row = x })
            .ToList();

        var employees = new clsEmployee()
                        .getDataTable()
                        .AsEnumerable()
                        .Select(x => new clsEmployee() { row = x })
                        .Where(x => x.active == 1)
                        .Join(leaves,
                                emp => emp.ID,
                                leave => leave.employeeID,
                                (emp, leave) => new {
                                    employeeID = emp.ID,
                                    leaveID = leave.ID,
                                    name = emp.name,
                                    start = leave.start,
                                    end = leave.end,
                                    reason = leave.reason
                                });
        return JsonConvert.SerializeObject(employees);
    }
}

