﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsProperty
/// </summary>
public class clsReservationAircond : Controller
{
    #region "Properties"
    public int ID { get; set; }
    public int aircondID { get; set; }
    public int reservationID { get; set; }

    public string aircondName { get; set; }
    public decimal aircondCharge { get; set; }
    #endregion
    

    public clsReservationAircond()
    {
        _tableName = "tb_reservation_aircond";
        _ID = "id";
        _columns = new List<Model>()
        {
            new Model(typeof(Int32)) { name = "ID", columnName = _ID,  },
            new Model(typeof(Int32)) { name = "aircondID", columnName = "aircond_id", },
            new Model(typeof(Int32)) { name = "reservationID", columnName = "res_id",  },
            new Model(typeof(string)) { name = "aircondName", columnName = "aircond_name",  },
            new Model(typeof(decimal)) { name = "aircondCharge", columnName = "aircond_charge",  },
        };

        resetValue();
    }
}