﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for wsSearchWTGroup
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class wsSearchWTGroup : System.Web.Services.WebService
{

    public wsSearchWTGroup()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string getGroupName()
    {
        clsWorkingConfigTimeslot workingTimeSlot = new clsWorkingConfigTimeslot();
        var arrTimeSlot =  workingTimeSlot.getGroupName().ToArray();
        return string.Join(",", arrTimeSlot);
    }

}
