﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsProperty
/// </summary>
public class clsProperty : Controller
{
    #region "Properties"
    public int ID { get; set; }
    public int memberID { get; set; }
    public int area { get; set; }
    public int type { get; set; }
    public string unitno { get; set; }
    public DateTime creation { get; set; }
    public DateTime lastupdated { get; set; }
    #endregion

    public clsConfigPropertyArea areaObj { get; set; }
    public clsConfigPropertyType typeObj { get; set; }

    private DataTable areas { get; set; }
    private DataTable types { get; set; }

    public clsProperty()
    {
        _tableName = "tb_property";
        _ID = "property_id";
        _createdAt = "property_creation";
        _updatedAt = "property_lastupdated";
        _columns = new List<Model>()
        {
            new Model(typeof(Int32)) { name = "ID", columnName = "property_id",  },
            new Model(typeof(Int32)) { name = "memberID", columnName = "member_id", },
            new Model(typeof(Int32)) { name = "area", columnName = "property_area",  },
            new Model(typeof(Int32)) { name = "type", columnName = "property_type",  },
            new Model(typeof(string)) { name = "unitno", columnName = "property_unitno",  },
            new Model(typeof(DateTime)) { name = "creation", columnName = "property_creation",fillable = false  },
            new Model(typeof(DateTime)) { name = "lastupdated", columnName = "property_lastupdated",fillable = false  },
        };

        resetValue();
    }

    public string[] getArea()
    {
        if (areas == null) { areas = getDataTable(); }
        return areas.AsEnumerable()
                        .Select(x => Misc.FixDBNullTrim(x["property_area"].ToString()))
                        .Distinct()
                        .OrderBy(x => x)
                        .ToArray();
    }

    public string[] getType()
    {
        if (types == null) { types = getDataTable(); }
        return types.AsEnumerable()
                        .Select(x => Misc.FixDBNullTrim(x["property_type"].ToString()))
                        .Distinct()
                        .OrderBy(x => x)
                        .ToArray();
    }
}