﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsVendor
/// </summary>
public class clsMember : Controller
{
    #region "Properties"
    public int ID { get; set; }
    public int active { get; set; }
    public int createdby { get; set; }
    public int updatedby { get; set; }
    public string name { get; set; }
    public string email { get; set; }

    // contact info
    public string contactTel { get; set; }
    public string contactPrefix { get; set; }

    // login info
    public string loginUsername { get; set; }
    public string loginPassword { get; set; }
    public DateTime loginTimestamp { get; set; }

    public DateTime creation { get; set; }
    public DateTime lastupdated { get; set; }
    #endregion

    public clsMember()
    {
        _tableName = "tb_member";
        _ID = "mem_id";
        _columns = new List<Model>()
        {
            new Model(typeof(Int32)) { name = "ID", columnName = "mem_id",  },
            new Model(typeof(Int32)) { name = "createdby", columnName = "mem_createdby", },
            new Model(typeof(Int32)) { name = "updatedby", columnName = "mem_updatedby", },
            new Model(typeof(Int16)) { name = "active", columnName = "mem_active", },
            new Model(typeof(string)) { name = "name", columnName = "mem_name",  },
            new Model(typeof(string)) { name = "email", columnName = "mem_email",  },

            new Model(typeof(string)) { name = "contactTel", columnName = "mem_contact_tel",  },

            new Model(typeof(string)) { name = "contactPrefix", columnName = "mem_tel_prefix",  },
            new Model(typeof(string)) { name = "loginUsername", columnName = "mem_login_username",  },
            new Model(typeof(string)) { name = "loginPassword", columnName = "mem_login_password",  },
            new Model(typeof(DateTime)) { name = "loginTimestamp", columnName = "mem_login_timestamp",  },

            new Model(typeof(DateTime)) { name = "creation", columnName = "mem_creation",fillable = false  },
            new Model(typeof(DateTime)) { name = "lastupdated", columnName = "mem_lastupdated",fillable = false  },

        };

        resetValue();
    }

    public List<clsProperty> getProperties()
    {
        if(ID > 0)
        {
            var areas = new clsConfigPropertyArea().getDataTable()
                .AsEnumerable()
                .Select(x => new clsConfigPropertyArea()
                {
                    row = x
                }).ToList();
            var types = new clsConfigPropertyType().getDataTable()
                .AsEnumerable()
                .Select(x => new clsConfigPropertyType()
                {
                    row = x
                }).ToList();
            var properties = new clsProperty().getDataTable().AsEnumerable()
                                    .Where(x => Convert.ToInt32(x["member_id"]) == ID)
                                    .Select(x => new clsProperty()
                                    {
                                        row = x,
                                        areaObj = areas.Where(a => a.ID == Convert.ToInt32(x["property_area"])).FirstOrDefault(),
                                        typeObj = types.Where(a => a.ID == Convert.ToInt32(x["property_type"])).FirstOrDefault(),
                                    }).ToList();
            
            return properties;
        }
        return new List<clsProperty>();
    }
    public bool resetPassword(int ID, string password)
    {
        int intRecordAffected = 0;

        try
        {
            setValue();
            OdbcTransaction trans;
            OdbcCommand cmd = new OdbcCommand();

            cmd.Connection = openConn();
            trans = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);

            string query = string.Format("UPDATE {0} SET {1},{2} WHERE {3}",
                                        _tableName,
                                        "mem_login_password = ?",
                                        _updatedAt + " = " + "NOW()",
                                        _ID + " = " + "?");

            cmd.Parameters.AddWithValue("mem_login_password", password);
            cmd.Parameters.AddWithValue(_ID, ID);

            cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;

            intRecordAffected = cmd.ExecuteNonQuery();


            if (intRecordAffected == 1)
            {
                trans.Commit();
            }
            else
            {
                trans.Rollback();
            }

            cmd.Dispose();
            trans.Dispose();
        }
        catch (Exception ex)
        {
            logErroMsg(ex.Message.ToString());
            throw ex;
        }
        finally
        {
            closeConn();
        }

        return intRecordAffected == 1;
    }

    public void sendEmailUserLogin(int ID, string password)
    {
        clsConfig config = new clsConfig();
        string subjectPrefix = config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1) ? config.value : "";
        string signature = config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1) ? config.longValue : "";
        string websiteName = config.extractItemByNameGroup(clsConfig.CONSTNAMEWEBSITENAME, clsConfig.CONSTGROUPWEBSITE, 1) ? config.value : "";
        string senderName = config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1) ? config.value : "";

        // custom
        string subject = config.extractItemByNameGroup("New User Email Subject (User)", clsConfig.CONSTGROUPEMAILCONTENT, 1) ? config.value : "";
        string content = config.extractItemByNameGroup("New User Email Content (User)", clsConfig.CONSTGROUPEMAILCONTENT, 1) ? config.longValue : "";
        string subjectAdmin = config.extractItemByNameGroup("New User Email Subject (Admin)", clsConfig.CONSTGROUPEMAILCONTENT, 1) ? config.value : "";
        string contentAdmin = config.extractItemByNameGroup("New User Email Content (Admin)", clsConfig.CONSTGROUPEMAILCONTENT, 1) ? config.longValue : "";

        string name = "";
        string email = "";
        if (extractByID(ID))
        {
            name = this.name;
            email = this.email;
        }

        content = content.Replace("[NAME]", name)
                        .Replace("[LOGINID]", email)
                        .Replace("[PASSWORD]", password)
                        .Replace("[SIGNATURE]", signature);
        contentAdmin = contentAdmin.Replace("[NAME]", name)
                        .Replace("[LOGINID]", email)
                        .Replace("[PASSWORD]", password)
                        .Replace("[SIGNATURE]", signature);

        if (!string.IsNullOrEmpty(subjectPrefix))
        {
            subject = subjectPrefix + " " + subjectPrefix;
        }

        // sent to admin
        clsEmail.send(senderName, "", "", "", "", email, subjectAdmin, contentAdmin, 1);

        // send to user
        clsEmail.send(senderName, "", email, "", "", "", subject, content, 1);

    }

    public override int delete()
    {
        clsProperty property = new clsProperty();
        property.delete("member_id", ID);

        return base.delete();
    }
}
