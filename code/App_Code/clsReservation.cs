﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsVendor
/// </summary>
public class clsReservation : Controller
{
    #region "Properties"
    public int ID { get; set; }
    public int memID { get; set; } // FK
    public string memName { get; set; }
    public int employeeID { get; set; } // FK
    public int createdby { get; set; }
    public int updatedby { get; set; }
    public int resstatus { get; set; }
    public string code { get; set; }
    public string memContactTel { get; set; }
    public string memEmail { get; set; }

    public DateTime creation { get; set; }
    public DateTime lastupdated { get; set; }

    // Service
    public DateTime serviceDatetimeFrom { get; set; }
    public DateTime serviceDatetimeTo { get; set; }
    public int serviceDurationHour { get; set; }
    public int serviceItemQty { get; set; }
    public decimal serviceChargeAmount { get; set; }
    public string serviceTeam { get; set; }
    public string serviceContactTel { get; set; }
    public string serviceRemark { get; set; }

    // property
    public string propertyUnitno { get; set; }
    public int propertyArea { get; set; }
    public int propertyType { get; set; }
    public string propertyAreaName { get; set; }
    public string propertyTypeName { get; set; }

    public clsEmployee employee { get; set; }
    public List<clsReservationAircond> airconds { get; set; }
    #endregion

    public clsReservation()
    {
        _tableName = "tb_reservation";
        _ID = "res_id";
        _columns = new List<Model>()
        {
            new Model(typeof(Int32)) { name = "ID", columnName = "res_id",  },
            new Model(typeof(Int32)) { name = "createdby", columnName = "res_createdby", },
            new Model(typeof(Int32)) { name = "updatedby", columnName = "res_updatedby", },
            new Model(typeof(string)) { name = "code", columnName = "res_code",  },
            
            new Model(typeof(Int32)) { name = "memID", columnName = "mem_id", },
            new Model(typeof(string)) { name = "memName", columnName = "mem_name", },
            new Model(typeof(string)) { name = "memContactTel", columnName = "mem_contact_tel", },
            new Model(typeof(string)) { name = "memEmail", columnName = "mem_email", },
            new Model(typeof(Int32)) { name = "employeeID", columnName = "employee_id", },
            new Model(typeof(Int32)) { name = "resstatus", columnName = "res_status", },

            new Model(typeof(DateTime)) { name = "serviceDatetimeFrom", columnName = "res_service_datetime_from",  },
            new Model(typeof(DateTime)) { name = "serviceDatetimeTo", columnName = "res_service_datetime_to",  },
            new Model(typeof(Int32)) { name = "serviceDurationHour", columnName = "res_service_duration_hour", },
            new Model(typeof(Int32)) { name = "serviceItemQty", columnName = "res_service_item_qty", },
            
            new Model(typeof(string)) { name = "serviceTeam", columnName = "res_service_team", },
            new Model(typeof(string)) { name = "serviceRemark", columnName = "res_service_remark", },
            new Model(typeof(string)) { name = "serviceContactTel", columnName = "res_service_contact_tel", },
            new Model(typeof(decimal)) { name = "serviceChargeAmount", columnName = "res_service_charge_amount", },

            new Model(typeof(string)) { name = "propertyUnitno", columnName = "property_unitno", },
            new Model(typeof(Int32)) { name = "propertyArea", columnName = "property_area", },
            new Model(typeof(Int32)) { name = "propertyType", columnName = "property_type", },
            new Model(typeof(string)) { name = "propertyAreaName", columnName = "property_area_name", },
            new Model(typeof(string)) { name = "propertyTypeName", columnName = "property_type_name", },
            
            new Model(typeof(DateTime)) { name = "creation", columnName = "res_creation",fillable = false  },
            new Model(typeof(DateTime)) { name = "lastupdated", columnName = "res_lastupdated",fillable = false  },

        };

        //_columnsForeign = new List<Model>()
        //{
        //    new Model(typeof(clsEmployee)) { name = "employee", columnName = "employee",  },
        //};
        resetValue();

        airconds = new List<clsReservationAircond>();
    }

    public override DataTable getDataTable()
    {
        DataTable datatable = base.getDataTable();
        datatable.Columns.Add("employee", typeof(clsEmployee));

        DataView employees = new clsEmployee().getDataView();


        foreach (DataRow row in datatable.Rows)
        {
            row["employee"] = new clsEmployee();


            employees.RowFilter = "employee_id = " + row["employee_id"];
            if (employees.Count == 1)
            {
                row["employee"] = new clsEmployee() { row = employees[0].Row };
            }

        }
        return datatable;
    }

    public override bool add()
    {
        bool isSuccess = base.add();
        foreach(var aircond in airconds)
        {
            aircond.reservationID = this.ID;
            aircond.add();
        }

        clsReservation model = new clsReservation();
        if (model.extractByID(this.ID))
        {
            DateTime creation = model.creation;
            model.resetValue();

            model.ID = this.ID;
            model.code = "ORD" + creation.ToString("yyyyMMdd") + this.ID.ToString().PadLeft(4, '0');
            model.update();
        }


        return isSuccess;
    }

    public override bool update()
    {
        clsReservationStatus status = new clsReservationStatus
        {
            ID = -1,
            resID = ID,
            staStatus = resstatus,
        };
        status.add();
        return base.update();
    }

    public string getStatusText()
    {
        clsMis mis = new clsMis();
        var list = mis.getListByListGrp("RESERVATION STATUS").Tables[0].AsEnumerable()
                  .Where(x => Convert.ToInt32(x["LIST_VALUE"]) == resstatus)
                  .Select(x => Misc.FixDBNullTrim(x["LIST_NAME"].ToString()))
                  .ToList();
        if (list.Count > 0)
            return list.FirstOrDefault();
        else
            return "";
    }

    public string getParameters()
    {
        List<string> paremters = new List<object>()
        {
            ID,
            memID , // FK
            memName ,
            employeeID , // FK
            createdby ,
            updatedby ,
            resstatus ,
            code ,
            memContactTel ,
            memEmail ,

            creation ,
            lastupdated ,

            // Service
            serviceDatetimeFrom ,
            serviceDatetimeTo ,
            serviceDurationHour ,
            serviceItemQty ,
            serviceChargeAmount ,
            serviceTeam ,
            serviceContactTel ,
            serviceRemark ,

            // property
            propertyUnitno ,
            propertyArea ,
            propertyType ,
            propertyAreaName ,
            propertyTypeName ,
        }.Select(x => (x ?? "").ToString()).ToList();

        return string.Join(";", paremters);
    }
}

public class Reservation
{

    // Custom
    public clsReservation config { get; set; }
    public int step { get; set; }

    public Reservation()
    {
        config = new clsReservation();
        step = 1;
    }
}