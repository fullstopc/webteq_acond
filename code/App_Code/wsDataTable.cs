﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using Newtonsoft.Json;
using System.Web.Script.Services;
using Newtonsoft.Json.Linq;
using System.Xml.Linq;
using System.Reflection;

/// <summary>
/// Summary description for wsFileUpload
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class wsDataTable : System.Web.Services.WebService {

    public wsDataTable() {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public DataTableJQuery getData(DataTableJQuerySearch search, 
                                    List<DataTableJQueryColumn> columns, 
                                    List<DatatTableJqueryOrder> order, 
                                    List<Dictionary<string, string>> searchCustom,
                                    int draw, int start, int length, string func)
    {
        // format data
        Dictionary<string, string> keyword = null;
        Dictionary<string, string> filterLike = null;
        Dictionary<string, string> filterAnd = null;
        Dictionary<string, string> filterDateRange = null;

        string sorting = null;
        formatFilter(search,searchCustom, columns, order,  out keyword, out filterLike, out filterAnd, out filterDateRange, out sorting);

        int recordFilter = 0;
        int recordTotal = 0;

        DataTable datatable = (DataTable)this.GetType().GetMethod(func).Invoke(this, new object[] { columns });
        DataTable datatableNew = performFilter(datatable, keyword, filterLike, filterAnd, filterDateRange, sorting, start, length, out recordFilter, out recordTotal);

        // Additional Datatable columns
        datatableNew.Columns.Add("index");
        for (int i = 0; i < datatableNew.Rows.Count; i++)
        {
            datatableNew.Rows[i]["index"] = i + 1;
        }
        return formatReturn(draw, recordFilter, recordTotal, datatableNew);
    }

    private void formatFilter(DataTableJQuerySearch search, 
                            List<Dictionary<string, string>> searchCustom, 
                            List<DataTableJQueryColumn> columns, 
                            List<DatatTableJqueryOrder> order,
                           out Dictionary<string, string> keyword, out Dictionary<string, string> filterLike, out Dictionary<string, string> filterAnd, out Dictionary<string, string> filterDateRange, out string sorting)
    {
        keyword = null;
        if (!string.IsNullOrEmpty(search.value))
        {
            keyword = columns.AsEnumerable().Where(x => x.searchable).ToDictionary(x => x.data, x => search.value);
        }


        filterLike = null;
        var columnsSearch = columns.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.search.value) && string.IsNullOrEmpty(x.search.columnSearch));
        if (columnsSearch.Count() > 0)
        {
            filterLike = columnsSearch.ToDictionary(x => !string.IsNullOrEmpty(x.searchColumn) ? x.searchColumn : x.data, x => x.search.value);
        }

        filterAnd = null;
        //var columnsSearchAnd = columns.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.search.value) && !string.IsNullOrEmpty(x.search.columnSearch));
        //if (columnsSearchAnd.Count() > 0)
        //{
        //    filterAnd = columnsSearchAnd.ToDictionary(x => x.search.columnSearch, x => x.search.value);
        //}

        filterDateRange = null;
        if (searchCustom != null && searchCustom.Count > 0)
        {
            // daterange filter
            var dateranges = searchCustom.AsEnumerable().Where(x => x["type"].ToString() == "daterange");
            if (dateranges.Any())
            {
                filterDateRange = dateranges.Select((x, i) => new { Index = i, Value = x })
                    .ToDictionary(x => "column_" + x.Index, x=> x.Value["column_from"] + "|" + x.Value["column_to"] + "|" + 
                                                                x.Value["date_from"] + "|" + x.Value["date_to"]);
            }
        }


        sorting = null;
        if (order != null && order.Count > 0)
        {
            sorting = columns[order[0].column].data + " " + order[0].dir;
        }
    }
    private DataTable performFilter(DataTable datatable, 
                                    Dictionary<string, string> keyword,
                                    Dictionary<string, string> filterLike,
                                    Dictionary<string, string> filterAnd,
                                    Dictionary<string, string> filterDateRange,
                                    string sorting, int start, int length,
                                    out int recordFilter, out int recordTotal)
    {
        recordTotal = datatable.Rows.Count;

        DataView dv = new DataView(datatable);
        dv.RowFilter = "1 = 1";

        if (keyword != null)
        {
            List<string> temp = new List<string>();
            foreach (KeyValuePair<string, string> entry in keyword)
            {
                temp.Add(entry.Key + " LIKE  '%" + entry.Value + "%'");
            }
            dv.RowFilter += " AND (" + string.Join(" OR ", temp.ToArray()) + ")";
        }

        if (filterLike != null)
        {
            foreach (KeyValuePair<string, string> entry in filterLike)
            {
                dv.RowFilter += " AND CONVERT(" + entry.Key + ", System.String) LIKE '%" + entry.Value + "%'";
            }
        }

        if (filterAnd != null)
        {
            foreach (KeyValuePair<string, string> entry in filterAnd)
            {
                dv.RowFilter += " AND " + entry.Key + " = '" + entry.Value + "'";
            }
        }

        if (filterDateRange != null)
        {
            foreach (KeyValuePair<string, string> entry in filterDateRange)
            {
                string[] datas = entry.Value.Split('|');
                if(datas.Length == 4)
                {
                    string column_from = datas[0];
                    string column_to = datas[1];
                    string date_from = datas[2];
                    string date_to = datas[3];

                    dv.RowFilter += " AND #" + date_from + "# <=  " + column_from +
                                    " AND #" + date_to + "# >=  " + column_to;
                }

                
            }


        }

        dv.Sort = sorting != null ? sorting : "";

        recordFilter = dv.Count;

        DataTable datatableReturn = dv.ToTable();
        if(length > 0 && datatableReturn.Rows.Count > 0)
        {
            datatableReturn = datatableReturn.AsEnumerable().Skip(start).Take(length).CopyToDataTable();
        }

        return datatableReturn;
    }
    private DataTableJQuery formatReturn(int draw, int recordsFiltered, int recordsTotal, DataTable data)
    {
        DataTableJQuery jsDatatable = new DataTableJQuery();
        jsDatatable.draw = draw;
        jsDatatable.recordsFiltered = recordsFiltered;
        jsDatatable.recordsTotal = recordsTotal;
        jsDatatable.data = JsonConvert.SerializeObject(data);
        return jsDatatable;
    }

    #region Custom
    public DataTable getMasterUserData(List<DataTableJQueryColumn> columns) { return new clsUser().getUserList(0).Tables[0]; }
    public DataTable getProjectData(List<DataTableJQueryColumn> columns) { return new clsProject().getProjectList(0).Tables[0]; }
    public DataTable getSalesData(List<DataTableJQueryColumn> columns) { return new clsOrder().getOrderList2(0).Tables[0]; }
    public DataTable getProductData(List<DataTableJQueryColumn> columns) { return new clsProduct().getProdList(0).Tables[0]; }
    public DataTable getPageData(List<DataTableJQueryColumn> columns) { return new clsPage().getPageDataTables().Tables[0]; }
    public DataTable getSlideGroupData(List<DataTableJQueryColumn> columns) { return new clsMasthead().getGrpList(0).Tables[0]; }
    public DataTable getHighlightData(List<DataTableJQueryColumn> columns) { return new clsHighlight().getHighlightList(0).Tables[0]; }
    public DataTable getEnquiryData(List<DataTableJQueryColumn> columns) { return new clsEnquiry().getUserEnquiry(0).Tables[0]; }
    public DataTable getObjectData(List<DataTableJQueryColumn> columns) { return new clsCMSObject().getCMSList(0).Tables[0]; }
    public DataTable getPropertyArea(List<DataTableJQueryColumn> columns) { return new clsConfigPropertyArea().getDataTable(); }
    public DataTable getPropertyType(List<DataTableJQueryColumn> columns)
    {
        var airconds = new clsConfigPropertyTypeAircond().getDataTable();
        var propertyTypes = new clsConfigPropertyType().getDataTable();
        propertyTypes.Columns.AddRange(new DataColumn[]
        {
            new DataColumn("type_airconds"),
        });

        foreach(DataRow propertType in propertyTypes.Rows)
        {
            var propertyTypeID = propertType["type_id"].ToInteger();
            var propertyTypeAirconds = string.Join(",", airconds.AsEnumerable().Where(x => x["type_id"].ToInteger() == propertyTypeID)
                                                          .Select(x => x["aircond_name"].ToString())
                                                          .ToArray());

            propertType["type_airconds"] = propertyTypeAirconds;

        }
        return propertyTypes;

    }


    #endregion Custom

    public DataTable getMemberData(List<DataTableJQueryColumn> columns) {
        DataTable members = new clsMember().getDataTable();
        members.Columns.Add("mem_property");

        DataTable properties = new clsProperty().getDataTable();
        foreach (DataRow member in members.Rows)
        {
            var rows = properties.AsEnumerable().Where(x => Convert.ToInt32(member["mem_id"]) == Convert.ToInt32(x["member_id"]));
            if (rows.Any())
            {
                member["mem_property"] = JsonConvert.SerializeObject(rows.CopyToDataTable());
            }
        }
        return members;
    }

    public DataTable getAgentData(List<DataTableJQueryColumn> columns)
    {
        var employees = new clsEmployee().getDataTable();
        var employeeLeaves = new clsEmployeeLeave().getDataView();

        employees.Columns.AddRange(new DataColumn[] {
            new DataColumn("employee_leave_flag"),
        });

        foreach(DataRow employee in employees.Rows)
        {
            employeeLeaves.RowFilter = "employee_id = '" + employee["employee_id"] + "'";
            employee["employee_leave_flag"] = employeeLeaves.Count > 0 ? 1 : 0;

        }
        return employees;
    }
    public DataTable getWorkingConfigTime(List<DataTableJQueryColumn> columns) { return new clsWorkingConfigTimeslot().getDataTable(); }
    public DataTable getWorkingConfigHoliday(List<DataTableJQueryColumn> columns) { return new clsWorkingConfigHoliday().getDataTable(); }
    public DataTable getResidentProperty(List<DataTableJQueryColumn> columns)
    {
        var properties = new clsProperty().getDataTable();
        properties.Columns.AddRange(new DataColumn[] {
            new DataColumn("property_area_name"),
            new DataColumn("property_type_name")
        });

        var areas = new clsConfigPropertyArea().getDataView();
        var types = new clsConfigPropertyType().getDataView();

        foreach(DataRow property in properties.Rows)
        {
            var propertyID = property["property_id"].ToInteger();
            var propertyArea = property["property_area"].ToInteger();
            var propertyType = property["property_type"].ToInteger();
            var propertyAreaName = "";
            var propertyTypeName = "";

            areas.RowFilter = "area_id = '" + propertyArea + "'";
            if (areas.Count == 1)
                propertyAreaName = areas[0]["area_name"].ToString();

            types.RowFilter = "type_id = '" + propertyType + "'";
            if (types.Count == 1)
                propertyTypeName = types[0]["type_name"].ToString();

            property["property_area_name"] = propertyAreaName;
            property["property_type_name"] = propertyTypeName;
        }

        return properties;

    }

    public DataTable getReservation(List<DataTableJQueryColumn> columns) { 
        var reservations = new clsReservation().getDataTable();
        reservations.Columns.Add("order_status");

        clsMis mis = new clsMis();
        var status = mis.getListByListGrp("RESERVATION STATUS").Tables[0];

        foreach (DataRow reservation in reservations.Rows)
        {
            var rows = status.AsEnumerable()
                             .Where(x => Convert.ToInt32(reservation["res_status"]) == Convert.ToInt32(x["LIST_VALUE"]))
                             .Select(x => Misc.FixDBNullTrim(x["LIST_NAME"].ToString()));
            if (rows.Any())
            {
                reservation["order_status"] = rows.FirstOrDefault();
            }
        }
        return reservations;
    }
    public DataTable getAgentLeave(List<DataTableJQueryColumn> columns)
    {
        var leaves = new clsEmployeeLeave().getDataTable();
        return leaves;
    }

    
    [WebMethod(EnableSession = true)]
    public DataTableJQuery getSalesProductData(DataTableJQuerySearch search,
                                List<DataTableJQueryColumn> columns,
                                List<DatatTableJqueryOrder> order,
                                List<Dictionary<string, string>> searchCustom,
                                int draw, int start, int length)
    {
        // format data
        Dictionary<string, string> keyword = null;
        Dictionary<string, string> filterLike = null;
        Dictionary<string, string> filterAnd = null;
        Dictionary<string, string> filterDateRange = null;

        string sorting = null;
        formatFilter(search, searchCustom, columns, order, out keyword, out filterLike, out filterAnd, out filterDateRange, out sorting);

        int recordFilter = 0;
        int recordTotal = 0;

        DataTableJQueryColumn columnStatus = columns.Where(x => x.data == "ORDER_STATUS").First();
        int status = 0;
        if (!string.IsNullOrEmpty(columnStatus.search.value))
        {
            status = Convert.ToInt16(columnStatus.search.value);
            columnStatus.search.value = null;
        }

        // get Date range
        string dateFrom = "1900-01-01 00:00:00";
        string dateTo = "9999-12-31 23:59:59";
        if(searchCustom != null)
        {
            dateFrom = searchCustom[0]["date_from"];
            dateTo = searchCustom[0]["date_to"];
            filterDateRange = null;
        }

        DataTable datatable = new clsOrder().getOrderItems(dateFrom, dateTo, 0, status, 0).Tables[0];
        DataTable datatableNew = performFilter(datatable, keyword, filterLike, filterAnd, filterDateRange, sorting, start, length, out recordFilter, out recordTotal);

        // Additional Datatable columns
        datatableNew.Columns.Add("index");
        for (int i = 0; i < datatableNew.Rows.Count; i++)
        {
            datatableNew.Rows[i]["index"] = i + 1;
        }
        return formatReturn(draw, recordFilter, recordTotal, datatableNew);
    }
}

public class DataTableJQuery
{
    public int draw { get; set; }
    public int recordsFiltered { get; set; }
    public int recordsTotal { get; set; }
    public string data { get; set; }
}

public class DataTableJQuerySearch
{
    public string value { get; set; }
    public string columnSearch { get; set; }
}

public class DataTableJQueryColumn
{
    public DataTableJQuerySearch search { get; set; }
    public bool searchable { get; set; }
    public string searchColumn { get; set; }
    public string data { get; set; }
    public string convert { get; set; }
}

public class DatatTableJqueryOrder
{
    public int column { get; set; }
    public string dir { get; set; }
}