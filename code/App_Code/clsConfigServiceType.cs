﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;

/// <summary>
/// Summary description for clsVendor
/// </summary>
public class clsConfigServiceType : Controller
{
    #region "Properties"
    public int ID { get; set; }
    public int createdby { get; set; }
    public int updatedby { get; set; }

    public decimal duration { get; set; }
    public decimal charge { get; set; }

    public string name { get; set; }

    public DateTime creation { get; set; }
    public DateTime lastupdated { get; set; }

    #endregion
    public clsConfigServiceType()
    {
        _tableName = "tb_config_service_type";
        _prefix = "type_";
        _ID = _prefix + "id";
        _columns = new List<Model>()
        {
            new Model(typeof(Int32)) { name = "ID", columnName = _ID },
            new Model(typeof(Int32)) { name = "createdby", columnName = _prefix + "createdby", },
            new Model(typeof(Int32)) { name = "updatedby", columnName = _prefix + "updatedby", },

            new Model(typeof(decimal)) { name = "duration", columnName = _prefix + "duration", },
            new Model(typeof(decimal)) { name = "charge", columnName = _prefix + "charge", },

            new Model(typeof(string)) { name = "name", columnName = _prefix + "name",  },
            
            new Model(typeof(DateTime)) { name = "creation", columnName = _prefix + "creation",fillable = false  },
            new Model(typeof(DateTime)) { name = "lastupdated", columnName = _prefix + "lastupdated",fillable = false  },
        };

        resetValue();
    }
}
