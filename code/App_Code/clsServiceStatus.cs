﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsServiceStatus
/// </summary>
public class clsServiceStatus : Controller
{
    #region "Properties"
    public int ID { get; set; }
    public int resID { get; set; }
    public int staStatus { get; set; }
    public DateTime staDateTime { get; set; }
    #endregion

    private DataTable datatable { get; set; }

    public clsServiceStatus()
    {
        _tableName = "tb_service_status";
        _ID = "sta_id";
        _columns = new List<Model>()
        {
            new Model(typeof(Int32)) { name = "ID", columnName = "sta_id",  },
            new Model(typeof(Int32)) { name = "resID", columnName = "res_id", },
            new Model(typeof(Int32)) { name = "staStatus", columnName = "sta_status",  },
            new Model(typeof(DateTime)) { name = "staDateTime", columnName = "sta_datetime",  },
        };

        resetValue();
    }

    public ServiceStatus getLastStatus(int resId)
    {
        if (datatable == null) { datatable = getDataTable(); }
        var items = datatable.AsEnumerable()
                        .Where(x => Convert.ToInt32(x["res_id"]) == resId)
                        .Select(x => new ServiceStatus
                        {
                            staStatus = Convert.ToInt32(x["sta_status"] ?? -1),
                            staDateTime = Convert.ToDateTime(x["sta_datetime"] ?? DateTime.MinValue),

                        });
        if (items.Count() > 0)
        {
            return items.OrderByDescending(x => x.staDateTime).FirstOrDefault();
        }
        else
        {
            return null;
        }
    }
}

public class ServiceStatus : clsServiceStatus
{
    public string StatusInText
    {
        get
        {
            var mis = new clsMis();
            var paymentType = mis.getListByListGrp("SERVICE STATUS").Tables[0].AsEnumerable()
                             .Where(x => staStatus == Convert.ToInt32(x["LIST_VALUE"]))
                             .Select(x => Misc.FixDBNullTrim(x["LIST_NAME"].ToString()));
            if (paymentType.Count() > 0)
            {
                return paymentType.FirstOrDefault();
            }
            else
            {
                return "";
            }
        }
    }
}