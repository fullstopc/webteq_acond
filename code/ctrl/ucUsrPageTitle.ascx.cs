﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ctrl_ucUsrPageTitle : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _pageId;
    protected int _prodId;
    protected int _grpId;
    protected int _grpingId;
    protected string pageTitle = "";
    protected string grpingName = "";
    #endregion

    #region "Property Methods"
    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public int prodId
    {
        get { return _prodId; }
        set { _prodId = value; }
    }

    public int grpId
    {
        get { return _grpId; }
        set { _grpId = value; }
    }

    public int grpingId
    {
        get { return _grpingId; }
        set { _grpingId = value; }
    }

    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.LoadComplete += new EventHandler(Page_LoadComplete);
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        clsConfig config = new clsConfig();
        clsMis Mis = new clsMis();
        clsPage page = new clsPage();
        string strDefaultTitle = config.defaultTitle;

        Boolean boolShowTitle = false;

        if (page.extractPageById_Page(pageId, 0))
        {
            boolShowTitle = page.showPageTitle > 0 ? true : false;
        }

        if (pageId > 0)
        {
            pageTitle = page.title;
        }

        if (boolShowTitle)
        {
            if (Session["ADMID"] != null && Session["ADMID"] != "")
            {
                Page.Title += clsMis.formatPageTitle((!string.IsNullOrEmpty(pageTitle) ? pageTitle.Replace("<br />", " ").Replace("<br/>", " ") : strDefaultTitle), true);
            }
            else
            {
                Page.Title += clsMis.formatPageTitle((!string.IsNullOrEmpty(pageTitle) ? pageTitle.Replace("<br />", " ").Replace("<br/>", " ") : strDefaultTitle), false);
            }
        }
        else
        {
            if (Session["ADMID"] != null && Session["ADMID"] != "")
            {
                Page.Title += clsMis.formatPageTitle(strDefaultTitle, true);
            }
            else
            {
                Page.Title += clsMis.formatPageTitle(strDefaultTitle, false);
            }
        }
    }
    #endregion
}
