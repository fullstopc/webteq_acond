<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrForgotPassword.ascx.cs" Inherits="ctlr_ucUsrForgotPassword" %>

<asp:Panel ID="pnlAck" runat="server" CssClass="divAck" Visible="false">
    <asp:Literal ID="litAck" runat="server"></asp:Literal>
</asp:Panel>
<asp:Panel ID="pnlForgotPasswordForm" runat="server" DefaultButton="imgbtnSubmit">
    <div class="row">
        <div class="col-sm-12">
            <div class="form-label">
                <asp:Label ID="lblEmail" runat="server" meta:resourcekey="lblEmail">Emel</asp:Label><span class="attention_compulsory">*</span>:
            </div>
            <div class="form-input">
                <asp:TextBox ID="txtEmail" runat="server" MaxLength="250" CssClass="text_medium" ValidationGroup="password"></asp:TextBox>
            </div>
            <div class="form-error">
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Sila masukkan emel" ControlToValidate="txtEmail" Display="Dynamic" CssClass="errmsg" ValidationGroup="password"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Sila masukkan emel yang sah" ControlToValidate="txtEmail" Display="Dynamic" CssClass="errmsg" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="password"></asp:RegularExpressionValidator>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12" style="text-align:center;">
            <asp:LinkButton ID="imgbtnSubmit" runat="server" ToolTip="<%$ Resources:GlobalResource, btnSubmit %>" Text="<%$ Resources:GlobalResource, btnSubmit %>" CssClass="lnkbtn" OnClick="imgbtnSubmit_Click" ValidationGroup="password"></asp:LinkButton>
        </div>
    </div>
</asp:Panel>