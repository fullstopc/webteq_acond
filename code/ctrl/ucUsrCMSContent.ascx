﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrCMSContent.ascx.cs" Inherits="ctrl_ucUsrCMSContent" %>
<%@ Register Src="~/ctrl/ucUsrMasthead.ascx" TagName="UsrMasthead" TagPrefix="uc" %>

<asp:Panel ID="pnlCMSContent" runat="server" CssClass="divCMSContentOuter1">
    <asp:Panel ID="pnlAck" runat="server" CssClass="divAck" Visible="false">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlCMSContentInner" runat="server" CssClass="divCMSContentInner">
        <asp:Panel ID="pnlCMSContentContent" runat="server" CssClass="divCMSContentContent" Visible="false">
            <asp:Literal ID="litCMSContent" runat="server"></asp:Literal>
        </asp:Panel>
        <asp:Panel ID="pnlCMSContentSlideShow" runat="server" CssClass="divCMSContentSlideShow" Visible="false">
            <uc:UsrMasthead ID="ucUsrMasthead" runat="server" />
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlCMSAction" runat="server" CssClass="divCMSContentAction">
       <asp:Literal ID="litTitle" runat="server"></asp:Literal>
       <asp:LinkButton ID="lnkbtnDelete" runat="server" meta:resourcekey="lnkbtnDelete" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
    </asp:Panel>
</asp:Panel>