﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmProgramGroupDesc.ascx.cs" Inherits="ctrl_ucAdmProgramGroupDesc" %>

<asp:Panel ID="pnlProgGroupDesc" runat="server">
    <asp:Panel ID="pnlProgGroupDescInner" runat="server">
        <table cellpadding="0" cellspacing="0" border="0" class="tblData formTbl">
            <tr>
                <td colspan="2" class="tdSectionHdr"><asp:Literal ID="litProgGroupContent" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td class="tdSpacer"></td>
                <td class="tdSpacer"></td>
            </tr>
            <tr>
                <td class="tdLabel nobr"><asp:Label ID="lblProgGroupDesc" runat="server"></asp:Label>:</td>
                <td class="tdMax"><asp:TextBox ID="txtProgCatDesc" runat="server" CssClass="text_big" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
            </tr>
            <tr id="trProgGroupDescZh" runat="server" visible="false">
                <td class="tdLabel nobr"><asp:Label ID="lblProgGroupDescZh" runat="server"></asp:Label>:</td>
                <td class="tdMax"><asp:TextBox ID="txtProgCatDescZh" runat="server" CssClass="text_big" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>