﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class ctrl_ucAdmMobileView : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected int _pageId = 0;
    protected string _pageListingURL = "";

    protected int intShowMobileView = 0;
    protected int intShowInMenu = 0;
    protected int intShowTop = 0;
    protected int intShowBottom = 0;
    protected string strCssClass = "";
    protected string strTemplate;
    protected string strPageContent;
    protected int intRedirect;
    protected string strRedirectLink;
    protected string strTarget;
    protected string _redirectTarget = "";
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName2(true); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int pageId
    {
        get
        {
            if (ViewState["PAGEID"] == null)
            {
                return _pageId;
            }
            else
            {
                return int.Parse(ViewState["PAGEID"].ToString());
            }
        }
        set { ViewState["PAGEID"] = value; }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }

    public int redirect
    {
        get { return !string.IsNullOrEmpty(ddlRedirect.SelectedValue) ? Convert.ToInt16(ddlRedirect.SelectedValue) : 0; }
        set
        {
            if (value > 0) { ddlRedirect.SelectedValue = value.ToString(); }

            if (ddlRedirect.SelectedValue == clsAdmin.CONSTEXTERNALLINK.ToString())
            {
                pnlExtLink.Visible = true;
            }
        }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void chkboxShowInMenu_CheckedChanged(object sender, EventArgs e)
    {
        pnlShowInMenu.Visible = chkboxShowInMenu.Checked;
    }
    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (mode == 2)
            {
                Boolean boolEdited = false;
                Boolean boolSuccess = true;
                clsPage page = new clsPage();
                int intRecordAffected = 0;

                intShowMobileView = chkboxShowMobileView.Checked ? 1 : 0;
                intShowInMenu = chkboxShowInMenu.Checked ? 1 : 0;
                intShowTop = chkboxTopMenu.Checked ? 1 : 0;
                intShowBottom = chkboxBottomMenu.Checked ? 1 : 0;
                strCssClass = !string.IsNullOrEmpty(ddlCssClass.SelectedValue) ? ddlCssClass.SelectedValue : "";
                strTemplate = !string.IsNullOrEmpty(ddlTemplate.SelectedValue) ? ddlTemplate.SelectedValue : "";
                strPageContent = !string.IsNullOrEmpty(txtPageContent.Text.Trim()) ? txtPageContent.Text.Trim() : "";
                intRedirect = !string.IsNullOrEmpty(ddlRedirect.SelectedValue) ? Convert.ToInt16(ddlRedirect.SelectedValue) : 0;
                strRedirectLink = txtLink.Text.Trim();
                strTarget = ddlTarget.SelectedValue;

                if (boolEdited || !page.isExactSameDataSet3(pageId, intShowMobileView, intShowInMenu, intShowTop, intShowBottom, strCssClass, strTemplate, strPageContent, intRedirect, strRedirectLink, strTarget))
                {
                    intRecordAffected = page.updatePageById3(pageId, intShowMobileView, intShowInMenu, intShowTop, intShowBottom, strCssClass, strTemplate, strPageContent, intRedirect, strRedirectLink, strTarget, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1)
                    {
                        boolEdited = true;
                    }
                    else
                    {
                        boolSuccess = false;
                    }
                }

                if (boolEdited)
                {
                    Session["EDITEDPAGEID"] = page.id;
                }
                else
                {
                    if (!boolSuccess)
                    {
                        page.extractPageById(pageId, 0);
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit page (" + page.displayName + "). Please try again.</div>";
                    }
                    else
                    {
                        Session["EDITEDPAGEID"] = pageId;
                        Session["NOCHANGE"] = 1;
                    }
                }

                Response.Redirect(Request.Url.ToString());
            }
        }
    }

    protected void lnkbtnBackup_Click(object sender, EventArgs e)
    {
        int intRecordAffected = 0;
        clsPage page = new clsPage();

        if (page.extractPageById(pageId, 0))
        {
            Boolean boolSave = true;
            string strBackupCont = "";
            int intTotalBackupPages = page.getTotalBackupPages(pageId, clsAdmin.CONSTMOBILEVIEW);
            if (intTotalBackupPages > 0)
            {
                strBackupCont = page.getLatestPageContent(pageId, clsAdmin.CONSTMOBILEVIEW);
                if (strBackupCont.Equals(page.mobileContent))
                {
                    boolSave = false;
                }
            }

            if (boolSave == true)
            {
                if (intTotalBackupPages >= 50)
                {
                    intRecordAffected = page.deleteOldPageBackup(pageId, clsAdmin.CONSTMOBILEVIEW);
                }
                else
                {
                    intRecordAffected = 1;
                }

                if (intRecordAffected == 1)
                {
                    intRecordAffected = 0;
                    intRecordAffected = page.addPageBackup(page.id, page.mobileContent, clsAdmin.CONSTMOBILEVIEW, Convert.ToInt16(Session["ADMID"]));
                    Response.Write("<script>alert('Successfully Backup.');</script>");
                }
                else
                {
                    Response.Write("<script>alert('Cannot backup this version.');</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('This version was backup previously.');</script>");
            }
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }

    protected void ddlRedirect_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlRedirect.SelectedValue == clsAdmin.CONSTEXTERNALLINK.ToString())
        {
            pnlExtLink.Visible = true;
        }
        else
        {
            pnlExtLink.Visible = false;
        }
    }

    protected void ddlTarget_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    #endregion

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            if (mode == 2)
            {
                setPageProperties();
                fillForm();
            }
        }

        lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
        registerCKEditorScript();
    }

    public void registerCKEditorScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR")))
        {
            int intFullPath = Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFULLPATH]) == 1 ? clsAdmin.CONSTFULLPATHACTIVE : clsAdmin.CONSTFULLPATHDEFAULT;
            string strJS = null;
            strJS = "<script type=\"text/javascript\">" +
                    "CKEDITOR.replace('" + txtPageContent.ClientID + "'," +
                    "{" +
                    "width:'99%'," +
                    "height:'300'," +
                    "toolbar: 'User3'," +
                    "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                     "}" +
                     ");";

            strJS += "CKEDITOR.add;";

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMEEDITORSTYLE, clsConfig.CONSTGROUPWEBSITE, 1);
            string strEditorStyle = config.value;
            if (!string.IsNullOrEmpty(strEditorStyle))
            {
                strJS += "CKEDITOR.config.contentsCss = 'body {" + strEditorStyle + "}';";
            }

            strJS += "CKEDITOR.config.stylesCombo_stylesSet= '" + clsAdmin.CONSTADMSTYLESET + ":" + System.Configuration.ConfigurationManager.AppSettings["jsBase"] + clsAdmin.CONSTADMSTYLESETFILE + "';" +
                     "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR", strJS, false);
        }
    }

    protected void setPageProperties()
    {
        lnkbtnRestore.Attributes["href"] = ConfigurationManager.AppSettings["scriptBase"] + "adm/admPageBackupList.aspx?id=" + pageId + "&type=" + clsAdmin.CONSTMOBILEVIEW + "&" + clsAdmin.CONSTUSRARTICLETHICKBOX;

        clsMis mis = new clsMis();
        DataSet ds = new DataSet();
        ds = mis.getListByListGrp("MOBILE PAGE TEMPLATE", 1);
        DataView dv = new DataView(ds.Tables[0]);
        dv.Sort = "LIST_ORDER ASC";

        ddlTemplate.DataSource = dv;
        ddlTemplate.DataTextField = "LIST_NAME";
        ddlTemplate.DataValueField = "LIST_VALUE";
        ddlTemplate.DataBind();

        ListItem ddlTemplateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlTemplate.Items.Insert(0, ddlTemplateDefaultItem);

        dv = new DataView(mis.getListByListGrp("MOBILE TOP MENU CSS", 1).Tables[0]);
        dv.Sort = "LIST_ORDER ASC";

        ddlCssClass.DataSource = dv;
        ddlCssClass.DataTextField = "LIST_NAME";
        ddlCssClass.DataValueField = "LIST_VALUE";
        ddlCssClass.DataBind();

        ListItem ddlCssClassDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlCssClass.Items.Insert(0, ddlCssClassDefaultItem);

        clsPage page = new clsPage();
        ds = new DataSet();
        ds = page.getPageList(0);
        dv = new DataView(ds.Tables[0]);
        dv.RowFilter = "1 = 1";

        if (pageId > 0) { dv.RowFilter += " AND PAGE_ID <> " + pageId; }

        dv.RowFilter += " AND PAGE_SHOWMOBILEVIEW = 1 AND PAGE_OTHER = 0 ";
        dv.Sort = "PAGE_TITLE ASC";

        ddlRedirect.DataSource = dv;
        ddlRedirect.DataTextField = "PAGE_DISPLAYNAME";
        ddlRedirect.DataValueField = "PAGE_ID";
        ddlRedirect.DataBind();

        ListItem ddlRedirectDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlRedirect.Items.Insert(0, ddlRedirectDefaultItem);

        ListItem ddlRedirectDefaultItem1 = new ListItem("External Link", "9999");
        ddlRedirect.Items.Insert(1, ddlRedirectDefaultItem1);

        ds = new DataSet();
        ds = mis.getListByListGrp("TARGET", 1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "LIST_VALUE ASC";

        ddlTarget.DataSource = dv;
        ddlTarget.DataTextField = "LIST_NAME";
        ddlTarget.DataValueField = "LIST_VALUE";
        ddlTarget.DataBind();



        pnlShowInMenu.Visible = chkboxShowInMenu.Checked;
    }

    protected void fillForm()
    {
        clsPage page = new clsPage();

        if (page.extractPageById(pageId, 0))
        {

            chkboxShowMobileView.Checked = (page.showMobileView == 1) ? true : false;
            ddlTemplate.SelectedValue = (!string.IsNullOrEmpty(page.mobileTemplate)) ? page.mobileTemplate : "";
            ddlCssClass.SelectedValue = (!string.IsNullOrEmpty(page.mobileCssClass)) ? page.mobileCssClass : "";
            chkboxShowInMenu.Checked = (page.mobileShowInMenu == 1) ? true : false;
            chkboxTopMenu.Checked = (page.mobileShowTop == 1) ? true : false;
            chkboxBottomMenu.Checked = (page.mobileShowBottom == 1) ? true : false;

            txtPageContent.Text = page.mobileContent;

            lnkbtnRestore.ToolTip = "Restore page content - " + page.displayName;

            if (page.mobileRedirect > 0) { ddlRedirect.SelectedValue = page.mobileRedirect.ToString(); }

            if (ddlRedirect.SelectedValue == clsAdmin.CONSTEXTERNALLINK.ToString())
            {
                pnlExtLink.Visible = true;
            }

            txtLink.Text = page.mobileExternalLink;
            ddlTarget.SelectedValue = page.mobileTarget;
        }
    }
    #endregion
}
