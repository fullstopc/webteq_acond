﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrCMSContainer.ascx.cs" Inherits="ctrl_ucUsrCMSContainer" %>
<%@ Register Src="~/ctrl/ucUsrCMSContent.ascx" TagName="UsrCMSContent" TagPrefix="uc" %>

<asp:Panel ID="pnlCMSContainer" runat="server" CssClass="divCMSContainerOuter">
    <asp:Panel ID="pnlCMSContainerEdit" runat="server" CssClass="divCMSContainerEdit">
        <div class="divCMSContainerEditTitle"><asp:Literal ID="litTitle" runat="server"></asp:Literal></div>
        <div class="divCMSContainerEditLastUpdate"><asp:Literal ID="litLastUpdate" runat="server"></asp:Literal></div>
    </asp:Panel>
    <asp:Panel ID="pnlCMSContainerInner" runat="server" CssClass="divCMSContainerInner">
        <asp:Repeater ID="rptCMSContent" runat="server">
            <ItemTemplate>
                <asp:Panel ID="pnlCMSContent" runat="server">
                    <%--<uc:UsrCMSContent ID="ucUsrCMSContent" runat="server" pageId='<%# Convert.ToInt16(DataBinder.Eval(Container.DataItem, "PAGE_ID")) %>' customId='<%# Convert.ToInt16(DataBinder.Eval(Container.DataItem, "CUSTOM_ID")) %>' customId2='<%# Convert.ToInt16(DataBinder.Eval(Container.DataItem, "CUSTOM_ID_2")) %>' blockId='<%# Convert.ToInt16(DataBinder.Eval(Container.DataItem, "BLOCK_ID")) %>' cmsId='<%# Convert.ToInt16(DataBinder.Eval(Container.DataItem, "CMS_ID")) %>' cmsType='<% #Convert.ToInt16(DataBinder.Eval(Container.DataItem, "PAGECMS_TYPE")) %>' cmsGroup='<%# Convert.ToInt16(DataBinder.Eval(Container.DataItem, "PAGECMS_GROUP")) %>' cmsContent='<% #DataBinder.Eval(Container.DataItem, "V_CMSCONTENT") %>' cmsCSSType='<%# SetCMSCSSTypeId(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "PAGECMS_TYPE"))) %>' lang='<%# DataBinder.Eval(Container.DataItem, "PAGECMS_LANG") %>' boolAllowUpdate='<%# setBoolAllowUpdate() %>' adminView='<%# setIsAdminView() %>' height='<%# setHeight() %>' width='<%# setWidth() %>' />--%>
                    <uc:UsrCMSContent ID="ucUsrCMSContent" runat="server" pageId='<%# Convert.ToInt16(DataBinder.Eval(Container.DataItem, "PAGE_ID")) %>' customId='<%# Convert.ToInt16(DataBinder.Eval(Container.DataItem, "CUSTOM_ID")) %>' customId2='<%# Convert.ToInt16(DataBinder.Eval(Container.DataItem, "CUSTOM_ID_2")) %>' blockId='<%# Convert.ToInt16(DataBinder.Eval(Container.DataItem, "BLOCK_ID")) %>' cmsId='<%# Convert.ToInt16(DataBinder.Eval(Container.DataItem, "CMS_ID")) %>' cmsType='<% #Convert.ToInt16(DataBinder.Eval(Container.DataItem, "PAGECMS_TYPE")) %>' cmsGroup='<%# Convert.ToInt16(DataBinder.Eval(Container.DataItem, "PAGECMS_GROUP")) %>' cmsContent='<% #DataBinder.Eval(Container.DataItem, "V_CMSCONTENT") %>' cmsCSSType='<%# SetCMSCSSTypeId(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "PAGECMS_TYPE"))) %>' lang='<%# DataBinder.Eval(Container.DataItem, "PAGECMS_LANG") %>' boolAllowUpdate='<%# setBoolAllowUpdate() %>' adminView='<%# setIsAdminView() %>' height='<%# setHeight() %>' width='<%# setWidth() %>' groupName='<%# (DataBinder.Eval(Container.DataItem, "SLIDEGRP_DNAME")) %>' cmsTitle='<%# (DataBinder.Eval(Container.DataItem, "V_CMSTITLE")) %>'/>
                </asp:Panel>
            </ItemTemplate>
        </asp:Repeater>
    </asp:Panel>
    <asp:Panel ID="pnlCMSContainerAction" runat="server" CssClass="divCMSContainerBtnRight" Visible="false">
        <div class="divhypCMS"><asp:HyperLink ID="hypAdd" runat="server" CssClass="thickbox" meta:resourcekey="hypAdd"></asp:HyperLink></div>
    </asp:Panel>
</asp:Panel>
