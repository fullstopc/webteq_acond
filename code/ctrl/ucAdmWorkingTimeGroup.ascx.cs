﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Data;

public partial class ctrl_ucAdmWorkingTimeGroup : System.Web.UI.UserControl
{
    #region "Properties"
    protected string _keywd = "";
    #endregion

    #region "Property Methods"
    public string keywd
    {
        get { return _keywd; }
        set { _keywd = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.ClientScript.IsStartupScriptRegistered("autocomplete" + txtWTGroup.ClientID))
        {
            string strJS = "<script type=\"text/javascript\">";
            strJS += "setAutocomplete('" + txtWTGroup.ClientID + "');";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "autocomplete" + txtWTGroup.ClientID, strJS, false);
        }

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(keywd))
            {
                txtWTGroup.Text = keywd;
            }
        }

    }
    #endregion
}