﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;
using System.Linq;

public partial class ctrl_ucAdmGoogleSettings : System.Web.UI.UserControl
{
    #region "Properties"
    protected clsConfig config = new clsConfig();
    protected DataView dvConfig = null;
    protected DataSet dsConfig = null;
    protected bool boolSuccess = false;
    protected bool boolEdited = false;
    protected int intRecordAffected = 0;
     
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setPageProperties();
            fill();
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        clsConfig config = new clsConfig();
        int intRecordAffected = 0;

        Boolean boolSuccess = true;
        Boolean boolEdited = false;

        string strGoogleAnalytic = lblGoogleAnalytic.Text;
        string strGoogleAnalyticValue = txtGoogleAnalytic.Text.Trim();
        string strGoogleAnalyticViewId = lblGoogleAnalyticViewId.Text;
        string strGoogleAnalyticViewIdValue = txtGoogleAnalyticViewId.Text.Trim();
        string strAutoScriptTag = lblAutoScriptTag.Text;
        string strAutoScriptTagValue = chkboxAutoScriptTag.Checked ? clsConfig.CONSTTRUE : clsConfig.CONSTFALSE;
        string strConversionCodeEnquiry = lblConversionCodeEnquiry.Text;
        string strConversionCodeEnquiryValue = txtConversionCodeEnquiry.Text.Trim();
        string strConversionCodeOrder = lblConversionCodeOrder.Text;
        string strConversionCodeOrderValue = txtConversionCodeOrder.Text.Trim();
        string strConversionCodeAsk = lblConversionCodeAsk.Text;
        string strConversionCodeAskValue = txtConversionCodeAsk.Text.Trim();
        string strConversionCodeShare = lblConversionCodeShare.Text;
        string strConversionCodeShareValue = txtConversionCodeShare.Text.Trim();
        string strConversionCodeCall = lblConversionCodeCall.Text;
        string strConversionCodeCallValue = txtConversionCodeCall.Text.Trim();

        if (boolSuccess && !config.isItemExist(strGoogleAnalytic, clsConfig.CONSTGROUPGOOGLE))
        {
            intRecordAffected = config.addItem2(strGoogleAnalytic, strGoogleAnalyticValue, clsConfig.CONSTGROUPGOOGLE, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strGoogleAnalytic, strGoogleAnalyticValue, clsConfig.CONSTGROUPGOOGLE))
        {
            
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPGOOGLE, strGoogleAnalytic, strGoogleAnalyticValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strGoogleAnalyticViewId, clsConfig.CONSTGROUPGOOGLE))
        {
            intRecordAffected = config.addItem(strGoogleAnalyticViewId, strGoogleAnalyticViewIdValue, clsConfig.CONSTGROUPGOOGLE, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strGoogleAnalyticViewId, strGoogleAnalyticViewIdValue, clsConfig.CONSTGROUPGOOGLE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPGOOGLE, strGoogleAnalyticViewId, strGoogleAnalyticViewIdValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strAutoScriptTag, clsConfig.CONSTGROUPGOOGLE))
        {
            intRecordAffected = config.addItem(strAutoScriptTag, strAutoScriptTagValue, clsConfig.CONSTGROUPGOOGLE, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strAutoScriptTag, strAutoScriptTagValue, clsConfig.CONSTGROUPGOOGLE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPGOOGLE, strAutoScriptTag, strAutoScriptTagValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strConversionCodeEnquiry, clsConfig.CONSTGROUPGOOGLE))
        {
            intRecordAffected = config.addItem2(strConversionCodeEnquiry, strConversionCodeEnquiryValue, clsConfig.CONSTGROUPGOOGLE, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strConversionCodeEnquiry, strConversionCodeEnquiryValue, clsConfig.CONSTGROUPGOOGLE))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPGOOGLE, strConversionCodeEnquiry, strConversionCodeEnquiryValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strConversionCodeOrder, clsConfig.CONSTGROUPGOOGLE))
        {
            intRecordAffected = config.addItem2(strConversionCodeOrder, strConversionCodeOrderValue, clsConfig.CONSTGROUPGOOGLE, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strConversionCodeOrder, strConversionCodeOrderValue, clsConfig.CONSTGROUPGOOGLE))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPGOOGLE, strConversionCodeOrder, strConversionCodeOrderValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strConversionCodeAsk, clsConfig.CONSTGROUPGOOGLE))
        {
            intRecordAffected = config.addItem2(strConversionCodeAsk, strConversionCodeAskValue, clsConfig.CONSTGROUPGOOGLE, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strConversionCodeAsk, strConversionCodeAskValue, clsConfig.CONSTGROUPGOOGLE))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPGOOGLE, strConversionCodeAsk, strConversionCodeAskValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strConversionCodeShare, clsConfig.CONSTGROUPGOOGLE))
        {
            intRecordAffected = config.addItem2(strConversionCodeShare, strConversionCodeShareValue, clsConfig.CONSTGROUPGOOGLE, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strConversionCodeShare, strConversionCodeShareValue, clsConfig.CONSTGROUPGOOGLE))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPGOOGLE, strConversionCodeShare, strConversionCodeShareValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strConversionCodeCall, clsConfig.CONSTGROUPGOOGLE))
        {
            intRecordAffected = config.addItem2(strConversionCodeCall, strConversionCodeCallValue, clsConfig.CONSTGROUPGOOGLE, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strConversionCodeCall, strConversionCodeCallValue, clsConfig.CONSTGROUPGOOGLE))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPGOOGLE, strConversionCodeCall, strConversionCodeCallValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (!boolSuccess)
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to update google settings. Please try again.</div>";

            Response.Redirect(currentPageName);
        }
        else
        {
            if (boolEdited)
            {
                Session["EDITED_GOOGLESETTINGS"] = 1;
            }
            else
            {
                Session["EDITED_GOOGLESETTINGS"] = 1;
                Session["NOCHANGE"] = 1;
            }

            Response.Redirect(currentPageName);
        }
    }
    #endregion

    
    #region "Methods"
    public void fill()
    {
        setPageProperties();
    }

    protected void setPageProperties()
    {
        lblGoogleAnalytic.Text = clsConfig.CONSTNAMEGOOGLEANALYTIC;
        lblAutoScriptTag.Text = clsConfig.CONSTNAMEAUTOSCRIPTTAG;
        lblGoogleAnalyticViewId.Text = clsConfig.CONSTNAMEGOOGLEANALYTICVIEWID;
        lblConversionCodeEnquiry.Text = clsConfig.CONSTNAMECONVERSIONCODEENQUIRY;
        lblConversionCodeOrder.Text = clsConfig.CONSTNAMECONVERSIONCODEORDER;
        lblConversionCodeAsk.Text = clsConfig.CONSTNAMECONVERSIONCODEASK;
        lblConversionCodeShare.Text = clsConfig.CONSTNAMECONVERSIONCODESHARE;
        lblConversionCodeCall.Text = clsConfig.CONSTNAMECONVERSIONCODECALL;

        fillForm();
    }

    protected void fillForm()
    {
        clsConfig config = new clsConfig();

        DataSet ds = config.getItemList(1);
        DataRow[] foundRow;

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPGOOGLE + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEGOOGLEANALYTIC, true) == 0) { txtGoogleAnalytic.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEGOOGLEANALYTICVIEWID, true) == 0) { txtGoogleAnalyticViewId.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEAUTOSCRIPTTAG, true) == 0) { chkboxAutoScriptTag.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTTRUE; }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMECONVERSIONCODEENQUIRY, true) == 0) { txtConversionCodeEnquiry.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMECONVERSIONCODEORDER, true) == 0) { txtConversionCodeOrder.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMECONVERSIONCODEASK, true) == 0) { txtConversionCodeAsk.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMECONVERSIONCODESHARE, true) == 0) { txtConversionCodeShare.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMECONVERSIONCODECALL, true) == 0) { txtConversionCodeCall.Text = row["CONF_LONGVALUE"].ToString(); }
        }
    }
    #endregion
}