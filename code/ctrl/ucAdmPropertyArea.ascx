﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmPropertyArea.ascx.cs" Inherits="ctrl_ucAdmPropertyArea" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<script type="text/javascript">
    function setAutocomplete2(target) {
        $.ajax({
            type: "POST",
            url: wsBase + "wsSearchArea.asmx/getArea",
            dataType: "json",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                console.log(data);
                var strList = data.d.split(',');
                $("#" + target).autocomplete({
                    source: strList,
                    change: function(event, ui) {
                        //fillForm(target);
                    },
                    appendTo: "#autocompletecontainer2",
                });
            },
            error: function(xmlHttpRequest, status, err) {
                //alert("error " + err);
            }
        });
    }
</script>

<asp:TextBox ID="txtArea" runat="server" CssClass="form-control" MaxLength="45" placeholder="Area"></asp:TextBox>
<div id="autocompletecontainer2"></div>