﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrHeader.ascx.cs" Inherits="ctrl_ucUsrHeader" %>
<%@ Register Src="~/ctrl/ucUsrCMSContainer.ascx" TagName="UsrCMSContainer" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrLogo.ascx" TagName="UsrLogo" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrTopMenu.ascx" TagName="UsrTopMenu" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrQuickLinks.ascx" TagName="UsrQuickLinks" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrSideMenuNav.ascx" TagName="UsrSideMenuNav" TagPrefix="uc" %>

<asp:Panel ID="pnlBackToTopContainer" runat="server" CssClass="divBackToTopContainer">
    <asp:HyperLink ID="hypBackToTop" runat="server" onclick="goTop();" CssClass="hypBackToTop" ToolTip="Back to Top"></asp:HyperLink>
</asp:Panel>
<div class="topmenu-container-root2" runat="server" visible="false">
    <uc:UsrTopMenu ID="ucUsrTopTopMenu" runat="server" topMenuType="TopTopMenu" />
</div>
<div class="header-inner">
    <div class="logo-container">
        <uc:UsrLogo ID="ucUsrLogo" runat="server"/>
    </div>
    <div class="topmenu-container">
        <uc:UsrTopMenu ID="ucUsrTopMenu" runat="server" topMenuType="TopMenu" />
    </div>
    <div class="contact-container">
        <uc:UsrQuickLinks ID="ucUsrQuickLinks" runat="server" />
    </div>
    <div id="google_translate_element"></div>
    <a class="sidebar-nav-toggle">
        <span></span>
        <span></span>
        <span></span>
    </a>
    <uc:UsrSideMenuNav ID="ucUsrSideMenuNav" runat="server" />
</div>
