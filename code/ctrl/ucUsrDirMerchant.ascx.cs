﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Collections;

public partial class ctrl_ucUsrDirMerchant : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _memId = 0;
    protected int _grpId = 0;
    protected int _currentPage = 0;
    protected int _pageCount = 0;
    protected int _pageNo = 1;
    protected int _sort = 1;
    protected string _keyWord = "";
    protected Boolean _boolSearch = false;
    #endregion


    #region "Property Methods"

    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int memId
    {
        get
        {
            if (ViewState["MEMID"] == null)
            {
                return _memId;
            }
            else
            {
                return Convert.ToInt16(ViewState["MEMID"]);
            }
        }
        set { ViewState["MEMID"] = value; }
    }

    public int grpId
    {
        get
        {
            if (ViewState["GRPID"] == null)
            {
                return _grpId;
            }
            else
            {
                return Convert.ToInt16(ViewState["GRPID"]);
            }
        }
        set { ViewState["GRPID"] = value; }
    }

    public int currentPage
    {
        get
        {
            if (ViewState["CURRENTPAGE"] == null)
            {
                return _currentPage;
            }
            else
            {
                return Convert.ToInt16(ViewState["CURRENTPAGE"]);
            }
        }
        set
        {
            ViewState["CURRENTPAGE"] = value;
        }
    }

    public int pageCount
    {
        get
        {
            if (ViewState["PAGECOUNT"] == null)
            {
                return _pageCount;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGECOUNT"]);
            }
        }
        set { ViewState["PAGECOUNT"] = value; }
    }

    public int pageNo
    {
        get
        {
            if (ViewState["PAGENO"] == null)
            {
                return _pageNo;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGENO"]);
            }
        }
        set { ViewState["PAGENO"] = value; }
    }

    public int sort
    {
        get
        {
            if (ViewState["MEMSORT"] == null)
            {
                return _sort;
            }
            else
            {
                return Convert.ToInt16(ViewState["MEMSORT"]);
            }
        }
        set { ViewState["MEMSORT"] = value; }
    }

    public string keyWord
    {
        get { return ViewState["KEYWORD"] as string ?? _keyWord; }
        set { ViewState["KEYWORD"] = value; }
    }

    public Boolean boolSearch
    {
        get
        {
            if (ViewState["BOOLSEARCH"] == null)
            {
                return _boolSearch;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLSEARCH"]);
            }
        }
        set { ViewState["BOOLSEARCH"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.LoadComplete += new EventHandler(Page_LoadComplete);

        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        bindRptData();
    }

    protected void rptPagination_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //LinkButton lnkbtnPageNo = (LinkButton)e.Item.FindControl("lnkbtnPageNo");
            HyperLink hypPageNo = (HyperLink)e.Item.FindControl("hypPageNo");

            Boolean boolFound = false;
            string strQuery = Request.Url.Query;

            if (!string.IsNullOrEmpty(strQuery))
            {
                string[] strQuerySplit = strQuery.Split((char)'&');
                strQuery = "";

                for (int i = 0; i <= strQuerySplit.Length - 1; i++)
                {
                    if (strQuerySplit[i].IndexOf("pg=") >= 0)
                    {
                        boolFound = true;

                        if (i == 0) { strQuerySplit[i] = "?pg=" + Convert.ToInt16(e.Item.DataItem); }
                        else { strQuerySplit[i] = "pg=" + Convert.ToInt16(e.Item.DataItem); }
                    }

                    if (i == (strQuerySplit.Length - 1))
                    {
                        strQuery += strQuerySplit[i];
                    }
                    else
                    {
                        strQuery += strQuerySplit[i] + "&";
                    }
                }
            }
            else
            {
                boolFound = true;
                strQuery = "?pg=" + Convert.ToInt16(e.Item.DataItem);
            }

            if (!boolFound)
            {
                strQuery += "&pg=" + Convert.ToInt16(e.Item.DataItem);
            }

            hypPageNo.NavigateUrl = currentPageName + strQuery;

            if (Convert.ToInt16(e.Item.DataItem) == currentPage + 1)
            {
                hypPageNo.CssClass = "btnPaginationSel";
                hypPageNo.Enabled = false;
            }
        }
    }

    protected void rptPagination_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdPage")
        {
            pageNo = Convert.ToInt16(e.CommandArgument);

            Response.Redirect(currentPageName + "?pg=" + pageNo);
        }
    }

    protected void imgbtnFirst_Click(object sender, EventArgs e)
    {
        Boolean boolFound = false;
        string strQuery = Request.Url.Query;

        if (!string.IsNullOrEmpty(strQuery))
        {
            string[] strQuerySplit = strQuery.Split((char)'&');
            strQuery = "";

            for (int i = 0; i <= strQuerySplit.Length - 1; i++)
            {
                if (strQuerySplit[i].IndexOf("pg=") >= 0)
                {
                    boolFound = true;

                    if (i == 0) { strQuerySplit[i] = "?pg=" + 1; }
                    else { strQuerySplit[i] = "pg=" + 1; }
                }

                if (i == (strQuerySplit.Length - 1))
                {
                    strQuery += strQuerySplit[i];
                }
                else
                {
                    strQuery += strQuerySplit[i] + "&";
                }
            }
        }
        else
        {
            boolFound = true;
            strQuery = "?pg=" + 1;
        }

        if (!boolFound)
        {
            strQuery += "&pg=" + 1;
        }

        Response.Redirect(currentPageName + strQuery);
    }

    //protected void imgbtnPrev_Click(object sender, EventArgs e)
    //{
    //    currentPage = pageNo - 1;

    //    Boolean boolFound = false;
    //    string strQuery = Request.Url.Query;

    //    if (!string.IsNullOrEmpty(strQuery))
    //    {
    //        string[] strQuerySplit = strQuery.Split((char)'&');
    //        strQuery = "";

    //        for (int i = 0; i <= strQuerySplit.Length - 1; i++)
    //        {
    //            if (strQuerySplit[i].IndexOf("pg=") >= 0)
    //            {
    //                boolFound = true;

    //                if (i == 0) { strQuerySplit[i] = "?pg=" + currentPage; }
    //                else { strQuerySplit[i] = "pg=" + currentPage; }
    //            }

    //            if (i == (strQuerySplit.Length - 1))
    //            {
    //                strQuery += strQuerySplit[i];
    //            }
    //            else
    //            {
    //                strQuery += strQuerySplit[i] + "&";
    //            }
    //        }
    //    }
    //    else
    //    {
    //        boolFound = true;
    //        strQuery = "?pg=" + currentPage;
    //    }

    //    if (!boolFound)
    //    {
    //        strQuery += "&pg=" + currentPage;
    //    }

    //    Response.Redirect(currentPageName + strQuery);
    //}

    protected void imgbtnNext_Click(object sender, EventArgs e)
    {
        currentPage = pageNo + 1;

        Boolean boolFound = false;
        string strQuery = Request.Url.Query;

        if (!string.IsNullOrEmpty(strQuery))
        {
            string[] strQuerySplit = strQuery.Split((char)'&');
            strQuery = "";

            for (int i = 0; i <= strQuerySplit.Length - 1; i++)
            {
                if (strQuerySplit[i].IndexOf("pg=") >= 0)
                {
                    boolFound = true;

                    if (i == 0) { strQuerySplit[i] = "?pg=" + currentPage; }
                    else { strQuerySplit[i] = "pg=" + currentPage; }
                }

                if (i == (strQuerySplit.Length - 1))
                {
                    strQuery += strQuerySplit[i];
                }
                else
                {
                    strQuery += strQuerySplit[i] + "&";
                }
            }
        }
        else
        {
            boolFound = true;
            strQuery = "?pg=" + currentPage;
        }

        if (!boolFound)
        {
            strQuery += "&pg=" + currentPage;
        }

        Response.Redirect(currentPageName + strQuery);
    }

    protected void imgbtnLast_Click(object sender, EventArgs e)
    {
        currentPage = pageCount;

        Boolean boolFound = false;
        string strQuery = Request.Url.Query;

        if (!string.IsNullOrEmpty(strQuery))
        {
            string[] strQuerySplit = strQuery.Split((char)'&');
            strQuery = "";

            for (int i = 0; i <= strQuerySplit.Length - 1; i++)
            {
                if (strQuerySplit[i].IndexOf("pg=") >= 0)
                {
                    boolFound = true;

                    if (i == 0) { strQuerySplit[i] = "?pg=" + currentPage; }
                    else { strQuerySplit[i] = "pg=" + currentPage; }
                }

                if (i == (strQuerySplit.Length - 1))
                {
                    strQuery += strQuerySplit[i];
                }
                else
                {
                    strQuery += strQuerySplit[i] + "&";
                }
            }
        }
        else
        {
            boolFound = true;
            strQuery = "?pg=" + currentPage;
        }

        if (!boolFound)
        {
            strQuery += "&pg=" + currentPage;
        }

        Response.Redirect(currentPageName + strQuery);
    }

    protected void ddlSort_SelectedIndexChanged(object sender, EventArgs e)
    {
        sort = Convert.ToInt16(ddlSort.SelectedValue);

        if (sort != 0)
        {
            Boolean boolFound = false;
            string strQuery = Request.Url.Query;

            if (!string.IsNullOrEmpty(strQuery))
            {
                string[] strQuerySplit = strQuery.Split((char)'&');
                strQuery = "";

                for (int i = 0; i <= strQuerySplit.Length - 1; i++)
                {
                    if (strQuerySplit[i].IndexOf("sort=") >= 0)
                    {
                        boolFound = true;

                        if (i == 0) { strQuerySplit[i] = "?sort=" + sort; }
                        else { strQuerySplit[i] = "sort=" + sort; }
                    }

                    if (i == (strQuerySplit.Length - 1))
                    {
                        strQuery += strQuerySplit[i];
                    }
                    else
                    {
                        strQuery += strQuerySplit[i] + "&";
                    }
                }
            }
            else
            {
                boolFound = true;
                strQuery = "?sort=" + sort;
            }

            if (!boolFound)
            {
                strQuery += "&sort=" + sort;
            }

            Response.Redirect(currentPageName + strQuery);
        }
    }

    protected void rptMember_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intMemId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "MEM_ID"));
            string strMemName = DataBinder.Eval(e.Item.DataItem, "MEM_NAME").ToString();
            string strMemLogo = DataBinder.Eval(e.Item.DataItem, "MEM_LOGO").ToString();
            string strMemAdd = DataBinder.Eval(e.Item.DataItem, "MEM_ADD").ToString();
            string strMemTel = DataBinder.Eval(e.Item.DataItem, "MEM_TEL").ToString();
            string strMemFax = DataBinder.Eval(e.Item.DataItem, "MEM_FAX").ToString();
            string strMemMobile = DataBinder.Eval(e.Item.DataItem, "MEM_MOBILE").ToString();
            string strMemEmail = DataBinder.Eval(e.Item.DataItem, "MEM_EMAIL").ToString();
            string strMemContactPerson = DataBinder.Eval(e.Item.DataItem, "MEM_CONTACTPERSON").ToString();
            string strMemWebsite = DataBinder.Eval(e.Item.DataItem, "MEM_WEBSITE").ToString();
            string strMemSnapshot = DataBinder.Eval(e.Item.DataItem, "MEM_SNAPSHOT").ToString();
            int intGrpId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "MEM_BIZTYPE"));
            string strMemDetails = "";                       

            strMemDetails = Request.Url.ToString() + "&id=" + intMemId;

            Boolean boolFound = false;
            string strQuery = Request.Url.Query;

            if (!string.IsNullOrEmpty(strQuery))
            {
                string[] strQuerySplit = strQuery.Split((char)'&');
                strQuery = "";

                for (int i = 0; i <= strQuerySplit.Length - 1; i++)
                {
                    if (strQuerySplit[i].IndexOf("grpid=") >= 0)
                    {
                        boolFound = true;
                    }
                }
            }

            if (!boolFound)
            {
                strMemDetails += "&grpid=" + intGrpId;
            }            

            //ImageButton imgbtnMemberImg = (ImageButton)e.Item.FindControl("imgbtnMemberImg");
            //imgbtnMemberImg.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + ConfigurationManager.AppSettings["uplBase"] + clsAdmin.CONSTADMDIRFOLDER + "/" + strMemLogo + "&w=" + clsAdmin.CONSTUSRDIRIMGWIDTH + "&h=" + clsAdmin.CONSTUSRDIRIMGHEIGHT;
            //imgbtnMemberImg.AlternateText = strMemName;
            //ForeColor = "Transparent"

            HyperLink hypMerchantName = (HyperLink)e.Item.FindControl("hypMerchantName");
            hypMerchantName.Text = strMemName;
            hypMerchantName.ToolTip = strMemName;
            hypMerchantName.NavigateUrl = strMemDetails;

            if (!string.IsNullOrEmpty(strMemLogo))
            {
                string strMerchantLogoPath = ConfigurationManager.AppSettings["uplBase"] + "/" + clsAdmin.CONSTADMDIRFOLDER + "/" + strMemLogo;

                HyperLink hypMerchantLogo = (HyperLink)e.Item.FindControl("hypMerchantLogo");
                hypMerchantLogo.NavigateUrl = strMemDetails;
                hypMerchantLogo.ToolTip = strMemName;

                Image resultImage = new Image();
                try
                {
                    if (!string.IsNullOrEmpty(strMerchantLogoPath))
                    {
                        string strImagePath = Server.MapPath(strMerchantLogoPath);
                        System.Drawing.Image objImage = System.Drawing.Image.FromFile(strImagePath);
                        resultImage = clsMis.getImageResize2(resultImage, clsAdmin.CONSTUSRDIRIMGWIDTH, clsAdmin.CONSTUSRDIRIMGHEIGHT, objImage.Width, objImage.Height);
                        objImage = null;
                    }
                }
                catch (Exception ex)
                {
                }

                Image imgMerchantLogo = (Image)e.Item.FindControl("imgMerchantLogo");
                imgMerchantLogo.ImageUrl = strMerchantLogoPath;
                imgMerchantLogo.AlternateText = strMemName;
                imgMerchantLogo.ToolTip = strMemName;
                imgMerchantLogo.Width = resultImage.Width;
                imgMerchantLogo.Height = resultImage.Height;

                imgMerchantLogo.Attributes["style"] = "top:" + (clsAdmin.CONSTUSRDIRIMGHEIGHT - resultImage.Height.Value) / 2 + "px;";
                imgMerchantLogo.Attributes["style"] += "left:" + (clsAdmin.CONSTUSRDIRIMGWIDTH - resultImage.Width.Value) / 2 + "px;";
                resultImage.Dispose();
                resultImage = null;
            }
            else 
            {
                Image imgMerchantLogo = (Image)e.Item.FindControl("imgMerchantLogo");
                imgMerchantLogo.Visible = false;
            }

            Literal litMemberAdd = (Literal)e.Item.FindControl("litMemberAdd");

            if (!string.IsNullOrEmpty(strMemAdd))
            {
                litMemberAdd.Text = "<div class=\"divMemInfo\">" + strMemAdd + "</div>";
            }

            Literal litMemberContactInfo = (Literal)e.Item.FindControl("litMemberContactInfo");

            if (!string.IsNullOrEmpty(strMemTel))
            {
                strMemTel = "<span class='spanMerchantContactInfo'>Tel: " + strMemTel + "</span>";
            }

            if (!string.IsNullOrEmpty(strMemFax))
            {
                strMemFax = "<span class='spanMerchantContactInfo'>Fax: " + strMemFax + "</span>";
            }

            if (!string.IsNullOrEmpty(strMemMobile))
            {
                strMemMobile = "Mobile: " + strMemMobile;

                if (!string.IsNullOrEmpty(strMemContactPerson))
                {
                    strMemMobile += " (" + strMemContactPerson + ")";
                }
                strMemMobile = "<span class='spanMerchantContactInfo'>" + strMemMobile + "</span>";
            }
            litMemberContactInfo.Text = strMemTel + strMemFax + strMemMobile;

            if (!string.IsNullOrEmpty(litMemberContactInfo.Text.Trim()))
            {
                litMemberContactInfo.Text = "<div class=\"divMemInfo\">" + litMemberContactInfo.Text + "</div>";
            }

            if(!string.IsNullOrEmpty(strMemEmail))
            {
                string strMemEmailLink = "";
                strMemEmailLink = "Email: <a href='mailto:" + strMemEmail + "' title='" + strMemEmail + "'>" + strMemEmail + "</a>";
                Literal litMerchantEmail = (Literal)e.Item.FindControl("litMerchantEmail");
                litMerchantEmail.Text = "<div class=\"divMemInfo\">" + strMemEmailLink + "</div>";            
            }

            if (!string.IsNullOrEmpty(strMemWebsite))
            {
                string strMemWebsiteLink = "";

                strMemWebsiteLink = "Website: <a href='" + strMemWebsite + "' title='" + strMemWebsite + "' target='_blank'>" + strMemWebsite + "</a>";
                Literal litMerchantWebsite = (Literal)e.Item.FindControl("litMerchantWebsite");
                litMerchantWebsite.Text = "<div class=\"divMemInfo\">" + strMemWebsiteLink + "</div>";
            } 

            if (!string.IsNullOrEmpty(strMemSnapshot))
            {
                Literal litMemberShortDesc = (Literal)e.Item.FindControl("litMemberShortDesc");
                litMemberShortDesc.Text = "<div class=\"divMemSnapshot\">" + strMemSnapshot + "</div>";
            }

            HyperLink hypReadMore = (HyperLink)e.Item.FindControl("hypReadMore");
            hypReadMore.NavigateUrl = strMemDetails;            
        }
    }

    protected void rptMember_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        //if (e.CommandName == "cmdDetail")
        //{
            //int intMemId = Convert.ToInt16(e.CommandArgument);
            //int intSectId = Convert.ToInt16(clsAdmin.CONSTUSRITEMPAGESECTID);

            //if (Application["PAGELIST"] == null)
            //{
            //    clsCMS cms = new clsCMS();
            //    Application["PAGELIST"] = cms.getPageList();
            //}

            //DataSet ds = new DataSet();
            //ds = (DataSet)Application["PAGELIST"];

            //DataTable dt = new DataTable();
            //dt = ds.Tables[0];

            //DataRow[] foundRow = dt.Select("SECT_ID = " + intSectId, "PAGE_DEFAULT DESC, PAGE_ORDER ASC");

            //if (foundRow.Length > 0)
            //{
            //    string strLandingPage;

            //    if (!string.IsNullOrEmpty(foundRow[0]["SECT_DEFAULT"].ToString()))
            //    {
            //        strLandingPage = foundRow[0]["SECT_DEFAULT"].ToString();
            //    }
            //    else
            //    {
            //        strLandingPage = foundRow[0]["SECT_TEMPLATE"].ToString();
            //    }

            //    Response.Redirect(strLandingPage + "?pgid=" + foundRow[0]["PAGE_ID"] + "&id=" + intMemId);
        //    }
        //}
    }

    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        //imgbtnFirstTop.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
        //imgbtnPrevTop.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
        //imgbtnNextTop.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
        //imgbtnLastTop.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
        //imgbtnFirstBottom.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
        //imgbtnPrevBottom.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
        //imgbtnNextBottom.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
        //imgbtnLastBottom.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";

        ddlSort.Items.Add(new ListItem("Company Name", "1"));
        ddlSort.Items.Add(new ListItem("Latest on Top", "2"));

        ddlSort.SelectedValue = sort.ToString();
    }

    protected void bindRptData()
    {
        if (Application["MERCHANTLIST"] == null)
        {
            clsDirMerchant mem = new clsDirMerchant();
            Application["MERCHANTLIST"] = mem.getMemberList(1);
        }

        DataSet ds = new DataSet();
        ds = (DataSet)Application["MERCHANTLIST"];
        DataView dv = new DataView(ds.Tables[0]);

        Boolean boolRowFilter = false;

        if (grpId > 0)
        {
            if (boolRowFilter) { dv.RowFilter += "AND MEM_BIZTYPE = " + grpId; }
            else { dv.RowFilter = "MEM_BIZTYPE = " + grpId; }

            boolRowFilter = true;
        }

        if (boolSearch)
        {
            if (boolRowFilter) { dv.RowFilter += "AND MEM_NAME LIKE '%" + keyWord + "%'"; }
            else { dv.RowFilter = "MEM_NAME LIKE '%" + keyWord + "%'"; }

            boolRowFilter = true;
        }

        if (sort == 1)
        {
            dv.Sort = "MEM_NAME ASC";
        }
        else
        {
            dv.Sort = "MEM_CREATION DESC";
        }

        PagedDataSource pds = new PagedDataSource();
        pds.DataSource = dv;
        pds.AllowPaging = true;
        pds.PageSize = clsSetting.CONSTUSRDIRPAGESIZE;
        pageCount = pds.PageCount;

        if (pageNo != 0) { currentPage = pageNo - 1; }

        if (currentPage < 0) { currentPage = 0; }
        if (currentPage > pageCount - 1) { currentPage = 0; }

        pds.CurrentPageIndex = currentPage;
        rptMember.DataSource = pds;
        rptMember.DataBind();

        if (dv.Count > 0)
        {
            int intCurrentPage = currentPage + 1;
            int intStart;
            int intEnd;
            int intPaginationSize = 6;
            int intPaginationSizeMid = Convert.ToInt16(intPaginationSize / 2);

            if (intCurrentPage <= intPaginationSizeMid)
            {
                intStart = 1;
                if (pageCount > intPaginationSize) { intEnd = intPaginationSize; }
                else { intEnd = pageCount; }
            }
            else if (intCurrentPage >= pageCount - intPaginationSizeMid)
            {
                intStart = pageCount - intPaginationSize + 1;
                if (intStart < 1) { intStart = 1; }
                intEnd = pageCount;
            }
            else
            {
                intStart = intCurrentPage - intPaginationSizeMid;
                intEnd = intCurrentPage + intPaginationSizeMid;
            }

            ArrayList pages = new ArrayList();
            for (int i = intStart; i <= intEnd; i++)
            {
                pages.Add(i);
            }

            //hypPaginationFirst.NavigateUrl = currentPageName + "?pg=1&sort=" + sort;
            //hypPaginationLast.NavigateUrl = currentPageName + "?pg=" + pageCount + "&sort=" + sort;

            Boolean boolQueryFound = false;
            string strQuery = Request.Url.Query;
            string strNewQueryFirstPage = "";
            string strNewQueryLastPage = "";

            if (!string.IsNullOrEmpty(strQuery))
            {
                string[] strQuerySplit = strQuery.Split((char)'&');
                string strQueryItemFirstPage = "";
                string strQueryItemLastPage = "";

                for (int i = 0; i <= strQuerySplit.Length - 1; i++)
                {
                    strQueryItemFirstPage = "";
                    strQueryItemLastPage = "";

                    if (strQuerySplit[i].IndexOf("pg=") >= 0)
                    {
                        boolQueryFound = true;

                        if (i == 0)
                        {
                            strQueryItemFirstPage = "?pg=1";
                            strQueryItemLastPage = "?pg=" + pageCount;
                        }
                        else
                        {
                            strQueryItemFirstPage = "pg=1";
                            strQueryItemLastPage = "pg=" + pageCount;
                        }
                    }
                    else
                    {
                        strQueryItemFirstPage = strQuerySplit[i];
                        strQueryItemLastPage = strQuerySplit[i];
                    }

                    if (i == (strQuerySplit.Length - 1))
                    {
                        strNewQueryFirstPage += strQueryItemFirstPage;
                        strNewQueryLastPage += strQueryItemLastPage;
                    }
                    else
                    {
                        strNewQueryFirstPage += strQueryItemFirstPage + "&";
                        strNewQueryLastPage += strQueryItemLastPage + "&";
                    }
                }
            }
            else
            {
                boolQueryFound = true;
                strNewQueryFirstPage = "?pg=" + 1;
                strNewQueryLastPage = "?pg=" + pageCount;
            }

            if (!boolQueryFound)
            {
                strNewQueryFirstPage += "&pg=" + 1;
                strNewQueryLastPage += "&pg=" + pageCount;
            }

            hypPaginationFirst.NavigateUrl = currentPageName + strNewQueryFirstPage;
            hypPaginationLast.NavigateUrl = currentPageName + strNewQueryLastPage;

            if (currentPage == 0)
            {
                hypPaginationFirst.Enabled = false;
                //imgbtnFirstBottom.Enabled = false;
                //imgbtnFirstTop.CssClass = "imgbtnPageFirstDisabled";
                //imgbtnFirstBottom.CssClass = "imgbtnPageFirstDisabled";

                //imgbtnPrevTop.Enabled = false;
                //imgbtnPrevBottom.Enabled = false;
                //imgbtnPrevTop.CssClass = "imgbtnPagePrevDisabled";
                //imgbtnPrevBottom.CssClass = "imgbtnPagePrevDisabled";

                if (pageCount > 1)
                {
                    //imgbtnNextTop.Enabled = true;
                    //imgbtnNextBottom.Enabled = true;
                    //imgbtnNextTop.CssClass = "imgbtnPageNext";
                    //imgbtnNextBottom.CssClass = "imgbtnPageNext";

                    hypPaginationLast.Enabled = true;
                    //imgbtnLastBottom.Enabled = true;
                    //imgbtnLastTop.CssClass = "imgbtnPageLast";
                    //imgbtnLastBottom.CssClass = "imgbtnPageLast";
                }
            }

            if (currentPage == pageCount - 1)
            {
                hypPaginationLast.Enabled = false;
                //imgbtnLastBottom.Enabled = false;
                //imgbtnLastTop.CssClass = "imgbtnPageLastDisabled";
                //imgbtnLastBottom.CssClass = "imgbtnPageLastDisabled";

                //imgbtnNextTop.Enabled = false;
                //imgbtnNextBottom.Enabled = false;
                //imgbtnNextTop.CssClass = "imgbtnPageNextDisabled";
                //imgbtnNextBottom.CssClass = "imgbtnPageNextDisabled";

                if (pageCount != 1)
                {
                    hypPaginationFirst.Enabled = true;
                    //imgbtnFirstBottom.Enabled = true;
                    //imgbtnFirstTop.CssClass = "imgbtnPageFirst";
                    //imgbtnFirstBottom.CssClass = "imgbtnPageFirst";

                    //imgbtnPrevTop.Enabled = true;
                    //imgbtnPrevBottom.Enabled = true;
                    //imgbtnPrevTop.CssClass = "imgbtnPagePrev";
                    //imgbtnPrevBottom.CssClass = "imgbtnPagePrev";
                }
            }

            if (currentPage != 0 && currentPage != pageCount - 1)
            {
                hypPaginationFirst.Enabled = true;
                //imgbtnFirstBottom.Enabled = true;
                //imgbtnFirstTop.CssClass = "imgbtnPageFirst";
                //imgbtnFirstBottom.CssClass = "imgbtnPageFirst";

                //imgbtnPrevTop.Enabled = true;
                //imgbtnPrevBottom.Enabled = true;
                //imgbtnPrevTop.CssClass = "imgbtnPagePrev";
                //imgbtnPrevBottom.CssClass = "imgbtnPagePrev";

                //imgbtnNextTop.Enabled = true;
                //imgbtnNextBottom.Enabled = true;
                //imgbtnNextTop.CssClass = "imgbtnPageNext";
                //imgbtnNextBottom.CssClass = "imgbtnPageNext";

                hypPaginationLast.Enabled = true;
                //imgbtnLastBottom.Enabled = true;
                //imgbtnLastTop.CssClass = "imgbtnPageLast";
                //imgbtnLastBottom.CssClass = "imgbtnPageLast";
            }

            rptPaginationTop.DataSource = pages;
            rptPaginationTop.DataBind();

            //rptPaginationBottom.DataSource = pages;
            //rptPaginationBottom.DataBind();

            //litTotalPage.Text = "<span class=\"spanTotal\">/ " + pageCount + "</span>";
            //litTotalPageBottom.Text = "<span class=\"spanTotal\">/ " + pageCount + "</span>";
        }
        else
        {
            pnlListing.Visible = false;
            pnlMemNoFound.Visible = true;
        }
    }
    #endregion
}
