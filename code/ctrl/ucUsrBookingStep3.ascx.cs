﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Collections;
using System.Net;
using System.IO;
using System.Collections.Specialized;

public partial class ctrl_ucUsr : System.Web.UI.UserControl
{
    #region "Property"
    
    #endregion

    #region "Properties Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    #endregion
    
    public void fill()
    {
        clsMember member = new clsMember();
        int memberID = Convert.ToInt32(Session[clsKey.SESSION_MEMBER_ID]);
        if (member.extractByID(memberID))
        {
            txtServiceContactTel.Text = member.contactPrefix + member.contactTel;
        }

        Reservation reserve = Session[clsKey.SESSION_RESERVATION] as Reservation;
        if(reserve != null)
        {
            litServiceDate.Text = reserve.config.serviceDatetimeFrom.ToString("d MMM yyyy");
            litServiceTime.Text = reserve.config.serviceDatetimeFrom.ToString("h:mm tt");
            litServiceAddrUnit.Text = reserve.config.propertyUnitno;
            litServiceItemQty.Text = reserve.config.serviceItemQty.ToString();
            litServiceAmount.Text = clsMis.formatPrice(reserve.config.serviceChargeAmount);
        }
    }

    protected void lnkbtnSubmit_Click(object sender, EventArgs e)
    {
        Reservation reserve = Session[clsKey.SESSION_RESERVATION] as Reservation;
        reserve.config.serviceRemark = txtServiceRemark.Text;
        reserve.config.serviceContactTel = txtServiceContactTel.Text;
        reserve.config.code = null;
        reserve.step = 4;

        if (reserve.config.add())
        {
            //Session[clsKey.SESSION_RESERVATION_SUCCESS] = 1;
            Session[clsKey.SESSION_RESERVATION] = reserve;

            //NameValueCollection parameters = HttpUtility.ParseQueryString(Request.QueryString.ToString());
            //parameters["step"] = "4";

            //string url = Request.Url.AbsolutePath + "?" + parameters.ToString();
            //Response.Redirect(url);
            Response.Redirect(new Paypal().payment(reserve.config));
        }
    }
}
