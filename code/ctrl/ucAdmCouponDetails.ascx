﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmCouponDetails.ascx.cs" Inherits="ctrl_ucAdmCouponDetails" %>

<script type="text/javascript">
    function validateCouponCode_client(source, args) {
        var txtCouponCode = document.getElementById("<%= txtCouponCode.ClientID %>");
        var couponCodeRE = /<%= clsAdmin.CONSTALPHANUMERICRE %>/;

        if (txtCouponCode.value.match(couponCodeRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "Please enter only alphanumeric.";
            source.innerHTML = "Please enter only alphanumeric.";
        }
    }

    function validateCurrency_client(source, args) {
        var txtDisValue = document.getElementById("<%= txtDisValue.ClientID %>");
        txtDisValue.value = formatCurrency(txtDisValue.value);

        var currRE = /<%= clsAdmin.CONSTCURRENCYRE %>/;

        if (txtDisValue.value.match(currRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "<br />Please enter valid value.";
            source.innerHTML = "<br />Please enter valid value.";
        }
    }

    var txtCouponPeriodFrom;
    var txtCouponPeriodTo;
    var hdnCouponPeriodFrom;
    var hdnCouponPeriodTo;

    function initSect3() {
        txtCouponPeriodFrom = document.getElementById("<%= txtCouponPeriodFrom.ClientID %>");
        txtCouponPeriodTo = document.getElementById("<%= txtCouponPeriodTo.ClientID %>");
        hdnCouponPeriodFrom = document.getElementById("<%= hdnCouponPeriodFrom.ClientID %>");
        hdnCouponPeriodTo = document.getElementById("<%= hdnCouponPeriodTo.ClientID %>");

        $(function() {
            $("#<% =txtCouponPeriodFrom.ClientID %>").datepicker({ dateFormat: 'd M yy' });
            $("#<% =txtCouponPeriodFrom.ClientID %>").datepicker({ changeMonth: true });
            $("#<% =txtCouponPeriodFrom.ClientID %>").datepicker({ changeYear: true });
            $("#<% =txtCouponPeriodFrom.ClientID %>").datepicker({ altField: "#<% =hdnCouponPeriodFrom.ClientID %>" });
            $("#<% =txtCouponPeriodFrom.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });

            $("#<% =txtCouponPeriodTo.ClientID %>").datepicker({ dateFormat: 'd M yy' });
            $("#<% =txtCouponPeriodTo.ClientID %>").datepicker({ changeMonth: true });
            $("#<% =txtCouponPeriodTo.ClientID %>").datepicker({ changeYear: true });
            $("#<% =txtCouponPeriodTo.ClientID %>").datepicker({ altField: "#<% =hdnCouponPeriodTo.ClientID %>" });
            $("#<% =txtCouponPeriodTo.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });

            //getter
            var changeMonth = $("#<% =txtCouponPeriodFrom.ClientID %>").datepicker('option', 'changeMonth');
            var changeYear = $("#<% =txtCouponPeriodFrom.ClientID %>").datepicker('option', 'changeYear');
            var altField = $("#<% =txtCouponPeriodFrom.ClientID %>").datepicker("option", "altField");
            var altFormat = $("#<% =txtCouponPeriodFrom.ClientID %>").datepicker("option", "altFormat");
            
            var changeMonth = $("#<% =txtCouponPeriodTo.ClientID %>").datepicker('option', 'changeMonth');
            var changeYear = $("#<% =txtCouponPeriodTo.ClientID %>").datepicker('option', 'changeYear');
            var altField = $("#<% =txtCouponPeriodTo.ClientID %>").datepicker("option", "altField");
            var altFormat = $("#<% =txtCouponPeriodTo.ClientID %>").datepicker("option", "altFormat");

            //setter
            $("#<% =txtCouponPeriodFrom.ClientID %>").datepicker('option', 'changeMonth', true);
            $("#<% =txtCouponPeriodFrom.ClientID %>").datepicker('option', 'changeYear', true);
            $("#<% =txtCouponPeriodFrom.ClientID %>").datepicker("option", "altField", '#<% =hdnCouponPeriodFrom.ClientID %>');
            $("#<% =txtCouponPeriodFrom.ClientID %>").datepicker("option", "altFormat", 'dd/mm/yy');
            
            $("#<% =txtCouponPeriodTo.ClientID %>").datepicker('option', 'changeMonth', true);
            $("#<% =txtCouponPeriodTo.ClientID %>").datepicker('option', 'changeYear', true);
            $("#<% =txtCouponPeriodTo.ClientID %>").datepicker("option", "altField", '#<% =hdnCouponPeriodTo.ClientID %>');
            $("#<% =txtCouponPeriodTo.ClientID %>").datepicker("option", "altFormat", 'dd/mm/yy');
        });
    }

    function validateCouponPeriod_client(source, args) {
        var bValid = false;

        source.errormessage = "<br />Please enter valid period.";
        source.innerHTML = "<br />Please enter valid period.";

        if ((hdnCouponPeriodFrom.value == '') && (hdnCouponPeriodTo.value == '')) {
            args.IsValid = true;
        }
        else {
            if (Trim(hdnCouponPeriodFrom.value) != '') {
                if (validateDate(hdnCouponPeriodFrom.value)) {
                    bValid = true;
                }
                else {
                    bValid = false;
                }
            }

            if ((bValid) && (Trim(hdnCouponPeriodTo.value) != '')) {
                if (validateDate(hdnCouponPeriodTo.value)) {
                    bValid = true;
                }
                else {
                    bValid = false;
                }
            }
            else {
                bValid = false;
            }

            if ((bValid) && (validateDateRange(hdnCouponPeriodFrom.value, hdnCouponPeriodTo.value))) {
                bValid = true;
            }
            else {
                bValid = false;
            }

            args.IsValid = bValid;
        }
    }
</script>
<asp:Panel ID="pnlCouponDetails" runat="server">
    <asp:Panel ID="pnlCouponDetailsInner" runat="server">
        <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Coupon Detail</td>
                </tr>
            </thead>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblCouponName" runat="server" meta:resourcekey="lblCouponName"></asp:Label>:<span class="attention_compulsory">*</span></td>
                <td>
                    <asp:TextBox ID="txtCouponName" runat="server" MaxLength="250" CssClass="text_big compulsory"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCouponName" runat="server" meta:resourcekey="rfvCouponName" Display="Dynamic" ControlToValidate="txtCouponName" CssClass="errmsg" ValidationGroup="coupon"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblCouponCode" runat="server" meta:resourcekey="lblCouponCode"></asp:Label>:<span class="attention_unique">*</span></td>
                <td id="tdTxtCouponCode" runat="server">
                    <asp:TextBox ID="txtCouponCode" runat="server" MaxLength="20" CssClass="text uniq"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCouponCode" runat="server" meta:resourcekey="rfvCouponCode" Display="Dynamic" ControlToValidate="txtCouponCode" CssClass="errmsg" ValidationGroup="coupon"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvCouponCode" runat="server" ControlToValidate="txtCouponCode" Display="Dynamic" CssClass="errmsg" ValidationGroup="coupon" ClientValidationFunction="validateCouponCode_client" OnServerValidate="validateCouponCode_server" OnPreRender="cvCouponCode_PreRender"></asp:CustomValidator>
                </td>
                <td class="tdLabel" id="tdLitCouponCode" runat="server" visible="false"><asp:Literal ID="litCouponCode" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblCouponDiscount" runat="server" meta:resourcekey="lblCouponDiscount"></asp:Label>:<span class="attention_compulsory">*</span></td>
                <td>
                    <asp:DropDownList ID="ddlDisOperator" runat="server" CssClass="ddl compulsory"></asp:DropDownList>
                    <asp:TextBox ID="txtDisValue" runat="server" MaxLength="10" CssClass="text_medium compulsory"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDisValue" runat="server" meta:resourcekey="rfvDisValue" ControlToValidate="txtDisValue" Display="Dynamic" CssClass="errmsg" ValidationGroup="coupon"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvDisValue" runat="server" ControlToValidate="txtDisValue" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateCurrency_client"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblCouponPeriod" runat="server" meta:resourcekey="lblCouponPeriod"></asp:Label>:</td>
                <td>
                    From <asp:TextBox ID="txtCouponPeriodFrom" runat="server" CssClass="text_medium"></asp:TextBox><asp:HiddenField ID="hdnCouponPeriodFrom" runat="server" />
                    To <asp:TextBox ID="txtCouponPeriodTo" runat="server" CssClass="text_medium"></asp:TextBox><asp:HiddenField ID="hdnCouponPeriodTo" runat="server" />
                    <asp:CustomValidator ID="cvCouponPeriod" runat="server" Display="Dynamic" CssClass="errmsg" ValidationGroup="coupon" ClientValidationFunction="validateCouponPeriod_client"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblCouponActive" runat="server" meta:resourcekey="lblCouponActive"></asp:Label>:</td>
                <td class="tdLabel"><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
            </tr>
            <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="coupon"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" class="btn btnCancel" Visible="false" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>    
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>
