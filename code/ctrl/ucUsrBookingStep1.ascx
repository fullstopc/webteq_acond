﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrBookingStep1.ascx.cs" Inherits="ctrl_ucUsr" %>
<div class="row bookingform__headline">
    <div class="col">
        Hi <asp:Literal runat="server" ID="litMemberName"></asp:Literal>, Please specify your Property & the aircond to be serviced.
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-input">
            <select id="ddlProperty"></select>
            <asp:HiddenField runat="server" ID="hdnPropertyID" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-input">
            <select id="ddlServiceType"></select>
            <asp:HiddenField runat="server" ID="hdnServiceTypeID" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-input">
            <div id="aircondlist" class="aircond-list display hide"></div>
            <asp:HiddenField runat="server" ID="hdnAircondIDs" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <asp:LinkButton ID="lnkbtnSubmit" runat="server" 
            CssClass="button button-full button-round button-icon-right button-edge" 
            OnClick="lnkbtnSubmit_Click" 
            ValidationGroup="vg" 
            ToolTip="Next">
            <span>Next</span>
            <i class="material-icons">keyboard_arrow_right</i>
        </asp:LinkButton>
    </div>
</div>
<script type="text/javascript">
    var BindProperty = function (data) {
        $("#ddlProperty").empty();
        $("#ddlProperty").append("<option>Please select your property.</option>");
        $("#ddlProperty").append(data.properties.map(function (property) {
            return "<option value='" + property.ID + "'>" + property.areaObj.name + " - " + property.typeObj.name + " - " + property.unitno + "</option>";
        }).join(""));
        $("#ddlProperty").change(function () {
            $("#<%= hdnPropertyID.ClientID %>").val($(this).val());
            
            var propertyID = parseInt($(this).val());
            var serviceTypeID = parseInt($("#ddlServiceType").val());

            // get property object
            var property = data.properties.filter(function(x){
                return x.ID == propertyID;
            });
            if(property.length == 1)
                property = property[0];

            // get airconds with property type
            var airconds = data.airconds.filter(function(x){
                return x.typeID == property.type;
            });
            if(serviceTypeID == <%= clsKey.CONFIG__SERVICE_TYPE__AIRCOND %>){
                $("#aircondlist").removeClass("hide");
                BindAircond(airconds);
            }
            else{
                $("#aircondlist").addClass("hide");
                BindAircond(airconds);
            }
        });
    }
    var BindServiceType = function (data) {
        $("#ddlServiceType").empty();
        $("#ddlServiceType").append("<option>Please select the service that you need.</option>");
        $("#ddlServiceType").append(data.serviceType.map(function (type) {
            return "<option value='" + type.ID + "'>" + type.name + "</option>";
        }).join(""));
        $(document).on('change', '#ddlServiceType', function (e) {
            var serviceTypeID = parseInt($(this).val());
            var propertyID = parseInt($("#ddlProperty").val());

            // update to hidden field
            $("#<%= hdnServiceTypeID.ClientID %>").val(serviceTypeID);

            // get property object
            var property = data.properties.filter(function(x){
                return x.ID == propertyID;
            });
            if(property.length == 1)
                property = property[0];

            // get airconds with property type
            var airconds = data.airconds.filter(function(x){
                return x.typeID == property.type;
            });
            if(serviceTypeID == <%= clsKey.CONFIG__SERVICE_TYPE__AIRCOND %>){
                $("#aircondlist").removeClass("hide");
                BindAircond(airconds);
            }
            else{
                $("#aircondlist").addClass("hide");
                BindAircond(airconds);
            }

        });
    }
    var BindAircond = function (airconds) {
        $("#aircondlist").empty();
        $("#aircondlist").append(airconds.map(function (aircond) {
            return "<label class='aircond-item'><input type='checkbox' value='" + aircond.ID+"' /> ${aircond.name}</label>"
        }).join(""));
        $("#aircondlist").on('change', 'input', function (e) {
            var aircondIDs = []
            $("#aircondlist").find("input").each(function(){
                if($(this).is(":checked")){
                    aircondIDs.push($(this).val());
                }
            });
            $("#<%= hdnAircondIDs.ClientID %>").val(aircondIDs.join());
        });
        
    }

    $(function () {
        var data = <%= Newtonsoft.Json.JsonConvert.SerializeObject(data) %>;

        BindProperty(data);
        BindServiceType(data);
    })
</script>