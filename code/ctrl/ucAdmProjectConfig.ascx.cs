﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucAdmProjectConfig : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _projId = 0;
    protected int _mode = 1;
    protected string _pageListingURL = "";
    protected int _type = 0;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int projId
    {
        get
        {
            if (ViewState["PROJID"] == null)
            {
                return _projId;
            }
            else
            {
                return int.Parse(ViewState["PROJID"].ToString());
            }
        }
        set { ViewState["PROJID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }

    protected int type
    {
        get
        {
            if (ViewState["TYPE"] == null)
            {
                return _type;
            }
            else
            {
                return int.Parse(ViewState["TYPE"].ToString());
            }
        }
        set { ViewState["TYPE"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            clsProject proj = new clsProject();
            int intRecordAffected = 0;
            Boolean boolSuccess = true;
            Boolean boolEdited = false;

            string strPageLimit = txtPageLimit.Text.Trim();
            if (!proj.isConfigExist(clsProject.CONSTNAMEPAGELIMIT, clsProject.CONSTGROUPLIMIT, projId))
            {
                intRecordAffected = proj.addConfig(projId, clsProject.CONSTNAMEPAGELIMIT, strPageLimit, clsProject.CONSTGROUPLIMIT, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }
            else if (!proj.isExactSameSetData(clsProject.CONSTNAMEPAGELIMIT, strPageLimit, clsProject.CONSTGROUPLIMIT, projId))
            {
                intRecordAffected = proj.updateConfig(clsProject.CONSTGROUPLIMIT, clsProject.CONSTNAMEPAGELIMIT, strPageLimit, int.Parse(Session["ADMID"].ToString()), projId);

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }

            string strObjectLimit = txtObjectLimit.Text.Trim();
            if (boolSuccess && !proj.isConfigExist(clsProject.CONSTNAMEOBJECTLIMIT, clsProject.CONSTGROUPLIMIT, projId))
            {
                intRecordAffected = proj.addConfig(projId, clsProject.CONSTNAMEOBJECTLIMIT, strObjectLimit, clsProject.CONSTGROUPLIMIT, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }
            else if (boolSuccess && !proj.isExactSameSetData(clsProject.CONSTNAMEOBJECTLIMIT, strObjectLimit, clsProject.CONSTGROUPLIMIT, projId))
            {
                intRecordAffected = proj.updateConfig(clsProject.CONSTGROUPLIMIT, clsProject.CONSTNAMEOBJECTLIMIT, strObjectLimit, int.Parse(Session["ADMID"].ToString()), projId);

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }

            string strSlideShowLimit = txtSlideShowLimit.Text.Trim();
            if (boolSuccess && !proj.isConfigExist(clsProject.CONSTNAMESLIDESHOWLIMIT, clsProject.CONSTGROUPLIMIT, projId))
            {
                intRecordAffected = proj.addConfig(projId, clsProject.CONSTNAMESLIDESHOWLIMIT, strSlideShowLimit, clsProject.CONSTGROUPLIMIT, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }
            else if (boolSuccess && !proj.isExactSameSetData(clsProject.CONSTNAMESLIDESHOWLIMIT, strSlideShowLimit, clsProject.CONSTGROUPLIMIT, projId))
            {
                intRecordAffected = proj.updateConfig(clsProject.CONSTGROUPLIMIT, clsProject.CONSTNAMESLIDESHOWLIMIT, strSlideShowLimit, int.Parse(Session["ADMID"].ToString()), projId);

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }

            string strSlideShowImageLimit = txtSlideShowImageLimit.Text.Trim();
            if (boolSuccess && !proj.isConfigExist(clsProject.CONSTNAMESLIDESHOWIMAGELIMIT, clsProject.CONSTGROUPLIMIT, projId))
            {
                intRecordAffected = proj.addConfig(projId, clsProject.CONSTNAMESLIDESHOWIMAGELIMIT, strSlideShowImageLimit, clsProject.CONSTGROUPLIMIT, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }
            else if (boolSuccess && !proj.isExactSameSetData(clsProject.CONSTNAMESLIDESHOWIMAGELIMIT, strSlideShowImageLimit, clsProject.CONSTGROUPLIMIT, projId))
            {
                intRecordAffected = proj.updateConfig(clsProject.CONSTGROUPLIMIT, clsProject.CONSTNAMESLIDESHOWIMAGELIMIT, strSlideShowImageLimit, int.Parse(Session["ADMID"].ToString()), projId);

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }

            if (trEvent.Visible)
            {
                string strEventLimit = txtEventLimit.Text.Trim();
                if (boolSuccess && !proj.isConfigExist(clsProject.CONSTNAMEEVENTLIMIT, clsProject.CONSTGROUPLIMIT, projId))
                {
                    intRecordAffected = proj.addConfig(projId, clsProject.CONSTNAMEEVENTLIMIT, strEventLimit, clsProject.CONSTGROUPLIMIT, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; }
                }
                else if (boolSuccess && !proj.isExactSameSetData(clsProject.CONSTNAMEEVENTLIMIT, strEventLimit, clsProject.CONSTGROUPLIMIT, projId))
                {
                    intRecordAffected = proj.updateConfig(clsProject.CONSTGROUPLIMIT, clsProject.CONSTNAMEEVENTLIMIT, strEventLimit, int.Parse(Session["ADMID"].ToString()), projId);

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; }
                }
            }
            
            if (trCategory.Visible)
            {
                string strCategoryLimit = txtCategoryLimit.Text.Trim();
                if (boolSuccess && !proj.isConfigExist(clsProject.CONSTNAMECATEGORYLIMIT, clsProject.CONSTGROUPLIMIT, projId))
                {
                    intRecordAffected = proj.addConfig(projId, clsProject.CONSTNAMECATEGORYLIMIT, strCategoryLimit, clsProject.CONSTGROUPLIMIT, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; }
                }
                else if (boolSuccess && !proj.isExactSameSetData(clsProject.CONSTNAMECATEGORYLIMIT, strCategoryLimit, clsProject.CONSTGROUPLIMIT, projId))
                {
                    intRecordAffected = proj.updateConfig(clsProject.CONSTGROUPLIMIT, clsProject.CONSTNAMECATEGORYLIMIT, strCategoryLimit, int.Parse(Session["ADMID"].ToString()), projId);

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; }
                }
            }

            if (trProduct.Visible)
            {
                string strProductLimit = txtProductLimit.Text.Trim();
                if (boolSuccess && !proj.isConfigExist(clsProject.CONSTNAMEPRODUCTLIMIT, clsProject.CONSTGROUPLIMIT, projId))
                {
                    intRecordAffected = proj.addConfig(projId, clsProject.CONSTNAMEPRODUCTLIMIT, strProductLimit, clsProject.CONSTGROUPLIMIT, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; }
                }
                else if (boolSuccess && !proj.isExactSameSetData(clsProject.CONSTNAMEPRODUCTLIMIT, strProductLimit, clsProject.CONSTGROUPLIMIT, projId))
                {
                    intRecordAffected = proj.updateConfig(clsProject.CONSTGROUPLIMIT, clsProject.CONSTNAMEPRODUCTLIMIT, strProductLimit, int.Parse(Session["ADMID"].ToString()), projId);

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; }
                }
            }

            if (trProgGroup.Visible)
            {
                string strProgGroupLimit = txtProgGroupLmit.Text.Trim();
                if (boolSuccess && !proj.isConfigExist(clsProject.CONSTNAMEPROGGROUPLIMIT, clsProject.CONSTGROUPLIMIT, projId))
                {
                    intRecordAffected = proj.addConfig(projId, clsProject.CONSTNAMEPROGGROUPLIMIT, strProgGroupLimit, clsProject.CONSTGROUPLIMIT, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; }
                }
                else if (boolSuccess && !proj.isExactSameSetData(clsProject.CONSTNAMEPROGGROUPLIMIT, strProgGroupLimit, clsProject.CONSTGROUPLIMIT, projId))
                {
                    intRecordAffected = proj.updateConfig(clsProject.CONSTGROUPLIMIT, clsProject.CONSTNAMEPROGGROUPLIMIT, strProgGroupLimit, int.Parse(Session["ADMID"].ToString()), projId);

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; }
                }
            }

            if (trProgram.Visible)
            {
                string strProgramLimit = txtProgramLimit.Text.Trim();
                if (boolSuccess && !proj.isConfigExist(clsProject.CONSTNAMEPROGRAMLIMIT, clsProject.CONSTGROUPLIMIT, projId))
                {
                    intRecordAffected = proj.addConfig(projId, clsProject.CONSTNAMEPROGRAMLIMIT, strProgramLimit, clsProject.CONSTGROUPLIMIT, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; }
                }
                else if (boolSuccess && !proj.isExactSameSetData(clsProject.CONSTNAMEPROGRAMLIMIT, strProgramLimit, clsProject.CONSTGROUPLIMIT, projId))
                {
                    intRecordAffected = proj.updateConfig(clsProject.CONSTGROUPLIMIT, clsProject.CONSTNAMEPROGRAMLIMIT, strProgramLimit, int.Parse(Session["ADMID"].ToString()), projId);

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; }
                }
            }

            if (boolEdited)
            {
                Session["EDITEDPROJECTID"] = projId;

                Response.Redirect(Request.Url.ToString());
            }
            else
            {
                if (boolSuccess)
                {
                    Session["EDITEDPROJECTID"] = projId;
                    Session["NOCHANGE"] = 1;

                    Response.Redirect(Request.Url.ToString());
                }
                else
                {
                    proj.extractProjectById(projId, 0);

                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit Project (" + proj.projName + "). Please try again.</div>";

                    Response.Redirect(currentPageName);
                }
            }
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
            fillForm();
        }
    }

    protected void setPageProperties()
    {
        lblPageLimit.Text = clsProject.CONSTNAMEPAGELIMIT;
        lblObjectLimit.Text = clsProject.CONSTNAMEOBJECTLIMIT;
        lblSlideShowLimit.Text = clsProject.CONSTNAMESLIDESHOWLIMIT;
        lblSlideShowImageLimit.Text = clsProject.CONSTNAMESLIDESHOWIMAGELIMIT;
        lblEventLimit.Text = clsProject.CONSTNAMEEVENTLIMIT;
        lblCategoryLimit.Text = clsProject.CONSTNAMECATEGORYLIMIT;
        lblProductLimit.Text = clsProject.CONSTNAMEPRODUCTLIMIT;

        lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
    }

    protected void fillForm()
    {
        clsProject proj = new clsProject();
        DataSet ds = new DataSet();
        ds = proj.getConfigByProject(projId, 1);

        DataRow[] foundRow;

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsProject.CONSTGROUPLIMIT + "' AND CONF_NAME = '" + clsProject.CONSTNAMEPAGELIMIT + "'");
        if (foundRow.Length > 0) { txtPageLimit.Text = foundRow[0]["CONF_VALUE"].ToString(); }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsProject.CONSTGROUPLIMIT + "' AND CONF_NAME = '" + clsProject.CONSTNAMEOBJECTLIMIT + "'");
        if (foundRow.Length > 0) { txtObjectLimit.Text = foundRow[0]["CONF_VALUE"].ToString(); }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsProject.CONSTGROUPLIMIT + "' AND CONF_NAME = '" + clsProject.CONSTNAMESLIDESHOWLIMIT + "'");
        if (foundRow.Length > 0) { txtSlideShowLimit.Text = foundRow[0]["CONF_VALUE"].ToString(); }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsProject.CONSTGROUPLIMIT + "' AND CONF_NAME = '" + clsProject.CONSTNAMESLIDESHOWIMAGELIMIT + "'");
        if (foundRow.Length > 0) { txtSlideShowImageLimit.Text = foundRow[0]["CONF_VALUE"].ToString(); }


        proj.extractProjectById(projId, 0);
        type = proj.projType;

        // add by sh.chong 30 Apr 2015 - show default page limit if there is empty in the textbox
        DataSet dsProjectType = proj.getTypeList(1);
        DataRow[] dwProjectType = dsProjectType.Tables[0].Select("TYPE_ID = " + type + "");
        if (string.IsNullOrEmpty(txtPageLimit.Text) && dwProjectType.Length > 0)
        {
            DataSet dsLanguage = proj.getLanguageById(projId);
            int intNumOfLanguage = 1 + dsLanguage.Tables[0].Rows.Count;

            txtPageLimit.Text = (Convert.ToInt16(dwProjectType[0]["TYPE_PAGELIMIT"]) * intNumOfLanguage).ToString();
        }
        // end by sh.chong 30 Apr 2015 - show default page limit if there is empty in the textbox

        if (type == clsAdmin.CONSTTYPEePROFILELITE)
        {
            foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsProject.CONSTGROUPLIMIT + "' AND CONF_NAME = '" + clsProject.CONSTNAMEEVENTLIMIT + "'");
            if (foundRow.Length > 0) { txtEventLimit.Text = foundRow[0]["CONF_VALUE"].ToString(); }

            trEvent.Visible = true;
        }
        else if (type == clsAdmin.CONSTTYPEeSHOP || type == clsAdmin.CONSTTYPEeSHOPLITE || type == clsAdmin.CONSTTYPEeCATALOG || type == clsAdmin.CONSTTYPEeCATALOGLITE)
        {
            foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsProject.CONSTGROUPLIMIT + "' AND CONF_NAME = '" + clsProject.CONSTNAMECATEGORYLIMIT + "'");
            if (foundRow.Length > 0) { txtCategoryLimit.Text = foundRow[0]["CONF_VALUE"].ToString(); }

            foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsProject.CONSTGROUPLIMIT + "' AND CONF_NAME = '" + clsProject.CONSTNAMEPRODUCTLIMIT + "'");
            if (foundRow.Length > 0) { txtProductLimit.Text = foundRow[0]["CONF_VALUE"].ToString(); }

            trCategory.Visible = true;
            trProduct.Visible = true;
        }
    }
    #endregion
}
