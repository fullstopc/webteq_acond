﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Linq;

public partial class ctrl_ucAdmAgentDetails : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected string _pageListingURL = "";
    protected int _agentId = 0;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    public int agentId
    {
        get
        {
            if (ViewState["agentId"] == null)
            {
                return _agentId;
            }
            else
            {
                return int.Parse(ViewState["agentId"].ToString());
            }
        }
        set { ViewState["agentId"] = value; }
    }
    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string url = currentPageName;
            int intAdmID = Convert.ToInt32(Session["ADMID"]);

            clsEmployee agent = new clsEmployee();
            agent.name = txtName.Text.Trim();
            agent.email = txtEmail.Text.Trim();
            agent.contactTel = txtContactNo.Text.Trim();
            agent.telPrefix = txtContactPrefix.Text.Trim();
            agent.team = txtTeam.Text.Trim();
            agent.loginUsername = txtLoginUsername.Text.Trim();
            agent.active = chkActive.Checked ? 1 : 0;
            agent.ID = mode == 2 ? agentId : -1;

            if (!string.IsNullOrEmpty(txtLoginPassword.Text))
            {
                agent.loginPassword = CryptorEngine.Encrypt(txtLoginPassword.Text, true);
            }

            bool isSuccess = false;

            if (mode == 1)
            {
                if (agent.add())
                {
                    Session[clsKey.SESSION_ADDED] = agent.ID;
                    Session[clsKey.SESSION_MSG] = String.Format(GetGlobalResourceObject("GlobalResource", "admSavedMessageWithParams.Text").ToString(), "agent");
                    isSuccess = true;
                }

            }
            else if (mode == 2)
            {
                if (!agent.isSame())
                {
                    if (agent.update())
                    {
                        isSuccess = true;
                        Session[clsKey.SESSION_EDITED] = agent.ID;
                        Session[clsKey.SESSION_MSG] = String.Format(GetGlobalResourceObject("GlobalResource", "admSavedMessageWithParams.Text").ToString(), "agent");
                    }
                }
                else
                {
                    isSuccess = true;
                    Session[clsKey.SESSION_NOCHANGE] = agent.ID;
                    Session[clsKey.SESSION_MSG] = GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString();
                }
            }


            if (!isSuccess)
            {
                Session[clsKey.SESSION_ERROR] = agent.ID;
                Session[clsKey.SESSION_MSG] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit . Please try again.</div>";
            }
            else
            {
                url += "?id=" + agent.ID;
            }

            
            Response.Redirect(url);
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            if(mode == 2) { fillForm();  }

        }
    }

    protected void fillForm()
    {
        clsEmployee agent = new clsEmployee();
        if (agent.extractByID(agentId))
        {
            txtEmail.Text = agent.email;
            txtName.Text = agent.name;
            txtContactNo.Text = agent.contactTel;
            txtContactPrefix.Text = agent.telPrefix;
            txtLoginUsername.Text = agent.loginUsername;
            txtTeam.Text = agent.team;
            txtLoginPassword.Text = CryptorEngine.Decrypt(agent.loginPassword, true);
            txtLoginPasswordRepeat.Text = CryptorEngine.Decrypt(agent.loginPassword, true);
            chkActive.Checked = agent.active == 1;
            lblLastLogin.Text = ((agent.loginTimestamp == new DateTime(1900, 1, 1)) ? " - " : agent.loginTimestamp.ToString("d MMM yyyy hh:mm tt"));
            lblLastUpdated.Text = agent.lastupdated.ToString("d MMM yyyy hh:mm tt");
            lblCreation.Text = agent.creation.ToString("d MMM yyyy hh:mm tt");
            pnlLastLogin.Visible = true;
        }
    }
    #endregion

    protected void cvLoginUsername_ServerValidate(object source, ServerValidateEventArgs args)
    {
        string username = txtLoginUsername.Text.Trim();
        DataTable agents = new clsEmployee().getDataTable();
        var agent = agents.AsEnumerable()
                            .Where(x => Convert.ToString(x["employee_login_username"]) == username &&
                                    Convert.ToInt32(x["employee_id"]) != agentId);
        args.IsValid = !agent.Any();
    }
    protected void cvLoginUsername_PreRender(object sender, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("employee_login_username"))
        {
            string strJS = "ValidatorHookupControlID('" + txtLoginUsername.ClientID + "', document.all['" + cvLoginUsername.ClientID + "']);";
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "employee_login_username", strJS, true);
        }
    }

    protected void validateEmail_server(object source, ServerValidateEventArgs args)
    {
        string email = txtEmail.Text.Trim();
        DataTable agents = new clsEmployee().getDataTable();
        var agent = agents.AsEnumerable()
                            .Where(x => Convert.ToString(x["employee_email"]) == email &&
                                    Convert.ToInt32(x["employee_id"]) != agentId);
        args.IsValid = !agent.Any();
    }
    protected void cvEmail_PreRender(object source, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("employee_email"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtEmail.ClientID + "', document.all['" + cvEmail.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "mememail", strJS, false);
        }
    }

    protected void validateContactNo_server(object source, ServerValidateEventArgs args)
    {
        string prefix = txtContactPrefix.Text.Trim();
        string contactNo = txtContactNo.Text.Trim();
        DataTable agents = new clsEmployee().getDataTable();
        var agent = agents.AsEnumerable()
                            .Where(x => Convert.ToString(x["employee_tel_prefix"]) == prefix &&
                                    Convert.ToString(x["employee_contact_tel"]) == contactNo &&
                                    Convert.ToInt32(x["employee_id"]) != agentId);
        args.IsValid = !agent.Any();
    }
    protected void cvContactNo_PreRender(object sender, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("employee_contact_tel"))
        {
            string strJS = "ValidatorHookupControlID('" + txtContactNo.ClientID + "', document.all['" + cvContactNo.ClientID + "']);";
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "employee_contact_tel", strJS, true);
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string name = "";

        clsEmployee agent = new clsEmployee();
        agent.ID = agentId;

        if (agent.extractByID(agentId))
        {
            name = agent.name;
        }

        if (agent.delete() == 1)
        {
            Session[clsKey.SESSION_DELETED] = 1;
            Session[clsKey.SESSION_MSG] = "Agent " + name + " delete successfully.";
        }
        Response.Redirect(pageListingURL);
    }

    protected void lnkbtnDeleteAgentLeave_Click(object sender, EventArgs e)
    {

    }
}