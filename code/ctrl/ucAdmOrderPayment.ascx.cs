﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ctrl_ucAdmOrderPayment : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _transId = 0;
    protected int _cancel = 1;
    protected int _status = 0;

    public clsOrder ord = new clsOrder();
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int transId
    {
        get
        {
            if (ViewState["TRANSID"] == null)
            {
                return _transId;
            }
            else
            {
                return int.Parse(ViewState["TRANSID"].ToString());
            }
        }
        set { ViewState["TRANSID"] = value; }
    }

    public int cancel
    {
        get
        {
            if (ViewState["CANCEL"] == null)
            {
                return _cancel;
            }
            else
            {
                return Convert.ToInt16(ViewState["CANCEL"]);
            }
        }
        set { ViewState["CANCEL"] = value; }
    }

    public int status
    {
        get
        {
            if (ViewState["STATUS"] == null)
            {
                return _status;
            }
            else
            {
                return Convert.ToInt16(ViewState["STATUS"]);
            }
        }
        set { ViewState["STATUS"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            fillForm();
        }
    }

    protected void fillForm()
    {
        

            clsConfig config = new clsConfig();
            string strCurrency = config.currency;

            litMethod.Text = !string.IsNullOrEmpty(ord.ordPayGateway) ? ord.ordPayGateway : "-";
            litPaymentRemarks.Text = !string.IsNullOrEmpty(ord.ordPayRemarks) ? ord.ordPayRemarks.Replace("\n", "<br />") : "-";
            litPaymentAmount.Text = ord.ordPayTotal > 0 ? strCurrency + " " + ord.ordPayTotal : "-";
            litPaymentDate.Text = DateTime.Compare(ord.ordPayment, DateTime.MaxValue) != 0 ? ord.ordPayment.ToString("dd MMM yyyy") : "-";

            litBank.Text = !string.IsNullOrEmpty(ord.ordPayBankName) ? ord.ordPayBankName : "-";
            litAccount.Text = !string.IsNullOrEmpty(ord.ordPayBankAccount) ? ord.ordPayBankAccount : "-";
            litTransId.Text = !string.IsNullOrEmpty(ord.ordPayTransId) ? ord.ordPayTransId : "-";
            litPayDate.Text = DateTime.Compare(ord.ordPayBankDate, DateTime.MaxValue) != 0 ? ord.ordPayBankDate.ToString("dd MMM yyyy") : "-";

            //if (ord.payGateway <= 0)
            //{
            //    litPaymentAmount.Text = ord.payAmount > 0 ? strCurrency + " " + ord.payAmount : "-";

            //    trBank.Visible = true;
            //    trAccount.Visible = true;
            //    trTransId.Visible = true;
            //    trPayDate.Visible = true;
            //}
 
    }
    #endregion
}
