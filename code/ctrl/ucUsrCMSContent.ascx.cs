﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

public partial class ctrl_ucUsrCMSContent : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _pageId = 0;
    protected int _customId = 0;
    protected int _customId2 = 0;
    protected int _blockId = 0;
    protected bool _adminView = false;
    protected int _cmsId = 0;
    protected int _cmsType = 0;
    protected int _cmsGroup = 0;
    protected string _cmsContent = "";
    protected int _cmsCSSType = 0;
    protected string _lang = "";
    protected Boolean _boolAllowUpdate = true;
    protected int _width = 0;
    protected int _height = 0;
    protected string _cmsTitle = "";
    protected string _groupName = "";
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public string cmsTitle
    {
        get
        {
            if (ViewState["CMSTITLE"] == null) { return _cmsTitle; }
            else { return ViewState["CMSTITLE"].ToString(); }
        }
        set { ViewState["CMSTITLE"] = value; }
    }

    public string groupName
    {
        get
        {
            if (ViewState["GROUPNAME"] == null) { return _groupName; }
            else { return ViewState["GROUPNAME"].ToString(); }
        }
        set { ViewState["GROUPNAME"] = value; }
    }

    public int pageId
    {
        get
        {
            if (ViewState["PAGEID"] == null) { return _pageId; }
            else { return Convert.ToInt16(ViewState["PAGEID"]); }
        }
        set { ViewState["PAGEID"] = value; }
    }

    public int customId
    {
        get
        {
            if (ViewState["CUSTOMID"] == null) { return _customId; }
            else { return Convert.ToInt16(ViewState["CUSTOMID"]); }
        }
        set { ViewState["CUSTOMID"] = value; }
    }

    public int customId2
    {
        get
        {
            if (ViewState["CUSTOMID2"] == null) { return _customId2; }
            else { return Convert.ToInt16(ViewState["CUSTOMID2"]); }
        }
        set { ViewState["CUSTOMID2"] = value; }
    }

    public int blockId
    {
        get
        {
            if (ViewState["BLOCKID"] == null) { return _blockId; }
            else { return Convert.ToInt16(ViewState["BLOCKID"]); }
        }
        set { ViewState["BLOCKID"] = value; }
    }

    public Boolean adminView
    {
        get
        {
            if (ViewState["ADMINVIEW"] == null) { return _adminView; }
            else { return Convert.ToBoolean(ViewState["ADMINVIEW"]); }
        }
        set { ViewState["ADMINVIEW"] = value; }
    }

    public int cmsId
    {
        get
        {
            if (ViewState["CMSID"] == null) { return _cmsId; }
            else { return Convert.ToInt16(ViewState["CMSID"]); }
        }
        set { ViewState["CMSID"] = value; }
    }

    public int cmsType
    {
        get
        {
            if (ViewState["CMSTYPE"] == null) { return _cmsType; }
            else { return Convert.ToInt16(ViewState["CMSTYPE"]); }
        }
        set { ViewState["CMSTYPE"] = value; }
    }

    public int cmsGroup
    {
        get
        {
            if (ViewState["CMSGROUP"] == null)
            {
                return _cmsGroup;
            }
            else
            {
                return Convert.ToInt16(ViewState["CMSGROUP"]);
            }
        }
        set { ViewState["CMSGROUP"] = value; }
    }

    public string lang
    {
        get { return ViewState["LANG"] as string ?? _lang; }
        set { ViewState["LANG"] = value; }
    }

    public string cmsContent
    {
        get { return ViewState["CMSCONTENT"] as string ?? _cmsContent; }
        set { ViewState["CMSCONTENT"] = value; }
    }

    public int cmsCSSType
    {
        get
        {
            if (ViewState["CMSCSSTYPE"] == null) { return _cmsCSSType; }
            else { return Convert.ToInt16(ViewState["CMSCSSTYPE"]); }
        }
        set { ViewState["CMSCSSTYPE"] = value; }
    }

    public Boolean boolAllowUpdate
    {
        get
        {
            if (ViewState["BOOLALLOWUPDATE"] == null) { return _boolAllowUpdate; }
            else { return Convert.ToBoolean(ViewState["BOOLALLOWUPDATE"]); }
        }
        set { ViewState["BOOLALLOWUPDATE"] = value; }
    }

    public int width
    {
        get
        {
            if (ViewState["WIDTH"] == null) { return _width; }
            else { return Convert.ToInt16(ViewState["WIDTH"]); }
        }
        set { ViewState["WIDTH"] = value; }
    }

    public int height
    {
        get
        {
            if (ViewState["HEIGHT"] == null) { return _height; }
            else { return Convert.ToInt16(ViewState["HEIGHT"]); }
        }
        set { ViewState["HEIGHT"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        HtmlControl htmlctrlCSS = null;
        htmlctrlCSS = this.Page.Header.FindControl("CMSContent") as HtmlControl;

        if (htmlctrlCSS == null)
        {
            HtmlLink cssLink = new HtmlLink();
            cssLink.ID = "CMSContent";
            cssLink.Href = ConfigurationManager.AppSettings["cssBase"] + "cmscontent.css";
            cssLink.Attributes.Add("rel", "stylesheet");
            cssLink.Attributes.Add("type", "text/css");

            this.Page.Header.Controls.Add(cssLink);
        }

        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        clsPageObject po = new clsPageObject();
        int intRecordAffected = 0;

        int intAdminView = 0;
        if (adminView) { intAdminView = 1; }
        
        if (!string.IsNullOrEmpty(lang)) { intRecordAffected = po.deleteItemById(pageId, customId, customId2, blockId, cmsId, cmsType, lang, intAdminView, cmsGroup); }
        else { intRecordAffected = po.deleteItemById(pageId, customId, customId2, blockId, cmsId, cmsType, intAdminView, cmsGroup); }

        if (intRecordAffected == 1)
        {
            clsMis.resetListing();

            Response.Redirect(Request.Url.ToString());
        }
        else
        {
            litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "userErrHdr.Text") + "<br />" + GetGlobalResourceObject("GlobalResource", "userErrUpdateContent.Text") + "</div>";

            pnlAck.Visible = true;
        }
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "userConfirmDeleteContent.Text").ToString() + "'); return false;";

        if (Session["ADMID"] != null && Session["ADMID"] != "" && boolAllowUpdate)
        {
            pnlCMSAction.Visible = true;
        }
        else
        {
            pnlCMSAction.Visible = false;
        }

        fillForm();
    }

    protected void fillForm()
    {
        if (cmsGroup == clsAdmin.CONSTCMSGROUPCONTENT)
        {
            litCMSContent.Text = "<div>" + cmsContent + "</div>";
            pnlCMSContentContent.Visible = true;
            litTitle.Text = cmsTitle;
        }
        else if (cmsGroup == clsAdmin.CONSTCMSGROUPSLIDESHOW)
        {
            ucUsrMasthead.grpId = cmsId;
            pnlCMSContentSlideShow.Visible = true;
            litTitle.Text = groupName;
        }

        pnlCMSContent.CssClass = "divCMSContentOuter" + cmsCSSType;

        if (height > 0) { pnlCMSContentInner.Height = height; }
        if (width > 0) { pnlCMSContentInner.Width = width; }
    }
    #endregion
}
