﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Configuration;

public partial class ctrl_ucAdmCheckAccessPage : System.Web.UI.UserControl
{
    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        if (Session[clsAdmin.CONSTPROJECTTYPE] != null && Session[clsAdmin.CONSTPROJECTTYPE] != "")
        {
            string strCurUrl = Path.GetFileName(Request.PhysicalPath);
            int intType = int.Parse(Session[clsAdmin.CONSTPROJECTTYPE].ToString());

            clsAdmin adm = new clsAdmin();
            DataSet ds = new DataSet();
            ds = adm.getAdminPageList2(strCurUrl, 1);
            DataView dv = new DataView(ds.Tables[0]);
            dv.RowFilter = "1 = 1";
            //dv.RowFilter = "ADMPAGE_URL LIKE '%" + Path.GetFileName(Request.PhysicalPath) + "%'";

            // if page is found
            if (dv.Count > 0)
            {
                dv.RowFilter += " AND (V_TYPE = " + int.Parse(Session[clsAdmin.CONSTPROJECTTYPE].ToString()) + " OR V_TYPE IS NULL)";

                if (dv.Count <= 0)
                {
                    Session["ADMACCESSDENIED"] = 1;
                    Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "adm/" + clsAdmin.CONSTADMACCESSDENIEDPAGE);
                }
            }
        }
    }
    #endregion
}
