﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmOrderItems.ascx.cs" Inherits="ctrl_ucAdmOrderItems" %>

<asp:Panel ID="pnlItems" runat="server">
    <asp:Repeater ID="rptItems" runat="server" OnItemDataBound="rptItems_ItemDataBound" OnItemCommand="rptItems_ItemCommand">
        <HeaderTemplate>
            <table class="dataTbl" cellpadding="0" cellspacing="0" border="0" id="tblOrderItems">
                <colgroup>
                    <col class="first" />
                    <col class="second" />
                    <col class="third" />
                    <col class="fourth" />
                    <col class="fifth" />
                </colgroup>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Item</th>
                        <th><asp:Literal ID="litPriceHeader" runat="server"></asp:Literal></th>
                        <th>Qty</th>
                        <th><asp:Literal ID="litTotalHeader" runat="server"></asp:Literal></th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
                <tr class="trItem">
                    <td id="tdNo" runat="server"><asp:Literal ID="litNo" runat="server"></asp:Literal></td>
                    <td id="tdItem" runat="server" class="td_norLeft">
                        <div class="divOrderProdItemDetail">
                            <div class="divOrderProdImg">
                                <div class="divOrderProdImgInner">
                                    <asp:Image ID="imgProd" runat="server" />
                                </div>
                            </div>
                            <div class="divOrderProdDetail">
                                <asp:Literal ID="litProdCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRODCODE").ToString().ToUpper() %>'></asp:Literal><br />
                                <asp:Literal ID="litProdName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRODDNAME").ToString().ToUpper() %>'></asp:Literal><br />
                                <asp:Literal ID="litProdPricingName1" runat="server" Text='<%# !string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRICINGNAME1").ToString()) ? DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRICINGNAME1").ToString() + "<br />" : "" %>'></asp:Literal>
                                <asp:Literal ID="litProdPricingName2" runat="server" Text='<%# !string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRICINGNAME2").ToString()) ? DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRICINGNAME2").ToString() + "<br />" : "" %>'></asp:Literal>                                
                                <asp:HiddenField ID="hdnProdId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRODID") %>' />
                                <asp:HiddenField ID="hdnDetailId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ORDDETAIL_ID") %>' />
                                <asp:Repeater ID="rptOrdProdSpec" runat="server" OnItemDataBound="rptOrdProdSpec_ItemDataBound">
                                    <HeaderTemplate><ul class="ulOrdSpec"></HeaderTemplate>
                                    <ItemTemplate>
                                        <li><asp:Literal ID="litProdSpec" runat="server"></asp:Literal></li>
                                    </ItemTemplate>
                                    <FooterTemplate></ul></FooterTemplate>
                                </asp:Repeater>
                                <br /><asp:LinkButton ID="lnkbtnRemove" runat="server" Text="remove" CommandName="cmdRemove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRODID") + "|" + DataBinder.Eval(Container.DataItem, "ORDDETAIL_ID") %>' CssClass="lnkbtn lnkbtnDelete"></asp:LinkButton>
                            </div>
                        </div>
                    </td>
                    <td id="tdUnitPrice" runat="server" class="td_right">
                        <asp:Literal ID="litProdPrice" runat="server"></asp:Literal>
                        <asp:HiddenField ID="hdnProdPrice" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRODPRICE") %>' />
                        <asp:HiddenField ID="hdnProdPricing" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRODPRICING") %>' />
                    </td>
                    <td id="tdQty" runat="server">
                        <asp:TextBox ID="txtProdQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRODQTY") %>' MaxLength="10" CssClass="text_small" Visible="false"></asp:TextBox>
                        <asp:HiddenField ID="hdnProdQty" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRODQTY") %>' />
                        <asp:Literal ID="litProdQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRODQTY") %>' Visible="false"></asp:Literal>
                        <span class="spanFieldText"><asp:Literal ID="litProdUnit" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRODUOM") %>'></asp:Literal></span>
                    </td>
                    <td id="tdPrice" runat="server" class="td_right">
                        <asp:Literal ID="litProdTotal" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRODTOTAL") %>'></asp:Literal>
                    </td>
                </tr>
        </ItemTemplate>
        <FooterTemplate>
                <tr class="trSpacer">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
            <tfoot>
                <tr class="trFooter">
                    <td id="tdItemSubtotalFooter" runat="server" colspan="3"></td>
                    <td class="td_right">Subtotal</td>
                    <td class="td_right"><asp:Literal ID="litSubtotal" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trShipping" runat="server" class="trFooter">
                    <td id="tdItemShippingFooter" runat="server" colspan="3"></td>
                    <td class="td_right">Postage</td>
                    <td class="td_right"><asp:Literal ID="litShipping" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trDiscount" runat="server" class="trFooter" visible="false">
                    <td id="tdItemDiscountFooter" runat="server" colspan="2"></td>
                    <td class="td_right" colspan="2"><asp:Label ID="lblDiscount" runat="server"></asp:Label></td>
                    <td class="td_right"><asp:Literal ID="litDiscount" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trProdGST" runat="server" class="trFooter" visible="false">
                    <td id="tdItemProdGSTFooter" runat="server" colspan="3"></td>
                    <td class="td_right">GST</td>
                    <td class="td_right"><asp:Literal ID="litGST" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trGST" runat="server" class="trFooter" visible="false">
                    <td id="tdItemGSTFooter" runat="server" colspan="3"></td>
                    <td class="td_right"><asp:Label ID="lblGST" runat="server"></asp:Label></td>
                    <td class="td_right"><asp:Literal ID="litTotalGST" runat="server"></asp:Literal></td>
                </tr>
                <tr class="trFooter">
                    <td id="tdItemTotalFooter" runat="server" colspan="3"></td>
                    <td class="td_right"><span class="boldmsg">Order Total</span></td>
                    <td class="td_right"><span class="boldmsg"><asp:Literal ID="litTotal" runat="server"></asp:Literal></span></td>
                </tr>
            </tfoot>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Panel>
<asp:Panel ID="pnlButton" runat="server" CssClass="divFormBtnRight" Visible="false">
    <asp:LinkButton ID="lnkbtnRevert" runat="server" Text="Revert to Initial Input" ToolTip="Revert to Initial Input" CssClass="btn btnRevert" CausesValidation="false" OnClick="lnkbtnRevert_Click"></asp:LinkButton>
    <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
</asp:Panel>
