﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmEnquiry.ascx.cs" Inherits="ctrl_ucAdmEnquiry" %>
<asp:Panel ID="pnlForm" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table id="tblEnquiry" cellpadding="0" cellspacing="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr">Enquiry Content</td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Recipient Email:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litRecipientEmail" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Domain URL:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litDomainURL" runat="server"></asp:Literal>
                </td>
            </tr>
             <tr>
                <td class="tdSpacer2">&nbsp;</td>
                <td class="tdSpacer2">&nbsp;</td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" class="formTbl tblData"> 
            <tr>
                <td colspan="2" class="tdSectionHdr">Enquiry Content</td>
            </tr>
            <tr>
                <td class="tdLabel">Received Date:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litSentDate" runat="server"></asp:Literal>
                    <asp:Literal ID="litResend" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Salutation:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litSalutation" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Name:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litName" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Company Name:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litCompanyName" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Contact:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litContact" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Email:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litEmail" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Subject:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litSubject" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Message:</td>
                <td class="tdLabel">
                    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr id="trLocation" visible="false" runat="server">
                <td class="tdLabel">Location:</td>
                <td class="tdLabel">
                    <div id="map" style="width:800px;height:480px;border:1px solid #ececec;"></div>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer2">&nbsp;</td>
                <td class="tdSpacer2">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdSpacer2">&nbsp;</td>
                <td class="tdSpacer2">&nbsp;</td>
            </tr>
            <tfoot>
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="pnlAction" runat="server">
                            <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false" Visible="false"></asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnResend" runat="server" Text="Resend" ToolTip="Resend" class="btn btnResend" OnClick="lnkbtnResend_Click" OnClientClick="return confirm('Are you sure you want to resend email?');"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>
<% if (trLocation.Visible){ %>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>gomap-1.3.2.min.js" type="text/javascript"></script>
<script>
$(function() {
    
    $.getJSON( "http://maps.googleapis.com/maps/api/geocode/json?latlng=<%= strLat %>,<%= strLng %>&sensor=true", function( json ) {
        var addr = json.results[0].formatted_address;
        console.log(addr);
        $("#map").goMap({
            markers: [{
                latitude: <%= strLat %>,
                longitude: <%= strLng %>,
                html: { 
                    content: addr, 
                    popup: true 
                }
            }],
            zoom: 15,
            maptype: 'ROADMAP',
            scaleControl: true,
        });
     });
});
</script>
<% } %>