﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCmnSiblingPages.ascx.cs" Inherits="ctrl_ucCmnSiblingPages" %>
<%@ Register Src="~/ctrl/ucUsrLoading.ascx" TagName="UsrLoading" TagPrefix="uc" %>

<asp:Panel ID="pnlCmnSiblingPages" runat="server" CssClass="divCmnSiblingPages">
    <asp:UpdatePanel ID="upnlSiblingPages" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="uprgSiblingPages" runat="server" AssociatedUpdatePanelID="upnlSiblingPages" DynamicLayout="true">
                <ProgressTemplate>
                    <uc:UsrLoading ID="ucUsrLoading" runat="server" />
                </ProgressTemplate>
            </asp:UpdateProgress>
            
            <table cellpadding="0" cellspacing="0" border="0" class="siblingpageTbl">
                <tr>
                    <td class="tdSpace20"></td>
                    <td class="tdSpace20"></td>
                </tr>
                <tr>
                    <td id="tdSiblingPage" class="tdSiblingPages">
                        <asp:Label ID="lblSiblingPages" runat="server" meta:resourcekey="lblSiblingPages" CssClass="divSectionTitle"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdSpace20"></td>
                    <td class="tdSpace20"></td>
                </tr>
                <tr>
                    <td><asp:Literal ID="litFound" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td class="tdSpace20"></td>
                    <td class="tdSpace20"></td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" class="dataTbl">
                            <thead>
                                <tr>
                                    <th class="tdNo"><asp:Literal ID="litNo" runat="server"></asp:Literal></th>
                                    <th class="tdPageName"><asp:Literal ID="litPageName" runat="server"></asp:Literal></th>
                                    <th class="tdLink"><asp:Literal ID="litLink" runat="server"></asp:Literal></th>
                                    <th class="tdAction"><asp:Literal ID="litAction" runat="server"></asp:Literal></th>   
                                </tr>
                            </thead>
                            <tbody id="tdItems" runat="server" visible="false" >
                                <asp:Repeater ID="rptItems" runat="server" OnItemDataBound="rptItems_ItemDataBound" OnItemCommand="rptItems_ItemCommand">
                                    <HeaderTemplate></HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td class="tdPageNo"><asp:Literal ID="litPageNo" runat="server"></asp:Literal></td>
                                            <td class="tdItem"><asp:Literal ID="litPageTitle" runat="server"></asp:Literal></td>
                                            <td class="tdDetails"><asp:HyperLink ID="hypSiblingPages" runat="server" CssClass="btnSP"></asp:HyperLink></td>
                                            <td id="tdAct" runat="server" visible="false" class="tdAct">
                                                <asp:LinkButton ID="lnkbtnUp" runat="server" CssClass="btnSP lnkbtn lnkbtnUp" CommandName="cmdUp" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PAGE_ID") + "|" + DataBinder.Eval(Container.DataItem, "PAGE_ORDER") %>'></asp:LinkButton>   
                                                <asp:LinkButton ID="lnkbtnDown" runat="server" CssClass="btnSP lnkbtn lnkbtnDown" CommandName="cmdDown" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PAGE_ID") + "|" + DataBinder.Eval(Container.DataItem, "PAGE_ORDER") %>'></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tr class="trItem">
                                            <td class="tdPageNo"><asp:Literal ID="litPageNo" runat="server"></asp:Literal></td>
                                            <td class="tdItem"><asp:Literal ID="litPageTitle" runat="server"></asp:Literal></td>
                                            <td class="tdDetails"><asp:HyperLink ID="hypSiblingPages" runat="server" CssClass="btnSP"></asp:HyperLink></td>
                                            <td id="tdAct" runat="server" visible="false" class="tdAct">
                                                <asp:LinkButton ID="lnkbtnUp" runat="server" CssClass="btnSP lnkbtn lnkbtnUp" CommandName="cmdUp" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PAGE_ID") + "|" + DataBinder.Eval(Container.DataItem, "PAGE_ORDER") %>'></asp:LinkButton>   
                                                <asp:LinkButton ID="lnkbtnDown" runat="server" CssClass="btnSP lnkbtn lnkbtnDown" CommandName="cmdDown" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PAGE_ID") + "|" + DataBinder.Eval(Container.DataItem, "PAGE_ORDER") %>'></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate></FooterTemplate>
                                </asp:Repeater>
                                <tr>
                                    <td id="tdNoItem" runat="server" visible="false" colspan="2">
                                        <asp:Literal ID="litNoItem" runat="server" meta:resourcekey="litNoItem"></asp:Literal>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr id="trBtn" runat="server" visible="false">
                    <td class="tdBtn"><asp:LinkButton ID="lnkbtnBack" runat="server" meta:resourcekey="lnkbtnBack" CssClass="btn btnBack"></asp:LinkButton></td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
