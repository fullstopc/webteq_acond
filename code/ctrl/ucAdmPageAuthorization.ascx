﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmPageAuthorization.ascx.cs" Inherits="ctrl_ucAdmPageAuthorization" %>

<script type="text/javascript">
     function validatePassword_client(source, args) {
            if(args.Value.length < <% =clsAdmin.CONSTADMPASSWORDMINLENGTH %>)
            {
                args.IsValid = false;
                
                source.errormessage = "<br />Min. length for password is 6.";
                source.innerHTML = "<br />Min. length for password is 6.";
            }            
        }
        
        function validatePwd_client(source, args) {
            var txtPassword = document.getElementById("<%= txtPwd.ClientID %>");
            var hdnPwd = document.getElementById("<%= hdnPwd.ClientID %>");
            
            var boolValid = false;

            source.errormessage = "<br />Please enter current password.";
            source.innerHTML = "<br />Please enter current password.";

            if ((Trim(txtPassword.value) != '') || (Trim(hdnPwd.value) != '' )) {
                args.IsValid = true;
            }
            else {
                args.IsValid = boolValid;
            }
        }
        
        function validateIndPwd_client(source, args) {
            var txtIndPwd = document.getElementById("<%= txtIndPwd.ClientID %>");
            var hdnIndPwd = document.getElementById("<%= hdnIndPwd.ClientID %>");

            var indBoolValid = false;

            source.errormessage = "<br />Please enter password.";
            source.innerHTML = "<br />Please enter password.";

            
            if ((Trim(txtIndPwd.value) != '') || (Trim(hdnIndPwd.value) != '' )) {
                args.IsValid = true;
            }
            else {
                args.IsValid = indBoolValid;
            }
        }
        
        function validateConfirmPwd_client(source, args) {
            var txtPassword = document.getElementById("<%= txtPwd.ClientID %>");
            var txtConfirmPwd = document.getElementById("<%= txtConfirmPwd.ClientID %>");

            var boolValid = false;

            source.errormessage = "<br />Please enter password to confirm.";
            source.innerHTML = "<br />Please enter password to confirm.";

            if (Trim(txtConfirmPwd.value) != '') {
                args.IsValid = true;
            }
            else {
                if (Trim(txtPassword.value) != '') {
                    boolValid = false;
                }
                else {
                    boolValid = true;
                }

                args.IsValid = boolValid;
            }
        }
        
        function validateConfirmIndPwd_client(source, args) {
            var txtIndPwd = document.getElementById("<%= txtIndPwd.ClientID %>");
            var txtIndConfirmPwd = document.getElementById("<%= txtIndConfirmPwd.ClientID %>");

            var boolValid = false;

            source.errormessage = "<br />Please enter password to confirm.";
            source.innerHTML = "<br />Please enter password to confirm.";

            if (Trim(txtIndConfirmPwd.value) != '') {
                args.IsValid = true;
            }
            else {
                if (Trim(txtIndPwd.value) != '') {
                    boolValid = false;
                }
                else {
                    boolValid = true;
                }

                args.IsValid = boolValid;
            }
        }
</script>
<asp:Panel ID="pnlAdmCMSControlPanel" runat="server" CssClass="divAdmCMSControlPanel">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table class="formTbl tblData noBorderBtm" cellpadding="0" cellspacing="0" id="trUniversalSettings" runat="server">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Universal Setting</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdLabel">Login<span class="attention_compulsory">*</span>:</td>
                    <td class="tdMax"><asp:TextBox ID="txtLogin" runat="server" class="text_big2" MaxLength="250"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvLogin" runat="server" ErrorMessage="<br />Please enter Login." ControlToValidate="txtLogin" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgLoginSetting"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revLogin" Text="Please enter valid email." runat="server" ControlToValidate="txtLogin" Display="Dynamic" CssClass="errmsg" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="vgLoginSetting"></asp:RegularExpressionValidator></td>
                </tr>
                <tr>
                    <td class="tdLabel">Password<span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtPwd" runat="server" class="text_big2 compulsory" MaxLength="250" TextMode="Password"></asp:TextBox>
                        <asp:HiddenField ID="hdnPwd" runat="server" />
                        <span class="">
                        <asp:CustomValidator ID="cvPwd" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtPwd" ClientValidationFunction="validatePwd_client" ValidateEmptyText="true" ValidationGroup="vgLoginSetting"></asp:CustomValidator>
                        <asp:CustomValidator ID="cvPwd2" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtPwd" ClientValidationFunction="validatePassword_client" ValidationGroup="vgLoginSetting"></asp:CustomValidator>
                        </span>
                    </td>
                </tr> 
                <tr>
                    <td class="tdLabel nobr">Confirm Password<span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtConfirmPwd" runat="server" class="text_big2 compulsory" MaxLength="250" TextMode="Password"></asp:TextBox>
                        <asp:CustomValidator ID="cvConfirmPwd" runat="server" Display="Dynamic" ControlToValidate="txtConfirmPwd" CssClass="errmsg" ClientValidationFunction="validateConfirmPwd_client" ValidateEmptyText="true" ValidationGroup="vgLoginSetting"></asp:CustomValidator>
                        <asp:CustomValidator ID="cvConfirmPwd2" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtConfirmPwd" ClientValidationFunction="validatePassword_client" ValidationGroup="vgLoginSetting"></asp:CustomValidator>
                        <asp:CompareValidator ID="cmvPassword" runat="server" ErrorMessage="<br />Password does not match." ControlToCompare="txtPwd" ControlToValidate="txtConfirmPwd" Display="Dynamic" class="errmsg" ValidationGroup="vgLoginSetting"></asp:CompareValidator>
                    </td>
                </tr>             
            </tbody>
        </table>
        <table class="formTbl tblData noBorderBtm" cellpadding="0" cellspacing="0" id="trIndividualSettings" runat="server" visible="false">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Individual Setting</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdLabel">Company Name<span class="attention_compulsory">*</span>:</td>
                    <td class="tdMax"><asp:TextBox ID="txtCompanyName" runat="server" class="text_big2" MaxLength="250"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCompanyName" runat="server" ErrorMessage="<br />Please enter company name." ControlToValidate="txtCompanyName" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgLoginSetting"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="tdLabel">Contact Person:</td>
                    <td class="tdMax"><asp:TextBox ID="txtContactPerson" runat="server" class="text_big2" MaxLength="250"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel">Email<span class="attention_compulsory">*</span>:</td>
                    <td colspan="2">
                        <asp:TextBox ID="txtEmail" runat="server" class="text_big2" MaxLength="250"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="<br />Please enter email." ControlToValidate="txtEmail" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgLoginSetting"></asp:RequiredFieldValidator>
                        <span class="fieldDesc"><asp:RegularExpressionValidator ID="revMail" ErrorMessage="<br />Please enter valid email." runat="server" ControlToValidate="txtEmail" Display="Dynamic" CssClass="errmsg" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel">Contact No:</td>
                    <td class="tdMax"><asp:TextBox ID="txtContactNo" runat="server" class="text_big2" MaxLength="20"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel">Remarks:</td>
                    <td class="tdMax"><asp:TextBox ID="txtRemarks" runat="server" class="text_fullwidth" MaxLength="1000" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel">Login<span class="attention_unique">*</span>:</td>
                    <td class="tdMax"><asp:TextBox ID="txtIndLogin" runat="server" class="text_big2" MaxLength="250"></asp:TextBox>
                    <span class="spanFieldDesc"><a href="javascript:void(0);" onclick="copyValue('<% =txtEmail.ClientID %>','<% =txtIndLogin.ClientID %>');" title="same as Email">same as Email</a></span>
                    <asp:RequiredFieldValidator ID="rfvIndLogin" runat="server" ErrorMessage="<br />Please enter Login." ControlToValidate="txtIndLogin" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgLoginSetting"></asp:RequiredFieldValidator>
                    <span class="spanFieldDesc"><asp:CustomValidator ID="cvIndLogin" runat="server" ControlToValidate="txtIndLogin" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateIndLogin_server" OnPreRender="cvIndLogin_PreRender" ValidationGroup="vgLoginSetting"></asp:CustomValidator></span></td>
                </tr>
                <tr>
                    <td class="tdLabel">Password<span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtIndPwd" runat="server" class="text_big2 compulsory" MaxLength="250" TextMode="Password"></asp:TextBox>
                        <asp:HiddenField ID="hdnIndPwd" runat="server" />
                        <asp:CustomValidator ID="cvIndPwd" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtIndPwd" ClientValidationFunction="validateIndPwd_client" ValidateEmptyText="true" ValidationGroup="vgLoginSetting"></asp:CustomValidator>
                        <asp:CustomValidator ID="cvIndPwd2" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtIndPwd" ClientValidationFunction="validatePassword_client" ValidationGroup="vgLoginSetting"></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel nobr">Confirm Password<span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtIndConfirmPwd" runat="server" class="text_big2 compulsory" MaxLength="250" TextMode="Password"></asp:TextBox>
                        <asp:CustomValidator ID="cvIndConfirmPwd" runat="server" Display="Dynamic" ControlToValidate="txtIndConfirmPwd" CssClass="errmsg" ClientValidationFunction="validateConfirmIndPwd_client" ValidateEmptyText="true" ValidationGroup="vgLoginSetting"></asp:CustomValidator>
                        <asp:CustomValidator ID="cvIndConfirmPwd2" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtIndConfirmPwd" ClientValidationFunction="validatePassword_client" ValidationGroup="vgLoginSetting"></asp:CustomValidator>
                        <asp:CompareValidator ID="cmvIndPassword" runat="server" ErrorMessage="<br />Password does not match." ControlToCompare="txtIndPwd" ControlToValidate="txtIndConfirmPwd" Display="Dynamic" class="errmsg" ValidationGroup="vgLoginSetting"></asp:CompareValidator>
                    </td>
                </tr>     
                <tr>
                    <td class="tdSpacer2" colspan="2">&nbsp;</td>
                </tr> 
             </tbody>           
        </table>
        <table class="formTbl tblData noBorderBtm" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="2">
                    <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                        <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="vgLoginSetting"></asp:LinkButton>
                    </asp:Panel>
                </td>
            </tr>   
        </table> 
        <table class="formTbl tblData" cellpadding="0" cellspacing="0" id="tblIndLoginListing" visible="false" runat="server">
            <tr>
                <td class="tdSpacer2" colspan="2">&nbsp;</td>
            </tr> 
            <tr>
                <td colspan="2">
                    <asp:Panel ID="pnlIndLoginList" runat="server" CssClass="divIndLoginList">
                        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
                            <div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
                        </asp:Panel>
                        <div class="divListingIndLogin">
                            <asp:GridView ID="gvIndLogin" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom" CssClass="dataTbl" OnRowDataBound="gvIndLogin_RowDataBound" OnSorting="gvIndLogin_Sorting" OnPageIndexChanging="gvIndLogin_PageIndexChanging" OnRowCommand="gvIndLogin_RowCommand" DataKeyNames="PGAUTH_ID" EmptyDataText="No record found.">
                                <PagerStyle CssClass="pg" HorizontalAlign="Right" />
                                <AlternatingRowStyle CssClass="td_alt" />
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemStyle CssClass="tdNo" />
                                        <ItemTemplate>
                                            <asp:Literal ID="litNo" runat="server" Text="<%#Container.DataItemIndex + 1 %>"></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Company Name" DataField="PGAUTH_INDCOMPANYNAME" SortExpression="PGAUTH_INDCOMPANYNAME" HeaderStyle-CssClass="td_big3" ItemStyle-CssClass="td_big3" />
                                    <asp:BoundField HeaderText="Contact Person" DataField="PGAUTH_INDCONTACTPERSON" HeaderStyle-CssClass="td_big3" ItemStyle-CssClass="td_big3" />
                                    <asp:BoundField HeaderText="Email" DataField="PGAUTH_INDEMAIL" SortExpression="PGAUTH_INDEMAIL" HeaderStyle-CssClass="td_big4" ItemStyle-CssClass="td_big4" />
                                    <asp:BoundField HeaderText="Contact No" DataField="PGAUTH_INDCONTACTNO" SortExpression="PGAUTH_INDCONTACTNO" HeaderStyle-CssClass="td_big3" ItemStyle-CssClass="td_big3" />
                                    <asp:BoundField HeaderText="Remarks" DataField="PGAUTH_INDREMARKS" SortExpression="PGAUTH_INDREMARKS" HeaderStyle-CssClass="td_big3" ItemStyle-CssClass="td_big3" />
                                    <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="tdAct">
                                        <ItemStyle CssClass="tdAct" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypEdit" CssClass="lnkbtn lnkbtnEdit" runat="server"></asp:HyperLink>
                                            <span class="spanSplitter">|</span>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CssClass="lnkbtn lnkbtnDelete" CausesValidation="false" CommandName="cmdDelete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PGAUTH_ID") %>'></asp:LinkButton>
                                         </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                </td>
            </tr>  
        </table>
    </asp:Panel>
</asp:Panel>
