﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmHighlightArticle.ascx.cs" Inherits="ctrl_ucAdmHighlightArticle" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>

<script type="text/javascript">
    var intLoadCount = 0;

    function iFrmArticleLoad() {
        lnkbtnArticle = document.getElementById("<%= lnkbtnArticle.ClientID %>");
        intLoadCount = intLoadCount + 1;

        if (intLoadCount > 1) {
            window.location.href = lnkbtnArticle.href;
        }
    }
</script>
<asp:Panel ID="pnlForm" runat="server" CssClass="tdLoading">
    <table id="tblAddArticle" cellpadding="0" cellspacing="0" class="formTbl tblData">
        <tr>
            <td colspan="2" style="padding: 0;">
                <iframe name="iFrmArticle" id="iFrmArticle" class="tdFrame" src="<% =System.Configuration.ConfigurationManager.AppSettings["scriptBase"] %>cmn/fileupload.aspx?" frameborder="0" scrolling="no" onload="iFrmArticleLoad();"></iframe>
                <asp:UpdatePanel ID="upnliFrame" runat="server">
                    <ContentTemplate>
                        <asp:UpdateProgress ID="uprgiFrame" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="upnliFrame">
                            <ProgressTemplate>
                                <uc:AdmLoading ID="ucAdmLoading1" runat="server" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:LinkButton ID="lnkbtnArticle" runat="server" CausesValidation="false" style="visibility:hidden;" OnClick="lnkbtnArticle_Click"></asp:LinkButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="formTbl tblData">
        <tr>
            <td colspan="2" class="tdSectionHdr">Uploaded Article(s):</td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:UpdatePanel ID="upnlArticle" runat="server">
                    <ContentTemplate>
                        <asp:UpdateProgress ID="uprgArticle" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="upnlArticle">
                            <ProgressTemplate>
                                <uc:AdmLoading ID="ucAdmLoading" runat="server" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:Literal ID="litArticle" runat="server" Visible="false"></asp:Literal>
                        <asp:Panel ID="pnlArticle" runat="server">
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tfoot>
            <tr>
                <td colspan="2">
                    <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                        <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                    </asp:Panel>
                </td>
            </tr>
        </tfoot>
    </table>
    
</asp:Panel>
