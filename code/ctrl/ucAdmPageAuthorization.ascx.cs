﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

public partial class ctrl_ucAdmPageAuthorization : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _pgAuthId = 0;
    protected int _mode = 1;
    protected string _gvSortExpression = "PGAUTH_ID";
    protected string _gvSortDirection = clsAdmin.CONSTSORTASC;
    protected string _gvSort = "";
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    protected string gvSortExpression
    {
        get { return ViewState["GVSORTEXPRESSION"] as string ?? _gvSortExpression; }
        set { ViewState["GVSORTEXPRESSION"] = value; }
    }

    protected string gvSortDirection
    {
        get { return ViewState["GVSORTDIRECTION"] as string ?? _gvSortDirection; }
        set { ViewState["GVSORTDIRECTION"] = value; }
    }

    protected string gvSort
    {
        get { return ViewState["GVSORT"] as string ?? _gvSort; }
        set { ViewState["GVSORT"] = value; }
    }

    public int pgAuthId
    {
        get
        {
            if (ViewState["PGAUTHID"] == null)
            {
                return _pgAuthId;
            }
            else
            {
                return int.Parse(ViewState["PGAUTHID"].ToString());
            }
        }
        set { ViewState["PGAUTHID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void validateIndLogin_server(object source, ServerValidateEventArgs args)
    {
        clsPage page = new clsPage();
        if (page.isIndLoginPageAuthExist(txtIndLogin.Text, pgAuthId))
        {
            args.IsValid = false;
            cvIndLogin.ErrorMessage = "This " + txtIndLogin.Text + " is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvIndLogin_PreRender(object source, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("individuallogin")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtIndLogin.ClientID + "', document.all['" + cvIndLogin.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "individuallogin", strJS);
        }
    }

    protected void gvIndLogin_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell tc in e.Row.Cells)
            {
                if (tc.HasControls())
                {
                    LinkButton lnkbtn = (LinkButton)tc.Controls[0];
                    //Image icon = new Image();
                    //icon.ImageUrl = "../img/adm/btn-cancel.gif";

                    if (lnkbtn != null && gvSortExpression == lnkbtn.CommandArgument)
                    {
                        //tc.Controls.Add(icon);
                        string strClass = (gvSortDirection == "DESC") ? "desc" : "asc";
                        tc.CssClass = strClass;
                    }


                }
            }
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //LinkButton lnkbtnEdit = (LinkButton)e.Row.FindControl("lnkbtnEdit");
            LinkButton lnkbtnDelete = (LinkButton)e.Row.FindControl("lnkbtnDelete");
            HyperLink hypEdit = (HyperLink)e.Row.FindControl("hypEdit");

            int intId = Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "PGAUTH_ID"));
            hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admPageAuthorization01.aspx?id=" + intId;

            //lnkbtnEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteIndPgAuth.Text") + "');";
        }
    }

    protected void gvIndLogin_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "cmdDelete")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int pgAuthId = Convert.ToInt16(gvIndLogin.DataKeys[rowIndex].Value.ToString());
            string strDeletedName = "";

            clsMis.performDeletePageAuthorization(pgAuthId, ref strDeletedName);
            Session["DELETEINDPGAUTHID"] = strDeletedName;

            Response.Redirect(currentPageName);
        }
    }

    protected void gvIndLogin_Sorting(object sender, GridViewSortEventArgs e)
    {
        string strSortExpDir = "";
        if (e.SortExpression != gvSortExpression)
        {
            gvSortExpression = e.SortExpression;
            gvSortDirection = clsAdmin.CONSTSORTASC;
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }
        else
        {
            switch (gvSortDirection)
            {
                case clsAdmin.CONSTSORTASC:
                    gvSortDirection = clsAdmin.CONSTSORTDESC;
                    break;
                case clsAdmin.CONSTSORTDESC:
                    gvSortDirection = clsAdmin.CONSTSORTASC;
                    break;
            }

            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }

        gvSort = strSortExpDir;
        bindGV(gvIndLogin, gvSort);
    }

    protected void gvIndLogin_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvIndLogin.PageIndex = e.NewPageIndex;
        bindGV(gvIndLogin, gvSort);
    }
   
    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strUniLogin = "";
            string strUniPwd = "";
            string uniEncryptPwd = "";

            string strIndCompName = "";
            string strIndContPerson = "";
            string strIndEmail = "";
            string strIndContactNo = "";
            string strIndRemarks = "";
            string strIndLogin = "";
            string strIndPwd = "";
            string IndEncryptPwd = "";

            string strGroup = "";

            strUniLogin = txtLogin.Text.Trim();
            strUniPwd = txtPwd.Text.Trim();

            clsMD5 md5 = new clsMD5();
            if (!string.IsNullOrEmpty(strUniPwd))
            {
                uniEncryptPwd = md5.encrypt(strUniPwd);
            }

            strIndCompName = txtCompanyName.Text.Trim();
            strIndContPerson = txtContactPerson.Text.Trim();
            strIndEmail = txtEmail.Text.Trim();
            strIndContactNo = txtContactNo.Text.Trim();
            strIndRemarks = txtRemarks.Text.Trim();
            strIndLogin = txtIndLogin.Text.Trim();
            strIndPwd = txtIndPwd.Text.Trim();

            if (!string.IsNullOrEmpty(strIndPwd))
            {
                IndEncryptPwd = md5.encrypt(strIndPwd);
            }

            clsPage page = new clsPage();
            int intRecordAffected = 0;
            Boolean boolAdded = false;
            Boolean boolSuccess = true;
            Boolean boolEdited = false;

            if (trUniversalSettings.Visible == true)
            {
                strGroup = clsAdmin.CONSTADMUNILOGIN;

                if (!page.isUniLoginExist(strGroup))
                {
                    intRecordAffected = page.addUniPageAuthorization(strGroup, strUniLogin, uniEncryptPwd);

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; }
                }
                else
                {
                    if (!page.isExactSameUniPageAuthDataSet(strGroup, strUniLogin))
                    {
                        intRecordAffected = page.updateUniPageAuthById(strGroup, strUniLogin);

                        if (intRecordAffected == 1) { boolEdited = true; }
                        else { boolSuccess = false; }
                    }

                    if (boolSuccess && !string.IsNullOrEmpty(strUniPwd))
                    {
                        if (string.Compare(hdnPwd.Value.Trim(), uniEncryptPwd) != 0)
                        {
                            intRecordAffected = page.updateUniPasswordById(strGroup, uniEncryptPwd);

                            if (intRecordAffected == 1)
                            {
                                boolEdited = true;
                            }
                            else
                            {
                                boolSuccess = false;
                            }
                        }
                    }
                }

                if (!boolSuccess)
                {
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to update. Please try again.</div>";

                    Response.Redirect(currentPageName);
                }
                else
                {
                    if (boolEdited)
                    {
                        Session["EDITEDPGAUTH"] = 1;
                    }
                    else
                    {
                        Session["EDITEDPGAUTH"] = 1;
                        Session["NOCHANGE"] = 1;
                    }

                    Response.Redirect(currentPageName);
                }
            }
            else if (trIndividualSettings.Visible == true)
            {
                strGroup = clsAdmin.CONSTADMINDLOGIN;

                if (mode == 1)
                {
                    intRecordAffected = page.addIndPageAuthorization(strGroup, strIndCompName, strIndContPerson, strIndEmail,
                                                                  strIndContactNo, strIndRemarks, strIndLogin, IndEncryptPwd);

                    if (intRecordAffected == 1)
                    {
                        pgAuthId = page.pgAuthid;

                        sendLoginDetails(strIndCompName, strIndContPerson, strIndEmail, strIndContactNo, strIndLogin, strIndPwd);

                        boolAdded = true;
                    }
                    else { boolSuccess = false; }
                }
                else if (mode == 2)
                {
                    if (!page.isExactSameIndPageAuthDataSet(pgAuthId, strIndCompName, strIndContPerson, strIndEmail,
                                                        strIndContactNo, strIndRemarks, strIndLogin))
                    {
                        intRecordAffected = page.updateIndPageAuthById(pgAuthId, strIndCompName, strIndContPerson, strIndEmail,
                                                        strIndContactNo, strIndRemarks, strIndLogin);

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;
                        }
                        else
                        {
                            boolSuccess = false;
                        }
                    }

                    if (boolSuccess && !string.IsNullOrEmpty(strIndPwd))
                    {
                        if (string.Compare(hdnIndPwd.Value.Trim(), IndEncryptPwd) != 0)
                        {
                            intRecordAffected = page.updateIndPasswordById(pgAuthId, IndEncryptPwd);

                            if (intRecordAffected == 1)
                            {
                                boolEdited = true;
                            }
                            else
                            {
                                boolSuccess = false;
                            }
                        }
                    }
                }

                if (mode == 1)
                {
                    if (boolAdded)
                    {
                        Session["NEWPGAUTH"] = page.pgAuthid;

                        Response.Redirect(currentPageName);
                    }
                    else
                    {
                        if (!boolSuccess)
                        {
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add. Please try again.</div>";

                            Response.Redirect(currentPageName);
                        }
                    }
                }
                else if (mode == 2)
                {
                    if (boolEdited)
                    {
                        Session["EDITEDPGAUTH"] = page.pgAuthid;

                        Response.Redirect(currentPageName); 
                    }
                    else
                    {
                        if (boolSuccess)
                        {
                            Session["EDITEDPGAUTH"] = pgAuthId;
                            Session["NOCHANGE"] = 1;

                            Response.Redirect(Request.Url.ToString());
                        }
                        else
                        {
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit. Please try again.</div>";

                            Response.Redirect(currentPageName);
                        }
                    }
                }
            }
        }
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void setPageProperties()
    {
        if (Session[clsAdmin.CONSTPROJECTUNIVERSALLOGIN] != null && clsAdmin.CONSTPROJECTUNIVERSALLOGIN != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTUNIVERSALLOGIN]) == 0)
            {
                trUniversalSettings.Visible = false;
            }
            else
            {
                trUniversalSettings.Visible = true;
                fillUniForm();
            }
        }

        if (Session[clsAdmin.CONSTPROJECTINDIVIDUALLOGIN] != null && clsAdmin.CONSTPROJECTINDIVIDUALLOGIN != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTINDIVIDUALLOGIN]) == 0)
            {
                trIndividualSettings.Visible = false;
                tblIndLoginListing.Visible = false;
            }
            else
            {
                trIndividualSettings.Visible = true;
                tblIndLoginListing.Visible = true;

                if (!string.IsNullOrEmpty(Request["id"]))
                {
                    int intTryParse;
                    if (int.TryParse(Request["id"], out intTryParse))
                    {
                        pgAuthId = intTryParse;
                        mode = 2;
                    }
                }
                else
                {
                    mode = 1;
                }

                switch (mode)
                {
                    case 1:

                        break;
                    case 2:
                        fillForm();

                        break;
                    default:
                        break;
                }

                clsConfig config = new clsConfig();
                config.extractItemByNameGroup(clsConfig.CONSTNAMELISTINGSIZE, clsConfig.CONSTGROUPOTHERSETTINGS, 1);

                gvIndLogin.PagerSettings.FirstPageText = GetGlobalResourceObject("GlobalResource", "contentAdmFirstPageText.Text").ToString();
                gvIndLogin.PagerSettings.LastPageText = GetGlobalResourceObject("GlobalResource", "contentAdmLastPageText.Text").ToString();
                gvIndLogin.PageSize = (!string.IsNullOrEmpty(config.value)) ? Convert.ToInt16(config.value) : clsAdmin.CONSTADMPAGESIZE;
                //gvCustomer.PageSize = clsAdmin.CONSTADMPAGESIZE;

                gvSort = gvSortExpression + " " + gvSortDirection;
                bindGV(gvIndLogin, "PGAUTH_ID ASC");

            }
        }
    }

    protected void fillUniForm()
    {
        clsPage page = new clsPage();

        DataSet ds = page.getPageAuthList();
        DataRow[] foundRow;

        foundRow = ds.Tables[0].Select("PGAUTH_GROUP = '" + clsAdmin.CONSTADMUNILOGIN + "'");

        foreach (DataRow row in foundRow)
        {
            txtLogin.Text = row["PGAUTH_UNILOGIN"].ToString();
            hdnPwd.Value = row["PGAUTH_UNIPWD"].ToString();
        }
    }

    protected void fillForm()
    {
        clsPage page = new clsPage();

        if (page.extractPageAuthById(pgAuthId))
        {
            txtCompanyName.Text = page.IndCompName;
            txtContactPerson.Text = page.IndContactPerson;
            txtEmail.Text = page.IndEmail;
            txtContactNo.Text = page.IndContactNo;
            txtRemarks.Text = page.IndRemarks;
            txtIndLogin.Text = page.IndLogin;
            hdnIndPwd.Value = page.IndPwd;
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }

    protected void bindGV(GridView gvIndLoginRef, string gvSortExpDir)
    {
        clsPage page = new clsPage();
        DataSet ds = new DataSet();
        ds = page.getPageAuthList();
        DataView dv = new DataView(ds.Tables[0]);

        dv.RowFilter = " PGAUTH_GROUP = '" + clsAdmin.CONSTADMINDLOGIN + "'";

        if (gvSortExpDir != "")
        {
            dv.Sort = gvSortExpDir;
        }

        Session["ADMDV"] = dv;

        gvIndLoginRef.DataSource = dv;
        gvIndLoginRef.DataBind();
    }

    protected void sendLoginDetails(string strCompanyName, string strContactPerson, string strEmail, string strContactNo, string strUsername, string strPassword)
    {
        string strSubjectPrefix;
        string strSignature;
        string strSubject;
        string strUserSubject;
        string strBody;
        string strUserBody;
        string strSenderName;
        string strName;

        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPGAUTHORIZATIONADMINEMAILSUBJECT, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT, 1);
        strSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPGAUTHORIZATIONCOMPEMAILSUBJECT, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT, 1);
        strUserSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPGAUTHORIZATIONADMINEMAILCONTENT, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT, 1);
        strBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPGAUTHORIZATIONCOMPEMAILCONTENT, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;


        if (!string.IsNullOrEmpty(strContactPerson))
        {
            strName = strContactPerson;
        }
        else
        {
            strName = strCompanyName;
        }

        strBody = strBody.Replace("[NAME]", strName);
        strBody = strBody.Replace("[CONTACTPERSON]", strContactPerson);
        strBody = strBody.Replace("[COMPANY]", strCompanyName);
        strBody = strBody.Replace("[EMAIL]", strEmail);
        strBody = strBody.Replace("[CONTACT]", strContactNo);
        strBody = strBody.Replace("[LOGIN]", strUsername);
        strBody = strBody.Replace("[PASSWORD]", strPassword);
        strBody = strBody.Replace("[SIGNATURE]", strSignature);

        strUserBody = strUserBody.Replace("[NAME]", strName);
        strUserBody = strUserBody.Replace("[CONTACTPERSON]", strContactPerson);
        strUserBody = strUserBody.Replace("[COMPANY]", strCompanyName);
        strUserBody = strUserBody.Replace("[EMAIL]", strEmail);
        strUserBody = strUserBody.Replace("[CONTACT]", strContactNo);
        strUserBody = strUserBody.Replace("[LOGIN]", strUsername);
        strUserBody = strUserBody.Replace("[PASSWORD]", strPassword);
        strUserBody = strUserBody.Replace("[SIGNATURE]", strSignature);

        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strSubject = strSubjectPrefix + " " + strSubject; strUserSubject = strSubjectPrefix + " " + strUserSubject; }

        if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        {
            if (clsEmail.send(strSenderName, "", strEmail, "", "", "", strUserSubject, strUserBody, 1))
            {

            }
            else
            {
                clsLog.logErroMsg("Failed to send registered company details.");
            }
        }
        else
        {
            clsLog.logErroMsg("Failed to send registered company details.");
        }
    }
    #endregion
}
