﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucAdmUserView : System.Web.UI.UserControl
{
    #region "enum"
    public enum DisplayType
    {
        view1 = 1,
        view2 = 2,
        view3 = 3
    }
    #endregion


    #region "Properties"
    protected string _userViewUrl = "";
    protected DisplayType _displayType = DisplayType.view1;
    #endregion


    #region "Property Methods"
    public string userViewUrl
    {
        get { return ViewState["USERVIEWURL"] as string ?? _userViewUrl; }
        set { ViewState["USERVIEWURL"] = value; }
    }

    public DisplayType displayType
    {
        get
        {
            if (ViewState["DISPLAYTYPE"] == null)
            {
                return _displayType;
            }
            else
            {
                return (DisplayType)ViewState["DISPLAYTYPE"];
            }
        }
        set { ViewState["DISPLAYTYPE"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "admuserview.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            if (displayType == DisplayType.view1)
            {
                hypUserView.NavigateUrl = userViewUrl;
                hypUserView.Visible = true;
            }
            else if (displayType == DisplayType.view2)
            {
                hypUserViewManage.NavigateUrl = userViewUrl;
                hypUserViewManage.Visible = true;
            }
            else if (displayType == DisplayType.view3)
            {
                clsConfig config = new clsConfig();
                string strProfilePage = !string.IsNullOrEmpty(config.admProfilePage) ? config.admProfilePage : clsAdmin.CONSTADMCHANGEPWD;
                hypUpdateProfile.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/" + strProfilePage;
                hypUpdateProfile.Visible = true;


                Session[clsAdmin.CONSTADMINCS] = 1;

                bool boolAdmRoot = false;
                bool boolController = false;
                int admID = 0;
                if (Session["ADMID"] != null && Session["ADMID"] != "")
                {
                    admID = int.Parse(Session["ADMID"].ToString());

                }
                if (Session["CTRLID"] != null && Session["CTRLID"] != "")
                {
                    boolController = true;
                }
                else if (Session["ADMROOT"] != null && Session["ADMROOT"] != "")
                {
                    boolAdmRoot = true;
                }

                string image = "";
                if (boolAdmRoot)
                {
                    clsAdmin adm = new clsAdmin();
                    if (adm.extractAdminById(admID, 0))
                    {
                        image = adm.image;
                    }
                }
                else
                {
                    clsUser usr = new clsUser();
                    if (usr.extractUserById(admID, 0))
                    {
                        image = usr.userImage;
                    }
                }

 

                Session[clsAdmin.CONSTADMINCS] = null;
            }
        }
    }
    #endregion
}
