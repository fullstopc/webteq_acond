﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucAdmShipping : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _shipId = 0;
    protected int _mode = 1;
    protected string _pageListingURL = "";
    protected int _shippingType = 1;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int shipId
    {
        get
        {
            if (ViewState["SHIPID"] == null)
            {
                return _shipId;
            }
            else
            {
                return int.Parse(ViewState["SHIPID"].ToString());
            }
        }
        set { ViewState["SHIPID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }

    public int shippingType
    {
        get
        {
            if (ViewState["SHIPPINGTYPE"] == null)
            {
                return _shippingType;
            }
            else
            {
                return Convert.ToInt16(ViewState["SHIPPINGTYPE"]);
            }
        }
        set { ViewState["SHIPPINGTYPE"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtState.Text = "";

        if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
        {
            string countryId = ddlCountry.SelectedValue;

            clsCountry country = new clsCountry();
            country.extractCountryById(countryId, 1);

            //Customization for YSHamper Project - start
            clsConfig config = new clsConfig();
            if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
            {
                clsMis mis = new clsMis();
                DataSet dsState = new DataSet();
                dsState = mis.getStateList(1);
                DataView dvState;

                if (mis.showState != 1)
                {
                    trState.Visible = false;
                }

                if (dsState.Tables.Count > 0)
                {
                    dvState = new DataView(dsState.Tables[0]);

                    dvState.RowFilter = "STATE_COUNTRY ='" + ddlCountry.SelectedValue + "'";

                    if (dvState.Count > 0)
                    {
                        ddlState.DataSource = dvState;
                        ddlState.DataTextField = "STATE_NAME";
                        ddlState.DataValueField = "STATE_NAME";
                        ddlState.DataBind();

                        ListItem ddlStateBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                        ddlState.Items.Insert(0, ddlStateBillDefaultItem);

                        tdStateDdl.Visible = true;
                        tdStatetxtbox.Visible = false;
                    }
                    else
                    {
                        txtState.Text = "";

                        tdStateDdl.Visible = false;
                        tdStatetxtbox.Visible = true;
                    }
                }
                else
                {
                    txtState.Text = "";

                    tdStateDdl.Visible = false;
                    tdStatetxtbox.Visible = true;
                }

                if (country.showState == 1)
                {
                    trState.Visible = true;
                }
                else
                {
                    trState.Visible = false;
                }
            }
            else
            {
                if (country.showState == 1)
                {
                    trState.Visible = true;
                    tdStatetxtbox.ColSpan = 3;
                }
                else
                {
                    trState.Visible = false;
                }
            }
            //Customization for YSHamper Project - end

            
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strCountry;
            string strState;
            decimal decShipWeightFirst = Convert.ToDecimal("0.00");
            decimal decShipFeeFirst = Convert.ToDecimal("0.00");
            decimal decShipWeightSub = Convert.ToDecimal("0.00");
            decimal decShipFeeSub = Convert.ToDecimal("0.00");
            decimal decShipFreeWeight = Convert.ToDecimal("0.00");
            int intActive = 0;
            int intCountry = 0;
            int intState = 0;
            int intCity = 0;
            string strCity = "";
            string strLeadTime = "";
            int intAllowLiquid = 0;

            strCountry = ddlCountry.SelectedValue;
            strState = txtState.Text.Trim();

            if (!string.IsNullOrEmpty(txtFirstWeight.Text.Trim())) { decShipWeightFirst = Convert.ToDecimal(txtFirstWeight.Text.Trim()); }
            if (!string.IsNullOrEmpty(txtFirstFee.Text.Trim())) { decShipFeeFirst = Convert.ToDecimal(txtFirstFee.Text.Trim()); }
            if (!string.IsNullOrEmpty(txtSubSeqWeight.Text.Trim())) { decShipWeightSub = Convert.ToDecimal(txtSubSeqWeight.Text.Trim()); }
            if (!string.IsNullOrEmpty(txtSubSeqFee.Text.Trim())) { decShipFeeSub = Convert.ToDecimal(txtSubSeqFee.Text.Trim()); }
            if (!string.IsNullOrEmpty(txtFreeShippingWeight.Text.Trim())) { decShipFreeWeight = Convert.ToDecimal(txtFreeShippingWeight.Text.Trim()); }
            if (chkboxActive.Checked) { intActive = 1; }

            //Customization for YSHamper Project - start
            clsConfig config = new clsConfig();
            if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
            {
                if (chkboxCountry.Checked) { intCountry = 1; }
                if (chkboxState.Checked) { intState = 1; }
                if (chkboxCity.Checked) { intCity = 1; }

                if (tdStateDdl.Visible) { strState = ddlState.SelectedValue; }
                else if (tdStatetxtbox.Visible) { strState = txtState.Text.Trim(); }
                else { strState = ""; }

                strCity = txtCity.Text.Trim();

                strLeadTime = txtLeadTime.Text.Trim();

                if (!string.IsNullOrEmpty(txtShippingFee.Text.Trim())) { decShipFeeSub = Convert.ToDecimal(txtShippingFee.Text.Trim()); }
                if (!string.IsNullOrEmpty(txtFreeAfter.Text.Trim())) { decShipFreeWeight = Convert.ToDecimal(txtFreeAfter.Text.Trim()); }

                if (chkboxAllowLiquid.Checked) { intAllowLiquid = 1; }
            }
            //Customization for YSHamper Project - end

            clsShipping ship = new clsShipping();
            int intRecordAffected = 0;

            switch (mode)
            {
                case 1:
                    if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
                    {
                        if (!ship.isShippingExist3(strCountry, strState, strCity))
                        {
                            intRecordAffected = ship.addShipping3(strCountry, strState, strCity, intActive, decShipWeightFirst, decShipFeeFirst, decShipWeightSub, decShipFeeSub, decShipFreeWeight, intCountry, intState, intCity, strLeadTime, intAllowLiquid, int.Parse(Session["ADMID"].ToString()));

                            if (intRecordAffected == 1)
                            {
                                Session["NEWSHIPID"] = ship.shipId;
                                Response.Redirect(currentPageName + "?id=" + ship.shipId);
                            }
                            else
                            {
                                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new shipping details. Please try again.</div>";
                                Response.Redirect(currentPageName);
                            }
                        }
                        else
                        {
                            Session["ERRMSG"] = "<div class=\"errmsg\">This shipping details already exists.</div>";
                            Response.Redirect(currentPageName);
                        }
                    }
                    else
                    {

                        if (!ship.isShippingExist(strCountry, strState))
                        {
                            intRecordAffected = ship.addShipping3(strCountry, strState, strCity, intActive, decShipWeightFirst, decShipFeeFirst, decShipWeightSub, decShipFeeSub, decShipFreeWeight, intCountry, intState, intCity, strLeadTime, intAllowLiquid, int.Parse(Session["ADMID"].ToString()));

                            if (intRecordAffected == 1)
                            {
                                Session["NEWSHIPID"] = ship.shipId;
                                Response.Redirect(currentPageName + "?id=" + ship.shipId);
                            }
                            else
                            {
                                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new shipping details. Please try again.</div>";
                                Response.Redirect(currentPageName);
                            }
                        }
                        else
                        {
                            Session["ERRMSG"] = "<div class=\"errmsg\">This shipping details already exists.</div>";
                            Response.Redirect(currentPageName);
                        }
                    }
                    break;
                case 2:
                    Boolean boolSuccess = true;
                    Boolean boolEdited = false;

                    if (!ship.isExactSameSet3(shipId, strCountry, strState, strCity, decShipWeightFirst, decShipFeeFirst, decShipWeightSub, decShipFeeSub, decShipFreeWeight, intCountry, intState, intCity, strLeadTime, intAllowLiquid, intActive))
                    {
                        intRecordAffected = ship.updateShippingById3(shipId, strCountry, strState, strCity, decShipWeightFirst, decShipFeeFirst, decShipWeightSub, decShipFeeSub, decShipFreeWeight, intCountry, intState, intCity, strLeadTime, intAllowLiquid, intActive, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;
                        }
                        else
                        {
                            boolSuccess = false;
                        }
                    }

                    if (boolEdited)
                    {
                        Session["EDITEDSHIPID"] = ship.shipId;
                    }
                    else
                    {
                        if (!boolSuccess)
                        {
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit shipping details. Please try again.</div>";
                        }
                        else
                        {
                            Session["EDITEDSHIPID"] = shipId;
                            Session["NOCHANGE"] = 1;
                        }
                    }

                    Response.Redirect(Request.Url.ToString());
                    break;
            }

        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        int intDeletedShipId = 0;

        clsMis.performDeleteShipping(shipId, ref intDeletedShipId);

        Session["DELETEDSHIPID"] = intDeletedShipId;
        Response.Redirect(currentPageName);
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void setPageProperties()
    {
        //Customization for YSHamper Project - start
        clsConfig config = new clsConfig();
        if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
        {
            tdCountryChkbox.Visible = true;
            tdCountryChkbox.ColSpan = 2;
            trState.Visible = true;
            tdStateChkbox.Visible = true;
            trCity.Visible = true;
            trFirstFee.Visible = false;
            trSubSeqFee.Visible = false;
            trLeadTime.Visible = true;
            lblLeadTimeUnit.Text = "day(s)";
            trShippingFee.Visible = true;
            lblShippingFee.Text = config.currency;
            trFreeAfterCost.Visible = true;
            lblFreeAfter.Text = config.currency;
            trAllowLiquid.Visible = true;
            trSpacer.Visible = false;
            trSectionHeader.Visible = false;
            trSpacer2.Visible = false;
        }
        else
        {
            tdCountryDdl.ColSpan = 3;
        }
        //Customization for YSHamper Project - end

        clsCountry country = new clsCountry();
        DataSet ds;
        DataView dv;

        ds = new DataSet();
        ds = country.getCountryList(1);
        dv = new DataView(ds.Tables[0]);
        dv.RowFilter = "COUNTRY_DEFAULT = 0";
        dv.Sort = "COUNTRY_NAME ASC";

        ddlCountry.DataSource = dv;
        ddlCountry.DataTextField = "COUNTRY_NAME";
        ddlCountry.DataValueField = "COUNTRY_CODE";
        ddlCountry.DataBind();

        ListItem ddlCountryDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlCountry.Items.Insert(0, ddlCountryDefaultItem);

        //clsConfig config = new clsConfig();
        string strShippingUnit = "kg";
        string strShippingUnitFree = config.shippingUnit;
        string strCurrency = config.currency;

        litFirstFee.Text = "First block (" + strShippingUnit + ")";
        litSubSeqFee.Text = "Subsequent block (" + strShippingUnit + ")";
        litFreeShippingFee.Text = "Free after (" + strShippingUnitFree + ")";
        litFirstFeeUnit.Text = strCurrency;
        litSubSeqFeeUnit.Text = strCurrency;

        switch (mode)
        {
            case 1:
                lnkbtnDelete.Visible = false;
                break;
            case 2:
                lnkbtnDelete.Visible = true;
                break;
        }

        if (shippingType == clsAdmin.CONSTSHIPPINGTYPE2 || shippingType == clsAdmin.CONSTSHIPPINGTYPE3)
        {
            trFreeAfter.Visible = true;
        }

        if (mode == 2)
        {
            fillForm();
        }

        lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteShipping.Text").ToString() + "'); return false;";
        lnkbtnCancel.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
    }

    protected void fillForm()
    {
        clsShipping ship = new clsShipping();
        clsConfig config = new clsConfig();
        if (ship.extractShippingById(shipId, 0))
        {
            clsCountry country = new clsCountry();

            if (ship.shipDefault != 0)
            {
                ListItem ddlCountryAllDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "ALL");
                ddlCountry.Items.Insert(0, ddlCountryAllDefaultItem);

                ddlCountry.SelectedValue = ship.shipCountry;

                ddlCountry.Enabled = false;
                txtState.Enabled = false;
                chkboxActive.Enabled = false;
                lnkbtnDelete.Visible = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(ship.shipCountry))
                {
                    ddlCountry.SelectedValue = ship.shipCountry;
                }
                else
                {
                    ddlCountry.SelectedValue = "MY";
                }
            }

            if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
            {
                string countryId = ddlCountry.SelectedValue;

                country.extractCountryById(countryId, 1);

                if (country.showState == 1)
                {
                    trState.Visible = true;
                    txtState.Text = ship.shipState;

                    if (!config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
                    {
                        tdStatetxtbox.ColSpan = 3;
                    }
                }
                else
                {
                    trState.Visible = false;
                }
            }

            txtFirstWeight.Text = ship.shipKgFirst.ToString();
            txtFirstFee.Text = ship.shipFeeFirst.ToString();
            txtSubSeqWeight.Text = ship.shipKg.ToString();
            txtSubSeqFee.Text = ship.shipFee.ToString();

            if (trFreeAfter.Visible)
            {
                txtFreeShippingWeight.Text = ship.shipFeeFree.ToString();
            }

            if (ship.shipActive == 1) { chkboxActive.Checked = true; }
            else { chkboxActive.Checked = false; }

            //Customization for YSHamper Project - start
            //clsConfig config = new clsConfig();
            if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
            {
                if (ship.shipCountryTxt == 1) { chkboxCountry.Checked = true; }
                else { chkboxCountry.Checked = false; }

                if (ship.shipStateTxt == 1) { chkboxState.Checked = true; }
                else { chkboxState.Checked = false; }

                if (ship.shipCityTxt == 1) { chkboxCity.Checked = true; }
                else { chkboxCity.Checked = false; }

                txtCity.Text = ship.shipCity.ToString();

                txtLeadTime.Text = ship.shipLeadTime.ToString();

                txtShippingFee.Text = ship.shipFee.ToString();

                txtFreeAfter.Text = ship.shipFeeFree.ToString();

                if (ship.shipAllowLiquid == 1) { chkboxAllowLiquid.Checked = true; }
                else { chkboxAllowLiquid.Checked = false; }

                if (ship.shipDefault != 0)
                {
                    trCity.Visible = false;
                }
                clsMis mis = new clsMis();
                DataSet dsState = new DataSet();
                dsState = mis.getStateList(1);
                DataView dvState;

                if (dsState.Tables.Count > 0)
                {
                    dvState = new DataView(dsState.Tables[0]);

                    dvState.RowFilter = "STATE_COUNTRY ='" + ddlCountry.SelectedValue + "'";

                    if (dvState.Count > 0)
                    {
                        ddlState.DataSource = dvState;
                        ddlState.DataTextField = "STATE_NAME";
                        ddlState.DataValueField = "STATE_NAME";
                        ddlState.DataBind();

                        ListItem ddlStateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                        ddlState.Items.Insert(0, ddlStateDefaultItem);

                        try
                        {
                            if (!string.IsNullOrEmpty(ship.shipState))
                            {
                                ddlState.SelectedValue = ship.shipState;
                            }
                        }
                        catch (Exception ex)
                        {
                            ddlState.SelectedValue = "";
                        }

                        tdStateDdl.Visible = true;
                        tdStatetxtbox.Visible = false;
                    }
                    else
                    {
                        txtState.Text = ship.shipState;

                        tdStateDdl.Visible = false;
                        tdStatetxtbox.Visible = true;
                    }
                }
                else
                {
                    txtState.Text = ship.shipState;

                    tdStateDdl.Visible = false;
                    tdStatetxtbox.Visible = true;
                }
            }
            //Customization for YSHamper Project - end
        }
    }
    #endregion
}
