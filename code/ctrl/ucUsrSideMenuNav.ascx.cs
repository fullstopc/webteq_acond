﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Linq;
using Newtonsoft.Json;

public partial class ctrl_ucUsrSideMenu : System.Web.UI.UserControl
{
    #region "Properties"
    protected string _lang = "";
    protected int _pageId = 0;
    protected int _sectId = 1;
    protected int _parentId = 0;
    protected int parentId2 = 0;
    #endregion


    #region "Property Methods"
    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }

    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public int parentId
    {
        get { return _parentId; }
        set { _parentId = value; }
    }

    public int sectId
    {
        get
        {
            if (ViewState["SECTID"] == null)
            {
                return _sectId;
            }
            else
            {
                return Convert.ToInt16(ViewState["SECTID"]);
            }
        }
        set { ViewState["SECTID"] = value; }
    }
    #endregion


    #region "Page Events"
    public void fill()
    {
        bindRptCatData();
    }

    protected void rptSubMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ID"));
            string strDisplayName = DataBinder.Eval(e.Item.DataItem, "PAGE_DISPLAYNAME").ToString();
            string strTitle = DataBinder.Eval(e.Item.DataItem, "PAGE_TITLE").ToString();
            string strCSSClass = DataBinder.Eval(e.Item.DataItem, "PAGE_CSSCLASS").ToString();
            string strLang = DataBinder.Eval(e.Item.DataItem, "PAGE_LANG").ToString();
            string strTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATE").ToString();
            int intRedirect = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECT"));
            int intParent = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_PARENT"));
            int intParentTemplate = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATEPARENT"));
            string strParentTemplate = DataBinder.Eval(e.Item.DataItem, "V_TEMPLATEPARENT").ToString();
            string strParentCSSClass = DataBinder.Eval(e.Item.DataItem, "V_PARENTCSSCLASS").ToString();
            int intEnableLink = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ENABLELINK"));
            string strRedirectLink = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTLINK").ToString();
            string strRedirectTarget = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTTARGET").ToString();
            string strParentName = DataBinder.Eval(e.Item.DataItem, "V_PARENTNAME").ToString();


            HyperLink hypSubMenu = (HyperLink)e.Item.FindControl("hypSubMenu");
            hypSubMenu.ToolTip = strDisplayName.Replace("<br/>", " ").Replace("<br />", " ");
            hypSubMenu.CssClass = strCSSClass;
            Literal litSubMenu = (Literal)e.Item.FindControl("litSubMenu");
            litSubMenu.Text = strDisplayName.Replace("<br/>", " ").Replace("<br />", " ");

            if (intId == pageId)
            {
                hypSubMenu.CssClass += " selected";
            }

            if (intEnableLink == 0)
            {
                hypSubMenu.NavigateUrl = string.Empty;
                hypSubMenu.Attributes.Remove("href");
                hypSubMenu.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "default";
            }
            else
            {
                if (intRedirect > 0)
                {
                    if (intRedirect == clsSetting.CONSTEXTERNALLINK)
                    {
                        if (!string.IsNullOrEmpty(strRedirectLink))
                        {
                            string strLink = strRedirectLink;

                            if (strRedirectLink.StartsWith("http://"))
                            {
                                hypSubMenu.NavigateUrl = strLink;
                            }
                            else
                            {
                                hypSubMenu.NavigateUrl = "http://" + strLink;
                            }

                            //hypSubMenu.NavigateUrl = strLink;
                            hypSubMenu.Target = strRedirectTarget;
                        }
                    }
                    else
                    {
                        if (Application["PAGELIST"] == null)
                        {
                            clsPage page = new clsPage();
                            Application["PAGELIST"] = page.getPageList(1);
                        }

                        DataSet ds = new DataSet();
                        ds = (DataSet)Application["PAGELIST"];

                        DataRow[] foundRow = ds.Tables[0].Select("PAGE_ID = " + intRedirect + " AND PAGE_ACTIVE = 1 AND PAGE_OTHER = 0");

                        if (foundRow.Length > 0)
                        {
                            int intRParent = Convert.ToInt16(foundRow[0]["PAGE_PARENT"]);
                            int intRTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                            string strRTemplateParent = foundRow[0]["V_TEMPLATEPARENT"].ToString();
                            string strRTemplate = intRParent > 0 ? (intRTemplateParent > 0 ? strRTemplateParent : strTemplate) : strTemplate;

                            if (!string.IsNullOrEmpty(strRTemplate))
                            {
                                if (!string.IsNullOrEmpty(lang)) { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strRTemplate + "?pgid=" + intRedirect + "&lang=" + strLang; }
                                else { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strRTemplate + "?pgid=" + intRedirect; }
                            }
                        }
                    }
                }
                else
                {

                    if (!string.IsNullOrEmpty(strTemplate))
                    {
                        if (!string.IsNullOrEmpty(lang)) { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + intId + "&lang=" + strLang; }
                        else { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + intId; }
                    }
                }
            }

            DataSet dsMenu = new DataSet();
            //dsMenu = (DataSet)Application["PAGELISTLV2"];
            //DataView dv = new DataView(dsMenu.Tables[0]);

            clsPage pageSub = new clsPage();
            Application["PAGELISTLV2"] = pageSub.getPageList(1);


            dsMenu = (DataSet)Application["PAGELISTLV2"];
            DataView dv = new DataView(dsMenu.Tables[0]);
            dv.RowFilter = "PAGE_PARENT = " + intId + " AND PAGE_SHOWINMENU = 1 AND PAGE_ACTIVE = 1 AND PAGE_OTHER = 0";
            dv.Sort = "PAGE_ORDER ASC";

            Repeater rptSubMenuLv2 = (Repeater)e.Item.FindControl("rptSubMenuLv2");

            if (dv.Count > 0)
            {
                hypSubMenu.CssClass += " parent";
                rptSubMenuLv2.DataSource = dv;
                rptSubMenuLv2.DataBind();
            }

        }
    }

    protected void rptSubMenuLv2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ID"));
            string strDisplayName = DataBinder.Eval(e.Item.DataItem, "PAGE_DISPLAYNAME").ToString();
            string strTitle = DataBinder.Eval(e.Item.DataItem, "PAGE_TITLE").ToString();
            string strCSSClass = DataBinder.Eval(e.Item.DataItem, "PAGE_CSSCLASS").ToString();
            string strLang = DataBinder.Eval(e.Item.DataItem, "PAGE_LANG").ToString();
            string strTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATE").ToString();
            int intRedirect = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECT"));
            int intParent = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_PARENT"));
            int intParentTemplate = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATEPARENT"));
            string strParentTemplate = DataBinder.Eval(e.Item.DataItem, "V_TEMPLATEPARENT").ToString();
            string strParentCSSClass = DataBinder.Eval(e.Item.DataItem, "V_PARENTCSSCLASS").ToString();
            int intEnableLink = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ENABLELINK"));
            string strRedirectLink = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTLINK").ToString();
            string strRedirectTarget = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTTARGET").ToString();
            string strParentName = DataBinder.Eval(e.Item.DataItem, "V_PARENTNAME").ToString();

            HyperLink hypSubMenuLv2 = (HyperLink)e.Item.FindControl("hypSubMenuLv2");
            hypSubMenuLv2.Text = strDisplayName;
            hypSubMenuLv2.ToolTip = strDisplayName;
            hypSubMenuLv2.CssClass = strCSSClass;

            if (intId == pageId || intParent == pageId)
            {
                hypSubMenuLv2.CssClass += " selected";

                HyperLink hypSubMenu = (HyperLink)e.Item.NamingContainer.NamingContainer.FindControl("hypSubMenu");
                hypSubMenu.CssClass += " selected expanded";
            }

            if (intEnableLink == 0)
            {
                hypSubMenuLv2.NavigateUrl = string.Empty;
                hypSubMenuLv2.Attributes.Remove("href");
                hypSubMenuLv2.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "default";
            }
            else
            {
                if (intRedirect > 0)
                {
                    if (intRedirect == clsSetting.CONSTEXTERNALLINK)
                    {
                        if (!string.IsNullOrEmpty(strRedirectLink))
                        {
                            string strLink = strRedirectLink;

                            if (strRedirectLink.StartsWith("http://"))
                            {
                                hypSubMenuLv2.NavigateUrl = strLink;
                            }
                            else
                            {
                                hypSubMenuLv2.NavigateUrl = "http://" + strLink;
                            }

                            //hypSubMenu.NavigateUrl = strLink;
                            hypSubMenuLv2.Target = strRedirectTarget;
                        }
                    }
                }
                else
                {
                    string strTemplateUse = intParentTemplate > 0 ? strParentTemplate : strTemplate;

                    if (!string.IsNullOrEmpty(strTemplateUse))
                    {
                        if (!string.IsNullOrEmpty(lang)) { hypSubMenuLv2.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateUse + "?pgid=" + intId + "&lang=" + strLang; }
                        else { hypSubMenuLv2.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateUse + "?pgid=" + intId; }
                    }
                }
            }
        }
    }
    protected void rptSubMenuLv3_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }
    #endregion


    #region "Methods"


    protected void bindRptCatData()
    {
        if (!string.IsNullOrEmpty(Request["pgid"]))
        {
            int intTryParse;
            if (int.TryParse(Request["pgid"], out intTryParse))
            {
                int intId = intTryParse;
            }
        }

        

        if (Application["PAGELIST"] == null)
        {
            Application["PAGELIST"] = new clsPage().getPageList(1);
        }

        DataSet dsMenu = (DataSet)Application["PAGELIST"];
        DataView dv = new DataView(dsMenu.Tables[0]);

        clsPage page = new clsPage();
        int intParentId = page.getParentId(parentId);

        parentId2 = parentId;
        if (intParentId > 0)
        {
            parentId = intParentId;
        }

        if (Application["PAGELIST"] == null)
        {
            Application["PAGELIST"] = page.getPageList(1);
        }

        dsMenu = (DataSet)Application["PAGELIST"];
        dv = new DataView(dsMenu.Tables[0]);
        dv.RowFilter = "PAGE_PARENT <= 0 AND PAGE_SHOWINMENU = 1 AND PAGE_ACTIVE = 1 AND PAGE_OTHER = 0";
        dv.Sort = "PAGE_ORDER ASC";

        if (dv.Count > 0)
        {
            //List<Dictionary<string, object>> list = branch(dsMenu.Tables[0], 0);
            rptSubMenu.DataSource = dv;
            rptSubMenu.DataBind();
        }
    }
    #endregion

    private List<Dictionary<string, object>> branch(DataTable pageList, int parentID)
    {
        List<Dictionary<string, object>> children = new List<Dictionary<string, object>>();

        var rows = pageList.AsEnumerable().Where(row => Convert.ToInt32(row["PAGE_PARENT"]) == parentID);
        if(rows.Any())
        {
            DataTable pageListNew = rows.CopyToDataTable();
            foreach (DataRow row in pageListNew.Rows)
            {
                int pageID = Convert.ToInt32(row["PAGE_ID"]);
                string pageName = Convert.ToString(row["PAGE_NAME"]);

                Dictionary<string, object> page = new Dictionary<string, object>();
                page["ID"] = pageID;
                page["name"] = pageName;
                page["children"] = branch(pageList, pageID);

                children.Add(page);
            }
        }
        return children;
    }
}
