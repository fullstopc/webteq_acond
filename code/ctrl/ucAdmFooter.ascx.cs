﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctlr_ucAdmFooter : System.Web.UI.UserControl
{
    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "admfooter.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            string strCompanyName = "";
            if(Session[clsAdmin.CONSTPROJECTNAME] != null && !string.IsNullOrEmpty(Session[clsAdmin.CONSTPROJECTNAME].ToString()))
            {
                strCompanyName = Session[clsAdmin.CONSTPROJECTNAME].ToString();
            }
            else
            {
                pnlAdmFooter.Visible = false;
            }
            litFooter.Text = GetLocalResourceObject("litFooter.Text").ToString().Replace(clsAdmin.CONSTCOMPANYNAMETAG, strCompanyName);
        }
    }
    #endregion
}
