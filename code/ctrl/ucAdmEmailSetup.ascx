﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmEmailSetup.ascx.cs" Inherits="ctrl_ucAdmEmailSetup" %>

<script type="text/javascript">
    function enableDisableGmailSMTP() {
        var isChecked = $('#<%= chkboxGmailSmtp.ClientID %>').attr('checked');

        if (isChecked) {
            $('#<%= txtPort.ClientID %>').removeAttr("disabled");
            $('#<%= txtPort.ClientID %>').val('<%= clsConfig.CONSTDEFAULTVALUEPORT %>');

            $('#<%= chkboxEnableSSL.ClientID %>').removeAttr("disabled");
            $('#<%= chkboxEnableSSL.ClientID %>').attr("checked", "checked");
        }
        else {
            $('#<%= txtPort.ClientID %>').attr("disabled", "disabled");
            $('#<%= txtPort.ClientID %>').val("");

            $('#<%= chkboxEnableSSL.ClientID %>').attr("disabled", "disabled");
            $('#<%= chkboxEnableSSL.ClientID %>').removeAttr("checked");
        }
    }
</script>

<asp:Panel ID="pnlAdmCMSControlPanel" runat="server" CssClass="divAdmCMSControlPanel">
    <asp:Panel ID="pnlFormDetails" runat="server" CssClass="divEmailSetupContainer">
        <div>
            <table class="formTbl" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Email Settings</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblSmtpServer" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtSmtpServer" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblDefaultSenderName" runat="server"></asp:Label></td>
                        <td><asp:TextBox ID="txtDefaultSenderName" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblDefaultSender" runat="server"></asp:Label>:</td>
                        <td>
                            <asp:TextBox ID="txtDefaultSender" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="revDefaultSender" runat="server" ErrorMessage="<br/>Please enter valid email." Display="Dynamic" ControlToValidate="txtDefaultSender" CssClass="errmsg" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel nobr"><asp:Label ID="lblDefaultSenderPwd" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtDefaultSenderPwd" runat="server" CssClass="text_fullwidth" MaxLength="250" TextMode="Password"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblDefaultRecipient" runat="server"></asp:Label>:</td>
                        <td>
                            <asp:TextBox ID="txtDefaultRecipient" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="revDefaultRecipient" runat="server" ErrorMessage="<br/>Please enter valid email." Display="Dynamic" ControlToValidate="txtDefaultRecipient" CssClass="errmsg" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblUseGmailSmtp" runat="server"></asp:Label>:</td>
                        <td class="tdLabel"><asp:CheckBox ID="chkboxGmailSmtp" runat="server" Checked="true" onclick="enableDisableGmailSMTP();" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <table cellpadding="0" cellspacing="0" class="">
                                <tr>
                                    <td class="tdLabel"><asp:Label ID="lblPort" runat="server"></asp:Label>:</td>
                                    <td class="tdMax"><asp:TextBox ID="txtPort" runat="server" CssClass="text_medium"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td class="tdLabel nobr"><asp:Label ID="lblEnableSSL" runat="server"></asp:Label>:</td>
                                    <td class="tdLabel"><asp:CheckBox ID="chkboxEnableSSL" runat="server" Checked="true" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblEmailPrefix" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtEmailPrefix" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblEmailSignature" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtEmailSignature" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
            <table class="formTbl" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Enquiry Email Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblAdminEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtAdminEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblAdminEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtAdminEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblUserEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtUserEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabelBig"><asp:Label ID="lblUserEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtUserEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>  
                </tbody>
            </table>
        </div>
        <div id="divProductContainer" runat="server" visible="false">
            <table class="formTbl" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Ask Email Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel nobr"><asp:Label ID="lblAskEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtAskEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel nobr"><asp:Label ID="lblAskAdminEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtAskAdminEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabelBig nobr"><asp:Label ID="lblAskUserEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtAskUserEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" class="formTbl">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Share to Friend Email Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel nobr"><asp:Label ID="lblSTFEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtSTFEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabelBig"><asp:Label ID="lblSTFUserEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtSTFUserEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="divOrderContainer" runat="server" visible="false">
            <table class="formTbl" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">New Order Email Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel nobr"><asp:Label ID="lblNewOrdEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtNewOrdEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblNewOrdAdminEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtNewOrdAdminEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabelBig"><asp:Label ID="lblNewOrdUserEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtNewOrdUserEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
            <table class="formTbl" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Payment Receive (Bank Transfer) Email Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblPayRecBTEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPayRecBTEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblPayRecBTAdminEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPayRecBTAdminEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabelBig"><asp:Label ID="lblPayRecBTUserEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPayRecBTUserEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
            <table class="formTbl" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Payment Receive (Online Payment) Email Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblPayRecOPEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPayRecOPEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblPayRecOPAdminEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPayRecOPAdminEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabelBig"><asp:Label ID="lblPayRecOPUserEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPayRecOPUserEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" class="formTbl">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Payment Error Email Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblPayErrorEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPayErrorEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblPayErrorAdminEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPayErrorAdminEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabelBig"><asp:Label ID="lblPayErrorUserEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPayErrorUserEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" class="formTbl" style="display:none;">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Payment Reject Email Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblPayRejectEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPayRejectEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblPayRejectAdminEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPayRejectAdminEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabelBig"><asp:Label ID="lblPayRejectUserEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPayRejectUserEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" class="formTbl">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Refund Email Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblPayRefundEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPayRefundEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblPayRefundAdminEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPayRefundAdminEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabelBig"><asp:Label ID="lblPayRefundUserEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPayRefundUserEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" class="formTbl">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Order Processing Email Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblOrdProEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtOrdProEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblOrdProAdminEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtOrdProAdminEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabelBig"><asp:Label ID="lblOrdProUserEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtOrdProUserEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" class="formTbl">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Order Delivery Email Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblOrdDelEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtOrdDelEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblOrdDelAdminEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtOrdDelAdminEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabelBig"><asp:Label ID="lblOrdDelUserEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtOrdDelUserEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" class="formTbl">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Order Done Email Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblOrdDoneEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtOrdDoneEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblOrdDoneAdminEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtOrdDoneAdminEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabelBig"><asp:Label ID="lblOrdDoneUserEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtOrdDoneUserEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" class="formTbl">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Order Cancel Email Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblOrdCancelEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtOrdCancelEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblOrdCancelAdminEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtOrdCancelAdminEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabelBig"><asp:Label ID="lblOrdCancelUserEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtOrdCancelUserEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="divMemberContainer" runat="server" visible="false">
            <table cellpadding="0" cellspacing="0" border="0" class="formTbl borderTop">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Forgot Password Email Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblForgotPwdEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtForgotPwdEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <%--<tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblForgotPwdAdminEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtForgotPwdAdminEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>--%>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabelBig"><asp:Label ID="lblForgotPwdUserEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtForgotPwdUserEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="divPageAuthorization" runat="server" visible="false">
            <table cellpadding="0" cellspacing="0" border="0" class="formTbl borderTop">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Page Authorization Email Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblPgAuthAdminEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPgAuthAdminEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblPgAuthAdminEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtPgAuthAdminEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblCompanyEmailSubject" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtCompanyEmailSubject" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabelBig"><asp:Label ID="lblCompanyEmailContent" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtCompanyEmailContent" runat="server" TextMode="MultiLine" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>  
                </tbody>
            </table>
        
        </div>
        <table class="formTbl" cellpadding="0" cellspacing="0">
        <tr>
        <td colspan="2">
        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="vgControlPanel"></asp:LinkButton>
        </asp:Panel>
        </td>
        </tr>
        
        </table>
        <div>
            <table class="infoBox" cellpadding="0" cellspacing="0" style="width:97%;">
                <colgroup>
                    <col width="33%" />
                    <col width="33%" />
                    <col width="33%" />
                </colgroup>
                <thead>
                    <tr>
                        <th colspan="3">
                            Annotation
                        </th>
                    </tr>
                    <tr>
                        <th colspan="3">
                            Some useful syntax to use in email content. 
                        </th>
                    </tr>
                </thead>
                <tr>
                    <td>
                        <table cellpadding="10" class="tblEmailSyntax">
                            <colgroup>
                                <col width="30%" />
                                <col width="70%" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td class="th">[SALUTATION]</td>
                                    <td>salutation</td>
                                </tr>
                                <tr>
                                    <td class="th">[NAME]</td>
                                    <td>name</td>
                                </tr>
                                <tr>
                                    <td class="th">[COMPANY]</td>
                                    <td>company</td>
                                </tr>
                                <tr>
                                    <td class="th">[CONTACT]</td>
                                    <td>contact</td>
                                </tr>
                                <tr>
                                    <td class="th">[EMAIL]</td>
                                    <td>email</td>
                                </tr>
                                <tr>
                                    <td class="th">[SUBJECT]</td>
                                    <td>subject</td>
                                </tr>
                                <tr>
                                    <td class="th">[MESSAGE]</td>
                                    <td>message</td>
                                </tr>
                                <tr>
                                    <td class="th">[SIGNATURE]</td>
                                    <td>signature</td>
                                </tr>
                                <tr id="trPageAuthContPerson" runat="server" visible="false">
                                    <td class="th">[CONTACTPERSON]</td>
                                    <td>contact person name</td>
                                </tr>
                                <tr id="trPageAuthLogin" runat="server" visible="false">
                                    <td class="th">[LOGIN]</td>
                                    <td>login id</td>
                                </tr>
                                <tr id="trPageAuthPwd" runat="server" visible="false">
                                    <td class="th">[PASSWORD]</td>
                                    <td>password</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td id="trProduct" runat="server" visible="false">
                        <table cellpadding="10" class="tblEmailSyntax">
                            <colgroup>
                                <col width="30%" />
                                <col width="70%" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td class="th">[SHARENAME]</td>
                                    <td>share to name</td>
                                </tr>
                                <tr>
                                    <td class="th">[SHAREEMAIL]</td>
                                    <td>share to email</td>
                                </tr>
                                <tr>
                                    <td class="th">[COMMENT]</td>
                                    <td>comment</td>
                                </tr>
                                <tr>
                                    <td class="th">[PRODUCT]</td>
                                    <td>product</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td id="trOrder" runat="server" visible="false">
                        <table cellpadding="10" class="tblEmailSyntax">
                            <colgroup>
                                <col width="30%" />
                                <col width="70%" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td class="th">[ORDNO]</td>
                                    <td>order no.</td>
                                </tr>
                                <tr>
                                    <td class="th">[ORDITEM]</td>
                                    <td>order item</td>
                                </tr>
                                <tr>
                                    <td class="th">[CUR]</td>
                                    <td>currency</td>
                                </tr>
                                <tr>
                                    <td class="th">[ORDTOTAL]</td>
                                    <td>order total</td>
                                </tr>
                                <tr>
                                    <td class="th">[BILLINGDETAILS]</td>
                                    <td>billing details</td>
                                </tr>
                                <tr>
                                    <td class="th">[DELIVERYDETAILS]</td>
                                    <td>delivery details</td>
                                </tr>
                                <tr>
                                    <td class="th">[ORDERDETAILS]</td>
                                    <td>order details</td>
                                </tr>
                                <tr>
                                    <td class="th">[PAYDETAILS]</td>
                                    <td>
                                        payment details<br />
                                        Paypal/MolPay/iPay88 information
                                    </td>
                                </tr>
                                <tr>
                                    <td class="th">[ORDSTATUSURL]</td>
                                    <td>order status url</td>
                                </tr>
                                <tr>
                                    <td class="th">[REJECTMSG]</td>
                                    <td>payment reject message</td>
                                </tr>
                                <tr>
                                    <td class="th">[REFUNDTOTAL]</td>
                                    <td>refund total</td>
                                </tr>
                                <tr>
                                    <td class="th">[REFUNDDETAILS]</td>
                                    <td>refund details</td>
                                </tr>
                                <tr>
                                    <td class="th">[SHIPPINGDATE]</td>
                                    <td>shipping date</td>
                                    
                                </tr>
                                <tr>
                                    <td class="th">[COURIER]</td>
                                    <td>shipping company</td>
                                </tr>
                                <tr>
                                    <td class="th">[CONSIGNMENTNO]</td>
                                    <td>consignment no.</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </div>              
    </asp:Panel>
    
</asp:Panel>
