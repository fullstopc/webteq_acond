﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmProductInventory.ascx.cs" Inherits="ctrl_ucAdmProductInventory" %>

<asp:Panel ID="pnlFormSect1" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table border="0" id="tblAddMember1" cellpadding="0" cellspacing="0" class="formTbl tblData">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Product Inventory</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel">Supplier Name:</td>
                    <td class="tdMax"><asp:TextBox ID="txtSupplierName" runat="server" class="text_big" MaxLength="50"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel">Inventory Level:</td>
                    <td><asp:TextBox ID="txtProdInventory" runat="server" class="text_medium" MaxLength="250"></asp:TextBox>
                    <asp:CustomValidator ID="cvProdInv" runat="server" Display="Dynamic" CssClass="errmsgAgentSearch"  ErrorMessage="Invalid" OnServerValidate="validateProdInv_server" ValidationGroup="grpProdInv"></asp:CustomValidator></td>
                </tr>
                <tr>
                    <td class="tdLabel">Allow Negative:</td>
                    <td class="tdCheckbox"><asp:CheckBox ID="chkboxAllowNegative" runat="server" Checked="true" /></td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="grpProdInv"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>