﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctlr_ucAdmPersonalization : System.Web.UI.UserControl
{
    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        clsConfig config = new clsConfig();
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "admpersonalization.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        //setup sessison time out
        string strSessionTimeout = config.admSessionTimeout;
        int intSessionTimeout = 20;
        if (!string.IsNullOrEmpty(strSessionTimeout) && int.TryParse(strSessionTimeout, out intSessionTimeout))
        {
            Session.Timeout = intSessionTimeout;
        }

        startTimer();

        if (!IsPostBack)
        {
            string strProfilePage = !string.IsNullOrEmpty(config.admProfilePage) ? config.admProfilePage : clsAdmin.CONSTADMCHANGEPWD;

            litLoginAs.Text = "";
            if (Session["ADMEMAIL"] != "" && Session["ADMEMAIL"] != null)
            {
                string strAccount = Session["ADMEMAIL"].ToString().Split('@')[0];
                hypLoginAs.Text = "<b>" + strAccount + "</b>";
            }
            else if (Session["CTRLEMAIL"] != "" && Session["CTRLEMAIL"] != null)
            {
                string strAccount = Session["CTRLEMAIL"].ToString().Split('@')[0];
                hypLoginAs.Text = "<b>" + strAccount + "</b>";
            }

            hypLoginAs.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/" + strProfilePage;

            hypLogout.Text = "Logout";
            hypLogout.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/" + clsAdmin.CONSTADMLOGIN;

            // if single login change function back to master admin.
            if (clsMis.isSingleLogin())
            {
                hypLogout.Text = "Back to Master";
                hypLogout.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admUser01.aspx";
            }
        }        
    }
    #endregion


    #region "Methods"
    protected void startTimer()
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("SESSIONTIMER"))
        {
            int intTime = Session.Timeout;

            string strJS = "";
            strJS = "<script type=\"text/javascript\">";
            strJS += "  var min = " + intTime + ";";
            strJS += "  var sec = 0;";
            strJS += "  var lblTime = document.getElementById('" + lblTime.ClientID + "');";
            strJS += "  lblTime.innerHTML = min + \"min \" + sec + \"sec\";";

            strJS += "  function timer(){";
            strJS += "      if(sec <= 0){";
            strJS += "          sec = 60;";
            strJS += "          min -= 1;";
            strJS += "      }";

            strJS += "      if(min <= -1){";
            strJS += "          sec = 0;";
            strJS += "          min = 0;";
            strJS += "      }";
            strJS += "      else{";
            strJS += "          sec -= 1;";
            strJS += "          lblTime.innerHTML = min + \"min \" + sec + \"sec\";";
            strJS += "          setTimeout(\"timer()\", 1000);";
            strJS += "      }";
            strJS += "  }";
            strJS += "  timer();";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "SESSIONTIMER", strJS, false);
        }
    }
    #endregion
}
