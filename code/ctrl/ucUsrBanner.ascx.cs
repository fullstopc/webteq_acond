﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucUsrBanner : System.Web.UI.UserControl
{
    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "banner.css' type='text/css' rel='Stylesheet' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            imgbtnProductHome.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
            imgbtnAccessoryHome.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
            imgbtnPaypalHome.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
            imgbtnFaqHome.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
            imgbtnContactUsHome.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
            imgbtnProductSub.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
            imgbtnAccessorySub.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
            imgbtnPaypalSub.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
            imgbtnFaqSub.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
            imgbtnContactUsSub.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";

        }
    }

    protected void imgbtnProductHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("product.aspx?new=1");
    }

    protected void imgbtnContactUs_Click(object sender, EventArgs e)
    {
        Response.Redirect("contactus.aspx");
    }

    protected void imgbtnFaq_Click(object sender, EventArgs e)
    {
        Response.Redirect("faq.aspx");
    }

    protected void imgbtnProduct_Click(object sender, EventArgs e)
    {
        Response.Redirect("product.aspx");
    }
    #endregion


    #region "Methods"
    public void fill(int intSectId)
    {
        switch (intSectId)
        {
            case 1:
                pnlBannerHome.Visible = true;
                pnlBannerSub.Visible = false;
                break;
            default:
                pnlBannerSub.Visible = true;
                pnlBannerHome.Visible = false;
                break;
        }
    }
    #endregion
}
