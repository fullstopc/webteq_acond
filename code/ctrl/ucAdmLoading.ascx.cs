﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctlr_ucAdmLoading : System.Web.UI.UserControl
{
    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        imgLoading.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/loading.gif";
    }
    #endregion
}
