﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucUsrFacebook : System.Web.UI.UserControl
{
    #region "Properties"
    protected Boolean _boolRegisterScript = true;
    protected Boolean _boolPageOrProducts = true;
    protected string _urlToLike = "";
    protected string _imageFile = "";
    protected string _entityTitle = "";
    protected string _entityType = "website";
    protected string _siteName = "";
    protected string _entityDesc = "";
    #endregion


    #region "Property Methods"
    public Boolean boolRegisterScript
    {
        get { return _boolRegisterScript; }
        set { _boolRegisterScript = value; }
    }

    public Boolean boolPageOrProducts
    {
        get { return _boolPageOrProducts; }
        set { _boolPageOrProducts = value; }
    }

    public string urlToLike
    {
        get { return ViewState["URLTOLIKE"] as string ?? _urlToLike; }
        set { ViewState["URLTOLIKE"] = value; }
    }

    public string imageFile
    {
        get { return ViewState["IMAGEFILE"] as string ?? _imageFile; }
        set { ViewState["IMAGEFILE"] = value; }
    }

    public string entityTitle
    {
        get { return ViewState["ENTITYTITLE"] as string ?? _entityTitle; }
        set { ViewState["ENTITYTITLE"] = value; }
    }

    public string entityType
    {
        get { return ViewState["ENTITYTYPE"] as string ?? _entityType; }
        set { ViewState["ENTITYTYPE"] = value; }
    }

    public string siteName
    {
        get { return ViewState["SITENAME"] as string ?? _siteName; }
        set { ViewState["SITENAME"] = value; }
    }

    public string entityDesc
    {
        get { return ViewState["ENTITYDESC"] as string ?? _entityDesc; }
        set { ViewState["ENTITYDESC"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "facebook.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            pnlFacebookScript.Visible = boolRegisterScript;

            litLike.Text = "<div class=\"fb-like\" data-href=\"" + urlToLike + "\" data-send=\"false\" data-width=\"80\" data-show-faces=\"true\" data-layout=\"button_count\"></div>";

            pnlFacebookLike.Visible = true;
        }

        if (boolPageOrProducts)
        {   
            ucUsrFacebookThumbnail.entityTitle = entityTitle;
            ucUsrFacebookThumbnail.entityType = entityType;
            ucUsrFacebookThumbnail.urlToLike = urlToLike;
            ucUsrFacebookThumbnail.imageFile = imageFile;
            ucUsrFacebookThumbnail.siteName = siteName;
            ucUsrFacebookThumbnail.entityDesc = entityDesc;
            ucUsrFacebookThumbnail.fill();
        }
        else
        {
            ucUsrFacebookThumbnail.Visible = false;
        }
    }
    #endregion
}
