﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ctrl_ucAdmPropertyType : System.Web.UI.UserControl
{
    #region "Properties"
    protected string _keywd = "";
    #endregion

    #region "Property Methods"
    public string keywd
    {
        get { return _keywd; }
        set { _keywd = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("autocomplete" + txtType.ClientID))
        {
            string strJS = "<script type=\"text/javascript\">";
            strJS += "setAutocomplete('" + txtType.ClientID + "');";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "autocomplete" + txtType.ClientID, strJS, false);
        }

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(keywd))
            {
                txtType.Text = keywd;
            }
        }
    }
    #endregion
}