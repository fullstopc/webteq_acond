﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ctrl_ucAdmProgramGroupDesc : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected int _progGrpId = 0;
    protected string _pageListingURL = "";

    protected string progGrpDesc;
    protected string progGrpDescZh;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName2(true); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int progGrpId
    {
        get
        {
            if (ViewState["PROGGROUPID"] == null)
            {
                return _progGrpId;
            }
            else
            {
                return Convert.ToInt16(ViewState["PROGGROUPID"]);
            }
        }
        set { ViewState["PROGGROUPID"] = value; }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (progGrpId == 0)
            {
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit program category. Please select a valid program category.</div>";
                Response.Redirect(currentPageName);
            }

            if (mode == 2)
            {
                Boolean bEdited = false;
                Boolean bSuccess = true;
                clsProgram prog = new clsProgram();
                int intRecordAffected = 0;

                progGrpDesc = txtProgCatDesc.Text.Trim();
                progGrpDescZh = txtProgCatDescZh.Text.Trim();

                if (bEdited || !prog.isExactSameProgGrpDescDataSet(progGrpId, progGrpDesc))
                {
                    intRecordAffected = prog.updateProgGrpDescById(progGrpId, progGrpDesc, Convert.ToInt16(Session["ADMID"]));

                    if (intRecordAffected == 1)
                    {
                        bEdited = true;
                    }
                    else { bSuccess = false; }
                }

                if (bEdited)
                {
                    Session["EDITEDPROGGRPID"] = prog.progGrpId;
                }
                else
                {
                    if (!bSuccess)
                    {
                        prog.extractProgGrpById(progGrpId, 0);
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit program category (" + prog.progGrpName + "). Please try again.</div>";
                    }
                    else
                    {
                        Session["EDITEDPROGGRPID"] = progGrpId;
                        Session["NOCHANGE"] = 1;
                    }
                }

                Response.Redirect(Request.Url.ToString());
            }
        }
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }

        registerCKEditorScript();
    }

    protected void setPageProperties()
    {
        if (!IsPostBack)
        {
            if (mode == 2)
            {
                fillForm();
            }

            litProgGroupContent.Text = "Program Group Content";
            lblProgGroupDesc.Text = "Program Group Description";
            lblProgGroupDescZh.Text = "Program Group Description (Chinese)";

            lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
        }
    }

    public void registerCKEditorScript()
    {
        Session["CMSPATH"] = "";
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">" +
                    "CKEDITOR.replace('" + txtProgCatDesc.ClientID + "'," +
                    "{" +
                    "height:'300'," +
                    "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSGRPVIEW + "&command=QuickUpload&type=Files&path=2'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSGRPVIEW + "&command=QuickUpload&type=Images&path=2'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSGRPVIEW + "&command=QuickUpload&type=Flash&path=2'" +
                     "}" +
                     ");" +
                     "CKEDITOR.replace('" + txtProgCatDescZh.ClientID + "'," +
                     "{" +
                     "height:'300'," +
                     "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSGRPVIEW + "&command=QuickUpload&type=Files&path=2'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSGRPVIEW + "&command=QuickUpload&type=Images&path=2'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSGRPVIEW + "&command=QuickUpload&type=Flash&path=2'" +
                     "}" +
                     ");";

            strJS += "CKEDITOR.add;" +
                     "CKEDITOR.config.contentsCss = '" + System.Configuration.ConfigurationManager.AppSettings["cssBase"] + clsAdmin.CONSTADMCMSCSS + "';";
            strJS += "CKEDITOR.config.stylesCombo_stylesSet= '" + clsAdmin.CONSTADMSTYLESET + ":" + System.Configuration.ConfigurationManager.AppSettings["jsBase"] + clsAdmin.CONSTADMSTYLESETFILE + "';" +
                     "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR", strJS, false);
        }
    }

    protected void fillForm()
    {
        clsProgram prog = new clsProgram();

        if (prog.extractProgGrpById(progGrpId, 0))
        {
            txtProgCatDesc.Text = prog.progGrpDesc;
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }
    #endregion
}
