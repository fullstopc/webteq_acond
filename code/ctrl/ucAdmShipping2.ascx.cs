﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucAdmShipping2 : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _shipId = 0;
    //protected int _countryId;
    protected int _mode = 1;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int shipId
    {
        get
        {
            if (ViewState["SHIPID"] == null)
            {
                return _shipId;
            }
            else
            {
                return Convert.ToInt16(ViewState["SHIPID"]);
            }
        }
        set { ViewState["SHIPID"] = value; }
    }

    //public int countryId
    //{
    //    get
    //    {
    //        if (ViewState["COUNTRYID"] == null)
    //        {
    //            return _countryId;
    //        }
    //        else
    //        {
    //            return Convert.ToInt16(ViewState["COUNTRYID"]);
    //        }
    //    }
    //    set { ViewState["COUNTRYID"] = value; }
    //}

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtState.Text = "";

        if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
        {
            string countryId = ddlCountry.SelectedValue;

            clsMis mis = new clsMis();
            mis.extractCountryById(countryId, 1);

            if (mis.showState == 1)
            {
                trState.Visible = true;
            }
            else
            {
                trState.Visible = false;
            }
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strCountry;
            string strState;
            decimal decShipWeightFirst = Convert.ToDecimal("1.00");
            decimal decShipFeeFirst = Convert.ToDecimal("0.00");
            decimal decShipWeightSub = Convert.ToDecimal("1.00");
            decimal decShipFeeSub = Convert.ToDecimal("0.00");
            decimal decShipFreeWeight = Convert.ToDecimal("1.00");
            int intActive = 0;

            strCountry = ddlCountry.SelectedValue;
            strState = txtState.Text.Trim();

            if (!string.IsNullOrEmpty(txtFirstWeight.Text.Trim())) { decShipWeightFirst = Convert.ToDecimal(txtFirstWeight.Text.Trim()); }
            if (!string.IsNullOrEmpty(txtFirstFee.Text.Trim())) { decShipFeeFirst = Convert.ToDecimal(txtFirstFee.Text.Trim()); }
            if (!string.IsNullOrEmpty(txtSubSeqWeight.Text.Trim())) { decShipWeightSub = Convert.ToDecimal(txtSubSeqWeight.Text.Trim()); }
            if (!string.IsNullOrEmpty(txtSubSeqFee.Text.Trim())) { decShipFeeSub = Convert.ToDecimal(txtSubSeqFee.Text.Trim()); }
            if (!string.IsNullOrEmpty(txtFreeShippingWeight.Text.Trim())) { decShipFreeWeight = Convert.ToDecimal(txtFreeShippingWeight.Text.Trim()); }
            if (chkboxActive.Checked) { intActive = 1; }

            clsShipping ship = new clsShipping();
            int intRecordAffected = 0;

            switch (mode)
            {
                case 1:
                    if (!ship.isShippingExist2(strCountry, strState))
                    {
                        intRecordAffected = ship.addShipping2(strCountry, strState, intActive, decShipWeightFirst, decShipFeeFirst, decShipWeightSub, decShipFeeSub, decShipFreeWeight, Convert.ToInt16(Session["ADMID"]));

                        if (intRecordAffected == 1)
                        {
                            Session["NEWSHIPID"] = ship.shipId;
                            Response.Redirect(currentPageName);
                        }
                        else
                        {
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new shipping details. Please try again.</div>";
                            Response.Redirect(currentPageName);
                        }
                    }
                    else
                    {
                        litAck.Text = "<div class=\"errmsg\">This shipping details already exists.</div>";

                        pnlAck.Visible = true;
                        pnlShippingDetails.Visible = false;
                    }
                    break;
                case 2:
                    Boolean boolSuccess = true;
                    Boolean boolEdited = false;

                    if (!ship.isExactSameSet2(shipId, strCountry, strState, decShipWeightFirst, decShipFeeFirst, decShipWeightSub, decShipFeeSub, decShipFreeWeight, intActive))
                    {
                        intRecordAffected = ship.updateShippingById2(shipId, strCountry, strState, decShipWeightFirst, decShipFeeFirst, decShipWeightSub, decShipFeeSub, decShipFreeWeight, intActive, Convert.ToInt16(Session["ADMID"]));

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;
                        }
                        else
                        {
                            boolSuccess = false;
                        }
                    }

                    if (boolEdited)
                    {
                        Session["EDITEDSHIPID"] = ship.shipId;
                        Response.Redirect(currentPageName);
                    }
                    else
                    {
                        if (!boolSuccess)
                        {
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit shipping details. Please try again.</div>";
                            Response.Redirect(currentPageName);
                        }
                        else
                        {
                            Session["EDITEDSHIPID"] = shipId;
                            Session["NOCHANGE"] = 1;
                            Response.Redirect(currentPageName);
                        }
                    }
                    break;
            }

        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        int intDeletedShipId = 0;

        clsMis.performDeleteShipping(shipId, ref intDeletedShipId);

        Session["DELETEDSHIPID"] = intDeletedShipId;
        Response.Redirect(currentPageName);
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        clsMis mis = new clsMis();
        DataSet ds;
        DataView dv;

        ds = new DataSet();
        ds = mis.getCountryList(1);
        dv = new DataView(ds.Tables[0]);
        dv.RowFilter = "COUNTRY_DEFAULT = 0";
        dv.Sort = "COUNTRY_NAME ASC";

        ddlCountry.DataSource = dv;
        ddlCountry.DataTextField = "COUNTRY_NAME";
        ddlCountry.DataValueField = "COUNTRY_CODE";
        ddlCountry.DataBind();

        ListItem ddlCountryDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlCountry.Items.Insert(0, ddlCountryDefaultItem);

        //mis.extractCountryById(countryId, 1);
        //Response.Write("country id: " + countryId + "<br/>");
        //Response.Write("showstate :" + mis.showState);     

        //if (mis.showState == 1)
        //{
        //    trState.Visible = true;
        //}
        //else
        //{
        //    trState.Visible = false;
        //}

        litFirstFee.Text = "First block";
        rfvFirstFee.ErrorMessage = "<br />Please enter shipping fee for first block";
        litSubSeqFee.Text = "Subsequent block";
        rfvSubSeqFee.ErrorMessage = "<br />Please enter shipping fee for subsequent block";
        litFreeShippingFee.Text = "Free after ";

        switch (mode)
        {
            case 1:
                lnkbtnDelete.Visible = false;
                break;
            case 2:
                lnkbtnDelete.Visible = true;
                break;
        }

        if (mode == 2)
        {
            if ((Session["EDITEDSHIPID"] == null) && (Session["DELETEDSHIPID"] == null)) { fillForm(); }
        }

        lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteShipping.Text").ToString() + "'); return false;";
        lnkbtnCancel.OnClientClick = "javascript:document.location.href='adm05.aspx'; return false;";
        lnkbtnBack.OnClientClick = "javascript:history.back(); return false;";
    }

    protected void fillForm()
    {
        clsShipping ship = new clsShipping();

        if (ship.extractShippingById2(shipId, 0))
        {
            clsMis mis = new clsMis();
            DataSet ds;
            DataView dv;

            if (ship.shipDefault != 0)
            {
                ListItem ddlCountryAllDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "ALL");
                ddlCountry.Items.Insert(0, ddlCountryAllDefaultItem);

                ddlCountry.SelectedValue = ship.shipCountry;

                ddlCountry.Enabled = false;
                txtState.Enabled = false;
                chkboxActive.Enabled = false;
                lnkbtnDelete.Visible = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(ship.shipCountry))
                {
                    ddlCountry.SelectedValue = ship.shipCountry;
                }
                else
                {
                    ddlCountry.SelectedValue = "MY";
                }
            }

            if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
            {
                string countryId = ddlCountry.SelectedValue;

                mis.extractCountryById(countryId, 1);

                if (mis.showState == 1)
                {
                    trState.Visible = true;
                    txtState.Text = ship.shipState;
                }
                else
                {
                    trState.Visible = false;
                }
            }

            txtFirstWeight.Text = ship.shipKgFirst.ToString();
            txtFirstFee.Text = ship.shipFeeFirst.ToString();
            txtSubSeqWeight.Text = ship.shipKg.ToString();
            txtSubSeqFee.Text = ship.shipFee.ToString();
            txtFreeShippingWeight.Text = ship.shipFeeFree.ToString();

            if (ship.shipActive == 1) { chkboxActive.Checked = true; }
            else { chkboxActive.Checked = false; }
        }
    }
    #endregion
}
