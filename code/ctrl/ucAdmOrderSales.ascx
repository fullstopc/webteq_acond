﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmOrderSales.ascx.cs" Inherits="ctrl_ucAdmOrderSales" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>

<script type="text/javascript">
    var txtSalesDate;
    var hdnSalesDate;

    function initSect3() {
        txtProdPromStartDate = document.getElementById("<%= txtSalesDate.ClientID %>");

        $(function() {
            $("#<% =txtSalesDate.ClientID %>").datepicker({ dateFormat: 'd M yy' });
            $("#<% =txtSalesDate.ClientID %>").datepicker({ changeMonth: true });
            $("#<% =txtSalesDate.ClientID %>").datepicker({ changeYear: true });
            $("#<% =txtSalesDate.ClientID %>").datepicker({ altField: "#<% =hdnSalesDate.ClientID %>" });
            $("#<% =txtSalesDate.ClientID %>").datepicker({ altFormat: "dd/mm/yy" });
            $("#<% =txtSalesDate.ClientID %>").datepicker("option", "yearRange", '-100:+0');

            //getter
            var changeMonth = $("#<% =txtSalesDate.ClientID %>").datepicker('option', 'changeMonth');
            var changeYear = $("#<% =txtSalesDate.ClientID %>").datepicker('option', 'changeYear');
            var altField = $("#<% =txtSalesDate.ClientID %>").datepicker("option", "altField");
            var altFormat = $("#<% =txtSalesDate.ClientID %>").datepicker("option", "altFormat");
            var yearRange = $("#<% =txtSalesDate.ClientID %>").datepicker("option", "yearRange");

            //setter
            $("#<% =txtSalesDate.ClientID %>").datepicker('option', 'changeMonth', true);
            $("#<% =txtSalesDate.ClientID %>").datepicker('option', 'changeYear', true);
            $("#<% =txtSalesDate.ClientID %>").datepicker("option", "altField", '#<% =hdnSalesDate.ClientID %>');
            $("#<% =txtSalesDate.ClientID %>").datepicker("option", "altFormat", 'dd/mm/yy');
            $("#<% =txtSalesDate.ClientID %>").datepicker("option", "yearRange", '-100:+0');
        });
    }

    function initSect4(txtPayDateId) {
        var txtPayDate = document.getElementById(txtPayDateId);

        $(function() {
            $("#" + txtPayDateId).datepicker({ dateFormat: 'dd/mm/yy' });

            $("#" + txtPayDateId).datepicker({ changeMonth: true });
            $("#" + txtPayDateId).datepicker({ changeYear: true });

            //getter
            var changeMonth = $("#" + txtPayDateId).datepicker('option', 'changeMonth');
            var changeYear = $("#" + txtPayDateId).datepicker('option', 'changeYear');

            //setter
            $("#" + txtPayDateId).datepicker('option', 'changeMonth', true);
            $("#" + txtPayDateId).datepicker('option', 'changeYear', true);
        });
    }

    function initSect5(txtDeliveryDateId) {
        var txtDeliveryDate = document.getElementById(txtDeliveryDateId);

        $(function() {
            $("#" + txtDeliveryDateId).datepicker({ dateFormat: 'dd/mm/yy' });

            $("#" + txtDeliveryDateId).datepicker({ changeMonth: true });
            $("#" + txtDeliveryDateId).datepicker({ changeYear: true });

            //getter
            var changeMonth = $("#" + txtDeliveryDateId).datepicker('option', 'changeMonth');
            var changeYear = $("#" + txtDeliveryDateId).datepicker('option', 'changeYear');

            //setter
            $("#" + txtDeliveryDateId).datepicker('option', 'changeMonth', true);
            $("#" + txtDeliveryDateId).datepicker('option', 'changeYear', true);
        });
    }

    function validateCustomer_client(source, args) {
        var cvCustomer = source.id;
        var cboxCustomer = $('#' + cvCustomer).attr('controltovalidate');
        var cboxCustomerInput = $('#' + cboxCustomer).find(':input[type=text]')[0].id;
        var cboxCustomerInputVal = $('#' + cboxCustomerInput).val();

        var boolValid = true;
        if (cboxCustomerInputVal == '') {
            boolValid = false;
        }

        args.IsValid = boolValid;
    }

    function checkExpandCollapse() {
        $('#<% =chkboxDeliveryDetails.ClientID %>').change(function() {
            var isChecked = $('#<% =chkboxDeliveryDetails.ClientID %>').attr('checked');
            if (isChecked) {
                $('#tblDeliveryDetail').hide();
            }
            else {
                $('#tblDeliveryDetail').show();
            }
        }).change();
    }

    function setAutocomplete(target) {
        $.ajax({
            type: "POST",
            url: wsBase + "wsSearchProducts.asmx/getNameList",
            dataType: "json",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            success: function(data) {
                var strNameList = data.d.split(',');
                $("#" + target).autocomplete({
                    source: strNameList,
                    change: function(event, ui) {
                        fillForm(target);
                    }
                });
            },
            error: function(xmlHttpRequest, status, err) {
                //alert("error " + err);
            }
        });
    }

    function calculateTotal(target) {
        target = $(target).attr("id");
        var tdList = $("#" + target).parent().parent().children();

        if (tdList.length > 0) {
            var intQty = $(tdList[3]).find("input:text").val();
            var decPrice = $(tdList[2]).find("input:text").val();

            if (isNaN(intQty) || intQty == "") {
                intQty = 0;
            }

            if (isNaN(decPrice) || decPrice == "") {
                decPrice = 0;
            }

            if (intQty > 0) {
                var decTotal = (parseFloat(decPrice) * parseFloat(intQty));
            }
            else {
                decTotal = 0;
            }

            $(tdList[4]).find("input:text").val(parseFloat(decTotal).toFixed(2));
            //$(tdList[4]).find("input:hidden").val(decTotal);
        }
    }

    function fillForm(source) {
        var strName = $("#" + source).val();
        var tdList = $("#" + source).parent().parent().children();
        var intId = 0;

        // fill product details
        var param = "{ 'strName': '" + strName + "'}";
        $.ajax({
            type: "POST",
            url: wsBase + "wsSearchProducts.asmx/getNamePriceByName",
            dataType: "json",
            data: param,
            contentType: "application/json; charset=utf-8",
            success: function(data) {
                if (data.d != "") {
                    var strNamePrice = data.d.split(strDefaultSeparator);
                    if (tdList.length > 0) {
                        $(tdList[1]).find("input:text").val(strNamePrice[1]);
                        $(tdList[2]).find("input:text").val(strNamePrice[2]);
                        intId = strNamePrice[3];
                        $(tdList[0]).find("input:hidden").val(intId);
                        $(tdList[1]).find("input:hidden").val(strNamePrice[4]);

                        var intQty = $(tdList[3]).find("input:text").val();
                        if (isNaN(intQty) || intQty == "") {
                            intQty = 0;
                        }

                        if (intQty == 0) {
                            $(tdList[3]).find("input:text").val("1");
                            intQty = 1;
                        }

                        if (intQty > 0) {
                            var decTotal = (parseFloat(strNamePrice[2]) * parseFloat(intQty));
                            $(tdList[4]).find("input:text").val(decTotal);
                        }
                    }
                }
                else {
                    if (tdList.length > 0) {
                        tdList.find("input:not(.code)").val("");
                    }
                }
            },
            error: function(xmlHttpRequest, status, err) {
                //alert("error " + err);
            }
        });
    }

    var intIndex = 0;
    function cloneForm() {
        var cloneItem = $('#trForm').clone(false, true);
        cloneItem.attr("id", "trForm" + intIndex);
        
        cloneItem
            .find("*").each(function() {
                $(this).val("")
                       .attr("id", function(_, id) {
                           if (id) {
                               $(this).attr("name", id + intIndex);
                               return id + intIndex;
                           }

                           return;
                       });

                if ($(this).hasClass("code")) {
                    setAutocomplete($(this).attr("id"));
                }

//                if ($(this).hasClass("price") || $(this).hasClass("qty")) {
//                    $(this).change(function() {
//                        calculateTotal($(this).attr("id"));
//                    });
//                }
            });

        $('#tbForm').append(cloneItem);
        intIndex++;  
    }

    function validatePrice_client(source, args) {
        //var txtProdUnitPrice = document.getElementById("<%= txtProdUnitPrice.ClientID %>");
        //txtProdUnitPrice.value = formatCurrency(txtProdUnitPrice.value);

        var currRe = /<%= clsAdmin.CONSTCURRENCYRE %>/;

        if (args.Value.match(currRe)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "<br/>Please enter only currency.";
            source.innerHTML = "<br/>Please enter only currency";
        }
    }

    function enableTextbox(chkid, txtid) {
        var chkboxShipping = document.getElementById(chkid);
        var txtShipping = document.getElementById(txtid);

        if (chkboxShipping.checked) {
            txtShipping.disabled = false;
            txtShipping.value = "";
        }
        else {
            txtShipping.disabled = true;
            txtShipping.value = "0.00";
        }
    }

    function validateShipping() {
        if ($('#tblDeliveryDetail').is(':visible')) {
            if (Page_ClientValidate('validateShipping')) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    function checkCustomer() {
        alert(document.getElementById("<%= chkboxBillingDelivery.ClientID %>").checked);
        var cboxCustomer = document.getElementById('<%= cboxCustomer.ClientID %>');
        
        alert(document.getElementById('<%= cboxCustomer.ClientID %>'));
        //document.getElementById("<%= chkboxBillingDelivery.ClientID %>")
        if (document.getElementById("<%= chkboxBillingDelivery.ClientID %>").checked) {
            //alert(cboxCustomer);
            if(cboxCustomer == null){
                alert("Please select sales customer");
            }
        }
    }

    $(document).ready(function() {
        setAutocomplete("<%= txtProdDName.ClientID %>");
        checkExpandCollapse();
    });
</script>

<asp:Panel ID="pnlOrderSalesContainer" runat="server" CssClass="divSalesDetail">
    <asp:Panel ID="pnlTop" runat="server" CssClass="divTop">
        <asp:Panel ID="pnlOrdInvoicePrint" runat="server" CssClass="divOrdInvoicePrint">
            <div id="divInvoicePrint" runat="server" class="divInvoicePrint" visible="false">
                <div class="divInvoicePrintLeft">Sales Order: 
                <asp:LinkButton ID="lnkbtnInvoicePrint" runat="server" Text="print" ToolTip="print"></asp:LinkButton> |
                <asp:LinkButton ID="lnkbtnInvoicePdf" runat="server" Text="pdf" ToolTip="pdf" OnClick="lnkbtnInvoicePdf_Click"></asp:LinkButton></div>
                <div class="divInvoicePrintRight">Delivery:
                <asp:LinkButton ID="lnkbtnDeliveryPrint" runat="server" Text="print" ToolTip="print"></asp:LinkButton> |
                <asp:LinkButton ID="lnkbtnDeliveryPdf" runat="server" Text="pdf" ToolTip="pdf" OnClick="lnkbtnDeliveryPdf_Click"></asp:LinkButton></div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlOrdSalesSummary" runat="server" CssClass="divSalesSummary">
            <table id="tblSalesSummary" cellpadding="0" cellspacing="0" border="0" class="formTbl">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Sales Summary</td>
                    </tr>
                </thead>
                <tr id="trSalesType" runat="server" visible="false">
                    <td class="tdLabelSales">Type:</td>
                    <td class="tdMaxSales"><asp:Literal ID="litType" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td class="tdLabelSales">Sales No<span class="attention_unique">*</span>:</td>
                    <td class="tdMaxSales">
                        <asp:TextBox ID="txtSalesNo" runat="server" MaxLength="20" CssClass="text_fullwidth uniq"></asp:TextBox><br />
                        <span class="tdLabelBigGrey"><asp:Literal ID="litLastSalesNo" runat="server"></asp:Literal></span>
                        <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvOrdSales" runat="server" ErrorMessage="<br/>Please enter Sales No." ControlToValidate="txtSalesNo" Display="Dynamic" CssClass="errmsg" ValidationGroup="ordsales"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cvSalesNo" runat="server" ControlToValidate="txtSalesNo" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateSalesNo_server" OnPreRender="cvSalesNo_PreRender" ValidationGroup="ordsales"></asp:CustomValidator></span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabelSales">Sales Date<span class="attention_compulsory">*</span>:</td>
                    <td class="tdMaxSales">
                        <asp:TextBox ID="txtSalesDate" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox><asp:HiddenField ID="hdnSalesDate" runat="server" />
                        <asp:RequiredFieldValidator ID="rfvSalesDate" runat="server" ErrorMessage="<br/>Please select sales date." ControlToValidate="txtSalesDate" Display="Dynamic" CssClass="errmsg" ValidationGroup="ordsales"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabelSales nobr">Customer P/O NO.:</td>
                    <td><asp:TextBox ID="txtCustomerNo" runat="server" MaxLength="250" CssClass="text_fullwidth"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabelSales">Customer<span class="attention_compulsory">*</span>:</td>
                    <td class="tdMaxSales cboxCustomer">
                        <asp:ComboBox ID="cboxCustomer" runat="server" AutoCompleteMode="SuggestAppend" MaxLength="250" DropDownStyle="DropDown" ValidationGroup="ordsales"></asp:ComboBox>
                        <asp:RequiredFieldValidator ID="rfvCustomer" runat="server" ControlToValidate="cboxCustomer" ErrorMessage="<br/>Please select one." Display="Dynamic" CssClass="errmsg" SetFocusOnError="true" ValidationGroup="checkdb"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cvCustomer" runat="server" ErrorMessage="<br/>Please enter Customer" Display="Dynamic" ControlToValidate="cboxCustomer" ValidateEmptyText="true" ClientValidationFunction="validateCustomer_client" ValidationGroup="ordsales"></asp:CustomValidator>
                    </td>
                </tr>
                <tr id="trSalesAmount" runat="server" visible="false">
                    <td class="tdLabelSales nobr">Sales Amount:</td>
                    <td class="tdMaxSales"><asp:Literal ID="litSalesAmount" runat="server"></asp:Literal></td>
                </tr>
                <tbody id="trSalesRemarks" runat="server" visible="false">
                    <tr>
                        <td class="tdLabelRemarks nobr">Remarks:</td>
                        <td class="tdMax">
                            <asp:TextBox ID="txtSalesRemarks" runat="server" TextMode="MultiLine" Rows="5" CssClass="text_fullwidth"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvSalesRemarks" runat="server" ErrorMessage="<br/>Please enter remarks." ControlToValidate="txtSalesRemarks" Display="Dynamic" CssClass="errmsg" ValidationGroup="remarks"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><asp:LinkButton ID="lnkbtnAddRemarks" CssClass="btn2 btnAdd" runat="server" Text="Add" ToolTip="Add" OnClick="lnkbtnAddRemarks_Click" ValidationGroup="remarks"></asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><asp:Panel ID="pnlSalesRemarksList" runat="server"></asp:Panel></td>
                    </tr>
                </tbody>
                <tr>
                    <td class="tdLabel nobr">Remarks in Invoice:</td>
                    <td class="tdMax"><asp:TextBox ID="txtRemarksInvoice" runat="server" TextMode="MultiLine" Rows="3" CssClass="text"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tfoot>
                    <tr>
                        <td></td>
                        <td colspan="">
                            <asp:Panel ID="pnlSalesActionBtns" runat="server" CssClass="divSalesActionBtns">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="ordsales" OnClientClick="javascript:return validateShipping();"></asp:LinkButton>
                                <a id="hypBack" title="Back" class="btn btnBack" href="<%= ConfigurationManager.AppSettings["scriptBase"] + "adm/admOrder01.aspx" %>">Back</a>
                                <div class="floated right">
                                    <asp:HyperLink ID="hypCancel" runat="server" Text="Cancel" ToolTip="Cancel" CssClass="thickbox btn btnCancel" Visible="false"></asp:HyperLink>
                                    <asp:HyperLink ID="hypDelete" runat="server" Text="Delete" ToolTip="Delete" CssClass="thickbox btn btnCancel" Visible="false"></asp:HyperLink>
                                    <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CssClass="btnRedPaddingRight" OnClick="lnkbtnDelete_Click" Visible="false"></asp:LinkButton>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                </tfoot>
            </table>
            </asp:Panel>
    </asp:Panel>
    <div class="divLeft">
        <asp:Panel ID="pnlDetails" runat="server" CssClass="divDetails">
            <div class="divDeliveryHeader">Delivery
            <asp:CheckBox ID="chkboxBillingDelivery" runat="server" OnCheckedChanged="chkboxBillingDelivery_Checked" AutoPostBack="true" ValidationGroup="checkdb" CausesValidation="true" /> Draw from database
            </div>
            <asp:Panel ID="pnlBillingDetails" runat="server" CssClass="divBillingDetails">
                <table cellpadding="0" cellspacing="0" class="formTbl2">
                    <thead>
                        <tr>
                            <td>
                                <span class="">Billing Details</span>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkboxCustomerDetails" runat="server" />
                                <span>Save in Customer Details</span>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="tdLabel">Company:</td>
                            <td class=""><asp:TextBox ID="txtBillingCompany" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                            
                          </tr>
                        <tr>  
                            <td class="tdLabel nobr">Contact Person<span class="attention_compulsory">*</span>:</td>
                            <td class="">
                                <asp:TextBox ID="txtBillingContactPerson" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvBillingContactPerson" runat="server" meta:resourcekey="rfvContactPerson" ControlToValidate="txtBillingContactPerson" Display="Dynamic" CssClass="errmsg" ValidationGroup="ordsales"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel">Address<span class="attention_compulsory">*</span>:</td>
                            <td class="">
                                <asp:TextBox ID="txtBillingAddress" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvBillingAddress" runat="server" meta:resourcekey="rfvAddress" ControlToValidate="txtBillingAddress" Display="Dynamic" CssClass="errmsg" ValidationGroup="ordsales"></asp:RequiredFieldValidator>
                            </td>
                            </tr>
                        <tr>
                            <td class="tdLabel">Email<span class="attention_compulsory">*</span>:</td>
                            <td class="">
                                <asp:TextBox ID="txtBillingEmail" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvBillingEmail" runat="server" meta:resourcekey="rfvEmail" ControlToValidate="txtBillingEmail" Display="Dynamic" CssClass="errmsg" ValidationGroup="ordsales"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revBillingEmail" runat="server" meta:resourcekey="revEmail" ControlToValidate="txtBillingEmail" Display="Dynamic" CssClass="errmsg" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="ordsales"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel nobr">Postal Code<span class="attention_compulsory">*</span>:</td>
                            <td class="">
                                <asp:TextBox ID="txtBillingPoscode" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvBillingPoscode" runat="server" meta:resourcekey="rfvPoscode" ControlToValidate="txtBillingPoscode" Display="Dynamic" CssClass="errmsg" ValidationGroup="ordsales"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel">Tel 1<span class="attention_compulsory">*</span>:</td>
                            <td class="">
                                <asp:TextBox ID="txtBillingTel1" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvBillingTel1" runat="server" meta:resourcekey="rfvTel1" ControlToValidate="txtBillingTel1" Display="Dynamic" CssClass="errmsg" ValidationGroup="ordsales"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel">City:</td>
                            <td class=""><asp:TextBox ID="txtBillingCity" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="tdLabel">Tel 2:</td>
                            <td class=""><asp:TextBox ID="txtBillingTel2" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="tdLabel">Country<span class="attention_compulsory">*</span>:</td>
                            <td class="">
                                <asp:DropDownList ID="ddlBillingCountry" runat="server" CssClass="ddl_fullwidth compulsory" AutoPostBack="true" OnSelectedIndexChanged="ddlBillingCountry_SelectedIndexChanged"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvBillingCountry" runat="server" meta:resourcekey="rfvCountry" ControlToValidate="ddlBillingCountry" Display="Dynamic" CssClass="errmsg" ValidationGroup="ordsales"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel">Fax:</td>
                            <td class=""><asp:TextBox ID="txtBillingFax" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="tdLabel">State<span class="attention_compulsory">*</span>:</td>
                            <td class="" id="tdBillingStateTxt" runat="server">
                                <asp:TextBox ID="txtBillingState" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvBillingStateText" runat="server" meta:resourcekey="rfvStateText" ControlToValidate="txtBillingState" Display="Dynamic" CssClass="errmsg" ValidationGroup="ordsales"></asp:RequiredFieldValidator>
                            </td>
                            <td class="" id="tdBillingStateDdl" runat="server" visible="false">
                                <asp:DropDownList ID="ddlBillingState" runat="server" CssClass="ddl_fullwidth compulsory"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvBillingStateDdl" runat="server" meta:resourcekey="rfvStateDdl" ControlToValidate="ddlBillingState" Display="Dynamic" CssClass="errmsg" ValidationGroup="ordsales"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlDeliveryDetails" runat="server" CssClass="divDeliveryDetails">
                <table id="tblDelivery" cellpadding="0" cellspacing="0" border="0" class="formTbl2">
                    <thead>
                        <td>Delivery Details</td>
                        <td><asp:CheckBox ID="chkboxDeliveryDetails" runat="server" Checked="true" />Same as Billing Details</td>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="tdLabel">Company:</td>
                        <td class=""><asp:TextBox ID="txtDeliveryCompany" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel nobr">Contact Person<span class="attention_compulsory">*</span>:</td>
                        <td class="">
                            <asp:TextBox ID="txtDeliveryContactPerson" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvDeliveryContactPerson" runat="server" meta:resourcekey="rfvContactPerson" ControlToValidate="txtDeliveryContactPerson" Display="Dynamic" CssClass="errmsg" ValidationGroup="validateShipping"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Address<span class="attention_compulsory">*</span>:</td>
                        <td class="">
                            <asp:TextBox ID="txtDeliveryAddress" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvDeliveryAddress" runat="server" meta:resourcekey="rfvAddress" ControlToValidate="txtDeliveryAddress" Display="Dynamic" CssClass="errmsg" ValidationGroup="validateShipping"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Email<span class="attention_compulsory">*</span>:</td>
                        <td class="">
                            <asp:TextBox ID="txtDeliveryEmail" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvDeliveryEmail" runat="server" meta:resourcekey="rfvEmail" ControlToValidate="txtDeliveryEmail" Display="Dynamic" CssClass="errmsg" ValidationGroup="validateShipping"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revDeliveryEmail" runat="server" meta:resourcekey="revEmail" ControlToValidate="txtDeliveryEmail" Display="Dynamic" CssClass="errmsg" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="validateShipping"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel nobr">Postal Code<span class="attention_compulsory">*</span>:</td>
                        <td class="">
                            <asp:TextBox ID="txtDeliveryPoscode" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvDeliveryPoscode" runat="server" meta:resourcekey="rfvPoscode" ControlToValidate="txtDeliveryPoscode" Display="Dynamic" CssClass="errmsg" ValidationGroup="validateShipping"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Tel 1<span class="attention_compulsory">*</span>:</td>
                        <td class="">
                            <asp:TextBox ID="txtDeliveryTel1" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvDeliveryTel1" runat="server" meta:resourcekey="rfvTel1" ControlToValidate="txtDeliveryTel1" Display="Dynamic" CssClass="errmsg" ValidationGroup="validateShipping"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">City:</td>
                        <td class=""><asp:TextBox ID="txtDeliveryCity" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Tel 2:</td>
                        <td class=""><asp:TextBox ID="txtDeliveryTel2" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Country<span class="attention_compulsory">*</span>:</td>
                        <td class="">
                            <asp:DropDownList ID="ddlDeliveryCountry" runat="server" CssClass="ddl_fullwidth compulsory" AutoPostBack="true" OnSelectedIndexChanged="ddlDeliveryCountry_SelectedIndexChanged"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvDeliveryCountry" runat="server" meta:resourcekey="rfvCountry" ControlToValidate="ddlDeliveryCountry" Display="Dynamic" CssClass="errmsg" ValidationGroup="validateShipping"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Fax:</td>
                        <td class=""><asp:TextBox ID="txtDeliveryFax" runat="server" CssClass="text"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">State<span class="attention_compulsory">*</span>:</td>
                        <td class="" id="tdDeliveryStateTxt" runat="server">
                            <asp:TextBox ID="txtDeliveryState" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvDeliveryStateText" runat="server" meta:resourcekey="rfvStateText" ControlToValidate="txtDeliveryState" Display="Dynamic" CssClass="errmsg" ValidationGroup="validateShipping"></asp:RequiredFieldValidator>
                        </td>
                        <td class="" id="tdDeliveryStateDdl" runat="server" visible="false">
                            <asp:DropDownList ID="ddlDeliveryState" runat="server" CssClass="ddl_fullwidth compulsory"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvDeliveryStateDdl" runat="server" meta:resourcekey="rfvStateDdl" ControlToValidate="ddlDeliveryState" Display="Dynamic" CssClass="errmsg" ValidationGroup="validateShipping"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    </tbody>  
                </table>
            </asp:Panel>
        </asp:Panel>
    </div>
    <div class="divRight">
        <asp:Panel ID="pnlStatus" runat="server" CssClass="divStatus">
            <div class="divStatusHeader">Order Status</div>
                <div id="divSalesStatus" runat="server" class="" visible="">
                <asp:Repeater ID="rptStatus" runat="server" OnItemDataBound="rptStatus_ItemDataBound" OnItemCommand="rptStatus_ItemCommand">
                    <HeaderTemplate>
                        <table id="tblStatus" cellpadding="0" cellspacing="0" class="tblStatus">
                            <thead>
                                <tr>
                                    <td colspan="2" class="tdSectionHdr"></td>
                                </tr>
                            </thead>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tbody>
                            <tr>
                                <td id="tdStatusAct" runat="server" class="tdStatusBtnLabel"><asp:LinkButton ID="lnkbtnStatus" runat="server" CssClass="btnStatus" Text='<%# DataBinder.Eval(Container.DataItem, "ORDERSTATUS_NAME") + ":"  %>' ToolTip='<%# DataBinder.Eval(Container.DataItem, "ORDERSTATUS_NAME") %>' CommandName="cmdStatus" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ORDERSTATUS_ID") %>' ValidationGroup="vgOrderStatus"></asp:LinkButton></td>
                                <td id="tdStatusDetails" runat="server" class="tdStatus">
                                    <div class="divLeft"><asp:Literal ID="litStatus" runat="server"></asp:Literal></div>
                                    <asp:Panel ID="pnlStatusAct" runat="server" CssClass="divStatusActRight" Visible="false">
                                        <asp:HyperLink ID="hypUndo" runat="server" CssClass="thickbox" ToolTip="Undo Status"></asp:HyperLink>
                                    </asp:Panel>
                                    <asp:HiddenField ID="hdnStatus" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ORDERSTATUS_ORDER") %>' />
                                </td>
                            </tr>
                            <tr id="trPaymentDate" runat="server" class="trItalic" visible="false">
                                <td id="tdPaymentDate" runat="server" class="tdStatusLabel">Date:</td>
                                <td id="tdPaymentDateDetails" runat="server" class="tdStatus">
                                    <asp:Panel ID="pnlPayDateText" runat="server" CssClass="divOrdStatusItalic" Visible="false">
                                        <asp:Literal ID="litPayDate" runat="server"></asp:Literal>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlPayDate" runat="server" Visible="false">
                                        <asp:TextBox ID="txtPayDate" runat="server" CssClass="text" ValidationGroup="vgOrderStatus"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPayDate" runat="server" ErrorMessage="<br/>Please enter payment date." ControlToValidate="txtPayDate" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgOrderStatus"></asp:RequiredFieldValidator>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trPaymentType" runat="server" class="trItalic" visible="false">
                                <td id="tdPaymentType" runat="server" class="tdStatusLabel">Payment Type:</td>
                                <td id="tdPaymentTypeDetails" runat="server" class="tdStatus">
                                    <asp:Panel ID="pnlPayTypeText" runat="server" CssClass="divOrdStatusItalic" Visible="false">
                                        <asp:Literal ID="litPayType" runat="server"></asp:Literal>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlPayType" runat="server" Visible="false">
                                        <asp:DropDownList ID="ddlPayType" runat="server" CssClass="ddl"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvPayType" runat="server" ErrorMessage="<br/>Please select one." ControlToValidate="ddlPayType" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgOrderStatus"></asp:RequiredFieldValidator>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trPaymentAmount" runat="server" class="trItalic" visible="false">
                                <td id="tdPaymentAmount" runat="server" class="tdStatusLabel">Payment Amount:</td>
                                <td id="tdPaymentAmountDetails" runat="server" class="tdStatus">
                                    <asp:Panel ID="pnlPayAmountTxt" runat="server" CssClass="divOrdStatusItalic" Visible="false">
                                        <asp:Literal ID="litPayAmount" runat="server"></asp:Literal>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlPayAmount" runat="server" CssClass="divPayAmount" Visible="false">
                                        <asp:TextBox ID="txtPayAmount" runat="server" MaxLength="1000" CssClass="text" ValidationGroup="vgOrderStatus" onchange="this.value = formatCurrency(this.value);"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPayAmount" runat="server" ErrorMessage="<br />Please enter payment amount." ControlToValidate="txtPayAmount" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgOrderStatus"></asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="cvPayAmount" runat="server" ControlToValidate="txtPayAmount" CssClass="errmsg" Display="Dynamic" ClientValidationFunction="validatePrice_client" ValidationGroup="vgOrderStatus"></asp:CustomValidator>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trPaymentDesc" runat="server" visible="false" class="trItalic">
                                <td id="tdPaymentDesc" runat="server" class="tdStatusLabel">Description:</td>
                                <td id="tdPaymentDescDetails" runat="server" class="tdStatus">
                                    <asp:Panel ID="pnlPayDescText" runat="server" CssClass="divOrdStatusItalic" Visible="false">
                                        <asp:Literal ID="litPayDesc" runat="server"></asp:Literal>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlPayDesc" runat="server" CssClass="divPayDesc" Visible="false">
                                        <asp:TextBox ID="txtPayDesc" runat="server" CssClass="text" MaxLength="1000"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trPaymentAction" runat="server" visible="false">
                                <td class="tdStatusLabelActive"></td>
                                <td class="tdStatusActive"><asp:LinkButton ID="lnkbtnConfirm" runat="server" Text="Confirm" ToolTip="Confirm" ValidationGroup="vgOrderStatus" CssClass="btnConfirm" CommandName="cmdStatus" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ORDERSTATUS_ID") %>'></asp:LinkButton></td>
                            </tr>
                            <tr id="trPaymentReject" runat="server" visible="false" class="trItalic">
                                <td id="tdPaymentRejectRemarks" runat="server" class="tdStatusLabel">Reject Remarks:</td>
                                <td id="tdPaymentRejectRemarksDetails" runat="server" class="tdStatus">
                                    <asp:Panel ID="pnlPayRejectRemarksText" runat="server" CssClass="divOrdStatusItalic" Visible="false">
                                        <asp:Literal ID="litPayRejectRemarks" runat="server"></asp:Literal>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlPayRejectRemarks" runat="server" CssClass="divPayRejectRemarks" Visible="false">
                                        <asp:TextBox ID="txtPayRejectRemarks" runat="server" CssClass="text" MaxLength="1000" Rows="2" TextMode="MultiLine" ValidationGroup="vgPayReject"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPayRejectRemarks" runat="server" ErrorMessage="<br />Please enter reject remarks." ValidationGroup="vgPayReject" CssClass="errmsg" Display="Dynamic" ControlToValidate="txtPayRejectRemarks"></asp:RequiredFieldValidator>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trPaymentRejectAction" runat="server" visible="false">
                                <td class="tdStatusLabelActive"></td>
                                <td class="tdStatusActive"><asp:LinkButton ID="lnkbtnReject" runat="server" Text="Reject" ToolTip="Reject" ValidationGroup="vgPayReject" CssClass="btnConfirm" CommandName="cmdReject"></asp:LinkButton></td>
                            </tr>
                            <tr id="trDeliveryDate" runat="server" visible="false" class="trItalic">
                                <td id="tdDeliveryDate" runat="server" class="tdStatusLabel">Shipping Date:</td>
                                <td id="tdDeliveryDateDetails" runat="server" class="tdStatus">
                                    <asp:Panel ID="pnlDeliveryDateText" runat="server" CssClass="divOrdStatusItalic" Visible="false">
                                        <asp:Literal ID="litDeliveryDate" runat="server"></asp:Literal>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlDeliveryDate" runat="server" Visible="false">
                                        <asp:TextBox ID="txtDeliveryDate" runat="server" CssClass="text"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trDeliveryCourier" runat="server" visible="false" class="trItalic">
                                <td id="tdDeliveryCourier" runat="server" class="tdStatusLabel">Shipping Company:</td>
                                <td id="tdDeliveryCourierDetails" runat="server" class="tdStatus">
                                    <asp:Panel ID="pnlDeliveryCourierText" runat="server" CssClass="divOrdStatusItalic" Visible="false">
                                        <asp:Literal ID="litDeliveryCourier" runat="server"></asp:Literal>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlDeliveryCourier" runat="server" CssClass="divDeliveryCourier" Visible="false">
                                        <asp:TextBox ID="txtDeliveryCourier" runat="server" CssClass="text" ValidationGroup="vgOrderStatus"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvDeliveryCourier" runat="server" ErrorMessage="<br />Please enter delivery courier." ControlToValidate="txtDeliveryCourier" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgOrderStatus"></asp:RequiredFieldValidator>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlDeliveryCourierCompany" runat="server" CssClass="divDeliveryCourierCompany" Visible="false">
                                        <asp:DropDownList ID="ddlDeliveryCourierCompany" runat="server" CssClass="ddl"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvDeliveryCourierCompany" runat="server" ErrorMessage="<br />Please select one shipping company." ControlToValidate="ddlDeliveryCourierCompany" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgOrderStatus"></asp:RequiredFieldValidator>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trDeliveryConsignment" runat="server" visible="false" class="trItalic">
                                <td id="tdDeliveryConsignment" runat="server" class="tdStatusLabel">Consignment No.:</td>
                                <td id="tdDeliveryConsignmentDetails" runat="server" class="tdStatus">
                                    <asp:Panel ID="pnlDeliveryConsignmentText" runat="server" CssClass="divOrdStatusItalic" Visible="false">
                                        <asp:Literal ID="litDeliveryConsignment" runat="server"></asp:Literal>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlDeliveryConsignment" runat="server" CssClass="divDeliveryConsignment" Visible="false">
                                        <asp:TextBox ID="txtDeliveryConsignment" runat="server" CssClass="text"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="rfvDeliveryConsignment" runat="server" ErrorMessage="<br />Please enter consignment no.." ControlToValidate="txtDeliveryConsignment" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgOrderStatus"></asp:RequiredFieldValidator>--%>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </tbody>
                    </ItemTemplate>
                    <SeparatorTemplate>
<%--                    <tr><td colspan="2" class="tdSpacer10"></td></tr>
                    <tr><td colspan="2" class="tdSpacer10_2"></td></tr>--%>
                    </SeparatorTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div class="divInvoiceQuickDone"><asp:LinkButton ID="lnkbtnQuickDone" runat="server" Text="Quick Done" ToolTip="Quick Done" CssClass="btn" Visible="false" OnClick="lnkbtnQuickDone_Click" ValidationGroup="ordsales" OnClientClick="javascript:return validateShipping();"></asp:LinkButton></div>
            </div>
        </asp:Panel>
    </div>
        
    
    <asp:Panel ID="pnlOrdSalesItems" runat="server" CssClass="divOrdSalesItems">
        <table ID="tblOrderItems" cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
            <tr>
                <td class="tdSectionHdr" style="text-align:left">Item(s)</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Repeater ID="rptItems" runat="server" OnItemDataBound="rptItems_ItemDataBound" OnItemCommand="rptItems_ItemCommand">
                        <HeaderTemplate>
                            <table cellpadding="0" cellspacing="0" border="0" class="dataTbl">
                                <colgroup>
                                    <col width="5%" />
                                    <col width="25%" />
                                    <col width="20%" />
                                    <col width="12%" />
                                    <col width="10%" />
                                    <col width="13%" />
                                    <col width="10%" />
                                </colgroup>
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Item</th>
                                    <th>Description</th>
                                    <th>Unit Price</th>
                                    <th>Qty</th>
                                    <th>Item Total</th>
                                    <th class="alignCenter">Action</th>
                                </tr>
                                </thead>
                        </HeaderTemplate>
                        <ItemTemplate>
                                <tr class="">
                                    <td id="tdNo" runat="server">
                                        <asp:Literal ID="litItemNo" runat="server"></asp:Literal>
                                        <asp:HiddenField ID="hdnSalesOri" runat="server" />
                                    </td>
                                    <td id="tdItemName" runat="server" class="">
                                        <asp:Literal ID="litItemName" runat="server"></asp:Literal>
                                        <asp:HiddenField ID="hdnDetailSalesId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ORDDETAIL_ID").ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + DataBinder.Eval(Container.DataItem, "SALES_ORI").ToString()  %>' />
                                        <asp:HiddenField ID="hdnProdId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ORDDETAIL_PRODID") %>' />
                                    </td>
                                    <td id="tdItemDesc" runat="server" class=""><asp:Literal ID="litItemDesc" runat="server"></asp:Literal></td>
                                    <td id="tdItemUnitPrice" runat="server" class="td_Left"><asp:Literal ID="litItemUnitPrice" runat="server"></asp:Literal></td>
                                    <td id="tdItemQty" runat="server">
                                        <asp:TextBox ID="txtItemQty" runat="server" CssClass="text_small"></asp:TextBox>
                                        <asp:HiddenField ID="hdnProdQty" runat="server" />
                                    </td>
                                    <td id="tdItemTotal" runat="server" class=""><asp:Literal ID="litItemTotal" runat="server"></asp:Literal></td>
                                    <td id="tdItemDelete" runat="server" class="tdAct">
                                        <asp:LinkButton ID="lnkbtnDelete" CssClass="lnkbtn lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CommandName="cmdDelete"></asp:LinkButton>
                                    </td>
                                </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                                <tfoot>
                                <tr class="trFooter">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td colspan="2" style="text-align:right;">Subtotal</td>
                                    <td style="text-align:right;"><asp:Literal ID="litSubtotal" runat="server"></asp:Literal></td>
                                    <td></td>
                                </tr>
                                <tr class="trFooter">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td colspan="2" style="text-align:right;vertical-align: middle;"><asp:CheckBox ID="chkboxShipping" runat="server" Checked="true" />Shipping</td>
                                    <td style="text-align:right;"><asp:TextBox ID="txtShipping" runat="server" CssClass="text_medium alignRight" onchange="this.value = formatCurrency(this.value);"></asp:TextBox></td>
                                    <td></td>
                                </tr>
                                <tr class="trFooter">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td colspan="2" style="text-align:right;">Grand Total</td>
                                    <td style="text-align:right;"><asp:Literal ID="litGrandTotal" runat="server"></asp:Literal></td>
                                    <td><asp:LinkButton ID="lnkbtnUpdate" CssClass="" runat="server" Text="Update" ToolTip="Update" OnClick="lnkbtnUpdate_Click"></asp:LinkButton></td>
                                </tr>
                                </tfoot>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
            <tr id="trAddItems" runat="server">
                <td>
                    <table id="tblAddItems" cellpadding="0" cellspacing="0" border="0" class="formTbl" style="position:relative;">
                        <colgroup>
                            <col width="45%" />
                            <col width="40%" />
                            <col width="5%" />
                            <col width="5%" />
                            <col width="5%" />
                            <col width="5%" />
                        </colgroup>
                        <tbody id="tbForm">
                            <tr id="trForm">
                                <td class="tdAddItemDName">
                                    <asp:TextBox ID="txtProdDName" runat="server" CssClass="text_fullwidth code" MaxLength="250"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvProdName" runat="server" meta:resourcekey="rfvProdName" ControlToValidate="txtProdDName" Display="Dynamic" CssClass="errmsg" ValidationGroup="orditem"></asp:RequiredFieldValidator>
                                    <asp:ComboBox ID="cboxProdName" runat="server" AutoCompleteMode="SuggestAppend" MaxLength="250" DropDownStyle="DropDown" Visible="false"></asp:ComboBox>
                                    <asp:HiddenField ID="hdnId" runat="server" />
                                </td>
                                <td class="tdAddItemDesc">
                                    <asp:TextBox ID="txtProdDesc" runat="server" TextMode="MultiLine" Rows="2" CssClass="text_fullwidth"></asp:TextBox>
                                    <asp:HiddenField ID="hdnProdCode" runat="server" />
                                </td>
                                <td class="tdAddItem">
                                    <asp:TextBox ID="txtProdUnitPrice" runat="server" CssClass="text_fullwidth price" onchange="this.value = formatCurrency(this.value); calculateTotal(this);"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvProdUnitPrice" runat="server" meta:resourcekey="rfvProdUnitPrice" ControlToValidate="txtProdUnitPrice" Display="Dynamic" CssClass="errmsg" ValidationGroup="orditem"></asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="cvProdUnitPrice" runat="server" ControlToValidate="txtProdUnitPrice" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validatePrice_client"></asp:CustomValidator>
                                </td>
                                <td class="tdAddItem">
                                    <asp:TextBox ID="txtProdQty" runat="server" CssClass="text_fullwidth qty" onchange="calculateTotal(this);"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvProdQty" runat="server" meta:resourcekey="rfvProdQty" ControlToValidate="txtProdQty" Display="Dynamic" CssClass="errmsg" ValidationGroup="orditem"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtProdItemTotal" runat="server" CssClass="text_fullwidth" onchange="this.value = formatCurrency(this.value);"></asp:TextBox>
                                    <asp:CustomValidator ID="cvProdItemTotal" runat="server" ControlToValidate="txtProdItemTotal" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validatePrice_client"></asp:CustomValidator>
                                </td>
                            </tr>
                        </tbody>
                            <tr>
                                <td colspan="6" class="alignRight">
                                    <asp:LinkButton ID="lnkbtnAdd" runat="server" Text="Add" ToolTip="Add" class="btn btnSave" OnClick="lnkbtnAdd_Click" ValidationGroup="orditem"></asp:LinkButton>
                                    <a id="btnMoreItem" value="more item" title="more item" class="btn" onclick="cloneForm()" style="cursor:pointer;"/>more item</a>
                                </td>
                            </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>
