﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ctrl_ucAdmJobDetail : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected string _pageListingURL = "";
    protected int _jobId = 0;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    public int jobId
    {
        get
        {
            if (ViewState["jobId"] == null)
            {
                return _jobId;
            }
            else
            {
                return int.Parse(ViewState["jobId"].ToString());
            }
        }
        set { ViewState["jobId"] = value; }
    }
    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            if (mode == 2) { fillForm(); }

        }
    }

    protected void fillForm()
    {
        clsReservation job = new clsReservation();
        if (job.extractByID(jobId))
        {
            string longDateFormat = GetGlobalResourceObject("GlobalResource", "longDateFormat").ToString();
            string shortTimeFormat = GetGlobalResourceObject("GlobalResource", "shortTimeFormat").ToString();

            #region Left Panel
            // Job Details
            litOrderID.Text = string.Format("<b>{0}</b>", job.code);
            litOrderDate.Text = job.creation.ToString(longDateFormat);
            litServiceDateTime.Text = string.Format("{0}-{1}", job.serviceDatetimeFrom.ToString(longDateFormat), job.serviceDatetimeTo.ToString(shortTimeFormat));
            litAreaUnit.Text = string.Format("{0}, {1}", job.propertyAreaName, job.propertyUnitno);
            litNoACond.Text = job.serviceItemQty.ToString();
            litSpecialRequest.Text = job.serviceRemark;
            litUrgentContact.Text = job.serviceContactTel;

            // Resident Details
            litResidentName.Text = job.memName;
            litContactNo.Text = job.memContactTel;
            litEmail.Text = job.memEmail;
            #endregion

            #region Right Panel
            // Order Status
            lblStatus.Text = job.getStatusText();

            // Cancel Job
            if (lblStatus.Text == "Completed" || lblStatus.Text == "Cancelled")
            {
                lnkbtnCancelJob.Visible = false;
                lnkbtnMarkasComplete.Visible = false;
            }

            // Payment History
            var payment = new clsPayment();
            var items = payment.getPaymentHistoryByOrderId(jobId);
            rptPayments.DataSource = items;
            rptPayments.DataBind();


            // Team
            if (lblStatus.Text == "Paid" || lblStatus.Text == "Assigned")
            {
                clsEmployee agents = new clsEmployee();
                var teams = agents.getTeams().ToList();

                List<ListItem> ddlItems = new List<ListItem>();
                foreach (var team in teams)
                {
                    ddlItems.Add(new ListItem(team, team));
                }
                ddlTeam.Items.AddRange(ddlItems.ToArray());
                ddlTeam.Items.Insert(0, new ListItem("Select One", "NA"));

                if (!string.IsNullOrEmpty(job.serviceTeam))
                    ddlTeam.Text = job.serviceTeam;

                lnkbtnAssign.Text = lblStatus.Text == "Paid" ? "Assign" : "Change";
                lblTeam.Visible = false;
            }
            else
            {
                ddlTeam.Visible = false;
                lnkbtnAssign.Visible = false;
                lblTeam.Text = job.serviceTeam;
            }


            // Assign On
            var resStatus = new clsReservationStatus();
            var lastAssign = resStatus.getLastAssign(jobId);
            if (lastAssign != null)
            {
                lblServiceAssignTime.Text = lastAssign.staDateTime.ToString(longDateFormat);
            }
            else
            {
                lblServiceAssignTime.Text = "-";
            }

            // Service Status
            var servStatus = new clsServiceStatus();
            var lastStatus = servStatus.getLastStatus(jobId);
            if (lastStatus != null)
            {
                lblServiceStatus.Text = lastStatus.StatusInText;
                lblServiceCompleteTime.Text = lastStatus.staDateTime.ToString(longDateFormat);
            }
            else
            {
                lblServiceStatus.Text = "-";
            }


            #endregion

            var datetimeNow = DateTime.Now;

            var leaves = new clsEmployeeLeave().getDataTable().AsEnumerable()
                .Select(x => new clsEmployeeLeave() { row = x })
                .Where(x => x.start.Date >= datetimeNow.Date &&
                            x.end.Date <= datetimeNow.Date)
                .Select(x => x.employeeID.ToString());

            var occupied = new clsReservation().getDataTable().AsEnumerable()
                .Select(x => new clsReservation() { row = x })
                .Where(x => job.serviceDatetimeFrom <= x.serviceDatetimeFrom && job.serviceDatetimeTo > x.serviceDatetimeFrom ||
                            job.serviceDatetimeFrom < x.serviceDatetimeTo && job.serviceDatetimeTo >= x.serviceDatetimeTo ||
                            job.serviceDatetimeFrom <= x.serviceDatetimeFrom && job.serviceDatetimeTo >= x.serviceDatetimeTo ||
                            job.serviceDatetimeFrom >= x.serviceDatetimeFrom && job.serviceDatetimeTo <= x.serviceDatetimeTo)
                .Select(x => x.employeeID.ToString());

            var employees = new clsEmployee().getDataTable().AsEnumerable().Select(x => new clsEmployee() { row = x });
            var employeeNoFree = leaves.Concat(occupied).Distinct().ToList();
            var employeeFree = employees.Where(x => x.ID == job.employeeID || !employeeNoFree.Contains(x.ID.ToString()));

            ddlAgent.DataSource = employeeFree;
            ddlAgent.DataValueField = "ID";
            ddlAgent.DataTextField = "name";
            ddlAgent.DataBind();
            ddlAgent.SelectedValue = job.employeeID.ToString();



        }
    }
    #endregion

    #region "Button Events"
    protected void lnkbtnCancelJob_Click(object sender, EventArgs e)
    {
        clsReservation job = new clsReservation();
        if (job.extractByID(jobId))
        {
            int intAdmID = Convert.ToInt32(Session["ADMID"]);
            string msg = "";
            string status = job.getStatusText();

            if (status == "Paid" || status == "Assigned")
            {
                job.resstatus = 3;
                var resStatus = new clsReservationStatus
                {
                    ID = -1,
                    resID = jobId,
                    staStatus = 3
                };

                if (job.update())
                {
                    string url = currentPageName + "?id=" + jobId;
                    Response.Redirect(url);
                }
                else
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString();
                    Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.SaveSuccess('" + msg + "')", true);
                }

            }
        }
    }

    protected void lnkbtnAssign_Click(object sender, EventArgs e)
    {
        string msg = "";
        if (ddlTeam.SelectedValue == "NA")
        {
            Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.SaveSuccess('Please select a team to assigned.')", true);
        }
        else
        {
            clsReservation job = new clsReservation();
            if (job.extractByID(jobId))
            {
                job.serviceTeam = ddlTeam.Text;
                job.resstatus = 1;
                int intAdmID = Convert.ToInt32(Session["ADMID"]);
                

                if (!job.isSame())
                {
                    if (job.update())
                    {
                        // Update Status Label 
                        lblStatus.Text = job.getStatusText();
                        msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();

                        // Update Assign On Label
                        var resStatus = new clsReservationStatus();
                        var lastAssign = resStatus.getLastAssign(jobId);
                        string longDateFormat = GetGlobalResourceObject("GlobalResource", "longDateFormat").ToString();
                        if (lastAssign != null)
                        {
                            lblServiceAssignTime.Text = lastAssign.staDateTime.ToString(longDateFormat);
                        }

                        
                    }
                    else
                    {
                        msg = GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString();
                    }
                }
                else
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString();
                }

                Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.SaveSuccess('" + msg + "')", true);
            }
        }
        
    }

    protected void lnkbtnMarkasComplete_Click(object sender, EventArgs e)
    {
        int intAdmID = Convert.ToInt32(Session["ADMID"]);
        string msg = "";

        clsReservation job = new clsReservation();
        if (job.extractByID(jobId))
        {
            job.resstatus = 2; // Set to Completed.
            if (!job.isSame())
            {
                if (job.update())
                {
                    clsServiceStatus serviceStatus = new clsServiceStatus
                    {
                        ID = -1,
                        resID = jobId,
                        staStatus = 1
                    };

                    if (serviceStatus.add())
                    {
                        string url = currentPageName + "?id=" + jobId;
                        Response.Redirect(url);

                    }
                }
                else
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString();
                }
            }
            else
            {
                msg = GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString();
            }
        }
        else
        {
            msg = GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString();
            Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.SaveSuccess('" + msg + "')", true);
        }
    }
    #endregion

    protected void lnkbtnAssignAgent_Click(object sender, EventArgs e)
    {
        string msg = "";
        clsReservation job = new clsReservation();
        if (job.extractByID(jobId))
        {
            job.employeeID = ddlAgent.SelectedValue.ToInteger();
            job.resstatus = 1;
            int intAdmID = Convert.ToInt32(Session["ADMID"]);


            if (!job.isSame())
            {
                if (job.update())
                {
                    // Update Status Label 
                    lblStatus.Text = job.getStatusText();
                    msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();

                    // Update Assign On Label
                    var resStatus = new clsReservationStatus();
                    var lastAssign = resStatus.getLastAssign(jobId);
                    string longDateFormat = GetGlobalResourceObject("GlobalResource", "longDateFormat").ToString();
                    if (lastAssign != null)
                    {
                        lblServiceAssignTime.Text = lastAssign.staDateTime.ToString(longDateFormat);
                    }


                }
                else
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString();
                }
            }
            else
            {
                msg = GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString();
            }

            Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.SaveSuccess('" + msg + "')", true);
        }
    }
}