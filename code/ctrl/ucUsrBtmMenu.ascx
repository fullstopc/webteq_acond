﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrBtmMenu.ascx.cs" Inherits="ctrl_ucUsrBtmMenu" %>

<asp:Panel ID="pnlBtmMenu" runat="server" CssClass="divBtmMenuList">
    <asp:Repeater ID="rptBtmMenu" runat="server" OnItemDataBound="rptBtmMenu_ItemDataBound">
        <ItemTemplate>
            <asp:HyperLink ID="hypBtmMenu" runat="server" CssClass="btmMenuItem"></asp:HyperLink>
        </ItemTemplate>
        <SeparatorTemplate><div class="divBtmSpliter"></div></SeparatorTemplate>
    </asp:Repeater>
</asp:Panel>
