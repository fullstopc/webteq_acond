﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrBookingStep4.ascx.cs" Inherits="ctrl_ucUsr" %>
<div class="row bookingform__headline">
    <div class="col">
        <h3>
            Hi <asp:Literal runat="server" ID="litMemberName"></asp:Literal>, 
            <asp:Literal runat="server" ID="litAckMsg"></asp:Literal>
        </h3>
    </div>
</div>
<div class="row bookingform__info">
    <div class="col">
        <span class="">
            You should be receiving a confirmation email at<br />
            <asp:HyperLink runat="server" ID="hypMemberEmail">
                <asp:Literal runat="server" ID="litMemberEmail"></asp:Literal>
            </asp:HyperLink>
        </span>
    </div>
</div>
<div class="row bookingform__info">
    <div class="col">
            Your booking details as below:
    </div>
</div>
<div class="bookingform__review">
    <div class="row">
        <div class="col">
            Date
        </div>
        <div class="col">
            <asp:Literal runat="server" ID="litServiceDate"></asp:Literal>
        </div>
    </div>
    <div class="row">
        <div class="col">
            Time
        </div>
        <div class="col">
            <asp:Literal runat="server" ID="litServiceTime"></asp:Literal>
        </div>
    </div>
    <div class="row">
        <div class="col">
            Unit Number:
        </div>
        <div class="col">
            <asp:Literal runat="server" ID="litServiceAddrUnit"></asp:Literal>
        </div>
    </div>
    <div class="row">
        <div class="col">
            No. of Aircond:
        </div>
        <div class="col">
            <asp:Literal runat="server" ID="litServiceItemQty"></asp:Literal>
        </div>
    </div>
</div>
<div class="row bookingform__remark">
    <div class="col">
        <div><b>Special Request :</b></div>
        <div><b><asp:Literal runat="server" ID="litServiceRemark"></asp:Literal></b></div>
    </div>
</div>
<div class="bookingform__contact">
    <div class="row bookingform__contact__info">
        <div class="col">
            For urgent case, we should contact this number: <br />
            <asp:Literal runat="server" ID="litServiceContactTel"></asp:Literal>
        </div>
    </div>
</div>
<div class="row bookingform__amount">
    <div class="col">
        Cost:
    </div>
    <div class="col">
        <asp:Literal runat="server" ID="litServiceAmount"></asp:Literal>
    </div>
</div>