﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

public partial class ctrl_ucAdmProgramDetails : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected int _progId = 0;
    protected string _pageListingURL = "";

    protected string progName;
    protected string progNameZh;
    protected string progSnapshot;
    protected string progSnapshotzh;
    protected string progDesc;
    protected string progDescZh;
    protected string progImage;
    protected DateTime progStartDate;
    protected DateTime progEndDate;
    protected decimal progFee;
    protected DateTime progPublishDate;
    protected int progTotalParticipant;
    protected int progNoOfHours;
    protected int progCat;
    protected int progActive;
    protected int progOnlineReg;
    protected int progArea;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int progId
    {
        get
        {
            if (ViewState["PROGRAMID"] == null)
            {
                return _progId;
            }
            else
            {
                return Convert.ToInt16(ViewState["PROGRAMID"]);
            }
        }
        set
        {
            ViewState["PROGRAMID"] = value;
        }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if (ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void validateProgImage_server(object source, ServerValidateEventArgs args)
    {
        if (fileProgImage.HasFile)
        {
            int intImgMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            //if (fileProgImage.PostedFile.ContentLength > clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH)
            //{
            //    cvProgImage.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH, "kb") + ".";
            //    args.IsValid = false;
            //}

            if (fileProgImage.PostedFile.ContentLength > intImgMaxSize)
            {
                cvProgImage.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + ".";
                args.IsValid = false;
            }
            else
            {
                Match matchImage = Regex.Match(fileProgImage.FileName, clsAdmin.CONSTADMIMGALLOWEDEXT, RegexOptions.IgnoreCase);
                if (matchImage.Success)
                {
                    args.IsValid = true;
                }
                else
                {
                    cvProgImage.ErrorMessage = "<br />Please enter valid product image. (" + clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + ").";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cvProgImage_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("ProgImage")))
        {
            string strJS = null;
            strJS = "<script type = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + fileProgImage.ClientID + "', document.all['" + cvProgImage.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "ProgImage", strJS, false);
        }
    }

    protected void lnkbtnDeleteImage_Click(object sender, EventArgs e)
    {
        pnlProgImage.Visible = false;
        hdnProgImage.Value = "";
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Boolean boolExist = false;
            Boolean boolEnd = false;
            string strStartDate = txtProgStartDate.Text.Trim();
            string strEndDate = txtProgEndDate.Text.Trim();
            string strProgramDate = strStartDate + "," + strEndDate + clsAdmin.CONSTDEFAULTSEPERATOR;

            boolExist = false;

            if (!string.IsNullOrEmpty(strStartDate) && !string.IsNullOrEmpty(strEndDate))
            {
                string strProgStartDate = strStartDate + clsAdmin.CONSTDEFAULTSEPERATOR;

                if (Session["ORISTARTDATE"] != null && Session["ORISTARTDATE"] != "")
                {
                    if (Session["ORISTARTDATE"].ToString().IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + strProgStartDate) >= 0) { boolExist = true; }
                }

                if (!boolExist && Session["STARTDATE"] != null && Session["STARTDATE"] != "")
                {
                    if (Session["STARTDATE"].ToString().IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + strProgStartDate) >= 0) { boolExist = true; }
                }

                boolEnd = false;

                string strProgEndDate = strEndDate + clsAdmin.CONSTDEFAULTSEPERATOR;

                if (Session["ORIENDDATE"] != null && Session["ORIENDDATE"] != "")
                {
                    if (Session["ORIENDDATE"].ToString().IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + strProgEndDate) >= 0) { boolEnd = true; }
                }

                if (!boolExist && Session["ENDDATE"] != null && Session["ENDDATE"] != "")
                {
                    if (Session["ENDDATE"].ToString().IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + strProgEndDate) >= 0) { boolEnd = true; }
                }

                if (!boolExist && !boolEnd)
                {
                    Session["STARTDATE"] += strProgStartDate;
                    Session["ENDDATE"] += strProgEndDate;
                }
                else if (boolExist && boolEnd)
                {
                    if (Session["DELSTARTDATE"] != null && Session["DELSTARTDATE"] != "")
                    {
                        if (Session["DELSTARTDATE"].ToString().IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + strProgStartDate) >= 0) { Session["DELSTARTDATE"].ToString().Replace(strProgStartDate, clsAdmin.CONSTDEFAULTSEPERATOR.ToString()); }
                    }

                    if (Session["DELENDDATE"] != null && Session["DELENDDATE"] != "")
                    {
                        if (Session["DELENDDATE"].ToString().IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + strProgEndDate) >= 0) { Session["DELENDDATE"].ToString().Replace(strProgEndDate, clsAdmin.CONSTDEFAULTSEPERATOR.ToString()); }
                    }
                }
            }
        }

        populateFileUpload();
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        clsProgram pro = new clsProgram();
        int intRecordAffected = 0;
        pro.extractProgramById(progId);

        intRecordAffected = pro.setDeleted(progId);

        if (intRecordAffected == 1)
        {
            Session["DELETEDPROGRAMNAME"] = pro.programName;
            Response.Redirect(currentPageName);
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            progName = txtProgName.Text.Trim();
            progNameZh = txtProgNameZh.Text.Trim();
            progSnapshot = txtProgSnapshot.Text.Trim();
            progSnapshotzh = txtProgSnapshotZh.Text.Trim();
            progDesc = txtProgDesc.Text.Trim();
            progDescZh = txtProgDescZh.Text.Trim();

            decimal decTryParse;
            if (decimal.TryParse(txtProgFee.Text.Trim(), out decTryParse))
            {
                progFee = decTryParse;
            }

            DateTime dtTryParse;
            if (DateTime.TryParse(clsMis.formatDateDDMMtoMMDD(txtProgPublishDate.Text.Trim()), out dtTryParse))
            {
                progPublishDate = dtTryParse;
            }
            else
            {
                progPublishDate = DateTime.MaxValue;
            }

            progTotalParticipant = txtProgTotalParticipant.Text != "" ? Convert.ToInt16(txtProgTotalParticipant.Text.Trim()) : 0;
            progNoOfHours = txtProgNoOfHours.Text != "" ? Convert.ToInt16(txtProgNoOfHours.Text.Trim()) : 0;
            progCat = !string.IsNullOrEmpty(ddlCat.SelectedValue) ? int.Parse(ddlCat.SelectedValue) : 0;
            progActive = chkboxProgActive.Checked ? 1 : 0;
            progOnlineReg = chkboxOnlineReg.Checked ? 1 : 0;
            progArea = !string.IsNullOrEmpty(ddlProgArea.SelectedValue) ? int.Parse(ddlProgArea.SelectedValue) : 0;

            if (fileProgImage.HasFile)
            {
                try
                {
                    HttpPostedFile postedFile = fileProgImage.PostedFile;
                    string fileName = Path.GetFileName(postedFile.FileName);
                    string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPROGRAMFOLDER + "/";
                    clsMis.createFolder(uploadPath);
                    string newFilename = clsMis.GetUniqueFilename(fileName, uploadPath);
                    postedFile.SaveAs(uploadPath + newFilename);
                    progImage = newFilename;
                }
                catch (Exception ex)
                {
                    clsLog.logErroMsg(ex.Message.ToString());
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />" + ex.Message.ToString() + "</div>";

                    throw ex;
                }
            }
            else
            {
                if (mode == 1)
                {
                    progImage = "";
                }
                else
                {
                    progImage = hdnProgImage.Value;
                }
            }

            clsProgram prog = new clsProgram();
            int intRecordAffected = 0;

            switch (mode)
            {
                case 1:
                    intRecordAffected = prog.addProgram(progName, progNameZh, progSnapshot, progSnapshotzh, progDesc, progDescZh, progImage, progFee, progPublishDate, progTotalParticipant, progNoOfHours, progActive, progOnlineReg, progArea, Convert.ToInt16(Session["ADMID"]));

                    if (intRecordAffected == 1)
                    {
                        if (Session["STARTDATE"] != null && Session["STARTDATE"] != "" && Session["ENDDATE"] != null && Session["ENDDATE"] != "")
                        {
                            string strProgStartDate = Session["STARTDATE"].ToString();
                            string strProgEndDate = Session["ENDDATE"].ToString();

                            strProgStartDate = strProgStartDate.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strProgStartDate.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                            strProgEndDate = strProgEndDate.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strProgEndDate.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                            if (!string.IsNullOrEmpty(strProgStartDate) && !string.IsNullOrEmpty(strProgEndDate))
                            {
                                strProgStartDate = strProgStartDate.Substring(0, strProgStartDate.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                                strProgEndDate = strProgEndDate.Substring(0, strProgEndDate.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                                string[] strProgStartDateSplit = strProgStartDate.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                                string[] strProgEndDateSplit = strProgEndDate.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                                for (int i = 0; i <= strProgStartDateSplit.GetUpperBound(0); i++)
                                {
                                    progStartDate = Convert.ToDateTime(clsMis.formatDateDDMMtoMMDD(strProgStartDateSplit[i]));
                                    progEndDate = Convert.ToDateTime(clsMis.formatDateDDMMtoMMDD(strProgEndDateSplit[i]));

                                    intRecordAffected = prog.addProgramDate(prog.programId, progStartDate, progEndDate);
                                }
                            }
                        }

                        if (progCat != 0)
                        {
                            prog.assignGrpToProg(prog.programId, progCat);
                        }

                        Session["NEWPROGRAMID"] = prog.programId;
                        Response.Redirect(currentPageName + "?id=" + prog.programId);
                    }
                    else
                    {
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new program. Please try again.</div>";
                        Response.Redirect(currentPageName);
                    }
                    break;
                case 2:
                    Boolean boolEdited = false;
                    Boolean boolSuccess = true;

                    int intCatOld = !string.IsNullOrEmpty(hdnCat.Value.Trim()) ? int.Parse(hdnCat.Value.Trim()) : 0;
                    if (progCat != intCatOld)
                    {
                        if (intCatOld == 0)
                        {
                            intRecordAffected = prog.assignGrpToProg(progId, progCat);
                        }
                        else
                        {
                            intRecordAffected = prog.updateGrpToProg(progId, progCat, intCatOld);
                        }

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;
                        }
                    }

                    if (boolSuccess && Session["DELSTARTDATE"] != null && Session["DELSTARTDATE"] != "" && Session["DELENDDATE"] != null && Session["DELENDDATE"] != "")
                    {
                        string strDelStartDate = Session["DELSTARTDATE"].ToString();
                        string strDelEndDate = Session["DELENDDATE"].ToString();

                        strDelStartDate = strDelStartDate.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strDelStartDate.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                        strDelEndDate = strDelEndDate.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strDelEndDate.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                        if (!string.IsNullOrEmpty(strDelStartDate) && !string.IsNullOrEmpty(strDelEndDate))
                        {
                            strDelStartDate = strDelStartDate.Substring(0, strDelStartDate.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                            strDelEndDate = strDelEndDate.Substring(0, strDelEndDate.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                            string[] strDelStartDateSplit = strDelStartDate.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                            string[] strDelEndDateSplit = strDelEndDate.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                            for (int i = 0; i <= strDelStartDateSplit.GetUpperBound(0); i++)
                            {
                                progStartDate = Convert.ToDateTime(strDelStartDateSplit[i]);
                                progEndDate = Convert.ToDateTime(strDelEndDateSplit[i]);

                                intRecordAffected = prog.deleteProgramDate(progId, progStartDate, progEndDate);

                                if (intRecordAffected == 1)
                                {
                                    boolEdited = true;
                                }
                                else
                                {
                                    boolSuccess = false;
                                    break;
                                }
                            }
                        }
                    }

                    if (boolSuccess && Session["STARTDATE"] != null && Session["STARTDATE"] != "" && Session["ENDDATE"] != null && Session["ENDDATE"] != "")
                    {
                        string strProgStartDate = Session["STARTDATE"].ToString();
                        string strProgEndDate = Session["ENDDATE"].ToString();

                        strProgStartDate = strProgStartDate.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strProgStartDate.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                        strProgEndDate = strProgEndDate.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strProgEndDate.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                        if (!string.IsNullOrEmpty(strProgStartDate) && !string.IsNullOrEmpty(strProgEndDate))
                        {
                            strProgStartDate = strProgStartDate.Substring(0, strProgStartDate.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                            strProgEndDate = strProgEndDate.Substring(0, strProgEndDate.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                            string[] strProgStartDateSplit = strProgStartDate.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                            string[] strProgEndDateSplit = strProgEndDate.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                            for (int i = 0; i <= strProgStartDateSplit.GetUpperBound(0); i++)
                            {
                                progStartDate = Convert.ToDateTime(clsMis.formatDateDDMMtoMMDD(strProgStartDateSplit[i]));
                                progEndDate = Convert.ToDateTime(clsMis.formatDateDDMMtoMMDD(strProgEndDateSplit[i]));

                                intRecordAffected = prog.addProgramDate(progId, progStartDate, progEndDate);

                                if (intRecordAffected == 1)
                                {
                                    boolEdited = true;
                                }
                                else
                                {
                                    boolSuccess = false;
                                    break;
                                }
                            }
                        }
                    }

                    if (boolSuccess && !prog.isExactSameDataSet(progId, progName, progNameZh, progSnapshot, progSnapshotzh, progDesc, progDescZh, progImage, progFee, progPublishDate, progTotalParticipant, progNoOfHours, progActive, progOnlineReg, progArea))
                    {
                        intRecordAffected = prog.updateProgramById(progId, progName, progNameZh, progSnapshot, progSnapshotzh, progDesc, progDescZh, progImage, progFee, progPublishDate, progTotalParticipant, progNoOfHours, progActive, progOnlineReg, progArea, Convert.ToInt16(Session["ADMID"]));

                        if (intRecordAffected == 1)
                        {
                            if (hdnProgImageRef.Value != progImage && hdnProgImageRef.Value != "")
                            {
                                string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPROGRAMFOLDER.ToString() + "/";
                                if (File.Exists(uploadPath + hdnProgImageRef.Value)) { File.Delete(uploadPath + hdnProgImageRef.Value); }
                            }

                            boolEdited = true;
                        }
                        else
                        {
                            boolSuccess = false;
                        }
                    }

                    if (boolEdited)
                    {
                        Session["EDITEDPROGRAMID"] = prog.programId;
                        Response.Redirect(Request.Url.ToString());
                    }
                    else
                    {
                        if (!boolSuccess)
                        {
                            prog.extractProgramById(progId);
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit program (" + prog.programName + "). Please try again.</div>";

                            Response.Redirect(Request.Url.ToString());
                        }
                        else
                        {
                            Session["EDITEDPROGRAMID"] = progId;
                            Session["NOCHANGE"] = 1;

                            Response.Redirect(Request.Url.ToString());
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        registerCKEditorScript();
        registerScript();

        if (!IsPostBack)
        {
            setPageProperties();
        }

        populateFileUpload();
    }

    public void registerScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("FORMSECT3")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "initSect3();";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "FORMSECT3", strJS, false);
        }
    }

    protected void registerCKEditorScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">" +
                    "CKEDITOR.replace('" + txtProgDesc.ClientID + "'," +
                    "{" +
                    "width:'99%'," +
                    "height:'300'," +
                    "toolbar: 'User3',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Files'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Images'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Flash'" +
                     "}" +
                     ");" +
                     "CKEDITOR.replace('" + txtProgDescZh.ClientID + "'," +
                     "{" +
                     "width:'99%'," +
                     "height:'300'," +
                     "toolbar: 'User3',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Files'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Images'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Flash'" +
                     "}" +
                     ");";

            strJS += "CKEDITOR.add;" +
                     "CKEDITOR.config.contentsCss = '" + System.Configuration.ConfigurationManager.AppSettings["cssBase"] + clsAdmin.CONSTADMCMSCSS + "';";
            strJS += "CKEDITOR.config.stylesCombo_stylesSet= '" + clsAdmin.CONSTADMSTYLESET + ":" + System.Configuration.ConfigurationManager.AppSettings["jsBase"] + clsAdmin.CONSTADMSTYLESETFILE + "';" +
                     "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR", strJS, false);
        }
    }

    protected void setPageProperties()
    {
        txtProgPublishDate.Text = clsMis.formatDateMMDDtoDDMM(DateTime.Now.ToShortDateString());

        clsProgram prog = new clsProgram();
        DataSet dsGroup = new DataSet();
        DataView dvGroup = new DataView();
        dsGroup = prog.getProgGrpList(1);
        dvGroup = new DataView(dsGroup.Tables[0]);
        dvGroup.Sort = "PROGGRP_NAME ASC";

        ddlCat.DataSource = dvGroup;
        ddlCat.DataTextField = "PROGGRP_DNAME";
        ddlCat.DataValueField = "PROGGRP_ID";
        ddlCat.DataBind();

        ListItem ddlCatDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlCat.Items.Insert(0, ddlCatDefaultItem);

        DataSet dsArea = new DataSet();
        clsMis mis = new clsMis();
        dsArea = mis.getListByListGrp("PROGRAM AREA", 1);

        DataView dvArea = new DataView(dsArea.Tables[0]);
        dvArea = new DataView(dsArea.Tables[0]);
        dvArea.Sort = "LIST_ORDER ASC";

        ddlProgArea.DataSource = dvArea;
        ddlProgArea.DataTextField = "LIST_NAME";
        ddlProgArea.DataValueField = "LIST_VALUE";
        ddlProgArea.DataBind();

        ListItem ddlAreaDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlProgArea.Items.Insert(0, ddlAreaDefaultItem);

        int intImgMaxSize = 0;

        if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
        {
            if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
            {
                intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
            }
            else
            {
                if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                {
                    if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                    {
                        intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                    }
                }
            }
        }

        //litUploadRule.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH, "kb");
        litUploadRule.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb");

        lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";

        switch (mode)
        {
            case 1:
                break;
            case 2:
                lnkbtnDelete.Visible = true;
                lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteProgram.Text").ToString() + "');";

                if (Session["DELETEPROGRAMNAME"] == null) { fillForm(); }
                break;
            default:
                break;
        }
    }

    protected void fillForm()
    {
        clsProgram prog = new clsProgram();

        if (prog.extractProgramById(progId))
        {
            if (prog.deleted == 0)
            {
                txtProgName.Text = prog.programName;
                txtProgNameZh.Text = prog.programNameZh;
                txtProgSnapshot.Text = prog.programSnapshot;
                txtProgSnapshotZh.Text = prog.programSnapshotZh;
                txtProgDesc.Text = prog.programDesc;
                txtProgDescZh.Text = prog.programDescZh;
                txtProgFee.Text = prog.programFee.ToString();
                txtProgTotalParticipant.Text = prog.programParticipant.ToString();
                txtProgNoOfHours.Text = prog.programNoOfHours.ToString();

                if (prog.programPublishDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
                {
                    txtProgPublishDate.Text = clsMis.formatDateDDMMtoMMDD(prog.programPublishDate.ToString());
                }
                else
                {
                    txtProgPublishDate.Text = "";
                }

                if (prog.programActive != 0) { chkboxProgActive.Checked = true; }
                else { chkboxProgActive.Checked = false; }

                if (prog.programOnlineRegistration != 0) { chkboxOnlineReg.Checked = true; }
                else { chkboxOnlineReg.Checked = false; }

                ddlProgArea.SelectedValue = prog.programArea.ToString();

                if (prog.programImage != "")
                {
                    pnlProgImage.Visible = true;
                    imgProgImage.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPROGRAMFOLDER + "/" + prog.programImage + "&w=" + clsAdmin.CONSTADMDEFAULTIMGWIDTH + "&h=" + clsAdmin.CONSTADMDEFAULTIMGHEIGHT + "&f=1";
                    hdnProgImage.Value = prog.programImage;
                    hdnProgImageRef.Value = prog.programImage;
                }

                Session["ORISTARTDATE"] = null;
                Session["ORIENDDATE"] = null;

                DataSet ds = new DataSet();
                ds = prog.getProgramDateListByProgId(progId);

                DataRow[] foundRow = ds.Tables[0].Select("DATE_ID = " + progId, "PROG_STARTDATE ASC");

                foreach (DataRow row in foundRow)
                {
                    Session["ORISTARTDATE"] += row["PROG_STARTDATE"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR;
                    Session["ORIENDDATE"] += row["PROG_ENDDATE"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR;
                }

                DataSet dsProgGroup = new DataSet();
                dsProgGroup = prog.getProgGroupListById();
                DataRow[] foundRowProgGroup;

                foundRowProgGroup = dsProgGroup.Tables[0].Select("PROG_ID = " + progId);
                if (foundRowProgGroup.Length > 0)
                {
                    int intCat1 = int.Parse(foundRowProgGroup[0]["GRP_ID"].ToString());
                    hdnCat.Value = intCat1.ToString();
                    if (intCat1 != 0) { ddlCat.SelectedValue = intCat1.ToString(); }
                }
            }
            else
            {
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
                Response.Redirect(currentPageName);
            }
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }

    protected void oriFileUploadItems_OnDelete(object sender, CommandEventArgs e)
    {
        string strFileUploadItemToDelete = e.CommandArgument.ToString();
        string[] strSplit = strFileUploadItemToDelete.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

        Session["DELSTARTDATE"] += strSplit[0] + clsAdmin.CONSTDEFAULTSEPERATOR;
        Session["DELENDDATE"] += strSplit[1] + clsAdmin.CONSTDEFAULTSEPERATOR;

        populateFileUpload();
    }

    protected void fileUploadItems1_OnDelete(object sender, CommandEventArgs e)
    {
        string strFileUploadItemToDelete = e.CommandArgument.ToString();
        string[] strSplit = strFileUploadItemToDelete.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

        //clsLog.logErroMsg("before delete start: " + Session["STARTDATE"].ToString() + "before delete end: " + Session["ENDDATE"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR + strSplit[0] + clsAdmin.CONSTDEFAULTSEPERATOR + clsAdmin.CONSTDEFAULTSEPERATOR + strSplit[1] + clsAdmin.CONSTDEFAULTSEPERATOR);

        Session["STARTDATE"] = Session["STARTDATE"].ToString().Replace(clsAdmin.CONSTDEFAULTSEPERATOR + strSplit[0] + clsAdmin.CONSTDEFAULTSEPERATOR, clsAdmin.CONSTDEFAULTSEPERATOR.ToString());
        Session["ENDDATE"] = Session["ENDDATE"].ToString().Replace(clsAdmin.CONSTDEFAULTSEPERATOR + strSplit[1] + clsAdmin.CONSTDEFAULTSEPERATOR, clsAdmin.CONSTDEFAULTSEPERATOR.ToString());

        //clsLog.logErroMsg("after delete start: " + Session["STARTDATE"].ToString() + "after delete end: " + Session["ENDDATE"].ToString());

        populateFileUpload();
    }

    protected void populateFileUpload()
    {
        pnlProgramDate.Controls.Clear();
        int intCount = 0;

        try
        {
            string strOriFileTitle = "";
            if (Session["ORISTARTDATE"] != null)
            {
                if (!Session["ORISTARTDATE"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["ORISTARTDATE"] = clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + Session["ORISTARTDATE"]; }
                strOriFileTitle = Session["ORISTARTDATE"].ToString();
            }

            string strOriFile = "";
            if (Session["ORIENDDATE"] != null)
            {
                if (!Session["ORIENDDATE"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["ORIENDDATE"] = clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + Session["ORIENDDATE"]; }
                strOriFile = Session["ORIENDDATE"].ToString();
            }

            string strFileTitle = "";
            if (Session["STARTDATE"] != null)
            {
                if (!Session["STARTDATE"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["STARTDATE"] = clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + Session["STARTDATE"]; }
                strFileTitle = Session["STARTDATE"].ToString();
            }

            string strFile = "";
            if (Session["ENDDATE"] != null)
            {
                if (!Session["ENDDATE"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["ENDDATE"] = clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + Session["ENDDATE"]; }
                strFile = Session["ENDDATE"].ToString();
            }

            string strDelFileUpload = "";
            if (Session["DELSTARTDATE"] != null)
            {
                if (!Session["DELSTARTDATE"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["DELSTARTDATE"] = clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + Session["DELSTARTDATE"]; }
                strDelFileUpload = Session["DELSTARTDATE"].ToString();
            }

            string strDelFile = "";
            if (Session["DELENDDATE"] != null)
            {
                if (!Session["DELENDDATE"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["DELENDDATE"] = clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + Session["DELENDDATE"]; }
                strDelFile = Session["DELENDDATE"].ToString();
            }

            if ((!string.IsNullOrEmpty(strOriFileTitle) || !string.IsNullOrEmpty(strFileTitle)) && (!string.IsNullOrEmpty(strOriFile) || !string.IsNullOrEmpty(strFile)))
            {
                //Response.Write("not null: ori - " + strOriFile + " ori name - " + strOriFileTitle + " file - " + strFile + " file name - " + strFileTitle);
                HtmlTable tbl = new HtmlTable();
                tbl.CellPadding = 0;
                tbl.CellSpacing = 0;
                tbl.Attributes.Add("class", "dataTbl");
                pnlProgramDate.Controls.Add(tbl);

                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cell1 = new HtmlTableCell("th");
                HtmlTableCell cell2 = new HtmlTableCell("th");
                HtmlTableCell cell3 = new HtmlTableCell("th");


                Literal litOpenBrac;

                // for table header
                cell1.Controls.Add(new LiteralControl("No."));
                cell2.Controls.Add(new LiteralControl("Article"));
                cell3.Controls.Add(new LiteralControl("Action"));

                cell1.Attributes["class"] = "tdNo";
                cell2.Attributes["style"] = "text-align:center;";
                cell3.Attributes["style"] = "text-align:center;";

                row.Controls.Add(cell1);
                row.Controls.Add(cell2);
                row.Controls.Add(cell3);
                tbl.Controls.Add(row);

                if (!string.IsNullOrEmpty(strOriFileTitle))
                {
                    strOriFileTitle = strOriFileTitle.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strOriFileTitle.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    strOriFile = strOriFile.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strOriFile.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                }

                if (!string.IsNullOrEmpty(strOriFileTitle))
                {
                    //Response.Write("ori not null");
                    strOriFileTitle = strOriFileTitle.Substring(0, strOriFileTitle.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    strOriFile = strOriFile.Substring(0, strOriFile.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                    LinkButton lnkbtn;
                    string[] fileTitleItems = strOriFileTitle.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    string[] fileItems = strOriFile.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                    for (int i = 0; i <= fileTitleItems.GetUpperBound(0); i++)
                    {
                        if (strDelFileUpload.IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + fileTitleItems[i] + clsAdmin.CONSTDEFAULTSEPERATOR) < 0)
                        {
                            intCount += 1;
                            var span = new HtmlGenericControl("span");
                            span.InnerHtml = (intCount).ToString();
                            cell1 = new HtmlTableCell();
                            cell1.Controls.Add(span);

                            litOpenBrac = new Literal();
                            DateTime dtStartDate = Convert.ToDateTime(fileTitleItems[i]);
                            DateTime dtEndDate = Convert.ToDateTime(fileItems[i]);
                            litOpenBrac.Text = dtStartDate.Day + " " + dtStartDate.ToString("MMM") + " " + dtStartDate.Year + " - " + dtEndDate.Day + " " + dtEndDate.ToString("MMM") + " " + dtEndDate.Year + "&nbsp;&nbsp;&nbsp";
                            cell2 = new HtmlTableCell();
                            cell2.Attributes["style"] = "text-align: center;width:45%;";
                            cell2.Controls.Add(litOpenBrac);

                            lnkbtn = new LinkButton();
                            lnkbtn.CssClass = "lnkbtn lnkbtnDelete";
                            lnkbtn.ID = intCount + "_delete_" + i;
                            lnkbtn.Text = "delete" + "<br/>";
                            lnkbtn.Command += new CommandEventHandler(oriFileUploadItems_OnDelete);
                            lnkbtn.CommandArgument = fileTitleItems[i] + clsAdmin.CONSTDEFAULTSEPERATOR + fileItems[i];
                            lnkbtn.CausesValidation = false;
                            cell3 = new HtmlTableCell();
                            cell3.Attributes["class"] = "tdAct";
                            cell3.Controls.Add(lnkbtn);

                            row = new HtmlTableRow();
                            row.Controls.Add(cell1);
                            row.Controls.Add(cell2);
                            row.Controls.Add(cell3);
                            tbl.Controls.Add(row);
                        }
                    }
                }
                //clsLog.logErroMsg("<br />before: " + strFileTitle + " " + strFile);

                if (!string.IsNullOrEmpty(strFileTitle))
                {
                    strFileTitle = strFileTitle = strFileTitle.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strFileTitle.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    strFile = strFile.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strFile.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                }

                //clsLog.logErroMsg("<br />after: " + strFileTitle + " " + strFile);
                if (!string.IsNullOrEmpty(strFileTitle))
                {
                    strFileTitle = strFileTitle.Substring(0, strFileTitle.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    strFile = strFile.Substring(0, strFile.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                    //clsLog.logErroMsg("<br />during: " + strFileTitle + " " + strFile);

                    LinkButton lnkbtn;
                    string[] fileTitleItems = strFileTitle.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    string[] fileItems = strFile.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                    for (int i = 0; i <= fileTitleItems.GetUpperBound(0); i++)
                    {
                        intCount += 1;
                        var span = new HtmlGenericControl("span");
                        span.InnerHtml = (intCount).ToString();
                        cell1 = new HtmlTableCell();
                        cell1.Controls.Add(span);

                        litOpenBrac = new Literal();
                        DateTime dtStartDate = Convert.ToDateTime(clsMis.formatDateDDMMtoMMDD(fileTitleItems[i]));
                        DateTime dtEndDate = Convert.ToDateTime(clsMis.formatDateDDMMtoMMDD(fileItems[i]));
                        litOpenBrac.Text = dtStartDate.Day + " " + dtStartDate.ToString("MMM") + " " + dtStartDate.Year + " - " + dtEndDate.Day + " " + dtEndDate.ToString("MMM") + " " + dtEndDate.Year + "&nbsp;&nbsp;&nbsp";
                        cell2 = new HtmlTableCell();
                        cell2.Attributes["style"] = "text-align: center;width:45%;";
                        cell2.Controls.Add(litOpenBrac);

                        lnkbtn = new LinkButton();
                        lnkbtn.CssClass = "lnkbtn lnkbtnDelete";
                        lnkbtn.ID = intCount + "_delete_" + i;
                        lnkbtn.Text = "delete" + "<br/>";
                        lnkbtn.Command += new CommandEventHandler(fileUploadItems1_OnDelete);
                        lnkbtn.CommandArgument = fileTitleItems[i] + clsAdmin.CONSTDEFAULTSEPERATOR + fileItems[i];
                        cell3 = new HtmlTableCell();
                        cell3.Attributes["class"] = "tdAct";
                        cell3.Controls.Add(lnkbtn);

                        row = new HtmlTableRow();
                        row.Controls.Add(cell1);
                        row.Controls.Add(cell2);
                        row.Controls.Add(cell3);
                        tbl.Controls.Add(row);
                    }
                }

            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message.ToString());
            throw ex;
        }
    }
    #endregion
}
