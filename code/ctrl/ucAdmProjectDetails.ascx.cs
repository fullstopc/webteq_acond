﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.IO;
using System.Configuration;
using System.Text.RegularExpressions;

public partial class ctrl_ucAdmProjectDetails : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _projId = 0;
    protected int _mode = 1;
    protected string _pageListingURL = "";

    private string strLiGstActive = "liGstActive";
    private string strLiGstNotAvailable = "liGstNotAvailable";
    private string strLiGstInclusive = "liGstInclusive";
    private string strLiGstApplied = "liGstApplied";

    // for slider setting
    private string strLiSliderActive = "liSliderActive";
    private string strLiSliderDefault = "liSliderDefault";
    private string strLiSliderFullScreen = "liSliderFullScreen";
    private string strLiSliderMobile = "liSliderMobile";

    // page authorization setting
    private string strLiAuthUniversalLogin = "liAuthUniversalLogin";
    private string strLiAuthIndividualLogin = "liAuthIndividualLogin";
    #endregion

    #region "Property Method"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int projId
    {
        get
        {
            if (ViewState["PROJID"] == null)
            {
                return _projId;
            }
            else
            {
                return int.Parse(ViewState["PROJID"].ToString());
            }
        }
        set { ViewState["PROJID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void validateName_server(object source, ServerValidateEventArgs args)
    {
        clsProject proj = new clsProject();
        string strName = txtProjName.Text.Trim();
        
        if (!proj.isNameExist(strName, projId))
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
            cvName.ErrorMessage = "<br />This Name is in used.";
        }
    }

    protected void cvName_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("ProjName")))
        {
            string strJS = null;
            strJS = "<script = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtProjName.ClientID + "', document.all['" + cvName.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "ProjName", strJS, false);
        }
    }

    protected void validateImagesSize_server(object source, ServerValidateEventArgs args)
    {
        bool isValid = false;

        if (!string.IsNullOrEmpty(txtImages.Text))
        {
            int intTryParse;
            if (int.TryParse(txtImages.Text.Trim(), out intTryParse))
            {
                Match matchQty = Regex.Match(txtImages.Text.Trim(), clsAdmin.CONSTNUMERICRE);

                if (matchQty.Success && (intTryParse >= 0)) { isValid = true; }
                else { isValid = false; }
            }
        }

        if (isValid)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
            cvImagesSize.ErrorMessage = "Please insert valid value";
        }
    }

    protected void validateFileSize_server(object source, ServerValidateEventArgs args)
    {
        bool isValid = false;

        if (!string.IsNullOrEmpty(txtFile.Text))
        {
            int intTryParse;
            if (int.TryParse(txtFile.Text.Trim(), out intTryParse))
            {
                Match matchQty = Regex.Match(txtFile.Text.Trim(), clsAdmin.CONSTNUMERICRE);

                if (matchQty.Success && (intTryParse >= 0)) { isValid = true; }
                else { isValid = false; }
            }
        }

        if (isValid)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
            cvFileSize.ErrorMessage = "Please insert valid value";
        }
    }

    protected void validateCkeditorFileSize_server(object source, ServerValidateEventArgs args)
    {
        bool isValid = false;

        if (!string.IsNullOrEmpty(txtCkeditorFile.Text))
        {
            int intTryParse;
            if (int.TryParse(txtCkeditorFile.Text.Trim(), out intTryParse))
            {
                Match matchQty = Regex.Match(txtCkeditorFile.Text.Trim(), clsAdmin.CONSTNUMERICRE);

                if (matchQty.Success && (intTryParse >= 0)) { isValid = true; }
                else { isValid = false; }
            }
        }

        if (isValid)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
            cvCkeditorFileSize.ErrorMessage = "Please insert valid value";
        }
    }

    protected void validateCkeditorImagesSize_server(object source, ServerValidateEventArgs args)
    {
        bool isValid = false;

        if (!string.IsNullOrEmpty(txtCkeditorImage.Text))
        {
            int intTryParse;
            if (int.TryParse(txtCkeditorImage.Text.Trim(), out intTryParse))
            {
                Match matchQty = Regex.Match(txtCkeditorImage.Text.Trim(), clsAdmin.CONSTNUMERICRE);

                if (matchQty.Success && (intTryParse >= 0)) { isValid = true; }
                else { isValid = false; }
            }
        }

        if (isValid)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
            cvCkeditorImagesSize.ErrorMessage = "Please insert valid value";
        }
    }

    protected void validateDefaultSize_server(object source, ServerValidateEventArgs args)
    {
        bool isValid = false;

        if (!string.IsNullOrEmpty(txtDefault.Text))
        {
            int intTryParse;
            if (int.TryParse(txtDefault.Text.Trim(), out intTryParse))
            {
                Match matchQty = Regex.Match(txtDefault.Text.Trim(), clsAdmin.CONSTNUMERICRE);

                if (matchQty.Success && (intTryParse >= 0)) { isValid = true; }
                else { isValid = false; }
            }
        }

        if (isValid)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
            cvDefaultSize.ErrorMessage = "Please insert valid value";
        }
    }

    protected void validateEventThumbSize_server(object source, ServerValidateEventArgs args)
    {
        bool isValid = false;

        if (!string.IsNullOrEmpty(txtEventThumb.Text))
        {
            int intTryParse;
            if (int.TryParse(txtEventThumb.Text.Trim(), out intTryParse))
            {
                Match matchQty = Regex.Match(txtEventThumb.Text.Trim(), clsAdmin.CONSTNUMERICRE);

                if (matchQty.Success && (intTryParse >= 0)) { isValid = true; }
                else { isValid = false; }
            }
        }

        if (isValid)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
            cvEventThumbSize.ErrorMessage = "Please insert valid value";
        }
    }

    protected void validateventGallSize_server(object source, ServerValidateEventArgs args)
    {
        bool isValid = false;

        if (!string.IsNullOrEmpty(txtEventGallery.Text))
        {
            int intTryParse;
            if (int.TryParse(txtEventGallery.Text.Trim(), out intTryParse))
            {
                Match matchQty = Regex.Match(txtEventGallery.Text.Trim(), clsAdmin.CONSTNUMERICRE);

                if (matchQty.Success && (intTryParse >= 0)) { isValid = true; }
                else { isValid = false; }
            }
        }

        if (isValid)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
            cvEventGallSize.ErrorMessage = "Please insert valid value";
        }
    }

    protected void validateProdThumbSize_server(object source, ServerValidateEventArgs args)
    {
        bool isValid = false;

        if (!string.IsNullOrEmpty(txtProductThumb.Text))
        {
            int intTryParse;
            if (int.TryParse(txtProductThumb.Text.Trim(), out intTryParse))
            {
                Match matchQty = Regex.Match(txtProductThumb.Text.Trim(), clsAdmin.CONSTNUMERICRE);

                if (matchQty.Success && (intTryParse >= 0)) { isValid = true; }
                else { isValid = false; }
            }
        }

        if (isValid)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
            cvProdThumbSize.ErrorMessage = "Please insert valid value";
        }
    }

    protected void validateProdGallSize_server(object source, ServerValidateEventArgs args)
    {
        bool isValid = false;

        if (!string.IsNullOrEmpty(txtProductGallery.Text))
        {
            int intTryParse;
            if (int.TryParse(txtProductGallery.Text.Trim(), out intTryParse))
            {
                Match matchQty = Regex.Match(txtProductGallery.Text.Trim(), clsAdmin.CONSTNUMERICRE);

                if (matchQty.Success && (intTryParse >= 0)) { isValid = true; }
                else { isValid = false; }
            }
        }

        if (isValid)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
            cvProdGallSize.ErrorMessage = "Please insert valid value";
        }
    }

    protected void lnkbtnAddLang_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (!string.IsNullOrEmpty(ddlLang.SelectedValue))
            {
                string strItem = ddlLang.SelectedItem.Text + clsAdmin.CONSTCOMMASEPARATOR.ToString() + ddlLang.SelectedValue + clsAdmin.CONSTDEFAULTSEPERATOR.ToString();
                Boolean boolFound = false;
                
                if (Session["LANG"] != null && Session["LANG"] != "")
                {
                    boolFound = Session["LANG"].ToString().IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + strItem) >= 0;
                }

                if (!boolFound && Session["DELLANG"] != null && Session["DELLANG"] != "")
                {
                    boolFound = Session["DELLANG"].ToString().IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + strItem) >= 0;
                    // if found, remove from Deleted
                    if (boolFound)
                    {
                        Session["DELLANG"] = Session["DELLANG"].ToString().Replace(clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + strItem, clsAdmin.CONSTDEFAULTSEPERATOR.ToString());
                    }
                }

                if (!boolFound && Session["ORILANG"] != null && Session["ORILANG"] != "")
                {
                    boolFound = Session["ORILANG"].ToString().IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + strItem) >= 0;
                }

                if (!boolFound)
                {
                    Session["LANG"] += strItem;
                    populateLang();
                }

                ddlLang.SelectedValue = "";
            }
        }
    }

    protected void oriLang_OnDelete(object sender, CommandEventArgs e)
    {
        string strDelItem = e.CommandArgument.ToString();
        Session["DELLANG"] += strDelItem + clsAdmin.CONSTDEFAULTSEPERATOR.ToString();

        populateLang();
    }

    protected void lang_OnDelete(object sender, CommandEventArgs e)
    {
        string strDelItem = e.CommandArgument.ToString();
        Session["LANG"] = Session["LANG"].ToString().Replace(clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + strDelItem + clsAdmin.CONSTDEFAULTSEPERATOR.ToString(), clsAdmin.CONSTDEFAULTSEPERATOR.ToString());

        populateLang();
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strName = txtProjName.Text.Trim();
            int intDriver = 0;
            string strServer = txtProjServer.Text.Trim();
            string strUserId = txtProjUserId.Text.Trim();
            string strPwd = txtProjPwd.Text.Trim();
            string strDatabase = txtProjDatabase.Text.Trim();
            string strOption = txtProjOption.Text.Trim();
            string strPort = txtProjPort.Text.Trim();
            string strUploadPath = txtUploadPath.Text.Trim();
            string strWebsite = txtWebsite.Text.Trim();
            int intGroupParentChild = 0;
            int intGroupDesc = 0;
            int intFAQ = 0;
            int intProgManager = 0;
            int intEventManager = 0;
            int intProdDesc = 0;
            int intProdGall = 0;
            int intCouponManager = 0;
            int intMultipleCurr = 0;
            int intInventory = chkboxInv.Checked ? 1 : 0; // add by sh.chong 02Mar2015
            int intMobileView = 0; // add by subi 12May2015
            int intFbLogin = 0; // add by sh.chong 18May2015
            int intWatermark = 0; // add by jue 07Jul2015
            int intFriendlyURL = 0; // add by jue 07Jul2015
            int intGroupMenu = 0; // add by jue 11Aug2015
            int intTraining = 0; // add by subi 17Aug2015
            int intDeliveryMsg = 0; // add by jue 04Jan2016
            int intProdPromo = 0; //add by sh.chong 18Feb2016
            int intAuthSetting = 0; // add by subi 6May2016
            int intRightClick = chkboxRightClick.Checked ? 1 : 0; // add by sh.chong 12Aug2016
            int intWebteqLogo = chkboxWebteqLogo.Checked ? 1 : 0; // add by sh.chong 12Aug2016
            int intCkeditorTopmenu = chkboxCkeditorTopmenu.Checked ? 1 : 0; // add by sh.chong 12Aug2016
            int intSMSSetting = chkboxSMSSetting.Checked ? 1 : 0; // add by lmhow 03102016
            int intDeliveryDatetime = chkboxDeliveryDatetime.Checked ? 1 : 0; // ad by sh.chong 28Aug2016
            int intProdAddon = chkboxProdAddon.Checked ? 1 : 0; // add by sh.chong 1Nov2016
            int intActive = chkboxActive.Checked ? 1 : 0;

            //top menu root
            int intTopMenuRoot = 0;
            if (!string.IsNullOrEmpty(ddlRootMenu.SelectedValue)) { intTopMenuRoot = int.Parse(ddlRootMenu.SelectedValue); }
            //end top menu root

            //Gst
            int intGstActive = 0;
            int intGstNotAvailable = 1;
            int intGstInclusive = 0;
            int intGstApplied = 0;
            //Gst

            // slider setting - add by sh.chong 18 sep 2015
            int intSliderActive = 0;
            int intSliderDefault = 0;
            int intSliderFullScreen = 0;
            int intSliderMobile = 0;

            int intAuthUniversalLogin = 0;
            int intIndividualLogin = 0;

            int intType = 0;
            if (!string.IsNullOrEmpty(ddlType.SelectedValue)) { intType = int.Parse(ddlType.SelectedValue); }

            if (!string.IsNullOrEmpty(ddlProjDriver.SelectedValue)) { intDriver = Convert.ToInt16(ddlProjDriver.SelectedValue); }

            if (chkboxGroupMenu.Checked) { intGroupMenu = 1; }
            if (chkGroupParentChild.Checked) { intGroupParentChild = 1; }
            if (chkboxGroupDesc.Checked) { intGroupDesc = 1; }
            if (chkboxFAQ.Checked) { intFAQ = 1; }
            if (chkboxProgManager.Checked) { intProgManager = 1; }
            if (chkboxEventManager.Checked) { intEventManager = 1; }
            if (chkboxProdDesc.Checked) { intProdDesc = 1; }
            if (chkboxProdGall.Checked) { intProdGall = 1; }
            if (chkboxCouponManager.Checked) { intCouponManager = 1; }
            if (chkboxMultipleCurr.Checked) { intMultipleCurr = 1; } 

            int intInvType = 0;
            if (chkboxInv.Checked)
            {
                if (!string.IsNullOrEmpty(ddlInv.SelectedValue)) { intInvType = Convert.ToInt16(ddlInv.SelectedValue); } // add by subi 31Jul2015
            }

            if (chkboxMobileView.Checked) { intMobileView = 1; } // add by subi 12May2015
            if (chkboxFbLogin.Checked) { intFbLogin = 1; } // add by sh.chong 18Mar2015
            if (chkboxWatermark.Checked) { intWatermark = 1; } // add by jue 07Jul2015
            if (chkboxFriendlyURL.Checked) { intFriendlyURL = 1; } // add by jue 07Jul2015
            if (chkboxTraining.Checked) { intTraining = 1; } // add by subi 17Aug2015
            if (chkboxDeliveryMsg.Checked) { intDeliveryMsg = 1; } // add by jue 04Jan2016
            if (chkboxProdPromo.Checked) { intProdPromo = 1; } // add by sh.chong 18Feb2016
            
            // start of GST
            //if (chklistGst.Items.FindByValue(strLiGstNotAvailable).Selected) { intGstNotAvailable = 1; }
            if (chklistGst.Items.FindByValue(strLiGstInclusive).Selected) { intGstInclusive = 1; }
            if (chklistGst.Items.FindByValue(strLiGstApplied).Selected) { intGstApplied = 1; }

            // if gst inactive, all setup should leave it default. 
            if (chklistGst.Items.FindByValue(strLiGstActive).Selected) { intGstActive = 1; }
            else {
                intGstInclusive = 0;
                intGstApplied = 0;
            }
            // end of GST

            // start of Slider - sh.chong 18 Sep 2015
            if (chklistSlider.Items.FindByValue(strLiSliderDefault).Selected) { intSliderDefault = 1; }
            if (chklistSlider.Items.FindByValue(strLiSliderFullScreen).Selected) { intSliderFullScreen = 1; }
            if (chklistSlider.Items.FindByValue(strLiSliderMobile).Selected) { intSliderMobile = 1; }
            if (chklistSlider.Items.FindByValue(strLiSliderActive).Selected) { intSliderActive = 1; }
            else { intSliderDefault = 0; intSliderFullScreen = 0; }
            // end of Slider

            // start page authorization -  subi 6 May 2016
            if (chkboxPgAuthSetting.Checked)
            {
                intAuthSetting = 1;
                
                if (rdoListLogin.Items.FindByValue(strLiAuthUniversalLogin).Selected) { intAuthUniversalLogin = 1; }
                if (rdoListLogin.Items.FindByValue(strLiAuthIndividualLogin).Selected) { intIndividualLogin = 1; }
            }
            
            // end of page authorization

            //File Upload Settings
            int intImagesSetting = Convert.ToInt32(txtImages.Text.Trim());
            int intFileSetting = Convert.ToInt32(txtFile.Text.Trim());
            int intCkeditorFileSetting = Convert.ToInt32(txtCkeditorFile.Text.Trim());
            int intCkeditorImagesSetting = Convert.ToInt32(txtCkeditorImage.Text.Trim());
            int intDefaultSetting = Convert.ToInt32(txtDefault.Text.Trim());
            int intEventThumbSetting = Convert.ToInt32(txtEventThumb.Text.Trim());
            int intEventGallSetting = Convert.ToInt32(txtEventGallery.Text.Trim());
            int intProdThumbSetting = Convert.ToInt32(txtProductThumb.Text.Trim());
            int intProdGallSetting = Convert.ToInt32(txtProductGallery.Text.Trim());
            //end of File Upload Settings

            //=============== Facebook Catalog 6 Jan 2017 =========================
            int intFbCatalogActive = chkboxFbCatalogActive.Checked ? 1 : 0;
            DateTime datetimeFbCatalogExpired = (!string.IsNullOrEmpty(txtFbCatalogExpired.Text)) ? Convert.ToDateTime(txtFbCatalogExpired.Text) : DateTime.MinValue;

            // ================ Enquiry Setting 6 Jan 2017 =====================
            int intEnquiryField = chkboxEnqField.Checked ? 1 : 0;

            int intFullPath = 0;
            string strFullPath = txtFullPath.Text.Trim();

            if (chkFullPath.Checked)
            {
                intFullPath = 1;
                if (strFullPath.Contains("http://") == false)
                {
                    strFullPath = strFullPath.Insert(0, "http://");
                }
            }
            else
            {
                strFullPath = "";
            }

            
            int intRecordAffected = 0;
            bool boolAdded = false;
            bool boolEdited = false;
            bool boolSuccess = true;

            clsProject input = new clsProject();
            input.projName = strName;
            input.projDriver = intDriver;
            input.projServer = strServer;
            input.projUserId = strUserId;
            input.projPwd = strPwd;
            input.projDatabase = strDatabase;
            input.projOption = strOption;
            input.projPort = strPort;
            input.projUploadPath = strUploadPath;
            input.projUserviewUrl = strWebsite;
            input.projType = intType;
            input.projProgManager = intProgManager;
            input.projEventManager = intEventManager;
            input.projProdDesc = intProdDesc;
            input.projProdGall = intProdGall;
            input.projCouponManager = intCouponManager;
            input.projCouponManagerPackage = Convert.ToInt16(ddlCouponManagerPackage.SelectedValue);
            input.projMultipleCurr = intMultipleCurr;
            input.projInventory = intInventory;
            input.projInventoryType = intInvType;
            input.projMobileView = intMobileView;
            input.projFbLogin = intFbLogin;
            input.projWatermark = intWatermark;
            input.projFriendlyURL = intFriendlyURL;
            input.projGst = intGstActive;
            input.projGstNotAvailable = intGstNotAvailable;
            input.projGstInclusive = intGstInclusive;
            input.projGstApplied = intGstApplied;
            input.projActive = intActive;
            input.projCreatedBy = int.Parse(Session["ADMID"].ToString());
            input.projUpdatedBy = int.Parse(Session["ADMID"].ToString());
            input.projFullPath = intFullPath;
            input.projFullPathValue = strFullPath;
            input.projGroupParentChild = intGroupParentChild;
            input.projGroupDesc = intGroupDesc;
            input.projFAQ = intFAQ;
            input.projGroupMenu = intGroupMenu;
            input.projImgSetting = intImagesSetting;
            input.projFileSetting = intFileSetting;
            input.projCkeditorFileSetting = intCkeditorFileSetting;
            input.projCkeditoImgSetting = intCkeditorImagesSetting;
            input.projDefaultSetting = intDefaultSetting;
            input.projEventThumbSetting = intEventThumbSetting;
            input.projEventGallSetting = intEventGallSetting;
            input.projProdThumbSetting = intProdThumbSetting;
            input.projProdGallSetting = intProdGallSetting;
            input.projTraining = intTraining;
            input.projSliderActive = intSliderActive;
            input.projSliderDefault = intSliderDefault;
            input.projSliderFullScreen = intSliderFullScreen;
            input.projSliderMobile = intSliderMobile;
            input.projDeliveryMsg = intDeliveryMsg;
            input.projProdPromo = intProdPromo;
            input.projPgAuthorization = intAuthSetting;
            input.projUniversalLogin = intAuthUniversalLogin;
            input.projIndividualLogin = intIndividualLogin;
            input.projRightClick = intRightClick;
            input.projWebteqLogo = intWebteqLogo;
            input.projCkeditorTopmenu = intCkeditorTopmenu;
            input.projSMSSetting = intSMSSetting;
            input.projTopmenuRoot = intTopMenuRoot;
            input.projDeliveryDatetime = intDeliveryDatetime;
            input.projProdAddon = intProdAddon;
            input.projFbCatalogActive = intFbCatalogActive;
            input.projFbCatalogExpired = datetimeFbCatalogExpired;
            input.projEnquiryField = intEnquiryField;

            clsProject proj = new clsProject();
            if (mode == 1)
            {
                // each row got 5 data, please dont more than that.
                intRecordAffected = proj.addProject(input);

                if (intRecordAffected == 1)
                {
                    projId = proj.projId;
                    boolAdded = true;
                }
                else { boolSuccess = false; }
            }
            else if (mode == 2)
            {
                input.projId = projId;

                // each row got 5 data, please dont more than that.
                if (!proj.isExactSameSetProject(input))
                {
                    intRecordAffected = proj.updateProjectById(input);

                    if (intRecordAffected == 1)
                    {
                        boolEdited = true;
                    }
                    else
                    {
                        boolSuccess = false;
                    }
                }
            }

            if (boolSuccess && Session["DELLANG"] != null && Session["DELLANG"] != "")
            {
                string strDelLang = Session["DELLANG"].ToString();
                strDelLang = strDelLang.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strDelLang.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                if (!string.IsNullOrEmpty(strDelLang)) { strDelLang = strDelLang.Substring(0, strDelLang.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length); }

                if (!string.IsNullOrEmpty(strDelLang))
                {
                    string[] strItemsSplit = strDelLang.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    for (int i = 0; i <= strItemsSplit.GetUpperBound(0); i++)
                    {
                        string[] strItemItemSplit = strItemsSplit[i].Split(clsAdmin.CONSTCOMMASEPARATOR);
                        
                        intRecordAffected = proj.deleteLanguage(projId, strItemItemSplit[1]);
                        if (intRecordAffected == 1) { boolEdited = true; }
                        else
                        {
                            boolSuccess = false;
                            break;
                        }
                    }
                }
            }

            if (boolSuccess && Session["LANG"] != null && Session["LANG"] != "")
            {
                string strLang = Session["LANG"].ToString();
                strLang = strLang.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strLang.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                if (!string.IsNullOrEmpty(strLang)) { strLang = strLang.Substring(0, strLang.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length); }

                if (!string.IsNullOrEmpty(strLang))
                {
                    string[] strItemsSplit = strLang.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    for (int i = 0; i <= strItemsSplit.GetUpperBound(0); i++)
                    {
                        string[] strItemItemSplit = strItemsSplit[i].Split(clsAdmin.CONSTCOMMASEPARATOR);

                        intRecordAffected = proj.addLanguage(projId, strItemItemSplit[1]);
                        if (intRecordAffected == 1) { boolEdited = true; }
                        else
                        {
                            boolSuccess = false;
                            break;
                        }
                    }
                }
            }
            //add by sh.chong 13 May 2015 - payment gateway
            if (boolSuccess)
            {
                DataSet dsPaymentGateway = proj.getPaymentGatewayList(projId);

                foreach (ListItem item in chklistPaymentGateway.Items)
                {
                    int intPgActive = 0;
                    if (item.Selected) { intPgActive = 1; }
                    DataRow[] dwPaymentGateway = dsPaymentGateway.Tables[0].Select("PG_NAME = '"+item.Value+"'");
                    
                    // if found row, update data, else add data
                    if (dwPaymentGateway.Count() > 0)
                    {
                        // if active data not same , update data, else no changed detected. 
                        if (int.Parse(dwPaymentGateway[0]["PG_ACTIVE"].ToString()) != intPgActive)
                        {
                            intRecordAffected = proj.updatePaymentGateway(projId, item.Value, intPgActive, int.Parse(Session["ADMID"].ToString()));
                            if (intRecordAffected == 1) { boolEdited = true; }
                            else
                            {
                                boolSuccess = false;
                                break;
                            }
                            boolEdited = true;
                        }
                    }
                    else
                    {
                        intRecordAffected = proj.addPaymentGateway(projId, item.Value, intPgActive, int.Parse(Session["ADMID"].ToString()));
                        if (intRecordAffected == 1) { boolEdited = true; }
                        else
                        {
                            boolSuccess = false;
                            break;
                        }
                        boolEdited = true;
                    }

                }
            }
            //end by sh.chong 13 May 2015 - payment gateway

            if (mode == 1)
            {
                if (boolAdded)
                {
                    Session["NEWPROJECTID"] = proj.projId;

                    Response.Redirect(currentPageName + "?id=" + proj.projId);
                }
                else
                {
                    if (!boolSuccess)
                    {
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new Project. Please try again.</div>";

                        Response.Redirect(currentPageName);
                    }
                }
            }
            else if (mode == 2)
            {
                if (boolEdited)
                {
                    Session["EDITEDPROJECTID"] = projId;

                    Response.Redirect(Request.Url.ToString());
                }
                else
                {
                    if (boolSuccess)
                    {
                        Session["EDITEDPROJECTID"] = projId;
                        Session["NOCHANGE"] = 1;

                        Response.Redirect(Request.Url.ToString());
                    }
                    else
                    {
                        proj.extractProjectById(projId, 0);

                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit Project (" + proj.projName + "). Please try again.</div>";

                        Response.Redirect(currentPageName);
                    }
                }
            }
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string strDeletedProjName = "";
        clsMis.performDeleteProject(projId, ref strDeletedProjName);

        Session["DELETEDPROJECTNAME"] = strDeletedProjName;
        Response.Redirect(currentPageName);
    }

    protected void chkboxInv_CheckedChanged(object sender, EventArgs e)
    {
        if (chkboxInv.Checked)
        {
            ddlInv.Visible = true;

        }
        else
        {
            ddlInv.Visible = false;
        }
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }

        populateLang();
    }

    protected void setPageProperties()
    {
        clsMis mis = new clsMis();
        DataSet ds = new DataSet();
        ds = mis.getListByListGrp("DRIVER TYPE", 0);
        DataView dv = new DataView(ds.Tables[0]);

        ddlProjDriver.DataSource = dv;
        ddlProjDriver.DataTextField = "LIST_NAME";
        ddlProjDriver.DataValueField = "LIST_VALUE";
        ddlProjDriver.DataBind();

        ListItem ddlProjDriverDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlProjDriver.Items.Insert(0, ddlProjDriverDefaultItem);

        ds = new DataSet();
        ds = mis.getListByListGrp("LANGUAGE", 1);
        dv = new DataView(ds.Tables[0]);

        ddlLang.DataSource = dv;
        ddlLang.DataTextField = "LIST_NAME";
        ddlLang.DataValueField = "LIST_VALUE";
        ddlLang.DataBind();

        ListItem ddlLangDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlLang.Items.Insert(0, ddlLangDefaultItem);

        clsProject proj = new clsProject();
        ds = new DataSet();
        ds = proj.getTypeList(1);

        dv = new DataView(ds.Tables[0]);
        dv.Sort = "TYPE_ORDER ASC";

        ddlType.DataSource = dv;
        ddlType.DataTextField = "TYPE_NAME";
        ddlType.DataValueField = "TYPE_ID";
        ddlType.DataBind();

        ListItem ddlTypeDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlType.Items.Insert(0, ddlTypeDefaultItem);

        //add by jue 3 Oct 2016 - top menu root
        ds = new DataSet();
        ds = mis.getListByListGrp("TOP MENU ROOT", 1);
        dv = new DataView(ds.Tables[0]);

        ddlRootMenu.DataSource = dv;
        ddlRootMenu.DataTextField = "LIST_VALUE";
        ddlRootMenu.DataValueField = "LIST_VALUE";
        ddlRootMenu.DataBind();
        //end by jue 3 Oct 2016 - top menu root

        //add by sh.chong 12 May 2015 - payment gateway
        DataView dvPaymentGateway = new DataView(mis.getListByListGrp("PAYMENT TYPE NAME", 1).Tables[0]);
        chklistPaymentGateway.DataSource = dvPaymentGateway;
        chklistPaymentGateway.DataTextField = "LIST_NAME";
        chklistPaymentGateway.DataValueField = "LIST_VALUE";
        chklistPaymentGateway.DataBind();
        //end by sh.chong 12 May 2015 - payment gateway

        // add by SH.Chong 20 Feb 2017 - Coupon Manager Package
        DataSet dsCouponManagerPackage = mis.getListByListGrp("COUPON MANAGER PACKAGE");
        ddlCouponManagerPackage.DataSource = dsCouponManagerPackage.Tables[0];
        ddlCouponManagerPackage.DataTextField = "LIST_NAME";
        ddlCouponManagerPackage.DataValueField = "LIST_VALUE";
        ddlCouponManagerPackage.DataBind();

        initChkboxListGst(); //add by sh.chong 20 May 2015 - gst
        initChkboxListSlider(); // add by sh.chong 18 Sep 2015 - Slider
        initChkboxListPgAuthorization(); // add by Subi 6 May 2016 - Page Authorization

        ds = new DataSet();
        ds = mis.getListByListGrp("INVENTORY", 1);
        dv = new DataView(ds.Tables[0]);

        ddlInv.DataSource = dv;
        ddlInv.DataTextField = "LIST_NAME";
        ddlInv.DataValueField = "LIST_VALUE";
        ddlInv.DataBind();

        ListItem ddlInvDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlInv.Items.Insert(0, ddlInvDefaultItem);

        lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";

        switch (mode)
        {
            case 1:
                fillForm2();
                break;
            case 2:
                lnkbtnDelete.Visible = true;
                lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteProject.Text").ToString() + "'); return false;";

                if (projId > 0) { fillForm(); }
                break;
        }
    }

    protected void fillForm()
    {
        clsProject proj = new clsProject();

        if (proj.extractProjectById(projId, 0))
        {
            txtProjName.Text = proj.projName;
            txtProjServer.Text = proj.projServer;
            txtProjUserId.Text = proj.projUserId;
            txtProjPwd.Text = proj.projPwd;
            txtProjDatabase.Text = proj.projDatabase;
            txtProjOption.Text = proj.projOption;
            txtProjPort.Text = proj.projPort;
            txtUploadPath.Text = proj.projUploadPath;
            txtWebsite.Text = proj.projUserviewUrl;
            txtFullPath.Text = proj.projFullPathValue;

            ddlCouponManagerPackage.SelectedValue = proj.projCouponManagerPackage.ToString();

            if (proj.projGroupMenu != 0) { chkboxGroupMenu.Checked = true; }
            else { chkboxGroupMenu.Checked = false; }
            if (proj.projGroupParentChild != 0) { chkGroupParentChild.Checked = true; }
            if (proj.projGroupDesc != 0) { chkboxGroupDesc.Checked = true; }
            if (proj.projFullPath != 0) { chkFullPath.Checked = true; }

            if (proj.projType != 0)
            {
                ddlType.SelectedValue = proj.projType.ToString();
            }

            if (proj.projDriver != 0)
            {
                ddlProjDriver.SelectedValue = proj.projDriver.ToString();
            }

            if (proj.projTopmenuRoot != 0)
            {
                ddlRootMenu.SelectedValue = proj.projTopmenuRoot.ToString();
            }

            chkboxFAQ.Checked = proj.projFAQ != 0;
            chkboxProgManager.Checked = proj.projProgManager != 0;
            chkboxEventManager.Checked = proj.projEventManager != 0;
            chkboxActive.Checked = proj.projActive != 0;
            chkboxProdDesc.Checked = proj.projProdDesc != 0;
            chkboxProdGall.Checked = proj.projProdGall != 0;
            chkboxCouponManager.Checked = proj.projCouponManager != 0;
            chkboxMultipleCurr.Checked = proj.projMultipleCurr != 0;
            chkboxInv.Checked = proj.projInventory != 0;
            chkboxMobileView.Checked = proj.projMobileView != 0;
            chkboxFbLogin.Checked = proj.projFbLogin != 0;
            chkboxWatermark.Checked = proj.projWatermark != 0;
            chkboxFriendlyURL.Checked = proj.projFriendlyURL != 0;
            chkboxTraining.Checked = proj.projTraining != 0;
            chkboxDeliveryMsg.Checked = proj.projDeliveryMsg != 0;
            chkboxProdPromo.Checked = proj.projProdPromo != 0;

            if (chkboxInv.Checked == true)
            {
                ddlInv.Visible = true;

                if (proj.projInventoryType != 0)
                {
                    ddlInv.SelectedValue = proj.projInventoryType.ToString();
                }
            }

           

            // Start GST feature
            if (proj.projGst != 0) { chklistGst.Items.FindByValue(strLiGstActive).Selected = true; }
            else { chklistGst.Items.FindByValue(strLiGstActive).Selected = false; }

            if (proj.projGstNotAvailable != 0) { chklistGst.Items.FindByValue(strLiGstNotAvailable).Selected = true; }
            else { chklistGst.Items.FindByValue(strLiGstNotAvailable).Selected = false; }

            if (proj.projGstInclusive != 0) { chklistGst.Items.FindByValue(strLiGstInclusive).Selected = true; }
            else { chklistGst.Items.FindByValue(strLiGstInclusive).Selected = false; }

            if (proj.projGstApplied != 0) { chklistGst.Items.FindByValue(strLiGstApplied).Selected = true; }
            else { chklistGst.Items.FindByValue(strLiGstApplied).Selected = false; }
            // End GST Feature

            // Start Slider setting feature
            if (proj.projSliderActive != 0) { chklistSlider.Items.FindByValue(strLiSliderActive).Selected = true; }
            else { chklistSlider.Items.FindByValue(strLiSliderActive).Selected = false; }

            if (proj.projSliderFullScreen != 0) { chklistSlider.Items.FindByValue(strLiSliderFullScreen).Selected = true; }
            else { chklistSlider.Items.FindByValue(strLiSliderFullScreen).Selected = false; }

            if (proj.projSliderMobile != 0) { chklistSlider.Items.FindByValue(strLiSliderMobile).Selected = true; }
            else { chklistSlider.Items.FindByValue(strLiSliderMobile).Selected = false; }
            // end of slider setting feature

            // Start Page Authorization Setting
            chkboxPgAuthSetting.Checked = proj.projPgAuthorization != 0;
            rdoListLogin.Items.FindByValue(strLiAuthUniversalLogin).Selected = proj.projUniversalLogin != 0;
            rdoListLogin.Items.FindByValue(strLiAuthIndividualLogin).Selected = proj.projIndividualLogin != 0;
            // end Page Authorization Setting

            chkboxRightClick.Checked = proj.projRightClick == 1;
            chkboxWebteqLogo.Checked = proj.projWebteqLogo == 1;
            chkboxCkeditorTopmenu.Checked = proj.projCkeditorTopmenu == 1;
            chkboxSMSSetting.Checked = proj.projSMSSetting == 1;
            chkboxDeliveryDatetime.Checked = proj.projDeliveryDatetime == 1;
            chkboxProdAddon.Checked = proj.projProdAddon == 1;

            //Start File Upload Settings
            txtImages.Text = proj.projImgSetting.ToString();
            txtFile.Text = proj.projFileSetting.ToString();
            txtCkeditorFile.Text = proj.projCkeditorFileSetting.ToString();
            txtCkeditorImage.Text = proj.projCkeditoImgSetting.ToString();
            txtDefault.Text = proj.projDefaultSetting.ToString();
            txtEventThumb.Text = proj.projEventThumbSetting.ToString();
            txtEventGallery.Text = proj.projEventGallSetting.ToString();
            txtProductThumb.Text = proj.projProdThumbSetting.ToString();
            txtProductGallery.Text = proj.projProdGallSetting.ToString();
            //End File Upload Settings

            // ============ Enquiry Setting =================
            chkboxEnqField.Checked = proj.projEnquiryField == 1;


            // ============ Facebook catalog ================
            chkboxFbCatalogActive.Checked = proj.projFbCatalogActive == 1;
            txtFbCatalogExpired.Text = (proj.projFbCatalogExpired == DateTime.MinValue) ? "" : proj.projFbCatalogExpired.ToString("d MMM yyyy");
            lblRandId.Text = proj.projRandId;
            lblRandId.Visible = !string.IsNullOrEmpty(proj.projRandId);

            DataSet dsLang = new DataSet();
            dsLang = proj.getLanguageById(projId);
            
            foreach (DataRow row in dsLang.Tables[0].Rows)
            {
                Session["ORILANG"] += row["V_LANGNAME"].ToString() + clsAdmin.CONSTCOMMASEPARATOR.ToString() + row["LANG_LANG"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR.ToString();
            }

            populatePaymentGateway();
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }

    protected void fillForm2()
    {
        txtImages.Text = clsAdmin.CONSTADMIMGALLOWEDMAXLENGTHINKB.ToString();
        txtFile.Text = clsAdmin.CONSTADMDOCALLOWEDMAXLENGTHINKB.ToString();
        txtCkeditorFile.Text = clsAdmin.CONSTADMDOCALLOWEDMAXLENGTHINKB.ToString();
        txtCkeditorImage.Text = clsAdmin.CONSTADMDOCALLOWEDMAXLENGTHINKB.ToString();
        txtDefault.Text = clsAdmin.CONSTADMIMGALLOWEDMAXLENGTHINKB.ToString();
        txtEventThumb.Text = clsAdmin.CONSTADMIMGALLOWEDMAXLENGTHINKB.ToString();
        txtEventGallery.Text = clsAdmin.CONSTADMIMGALLOWEDMAXLENGTHINKB.ToString();
        txtProductThumb.Text = clsAdmin.CONSTADMIMGALLOWEDMAXLENGTHINKB.ToString();
        txtProductGallery.Text = clsAdmin.CONSTADMIMGALLOWEDMAXLENGTHINKB.ToString();
    }

    protected void populateLang()
    {
        pnlLang.Controls.Clear();
        int intCount = 0;

        try
        {
            string strOriLang = "";
            if (Session["ORILANG"] != null && Session["ORILANG"] != "")
            {
                if (!Session["ORILANG"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["ORILANG"] = clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + Session["ORILANG"].ToString(); }
                strOriLang = Session["ORILANG"].ToString();
            }

            string strDelLang = "";
            if (Session["DELLANG"] != null && Session["DELLANG"] != "")
            {
                if (!Session["DELLANG"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["DELLANG"] = clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + Session["DELLANG"].ToString(); }
                strDelLang = Session["DELLANG"].ToString();
            }

            string strLang = "";
            if (Session["LANG"] != null && Session["LANG"] != "")
            {
                if (!Session["LANG"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["LANG"] = clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + Session["LANG"].ToString(); }
                strLang = Session["LANG"].ToString();
            }

            if (!string.IsNullOrEmpty(strOriLang) || !string.IsNullOrEmpty(strLang))
            {
                HtmlTable tbl = new HtmlTable();
                tbl.CellPadding = 0;
                tbl.CellSpacing = 0;
                tbl.Attributes.Add("class", "innerTbl");
                pnlLang.Controls.Add(tbl);

                HtmlTableRow row;
                HtmlTableCell cell;
                Literal litOpenBrac;

                cell = new HtmlTableCell();

                if (!string.IsNullOrEmpty(strOriLang))
                {
                    strOriLang = strOriLang.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strOriLang.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                }

                if (!string.IsNullOrEmpty(strOriLang))
                {
                    strOriLang = strOriLang.Substring(0, strOriLang.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                    LinkButton lnkbtn;
                    string[] strItemsSplit = strOriLang.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                    for (int i = 0; i <= strItemsSplit.GetUpperBound(0); i++)
                    {
                        if (strDelLang.IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + strItemsSplit[i] + clsAdmin.CONSTDEFAULTSEPERATOR) < 0)
                        {
                            string[] strItemItemSplit = strItemsSplit[i].Split(clsAdmin.CONSTCOMMASEPARATOR);
                            intCount += 1;

                            if (intCount > 1)
                            {
                                litOpenBrac = new Literal();
                                litOpenBrac.Text = " | ";
                                cell.Controls.Add(litOpenBrac);
                            }

                            lnkbtn = new LinkButton();
                            lnkbtn.ID = strItemItemSplit[1] + "_delete_" + i;
                            lnkbtn.Text = strItemItemSplit[0];
                            lnkbtn.Command += new CommandEventHandler(oriLang_OnDelete);
                            lnkbtn.CommandArgument = strItemsSplit[i];
                            lnkbtn.CausesValidation = false;
                            cell.Controls.Add(lnkbtn);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(strLang))
                {
                    strLang = strLang.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strLang.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                }

                if (!string.IsNullOrEmpty(strLang))
                {
                    strLang = strLang.Substring(0, strLang.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                    LinkButton lnkbtn;
                    string[] strItemsSplit = strLang.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                    for (int i = 0; i <= strItemsSplit.GetUpperBound(0); i++)
                    {
                        string[] strItemItemSplit = strItemsSplit[i].Split(clsAdmin.CONSTCOMMASEPARATOR);
                        intCount += 1;

                        if (intCount > 1)
                        {
                            litOpenBrac = new Literal();
                            litOpenBrac.Text = " | ";
                            cell.Controls.Add(litOpenBrac);
                        }

                        lnkbtn = new LinkButton();
                        lnkbtn.ID = strItemItemSplit[1] + "_delete_" + i;
                        lnkbtn.Text = strItemItemSplit[0];
                        lnkbtn.Command += new CommandEventHandler(lang_OnDelete);
                        lnkbtn.CommandArgument = strItemsSplit[i];
                        cell.Controls.Add(lnkbtn);
                    }
                }

                row = new HtmlTableRow();
                row.Controls.Add(cell);
                tbl.Controls.Add(row);
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message.ToString());
            throw ex;
        }
    }
    protected void populatePaymentGateway()
    {
        clsProject proj = new clsProject();
        DataSet dsPaymentGateway = proj.getPaymentGatewayList(projId , 1);
        if (dsPaymentGateway.Tables[0].Rows.Count > 0)
        {
            foreach(DataRow row in dsPaymentGateway.Tables[0].Rows)
            {
                chklistPaymentGateway.Items.FindByValue(row["PG_NAME"].ToString()).Selected = true;
            }
        }

    }
    private void initChkboxListSlider()
    {
        ListItem liSliderActive = new ListItem();
        liSliderActive.Value = strLiSliderActive;
        liSliderActive.Text = "Active";
        liSliderActive.Attributes["onchange"] = "toggleChkboxListSetting(this);";

        ListItem liSliderFullScreen = new ListItem();
        liSliderFullScreen.Value = strLiSliderFullScreen;
        liSliderFullScreen.Text = "Full Screen";

        ListItem liSliderDefault = new ListItem();
        liSliderDefault.Value = strLiSliderDefault;
        liSliderDefault.Selected = true;
        liSliderDefault.Text = "Default";
        liSliderDefault.Enabled = false;

        ListItem liSliderMobile = new ListItem();
        liSliderMobile.Value = strLiSliderMobile;
        liSliderMobile.Selected = true;
        liSliderMobile.Text = "Mobile";

        liSliderDefault.Enabled = false;
        chklistSlider.Items.Add(liSliderActive);
        chklistSlider.Items.Add(liSliderDefault);
        chklistSlider.Items.Add(liSliderFullScreen);
        chklistSlider.Items.Add(liSliderMobile);
    }
    private void initChkboxListGst()
    {
        ListItem liGstActive = new ListItem();
        liGstActive.Value = strLiGstActive;
        liGstActive.Text = "Active";
        liGstActive.Attributes["onchange"] = "toggleGstSetting(this);";

        ListItem liGstNotAvailable = new ListItem();
        liGstNotAvailable.Value = strLiGstNotAvailable;
        liGstNotAvailable.Text = "Not Available";
        liGstNotAvailable.Enabled = false;

        ListItem liGstInclusive = new ListItem();
        liGstInclusive.Value = strLiGstInclusive;
        liGstInclusive.Text = "Gst Inclusive";

        ListItem liGstApplied = new ListItem();
        liGstApplied.Value = strLiGstApplied;
        liGstApplied.Text = "Gst Applied";

        chklistGst.Items.Add(liGstActive);
        chklistGst.Items.Add(liGstNotAvailable);
        chklistGst.Items.Add(liGstInclusive);
        chklistGst.Items.Add(liGstApplied);
    }

    private void initChkboxListPgAuthorization()
    {
        chkboxPgAuthSetting.Checked = false;
        chkboxPgAuthSetting.Attributes["onchange"] = "toggleAuthSetting(this);";

        ListItem liAuthUniversalLogin = new ListItem();
        liAuthUniversalLogin.Value = strLiAuthUniversalLogin;
        liAuthUniversalLogin.Text = "Universal Login";

        ListItem liAuthIndividualLogin = new ListItem();
        liAuthIndividualLogin.Value = strLiAuthIndividualLogin;
        liAuthIndividualLogin.Text = "Individual Login";

        rdoListLogin.Items.Add(liAuthUniversalLogin);
        rdoListLogin.Items.Add(liAuthIndividualLogin);
    }
    #endregion

}
