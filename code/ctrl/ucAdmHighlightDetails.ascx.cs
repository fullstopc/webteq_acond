﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Data;
using System.Configuration;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

public partial class ctrl_ucAdmHighlightDetails : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _prodId = 0;
    protected int _mode = 0;
    protected string _pageListingURL = "";

    protected string hlTitle;
    protected string hlTitleJp;
    protected string hlTitleMs;
    protected string hlTitleZh;
    protected string hlTitleFURL;
    protected string hlSnapshot;
    protected string hlSnapshotJp;
    protected string hlSnapshotMs;
    protected string hlSnpashotZh;
    protected string hlShortDesc;
    protected string hlShortDescJp;
    protected string hlShortDescMs;
    protected string hlShortDescZh;
    protected DateTime hlStart = DateTime.MaxValue;
    protected DateTime hlEnd = DateTime.MaxValue;
    protected int hlCat;
    protected string hlImage;
    protected int hlOrder;
    protected int hlShowTop;
    protected int hlVisible;
    protected int hlAllowComment;

    // crop image - sh.chong 22 Sep 2015
    protected decimal decImgWidth = Convert.ToDecimal("0.00");
    protected decimal decImgHeight = Convert.ToDecimal("0.00");
    protected decimal decImgX = Convert.ToDecimal("0.00");
    protected decimal decImgY = Convert.ToDecimal("0.00");
    protected string strImgCropped = "";
    protected string strImgName = "";
    protected string uploadPath = "";
    protected string aspectRatioX = "1";
    protected string aspectRatioY = "1";
    protected string aspectRatio = "1/1";
    // end crop image
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int highId
    {
        get
        {
            if (ViewState["HIGHID"] == null)
            {
                return _prodId;
            }
            else
            {
                return Convert.ToInt16(ViewState["HIGHID"]);
            }
        }
        set { ViewState["HIGHID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if (ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //protected void validateDesc_server(object source, ServerValidateEventArgs args)
    //{
    //    if (!string.IsNullOrEmpty(txtHltShortDesc.Text.Trim())) { args.IsValid = true; }
    //    else
    //    {
    //        args.IsValid = false;
    //        cvDesc.ErrorMessage = "<br />Please enter Description.";
    //    }
    //}

    //protected void cvDesc_PreRender(object sender, EventArgs e)
    //{
    //    if ((!Page.ClientScript.IsStartupScriptRegistered("Desc")))
    //    {
    //        string strJS = null;
    //        strJS = "<script type = \"text/javascript\">";
    //        strJS += "ValidatorHookupControlID('" + txtHltShortDesc.ClientID + "', document.all['" + cvDesc.ClientID + "']);";
    //        strJS += "</script>";

    //        Page.ClientScript.RegisterStartupScript(Page.GetType(), "Desc", strJS, false);
    //    }
    //}

    protected void validatePageTitleFriendlyUrl_server(object source, ServerValidateEventArgs args)
    {
        clsPage page = new clsPage();
        if (page.isPageTitleFriendlyUrlExist(txtHltTitleFriendlyURL.Text, highId))
        {
            args.IsValid = false;
            cvPageTitleFriendlyUrl.ErrorMessage = "This " + txtHltTitleFriendlyURL.Text + " is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvPageTitleFriendlyUrl_PreRender(object source, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("pagetitlefriendlyurl")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtHltTitleFriendlyURL.ClientID + "', document.all['" + cvPageTitleFriendlyUrl.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "pagetitlefriendlyurl", strJS);
        }
    }

    protected void validateHltImage_server(object source, ServerValidateEventArgs args)
    {
        if (fileHltImage.HasFile)
        {

            int intImgMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTEVENTTHUMBSETTING] != null && Session[clsAdmin.CONSTPROJECTEVENTTHUMBSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTEVENTTHUMBSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTEVENTTHUMBSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            //if (fileHltImage.PostedFile.ContentLength > clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH)
            //{
            //    cvHltImage.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH, "kb") + ".";
            //    args.IsValid = false;
            //}

            if (fileHltImage.PostedFile.ContentLength > intImgMaxSize)
            {
                cvHltImage.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + ".";
                args.IsValid = false;
            }
            else
            {
                Match matchImage = Regex.Match(fileHltImage.FileName, clsAdmin.CONSTADMIMGALLOWEDEXT, RegexOptions.IgnoreCase);
                if (matchImage.Success)
                {
                    args.IsValid = true;
                }
                else
                {
                    cvHltImage.ErrorMessage = "<br />Please enter valid image. (" + clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + ").";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cvHltImage_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("ProdImage")))
        {
            string strJS = null;
            strJS = "<script type = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + fileHltImage.ClientID + "', document.all['" + cvHltImage.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "ProdImage", strJS, false);
        }
    }

    protected void lnkbtnDeleteImage_Click(object sender, EventArgs e)
    {
        pnlHltImage.Visible = false;
        hdnHltImage.Value = "";
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMHIGHFOLDER + "/";
        string strDeletedHighName = "";

        clsMis.performDeleteHighlight(highId, uploadPath, ref strDeletedHighName);

        Session["DELETEDHIGHNAME"] = strDeletedHighName;

        Response.Redirect(currentPageName);
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            hlTitle = txtHltTitle.Text.Trim();
            hlTitleJp = txtHltTitleJp.Text.Trim();
            hlTitleMs = txtHltTitleMs.Text.Trim();
            hlTitleZh = txtHltTitleZh.Text.Trim();
            hlTitleFURL = txtHltTitleFriendlyURL.Text.Trim();
            hlSnapshot = txtSnapshot.Text.Replace("/n", "<br/>");
            hlSnapshotJp = txtSnapshotJp.Text.Trim().Replace("/n", "<br />");
            hlSnapshotMs = txtSnapshotMs.Text.Trim().Replace("/n", "<br />");
            hlSnpashotZh = txtSnapshotZh.Text.Trim().Replace("/n", "<br />");
            hlShortDesc = txtHltShortDesc.Text.Trim();
            hlShortDescJp = txtHltShortDescJp.Text.Trim();
            hlShortDescMs = txtHltShortDescMs.Text.Trim();
            hlShortDescZh = txtHltShortDescZh.Text.Trim();
            string hlSourceUrl = txtSourceUrl.Text.Trim();
            string hlSourceTitle = txtSourceTitle.Text.Trim();

            if (!string.IsNullOrEmpty(ddlCategory.SelectedValue))
            {
                hlCat = int.Parse(ddlCategory.SelectedValue);
            }

            hlOrder = !string.IsNullOrEmpty(txtOrder.Text.Trim()) ? Convert.ToInt16(txtOrder.Text.Trim()) : 0;

            if (chkboxShowTop.Checked) { hlShowTop = 1; }
            else { hlShowTop = 0; }

            if (chkboxVisible.Checked) { hlVisible = 1; }
            else { hlVisible = 0; }

            if (chkboxComment.Checked) { hlAllowComment = 1; }
            else { hlAllowComment = 0; }

            DateTime dtTryParse;

            if (DateTime.TryParse(clsMis.formatDateDDMMtoMMDD(txtHltStartDate.Text.Trim()), out dtTryParse))
            {
                hlStart = dtTryParse;
            }
            else { hlStart = DateTime.MaxValue; }

            if (DateTime.TryParse(clsMis.formatDateDDMMtoMMDD(txtHltEndDate.Text.Trim()), out dtTryParse))
            {
                hlEnd = dtTryParse;
            }
            else { hlEnd = DateTime.MaxValue; }

            //  cropped image - sh.chong 22 Sep 2015
            uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMHIGHFOLDER + "/";
            bool boolHasCroppedImg = (!string.IsNullOrEmpty(hdnImgWidth.Value)) &&
                                     (!string.IsNullOrEmpty(hdnImgHeight.Value)) &&
                                     (!string.IsNullOrEmpty(hdnImgX.Value)) &&
                                     (!string.IsNullOrEmpty(hdnImgY.Value)) &&
                                     (!string.IsNullOrEmpty(hdnImgCropped.Value)) &&
                                     (!string.IsNullOrEmpty(hdnImgName.Value));
            bool boolHasFile = fileHltImage.HasFile;

            if (boolHasCroppedImg || boolHasFile)
            {
                try
                {
 

                    string newFilename = "";
                    if (boolHasCroppedImg)
                    {
                        decImgWidth = Convert.ToDecimal(hdnImgWidth.Value);
                        decImgHeight = Convert.ToDecimal(hdnImgHeight.Value);
                        decImgX = Convert.ToDecimal(hdnImgX.Value);
                        decImgY = Convert.ToDecimal(hdnImgY.Value);
                        strImgCropped = hdnImgCropped.Value;
                        strImgName = hdnImgName.Value;

                        newFilename = clsMis.GetUniqueFilename(strImgName, uploadPath);
                        string base64 = strImgCropped;
                        byte[] bytes = Convert.FromBase64String(base64.Split(',')[1]);
                        hlImage = newFilename;

                        using (FileStream fs = new FileStream(uploadPath + newFilename, FileMode.Create))
                        {
                            using (BinaryWriter bw = new BinaryWriter(fs)) { bw.Write(bytes); bw.Close(); }
                        }
                    }
                    else if (boolHasFile)
                    {
                        HttpPostedFile postedFile = fileHltImage.PostedFile;
                        string fileName = Path.GetFileName(postedFile.FileName);
                        clsMis.createFolder(uploadPath);
                        newFilename = clsMis.GetUniqueFilename(fileName, uploadPath);
                        postedFile.SaveAs(uploadPath + newFilename);
                        hlImage = newFilename;
                    }

                    //Add by sh.chong 20Apr2015 for water mark purpose
                    clsMis mis = new clsMis();
                    clsConfig config = new clsConfig();

                    DataSet ds = config.getItemList(1);
                    DataRow[] dwWatermarks = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPWATERMARKSETTINGS + "'");

                    string strWatermarkType = "0",
                           strWatermarkValue = "",
                           strWatermarkOpacity = "0.1";

                    if (dwWatermarks.Length > 0)
                    {
                        foreach (DataRow dwWatermark in dwWatermarks)
                        {
                            if (dwWatermark["CONF_NAME"].ToString() == clsConfig.CONSTNAMEWATERMARKTYPE)
                            {
                                if (!string.IsNullOrEmpty(dwWatermark["CONF_VALUE"].ToString()))
                                {
                                    strWatermarkType = dwWatermark["CONF_VALUE"].ToString();
                                }
                            }
                            else if (dwWatermark["CONF_NAME"].ToString() == clsConfig.CONSTNAMEWATERMARKVALUE)
                            {
                                strWatermarkValue = dwWatermark["CONF_VALUE"].ToString();
                            }
                            else if (dwWatermark["CONF_NAME"].ToString() == clsConfig.CONSTNAMEWATERMARKOPACITY)
                            {
                                strWatermarkOpacity = dwWatermark["CONF_VALUE"].ToString();
                            }
                        }

                    }
                    bool boolWatermarkImageExists = (strWatermarkType == clsAdmin.CONSTWATERMARKTYPEIMAGE && !string.IsNullOrEmpty(strWatermarkValue));
                    bool boolWatermarkTextExists = (strWatermarkType == clsAdmin.CONSTWATERMARKTYPETEXT && !string.IsNullOrEmpty(strWatermarkValue));

                    if (boolWatermarkImageExists)
                    {
                        string strWatermarkUploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCTRLPNLADMFOLDER + "/";

                        Bitmap bitOriginal = new Bitmap(uploadPath + newFilename);
                        Bitmap bitWatermark = new Bitmap(strWatermarkUploadPath + strWatermarkValue, false);
                        float opacity = float.Parse(strWatermarkOpacity);

                        Bitmap bitNew = new Bitmap(mis.drawWatermarkImage(bitOriginal, bitWatermark, opacity));
                        bitOriginal.Dispose();
                        bitOriginal = null;
                        bitNew.Save(uploadPath + newFilename);
                    }
                    else if (boolWatermarkTextExists)
                    {
                        double dblOpacity = double.Parse(strWatermarkOpacity) * 255;
                        Bitmap bitOriginal = new Bitmap(uploadPath + newFilename);
                        Bitmap bitNew = new Bitmap(mis.drawWatermarkText(bitOriginal, strWatermarkValue, (int)dblOpacity));
                        bitOriginal.Dispose();
                        bitOriginal = null;
                        bitNew.Save(uploadPath + newFilename);
                    }
                    //End by sh.chong 20Apr2015 for water mark purpose
                }
                catch (Exception ex)
                {
                    clsLog.logErroMsg(ex.Message.ToString());
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />" + ex.Message.ToString() + "</div>";
                }
            }
            else
            {
                if (mode == 1)
                {
                    hlImage = "";
                }
                else
                {
                    hlImage = hdnHltImage.Value;
                }
            }


            clsHighlight high = new clsHighlight();
            int intRecordAffected = 0;

            switch (mode)
            {
                case 1:
                    intRecordAffected = high.addHighlightDetails(hlTitle, hlTitleJp, hlTitleMs, hlTitleZh, hlTitleFURL, hlSnapshot, hlSnapshotJp, hlSnapshotMs, hlSnpashotZh,
                                                                 hlShortDesc, hlShortDescJp, hlShortDescMs, hlShortDescZh, hlStart, hlEnd, hlImage, hlOrder,
                                                                 hlShowTop, hlVisible, hlAllowComment,hlSourceUrl,hlSourceTitle, Convert.ToInt16(Session["ADMID"]));

                    if (intRecordAffected == 1)
                    {
                        if (hlCat != 0) { high.assignHighlightGroup(high.highId, hlCat); }

                        Session["NEWHIGHID"] = high.highId;
                        Response.Redirect(currentPageName + "?id=" + high.highId);
                    }
                    else
                    {
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new highlight. Please try again.</div>";
                        Response.Redirect(currentPageName);
                    }
                    break;
                case 2:
                    Boolean boolEdited = false;
                    Boolean boolSuccess = true;

                    if (hlCat != int.Parse(hdnCategory.Value))
                    {
                        if (int.Parse(hdnCategory.Value) == 0)
                        {
                            intRecordAffected = high.assignHighlightGroup(highId, hlCat);
                        }
                        else
                        {
                            intRecordAffected = high.updateHighlightGroup(highId, hlCat);
                        }

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;
                        }
                        else { boolSuccess = false; }
                    }

                    if (boolSuccess && !high.isExactSameHighlightDetails(highId, hlTitle, hlTitleJp, hlTitleMs, hlTitleZh, hlTitleFURL, hlSnapshot, hlSnapshotJp, hlSnapshotMs, hlSnpashotZh,
                                                                         hlShortDesc, hlShortDescJp, hlShortDescMs, hlShortDescZh, hlStart, hlEnd, hlImage, hlOrder, hlShowTop,
                                                                         hlVisible, hlAllowComment, hlSourceUrl, hlSourceTitle))
                    {
                        intRecordAffected = high.updateHighlightDetails(highId, hlTitle, hlTitleJp, hlTitleMs, hlTitleZh, hlTitleFURL, hlSnapshot, hlSnapshotJp, hlSnapshotMs, hlSnpashotZh,
                                                                        hlShortDesc, hlShortDescJp, hlShortDescMs, hlShortDescZh, hlStart, hlEnd, hlImage, hlOrder, hlShowTop,
                                                                        hlVisible, hlAllowComment, hlSourceUrl, hlSourceTitle, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1)
                        {
                            if (hdnHltImageRef.Value != hlImage && hdnHltImageRef.Value != "")
                            {
                                uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMHIGHFOLDER + "/";
                                if (File.Exists(uploadPath + hdnHltImageRef.Value)) { File.Delete(uploadPath + hdnHltImageRef.Value); }
                            }
                            boolEdited = true;
                        }
                        else
                        {
                            boolSuccess = false;
                        }
                    }

                    if (boolEdited)
                    {
                        Session["EDITEDHIGHID"] = high.highId;
                    }
                    else
                    {
                        if (!boolSuccess)
                        {
                            high.extractHighlightById(highId, 0);
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit highlight (" + high.highTitle + "). Please try again.</div>";
                        }
                        else
                        {
                            Session["EDITEDHIGHID"] = highId;
                            Session["NOCHANGE"] = 1;
                        }
                    }

                    Response.Redirect(Request.Url.ToString());
                    break;
                default:
                    break;
            }
        }
    }
    #endregion


    #region "Methods"
    public void registerScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("FORMSECT3")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "initSect3();";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "FORMSECT3", strJS, false);
        }
    }

    protected void registerCKEditorScript(string strId)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR" + strId)))
        {
            int intFullPath = Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFULLPATH]) == 1 ? clsAdmin.CONSTFULLPATHACTIVE : clsAdmin.CONSTFULLPATHDEFAULT;
            string strJS = null;
            strJS = "<script type=\"text/javascript\">" +
                    "CKEDITOR.replace('" + strId + "'," +
                    "{" +
                    "width:'95%'," +
                    "height:'300'," +
                    "toolbar: 'User3'," +
                    "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                     "}" +
                     ");";

            strJS += "CKEDITOR.add;";

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMEEDITORSTYLE, clsConfig.CONSTGROUPWEBSITE, 1);
            string strEditorStyle = config.value;
            if (!string.IsNullOrEmpty(strEditorStyle))
            {
                strJS += "CKEDITOR.config.contentsCss = 'body {" + strEditorStyle + "}';";
            }
            
            strJS += "CKEDITOR.config.stylesCombo_stylesSet= '" + clsAdmin.CONSTADMSTYLESET + ":" + System.Configuration.ConfigurationManager.AppSettings["jsBase"] + clsAdmin.CONSTADMSTYLESETFILE + "';" +
                     "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR" + strId, strJS, false);
        }
    }

    public void fill()
    {
        registerScript();
        
        if (!IsPostBack)
        {
            setPageProperties();

            clsConfig config = new clsConfig();
            string aspectRatio = config.aspectRatio;
            string[] aspectRatioXandY = aspectRatio.Split('/');
            aspectRatioX = aspectRatioXandY[0];
            aspectRatioY = aspectRatioXandY[1];

        }

        checkLanguage();
        registerCKEditorScript(txtHltShortDesc.ClientID);
    }

    protected void setPageProperties()
    {
        if (Session[clsAdmin.CONSTPROJECTFRIENDLYURL] != null && clsAdmin.CONSTPROJECTFRIENDLYURL != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFRIENDLYURL]) == 0)
            {
                trFriendlyURL.Visible = false;
            }
        }


        DataSet ds = new DataSet();
        clsMis mis = new clsMis();
        ds = mis.getListByListGrp("HIGHLIGHT GROUP", 1);

        DataView dv = new DataView(ds.Tables[0]);
        dv.Sort = "LIST_ORDER ASC";

        if (dv.Count > 0)
        {
            ddlCategory.DataSource = dv;
            ddlCategory.DataTextField = "LIST_NAME";
            ddlCategory.DataValueField = "LIST_VALUE";
            ddlCategory.DataBind();

            ListItem ddlCategoryDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
            ddlCategory.Items.Insert(0, ddlCategoryDefaultItem);

            trCategory.Visible = true;
        }

        switch (mode)
        {
            case 1:
                break;
            case 2:
                lnkbtnDelete.Visible = true;

                fillForm();

                break;
            default:
                break;
        }

        int intImgMaxSize = 0;

        if (Session[clsAdmin.CONSTPROJECTEVENTTHUMBSETTING] != null && Session[clsAdmin.CONSTPROJECTEVENTTHUMBSETTING] != "")
        {
            if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTEVENTTHUMBSETTING]) > 0)
            {
                intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTEVENTTHUMBSETTING]) * 1024;
            }
            else
            {
                if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                {
                    if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                    {
                        intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                    }
                }
            }
        }

        litUploadRule.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb");
        //litUploadRule.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH, "kb");
        
        lnkbtnCancel.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;"; 
        lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "admConfirmDeleteHighlight.Text").ToString() + "');";

    }

    protected void fillForm()
    {
        clsHighlight high = new clsHighlight();
        if (high.extractHighlightById(highId, 0))
        {
            txtHltTitle.Text = high.highTitle;
            txtHltTitleJp.Text = high.highTitle_jp;
            txtHltTitleMs.Text = high.highTitle_ms;
            txtHltTitleZh.Text = high.highTitle_zh;
            txtHltTitleFriendlyURL.Text = high.highTitleFURL;
            txtSnapshot.Text = high.highSnapshot.Replace("<br />", "/n");
            txtSnapshotJp.Text = high.highSnapshotJp.Replace("<br />", "/n");
            txtSnapshotMs.Text = high.highSnapshotMs.Replace("<br />", "/n");
            txtSnapshotZh.Text = high.highSnapshotZh.Replace("<br />", "/n");
            txtHltShortDesc.Text = high.highShortDesc;
            txtHltShortDescJp.Text = high.highShortDesc_jp;
            txtHltShortDescMs.Text = high.highShortDesc_ms;
            txtHltShortDescZh.Text = high.highShortDesc_zh; 
            hdnCategory.Value = high.vGroup.ToString();
            txtSourceTitle.Text = high.highSourceTitle;
            txtSourceUrl.Text = high.highSourceUrl;

            if (high.highStartDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
            {
                txtHltStartDate.Text = clsMis.formatDateMMDDtoDDMM(high.highStartDate.ToString());
            }
            else { txtHltStartDate.Text = ""; }

            if (high.highEndDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
            {
                txtHltEndDate.Text = clsMis.formatDateMMDDtoDDMM(high.highEndDate.ToString());
            }
            else { txtHltEndDate.Text = ""; }

            if (high.vGroup != 0)
            {
                ddlCategory.SelectedValue = high.vGroup.ToString();
            }

            if (high.highImage != "")
            {
                imgHltImage.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMHIGHFOLDER + "/" + high.highImage + "&w=" + clsAdmin.CONSTADMDEFAULTIMGWIDTH + "&h=" + clsAdmin.CONSTADMDEFAULTIMGHEIGHT + "&f=1";
                hdnHltImage.Value = high.highImage;
                hdnHltImageRef.Value = high.highImage;
                pnlHltImage.Visible = true;
            }

            txtOrder.Text = high.highOrder.ToString();

            if (high.highShowTop != 0) { chkboxShowTop.Checked = true; }
            else { chkboxShowTop.Checked = false; }

            if (high.highVisible != 0) { chkboxVisible.Checked = true; }
            else { chkboxVisible.Checked = false; }

            if (high.highAllowComment != 0) { chkboxComment.Checked = true; }
            else { chkboxComment.Checked = false; }

            Session["HIGHTITLE"] = high.highTitle;
            Session["HIGHTITLE_JP"] = high.highTitle_jp;
            Session["HIGHTITLE_MS"] = high.highTitle_ms;
            Session["HIGHTITLE_ZH"] = high.highTitle_zh;
            Session["HIGHSHORTDESC"] = high.highShortDesc;
            Session["HIGHSHORTDESC_JP"] = high.highShortDesc_jp;
            Session["HIGHSHORTDESC_MS"] = high.highShortDesc_ms;
            Session["HIGHSHORTDESC_ZH"] = high.highShortDesc_zh;
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }
    protected void checkLanguage()
    {
        if (clsMis.isLangExist(clsAdmin.CONSTLANGJAPANESE))
        {
            trTitleJp.Visible = true;
            trSnapshotJp.Visible = true;
            trDescJp.Visible = true;

            registerCKEditorScript(txtHltShortDescJp.ClientID);
        }
        if (clsMis.isLangExist(clsAdmin.CONSTLANGMALAY))
        {
            trTitleMs.Visible = true;
            trSnapshotMs.Visible = true;
            trDescMs.Visible = true;

            registerCKEditorScript(txtHltShortDescMs.ClientID);
        }
        if (clsMis.isLangExist(clsAdmin.CONSTLANGCHINESE))
        {
            trTitleZh.Visible = true;
            trSnapshotZh.Visible = true;
            trDescZh.Visible = true;

            registerCKEditorScript(txtHltShortDescZh.ClientID);
        }
    }
    #endregion
}
