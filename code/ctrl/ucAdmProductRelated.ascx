﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmProductRelated.ascx.cs" Inherits="ctrl_ucAdmProductRelated" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>

<asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
    <asp:Literal ID="litAck" runat="server"></asp:Literal>
</asp:Panel>
<asp:Panel ID="pnlForm" runat="server">
    <div class="hide">
        <div class="filter">
            <div class="divFilterContainer">
                <div class="divFilter">
                    <asp:TextBox ID="txtKeyword" runat="server" placeholder="Keyword Search" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
                    <a href="#" class="btnDropdown" onclick="toggleFilterOption();"></a>
                    <div runat="server" visible="false">(Code, Name, Display Name)</div>
                </div>
                <div class="divFilterMore">
                    <div></div>
                    <div>
                        <table class="tblSearch">
                            <tr>
                                <td class="">
                                    <div class="divFilterLabel">New Product:</div>
                                    <asp:DropDownList ID="ddlNew" runat="server" CssClass="ddl_medium"></asp:DropDownList>
                                </td>
                                <td class="">
                                    <div class="divFilterLabel">Show on Top:</div>
                                    <asp:DropDownList ID="ddlShowTop" runat="server" CssClass="ddl_medium"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="">
                                    <div class="divFilterLabel">Category:</div>
                                    <asp:DropDownList ID="ddlCat" runat="server" CssClass="ddl_medium"></asp:DropDownList>
                                </td>
                                <td class="">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="divFilterAction"><asp:Button ID="lnkbtnGo" runat="server" Text="" ToolTip="Search" CssClass="btn btnSearch" OnClick="lnkbtnGo_Click"/></div>
        </div>
    </div>
    <asp:Panel ID="pnlProducts" runat="server" CssClass="divListing">
        <asp:Panel ID="pnlProd" runat="server" CssClass="divProdList">
            <asp:Panel ID="pnlProdRelNoFound" runat="server" CssClass="divProdRelNoFound" Visible="false">
                <asp:Literal ID="litNoFound" runat="server" meta:resourcekey="litNoFound"></asp:Literal>
            </asp:Panel>
            <asp:Panel ID="pnlProdRelProd" runat="server" CssClass="divProdListItems">
                <asp:Panel ID="pnlProdRelProdTotal" runat="server" CssClass="divProdRelProdTotal">
                    <asp:Literal ID="litProdRelProdTotal" runat="server"></asp:Literal>
                </asp:Panel>
                <asp:Panel ID="pnlProdRelProdPagingTop" runat="server" CssClass="divPaging">
                    <asp:Panel ID="pnlProdRelProdPagingPaginationTop" runat="server" CssClass="divProdRelProdPagingPagination">
                        <div class="divPagingInner">
                            <asp:LinkButton ID="lnkbtnFirstTop" runat="server" meta:resourcekey="lnkbtnFirst" OnClick="lnkbtnFirst_Click" CssClass="linkPageLead" Visible="false"></asp:LinkButton>
                            <span class="spanPagination">
                                <asp:Repeater ID="rptPaginationTop" runat="server" OnItemDataBound="rptPagination_ItemDataBound" OnItemCommand="rptPagination_ItemCommand">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkbtnPage" runat="server" CommandName="cmdPage" CommandArgument='<%# Container.DataItem %>' Text='<%# Container.DataItem %>' ToolTip='<%# Container.DataItem %>' CssClass="linkPage"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </span>
                            <asp:LinkButton ID="lnkbtnLastTop" runat="server" meta:resourcekey="lnkbtnLast" OnClick="lnkbtnLast_Click" CssClass="linkPageLead" Visible="false"></asp:LinkButton>
                        </div>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="pnlProdRelProdItems" runat="server" CssClass="divProdRelProdItems">
                    <asp:Repeater ID="rptProd" runat="server" OnItemDataBound="rptProd_ItemDataBound" OnItemCommand="rptProd_ItemCommand">
                        <HeaderTemplate><table cellpadding="0" cellspacing="0" class="relprodTbl"></HeaderTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litRowOpen" runat="server" Visible="false"></asp:Literal>
                            <td id="tdRelProdItem" runat="server" class="tdRelProdItem">
                            <label runat="server" id="lblProd">
                            <asp:Panel ID="pnlProduct" runat="server" CssClass="divIndProd">
                                <div class="divRelProdImgProd">
                                    <div class="divRelProdImgProdInner">
                                        <asp:Image runat="server" ID="imgProd" />
                                        <%--<asp:ImageButton ID="imgbtnProd" runat="server" CommandName="cmdSelect" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PROD_ID") + "|" + DataBinder.Eval(Container.DataItem, "PROD_NAME") + "|" + DataBinder.Eval(Container.DataItem, "PROD_IMAGE") %>' />--%>
                                    </div>
                                </div>
                                <div class="divRelProdName"><asp:Literal ID="litProdName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PROD_NAME") %>'></asp:Literal></div>
                                <asp:CheckBox ID="chkboxSelect" runat="server" />
                                <asp:HiddenField ID="hdnId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "PROD_ID") %>' />
                                <%--<asp:LinkButton ID="lnkbtnSelect" runat="server" Text="select" ToolTip="select" CommandName="cmdSelect" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PROD_ID") + "|" + DataBinder.Eval(Container.DataItem, "PROD_NAME") + "|" + DataBinder.Eval(Container.DataItem, "PROD_IMAGE") %>'></asp:LinkButton>--%>
                            </asp:Panel>
                            </label>
                            </td>
                            <asp:Literal ID="litRowClose" runat="server" Visible="false"></asp:Literal>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Literal ID="litRowClose2" runat="server" Visible="false"></asp:Literal>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </asp:Panel>
                <asp:Panel ID="pnlProdRelProdPagingBottom" runat="server" CssClass="divPaging">
                    <asp:Panel ID="pnlProdRelProdPaginationBottom" runat="server" CssClass="divProdRelProdPagingPagination">
                        <div class="divPagingInner">
                            <asp:LinkButton ID="lnkbtnFirstBottom" runat="server" meta:resourcekey="lnkbtnFirst" OnClick="lnkbtnFirst_Click" CssClass="linkPageLead" Visible="false"></asp:LinkButton>
                            <span class="spanPagination">
                                <asp:Repeater ID="rptPaginationBottom" runat="server" OnItemDataBound="rptPagination_ItemDataBound" OnItemCommand="rptPagination_ItemCommand">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkbtnPage" runat="server" CommandName="cmdPage" CommandArgument='<%# Container.DataItem %>' Text='<%# Container.DataItem %>' ToolTip='<%# Container.DataItem %>' CssClass="linkPage"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </span>
                            <asp:LinkButton ID="lnkbtnLastBottom" runat="server" meta:resourcekey="lnkbtnLast" OnClick="lnkbtnLast_Click" CssClass="linkPageLead" Visible="false"></asp:LinkButton>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlSelectedProd" runat="server" CssClass="divListing">
        <div class="divSectionTitle">Selected Product(s):</div>
        <asp:Panel ID="pnlProdSel" runat="server" CssClass="divProdSelList">
            <asp:Panel ID="pnlProdSelNoFound" runat="server" CssClass="divProdRelNoFound" Visible="false">
                <asp:Literal ID="litSelNoFound" runat="server" meta:resourcekey="litNoFound"></asp:Literal>
            </asp:Panel>
            <asp:Panel ID="pnlProdSelList" runat="server" CssClass="divProdListItems">
                <asp:Repeater ID="rptProdSel" runat="server" OnItemDataBound="rptProdSel_ItemDataBound" OnItemCommand="rptProdSel_ItemCommand">
                    <HeaderTemplate><table cellpadding="0" cellspacing="0" class="relprodTbl"></HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="litRowOpen" runat="server" Visible="false"></asp:Literal>
                        <td id="tdRelProdItem" runat="server" class="tdRelProdItem">
                        <asp:Panel ID="pnlProdSel" runat="server" CssClass="divIndProd">
                            <div class="divRelProdImgProd">
                                <div class="divRelProdImgProdInner">
                                    <asp:ImageButton ID="imgbtnProd" runat="server" CommandName="cmdRemove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "RELPROD_ID") %>' />
                                </div>
                            </div>
                            <div class="divRelProdName"><asp:Literal ID="litProdSelName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RELPROD_NAME") %>' ></asp:Literal></div>
                            <asp:LinkButton ID="lnkbtnRemove" runat="server" Text="remove" ToolTip="remove" CommandName="cmdRemove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "RELPROD_ID") %>'></asp:LinkButton>
                        </asp:Panel>
                        </td>
                        <asp:Literal ID="litRowClose" runat="server" Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Literal ID="litRowClose2" runat="server" Visible="false"></asp:Literal>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
            <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
        </asp:Panel>
    </asp:Panel>
</asp:Panel>

<script type="text/javascript">
    $(function () {
        jQuery(".filter").detach().appendTo('.main-content__header');
    })
</script>
