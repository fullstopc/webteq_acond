﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrIndEvent.ascx.cs" Inherits="ctrl_ucUsrIndEvent" %>

<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>lazysizes.min.js" type="text/javascript"></script>
<script>
    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.expand = 9;

    $(function () {
        $(".photo .grid-item-image > a").magnificPopup({
            type: 'image', closeOnContentClick: true,
            closeBtnInside: false,
            gallery: { enabled: true }
        });
        $(".video").magnificPopup({
            delegate: " .grid-item-image > a", type: 'iframe', closeOnContentClick: true,
            closeBtnInside: false,
            gallery: { enabled: true }
        });
    });
</script>


<asp:Panel ID="pnlIndEvent" runat="server" CssClass="divIndEventOuter">
    <asp:Panel ID="pnlIndEventContent" runat="server" CssClass="divIndEventContent">
        <asp:Panel ID="pnlIndEventTop" runat="server" CssClass="divIndEventTopTop">
            <asp:Panel ID="pnlBackToListTop" runat="server" CssClass="divBackToListTop">
                <asp:Panel ID="pnlEventTopDescInnerTopRight" runat="server" CssClass="divEventTopDescInnerTopRight">
                    <asp:Panel ID="pnlIndEvtNext" runat="server" CssClass="divIndEvtNext">
                        <asp:LinkButton ID="lnkbtnIndEvtNext" runat="server" ToolTip="Next" Text="" CssClass="button button-small button-flex button-icon-right button-next ">
                            <span>Next</span><i class="material-icons">navigate_next</i>
                        </asp:LinkButton>
                    </asp:Panel>
                    <asp:Panel ID="pnlIndEvtPrev" runat="server" CssClass="divIndEvtPrev">
                        <asp:LinkButton ID="lnkbtnIndEvtPrev" runat="server" ToolTip="Previous" Text="" CssClass="button button-small button-flex button-icon-left button-prev">
                            <i class="material-icons">navigate_before</i><span>Prev</span>
                        </asp:LinkButton>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="pnlIndEvtBack" runat="server" class="divIndEvtBack">
                    <asp:HyperLink ID="hypIndEvtBack" runat="server" Text="Back to Listing" ToolTip="Back to Listing" CssClass="btnIndEventBack"></asp:HyperLink>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlEventHeader" runat="server" CssClass="divEventHeader">
                <h2><asp:Literal ID="litIndEventTitle" runat="server"></asp:Literal></h2>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnlIndEventDate" runat="server" CssClass="divIndEventDate">
            <h3><asp:Literal ID="litEventDate" runat="server"></asp:Literal></h3>
        </asp:Panel>
        <asp:Panel ID="pnlIndEventDetails" runat="server" CssClass="divIndEventDetails">
            <asp:Panel ID="pnlEventImage" runat="server" CssClass="divIndEventImage" Visible="false">
                <asp:Panel ID="pnlIndEventImg" runat="server" CssClass="divIndEventImg">
                    <div class="divIndEventImgInner">
                        <asp:Image ID="imgEvent" runat="server" BorderWidth="0" />
                    </div>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlEventDetails" runat="server" CssClass="divEventDetails">
                <asp:Panel ID="pnlEventTopDesc" runat="server">
                    <asp:Panel ID="pnlEventTopDescInner" runat="server">
                        <asp:Panel ID="pnlEventTopDescInnerTop" runat="server" CssClass="divEventTopDescInnerTop" Visible="false">
                            <asp:Panel ID="pnlEventTopDescInnerTopLeft" runat="server" CssClass="divEventTopDescInnerTopLeft">
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlEventTopDescInnerBottom" runat="server" CssClass="divEventTopDescInnerBottom">
                            <asp:Panel ID="pnlIndEventSnapshot" runat="server" CssClass="divIndEventSnapshot">
                                <p>
                                    <asp:Literal ID="litIndEventSnapShot" runat="server"></asp:Literal>
                                </p>
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnlIndEventDetailsBtm" runat="server" CssClass="divIndEventDetailsBtm" Visible="false">
            <asp:Panel ID="pnlIndEventDesc" runat="server" CssClass="divIndEventDesc">
                <asp:Literal ID="litIndEventDesc" runat="server"></asp:Literal>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnlIndEventArticle" runat="server" CssClass="divIndEventArticle" Visible="false">
            <asp:Repeater ID="rptArticle" runat="server" OnItemDataBound="rptArticle_ItemDataBound" OnItemCommand="rptArticle_ItemCommand">
                <ItemTemplate>
                    <asp:HyperLink ID="hypArticle" CssClass="lnkbtnArticle thickbox" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "HIGHART_TITLE") %>' ToolTip='<%# DataBinder.Eval(Container.DataItem, "HIGHART_TITLE") %>'></asp:HyperLink>
                </ItemTemplate>
                <SeparatorTemplate><span class="spanArticleSplitter">|</span></SeparatorTemplate>
            </asp:Repeater>
        </asp:Panel>
    </asp:Panel>

    <asp:Panel ID="pnlIndEventAlbum" runat="server" CssClass="divIndEvent" Visible="false">
        <asp:Panel ID="pnlIndEventAlbumTop" runat="server" CssClass="divIndEventTop">
            <asp:Panel ID="pnlIndEventAlbumHdr" runat="server" CssClass="divIndEventHdr">
                <asp:Panel ID="pnlIndEventAlbumHdrInner" runat="server" CssClass="divIndEventHdrInnerPhoto">
                    <div class="event-title">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-camera fa-stack-1x"></i>
                        </span>
                        <h3><asp:Literal ID="litIndEvtAlbumHdr" runat="server"></asp:Literal></h3>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlIndEventAlbumImgCount" runat="server" CssClass="divIndEventImgCount" Visible="false">
                    <asp:Image ID="imgGallery" runat="server" />
                </asp:Panel>
                <asp:Panel ID="pnlIndEventAlbumCount" runat="server" CssClass="divIndEventCount" Visible="false">
                    <asp:Literal ID="litEvtGallCount" runat="server"></asp:Literal>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlIndPaginationGall" runat="server" CssClass="divIndPagination">
                <div class="divIndEvtListPaginationWrap">
                    <div class="divEvtPageLast">
                        <asp:LinkButton ID="lnkbtnLastGallTop" runat="server" Text=">>" ToolTip="Last" CssClass="lnkbtnLast" OnClick="lnkbtnLastGall_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnNextGallTop" runat="server" Text=">" ToolTip="Next" CssClass="lnkbtnNext" OnClick="lnkbtnNextGall_Click"></asp:LinkButton>
                    </div>
                    <div class="divListIndPagination">
                        <div class="divListIndPaginationInner">
                            <asp:Repeater ID="rptPaginationGall" runat="server" OnItemDataBound="rptPaginationGall_ItemDataBound" OnItemCommand="rptPaginationGall_ItemCommand">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtnPageNo" runat="server" CommandName="cmdPage" CommandArgument="<% #Container.DataItem%>" Text="<% #Container.DataItem%>" ToolTip="<% #Container.DataItem%>" CssClass="btnEventPagination"></asp:LinkButton>
                                </ItemTemplate>
                                <SeparatorTemplate>
                                    <div class="divIndPaginationSplitter" visible="true"></div>
                                </SeparatorTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div class="divEvtPageFirst">
                        <asp:LinkButton ID="lnkbtnPrevGallTop" runat="server" Text="<" ToolTip="Prev" CssClass="lnkbtnPrev" OnClick="lnkbtnPrevGall_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnFirstGallTop" runat="server" Text="<<" ToolTip="First" CssClass="lnkbtnFirst" OnClick="lnkbtnFirstGall_Click"></asp:LinkButton>
                    </div>
                </div>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnlIndEventAlbumItems" runat="server" CssClass="grid-items photo">
            <asp:Repeater ID="rptAlbum" runat="server" OnItemDataBound="rptAlbum_ItemDataBound">
                <ItemTemplate>
                    <asp:Panel ID="pnlIndEvtAlbumItem" runat="server" CssClass="grid-item">
                        <asp:Panel ID="pnlAlbumImgOuter" runat="server" CssClass="">
                            <asp:Panel ID="pnlAlbumImg" runat="server" CssClass="grid-item-image">
                                <asp:HyperLink ID="hypAlbumImg" runat="server" CssClass="" data-group="1">
                                    <asp:Image ID="imgAlbumImage" runat="server" BorderWidth="0" />
                                </asp:HyperLink>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlIndEvtAlbumTitle" runat="server" CssClass="divIndEvtAlbumTitle" Visible="false">
                            <asp:Literal ID="litIndEvtAlbumTitle" runat="server"></asp:Literal>
                        </asp:Panel>
                    </asp:Panel>
                </ItemTemplate>
            </asp:Repeater>
        </asp:Panel>
    </asp:Panel>

    <asp:Panel ID="pnlIndEventVideo" runat="server" CssClass="divIndEvent" Visible="False">
        <asp:Panel ID="pnlIndEventVideoTop" runat="server" CssClass="divIndEventTop">
            <asp:Panel ID="pnlIndEventVideoHdr" runat="server" CssClass="divIndEventHdr">
                <asp:Panel ID="pnlIndEventVideoHdrInner" runat="server" CssClass="divIndEventHdrInnerVideo">
                    <div class="event-title">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-video-camera fa-stack-1x"></i>
                        </span>
                        <h3><asp:Literal ID="litIndEvtVideoHdr" runat="server"></asp:Literal></h3>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlIndEventVideoImgCount" runat="server" CssClass="divIndEventImgCount" Visible="false">
                    <asp:Image ID="imgVideo" runat="server" />
                </asp:Panel>
                <asp:Panel ID="pnlIndEventVideoCount" runat="server" CssClass="divIndEventCount" Visible="false">
                    <asp:Literal ID="litEvtVideoCount" runat="server"></asp:Literal>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlIndPaginationVideo" runat="server" CssClass="divIndPagination" Visible="false">
                <div class="divIndEvtListPaginationWrap">
                    <div class="divEvtPageLast">
                        <asp:LinkButton ID="lnkbtnLastVidTop" runat="server" Text=">>" ToolTip="Last" CssClass="lnkbtnLast" OnClick="lnkbtnLastVid_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnNextVidTop" runat="server" Text=">" ToolTip="Next" CssClass="lnkbtnNext" OnClick="lnkbtnNextVid_Click"></asp:LinkButton>
                    </div>
                    <div class="divListIndPagination">
                        <div class="divListIndPaginationInner">
                            <asp:Repeater ID="rptPaginationVid" runat="server" OnItemDataBound="rptPaginationVid_ItemDataBound" OnItemCommand="rptPaginationVid_ItemCommand">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtnPageNo" runat="server" CommandName="cmdPage" CommandArgument="<% #Container.DataItem%>" Text="<% #Container.DataItem%>" ToolTip="<% #Container.DataItem%>" CssClass="btnEventPagination"></asp:LinkButton>
                                </ItemTemplate>
                                <SeparatorTemplate>
                                    <div class="divIndPaginationSplitter" visible="true"></div>
                                </SeparatorTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div class="divEvtPageFirst">
                        <asp:LinkButton ID="lnkbtnPrevVidTop" runat="server" Text="<" ToolTip="Prev" CssClass="lnkbtnPrev" OnClick="lnkbtnPrevVid_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnFirstVidTop" runat="server" Text="<<" ToolTip="First" CssClass="lnkbtnFirst" OnClick="lnkbtnFirstVid_Click"></asp:LinkButton>
                    </div>
                </div>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnlIndEventVideoItems" runat="server" CssClass="grid-items video">
            <asp:Repeater ID="rptVideo" runat="server" OnItemDataBound="rptVideo_ItemDataBound">
                <ItemTemplate>
                    <asp:Panel ID="pnlIndEvtVideoItem" runat="server" CssClass="grid-item">
                        <asp:Panel ID="pnlVideoImgOuter" runat="server" CssClass="">
                            <asp:Panel ID="pnlVideoImg" runat="server" CssClass="grid-item-image">
                                <asp:HyperLink ID="hypVideoImg" runat="server" data-group="2">
                                    <asp:Image ID="imgVideoImage" runat="server" BorderWidth="0" />
                                    <div class="divWaterMark">
                                        <asp:Image ID="imgVideoWatermark" runat="server" />
                                    </div>
                                    <%--<div ID="imgPlayIcon" runat="server" class="playIcon"></div>--%>
                                </asp:HyperLink>
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                </ItemTemplate>
            </asp:Repeater>
        </asp:Panel>
    </asp:Panel>

    <asp:Panel ID="pnlIndEventComment" runat="server" CssClass="divIndEvent" Visible="false">
        <asp:Panel ID="pnlIndEventCommentTop" runat="server" CssClass="divIndEventTop">
            <asp:Panel ID="pnlIndEventCommentHdr" runat="server" CssClass="divIndEventHdr">
                <asp:Panel ID="pnlIndEventCommentHdrInner" runat="server" CssClass="divIndEventHdrInnerCmmt">
                    <div class="event-title">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-commenting fa-stack-1x"></i>
                        </span>
                        <h3><asp:Literal ID="litIndEvtCommentHdr" runat="server"></asp:Literal></h3>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlIndEventCommentImgCount" runat="server" CssClass="divIndEventImgCount" Visible="false">
                    <asp:Image ID="imgComment" runat="server" />
                </asp:Panel>
                <asp:Panel ID="pnlIndEventCommentCount" runat="server" CssClass="divIndEventCount" Visible="false">
                    <asp:Literal ID="litEvtCommentCount" runat="server"></asp:Literal>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>

        <asp:Panel ID="pnlIndEventCommentItems" runat="server" CssClass="event-comments">
            <asp:Repeater ID="rptComment" runat="server" OnItemDataBound="rptComment_ItemDataBound">
                <ItemTemplate>
                    <div class="event-comment">
                        <div class="event-comment-author">
                            <div class="event-comment-author-name">
                                <h4><asp:Literal ID="litCommentPostedName" runat="server"></asp:Literal></h4>
                            </div>
                            <div class="event-comment-postdate">
                                <h4><asp:Literal ID="litCommentPostedDate" runat="server"></asp:Literal></h4>
                            </div>
                        </div>
                        <div class="event-comment-contet">
                            <p><asp:Literal ID="litCommentPosted" runat="server"></asp:Literal></p>
                        </div>                            
                        <asp:Panel runat="server" ID="pnlIndEvtReply" CssClass="event-comment-reply" Visible="false">
                            <i class="material-icons icon">reply</i>
                            <div><asp:Literal ID="litReplyPosted" runat="server"></asp:Literal></div>
                        </asp:Panel>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </asp:Panel>
    </asp:Panel>

    <asp:Panel ID="pnlIndEventPostCommentForm" runat="server" CssClass="divIndEventPostCommentForm">
        <asp:Panel ID="pnlIndEventPostCommentFormHdr" runat="server" CssClass="divIndEventCommentHdr">
            <h2><asp:Literal ID="litPostCommentFormHdr" runat="server"></asp:Literal></h2>
        </asp:Panel>
        <asp:Panel ID="pnlPostCommentForm" runat="server">
            <table cellpadding="0" cellspacing="0" border="0" class="event-comment-form">
                <tr>
                    <%--<td class="tdLabelCommentForm">Name :</td>--%>
                    <td>
                        <asp:TextBox ID="txtPostCommName" runat="server" MaxLength="250" CssClass="text_Commentsmall" placeholder="Name" TabIndex="1"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvPostCommName" runat="server" ErrorMessage="<br/>Please enter your name." ControlToValidate="txtPostCommName" CssClass="errmsg" Display="Dynamic" ValidationGroup="comment"></asp:RequiredFieldValidator>
                    </td>
                    <td width="20"></td>
                    <%--<td class="tdLabelCommentForm">Email :</td>--%>
                    <td>
                        <asp:TextBox ID="txtPostCommEmail" runat="server" MaxLength="250" CssClass="text_Commentsmall" placeholder="Email" TabIndex="2"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvPostCommEmail" runat="server" ErrorMessage="<br/>Please enter your email." ControlToValidate="txtPostCommEmail" CssClass="errmsg" Display="Dynamic" ValidationGroup="comment"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revPostCommEmail" runat="server" ErrorMessage="<br/>Please enter a valid email." ControlToValidate="txtPostCommEmail" CssClass="errmsg" Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="comment"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <%--<td class="tdLabelCommentForm">Comment :</td>--%>
                    <td colspan="3">
                        <asp:TextBox ID="txtPostComment" runat="server" CssClass="text_Commentbig" TextMode="MultiLine" Rows="5" placeholder="Comment" TabIndex="3"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvPostComment" runat="server" ErrorMessage="<br/>Please enter your comment." ControlToValidate="txtPostComment" CssClass="errmsg" Display="Dynamic" ValidationGroup="comment"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td colspan="3">
                        <img src="../img/cmn/trans.gif" style="width: 1px; height: 1px;" /></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:LinkButton ID="lnkbtnComment" runat="server" Text="Post Comment" TabIndex="4" ToolTip="Post Comment" CssClass="button" OnClick="lnkbtnComment_Click" ValidationGroup="comment"></asp:LinkButton></td>
                </tr>
                <tr id="trAck" runat="server" visible="false">
                    <td colspan="3">
                        <asp:Literal ID="litAck" runat="server"></asp:Literal></td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>
</asp:Panel>
