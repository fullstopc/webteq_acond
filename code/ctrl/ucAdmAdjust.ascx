﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmAdjust.ascx.cs" Inherits="ctrl_ucAdmAdjust" %>

<script type="text/javascript">
    var txtProdPromStartDate;

    function initSect3() {
        txtProdPromStartDate = document.getElementById("<%= txtDate.ClientID %>");

        $(function() {
            $("#<% =txtDate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });

            $("#<% =txtDate.ClientID %>").datepicker({ changeMonth: true });
            $("#<% =txtDate.ClientID %>").datepicker({ changeYear: true });

                //getter
            var changeMonth = $("#<% =txtDate.ClientID %>").datepicker('option', 'changeMonth');
            var changeYear = $("#<% =txtDate.ClientID %>").datepicker('option', 'changeYear');

                //setter
            $("#<% =txtDate.ClientID %>").datepicker('option', 'changeMonth', true);
            $("#<% =txtDate.ClientID %>").datepicker('option', 'changeYear', true);
        });
    }    

    function validateCode_client(source, args) {
        var prodCodeRE = /<% =clsAdmin.CONSTADMPRODCODERE %>/;

        if (args.Value.match(prodCodeRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "<br />Please enter only alphanumeric.";
            source.innerHTML = "<br />Please enter only alphanumeric.";
        }
    }

    function validateUnique_client(source, args) {
        var param = "{ 'strAdjustNo': '" +$("#<%= txtNo.ClientID %>").val()+ "','strMode':'<%= mode%>','strAdId':'<%= adId%>'}";
        var boolFound = false;
        $.ajax({
            type: "POST",
            url: wsBase + "wsSearchProducts.asmx/isUniqueAdjustNo",
            dataType: "json",
            data: param,
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function(data) {
                var obj = JSON.parse(data.d);

                if (obj.isFound == "1") {
                    boolFound = false;
                }
                else {
                    boolFound = true;
                }
            },
            error: function(xmlHttpRequest, status, err) {
                //alert("error " + err);
            }
        });
        args.IsValid = boolFound;
    }
    
    function setAutocomplete(target) {
        $.ajax({
            type: "POST",
            url: wsBase + "wsSearchProducts.asmx/getCodeList",
            dataType: "json",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            success: function(data) {
                var strCodeList = data.d.split(',');
                $("#" + target).autocomplete({
                    source: strCodeList,
                    change: function(event, ui) {
                        fillForm(target);
                    }
                });
            },
            error: function(xmlHttpRequest, status, err) {
                //alert("error " + err);
            }
        });
    }


    function fillForm(source) {
        var strCode = $("#" + source).val().split(strDefaultSeparator)[0];
        var strName = ($("#" + source).val().split(strDefaultSeparator)[1]);
        strName = strName === undefined ? "" : strName
        
        var tdList = $("#" + source).parent().parent().children();
        var intId = 0;
        
        // fill product details
        var param = "{ 'strCode': '" + strCode + "' , 'strName':'" + strName + "'}";
        $.ajax({
            type: "POST",
            url: wsBase + "wsSearchProducts.asmx/getNameByCode",
            dataType: "json",
            data: param,
            contentType: "application/json; charset=utf-8",
            success: function(data) {
                if (data.d != "") {
                    var strName = data.d.split(strDefaultSeparator);
                    if (tdList.length > 0) {
                        tdList.find("input:not(.code)").removeClass("disabled").attr("disabled", "");
                        tdList.find("#spanUOM").text(strName[1]);
                       
                        intId = strName[0];
                        $(tdList[0]).find("input:hidden").val(intId);

                        var intQty = tdList.find("input.qty").val();
                        if (isNaN(intQty) || intQty == "") {
                            intQty = 0;
                        }

                        if (intQty == 0) {
                            tdList.find("input.qty").val("1");
                            intQty = 1;
                        }
                    }
                }
                else {
                    if (tdList.length > 0) {
                        tdList.find("input:not(.code)").val("").addClass("disabled").attr("disabled", "disabled"); ;
                        tdList.find("select").val("");
                        tdList.find("option").remove();
                    }
                }
            },
            error: function(xmlHttpRequest, status, err) {
                //alert("error " + err);
            }
        });
        
    }

    var intIndex = 0;
    function cloneForm(target) {
        var cloneItem = $('#' + target).clone(false, true);
        cloneItem.attr("id", "trForm" + intIndex);

        cloneItem
            .find("option")
            .remove()
            .end()
            .find("*").each(function() {
                $(this).val("")
                       .attr("id", function(_, id) {
                           if (id) {
                               $(this).attr("name", id + intIndex);
                               return id + intIndex;
                           }

                           return;
                       });

                if ($(this).hasClass("code")) {
                    setAutocomplete($(this).attr("id"));
                }

                if ($(this).hasClass("color") || $(this).hasClass("size")) {
                    $(this).change(function() {
                        setSelectedValue($(this).attr("id"));
                    });
                }
            });

        $('#tbForm').append(cloneItem);
        intIndex++;
    }
</script>

<asp:Panel ID="pnlOrder" runat="server">
    <asp:Panel ID="pnlActionPrint" runat="server" CssClass="divActionPrint" Visible="false">
        <asp:LinkButton ID="imgbtnPrint" runat="server" AlternateText="Print" ToolTip="Print" Text="Print" CssClass="btn2 btnPrint" OnClick="imgbtnPrint_Click" />
    </asp:Panel>
    <asp:Panel ID="pnlTop" runat="server" CssClass="divTop">
        <table cellpadding="0" cellspacing="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr">Inventory Receive Details</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdMax2" valign="top" colspan="2">
                    <table cellpadding="0" cellspacing="0" class="formTbl">
                        <tr>
                            <td class="tdLabel nobr">Adjustment Date<span class="attention_compulsory">*</span>:</td>
                            <td class="tdMax">
                                <asp:TextBox ID="txtDate" runat="server" CssClass="text_fullwidth compulsory" MaxLength="20"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvDate" runat="server" ErrorMessage="<br />Please select Date." ControlToValidate="txtDate" CssClass="errmsg" Display="Dynamic" ValidationGroup="vgAdjust"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel nobr">Adjustment No.<span class="attention_unique">*</span>:</td>
                            <td class="tdMax">
                                <asp:TextBox ID="txtNo" runat="server" CssClass="text_fullwidth uniq" MaxLength="20"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvNo" runat="server" ControlToValidate="txtNo" ErrorMessage="<br />Please enter adjustment no." CssClass="errmsg" Display="Dynamic" ValidationGroup="vgAdjust"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvNo" runat="server" ControlToValidate="txtNo" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateCode_client" OnServerValidate="validateCode_server" ValidationGroup="vgAdjust"></asp:CustomValidator>
                                <asp:CustomValidator ID="cvNoUnique" runat="server" ControlToValidate="txtNo" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateUnique_client" ValidationGroup="vgAdjust" ErrorMessage="<br/>This Adjustment No. is repeated" ></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel">Title<span class="attention_compulsory">*</span>:</td>
                            <td>
                                <asp:TextBox ID="txtTitle" runat="server" CssClass="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ErrorMessage="<br />Please enter adjustment title." CssClass="errmsg" Display="Dynamic" ControlToValidate="txtTitle" ValidationGroup="vgAdjust"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel">Adjust By<span class="attention_compulsory">*</span>:</td>
                            <td>
                                <asp:TextBox ID="txtAdjustBy" runat="server" CssClass="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvAdjustBy" runat="server" ErrorMessage="<br />Please enter adjust by." CssClass="errmsg" Display="Dynamic" ControlToValidate="txtAdjustBy" ValidationGroup="vgAdjust"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="tdSectionHdr"><a id="item" name="item"></a>Items Details</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <table cellpadding="0" cellspacing="0" class="formTbl" id="tblAdjustItemForm">
                        <tr>
                            <td colspan="2" align="right"><asp:HyperLink ID="hypAdd" runat="server" Text="Add" ToolTip="Add" href="#item"></asp:HyperLink></td>
                        </tr>
                        <tr>
                            <td>Product Code | Product Name</td>
                            <td>Quantity</td>
                        </tr>
                        <tbody id="tbForm">
                            <asp:Repeater ID="rptItems" runat="server" OnItemDataBound="rptItems_ItemDataBound">
                                <ItemTemplate>
                                    <tr>
                                        <td colspan="2">
                                            <asp:HiddenField ID="hdnId2" runat="server" />
                                            <asp:HiddenField ID="hdnIdOld" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trForm" runat="server">
                                        <td>
                                            <asp:TextBox ID="txtProdCode" runat="server" CssClass="text_medium2 code" MaxLength="250"></asp:TextBox>
                                            <asp:HiddenField ID="hdnId" runat="server" />
                                        </td>
                                        <td>
                                            <div id="divQtyContainer" class="divQtyContainer" runat="server">
                                                <asp:TextBox ID="txtQty" runat="server" CssClass="text_small qty" MaxLength="5"></asp:TextBox>
                                                <span ID="spanUOM" class="spanUOM" runat="server">Unit</span>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tfoot>
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="vgAdjust"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CssClass="btn btnCancel" OnClick="lnkbtnDelete_Click" Visible="false"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>
