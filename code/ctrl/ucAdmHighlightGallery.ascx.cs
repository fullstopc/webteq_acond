﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;

public partial class ctrl_ucAdmHighlightGallery : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode;
    protected int _highId;
    protected DataTable _dtHighImage;
    protected string _pageListingURL = "";
    protected int _noOfImageInRow = 5;

    protected int _numOfPics;
    protected int _limit;
    protected int intFileMaxLength;

    protected string highImageTitle;
    protected string highImageTitleJp;
    protected string highImageTitleMs;
    protected string highImageTitleZh;
    protected string highImage;
    protected int intImageCount = 0;
    protected int _type = 0;
    protected int intUploadCount = 0;
    protected string strFileExt;
    protected string strEventGalleryList = "";

    protected string strTempPath = HttpContext.Current.Session[clsAdmin.CONSTPROJECTUPLOADPATH] +ConfigurationManager.AppSettings["uplBase2"] +clsAdmin.CONSTADMTEMPFOLDER.ToString() + "/";
    protected string strGalleryPath = HttpContext.Current.Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMHIGHFOLDER.ToString() + "/" + clsAdmin.CONSTHIGHGALLERYFOLDER + "/";
    protected string strThumbPath = HttpContext.Current.Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMHIGHFOLDER + "/" + clsAdmin.CONSTHIGHGALLERYFOLDER + "/" + clsAdmin.CONSTTHUMBNAILFOLDER + "/";
    protected string strShowImage = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=";
    protected clsHighlight high = new clsHighlight();
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName2(true); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int highId
    {
        get
        {
            if (ViewState["HIGHID"] == null)
            {
                return _highId;
            }
            else
            {
                return int.Parse(ViewState["HIGHID"].ToString());
            }
        }
        set { ViewState["HIGHID"] = value; }
    }

    public DataTable dtHighImage
    {
        get
        {
            if (ViewState["DTHIGHIMAGE"] == null)
            {
                return _dtHighImage;
            }
            else
            {
                return (DataTable)ViewState["DTHIGHIMAGE"];
            }
        }
        set { ViewState["DTHIGHIMAGE"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if (ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }

    public int limit
    {
        get
        {
            if (ViewState["LIMIT"] == null)
            {
                return _limit;
            }
            else
            {
                return Convert.ToInt16(ViewState["LIMIT"]);
            }
        }
        set { ViewState["LIMIT"] = value; }
    }

    public int numOfPics
    {
        get
        {
            if (ViewState["NOOFPICS"] == null)
            {
                return _numOfPics;
            }
            else
            {
                return Convert.ToInt16(ViewState["NOOFPICS"]);
            }
        }
        set { ViewState["NOOFPICS"] = value; }
    }

    public int type
    {
        get
        {
            if (ViewState["TYPE"] == null)
            {
                return _type;
            }
            else
            {
                return Convert.ToInt16(ViewState["TYPE"]);
            }
        }
        set { ViewState["TYPE"] = value; }
    }

    public int noOfImageInRow
    {
        get
        {
            if (ViewState["NOOFIMAGEINROW"] == null)
            {
                return _noOfImageInRow;
            }
            else
            {
                return Convert.ToInt16(ViewState["NOOFIMAGEINROW"]);
            }
        }
        set { ViewState["NOOFIMAGEINROW"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lnkbtnImage_Click(object sender, EventArgs e)
    {
    }

    protected void rptHighlightGallery_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }

    protected void rptHighlightGallery_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {

    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        System.Web.Script.Serialization.JavaScriptSerializer json = new System.Web.Script.Serialization.JavaScriptSerializer();
        bool boolEdited = false;
        bool boolSuccess = true;

        int intRecordAffected = 0;
        clsConfig config = new clsConfig();
        try
        {
            bool boolEventGalleryExists = (Session["EVENT_GALLERY"] != null);

            if (boolEventGalleryExists)
            {
                Dictionary<string, EventGalleryDetail> dictEventGalleryDetail = json.Deserialize<Dictionary<string, EventGalleryDetail>>(Session["EVENT_GALLERY"].ToString());
                Dictionary<string, int> dictEventGalleryDetailSort = json.Deserialize<Dictionary<string, int>>(Session["EVENT_GALLERY_SORTING"].ToString());

                foreach (KeyValuePair<string, EventGalleryDetail> entry in dictEventGalleryDetail)
                {
                    EventGalleryDetail eventGalleryDetail = entry.Value;
                    if(eventGalleryDetail.type == "session" && !eventGalleryDetail.isDelete) { 
                        intRecordAffected = high.addHighlightGallery(highId, eventGalleryDetail.title, "", "", "", eventGalleryDetail.filename, 0);
                        int intThumbWidth = config.thumbnailWidth;
                        int intThumbHeight = config.thumbnailHeight;

                        if (intRecordAffected == 1)
                        {
                            File.Move(strTempPath + eventGalleryDetail.filename, strGalleryPath + eventGalleryDetail.filename);
                            string strThumbnail = eventGalleryDetail.filename.Substring(0, eventGalleryDetail.filename.LastIndexOf('.'));
                            if (intThumbWidth > 0 && intThumbHeight > 0)
                            {
                                clsMis.saveThumbnail(strGalleryPath + eventGalleryDetail.filename, strThumbPath, ref strThumbnail, intThumbWidth, intThumbHeight);
                                high.updateHighGalleryThumbnail(high.highGallId, strThumbnail);
                            }
                            intRecordAffected = high.updateGallery(high.highGallId, HttpUtility.UrlDecode(eventGalleryDetail.title), "", "", "", eventGalleryDetail.filename, dictEventGalleryDetailSort[eventGalleryDetail.filename]);

                            boolEdited = true;
                        }
                    }
                    else if(eventGalleryDetail.type == "database")
                    {
                        if (eventGalleryDetail.isDelete) {
                            intRecordAffected = high.deleteHighlightGalleryImage(highId, HttpUtility.UrlDecode(eventGalleryDetail.title), eventGalleryDetail.filename);
                            if (intRecordAffected == 1)
                            {
                                if (File.Exists(strGalleryPath + eventGalleryDetail.filename)) { File.Delete(strGalleryPath + eventGalleryDetail.filename); }
                                if (!string.IsNullOrEmpty(eventGalleryDetail.thumbname))
                                {
                                    if (File.Exists(strThumbPath + eventGalleryDetail.thumbname)) { File.Delete(strThumbPath + eventGalleryDetail.thumbname); }
                                }
                                
                                boolEdited = true;
                            }
                        }
                        else
                        {
                            //if (dictEventGalleryDetailSort[eventGalleryDetail.filename] != eventGalleryDetail.order)
                            {
                                intRecordAffected = high.updateGallery(eventGalleryDetail.id, HttpUtility.UrlDecode(eventGalleryDetail.title), "", "", "", eventGalleryDetail.filename, dictEventGalleryDetailSort[eventGalleryDetail.filename]);
                            }
                            boolEdited = true;
                        }
                    }


                }
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message.ToString());
        }
        Response.Redirect(currentPageName);
    }

    protected void lnkbtnUpload_Click(object sender, EventArgs e)
    {
        
    }

    protected void validateUpload_server(object source, ServerValidateEventArgs args)
    {
        
    }

    protected void cvUpload_PreRender(object sender, EventArgs e)
    {
        
    }

    #endregion


    #region "Methods"
    public void fill()
    {
        double intEvtGallSet = 0;
        if (Session[clsAdmin.CONSTPROJECTEVENTGALLSETTING] != null && Session[clsAdmin.CONSTPROJECTEVENTGALLSETTING] != "")
        {
            if (Double.Parse(Session[clsAdmin.CONSTPROJECTEVENTGALLSETTING].ToString()) > 0)
            {
                intEvtGallSet = Double.Parse(Session[clsAdmin.CONSTPROJECTEVENTGALLSETTING].ToString());
            }
            else
            {
                if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                {
                    if (Double.Parse(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING].ToString()) > 0)
                    {
                        intEvtGallSet = Double.Parse(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING].ToString());
                    }
                }
            }
        }

        intEvtGallSet = intEvtGallSet / 1000;

        if ((!Page.ClientScript.IsStartupScriptRegistered("UploadMaxSize")))
        {
            clsConfig config = new clsConfig();
            string aspectRatio = config.aspectRatio;
            string[] aspectRatioXandY = aspectRatio.Split('/');
            string aspectRatioX = aspectRatioXandY[0];
            string aspectRatioY = aspectRatioXandY[1];

            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "updateMaxSize('" + intEvtGallSet + "'," + aspectRatioX + "," + aspectRatioY + ");";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "UploadMaxSize", strJS, false);
        }

        if (!IsPostBack)
        {
            lnkbtnCancel.OnClientClick = "javascript:document.location.href='" + pageListingURL + "?id=" + highId + "'; return false;";
            Session["EVENT_GALLERY"] = null;
            Session["EVENT_GALLERY_SORTING"] = null;
            bindRptData();
        }

    }

    protected void bindRptData()
    {
        Session["EVENT_GALLERY"] = high.getEventGallery(highId);
        Session["EVENT_GALLERY_SORTING"] = high.getEventGallerySorting();
    }

    
    #endregion
}
