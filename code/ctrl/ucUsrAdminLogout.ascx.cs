﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucUsrAdminLogout : System.Web.UI.UserControl
{
    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ADMID"] != null && Session["ADMID"] != "")
        {
            pnlAdminLogout.Visible = true;

            Literal litCSS = new Literal();
            litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "adminlogout.css' rel='Stylesheet' type='text/css' />";
            this.Page.Header.Controls.Add(litCSS);
        }
    }

    protected void lnkbtnLogout_Click(object sender, EventArgs e)
    {
        Session["ADMID"] = null;

        Response.Redirect(Request.Url.ToString());
    }
    #endregion
}
