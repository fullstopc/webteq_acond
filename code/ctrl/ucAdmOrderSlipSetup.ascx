﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmOrderSlipSetup.ascx.cs" Inherits="ctrl_ucAdmOrderSlipSetup" %>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>watermark.js" type="text/javascript"></script>

<script type="text/javascript">

</script>

<asp:Panel ID="pnlAdmCMSControlPanel" runat="server" CssClass="divAdmCMSControlPanel">
    <asp:Panel ID="pnlFormDetails" runat="server" CssClass="divOrderSlipSetupContainer">
        <div id="divHeaderSetting">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td class="tdSectionHdr" colspan="2">Header Settings</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel">Company Logo</td>
                        <td>
                            <div class="divFileUpload">
                                <div class="divFileUploadAction">
                                    <asp:FileUpload ID="fileCompanyLogo" runat="server" />
                                </div>
                                <span class="spanTooltip">
                                    <span class="icon"></span>
                                    <span class="msg right">
                                        <asp:Literal ID="litUploadRule" runat="server"></asp:Literal>
                                    </span>
                                </span>
                            </div>
                            <asp:HiddenField ID="hdnCompanyLogo" runat="server" />
                            <asp:HiddenField ID="hdnCompanyLogoRef" runat="server" />
                            <asp:CustomValidator ID="cvCompanyLogo" runat="server" ControlToValidate="fileCompanyLogo" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateImage_server" OnPreRender="cvImage_PreRender" ></asp:CustomValidator>
                                            
                            <asp:Panel ID="pnlCompanyLogo" runat="server" Visible="false">
                                <br /><asp:LinkButton ID="lnkbtnDeleteCompanyLogo" runat="server" Text="Delete" ToolTip="Delete" OnCommand="lnkbtnDelete_Click" CausesValidation="false" CommandName="cmdDelete" CommandArgument="cmaCompanyLogo"></asp:LinkButton>
                                <br /><asp:Image ID="imgCompanyLogo" runat="server" />
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Company Name</td>
                        <td><asp:TextBox ID="txtCompanyName" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Address</td>
                        <td>
                            <asp:TextBox ID="txtAddr1" runat="server" CssClass="text_fullwidth"></asp:TextBox>
                            <asp:TextBox ID="txtAddr2" runat="server" CssClass="text_fullwidth"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Tel</td>
                        <td><asp:TextBox ID="txtTel" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Fax</td>
                        <td><asp:TextBox ID="txtFax" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Email</td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="text_fullwidth"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="revEmail" runat="server" CssClass="errmsg" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Please enter valid Email.<br />" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="orderSlipSetup"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Website</td>
                        <td><asp:TextBox ID="txtWebsite" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
                <tbody id="tbodyGST" runat="server">
                    <tr>
                        <td class="tdLabel">Co. Reg. No.</td>
                        <td><asp:TextBox ID="txtCompanyRegNo" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">GST Reg. No.</td>
                        <td><asp:TextBox ID="txtGstRegNo" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="divFooterSetting">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td class="tdSectionHdr" colspan="2">Footer Settings</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel">Line 1</td>
                        <td><asp:TextBox ID="txtLine1" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Line 2</td>
                        <td><asp:TextBox ID="txtLine2" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Line 3</td>
                        <td><asp:TextBox ID="txtLine3" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="">
            <table id="tblAction" class="formTbl tblData" cellpadding="0" cellspacing="0" style="padding-top: 0;">
                <tr>
                    <td class="tdLabel"></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated" style="padding-top:0px;">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="orderSlipSetup"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    
</asp:Panel>
