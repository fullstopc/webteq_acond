﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

public partial class ctrl_ucCmnDataTable : System.Web.UI.UserControl
{
    #region "Properties"
    public int intPageLength = clsAdmin.CONSTADMPAGESIZE;
    #endregion


    #region "Property Methods"
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        fill();
    }

    public void fill()
    {
        includeFile();
        includeExtenstion();

        clsConfig config = new clsConfig();
        if (config.extractItemByNameGroup(clsConfig.CONSTNAMELISTINGSIZE, clsConfig.CONSTGROUPOTHERSETTINGS, 1))
        {
            int intTryParse;
            intPageLength = int.TryParse(Convert.ToString(config.value), out intTryParse) ? intTryParse : clsAdmin.CONSTADMPAGESIZE;
        }

    }

    private void includeFile()
    {
        bool cssIncluded = false;
        bool cssCustomIncluded = false;
        bool jsIncluded = false;

        foreach (Control ctrl in Page.Header.Controls)
        {
            if (ctrl.GetType() == typeof(HtmlLink))
            {
                HtmlLink linkAtual = (HtmlLink)ctrl;
                if (linkAtual.Attributes["href"].Contains("jquery.dataTables.min.css"))
                {
                    cssIncluded = true;
                }
                else if (linkAtual.Attributes["href"].Contains("jquery.dataTables.custom.css"))
                {
                    cssCustomIncluded = true;
                }
            }
        }

        if (!cssIncluded)
        {
            HtmlLink link = new HtmlLink();
            link.Attributes.Add("href", System.Configuration.ConfigurationManager.AppSettings["cssBase"] + "datatable/jquery.dataTables.min.css");
            link.Attributes.Add("type", "text/css");
            link.Attributes.Add("rel", "stylesheet");
            Page.Header.Controls.Add(link);
        }
        if (!cssCustomIncluded)
        {
            HtmlLink link = new HtmlLink();
            link.Attributes.Add("href", System.Configuration.ConfigurationManager.AppSettings["cssBase"] + "datatable/jquery.dataTables.custom.css");
            link.Attributes.Add("type", "text/css");
            link.Attributes.Add("rel", "stylesheet");
            Page.Header.Controls.Add(link);
        }
        Page.ClientScript.RegisterClientScriptInclude("datatable", System.Configuration.ConfigurationManager.AppSettings["jsBase"] + "datatable/jquery.dataTables.js");
    }

    private void includeExtenstion()
    {
        includeColResize();
        //includeButtons();
    }

    private void includeColResize()
    {
        Page.ClientScript.RegisterClientScriptInclude("colresize", System.Configuration.ConfigurationManager.AppSettings["jsBase"] + "datatable/extenstions/ColResize/dataTables.colResize.js");
    }
    private void includeButtons()
    {
        Page.ClientScript.RegisterClientScriptInclude("buttons", System.Configuration.ConfigurationManager.AppSettings["jsBase"] + "datatable/extenstions/Buttons/js/dataTables.buttons.js");
        Page.ClientScript.RegisterClientScriptInclude("buttons_jquery", System.Configuration.ConfigurationManager.AppSettings["jsBase"] + "datatable/extenstions/Buttons/js/buttons.jqueryui.min.js");
        Page.ClientScript.RegisterClientScriptInclude("buttons_html5", System.Configuration.ConfigurationManager.AppSettings["jsBase"] + "datatable/extenstions/Buttons/js/buttons.html5.min.js");
        Page.ClientScript.RegisterClientScriptInclude("buttons_print", System.Configuration.ConfigurationManager.AppSettings["jsBase"] + "datatable/extenstions/Buttons/js/buttons.print.min.js");
    }
    #endregion
}
