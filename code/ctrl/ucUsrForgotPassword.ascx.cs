﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctlr_ucUsrForgotPassword : System.Web.UI.UserControl
{
    #region "Properties"
    public bool isMobile = false;
    protected string _lang = "en";
    #endregion

    #region "Property Methods"
    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void imgbtnSubmit_Click(object sender, EventArgs e)
    {

        string email = txtEmail.Text.Trim();
        clsContestant contestant = new clsContestant();
        bool isSuccess = false;
        bool isEmailExists = true;
        if (contestant.extractData("contestant_email", email))
        {
            string password = RandomPassword.Generate(8, 8);
            if (contestant.resetPassword(contestant.ID, new clsMD5().encrypt(password)))
            {
                contestant.sendEmailUserLogin(contestant.ID, password);
                isSuccess = true;
                Session["EDITED"] = "Kata laluan baru telah dihantar ke e-mel anda.";
            }
        }
        else
        {
            isEmailExists = false;
        }

        if (!isSuccess)
        {
            Session["ERROR"] = GetGlobalResourceObject("GlobalResource", "crud_error").ToString();
            if (!isEmailExists)
            {
                Session["ERROR"] = "E-mel tidak sah. Sila cuba lagi.";
            }
        }
        Response.Redirect(clsMis.getCurrentPageName());
    }

    public void fill()
    {

    }
    #endregion
}