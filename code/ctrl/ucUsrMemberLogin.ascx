<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrMemberLogin.ascx.cs" Inherits="ctrl_ucUsrMemberLogin" %>
<%@ Register Src="~/ctrl/ucUsrLogo.ascx" TagName="UsrLogo" TagPrefix="uc" %>


<asp:Panel ID="pnlLoginForm" runat="server" DefaultButton="lnkbtnLogin" CssClass="loginform">
    <div class="wrapper padding">
        <div class="loginform__logo">
            <uc:UsrLogo ID="ucUsrLogo" runat="server" viewType="MobileView" />
        </div>
        
        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="ack-container error">
            <asp:Literal ID="litAck" runat="server"></asp:Literal>
        </asp:Panel>
        <div class="loginform__title"><h1>Welcome to Acond</h1></div>
        <div class="loginform__subtitle">Please Login</div>
        <div class="loginform__form form__container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form__label">
                        <asp:Label ID="lblEmail" runat="server">
                            <%= GetGlobalResourceObject("LabelResource","lbl_email") %>
                        </asp:Label>:
                    </div>
                    <div class="form__control">
                        <asp:TextBox ID="txtEmailLogin" runat="server" CssClass="text_medium" ValidationGroup="Login" placeholder="Login ID"></asp:TextBox>
                        <i class="material-icons">account_circle</i>
                    </div>
                    <div class="form-error">
                        <asp:RequiredFieldValidator ID="rfvEmailLogin" runat="server" ControlToValidate="txtEmailLogin" Display="Dynamic" ErrorMessage="Please enter Login ID." ValidationGroup="Login" CssClass="errmsg"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form__label">
                        <asp:Label ID="lblPassword" runat="server">
                            <%= GetGlobalResourceObject("LabelResource","lbl_password") %>
                        </asp:Label>:
                    </div>
                    <div class="form__control">
                        <asp:TextBox ID="txtPwdLogin" runat="server" CssClass="text_medium" TextMode="Password" ValidationGroup="Login" placeholder="Password"></asp:TextBox>
                        <i class="material-icons">lock_outline</i>
                    </div>
                    <div class="form-error">
                        <asp:RequiredFieldValidator ID="rfvPwdLogin" runat="server" Display="Dynamic" ControlToValidate="txtPwdLogin" ErrorMessage="Please enter Password" ValidationGroup="Login" CssClass="errmsg"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <asp:LinkButton ID="lnkbtnLogin" runat="server" CssClass="button button-full button-round" OnClick="lnkbtnLogin_Click" ValidationGroup="Login" 
                    Text="<%$ Resources:LabelResource, lbl_login %>" 
                    ToolTip="<%$ Resources:LabelResource, lbl_login %>"></asp:LinkButton>
                </div>
            </div>
            <div class="row" runat="server" visible="false">
                <div class="col">
                    <asp:HyperLink ID="hypRegister" runat="server" CssClass="floated left"
                    Text="<%$ Resources:LabelResource, lbl_registerAsResident %>"
                    ToolTip="<%$ Resources:LabelResource, lbl_registerAsResident %>"></asp:HyperLink>
                </div>
                <div class="col">
                    <asp:HyperLink ID="hypForgot" runat="server" CssClass="floated right"
                    Text="<%$ Resources:LabelResource, lbl_forgotPassword %>"
                    ToolTip="<%$ Resources:LabelResource, lbl_forgotPassword %>"></asp:HyperLink>
                </div>
            </div>
        </div><!-- end of wrapper -->
    </div><!-- end of loginform -->
</asp:Panel>

