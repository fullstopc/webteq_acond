﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucAdmHighlightComment : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _highId;
    protected int _mode;
    protected string _pageListingURL = "";
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int highId
    {
        get
        {
            if (ViewState["HIGHID"] == null)
            {
                return _highId;
            }
            else
            {
                return Convert.ToInt16(ViewState["HIGHID"]);
            }
        }
        set { ViewState["HIGHID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if (ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void rptHighComment_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strPostedByName = DataBinder.Eval(e.Item.DataItem, "HIGHCOMM_POSTEDBYNAME").ToString();
            string strPostedEmail = DataBinder.Eval(e.Item.DataItem, "HIGHCOMM_EMAIL").ToString();
            int intReply = int.Parse(DataBinder.Eval(e.Item.DataItem, "HIGHCOMM_REPLY").ToString());

            Literal litPostedBy = (Literal)e.Item.FindControl("litPostedBy");
            litPostedBy.Text = strPostedByName + " (" + strPostedEmail + ")";

            LinkButton lnkbtnReply = (LinkButton)e.Item.FindControl("lnkbtnReply");
            lnkbtnReply.Text = GetGlobalResourceObject("GlobalResource", "contentAdmReply.Text").ToString();
            lnkbtnReply.ToolTip = GetGlobalResourceObject("GlobalResource", "contentAdmReply.Text").ToString();

            LinkButton lnkbtnRemove = (LinkButton)e.Item.FindControl("lnkbtnRemove");
            lnkbtnRemove.Text = GetGlobalResourceObject("GlobalResource", "contentAdmRemove.Text").ToString();
            lnkbtnRemove.ToolTip = GetGlobalResourceObject("GlobalResource", "contentAdmRemove.Text").ToString();

            lnkbtnRemove.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "admConfirmDeleteComment.Text").ToString() + "');";

            if (intReply != 0)
            {
                Panel pnlActReply = (Panel)e.Item.FindControl("pnlActReply");
                pnlActReply.Visible = false;

                Panel pnlReplied = (Panel)e.Item.FindControl("pnlReplied");
                pnlReplied.Visible = true;

                Panel pnlAction = (Panel)e.Item.FindControl("pnlAction");
                pnlAction.CssClass = "divActBtn";
            }
        }
    }

    protected void rptHighComment_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdReply")
        {
            Panel pnlReply = (Panel)e.Item.FindControl("pnlReply");
            pnlReply.Visible = true;

            //LinkButton lnkbtnReply = (LinkButton)e.Item.FindControl("lnkbtnReply");
            //lnkbtnReply.OnClientClick = "javascript:return false;";
        }
        else if (e.CommandName == "cmdSave")
        {
            int highcommId = int.Parse(e.CommandArgument.ToString());

            TextBox txtReply = (TextBox)e.Item.FindControl("txtReply");
            string strComment = txtReply.Text.Trim();
            strComment = strComment.Replace(Environment.NewLine, "<br />");

            if (!string.IsNullOrEmpty(strComment))
            {
                clsHighlight high = new clsHighlight();
                int intRecordAffected = 0;

                intRecordAffected = high.addHighlightReply(highcommId, highId, strComment, Convert.ToInt16(Session["ADMID"]), Session["ADMEMAIL"].ToString(), Session["ADMEMAIL"].ToString());

                if (intRecordAffected == 1)
                {
                    clsMis.resetListing();
                    high.setHighlightCommentReplied(highcommId);
                }
            }

            bindRptData();
        }
        else if (e.CommandName == "cmdCancel")
        {
            Panel pnlReply = (Panel)e.Item.FindControl("pnlReply");
            pnlReply.Visible = false;
        }
        else if (e.CommandName == "cmdRemove")
        {
            int highCommId = int.Parse(e.CommandArgument.ToString());

            clsHighlight high = new clsHighlight();
            int intRecordAffected = 0;

            intRecordAffected = high.deleteHighlightComment(highCommId);

            if (intRecordAffected == 1)
            {
                high.deleteHighlightReply(highCommId);
            }

            bindRptData();
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        int intAllowComment;
        if (chkAllowComment.Checked) { intAllowComment = 1; }
        else { intAllowComment = 0; }

        Boolean boolEdited = false;
        Boolean boolSuccess = true;
        int intRecordAffected = 0;

        clsHighlight high = new clsHighlight();

        if (!high.isExactSameHighlightComment(highId, intAllowComment))
        {
            intRecordAffected = high.updateHighlightComment(highId, intAllowComment);

            if (intRecordAffected == 1)
            {
                boolEdited = true;
            }
            else
            {
                boolSuccess = false;
            }
        }

        if (boolEdited)
        {
            Session["EDITEDHIGHID"] = high.highId;

            Response.Redirect(Request.Url.ToString());
        }
        else
        {
            if (!boolSuccess)
            {
                high.extractHighlightById(highId, 0);

                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit highlight (" + high.highTitle + "). Please try again.</div>";

                Response.Redirect(Request.Url.ToString());
            }
            else
            {
                Session["EDITEDHIGHID"] = highId;
                Session["NOCHANGE"] = 1;

                Response.Redirect(Request.Url.ToString());
            }
        }
    }
    #endregion


    #region "Methods"
    public void fill(int intMode)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;

                if (int.TryParse(Request["id"], out intTryParse))
                {
                    highId = intTryParse;
                }
                else
                {
                    highId = 0;
                }
            }

            mode = intMode;

            if (mode == 2)
            {
                if (Session["DELETEDHIGHNAME"] == null)
                {
                    fillForm(); 
                }
            }

            lnkbtnCancel.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
        }
    }

    protected void fillForm()
    {
        clsHighlight high = new clsHighlight();

        if (high.extractHighlightById(highId, 0))
        {
            bindRptData();

            if (high.highAllowComment != 0) { chkAllowComment.Checked = true; }
            else { chkAllowComment.Checked = false; }
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }

    protected void bindRptData()
    {
        clsHighlight high = new clsHighlight();

        DataSet ds = new DataSet();
        ds = high.getHighlightCommentsById(highId);

        DataView dv = new DataView(ds.Tables[0]);
        dv.Sort = "HIGHCOMM_POSTEDDATE ASC";

        PagedDataSource pds = new PagedDataSource();
        pds.DataSource = dv;

        rptHighComment.DataSource = pds;
        rptHighComment.DataBind();
        
        if (dv.Count > 0)
        {
            pnlHighComment.Visible = true;
            pnlNoComment.Visible = false;
        }
        else
        {
            pnlHighComment.Visible = false;
            pnlNoComment.Visible = true;
        }
    }
    #endregion
}