﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.IO;



public partial class ctrl_ucAdmOrderItems : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _transId = 0;
    protected int _cancel = 1;
    protected int _status = 0;
    protected int _type = 1;
    protected DataTable _dtItems = new DataTable();
    protected string _deletedId = "";
    protected decimal _decShipping = Convert.ToDecimal("0.00");
    protected Boolean _boolShowAction = true;

    protected Boolean boolRemove = false;
    protected int intProdCount;
    protected int intCount;

    protected decimal decSubtotal = Convert.ToDecimal("0.00");
    protected decimal decTotalGST = Convert.ToDecimal("0.00");
    protected string strCurrencyUnit = "";
    public clsOrder ord = new clsOrder();

    protected string defaultGroupImg = "";
    protected bool isYsHamper = false;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int transId
    {
        get
        {
            if (ViewState["TRANSID"] == null)
            {
                return _transId;
            }
            else
            {
                return int.Parse(ViewState["TRANSID"].ToString());
            }
        }
        set { ViewState["TRANSID"] = value; }
    }

    public int cancel
    {
        get
        {
            if (ViewState["CANCEL"] == null)
            {
                return _cancel;
            }
            else
            {
                return Convert.ToInt16(ViewState["CANCEL"]);
            }
        }
        set { ViewState["CANCEL"] = value; }
    }

    public int status
    {
        get
        {
            if (ViewState["STATUS"] == null)
            {
                return _status;
            }
            else
            {
                return Convert.ToInt16(ViewState["STATUS"]);
            }
        }
        set { ViewState["STATUS"] = value; }
    }

    public int type
    {
        get
        {
            if (ViewState["TYPE"] == null)
            {
                return _type;
            }
            else
            {
                return Convert.ToInt16(ViewState["TYPE"]);
            }
        }
        set { ViewState["TYPE"] = value; }
    }

    public DataTable dtItems
    {
        get
        {
            if (ViewState["DTITEMS"] == null)
            {
                return _dtItems;
            }
            else
            {
                return (DataTable)ViewState["DTITEMS"];
            }
        }
        set { ViewState["DTITEMS"] = value; }
    }

    protected string deletedId
    {
        get
        {
            if (ViewState["DELETEDID"] == null)
            {
                return _deletedId;
            }
            else
            {
                return ViewState["DELETEDID"].ToString();
            }
        }
        set { ViewState["DELETEDID"] = value; }
    }

    public decimal decShipping
    {
        get
        {
            if (ViewState["SHIPPING"] == null)
            {
                return _decShipping;
            }
            else
            {
                return Convert.ToDecimal(ViewState["SHIPPING"]);
            }
        }
        set { ViewState["SHIPPING"] = value; }
    }

    public bool boolShowAction
    {
        get
        {
            if (ViewState["BOOLSHOWACTION"] == null)
            {
                return _boolShowAction;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLSHOWACTION"]);
            }
        }
        set { ViewState["BOOLSHOWACTION"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void rptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            Literal litPriceHeader = (Literal)e.Item.FindControl("litPriceHeader");
            Literal litTotalHeader = (Literal)e.Item.FindControl("litTotalHeader");


            litPriceHeader.Text = "Unit Price" + (!string.IsNullOrEmpty(strCurrencyUnit) ? "(" + strCurrencyUnit + ")" : "");
            litTotalHeader.Text = "Item Total" + (!string.IsNullOrEmpty(strCurrencyUnit) ? "(" + strCurrencyUnit + ")" : "");
        }
        else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intDetailId = int.Parse(DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_ID").ToString());
            int intProdId = int.Parse(DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODID").ToString());
            string strProdImage = DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODIMAGE").ToString();
            string strProdDName = DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODDNAME").ToString();
            decimal decPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODPRICE"));
            decimal decProdTotal = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODTOTAL"));
            int intProdQty = int.Parse(DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODQTY").ToString());
            decimal decProdPricing = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODPRICING"));

            clsOrder ord = new clsOrder();
            //string strCurrencyUnit = config.currency;


           



            intProdCount += 1;

            Literal litNo = (Literal)e.Item.FindControl("litNo");
            litNo.Text = intProdCount.ToString();

            if (string.IsNullOrEmpty(strProdImage))
            {
                strProdImage = Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMORDFOLDER + "/" + defaultGroupImg;
            }
            else
            {
                strProdImage = Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMORDFOLDER + "/" + strProdImage;
            }

            Image imgProd = (Image)e.Item.FindControl("imgProd");
            imgProd.ImageUrl = strProdImage;
            imgProd.ToolTip = strProdDName;
            imgProd.AlternateText = strProdDName;

            string addBackSlash = Session[clsAdmin.CONSTPROJECTUPLOADPATH].ToString().Replace(@"\", @"\\");
            //imgProd.Attributes["onerror"] = "this.src='" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + ConfigurationManager.AppSettings["uplBase2"] + defaultGroupImg + "';";


            Literal litProdPrice = (Literal)e.Item.FindControl("litProdPrice");
            litProdPrice.Text = strCurrencyUnit + " " + decPrice;

            TextBox txtProdQty = (TextBox)e.Item.FindControl("txtProdQty");
            txtProdQty.Attributes.Add("onkeydown", "javascript:if(event.keyCode == 13){return false;}");

            LinkButton lnkbtnRemove = (LinkButton)e.Item.FindControl("lnkbtnRemove");

            if (cancel != 1 && status < clsAdmin.CONSTORDERSTATUSPAY && boolShowAction)
            {
                lnkbtnRemove.Visible = true;
                txtProdQty.Visible = true;
            }
            else
            {
                lnkbtnRemove.Visible = false;
                Literal litProdQty = (Literal)e.Item.FindControl("litProdQty");
                litProdQty.Visible = true;
            }
            
            decSubtotal += decProdTotal;

            DataSet ds = new DataSet();
            ds = ord.getOrderSpecItem(intDetailId);

            DataView dv = new DataView(ds.Tables[0]);

            if (dv.Count > 0)
            {
                Repeater rptOrdProdSpec = (Repeater)e.Item.FindControl("rptOrdProdSpec");

                rptOrdProdSpec.DataSource = dv;
                rptOrdProdSpec.DataBind();
            }
            
            

            if (isYsHamper)
            {
                decimal decProdGST = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODGST"));
                //Literal litProdPrice = (Literal)e.Item.FindControl("litProdPrice");
                litProdPrice.Text = strCurrencyUnit + " " + decProdPricing;

                Literal litProdTotal = (Literal)e.Item.FindControl("litProdTotal");
                litProdTotal.Text = (decProdPricing * intProdQty).ToString();

                decTotalGST += (decProdGST * intProdQty);
            }
        }
        else if (e.Item.ItemType == ListItemType.Footer)
        {
            clsConfig config = new clsConfig();
            //string strCurrencyUnit = config.currency;


            if (ord.disValue > 0)
            {
                HtmlTableRow trDiscount = (HtmlTableRow)e.Item.FindControl("trDiscount");
                trDiscount.Visible = true;
                
                Literal litDiscount = (Literal)e.Item.FindControl("litDiscount");
                litDiscount.Text = "- " + strCurrencyUnit + " " + ord.disValue.ToString() + "";

                Label lblDiscount = (Label)e.Item.FindControl("lblDiscount");
                lblDiscount.Text = "Discount ("+ ord.couponCode + ")";
            }

            Literal litShipping = (Literal)e.Item.FindControl("litShipping");
            litShipping.Text = strCurrencyUnit + " " + decShipping.ToString();

            decimal decTotal = decSubtotal + decShipping - ord.disValue;

            Literal litSubtotal = (Literal)e.Item.FindControl("litSubtotal");
            Literal litTotal = (Literal)e.Item.FindControl("litTotal");

            if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
            {
                if (ord.shippingCountry == "SG")
                {
                    HtmlTableRow trProdGST = (HtmlTableRow)e.Item.FindControl("trProdGST");
                    trProdGST.Visible = true;

                    Literal litGST = (Literal)e.Item.FindControl("litGST");
                    litGST.Text = decTotalGST.ToString();
                }

                litSubtotal.Text = strCurrencyUnit + " " + (decSubtotal - decTotalGST).ToString();
            }
            else
            {
                ord.extractOrderById6(transId);
                if (ord.ordTotalGST > 0)
                {
                    HtmlTableRow trGST = (HtmlTableRow)e.Item.FindControl("trGST");
                    trGST.Visible = true;

                    Label lblGST = (Label)e.Item.FindControl("lblGST");
                    lblGST.Text = "Total GST " + ord.ordGST + "%";

                    Literal litTotalGST = (Literal)e.Item.FindControl("litTotalGST");
                    litTotalGST.Text = strCurrencyUnit + " " + ord.ordTotalGST;

                    decTotal = decTotal + ord.ordTotalGST;
                }

                litSubtotal.Text = strCurrencyUnit + " " + decSubtotal.ToString();
            }
            
            litTotal.Text = strCurrencyUnit + " " + decTotal.ToString();
            
        }

        if (e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //HtmlTableCell tdNo = (HtmlTableCell)e.Item.FindControl("tdNo");
            //HtmlTableCell tdItem = (HtmlTableCell)e.Item.FindControl("tdItem");
            //HtmlTableCell tdUnitPrice = (HtmlTableCell)e.Item.FindControl("tdUnitPrice");
            //HtmlTableCell tdQty = (HtmlTableCell)e.Item.FindControl("tdQty");
            //HtmlTableCell tdPrice = (HtmlTableCell)e.Item.FindControl("tdPrice");

            //tdNo.Attributes["class"] = "td_alt";
            //tdItem.Attributes["class"] = "td_norLeft_alt";
            //tdUnitPrice.Attributes["class"] = "td_right_alt";
            //tdQty.Attributes["class"] = "td_alt";
            //tdPrice.Attributes["class"] = "td_right_alt";
        }
    }

    protected void rptOrdProdSpec_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intProdId = int.Parse(DataBinder.Eval(e.Item.DataItem, "PROD_ID").ToString());
            int intSpecId = int.Parse(DataBinder.Eval(e.Item.DataItem, "SPEC_ID").ToString());
            int intSpecItemId = int.Parse(DataBinder.Eval(e.Item.DataItem, "SI_ID").ToString());
            string strSIName = DataBinder.Eval(e.Item.DataItem, "SI_NAME").ToString();
            string strSIOperator = DataBinder.Eval(e.Item.DataItem, "SI_OPERATOR").ToString();
            string strSIPrice = DataBinder.Eval(e.Item.DataItem, "SI_PRICE").ToString();
            int intSIDateActive = int.Parse(DataBinder.Eval(e.Item.DataItem, "SI_DATEACTIVE").ToString());
            string strDateTitle = DataBinder.Eval(e.Item.DataItem, "SI_DATETITLE").ToString();
            string strDate = DataBinder.Eval(e.Item.DataItem, "SI_DATE").ToString();
            string strSpecType = DataBinder.Eval(e.Item.DataItem, "SPEC_TYPE").ToString();

            Literal litProdSpec = (Literal)e.Item.FindControl("litProdSpec");
            clsConfig config = new clsConfig();
            litProdSpec.Text = strSpecType + ": " + strSIName + " <span class=\"ordSpecPrice\">" + strSIOperator + " " + config.currency + strSIPrice + "</span>";
            //if (intSIDateActive > 0)
            //{
            //    litProdSpec.Text = litProdSpec.Text + " | " + strDateTitle + ": " + strDate;
            //}
        }
    }

    protected void rptItems_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdRemove")
        {
            boolRemove = true;

            string strArgument = e.CommandArgument.ToString();
            string[] strArg = strArgument.Split((char)'|');

            int intProdId = int.Parse(strArg[0]);
            int intDetailId = int.Parse(strArg[1]);

            for (int i = 0; i < dtItems.Rows.Count; i++)
            {
                if (int.Parse(dtItems.Rows[i]["ORDDETAIL_ID"].ToString()) == intDetailId && int.Parse(dtItems.Rows[i]["ORDDETAIL_PRODID"].ToString()) == intProdId)
                {
                    dtItems.Rows[i].Delete();
                }
            }

            if (intProdId != 0)
            {
                deletedId += intProdId + "," + intDetailId + "|";
            }

            dtItems.AcceptChanges();
            bindRptData();
        }
    }

    protected void lnkbtnRevert_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.Url.ToString());
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        Boolean boolEdited = false;
        Boolean boolSuccess = true;
        int intRecordAffected = 0;
        clsOrder ord = new clsOrder();

        foreach (RepeaterItem rptItem in rptItems.Items)
        {
            int intDetailId = 0;
            int intProdId = 0;
            int intProdQty = 0;
            decimal decProdPrice = Convert.ToDecimal("0.00");
            decimal decProdTotal = Convert.ToDecimal("0.00");
            decimal decProdPricing = Convert.ToDecimal("0.00");
            decimal decProdGST = Convert.ToDecimal("0.00");
            decimal decProdTotalGST = Convert.ToDecimal("0.00");

            HiddenField hdnDetailId = (HiddenField)rptItem.FindControl("hdnDetailId");
            HiddenField hdnProdId = (HiddenField)rptItem.FindControl("hdnProdId");
            TextBox txtProdQty = (TextBox)rptItem.FindControl("txtProdQty");
            HiddenField hdnProdQty = (HiddenField)rptItem.FindControl("hdnProdQty");
            HiddenField hdnProdPrice = (HiddenField)rptItem.FindControl("hdnProdPrice");
            HiddenField hdnProdPricing = (HiddenField)rptItem.FindControl("hdnProdPricing");

            intProdId = int.Parse(hdnProdId.Value);
            intDetailId = int.Parse(hdnDetailId.Value);

            if (!string.IsNullOrEmpty(txtProdQty.Text.Trim()))
            {
                intProdQty = int.Parse(txtProdQty.Text.Trim());
            }
            else { intProdQty = 0; }

            decProdPrice = Convert.ToDecimal(hdnProdPrice.Value);
            decProdPricing = Convert.ToDecimal(hdnProdPricing.Value);

            int intProdQtyRef = Convert.ToInt16(hdnProdQty.Value);

            clsConfig config = new clsConfig();
            if (intProdQty != intProdQtyRef)
            {
                if (intProdQty == 0)
                {
                    deletedId += int.Parse(hdnProdId.Value) + "," + intDetailId + "|";
                }
                else
                {
                    DataSet ds = new DataSet();
                    ds = ord.getOrderSpecItem(intDetailId);
                    DataRow[] foundRow = ds.Tables[0].Select("PROD_ID = " + intProdId);

                    if (foundRow.Length > 0)
                    {
                        string strOperator = "";
                        strOperator = foundRow[0]["SI_OPERATOR"].ToString();

                        decimal decSIPrice = Convert.ToDecimal("0.00");
                        decSIPrice = Convert.ToDecimal(foundRow[0]["SI_PRICE"].ToString());

                        if (strOperator == "+")
                        {
                            decProdTotal += (decSIPrice * intProdQty);
                        }
                        else
                        {
                            decProdTotal -= (decSIPrice * intProdQty);
                        }

                        decimal decTempTotal = intProdQty * decProdPrice;
                        decProdTotal += decTempTotal;
                    }
                    else
                    {
                        if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
                        {
                            ord.extractOrderById(transId);

                            if (ord.shippingCountry == "SG")
                            {
                                decProdGST = (decProdPricing * 7 / 100);
                                decProdTotalGST = decProdGST * intProdQty;
                            }

                            decimal decTempTotal = intProdQty * decProdPricing;
                            decProdTotal = (intProdQty * decProdPricing) + decProdTotalGST;
                        }
                        else
                        {
                            decimal decTempTotal = intProdQty * decProdPrice;
                            decProdTotal = intProdQty * decProdPrice;
                        }
                    }

                    if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
                    {
                        intRecordAffected = ord.updateOrderItem2(transId, intDetailId, intProdId, intProdQty, decProdTotal, decProdGST);
                    }
                    else
                    {
                        intRecordAffected = ord.updateOrderItem(transId, intDetailId, intProdId, intProdQty, decProdTotal);
                    }

                    if (intRecordAffected == 1)
                    {
                        boolEdited = true;
                    }
                    else { boolSuccess = false; }    
                }
            }
        }

        if (boolEdited)
        {
            ord.extractOrderById(transId);

            decimal decShippingNew = Convert.ToDecimal("0.00");
 
            clsConfig config = new clsConfig();
            if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
            {
                int intShippingType = !string.IsNullOrEmpty(config.shippingType) ? Convert.ToInt16(config.shippingType) : clsAdmin.CONSTSHIPPINGTYPE1;

                if (intShippingType == clsAdmin.CONSTSHIPPINGTYPE1)
                {
                    decShippingNew = clsMis.getShippingFee1(ord.shippingCountry, ord.shippingState, ord.shippingCity, ord.ordTotalWeight, ord.ordSubtotal);
                }
                ord.updateOrderShipping(transId, decShippingNew);
            }
            //else
            //{
            //    //clsConfig config = new clsConfig();
            //    int intShippingType = !string.IsNullOrEmpty(config.shippingType) ? Convert.ToInt16(config.shippingType) : clsAdmin.CONSTSHIPPINGTYPE1;

            //    if (intShippingType == clsAdmin.CONSTSHIPPINGTYPE1)
            //    {
            //        decShippingNew = clsMis.getShippingFee(ord.shippingCountry, ord.shippingState, ord.ordTotalWeight, ord.ordSubtotal);
            //    }
            //    else if (intShippingType == clsAdmin.CONSTSHIPPINGTYPE2)
            //    {
            //        decShippingNew = clsMis.getShippingFee2(ord.shippingCountry, ord.shippingState, ord.ordTotalWeight, ord.ordSubtotal);
            //    }
            //    else if (intShippingType == clsAdmin.CONSTSHIPPINGTYPE3)
            //    {
            //        decShippingNew = clsMis.getShippingFee3(ord.shippingCountry, ord.shippingState, ord.ordTotalWeight, ord.ordSubtotal);
            //    }
            //}
        }

        if (!string.IsNullOrEmpty(deletedId))
        {
            string strDeletedId = deletedId.Substring(0, deletedId.Length - "|".Length);
            string[] strDeletedIdSplit = strDeletedId.Split((char)'|');

            for (int i = 0; i < strDeletedIdSplit.Length; i++)
            {
                string[] strDeletedIdsSplit = strDeletedIdSplit[i].Split((char)',');
                int intProdId = 0;
                int intDetailId = 0;

                intProdId = int.Parse(strDeletedIdsSplit[0]);
                intDetailId = int.Parse(strDeletedIdsSplit[1]);

                intRecordAffected = ord.deleteOrderItem(transId, intDetailId, intProdId);

                if (intRecordAffected == 1)
                {
                    boolEdited = true;
                }
                else
                {
                    boolSuccess = false;
                    break;
                }
            }
        }

        if (boolEdited) { Session["EDITEDORDID"] = ord.ordId; }
        else
        {
            if (!boolSuccess)
            {
                ord.extractOrderById(transId);
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text").ToString() + "<br />Failed to edit transaction (" + ord.ordNo + ").Please try again.</div>";
            }
            else
            {
                Session["EDITEDORDID"] = transId;
                Session["NOCHANGE"] = 1;
            }
        }

        Response.Redirect(Request.Url.ToString());
    }
    #endregion


    #region "Methods"
    public void fill()
    {

        if (!IsPostBack) { setPageProperties(); }

        bindRptData();

    }

    protected void setPageProperties()
    {
        if (cancel != 1 && status < clsAdmin.CONSTORDERSTATUSPAY && transId > 0 && boolShowAction) { pnlButton.Visible = true; }
        else { pnlButton.Visible = false; }
        
    }

    protected void bindRptData()
    {
        clsOrder ord = new clsOrder();
        clsConfig config = new clsConfig();
        DataSet ds = new DataSet();
        DataView dv;

        defaultGroupImg = config.defaultGroupImg;

        if (ord.extractConvertionRateUnitById(transId))
        {
            if (!string.IsNullOrEmpty(ord.ordConvUnit))
            {
                strCurrencyUnit = ord.ordConvUnit;
            }
            else
            {
                strCurrencyUnit = config.currency;
            }
        }

        

        if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
        {
            isYsHamper = true;
            if (!boolRemove)
            {
                ds = ord.getOrderItemsByOrderId3(transId);
                dtItems = ds.Tables[0];
            }
        }
        else
        {
            isYsHamper = false;
            if (!boolRemove)
            {
                ds = ord.getOrderItemsByOrderId(transId);
                dtItems = ds.Tables[0];
            }
        }

        dv = new DataView(dtItems);
        intProdCount = 0;
   
        decSubtotal = Convert.ToDecimal("0.00");
        
        rptItems.DataSource = dv;
        rptItems.DataBind();

    }
    #endregion
}
