﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmWorkingTimeGroup.ascx.cs" Inherits="ctrl_ucAdmWorkingTimeGroup" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<script type="text/javascript">
    function setAutocomplete(target) {
        $.ajax({
            type: "POST",
            url: wsBase + "wsSearchWTGroup.asmx/getGroupName",
            dataType: "json",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                console.log(data);
                var strList = data.d.split(',');
                $("#" + target).autocomplete({
                    source: strList,
                    change: function(event, ui) {
                        //fillForm(target);
                    },
                    appendTo: "#autocompletecontainer",
                });
            },
            error: function(xmlHttpRequest, status, err) {
                //alert("error " + err);
            }
        });
    }
</script>

<asp:TextBox ID="txtWTGroup" runat="server" CssClass="form-control" MaxLength="250" placeholder="Group"></asp:TextBox>
<div id="autocompletecontainer"></div>