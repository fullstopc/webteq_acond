﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrDirGroup.ascx.cs" Inherits="ctrl_ucUsrDirGroup" %>
<%@ Register Src="~/ctrl/ucUsrLoading.ascx" TagName="UsrLoading" TagPrefix="uc" %>

<asp:Panel ID="pnlDirGroup" runat="server" CssClass="divDirGroup">   
    <div class="divDirGroupHdr"><asp:Literal ID="litDirGroupHdr" runat="server" meta:resourcekey="litDirGroupHdr"></asp:Literal></div>
    <asp:Repeater ID="rptGroup" runat="server" OnItemDataBound="rptGroup_ItemDataBound" OnItemCommand="rptGroup_ItemCommand">
        <HeaderTemplate><div class="divDirGroupList"></HeaderTemplate>
        <ItemTemplate>
        <div class="divGroupBullet">
            <asp:HyperLink ID="hypGroupDName" runat="server" class="hypGroupDName"></asp:HyperLink>
        </div>
        </ItemTemplate> 
        <FooterTemplate></div></FooterTemplate>       
    </asp:Repeater>
</asp:Panel>