﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class ctrl_ucUsrBackground : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _pageId = 0;
    protected int _height = 0;
    protected int _width = 0;
    #endregion

    #region "Property Methods"
    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public int height
    {
        get { return _height; }
        set { _height = value; }
    }

    public int width
    {
        get { return _width; }
        set { _width = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("GI")))
        {
            string SScript = null;
            SScript = "<script type=\"text/javascript\">";
            SScript += "jQuery(function($) {";
            SScript += "$.supersized({";
            // Functionality
            SScript += "slideshow: 1,";		            // Slideshow on/off
            SScript += "autoplay: 1,"; 		            // Slideshow starts playing automatically
            SScript += "start_slide: 1,"; 		        // Start slide (0 is random)
            SScript += "stop_loop: 0,"; 		        // Pauses slideshow on last slide
            SScript += "random: 0,"; 		            // Randomize slide order (Ignores start slide)
            SScript += "slide_interval: 5000,"; 	    // Length between transitions
            SScript += "transition: 1,"; 			    // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
            SScript += "transition_speed: 1000,"; 	    // Speed of transition
            SScript += "new_window: 1,"; 		        // Image links open in new window/tab
            SScript += "pause_hover: 0,"; 		        // Pause slideshow on hover
            SScript += "keyboard_nav: 1,"; 		        // Keyboard navigation on/off
            SScript += "performance: 2,"; 		        // 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
            SScript += "image_protect: 1,"; 		    // Disables image dragging and right click with Javascript
            // Size & Position						   
            SScript += "min_width: " + width + ",";     		    // Min width allowed (in pixels)
            SScript += "min_height: " + height + ",";    		    // Min height allowed (in pixels)
            SScript += "vertical_center: 0,"; 		    // Vertically center background
            SScript += "horizontal_center: 1,"; 	    // Horizontally center background
            SScript += "fit_always: 0,"; 		        // Image will never exceed browser width or height (Ignores min. dimensions)
            SScript += "fit_portrait: 1,"; 		        // Portrait images will not exceed browser height
            SScript += "fit_landscape: 0,"; 		    // Landscape images will not exceed browser width
            // Components							
            SScript += "slide_links: 'blank',";         // Individual links for each slide (Options: false, 'number', 'name', 'blank')
            SScript += "thumb_links: 1,"; 		        // Individual thumb links for each slide
            SScript += "thumbnail_navigation: 0,";      // Thumbnail navigation
            SScript += "slides: [";			            // Slideshow Images

            clsPage page = new clsPage();
            page.extractPageById(pageId, 1);
            int intMastGrpId = page.grpId;

            DataSet ds = new DataSet();
            clsMasthead masthead = new clsMasthead();
            ds = masthead.getMastheadImageFromGoupID(intMastGrpId);

            DataView dv = new DataView (ds.Tables[0]);
            dv.RowFilter = "MASTHEAD_ACTIVE = 1";
            dv.Sort = " MASTHEAD_ORDER ASC";
            
            int intCounter = 0;
            foreach (DataRow row in dv.ToTable().Rows)
            {
                intCounter += 1;
                if (intCounter == ds.Tables[0].Rows.Count)
                {
                    SScript += "{image: '" + ConfigurationManager.AppSettings["uplBase"].ToString() + clsSetting.CONSTMASTHEADFOLDER + "/" + row["MASTHEAD_IMAGE"].ToString() + "', " +
                    " description  : '" + row["MASTHEAD_TAGLINE1"].ToString().Replace("\n", "").Replace("\r", "") + "'}";
                }
                else
                {
                    SScript += "{image: '" + ConfigurationManager.AppSettings["uplBase"].ToString() + clsSetting.CONSTMASTHEADFOLDER + "/" + row["MASTHEAD_IMAGE"].ToString() + "', " +
                    " description  : '" + row["MASTHEAD_TAGLINE1"].ToString().Replace("\n", "").Replace("\r", "") + "'},";
                }
            }

            SScript += "		],";
            // Theme Options			   
            SScript += "progress_bar: 1,"; 		        // Timer for each slide							
            SScript += "mouse_scrub: 1";
            SScript += "});";
            SScript += "});";
            SScript += "</script>";

            if(dv.Count > 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "GI", SScript, false);
                //Page.ClientScript.RegisterStartupScript(Page.GetType(), "GI", SScript, false);
            }
        }
    }
    #endregion
}
