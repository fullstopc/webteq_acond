﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucAdmCopy : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "admcopy.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            string strYear = GetGlobalResourceObject("GlobalResource", "contentWebsiteYear.Text").ToString();

            litCopy.Text = GetLocalResourceObject("litCopy.Text").ToString().Replace(clsAdmin.CONSTYEARTAG, strYear);

            imgLogo.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/logo_webteq_lightblue.gif";
        }
    }
}
