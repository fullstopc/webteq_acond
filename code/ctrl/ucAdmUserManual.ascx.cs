﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ctrl_ucAdmUserManual : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _adminview;
    #endregion

    #region "Properties Methods"
    public int adminview
    {
        get { return _adminview; }
        set { _adminview = value; }
    }

    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        clsConfig conf = new clsConfig();
        if (txtUserManual.Text != hdnUserManual.Value)
        {
            conf.updateUserManualById(clsConfig.CONSTGROUPUSERMANUAL, clsConfig.CONSTNAMEUSERMANUALCONTENT, txtUserManual.Text, Convert.ToInt16(Session["ADMID"].ToString()));
            pnlAck.Visible = true;
            litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";
        }
        else
        {

            litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
            pnlAck.Visible = true;
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (adminview == 0)
        {
            registerCKEditorScript();
        }

        setPageProperties();
    }

    protected void registerCKEditorScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR")))
        {
            int intFullPath = Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFULLPATH]) == 1 ? clsAdmin.CONSTFULLPATHACTIVE : clsAdmin.CONSTFULLPATHDEFAULT;
            string strJS = null;
            strJS = "<script type=\"text/javascript\">" +
                    "CKEDITOR.replace('" + txtUserManual.ClientID + "'," +
                    "{" +
                    "width:'99%'," +
                    "height:'440'," +
                    "toolbar: 'User3'," +
                    "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";

            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                     "}" +
                     ");";

            strJS += "CKEDITOR.add;";
            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMEEDITORSTYLE, clsConfig.CONSTGROUPWEBSITE, 1);
            string strEditorStyle = config.value;
            if (!string.IsNullOrEmpty(strEditorStyle))
            {
                strJS += "CKEDITOR.config.contentsCss = 'body {" + strEditorStyle + "}';";
            }

            strJS += "CKEDITOR.config.stylesCombo_stylesSet= '" + clsAdmin.CONSTADMSTYLESET + ":" + System.Configuration.ConfigurationManager.AppSettings["jsBase"] + clsAdmin.CONSTADMSTYLESETFILE + "';" +
                     "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR", strJS, false);
        }
    }

    protected void setPageProperties()
    {
        fillForm();
    }

    protected void fillForm()
    {
        clsConfig conf = new clsConfig();
        if (!IsPostBack)
        {
            if (adminview == 0)
            {
                pnlFormUserManual.Visible = true;
                txtUserManual.Text = conf.userManualContent;
                hdnUserManual.Value = conf.userManualContent;
                pnlFormUserManualDisplay.Visible = false;
                pnlFormUserManualDisplay.Attributes.Add("visibility", "hidden");

            }
            else
            {
                pnlFormUserManual.Visible = false;
                pnlFormUserManualDisplay.Visible = true;
                Session[clsAdmin.CONSTADMINCS] = 1;
                litUserManual.Text = conf.userManualContent;
                Session[clsAdmin.CONSTADMINCS] = null;
                pnlFormUserManualDisplay.Attributes.Add("visibility", "visible");
            }
        }
        else
        {
            hdnUserManual.Value = conf.userManualContent;
        }
    }
    #endregion
}
