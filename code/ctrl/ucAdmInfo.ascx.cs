﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucAdmInfo : System.Web.UI.UserControl
{
    #region "Properties"
    protected string _message = "Info";
    #endregion


    #region "Property Methods"
    public string message
    {
        get { return _message; }
        set { _message = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "adminfo.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            litInfo.Text = message;
        }
    }
    #endregion
}
