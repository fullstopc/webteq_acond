﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmShippingWaiver.ascx.cs" Inherits="ctrl_ucAdmShippingWaiver" %>

<script type="text/javascript">
    function validateWaive_client(source, args) {
        var txtWaive = document.getElementById("<%= txtWaive.ClientID %>");
        var currRE = /<%= clsAdmin.CONSTCURRENCYRE %>/;
        

        if (Trim(args.Value) != '') {
            if (args.Value.match(currRE)) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
                source.errormessage = "<br />Please enter only 5 digit currency.";
                source.innerHTML = "<br />Please enter only 5 digit currency.";
            }   
        }
    }

    function validateCostRange_client(source, args) {
        var txtCostStart = document.getElementById("<% =txtCostStart.ClientID %>");
        var txtCostEnd = document.getElementById("<% =txtCostEnd.ClientID %>");
        var currRE = /<%= clsAdmin.CONSTCURRENCYRE %>/;
        var boolValid = false;

        source.errormessage = "<br />Please enter valid shipping cost range.";
        source.innerHTML = "<br />Please enter valid shipping cost range.";

        if ((Trim(txtCostStart.value) == '') && (Trim(txtCostEnd.value) == '')) {
            args.IsValid = false;
        }
        else {
            if (Trim(txtCostStart.value) != '') {
                if (Trim(txtCostStart.value).match(currRE)) {
                    boolValid = true;
                }
                else {
                    source.errormessage = "<br />Please enter only currency.";
                    source.innerHTML = "<br />Please enter only currency.";
                    
                    boolValid = false;
                }
            }
            else {
                boolValid = false;
            }

            if ((boolValid) && (Trim(txtCostEnd.value) != '')) {
                if (Trim(txtCostEnd.value).match(currRE)) {
                    boolValid = true;
                }
                else {
                    source.errormessage = "<br />Please enter only currency.";
                    source.innerHTML = "<br />Please enter only currency.";
                    
                    boolValid = false;
                }
            }
            else {
                boolValid = false;
            }

            if ((boolValid) && validateNumericRange(txtCostStart.value, txtCostEnd.value)) {
                boolValid = true;
            }
            else {
                boolValid = false;
            }

            args.IsValid = boolValid;
        }
    }
</script>

<asp:Panel ID="pnlShippingWaiver" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table  cellpadding="0" cellspacing="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr">Shipping Waiver Details</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Shipping Cost Range<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <span class="spanFieldDesc">
                        From <asp:TextBox ID="txtCostStart" runat="server" CssClass="text_medium" MaxLength="20" onchange="formatCurrency(this.value)"></asp:TextBox> To <asp:TextBox ID="txtCostEnd" runat="server" CssClass="text_10" MaxLength="20" onchange="formatCurrency(this.value)"></asp:TextBox>
                        <asp:HiddenField ID="hdnCostStart" runat="server" />
                        <asp:HiddenField ID="hdnCostEnd" runat="server" />
                    </span>
                    <span class="spanFieldDesc"><asp:CustomValidator ID="cvCostRange" runat="server" ControlToValidate="txtCostStart" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateCostRange_client" OnServerValidate="validateCostRange_server" OnPreRender="cvCostRange_PreRender" ValidateEmptyText="true"></asp:CustomValidator></span>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Waive (<asp:Literal ID="litCurrency" runat="server"></asp:Literal>)<span class="attention_compulsory">*</span>:</td>
                <td>
                    <asp:TextBox ID="txtWaive" runat="server" MaxLength="10" CssClass="text" onchange="formatCurrency(this.value)"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvWaive" runat="server" ErrorMessage="<br />Please enter value." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtWaive"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvWaive" runat="server" ControlToValidate="txtWaive" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateWaive_client"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="tdSectionHdr">Shipping Waiver Settings</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Active:</td>
                <td><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CssClass="btn btnCancel" OnClick="lnkbtnDelete_Click" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>
