﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrTopMenu.ascx.cs" Inherits="ctrl_ucUsrTopMenu" %>

<asp:Panel ID="pnlTopMenuContainer" runat="server" CssClass="topmenu-root">
    <ul class="nav nav-one">
        <asp:Repeater ID="rptMenu" runat="server" OnItemDataBound="rptMenu_ItemDataBound">
            <ItemTemplate>
                <li runat="server" id="liMenu">
                    <asp:Panel ID="pnlMenu" runat="server">
                        <asp:HyperLink ID="hypMenu" runat="server" CssClass="divTopMenuLink"><asp:Image ID="imgMenu" runat="server" CssClass="imgMenu" BorderStyle="none" /></asp:HyperLink>
                        <asp:Panel ID="pnlDropDownContainer" runat="server" CssClass="divDropDownContainerLeft" Visible="false">
                            <div class="divDropDownContainerRight"><div class="divDropDownRepeat"></div></div>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlMenuContent" runat="server" CssClass="divMenuContent" Visible="false">
                        <asp:Literal ID="litMenuContent" runat="server"></asp:Literal>
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" CssClass="divTopMenuSub" >
                        <asp:Repeater ID="rptSubMenu" runat="server" OnItemDataBound="rptSubMenu_ItemDataBound">
                            <HeaderTemplate>
                                <ul class="nav nav-two">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li runat="server" id="liSubMenu">
                                    <asp:HyperLink ID="hypSubMenu" runat="server"></asp:HyperLink>
                                    <asp:Panel ID="pnlSubMenuLv2" runat="server" CssClass="divSubMenuLv2">
                                        <asp:Repeater ID="rptSubMenuLv2" runat="server" OnItemDataBound="rptSubMenuLv2_ItemDataBound">
                                            <HeaderTemplate>
                                                <ul class="nav nav-three">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <li runat="server" id="liSubMenuLv2">
                                                    <asp:HyperLink ID="hypSubMenuLv2" runat="server" CssClass="hypTopSubMenuLv2"></asp:HyperLink>
                                                </li>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </ul>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </asp:Panel>
                                </li>
                            </ItemTemplate>
                                <SeparatorTemplate>
                                <li class="splitter"></li>
                                </SeparatorTemplate> 
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:Repeater ID="rptGallery" runat="server" OnItemDataBound="rptGallery_ItemDataBound">
                        <HeaderTemplate>
                            <ul class="nav nav-two">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li runat="server" id="liSubMenu">
                                <asp:HyperLink ID="hypGalleryMenu" runat="server"></asp:HyperLink>
                            </li>
                        </ItemTemplate>
                            <SeparatorTemplate>
                            <li class="splitter"></li>
                            </SeparatorTemplate> 
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                    </asp:Panel>
                </li>
            </ItemTemplate>  
            <SeparatorTemplate><li class="splitter"></li></SeparatorTemplate>      
        </asp:Repeater>
    </ul>
</asp:Panel>