﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrFooter.ascx.cs" Inherits="ctrl_ucUsrFooter" %>
<%@ Register Src="~/ctrl/ucUsrCMSContainer.ascx" TagName="UsrCMSContainer" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrBtmMenu.ascx" TagName="UsrBtmMenu" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrFacebook.ascx" TagName="UsrFacebook" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrCopy.ascx" TagName="UsrCopy" TagPrefix="uc" %>

<div class="footer-inner">
    <div class="footer-bottommenu">
        <ul class="nav nav-one">
            <li><a class="booking"><i></i><span>Book A Job</span></a></li>
            <li><a class="history"><i></i><span>Book A Job</span></a></li>
            <li><a class="marked"><i></i><span>Book A Job</span></a></li>
            <li><a class="chat"><i></i><span>Book A Job</span></a></li>
            <li><a class="login selected"><i></i><span>Book A Job</span></a></li>
        </ul>
    </div>
    <div class="footer-bottommenu" runat="server" visible="false">
        <uc:UsrBtmMenu ID="ucUsrBtmMenu" runat="server" />
    </div>
    <div class="footer-bottombanner" runat="server" visible="false">
        <uc:UsrCMSContainer ID="ucUsrCMSBtmBanner" runat="server" blockId="1" cmsType="2" />
    </div>
    <div class="footer-socialmedia" runat="server" visible="false">
        <asp:Panel ID="pnlFbPageContainer" runat="server" CssClass="divFacebookPageContainer">
            <div class="divFacebookFollow">Follow Us: </div>
            <asp:Panel ID="pnlFacebookPage" runat="server" CssClass="divFacebookPage" Visible="false">
                <asp:HyperLink ID="hypFacebookPage" runat="server" CssClass="hypFacebookPage"></asp:HyperLink>
            </asp:Panel>
            <asp:Panel ID="pnlFacebookLike" runat="server" CssClass="divFacebookLike" Visible="false">
                <uc:UsrFacebook ID="ucUsrFacebook" runat="server" />
            </asp:Panel>
        </asp:Panel>
    </div>
    <div class="footer-copyright" runat="server" visible="false">
        <div class="wrapper padding">
            <uc:UsrCopy ID="ucUsrCopy" runat="server" />
        </div>
    </div>
</div>