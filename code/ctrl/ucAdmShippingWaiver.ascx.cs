﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucAdmShippingWaiver : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _waiverId = 0;
    protected int _mode = 1;
    protected string _pageListingURL = "";
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int waiverId
    {
        get
        {
            if (ViewState["WAIVERID"] == null)
            {
                return _waiverId;
            }
            else
            {
                return int.Parse(ViewState["WAIVERID"].ToString());
            }
        }
        set { ViewState["WAIVERID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }

    protected void cvCostRange_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("CostRange")))
        {
            string strJS = null;
            strJS = "<script = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtCostEnd.ClientID + "', document.all['" + cvCostRange.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CostRange", strJS, false);
        }
    }

    protected void validateCostRange_server(object source, ServerValidateEventArgs args)
    {
        if ((string.Compare(txtCostStart.Text.Trim(), hdnCostStart.Value) != 0) && (string.Compare(txtCostEnd.Text.Trim(), hdnCostEnd.Value) != 0))
        {
            decimal decFrom = Convert.ToDecimal("0.00");
            decimal decTo = Convert.ToDecimal("0.00");

            decimal decTryParse;

            if (!string.IsNullOrEmpty(txtCostStart.Text.Trim()))
            {
                if (decimal.TryParse(txtCostStart.Text.Trim(), out decTryParse))
                {
                    decFrom = decTryParse;
                }
            }

            if (!string.IsNullOrEmpty(txtCostEnd.Text.Trim()))
            {
                if (decimal.TryParse(txtCostEnd.Text.Trim(), out decTryParse))
                {
                    decTo = decTryParse;
                }
            }

            clsWaiver waiver = new clsWaiver();
            if (waiver.isWaiverOverlap(decFrom, decTo, waiverId))
            {
                args.IsValid = false;
                cvCostRange.ErrorMessage = "<br />This shipping cost range is overlapped.";
            }
            else
            {
                args.IsValid = true;
            }
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            decimal decFrom = Convert.ToDecimal("0.00");
            decimal decTo = Convert.ToDecimal("0.00");
            decimal decWaiver = Convert.ToDecimal("0.00");
            int intActive = 0;

            decimal decTryParse;

            if (!string.IsNullOrEmpty(txtCostStart.Text.Trim()))
            {
                if (decimal.TryParse(txtCostStart.Text.Trim(), out decTryParse))
                {
                    decFrom = decTryParse;
                }
            }

            if (!string.IsNullOrEmpty(txtCostEnd.Text.Trim()))
            {
                if (decimal.TryParse(txtCostEnd.Text.Trim(), out decTryParse))
                {
                    decTo = decTryParse;
                }
            }

            if (!string.IsNullOrEmpty(txtWaive.Text.Trim()))
            {
                if (decimal.TryParse(txtWaive.Text.Trim(), out decTryParse))
                {
                    decWaiver = decTryParse;
                }
            }

            if (chkboxActive.Checked) { intActive = 1; }
            else { intActive = 0; }

            clsWaiver waiver = new clsWaiver();
            int intRecordAffected = 0;

            switch (mode)
            {
                case 1:
                    intRecordAffected = waiver.addWaiver(decFrom, decTo, decWaiver, intActive, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1)
                    {
                        Session["NEWWAIVERID"] = waiver.waiverId;
                        Response.Redirect(currentPageName + "?id=" + waiver.waiverId);
                    }
                    else
                    {
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new shipping waiver details. Please try again.</div>";
                        Response.Redirect(currentPageName);
                    }
                    break;
                case 2:
                    Boolean boolSuccess = true;
                    Boolean boolEdited = false;

                    if (!waiver.isExactSameSet(waiverId, decFrom, decTo, decWaiver, intActive))
                    {
                        intRecordAffected = waiver.updateWaiverById(waiverId, decFrom, decTo, decWaiver, intActive, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;
                        }
                        else
                        {
                            boolSuccess = false;
                        }
                    }

                    if (boolEdited)
                    {
                        Session["EDITEDWAIVERID"] = waiver.waiverId;
                    }
                    else
                    {
                        if (!boolSuccess)
                        {
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit shipping waiver details. Please try again.</div>";
                        }
                        else
                        {
                            Session["EDITEDWAIVERID"] = waiverId;
                            Session["NOCHANGE"] = 1;
                        }
                    }

                    Response.Redirect(Request.Url.ToString());
                    break;
            }
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        int intDeletedWaiverId = 0;

        clsMis.performDeleteWaiver(waiverId, ref intDeletedWaiverId);

        Session["DELETEDWAIVERID"] = intDeletedWaiverId;

        Response.Redirect(currentPageName);
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void setPageProperties()
    {
        clsConfig config = new clsConfig();
        string strCurrency = config.currency;
        litCurrency.Text = strCurrency;

        switch (mode)
        {
            case 1:
                lnkbtnDelete.Visible = false;
                break;
            case 2:
                lnkbtnDelete.Visible = true;
                break;
        }
        
        if (mode == 2)
        {
            fillForm();
        }

        lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteWaiver.Text").ToString() + "'); return false;";
        lnkbtnCancel.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
    }

    protected void fillForm()
    {
        clsWaiver waiver = new clsWaiver();

        if (waiver.extractWaiverById(waiverId, 0))
        {
            txtCostStart.Text = waiver.waiverFrom.ToString();
            hdnCostStart.Value = waiver.waiverFrom.ToString();
            txtCostEnd.Text = waiver.waiverTo.ToString();
            hdnCostEnd.Value = waiver.waiverTo.ToString();
            txtWaive.Text = waiver.waiverValue.ToString();

            if (waiver.waiverActive == 1) { chkboxActive.Checked = true; }
            else { chkboxActive.Checked = false; }
        }
    }
    #endregion
}
