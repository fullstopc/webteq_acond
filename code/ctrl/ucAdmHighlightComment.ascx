﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmHighlightComment.ascx.cs" Inherits="ctrl_ucAdmHighlightComment" %>

<asp:Panel ID="pnlManageComment" runat="server">
    <table id="tblManageComment" cellpadding="0" cellspacing="0" class="formTbl tblData">
        <tr>
            <td class="">Allow comment:</td>
            <td><asp:CheckBox ID="chkAllowComment" runat="server" CssClass="chk" /></td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td class="">Comment:</td>
            <td>
                <asp:Panel ID="pnlHighComment" runat="server" CssClass="divComment">
                    <asp:Repeater ID="rptHighComment" runat="server" OnItemDataBound="rptHighComment_ItemDataBound" OnItemCommand="rptHighComment_ItemCommand">
                        <ItemTemplate>
                            <asp:Panel ID="pnlComment" runat="server" CssClass="divIndComment">
                                <asp:Panel ID="pnlPostedBy" runat="server" CssClass="divPostedBy">
                                    <span class="italicmsg"><asp:Literal ID="litComment" runat="server" Text='<% #DataBinder.Eval(Container.DataItem, "HIGHCOMM_COMMENT").ToString().Replace("\n", "<br />") %>'></asp:Literal><br />
                                    Posted By: <span class="keyword"><asp:Literal ID="litPostedBy" runat="server"></asp:Literal></span></span>
                                </asp:Panel>
                                <asp:Panel ID="pnlReplied" runat="server" Visible="false" CssClass="divReply">
                                    <div class="divReplyMsg">
                                        <span class="italicmsg"><asp:Literal ID="litReply" runat="server" Text='<% #DataBinder.Eval(Container.DataItem, "HIGHREPLY_COMMENT").ToString().Replace("\n", "<br />") %>'></asp:Literal></span>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlReply" runat="server" CssClass="divReply" Visible="false">
                                    <asp:TextBox ID="txtReply" runat="server" MaxLength="1000" TextMode="MultiLine" Rows="5" CssClass="text_big"></asp:TextBox>
                                    <asp:Panel ID="pnlReplyBtn" runat="server" CssClass="divReplyBtn">
                                        <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn" CommandName="cmdSave" CommandArgument='<% #DataBinder.Eval(Container.DataItem, "HIGHCOMM_ID") %>'></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Cancel" ToolTip="Cancel" CssClass="btn" CommandName="cmdCancel"></asp:LinkButton>
                                    </asp:Panel>
                                </asp:Panel>
                                <asp:Panel ID="pnlAction" runat="server" CssClass="divActBtns">
                                    <asp:Panel ID="pnlActionInner" runat="server" CssClass="divActBtnInner">
                                        <asp:Panel ID="pnlActReply" runat="server" CssClass="divActReply">
                                            <asp:LinkButton ID="lnkbtnReply" runat="server" CommandName="cmdReply" CssClass="actLink"></asp:LinkButton>
                                            <div class="divLinkSplit">|</div>
                                        </asp:Panel>
                                        <asp:LinkButton ID="lnkbtnRemove" runat="server" CommandName="cmdRemove" CssClass="actLink" CommandArgument='<% #DataBinder.Eval(Container.DataItem, "HIGHCOMM_ID") %>'></asp:LinkButton>
                                    </asp:Panel>
                                </asp:Panel>
                            </asp:Panel>
                        </ItemTemplate>
                        <SeparatorTemplate>
                            <div class="divCommentSplit"><br /><br /></div>
                        </SeparatorTemplate>
                    </asp:Repeater>
                </asp:Panel>
                <asp:Panel ID="pnlNoComment" runat="server" Visible="false">
                    <span class="italicmsg"><asp:Literal ID="litNoComment" runat="server" meta:resourcekey="litNoComment"></asp:Literal></span>
                </asp:Panel>
            </td>
        </tr>
        <tfoot>
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="btn btnSave" Text="Save" ToolTip="Save" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnCancel" runat="server" CssClass="btn btnBack" Text="Cancel" ToolTip="Cancel" CausesValidation="false"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
    </table>
</asp:Panel>
