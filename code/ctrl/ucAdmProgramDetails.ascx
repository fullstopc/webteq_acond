﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmProgramDetails.ascx.cs" Inherits="ctrl_ucAdmProgramDetails" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>

<script type="text/javascript">
    var txtProgStartDate;
    var txtProgEndDate;
    var txtProgPublishDate;
    var currentTime = new Date();
    var day = currentTime.getDate() + 1;
    var month = currentTime.getMonth() + 1;

    function initSect3() {
        txtProgStartDate = document.getElementById("<%= txtProgStartDate.ClientID %>");
        txtProgEndDate = document.getElementById("<%= txtProgEndDate.ClientID %>");

        $(function() {
            $("#<% =txtProgStartDate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
            $("#<% =txtProgEndDate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });

            $("#<% =txtProgStartDate.ClientID %>").datepicker({ changeMonth: true });
            $("#<% =txtProgStartDate.ClientID %>").datepicker({ changeYear: true });
            $("#<% =txtProgEndDate.ClientID %>").datepicker({ changeMonth: true });
            $("#<% =txtProgEndDate.ClientID %>").datepicker({ changeYear: true });

            //getter
            var changeMonth = $("#<% =txtProgStartDate.ClientID %>").datepicker('option', 'changeMonth');
            var changeYear = $("#<% =txtProgStartDate.ClientID %>").datepicker('option', 'changeYear');
            var changeMonth = $("#<% =txtProgEndDate.ClientID %>").datepicker('option', 'changeMonth');
            var changeYear = $("#<% =txtProgEndDate.ClientID %>").datepicker('option', 'changeYear');

            //setter
            $("#<% =txtProgStartDate.ClientID %>").datepicker('option', 'changeMonth', true);
            $("#<% =txtProgStartDate.ClientID %>").datepicker('option', 'changeYear', true);
            $("#<% =txtProgEndDate.ClientID %>").datepicker('option', 'changeMonth', true);
            $("#<% =txtProgEndDate.ClientID %>").datepicker('option', 'changeYear', true);
        });

        txtProgPublishDate = document.getElementById("<%= txtProgPublishDate.ClientID %>");

        $(function() {
            $("#<% =txtProgPublishDate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });

            $("#<% =txtProgPublishDate.ClientID %>").datepicker({ changeMonth: true });
            $("#<% =txtProgPublishDate.ClientID %>").datepicker({ changeYear: true });

            //getter
            var changeMonth = $("#<% =txtProgPublishDate.ClientID %>").datepicker('option', 'changeMonth');
            var changeYear = $("#<% =txtProgPublishDate.ClientID %>").datepicker('option', 'changeYear');
            var yearRange = $("#<%= txtProgPublishDate.ClientID %>").datepicker('option', 'yearRange');

            //setter
            $("#<% =txtProgPublishDate.ClientID %>").datepicker('option', 'changeMonth', true);
            $("#<% =txtProgPublishDate.ClientID %>").datepicker('option', 'changeYear', true);
            $("#<%= txtProgPublishDate.ClientID %>").datepicker('option', 'yearRange', '-100:+0')
        });
    }

    function validateProgPeriod_client(source, args) {
        var bValid = false;

        source.errormessage = "Please enter valid program period.";
        source.innerHTML = "Please enter valid program period.";

        if ((Trim(txtProgStartDate.value) == '') && (Trim(txtProgEndDate.value) == '')) {
            args.IsValid = true;
        }
        else 
        {
            if (Trim(txtProgStartDate.value) != '') {
                if (validateDate(txtProgStartDate.value)) {
                    bValid = true;
                }
                else {
                    bValid = false;
                }
            }

            if ((bValid) && (Trim(txtProgEndDate.value) != '')) 
            {
                if (validateDate(txtProgEndDate.value)) 
                {
                    if (validateDateRange(txtProgStartDate.value, txtProgEndDate.value)) {
                        bValid = true;
                    }
                    else {
                        bValid = false;
                    }
                }
                else {
                    bValid = false;
                }
            }
            else {
                 bValid = false;
            }

            args.IsValid = bValid;
        }
    }

    function validateDate_client(source, args) {
        var boolValid = false;
        source.errormessage = "<br />Please select a valid publish date";
        source.innerHTML = "<br />Please select a valid publish date";

        if (Trim(txtProgPublishDate.value) == '') {
            args.IsValid = false;
        }
        else {
            if (validateDate(txtProgPublishDate.value)) {
                boolValid = true;
            }
            else {
                boolValid = false;
            }

            if ((boolValid) && (validateDateRange(currentDate, txtProgPublishDate.value))) {

                boolValid = true;
            }
            else {
                boolValid = false;
            }

            args.IsValid = boolValid;
        }
    }

    function validateProgPeriod_client(source, args) {
        
        var bValid = false;

        if ((Trim(txtProgStartDate.value) == '') && (Trim(txtProgEndDate.value) == '')) {
            source.errormessage = "Please enter program period.";
            source.innerHTML = "Please enter program period.";
            
            args.IsValid = false;
        }
        else {
            source.errormessage = "Please enter valid program period.";
            source.innerHTML = "Please enter valid program period.";
            
            if (Trim(txtProgStartDate.value) != '') {
                if (validateDate(txtProgStartDate.value)) {
                    bValid = true;
                }
                else {
                    bValid = false;
                }
            }

            if ((bValid) && (Trim(txtProgEndDate.value) != '')) {
                if (validateDate(txtProgEndDate.value)) {
                    bValid = true;
                }
                else {
                    bValid = false;
                }
            }
            else {
                bValid = false;
            }

            if ((bValid) && (validateDateRange(txtProgStartDate.value, txtProgEndDate.value))) {
                bValid = true;
            }
            else {
                bValid = false;
            }

            args.IsValid = bValid;
        }
    }
</script>

<asp:Panel ID="pnlProgramContainer" runat="server">
    <asp:Panel ID="pnlProgramDetailsContainer" runat="server">
        <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr"><asp:Literal ID="litProgramHdr" runat="server" meta:resourcekey="litProgramHdr"></asp:Literal></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblProgramName" runat="server" meta:resourcekey="lblProgramName"></asp:Label>:<span class="attention_compulsory">*</span></td>
                <td class="tdMax">
                    <asp:TextBox ID="txtProgName" runat="server" CssClass="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvProgName" runat="server" meta:resourcekey="rfvProgName" ControlToValidate="txtProgName" Display="Dynamic" CssClass="errmsg" ValidationGroup="programdetails"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr id="trProgramNameZh" runat="server" visible="false">
                <td class="tdLabel"><asp:Label ID="lblProgramNameZh" runat="server" meta:resourcekey="lblProgramNameZh"></asp:Label>:<span class="attention_compulsory">*</span></td>
                <td class="tdMax">
                    <asp:TextBox ID="txtProgNameZh" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvProgNameZh" runat="server" meta:resourcekey="rfvProgNameZh" ControlToValidate="txtProgNameZh" Display="Dynamic" CssClass="errmsg" ValidationGroup="programdetails"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblProgramSnapshot" runat="server" meta:resourcekey="lblProgramSnapshot"></asp:Label>:</td>
                <td class="tdMax"><asp:TextBox ID="txtProgSnapshot" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
            </tr>
            <tr id="trProgramSnapshotZh" runat="server" visible="false">
                <td class="tdLabel"><asp:Label ID="lblProgramSnapshotZh" runat="server" meta:resourcekey="lblProgramSnapshotZh"></asp:Label>:</td>
                <td class="tdMax"><asp:TextBox ID="txtProgSnapshotZh" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblProgramDesc" runat="server" meta:resourcekey="lblProgramDesc"></asp:Label>:</td>
                <td class="tdMax"><asp:TextBox ID="txtProgDesc" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
            </tr>
            <tr id="trProgramDescZh" runat="server" visible="false">
                <td class="tdLabel"><asp:Label ID="lblProgramDescZh" runat="server" meta:resourcekey="lblProgramDescZh"></asp:Label>:</td>
                <td class="tdMax"><asp:TextBox ID="txtProgDescZh" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblProgramImage" runat="server" meta:resourcekey="lblProgramImage"></asp:Label>:</td>
                <td>
                    <asp:Panel ID="pnlFileUpload" runat="server" CssClass="divFileUpload">
                        <asp:Panel ID="pnlFileUploadAction" runat="server" CssClass="divFileUploadAction">
                            <asp:FileUpload ID="fileProgImage" runat="server" ValidationGroup="programdetails" />
                        </asp:Panel>
                        <span class="spanTooltip">
                            <span class="icon"></span>
                            <span class="msg">
                                <asp:Literal ID="litUploadRule" runat="server"></asp:Literal>
                            </span>
                        </span>
                    </asp:Panel>
                    <asp:HiddenField ID="hdnProgImage" runat="server" />
                    <asp:HiddenField ID="hdnProgImageRef" runat="server" />
                    <asp:CustomValidator ID="cvProgImage" runat="server" ControlToValidate="fileProgImage" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateProgImage_server" OnPreRender="cvProgImage_PreRender" ValidationGroup="programdetails"></asp:CustomValidator>
                    <asp:Panel ID="pnlProgImage" runat="server" Visible="false">
                        <br /><asp:LinkButton ID="lnkbtnDeleteImage" runat="server" Text="Delete" ToolTip="Delete" CausesValidation="false" OnClick="lnkbtnDeleteImage_Click"></asp:LinkButton>
                        <br /><asp:Image ID="imgProgImage" runat="server" />
                    </asp:Panel>
                </td>
            </tr>
            

            <tr>
                <td class="tdLabelProg"><asp:Label ID="lblProgDate" runat="server" meta:resourcekey="lblProgDate"></asp:Label>:</td>
                <td>
                    <asp:TextBox ID="txtProgStartDate" runat="server" CssClass="text_medium" ClientValidationFunction="validateProgPeriod_client"></asp:TextBox> to 
                    <asp:TextBox ID="txtProgEndDate" runat="server" CssClass="text_medium" ClientValidationFunction="validateProgPeriod_client"></asp:TextBox>
                    <asp:LinkButton ID="lnkbtnAdd" runat="server" CssClass="btnConfirm" Text="Add" OnClick="btnAdd_Click" ValidationGroup="vgProgDate"></asp:LinkButton>
                    <asp:CustomValidator ID="cvProgPeriod" runat="server" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateProgPeriod_client" ValidationGroup="vgProgDate"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabelProg"></td>
                <td>
                    <asp:UpdatePanel ID="upnlRptDate" runat="server">
                        <ContentTemplate>
                            <asp:UpdateProgress ID="uprgRptDate" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="upnlRptDate">
                                <ProgressTemplate>
                                    <uc:AdmLoading ID="ucAdmLoading" runat="server" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:Panel ID="pnlProgramDate" runat="server" CssClass="divProgramDate">
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
                   
            <tr>
                <td class="tdLabel"><asp:Label ID="lblProgFee" runat="server" meta:resourcekey="lblProgFee"></asp:Label>:</td>
                <td class="tdMax"><asp:TextBox ID="txtProgFee" runat="server" CssClass="text_medium"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblProgPublishDate" runat="server" meta:resourcekey="lblProgPublishDate"></asp:Label>:</td>
                <td class="tdMax"><asp:TextBox ID="txtProgPublishDate" runat="server" CssClass="text_big"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel nobr"><asp:Label ID="lblProdTotalParticipant" runat="server" meta:resourcekey="lblProdTotalParticipant"></asp:Label>:</td>
                <td class="tdMax"><asp:TextBox ID="txtProgTotalParticipant" runat="server" CssClass="text_medium"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblProgNoOfHours" runat="server" meta:resourcekey="lblProgNoOfHours"></asp:Label></td>
                <td class="tdMax"><asp:TextBox ID="txtProgNoOfHours" runat="server" CssClass="text_medium"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="tdSectionHdr"><asp:Literal ID="litProgramSettings" runat="server" meta:resourcekey="litProgramSettings"></asp:Literal></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblCat" runat="server" meta:resourcekey="lblCat"></asp:Label>:<span class="attention_compulsory">*</span></td>
                <td><asp:DropDownList ID="ddlCat" runat="server" CssClass="ddl"></asp:DropDownList>
                    <asp:HiddenField ID="hdnCat" runat="server" />
                    <asp:RequiredFieldValidator ID="rfvCat" runat="server" ErrorMessage="<br/>Please select one category" ControlToValidate="ddlCat" Display="Dynamic" CssClass="errmsg" ValidationGroup="programdetails"></asp:RequiredFieldValidator>
                </td>
            </tr> 
            <tr>
                <td class="tdLabel"><asp:Label ID="lblProgActive" runat="server" meta:resourcekey="lblProgActive"></asp:Label>:</td>
                <td class="tdMax"><asp:CheckBox ID="chkboxProgActive" runat="server" Checked="true" /></td>
            </tr>
            <tr>
                <td class="tdLabel nobr"><asp:Label ID="lblProgOnlineReg" runat="server" meta:resourcekey="lblProgOnlineReg"></asp:Label>:</td>
                <td class="tdMax"><asp:CheckBox ID="chkboxOnlineReg" runat="server" Checked="true" />Tick to allow online registration</td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblProgArea" runat="server" meta:resourcekey="lblProgArea"></asp:Label>:<span class="attention_compulsory">*</span></td>
                <td class="tdMax">
                    <asp:DropDownList ID="ddlProgArea" runat="server" CssClass="ddl"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvProgArea" runat="server" meta:resourcekey="rfvProgArea" ControlToValidate="ddlProgArea" Display="Dynamic" CssClass="errmsg" ValidationGroup="programdetails"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="programdetails"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CssClass="btn btnCancel" Visible="false" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>
