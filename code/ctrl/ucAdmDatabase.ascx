﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmDatabase.ascx.cs" Inherits="ctrl_ucAdmDatabase" %>
<%@ Register Src="~/ctrl/ucUsrLoading.ascx" TagName="UsrLoading" TagPrefix="uc" %>

<asp:Panel ID="pnlForm" runat="server">
    <asp:UpdatePanel ID="upnlCorrespondEdit" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="uprgCorrespondEdit" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="upnlCorrespondEdit">
                <ProgressTemplate>
                    <uc:UsrLoading ID="ucUsrLoadingCorrespondEdit" runat="server" />
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:Panel ID="pnlFormDB" runat="server" CssClass="divFormDB">
                <table id="tblDB" cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
                    <tr>
                        <td colspan="4" class="tdSectionHdr">DB Details<asp:ImageButton ID="imgbtnTrans" Width="110px" Height="10px" CssClass="imgbtnTrans" runat="server" /></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="tdSpacer"></td>
                    </tr>
                    <tr>
                        <td class="tdLabel nobr"><asp:Label ID="lblSQLScript" runat="server" meta:resourcekey="lblSQLScript"></asp:Label>:</td>
                        <td colspan="3" class="tdMax">
                            <asp:TextBox ID="txtSQLScript" runat="server" class="text_big2" TextMode="MultiLine" Rows="10" OnTextChanged="txtSQLScript_TextChanged" AutoPostBack="true" ValidationGroup="vgSQL"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvSQLScript" runat="server" ErrorMessage="Please enter SQL script." ControlToValidate="txtSQLScript" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgSQL"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="cvSQLScript" runat="server" CssClass="errmsg" ControlToValidate="txtSQLScript" Display="Dynamic" OnServerValidate="validateSQLScript_server" SetFocusOnError="true" ValidationGroup="vgSQL"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="3">
                            <asp:DropDownList ID="ddlSQLCommands" runat="server" CssClass="ddl" ValidationGroup="vgSQL"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvSQLCommands" runat="server" ErrorMessage="Please select SQL command." ControlToValidate="ddlSQLCommands" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgSQL"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="cvSQLCommands" runat="server" CssClass="errmsg" ControlToValidate="ddlSQLCommands" Display="Dynamic" OnServerValidate="validateSQLCommands_server" SetFocusOnError="true" ValidationGroup="vgSQL"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="tdSpacer">
                            &nbsp;
                        </td>
                    </tr>
                    <tr data-beforeleave="0">
                        <td class="tdLabel"><asp:Label ID="lblProject" runat="server" meta:resourcekey="lblProject"></asp:Label>:</td>
                        <td class="tdlistbox">
                            <asp:ListBox ID="lstboxAllProjects" runat="server" SelectionMode="Multiple" CssClass="ddl3" Rows="10" Height="310px"></asp:ListBox>                
                        </td>
                        <td class="tdbtnNextPrev">
                            <asp:LinkButton ID="lnkbtnNext" runat="server" Text=">" ToolTip=">" class="btnNext" OnClick="lnkbtnNext_Click"></asp:LinkButton><br />
                            <asp:LinkButton ID="lnkbtnPrev" runat="server" Text="<" ToolTip="<" class="btnNext" OnClick="lnkbtnPrev_Click"></asp:LinkButton>
                        </td>
                        <td class="tdlistbox">
                            <asp:ListBox ID="lstboxSelectedProjects" runat="server" SelectionMode="Multiple" CssClass="ddl3" Rows="10" Height="310px"></asp:ListBox>      
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            <asp:CheckBox ID="chkboxAllProjects" runat="server" Checked="false" OnCheckedChanged="chkboxAllProjects_CheckedChanged" AutoPostBack="true" />
                            <asp:Label ID="lblAllProjects" runat="server" meta:resourcekey="lblAllProjects"></asp:Label>
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnClearAll" runat="server" Text="Clear All" ToolTip="Clear All" OnClick="lnkbtnClearAll_Click"></asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                        <td>
                            <asp:Literal ID="litRecSelected" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="tdSpacer2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="tdSpacer">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="3">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnGo" runat="server" Text="Go" ToolTip="Go" class="btn" ValidationGroup="vgSQL" OnClick="lnkbtnGo_Click"></asp:LinkButton>   
                                <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" Enabled="false"></asp:LinkButton>                                 
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckDB">
                <asp:Literal ID="litAck" runat="server"></asp:Literal>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" style="display:none" ValidationGroup="vgSQL"/>
    <asp:Button ID="btnTransImg" runat="server" OnClick="btnTransImg_Click" style="display:none"/>  
</asp:Panel>

<script type="text/javascript">
    function pageLoad() {
      var pressTimer
        $("#ctl00_cphContent_AdmDatabase_lnkbtnBack").dblclick(function() {
            $('input[id$=btnBack]').click();
        });
    
      var pressTransImg
      $("#ctl00_cphContent_AdmDatabase_imgbtnTrans").mouseup(function() {
          clearTimeout(pressTransImg)
          // Clear timeout
          return false;
      }).mousedown(function() {
      // Set timeout
        pressTransImg = window.setTimeout(function() { $('input[id$=btnTransImg]').click(); }, 3000)
          return false;
      });       
        
//        $("#ctl00_cphContent_AdmDatabase_lnkbtnGo").longpress(function() {
//            $('input[id$=btnCutOffDate]').click();
//        }, function() {
//        }, 2000);
  }
</script>


