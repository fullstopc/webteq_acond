﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmFAQ.ascx.cs" Inherits="ctrl_ucAdmFAQ" %>

<asp:Panel ID="pnlForm" runat="server">
    <table cellpadding="0" cellspacing="0" class="formTbl">
        <tr id="trHeader" runat="server" visible="false">
            <td class="tdSectionHdr" colspan="2"><asp:Literal ID="litHeader" runat="server"></asp:Literal></td>
        </tr>
        <tr id="trHeaderSpace" runat="server" visible="false">
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td class="tdLabelNor2">Title:</td>
            <td class="tdMax"><span class="boldmsg"><asp:Literal ID="litTitle" runat="server"></asp:Literal></span></td>
        </tr>
        <tr id="trTitleJp" runat="server" visible="false">
            <td class="tdLabelNor2">Title (Japanese):</td>
            <td class="tdMax"><span class="boldmsg"><asp:Literal ID="litTitleJp" runat="server"></asp:Literal></span></td>
        </tr>
        <tr id="trTitleMs" runat="server" visible="false">
            <td class="tdLabelNor2">Title (Malay):</td>
            <td class="tdMax"><span class="boldmsg"><asp:Literal ID="litTitleMs" runat="server"></asp:Literal></span></td>
        </tr>
        <tr id="trTitleZh" runat="server" visible="false">
            <td class="tdLabelNor2">Title (Chinese):</td>
            <td class="tdMax"><span class="boldmsg"><asp:Literal ID="litTitleZh" runat="server"></asp:Literal></span></td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr id="trShortDesc" runat="server">
            <td class="tdLabelNor2 nobr">Short Description:</td>
            <td><asp:Literal ID="litShortDesc" runat="server"></asp:Literal></td>
        </tr>
        <tr id="trShortDescJp" runat="server" visible="false">
            <td class="tdLabelNor2 nobr">Short Description (Japanese):</td>
            <td><asp:Literal ID="litShortDescJp" runat="server"></asp:Literal></td>
        </tr>
        <tr id="trShortDescMs" runat="server" visible="false">
            <td class="tdLabelNor2 nobr">Short Description (Malay):</td>
            <td><asp:Literal ID="litShortDescMs" runat="server"></asp:Literal></td>
        </tr>
        <tr id="trShortDescZh" runat="server" visible="false">
            <td class="tdLabelNor2 nobr">Short Description (Chinese):</td>
            <td><asp:Literal ID="litShortDescZh" runat="server"></asp:Literal></td>
        </tr>
    </table>
</asp:Panel>