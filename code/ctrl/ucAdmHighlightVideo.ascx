﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmHighlightVideo.ascx.cs" Inherits="ctrl_ucAdmHighlightVideo" %>

<script type="text/javascript">
    function validateHighVideo_client(source, args) {
        var txtHighThumbnail = document.getElementById("<%= txtHighThumbnail.ClientID %>");
        
        if (Trim(txtHighThumbnail.value) != '') {
            if (Trim(args.Value) == '') {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }
    }

    function validateHighThumbnail_client(source, args) {
        var txtHighVideo = document.getElementById("<%= txtHighVideo.ClientID %>");
        
        if (Trim(txtHighVideo.value) != '') {
            if (Trim(args.Value) == '') {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }
    }
</script>
<asp:Panel ID="pnlForm" runat="server">
    <table id="tblAddVideo" cellpadding="0" cellspacing="0" class="formTbl tblData">
        <tr>
            <td class="tdLabel">Thumbnail:</td>
            <td>
                <asp:TextBox ID="txtHighThumbnail" runat="server" MaxLength="1000" CssClass="text_big" ValidationGroup="vgHighVideo"></asp:TextBox> <span class="fieldDesc">
                <span class="spanTooltip">
                    <span class="icon"></span>
                    <span class="msg">
                        Sample: http://i4.ytimg.com/vi/<b>xxxxxxxxxxx</b>/default.jpg
                    </span>
                
                </span>
                <br />
                <span class="fieldDesc"><asp:CustomValidator ID="cvHighThumbnail" runat="server" ControlToValidate="txtHighThumbnail" Display="Dynamic" ErrorMessage="Please enter video thumbnail link." CssClass="errmsg" ClientValidationFunction="validateHighThumbnail_client" ValidationGroup="vgHighVideo" ValidateEmptyText="true"></asp:CustomValidator></span>
            </td>
        </tr>
        <tr>
            <td class="tdLabel">Video:</td>
            <td>
                <asp:TextBox ID="txtHighVideo" runat="server" MaxLength="1000" CssClass="text_big" ValidationGroup="vgHighVideo"></asp:TextBox>
                <span class="spanTooltip">
                    <span class="icon"></span>
                    <span class="msg">
                        Sample: http://www.youtube.com/embed/<b>xxxxxxxxxxx</b>
                    </span>
                
                </span>
                <br />
                <span class="fieldDesc"><asp:CustomValidator ID="cvHighVideo" runat="server" ControlToValidate="txtHighVideo" Display="Dynamic" ErrorMessage="Please enter video link." CssClass="errmsg" ClientValidationFunction="validateHighVideo_client" ValidationGroup="vgHighVideo" ValidateEmptyText="true"></asp:CustomValidator></span>
            </td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="tdSectionHdr">Uploaded video(s):</td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2"><asp:Literal ID="litVideoFound" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="pnlHighlightVideo" runat="server" CssClass="divHighVideo">
                    <asp:Repeater ID="rptHighVideo" runat="server" OnItemDataBound="rptHighVideo_ItemDataBound" OnItemCommand="rptHighVideo_ItemCommand">
                        <ItemTemplate>
                            
                            <asp:Panel ID="pnlHighVideo" runat="server" CssClass="divIndImage">
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td>
                                             <div class="divVideo"><asp:ImageButton ID="imgbtnHighVideo" runat="server" CommandName="cmdVideo" style="width: 100%;"/></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <asp:LinkButton ID="lnkbtnRemove" runat="server" Text="Remove" ToolTip="Remove" CommandName="cmdRemove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "HIGHVIDEO_ID") %>'></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </td>
        </tr>
        <tfoot>
        <tr>
            <td colspan="2">
                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="vgHighVideo"></asp:LinkButton>
                <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Cancel" ToolTip="Cancel" CssClass="btn btnBack" CausesValidation="false"></asp:LinkButton>
            </td>
        </tr>
        </tfoot>
    </table>
</asp:Panel>
