﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Linq;

public partial class ctrl_ucAdmMemberDetails : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected string _pageListingURL = "";
    protected int _memId = 0;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    public int memId
    {
        get
        {
            if (ViewState["MEMID"] == null)
            {
                return _memId;
            }
            else
            {
                return int.Parse(ViewState["MEMID"].ToString());
            }
        }
        set { ViewState["MEMID"] = value; }
    }
    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string url = currentPageName;
            int intAdmID = Convert.ToInt32(Session["ADMID"]);

            clsMember member = new clsMember();
            member.name = txtName.Text.Trim();
            member.email = txtEmail.Text.Trim();
            member.contactTel = txtContactNo.Text.Trim();
            member.contactPrefix = txtContactPrefix.Text.Trim();
            member.loginUsername = txtLoginUsername.Text.Trim();
            member.active = chkActive.Checked ? 1 : 0;
            member.ID = mode == 2 ? memId : -1;

            if (!string.IsNullOrEmpty(txtLoginPassword.Text))
            {
                member.loginPassword = CryptorEngine.Encrypt(txtLoginPassword.Text, true);
            }

            bool isSuccess = false;

            if (mode == 1)
            {
                if (member.add())
                {
                    Session[clsKey.SESSION_ADDED] = member.ID;
                    Session[clsKey.SESSION_MSG] = String.Format(GetGlobalResourceObject("GlobalResource", "admSavedMessageWithParams.Text").ToString(), "Resident");
                    isSuccess = true;
                }

            }
            else if (mode == 2)
            {
                if (!member.isSame())
                {
                    if (member.update())
                    {
                        isSuccess = true;
                        Session[clsKey.SESSION_EDITED] = member.ID;
                        Session[clsKey.SESSION_MSG] = String.Format(GetGlobalResourceObject("GlobalResource", "admSavedMessageWithParams.Text").ToString(), "Resident");
                    }
                }
                else
                {
                    isSuccess = true;
                    Session[clsKey.SESSION_NOCHANGE] = member.ID;
                    Session[clsKey.SESSION_MSG] = GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString();
                }
            }


            if (!isSuccess)
            {
                Session[clsKey.SESSION_ERROR] = member.ID;
                Session[clsKey.SESSION_MSG] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit . Please try again.</div>";
            }
            else
            {
                url += "?id=" + member.ID;
            }

            
            Response.Redirect(url);
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            if(mode == 2) { fillForm();  }

        }
    }

    protected void fillForm()
    {
        clsMember member = new clsMember();
        if (member.extractByID(memId))
        {
            txtEmail.Text = member.email;
            txtName.Text = member.name;
            txtContactNo.Text = member.contactTel;
            txtContactPrefix.Text = member.contactPrefix;
            txtLoginUsername.Text = member.loginUsername;
            chkActive.Checked = member.active == 1;
            txtLoginPassword.Text = CryptorEngine.Decrypt(member.loginPassword, true);
            txtLoginPasswordRepeat.Text = CryptorEngine.Decrypt(member.loginPassword, true);

            lblLastLogin.Text = ((member.loginTimestamp == new DateTime(1900, 1, 1)) ? " - " : member.loginTimestamp.ToString("d MMM yyyy hh:mm tt"));
            lblLastUpdated.Text = member.lastupdated.ToString("d MMM yyyy hh:mm tt");
            lblCreation.Text = member.creation.ToString("d MMM yyyy hh:mm tt");
            pnlLastLogin.Visible = true;

        }
    }
    #endregion


    #region "Custom Validator for Unique fields"
    protected void cvLoginUsername_ServerValidate(object source, ServerValidateEventArgs args)
    {
        string username = txtLoginUsername.Text.Trim();
        DataTable members = new clsMember().getDataTable();
        var member = members.AsEnumerable()
                            .Where(x => Convert.ToString(x["mem_login_username"]) == username &&
                                    Convert.ToInt32(x["mem_id"]) != memId);
        args.IsValid = !member.Any();
    }
    protected void cvLoginUsername_PreRender(object sender, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("mem_login_username"))
        {
            string strJS = "ValidatorHookupControlID('" + txtLoginUsername.ClientID + "', document.all['" + cvLoginUsername.ClientID + "']);";
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "mem_login_username", strJS, true);
        }
    }

    protected void validateEmail_server(object source, ServerValidateEventArgs args)
    {
        string email = txtEmail.Text.Trim();
        DataTable members = new clsMember().getDataTable();
        var member = members.AsEnumerable()
                            .Where(x => Convert.ToString(x["mem_email"]) == email &&
                                    Convert.ToInt32(x["mem_id"]) != memId);
        args.IsValid = !member.Any();
    }
    protected void cvEmail_PreRender(object source, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("mem_email"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtEmail.ClientID + "', document.all['" + cvEmail.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "mememail", strJS, false);
        }
    }

    protected void validateContactNo_server(object source, ServerValidateEventArgs args)
    {
        string prefix = txtContactPrefix.Text.Trim();
        string contactNo = txtContactNo.Text.Trim();
        DataTable members = new clsMember().getDataTable();
        var member = members.AsEnumerable().Where(x => Convert.ToString(x["mem_tel_prefix"]) == prefix
                                   && Convert.ToString(x["mem_contact_tel"]) == contactNo
                                   && Convert.ToInt32(x["mem_id"]) != memId);
        args.IsValid = !member.Any();
    }
    protected void cvContactNo_PreRender(object sender, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("mem_contact_tel"))
        {
            string strJS = "ValidatorHookupControlID('" + txtContactNo.ClientID + "', document.all['" + cvContactNo.ClientID + "']);";
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "mem_contact_tel", strJS, true);
        }
    }
    #endregion


    #region "Button Events"
    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string name = "";

        clsMember member = new clsMember();
        member.ID = memId;

        if (member.extractByID(memId))
        {
            name = member.name;
        }

        if (member.delete() == 1)
        {
            Session[clsKey.SESSION_DELETED] = 1;
            Session[clsKey.SESSION_MSG] = "Member " + name + " delete successfully.";
        }
        Response.Redirect(pageListingURL);
    }

    protected void lnkbtnDeleteProperty_Click(object sender, EventArgs e)
    {
        int objectID = int.Parse(hdnPropertyID.Value);
        clsProperty property = new clsProperty();
        property.extractByID(objectID);

        string msg = "";
        if (property.delete() == 1)
        {
            msg = GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString();
        }
        else
        {
            msg = GetGlobalResourceObject("GlobalResource", "admFailedDelete.Text").ToString();
        }
        Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.propertySaveSuccess('" + msg + "')", true);
    }
    #endregion"
}