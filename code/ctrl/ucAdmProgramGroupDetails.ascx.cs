﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;
public partial class ctrl_ucAdmProgramGroupDetails : System.Web.UI.UserControl
{
    #region "Properties"
    //protected int sectId = 4;
    protected int _mode = 1;
    protected int _progGroupId = 0;
    protected int _formSect = 1;
    protected string _pageListingURL = "";
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int progGroupId
    {
        get
        {
            if (ViewState["progGroupId"] == null)
            {
                return _progGroupId;
            }
            else
            {
                return Convert.ToInt16(ViewState["progGroupId"]);
            }
        }
        set { ViewState["progGroupId"] = value; }
    }

    public int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set { ViewState["FORMSECT"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if (ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void validateProgCatCode_server(object source, ServerValidateEventArgs args)
    {
        txtProgCatCode.Text = txtProgCatCode.Text.Trim().ToUpper();

        clsProgram prog = new clsProgram();
        if (prog.isProgGrpExist(txtProgCatCode.Text, progGroupId))
        {
            args.IsValid = false;
            cvProgCatCode.ErrorMessage = "<br/>This program category code is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvProgCatCode_PreRender(object source, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("catcode")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtProgCatCode.ClientID + "', document.all['" + cvProgCatCode.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "catcode", strJS);
        }
    }

    protected void validateProgCatName_server(object source, ServerValidateEventArgs args)
    {
        txtProgCatName.Text = txtProgCatName.Text.Trim();

        clsProgram prog = new clsProgram();
        if (prog.isProgGrpNameExist(txtProgCatName.Text, progGroupId))
        {
            args.IsValid = false;
            cvProgCatName.ErrorMessage = "<br/>This program category name is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void validateProgCatNameZH_server(object source, ServerValidateEventArgs args)
    {
        txtProgCatName_zh.Text = txtProgCatName_zh.Text.Trim();

        clsProgram prog = new clsProgram();
        if (prog.isProgGrpNameZhExist(txtProgCatName_zh.Text, progGroupId))
        {
            args.IsValid = false;
            cvProgCatName_zh.ErrorMessage = "<br/>This program category name (Chinese) is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvProgCatName_PreRender(object source, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("catname"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtProgCatName.ClientID + "', document.getElementById('" + cvProgCatName.ClientID + "'));";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "catname", strJS, false);
        }
    }

    protected void cvProgCatNameZH_PreRender(object source, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("catnamezh"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtProgCatName_zh.ClientID + "', document.getElementById('" + cvProgCatName_zh.ClientID + "'));";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "catnamezh", strJS, false);
        }
    }

    protected void validateProgCatImage_server(object source, ServerValidateEventArgs args)
    {
        if (fileProgCatImage.HasFile)
        {
            int intImgMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            //if (fileProgCatImage.PostedFile.ContentLength > clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH)
            //{
            //    cvProgCatImage.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH, "kb") + ".";
            //    args.IsValid = false;
            //}

            if (fileProgCatImage.PostedFile.ContentLength > intImgMaxSize)
            {
                cvProgCatImage.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + ".";
                args.IsValid = false;
            }
            else
            {
                Match matchImage = Regex.Match(fileProgCatImage.FileName, clsAdmin.CONSTADMIMGALLOWEDEXT);
                if (matchImage.Success)
                {
                    args.IsValid = true;
                }
                else
                {
                    cvProgCatImage.ErrorMessage = "<br />Please enter valid program category image. (" + clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + ").";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cvProgCatImage_PreRender(object source, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("catimage")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + fileProgCatImage.ClientID + "', document.getElementById['" + cvProgCatImage.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "catimage", strJS, false);
        }
    }

    protected void lnkbtnCancel_Click(object sender, EventArgs e)
    {
        Session["LISTING"] = 1;
        Response.Redirect(pageListingURL);
    }

    protected void lnkbtnDeleteImage_Click(object sender, EventArgs e)
    {
        pnlProgCatImage.Visible = false;
        hdnProgCatImage.Value = "";
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string uploadPath = Server.MapPath(ConfigurationManager.AppSettings["uplBase"].ToString() + "/" + clsAdmin.CONSTIADMPROGGRPFOLDER.ToString() + "/");

        clsProgram prog = new clsProgram();
        prog.extractProgGrpById(progGroupId, 0);

        int intRecordAffected = prog.deleteProgGroup(progGroupId);

        if (intRecordAffected == 1)
        {
            if (prog.progGrpImage != "") { File.Delete(uploadPath + prog.progGrpImage); }

            Session["DELETEDPROGGRPNAME"] = prog.progGrpName;

            Response.Redirect(currentPageName);
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strProgGrpCode = "";
            string strProgGrpName = "";
            string strProgGrpNameZh = "";
            string strProgGrpDName = "";
            string strProgGrpDNameZh = "";
            string strProgGrpImage = "";
            int intProgGrpShowtop = 0;
            int intProgGrpActive = 0;

            strProgGrpCode = txtProgCatCode.Text.Trim();
            strProgGrpName = txtProgCatName.Text.Trim();
            strProgGrpNameZh = txtProgCatName_zh.Text.Trim();
            strProgGrpDName = txtProgCatDName.Text.Trim();
            strProgGrpDNameZh = txtProgCatDName_zh.Text.Trim();

            if (chkboxActive.Checked) { intProgGrpActive = 1; }

            if (fileProgCatImage.PostedFile.FileName != "")
            {
                try
                {
                    HttpPostedFile postedFile = fileProgCatImage.PostedFile;
                    string filename = Path.GetFileName(postedFile.FileName);
                    string uploadPath = Server.MapPath(ConfigurationManager.AppSettings["uplBase"].ToString() + "/" + clsAdmin.CONSTIADMPROGGRPFOLDER.ToString() + "/");
                    string newFilename = clsMis.GetUniqueFilename(filename, uploadPath);
                    postedFile.SaveAs(uploadPath + newFilename);
                    strProgGrpImage = newFilename;

                }
                catch (Exception ex)
                {
                    clsLog.logErroMsg(ex.Message.ToString());
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />" + ex.Message.ToString() + "</div>";

                    throw ex;
                }
            }
            else
            {
                if (mode == 1)
                {
                    strProgGrpImage = "";
                }
                else
                {
                    strProgGrpImage = hdnProgCatImage.Value;
                }
            }

            clsProgram prog = new clsProgram();
            int intRecordAffected = 0;

            switch (mode)
            {
                case 1: //Add Program Category
                    intRecordAffected = prog.addProgGroup(strProgGrpCode, strProgGrpName, strProgGrpDName, strProgGrpImage, intProgGrpShowtop, intProgGrpActive, Convert.ToInt16(Session["ADMID"]));

                    if (intRecordAffected == 1)
                    {
                        Session["NEWPROGGRPID"] = prog.progGrpId;
                        Response.Redirect(currentPageName + "?id=" + prog.progGrpId);
                    }
                    else
                    {
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new program category. Please try again.</div>";
                        Response.Redirect(currentPageName);
                    }
                    break;
                case 2: //Edit Program category
                    Boolean boolEdited = false;
                    Boolean boolSuccess = true;

                    if (!prog.isExactSameProgGrp(progGroupId, strProgGrpCode, strProgGrpName, strProgGrpDName, strProgGrpImage, intProgGrpShowtop, intProgGrpActive))
                    {
                        intRecordAffected = prog.updateProgGroupById(progGroupId, strProgGrpName, strProgGrpDName, strProgGrpImage, intProgGrpShowtop, intProgGrpActive, Convert.ToInt16(Session["ADMID"]));

                        if (intRecordAffected == 1)
                        {
                            string uploadPath = Server.MapPath(ConfigurationManager.AppSettings["uplBase"].ToString() + "/" + clsAdmin.CONSTIADMPROGGRPFOLDER.ToString() + "/");
                            if(hdnProgCatImage.Value == "" && hdnProgCatImageRef.Value != "") { File.Delete(uploadPath + hdnProgCatImageRef.Value); }

                            boolEdited = true;
                        }
                        else { boolSuccess = false; }
                    }

                    if (boolEdited)
                    {
                        Session["EDITEDPROGGRPID"] = prog.progGrpId;
                    }
                    else
                    {
                        if (!boolSuccess)
                        {
                            prog.extractProgGrpById(progGroupId, 0);
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit program category (" + prog.progGrpName + "). Please try again.</div>";
                        }
                        else
                        {
                            Session["EDITEDPROGGRPID"] = progGroupId;
                            Session["NOCHANGE"] = 1;
                        }
                    }

                    Response.Redirect(Request.Url.ToString());
                    break;
                default:
                    break;
            }
        }
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        lblProgGroupCode.Text = "Group Code";
        litProgGroupDetails.Text = "Program Group Details";
        lblProgGroupName.Text = "Group Name";
        lblProgGroupName_zh.Text = "Group Chinese Name";
        lblProgGroupDName.Text = "Group Display Name";
        lblProgGroupDName_zh.Text = "Group Display Chinese Name";
        lblProgGroupImage.Text = "Group Image";

        litProgGroupSetting.Text = "Program Group Settings";

        if (!IsPostBack)
        {
            hypUserViewLink.Text = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/product.aspx?pgid=8&grpid=" + progGroupId;
            hypUserViewLink.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/product.aspx?pgid=8&grpid=" + progGroupId;
            hypUserViewLink.Target = "_blank";

            if (mode == 2)
            {
                trAdmLink.Visible = trAdmLinkSpacer.Visible = false;

                lnkbtnDelete.Visible = true;
                fillForm();

                lnkbtnDelete.OnClientClick = "javascript:return confirm'" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteProgGroup.Text").ToString() + "'; return false;";
            }

            //lnkbtnCancel.OnClientClick = "javascript:document.location.href='" + pageListingURL + (parentId > 0 ? "?id=" + parentId : "") + "'; return false;";

            int intImgMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            //litUploadRule.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH, "kb") + " | " + clsMis.formatAllowedFileDimension(clsAdmin.CONSTADMDEFAULTIMGWIDTH, clsAdmin.CONSTADMDEFAULTIMGHEIGHT, "px");
            litUploadRule.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + " | " + clsMis.formatAllowedFileDimension(clsAdmin.CONSTADMDEFAULTIMGWIDTH, clsAdmin.CONSTADMDEFAULTIMGHEIGHT, "px");
        }
    }

    protected void fillForm()
    {
        clsProgram prog = new clsProgram();

        if (prog.extractProgGrpById(progGroupId, 0))
        {
            txtProgCatCode.Text = prog.progGrpCode;
            txtProgCatCode.Enabled = false;

            txtProgCatName.Text = prog.progGrpName;
            txtProgCatDName.Text = prog.progGrpDName;

            if (prog.progGrpActive == 1) { chkboxActive.Checked = true; }
            else { chkboxActive.Checked = false; }

            if(prog.progGrpImage != "")
            {
                pnlProgCatImage.Visible = true;
                string strImagePath = ConfigurationManager.AppSettings["uplBase"] + clsAdmin.CONSTADMPROGRAMFOLDER + "/" + prog.progGrpImage;
                imgProgCatImage.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + strImagePath + "&w=" + clsAdmin.CONSTADMDEFAULTIMGWIDTH + "&h=" + clsAdmin.CONSTADMDEFAULTIMGHEIGHT;
                hdnProgCatImage.Value = prog.progGrpImage;
                hdnProgCatImageRef.Value = prog.progGrpImage;
                litImage.Text = ConfigurationManager.AppSettings["fullBase"] + strImagePath;
            }
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
        }
    }
    #endregion
}
