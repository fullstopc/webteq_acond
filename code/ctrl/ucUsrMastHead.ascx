﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrMastHead.ascx.cs"
    Inherits="ctrl_ucUsrMastHead" %>

<script type="text/javascript">
    $('.divSlideShowMasthead').cycle({
        fx: 'fade',
        speed: 700,
        timeout: 3000,
        //next: '#<% =pnlMastheadSlideShow.ClientID %>',
        pause: 1,
        pager: '.divSSPaging', pagerAnchorBuilder: pagerFactory
    });
    function pagerFactory(idx, slide) {
        var s = idx > 2 ? ' style="display:none"' : '';
        return '<a href="#"><img src="../img/cmn/trans.gif" alt="" width="15" height="15" border="0" /></a>';
    };
 </script>

<asp:Panel ID="pnlHome" runat="server" CssClass="divMastHead">
    <asp:Panel ID="pnlMastheadSlideShow" runat="server" CssClass="divSlideShowMasthead">
        <asp:Repeater ID="rptMasthead" runat="server" OnItemDataBound="rptMasthead_ItemDataBound">
            <ItemTemplate>
              <asp:Panel ID="pnlMastheadimgtag" runat="server" CssClass="divMastheadimgtag">
                <asp:Panel ID="pnlimgMasthead" runat="server" CssClass="divimgMasthead">
                    <asp:Image ID="imgMasthead" runat="server" BorderWidth="0" />
                    <asp:HyperLink ID="hypMasthead" runat="server" Visible="false"><asp:Image ID="imgMastheadImage" runat="server" BorderWidth="0" /></asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="pnltagline" runat="server" CssClass="divtagline">
                    <asp:Panel ID="pnlLittag1" runat="server" CssClass="divLittag1">
                        <asp:Literal ID="litMastTagline1" runat="server"></asp:Literal>
                    </asp:Panel>
                    <asp:Panel ID="pnlLittag2" runat="server" CssClass="divLittag2">
                        <asp:Literal ID="litMastTagline2" runat="server"></asp:Literal>
                    </asp:Panel> 
                </asp:Panel> 
              </asp:Panel>  
            </ItemTemplate>
        </asp:Repeater>
    </asp:Panel>
    <asp:Panel ID="pnlMastheadSlideShowPaging" runat="server" CssClass="divSSPaging">
    </asp:Panel>
</asp:Panel>


<asp:Repeater ID="rptThumb" runat="server" OnItemDataBound="rptThumb_ItemDataBound">
    <HeaderTemplate><style type="text/css"></HeaderTemplate>
    <ItemTemplate>
        .imgPage<asp:Literal ID='litGrpId' runat='server'></asp:Literal><%# Container.ItemIndex %> {background:url(<asp:Literal ID='litThumbUrl' runat='server'></asp:Literal>) no-repeat; <asp:Literal ID='litThumbDimension' runat='server'></asp:Literal> margin-right:5px;}
    </ItemTemplate>
    <FooterTemplate></style></FooterTemplate>
</asp:Repeater>