﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Collections;
using System.IO;

public partial class ctrl_ucUsrEvent : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _sectId = 1;
    protected int _highId = 0;
    protected int _currentPage = 0;
    protected int _pageCount = 0;
    protected int _pageNo = 0;
    protected int _filter = 0;
    protected string _sort = "HIGH_ORDER ASC";

    protected string strSortConst = "HIGH_SHOWTOP DESC, HIGH_CREATION DESC, HIGH_ORDER ASC";
    protected int intEventPageId = 0;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int sectId
    {
        get
        {
            if (ViewState["SECTID"] == null)
            {
                return _sectId;
            }
            else
            {
                return Convert.ToInt16(ViewState["SECTID"]);
            }
        }
        set { ViewState["SECTID"] = value; }
    }

    public int highId
    {
        get
        {
            if (ViewState["HIGHID"] != "" && ViewState["HIGHID"] != null)
            {
                return Convert.ToInt16(ViewState["HIGHID"]);
            }
            else
            {
                return _highId;
            }
        }
        set { ViewState["HIGHID"] = value; }
    }

    public int currentPage
    {
        get
        {
            if (ViewState["CURRENTPAGE"] == null)
            {
                return _currentPage;
            }
            else
            {
                return Convert.ToInt16(ViewState["CURRENTPAGE"]);
            }
        }
        set
        {
            ViewState["CURRENTPAGE"] = value;
        }
    }

    public int pageCount
    {
        get
        {
            if (ViewState["PAGECOUNT"] == null)
            {
                return _pageCount;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGECOUNT"]);
            }
        }
        set { ViewState["PAGECOUNT"] = value; }
    }

    public int pageNo
    {
        get
        {
            if (ViewState["PAGENO"] == null)
            {
                return _pageNo;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGENO"]);
            }
        }
        set { ViewState["PAGENO"] = value; }
    }

    public int filter
    {
        get
        {
            if (ViewState["FILTER"] == null)
            {
                return _filter;
            }
            else
            {
                return Convert.ToInt16(ViewState["FILTER"]);
            }
        }
        set { ViewState["FILTER"] = value; }
    }

    public string sort
    {
        get { return ViewState["SORT"] as string ?? _sort; }
        set { ViewState["SORT"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "event.css' type='text/css' rel='Stylesheet' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            intEventPageId = clsPage.getPageIdByTemplate("gallery.aspx", "", 1);

            clsPage page = new clsPage();
            page.extractPageById(intEventPageId, 1);
            litEventHdr.Text = page.displayName;

            setPageProperties();
            bindRptData();
        }
    }

    protected void ddlSort_SelectedIndexChanged(object sender, EventArgs e)
    {
        sort = ddlSort.SelectedValue;
        bindRptData();
    }

    protected void ddlFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        filter = Convert.ToInt16(ddlFilter.SelectedValue);
        bindRptData();
    }

    protected void lnkbtnFirst_Click(object sender, EventArgs e)
    {
        string strQuery = clsMis.getNewQueryString("pg", "1");
        Response.Redirect(currentPageName + strQuery);
    }

    protected void lnkbtnLast_Click(object sender, EventArgs e)
    {
        currentPage = pageCount;

        string strQuery = clsMis.getNewQueryString("pg", currentPage.ToString());
        Response.Redirect(currentPageName + strQuery);
    }

    protected void rptPagination_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton lnkbtnPageNo = (LinkButton)e.Item.FindControl("lnkbtnPageNo");

            Boolean boolFound = false;
            string strQuery = Request.Url.Query;

            if (!string.IsNullOrEmpty(strQuery))
            {
                string[] strQuerySplit = strQuery.Split((char)'&');
                strQuery = "";

                for (int i = 0; i <= strQuerySplit.Length - 1; i++)
                {
                    if (strQuerySplit[i].IndexOf("pg=") >= 0)
                    {
                        boolFound = true;

                        if (i == 0) { strQuerySplit[i] = "?pg=" + Convert.ToInt16(e.Item.DataItem); }
                        else { strQuerySplit[i] = "pg=" + Convert.ToInt16(e.Item.DataItem); }
                    }

                    if (i == (strQuerySplit.Length - 1))
                    {
                        strQuery += strQuerySplit[i];
                    }
                    else
                    {
                        strQuery += strQuerySplit[i] + "&";
                    }
                }
            }
            else
            {
                boolFound = true;
                strQuery = "?pg=" + Convert.ToInt16(e.Item.DataItem);
            }

            if (!boolFound)
            {
                strQuery += "&pg=" + Convert.ToInt16(e.Item.DataItem);
            }

            lnkbtnPageNo.OnClientClick = "javascript:document.location.href='" + currentPageName + strQuery + "'; return false;";

            if (Convert.ToInt16(e.Item.DataItem) == currentPage + 1)
            {
                lnkbtnPageNo.CssClass += "Sel";
                lnkbtnPageNo.OnClientClick = "javascript:return false;";
            }
        }
    }

    protected void rptPagination_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdPage")
        {
            currentPage = Convert.ToInt16(e.CommandArgument) - 1;
        }
    }

    protected void rptEvent_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strEventImgName = DataBinder.Eval(e.Item.DataItem, "HIGH_IMAGE").ToString();
            string strEventTitle = DataBinder.Eval(e.Item.DataItem, "HIGH_TITLE").ToString();
            DateTime dtEventStartDate = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "HIGH_STARTDATE"));
            DateTime dtEventEndDate = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "HIGH_ENDDATE"));
            string strEventDesc = DataBinder.Eval(e.Item.DataItem, "HIGH_SHORTDESC").ToString();
            int intHighId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "HIGH_ID").ToString());
            //int intHighGallCount = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "V_NOG"));
            //int intHighVideoCount = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "V_NOV"));
            //int intHighCommentCount = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "V_NOC"));
            //string strImgPath = ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMPRODFOLDER + "/" + strEventImg; ;
            string strEventImg = "";

            if (string.IsNullOrEmpty(strEventImgName))
            {
                clsConfig config = new clsConfig();
                strEventImg = ConfigurationManager.AppSettings["scriptBase"] + config.defaultGroupImg;
            }
            else
            {
                strEventImg = ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMHIGHFOLDER + "/" + strEventImgName;
            }

            HyperLink hypEvent = (HyperLink)e.Item.FindControl("hypEvent");
            hypEvent.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/gallery.aspx?pgid=" + intEventPageId + "&eid=" + intHighId;
            hypEvent.Text = "read more >>";
            hypEvent.ToolTip = "read more >>";

            HyperLink hypEventImg = (HyperLink)e.Item.FindControl("hypEventImg");
            hypEventImg.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/gallery.aspx?pgid=" + intEventPageId + "&eid=" + intHighId;
            hypEventImg.ToolTip = "read more >>";

            

            Image imgEvent = (Image)e.Item.FindControl("imgEvent");
            //imgEvent.ImageUrl = strEventImg;

            try
            {
                if (!string.IsNullOrEmpty(strEventImg))
                {
                    Dictionary<string, decimal> imageRatio = new Dictionary<string, decimal>()
                    {
                        { "x",4 }, {"y",3 }
                    };
                    string strImagePath = Server.MapPath(strEventImg);
                    System.Drawing.Image objImage = System.Drawing.Image.FromFile(strImagePath);

                    bool containerLandscape = (imageRatio != null) ? (imageRatio["x"] > imageRatio["y"]) : true;
                    bool imageLandscape = objImage.Width > objImage.Height;

                    int scale = (int)(objImage.Width / imageRatio["x"]);
                    int containerHeight = (int)(scale * imageRatio["y"]);


                    if (!containerLandscape && imageLandscape ||
                        (containerLandscape && imageLandscape && containerHeight > objImage.Height) || // follow width
                        (!containerLandscape && !imageLandscape && containerHeight > objImage.Height))
                    {
                        imgEvent.CssClass += " potrait";
                    }

                    // create thumbmail
                    string strThumbImage = "";
                    if (!string.IsNullOrEmpty(strEventImgName))
                    {
                        string filePath = Server.MapPath("~/data/" + clsSetting.CONSTADMHIGHFOLDER + "/" + clsSetting.CONSTADMHIGHGALLERYFOLDER + "/thumb/");


                        if (!System.IO.File.Exists(filePath + "thumb_" + strEventImgName))
                        {
                            System.Drawing.Bitmap bitThumb = GenerateThumbnails(new System.Drawing.Bitmap(Server.MapPath(strEventImg)), (int)objImage.Width, (int)objImage.Height);
                            bitThumb.Save(filePath + "thumb_" + strEventImgName);
                        }
                        strThumbImage = ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMHIGHFOLDER + "/" + clsSetting.CONSTADMHIGHGALLERYFOLDER + "/thumb/thumb_" + strEventImgName;
                    }
                    else
                    {
                        clsConfig config = new clsConfig();
                        strThumbImage = ConfigurationManager.AppSettings["scriptBase"] + config.defaultGroupImg;
                    }
                    //end create thumbnail

                    imgEvent.AlternateText = strEventTitle;
                    imgEvent.ToolTip = strEventTitle;
                    imgEvent.Attributes["data-src"] = strThumbImage;
                    imgEvent.CssClass += " lazyload";

                    objImage.Dispose();
                    objImage = null;
                }
            }
            catch (Exception ex)
            {
            }





            //Image imgGallery = (Image)e.Item.FindControl("imgGallery");
            //imgGallery.ImageUrl = ConfigurationManager.AppSettings["imagebase"] + "usr/icon-album.png";

            //Image imgVideo = (Image)e.Item.FindControl("imgVideo");
            //imgVideo.ImageUrl = ConfigurationManager.AppSettings["imagebase"] + "usr/icon-video.png";

            //Image imgComment = (Image)e.Item.FindControl("imgComment");
            //imgComment.ImageUrl = ConfigurationManager.AppSettings["imagebase"] + "usr/icon-comment.png";

            //Literal litEvtGallCount = (Literal)e.Item.FindControl("litEvtGallCount");
            //litEvtGallCount.Text = "(" + intHighGallCount.ToString() + ")";

            //Literal litEvtVideoCount = (Literal)e.Item.FindControl("litEvtVideoCount");
            //litEvtVideoCount.Text = "(" + intHighVideoCount.ToString() + ")";

            //Literal litEvtCommentCount = (Literal)e.Item.FindControl("litEvtCommentCount");
            //litEvtCommentCount.Text = "(" + intHighCommentCount.ToString() + ")";
            Panel pnlEventStartDate = (Panel)e.Item.FindControl("pnlEventStartDate");

            HyperLink hypEventName = (HyperLink)e.Item.FindControl("hypEventName");
            hypEventName.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/gallery.aspx?pgid=" + intEventPageId + "&eid=" + intHighId;
            hypEventName.ToolTip = strEventTitle;
            hypEventName.Text = strEventTitle;

            Literal litEventStartDate = (Literal)e.Item.FindControl("litEventStartDate");
            if ((dtEventStartDate.ToShortDateString() == dtEventEndDate.ToShortDateString()) && (dtEventEndDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString()))
            {
                litEventStartDate.Text = dtEventStartDate.ToString("dd MMM yyyy");
                pnlEventStartDate.Visible = true;
            }
            else if ((dtEventStartDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString()) && (dtEventEndDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString()))
            {
                litEventStartDate.Text = dtEventStartDate.ToString("dd MMM yyyy") + " to " + dtEventEndDate.ToString("dd MMM yyyy");
                pnlEventStartDate.Visible = true;
            }
            else
            { litEventStartDate.Text = ""; }

            //Literal litEventDesc = (Literal)e.Item.FindControl("litEventDesc");
            //litEventDesc.Text = strEventDesc;


            //clsHighlight high = new clsHighlight();

            //if (Application["HIGHLIGHTGALLERY"] == null)
            //{
            //    Application["HIGHLIGHTGALLERY"] = high.getHighlightGallery();
            //}

            //DataSet dsPhoto = (DataSet)Application["HIGHLIGHTGALLERY"];
            //DataView dvPhoto = new DataView(dsPhoto.Tables[0]);

            //if (intHighId > 0)
            //{
            //    dvPhoto.RowFilter = "HIGH_ID = " + intHighId;
            //}

            //dvPhoto.Sort = "HIGHGALL_TITLE ASC";

            //if (dvPhoto.Count > 0)
            //{
            //    Panel pnlEventGallery = (Panel)e.Item.FindControl("pnlEventGallery");
            //    pnlEventGallery.Visible = true;

            //    Repeater rptEventGallery = (Repeater)e.Item.FindControl("rptEventGallery");

            //    rptEventGallery.DataSource = dvPhoto;
            //    rptEventGallery.DataBind();
            //}
        }


    }

    //protected void rptEventGallery_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //    {
    //        string strHighTitle = DataBinder.Eval(e.Item.DataItem, "HIGHGALL_TITLE").ToString();
    //        string strHighId = DataBinder.Eval(e.Item.DataItem, "HIGH_ID").ToString();
    //        string strHighImage = DataBinder.Eval(e.Item.DataItem, "HIGHGALL_IMAGE").ToString();

    //        strHighImage = ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMHIGHFOLDER + "/" + clsSetting.CONSTADMHIGHGALLERYFOLDER + "/" + strHighImage;

    //        HyperLink hypEventGalleryImg = (HyperLink)e.Item.FindControl("hypEventGalleryImg");
    //        hypEventGalleryImg.NavigateUrl = strHighImage;
    //        hypEventGalleryImg.ToolTip = strHighTitle;
    //        hypEventGalleryImg.Attributes["rel"] = "event" + strHighId;

    //        Image imgEventGalleryImg = (Image)e.Item.FindControl("imgEventGalleryImg");
    //        imgEventGalleryImg.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMHIGHFOLDER + "/" + clsSetting.CONSTADMHIGHGALLERYFOLDER + "/" + DataBinder.Eval(e.Item.DataItem, "HIGHGALL_IMAGE") + "&w=" + clsSetting.CONSTUSRPHOTOGALLERYIMGWIDTHINDE + "&h=" + clsSetting.CONSTUSRPHOTOGALLERYIMGHEIGHTINDE;
    //        imgEventGalleryImg.AlternateText = strHighTitle;
    //        imgEventGalleryImg.ToolTip = strHighTitle;

    //        intEventImgCount += 1;
    //        if (intEventImgCount % intEventImgNoInRow == 0)
    //        {
    //            //Panel pnlEventGalleryItems = (Panel)e.Item.FindControl("pnlEventGalleryItems");
    //            //pnlEventGalleryItems.CssClass = "divEventGalleryItemsLast";
    //        }
    //    }
    //}
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        litShow.Text = "Show: ";
        litSort.Text = "Sort By: ";
        DataSet dsYear = new DataSet();
        DataView dvYear, dvYearCheck;
        Boolean boolStartDateNull = false;

        clsHighlight high = new clsHighlight();
        dsYear = high.getEventYear(1);
        dvYear = new DataView(dsYear.Tables[0]);

        dvYearCheck = new DataView(dsYear.Tables[0]);
        dvYearCheck.RowFilter = "HIGH_STARTDATE = '9999-12-31 11:59:59' OR HIGH_STARTDATE = null";

        if (dvYearCheck.Count > 0)
        {
            boolStartDateNull = true;
        }

        dvYear.RowFilter = "HIGH_STARTDATE <> '9999-12-31 11:59:59' OR HIGH_STARTDATE <> null";
        dvYear.Sort = "YEAR(HIGH_STARTDATE) DESC";

        ddlFilter.DataSource = dvYear;
        ddlFilter.DataTextField = "YEAR(HIGH_STARTDATE)";
        ddlFilter.DataValueField = "YEAR(HIGH_STARTDATE)";
        ddlFilter.DataBind();

        ListItem ddlYearDefaultItem = new ListItem("All", "0");
        ddlFilter.Items.Insert(0, ddlYearDefaultItem);

        if (boolStartDateNull && dvYear.Count > 0)
        {
            ddlFilter.Items.Add(new ListItem("Others", "9"));
        }

        ddlSort.Items.Add(new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), sort));
        ddlSort.Items.Add(new ListItem("Latest on Top", "HIGH_STARTDATE DESC, HIGH_SHOWTOP DESC, HIGH_CREATION DESC, HIGH_ORDER ASC"));
        ddlSort.Items.Add(new ListItem("Oldest on Top", "HIGH_STARTDATE ASC, HIGH_SHOWTOP DESC, HIGH_CREATION DESC, HIGH_ORDER ASC"));
        ddlSort.Items.Add(new ListItem("Title (A - Z)", "HIGH_TITLE ASC, " + strSortConst));
        ddlSort.Items.Add(new ListItem("Title (Z - A)", "HIGH_TITLE DESC, " + strSortConst));
    }

    protected void bindRptData()
    {
        int intPageSize = 0;

        clsHighlight high = new clsHighlight();
        DataSet ds = high.getHighlightList(1);
        DataView dv = new DataView(ds.Tables[0]);

        dv.RowFilter = "HIGH_VISIBLE = 1 AND HIGH_ACTIVE = 1";

        if (filter > 0)
        {
            ddlFilter.Items.FindByValue(filter.ToString()).Selected = true;

            if (filter == 9)
            {
                dv.RowFilter += " AND (HIGH_STARTDATE = '9999-12-31 11:59:59' OR HIGH_STARTDATE = null)";
            }
            else
            {
                dv.RowFilter += " AND (HIGH_STARTDATE >= #" + filter + "/01/01# AND HIGH_STARTDATE <= #" + filter + "/12/31#)";
            }
        }

        dv.Sort = sort;

        intPageSize = clsSetting.CONSTUSREVENTGALLERYSIZE;
        PagedDataSource pds = new PagedDataSource();
        pds.DataSource = dv;
        pds.AllowPaging = true;
        pds.PageSize = intPageSize;
        pageCount = pds.PageCount;

        if (pageNo != 0) { currentPage = pageNo - 1; }

        if (currentPage < 0) { currentPage = 0; }
        if (currentPage > pageCount - 1) { currentPage = 0; }

        pds.CurrentPageIndex = currentPage;

        rptEvent.DataSource = pds;
        rptEvent.DataBind();

        if (dv.Count > 0)
        {
            pnlDdlFilter.Visible = true;
            pnlDdlSort.Visible = true;

        }
        else
        {
            pnlDdlFilter.Visible = false;
            pnlDdlSort.Visible = false;
        }

        // force to invisible
        pnlDdlFilter.Visible = false;
        pnlDdlSort.Visible = false;

        imgbtnFirstTop.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
        imgbtnLastTop.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
        imgbtnFirstBottom.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
        imgbtnLastBottom.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";

        if (pageCount > 1)
        {
            pnlListPaginationTop.Visible = true;
            pnlListPaginationBottom.Visible = true;

            if (dv.Count > 0)
            {
                int intCurrentPage = currentPage + 1;
                int intStart;
                int intEnd;
                int intPaginationSize = 6;
                int intPaginationSizeMid = Convert.ToInt16(intPaginationSize / 2);

                if (intCurrentPage <= intPaginationSizeMid)
                {
                    intStart = 1;
                    if (pageCount > intPaginationSize) { intEnd = intPaginationSize; }
                    else { intEnd = pageCount; }
                }
                else if (intCurrentPage >= pageCount - intPaginationSizeMid)
                {
                    intStart = pageCount - intPaginationSize + 1;
                    if (intStart < 1) { intStart = 1; }
                    intEnd = pageCount;
                }
                else
                {
                    intStart = intCurrentPage - intPaginationSizeMid;
                    intEnd = intCurrentPage + intPaginationSizeMid;
                }

                ArrayList pages = new ArrayList();
                for (int i = intStart; i <= intEnd; i++)
                {
                    pages.Add(i);
                }

                if (currentPage == 0)
                {
                    imgbtnFirstTop.Enabled = false;
                    imgbtnFirstBottom.Enabled = false;
                    imgbtnFirstTop.CssClass = "imgbtnPageFirstTopDisabled";
                    imgbtnFirstBottom.CssClass = "imgbtnPageFirstBottomDisabled";

                    if (pageCount > 1)
                    {
                        imgbtnLastTop.Enabled = true;
                        imgbtnLastBottom.Enabled = true;
                        imgbtnLastTop.CssClass = "imgbtnPageLastTop";
                        imgbtnLastBottom.CssClass = "imgbtnPageLastBottom";
                    }
                    else
                    {
                        imgbtnLastTop.Enabled = false;
                        imgbtnLastBottom.Enabled = false;
                        imgbtnLastTop.CssClass = "imgbtnPageLastTopDisabled";
                        imgbtnLastBottom.CssClass = "imgbtnPageLastBottomDisabled";
                    }
                }
                else if (currentPage == pageCount - 1)
                {
                    imgbtnLastTop.Enabled = false;
                    imgbtnLastBottom.Enabled = false;
                    imgbtnLastTop.CssClass = "imgbtnPageLastTopDisabled";
                    imgbtnLastBottom.CssClass = "imgbtnPageLastBottomDisabled";

                    if (pageCount != 1)
                    {
                        imgbtnFirstTop.Enabled = true;
                        imgbtnFirstBottom.Enabled = true;
                        imgbtnFirstTop.CssClass = "imgbtnPageFirstTop";
                        imgbtnFirstBottom.CssClass = "imgbtnPageFirstBottom";
                    }
                }
                else
                {
                    imgbtnFirstTop.Enabled = true;
                    imgbtnFirstBottom.Enabled = true;
                    imgbtnFirstTop.CssClass = "imgbtnPageFirstTop";
                    imgbtnFirstBottom.CssClass = "imgbtnPageFirstBottom";

                    imgbtnLastTop.Enabled = true;
                    imgbtnLastBottom.Enabled = true;
                    imgbtnLastTop.CssClass = "imgbtnPageLastTop";
                    imgbtnLastBottom.CssClass = "imgbtnPageLastBottom";
                }

                rptPaginationTop.DataSource = pages;
                rptPaginationTop.DataBind();

                rptPaginationBottom.DataSource = pages;
                rptPaginationBottom.DataBind();

            }
        }
        else
        {
            pnlListPaginationTop.Visible = false;
            pnlListPaginationBottom.Visible = false;
        }
    }

    private System.Drawing.Bitmap GenerateThumbnails(System.Drawing.Bitmap srcImage, int width, int height)
    {

        int newWidth = width;
        int newHeight = height;
        System.Drawing.Bitmap newImage = new System.Drawing.Bitmap(newWidth, newHeight);

        using (System.Drawing.Graphics gr = System.Drawing.Graphics.FromImage(newImage))
        {
            gr.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            gr.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            gr.DrawImage(srcImage, new System.Drawing.Rectangle(0, 0, newWidth, newHeight));
        }
        return newImage;
    }
    #endregion
}
