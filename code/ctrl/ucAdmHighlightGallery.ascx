﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmHighlightGallery.ascx.cs" Inherits="ctrl_ucAdmHighlightGallery" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>
<link href="<%= System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>cropper.min.css" rel="stylesheet">
<script src="<%= System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>cropper.min.js"></script>


<link href="<%= System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>fileupload/dropzone.css" rel="stylesheet">
<link href="<%= System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>fileupload/dropzone.custom.css" rel="stylesheet">

<script src="<%= System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jquery.fancybox.js"></script>
<link href="<%= System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>fancybox/jquery.fancybox.css" rel="stylesheet">
<script type="text/javascript">

    function cropActive($this) {

    }

    function cropInActive() {


    }

    function cropDone() {

    }
    function cropReset($this) {

    }
    $(function () {




    });
</script>
<asp:Panel ID="pnlForm" runat="server" CssClass="">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <asp:Panel ID="pnlGalAck" runat="server" Visible="false" CssClass="divAck">
            <asp:Literal ID="litGalAck" runat="server"></asp:Literal>
        </asp:Panel>
        <%--        <div id="dZUpload" class="dropzone">
            <div class="dz-default dz-message"></div>
        </div>--%>
        <div id="upload">
            <div id="upload-area">
                <table id="tblFileUpload" cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
                    <tbody>
                        <tr>
                            <td class="tdLabel">
                                <asp:Label ID="lblTitle" runat="server">Image Title</asp:Label><span class="attention_compulsory">*</span>:
                            </td>
                            <td>
                                <div id="actions" class="row">
                                    <!-- The fileinput-button span is used to style the file input field as button -->
                                    <a class="btn3 fileinput-button" href="#"><i class="material-icons">note_add</i><span>Add files</span></a>
                                    <a class="btn3 btn-primary start"><i class="material-icons">file_upload</i><span>Upload All</span></a>
                                    <a class="btn3 btn-warning cancel"><i class="material-icons">not_interested</i><span>Cancel All</span></a>
                                </div>

                                <div id="previews" class="dropzone">

                                    <div class="dz-preview dz-file-preview" id="template">
                                        <div class="dz-image">
                                            <img data-dz-thumbnail />
                                            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                                            <div class="dz-success-mark">
                                                <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                    <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                                                    <title>Check</title>
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                                                        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="dz-error-mark">
                                                <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                    <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                                                    <title>error</title>
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                                                        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
                                                            <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="dz-details">
                                            <div class="dz-filename"><span data-dz-name></span></div>
                                            <div class="dz-size" data-dz-size></div>

                                        </div>
                                        <div class="dz-input">
                                            <div>Description of the picture :</div>
                                            <input name="txtTitle" id="txtTitle" type="text" class="text" />
                                        </div>

                                        <div class="dz-action">
                                            <a class="btn3 crop" href="#divCropImgContainer"><i class="material-icons">crop</i><span>Crop</span></a>
                                            <a class="btn3 start"><i class="material-icons">file_upload</i><span>Start</span></a>
                                            <a class="btn3 cancel" data-dz-remove><i class="material-icons">not_interested</i><span>Cancel</span></a>
                                        </div>

                                        
                                        <div class="dz-error-message"><span data-dz-errormessage></span></div>

                                    </div>

                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </asp:Panel>
    <table cellpadding="0" cellspacing="0" border="0" id="pnlHighlightGallery" runat="server" class="formTbl tblData">
        <tr>
            <td class="tdSectionHdr">Uploaded Image(s):
            </td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="litHighImageFound" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td>
                <div id="divUploadedImageContainer">
                </div>
            </td>
        </tr>
        <tfoot>
            <tr>
                <td>
                    <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                        <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack"></asp:LinkButton>
                    </asp:Panel>
                </td>
            </tr>
        </tfoot>
    </table>
</asp:Panel>

<script src="<%= System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>fileupload/dropzone.js"></script>
<script src="<%= System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>fileupload/init.js"></script>
<script type="text/javascript">
    //var galleryPath = "<%= strShowImage %>" + "<%= strGalleryPath.Replace(@"\", @"\\") %>";
    //var tempPath = "<%= strShowImage %>" + "<%= strTempPath.Replace(@"\", @"\\") %>";

    var galleryPath = "<%= Session[clsAdmin.CONSTPROJECTUSERVIEWURL] %><%= ConfigurationManager.AppSettings["uplBase2"] %>" + "<%= clsAdmin.CONSTADMHIGHFOLDER%>/<%= clsAdmin.CONSTADMHIGHGALLERYFOLDER%>/";
    var tempPath = "<%= Session[clsAdmin.CONSTPROJECTUSERVIEWURL] %><%= ConfigurationManager.AppSettings["uplBase2"] %>" + "<%= clsAdmin.CONSTADMTEMPFOLDER%>/";


    function populateImage() {
        
        var eventGalleryJson = jQuery.parseJSON('<%= high.getEventGalleryList() %>');
        //console.log(eventGalleryJson);
        
        
        for (var i = 0; i < eventGalleryJson.length; i++) {
            //var eventGalleryJson = JSON.stringify(eval("({" + eventGalleryList[i] + "})"));
            var eventGallery = eventGalleryJson[i];

            addImage(eventGalleryJson[i], galleryPath);
        }
    }

    function addImage(eventGalleryJson,filePath) {
        var eventGalleryPath = filePath || tempPath;
        var eventOrder = eventGalleryJson.order == 9999 || eventGalleryJson.order == 0 ? "" : eventGalleryJson.order;
        var template = "";
        template += "<div class='divIndImage'>";
        template += "<table>"
        template += "<tr>"
        template += "<td><a href='" + (eventGalleryPath + eventGalleryJson.filename) + "' class='fancybox img-link' rel='gallery'><img src='" + (eventGalleryPath + eventGalleryJson.filename) + "' /></a></td>"
        template += "</tr>"
        template += "<tr>"
        template += "<td class='tdIndImageDesc'>";
        template += "<span class='spanIndImageDesc'>" + decodeURIComponent(eventGalleryJson.title) + "</span>";
        template += "<input class='txtIndImageDesc' data-id=" + encodeURI(eventGalleryJson.filename) + " value='" + decodeURIComponent(eventGalleryJson.title) + "'/>";
        template += "</td>"
        template += "</tr>"
        template += "<tr>"
        template += "<td class='tdHighImageNumber'><span># </span><input type='number' min=1 class='text_small txtOrder' value='" + eventOrder + "' data-id=" + encodeURI(eventGalleryJson.filename) + " name='txtOrder'/></td>"
        template += "</tr>"
        template += "<tr>"
        template += "<td><a href='#' class='btnRemove' data-id='" + eventGalleryJson.filename + "'>Remove</a></td>"
        template += "</tr>"
        template += "</table>";
        template += "<input type='hidden' name='hdnImgSrc' id=''/>";
        template += "</div>";
        $("#divUploadedImageContainer").append($(template));
        $(".fancybox").fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            type:'image',
        });

        var img = new Image();
        img.src = (eventGalleryPath + eventGalleryJson.filename) + "&f=1";
        img.onload = function () {
            //console.log(this.width + 'x' + this.height);
        }
    }

    function removeImage(id,element) {
        $.ajax({
            type: "POST",
            url: wsBase + "wsFileUpload.asmx/deleteEventGallery",
            data: '{"strId":"'+id+'" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                console.log(data);
                $(element).addClass("remove");
            },
            failure: function(response) {
                console.log(data);
            }
        });
    }
    function updateImage(id, order) {
        order = order || 9999;
        $.ajax({
            type: "POST",
            url: wsBase + "wsFileUpload.asmx/updateOrderEventGallery",
            data: '{"strId":"' + id + '","intOrder":'+order+' }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                console.log(data);
            },
            failure: function (response) {
                console.log(data);
            }
        });
    }

    function updateDesc(id, desc) {
        desc = desc || '';
        $.ajax({
            type: "POST",
            url: wsBase + "wsFileUpload.asmx/updateDescEventGallery",
            data: '{"strId":"' + id + '","strDesc":"' + desc + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                console.log(data);
            },
            failure: function (response) {
                console.log(data);
            }
        });
    }
    $(function () {
   
        populateImage();

        $(".btnRemove").live('click', function (e) {
            e.preventDefault();
            removeImage($(this).data('id'), $(this).parentsUntil(".divIndImage").parent());
        });

        $(".txtOrder").live('focusout', function (e) {
            updateImage(decodeURIComponent($(this).data('id')), $(this).val())
        })

        $(".tdIndImageDesc").live('dblclick', function (e) {
            $(this).addClass("edit");
            $(this).find(".txtIndImageDesc").focus();
        })

        $(".txtIndImageDesc").live('focusout', function (e) {
            var $this = $(this);
            var $parent = $this.parent()
            var $label = $parent.find(".spanIndImageDesc");

            var desc = $this.val();

            $label.text(desc);
            $this.val(desc);
            $parent.removeClass("edit");
            updateDesc(decodeURIComponent($(this).data('id')), desc);
        })

    });

    function updateMaxSize(maxSize, aspectRatioW, aspectRatioH) {
        callDropzone(maxSize, aspectRatioW, aspectRatioH);
    }
    
</script>
