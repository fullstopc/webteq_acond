﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmUserManual.ascx.cs"
    Inherits="ctrl_ucAdmUserManual" %>
<asp:Panel ID="pnlForm" runat="server">
    <asp:Panel ID="pnlFormUserManual" runat="server" Visible="false">
        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
            <asp:Literal ID="litAck" runat="server"></asp:Literal>
        </asp:Panel>
        <table id="tblUserManual" cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
            <tr>
                <td class="tdSpacer">
                    &nbsp;
                </td>
                <td class="tdSpacer">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="tdLabel nobr">
                    User Manual:
                </td>
                <td class="tdMax">
                    <asp:TextBox ID="txtUserManual" runat="server" class="text_big" TextMode="MultiLine" Rows="5"></asp:TextBox>
                    <asp:HiddenField ID="hdnUserManual" runat="server" />
                </td>
            </tr>
            <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave"
                                    OnClick="lnkbtnSave_Click"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
        
    </asp:Panel>
    <asp:Panel ID="pnlFormUserManualDisplay" runat="server">
        <asp:Literal ID="litUserManual" runat="server"></asp:Literal>
    </asp:Panel>
</asp:Panel>
