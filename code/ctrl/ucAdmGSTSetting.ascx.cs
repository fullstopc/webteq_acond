﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Configuration;
using System.Text.RegularExpressions;

public partial class ctrl_ucAdmGSTSetting : System.Web.UI.UserControl
{

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion


    #region "Page Events"
    protected void ddlGstActivation_OnSelectedChanged(object sender, EventArgs e)
    {
        populateView();
    }


    protected void validateGstIcon_server(object source, ServerValidateEventArgs args)
    {
        if (fileGstIcon.HasFile)
        {
            int intImgMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            //if (fileGstIcon.PostedFile.ContentLength > clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH)
            //{
            //    cvGstIcon.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH, "kb") + ".";
            //    args.IsValid = false;
            //}

            if (fileGstIcon.PostedFile.ContentLength > intImgMaxSize)
            {
                cvGstIcon.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + ".";
                args.IsValid = false;
            }
            else
            {
                Match matchImage = Regex.Match(fileGstIcon.FileName, clsAdmin.CONSTADMIMGALLOWEDEXT, RegexOptions.IgnoreCase);
                if (matchImage.Success)
                {
                    args.IsValid = true;
                }
                else
                {
                    cvGstIcon.ErrorMessage = "<br />Please enter valid image. (" + clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + ").";
                    args.IsValid = false;
                }
            }
        }
    }


    protected void cvGstIcon_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("GstIcon")))
        {
            string strJS = null;
            strJS = "<script type = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + fileGstIcon.ClientID + "', document.all['" + cvGstIcon.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "GstIcon", strJS, false);
        }
    }


    protected void lnkbtnDeleteGstIcon_Click(object sender, EventArgs e)
    {
        pnlGstIcon.Visible = false;
        hdnGstIcon.Value = "";
    }


    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            clsConfig config = new clsConfig();
            int intRecordAffected = 0;

            Boolean boolSuccess = true;
            Boolean boolEdited = false;

            string strNameGstActivationValue = ddlGstActivation.SelectedValue;
            string strNameGstInclusiveIconValue = "";
            string strNameGstPercentValue = "";
            string strNameGstProductValue = "";
            string strNameShowItemizedValue = "";


            if (hdnGstIcon.Value != "") strNameGstInclusiveIconValue = hdnGstIcon.Value;
            else strNameGstInclusiveIconValue = "";
            int intSel = int.Parse(strNameGstActivationValue);
            switch (intSel)
            {
                case 2:
                    if (fileGstIcon.HasFile)
                    {
                        try
                        {
                            HttpPostedFile postedFile = fileGstIcon.PostedFile;
                            string fileName = Path.GetFileName(postedFile.FileName);
                            string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCTRLPNLADMFOLDER + "/";
                            clsMis.createFolder(uploadPath);

                            string newFilename = clsMis.GetUniqueFilename(fileName, uploadPath);
                            postedFile.SaveAs(uploadPath + newFilename);
                            strNameGstInclusiveIconValue = newFilename;
                        }
                        catch (Exception ex)
                        {
                            clsLog.logErroMsg(ex.Message.ToString());
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />" + ex.Message.ToString() + "</div>";
                            throw ex;
                        }
                    }

                    break;
                case 3:
                    //GST Applied
                    strNameGstPercentValue = txtGstPercent.Text == "" ? "0" : txtGstPercent.Text;
                    strNameGstProductValue = ddlGstProduct.SelectedValue;
                    strNameShowItemizedValue = chkShowItemized.Checked ? clsConfig.CONSTDEFAULTTRUEVALUE : clsConfig.CONSTDEFAULTFALSEVALUE;
                    break;
            }



            string strNameGstActivation = lblGstActivation.Text;
            if (boolSuccess && !config.isItemExist(strNameGstActivation, clsConfig.CONSTGROUPGSTSETTING))
            {
                intRecordAffected = config.addItem(strNameGstActivation, strNameGstActivationValue, clsConfig.CONSTGROUPGSTSETTING, 1, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }
            else if (!config.isExactSameSetData(strNameGstActivation, strNameGstActivationValue, clsConfig.CONSTGROUPGSTSETTING))
            {
                intRecordAffected = config.updateItem(clsConfig.CONSTGROUPGSTSETTING, strNameGstActivation, strNameGstActivationValue, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }


            string strNameGstInclusiveIcon = lblGstInclusiveIcon.Text;
            if (boolSuccess && !config.isItemExist(strNameGstInclusiveIcon, clsConfig.CONSTGROUPGSTSETTING))
            {
                intRecordAffected = config.addItem(strNameGstInclusiveIcon, strNameGstInclusiveIconValue, clsConfig.CONSTGROUPGSTSETTING, 1, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }
            else if (!config.isExactSameSetData(strNameGstInclusiveIcon, strNameGstInclusiveIconValue, clsConfig.CONSTGROUPGSTSETTING))
            {
                intRecordAffected = config.updateItem(clsConfig.CONSTGROUPGSTSETTING, strNameGstInclusiveIcon, strNameGstInclusiveIconValue, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }


            string strNameGstPercent = lblGstPercent.Text;
            if (boolSuccess && !config.isItemExist(strNameGstPercent, clsConfig.CONSTGROUPGSTSETTING))
            {
                intRecordAffected = config.addItem(strNameGstPercent, strNameGstPercentValue, clsConfig.CONSTGROUPGSTSETTING, 1, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }
            else if (!config.isExactSameSetData(strNameGstPercent, strNameGstPercentValue, clsConfig.CONSTGROUPGSTSETTING))
            {
                intRecordAffected = config.updateItem(clsConfig.CONSTGROUPGSTSETTING, strNameGstPercent, strNameGstPercentValue, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }

            string strNameGstProduct = lblGstProduct.Text;
            if (boolSuccess && !config.isItemExist(strNameGstProduct, clsConfig.CONSTGROUPGSTSETTING))
            {
                intRecordAffected = config.addItem(strNameGstProduct, strNameGstProductValue, clsConfig.CONSTGROUPGSTSETTING, 1, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }
            else if (!config.isExactSameSetData(strNameGstProduct, strNameGstProductValue, clsConfig.CONSTGROUPGSTSETTING))
            {
                intRecordAffected = config.updateItem(clsConfig.CONSTGROUPGSTSETTING, strNameGstProduct, strNameGstProductValue, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }

            string strNameShowItemized = lblShowItemized.Text;
            if (boolSuccess && !config.isItemExist(strNameShowItemized, clsConfig.CONSTGROUPGSTSETTING))
            {
                intRecordAffected = config.addItem(strNameShowItemized, strNameShowItemizedValue, clsConfig.CONSTGROUPGSTSETTING, 1, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }
            else if (!config.isExactSameSetData(strNameShowItemized, strNameShowItemizedValue, clsConfig.CONSTGROUPGSTSETTING))
            {
                intRecordAffected = config.updateItem(clsConfig.CONSTGROUPGSTSETTING, strNameShowItemized, strNameShowItemizedValue, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }

            if (!boolSuccess)
            {
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to update control panel. Please try again.</div>";

                Response.Redirect(currentPageName);
            }
            else
            {
                if (boolEdited)
                {
                    Session["EDITEDCMSCP"] = 1;
                }
                else
                {
                    Session["EDITEDCMSCP"] = 1;
                    Session["NOCHANGE"] = 1;
                }

                Response.Redirect(currentPageName);
            }
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setPageProperties();
            fillForm();
            populateView();

        }

    }


    #endregion

    #region "Methods"

    protected void setPageProperties()
    {
        lblGstActivation.Text = clsConfig.CONSTNAMEGSTACTIVATION;
        lblGstInclusiveIcon.Text = clsConfig.CONSTNAMEGSTINCLUSIVEICON;
        lblGstPercent.Text = clsConfig.CONSTNAMEGSTPERCENTAGE;
        lblGstProduct.Text = clsConfig.CONSTNAMEGSTPRODUCTWITHGST;
        lblShowItemized.Text = clsConfig.CONSTNAMEGSTSHOWITEMIZED;


        clsMis mis = new clsMis();

        DataView dvGstAct = new DataView(mis.getListByListGrp("GST TYPE", 1).Tables[0]);
        dvGstAct.Sort = "LIST_ORDER ASC";
        foreach (DataRow row in dvGstAct.Table.Rows)
        {
            if (row["LIST_VALUE"].ToString() == "1" && Session[clsAdmin.CONSTPROJECTGSTNOTAVAILABLE] != null)
            {
                if (Session[clsAdmin.CONSTPROJECTGSTNOTAVAILABLE].ToString() == "1")
                {
                    ListItem liGstOption = new ListItem(row["LIST_NAME"].ToString(), row["LIST_VALUE"].ToString());
                    ddlGstActivation.Items.Add(liGstOption);
                }
            }

            if (row["LIST_VALUE"].ToString() == "2" && Session[clsAdmin.CONSTPROJECTGSTINCLUSIVE] != null)
            {
                if (Session[clsAdmin.CONSTPROJECTGSTINCLUSIVE].ToString() == "1")
                {
                    ListItem liGstOption = new ListItem(row["LIST_NAME"].ToString(), row["LIST_VALUE"].ToString());
                    ddlGstActivation.Items.Add(liGstOption);
                }
            }

            if (row["LIST_VALUE"].ToString() == "3" && Session[clsAdmin.CONSTPROJECTGSTAPPLIED] != null)
            {
                if (Session[clsAdmin.CONSTPROJECTGSTAPPLIED].ToString() == "1")
                {
                    ListItem liGstOption = new ListItem(row["LIST_NAME"].ToString(), row["LIST_VALUE"].ToString());
                    ddlGstActivation.Items.Add(liGstOption);
                }
            }
        }

        //ddlGstActivation.DataSource = dvGstAct;
        //ddlGstActivation.DataBind();

        DataView dvGstProductWithGst = new DataView(mis.getListByListGrp("GST PRODUCT TYPE", 1).Tables[0]);
        dvGstProductWithGst.Sort = "LIST_ORDER ASC";
        ddlGstProduct.DataSource = dvGstProductWithGst;
        ddlGstProduct.DataBind();

        int intImgMaxSize = 0;

        if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
        {
            if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
            {
                intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
            }
            else
            {
                if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                {
                    if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                    {
                        intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                    }
                }
            }
        }

        //litUploadRule.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH, "kb") + " | " + clsMis.formatAllowedFileDimension(clsAdmin.CONSTADMCURIMGALLOWEDWIDTH, clsAdmin.CONSTADMCURIMGALLOWEDHEIGHT, "px");
        litUploadRule.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + " | " + clsMis.formatAllowedFileDimension(clsAdmin.CONSTADMCURIMGALLOWEDWIDTH, clsAdmin.CONSTADMCURIMGALLOWEDHEIGHT, "px");

    }


    protected void fillForm()
    {
        clsConfig config = new clsConfig();

        DataSet ds = config.getItemList(1);
        DataRow[] foundRow;

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPGSTSETTING + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEGSTACTIVATION, true) == 0) { ddlGstActivation.SelectedValue = row["CONF_VALUE"].ToString(); }

            string strGstIcon = "";
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEGSTINCLUSIVEICON, true) == 0)
            {
                strGstIcon = row["CONF_VALUE"].ToString();
                if (strGstIcon != "")
                {
                    pnlGstIcon.Visible = true;
                    imgGstIcon.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCTRLPNLADMFOLDER + "/" + strGstIcon + "&f=1";
                    hdnGstIcon.Value = strGstIcon;
                    hdnGstIconRef.Value = strGstIcon;
                }
            }

            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEGSTPERCENTAGE, true) == 0) { txtGstPercent.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEGSTPRODUCTWITHGST, true) == 0) { ddlGstProduct.SelectedValue = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEGSTSHOWITEMIZED, true) == 0) { chkShowItemized.Checked = (row["CONF_VALUE"].ToString() == clsConfig.CONSTDEFAULTTRUEVALUE ? Convert.ToBoolean(clsConfig.CONSTTRUE) : Convert.ToBoolean(clsConfig.CONSTFALSE)); }
        }


    }

    protected void populateView()
    {
        int intSel = int.Parse(ddlGstActivation.SelectedValue);

        switch (intSel)
        {
            case 1:
                mvGSTSetting.SetActiveView(mvvNotAvailable);
                break;
            case 2:
                mvGSTSetting.SetActiveView(mvvGstInclusive);
                break;
            case 3:
                mvGSTSetting.SetActiveView(mvvGstApplied);
                break;
            default:
                mvGSTSetting.SetActiveView(mvvGstInclusive);
                break;
        }
    }

    #endregion
}
