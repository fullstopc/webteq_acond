﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctlr_ucUsrWebteq : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "webteq.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            imgWebteqLogo.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "usr/webteq-logo.jpg";
            imgWebteqLogo.ToolTip = "powered by Webteq Solution";
            hypWebteq.NavigateUrl = "http://www.webteq.com.my";
            hypWebteq.Target = "blank";
        }
    }
}
