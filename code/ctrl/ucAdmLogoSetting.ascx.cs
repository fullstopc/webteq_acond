﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;
using System.Linq;

public partial class ctrl_ucAdmLogoSetting : System.Web.UI.UserControl
{
    #region "Properties"
    protected clsConfig config = new clsConfig();
    protected Boolean boolSuccess;
    protected Boolean boolEdited;
    protected int intRecordAffected;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setPageProperties();
            fill();
        }
    }
    protected void validateImage_server(object source, ServerValidateEventArgs args)
    {
        string strControlValidator = ((CustomValidator)source).ControlToValidate;
        FileUpload fileUpload = new FileUpload();
        CustomValidator customValidator = new CustomValidator();
        if (strControlValidator == fileCompanyLogo.ID)
        {
            fileUpload = fileCompanyLogo;
            customValidator = cvCompanyLogo;
        }
        else if (strControlValidator == fileNewIcon.ID)
        {
            fileUpload = fileNewIcon;
            customValidator = cvNewIcon;
        }
        else if (strControlValidator == fileRecIcon.ID)
        {
            fileUpload = fileRecIcon;
            customValidator = cvRecIcon;
        }
        else if (strControlValidator == fileFavicon.ID)
        {
            fileUpload = fileFavicon;
            customValidator = cvFavicon;
        }

        if (fileUpload.HasFile)
        {
            int intImgMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            if (fileUpload.PostedFile.ContentLength > intImgMaxSize)
            {
                customValidator.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + ".";
                args.IsValid = false;
            }
            else
            {
                Match matchImage = Regex.Match(fileUpload.FileName, clsAdmin.CONSTADMIMGALLOWEDEXT, RegexOptions.IgnoreCase);
                if (matchImage.Success)
                {
                    args.IsValid = true;
                }
                else
                {
                    customValidator.ErrorMessage = "<br />Please enter valid image. (" + clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + ").";
                    args.IsValid = false;
                }
            }
        }
    }
    protected void cvImage_PreRender(object sender, EventArgs e)
    {
        string strControlValidator = ((CustomValidator)sender).ControlToValidate;
        if (strControlValidator == fileCompanyLogo.ID)
        {
            if ((!Page.ClientScript.IsStartupScriptRegistered("CompanyLogo")))
            {
                string strJS = null;
                strJS = "<script type = \"text/javascript\">";
                strJS += "ValidatorHookupControlID('" + fileCompanyLogo.ClientID + "', document.all['" + cvCompanyLogo.ClientID + "']);";
                strJS += "</script>";

                Page.ClientScript.RegisterStartupScript(Page.GetType(), "CompanyLogo", strJS, false);
            }
        }

        else if (strControlValidator == fileRecIcon.ID)
        {
            if ((!Page.ClientScript.IsStartupScriptRegistered("RecIcon")))
            {
                string strJS = null;
                strJS = "<script type = \"text/javascript\">";
                strJS += "ValidatorHookupControlID('" + fileRecIcon.ClientID + "', document.all['" + cvRecIcon.ClientID + "']);";
                strJS += "</script>";

                Page.ClientScript.RegisterStartupScript(Page.GetType(), "CompanyLogo", strJS, false);
            }
        }

        else if (strControlValidator == fileNewIcon.ID)
        {
            if ((!Page.ClientScript.IsStartupScriptRegistered("NewIcon")))
            {
                string strJS = null;
                strJS = "<script type = \"text/javascript\">";
                strJS += "ValidatorHookupControlID('" + fileNewIcon.ClientID + "', document.all['" + cvNewIcon.ClientID + "']);";
                strJS += "</script>";

                Page.ClientScript.RegisterStartupScript(Page.GetType(), "CompanyLogo", strJS, false);
            }
        }
        else if (strControlValidator == fileFavicon.ID)
        {
            if ((!Page.ClientScript.IsStartupScriptRegistered("Favicon")))
            {
                string strJS = null;
                strJS = "<script type = \"text/javascript\">";
                strJS += "ValidatorHookupControlID('" + fileFavicon.ClientID + "', document.all['" + cvFavicon.ClientID + "']);";
                strJS += "</script>";

                Page.ClientScript.RegisterStartupScript(Page.GetType(), "Favicon", strJS, false);
            }
        }

        else if (strControlValidator == fileCALogo.ID)
        {
            if ((!Page.ClientScript.IsStartupScriptRegistered("CALogo")))
            {
                string strJS = null;
                strJS = "<script type = \"text/javascript\">";
                strJS += "ValidatorHookupControlID('" + fileCALogo.ClientID + "', document.all['" + cvCALogo.ClientID + "']);";
                strJS += "</script>";

                Page.ClientScript.RegisterStartupScript(Page.GetType(), "CALogo", strJS, false);
            }
        }
    }
    protected void cvWatermarkImage_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("WatermarkImage")))
        {
            string strJS = null;
            strJS = "<script type = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + fileWaterMark.ClientID + "', document.all['" + cvWatermarkImage.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WatermarkImage", strJS, false);
        }
    }
    protected void validateWatermarkImage_server(object source, ServerValidateEventArgs args)
    {
        if (fileWaterMark.HasFile)
        {
            int intImgMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            if (fileWaterMark.PostedFile.ContentLength > intImgMaxSize)
            {
                cvWatermarkImage.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + ".";
                args.IsValid = false;
            }
            else
            {
                Match matchImage = Regex.Match(fileWaterMark.FileName, clsAdmin.CONSTADMIMGALLOWEDEXT);
                if (matchImage.Success)
                {
                    args.IsValid = true;
                }
                else
                {
                    cvWatermarkImage.ErrorMessage = "<br />Please enter valid watermark image. (" + clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + ").";
                    args.IsValid = false;
                }
            }
        }
    }
    protected void lnkbtnDelete_Click(object sender, CommandEventArgs e)
    {

        if (e.CommandArgument.ToString() == "cmaCompanyLogo")
        {
            pnlCompanyLogo.Visible = false;
            hdnCompanyLogo.Value = "";
        }
        else if (e.CommandArgument.ToString() == "cmaRecIcon")
        {
            pnlRecIcon.Visible = false;
            hdnRecIcon.Value = "";
        }
        else if (e.CommandArgument.ToString() == "cmaNewIcon")
        {
            pnlNewIcon.Visible = false;
            hdnNewIcon.Value = "";
        }
        else if (e.CommandArgument.ToString() == "cmaFavicon")
        {
            pnlFavicon.Visible = false;
            hdnFavicon.Value = "";
        }
        else if (e.CommandArgument.ToString() == "cmaWatermarkImage")
        {
            pnlWatermark.Visible = false;
            hdnWatermarkImage.Value = "";
        }
        else if (e.CommandArgument.ToString() == "cmaCALogo")
        {
            pnlCALogo.Visible = false;
            hdnCALogo.Value = "";
        }
    }
    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            boolSuccess = true;
            boolEdited = false;

            string strLogoGroupName = clsConfig.CONSTGROUPLOGOANDICONSETTINGS;
            intRecordAffected = 0;

            ArrayList listGroupName = new ArrayList();
            ArrayList listLblVal = new ArrayList();
            ArrayList listVal = new ArrayList();
            ArrayList listHdnVal = new ArrayList();
            ArrayList listFileUpload = new ArrayList();
            ArrayList listCropImg = new ArrayList();

            // 0 = company logo
            listLblVal.Add(lblCompanyLogo.Text);
            listVal.Add("");
            listHdnVal.Add(hdnCompanyLogo.Value);
            listFileUpload.Add(fileCompanyLogo);
            listGroupName.Add(strLogoGroupName);
            listCropImg.Add(hdnCompanyLogoImgCropped.Value);

            // 1 = new icon
            listLblVal.Add(lblNewIcon.Text);
            listVal.Add("");
            listHdnVal.Add(hdnNewIcon.Value);
            listFileUpload.Add(fileNewIcon);
            listGroupName.Add(strLogoGroupName);
            listCropImg.Add("");

            // 2 = recommend icon
            listLblVal.Add(lblRecIcon.Text);
            listVal.Add("");
            listHdnVal.Add(hdnRecIcon.Value);
            listFileUpload.Add(fileRecIcon);
            listGroupName.Add(strLogoGroupName);
            listCropImg.Add("");

            // 3 = fav icon
            listLblVal.Add(lblFaviconImage.Text);
            listVal.Add("");
            listHdnVal.Add(hdnFavicon.Value);
            listFileUpload.Add(fileFavicon);
            listGroupName.Add(clsConfig.CONSTGROUPFAVICON);
            listCropImg.Add("");

            if (ddlWaterMarkType.SelectedValue == clsAdmin.CONSTWATERMARKTYPEIMAGE)
            {
                // 4 = watermark image
                listLblVal.Add(clsConfig.CONSTNAMEWATERMARKVALUE);
                listVal.Add("");
                listHdnVal.Add(hdnWatermarkImage.Value);
                listFileUpload.Add(fileWaterMark);
                listGroupName.Add(clsConfig.CONSTGROUPWATERMARKSETTINGS);
                listCropImg.Add(hdnWatermarkImgCropped.Value);
            }

            // 5 = CA Logo
            listLblVal.Add(lblCALogo.Text);
            listVal.Add("");
            listHdnVal.Add(hdnCALogo.Value);
            listFileUpload.Add(fileCALogo);
            listGroupName.Add(strLogoGroupName);
            listCropImg.Add(hdnCALogoImgCropped.Value);

            // SAVING IMAGE
            if (new[] { listGroupName.Count,listLblVal.Count, listVal.Count, listHdnVal.Count }.All(x => x == listFileUpload.Count))
            {
                for (int i = 0; i < listFileUpload.Count; i++)
                {
                    FileUpload fileUpload = (FileUpload)listFileUpload[i];
                    if (fileUpload.HasFile)
                    {
                        try
                        {
                            HttpPostedFile postedFile = fileUpload.PostedFile;
                            string fileName = Path.GetFileName(postedFile.FileName);
                            string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCTRLPNLADMFOLDER + "/";
                            clsMis.createFolder(uploadPath);

                            string newFilename = clsMis.GetUniqueFilename(fileName, uploadPath);
                            bool boolCroppedImg = (!string.IsNullOrEmpty(listCropImg[i].ToString()));
                            if (boolCroppedImg) {
                                string base64 = listCropImg[i].ToString();
                                byte[] bytes = Convert.FromBase64String(base64.Split(',')[1]);

                                using (FileStream fs = new FileStream(uploadPath + newFilename, FileMode.Create))
                                {
                                    using (BinaryWriter bw = new BinaryWriter(fs)) { bw.Write(bytes); bw.Close(); }
                                }
                            }
                            else { postedFile.SaveAs(uploadPath + newFilename); }

                            

                            listVal[i] = newFilename;
                        }
                        catch (Exception ex)
                        {
                            clsLog.logErroMsg(ex.Message.ToString());
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />" + ex.Message.ToString() + "</div>";

                            throw ex;
                        }
                    }
                    else { listVal[i] = listHdnVal[i]; }
                }
                for (int i = 0; i < listFileUpload.Count; i++)
                {
                    
                    string strLblVal = (string)listLblVal[i];
                    string strVal = (string)listVal[i];
                    string strGroupName = (string)listGroupName[i];

                    if (boolSuccess && !config.isItemExist(strLblVal, strGroupName))
                    {
                        intRecordAffected = config.addItem(strLblVal, strVal, strGroupName, 1, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1) { boolEdited = true; }
                        else { boolSuccess = false; }
                    }
                    else if (!config.isExactSameSetData(strLblVal, strVal, strGroupName))
                    {
                        intRecordAffected = config.updateItem(strGroupName, strLblVal, strVal, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1) { boolEdited = true; }
                        else { boolSuccess = false; }
                    }
                }
            }
            else { boolSuccess = false; }

            // SAVING WATERMARK SETTING
            string strWatermarkGroupName = clsConfig.CONSTGROUPWATERMARKSETTINGS;
            string strWatermarkType = ddlWaterMarkType.SelectedValue;
            string strWatermarkTypeLbl = clsConfig.CONSTNAMEWATERMARKTYPE;
            if (boolSuccess && !config.isItemExist(strWatermarkTypeLbl, strWatermarkGroupName))
            {
                intRecordAffected = config.addItem(strWatermarkTypeLbl, strWatermarkType, strWatermarkGroupName, 1, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }
            else if (!config.isExactSameSetData(strWatermarkTypeLbl, strWatermarkType, strWatermarkGroupName))
            {
                intRecordAffected = config.updateItem(strWatermarkGroupName, strWatermarkTypeLbl, strWatermarkType, int.Parse(Session["ADMID"].ToString()));
                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }

            if (!string.IsNullOrEmpty(strWatermarkType))
            {
                string strWatermarkOpacity = Request["txtOpacity"].ToString();
                string strWatermarkOpacityLbl = clsConfig.CONSTNAMEWATERMARKOPACITY;
                if (boolSuccess && !config.isItemExist(strWatermarkOpacityLbl, strWatermarkGroupName))
                {
                    intRecordAffected = config.addItem(strWatermarkOpacityLbl, strWatermarkOpacity, strWatermarkGroupName, 1, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; }
                }
                else if (!config.isExactSameSetData(strWatermarkOpacityLbl, strWatermarkOpacity, strWatermarkGroupName))
                {
                    intRecordAffected = config.updateItem(strWatermarkGroupName, strWatermarkOpacityLbl, strWatermarkOpacity, int.Parse(Session["ADMID"].ToString()));
                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; }
                }

                if (strWatermarkType == clsAdmin.CONSTWATERMARKTYPETEXT)
                {
                    string strWatermarkValue = txtWaterMark.Text;
                    string strWatermarkValueLbl = clsConfig.CONSTNAMEWATERMARKVALUE;
                    if (boolSuccess && !config.isItemExist(strWatermarkValueLbl, strWatermarkGroupName))
                    {
                        intRecordAffected = config.addItem(strWatermarkValueLbl, strWatermarkValue, strWatermarkGroupName, 1, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1) { boolEdited = true; }
                        else { boolSuccess = false; }
                    }
                    else if (!config.isExactSameSetData(strWatermarkValueLbl, strWatermarkValue, strWatermarkGroupName))
                    {
                        intRecordAffected = config.updateItem(strWatermarkGroupName, strWatermarkValueLbl, strWatermarkValue, int.Parse(Session["ADMID"].ToString()));
                        if (intRecordAffected == 1) { boolEdited = true; }
                        else { boolSuccess = false; }
                    }
                }
                
            }
            // END OF SAVING WATERMARK SETTING

            // SAVING Company Logo Value
            if (boolSuccess && !config.isItemExist(lblCompanyLogo.Text, strLogoGroupName))
            {
                intRecordAffected = config.addItem(lblCompanyLogo.Text, txtCompanyLogoTitle.Text, strLogoGroupName, 1, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }
            else if (!config.isExactSameSetData(lblCompanyLogo.Text, txtCompanyLogoTitle.Text, strLogoGroupName))
            {
                intRecordAffected = config.updateItem2(strLogoGroupName, lblCompanyLogo.Text, txtCompanyLogoTitle.Text, int.Parse(Session["ADMID"].ToString()));
                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }

            // crop image feature - sh.chong 28 Sep 2015
            {
                string confGroup = clsConfig.CONSTGROUPCROPIMAGESETTINGS;
                string confValue = "1/1";
                bool boolNotEmpty = !string.IsNullOrEmpty(txtRatioX.Value) && !string.IsNullOrEmpty(txtRatioY.Value);
                if (boolNotEmpty) { confValue = txtRatioX.Value + "/" + txtRatioY.Value; }

                string[,] confArray = { { clsConfig.CONSTNAMEASPECTRATIO, confValue } };
                saveConfig(confArray, confGroup);
            }
            // end crop image

            // prod image ratio - sh.chong 2 Nov 2016
            {
                string confGroup = clsConfig.CONSTGROUPPRODUCT;
                string confValue = "1/1";
                if (!string.IsNullOrEmpty(txtProdRatioX.Value) && !string.IsNullOrEmpty(txtProdRatioY.Value))
                {
                    confValue = txtProdRatioX.Value + "/" + txtProdRatioY.Value;
                }

                string[,] confArray = { { clsConfig.CONSTNAMEPRODIMAGERATIO, confValue } };
                saveConfig(confArray, confGroup);
            }
            // end crop image

            if (!boolSuccess)
            {
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to update control panel. Please try again.</div>";
                Response.Redirect(currentPageName);
            }
            else
            {
                if (boolEdited)
                {
                    Session["EDITEDLOGO"] = 1;
                }
                else
                {
                    Session["EDITEDLOGO"] = 1;
                    Session["NOCHANGE"] = 1;
                }

                Response.Redirect(currentPageName);
            }
        }
    }
    #endregion

    
    #region "Methods"
    public void fill()
    {

        DataSet ds = config.getItemList(1);
        DataRow[] foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPLOGOANDICONSETTINGS + "'");
        DataRow[] dwWatermarks = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPWATERMARKSETTINGS + "'");
        DataRow[] dwFavicon = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPFAVICON + "'");
        string strWatermarkType = "0",
               strWatermarkValue = "",
               strWatermarkOpacity = "0.1";

        #region "watermark"
        if (dwWatermarks.Length > 0)
        {
            foreach (DataRow dwWatermark in dwWatermarks)
            {
                if(dwWatermark["CONF_NAME"].ToString() == clsConfig.CONSTNAMEWATERMARKTYPE)
                {
                    if (!string.IsNullOrEmpty(dwWatermark["CONF_VALUE"].ToString()))
                    {
                        strWatermarkType = dwWatermark["CONF_VALUE"].ToString();
                    }
                }
                else if(dwWatermark["CONF_NAME"].ToString() == clsConfig.CONSTNAMEWATERMARKVALUE)
                {
                    strWatermarkValue = dwWatermark["CONF_VALUE"].ToString();
                }
                else if(dwWatermark["CONF_NAME"].ToString() == clsConfig.CONSTNAMEWATERMARKOPACITY)
                {
                    strWatermarkOpacity = dwWatermark["CONF_VALUE"].ToString();
                }
            }

        }
        ddlWaterMarkType.SelectedValue = strWatermarkType;
        hdnOpacity.Value = strWatermarkOpacity;
        bool boolWatermarkImageExists = (strWatermarkType == clsAdmin.CONSTWATERMARKTYPEIMAGE && !string.IsNullOrEmpty(strWatermarkValue));
        bool boolWatermarkTextExists = (strWatermarkType == clsAdmin.CONSTWATERMARKTYPETEXT && !string.IsNullOrEmpty(strWatermarkValue));

        if (boolWatermarkTextExists)
        {
            txtWaterMark.Text = strWatermarkValue;
        }
        else if (boolWatermarkImageExists)
        { 
            pnlWatermark.Visible = true;
            imgWatermarkImage.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCTRLPNLADMFOLDER + "/" + strWatermarkValue + "&f=1";
            hdnWatermarkImage.Value = hdnWatermarkImageRef.Value = strWatermarkValue;
        }
        #endregion
        #region "favicon"
        foreach (DataRow row in dwFavicon)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFAVICONIMAGE, true) == 0)
            {
                string strImageUrl = row["CONF_VALUE"].ToString();
                if (!string.IsNullOrEmpty(strImageUrl))
                {
                    pnlFavicon.Visible = true;
                    imgFavicon.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCTRLPNLADMFOLDER + "/" + strImageUrl + "&f=1";
                    if (strImageUrl.StartsWith("http"))
                    {
                        imgFavicon.ImageUrl = strImageUrl;
                    }
                    hdnFavicon.Value = hdnFaviconRef.Value = strImageUrl;
                }
            }
        }
        #endregion
        #region "New,Recommend,Companylogo"
        int intSize = 4;
        string[] strConfName = new string[intSize];

        Panel [] pnlVisible = new Panel[intSize];
        Image[] imgUrl = new Image[intSize];
        HiddenField[] hdnIcon = new HiddenField[intSize];
        HiddenField[] hdnIconValue = new HiddenField[intSize];

        strConfName[0] = clsConfig.CONSTNAMELOGOCOMPANYLOGO;
        pnlVisible[0] = pnlCompanyLogo;
        imgUrl[0] = imgCompanyLogo;
        hdnIcon[0] = hdnCompanyLogo;
        hdnIconValue[0] = hdnCompanyLogoRef;

        strConfName[1] = clsConfig.CONSTNAMELOGORECICON;
        pnlVisible[1] = pnlRecIcon;
        imgUrl[1] = imgRecIcon;
        hdnIcon[1] = hdnRecIcon;
        hdnIconValue[1] = hdnRecIconRef;

        strConfName[2] = clsConfig.CONSTNAMELOGONEWICON;
        pnlVisible[2] = pnlNewIcon;
        imgUrl[2] = imgNewIcon;
        hdnIcon[2] = hdnNewIcon;
        hdnIconValue[2] = hdnNewIconRef;

        strConfName[3] = clsConfig.CONSTNAMELOGOCALOGO;
        pnlVisible[3] = pnlCALogo;
        imgUrl[3] = imgCALogo;
        hdnIcon[3] = hdnCALogo;
        hdnIconValue[3] = hdnCALogoRef;

        foreach (DataRow row in foundRow)
        {
            string strImageUrl = "";
            if (strConfName.Contains(row["CONF_NAME"].ToString()))
            {
                int index = Array.IndexOf(strConfName, row["CONF_NAME"].ToString());
                strImageUrl = row["CONF_VALUE"].ToString();
                if (strImageUrl != "")
                {
                    pnlVisible[index].Visible = true;
                    imgUrl[index].ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCTRLPNLADMFOLDER + "/" + strImageUrl + "&f=1";
                    hdnIcon[index].Value = hdnIconValue[index].Value = strImageUrl;
                }

                if(strConfName[index] == clsConfig.CONSTNAMELOGOCOMPANYLOGO)
                {
                    txtCompanyLogoTitle.Text = row["CONF_LONGVALUE"].ToString();
                }
            }
        }
        #endregion


    }
    protected void setPageProperties()
    {
        if (Session[clsAdmin.CONSTPROJECTWATERMARK] != null && clsAdmin.CONSTPROJECTWATERMARK != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTWATERMARK]) == 0)
            {
                divWatermarkContainer.Visible = false;
            }
        }

        if (Session[clsAdmin.CONSTPROJECTTYPE] != null && Session[clsAdmin.CONSTPROJECTTYPE] != "")
        {
            int intType = int.Parse(Session[clsAdmin.CONSTPROJECTTYPE].ToString());
            int[] intEshopEcatalog = {
                                         clsAdmin.CONSTTYPEeSHOP, clsAdmin.CONSTTYPEeSHOPLITE, clsAdmin.CONSTTYPEESHOPINVOICE, clsAdmin.CONSTTYPEESHOPLITEINVOICE,
                                         clsAdmin.CONSTTYPEeSHOPEVENTMANAGER, clsAdmin.CONSTTYPEeCATALOG, clsAdmin.CONSTTYPEeCATALOGLITE,clsAdmin.CONSTTYPEeCATALOGLITEEVENTMANAGER,
                                         clsAdmin.CONSTTYPEeCATALOGEVENTMANAGER, clsAdmin.CONSTTYPEeSHOP, clsAdmin.CONSTTYPEeSHOPLITE, clsAdmin.CONSTTYPEESHOPINVOICE,
                                         clsAdmin.CONSTTYPEESHOPLITEINVOICE,clsAdmin.CONSTTYPEeSHOPEVENTMANAGER
                                     };


            if (intEshopEcatalog.Contains(intType))
            {
                divECatalogEshopContainer.Visible = true;
            }

            int intImgMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            litUploadRule.Text = 
            litUploadRule2.Text = 
            litUploadRule3.Text =
            litUploadRule4.Text =
            litUploadRule5.Text =
            litUploadRule6.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + " | " + clsMis.formatAllowedFileDimension(clsAdmin.CONSTADMCURIMGALLOWEDWIDTH, clsAdmin.CONSTADMCURIMGALLOWEDHEIGHT, "px");

            lblCompanyLogo.Text = clsConfig.CONSTNAMELOGOCOMPANYLOGO;
            lblRecIcon.Text = clsConfig.CONSTNAMELOGORECICON;
            lblNewIcon.Text = clsConfig.CONSTNAMELOGONEWICON;
            lblWaterMarkType.Text = clsConfig.CONSTNAMEWATERMARKTYPE;
            lblFaviconImage.Text = clsConfig.CONSTNAMEFAVICONIMAGE;
            lblCALogo.Text = clsConfig.CONSTNAMELOGOCALOGO;
            lblCompanyLogoTitle.Text = clsConfig.CONSTNAMELOGOCOMPANYLOGO + " Title";

            //Add by sh.chong 16Apr2015 for water mark purpose
            clsMis mis = new clsMis();
            DataSet dsWatermarkType = mis.getListByListGrp(clsConfig.CONSTGROUPWATERMARKSETTINGS);
            DataView dvWatermarkType = new DataView(dsWatermarkType.Tables[0]);

            ddlWaterMarkType.DataSource = dvWatermarkType;
            ddlWaterMarkType.DataValueField = "LIST_VALUE";
            ddlWaterMarkType.DataTextField = "LIST_NAME";
            ddlWaterMarkType.DataBind();

            ddlWaterMarkType.Items.Insert(0, new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), ""));
            ddlWaterMarkType.Attributes["onchange"] = "ddlWaterMarkTypeChange(this);";
            //End by sh.chong 16Apr2015 for water mark purpose

            // Crop Image feature - sh.chong 28 Sep 2015
            lblAspectRatio.Text = clsConfig.CONSTNAMEASPECTRATIO;
            txtRatioX.Attributes["type"] = "number";
            txtRatioY.Attributes["type"] = "number";

            DataRow[] dwCropImage = config.getItemsByGroup(clsConfig.CONSTGROUPCROPIMAGESETTINGS, 1).Tables[0].Select();
            if(dwCropImage.Length > 0)
            {
                foreach(DataRow dw in dwCropImage)
                {
                    if (string.Compare(dw["CONF_NAME"].ToString(), clsConfig.CONSTNAMEASPECTRATIO, true) == 0) {
                        string[] strXandY = dw["CONF_VALUE"].ToString().Split('/');
                        if(strXandY.Length == 2)
                        {
                            txtRatioX.Value = strXandY[0];
                            txtRatioY.Value = strXandY[1];
                        }
                    }
                }
            }
            // end Crop Image feature

            // Prod Image ratio - sh.chong 2 Nov 2016
            txtProdRatioX.Attributes["type"] = "number";
            txtProdRatioY.Attributes["type"] = "number";

            DataRow[] dwProdSettings = config.getItemsByGroup(clsConfig.CONSTGROUPPRODUCT, 1).Tables[0].Select();
            if (dwProdSettings.Length > 0)
            {
                foreach (DataRow dw in dwProdSettings)
                {
                    if (string.Compare(dw["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPRODIMAGERATIO, true) == 0)
                    {
                        string[] strXandY = dw["CONF_VALUE"].ToString().Split('/');
                        if (strXandY.Length == 2)
                        {
                            txtProdRatioX.Value = strXandY[0];
                            txtProdRatioY.Value = strXandY[1];
                        }
                    }
                }
            }
            // end Crop Image feature

        }
    }

    protected void saveConfig(string[,] confArray, string confGroup)
    {
        for (int i = 0; i < confArray.GetLength(0); i++)
        {
            string confName = confArray[i, 0];
            string confValue = confArray[i, 1];
            if (boolSuccess && !config.isItemExist(confName, confGroup))
            {
                intRecordAffected = config.addItem(confName, confValue, confGroup, 1, int.Parse(Session["ADMID"].ToString()));
            }
            else if (!config.isExactSameSetData(confName, confValue, confGroup))
            {
                intRecordAffected = config.updateItem(confGroup, confName, confValue, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; break; }
            }
        }
    }
    #endregion
}