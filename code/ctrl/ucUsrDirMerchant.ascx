﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrDirMerchant.ascx.cs" Inherits="ctrl_ucUsrDirMerchant" %>

<asp:Panel ID="pnlMemNoFound" runat="server" CssClass="divMemNoFoundContainer" Visible="false">
    <div class="divMemNoFound">
        <asp:Literal ID="litMemNoFound" runat="server" meta:resourcekey="litMemNoFound"></asp:Literal>
    </div>
</asp:Panel>
<asp:Panel ID="pnlListing" runat="server" CssClass="divMemListContainer">
    <asp:Panel ID="pnlListPaginationContainer" runat="server" CssClass="divListPaginationContainer">
        <asp:Panel ID="pnlListPaginationTop" runat="server" CssClass="divListPaginationTop">
            <div class="divListPaginationInner">   
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td><asp:HyperLink ID="hypPaginationFirst" runat="server" CssClass="hypPagination" meta:resourcekey="hypPaginationFirst"></asp:HyperLink></td>
                        <td>
                            <span class="spanPagination">
                                <asp:Repeater ID="rptPaginationTop" runat="server" OnItemDataBound="rptPagination_ItemDataBound" OnItemCommand="rptPagination_ItemCommand">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hypPageNo" runat="server" CssClass="btnPagination" Text="<% #Container.DataItem %>" ToolTip="<% #Container.DataItem %>"></asp:HyperLink>                                        
                                    </ItemTemplate>
                                </asp:Repeater>
                            </span>                        
                        </td>
                        <td><asp:HyperLink ID="hypPaginationLast" runat="server" CssClass="hypPagination" meta:resourcekey="hypPaginationLast"></asp:HyperLink></td>
                    </tr>
                </table>    
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlSort" runat="server" CssClass="divMemberSort">
            <asp:Label ID="lblSort" runat="server" meta:resourcekey="lblSort"></asp:Label><asp:DropDownList ID="ddlSort" runat="server" OnSelectedIndexChanged="ddlSort_SelectedIndexChanged" AutoPostBack="true" CssClass="ddl_medium"></asp:DropDownList>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlMemberList" runat="server" CssClass="divMemberListOuter">
        <asp:Repeater ID="rptMember" runat="server" OnItemDataBound="rptMember_ItemDataBound" OnItemCommand="rptMember_ItemCommand">
            <ItemTemplate>
                <asp:Panel ID="pnlIndMember" runat="server" CssClass="divMemberItem">
                    <asp:Panel ID="pnlMemberImg" runat="server" CssClass="divMemberItemImg">
                        <div class="divMemberItemImgInner">            
                            <asp:HyperLink ID="hypMerchantLogo" runat="server"><asp:Image ID="imgMerchantLogo" runat="server" /></asp:HyperLink>                            
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlMemberDetail" runat="server" CssClass="divMemberItemDetail">
                        <asp:Panel ID="pnlMemberTitle" runat="server" CssClass="divMemberItemTitle">
                            <h2><asp:HyperLink ID="hypMerchantName" runat="server" class="hypMerchantName"></asp:HyperLink></h2>
                        </asp:Panel>
                        <asp:Literal ID="litMemberAdd" runat="server"></asp:Literal>
                        <asp:Literal ID="litMemberContactInfo" runat="server"></asp:Literal>                        
                        <asp:Literal ID="litMerchantEmail" runat="server"></asp:Literal>
                        <asp:Literal ID="litMerchantWebsite" runat="server"></asp:Literal>
                        <asp:Literal ID="litMemberShortDesc" runat="server"></asp:Literal>
                        <asp:Panel ID="pnlReadMore" runat="server" CssClass="divMemberRead">
                            <asp:HyperLink ID="hypReadMore" runat="server" meta:resourcekey="hypReadMore" CssClass="linkRead"></asp:HyperLink>                            
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </ItemTemplate>
            <SeparatorTemplate>
            <div class="divMemberSeperator"></div>
            </SeparatorTemplate>
        </asp:Repeater>
    </asp:Panel>  
    
</asp:Panel>
