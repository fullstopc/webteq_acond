﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrBookingStep3.ascx.cs" Inherits="ctrl_ucUsr" %>
<div class="row">
    <div class="col">
        <h3>
            Your booking is almost done. Please double check the booking details below.
        </h3>
    </div>
</div>
<div class="bookingform__review">
    <div class="row">
        <div class="col bookingform__text--lightgray">
            Date
        </div>
        <div class="col">
            <asp:Literal runat="server" ID="litServiceDate"></asp:Literal>
        </div>
    </div>
    <div class="row">
        <div class="col bookingform__text--lightgray">
            Time
        </div>
        <div class="col">
            <asp:Literal runat="server" ID="litServiceTime"></asp:Literal>
        </div>
    </div>
    <div class="row">
        <div class="col bookingform__text--lightgray">
            Unit Number:
        </div>
        <div class="col">
            <asp:Literal runat="server" ID="litServiceAddrUnit"></asp:Literal>
        </div>
    </div>
    <div class="row">
        <div class="col bookingform__text--lightgray">
            No. of Aircond:
        </div>
        <div class="col">
            <asp:Literal runat="server" ID="litServiceItemQty"></asp:Literal>
        </div>
    </div>
    <div class="row bookingform__amount">
        <div class="col">
            Cost:
        </div>
        <div class="col">
            <asp:Literal runat="server" ID="litServiceAmount"></asp:Literal>
        </div>
    </div>
</div>
<div class="row bookingform__remark">
    <div class="col">
        <asp:TextBox runat="server" 
            ID="txtServiceRemark" 
            TextMode="MultiLine"
            Rows="5"
            placeholder="If you have special request, please input here."></asp:TextBox>
    </div>
</div>
<div class="bookingform__contact align center">
    <div class="row bookingform__contact__info">
        <div class="col">
            <h3>
                For urgent case, we should contact this number:
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <asp:TextBox runat="server" ID="txtServiceContactTel"></asp:TextBox>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <asp:LinkButton ID="lnkbtnSubmit" runat="server" 
            CssClass="button button-full button-round button-icon-right button-edge" 
            OnClick="lnkbtnSubmit_Click" 
            ValidationGroup="vg" 
            ToolTip="Confirm & Pay">
            <span>Confirm & Pay</span>
            <i class="material-icons">keyboard_arrow_right</i>
        </asp:LinkButton>
    </div>
</div>