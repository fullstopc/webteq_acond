﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

public partial class ctrl_ucAdmCMSControlPanel : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _shipCompId = 0;
    protected int _shipMethodId = 0;
    protected int _areaId = 0;
    protected int _mode = 1;
    protected int _currId = 0;
    protected int itemCount = 0;
    protected int itemTotal = 0;
    protected DataSet _dsCurrency = new DataSet();
    private enum status
    {
        success = 1,
        fail = 2,
        nochange = 3
    };
    
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int shipCompId
    {
        get { return _shipCompId; }
        set { _shipCompId = value; }
    }

    public int shipMethodId
    {
        get { return _shipMethodId; }
        set { _shipMethodId = value; }
    }

    public int areaId
    {
        get { return _areaId; }
        set { _areaId = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int currId
    {
        get
        {
            if (ViewState["currId"] == null)
            {
                return _currId;
            }
            else
            {
                return int.Parse(ViewState["currId"].ToString());
            }
        }
        set { ViewState["currId"] = value; }
    }

    protected DataSet dsCurrency
    {
        get
        {
            if (ViewState["DSCURRENCY"] == null)
            {
                return _dsCurrency;
            }
            else
            {
                return (DataSet)ViewState["DSCURRENCY"];
            }
        }
        set { ViewState["DSCURRENCY"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void validateInvoiceImage_server(object source, ServerValidateEventArgs args)
    {
        if (fileInvoiceImage.HasFile)
        {
            int intFileMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTFILESETTING] != null && Session[clsAdmin.CONSTPROJECTFILESETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTFILESETTING]) > 0)
                {
                    intFileMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTFILESETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intFileMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            //if (fileInvoiceImage.PostedFile.ContentLength > clsAdmin.CONSTADMDOCALLOWEDMAXLENGTH)
            //{
            //    cvInvoiceImage.ErrorMessage = "<br/>File size exceeded " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMDOCALLOWEDMAXLENGTH, "kb") + ".";
            //    args.IsValid = false;
            //}

            if (fileInvoiceImage.PostedFile.ContentLength > intFileMaxSize)
            {
                cvInvoiceImage.ErrorMessage = "<br/>File size exceeded " + clsMis.formatAllowedFileSize(intFileMaxSize, "kb") + ".";
                args.IsValid = false;
            }
            else
            {
                Match matchImage = Regex.Match(fileInvoiceImage.FileName, clsAdmin.CONSTADMIMGALLOWEDEXT, RegexOptions.IgnoreCase);
                if (matchImage.Success)
                {
                    args.IsValid = true;
                }
                else
                {
                    cvInvoiceImage.ErrorMessage = "<br/>Please enter valid image. (" + clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + ").";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cvInvoiceImage_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("InvoiceImage")))
        {
            string strJS = null;
            strJS = "<script type = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + fileInvoiceImage.ClientID + "', document.all['" + cvInvoiceImage.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "InvoiceImage", strJS, false);
        }
    }

    protected void lnkbtnDeleteImage_Click(object sender, EventArgs e)
    {
        pnlInvoiceImage.Visible = false;
        hdnInvoiceImage.Value = "";
    }

    protected void ddlShippingType_OnSelectedChanged(object sender, EventArgs e)
    {
        if (ddlShippingType.SelectedValue == clsAdmin.CONSTSHIPPINGTYPE5.ToString())
        {
            trShippingComp.Visible = true;
            trShipMethod.Visible = true;
        }
        else
        {
            trShippingComp.Visible = false;
            trShipMethod.Visible = false;
        }
    }

    #region "Shipping Company"
    protected void rptShipComp_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strShipCompName = DataBinder.Eval(e.Item.DataItem, "CONF_NAME").ToString();
            string strShipCompOrder = DataBinder.Eval(e.Item.DataItem, "CONF_ORDER").ToString();

            Label lblShipCompName = (Label)e.Item.FindControl("lblShipCompName");
            lblShipCompName.Text = strShipCompName;

            Label lblShipCompOrder = (Label)e.Item.FindControl("lblShipCompOrder");
            lblShipCompOrder.Text = strShipCompOrder;

            LinkButton lnkbtnShipCompDelete = (LinkButton)e.Item.FindControl("lnkbtnShipCompDelete");
            lnkbtnShipCompDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteShippingCompany.Text").ToString() + "'); return false;";
        }
    }

    protected void rptShipComp_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdEdit")
        {
            int intShipCompId = Convert.ToInt16(e.CommandArgument);

            Response.Redirect(clsAdmin.CONSTADMCONTROLPANEL + "?id=" + intShipCompId);
        }
        else if (e.CommandName == "cmdDelete")
        {
            int intShipCompId = Convert.ToInt16(e.CommandArgument);

            clsConfig config = new clsConfig();
            int intRecordAffected = 0;

            intRecordAffected = config.deleteShippingSettingById(intShipCompId);

            if (intRecordAffected == 1)
            {
                clsMis.resetListing();
                bindRptData();
            }
        }
    }

    protected void lnkbtnAddShipComp_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strShipCompName = txtShipComp.Text.Trim();
            string strShipCompValue = txtShipComp.Text.Trim();
            int intShipCompOrder = 0;

            int intTryParse;
            if (int.TryParse(txtShipCompOrder.Text.Trim(), out intTryParse)) { intShipCompOrder = intTryParse; }

            clsConfig config = new clsConfig();
            int intRecordAffected = 0;
            Boolean boolEdited = false;
            Boolean boolSuccess = true;

            switch (mode)
            {
                case 1:
                    if (!config.isShippingSettingExist(strShipCompName, clsConfig.CONSTGROUPSHIPPINGCOMPANY))
                    {
                        intRecordAffected = config.addShippingSetting(strShipCompName, strShipCompValue, intShipCompOrder, clsConfig.CONSTGROUPSHIPPINGCOMPANY, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1) { boolEdited = true; }
                        else { boolSuccess = false; }

                        if (!boolSuccess)
                        {
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add shipping company. Please try again.</div>";

                            Response.Redirect(currentPageName);
                        }
                        else
                        {
                            if (boolEdited)
                            {
                                Session["EDITEDCMSCP"] = 1;
                            }
                            else
                            {
                                Session["EDITEDCMSCP"] = 1;
                                Session["NOCHANGE"] = 1;
                            }

                            Response.Redirect(currentPageName);
                        }
                    }
                    break;
                case 2:
                    if (!config.isExactSameSetData3(strShipCompName, strShipCompValue, clsConfig.CONSTGROUPSHIPPINGCOMPANY, intShipCompOrder))
                    {
                        intRecordAffected = config.updateItemById2(shipCompId, clsConfig.CONSTGROUPSHIPPINGCOMPANY, strShipCompName, strShipCompValue, intShipCompOrder, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;
                        }
                        else
                        {
                            boolSuccess = false;
                        }
                    }

                    if (boolEdited)
                    {
                        Session["EDITEDCMSCP"] = config.id;
                        Response.Redirect(currentPageName);
                    }
                    else
                    {
                        if (!boolSuccess)
                        {
                            config.extractConfigByConfigId(shipCompId, 0);
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit " + config.name + ". Please try again.</div>";
                            Response.Redirect(currentPageName);
                        }
                        else
                        {
                            Session["EDITEDCMSCP"] = shipCompId;
                            Session["NOCHANGE"] = 1;
                            Response.Redirect(currentPageName);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
    #endregion

    #region "Shipping Method"
    protected void rptShipMethod_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strShipMethodName = DataBinder.Eval(e.Item.DataItem, "CONF_NAME").ToString();
            string strShipMethodOrder = DataBinder.Eval(e.Item.DataItem, "CONF_ORDER").ToString();

            Label lblShipMethodName = (Label)e.Item.FindControl("lblShipMethodName");
            lblShipMethodName.Text = strShipMethodName;

            Label lblShipMethodOrder = (Label)e.Item.FindControl("lblShipMethodOrder");
            lblShipMethodOrder.Text = strShipMethodOrder;

            LinkButton lnkbtnShipMethodDelete = (LinkButton)e.Item.FindControl("lnkbtnShipMethodDelete");
            lnkbtnShipMethodDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteShippingMethod.Text").ToString() + "'); return false;";
        }
    }

    protected void rptShipMethod_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdEdit")
        {
            int intShipMethodId = Convert.ToInt16(e.CommandArgument);

            Response.Redirect(clsAdmin.CONSTADMCONTROLPANEL + "?id=" + intShipMethodId);
        }
        else if (e.CommandName == "cmdDelete")
        {
            int intShipMethodId = Convert.ToInt16(e.CommandArgument);

            clsConfig config = new clsConfig();
            int intRecordAffected = 0;

            intRecordAffected = config.deleteShippingSettingById(intShipMethodId);

            if (intRecordAffected == 1)
            {
                clsMis.resetListing();
                bindRptData();
            }
        }
    }

    protected void lnkbtnAddShipMethod_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strShipMethodName = txtShipMethod.Text.Trim();
            string strShipMethodValue = txtShipMethod.Text.Trim();
            int intShipMethodOrder = 0;

            int intTryParse;
            if (int.TryParse(txtShipMethodOrder.Text.Trim(), out intTryParse)) { intShipMethodOrder = intTryParse; }

            clsConfig config = new clsConfig();
            int intRecordAffected = 0;
            Boolean boolEdited = false;
            Boolean boolSuccess = true;

            switch (mode)
            {
                case 1:
                    if (!config.isShippingSettingExist(strShipMethodName, clsConfig.CONSTGROUPSHIPPINGMETHOD))
                    {
                        intRecordAffected = config.addShippingSetting(strShipMethodName, strShipMethodValue, intShipMethodOrder, clsConfig.CONSTGROUPSHIPPINGMETHOD, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1) { boolEdited = true; }
                        else { boolSuccess = false; }

                        if (!boolSuccess)
                        {
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add shipping method. Please try again.</div>";

                            Response.Redirect(currentPageName);
                        }
                        else
                        {
                            if (boolEdited)
                            {
                                Session["EDITEDCMSCP"] = 1;
                            }
                            else
                            {
                                Session["EDITEDCMSCP"] = 1;
                                Session["NOCHANGE"] = 1;
                            }

                            Response.Redirect(currentPageName);
                        }
                    }
                    break;
                case 2:
                    if (!config.isExactSameSetData3(strShipMethodName, strShipMethodValue, clsConfig.CONSTGROUPSHIPPINGMETHOD, intShipMethodOrder))
                    {
                        intRecordAffected = config.updateItemById2(shipMethodId, clsConfig.CONSTGROUPSHIPPINGMETHOD, strShipMethodName, strShipMethodValue, intShipMethodOrder, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;
                        }
                        else
                        {
                            boolSuccess = false;
                        }
                    }

                    if (boolEdited)
                    {
                        Session["EDITEDCMSCP"] = config.id;
                        Response.Redirect(currentPageName);
                    }
                    else
                    {
                        if (!boolSuccess)
                        {
                            config.extractConfigByConfigId(shipMethodId, 0);
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit " + config.name + ". Please try again.</div>";
                            Response.Redirect(currentPageName);
                        }
                        else
                        {
                            Session["EDITEDCMSCP"] = shipMethodId;
                            Session["NOCHANGE"] = 1;
                            Response.Redirect(currentPageName);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
    #endregion

    #region "Currency Conversion"
    protected void validateFlagImage_server(object source, ServerValidateEventArgs args)
    {
        if (fileCurrFlagImage.HasFile)
        {
            int intFileMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTFILESETTING] != null && Session[clsAdmin.CONSTPROJECTFILESETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTFILESETTING]) > 0)
                {
                    intFileMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTFILESETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intFileMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            //if (fileCurrFlagImage.PostedFile.ContentLength > clsAdmin.CONSTADMDOCALLOWEDMAXLENGTH)
            //{
            //    cvCurrFlagImage.ErrorMessage = "<br/>File size exceeded " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMDOCALLOWEDMAXLENGTH, "kb") + ".";
            //    args.IsValid = false;
            //}

            if (fileCurrFlagImage.PostedFile.ContentLength > intFileMaxSize)
            {
                cvCurrFlagImage.ErrorMessage = "<br/>File size exceeded " + clsMis.formatAllowedFileSize(intFileMaxSize, "kb") + ".";
                args.IsValid = false;
            }
            else
            {
                Match matchImage = Regex.Match(fileInvoiceImage.FileName, clsAdmin.CONSTADMIMGALLOWEDEXT, RegexOptions.IgnoreCase);
                if (matchImage.Success)
                {
                    args.IsValid = true;
                }
                else
                {
                    cvCurrFlagImage.ErrorMessage = "<br/>Please enter valid image. (" + clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + ").";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cvCurrFlagImage_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("CurFlagImage")))
        {
            string strJS = null;
            strJS = "<script type = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + fileCurrFlagImage.ClientID + "', document.all['" + cvCurrFlagImage.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CurFlagImage", strJS, false);
        }
    }

    protected void lnkbtnDeleteImageCurrFlag_Click(object sender, EventArgs e)
    {
        pnlFlagImage.Visible = false;
        hdnCurrFlagImage.Value = "";
    }

    protected void lnkbtnSaveCurrency_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strCurrName = txtCurrName.Text.Trim();
            string strCurrValue = txtCurrValue.Text.Trim();
            string strCurrConversionRate = txtCurrConversionRate.Text.Trim();

            string strcurrflag;

            if (fileCurrFlagImage.HasFile)
            {
                try
                {
                    HttpPostedFile postedFile = fileCurrFlagImage.PostedFile;
                    string fileName = Path.GetFileName(postedFile.FileName);
                    string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMTEMPFOLDER + "/";
                    clsMis.createFolder(uploadPath);

                    string newFilename = clsMis.GetUniqueFilename(fileName, uploadPath);
                    postedFile.SaveAs(uploadPath + newFilename);
                    strcurrflag = newFilename;
                }
                catch (Exception ex)
                {
                    clsLog.logErroMsg(ex.Message.ToString());
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />" + ex.Message.ToString() + "</div>";

                    throw ex;
                }
            }
            else { strcurrflag = ""; }

            string strCurrOrder = txtCurrOrder.Text.Trim();
            int intActive = chkboxCurrActive.Checked ? 1 : 0;

            clsCurrency currency = new clsCurrency();
            int intRecordAffected = 0;
            Boolean boolEdited = false;
            Boolean boolSuccess = true;

            switch (mode)
            {
                case 1:
                    if (!currency.isCurrencyExist(strCurrName, strCurrValue))
                    {
                        intRecordAffected = currency.addCurrency(strCurrName, strCurrValue, strcurrflag, strCurrOrder, intActive, strCurrConversionRate, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1) { boolEdited = true; }
                        else { boolSuccess = false; }

                        if (!boolSuccess)
                        {
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add currency. Please try again.</div>";

                            Response.Redirect(currentPageName + "#currency");
                        }
                        else
                        {
                            if (boolEdited)
                            {
                                Session["EDITEDCMSCP"] = 1;
                            }
                            else
                            {
                                Session["EDITEDCMSCP"] = 1;
                                Session["NOCHANGE"] = 1;
                            }

                            Response.Redirect(currentPageName + "#currency");
                        }
                    }
                    break;
                case 2:
                    if (!currency.isExactSameDataSet(currId, strCurrName, strCurrValue, strcurrflag, strCurrOrder, intActive, strCurrConversionRate))
                    {
                        intRecordAffected = currency.updateCurrencyById(currId, strCurrName, strCurrValue, strcurrflag, strCurrOrder, intActive, strCurrConversionRate, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1) { boolEdited = true; }
                        else { boolSuccess = false; }
                    }

                    if (boolEdited)
                    {
                        Session["EDITEDCMSCP"] = currency.currId;
                        Response.Redirect(currentPageName + "#currency");
                    }
                    else
                    {
                        if (!boolSuccess)
                        {
                            currency.extractCurById(currId);
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit " + currency.curname + ". Please try again.</div>";
                            Response.Redirect(currentPageName + "#currency");
                        }
                        else
                        {
                            Session["EDITEDCMSCP"] = currId;
                            Session["NOCHANGE"] = 1;
                            Response.Redirect(currentPageName + "#currency");
                        }
                    }

                    break;
                default:
                    break;
            }
        }
    }

    protected void rptCurrency_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            itemCount += 1;

            int intCurrId = int.Parse(DataBinder.Eval(e.Item.DataItem, "CUR_ID").ToString());
            int intCurrActive = int.Parse(DataBinder.Eval(e.Item.DataItem, "CUR_ACTIVE").ToString());

            Label lblCurActive = (Label)e.Item.FindControl("lblCurActive");
            lblCurActive.Text = intCurrActive > 0 ? clsAdmin.CONSTACTIVEYES : clsAdmin.CONSTACTIVENO;

            HyperLink hypEdit = (HyperLink)e.Item.FindControl("hypEdit");
            hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.NavigateUrl = currentPageName + "?id=" + intCurrId + "#currency";

            LinkButton lnkbtnDelete = (LinkButton)e.Item.FindControl("lnkbtnDelete");
            lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteCurrency.Text") + "');";

            if (itemCount == 1)
            {
                LinkButton lnkbtnUp = (LinkButton)e.Item.FindControl("lnkbtnUp");
                lnkbtnUp.Enabled = false;
                lnkbtnUp.CssClass = "btnSPDisabled";
            }

            if (itemCount == itemTotal)
            {
                LinkButton lnkbtnDown = (LinkButton)e.Item.FindControl("lnkbtnDown");
                lnkbtnDown.Enabled = false;
                lnkbtnDown.CssClass = "btnSPDisabled";
            }
        }
    }

    protected void rptCurrency_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        clsCurrency currency = new clsCurrency();

        if (e.CommandName == "cmdDelete")
        {
            int intId = int.Parse(e.CommandArgument.ToString());

            int intRecordAffected = 0;

            intRecordAffected = currency.deleteCurById(intId);

            if (intRecordAffected == 1)
            {
                clsMis.resetListing();
                bindRptData();

                Response.Redirect(currentPageName + "#currency");
            }
        }
        else if (e.CommandName == "cmdUp")
        {
            DataRow[] foundRow;

            string strArg = e.CommandArgument.ToString();
            string[] strArgSplit = strArg.Split((char)'|');

            int intId = Convert.ToInt16(strArgSplit[0]);
            int intOrder = Convert.ToInt16(strArgSplit[1]);

            int intPrevOrder = 0;
            int intPrevId = 0;

            foundRow = dsCurrency.Tables[0].Select("CUR_ORDER < " + intOrder, "CUR_ORDER DESC");

            if (foundRow.Length > 0)
            {
                intPrevOrder = Convert.ToInt16(foundRow[0]["CUR_ORDER"]);
                intPrevId = Convert.ToInt16(foundRow[0]["CUR_ID"]);
            }

            currency.updateCurOrderById(intId, intPrevOrder);
            currency.updateCurOrderById(intPrevId, intOrder);

            bindRptData();
        }
        else if (e.CommandName == "cmdDown")
        {
            DataRow[] foundRow;

            string strArg = e.CommandArgument.ToString();
            string[] strArgSplit = strArg.Split((char)'|');

            int intId = Convert.ToInt16(strArgSplit[0]);
            int intOrder = Convert.ToInt16(strArgSplit[1]);

            int intNextOrder = 0;
            int intNextId = 0;

            foundRow = dsCurrency.Tables[0].Select("CUR_ORDER > " + intOrder, "CUR_ORDER ASC");

            if (foundRow.Length > 0)
            {
                intNextOrder = Convert.ToInt16(foundRow[0]["CUR_ORDER"]);
                intNextId = Convert.ToInt16(foundRow[0]["CUR_ID"]);
            }

            currency.updateCurOrderById(intId, intNextOrder);
            currency.updateCurOrderById(intNextId, intOrder);

            bindRptData();
        }
    }
    #endregion


    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        clsConfig config = new clsConfig();
        clsMis mis = new clsMis();
        int intRecordAffected = 0;

        Boolean boolSuccess = true;
        Boolean boolEdited = false;

        // Page Details
        string strNamePrefix = lblPrefix.Text;
        string strNamePageTitle = lblPageTitle.Text;
        string strNameKeyword = lblKeyword.Text;
        string strNameDesc = lblDesc.Text;

        string strNamePrefixValue = txtPrefix.Text.Trim();
        string strNamePageTitleValue = txtPageTitle.Text.Trim();
        string strNameKeywordValue = txtKeyword.Text.Trim();
        string strNameDescValue = txtDesc.Text.Trim();

        if (!config.isItemExist(strNamePrefix, clsConfig.CONSTGROUPPAGEDETAILS))
        {
            intRecordAffected = config.addItem(strNamePrefix, strNamePrefixValue, clsConfig.CONSTGROUPPAGEDETAILS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNamePrefix, strNamePrefixValue, clsConfig.CONSTGROUPPAGEDETAILS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPPAGEDETAILS, strNamePrefix, strNamePrefixValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNamePageTitle, clsConfig.CONSTGROUPPAGEDETAILS))
        {
            intRecordAffected = config.addItem(strNamePageTitle, strNamePageTitleValue, clsConfig.CONSTGROUPPAGEDETAILS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNamePageTitle, strNamePageTitleValue, clsConfig.CONSTGROUPPAGEDETAILS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPPAGEDETAILS, strNamePageTitle, strNamePageTitleValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNameKeyword, clsConfig.CONSTGROUPPAGEDETAILS))
        {
            intRecordAffected = config.addItem(strNameKeyword, strNameKeywordValue, clsConfig.CONSTGROUPPAGEDETAILS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNameKeyword, strNameKeywordValue, clsConfig.CONSTGROUPPAGEDETAILS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPPAGEDETAILS, strNameKeyword, strNameKeywordValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNameDesc, clsConfig.CONSTGROUPPAGEDETAILS))
        {
            intRecordAffected = config.addItem(strNameDesc, strNameDescValue, clsConfig.CONSTGROUPPAGEDETAILS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNameDesc, strNameDescValue, clsConfig.CONSTGROUPPAGEDETAILS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPPAGEDETAILS, strNameDesc, strNameDescValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Page Details

        //Quick Contact
        string strQuickContactTel = lblQuickContactTel.Text;
        string strQuickContactTelValue = txtQuickContactTel.Text.Trim();
        string strQuickContactEmail = lblQuickContactEmail.Text;
        string strQuickContactEmailValue = txtQuickContactEmail.Text.Trim();

        if (boolSuccess && !config.isItemExist(strQuickContactTel, clsConfig.CONSTGROUPQUICKCONTACT))
        {
            intRecordAffected = config.addItem(strQuickContactTel, strQuickContactTelValue, clsConfig.CONSTGROUPQUICKCONTACT, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strQuickContactTel, strQuickContactTelValue, clsConfig.CONSTGROUPQUICKCONTACT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPQUICKCONTACT, strQuickContactTel, strQuickContactTelValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strQuickContactEmail, clsConfig.CONSTGROUPQUICKCONTACT))
        {
            intRecordAffected = config.addItem(strQuickContactEmail, strQuickContactEmailValue, clsConfig.CONSTGROUPQUICKCONTACT, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strQuickContactEmail, strQuickContactEmailValue, clsConfig.CONSTGROUPQUICKCONTACT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPQUICKCONTACT, strQuickContactEmail, strQuickContactEmailValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        //End of Quick Contact


        //Website Settings
        string strEditorStyle = lblEditorStyle.Text;
        string strEditorStyleValue = txtEditorStyle.Text.Trim();

        if (boolSuccess && !config.isItemExist(strEditorStyle, clsConfig.CONSTGROUPWEBSITE))
        {
            intRecordAffected = config.addItem(strEditorStyle, strEditorStyleValue, clsConfig.CONSTGROUPWEBSITE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strEditorStyle, strEditorStyleValue, clsConfig.CONSTGROUPWEBSITE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPWEBSITE, strEditorStyle, strEditorStyleValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        string strCopyright = lblCopyright.Text;
        string strCopyrightValue = txtCopyright.Text.Trim();

        if (boolSuccess && !config.isItemExist(strCopyright, clsConfig.CONSTGROUPWEBSITE))
        {
            intRecordAffected = config.addItem(strCopyright, strCopyrightValue, clsConfig.CONSTGROUPWEBSITE, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strCopyright, strCopyrightValue, clsConfig.CONSTGROUPWEBSITE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPWEBSITE, strCopyright, strCopyrightValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        string strDefaultImage = lblDefaultImage.Text;
        string strDefaultImageValue = txtDefaultImage.Text.Trim();

        if (boolSuccess && !config.isItemExist(strDefaultImage, ""))
        {
            intRecordAffected = config.addItem(strDefaultImage, strDefaultImageValue, "", 1, int.Parse(Session["ADMID"].ToString()));
        }
        else if (!config.isExactSameSetData(strDefaultImage, strDefaultImageValue, ""))
        {
            intRecordAffected = config.updateItem("", strDefaultImage, strDefaultImageValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        string strThumbnailWidth = txtThumbnailWidth.Text.Trim();

        if (boolSuccess && !config.isItemExist(clsConfig.CONSTNAMETHUMBNAILWIDTH, clsConfig.CONSTGROUPWEBSITE))
        {
            intRecordAffected = config.addItem(clsConfig.CONSTNAMETHUMBNAILWIDTH, strThumbnailWidth, clsConfig.CONSTGROUPWEBSITE, 1, int.Parse(Session["ADMID"].ToString()));
        }
        else if (!config.isExactSameSetData(clsConfig.CONSTNAMETHUMBNAILWIDTH, strThumbnailWidth, clsConfig.CONSTGROUPWEBSITE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPWEBSITE, clsConfig.CONSTNAMETHUMBNAILWIDTH, strThumbnailWidth, int.Parse(Session["ADMID"].ToString()));
        }

        string strThumbnailHeight = txtThumbnailHeight.Text.Trim();

        if (boolSuccess && !config.isItemExist(clsConfig.CONSTNAMETHUMBNAILHEIGHT, clsConfig.CONSTGROUPWEBSITE))
        {
            intRecordAffected = config.addItem(clsConfig.CONSTNAMETHUMBNAILHEIGHT, strThumbnailHeight, clsConfig.CONSTGROUPWEBSITE, 1, int.Parse(Session["ADMID"].ToString()));
        }
        else if (!config.isExactSameSetData(clsConfig.CONSTNAMETHUMBNAILHEIGHT, strThumbnailHeight, clsConfig.CONSTGROUPWEBSITE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPWEBSITE, clsConfig.CONSTNAMETHUMBNAILHEIGHT, strThumbnailHeight, int.Parse(Session["ADMID"].ToString()));
        }
        //End of Website Settings

        //page default Settings
        if (trEventPage.Visible)
        {
            string strSelectedPageId = ddlEventPage.SelectedValue;
            string strConfName = lblEventPage.Text;
            //string strLabel
            if (boolSuccess && !config.isItemExist(strConfName, clsConfig.CONSTGROUPDEFAULTPAGE))
            {
                intRecordAffected = config.addItem(strConfName, strSelectedPageId, clsConfig.CONSTGROUPDEFAULTPAGE, 1, Convert.ToInt16(Session["ADMID"]));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }
            else if (!config.isExactSameSetData(strConfName, strSelectedPageId, clsConfig.CONSTGROUPDEFAULTPAGE))
            {
                intRecordAffected = config.updateItem(clsConfig.CONSTGROUPDEFAULTPAGE, strConfName, strSelectedPageId, Convert.ToInt16(Session["ADMID"]));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }
        }

        if (trProductPage.Visible)
        {
            string strSelectedPageId = ddlProductPage.SelectedValue;
            string strConfName = lblProductPage.Text;
            //string strLabel
            if (boolSuccess && !config.isItemExist(strConfName, clsConfig.CONSTGROUPDEFAULTPAGE))
            {
                intRecordAffected = config.addItem(strConfName, strSelectedPageId, clsConfig.CONSTGROUPDEFAULTPAGE, 1, Convert.ToInt16(Session["ADMID"]));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }
            else if (!config.isExactSameSetData(strConfName, strSelectedPageId, clsConfig.CONSTGROUPDEFAULTPAGE))
            {
                intRecordAffected = config.updateItem(clsConfig.CONSTGROUPDEFAULTPAGE, strConfName, strSelectedPageId, Convert.ToInt16(Session["ADMID"]));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }
        }

        //Product Settings
        List<Dictionary<string, string>> productSettinggs = new List<Dictionary<string, string>>()
        {
            new Dictionary<string, string>() {
                { "value", ddlProdDimension.SelectedValue },
                { "group", clsConfig.CONSTGROUPPRODUCT },
                { "name", lblProdDimension.Text }
            },
            new Dictionary<string, string>() {
                { "value", ddlProdWeight.SelectedValue },
                { "group", clsConfig.CONSTGROUPPRODUCT },
                { "name", lblProdWeight.Text }
            },
            new Dictionary<string, string>() {
                { "value", chkboxInventory.Checked ? clsConfig.CONSTDEFAULTTRUEVALUE : clsConfig.CONSTDEFAULTFALSEVALUE },
                { "group", clsConfig.CONSTGROUPPRODUCT },
                { "name", lblProdInventory.Text }
            },
            new Dictionary<string, string>() {
                { "value", chkboxProdDName.Checked ? clsConfig.CONSTDEFAULTTRUEVALUE : clsConfig.CONSTDEFAULTFALSEVALUE },
                { "group", clsConfig.CONSTGROUPPRODUCT },
                { "name", lblProdDName.Text }
            },
            new Dictionary<string, string>() {
                { "value", chkboxProdCode.Checked ? clsConfig.CONSTDEFAULTTRUEVALUE : clsConfig.CONSTDEFAULTFALSEVALUE },
                { "group", clsConfig.CONSTGROUPPRODUCT },
                { "name", lblProdCode.Text }
            },
            new Dictionary<string, string>() {
                { "value", chkboxProdSnapshot.Checked ? clsConfig.CONSTDEFAULTTRUEVALUE : clsConfig.CONSTDEFAULTFALSEVALUE },
                { "group", clsConfig.CONSTGROUPPRODUCT },
                { "name", lblProdSnapshot.Text }
            },
            new Dictionary<string, string>() {
                { "value", chkboxProdPrice.Checked ? clsConfig.CONSTDEFAULTTRUEVALUE : clsConfig.CONSTDEFAULTFALSEVALUE },
                { "group", clsConfig.CONSTGROUPPRODUCT },
                { "name", lblProdPrice.Text }
            },
            new Dictionary<string, string>() {
                { "value", txtMaxQtyPerProduct.Text },
                { "group", clsConfig.CONSTGROUPPRODUCT },
                { "name", lblMaxQtyPerProduct.Text }
            },
            new Dictionary<string, string>() {
                { "value", txtMaxQtyPerPromoProduct.Text },
                { "group", clsConfig.CONSTGROUPPRODUCT },
                { "name", lblMaxQtyPerPromoProduct.Text }
            },
            new Dictionary<string, string>() {
                { "value", chkboxRandomRelatedProdEnabled.Checked ? clsConfig.CONSTDEFAULTTRUEVALUE : clsConfig.CONSTDEFAULTFALSEVALUE },
                { "group", clsConfig.CONSTGROUPPRODUCT },
                { "name", clsConfig.CONSTNAMERANDOMRELATEDPRODENABLED }
            },
            new Dictionary<string, string>() {
                { "value", chkboxPriceRange.Checked ? clsConfig.CONSTDEFAULTTRUEVALUE : clsConfig.CONSTDEFAULTFALSEVALUE },
                { "group", clsConfig.CONSTGROUPPRODUCT },
                { "name", clsConfig.CONSTNAMEPRODUCTPRICERANGEFILTER }
            },
            new Dictionary<string, string>() {
                { "value", ddlProdDetailDisplayType.SelectedValue },
                { "group", clsConfig.CONSTGROUPPRODUCT },
                { "name", clsConfig.CONSTNAMEPRODUCTDETAILDISPLAYTYPE }
            },
            
        };
        int intProdSettingStatus = saveConfig(productSettinggs);
        if ((status)intProdSettingStatus == status.fail) boolSuccess = false;
        else if ((status)intProdSettingStatus == status.nochange) { boolSuccess = true; boolEdited = false; }
        else if ((status)intProdSettingStatus == status.success) { boolSuccess = true; boolEdited = true; }


        foreach (RepeaterItem item in rptProdSortOption.Items)
        {
            Label txtOrder = item.FindControl("txtOrder") as Label;
            HiddenField hdnListOrder = item.FindControl("hdnListOrder") as HiddenField;
            CheckBox chkboxAcitve = item.FindControl("chkboxActive") as CheckBox;
            Label lblName = item.FindControl("lblName") as Label;

            intRecordAffected = mis.updateItem("PRODUCT SORT OPTION", lblName.Text, null, !string.IsNullOrEmpty(hdnListOrder.Value) ? Convert.ToInt16(hdnListOrder.Value) : Convert.ToInt16(txtOrder.Text), chkboxAcitve.Checked ? 1 : 0, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; break; }
        }

        
        //End of Product Settings

        // Shipping
        string strNameShippingType = lblShippingType.Text;
        string strShippingType = ddlShippingType.SelectedValue;

        if (boolSuccess && !config.isItemExist(strNameShippingType, clsConfig.CONSTGROUPSHIPPING))
        {
            intRecordAffected = config.addItem(strNameShippingType, strShippingType, clsConfig.CONSTGROUPSHIPPING, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNameShippingType, strShippingType, clsConfig.CONSTGROUPSHIPPING))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPSHIPPING, strNameShippingType, strShippingType, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        //Shipping - Volumetric Weight
        string strNameVolumetricWeight = lblVolumetricWeight.Text;
        string strVolumetricWeight = chkboxVolumetricWeight.Checked ? "1" : "0";
        if (boolSuccess && !config.isItemExist(strNameVolumetricWeight, clsConfig.CONSTGROUPSHIPPING))
        {
            intRecordAffected = config.addItem(strNameVolumetricWeight, strVolumetricWeight, clsConfig.CONSTGROUPSHIPPING, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNameVolumetricWeight, strVolumetricWeight, clsConfig.CONSTGROUPSHIPPING))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPSHIPPING, strNameVolumetricWeight, strVolumetricWeight, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        // if Shipping Type updated, update Shipping Unit - for Display purpose
        if (boolEdited)
        {
            int intShippingType = !string.IsNullOrEmpty(strShippingType) ? Convert.ToInt16(strShippingType) : 0;
            string strShippingUnit = "kg";

            if (intShippingType == clsAdmin.CONSTSHIPPINGTYPE3)
            {
                strShippingUnit = config.currency;
            }

            config.updateItem(clsConfig.CONSTGROUPSHIPPING, clsConfig.CONSTNAMESHIPPINGUNIT, strShippingUnit, int.Parse(Session["ADMID"].ToString()));
        }

        if (trDeliveryWeek.Visible && trDeliveryTime.Visible)
        {
            string[] week = chklistDeliveryWeek.Items.Cast<ListItem>()
                              .Select(x => (x.Selected ? "1" : "0"))
                              .ToArray();
            if (boolSuccess)
            {
                boolSuccess = config.saveDeliveryWeek(week, int.Parse(Session["ADMID"].ToString()));
            }

            string times = hdnDeliveryTime.Value;
            if (boolSuccess)
            {
                boolSuccess = config.saveDeliveryTime(times, (chkboxDeliveryTimeActive.Checked ? "1" : "0"), int.Parse(Session["ADMID"].ToString()));
            }




        }

        // End of Shipping

        // Invoice Settings
        string strInvoiceImg = lblInvoiceLogo.Text;
        string strCompanyName = lblCompanyName.Text;
        string strROC = lblROC.Text;
        string strCompanyAddress = lblCompanyAddress.Text;
        string strCompanyTel = lblCompanyTel.Text;
        string strCompanyEmail = lblCompanyEmail.Text;
        string strCompanyWebsite = lblCompanyWebsite.Text;
        string strInvoiceRemarks = lblInvoiceRemarks.Text;
        string strInvoiceRemarksPdf = lblInvoiceRemarksPdf.Text;
        string strInvoiceSignature = lblInvoiceSignature.Text;
        string strInvoiceSignaturePdf = lblInvoiceSignaturePdf.Text;
        string strPaymentType = lblPaymentType.Text;
        string strShipCom = lblShippingCom.Text;
        string strInvoicePaymentEmail = lblInvoicePayment.Text;
        string strInvoiceProcessingEmail = lblInvoiceProcessing.Text;
        string strInvoiceDeliveryEmail = lblInvoiceDelivery.Text;

        string strInvoiceImgValue;
        string strCompanyNameValue = txtCompanyName.Text.Trim();
        string strROCValue = txtROC.Text.Trim();
        string strCompanyAddressValue = txtCompanyAddress.Text.Trim();
        string strCompanyTelValue = txtCompanyTel.Text.Trim();
        string strCompanyEmailValue = txtCompanyEmail.Text.Trim();
        string strCompanyWebsiteValue = txtCompanyWebsite.Text.Trim();
        string strInvoiceRemarksValue = txtInvoiceRemarks.Text.Trim();
        string strInvoiceRemarksPdfValue = txtInvoiceRemarksPdf.Text.Trim();
        string strInvoiceSignatureValue = txtInvoiceSig.Text.Trim();
        string strInvoiceSignaturePdfValue = txtInvoiceSigPdf.Text.Trim();
        string strPaymentTypeValue = ddlPaymentType.SelectedValue;
        string strShipComValue = ddlShippingCompany.SelectedValue;
        string strInvoicePaymentEmailValue = chkboxPayment.Checked ? clsConfig.CONSTTRUE : clsConfig.CONSTFALSE;
        string strInvoiceProcessingEmailValue = chkboxProcessing.Checked ? clsConfig.CONSTTRUE : clsConfig.CONSTFALSE;
        string strInvoiceDeliveryEmailValue = chkboxDelivery.Checked ? clsConfig.CONSTTRUE : clsConfig.CONSTFALSE;

        if (fileInvoiceImage.HasFile)
        {
            try
            {
                HttpPostedFile postedFile = fileInvoiceImage.PostedFile;
                string fileName = Path.GetFileName(postedFile.FileName);
                string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCMSFOLDER.ToString() + "/" + clsAdmin.CONSTIMAGESFOLDER.ToString() + "/";
                clsMis.createFolder(uploadPath);

                string newFileName = clsMis.GetUniqueFilename(fileName, uploadPath);
                postedFile.SaveAs(uploadPath + newFileName);
                strInvoiceImgValue = newFileName;
            }
            catch (Exception ex)
            {
                clsLog.logErroMsg(ex.Message.ToString());
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />" + ex.Message.ToString() + "</div>";

                throw ex;
            }
        }
        else
        {
            strInvoiceImgValue = hdnInvoiceImage.Value;
        }

        if (boolSuccess && !config.isItemExist(strCompanyName, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem(strCompanyName, strCompanyNameValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strCompanyName, strCompanyNameValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPINVOICE, strCompanyName, strCompanyNameValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strROC, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem(strROC, strROCValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strROC, strROCValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPINVOICE, strROC, strROCValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strCompanyAddress, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem(strCompanyAddress, strCompanyAddressValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strCompanyAddress, strCompanyAddressValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPINVOICE, strCompanyAddress, strCompanyAddressValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strCompanyTel, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem(strCompanyTel, strCompanyTelValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strCompanyTel, strCompanyTelValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPINVOICE, strCompanyTel, strCompanyTelValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strCompanyEmail, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem(strCompanyEmail, strCompanyEmailValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strCompanyEmail, strCompanyEmailValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPINVOICE, strCompanyEmail, strCompanyEmailValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strCompanyWebsite, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem(strCompanyWebsite, strCompanyWebsiteValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strCompanyWebsite, strCompanyWebsiteValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPINVOICE, strCompanyWebsite, strCompanyWebsiteValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strInvoiceImg, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem(strInvoiceImg, strInvoiceImgValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strInvoiceImg, strInvoiceImgValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPINVOICE, strInvoiceImg, strInvoiceImgValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strInvoiceRemarks, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem2(strInvoiceRemarks, strInvoiceRemarksValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strInvoiceRemarks, strInvoiceRemarksValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPINVOICE, strInvoiceRemarks, strInvoiceRemarksValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strInvoiceRemarksPdf, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem2(strInvoiceRemarksPdf, strInvoiceRemarksPdfValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strInvoiceRemarksPdf, strInvoiceRemarksPdfValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPINVOICE, strInvoiceRemarksPdf, strInvoiceRemarksPdfValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strInvoiceSignature, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem2(strInvoiceSignature, strInvoiceSignatureValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strInvoiceSignature, strInvoiceSignatureValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPINVOICE, strInvoiceSignature, strInvoiceSignatureValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strInvoiceSignaturePdf, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem2(strInvoiceSignaturePdf, strInvoiceSignaturePdfValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strInvoiceSignaturePdf, strInvoiceSignaturePdfValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPINVOICE, strInvoiceSignaturePdf, strInvoiceSignaturePdfValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strPaymentType, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem(strPaymentType, strPaymentTypeValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strPaymentType, strPaymentTypeValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPINVOICE, strPaymentType, strPaymentTypeValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strShipCom, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem(strShipCom, strShipComValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strShipCom, strShipComValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPINVOICE, strShipCom, strShipComValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strInvoicePaymentEmail, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem(strInvoicePaymentEmail, strInvoicePaymentEmailValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strInvoicePaymentEmail, strInvoicePaymentEmailValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPINVOICE, strInvoicePaymentEmail, strInvoicePaymentEmailValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strInvoiceProcessingEmail, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem(strInvoiceProcessingEmail, strInvoiceProcessingEmailValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strInvoiceProcessingEmail, strInvoiceProcessingEmailValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPINVOICE, strInvoiceProcessingEmail, strInvoiceProcessingEmailValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strInvoiceDeliveryEmail, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.addItem(strInvoiceDeliveryEmail, strInvoiceDeliveryEmailValue, clsConfig.CONSTGROUPINVOICE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strInvoiceDeliveryEmail, strInvoiceDeliveryEmailValue, clsConfig.CONSTGROUPINVOICE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPINVOICE, strInvoiceDeliveryEmail, strInvoiceDeliveryEmailValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Invoice Settings

        //Other Settings
        string strListingSize = lblListingSize.Text;
        string strSessionTimeout = lblSessionTimeout.Text;
        string strFriendlyUrl = lblFriendlyURL.Text;
        string strMobileView = lblMobileView.Text;
        string strDeliveryMsg = lblDeliveryMsg.Text;

        string strListingSizeValue = txtListingSize.Text.Trim();
        string strSessionTimeoutValue = txtSessionTimeout.Text.Trim();
        string strFriendlyUrlValue = chkboxFriendlyURL.Checked ? clsConfig.CONSTDEFAULTTRUEVALUE : clsConfig.CONSTDEFAULTFALSEVALUE;
        string strMobileViewValue = chkboxMobileView.Checked ? clsConfig.CONSTDEFAULTTRUEVALUE : clsConfig.CONSTDEFAULTFALSEVALUE;
        string strDeliveryMsgValue = chkboxDeliveryMsg.Checked ? clsConfig.CONSTDEFAULTTRUEVALUE : clsConfig.CONSTDEFAULTFALSEVALUE;

        if (boolSuccess && !config.isItemExist(strListingSize, clsConfig.CONSTGROUPOTHERSETTINGS))
        {
            intRecordAffected = config.addItem(strListingSize, strListingSizeValue, clsConfig.CONSTGROUPOTHERSETTINGS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strListingSize, strListingSizeValue, clsConfig.CONSTGROUPOTHERSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPOTHERSETTINGS, strListingSize, strListingSizeValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strSessionTimeout, clsConfig.CONSTGROUPOTHERSETTINGS))
        {
            intRecordAffected = config.addItem(strSessionTimeout, strSessionTimeoutValue, clsConfig.CONSTGROUPOTHERSETTINGS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strSessionTimeout, strSessionTimeoutValue, clsConfig.CONSTGROUPOTHERSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPOTHERSETTINGS, strSessionTimeout, strSessionTimeoutValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strFriendlyUrl, clsConfig.CONSTGROUPOTHERSETTINGS))
        {
            intRecordAffected = config.addItem(strFriendlyUrl, strFriendlyUrlValue, clsConfig.CONSTGROUPOTHERSETTINGS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strFriendlyUrl, strFriendlyUrlValue, clsConfig.CONSTGROUPOTHERSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPOTHERSETTINGS, strFriendlyUrl, strFriendlyUrlValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strMobileView, clsConfig.CONSTGROUPOTHERSETTINGS))
        {
            intRecordAffected = config.addItem(strMobileView, strMobileViewValue, clsConfig.CONSTGROUPOTHERSETTINGS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strMobileView, strMobileViewValue, clsConfig.CONSTGROUPOTHERSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPOTHERSETTINGS, strMobileView, strMobileViewValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strDeliveryMsg, clsConfig.CONSTGROUPOTHERSETTINGS))
        {
            intRecordAffected = config.addItem(strDeliveryMsg, strDeliveryMsgValue, clsConfig.CONSTGROUPOTHERSETTINGS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strDeliveryMsg, strDeliveryMsgValue, clsConfig.CONSTGROUPOTHERSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPOTHERSETTINGS, strDeliveryMsg, strDeliveryMsgValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        //End of Other Settings

        //Enquiry Setting
        string strNameEnquiryDesc = lblEnquiryDesc.Text;
        string strNameEnquiryTitle = lblEnquiryTitle.Text;
        string strNameEnquiryRecaptcha = lblEnquiryRecaptcha.Text;
        string strNameRecaptchaSiteKey = lblRecaptchaSiteKey.Text;
        string strNameRecaptchaSecretKey = lblRecaptchaSecretKey.Text.Trim();
        string strNameRecipientEnable = clsConfig.CONSTNAMEENABLEENQUIRYRECIPIENT;

        string strValueEnquiryDesc = txtEnquiryDesc.Text.Trim();
        string strValueEnquiryTitle = txtEnquiryTitle.Text.Trim();
        string strValueEnquiryRecaptcha = (chkboxEnquiryRecaptcha.Checked) ? "1" : "0";
        string strValueRecaptchaSiteKey = txtRecaptchaSiteKey.Text;
        string strValueRecaptchaSecretKey = txtRecaptchaSecretKey.Text.Trim();
        string strValueRecipientEnable = (chkboxEnquiryRecipient.Checked) ? "1" : "0";

        if (!config.isItemExist(strNameEnquiryTitle, clsConfig.CONSTGROUPENQUIRYSETTINGS))
        {
            intRecordAffected = config.addItem(strNameEnquiryTitle, strValueEnquiryTitle, clsConfig.CONSTGROUPENQUIRYSETTINGS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNameEnquiryTitle, strValueEnquiryTitle, clsConfig.CONSTGROUPENQUIRYSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPENQUIRYSETTINGS, strNameEnquiryTitle, strValueEnquiryTitle, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNameEnquiryDesc, clsConfig.CONSTGROUPENQUIRYSETTINGS))
        {
            intRecordAffected = config.addItem2(strNameEnquiryDesc, strValueEnquiryDesc, clsConfig.CONSTGROUPENQUIRYSETTINGS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strNameEnquiryDesc, strValueEnquiryDesc, clsConfig.CONSTGROUPENQUIRYSETTINGS))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPENQUIRYSETTINGS, strNameEnquiryDesc, strValueEnquiryDesc, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNameEnquiryRecaptcha, clsConfig.CONSTGROUPENQUIRYSETTINGS))
        {
            intRecordAffected = config.addItem(strNameEnquiryRecaptcha, strValueEnquiryRecaptcha, clsConfig.CONSTGROUPENQUIRYSETTINGS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strNameEnquiryRecaptcha, strValueEnquiryRecaptcha, clsConfig.CONSTGROUPENQUIRYSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPENQUIRYSETTINGS, strNameEnquiryRecaptcha, strValueEnquiryRecaptcha, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNameRecaptchaSiteKey, clsConfig.CONSTGROUPGOOGLERECAPTCHASETTINGS))
        {
            intRecordAffected = config.addItem(strNameRecaptchaSiteKey, strValueRecaptchaSiteKey, clsConfig.CONSTGROUPGOOGLERECAPTCHASETTINGS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strNameRecaptchaSiteKey, strValueRecaptchaSiteKey, clsConfig.CONSTGROUPGOOGLERECAPTCHASETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPGOOGLERECAPTCHASETTINGS, strNameRecaptchaSiteKey, strValueRecaptchaSiteKey, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNameRecaptchaSecretKey, clsConfig.CONSTGROUPGOOGLERECAPTCHASETTINGS))
        {
            intRecordAffected = config.addItem(strNameRecaptchaSecretKey, strValueRecaptchaSecretKey, clsConfig.CONSTGROUPGOOGLERECAPTCHASETTINGS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strNameRecaptchaSecretKey, strValueRecaptchaSecretKey, clsConfig.CONSTGROUPGOOGLERECAPTCHASETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPGOOGLERECAPTCHASETTINGS, strNameRecaptchaSecretKey, strValueRecaptchaSecretKey, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNameRecipientEnable, clsConfig.CONSTGROUPENQUIRYSETTINGS))
        {
            intRecordAffected = config.addItem(strNameRecipientEnable, strValueRecipientEnable, clsConfig.CONSTGROUPENQUIRYSETTINGS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strNameRecipientEnable, strValueRecipientEnable, clsConfig.CONSTGROUPENQUIRYSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPENQUIRYSETTINGS, strNameRecipientEnable, strValueRecipientEnable, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        if (chkboxEnquiryRecipient.Checked)
        {
            clsEnquiryRecipient ctrlRecipient = new clsEnquiryRecipient();
            List<Dictionary<string, string>> recipients = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<List<Dictionary<string, string>>>(hdnEnquiryRecipient.Value);
            foreach (Dictionary<string, string> recipient in recipients)
            {
                ctrlRecipient.reset();
                ctrlRecipient.id = Convert.ToInt16(recipient["ID"]);
                ctrlRecipient.order = Convert.ToInt16(recipient["order"]);
                ctrlRecipient.name = Convert.ToString(recipient["name"]);
                ctrlRecipient.email = Convert.ToString(recipient["email"]);

                bool isDeleted = recipient["deleted"] == "1";

                if (ctrlRecipient.id == 0 && !isDeleted)
                {
                    ctrlRecipient.add();
                }
                else if (ctrlRecipient.id != 0 && isDeleted)
                {
                    ctrlRecipient.delete();
                }
                else
                {
                    ctrlRecipient.update();
                }
            }
        }



        // webteq logo
        string strNameRightClickEnable = lblRightClickEnable.Text;
        string strValueRightClickEnable = ddlRightClickEnable.SelectedValue;

        if (boolSuccess && !config.isItemExist(strNameRightClickEnable, clsConfig.CONSTGROUPWEBSITE))
        {
            intRecordAffected = config.addItem(strNameRightClickEnable, strValueRightClickEnable, clsConfig.CONSTGROUPWEBSITE, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNameRightClickEnable, strValueRightClickEnable, clsConfig.CONSTGROUPWEBSITE))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPWEBSITE, strNameRightClickEnable, strValueRightClickEnable, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }


        // event option update
        if (tblEventSetting.Visible && boolSuccess)
        {
            foreach (RepeaterItem item in rptEventSortOption.Items)
            {
                Label txtOrder = item.FindControl("txtOrder") as Label;
                CheckBox chkboxAcitve = item.FindControl("chkboxActive") as CheckBox;
                Label lblName = item.FindControl("lblName") as Label;

                intRecordAffected = mis.updateItem("EVENT SORT OPTION", lblName.Text, null, Convert.ToInt16(txtOrder.Text), chkboxAcitve.Checked ? 1 : 0, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; break; }
            }
        }

        #region Multiple Language
        if (Session[clsAdmin.CONSTPROJECTLANG] != null)
        {
            string[] languages = Session[clsAdmin.CONSTPROJECTLANG].ToString()
                                    .TrimStart(clsAdmin.CONSTDEFAULTSEPERATOR)
                                    .TrimEnd(clsAdmin.CONSTDEFAULTSEPERATOR)
                                    .Split(clsAdmin.CONSTDEFAULTSEPERATOR);

            for (int i = 0; i < languages.Length; i++)
            {
                RepeaterItem item = rptPageDetail.Items[i];

                TextBox txtPrefixLang = item.FindControl("txtPrefix") as TextBox;
                TextBox txtPageTitleLang = item.FindControl("txtPageTitle") as TextBox;
                TextBox txtKeywordLang = item.FindControl("txtKeyword") as TextBox;
                TextBox txtDescLang = item.FindControl("txtDesc") as TextBox;

                List<Dictionary<string, string>> confList = new List<Dictionary<string, string>>()
                {
                    new Dictionary<string, string>() {
                        { "value", txtPrefixLang.Text },
                        { "group", clsConfig.CONSTGROUPPAGEDETAILSLANG },
                        { "lang", languages[i] },
                        { "name", clsConfig.CONSTNAMEPAGETITLEPREFIX }
                    },
                    new Dictionary<string, string>() {
                        { "value", txtPageTitleLang.Text },
                        { "group", clsConfig.CONSTGROUPPAGEDETAILSLANG },
                        { "lang", languages[i] },
                        { "name", clsConfig.CONSTNAMEPAGETITLE }
                    },
                    new Dictionary<string, string>() {
                        { "value", txtKeywordLang.Text },
                        { "group", clsConfig.CONSTGROUPPAGEDETAILSLANG },
                        { "lang", languages[i] },
                        { "name", clsConfig.CONSTNAMEKEYWORD }
                    },
                    new Dictionary<string, string>() {
                        { "value", txtDescLang.Text },
                        { "group", clsConfig.CONSTGROUPPAGEDETAILSLANG },
                        { "lang", languages[i] },
                        { "name", clsConfig.CONSTNAMEDESCRIPTION }
                    },
                };
                int langStatus = saveConfig(confList);
                boolSuccess = (status)langStatus == status.success || (status)langStatus == status.nochange;

            }

        }
        #endregion Multiple Language


        //Currency Conversion
        //foreach (RepeaterItem item in rptCurrency.Items)
        //{
        //    int intCurrId = 0;
        //    string strCurrName = "";
        //    string strCurrValue = "";
        //    decimal decConvRate = Convert.ToDecimal("0.00");

        //    Label lblDefaultCurrency = (Label)item.FindControl("lblDefaultCurrency");
        //    HiddenField hdnCurrId = (HiddenField)item.FindControl("hdnCurrId");
        //    Label lblCurrRate = (Label)item.FindControl("lblCurrRate");
        //    TextBox txtCurrRate = (TextBox)item.FindControl("txtCurrRate");

        //    intCurrId = int.Parse(hdnCurrId.Value);
        //    strCurrName = lblDefaultCurrency.Text;
        //    strCurrValue = lblCurrRate.Text;
        //    decConvRate = Convert.ToDecimal(txtCurrRate.Text);

        //    clsCurrency curr = new clsCurrency();

        //    if (!curr.isExactSameDataSet(intCurrId, strCurrName, strCurrValue, decConvRate))
        //    {
        //        intRecordAffected = curr.updateCurrencyById(intCurrId, decConvRate, int.Parse(Session["ADMID"].ToString()));

        //        if (intRecordAffected == 1) { boolEdited = true; }
        //        else { boolSuccess = false; }
        //    }
        //}
        //End of Currency Conversion

        if (!boolSuccess)
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to update control panel. Please try again.</div>";

            Response.Redirect(currentPageName);
        }
        else
        {
            if (boolEdited)
            {
                Session["EDITEDCMSCP"] = 1;
            }
            else
            {
                Session["EDITEDCMSCP"] = 1;
                Session["NOCHANGE"] = 1;
            }

            Response.Redirect(currentPageName);
        }
    }
    #endregion

    #region "Methods"
    public void fill()
    {
       

        if (!string.IsNullOrEmpty(Request["id"]))
        {
            int intTryParse;
            if (int.TryParse(Request["id"], out intTryParse))
            {
                shipCompId = intTryParse;
            }
            else
            {
                shipCompId = 0;
            }
        }

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                mode = 2;
            }

            switch (mode)
            {
                case 1:
                    break;
                case 2:
                    lnkbtnAddShipComp.Text = "Save";
                    lnkbtnAddShipMethod.Text = "Save";
                    break;
                default:
                    break;
            }

            setPageProperties();
        }

        if (ddlShippingType.SelectedValue == clsAdmin.CONSTSHIPPINGTYPE5.ToString())
        {
            trShippingComp.Visible = true;
            trShipMethod.Visible = true;
        }
        registerCKEditorScript();

    }

    protected void registerCKEditorScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR")))
        {
            int intFullPath = Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFULLPATH]) == 1 ? clsAdmin.CONSTFULLPATHACTIVE : clsAdmin.CONSTFULLPATHDEFAULT;
            string strJS = "<script type=\"text/javascript\">";
            if (txtInvoiceRemarks.Visible)
            {
                strJS += "CKEDITOR.replace('" + txtInvoiceRemarks.ClientID + "'," +
                         "{" +
                         "width:'94%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "'," +
                         "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");";
            }
            if (txtInvoiceRemarksPdf.Visible)
            {
                strJS += "CKEDITOR.replace('" + txtInvoiceRemarksPdf.ClientID + "'," +
                          "{" +
                          "width:'94%'," +
                          "height:'300'," +
                          "toolbar: 'User3'," +
                          "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "'," +
                          "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");";
            }
            if (txtEnquiryDesc.Visible)
            {
                strJS += "CKEDITOR.replace('" + txtEnquiryDesc.ClientID + "'," +
                     "{" +
                     "width:'94%'," +
                     "height:'300'," +
                     "toolbar: 'User3'," +
                     "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "'," +
                     "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                      "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                      "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                      "}" +
                      ");";
            }
            if (txtInvoiceSig.Visible)
            {
                strJS += "CKEDITOR.replace('" + txtInvoiceSig.ClientID + "'," +
                     "{" +
                     "width:'94%'," +
                     "height:'300'," +
                     "toolbar: 'User3'," +
                     "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "'," +
                     "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                     "}" +
                     ");";
            }
            if (txtInvoiceSigPdf.Visible)
            {
                strJS += "CKEDITOR.replace('" + txtInvoiceSigPdf.ClientID + "'," +
                    "{" +
                    "width:'94%'," +
                    "height:'300'," +
                    "toolbar: 'User3'," +
                    "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "'," +
                    "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                    "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                    "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                    "}" +
                    ");";
            }
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR", strJS, false);
        }
    }

    protected void setPageProperties()
    {
        clsMis mis = new clsMis();

        if (Session[clsAdmin.CONSTPROJECTFRIENDLYURL] != null && clsAdmin.CONSTPROJECTFRIENDLYURL != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFRIENDLYURL]) == 0)
            {
                trFriendlyURL.Visible = false;
            }
        }

        if (Session[clsAdmin.CONSTPROJECTMOBILEVIEW] != null && clsAdmin.CONSTPROJECTMOBILEVIEW != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTMOBILEVIEW]) == 0)
            {
                trMobileView.Visible = false;
            }
        }

        if (Session[clsAdmin.CONSTPROJECTDELIVERYMSG] != null && clsAdmin.CONSTPROJECTDELIVERYMSG != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTDELIVERYMSG]) == 0)
            {
                trDeliveryMsg.Visible = false;
            }
        }

        if (Session[clsAdmin.CONSTPROJECTTYPE] != null && Session[clsAdmin.CONSTPROJECTTYPE] != "")
        {
            int intType = int.Parse(Session[clsAdmin.CONSTPROJECTTYPE].ToString());

            if (intType == clsAdmin.CONSTTYPEeCATALOG || intType == clsAdmin.CONSTTYPEeCATALOGLITE || intType == clsAdmin.CONSTTYPEeCATALOGLITEEVENTMANAGER || intType == clsAdmin.CONSTTYPEeCATALOGEVENTMANAGER || intType == clsAdmin.CONSTTYPEeSHOP || intType == clsAdmin.CONSTTYPEeSHOPLITE || intType == clsAdmin.CONSTTYPEESHOPINVOICE || intType == clsAdmin.CONSTTYPEESHOPLITEINVOICE || intType == clsAdmin.CONSTTYPEeSHOPEVENTMANAGER)
            {
                trProductSettings.Visible = true;

                if (Session[clsAdmin.CONSTPROJECTMULTIPLECURRENCY] != null && Session[clsAdmin.CONSTPROJECTMULTIPLECURRENCY] != "")
                {
                    if (Session[clsAdmin.CONSTPROJECTMULTIPLECURRENCY].ToString() == "1")
                    {
                        trCurrencyConversion.Visible = true;
                    }
                }

                DataSet dsProdSortOption = mis.getListByListGrp("PRODUCT SORT OPTION", 0);
                if (dsProdSortOption != null)
                {
                    DataView dvProdSortOption = new DataView(dsProdSortOption.Tables[0]);
                    dvProdSortOption.Sort = "LIST_ORDER ASC";

                    rptProdSortOption.DataSource = dvProdSortOption;
                    rptProdSortOption.DataBind();
                }
            }

            if (intType == clsAdmin.CONSTTYPEeSHOP || intType == clsAdmin.CONSTTYPEeSHOPLITE || intType == clsAdmin.CONSTTYPEESHOPINVOICE || intType == clsAdmin.CONSTTYPEESHOPLITEINVOICE || intType == clsAdmin.CONSTTYPEeSHOPEVENTMANAGER)
            {
                trShippingType.Visible = true;
            }

            if (intType == clsAdmin.CONSTTYPEESHOPINVOICE || intType == clsAdmin.CONSTTYPEESHOPLITEINVOICE)
            {
                trInvoiceSettings.Visible = true;
            }
        }

        if (Session[clsAdmin.CONSTPROJECTINVENTORY] != null && clsAdmin.CONSTPROJECTINVENTORY != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTINVENTORY]) == 0)
            {
                trInventory.Visible = false;
            }
        }
        if (Session[clsAdmin.CONSTPROJECTENQUIRYFIELD] != null && clsAdmin.CONSTPROJECTENQUIRYFIELD != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTENQUIRYFIELD]) == 0)
            {
                trEnquiryField.Visible = false;
            }
        }
        lblPrefix.Text = clsConfig.CONSTNAMEPAGETITLEPREFIX;
        lblPageTitle.Text = clsConfig.CONSTNAMEPAGETITLE;
        lblKeyword.Text = clsConfig.CONSTNAMEKEYWORD;
        lblDesc.Text = clsConfig.CONSTNAMEDESCRIPTION;

        lblQuickContactTel.Text = clsConfig.CONSTNAMEQUICKCONTACTTEL;
        lblQuickContactEmail.Text = clsConfig.CONSTNAMEQUICKCONTACTEML;
        //hypQuickContactEmailSameAs.Text = "same as " + clsConfig.CONSTNAMEDEFAULTRECIPIENT;
        //hypQuickContactEmailSameAs.ToolTip = "same as " + clsConfig.CONSTNAMEDEFAULTRECIPIENT;
        //hypQuickContactEmailSameAs.NavigateUrl = "javascript:void(0);";
        //hypQuickContactEmailSameAs.Attributes.Add("onclick", "copyValue('" + txtDefaultRecipient.ClientID + "','" + txtQuickContactEmail.ClientID + "');");



        lblDefaultImagePath.Text = Session[clsAdmin.CONSTPROJECTUSERVIEWURL].ToString() + "/";
        lblDefaultImage.Text = clsConfig.CONSTNAMEGROUPIMG;
        lblCopyright.Text = clsConfig.CONSTNAMECOPYRIGHT;
        lblEditorStyle.Text = clsConfig.CONSTNAMEEDITORSTYLE;

        lblProductPage.Text = clsConfig.CONSTNAMEPRODUCTPAGE;
        lblEventPage.Text = clsConfig.CONSTNAMEEVENTPAGE;

        lblProdDimension.Text = clsConfig.CONSTNAMEDIMENSIONUNIT;
        lblProdWeight.Text = clsConfig.CONSTNAMEWEIGHTUNIT;
        lblProdInventory.Text = clsConfig.CONSTNAMEPRODINVENTORY;
        lblProdDName.Text = clsConfig.CONSTNAMEPRODDNAME;
        lblProdCode.Text = clsConfig.CONSTNAMEPRODCODE;
        lblProdSnapshot.Text = clsConfig.CONSTNAMEPRODSNAPSHOT;
        lblProdPrice.Text = clsConfig.CONSTNAMEPRODPRICE;

        lblInvoiceLogo.Text = clsConfig.CONSTNAMEINVOICELOGO;
        lblCompanyName.Text = clsConfig.CONSTNAMECOMPANYNAME;
        lblROC.Text = clsConfig.CONSTNAMEROC;
        lblCompanyAddress.Text = clsConfig.CONSTNAMECOMPANYADDRESS;
        lblCompanyTel.Text = clsConfig.CONSTNAMECOMPANYTEL;
        lblCompanyEmail.Text = clsConfig.CONSTNAMECOMPANYEMAIL;
        lblCompanyWebsite.Text = clsConfig.CONSTNAMECOMPANYWEBSITE;
        lblInvoiceRemarks.Text = clsConfig.CONSTNAMEINVOICEREMARKS;
        lblInvoiceRemarksPdf.Text = clsConfig.CONSTNAMEINVOICEREMARKSPDF;
        lblInvoiceSignature.Text = clsConfig.CONSTNAMEINVOICESIGNATURE;
        lblInvoiceSignaturePdf.Text = clsConfig.CONSTNAMEINVOICESIGNATUREPDF;
        lblPaymentType.Text = clsConfig.CONSTNAMEPAYMENTTYPE;
        lblShippingCom.Text = clsConfig.CONSTNAMESHIPPINGCOMPANY;
        lblInvoicePayment.Text = clsConfig.CONSTNAMEINVOICEPAYMENTEMAIL;
        lblInvoiceProcessing.Text = clsConfig.CONSTNAMEINVOICEPROCESSINGEMAIL;
        lblInvoiceDelivery.Text = clsConfig.CONSTNAMEINVOICEDELIVERYEMAIL;

        lblListingSize.Text = clsConfig.CONSTNAMELISTINGSIZE;
        lblSessionTimeout.Text = clsConfig.CONSTNAMESESSIONTIMEOUT;
        lblFriendlyURL.Text = clsConfig.CONSTNAMEFRIENDLYURL;
        lblMobileView.Text = clsConfig.CONSTNAMEMOBILEVIEW;
        lblDeliveryMsg.Text = clsConfig.CONSTNAMEDELIVERYMSG;

        lblEnquiryDesc.Text = clsConfig.CONSTNAMEENQUIRYDESC; // add by sh.chong - enquiry setting 22Jul2016
        lblEnquiryTitle.Text = clsConfig.CONSTNAMEENQUIRYTITLE; // add by sh.chong - enquiry setting 22Jul2016
        lblEnquiryRecaptcha.Text = clsConfig.CONSTNAMEENQUIRYRECAPTCHA;
        lblRecaptchaSiteKey.Text = clsConfig.CONSTNAMERECAPTCHASITEKEY;
        lblRecaptchaSecretKey.Text = clsConfig.CONSTNAMERECAPTCHASECRETKEY;
        lblEnquiryRecipient.Text = "Enquiry Type";

        lblRightClickEnable.Text = clsConfig.CONSTNAMERIGHTCLICKENABLE;
        lblMaxQtyPerProduct.Text = clsConfig.CONSTNAMEMAXQTYPERPRODUCT;
        lblMaxQtyPerPromoProduct.Text = clsConfig.CONSTNAMEMAXQTYPERPROMOPRODUCT;


        lblEventSortOption.Text = "Sorting Option";

        DataSet dsPayType = mis.getListByListGrp("PAYMENT TYPE", 1);
        DataView dvPayType = new DataView(dsPayType.Tables[0]);

        dvPayType.RowFilter = "LIST_ACTIVE = 1";
        dvPayType.Sort = "LIST_ORDER ASC";

        ddlPaymentType.DataSource = dvPayType;
        ddlPaymentType.DataTextField = "LIST_NAME";
        ddlPaymentType.DataValueField = "LIST_VALUE";
        ddlPaymentType.DataBind();

        ListItem ddlPaymentTypeDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "0");
        ddlPaymentType.Items.Insert(0, ddlPaymentTypeDefaultItem);

        DataSet dsShipCom = mis.getListByListGrp("SHIPPING COMPANY", 1);
        DataView dvShipCom = new DataView(dsShipCom.Tables[0]);

        dvShipCom.RowFilter = "LIST_ACTIVE = 1";
        dvShipCom.Sort = "LIST_ORDER ASC";

        ddlShippingCompany.DataSource = dvShipCom;
        ddlShippingCompany.DataTextField = "LIST_NAME";
        ddlShippingCompany.DataValueField = "LIST_VALUE";
        ddlShippingCompany.DataBind();

        ListItem ddlShippingCompanyDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "0");
        ddlShippingCompany.Items.Insert(0, ddlShippingCompanyDefaultItem);


        // add by sh.chong 29 Apr 2015 - to trace product and page using which template
        int intProjectType = int.Parse(Session[clsAdmin.CONSTPROJECTTYPE].ToString());
        int[] intEshopEcatalog = {
                                         clsAdmin.CONSTTYPEeSHOP, clsAdmin.CONSTTYPEeSHOPLITE, clsAdmin.CONSTTYPEESHOPINVOICE, clsAdmin.CONSTTYPEESHOPLITEINVOICE,
                                         clsAdmin.CONSTTYPEeSHOPEVENTMANAGER, clsAdmin.CONSTTYPEeCATALOG, clsAdmin.CONSTTYPEeCATALOGLITE,clsAdmin.CONSTTYPEeCATALOGLITEEVENTMANAGER,
                                         clsAdmin.CONSTTYPEeCATALOGEVENTMANAGER, clsAdmin.CONSTTYPEeSHOP, clsAdmin.CONSTTYPEeSHOPLITE, clsAdmin.CONSTTYPEESHOPINVOICE,
                                         clsAdmin.CONSTTYPEESHOPLITEINVOICE,clsAdmin.CONSTTYPEeSHOPEVENTMANAGER
                                     };

        // if this project contains "Event manager" and "Product manager"
        if (intEshopEcatalog.Contains(intProjectType) || (Session[clsAdmin.CONSTPROJECTEVENTMANAGER] != null || intProjectType == clsAdmin.CONSTTYPEePROFILELITE))
        {
            clsPage page = new clsPage();
            DataSet dsPage = page.getPageList(1);
            DataView dvPage = new DataView(dsPage.Tables[0]);
            DataView dvPage2 = new DataView(dsPage.Tables[0]);
            dvPage.RowFilter = "PAGE_ENABLELINK <> 0";
            dvPage.Sort = "PAGE_NAME ASC";


            tblDefaultPageSettings.Visible = true;

            if (intEshopEcatalog.Contains(intProjectType))
            {

                ddlProductPage.DataSource = dvPage;
                ddlProductPage.DataTextField = "PAGE_DISPLAYNAME";
                ddlProductPage.DataValueField = "PAGE_ID";
                ddlProductPage.DataBind();

                ListItem ddlDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "0");
                ddlProductPage.Items.Insert(0, ddlDefaultItem);

                trProductPage.Visible = true;


            }

            if ((Convert.ToInt16(Session[clsAdmin.CONSTPROJECTEVENTMANAGER]) != 0 && Session[clsAdmin.CONSTPROJECTEVENTMANAGER] != null) || intProjectType == clsAdmin.CONSTTYPEePROFILELITE)
            {

                ddlEventPage.DataSource = dvPage;
                ddlEventPage.DataTextField = "PAGE_DISPLAYNAME";
                ddlEventPage.DataValueField = "PAGE_ID";
                ddlEventPage.DataBind();

                ListItem ddlDefaultItem2 = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "0");
                ddlEventPage.Items.Insert(0, ddlDefaultItem2);

                trEventPage.Visible = true;

                // Event Setting
                tblEventSetting.Visible = true;
                DataSet dsEventSortOption = mis.getListByListGrp("EVENT SORT OPTION", 0);
                if (dsEventSortOption != null)
                {
                    DataView dvEventSortOption = new DataView(dsEventSortOption.Tables[0]);
                    dvEventSortOption.Sort = "LIST_ORDER ASC";

                    rptEventSortOption.DataSource = dsEventSortOption.Tables[0];
                    rptEventSortOption.DataBind();
                }
            }

            DataSet dsProdDetailDisplayType = new clsMis().getListByListGrp("Product Detail Display Type", 1);
            ddlProdDetailDisplayType.DataSource = dsProdDetailDisplayType.Tables[0];
            ddlProdDetailDisplayType.DataTextField = "LIST_NAME";
            ddlProdDetailDisplayType.DataValueField = "LIST_VALUE";
            ddlProdDetailDisplayType.DataBind();
        }

        // End by sh.chong 29 Apr 2015 - to trace product and page using which template

        int intImgMaxSize = 0;

        if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
        {
            if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
            {
                intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
            }
            else
            {
                if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                {
                    if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                    {
                        intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                    }
                }
            }
        }

        if (Session[clsAdmin.CONSTPROJECTDELIVERYDATETIME] != null && Convert.ToString(Session[clsAdmin.CONSTPROJECTDELIVERYDATETIME]) == "1")
        {
            trDeliveryWeek.Visible = true;
            trDeliveryTime.Visible = true;
        }

        //litUploadRuleCurr.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH, "kb") + " | " + clsMis.formatAllowedFileDimension(clsAdmin.CONSTADMCURIMGALLOWEDWIDTH, clsAdmin.CONSTADMCURIMGALLOWEDHEIGHT, "px");
        litUploadRuleCurr.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + " | " + clsMis.formatAllowedFileDimension(clsAdmin.CONSTADMCURIMGALLOWEDWIDTH, clsAdmin.CONSTADMCURIMGALLOWEDHEIGHT, "px");

        fillForm();
        bindRptData();
    }

    protected void fillForm()
    {
        clsConfig config = new clsConfig();

        DataSet ds = config.getItemList(1);
        DataRow[] foundRow;

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPPAGEDETAILS + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAGETITLEPREFIX, true) == 0) { txtPrefix.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAGETITLE, true) == 0) { txtPageTitle.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEKEYWORD, true) == 0) { txtKeyword.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEDESCRIPTION, true) == 0) { txtDesc.Text = row["CONF_VALUE"].ToString(); }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPQUICKCONTACT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEQUICKCONTACTTEL, true) == 0) { txtQuickContactTel.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEQUICKCONTACTEML, true) == 0) { txtQuickContactEmail.Text = row["CONF_VALUE"].ToString(); }
        }

        /* Website Settings */
        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPWEBSITE + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMECOPYRIGHT, true) == 0) { txtCopyright.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEDITORSTYLE, true) == 0) { txtEditorStyle.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMETHUMBNAILWIDTH, true) == 0) { txtThumbnailWidth.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMETHUMBNAILHEIGHT, true) == 0) { txtThumbnailHeight.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMERIGHTCLICKENABLE, true) == 0) { ddlRightClickEnable.SelectedValue = row["CONF_VALUE"].ToString(); }
        }
        /* End of Website Settings */

        //page default settings 
        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPDEFAULTPAGE + "'");
        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPRODUCTPAGE, true) == 0)
            {
                if (!string.IsNullOrEmpty(row["CONF_VALUE"].ToString()))
                {
                    ddlProductPage.SelectedValue = row["CONF_VALUE"].ToString();
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEVENTPAGE, true) == 0) {
                if (!string.IsNullOrEmpty(row["CONF_VALUE"].ToString()))
                {
                    ddlEventPage.SelectedValue = row["CONF_VALUE"].ToString();
                }
            }
        }
        //ddlProductPage.SelectedValue = "4";
        // end of page default settings

        /*Product Setting*/
        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPPRODUCT + "'");

        foreach (DataRow row in foundRow)
        {

            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEMAXQTYPERPRODUCT, true) == 0) { txtMaxQtyPerProduct.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEMAXQTYPERPROMOPRODUCT, true) == 0) { txtMaxQtyPerPromoProduct.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPRODUCTDETAILDISPLAYTYPE, true) == 0) { ddlProdDetailDisplayType.SelectedValue = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPRODUCTPRICERANGEFILTER, true) == 0) { chkboxPriceRange.Checked = row["CONF_VALUE"].ToString() == "1"; }

            

        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPPRODUCT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPRODINVENTORY, true) == 0) { chkboxInventory.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTDEFAULTTRUEVALUE; }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPRODDNAME, true) == 0) { chkboxProdDName.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTDEFAULTTRUEVALUE; }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPRODCODE, true) == 0) { chkboxProdCode.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTDEFAULTTRUEVALUE; }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPRODSNAPSHOT, true) == 0) { chkboxProdSnapshot.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTDEFAULTTRUEVALUE; }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPRODPRICE, true) == 0) { chkboxProdPrice.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTDEFAULTTRUEVALUE; }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMERANDOMRELATEDPRODENABLED, true) == 0) { chkboxRandomRelatedProdEnabled.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTDEFAULTTRUEVALUE; }
        }

        foundRow = ds.Tables[0].Select("CONF_NAME = '" + clsConfig.CONSTNAMEGROUPIMG + "'");
        if (foundRow.Length > 0)
        {
            txtDefaultImage.Text = foundRow[0]["CONF_VALUE"].ToString();
        }
        /*End Product Setting*/

        /*Shipping Setting*/
        lblShippingType.Text = clsConfig.CONSTNAMESHIPPINGTYPE;

        if (trShippingType.Visible)
        {
            clsMis mis = new clsMis();
            DataSet ds2 = new DataSet();
            ds2 = mis.getListByListGrp("SHIPPING TYPE", 1);
            DataView dv2 = new DataView();
            dv2 = new DataView(ds2.Tables[0]);
            dv2.Sort = "LIST_ORDER ASC";

            ddlShippingType.DataSource = dv2;
            ddlShippingType.DataTextField = "LIST_NAME";
            ddlShippingType.DataValueField = "LIST_VALUE";
            ddlShippingType.DataBind();

            ListItem ddlShippingTypeDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
            ddlShippingType.Items.Insert(0, ddlShippingTypeDefaultItem);

            foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPSHIPPING + "' AND CONF_NAME = '" + clsConfig.CONSTNAMESHIPPINGTYPE + "'");
            if (foundRow.Length > 0)
            {
                string strShippingType = foundRow[0]["CONF_VALUE"].ToString();
                ddlShippingType.SelectedValue = strShippingType;
            }

            //Volumatric Weight
            lblVolumetricWeight.Text = clsConfig.CONSTNAMEUSEVOLUMETRICWEIGHT;
            foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPSHIPPING + "' AND CONF_NAME = '" + clsConfig.CONSTNAMEUSEVOLUMETRICWEIGHT + "'");
            if (foundRow.Length > 0)
            {
                string strVolumetricValue = foundRow[0]["CONF_VALUE"].ToString();

                if (strVolumetricValue == clsConfig.CONSTVALUEVOLUMETRICWEIGHT) { chkboxVolumetricWeight.Checked = true; }
                else { chkboxVolumetricWeight.Checked = false; }
            }
            //End Volumetric Weight

            // Delivery Week
            string[,] defaultWeek = config.getDeliveryWeek();
            if (defaultWeek != null)
            {
                chklistDeliveryWeek.Items.Add(new ListItem("Any Date", "")
                {
                    Selected = config.value == "1"
                });
                for (int i = 0; i <= defaultWeek.GetUpperBound(0); i++)
                {
                    chklistDeliveryWeek.Items.Add(new ListItem(defaultWeek[i, 0])
                    {
                        Selected = defaultWeek[i, 1] == "1"
                    });
                }
            }

            // Delivery Time
            string[] defaultTime = config.getDeliveryTime();
            if (defaultTime != null)
            {
                chkboxDeliveryTimeActive.Checked = config.value == "1";
                hdnDeliveryTime.Value = string.Join("|", defaultTime);
            }
        }
        /*End Shipping Setting*/

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPINVOICE + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEINVOICELOGO, true) == 0)
            {
                if (!string.IsNullOrEmpty(row["CONF_VALUE"].ToString()))
                {
                    pnlInvoiceImage.Visible = true;
                    imgInvoiceImage.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCMSFOLDER + "/" + clsAdmin.CONSTIMAGESFOLDER + "/" + row["CONF_VALUE"].ToString() + "&w=" + clsAdmin.CONSTADMDEFAULTIMGWIDTH + "&h=" + clsAdmin.CONSTADMDEFAULTIMGHEIGHT + "&f=1";
                    hdnInvoiceImage.Value = row["CONF_VALUE"].ToString();
                    hdnInvoiceImageRef.Value = row["CONF_VALUE"].ToString();
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMECOMPANYNAME, true) == 0) { txtCompanyName.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEROC, true) == 0) { txtROC.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMECOMPANYADDRESS, true) == 0) { txtCompanyAddress.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMECOMPANYTEL, true) == 0) { txtCompanyTel.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMECOMPANYEMAIL, true) == 0) { txtCompanyEmail.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMECOMPANYWEBSITE, true) == 0) { txtCompanyWebsite.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEINVOICEREMARKS, true) == 0) { txtInvoiceRemarks.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEINVOICEREMARKSPDF, true) == 0) { txtInvoiceRemarksPdf.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEINVOICESIGNATURE, true) == 0) { txtInvoiceSig.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEINVOICESIGNATUREPDF, true) == 0) { txtInvoiceSigPdf.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYMENTTYPE, true) == 0) { ddlPaymentType.SelectedValue = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMESHIPPINGCOMPANY, true) == 0) { ddlShippingCompany.SelectedValue = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEINVOICEPAYMENTEMAIL, true) == 0) { chkboxPayment.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTTRUE; }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEINVOICEPROCESSINGEMAIL, true) == 0) { chkboxProcessing.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTTRUE; }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEINVOICEDELIVERYEMAIL, true) == 0) { chkboxDelivery.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTTRUE; }
        }

        /* Other Settings */
        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPOTHERSETTINGS + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMELISTINGSIZE, true) == 0) { txtListingSize.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMESESSIONTIMEOUT, true) == 0) { txtSessionTimeout.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFRIENDLYURL, true) == 0) { chkboxFriendlyURL.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTDEFAULTTRUEVALUE; }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEMOBILEVIEW, true) == 0) { chkboxMobileView.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTDEFAULTTRUEVALUE; }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEDELIVERYMSG, true) == 0) { chkboxDeliveryMsg.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTDEFAULTTRUEVALUE; }
        }
        if (string.IsNullOrEmpty(txtSessionTimeout.Text.Trim()))
        {
            txtSessionTimeout.Text = 20.ToString();
        }
        /* End of Other Settings */

        /* Shipping Company */
        if (config.extractConfigByConfigId(shipCompId, 0))
        {
            txtShipComp.Text = config.name;
            txtShipCompOrder.Text = config.order.ToString();
        }
        /* End of Shipping Company */

        /* Shipping Method */
        if (config.extractConfigByConfigId(shipMethodId, 0))
        {
            txtShipMethod.Text = config.name;
            txtShipMethodOrder.Text = config.order.ToString();
        }
        /* End of Shipping Method */

        /* Enquiry Settings */
        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPENQUIRYSETTINGS + "'");
        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEENQUIRYDESC, true) == 0) { txtEnquiryDesc.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEENQUIRYTITLE, true) == 0) { txtEnquiryTitle.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEENQUIRYRECAPTCHA, true) == 0) { chkboxEnquiryRecaptcha.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTDEFAULTTRUEVALUE; }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEENABLEENQUIRYRECIPIENT, true) == 0) { chkboxEnquiryRecipient.Checked = row["CONF_VALUE"].ToString() == "1"; }
        }

        DataTable dtEnquiryRecipient = new clsEnquiryRecipient().getDataTable();
        hdnEnquiryRecipient.Value = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(dtEnquiryRecipient.AsEnumerable().Select(x => new Dictionary<string, object>()
        {
            { "ID" , x["enqrcpt_id"].ToString() },
            { "name" , x["enqrcpt_name"].ToString() },
            { "email" , x["enqrcpt_email"].ToString() },
            { "order" , x["enqrcpt_order"].ToString() },
            { "deleted" ,0},
        }).ToList());
        /* End of Enquiry Settings */

        /* Google reCaptcha Settings */
        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPGOOGLERECAPTCHASETTINGS + "'");
        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMERECAPTCHASITEKEY, true) == 0) { txtRecaptchaSiteKey.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMERECAPTCHASECRETKEY, true) == 0) { txtRecaptchaSecretKey.Text = row["CONF_VALUE"].ToString(); }
        }
        /* End of Google reCaptcha Settings */



        /* End of Website Settings */
        #region "Currency"
        clsCurrency currency = new clsCurrency();
        if (mode == 2)
        {
            if (currency.extractCurById(currId))
            {
                txtCurrName.Text = currency.curname;
                txtCurrValue.Text = currency.curvalue;
                txtCurrConversionRate.Text = currency.curConvRate.ToString();
                txtCurrOrder.Text = currency.curorder.ToString();
                chkboxCurrActive.Checked = currency.curactive > 0 ? true : false;
            }
        }
        #endregion

        #region Multiple Language
        if (Session[clsAdmin.CONSTPROJECTLANG] != null)
        {
            string[] languages = Session[clsAdmin.CONSTPROJECTLANG].ToString()
                                    .TrimStart(clsAdmin.CONSTDEFAULTSEPERATOR)
                                    .TrimEnd(clsAdmin.CONSTDEFAULTSEPERATOR)
                                    .Split(clsAdmin.CONSTDEFAULTSEPERATOR);

            rptPageDetail.DataSource = languages;
            rptPageDetail.DataBind();

            for (int i = 0; i < languages.Length; i++)
            {
                RepeaterItem item = rptPageDetail.Items[i];
                Literal litTitle = item.FindControl("litTitle") as Literal;
                litTitle.Text = "Page Details " + "(" + languages[i].ToUpper() + ")";

                TextBox txtPrefixLang = item.FindControl("txtPrefix") as TextBox;
                TextBox txtPageTitleLang = item.FindControl("txtPageTitle") as TextBox;
                TextBox txtKeywordLang = item.FindControl("txtKeyword") as TextBox;
                TextBox txtDescLang = item.FindControl("txtDesc") as TextBox;

                foundRow = ds.Tables[0].Select(string.Format("CONF_GROUP = '{0}' AND CONF_LANG = '{1}'", clsConfig.CONSTGROUPPAGEDETAILSLANG, languages[i]));
                foreach (DataRow row in foundRow)
                {
                    if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAGETITLEPREFIX, true) == 0) { txtPrefixLang.Text = row["CONF_VALUE"].ToString(); }
                    if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAGETITLE, true) == 0) { txtPageTitleLang.Text = row["CONF_VALUE"].ToString(); }
                    if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEKEYWORD, true) == 0) { txtKeywordLang.Text = row["CONF_VALUE"].ToString(); }
                    if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEDESCRIPTION, true) == 0) { txtDescLang.Text = row["CONF_VALUE"].ToString(); }
                }
            }

        }




        #endregion Multiple Language
    }

    protected void bindRptData()
    {
        #region "Shipping Method & Shipping Company"
        /*SS*/
        clsConfig config = new clsConfig();
        DataSet ds = new DataSet();
        ds = config.getShippingCompanyByGroup(clsConfig.CONSTGROUPSHIPPINGCOMPANY, 1);
        DataView dv;
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "CONF_ORDER ASC";

        if (dv.Count > 0)
        {
            rptShipComp.DataSource = dv;
            rptShipComp.DataBind();
        }
        else { trShipCompList.Visible = false; }

        ds = config.getShippingCompanyByGroup(clsConfig.CONSTGROUPSHIPPINGMETHOD, 1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "CONF_ORDER ASC";

        if (dv.Count > 0)
        {
            rptShipMethod.DataSource = dv;
            rptShipMethod.DataBind();
        }
        else { trShipMethodList.Visible = false; }
        #endregion

        #region "Currency"
        /*SS*/

        //if (Session["TABLECUR"] != null)
        //{
        //    DataTable dt = new DataTable();
        //    dt = (DataTable)Session["TABLECUR"];

        //    DataView dvCur = new DataView(dt);
        //    dvCur.RowFilter = "CUR_DELETED = 0";

        //    rptCurrency.DataSource = dvCur;
        //    rptCurrency.DataBind();
        //}

        clsCurrency curr = new clsCurrency();
        ds = curr.getCurrencyList();
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "CUR_ORDER";
        dsCurrency = ds;
        itemTotal = dv.Count;

        if (dv.Count > 0)
        {
            itemCount = 0;
            rptCurrency.DataSource = dv;
            rptCurrency.DataBind();
        }
        #endregion

    }

    protected int saveConfig(List<Dictionary<string, string>> confList)
    {
        clsConfig config = new clsConfig();
        bool boolSuccess = true;
        bool boolChange = false;

        int intRecordAffected = 0;
        for (int i = 0; i < confList.Count; i++)
        {
            Dictionary<string, string> confObj = confList[i];

            string confName = confObj["name"];
            string confValue = confObj["value"];
            string confGroup = confObj["group"];
            string confLang = confObj.ContainsKey("lang") ? confObj["lang"] : null;

            if (!config.isExactSameSetData(confName, confValue, confGroup, confLang))
            {
                intRecordAffected = config.updateItem(confGroup, confName, confValue, confLang, int.Parse(Session["ADMID"].ToString()));
                boolSuccess = intRecordAffected == 1 || intRecordAffected == 0;

                if (!boolChange)
                {
                    boolChange = intRecordAffected == 1;
                }
            }

            if (!boolSuccess)
            {
                break;
            }
        }

        if (!boolSuccess) return (int)status.fail;
        else if (boolChange) return (int)status.success;
        else return (int)status.nochange;
    }
    #endregion
}
