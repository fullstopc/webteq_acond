﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Drawing.Imaging;



public partial class ctrl_ucAdmProductGallery : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode;
    protected int _id;
    protected DataTable _dtImage;
    protected string _pageListingURL = "";
    protected Boolean _boolDefault = false;
    protected int _maxImage = 0;
    protected int _currentImage = 0;
    protected int _fileMaxLength;
    protected string _fileExt;

    protected int numOfPics;
    protected int intImageInRow = 5;
    protected int intImageCount = 0;

    protected string strTempPath = HttpContext.Current.Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMTEMPFOLDER.ToString() + "/";
    protected string strProdPath = HttpContext.Current.Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPRODFOLDER.ToString() + "/" + clsAdmin.CONSTADMGALLERYFOLDER + "/";
    protected string strThumbPath = HttpContext.Current.Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPRODFOLDER + "/" + clsAdmin.CONSTADMGALLERYFOLDER + "/" + clsAdmin.CONSTTHUMBNAILFOLDER + "/";
    protected string strShowImage = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=";
    protected string aspectRatio = "1/1";
    protected clsProduct prod = new clsProduct();
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int id
    {
        get
        {
            if (ViewState["ID"] == null)
            {
                return _id;
            }
            else
            {
                return Convert.ToInt16(ViewState["ID"]);
            }
        }
        set { ViewState["ID"] = value; }
    }

    public DataTable dtImage
    {
        get
        {
            if (ViewState["DTIMAGE"] == null)
            {
                return _dtImage;
            }
            else
            {
                return (DataTable)ViewState["DTIMAGE"];
            }
        }
        set { ViewState["DTIMAGE"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if (ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }

    protected Boolean boolDefault
    {
        get
        {
            if (ViewState["BOOLDEFAULT"] == null)
            {
                return _boolDefault;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLDEFAULT"]);
            }
        }
        set { ViewState["BOOLDEFAULT"] = value; }
    }

    public int maxImage
    {
        get
        {
            if (ViewState["MAXIMAGE"] == null)
            {
                return _maxImage;
            }
            else
            {
                return Convert.ToInt16(ViewState["MAXIMAGE"]);
            }
        }
        set { ViewState["MAXIMAGE"] = value; }
    }

    protected int currentImage
    {
        get
        {
            if (ViewState["CURRENTIMAGE"] == null)
            {
                return _currentImage;
            }
            else
            {
                return Convert.ToInt16(ViewState["CURRENTIMAGE"]);
            }
        }
        set { ViewState["CURRENTIMAGE"] = value; }
    }

    protected int fileMaxLength
    {
        get
        {
            if (ViewState["FILEMAXLENGTH"] == null)
            {
                return _fileMaxLength;
            }
            else
            {
                return int.Parse(ViewState["FILEMAXLENGTH"].ToString());
            }
        }
        set { ViewState["FILEMAXLENGTH"] = value; }
    }

    protected string fileExt
    {
        get { return ViewState["FILEEXT"] as string ?? _fileExt; }
        set { ViewState["FILEEXT"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    
    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            System.Web.Script.Serialization.JavaScriptSerializer json = new System.Web.Script.Serialization.JavaScriptSerializer();
            int intRecordAffected = 0;
            int intAdmID = Convert.ToInt32(Session["ADMID"]);
            Boolean boolSuccess = true;
            Boolean boolEdited = false;

            clsConfig config = new clsConfig();
            try
            {
                bool boolProdGalleryExists = (Session["PROD_GALLERY"] != null);

                if (boolProdGalleryExists)
                {
                    Dictionary<string, ProdGalleryDetail> dictProdGalleryDetail = json.Deserialize<Dictionary<string, ProdGalleryDetail>>(Session["PROD_GALLERY"].ToString());
                    Dictionary<string, int> dictProdGalleryDetailSort = json.Deserialize<Dictionary<string, int>>(Session["PROD_GALLERY_SORTING"].ToString());
                    Dictionary<string, int> dictProdGalleryDefault = json.Deserialize<Dictionary<string, int>>(Session["PROD_GALLERY_DEFAULT"].ToString());

                    string galleryPath = strProdPath;
                    if (!Directory.Exists(galleryPath)) { Directory.CreateDirectory(galleryPath); }

                    string tempPath = strTempPath;
                    if (!Directory.Exists(tempPath)) { Directory.CreateDirectory(tempPath); }

                    foreach (KeyValuePair<string, ProdGalleryDetail> entry in dictProdGalleryDetail)
                    {
                        ProdGalleryDetail prodGalleryDetail = entry.Value;
                        prodGalleryDetail.prodId = id;
                        string filenameEncode = prodGalleryDetail.filenameEncode;
                        if (prodGalleryDetail.type == "session" && !prodGalleryDetail.isDelete)
                        {
                            intRecordAffected = prod.addGallery(prodGalleryDetail, dictProdGalleryDefault[filenameEncode], dictProdGalleryDetailSort[filenameEncode],intAdmID);

                            if (intRecordAffected == 1)
                            {
                                int prodGallID = prod.prodGallId;
                                File.Move(strTempPath + prodGalleryDetail.filename, strProdPath + prodGalleryDetail.filename);
                                boolEdited = true;
                            }
                            else
                            {
                                boolSuccess = false;
                                break;
                            }
                        }
                        else if (prodGalleryDetail.type == "database")
                        {
                           
                            if (prodGalleryDetail.isDelete)
                            {
                                intRecordAffected = prod.deleteGalleryImage(prodGalleryDetail.id);
                                if (intRecordAffected == 1)
                                {
                                    if (File.Exists(strProdPath + prodGalleryDetail.filename)) { File.Delete(strProdPath + prodGalleryDetail.filename); }
                                    boolEdited = true;
                                }
                            }
                            else
                            {
                                if (!prod.isExactSameGallery(prodGalleryDetail.id, prodGalleryDetail, dictProdGalleryDefault[filenameEncode], dictProdGalleryDetailSort[filenameEncode]))
                                {
                                    intRecordAffected = prod.updateGallery(prodGalleryDetail.id, prodGalleryDetail, dictProdGalleryDefault[filenameEncode], dictProdGalleryDetailSort[filenameEncode], intAdmID);
                                    boolEdited = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                clsLog.logErroMsg(ex.Message.ToString());
            }

            if (boolEdited)
            {
                Session["EDITPRODID"] = prod.prodId;
            }
            else
            {
                if (!boolSuccess)
                {
                    prod.extractProdById2(id, 0);
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit product (" + prod.prodName + "). Please try again.</div>";
                }
                else
                {
                    Session["EDITPRODID"] = id;
                    Session["NOCHANGE"] = 1;
                }
            }

            Response.Redirect(Request.Url.ToString());
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        double intProdGallSet = 0;
        if (Session[clsAdmin.CONSTPROJECTPRODGALLSETTING] != null && Session[clsAdmin.CONSTPROJECTPRODGALLSETTING] != "")
        {
            if (Double.Parse(Session[clsAdmin.CONSTPROJECTPRODGALLSETTING].ToString()) > 0)
            {
                intProdGallSet = Double.Parse(Session[clsAdmin.CONSTPROJECTPRODGALLSETTING].ToString());
            }
            else
            {
                if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                {
                    if (Double.Parse(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING].ToString()) > 0)
                    {
                        intProdGallSet = Double.Parse(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING].ToString());
                    }
                }
            }
        }

        intProdGallSet = intProdGallSet / 1000;

        if ((!Page.ClientScript.IsStartupScriptRegistered("UploadMaxSize")))
        {
            clsConfig config = new clsConfig();
            string aspectRatio = config.aspectRatio;
            string[] aspectRatioXandY = aspectRatio.Split('/');
            string aspectRatioX = aspectRatioXandY[0];
            string aspectRatioY = aspectRatioXandY[1];

            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "updateMaxSize('" + intProdGallSet + "'," + aspectRatioX + "," + aspectRatioY + ");";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "UploadMaxSize", strJS, false);
        }


        if (!IsPostBack)
        {
            Session["PROD_GALLERY"] = null;
            Session["PROD_GALLERY_SORTING"] = null;
            Session["PROD_GALLERY_DEFAULT"] = null;
            bindRptData();
        }
    }

    protected void bindRptData()
    {
        Session["PROD_GALLERY"] = prod.getProdGallery(id);
        Session["PROD_GALLERY_SORTING"] = prod.getProdGallerySorting();
        Session["PROD_GALLERY_DEFAULT"] = prod.getProdGalleryDefault();
    }
    #endregion
}
