﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmCopy.ascx.cs" Inherits="ctrl_ucAdmCopy" %>

<asp:Panel ID="pnlAdmCopy" runat="server" CssClass="divAdmCopy">
    <asp:Panel ID="pnlAdmCopyText" runat="server" CssClass="divAdmCopyText">
        <asp:Literal ID="litCopy" runat="server" meta:resourcekey="litCopy"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlAdmCopyLogo" runat="server" CssClass="divAdmCopyLogo" Visible="false">
        <asp:Image ID="imgLogo" runat="server" meta:resourcekey="imgLogo" />
    </asp:Panel>
</asp:Panel>
