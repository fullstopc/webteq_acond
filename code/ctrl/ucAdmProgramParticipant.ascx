﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmProgramParticipant.ascx.cs" Inherits="ctrl_ucAdmProgramParticipant" %>

<script type="text/javascript">
    function OpenPopup() {
        window.open("admRegistrationDetails.aspx", "scrollbars=no, resizable=no,width=400,height=280");
        return false;
    }
</script>

<asp:Panel ID="pnlParticipant" runat="server">
    <div id="divListing" class="divListing">
        <div class="divListingHdr">
            <div id="divBtn" class="divParticipantBtn">
                <div class="divListingHdrLeft"><asp:Literal ID="litParticipantFound" runat="server"></asp:Literal></div>
                <div class="divListingAction"><asp:LinkButton ID="lnkbtnExcel" runat="server" Text="Excel" ToolTip="Excel" CssClass="btn" OnClick="lnkbtnExcel_Click"></asp:LinkButton></div>
            </div>
        </div>
        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
            <asp:Literal ID="litAck" runat="server"></asp:Literal>
        </asp:Panel>
        <div id="divListingData" runat="server" class="divListingData">
            <asp:GridView ID="gvParticipant" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom" CssClass="dataTbl" OnRowDataBound="gvParticipant_RowDataBound" OnSorting="gvParticipant_Sorting" OnPageIndexChanging="gvParticipant_PageIndexChanging" OnRowCommand="gvParticipant_RowCommand" DataKeyNames="PAR_ID" EmptyDataText="No record found.">
                <PagerStyle CssClass="pg" HorizontalAlign="Right" />
                <Columns>
                    <asp:TemplateField HeaderText="No.">
                        <ItemStyle CssClass="tdNo" />
                        <ItemTemplate>
                            <asp:Literal ID="litNo" runat="server" Text="<%# Container.DataItemIndex + 1 %>"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reg. Date" SortExpression="PAR_REGISTEREDDATE">
                        <ItemStyle />
                        <ItemTemplate>
                            <asp:Literal ID="litRegDate" runat="server"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle />
                        <ItemTemplate>
                            <asp:Literal ID="litProgName" runat="server"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="DOB"  HeaderStyle-CssClass="hiddenCol" ItemStyle-CssClass="hiddenCol">
                        <ItemTemplate>
                            <asp:Literal ID="litDOB" runat="server"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Contact No." DataField="PAR_CONTACTNO" SortExpression="PAR_CONTACTNO" HeaderStyle-CssClass="hiddenCol" ItemStyle-CssClass="hiddenCol" />
                    <asp:BoundField HeaderText="Email" DataField="PAR_EMAIL" SortExpression="PAR_EMAIL" HeaderStyle-CssClass="hiddenCol" ItemStyle-CssClass="hiddenCol" />
                    <asp:TemplateField HeaderText="Program Date">
                        <ItemTemplate>
                            <asp:Literal ID="litProgDate" runat="server"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Action">
                        <ItemStyle CssClass="tdAct" />
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnView" runat="server" CommandName="cmdView" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PAR_ID") %>'></asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnCancel" runat="server" CausesValidation="false" CommandName="cmdCancel"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Panel>