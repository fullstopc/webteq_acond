﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmLogoSetting.ascx.cs" Inherits="ctrl_ucAdmLogoSetting" %>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>watermark.js" type="text/javascript"></script>

<link  href="<%= System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>cropper.min.css" rel="stylesheet">
<script src="<%= System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>cropper.min.js"></script>
<script type="text/javascript">
    function ddlWaterMarkTypeChange(element) {
        var strOptionSel = element.options[element.selectedIndex].value;
        ddlWaterMarkTypeUpdate(strOptionSel);
    }

    function ddlWaterMarkTypeUpdate(strOptionSel) {
        if (strOptionSel == "<%= clsAdmin.CONSTWATERMARKTYPETEXT %>") {
            $("#tbodyWatermarkText").show();
            $("#tbodyWatermarkImage").hide();
            $("#tbodyWatermarkOption").show();
        } else if (strOptionSel == "<%= clsAdmin.CONSTWATERMARKTYPEIMAGE %>") {
            $("#tbodyWatermarkText").hide();
            $("#tbodyWatermarkImage").show();
            $("#tbodyWatermarkOption").show();
        } else {
            $("#tbodyWatermarkText").hide();
            $("#tbodyWatermarkImage").hide();
            $("#tbodyWatermarkOption").hide();
        }
    }

    function txtOpacityChange(value) {
        var intValue = 0.1;
        if (value) intValue = parseFloat(value);
        $("#lblOpacity").text(intValue * 100 + "%");
    }
    function cropActive(name) {
        $(".div"+name+"FileUpload").hide();
        $(".div" + name + "SavedImageContainer").hide();
        $("#tbl" + name + "CropImg").show();
        $("#canvas" + name + "-temp").remove();
        $(".div" + name + "ImgPreview").show();
        $("input[type='hidden'][data-id='hdn" + name + "ImgName']").val($(".file" + name + "")[0].files[0].name);
        $(".icon-edit."+name).removeClass("active");
    }

    function cropInActive(name) {
        $(".div" + name + "FileUpload").show();
        $(".div" + name + "SavedImageContainer").show();
        $("#tbl" + name + "CropImg").hide();
        $(".icon-edit." + name).addClass("active");

    }

    function cropDone(name) {
        $(".div" + name + "FileUpload").show();
        $(".div" + name + "SavedImageContainer").hide();
        $("#tbl" + name + "CropImg").hide();
        $(".div" + name + "ImgPreview").hide();
        $(".icon-edit." + name).addClass("active");
    }
    function cropReset(name) {
        $("input[type='hidden'][data-id='hdn" + name + "ImgX']").val("");
        $("input[type='hidden'][data-id='hdn" + name + "ImgY']").val("");
        $("input[type='hidden'][data-id='hdn" + name + "ImgW']").val("");
        $("input[type='hidden'][data-id='hdn" + name + "ImgH']").val("");
        $("input[type='hidden'][data-id='hdn" + name + "ImgName']").val("");
        $("input[type='hidden'][data-id='hdn" + name + "ImgCropped']").val("");

        $("#canvas" + name + "-temp").remove();
        $("#tbl" + name + "CropImg").hide();
        $(".icon-edit." + name).removeClass("active");
    }

    function cropInit(name, settings_addon) {
        var $image = $(".div"+name+"ImgContainer > img");
        var $tblCropImg = $("#tbl" + name + "CropImg");
        var URL = window.URL || window.webkitURL;
        var blobURL;
        var $inputImage = $(".file" + name + "");

        var settings = {
            mouseWheelZoom: false,
            movable: false,
            preview: '.div' + name + 'ImgPreview',
            crop: function (e) {
                // Output the result data for cropping image.
                $("input[type='hidden'][data-id='hdn" + name + "ImgX']").val(e.x);
                $("input[type='hidden'][data-id='hdn" + name + "ImgY']").val(e.y);
                $("input[type='hidden'][data-id='hdn" + name + "ImgW']").val(e.width);
                $("input[type='hidden'][data-id='hdn" + name + "ImgH']").val(e.height);

            }
        };
        if (settings_addon != 'undefined') {
            var obj3 = {};
            for (var attrname in settings) { obj3[attrname] = settings[attrname]; }
            for (var attrname in settings_addon) { obj3[attrname] = settings_addon[attrname]; }

            settings = obj3;
        }
        console.log(name, settings);
        $image.cropper(settings);


        $inputImage.change(function () {
            cropReset(name);
            if ($(this).val() != "") {
                $(".icon-edit." + name).addClass("active");
            }
        });

        $(".icon-edit." + name).click(function () {
            var files = $inputImage[0].files;
            var file;

            if (files && files.length) {
                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    cropActive(name);
                    blobURL = URL.createObjectURL(file);
                    $image.one('built.cropper', function () {
                        URL.revokeObjectURL(blobURL); // Revoke when load complete
                    }).cropper('reset').cropper('replace', blobURL);
                } else {
                    alert("Please choose an image file.");
                    $inputImage.val('');
                }
            }

        });

        $("#btn"+name+"CancelCrop").click(function () {
            cropInActive(name);
        });
        $("#btn" + name + "ConfirmCrop").click(function () {
            cropDone(name);
            var width = $("input[type='hidden'][data-id='hdn" + name + "ImgW']").val(),
                height = $("input[type='hidden'][data-id='hdn" + name + "ImgH']").val();

            var cropImgToCanvas = $image.cropper('getCroppedCanvas');
            var $canvas = $(cropImgToCanvas);

            $canvas.attr("id", "canvas" + name + "-temp");
            $canvas.css({ "max-width": "80%", "width": "50%", "height": "auto","padding-top":"10px" });
            $canvas.insertAfter('.div' + name + 'FileUpload');

            $("input[type='hidden'][data-id='hdn"+name+"ImgCropped']").val(cropImgToCanvas.toDataURL());

        });
    }


    $(document).ready(function() {
        ddlWaterMarkTypeUpdate($("#<%= ddlWaterMarkType.ClientID %>").val());
        txtOpacityChange($("#<%= hdnOpacity.ClientID %>").val());
        $("#txtOpacity").val($("#<%= hdnOpacity.ClientID %>").val());

        //        txtOpacityChange($("#txtOpacity").val())
        cropInit("Watermark");
        cropInit("CompanyLogo");
        cropInit("CALogo", { aspectRatio: 1/1, mouseWheelZoom: true, movable :true});
    });
</script>

<asp:Panel ID="pnlAdmCMSControlPanel" runat="server" CssClass="divAdmCMSControlPanel">
    <asp:Panel ID="pnlFormDetails" runat="server" CssClass="divLogoSettingContainer">
        <div id="divEProfileContainer">
            <div id="divCompanyLogoContainer">
                <table class="formTbl tblData" cellpadding="0" cellspacing="0" style="">
                    <thead>
                        <tr>
                            <td colspan="2" class="tdSectionHdr">Profile</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><asp:Label ID="lblCompanyLogo" runat="server"></asp:Label> : </td>
                            <td>
                                <div class="divCompanyLogoFileUpload">
                                    <div class="divFileUploadAction">
                                        <asp:FileUpload ID="fileCompanyLogo" runat="server" CssClass="fileCompanyLogo"/>
                                    </div>
                                    <span class="spanTooltip">
                                        <span class="icon"></span>
                                        <span class="msg right">
                                            <asp:Literal ID="litUploadRule" runat="server"></asp:Literal>
                                        </span>
                                    </span>
                                    <i class="material-icons icon-edit CompanyLogo">crop</i>
                                </div>
                                <table width="100%" id="tblCompanyLogoCropImg" style="display:none;">
                                    <thead>
                                        <tr>
                                            <td colspan="2">
                                                <button type="button" class="btnDarkBlue" id="btnCompanyLogoConfirmCrop">Crop</button>
                                                <button type="button" class="btnDarkBlue" id="btnCompanyLogoCancelCrop">Cancel</button>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td width="70%">
                                                <div class="divCompanyLogoImgContainer" style="width: 100%;">
                                                    <img style="max-width:100%;width:100%;height:auto;"/>
                                                </div>
                                            </td>
                                            <td width="30%">
                                                <div class="divCompanyLogoImgPreview" style="overflow:hidden;height:200px;width:200px;border:1px solid #ececec;">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2">
                                                <input type="hidden" id="hdnCompanyLogoImgX" runat="server" data-id="hdnCompanyLogoImgX" class="hdnCompanyLogoImgX"/>
                                                <input type="hidden" id="hdnCompanyLogoImgY" runat="server" data-id="hdnCompanyLogoImgY" class="hdnCompanyLogoImgY"/>
                                                <input type="hidden" id="hdnCompanyLogoImgW" runat="server" data-id="hdnCompanyLogoImgW" class="hdnCompanyLogoImgW"/>
                                                <input type="hidden" id="hdnCompanyLogoImgH" runat="server" data-id="hdnCompanyLogoImgH" class="hdnCompanyLogoImgH"/>
                                                <input type="hidden" id="hdnCompanyLogoImgName" runat="server" data-id="hdnCompanyLogoImgName" class="hdnCompanyLogoImgName"/>
                                                <input type="hidden" id="hdnCompanyLogoImgCropped" runat="server" data-id="hdnCompanyLogoImgCropped" class="hdnCompanyLogoImgCropped"/>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                                
                                <asp:HiddenField ID="hdnCompanyLogo" runat="server" />
                                <asp:HiddenField ID="hdnCompanyLogoRef" runat="server" />
                                <asp:CustomValidator ID="cvCompanyLogo" runat="server" ControlToValidate="fileCompanyLogo" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateImage_server" OnPreRender="cvImage_PreRender" ></asp:CustomValidator>
                                                
                                <asp:Panel ID="pnlCompanyLogo" runat="server" Visible="false" CssClass="divCompanyLogoSavedImageContainer">
                                    <br /><asp:LinkButton ID="lnkbtnDeleteCompanyLogo" runat="server" Text="Delete" ToolTip="Delete" OnCommand="lnkbtnDelete_Click" CausesValidation="false" CommandName="cmdDelete" CommandArgument="cmaCompanyLogo"></asp:LinkButton>
                                    <br /><asp:Image ID="imgCompanyLogo" runat="server" />
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblCompanyLogoTitle" runat="server"></asp:Label> : </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtCompanyLogoTitle"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblCALogo" runat="server"></asp:Label> : </td>
                            <td>
                                <div class="divCALogoFileUpload">
                                    <div class="divFileUploadAction">
                                        <asp:FileUpload ID="fileCALogo" runat="server" CssClass="fileCALogo"/>
                                    </div>
                                    <span class="spanTooltip">
                                        <span class="icon"></span>
                                        <span class="msg right">
                                            <asp:Literal ID="litUploadRule6" runat="server"></asp:Literal>
                                        </span>
                                    </span>
                                    <i class="material-icons icon-edit CALogo">crop</i>
                                </div>
                                <table width="100%" id="tblCALogoCropImg" style="display:none;">
                                    <thead>
                                        <tr>
                                            <td colspan="2">
                                                <button type="button" class="btnDarkBlue" id="btnCALogoConfirmCrop">Crop</button>
                                                <button type="button" class="btnDarkBlue" id="btnCALogoCancelCrop">Cancel</button>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td width="70%">
                                                <div class="divCALogoImgContainer" style="width: 100%;">
                                                    <img style="max-width:100%;width:100%;height:auto;"/>
                                                </div>
                                            </td>
                                            <td width="30%">
                                                <div class="divCALogoImgPreview" style="overflow:hidden;height:200px;width:200px;border:1px solid #ececec;">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2">
                                                <input type="hidden" id="hdnCALogoImgX" runat="server" data-id="hdnCALogoImgX" class="hdnCALogoImgX"/>
                                                <input type="hidden" id="hdnCALogoImgY" runat="server" data-id="hdnCALogoImgY" class="hdnCALogoImgY"/>
                                                <input type="hidden" id="hdnCALogoImgW" runat="server" data-id="hdnCALogoImgW" class="hdnCALogoImgW"/>
                                                <input type="hidden" id="hdnCALogoImgH" runat="server" data-id="hdnCALogoImgH" class="hdnCALogoImgH"/>
                                                <input type="hidden" id="hdnCALogoImgName" runat="server" data-id="hdnCALogoImgName" class="hdnCALogoImgName"/>
                                                <input type="hidden" id="hdnCALogoImgCropped" runat="server" data-id="hdnCALogoImgCropped" class="hdnCALogoImgCropped"/>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                                
                                <asp:HiddenField ID="hdnCALogo" runat="server" />
                                <asp:HiddenField ID="hdnCALogoRef" runat="server" />
                                <asp:CustomValidator ID="cvCALogo" runat="server" ControlToValidate="fileCALogo" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateImage_server" OnPreRender="cvImage_PreRender" ></asp:CustomValidator>
                                                
                                <asp:Panel ID="pnlCALogo" runat="server" Visible="false" CssClass="divCALogoSavedImageContainer">
                                    <br /><asp:LinkButton ID="lnkbtnDeleteCALogo" runat="server" Text="Delete" ToolTip="Delete" OnCommand="lnkbtnDelete_Click" CausesValidation="false" CommandName="cmdDelete" CommandArgument="cmaCALogo"></asp:LinkButton>
                                    <br /><asp:Image ID="imgCALogo" runat="server" />
                                </asp:Panel>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="divFaviconContainer">
                <table class="formTbl tblData borderTop" cellpadding="0" cellspacing="0" style="">
                    <thead>
                        <tr>
                            <td colspan="2" class="tdSectionHdr">Favicon settings</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><asp:Label ID="lblFaviconImage" runat="server"></asp:Label> : </td>
                            <td>
                                <div class="divFileUpload">
                                    <div class="divFileUploadAction">
                                        <asp:FileUpload ID="fileFavicon" runat="server" />
                                    </div>
                                    <span class="spanTooltip">
                                        <span class="icon"></span>
                                        <span class="msg right">
                                            <asp:Literal ID="litUploadRule5" runat="server"></asp:Literal>
                                        </span>
                                    </span>
                                </div>
                                <asp:HiddenField ID="hdnFavicon" runat="server" />
                                <asp:HiddenField ID="hdnFaviconRef" runat="server" />
                                <asp:CustomValidator ID="cvFavicon" runat="server" ControlToValidate="fileFavicon" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateImage_server" OnPreRender="cvImage_PreRender" ></asp:CustomValidator>
                                                
                                <asp:Panel ID="pnlFavicon" runat="server" Visible="false">
                                    <br /><asp:LinkButton ID="lnkbtnDeleteFavicon" runat="server" Text="Delete" ToolTip="Delete" OnCommand="lnkbtnDelete_Click" CausesValidation="false" CommandName="cmdDelete" CommandArgument="cmaFavicon"></asp:LinkButton>
                                    <br /><asp:Image ID="imgFavicon" runat="server" />
                                </asp:Panel>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="divECatalogEshopContainer" class="borderTop" runat="server" visible="false">
            <div id="divProductContainer">
                <table class="formTbl tblData" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <td colspan="2" class="tdSectionHdr">Product</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <asp:Label ID="lblRecIcon" runat="server"></asp:Label>
                                :</td>
                            <td>
                                <div class="divFileUpload">
                                    <div class="divFileUploadAction">
                                        <asp:FileUpload ID="fileRecIcon" runat="server" />
                                    </div>
                                    <span class="spanTooltip"><span class="icon"></span><span class="msg">
                                    <asp:Literal ID="litUploadRule2" runat="server"></asp:Literal>
                                    </span></span>
                                </div>
                                <asp:HiddenField ID="hdnRecIcon" runat="server" />
                                <asp:HiddenField ID="hdnRecIconRef" runat="server" />
                                <asp:CustomValidator ID="cvRecIcon" runat="server" ControlToValidate="fileRecIcon" CssClass="errmsg" Display="Dynamic" OnPreRender="cvImage_PreRender" OnServerValidate="validateImage_server"></asp:CustomValidator>
                                <asp:Panel ID="pnlRecIcon" runat="server" Visible="false">
                                    <br />
                                    <asp:LinkButton ID="lnkbtnDeleteRecIcon" runat="server" CausesValidation="false" CommandArgument="cmaRecIcon" CommandName="cmdDelete" OnCommand="lnkbtnDelete_Click" Text="Delete" ToolTip="Delete"></asp:LinkButton>
                                    <br />
                                    <asp:Image ID="imgRecIcon" runat="server" />
                                </asp:Panel>
                            </td>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <td>
                                <asp:Label ID="lblNewIcon" runat="server"></asp:Label>
                                :</td>
                            <td>
                                <div class="divFileUpload">
                                    <div class="divFileUploadAction">
                                        <asp:FileUpload ID="fileNewIcon" runat="server" />
                                    </div>
                                    <span class="spanTooltip"><span class="icon"></span><span class="msg">
                                    <asp:Literal ID="litUploadRule3" runat="server"></asp:Literal>
                                    </span></span>
                                </div>
                                <asp:HiddenField ID="hdnNewIcon" runat="server" />
                                <asp:HiddenField ID="hdnNewIconRef" runat="server" />
                                <asp:CustomValidator ID="cvNewIcon" runat="server" ControlToValidate="fileNewIcon" CssClass="errmsg" Display="Dynamic" OnPreRender="cvImage_PreRender" OnServerValidate="validateImage_server"></asp:CustomValidator>
                                <asp:Panel ID="pnlNewIcon" runat="server" Visible="false">
                                    <br />
                                    <asp:LinkButton ID="lnkbtnDeleteNewIcon" runat="server" CausesValidation="false" CommandArgument="cmaNewIcon" CommandName="cmdDelete" OnCommand="lnkbtnDelete_Click" Text="Delete" ToolTip="Delete"></asp:LinkButton>
                                    <br />
                                    <asp:Image ID="imgNewIcon" runat="server" />
                                </asp:Panel>
                            </td>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <td>Image Aspect Ratio:</td>
                            <td>
                                <input runat="server" id="txtProdRatioX"  min="1"/> : 
                                <input runat="server" id="txtProdRatioY"  min="1"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="divWatermarkContainer" class="borderTop" runat="server">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <td colspan="2" class="tdSectionHdr">Water Mark</td>
                        </tr>
                    </thead>
                    <%--Add by sh.chong 16Apr2015 for water mark purpose--%>
                <tr>
                    <td class="tdLabel">
                        <asp:Label ID="lblWaterMarkType" runat="server"></asp:Label>:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlWaterMarkType" runat="server"></asp:DropDownList>
                    </td>
                </tr>
                <tbody id="tbodyWatermarkOption" style="display:none;">
                <tr>
                    <td class="tdLabel">
                        <asp:Label ID="lblWaterMarkOpacity" runat="server">Transparent Percentage (%)</asp:Label>:
                    </td>
                    <td>
                        <input type="range" name="txtOpacity" id="txtOpacity" min="0.1" max="1" step ="0.1" value="0.1" onchange="txtOpacityChange(this.value);">
                        <label id="lblOpacity"></label>
                        <asp:HiddenField ID="hdnOpacity" runat="server"/>
                    </td>
                </tr>
                </tbody>
                <tbody ID="tbodyWatermarkText" style="display:none;">
                    <tr>
                        <td class="tdLabel">
                            <asp:Label ID="lblWaterMarkText" runat="server">Text</asp:Label>:
                        </td>
                        <td>
                            <asp:TextBox runat="server" type="text" id="txtWaterMark" />
                        </td>
                    </tr>
                </tbody>
                <tbody ID="tbodyWatermarkImage" style="display:none;">
                    <tr>
                        <td class="tdLabel">
                            <asp:Label ID="lblWaterMarkImage" runat="server">Image</asp:Label>:
                        </td>
                        <td>
                            <div class="divWatermarkFileUpload">
                                <div class="divFileUploadAction">
                                    <asp:FileUpload ID="fileWaterMark" runat="server" CssClass="fileWatermark"/>
                                </div>
                                <span class="spanTooltip" runat="server">
                                    <span class="icon"></span>
                                    <span class="msg">
                                        <asp:Literal ID="litUploadRule4" runat="server"></asp:Literal>
                                    </span>
                                </span>
                                <i class="material-icons icon-edit Watermark">crop</i>
                            </div>
                            <table width="100%" id="tblWatermarkCropImg" style="display:none;">
                                    <thead>
                                        <tr>
                                            <td colspan="2">
                                                <button type="button" class="btnDarkBlue" id="btnWatermarkConfirmCrop">Crop</button>
                                                <button type="button" class="btnDarkBlue" id="btnWatermarkCancelCrop">Cancel</button>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td width="70%">
                                                <div class="divWatermarkImgContainer" style="width: 100%;">
                                                    <img style="max-width:100%;width:100%;height:auto;"/>
                                                </div>
                                            </td>
                                            <td width="30%">
                                                <div class="divWatermarkImgPreview" style="overflow:hidden;height:200px;width:200px;border:1px solid #ececec;">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2">
                                                <input type="hidden" id="hdnWatermarkImgX" runat="server" data-id="hdnWatermarkImgX" class="hdnWatermarkImgX"/>
                                                <input type="hidden" id="hdnWatermarkImgY" runat="server" data-id="hdnWatermarkImgY" class="hdnWatermarkImgY"/>
                                                <input type="hidden" id="hdnWatermarkImgW" runat="server" data-id="hdnWatermarkImgW" class="hdnWatermarkImgW"/>
                                                <input type="hidden" id="hdnWatermarkImgH" runat="server" data-id="hdnWatermarkImgH" class="hdnWatermarkImgH"/>
                                                <input type="hidden" id="hdnWatermarkImgName" runat="server" data-id="hdnWatermarkImgName" class="hdnWatermarkImgName"/>
                                                <input type="hidden" id="hdnWatermarkImgCropped" runat="server" data-id="hdnWatermarkImgCropped" class="hdnWatermarkImgCropped"/>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            
                    
                            <asp:HiddenField ID="hdnWatermarkImage" runat="server" />
                            <asp:HiddenField ID="hdnWatermarkImageRef" runat="server" />
                            <asp:CustomValidator ID="cvWatermarkImage" runat="server" ControlToValidate="fileWaterMark" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateWatermarkImage_server" OnPreRender="cvWatermarkImage_PreRender"></asp:CustomValidator>
                            <asp:Panel ID="pnlWatermark" runat="server" Visible="false" CssClass="divWatermarkSavedImageContainer">
                                <br /><asp:LinkButton ID="lnkbtnDeleteWatermarkImage" runat="server" Text="Delete" ToolTip="Delete" OnCommand="lnkbtnDelete_Click" CausesValidation="false" CommandName="cmdDelete" CommandArgument="cmaWatermarkImage"></asp:LinkButton>
                                <br /><asp:Image ID="imgWatermarkImage" runat="server" />
                            </asp:Panel>
                       
                        </td>
                    </tr>
                </tbody>
                <%--End by sh.chong 16Apr2015 for water mark purpose--%>
                </table>
        </div>
        <div id="divCropImageContainer" class="borderTop" runat="server">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Crop Image Setting</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><asp:Label ID="lblAspectRatio" runat="server"></asp:Label>:</td>
                        <td>
                            <input runat="server" id="txtRatioX"  min="1"/> : 
                            <input runat="server" id="txtRatioY"  min="1"/>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="">
            <table id="tblAction" class="formTbl tblData" cellpadding="0" cellspacing="0" style="padding-top: 0;">
                <tr>
                    <td class="tdLabel"></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    
</asp:Panel>
