<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrDirIndMerchant.ascx.cs" Inherits="ctrl_ucUsrIndMember" %>

<asp:Panel ID="pnlIndMemberContainer" runat="server" CssClass="divIndMemberContainer">
    <asp:Panel ID="pnlIndMemberContent" runat="server" CssClass="divIndMemberContent">
        <asp:Literal ID="litIndMemContent" runat="server"></asp:Literal>
    </asp:Panel>
</asp:Panel>
