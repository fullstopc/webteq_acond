﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmSupplierDetails.ascx.cs" Inherits="ctrl_ucAdmSupplierDetails" %>

<asp:Panel ID="pnlSectForm6" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table cellpadding="0" cellspacing="0" border="0" class="formTbl">
            <tr>
                <td colspan="2" class="tdSectionHdr">Supplier Details</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Company:</td>
                <td class="tdMax"><asp:TextBox ID="txtCompany" runat="server" MaxLength="250" CssClass="text_medium"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel">Address:</td>
                <td class="tdMax"><asp:TextBox ID="txtAddress" runat="server" MaxLength="1000" CssClass="text"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Contact Person:</td>
                <td class="tdMax"><asp:TextBox ID="txtContactPerson" runat="server" MaxLength="250" CssClass="text_medium"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel">Tel:</td>
                <td class="tdMax"><asp:TextBox ID="txtTel" runat="server" MaxLength="20" CssClass="text_medium"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel">Fax:</td>
                <td class="tdMax"><asp:TextBox ID="txtFax" runat="server" MaxLength="20" CssClass="text_medium"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel">Email:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="250" CssClass="text"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="<br />Please enter valid Email." Display="Dynamic" ControlToValidate="txtEmail" CssClass="errmsg" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Shop No.:</td>
                <td class="tdMax"><asp:TextBox ID="txtShopNo" runat="server" MaxLength="20" CssClass="text_medium"></asp:TextBox></td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
        <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn" OnClick="lnkbtnSave_Click"></asp:LinkButton>
        <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn" CausesValidation="false"></asp:LinkButton>
    </asp:Panel>
</asp:Panel>
