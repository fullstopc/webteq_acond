﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucUsrMemberLogin : System.Web.UI.UserControl
{
    #region "Properties"
    public bool isMobile = false;
    #endregion

    #region "Property Methods"
    #endregion

    #region "Page Events"

    protected void lnkbtnLogin_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string email = txtEmailLogin.Text.Trim();
            string password = txtPwdLogin.Text.Trim();

            clsMember member = new clsMember();
            Dictionary<string, object> parameters = new Dictionary<string, object>()
            {
                { "mem_login_username" , email },
                { "mem_login_password", CryptorEngine.Encrypt(password, true) }
            };

            if (member.extractData(parameters))
            {
                Session[clsKey.SESSION_MEMBER_ID] = member.ID;
                Session[clsKey.SESSION_MEMBER_EMAIL] = member.email;
                Session[clsKey.SESSION_MEMBER_NAME] = member.name;

                Response.Redirect("bookingmb.aspx");
            }
            else
            {
                Session[clsKey.SESSION_MEMBER_ID] = null;
                Session[clsKey.SESSION_MEMBER_EMAIL] = null;
                Session[clsKey.SESSION_MEMBER_NAME] = null;
                pnlAck.Visible = true;
                litAck.Text = "Login fail. Please try again. ";
            }
        }
    }

    #endregion

    #region "Methods"
    public void fill()
    {
        hypForgot.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/forgotpassword.aspx";
        if (isMobile)
        {
            hypForgot.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/mobile/forgotpasswordmb.aspx";

            lnkbtnLogin.CssClass += " half";
            hypForgot.CssClass += " half";
        }
    }
    #endregion
}
