﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Collections;

public partial class ctrl_ucAdmDatabase : System.Web.UI.UserControl
{
    #region "Property Methods"
    protected string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ListItem listItem1 = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
            ddlSQLCommands.Items.Add(listItem1);
            ListItem listItem2 = new ListItem("SELECT", "1");
            ddlSQLCommands.Items.Add(listItem2);
            ListItem listItem3 = new ListItem("INSERT", "2");
            ddlSQLCommands.Items.Add(listItem3);
            ListItem listItem4 = new ListItem("UPDATE", "3");
            ddlSQLCommands.Items.Add(listItem4);
            ListItem listItem5 = new ListItem("DELETE", "4");
            ddlSQLCommands.Items.Add(listItem5);
            ListItem listItem6 = new ListItem("ALTER", "5");
            ddlSQLCommands.Items.Add(listItem6);
            ListItem listItem7 = new ListItem("CREATE", "6");
            ddlSQLCommands.Items.Add(listItem7);

            imgbtnTrans.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";

            Session["ACTIVATION"] = null;
        }
    }

    protected void lnkbtnNext_Click(object sender, EventArgs e)
    {
        for (int i = lstboxAllProjects.Items.Count - 1; i >= 0; i--)
        {
            if (lstboxAllProjects.Items[i].Selected == true)
            {
                lstboxSelectedProjects.Items.Add(lstboxAllProjects.Items[i]);
                ListItem li = lstboxAllProjects.Items[i];
                lstboxAllProjects.Items.Remove(li);
            }
        }

        litRecSelected.Text = lstboxSelectedProjects.Items.Count.ToString() + " record(s) selected.";
    }

    protected void lnkbtnPrev_Click(object sender, EventArgs e)
    {
        for (int i = lstboxSelectedProjects.Items.Count - 1; i >= 0; i--)
        {
            if (lstboxSelectedProjects.Items[i].Selected == true)
            {
                lstboxAllProjects.Items.Add(lstboxSelectedProjects.Items[i]);
                ListItem li = lstboxSelectedProjects.Items[i];
                lstboxSelectedProjects.Items.Remove(li);
            }
        }

        litRecSelected.Text = lstboxSelectedProjects.Items.Count.ToString() + " record(s) selected.";
    }

    protected void chkboxAllProjects_CheckedChanged(object sender, EventArgs e)
    {
        if (chkboxAllProjects.Checked)
        {
            lstboxAllProjects.Enabled = false;
            lstboxSelectedProjects.Enabled = false;
            lnkbtnNext.Enabled = false;
            lnkbtnPrev.Enabled = false;
        }
        else
        {
            lstboxAllProjects.Enabled = true;
            lstboxSelectedProjects.Enabled = true;
            lnkbtnNext.Enabled = true;
            lnkbtnPrev.Enabled = true;
        }        
    }

    protected void txtSQLScript_TextChanged(object sender, EventArgs e)
    {
        pnlAck.Visible = false;
        litAck.Text = "";
    }

    protected void lnkbtnClearAll_Click(object sender, EventArgs e)
    {
        for (int i = lstboxSelectedProjects.Items.Count - 1; i >= 0; i--)
        {
            lstboxAllProjects.Items.Add(lstboxSelectedProjects.Items[i]); 
        }

        lstboxSelectedProjects.Items.Clear();
    }

    protected void validateSQLScript_server(object source, ServerValidateEventArgs args)
    {
        string strSqlScript = txtSQLScript.Text.Trim();

        string strFirstBracket = strSqlScript.Substring(0, 3);

        string strQuery = GetSubstringByString(clsAdmin.CONSTDBCHAR1, clsAdmin.CONSTDBCHAR2, strSqlScript);

        int intStartLength = 0;
        if (strQuery.Length > 0)
        {
            intStartLength = strFirstBracket.Length + strQuery.Length;
        }

        string strSecondbracket = strSqlScript.Substring(intStartLength, 3);

        string strEndChar = strSqlScript.Substring(strSqlScript.Length - 3);

        if (strFirstBracket == clsAdmin.CONSTDBCHAR1 && strSecondbracket == clsAdmin.CONSTDBCHAR2 && strEndChar == clsAdmin.CONSTDBCHAR3)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
            cvSQLScript.ErrorMessage = "<br />Please enter valid SQL command. (e.g <|*SELECT*|> * FROM TABLE ;;;)";
        }
    }

    protected void validateSQLCommands_server(object source, ServerValidateEventArgs args)
    {
        if (Page.IsValid)
        {
            string strSqlScript = txtSQLScript.Text.Trim();

            if (!string.IsNullOrEmpty(strSqlScript))
            {
                string strResult = GetSubstringByString(clsAdmin.CONSTDBCHAR1, clsAdmin.CONSTDBCHAR2, strSqlScript);

                if (strResult == ddlSQLCommands.SelectedItem.Text.ToUpper())
                {
                    args.IsValid = true;
                }
                else
                {
                    args.IsValid = false;
                    cvSQLCommands.ErrorMessage = "SQL command doesn't match.";
                }
            }
        }
    }

    public string GetSubstringByString(string a, string b, string c)
    {
        string strResult = "";
        if (((c.IndexOf(a)) >= 0) && ((c.IndexOf(b)) >= 0))
        {
            strResult = c.Substring((c.IndexOf(a) + a.Length), (c.IndexOf(b) - c.IndexOf(a) - a.Length));
        }

        return strResult;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strSqlScript = txtSQLScript.Text.Trim();
      
            string strFirstChars = strSqlScript.Substring(0, 3);

            string strSQLQuery = GetSubstringByString(clsAdmin.CONSTDBCHAR1, clsAdmin.CONSTDBCHAR2, strSqlScript);

            int intStartLength = 0;
            if (strSQLQuery.Length > 0)
            {
                intStartLength = strFirstChars.Length + strSQLQuery.Length;
            }

            string strSecondChars = strSqlScript.Substring(intStartLength, 3);

            int intTotLength = strFirstChars.Length + strSQLQuery.Length + strSecondChars.Length;

            string strScript = strSqlScript.Substring(intTotLength);
            string strScript2 = strScript.Substring(0, strScript.Length - 2);
            string strFinalScript = strSQLQuery + strScript2;

            clsProject proj = new clsProject();
            int strRecordAffected = 0;

            litAck.Text = "Result:<br/>";

            if (chkboxAllProjects.Checked)
            {
                for (int i = lstboxAllProjects.Items.Count - 1; i >= 0; i--)
                {
                    int intProjId = Convert.ToInt16(lstboxAllProjects.Items[i].Value);

                    Session[clsAdmin.CONSTADMINCS] = 1;
                    if (proj.extractProjectById3(intProjId, 0))
                    {
                        Session[clsAdmin.CONSTPROJECTNAME] = proj.projName;
                        Session[clsAdmin.CONSTPROJECTDRIVER] = proj.projDriverName;
                        Session[clsAdmin.CONSTPROJECTSERVER] = proj.projServer;
                        Session[clsAdmin.CONSTPROJECTUSERID] = proj.projUserId;
                        Session[clsAdmin.CONSTPROJECTPASSWORD] = proj.projPwd;
                        Session[clsAdmin.CONSTPROJECTDATABASE] = proj.projDatabase;
                        Session[clsAdmin.CONSTPROJECTOPTION] = proj.projOption;
                        Session[clsAdmin.CONSTPROJECTPORT] = proj.projPort;
                    }

                    Session[clsAdmin.CONSTADMINCS] = null;

                    strRecordAffected = proj.SQLScript(strFinalScript);

                    if (Session[clsAdmin.CONSTPROJECTNAME] != null && Session[clsAdmin.CONSTPROJECTNAME] != "")
                    {
                        if (strRecordAffected == 0 || strRecordAffected == 1)
                        {
                            clsLog.logErroMsg(Session[clsAdmin.CONSTPROJECTNAME].ToString() + " > " + strRecordAffected + " row");
                            litAck.Text += "<b>" + Session[clsAdmin.CONSTPROJECTNAME].ToString() + "</b>" + " > " + strRecordAffected + " row" + (!string.IsNullOrEmpty(proj.ErrorMsg) ? "<br/>(" + proj.ErrorMsg + ")" : "") + "<br/>";
                        }
                        else if (strRecordAffected > 1)
                        {
                            clsLog.logErroMsg(Session[clsAdmin.CONSTPROJECTNAME].ToString() + " > " + strRecordAffected + " rows");
                            litAck.Text += "<b>" + Session[clsAdmin.CONSTPROJECTNAME].ToString() + "</b>" + " > " + strRecordAffected + " rows" + (!string.IsNullOrEmpty(proj.ErrorMsg) ? "<br/>(" + proj.ErrorMsg + ")" : "") + "<br/>";
                        }
                        
                        pnlAck.Visible = true;
                    }
                }
            }

            for (int i = lstboxSelectedProjects.Items.Count - 1; i >= 0; i--)
            {
                int intProjId = Convert.ToInt16(lstboxSelectedProjects.Items[i].Value);

                Session[clsAdmin.CONSTADMINCS] = 1;
                if (proj.extractProjectById3(intProjId, 0))
                {
                    Session[clsAdmin.CONSTPROJECTNAME] = proj.projName;
                    Session[clsAdmin.CONSTPROJECTDRIVER] = proj.projDriverName;
                    Session[clsAdmin.CONSTPROJECTSERVER] = proj.projServer;
                    Session[clsAdmin.CONSTPROJECTUSERID] = proj.projUserId;
                    Session[clsAdmin.CONSTPROJECTPASSWORD] = proj.projPwd;
                    Session[clsAdmin.CONSTPROJECTDATABASE] = proj.projDatabase;
                    Session[clsAdmin.CONSTPROJECTOPTION] = proj.projOption;
                    Session[clsAdmin.CONSTPROJECTPORT] = proj.projPort;
                }

                Session[clsAdmin.CONSTADMINCS] = null;

                strRecordAffected = proj.SQLScript(strFinalScript);

                if (Session[clsAdmin.CONSTPROJECTNAME] != null && Session[clsAdmin.CONSTPROJECTNAME] != "")
                {
                    if (strRecordAffected == 0 || strRecordAffected == 1)
                    {
                        clsLog.logErroMsg(Session[clsAdmin.CONSTPROJECTNAME].ToString() + " > " + strRecordAffected + " row");
                        litAck.Text += "<b>" + Session[clsAdmin.CONSTPROJECTNAME].ToString() + "</b>" + " > " + strRecordAffected + " row" + (!string.IsNullOrEmpty(proj.ErrorMsg) ? "<br/>(" + proj.ErrorMsg + ")" : "") + "<br/>";
                    }
                    else if (strRecordAffected > 1)
                    {
                        clsLog.logErroMsg(Session[clsAdmin.CONSTPROJECTNAME].ToString() + " > " + strRecordAffected + " rows");
                        litAck.Text += "<b>" + Session[clsAdmin.CONSTPROJECTNAME].ToString() + "</b>" + " > " + strRecordAffected + " rows" + (!string.IsNullOrEmpty(proj.ErrorMsg) ? "<br/>(" + proj.ErrorMsg + ")" : "") + "<br/>";
                    }

                    pnlAck.Visible = true;
                }
            }

            Session[clsAdmin.CONSTADMINCS] = 1;
        }
    }

    protected void lnkbtnGo_Click(object sender, EventArgs e)
    {
        Session["ACTIVATION"] = null;
        Response.Redirect(currentPageName);
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        Session["ACTIVATION"] = null;
    } 

    protected void btnTransImg_Click(object sender, EventArgs e)
    {
        Session["ACTIVATION"] = 1;

        setPageProperties();
    }   
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        if (Session["ACTIVATION"] != null && Session["ACTIVATION"] != "")
        {
            clsProject proj = new clsProject();
            DataSet ds = new DataSet();
            ds = proj.getProjectList(0);
            DataView dv = new DataView(ds.Tables[0]);
            dv.RowFilter = "1 = 1";
            dv.Sort = "PROJ_NAME ASC";

            lstboxAllProjects.DataSource = dv;
            lstboxAllProjects.DataTextField = "V_PROJNAME";
            lstboxAllProjects.DataValueField = "PROJ_ID";
            lstboxAllProjects.DataBind();

            lnkbtnBack.Enabled = true;
        }        
    }
    #endregion
}
