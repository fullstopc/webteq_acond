﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucAdmDetail : System.Web.UI.UserControl
{
    #region "Properties"
    protected string _header;
    protected string _title;
    protected string _titleJp;
    protected string _titleMs;
    protected string _titleZh;
    protected string _shortDesc;
    protected string _shortDescJp;
    protected string _shortDescMs;
    protected string _shortDescZh;
    protected Boolean _boolShowShortDesc = true;
    protected Boolean _boolShowShortDescJp = true;
    protected Boolean _boolShowShortDescMs = true;
    protected Boolean _boolShowShortDescZh = true;
    protected Boolean _boolShowDesc;

    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public string header
    {
        get { return _header; }
        set { _header = value; }
    }

    public string title
    {
        get { return _title; }
        set { _title = value; }
    }

    public string titleJp
    {
        get { return _titleJp; }
        set { _titleJp = value; }
    }

    public string titleMs
    {
        get { return _titleMs; }
        set { _titleMs = value; }
    }

    public string titleZh
    {
        get { return _titleZh; }
        set { _titleZh = value; }
    }

    public string shortDesc
    {
        get { return _shortDesc; }
        set { _shortDesc = value; }
    }

    public string shortDescJp
    {
        get { return _shortDescJp; }
        set { _shortDescJp = value; }
    }

    public string shortDescMs
    {
        get { return _shortDescMs; }
        set { _shortDescMs = value; }
    }

    public string shortDescZh
    {
        get { return _shortDescZh; }
        set { _shortDescZh = value; }
    }

    public Boolean boolShowShortDesc
    {
        get { return _boolShowShortDesc; }
        set { _boolShowShortDesc = value; }
    }

    public Boolean boolShowShortDescJp
    {
        get { return _boolShowShortDescJp; }
        set { _boolShowShortDescJp = value; }
    }

    public Boolean boolShowShortDescMs
    {
        get { return _boolShowShortDescMs; }
        set { _boolShowShortDescMs = value; }
    }

    public Boolean boolShowShortDescZh
    {
        get { return _boolShowShortDescZh; }
        set { _boolShowShortDescZh = value; }
    }

    public Boolean boolShowDesc
    {
        get { return _boolShowDesc; }
        set { _boolShowDesc = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            fillForm();
            checkLanguage();
        }
    }

    protected void fillForm()
    {
        trHeader.Visible = trHeaderSpace.Visible = !string.IsNullOrEmpty(header);

        litHeader.Text = header;
        litTitle.Text = title;
        litTitleJp.Text = titleJp;
        litTitleMs.Text = titleMs;
        litTitleZh.Text = titleZh;
        litShortDesc.Text = !string.IsNullOrEmpty(shortDesc) ? shortDesc : "<span class=\"spanItalic\">" + GetGlobalResourceObject("GlobalResource", "statusDefaultMessage.Text").ToString() + "</span>";
        litShortDescJp.Text = shortDescJp;
        litShortDescMs.Text = shortDescMs;
        litShortDescZh.Text = shortDescZh;

        if (boolShowShortDesc && boolShowDesc) { trShortDesc.Visible = true; }
        else { trShortDesc.Visible = false; }
    }

    protected void checkLanguage()
    {
        if (clsMis.isLangExist(clsAdmin.CONSTLANGJAPANESE))
        {
            trTitleJp.Visible = true;

            if (boolShowShortDescJp && boolShowDesc) { trShortDescJp.Visible = true; }
            else { trShortDescJp.Visible = false; }
        }
        if (clsMis.isLangExist(clsAdmin.CONSTLANGMALAY))
        {
            trTitleMs.Visible = true;

            if (boolShowShortDescMs && boolShowDesc) { trShortDescMs.Visible = true; }
            else { trShortDescMs.Visible = false; }
        }
        if (clsMis.isLangExist(clsAdmin.CONSTLANGCHINESE))
        {
            trTitleZh.Visible = true;

            if (boolShowShortDescZh && boolShowDesc) { trShortDescZh.Visible = true; }
            else { trShortDescZh.Visible = false; }
        }
    }
    #endregion
}
