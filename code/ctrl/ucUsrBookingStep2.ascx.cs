﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Collections;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using Newtonsoft.Json;

public partial class ctrl_ucUsr : System.Web.UI.UserControl
{
    #region "Property"
    protected Dictionary<string, object> dictReturn;
    #endregion
    
    public void fill()
    {
        Reservation reservation = Session[clsKey.SESSION_RESERVATION] as Reservation;
        if(reservation != null)
        {
            litAddrUnit.Text = reservation.config.propertyUnitno;
            litItemQty.Text = reservation.config.serviceItemQty.ToString();
        }

        clsWorkingConfigTimeslot timeslot = new clsWorkingConfigTimeslot();

        dictReturn = new Dictionary<string, object>()
        {
            { "holidays", new clsWorkingConfigHoliday()
                            .getDataTable()
                            .AsEnumerable()
                            .Select(x => new clsWorkingConfigHoliday() { row = x }).ToList() },
            { "timeslots", timeslot
                            .getDataTable()
                            .AsEnumerable()
                            .Select(x => new clsWorkingConfigTimeslot() { row =x })
                            .ToList() },
            { "week", new clsWorkingConfigWeek()
                            .getDataTable()
                            .AsEnumerable()
                            .Select(x => new clsWorkingConfigWeek() { row =x })
                            .ToList() },
            { "reservation", Session[clsKey.SESSION_RESERVATION] as Reservation },
            { "reserved_list", new clsReservation().getDataTable()
                                                    .AsEnumerable()
                                                    .Select(x => new clsReservation() { row = x })
                                                    .ToList() },
            { "agents", new clsEmployee().getDataTable()
                                        .AsEnumerable()
                                        .Select(x => new clsEmployee() { row = x })
                                        .Where(x => x.active == 1)
                                        .ToList() },
            { "leaves", new clsEmployeeLeave().getDataTable()
                                        .AsEnumerable()
                                        .Select(x => new clsEmployeeLeave() { row = x })
                                        .ToList() },
            { "config", new Dictionary<string, object>()
            {
                { "working_total_hour",timeslot.getTotalWorkingHour() },
                { "working_free_hour",timeslot.getTotalFreeHour() },
                { "working_hour",timeslot.getWorkingHour() },
                { "working_end",timeslot.getLatestTimeslotEnd() },
                { "lunchtime_start",timeslot.getBreaktimeStart() },
                { "lunchtime_end",timeslot.getBreaktimeEnd() },
                { "lunchtime_total_hour",timeslot.getBreaktimeHour() },
            } }
        };
    }


    protected void lnkbtnSubmit_Click(object sender, EventArgs e)
    {
        Reservation reservation = Session[clsKey.SESSION_RESERVATION] as Reservation;
        if (Page.IsValid)
        {
            if (reservation != null)
            {
                Dictionary<string, object> ServiceConfig = JsonConvert.DeserializeObject<Dictionary<string, object>>(hdnServiceConfig.Value);

                int[] agents = JsonConvert.DeserializeObject<int[]>(ServiceConfig["agents"].ToString());
                Random random = new Random();

                reservation.config.serviceDatetimeFrom = Convert.ToDateTime(ServiceConfig["from"]);
                reservation.config.serviceDatetimeTo = Convert.ToDateTime(ServiceConfig["to"]);
                reservation.config.employeeID = agents[random.Next(0, agents.Length - 1)];
                reservation.step = 3;
            }

            Session[clsKey.SESSION_RESERVATION] = reservation;

            NameValueCollection parameters = HttpUtility.ParseQueryString(Request.QueryString.ToString());
            parameters["step"] = "3";
            string url = Request.Url.AbsolutePath + "?" + parameters.ToString();
            Response.Redirect(url);
        }
    }
}
