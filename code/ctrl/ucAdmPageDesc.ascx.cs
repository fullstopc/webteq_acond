﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucAdmPageDesc : System.Web.UI.UserControl
{
    #region "Properties"
    private string _desc = "";
    #endregion


    #region "Property Methods"
    public string desc
    {
        get { return _desc; }
        set { _desc = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "admpagedesc.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            litCompulsory.Text = GetGlobalResourceObject("GlobalResource", "contentCompulsory.Text").ToString();
            litUnique.Text = GetGlobalResourceObject("GlobalResource", "contentUnique.Text").ToString();
            litDesc.Text = desc;
        }
    }
    protected void Page_LoadComplete(object sender, EventArgs e)
    {

    }
    #endregion
}
