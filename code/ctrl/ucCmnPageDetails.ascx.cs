﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Collections;

public partial class ctrl_ucCmnPageDetails : System.Web.UI.UserControl
{
    #region "Enum"
    public enum View
    {
        Admin = 1,
        User = 2
    }

    public enum Mode
    {
        View = 1,
        Edit = 2
    }
    #endregion


    #region "Properties"
    protected View _viewType = View.Admin;
    protected Mode _modeType = Mode.View;
    protected Boolean _enableLanguage = false;
    protected Boolean _enableCorrespondPage = false;
    protected int _id = 0;
    protected string _redirectTarget = "";
    #endregion


    #region "Property Methods"
    public View viewType
    {
        get
        {
            if (ViewState["VIEW"] == null)
            {
                return _viewType;
            }
            else
            {
                return (View)ViewState["VIEW"];
            }
        }
        set { ViewState["VIEW"] = value; }
    }

    public Mode modeType
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _modeType;
            }
            else
            {
                return (Mode)ViewState["MODE"];
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageName
    {
        get { return txtPageName.Text.Trim(); }
        set
        {
            txtPageName.Text = value;
            litPageName.Text = value;
        }
    }

    public string pageDisplayName
    {
        get { return txtPageDisplayName.Text.Trim(); }
        set
        {
            txtPageDisplayName.Text = value;
            litPageDisplayName.Text = value;
        }
    }

    public string pageTitle
    {
        get { return txtPageTitle.Text.Trim(); }
        set
        {
            txtPageTitle.Text = value;
            litPageTitle.Text = value;
        }
    }

    public string pageTitleFriendlyUrl
    {
        get { return txtPageTitleFriendlyURL.Text.Trim(); }
        set
        {
            txtPageTitleFriendlyURL.Text = value;
            litPageTitleFriendlyUrl.Text = value;
        }
    }

    public string template
    {
        get { return ddlTemplate.SelectedValue; }
        set
        {
            ddlTemplate.SelectedValue = value;
            litTemplate.Text = value;
        }
    }

    public Boolean templateParent
    {
        get { return chkboxTemplate.Checked; }
        set { chkboxTemplate.Checked = value; }
    }

    public int parent
    {
        get { return !string.IsNullOrEmpty(ddlParent.SelectedValue) ? Convert.ToInt16(ddlParent.SelectedValue) : 0; }
        set
        {
            ddlParent.SelectedValue = value.ToString();
            if (Convert.ToInt16(value) > 0)
            {
                chkboxTemplate.Visible = true;
            }
            //if (value <= 0)
            //{
            //    ddlParent.SelectedValue = value.ToString();
            //    chkboxTemplate.Visible = true;
            //}
        }
    }

    public string parentText
    {
        set { litParent.Text = value; }
    }

    public string language
    {
        get { return ddlLanguage.SelectedValue; }
        set{ ddlLanguage.SelectedValue = value;
            litLanguage.Text = value; }
    }

    public ArrayList alRemain
    {
        get
        {
            ArrayList alSel = new ArrayList();

            foreach (ListItem lstItem in lstboxPages.Items)
            {
                alSel.Add(lstItem.Value);
            }

            return alSel;
        }

    }

    public ArrayList alCorrespond
    {
        get
        {
            ArrayList alSel = new ArrayList();

            foreach (ListItem lstItem in lstboxPageCorrespond.Items)
            {
                alSel.Add(lstItem.Value);
            }

            return alSel;
        }
    }

    public DataSet dsCorrespond
    {
        set
        {
            DataView dv = new DataView(value.Tables[0]);
            dv.Sort = "V_PAGETITLE ASC";

            lstboxPageCorrespond.DataSource = dv;
            lstboxPageCorrespond.DataTextField = "V_PAGETITLE";
            lstboxPageCorrespond.DataValueField = "PC_PAGEID";
            lstboxPageCorrespond.DataBind();

            foreach (ListItem lstItem in lstboxPageCorrespond.Items)
            {
                lstboxPages.Items.Remove(lstboxPages.Items.FindByValue(lstItem.Value));
            }
        }
    }

    public Boolean showPageTitle
    {
        get { return chkboxShowPageTitle.Checked; }
        set
        {
            chkboxShowPageTitle.Checked = value;
            litShowPageTitle.Text = clsMis.formatYesNo(value);
        }
    }

    public Boolean showInMenu
    {
        get { return chkboxShowInMenu.Checked; }
        set
        {
            chkboxShowInMenu.Checked = value;
            litShowInMenu.Text = clsMis.formatYesNo(value);
        }
    }

    public Boolean showTop
    {
        get { return chkboxTopMenu.Checked; }
        set { chkboxTopMenu.Checked = value; }
    }

    public Boolean showBottom
    {
        get { return chkboxBottomMenu.Checked; }
        set { chkboxBottomMenu.Checked = value; }
    }

    public Boolean showInBreadcrumb
    {
        get { return chkboxShowInBreadcrumb.Checked; }
        set
        {
            chkboxShowInBreadcrumb.Checked = value;
            litShowInBreadcrumb.Text = clsMis.formatYesNo(value);
        }
    }

    public Boolean userEditable
    {
        get { return chkboxUserEditable.Checked; }
        set 
        {
            chkboxUserEditable.Checked = value;
            litUserEditable.Text = clsMis.formatYesNo(value);
        }
    }

    public string cssClass
    {
        get { return ddlCssClass.SelectedValue; }
        set
        {
            ddlCssClass.SelectedValue = value;
            litCSSClass.Text = value;
        }
    }
    public string slideShowGroup
    {
        get { return ddlSlideShowGroup.SelectedValue; }
        set
        {
            ddlSlideShowGroup.SelectedValue = value;
            litSlideShowGroup.Text = value;
        }
    }
    public string externalLink
    {
        get { return txtLink.Text.Trim(); }
        set
        {
            txtLink.Text = value;
            litLink.Text = value;
        }
    }

    public string target
    {
        get { return ddlTarget.SelectedValue; }
        set{ ddlTarget.SelectedValue = value;
            litTarget.Text = value; }
    }

    public string targetText
    {
        set { litTarget.Text = value; }
    }

    public string keyword
    {
        get { return txtKeyword.Text.Trim(); }
        set
        {
            txtKeyword.Text = value;
            litKeyword.Text = value;
        }
    }

    public Boolean defaultKeyword
    {
        get { return chkboxKeyword.Checked; }
        set { chkboxKeyword.Checked = value; }
    }

    public string desc
    {
        get { return txtDesc.Text.Trim(); }
        set
        {
            txtDesc.Text = value;
            litDesc.Text = value;
        }
    }

    public Boolean defaultDesc
    {
        get { return chkboxDesc.Checked; }
        set { chkboxDesc.Checked = value; }
    }

    public Boolean enableLink
    {
        get { return chkboxEnableLink.Checked; }
        set 
        {
            chkboxEnableLink.Checked = value;
            litEnableLink.Text = clsMis.formatYesNo(value);
        }
    }

    public Boolean showInSitemap
    {
        get { return chkboxShowInSitemap.Checked; }
        set
        {
            chkboxShowInSitemap.Checked = value;
            litShowInSitemap.Text = clsMis.formatYesNo(value);
        }
    }

    public Boolean pageAuthorize
    {
        get { return chkboxAuthorize.Checked; }
        set 
        {
            chkboxAuthorize.Checked = value;
            litAuthorize.Text = clsMis.formatYesNo(value);
        }
    }

    public Boolean active
    {
        get { return chkboxActive.Checked; }
        set
        {
            chkboxActive.Checked = value;
            litActive.Text = clsMis.formatYesNo(value);
        }
    }

    public int redirect
    {
        get { return !string.IsNullOrEmpty(ddlRedirect.SelectedValue) ? Convert.ToInt16(ddlRedirect.SelectedValue) : 0; }
        set
        {
            if (value > 0) { ddlRedirect.SelectedValue = value.ToString(); }

            if (ddlRedirect.SelectedValue == clsAdmin.CONSTEXTERNALLINK.ToString())
            {
                pnlExtLink.Visible = true;
            }
        }
    }

    public string redirectText
    {
        set { litRedirect.Text = value; }
    }

    public Boolean enableLanguage
    {
        get
        {
            if (ViewState["ENABLELANGUAGE"] == null)
            {
                return _enableLanguage;
            }
            else
            {
                return Convert.ToBoolean(ViewState["ENABLELANGUAGE"]);
            }
        }
        set { ViewState["ENABLELANGUAGE"] = value; }
    }

    public Boolean enableCorrespondPage
    {
        get
        {
            if (ViewState["ENABLECORRESPONDPAGE"] == null)
            {
                return _enableCorrespondPage;
            }
            else
            {
                return Convert.ToBoolean(ViewState["ENABLECORRESPONDPAGE"]);
            }
        }
        set { ViewState["ENABLECORRESPONDPAGE"] = value; }
    }

    public int pageId
    {
        get
        {
            if (ViewState["ID"] == null)
            {
                return _id;
            }
            else
            {
                return Convert.ToInt16(ViewState["ID"]);
            }
        }
        set { ViewState["ID"] = value; }
    }

    public string redirectTarget
    {
        get
        {
            if (ViewState["TARGET"] == null)
            {
                return _redirectTarget;
            }
            else
            {
                return ViewState["TARGET"].ToString();
            }
        }
        set { ViewState["TARGET"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "pagedetails.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);
    }

    protected void validatePageTitleFriendlyUrl_server(object source, ServerValidateEventArgs args)
    {
        clsPage page = new clsPage();
        if (page.isPageTitleFriendlyUrlExist(txtPageTitleFriendlyURL.Text, pageId))
        {
            args.IsValid = false;
            cvPageTitleFriendlyUrl.ErrorMessage = "This " + txtPageTitleFriendlyURL.Text + " is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvPageTitleFriendlyUrl_PreRender(object source, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("pagetitlefriendlyurl")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtPageTitleFriendlyURL.ClientID + "', document.all['" + cvPageTitleFriendlyUrl.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "pagetitlefriendlyurl", strJS);
        }
    }

    protected void ddlParent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(ddlParent.SelectedValue) || Convert.ToInt16(ddlParent.SelectedValue) <= 0)
        {
            clsMis mis = new clsMis();
            DataSet ds = mis.getListByListGrp("TOP MENU CSS", 1);
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = "LIST_ORDER ASC";

            ddlCssClass.DataSource = dv;
            ddlCssClass.DataTextField = "LIST_NAME";
            ddlCssClass.DataValueField = "LIST_VALUE";
            ddlCssClass.DataBind();

            ddlCssClass.Enabled = true;

            chkboxTemplate.Visible = false;
        }
        else
        {
            chkboxTemplate.Visible = true;
            ddlCssClass.Items.Clear();
            ddlCssClass.Enabled = false;
        }

        ListItem ddlCssClassDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlCssClass.Items.Insert(0, ddlCssClassDefaultItem);
    }

    protected void ddlRedirect_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlRedirect.SelectedValue == clsAdmin.CONSTEXTERNALLINK.ToString())
        {
            pnlExtLink.Visible = true;
        }
        else
        {
            pnlExtLink.Visible = false;
        }
    }

    protected void ddlTarget_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void chkboxShowInMenu_CheckedChanged(object sender, EventArgs e)
    {
        pnlShowInMenu.Visible = chkboxShowInMenu.Checked;
    }

    protected void lstboxPages_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListItem lstboxItemSelected = new ListItem(lstboxPages.SelectedItem.Text, lstboxPages.SelectedValue);
        lstboxPageCorrespond.Items.Add(lstboxItemSelected);
        lstboxPages.Items.Remove(lstboxItemSelected);
    }

    protected void lstboxPageCorrespond_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListItem lstboxItemSelected = new ListItem(lstboxPageCorrespond.SelectedItem.Text, lstboxPageCorrespond.SelectedValue);
        lstboxPageCorrespond.Items.Remove(lstboxItemSelected);
        lstboxPages.Items.Add(lstboxItemSelected);
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        setPageProperties();
    }

    protected void setPageProperties()
    {
        if (Session[clsAdmin.CONSTPROJECTFRIENDLYURL] != null && clsAdmin.CONSTPROJECTFRIENDLYURL != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFRIENDLYURL]) == 0)
            {
                trFriendlyURL.Visible = false;
            }
        }

        DataSet ds = new DataSet();
        DataView dv;
        DataTable dt = new DataTable();

        clsMis mis = new clsMis();
        clsPage page = new clsPage();

        ds = mis.getListByListGrp("PAGE TEMPLATE", 1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "LIST_ORDER ASC";

        ddlTemplate.DataSource = dv;
        ddlTemplate.DataTextField = "LIST_NAME";
        ddlTemplate.DataValueField = "LIST_VALUE";
        ddlTemplate.DataBind();

        ListItem ddlTemplateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlTemplate.Items.Insert(0, ddlTemplateDefaultItem);

        ds = mis.getListByListGrp("LANGUAGE", 1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "LIST_ORDER ASC";

        ddlLanguage.DataSource = dv;
        ddlLanguage.DataTextField = "LIST_NAME";
        ddlLanguage.DataValueField = "LIST_VALUE";
        ddlLanguage.DataBind();

        ListItem ddlLanguageDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlLanguage.Items.Insert(0, ddlLanguageDefaultItem);

        clsMasthead masthead = new clsMasthead();
        ds = masthead.getGrpList(1);
        dv = new DataView(ds.Tables[0]);

        ddlSlideShowGroup.DataSource = dv;
        ddlSlideShowGroup.DataTextField = "GRP_DNAME";
        ddlSlideShowGroup.DataValueField = "GRP_ID";
        ddlSlideShowGroup.DataBind();

        ListItem ddlSlideShowGroupItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlSlideShowGroup.Items.Insert(0, ddlSlideShowGroupItem);

        page.extractPageById(pageId, 1);

        if (page.parent <= 0)
        {
            ds = mis.getListByListGrp("TOP MENU CSS", 1);
            
            dv = new DataView(ds.Tables[0]);
            
            dv.Sort = "LIST_ORDER ASC";

            ddlCssClass.DataSource = dv;
            ddlCssClass.DataTextField = "LIST_NAME";
            ddlCssClass.DataValueField = "LIST_VALUE";
            ddlCssClass.DataBind();
        }
        else
        {
            ddlCssClass.Enabled = false;
        }

        ListItem ddlCssClassDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlCssClass.Items.Insert(0, ddlCssClassDefaultItem);

        //top menu root
        dt.Columns.Add("ROOT_NAME", typeof(string));
        dt.Columns.Add("ROOT_VALUE", typeof(string));
        string strRootName;
        string strRootValue;

        ds = new DataSet();
        ds = mis.getListByListGrp("TOP MENU ROOT", 1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "LIST_VALUE ASC";

        if(dv.Count > 0)
        {
            int intLimit = 0;
            int intCount = 0;

            if (Session[clsAdmin.CONSTPROJECTTOPMENUROOT] != null && clsAdmin.CONSTPROJECTTOPMENUROOT != "")
            {
                intLimit = Convert.ToInt16(Session[clsAdmin.CONSTPROJECTTOPMENUROOT]);
            }

            foreach (DataRow row in dv.ToTable().Rows)
            {
                if (intCount < intLimit)
                {
                    strRootName = row["LIST_NAME"].ToString();
                    strRootValue = row["LIST_VALUE"].ToString();

                    dt.Rows.Add(strRootName, strRootValue);
                }
                else { break; }
                intCount++;
            }
        }

        ds = new DataSet();
        ds = page.getPageList(0);
        dv = new DataView(ds.Tables[0]);
        dv.RowFilter = "1 = 1";
        if (pageId > 0) { dv.RowFilter += " AND PAGE_ID <> " + pageId; }
        dv.Sort = "PAGE_TITLE ASC";

        if (dv.Count > 0)
        {
            foreach (DataRow row in dv.ToTable().Rows)
            {
                strRootName = row["PAGE_TITLE"].ToString();
                strRootValue = row["PAGE_ID"].ToString();

                dt.Rows.Add(strRootName, strRootValue);
            }
        }
        
        ddlParent.DataSource = dt;
        ddlParent.DataTextField = "ROOT_NAME";
        ddlParent.DataValueField = "ROOT_VALUE";
        ddlParent.DataBind();
        //end top menu root

        dv.RowFilter += " AND PAGE_OTHER = 0";
        dv.Sort = "PAGE_TITLE ASC";

        ddlRedirect.DataSource = dv;
        ddlRedirect.DataTextField = "PAGE_DISPLAYNAME";
        ddlRedirect.DataValueField = "PAGE_ID";
        ddlRedirect.DataBind();

        lstboxPages.DataSource = dv;
        lstboxPages.DataTextField = "PAGE_TITLE";
        lstboxPages.DataValueField = "PAGE_ID";
        lstboxPages.DataBind();

        ds = new DataSet();
        ds = mis.getListByListGrp("TARGET", 1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "LIST_VALUE ASC";

        ddlTarget.DataSource = dv;
        ddlTarget.DataTextField = "LIST_NAME";
        ddlTarget.DataValueField = "LIST_VALUE";
        ddlTarget.DataBind();

        //ListItem ddlParentDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlRoot.Text").ToString(), "");
        //ddlParent.Items.Insert(0, ddlParentDefaultItem);

        //ListItem ddlParentDefault2Item = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlRoot2.Text").ToString(), clsAdmin.CONSTPARENTROOT2);
        //ddlParent.Items.Insert(1, ddlParentDefault2Item);

        ListItem ddlRedirectDefaultItem = new ListItem(GetLocalResourceObject("ddlRedirectionSelect.Text").ToString(), "");
        ddlRedirect.Items.Insert(0, ddlRedirectDefaultItem);

        ListItem ddlRedirectDefaultItem1 = new ListItem(GetLocalResourceObject("ddlExternalLink.Text").ToString(), "9999");
        ddlRedirect.Items.Insert(1, ddlRedirectDefaultItem1);

        //ListItem ddlTargetDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        //ddlTarget.Items.Insert(0, ddlTargetDefaultItem);

        if (Session["ADMID"] != null && Session["ADMID"] != "")
        {
            if (Convert.ToInt16(Session["ADMTYPE"]) == clsAdmin.CONSTADMTYPE)
            {
                trUserEditable.Visible = true;
            }
        }

         if (Session[clsAdmin.CONSTPROJECTPGAUTHORIZATION] != null && clsAdmin.CONSTPROJECTPGAUTHORIZATION != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTPGAUTHORIZATION]) == 1)
            {
                trAuthorize.Visible = true;
            }
         }
        
        if (viewType == View.Admin)
        {
            if (modeType == Mode.Edit)
            {
                trLanguageEdit.Visible = enableLanguage;
                trCorrespondEdit.Visible = enableCorrespondPage;
                
                pnlCmnPageDetailsEdit.Visible = true;
            }
        }
        else if(viewType == View.User) 
        {
            if (modeType == Mode.View) { pnlCmnPageDetailsView.Visible = true; }
            else if (modeType == Mode.Edit)
            {
                trCorrespondEdit.Visible = enableCorrespondPage;

                pnlCmnPageDetailsEdit.Visible = true;
            }
        }

        pnlShowInMenu.Visible = chkboxShowInMenu.Checked;
    }
    #endregion
}
