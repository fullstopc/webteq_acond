﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmPersonalization.ascx.cs" Inherits="ctlr_ucAdmPersonalization" %>

<asp:Panel ID="pnlAdmPersonalization" runat="server" CssClass="divAdmPersonalization">
    <asp:Panel ID="pnlAdmPersonalizationDetails" runat="server" CssClass="divAdmPersonalizationDetails">
        <div class="divTime">
            <span> Session is expiring in </span>
            <asp:Label ID="lblTime" runat="server" CssClass="spanTime"></asp:Label>
        </div> 
        <span>|</span>
        <asp:Literal ID="litLoginAs" runat="server"></asp:Literal>
        <asp:HyperLink ID="hypLoginAs" runat="server"></asp:HyperLink>
        <span>|</span>
        <asp:HyperLink ID="hypLogout" runat="server"></asp:HyperLink>
    </asp:Panel>
</asp:Panel>
