﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmProductDesc.ascx.cs" Inherits="ctrl_ucAdmProductDesc" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>
<script type="text/javascript">
    var intLoadCount = 0;
    function iFrmRelArtLoad() {
        lnkbtnRelArt = document.getElementById("<%= lnkbtnRelArt.ClientID %>");
        intLoadCount = intLoadCount + 1;

        if (intLoadCount > 1) {
            window.location.href = lnkbtnRelArt.href;
        }
    }

    $(document).ready(function () {
        $("#<%= txtProdSnapshot.ClientID %>").change(function () {
            var snapshottext = $("#<%= txtProdSnapshot.ClientID %>").val().replace("<br />", "\n").replace("<br/>", "\n").replace("<br>","\n")
            $("#<%= txtProdSnapshot.ClientID %>").val(snapshottext);
        });
    });
</script>
<asp:Panel ID="pnlFormSect2" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table id="tblAddMember2" cellpadding="0" cellspacing="0" class="formTbl tblData">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Product Content</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel nobr">Product Snapshot:&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="tdMax"><asp:TextBox ID="txtProdSnapshot" runat="server" class="text_fullwidth" MaxLength="1000" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                </tr>
                <tr id="trSnapshotJp" runat="server" visible="false">
                    <td class="tdLabel nobr">Product Snapshot (Japanese):</td>
                    <td class="tdMax"><asp:TextBox ID="txtProdSnapshotJp" runat="server" class="text_fullwidth" MaxLength="1000" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                </tr>
                <tr id="trSnapshotMs" runat="server" visible="false">
                    <td class="tdLabel nobr">Product Snapshot (Malay):</td>
                    <td class="tdMax"><asp:TextBox ID="txtProdSnapshotMs" runat="server" class="text_fullwidth" MaxLength="1000" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                </tr>
                <tr id="trSnapshotZh" runat="server" visible="false">
                    <td class="tdLabel nobr">Product Snapshot (Chinese):</td>
                    <td class="tdMax"><asp:TextBox ID="txtProdSnapshotZh" runat="server" class="text_fullwidth" MaxLength="1000" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel nobr">Product Remarks:</td>
                    <td class="tdMax"><asp:TextBox ID="txtProdRemarks" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                </tr>
                <tr id="trRemarksJp" runat="server" visible="false">
                    <td class="tdLabel nobr">Product Remarks (Japanese):</td>
                    <td class="tdMax"><asp:TextBox ID="txtProdRemarksJp" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                </tr>
                <tr id="trRemarksMs" runat="server" visible="false">
                    <td class="tdLabel nobr">Product Remarks (Malay):</td>
                    <td class="tdMax"><asp:TextBox ID="txtProdRemarksMs" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                </tr>
                <tr id="trRemarksZh" runat="server" visible="false">
                    <td class="tdLabel nobr">Product Remarks (Chinese):</td>
                    <td class="tdMax"><asp:TextBox ID="txtProdRemarksZh" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                </tbody>
                <tbody id="trProdDesc" runat="server">
                    <tr>
                        <td class="tdLabel nobr">Product Description:</td>
                        <td>
                            <asp:TextBox ID="txtProdDesc" runat="server" class="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="trDescJp" runat="server" visible="false">
                        <td class="tdLabel nobr">Product Description (Japanese):</td>
                        <td><asp:TextBox ID="txtProdDescJp" runat="server" class="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                    </tr>
                    <tr id="trDescMs" runat="server" visible="false">
                        <td class="tdLabel nobr">Product Description (Malay):</td>
                        <td><asp:TextBox ID="txtProdDescMs" runat="server" class="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                    </tr>
                    <tr id="trDescZh" runat="server" visible="false">
                        <td class="tdLabel nobr">Product Description (Chinese):</td>
                        <td><asp:TextBox ID="txtProdDescZh" runat="server" class="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                    </tr>
                        
                </tbody>
                <tbody id="trProdDescList" runat="server" visible="false">

                    <%--<asp:UpdatePanel ID="upnlProdDesc" runat="server">
                        <ContentTemplate>
                            <asp:UpdateProgress ID="uprgProdDesc" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="upnlProdDesc">
                                <ProgressTemplate>
                                    <uc:AdmLoading ID="ucAdmLoadingProdDesc" runat="server" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>

                                <tr>
                                    <td colspan="2" class="tdProdDescListHdr">Product Description:</td>
                                </tr>
                                <tr>
                                    <td class="tdSpacer">&nbsp;</td>
                                    <td class="tdSpacer">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="tdLabel">Title:</td>
                                    <td class="tdMax">
                                        <asp:TextBox ID="txtProdDescTitle" runat="server" CssClass="text_fullwidth"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvProdDescTitle" runat="server" ErrorMessage="<br/>Please enter product description title." ControlToValidate="txtProdDescTitle" Display="Dynamic" CssClass="errmsg" ValidationGroup="prodDesc"></asp:RequiredFieldValidator>
                                        <asp:HiddenField ID="hdnProdDesc" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdLabel nobr">Description:</td>
                                    <td class="tdMax"><asp:TextBox ID="txtProdDescContent" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td class="tdSpacer">&nbsp;</td>
                                    <td class="tdSpacer">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="tdLabel"></td>
                                    <td class="tdMax"><asp:LinkButton ID="lnkbtnAdd" runat="server" CssClass="btn2 btnAdd" Text="Add" ToolTip="Add" OnClick="lnkbtnAdd_Click" ValidationGroup="prodDesc"></asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td class="tdSpacer">&nbsp;</td>
                                    <td class="tdSpacer">&nbsp;</td>
                                </tr>
                            <tr>
                            <td colspan="2">
                            <asp:Repeater ID="rptProdDesc" runat="server" OnItemDataBound="rptProdDesc_ItemDataBound" OnItemCommand="rptProdDesc_ItemCommand">
                                <HeaderTemplate><table cellpadding="0" cellspacing="0"></HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="trProdDesc" runat="server">
                                        <td class="tdLabelProdDesc nobr"><asp:Label ID="lblProdDescTitle" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PRODDESC_TITLE") %>'></asp:Label></td>
                                        <td class="tdMax2"><asp:Label ID="lblProdDescContent" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PRODDESC_DESC") %>'></asp:Label></td>
                                        <td class="tdLabelProdAct">
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="cmdEdit" CssClass="lnkbtn lnkbtnEdit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PRODDESC_TEMPID") %>'></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="cmdDelete" CssClass="lnkbtn lnkbtnDelete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PRODDESC_TEMPID") %>'></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
                            </asp:Repeater>
                        <%--</ContentTemplate>
                    </asp:UpdatePanel>--%>
                    </td></tr>
                        
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <table id="" cellpadding="0" cellspacing="0" class="formTbl tblData">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Product Related Article</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="tdLoading" style="padding: 0;"><iframe name="iFrmRelArt" id="iFrmRelArt" src="<% =System.Configuration.ConfigurationManager.AppSettings["scriptBase"] %>cmn/fileupload.aspx" frameborder="0" scrolling="no" onload="iFrmRelArtLoad();"></iframe>
                        <asp:UpdatePanel ID="upnlRelArt" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="uprgAdmLoadingRelArt" DynamicLayout="true" runat="server" AssociatedUpdatePanelID="upnlRelArt">
                                    <ProgressTemplate>
                                        <uc:AdmLoading ID="ucAdmLoadingRelArt" runat="server" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:Literal ID="litRelArt" runat="server" Visible="false"></asp:Literal>
                                <asp:Panel ID="pnlRelArt" runat="server">
                                </asp:Panel>
                                <asp:LinkButton ID="lnkbtnRelArt" runat="server" CausesValidation="false" style="visibility:hidden;" OnClick="lnkbtnRelArt_OnClick"></asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2">
                         <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
   
</asp:Panel>
