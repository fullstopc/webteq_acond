﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class ctrl_ucAdmCMSObject : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _cmsId = 0;
    protected int _mode = 1;
    protected string _pageListingURL = "";
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int cmsId
    {
        get
        {
            if (ViewState["CMSID"] == null)
            {
                return _cmsId;
            }
            else
            {
                return int.Parse(ViewState["CMSID"].ToString());
            }
        }
        set { ViewState["CMSID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if (ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strTitle = txtCMSTitle.Text.Trim();
            string strContent = txtCMSContent.Text.Trim();
            int intActive = 0;
            int intType = 0;
            
            if(chkboxActive.Checked) { intActive = 1; }
            if (!string.IsNullOrEmpty(ddlType.SelectedValue)) { intType = Convert.ToInt16(ddlType.SelectedValue); }

            clsCMSObject cms = new clsCMSObject();
            int intRecordAffected = 0;

            if (mode == 1)
            {
                intRecordAffected = cms.addCMS(strTitle, strContent, int.Parse(Session["ADMID"].ToString()), intType, intActive);

                if (intRecordAffected == 1)
                {
                    Session["NEWCMSID"] = cms.cmsId;

                    Response.Redirect(currentPageName + "?id=" + cms.cmsId);
                }
                else
                {
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new CMS. Please try again.</div>";

                    Response.Redirect(currentPageName);
                }
            }
            else if (mode == 2)
            {
                Boolean boolEdited = false;
                Boolean boolSuccess = true;

                if (!cms.isExactSameSetCMS(cmsId, strTitle, strContent, intType, intActive))
                {
                    intRecordAffected = cms.updateCMSById(cmsId, strTitle, strContent, int.Parse(Session["ADMID"].ToString()), intType, intActive);

                    if (intRecordAffected == 1)
                    {
                        boolEdited = true;
                    }
                    else
                    {
                        boolSuccess = false;
                    }
                }

                if (boolEdited)
                {
                    Session["EDITEDCMSID"] = cmsId;

                    Response.Redirect(Request.Url.ToString());
                }
                else
                {
                    if (boolSuccess)
                    {
                        Session["EDITEDCMSID"] = cmsId;
                        Session["NOCHANGE"] = 1;

                        Response.Redirect(Request.Url.ToString());
                    }
                    else
                    {
                        cms.extractCMSById(cmsId, 0);

                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit CMS (" + cms.cmsTitle + "). Please try again.</div>";

                        Response.Redirect(currentPageName);
                    }
                }
            }
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string strDeletedCMSTitle = "";

        clsCMSObject cms = new clsCMSObject();
        clsPageObject po = new clsPageObject();
        int intRecordAffected = 0;

        cms.extractCMSById(cmsId, 0);
        strDeletedCMSTitle = cms.cmsTitle;

        intRecordAffected = cms.deleteCMSById(cmsId);

        if (intRecordAffected == 1)
        {
            po.deleteItemById(cmsId, clsAdmin.CONSTCMSGROUPCONTENT);
            Session["DELETEDCMSTITLE"] = strDeletedCMSTitle;
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to delete CMS (" + strDeletedCMSTitle + "). Please try again.</div>";
        }

        Response.Redirect(currentPageName);
    }

    protected void lnkbtnBackup_Click(object sender, EventArgs e)
    {
        int intRecordAffected = 0;
        clsCMSObject cms = new clsCMSObject();

        if (cms.extractCMSById(cmsId, 0))
        {
            Boolean boolSave = true;
            string strBackupCont = "";
            int intTotalBackupPages = cms.getTotalCMSBackup(cmsId);
            if (intTotalBackupPages > 0)
            {
                strBackupCont = cms.getLastCMSBackupContent(cmsId);
                if (strBackupCont.Equals(cms.cmsContent))
                {
                    boolSave = false;
                }
            }

            if (boolSave == true)
            {
                if (intTotalBackupPages >= 50)
                {
                    intRecordAffected = cms.deleteOldCMSBackup(cmsId);
                }
                else
                {
                    intRecordAffected = 1;
                }

                if (intRecordAffected == 1)
                {
                    intRecordAffected = 0;
                    intRecordAffected = cms.addCMSBackup(cms.cmsId, cms.cmsContent, Convert.ToInt16(Session["ADMID"]));
                    Response.Write("<script>alert('Successfully Backup.');</script>");
                }
                else
                {
                    Response.Write("<script>alert('Cannot backup this version.');</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('This version was backup previously.');</script>");
            }
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }
    #endregion


    #region "Methods"
    protected void registerCKEditorScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR")))
        {
            int intFullPath = Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFULLPATH]) == 1 ? clsAdmin.CONSTFULLPATHACTIVE : clsAdmin.CONSTFULLPATHDEFAULT;
            string strJS = null;
            strJS = "<script type=\"text/javascript\">" +
                    "CKEDITOR.replace('" + txtCMSContent.ClientID + "'," +
                    "{" +
                    "width:'99%'," +
                    "height:'300'," +
                    "toolbar: 'User3'," +
                    "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSADMCMSVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSADMCMSVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSADMCMSVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                     "}" +
                     ");";

            strJS += "CKEDITOR.add;";

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMEEDITORSTYLE, clsConfig.CONSTGROUPWEBSITE, 1);
            string strEditorStyle = config.value;
            if (!string.IsNullOrEmpty(strEditorStyle))
            {
                strJS += "CKEDITOR.config.contentsCss = 'body {" + strEditorStyle + "}';";
            }
            
            strJS += "CKEDITOR.config.stylesCombo_stylesSet= '" + clsAdmin.CONSTADMSTYLESET + ":" + System.Configuration.ConfigurationManager.AppSettings["jsBase"] + clsAdmin.CONSTADMSTYLESETFILE + "';" +
                     "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR", strJS, false);
        }
    }

    public void fill()
    {
        registerCKEditorScript();

        if (!IsPostBack)
        {
            clsMis mis = new clsMis();
            DataSet ds = new DataSet();
            ds = mis.getListByListGrp("CMS TYPE",1);

            DataView dv = new DataView(ds.Tables[0]);
            ddlType.DataSource = dv;
            ddlType.DataTextField = "LIST_NAME";
            ddlType.DataValueField = "LIST_VALUE";
            ddlType.DataBind();

            ListItem ddlTypeDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
            ddlType.Items.Insert(0, ddlTypeDefaultItem);

            if (mode == 2)
            {
                clsCMSObject cms = new clsCMSObject();

                if (cms.extractCMSById(cmsId, 0))
                {
                    txtCMSTitle.Text = cms.cmsTitle;
                    txtCMSContent.Text = cms.cmsContent;

                    if (cms.cmsType != 0)
                    {
                        ddlType.SelectedValue = cms.cmsType.ToString();
                    }

                    if (cms.cmsActive != 0) { chkboxActive.Checked = true; }
                    else { chkboxActive.Checked = false; }

                    if (cms.cmsAllowDelete == 0)
                    {
                        lnkbtnDelete.Visible = false;
                        trType.Visible = false;
                    }
                    else
                    {
                        lnkbtnDelete.Visible = true;
                    }
                }
                else
                {
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";

                    Response.Redirect(currentPageName);
                }

                lnkbtnDelete.Visible = true;
                lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteCMS.Text").ToString() + "'); return false;";
            }
            lnkbtnRestore.Attributes["href"] = ConfigurationManager.AppSettings["scriptBase"] + "adm/admCMSObjectBackupList.aspx?id=" + cmsId + "&" + clsAdmin.CONSTUSRARTICLETHICKBOX;
            lnkbtnCancel.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
        }
    }
    #endregion
}
