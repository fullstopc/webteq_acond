﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;
using System.Linq;

public partial class ctrl_ucAdmDashboard : System.Web.UI.UserControl
{
    #region "Properties"
    protected clsConfig config = new clsConfig();
    protected string strGoogleAnalyticsClientId = "";
    protected Dictionary<string, object> dictReturn;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "admdashboard.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        //Literal litJquery = new Literal();
        //litJquery.Text = "<script src='" + ConfigurationManager.AppSettings["jsBase"] + "jquery-1.8.2.min.js' type='text/javascript' ></script>";
        //this.Page.Header.Controls.Add(litJquery);

        //Literal litJS = new Literal();
        //litJS.Text = "<script src='" + ConfigurationManager.AppSettings["jsBase"] + "stupidtable.min.js' type='text/javascript' ></script>";
        //this.Page.Header.Controls.Add(litJS);
        
        if (!IsPostBack)
        {
            setPageProperties();
            fill();
        }
    }
    protected void rptEnquiry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Literal litNo = (Literal)e.Item.FindControl("litNo");
            Literal litCreation = (Literal)e.Item.FindControl("litCreation");
            Literal litName = (Literal)e.Item.FindControl("litName");
            Literal litCompanyName = (Literal)e.Item.FindControl("litCompanyName");
            Literal litContactNo = (Literal)e.Item.FindControl("litContactNo");
            Literal litEmailSender = (Literal)e.Item.FindControl("litEmailSender");
            Literal litSubject = (Literal)e.Item.FindControl("litSubject");
            Literal litMessage = (Literal)e.Item.FindControl("litMessage");
            HyperLink hypView = (HyperLink)e.Item.FindControl("hypView");

            litNo.Text = e.Item.ItemIndex + 1 + "";
            litCreation.Text = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "ENQ_CREATION")).ToString("dd MMM yyyy hh:mm:ss tt");
            litName.Text = DataBinder.Eval(e.Item.DataItem, "ENQ_NAME").ToString();
            litCompanyName.Text = DataBinder.Eval(e.Item.DataItem, "ENQ_COMPANYNAME").ToString();
            litContactNo.Text = DataBinder.Eval(e.Item.DataItem, "ENQ_CONTACTNO").ToString();
            litEmailSender.Text = DataBinder.Eval(e.Item.DataItem, "ENQ_EMAILSENDER").ToString();
            litSubject.Text = DataBinder.Eval(e.Item.DataItem, "ENQ_SUBJECT").ToString();
            litMessage.Text = DataBinder.Eval(e.Item.DataItem, "ENQ_MESSAGE").ToString();


            hypView.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admEnquiry0101.aspx?id=" + DataBinder.Eval(e.Item.DataItem, "ENQ_ID").ToString();
            hypView.Text = GetGlobalResourceObject("GlobalResource", "contentAdmView.Text").ToString();
            hypView.ToolTip = GetGlobalResourceObject("GlobalResource", "contentAdmView.Text").ToString();
            hypView.CssClass += " fancybox fancybox.iframe";
            hypView.Attributes["rel"] = "enquiry-list";
        }
    }
    protected void rptPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Literal litNo = (Literal)e.Item.FindControl("litNo");
            Literal litPageName = (Literal)e.Item.FindControl("litPageName");
            Literal litParent = (Literal)e.Item.FindControl("litParent");
            Literal litActive = (Literal)e.Item.FindControl("litActive");
            Literal litLastUpdated = (Literal)e.Item.FindControl("litLastUpdated");

            litNo.Text = e.Item.ItemIndex + 1 + "";
            litPageName.Text = DataBinder.Eval(e.Item.DataItem, "PAGE_DISPLAYNAME").ToString();

            int intParent = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_PARENT"));

            if (intParent == 0)
            {
                litParent.Text = "Root";
            }
            else if (intParent == -1)
            {
                litParent.Text = "Root2";
            }
            else
            {
                litParent.Text = DataBinder.Eval(e.Item.DataItem, "V_PARENTNAME").ToString();
            }

            litActive.Text = clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ACTIVE")));
            litLastUpdated.Text = DataBinder.Eval(e.Item.DataItem, "PAGE_LASTUPDATE") != DBNull.Value ? Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "PAGE_LASTUPDATE")).ToString("dd MMM yyyy hh:mm:ss tt") : Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "PAGE_CREATION")).ToString("dd MMM yyyy hh:mm:ss tt");

            HyperLink hypEdit = (HyperLink)e.Item.FindControl("hypEdit");
            hypEdit.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admPage0101.aspx?id=" + DataBinder.Eval(e.Item.DataItem, "PAGE_ID").ToString();
            hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.ToolTip = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
        }
    }
    protected void rptObject_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Literal litNo = (Literal)e.Item.FindControl("litNo");
            Literal litTitle = (Literal)e.Item.FindControl("litTitle");
            Literal litType = (Literal)e.Item.FindControl("litType");
            Literal litActive = (Literal)e.Item.FindControl("litActive");
            Literal litLastUpdated = (Literal)e.Item.FindControl("litLastUpdated");

            litNo.Text = e.Item.ItemIndex + 1 + "";
            litTitle.Text = DataBinder.Eval(e.Item.DataItem, "CMS_TITLE").ToString();
            litType.Text = DataBinder.Eval(e.Item.DataItem, "V_CMSTYPENAME").ToString();
            litActive.Text = clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "CMS_ACTIVE")));
            litLastUpdated.Text = DataBinder.Eval(e.Item.DataItem, "CMS_LASTUPDATE") != DBNull.Value ? Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "CMS_LASTUPDATE")).ToString("dd MMM yyyy hh:mm:ss tt") : Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "CMS_CREATION")).ToString("dd MMM yyyy hh:mm:ss tt");

            HyperLink hypEdit = (HyperLink)e.Item.FindControl("hypEdit");
            hypEdit.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admCMS0101.aspx?id=" + DataBinder.Eval(e.Item.DataItem, "CMS_ID").ToString();
            hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.ToolTip = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
        }
    }
    protected void rptEvent_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Literal litNo = (Literal)e.Item.FindControl("litNo");
            Literal litTitle = (Literal)e.Item.FindControl("litTitle");
            Literal litStartDate = (Literal)e.Item.FindControl("litStartDate");
            Literal litEndDate = (Literal)e.Item.FindControl("litEndDate");
            Literal litActive = (Literal)e.Item.FindControl("litActive");
            Literal litLastUpdated = (Literal)e.Item.FindControl("litLastUpdated");

            litNo.Text = e.Item.ItemIndex + 1 + "";
            litTitle.Text = DataBinder.Eval(e.Item.DataItem, "HIGH_TITLE").ToString();
            litStartDate.Text = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "HIGH_STARTDATE")).ToString();
            litEndDate.Text = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "HIGH_ENDDATE")).ToString();
            litActive.Text = clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "HIGH_ACTIVE")));
            litLastUpdated.Text = DataBinder.Eval(e.Item.DataItem, "HIGH_LASTUPDATE") != DBNull.Value ? Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "HIGH_LASTUPDATE")).ToString("dd MMM yyyy hh:mm:ss tt") : Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "HIGH_CREATION")).ToString("dd MMM yyyy hh:mm:ss tt");

            HyperLink hypEdit = (HyperLink)e.Item.FindControl("hypEdit");
            hypEdit.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admHighlight0101.aspx?id=" + DataBinder.Eval(e.Item.DataItem, "HIGH_ID").ToString();
            hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.ToolTip = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
        }
    }
    protected void rptProduct_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Literal litNo = (Literal)e.Item.FindControl("litNo");
            Literal litCode = (Literal)e.Item.FindControl("litCode");
            Literal litName = (Literal)e.Item.FindControl("litName");
            Literal litActive = (Literal)e.Item.FindControl("litActive");
            Literal litLastUpdated = (Literal)e.Item.FindControl("litLastUpdated");

            litNo.Text = e.Item.ItemIndex + 1 + "";
            litCode.Text = DataBinder.Eval(e.Item.DataItem, "PROD_CODE").ToString();
            litName.Text = DataBinder.Eval(e.Item.DataItem, "PROD_NAME").ToString();
            litActive.Text = clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PROD_ACTIVE")));

            litLastUpdated.Text = DataBinder.Eval(e.Item.DataItem, "PROD_LASTUPDATE") != DBNull.Value ? Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "PROD_LASTUPDATE")).ToString("dd MMM yyyy hh:mm:ss tt") : Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "PROD_CREATION")).ToString("dd MMM yyyy hh:mm:ss tt");

            HyperLink hypEdit = (HyperLink)e.Item.FindControl("hypEdit");
            hypEdit.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admProduct0101.aspx?id=" + DataBinder.Eval(e.Item.DataItem, "PROD_ID").ToString();
            hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.ToolTip = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
        }
    }
    protected void rptOrder_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            decimal total = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "ORDER_TOTAL"));
            decimal discount = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "ORDER_DISVALUE"));


            Literal litNo = (Literal)e.Item.FindControl("litNo");
            Literal litSaleDate = (Literal)e.Item.FindControl("litSaleDate");
            Literal litSaleNo = (Literal)e.Item.FindControl("litSaleNo");
            Literal litSaleAmt = (Literal)e.Item.FindControl("litSaleAmt");
            Literal litStatus = (Literal)e.Item.FindControl("litStatus");

            litNo.Text = e.Item.ItemIndex + 1 + "";
            litSaleDate.Text = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "ORDER_CREATION")).ToString();
            litSaleNo.Text = DataBinder.Eval(e.Item.DataItem, "ORDER_NO").ToString();
            litSaleAmt.Text = clsMis.formatPrice(total - discount);
            litStatus.Text = DataBinder.Eval(e.Item.DataItem, "V_STATUSNAME").ToString();

            HyperLink hypEdit = (HyperLink)e.Item.FindControl("hypEdit");
            hypEdit.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admOrder0101.aspx?id=" + DataBinder.Eval(e.Item.DataItem, "ORDER_ID").ToString();
            hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.ToolTip = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
        }
    }
    protected void rptAblImg_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strMastheadName = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_NAME").ToString();
            string strMastheadId = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_ID").ToString();
            string strMastheadImg = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_IMAGE").ToString();
            string strMastheadLastUpdated = (!string.IsNullOrEmpty(DataBinder.Eval(e.Item.DataItem, "MASTHEAD_LASTUPDATED").ToString())) ?
                                            Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "MASTHEAD_LASTUPDATED")).ToString("dd MMM yyyy hh:mm:ss tt") : "-";

            HyperLink hypAblImg = (HyperLink)e.Item.FindControl("hypAblImg");
            hypAblImg.Attributes["rel"] = "lightbox[slideshow]";
            hypAblImg.Attributes["title"] = strMastheadName;

            string strFullPathImg = "";
            if (string.IsNullOrEmpty(strMastheadImg))
            {
                clsConfig config = new clsConfig();
                strFullPathImg = ConfigurationManager.AppSettings["imageBase"] + "cmn/" + config.defaultGroupImg;
            }

            else
            {
                strFullPathImg = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + strMastheadImg;
            }
            Image resultImage = new Image();
            try
            {
                if (!string.IsNullOrEmpty(strMastheadImg))
                {
                    System.Drawing.Image objImage = System.Drawing.Image.FromFile(strFullPathImg);
                    resultImage = clsMis.getImageResize2(resultImage, clsSetting.CONSTSLIDESHOWMANAGERIMGWIDTH, clsSetting.CONSTSLIDESHOWMANAGERIMGHEIGHT, objImage.Width, objImage.Height);
                    objImage.Dispose();
                    objImage = null;
                }
            }
            catch (Exception ex) { }

            if (!string.IsNullOrEmpty(strMastheadImg))
            {
                Image imgAblImg = (Image)e.Item.FindControl("imgAblImg");
                imgAblImg.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + strFullPathImg + "&f=1";
                imgAblImg.AlternateText = strMastheadName;
                imgAblImg.ToolTip = strMastheadName;

                imgAblImg.Height = resultImage.Height;
                imgAblImg.Width = resultImage.Width;
                imgAblImg.Attributes["style"] = "margin-top:-" + (resultImage.Height.Value / 2) + "px;";
                imgAblImg.Attributes["style"] += "margin-left:-" + (resultImage.Width.Value / 2) + "px;";
                resultImage.Dispose();
                resultImage = null;
                hypAblImg.NavigateUrl = Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + strMastheadImg;

            }
            else
            {
                Image imgAblImg = (Image)e.Item.FindControl("imgAblImg");
                imgAblImg.ImageUrl = Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + config.defaultGroupImg;
                imgAblImg.AlternateText = strMastheadName;
                imgAblImg.ToolTip = strMastheadName;
                imgAblImg.Attributes["style"] = "width:100%;transform: translate(-50%, -50%);";
                resultImage.Dispose();
                resultImage = null;
                hypAblImg.NavigateUrl = Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + config.defaultGroupImg;
            }


            HyperLink hypEdit = (HyperLink)e.Item.FindControl("hypEdit");
            hypEdit.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admSlide0101.aspx?id=" + DataBinder.Eval(e.Item.DataItem, "MASTHEAD_ID").ToString();
            hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.ToolTip = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();

            Literal litLastUpdated = e.Item.FindControl("litLastUpdated") as Literal;
            litLastUpdated.Text = strMastheadLastUpdated;

        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {

        
        
    }

    protected void setPageProperties()
    {
        // google analytics & page manager & object manager & enquiry manager & slide show manager always show
        // sales manager show  - when eshop project
        // product manager show - when ecatalog OR eshop project
        // event manager show - when eprofile lite OR in dev account the event checkbox checked

        litGALoginDesc.Text = "To view your Google Analisis report, please login your registered gmail account in another window/tab.";

        divSalesManager.Visible = false;
        divProductManager.Visible = false;
        divEventManager.Visible = false;
        int intProjectType = int.Parse(Session[clsAdmin.CONSTPROJECTTYPE].ToString());
        int[] intEcatalog = {
                                clsAdmin.CONSTTYPEeCATALOG, 
                                clsAdmin.CONSTTYPEeCATALOGLITE,clsAdmin.CONSTTYPEeCATALOGLITEEVENTMANAGER,
                                clsAdmin.CONSTTYPEeCATALOGEVENTMANAGER,clsAdmin.CONSTTYPEESHOPLITEINVOICE
                            };

        int[] intEshop = {
                             clsAdmin.CONSTTYPEeSHOP, clsAdmin.CONSTTYPEeSHOPLITE, 
                             clsAdmin.CONSTTYPEESHOPINVOICE, clsAdmin.CONSTTYPEESHOPLITEINVOICE,
                             clsAdmin.CONSTTYPEeSHOPEVENTMANAGER,
                         };

        int[] intEvent = {
                            clsAdmin.CONSTTYPEeSHOPEVENTMANAGER,
                            clsAdmin.CONSTTYPEeCATALOGEVENTMANAGER,
                            clsAdmin.CONSTTYPEePROFILELITE
                         };

        if (intEcatalog.Contains(intProjectType) || intEshop.Contains(intProjectType)) { divProductManager.Visible = true; }
        if (intEshop.Contains(intProjectType)) { divSalesManager.Visible = true; }
        if (intEvent.Contains(intProjectType) || Session[clsAdmin.CONSTPROJECTEVENTMANAGER] != null)
        {
            if (intEvent.Contains(intProjectType))
            {
                divEventManager.Visible = true;
            }
            else{
                if (Session[clsAdmin.CONSTPROJECTEVENTMANAGER].ToString() == "1")
                {
                    divEventManager.Visible = true;
                }
            }
        }
        bindRptData();

        divGoogleAnalytic.Visible = false;
        divSalesManager.Visible = false;
        divProductManager.Visible = false;

        dictReturn = new Dictionary<string, object>()
        {
            { "agents", new clsEmployee().getDataTable().AsEnumerable().Select(x => new clsEmployee() { row = x }).ToList() },
            { "reserved_list", new clsReservation().getDataTable().AsEnumerable().Select(x => new clsReservation() { row = x }).ToList() }
        };

    }

    protected void bindRptData()
    {
        bindGoogleAnalytics();
        bindRptEnquiry();
        bindRptPage();
        bindRptObject();
        bindRptSlideShow();

        if (divEventManager.Visible) { bindRptEvent(); }
        if (divSalesManager.Visible) { bindRptOrder(); }
        if (divProductManager.Visible) { bindRptProduct(); }
    }

    private void bindRptEnquiry()
    {
        clsEnquiry enquiry = new clsEnquiry();
        DataView dv = new DataView(enquiry.getUserEnquiry(0).Tables[0]);

        dv.Sort = "ENQ_CREATION DESC";
        if (dv.Count > 0)
        {
            rptEnquiry.DataSource = getLimitRow(dv, 10);
            rptEnquiry.DataBind();
        }
        else
        {
            divEnquiryManager.Visible = false;
        }
    }
    private void bindRptObject()
    {
        clsCMSObject cms = new clsCMSObject();
        DataView dv = new DataView(cms.getCMSList(0).Tables[0]);

        dv.Sort = "CMS_LASTUPDATE DESC,CMS_CREATION DESC";
        if (dv.Count > 0)
        {
            rptObject.DataSource = getLimitRow(dv, 10);
            rptObject.DataBind();
        }
    }
    private void bindRptPage()
    {
        clsPage page = new clsPage();
        DataView dv = new DataView(page.getPageList(0).Tables[0]);

        dv.Sort = "PAGE_LASTUPDATE DESC,PAGE_CREATION DESC";
        if (dv.Count > 0)
        {
            rptPage.DataSource = getLimitRow(dv, 10);
            rptPage.DataBind();
        }
    }
    private void bindRptEvent()
    {
        clsHighlight high = new clsHighlight();
        DataView dv = new DataView(high.getHighlightList(0).Tables[0]);

        dv.Sort = "HIGH_LASTUPDATE DESC,HIGH_CREATION DESC";
        if (dv.Count > 0)
        {
            rptEvent.DataSource = getLimitRow(dv, 10);
            rptEvent.DataBind();
        }
    }
    private void bindRptOrder()
    {
        clsOrder ord = new clsOrder();
        DataView dv = new DataView(ord.getOrderList2(0).Tables[0]);

        dv.Sort = "ORDER_CREATION DESC";
        if (dv.Count > 0)
        {
            rptOrder.DataSource = getLimitRow(dv, 10);
            rptOrder.DataBind();
        }
    }
    private void bindRptProduct()
    {
        clsProduct prod = new clsProduct();
        DataView dv = new DataView(prod.getProdList(0).Tables[0]);

        dv.Sort = "PROD_LASTUPDATE DESC,PROD_CREATION DESC";
        if (dv.Count > 0)
        {
            rptProduct.DataSource = getLimitRow(dv, 10);
            rptProduct.DataBind();
        }
    }
    private void bindRptSlideShow()
    {
        clsMasthead masthead = new clsMasthead();
        DataView dv = new DataView(masthead.getMastheadImage3().Tables[0]);

        dv.Sort = "MASTHEAD_IMAGE DESC";
        if (dv.Count > 0)
        {
            rptAblImg.DataSource = getLimitRow(dv, 10);
            rptAblImg.DataBind();
        }
    }
    private void bindGoogleAnalytics()
    {
        if (Session[clsAdmin.CONSTGOOGLEANALYTICSCLIENTID] != null)
        {
            strGoogleAnalyticsClientId = Session[clsAdmin.CONSTGOOGLEANALYTICSCLIENTID].ToString();
        }
    }
    
    protected DataView getLimitRow(DataView dv,int intRowLimit)
    {
        DataTable dataTable = dv.ToTable();
        while (dataTable.Rows.Count > intRowLimit)
        {
            dataTable = dataTable.AsEnumerable().Skip(0).Take(intRowLimit).CopyToDataTable();
        }
        return new DataView(dataTable);
    }
    #endregion
}