﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.Collections;

public partial class ctrl_ucAdmProductAddon : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _prodId = 0;
    protected int _brandId = 0;
    protected int _catId = 0;
    protected int _mode = 1;
    protected DataTable _dtProdSel = null;
    protected string _selectedId = null;
    protected string _deletedId = null;
    protected string _pageListingURL = "";

    protected Boolean _boolListRowClose = false;
    protected Boolean _boolSelRowClose = false;

    protected int _currentPage = 0;
    protected int _pageNo = 1;
    protected int _pageCount = 0;

    protected int intProdCount;
    protected int intProdCountInRow = 5;
    protected int intProdTotal;
    protected int intProdPageSize;

    protected int intProdCountSel;
    protected int intProdCountInRowSel = 5;
    protected int intProdSelTotal;
    protected int intProdSelPageSize;

    protected string strKeyword;
    protected int intNew;
    protected int intShowTop;
    protected Boolean _boolSearch;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int prodId
    {
        get
        {
            if (ViewState["PRODID"] == null)
            {
                return _prodId;
            }
            else
            {
                return int.Parse(ViewState["PRODID"].ToString());
            }
        }
        set { ViewState["PRODID"] = value; }
    }

    public int brandId
    {
        get
        {
            if (ViewState["BRANDID"] == null)
            {
                return _brandId;
            }
            else
            {
                return int.Parse(ViewState["BRANDID"].ToString());
            }
        }
        set { ViewState["BRANDID"] = value; }
    }

    public int catId
    {
        get
        {
            if (ViewState["CATID"] == null)
            {
                return _catId;
            }
            else
            {
                return int.Parse(ViewState["CATID"].ToString());
            }
        }
        set { ViewState["CATID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if (ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }

    public DataTable dtProdSel
    {
        get
        {
            if (ViewState["DTPRODSEL"] == null)
            {
                return _dtProdSel;
            }
            else
            {
                return (DataTable)ViewState["DTPRODSEL"];
            }
        }
        set { ViewState["DTPRODSEL"] = value; }
    }

    public string selectedId
    {
        get
        {
            if (ViewState["SELECTEDID"] == null)
            {
                return _selectedId;
            }
            else
            {
                return ViewState["SELECTEDID"].ToString();
            }
        }
        set { ViewState["SELECTEDID"] = value; }
    }

    public string deletedId
    {
        get
        {
            if (ViewState["DELETEDID"] == null)
            {
                return _deletedId;
            }
            else
            {
                return ViewState["DELETEDID"].ToString();
            }
        }
        set { ViewState["DELETEDID"] = value; }
    }

    public Boolean boolSearch
    {
        get
        {
            if (ViewState["BOOLSEARCH"] == null)
            {
                return _boolSearch;
            }
            else
            {
                return (Boolean)ViewState["BOOLSEARCH"];
            }
        }
        set { ViewState["BOOLSEARCH"] = value; }
    }

    protected Boolean boolListRowClose
    {
        get
        {
            if (ViewState["BOOLLISTROWCLOSE"] == null)
            {
                return _boolListRowClose;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLLISTROWCLOSE"]);
            }
        }
        set { ViewState["BOOLLISTROWCLOSE"] = value; }
    }

    protected Boolean boolSelRowClose
    {
        get
        {
            if (ViewState["BOOLSELROWCLOSE"] == null)
            {
                return _boolSelRowClose;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLSELROWCLOSE"]);
            }
        }
        set { ViewState["BOOLSELROWCLOSE"] = value; }
    }

    protected int currentPage
    {
        get
        {
            if (ViewState["CURRENTPAGE"] == null)
            {
                return _currentPage;
            }
            else
            {
                return Convert.ToInt16(ViewState["CURRENTPAGE"]);
            }
        }
        set { ViewState["CURRENTPAGE"] = value; }
    }

    protected int pageNo
    {
        get
        {
            if (ViewState["PAGENO"] == null)
            {
                return _pageNo;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGENO"]);
            }
        }
        set { ViewState["PAGENO"] = value; }
    }

    protected int pageCount
    {
        get
        {
            if (ViewState["PAGECOUNT"] == null)
            {
                return _pageCount;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGECOUNT"]);
            }
        }
        set { ViewState["PAGECOUNT"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            txtKeyword.Attributes.Add("onkeydown", "javascript:if(event.keyCode == 13){document.getElementById('" + lnkbtnGo.ClientID + "').click(); return false;}");
        }
    }

    protected void rptProd_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strProdName = DataBinder.Eval(e.Item.DataItem, "PROD_NAME").ToString();
            string strProdId = DataBinder.Eval(e.Item.DataItem, "PROD_ID").ToString();

            Image imgbtnProd = (Image)e.Item.FindControl("imgbtnProd");
            if (string.IsNullOrEmpty(DataBinder.Eval(e.Item.DataItem, "PROD_IMAGE").ToString()))
            {
                clsConfig config = new clsConfig();
                imgbtnProd.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + "/" + config.defaultGroupImg + "&w=100&h=100&f=1";
            }
            else
            {
                imgbtnProd.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPRODFOLDER + "/" + DataBinder.Eval(e.Item.DataItem, "PROD_IMAGE") + "&w=100&h=100&f=1";
            }
            //imgbtnProd.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPRODFOLDER + "/" + DataBinder.Eval(e.Item.DataItem, "PROD_IMAGE") + "&w=100&h=100&f=1";
            imgbtnProd.ToolTip = strProdName;
            imgbtnProd.AlternateText = strProdName;

        }
        else if (e.Item.ItemType == ListItemType.Footer) { }
    }

    /* -- No Use -- */
    protected void rptProd_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        //System.Threading.Thread.Sleep(2000);
        if (e.CommandName == "cmdSelect")
        {
            string strArg = e.CommandArgument.ToString();
            string[] strArgSplit = strArg.Split((Char) '|');

            int intSelectedId = int.Parse(strArgSplit[0]);
            string strSelProdName = strArgSplit[1].ToString();
            string strSelProdImg = strArgSplit[2].ToString();

            updateDeletedList(intSelectedId);
            addSelection(intSelectedId, strSelProdName, strSelProdImg);

            bindRptProdData();
            bindRptProdSelData();
        }
    }
    /* -- No Use -- */

    protected void rptProdSel_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strProdName = DataBinder.Eval(e.Item.DataItem, "ADDON_NAME").ToString();
            string strProdId = DataBinder.Eval(e.Item.DataItem, "ADDON_ID").ToString();

            ImageButton imgbtnProd = (ImageButton)e.Item.FindControl("imgbtnProd");
            if (string.IsNullOrEmpty(DataBinder.Eval(e.Item.DataItem, "ADDON_IMAGE").ToString()))
            {
                clsConfig config = new clsConfig();
                imgbtnProd.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + "/" + config.defaultGroupImg + "&w=100&h=100&f=1";
            }
            else
            {
                imgbtnProd.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPRODFOLDER + "/" + DataBinder.Eval(e.Item.DataItem, "ADDON_IMAGE") + "&w=100&h=100&f=1";
            }
            //imgbtnProd.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPRODFOLDER + "/" + DataBinder.Eval(e.Item.DataItem, "ADDON_IMAGE") + "&w=100&h=100&f=1";
            imgbtnProd.ToolTip = strProdName;
            imgbtnProd.AlternateText = strProdName;

            Literal litProdSelName = (Literal)e.Item.FindControl("litProdSelName");
            LinkButton lnkbtnRemove = (LinkButton)e.Item.FindControl("lnkbtnRemove");



        }
        else if (e.Item.ItemType == ListItemType.Footer) { }
    }
    protected void rptProdSel_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        //System.Threading.Thread.Sleep(2000);
        if (e.CommandName == "cmdRemove")
        {
            if (selectedId != null && selectedId != "")
            {
                int intSelectedId = Convert.ToInt16(e.CommandArgument);
                string strSelectedId = selectedId.Substring(0, selectedId.Length - 1);
                string[] strSelectedSplit = strSelectedId.Split(clsAdmin.CONSTCOMMASEPARATOR);
                string strNewSelected = "";

                try
                {
                    for (int i = 0; i < strSelectedSplit.Length; i++)
                    {
                        if (Convert.ToInt16(strSelectedSplit[i]) != intSelectedId)
                        {
                            strNewSelected += strSelectedSplit[i] + clsAdmin.CONSTCOMMASEPARATOR.ToString();
                        }
                    }
                }
                catch (Exception ex) { }

                //int intSelectedId = int.Parse(e.CommandArgument.ToString());
                //string strSelectedId = selectedId.Substring(0, selectedId.Length - 1);
                //string[] strSelectedSplit = strSelectedId.Split((char)',');
                //string strNewSelected = "";

                //try
                //{
                //    for (int i = 0; i < strSelectedSplit.Length; i++)
                //    {
                //        if (int.Parse(strSelectedSplit[i]) != intSelectedId)
                //        {
                //            strNewSelected += strSelectedSplit[i] + ",";
                //        }
                //    }
                //}
                //catch (Exception ex) { }

                selectedId = null;
                selectedId = strNewSelected;
                deletedId += intSelectedId.ToString() + clsAdmin.CONSTCOMMASEPARATOR.ToString(); 

                DataTable dt = new DataTable();
                dt = dtProdSel;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (int.Parse(dt.Rows[i]["ADDON_ID"].ToString()) == intSelectedId)
                    {
                        dt.Rows[i].Delete();
                    }
                }

                dt.AcceptChanges();
                dtProdSel = dt;

                if (dt.Rows.Count <= clsAdmin.CONSTMAXPRODUCTRELATED) { pnlAck.Visible = false; }

                bindRptProdData();
                bindRptProdSelData();
            }
        }
    }

    protected void lnkbtnGo_Click(object sender, EventArgs e)
    {
        boolSearch = true;

        bindRptProdData();
    }

    protected void rptPagination_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intPage = Convert.ToInt16(e.Item.DataItem);

            LinkButton lnkbtnPage = (LinkButton)e.Item.FindControl("lnkbtnPage");

            if (intPage == pageNo)
            {
                lnkbtnPage.CssClass = "linkPageSel";
                lnkbtnPage.Enabled = false;
            }
        }
    }

    protected void rptPagination_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdPage")
        {
            pageNo = Convert.ToInt16(e.CommandArgument);

            bindRptProdData();
        }
    }

    protected void lnkbtnFirst_Click(object sender, EventArgs e)
    {
        pageNo = 1;

        bindRptProdData();
    }

    protected void lnkbtnLast_Click(object sender, EventArgs e)
    {
        pageNo = pageCount;

        bindRptProdData();
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Boolean bEdited = false;
            Boolean bSuccess = true;

            clsProduct prod = new clsProduct();
            int intProdId;
            int intRecordAffected;

            string strDeletedId = clsAdmin.CONSTCOMMASEPARATOR.ToString() + deletedId;

            foreach (RepeaterItem i in rptProd.Items)
            {
                CheckBox chkboxSelect = (CheckBox)i.FindControl("chkboxSelect");

                if (chkboxSelect.Checked)
                {
                    HiddenField hdnId = (HiddenField)i.FindControl("hdnId");
                    int intId = !string.IsNullOrEmpty(hdnId.Value.Trim()) ? Convert.ToInt16(hdnId.Value.Trim()) : 0;

                    strDeletedId = strDeletedId.Replace(clsAdmin.CONSTCOMMASEPARATOR.ToString() + intId.ToString() + clsAdmin.CONSTCOMMASEPARATOR.ToString(), clsAdmin.CONSTCOMMASEPARATOR.ToString());

                    if (!prod.isExactSameAddonProd(prodId, intId))
                    {
                        intRecordAffected = prod.addAddonProd(prodId, intId);

                        if (intRecordAffected == 1) { bEdited = true; }
                        else
                        {
                            bSuccess = false;
                            break;
                        }
                    }
                }
            }

            if (bSuccess && !string.IsNullOrEmpty(strDeletedId))
            {
                strDeletedId = strDeletedId.Substring(clsAdmin.CONSTCOMMASEPARATOR.ToString().Length, strDeletedId.Length - clsAdmin.CONSTCOMMASEPARATOR.ToString().Length);

                if (!string.IsNullOrEmpty(strDeletedId))
                {
                    strDeletedId = strDeletedId.Substring(0, strDeletedId.Length - clsAdmin.CONSTCOMMASEPARATOR.ToString().Length);
                    string[] strDeletedIdSplit = strDeletedId.Split(clsAdmin.CONSTCOMMASEPARATOR);

                    for (int i = 0; i < strDeletedIdSplit.Length; i++)
                    {
                        intProdId = Convert.ToInt16(strDeletedIdSplit[i]);

                        intRecordAffected = prod.deleteAddonProd(prodId, intProdId);

                        if (intRecordAffected == 1) { bEdited = true; }
                        else
                        {
                            bSuccess = false;
                            break;
                        }
                    }
                }
            }


            if (bEdited)
            {
                Session["EDITPRODID"] = prodId;
                Response.Redirect(Request.Url.ToString());
            }
            else
            {
                Session["EDITPRODID"] = prodId;
                Session["NOCHANGE"] = 1;
                Response.Redirect(Request.Url.ToString());
            }
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            clsProduct prod = new clsProduct();
            if (prod.extractProdById(prodId, 0))
            {
                setPageProperties();

                if (mode == 2)
                {
                    fillForm();
                }

                bindRptProdData();
                bindRptProdSelData();
            }
            else
            {
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
                Response.Redirect(currentPageName);
            }
        }
        
        
        lnkbtnCancel.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
    }

    protected void setPageProperties()
    {
        clsMis mis = new clsMis();
        DataSet ds = new DataSet();
        DataView dv;

        ListItem ddlNewDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlNew.Items.Insert(0, ddlNewDefaultItem);
        ddlNew.Items.Add(new ListItem("Yes", "1"));
        ddlNew.Items.Add(new ListItem("No", "0"));

        ListItem ddlShowTopDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlShowTop.Items.Insert(0, ddlShowTopDefaultItem);
        ddlShowTop.Items.Add(new ListItem("Yes", "1"));
        ddlShowTop.Items.Add(new ListItem("No", "0"));

        DataSet dsGroup = new DataSet();
        clsGroup grp = new clsGroup();
        dsGroup = grp.getGrpListByGrpingId(0, 0);

        DataView dvCat = new DataView(dsGroup.Tables[0]);
        dvCat.RowFilter = "GRPING_ID = " + clsAdmin.CONSTLISTGROUPINGCAT;
        dvCat.Sort = "GRP_NAME ASC";

        ddlCat.DataSource = dvCat;
        ddlCat.DataTextField = "GRP_NAME";
        ddlCat.DataValueField = "GRP_ID";
        ddlCat.DataBind();

        ListItem ddlCatDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlCat.Items.Insert(0, ddlCatDefaultItem);
    }

    protected void fillForm()
    {
        dtProdSel = null;
        selectedId = null;
        deletedId = null;

        clsProduct prod = new clsProduct();
        DataSet ds = prod.getAddonProducts(prodId, 0);
        DataView dv = new DataView(ds.Tables[0]);

        dtProdSel = dv.ToTable();

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            selectedId += row["ADDON_ID"].ToString() + clsAdmin.CONSTCOMMASEPARATOR.ToString();
        }
    }

    protected void bindRptProdData()
    {
        clsProduct prod = new clsProduct();
        DataSet dsProd = prod.getProdList2(1);
        DataView dvProd = new DataView(dsProd.Tables[0]);

        string strSelectedId = prodId.ToString() + clsAdmin.CONSTCOMMASEPARATOR.ToString();

        if (selectedId != null && selectedId != "")
        {
            strSelectedId += selectedId.ToString().Substring(0, selectedId.ToString().Length - 1);
        }

        //if (selectedId != null && selectedId != "")
        //{
        //    strSelectedId += "," + selectedId.ToString().Substring(0, selectedId.ToString().Length - 1);
        //}
        dvProd.RowFilter = " PROD_ADDON = 1";
        dvProd.RowFilter += " AND PROD_ID NOT IN (" + strSelectedId + ")";
        
        if (boolSearch)
        {
            if(!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                strKeyword = txtKeyword.Text.Trim();
                dvProd.RowFilter += " AND (PROD_CODE LIKE '%" + strKeyword + "%' OR PROD_NAME LIKE '%" + strKeyword + "%' OR PROD_NAME_JP LIKE '%" + strKeyword + "%' OR PROD_NAME_MS LIKE '%" + strKeyword + "%' OR PROD_NAME_ZH LIKE '%" + strKeyword + "%' OR PROD_DNAME LIKE '%" + strKeyword + "%' OR PROD_DNAME_JP LIKE '%" + strKeyword + "%' OR PROD_DNAME_MS LIKE '%" + strKeyword + "%' OR PROD_DNAME_ZH LIKE '%" + strKeyword + "%')";

                //pnlSeachForm.CssClass = "divSearchFormShow";
                //hypHideShow.CssClass = "linkHide";
            }
            else
            {
                //pnlSeachForm.CssClass = "divSearchForm";
                //hypHideShow.CssClass = "linkShow";
            }

            if (ddlNew.SelectedValue != "") 
            {
                dvProd.RowFilter += " AND PROD_NEW = " + ddlNew.SelectedValue;
            }

            if (ddlShowTop.SelectedValue != "") 
            {
                dvProd.RowFilter += " AND PROD_SHOWTOP = " + ddlShowTop.SelectedValue;
            }

            if (!string.IsNullOrEmpty(ddlCat.SelectedValue))
            {
                int intGroup = int.Parse(ddlCat.SelectedValue);
                string strIds = prod.getProdIdsByGrpId(intGroup);
                dvProd.RowFilter += !string.IsNullOrEmpty(strIds) ? " AND PROD_ID IN (" + strIds + ")" : " AND 1 = 0";
            }
        }

        intProdCount = 0;
        intProdPageSize = clsAdmin.CONSTADMPAGESIZE;
        intProdTotal = dvProd.Count;

        litProdRelProdTotal.Text = dvProd.Count.ToString() + " record(s) listed.";

        if (dvProd.Count > 0)
        {
            PagedDataSource pdsProd;
            pdsProd = new PagedDataSource();
            pdsProd.DataSource = dvProd;
            pdsProd.AllowPaging = true;
            pdsProd.PageSize = clsAdmin.CONSTADMPAGESIZE;
            pageCount = pdsProd.PageCount;

            currentPage = pageNo > 0 ? (pageNo > pageCount ? pageCount - 1 : pageNo - 1) : 0;

            pdsProd.CurrentPageIndex = currentPage;

            rptProd.DataSource = pdsProd;
            rptProd.DataBind();

            pnlProdRelNoFound.Visible = false;
            pnlProdRelProd.Visible = true;

            int intCurrentPage = currentPage + 1;
            int intStart;
            int intEnd;
            int intPaginationSize = 9;
            int intPaginationSizeMid = Convert.ToInt16(intPaginationSize / 2);

            if (intCurrentPage <= intPaginationSizeMid)
            {
                intStart = 1;
                if (pageCount > intPaginationSize) { intEnd = intPaginationSize; }
                else { intEnd = pageCount; }
            }
            else if (intCurrentPage >= pageCount - intPaginationSizeMid)
            {
                intStart = pageCount - intPaginationSize + 1;
                if (intStart < 1) { intStart = 1; }
                intEnd = pageCount;
            }
            else
            {
                intStart = intCurrentPage - intPaginationSizeMid;
                intEnd = intCurrentPage + intPaginationSizeMid;
            }

            ArrayList pages = new ArrayList();
            for (int i = intStart; i <= intEnd; i++)
            {
                pages.Add(i);
            }

            //if (currentPage == 0)
            //{
            //    lnkbtnFirstTop.Visible = false;
            //    lnkbtnFirstBottom.Visible = false;

            //    if (pageCount > 1)
            //    {
            //        lnkbtnLastTop.Visible = true;
            //        lnkbtnLastBottom.Visible = true;
            //    }
            //}

            //if (currentPage == pageCount - 1)
            //{
            //    lnkbtnLastTop.Visible = false;
            //    lnkbtnLastBottom.Visible = false;

            //    if (pageCount != 1)
            //    {
            //        lnkbtnFirstTop.Visible = true;
            //        lnkbtnFirstBottom.Visible = true;
            //    }
            //}

            //if (currentPage != 0 && currentPage != pageCount - 1)
            //{
            //    lnkbtnFirstTop.Visible = true;
            //    lnkbtnFirstBottom.Visible = true;

            //    lnkbtnLastTop.Visible = true;
            //    lnkbtnLastBottom.Visible = true;
            //}

            rptPaginationTop.DataSource = pages;
            rptPaginationTop.DataBind();

            rptPaginationBottom.DataSource = pages;
            rptPaginationBottom.DataBind();
        }
        else
        {
            pnlProdRelNoFound.Visible = true;
            pnlProdRelProd.Visible = false;
        }
    }

    protected void bindRptProdSelData()
    {
        if (dtProdSel != null)
        {
            DataTable dt = dtProdSel;
            DataView dvProdSel = new DataView(dt);

            intProdCountSel = 0;
            intProdSelPageSize = clsAdmin.CONSTMAXPRODUCTRELATED;
            intProdSelTotal = dvProdSel.Count;

            if (dvProdSel.Count > 0)
            {
                PagedDataSource pdsProdSel;
                pdsProdSel = new PagedDataSource();
                pdsProdSel.DataSource = dvProdSel;
                pdsProdSel.AllowPaging = false;
                pdsProdSel.PageSize = clsAdmin.CONSTMAXPRODUCTRELATED;
                rptProdSel.DataSource = pdsProdSel;
                rptProdSel.DataBind();

                pnlProdSelList.Visible = true;
                pnlProdSelNoFound.Visible = false;
            }
            else
            {
                pnlProdSelList.Visible = false;
                pnlProdSelNoFound.Visible = true;
            }
        }
    }

    protected DataTable createDataTable()
    {
        DataTable dtSel = new DataTable();
        DataColumn col;

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.Int16");
        col.ColumnName = "ADDON_ID";
        dtSel.Columns.Add(col);

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.String");
        col.ColumnName = "ADDON_NAME";
        dtSel.Columns.Add(col);

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.String");
        col.ColumnName = "ADDON_IMAGE";
        dtSel.Columns.Add(col);
        
        return dtSel;
    }

    protected void addSelection(int intSelectedId, string strSelProdName, string strSelProdImg)
    {
        DataTable dt = new DataTable();

        if (dtProdSel != null)
        {
            dt = dtProdSel;
        }
        else
        {
            dt = createDataTable();
        }

        int intSelectedCount = 0;

        if (selectedId != null && selectedId != "")
        {
            string strSelectedId = selectedId;
            string[] strSelectedIdSplit = strSelectedId.Split((char)',');
            intSelectedCount = strSelectedIdSplit.Length;
        }

        if (intSelectedCount <= clsAdmin.CONSTMAXPRODUCTRELATED)
        {
            DataRow[] foundRow = dt.Select("ADDON_ID = '" + intSelectedId + "'");
            if (foundRow.Length <= 0)
            {
                DataRow row;
                row = dt.NewRow();
                row["ADDON_ID"] = intSelectedId;
                row["ADDON_NAME"] = strSelProdName;
                row["ADDON_IMAGE"] = strSelProdImg;
                dt.Rows.Add(row);
                selectedId += intSelectedId + ",";
            }

            dtProdSel = dt;
        }
        else
        {
            pnlAck.Visible = true;
            litAck.Text = "<div class=\"errmsg\">Maximum " + clsAdmin.CONSTMAXPRODUCTRELATED + " addon products.</div>";
        }
    }

    protected void updateDeletedList(int intRelProdId)
    {
        if (deletedId != null && deletedId != "")
        {
            string strDeletedId = deletedId;
            string[] strDeletedIdSplit = strDeletedId.Split((char)',');

            strDeletedId = "";
           
            for (int i = 0; i < strDeletedIdSplit.Length - 1; i++)
            {

                if (int.Parse(strDeletedIdSplit[i]) != intRelProdId)
                {
                    strDeletedId += strDeletedIdSplit[i] + ",";
                }
            }
            
            deletedId = strDeletedId;
        }
    }
    #endregion
}
