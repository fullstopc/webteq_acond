﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmProductPrice.ascx.cs" Inherits="ctrl_ucAdmProductPrice" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>
<script type="text/javascript">
    var txtProdPromStartDate;
    var txtProdPromEndDate;

    function initSect3() {
        txtProdPromStartDate = document.getElementById("<%= txtProdPromStartDate.ClientID %>");
        txtProdPromEndDate = document.getElementById("<%= txtProdPromEndDate.ClientID %>");
        txtProdPrice = document.getElementById("<%= txtProdPrice.ClientID %>");
        
        $(function() {
            var option = {
                format: 'd M Y h:i A',
                minDate:0,
            };
            $("#<% =txtProdPromStartDate.ClientID %>").datetimepicker(option);
            $("#<% =txtProdPromEndDate.ClientID %>").datetimepicker(option);
            

        });
    }

    function validateProdCost_client(source, args) {
        var txtProdCost = document.getElementById("<%= txtProdCost.ClientID %>");
        txtProdCost.value = formatCurrency(txtProdCost.value);

        var currRE = /<%= clsAdmin.CONSTCURRENCYRE3 %>/;

        if (txtProdCost.value.match(currRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "<br />Please enter only currency.";
            source.innerHTML = "<br />Please enter only currency.";
        }
    }

    function validateProdPrice_client(source, args) {
        var txtProdPrice = document.getElementById("<%= txtProdPrice.ClientID %>");
        txtProdPrice.value = formatCurrency(txtProdPrice.value);

        var currRE = /<%= clsAdmin.CONSTCURRENCYRE3 %>/;

        if (Trim(txtProdPrice.value) == '') {
            if ((Trim(txtProdPromStartDate.value) != '') || (Trim(txtProdPromEndDate.value) != '')) {
                args.IsValid = false;
                source.errormessage = "<br />Promotion is activated. Please enter normal price.";
                source.innerHTML = "<br />Promotion is activated. Please enter normal price.";
            }
        }
        else {
            if (txtProdPrice.value.match(currRE)) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
                source.errormessage = "<br />Please enter only currency.";
                source.innerHTML = "<br />Please enter only currency.";
            }
        }
    }

    function validateProdPromPrice_client(source, args) {
        var txtProdPromPrice = document.getElementById("<%= txtProdPromPrice.ClientID %>");
        txtProdPromPrice.value = formatCurrency(txtProdPromPrice.value);

        var currRE = /<%= clsAdmin.CONSTCURRENCYRE3 %>/;

        if (Trim(txtProdPromPrice.value) == '') {
            if ((Trim(txtProdPromStartDate.value) != '') || (Trim(txtProdPromEndDate.value) != '')) {
                args.IsValid = false;
                source.errormessage = "<br />Promotion is activated. Please enter promotion price.";
                source.innerHTML = "<br />Promotion is activated. Please enter promotion price.";
            }
        }
        else {
            if (txtProdPromPrice.value.match(currRE)) {
                var decPromPrice = parseFloat(txtProdPromPrice.value);
                var decProdPrice = parseFloat(Trim(txtProdPrice.value));
                
                if ((Trim(txtProdPrice.value) != "") && (decPromPrice >= decProdPrice)) {
                    args.IsValid = false;
                    source.errormessage = "<br />Please enter valid promotion price.";
                    source.innerHTML = "<br />Please enter valid promotion price.";
                }
                else {
                    args.IsValid = true;
                }
            }
            else {
                args.IsValid = false;
                source.errormessage = "<br />Please enter only currency.";
                source.innerHTML = "<br />Please enter only currency.";
            }
        }
    }

    function validateProdPromPeriod_client(source, args) {
        var bValid = false;

        source.errormessage = "<br />Please enter valid promotion period.";
        source.innerHTML = "<br />Please enter valid promotion period.";

        if ((Trim(txtProdPromStartDate.value) == '') && (Trim(txtProdPromEndDate.value) == '')) {
            args.IsValid = true;
        }
        else {
            if (validateDateRange2(txtProdPromStartDate.value, txtProdPromEndDate.value)) {
                bValid = true;
            }
            else {
                bValid = false;
            }

            args.IsValid = bValid;
        }
    }
    function validateProdPromPeriod2_client(source, args){

        var dateFromTxt = txtProdPromStartDate.value,
            dateToTxt = txtProdPromEndDate.value;
        var isEmpty = (Trim(dateFromTxt) == '') || (Trim(dateToTxt) == '');
        var isDateRange = validateDateRange2(dateFromTxt, dateToTxt);
    
        if(isEmpty) {
            args.IsValid = false;
            source.errormessage = "<br/>Please enter promotion period From and To";
            source.innerHTML = "<br/>Please enter promotion period From and To";
        }

        if(!isDateRange){
            args.IsValid =  false;
            source.errormessage = "<br/>Please enter valid promotion period From and To";
            source.innerHTML = "<br/>Please enter valid promotion period From and To";
        }

        if(!isEmpty && isDateRange){
            var editingIndex = $("#<%= lnkbtnUpdatePromo.ClientID %>").attr("data-promoid");
            $("#tblPromo > tbody > tr").each(function(index){
                // if updating, dont validate self data.
                if(index == editingIndex){
                    return true;
                }
                var $tr = $(this),
                    promoFrom = new Date($tr.find(".spanPromoFrom").html()),
                    promoTo = new Date($tr.find(".spanPromoTo").html());

                var dateFrom = new Date(dateFromTxt);
                var dateTo = new Date(dateToTxt);

                if(!((promoFrom > dateTo)  || (dateFrom > promoTo))){
                    args.IsValid =  false;
                    source.errormessage = "<br/>This date range conflict with existing one.";
                    source.innerHTML = "<br/>This date range conflict with existing one.";

                    return false;
                }


            });
        }

       
    }
    function validateCurrency_client(source, args) {
        var currRE = /<%= clsAdmin.CONSTCURRENCYRE %>/;

        if (args.Value.match(currRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "<br />Please enter valid value.";
            source.innerHTML = "<br />Please enter valid value.";
        }
    }

    function clearPromoPrice(){
        $("#<%= txtProdPromPrice.ClientID %>").val("");
        $("#<%= txtProdPromStartDate.ClientID %>").val("");
        $("#<%= txtProdPromEndDate.ClientID %>").val("");  
    }

    function resetPricingForm(){
        $("#<%= hdnPricingID.ClientID %>").val("");
        $("#<%= txtPriceName1.ClientID %>").val("");
        $("#<%= txtPriceName2.ClientID %>").val("");
        $("#<%= txtPricing.ClientID %>").val("");
        $("#<%= txtEquivalent.ClientID %>").val("");

        $("#<%= lnkbtnAddPricing.ClientID %>").show();
        $("#<%= lnkbtnEditPricing.ClientID %>").hide();
        $("#<%= lnkbtnCancelPricing.ClientID %>").hide();
    }
</script>

<asp:Panel ID="pnlSectForm3" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table id="tblAddMember3" cellpadding="0" cellspacing="0" class="formTbl tblData">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Product Pricing</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel">Product Cost:</td>
                    <td class="tdMax"><span class="spanFieldDesc"><asp:Literal ID="litProdCostCurr" runat="server"></asp:Literal></span>
                        <asp:TextBox ID="txtProdCost" runat="server" class="text_medium" MaxLength="15" onchange="formatCurrency(this.value)"></asp:TextBox>
                        <span class="spanFieldDesc"><asp:CustomValidator ID="cvProdCost" runat="server" ControlToValidate="txtProdCost" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateProdCost_client"></asp:CustomValidator></span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel">Product Price:</td>
                    <td><span class="spanFieldDesc"><asp:Literal ID="litProdPriceCurr" runat="server"></asp:Literal></span>
                        <asp:TextBox ID="txtProdPrice" runat="server" class="text_medium" onchange="formatCurrency(this.value)"></asp:TextBox>
                        <span class="fieldDesc"><asp:CustomValidator ID="cvProdPrice" runat="server" ControlToValidate="txtProdPrice" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateProdPrice_client"></asp:CustomValidator></span>
                    </td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr runat="server" id="trMultiAction">
                    <td></td>
                    <td>
                        <asp:LinkButton ID="lnkbtnSave2" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnBack2" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                    </td>
                </tr>
            </tbody>

        </table>
        <table cellpadding="0" cellspacing="0" class="formTbl tblData">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Promotion Pricing 
                        <span class="spanSectionHdrLink">
                            <a class="hypClear" title="Clear" href="javascript:clearPromoPrice()">Clear</a>
                        </span>
                        
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel">Promotion Price:</td>
                    <td><span class="spanFieldDesc"><asp:Literal ID="litProdPromPriceCurr" runat="server"></asp:Literal></span>
                        <asp:TextBox ID="txtProdPromPrice" runat="server" class="text_medium" onchange="formatCurrency(this.value)"></asp:TextBox>
                        <span class="fieldDesc">
                            <asp:CustomValidator ID="cvProdPromPrice" runat="server" ControlToValidate="txtProdPromPrice" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateProdPromPrice_client" ValidateEmptyText="true"></asp:CustomValidator>
                            <asp:RequiredFieldValidator ID="rvProdPromPrice" 
                                                        runat="server" ErrorMessage="<br />Please enter promo price." 
                                                        ControlToValidate="txtProdPromPrice" 
                                                        CssClass="errmsg" Display="Dynamic" 
                                                        ValidationGroup="vgPromoSet"></asp:RequiredFieldValidator>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel nobr">Promotion Period:</td>
                    <td>
                        <span class="spanFieldDesc"> From </span>
                        <div class="clearable">
                            <asp:TextBox ID="txtProdPromStartDate" runat="server" class="text clearable"></asp:TextBox>
                            <i class="remove">&times;</i>
                        </div>
                        <span class="spanFieldDesc"> To </span>
                        <div class="clearable">
                            <asp:TextBox ID="txtProdPromEndDate" runat="server" CssClass="text clearable"></asp:TextBox>
                            <i class="remove">&times;</i>
                        </div>
                        
                        <span class="fieldDesc">
                            <asp:CustomValidator ID="cvProdPromPeriod" runat="server" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateProdPromPeriod_client"></asp:CustomValidator>
                            <asp:CustomValidator ID="cvProdPromPeriod2" 
                                                        runat="server"
                                                        ClientValidationFunction="validateProdPromPeriod2_client"
                                                        CssClass="errmsg" Display="Dynamic" 
                                                        ValidationGroup="vgPromoSet"></asp:CustomValidator>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr runat="server" id="trSingleAction">
                    <td></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                        </asp:Panel>

                    </td>
                </tr>
                <tr runat="server" id="trProdPromoAdd">
                    <td></td>
                    <td>
                        <asp:LinkButton ID="lnkbtnAddPromo" runat="server" Text="Add" ToolTip="Add" class="btn" OnClick="lnkbtnAddPromo_Click" ValidationGroup="vgPromoSet"></asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnUpdatePromo" runat="server" Text="Update" ToolTip="Update" class="btn" OnClick="lnkbtnUpdatePromo_Click" ValidationGroup="vgPromoSet" Visible="false" data-promoid="-1"></asp:LinkButton>
                        <asp:HiddenField ID="hdnPromoID" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
            </tbody>
            <tbody runat="server" id="tbodyMultiProdPromo">
                <tr>
                    <td colspan="2">
                        <asp:Repeater ID="rptPromo" runat="server" OnItemCommand="rptPromo_ItemCommand">
                            <HeaderTemplate>
                                <table cellpadding="0" cellspacing="0" class="dataTbl" id="tblPromo">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Price</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th class="alignCenter">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                
                                    <tr>
                                        <td><asp:Literal runat="server" ID="litNo" Text='<%# Container.ItemIndex + 1 %>'></asp:Literal></td>
                                        <td><asp:Literal runat="server" ID="litPrice" Text='<%# String.Format("{0:f2}", DataBinder.Eval(Container.DataItem,"PROMO_PRICE")) %>'></asp:Literal></td>
                                        <td><span class="spanPromoFrom"><asp:Literal runat="server" ID="litFrom" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem,"PROMO_FROM")).ToString("dd MMM yyyy") %>'></asp:Literal></span></td>
                                        <td><span class="spanPromoTo"><asp:Literal runat="server" ID="litTo" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem,"PROMO_TO")).ToString("dd MMM yyyy") %>'></asp:Literal></span></td>
                                        <td class="tdAct">
                                            <asp:LinkButton ID="lnkbtnEdit" CssClass="lnkbtn lnkbtnEdit" runat="server" Text="Edit" ToolTip="Edit" CommandName="cmdEdit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PROMO_ID") %>'></asp:LinkButton>

                                            <asp:LinkButton ID="lnkbtnDelete" CssClass="lnkbtn lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CommandName="cmdDelete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PROMO_ID") %>' OnClientClick="return confirm('Are you sure you want to delete?');"></asp:LinkButton>
                                        </td>
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </tbody>
        </table>
        <asp:UpdatePanel runat="server" ID="upnlPricing">
            <ContentTemplate>
                <table cellpadding="0" cellspacing="0" class="formTbl tblData" runat="server" id="tblPriceSet" data-beforeleave="0">
                    <thead>
                        <tr>
                            <td colspan="2" class="tdSectionHdr">Pricing Set</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="tdSpacer">&nbsp;</td>
                            <td class="tdSpacer">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="tdLabel nobr">Pricing Name 1:</td>
                            <td>
                                <asp:TextBox ID="txtPriceName1" runat="server" CssClass="text_big" MaxLength="250"></asp:TextBox>
                                <span class="fieldDesc">
                                    <asp:RequiredFieldValidator ID="rfvPriceName1" runat="server" ErrorMessage="<br />Please enter Pricing Name 1." ControlToValidate="txtPriceName1" CssClass="errmsg" Display="Dynamic" ValidationGroup="vgPricingSet"></asp:RequiredFieldValidator>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel nobr">Pricing Name 2:</td>
                            <td>
                                <asp:TextBox ID="txtPriceName2" runat="server" CssClass="text_big" MaxLength="250"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel nobr">Pricing:</td>
                            <td>
                                <span class="spanFieldDesc"><asp:Literal ID="litPricingUnit" runat="server"></asp:Literal></span>
                                <asp:TextBox ID="txtPricing" runat="server" class="text_medium" MaxLength="10" onchange="this.value = formatCurrency(this.value);"></asp:TextBox>
                                <span class="fieldDesc">
                                    <asp:RequiredFieldValidator ID="rfvPricing" runat="server" ErrorMessage="<br />Please enter Pricing." ControlToValidate="txtPricing" CssClass="errmsg" Display="Dynamic" ValidationGroup="vgPricingSet"></asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="cvPricing" runat="server" ControlToValidate="txtPricing" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateCurrency_client" ValidationGroup="vgPricingSet"></asp:CustomValidator>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel nobr">Equivalent to:</td>
                            <td>
                                <asp:TextBox ID="txtEquivalent" runat="server" CssClass="text_medium" MaxLength="10"></asp:TextBox>
                                <span ID="spanUOM" class="spanFieldDesc" runat="server">Unit</span>
                                <span class="fieldDesc">
                                <asp:CompareValidator ID="cvEquivalent" runat="server" ControlToValidate="txtEquivalent" Type="Integer" Operator="DataTypeCheck" ErrorMessage="<br/>Value must be an integer!"  ValidationGroup="vgPricingSet"/>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:LinkButton ID="lnkbtnAddPricing" runat="server" Text="Add" ToolTip="Add" CssClass="btn" OnClick="lnkbtnAddPricing_Click" ValidationGroup="vgPricingSet"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnEditPricing" runat="server" Text="Save" ToolTip="Save" CssClass="btn" style="display:none;" OnClick="lnkbtnEditPricing_Click" ValidationGroup="vgPricingSet"></asp:LinkButton>
                                <asp:HyperLink ID="lnkbtnCancelPricing" runat="server" Text="Cancel" ToolTip="Cancel" CssClass="btn btnBack" style="display:none;" onclick="resetPricingForm();"></asp:HyperLink>
                                <asp:HiddenField runat="server" ID="hdnPricingID" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdSpacer">&nbsp;</td>
                            <td class="tdSpacer">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-bottom:10px;"><u>Added Pricing Set(s)</u></td>
                        </tr>
                        <tr id="trPricingNoRecord" runat="server" visible="false">
                            <td colspan="2">No record found.</td>
                        </tr>
                        <tr id="trPricingList" runat="server">
                            <td colspan="2">
                                <asp:Repeater ID="rptPricing" runat="server" OnItemDataBound="rptPricing_ItemDataBound" OnItemCommand="rptPricing_ItemCommand">
                                    <HeaderTemplate>
                                        <table cellpadding="0" cellspacing="0" class="dataTbl">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Name 1</th>
                                                    <th>Name 2</th>
                                                    <th>Price</th>
                                                    <th>Equivalent</th>
                                                    <th class="alignCenter">Action</th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                            <tr>
                                                <td><asp:Label ID="lblNo" runat="server" CssClass="nowrap"></asp:Label></td>
                                                <td><asp:Label ID="lblName" runat="server" CssClass="nowrap"></asp:Label></td>
                                                <td><asp:Label ID="lblName2" runat="server" CssClass="nowrap"></asp:Label></td>
                                                <td><asp:Label ID="lblPrice" runat="server" CssClass="nowrap"></asp:Label></td>
                                                <td><asp:Label ID="lblEquivalent" runat="server" CssClass="nowrap"></asp:Label></td>
                                                <td class="tdAct">
                                                    <asp:LinkButton ID="lnkbtnEdit" CssClass="lnkbtn lnkbtnNoIcon" runat="server" ToolTip="Edit" CommandName="cmdEdit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PRICING_ID") %>'>
                                                        <i class="material-icons">mode_edit</i>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnDeactivate" CssClass="lnkbtn lnkbtnNoIcon" runat="server" ToolTip="Deactivate" CommandName="cmdDeactive" Visible="false" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PRICING_ID") %>'>
                                                        <i class="material-icons">visibility_off</i>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnDelete" CssClass="lnkbtn lnkbtnNoIcon" runat="server" ToolTip="Delete" CommandName="cmdDelete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PRICING_ID") + "|" + DataBinder.Eval(Container.DataItem, "PRICING_ORDER") %>'>
                                                        <i class="material-icons">delete</i>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnUp" CssClass="lnkbtn lnkbtnNoIcon" runat="server" ToolTip="Up" CommandName="cmdUp" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PRICING_ID") + "|" + DataBinder.Eval(Container.DataItem, "PRICING_ORDER") %>'>
                                                        <i class="material-icons">arrow_upward</i>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnDown" CssClass="lnkbtn lnkbtnNoIcon" runat="server" ToolTip="Down" CommandName="cmdDown" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PRICING_ID") + "|" + DataBinder.Eval(Container.DataItem, "PRICING_ORDER") %>'>
                                                        <i class="material-icons">arrow_downward</i>
                                                    </asp:LinkButton>

                                                    <asp:LinkButton ID="lnkbtnActivate" CssClass="lnkbtn lnkbtnNoIcon" runat="server" ToolTip="Activate" CommandName="cmdActive" Visible="false" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PRICING_ID") %>'>

                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="uprgPricing" runat="server" AssociatedUpdatePanelID="upnlPricing" DynamicLayout="true" DisplayAfter="10">
		    <ProgressTemplate>
			    <uc:AdmLoading ID="ucAdmLoading" runat="server" />
		    </ProgressTemplate>
	    </asp:UpdateProgress>
    </asp:Panel>

</asp:Panel>
