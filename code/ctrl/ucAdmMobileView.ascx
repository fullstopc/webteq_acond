﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmMobileView.ascx.cs" Inherits="ctrl_ucAdmMobileView" %>
<%@ Register Src="~/ctrl/ucUsrLoading.ascx" TagName="UsrLoading" TagPrefix="uc" %>
<asp:Panel ID="pnlForm3" runat="server">
        <asp:Panel ID="pnlFormDetails" runat="server">
             <asp:UpdatePanel ID="upnlMobileView" runat="server">
                <ContentTemplate>
                    <asp:UpdateProgress ID="uprgMobileView" runat="server" AssociatedUpdatePanelID="upnlMobileView" DynamicLayout="true">
                        <ProgressTemplate>
                            <uc:UsrLoading ID="ucUsrLoadingMobileView" runat="server" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
                        <tr>
                            <td colspan="2" class="tdSectionHdr">PAGE DETAILS</td>
                        </tr>
                        <tr>
                            <td class="tdSpacer">&nbsp;</td>
                            <td class="tdSpacer">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="tdLabel nobr">Show in Mobile view:</td>
                            <td class="tdLabel"><asp:CheckBox ID="chkboxShowMobileView" runat="server" /></td>
                        </tr>
                        <tr>
                            <td class="tdLabel">Show in Menu:</td>
                            <td class="tdLabel">
                                <asp:Panel ID="pnlLoadingShowInMenu" runat="server" CssClass="divCmnPageDetailsLoadingContainer">
                                    
                                            <asp:CheckBox ID="chkboxShowInMenu" runat="server" Checked="true" OnCheckedChanged="chkboxShowInMenu_CheckedChanged" AutoPostBack="true" />
                                            <asp:Panel ID="pnlShowInMenu" runat="server" CssClass="divShowInMenu" Visible="false">
                                                <asp:CheckBox ID="chkboxTopMenu" runat="server" Text="Top Menu" Checked="true"/>
                                                <asp:CheckBox ID="chkboxBottomMenu" runat="server" Text="Bottom Menu" Checked="true" />
                                            </asp:Panel>
                                        
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel">Template:</td>
                            <td class="tdMax"><asp:DropDownList ID="ddlTemplate" runat="server" CssClass="ddl_fullwidth"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td class="tdLabel">Menu Styling:</td>
                            <td class="tdMax"><asp:DropDownList ID="ddlCssClass" runat="server" CssClass="ddl_fullwidth"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td class="tdLabel">Redirection:</td>
                            <td class="tdMax"><asp:DropDownList ID="ddlRedirect" runat="server" CssClass="ddl_fullwidth" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlRedirect_SelectedIndexChanged"></asp:DropDownList></td>
                        </tr>        
                        <tr>
                            <td></td>
                            <td>
                                <asp:Panel ID="pnlExtLink" runat="server" Visible="false">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td class="tdLabelExLink">Link:</td>
                                            <td><asp:TextBox ID="txtLink" runat="server" CssClass="text_fullwidth" MaxLength="1000"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="tdLabelExLink">Target:</td>
                                            <td><asp:DropDownList ID="ddlTarget" runat="server" CssClass="ddl_fullwidth" AutoPostBack="false" CausesValidation="false" OnSelectedIndexChanged="ddlTarget_SelectedIndexChanged"></asp:DropDownList></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdSpacerPageDetails"></td>
                            <td class="tdSpacerPageDetails"></td>
                        </tr>
                        <tr>
                            <td class="tdSpacer">&nbsp;</td>
                            <td class="tdSpacer">&nbsp;</td>
                        </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
                <tr>
                    <td colspan="2">
                        <table cellpadding="0" cellspacing="0" border="0" class="formTblNested">
                            <tr>
                                <td class="tdSectionHdr tdWidth">PAGE CONTENT
                                    <asp:HyperLink ID="hypPreview" runat="server" CssClass="btnPreview" Width="13px" Height="12px" Visible="false"><asp:Image ID="imgPreview" runat="server" /></asp:HyperLink></td>
                                <td class="tdAction">
                                    <asp:UpdatePanel runat="server" ID="upnlBackup" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="lnkbtnBackup" runat="server" OnClick="lnkbtnBackup_Click" ToolTip="Backup This Version" CssClass="btn btnSave">Backup this</asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnRestore" runat="server" ToolTip="Restore" CssClass="thickbox btn btnBack">Restore</asp:LinkButton>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lnkbtnBackup" />
                                            <%--<asp:PostBackTrigger ControlID="lnkbtnRestore" />--%>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2"><asp:TextBox ID="txtPageContent" runat="server" TextMode="MultiLine" Rows="5" CssClass="text_big"></asp:TextBox></td>
                </tr>
                <tfoot>
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </asp:Panel>
</asp:Panel>
