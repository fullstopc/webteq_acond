﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucUsrChangePassword : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _memId;
    protected string _lang = "en";

    public bool isMobile = false;
    #endregion

    #region "Property Methods"
    public int memId
    {
        get
        {
            if (ViewState["MEMID"] == null)
            {
                return _memId;
            }
            else
            {
                return Convert.ToInt16(ViewState["MEMID"]);
            }
        }
        set { ViewState["MEMID"] = value; }
    }

    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void cvCurrentPwd_PreRender(object sender, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("usrmempwd"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtCurrentPwd.Text.Trim() + "', document.all['" + cvCurrentPwd.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "usrmempwd", strJS, false);
        }
    }

    protected void validateCurrentPwd_server(object source, ServerValidateEventArgs args)
    {
        if (!string.IsNullOrEmpty(txtCurrentPwd.Text.Trim()))
        {
            clsMD5 md5 = new clsMD5();
            string strCurPwd = md5.encrypt(txtCurrentPwd.Text.Trim());

            if (string.Compare(strCurPwd, hdnPassword.Value) != 0)
            {
                args.IsValid = false;

                cvCurrentPwd.ErrorMessage = "<br />Current password does not match.";
            }
            else
            {
                args.IsValid = true;
            }
        }
    }

    protected void lnkbtnChange_OnClick(object sender, EventArgs e)
    {
        memId = Convert.ToInt32(Session["MEM_ID"]);
        if (memId > 0)
        {
            if (Page.IsValid)
            {
                clsContestant mem = new clsContestant();

                Boolean isChange = false;
                Boolean isSuccess = true;

                string password = hdnPassword.Value;
                string passwordNew = new clsMD5().encrypt(txtNewPwd.Text);

                bool isSame = !(string.Compare(password, passwordNew) != 0);

                if (!isSame)
                {
                    if (mem.extractByID(memId))
                    {
                        mem.password = passwordNew;
                        mem.ID = memId;
                        mem.updatedby = memId;

                        if (mem.update())
                        {
                            isChange = true;
                        }
                        else
                        {
                            isSuccess = false;
                        }
                    }
                }

                if (!isSuccess)
                {
                    Session["ERRPR"] = GetGlobalResourceObject("GlobalResource","crud_error").ToString();
                }
                else if (isChange)
                {
                    Session["EDITED"] = GetGlobalResourceObject("GlobalResource", "crud_updated").ToString();
                }
                else if (!isChange && isSuccess)
                {
                    Session["NOCHANGE"] = GetGlobalResourceObject("GlobalResource", "crud_nochange").ToString();
                }

                Response.Redirect(clsMis.getCurrentPageName());
            }
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        memId = Convert.ToInt32(Session["MEM_ID"]);

        clsContestant mem = new clsContestant();
        if (mem.extractByID(memId))
        {
            hdnPassword.Value = mem.password;
        }
        
    }
    #endregion
}
