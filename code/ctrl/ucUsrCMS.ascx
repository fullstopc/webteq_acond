﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrCMS.ascx.cs" Inherits="ctrl_ucUsrCMS" %>
<%@ Register Src="~/ctrl/ucCmnPageDetails.ascx" TagName="CmnPageDetails" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucCmnSiblingPages.ascx" TagName="CmnSiblingPages" TagPrefix="uc" %>

<asp:Panel ID="pnlCMS" runat="server" CssClass="divCMS">
    <asp:Panel ID="pnlAck" runat="server" CssClass="divCMSAck" Visible="false">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
        <asp:Panel ID="pnlButtonAck" runat="server" CssClass="divCMSAction" Visible="false">
            <asp:LinkButton ID="lnkbtnAckCancel" runat="server" meta:resourcekey="lnkbtnCancel" CssClass="btn" OnClick="lnkbtnAckCancel_OnClick"></asp:LinkButton>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlContentSection" runat="server" CssClass="divCMSContentSection">
        <asp:Panel ID="pnlEditorStart" runat="server" CssClass="divEditorStart" Visible="false">
        </asp:Panel>
        <asp:Panel ID="pnlContent" runat="server" CssClass="divCMSContent">
            <asp:Literal ID="litContent" runat="server"></asp:Literal>
            <asp:HiddenField ID="hdnContent" runat="server" />
        </asp:Panel>
        <asp:Panel ID="pnlAdminAction" runat="server" CssClass="divCMSAdminAction" Visible="false">
            <asp:Panel ID="pnlViewMode" runat="server" CssClass="divCMSViewMode">
                <asp:Panel ID="pnlViewModeAction" runat="server" CssClass="divCMSAction">
                    <asp:LinkButton ID="lnkbtnAddNew" runat="server" meta:resourcekey="lnkbtnAddNew" CssClass="btn" OnClick="lnkbtnAddNew_OnClick"></asp:LinkButton>
                    <asp:LinkButton ID="lnkbtnUpdate" runat="server" meta:resourcekey="lnkbtnUpdate" CssClass="btn" OnClick="lnkbtnUpdate_OnClick"></asp:LinkButton>
                </asp:Panel>
                <asp:Panel ID="pnlHeaderViewMode" runat="server" CssClass="divCMSViewModeItems">
                    <uc:CmnPageDetails ID="ucCmnPageDetailsView" runat="server" viewType="User" modeType="View" />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlEditMode" runat="server" CssClass="divCMSEditMode" Visible="false">
                <asp:Panel ID="pnlEditor" runat="server" CssClass="divCMSEditor">
                    <asp:TextBox ID="txtContent" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>
                </asp:Panel>
                <asp:Panel ID="pnlEditModeAck" runat="server" CssClass="divCMSEditModeAck" Visible="false">
                    <asp:Literal ID="litEditModeAck" runat="server"></asp:Literal>
                </asp:Panel>
                <asp:Panel ID="pnlHeaderEditMode" runat="server" CssClass="divCMSEditModeItems">
                    <uc:CmnPageDetails ID="ucCmnPageDetailsEdit" runat="server" viewType="User" modeType="Edit" />
                </asp:Panel>
                <asp:Panel ID="pnlButton" runat="server" CssClass="divCMSAction">
                    <asp:LinkButton ID="lnkbtnSave" runat="server" meta:resourcekey="lnkbtnSave" CssClass="btn" OnClick="lnkbtnSave_OnClick"></asp:LinkButton>
                    <asp:LinkButton ID="lnkbtnCancel" runat="server" meta:resourcekey="lnkbtnCancel" CssClass="btn" OnClick="lnkbtnCancel_OnClick"></asp:LinkButton>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlSiblingPages" runat="server" CssClass="divCMSSiblingPages">
                <uc:CmnSiblingPages ID="ucCmnSiblingPages" runat="server" viewType="User" enableLanguage="false" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Panel>
<asp:PlaceHolder ID="phCMS" runat="server"></asp:PlaceHolder>
