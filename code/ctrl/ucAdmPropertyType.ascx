﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmPropertyType.ascx.cs" Inherits="ctrl_ucAdmPropertyType" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<script type="text/javascript">
    function setAutocomplete(target) {
        $.ajax({
            type: "POST",
            url: wsBase + "wsSearchType.asmx/getType",
            dataType: "json",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                console.log(data);
                var strList = data.d.split(',');
                $("#" + target).autocomplete({
                    source: strList,
                    change: function(event, ui) {
                        //fillForm(target);
                    },
                    appendTo: "#autocompletecontainer1",
                });
            },
            error: function(xmlHttpRequest, status, err) {
                //alert("error " + err);
            }
        });
    }
</script>

<asp:TextBox ID="txtType" runat="server" CssClass="form-control" MaxLength="45" placeholder="Type"></asp:TextBox>
<div id="autocompletecontainer1"></div>