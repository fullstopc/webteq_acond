﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ctrl_ucAdmCouponDetails : System.Web.UI.UserControl
{
    #region "Properties"
    public int sectId = 28;
    protected int _couponId = 0;
    protected int _mode = 1;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int couponId
    {
        get
        {
            if (ViewState["couponId"] == null)
            {
                return _couponId;
            }
            else
            {
                return Convert.ToInt16(ViewState["couponId"]);
            }
        }
        set { ViewState["couponId"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void validateCouponCode_server(object source, ServerValidateEventArgs args)
    {
        txtCouponCode.Text = txtCouponCode.Text.Trim().ToUpper();

        clsCoupon coupon = new clsCoupon();

        if (coupon.isCouponCodeExist(txtCouponCode.Text, couponId))
        {
            args.IsValid = false;
            cvCouponCode.ErrorMessage = "<br/>This coupon code is in used.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvCouponCode_PreRender(object source, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("couponcode")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtCouponCode.ClientID + "', document.all['" + cvCouponCode.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "couponcode", strJS);
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string couponName = txtCouponName.Text.Trim();
            string couponCode = txtCouponCode.Text.Trim();
            string disOperator = "";
            decimal decDisValue = Convert.ToDecimal("0.00");
            DateTime dtCouponPeriodFrom = DateTime.MaxValue;
            DateTime dtCouponPeriosTo = DateTime.MaxValue;
            int active = 0;

            if (!string.IsNullOrEmpty(ddlDisOperator.SelectedValue)) { disOperator = ddlDisOperator.SelectedValue; }

            decimal decTryParse = Convert.ToDecimal("0.00");

            if (!string.IsNullOrEmpty(txtDisValue.Text.Trim()))
            {
                if (decimal.TryParse(txtDisValue.Text.Trim(), out decTryParse)) { decDisValue = decTryParse; }
            }

            if (!string.IsNullOrEmpty(txtCouponPeriodFrom.Text.Trim()) && !string.IsNullOrEmpty(txtCouponPeriodTo.Text.Trim()))
            {
                DateTime dtTryParse;

                if (DateTime.TryParse(clsMis.formatDateDDMMtoMMDD(hdnCouponPeriodFrom.Value.Trim()), out dtTryParse))
                {
                    dtCouponPeriodFrom = dtTryParse;
                }

                if (DateTime.TryParse(clsMis.formatDateDDMMtoMMDD(hdnCouponPeriodTo.Value.Trim()), out dtTryParse))
                {
                    dtCouponPeriosTo = dtTryParse;
                }
            }

            if (chkboxActive.Checked) { active = 1; }

            clsCoupon coupon = new clsCoupon();
            int intRecordAffected = 0;
            Boolean boolEdited = false;
            Boolean boolSuccess = true;

            if (mode == 1)
            {
                intRecordAffected = coupon.addCoupon(couponName, couponCode, disOperator, decDisValue, dtCouponPeriodFrom, dtCouponPeriosTo, active, Convert.ToInt16(Session["ADMID"]));

                if (intRecordAffected == 1)
                {
                    Session["NEWCOUPONID"] = coupon.couponId;
                    Response.Redirect(currentPageName + "?id=" + coupon.couponId);
                }
                else
                {
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br/>Failed to add new coupon. Please try again</div>";
                    Response.Redirect(currentPageName);
                }
            }
            else if(mode == 2)
            {
                if (!coupon.isExactSameDataSet(couponId, couponName, disOperator, decDisValue, dtCouponPeriodFrom, dtCouponPeriosTo, active))
                {
                    intRecordAffected = coupon.updateCouponById(couponId, couponName, disOperator, decDisValue, dtCouponPeriodFrom, dtCouponPeriosTo, active, Convert.ToInt16(Session["ADMID"]));

                    if (intRecordAffected == 1)
                    {
                        boolEdited = true;
                    }
                    else
                    {
                        boolSuccess = false;
                    }
                }

                if (boolEdited)
                {
                    Session["EDITEDCOUPONID"] = coupon.couponId;
                    Response.Redirect(Request.Url.ToString());
                }
                else
                {
                    if (!boolSuccess)
                    {
                        coupon.extractCouponById(couponId, 0);
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br/>Failed to edit coupon (" + coupon.couponName + "). Please try again.</div>";

                        Response.Redirect(Request.Url.ToString());
                    }
                    else
                    {
                        Session["EDITEDCOUPONID"] = couponId;
                        Session["NOCHANGE"] = 1;

                        Response.Redirect(Request.Url.ToString());
                    }
                }
            }
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string strDeletedName = "";

        clsCoupon.performDeleteCoupon(couponId, ref strDeletedName);

        Session["DELETEDCOUPONNAME"] = strDeletedName;

        Response.Redirect(currentPageName);
    }
    #endregion

    #region "Methods"
    public void registerScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("FORMSECT3")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "initSect3();";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "FORMSECT3", strJS, false);
        }
    }

    public void fill()
    {
        registerScript();

        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void setPageProperties()
    {
        switch (mode)
        {
            case 1://Add Coupon
                break;
            case 2://Edit Coupon
                lnkbtnDelete.Visible = true;

                if (Session["DELETEDCOUPONNAME"] == null) { fillForm(); }

                break;
            default:
                break;
        }

        lnkbtnCancel.OnClientClick = "javascript:document.location.href='admCoupon01.aspx'; return false;";
        lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteCoupon.Text").ToString() + "'); return false;";

        clsMis mis = new clsMis();
        DataSet ds = new DataSet();
        ds = mis.getListByListGrp("ORDER UNIT", 0);
        DataView dv = new DataView(ds.Tables[0]);
        dv.Sort = "LIST_ORDER ASC";

        ddlDisOperator.DataSource = dv;
        ddlDisOperator.DataTextField = "LIST_NAME";
        ddlDisOperator.DataValueField = "LIST_VALUE";
        ddlDisOperator.DataBind();
    }

    protected void fillForm()
    {
        clsCoupon coupon = new clsCoupon();

        if (coupon.extractCouponById(couponId, 0))
        {
            txtCouponName.Text = coupon.couponName;
            tdTxtCouponCode.Visible = false;
            tdLitCouponCode.Visible = true;
            litCouponCode.Text = coupon.couponCode;

            if (!string.IsNullOrEmpty(coupon.couponOperator)) { ddlDisOperator.SelectedValue = coupon.couponOperator; }

            txtDisValue.Text = coupon.couponValue > 0 ? coupon.couponValue.ToString() : "";

            DateTime dtPeriodStart = coupon.couponStartDate;

            if (dtPeriodStart.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
            {
                txtCouponPeriodFrom.Text = dtPeriodStart.Day + " " + dtPeriodStart.ToString("MMM") + " " + dtPeriodStart.Year;
            }

            DateTime dtPeriodEnd = coupon.couponEndDate;

            if (dtPeriodEnd.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
            {
                txtCouponPeriodTo.Text = dtPeriodEnd.Day + " " + dtPeriodEnd.ToString("MMM") + " " + dtPeriodEnd.Year;
            }

            chkboxActive.Checked = coupon.couponActive != 0 ? true : false;
        }
    }
    #endregion
}
