﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;
using System.IO;

public partial class ctrl_ucAdmHighlightArticle : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _highId;
    protected int _mode;
    protected string _pageListingURL = "";
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int highId
    {
        get
        {
            if (ViewState["HIGHID"] == null)
            {
                return _highId;
            }
            else
            {
                return Convert.ToInt16(ViewState["HIGHID"]);
            }
        }
        set { ViewState["HIGHID"] = value; }
    }

    protected int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if (ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void oriFileUploadItems_OnDelete(object sender, CommandEventArgs e)
    {
        string strFileUploadItemToDelete = e.CommandArgument.ToString();
        string[] strSplit = strFileUploadItemToDelete.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

        Session["DELFILETITLE"] += strSplit[0] + clsAdmin.CONSTDEFAULTSEPERATOR;
        Session["DELFILEUPLOAD"] += strSplit[1] + clsAdmin.CONSTDEFAULTSEPERATOR;

        populateFileUpload();
    }

    protected void fileUploadItems_OnDelete(object sender, CommandEventArgs e)
    {
        string strFileUploadItemToDelete = e.CommandArgument.ToString();
        string[] strSplit = strFileUploadItemToDelete.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

        Session["FILETITLE"] = Session["FILETITLE"].ToString().Replace(clsAdmin.CONSTDEFAULTSEPERATOR + strSplit[0] + clsAdmin.CONSTDEFAULTSEPERATOR, clsAdmin.CONSTDEFAULTSEPERATOR.ToString());
        Session["FILETITLE_ZH"] = Session["FILETITLE_ZH"].ToString().Replace(clsAdmin.CONSTDEFAULTSEPERATOR + strSplit[1] + clsAdmin.CONSTDEFAULTSEPERATOR, clsAdmin.CONSTDEFAULTSEPERATOR.ToString());
        Session["FILEUPLOAD"] = Session["FILEUPLOAD"].ToString().Replace(strSplit[2] + clsAdmin.CONSTDEFAULTSEPERATOR, "");

        File.Delete(Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMTEMPFOLDER + "/" + strSplit[2]);

        populateFileUpload();
    }

    protected void lnkbtnArticle_Click(object sender, EventArgs e)
    {
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Boolean boolEdited = false;
            Boolean boolSuccess = true;
            int intRecordAffected = 0;

            clsHighlight high = new clsHighlight();

            try
            {
                string strFileTitle = Session["FILETITLE"].ToString();
                string strFileTitle_zh = Session["FILETITLE_ZH"].ToString();
                string strFileUpload = Session["FILEUPLOAD"].ToString();

                if (!string.IsNullOrEmpty(strFileTitle) && !string.IsNullOrEmpty(strFileUpload))
                {
                    strFileTitle = strFileTitle.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strFileTitle.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    strFileTitle_zh = strFileTitle_zh.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strFileTitle_zh.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    strFileUpload = strFileUpload.Substring(0, strFileUpload.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                    string[] fileTitleItems = strFileTitle.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    string[] fileTitleItems_zh = strFileTitle_zh.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    string[] fileUploadItems = strFileUpload.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    string strTempPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMTEMPFOLDER + "/";
                    string strNewPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMHIGHFOLDER + "/" + clsAdmin.CONSTADMHIGHARTFOLDER + "/";

                    for (int i = 0; i <= fileTitleItems.GetUpperBound(0); i++)
                    {
                        intRecordAffected = high.addHighlightArticle(highId, fileTitleItems[i], fileUploadItems[i]);

                        if (intRecordAffected == 1)
                        {
                            File.Move(strTempPath + fileUploadItems[i], strNewPath + fileUploadItems[i]);
                            Session["FILETITLE"] = Session["FILETITLE"].ToString().Replace(fileTitleItems[i] + clsAdmin.CONSTDEFAULTSEPERATOR, "");
                            Session["FILETITLE_ZH"] = Session["FILETITLE_ZH"].ToString().Replace(fileTitleItems_zh[i] + clsAdmin.CONSTDEFAULTSEPERATOR, "");
                            Session["FILEUPLOAD"] = Session["FILEUPLOAD"].ToString().Replace(fileUploadItems[i] + clsAdmin.CONSTDEFAULTSEPERATOR, "");

                            boolEdited = true;
                        }
                        else
                        {
                            boolSuccess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                clsLog.logErroMsg(ex.Message.ToString());
                throw ex;
            }

            try
            {
                string strDelFileTitle = Session["DELFILETITLE"].ToString();
                string strDelFileUpload = Session["DELFILEUPLOAD"].ToString();

                if (!string.IsNullOrEmpty(strDelFileTitle) && !string.IsNullOrEmpty(strDelFileUpload))
                {
                    strDelFileTitle = strDelFileTitle.Substring(0, strDelFileTitle.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    strDelFileUpload = strDelFileUpload.Substring(0, strDelFileUpload.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    string[] delFileTitle = strDelFileTitle.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    string[] delFileUpload = strDelFileUpload.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    string strDelPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMHIGHFOLDER + "/" + clsAdmin.CONSTADMHIGHARTFOLDER + "/";

                    for (int i = 0; i <= delFileTitle.GetUpperBound(0); i++)
                    {
                        intRecordAffected = high.deleteHighlightArticle(highId, delFileTitle[i], delFileUpload[i]);

                        if (intRecordAffected == 1)
                        {
                            File.Delete(strDelPath + delFileUpload[i]);
                            Session["DELFILETITLE"] = Session["DELFILETITLE"].ToString().Replace(delFileTitle[i] + clsAdmin.CONSTDEFAULTSEPERATOR, "");
                            Session["DELFILEUPLOAD"] = Session["DELFILEUPLOAD"].ToString().Replace(delFileUpload[i] + clsAdmin.CONSTDEFAULTSEPERATOR, "");

                            boolEdited = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                clsLog.logErroMsg(ex.Message.ToString());
                throw ex;
            }

            if (boolEdited)
            {
                Session["EDITEDHIGHID"] = highId;

                Response.Redirect(Request.Url.ToString());
            }
            else
            {
                if (!boolSuccess)
                {
                    high.extractHighlightById(highId, 0);
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit highlight (" + high.highTitle + "). Please try again.</div>";

                    Response.Redirect(Request.Url.ToString());
                }
                else
                {
                    Session["EDITEDHIGHID"] = highId;
                    Session["NOCHANGE"] = 1;

                    Response.Redirect(Request.Url.ToString());
                }
            }
        }
    }
    #endregion


    #region "Methods"
    public void fill(int intMode)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;

                if (int.TryParse(Request["id"], out intTryParse))
                {
                    highId = intTryParse;
                }
                else
                {
                    highId = 0;
                }
            }

            mode = intMode;

            if (mode == 2)
            {
                if (Session["DELETEDHIGHNAME"] == null) { fillForm(); }
            }

            lnkbtnCancel.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
        }

        populateFileUpload();
    }

    protected void fillForm()
    {
        clsHighlight high = new clsHighlight();

        Session["ORIFILETITLE"] = "";
        Session["ORIFILEUPLOAD"] = "";

        DataSet ds = high.getHighlightArticlesById(highId);

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            Session["ORIFILETITLE"] += row["HIGHART_TITLE"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR;
            Session["ORIFILEUPLOAD"] += row["HIGHART_FILE"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR;
        }
    }

    protected void populateFileUpload()
    {
        pnlArticle.Controls.Clear();
        litArticle.Visible = false;
        litArticle.Text = string.Empty;
        int intCount = 0;

        try
        {
            string strOriFileTitle = "";
            if (Session["ORIFILETITLE"] != null) { strOriFileTitle = Session["ORIFILETITLE"].ToString(); }

            string strOriFileUpload = "";
            if (Session["ORIFILEUPLOAD"] != null) { strOriFileUpload = Session["ORIFILEUPLOAD"].ToString(); }

            string strFileTitle = "";
            if (Session["FILETITLE"] != null)
            {
                strFileTitle = Session["FILETITLE"].ToString();

                if (!strFileTitle.StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["FILETITLE"] = clsAdmin.CONSTDEFAULTSEPERATOR + Session["FILETITLE"].ToString(); }

                strFileTitle = Session["FILETITLE"].ToString();
            }

            string strFileTitle_zh = "";
            if (Session["FILETITLE_ZH"] != null)
            {
                strFileTitle_zh = Session["FILETITLE_ZH"].ToString();

                if (!strFileTitle_zh.StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["FILETITLE_ZH"] = clsAdmin.CONSTDEFAULTSEPERATOR + Session["FILETITLE_ZH"].ToString(); }

                strFileTitle_zh = Session["FILETITLE_ZH"].ToString();
            }

            string strFileUpload = "";
            if (Session["FILEUPLOAD"] != null) { strFileUpload = Session["FILEUPLOAD"].ToString(); }

            string strDelFileUpload = "";
            if (Session["DELFILEUPLOAD"] != null) { strDelFileUpload = Session["DELFILEUPLOAD"].ToString(); }
        
            if((!string.IsNullOrEmpty(strOriFileTitle) && !string.IsNullOrEmpty(strOriFileUpload)) || (!string.IsNullOrEmpty(strFileTitle) || !string.IsNullOrEmpty(strDelFileUpload)))
            {
                HtmlTable tbl = new HtmlTable();
                tbl.CellPadding = 0;
                tbl.CellSpacing = 0;
                tbl.Attributes.Add("class", "dataTbl");
                pnlArticle.Controls.Add(tbl);

                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cell1 = new HtmlTableCell("th");
                HtmlTableCell cell2 = new HtmlTableCell("th");
                HtmlTableCell cell3 = new HtmlTableCell("th");
                //HtmlGenericControl spanSplitter;

                // for table header
                cell1.Controls.Add(new LiteralControl("No."));
                cell2.Controls.Add(new LiteralControl("Article"));
                cell3.Controls.Add(new LiteralControl("Action"));

                cell1.Attributes["class"] = "tdNo";
                cell2.Attributes["style"] = "text-align:center;";
                cell3.Attributes["style"] = "text-align:center;";

                row.Controls.Add(cell1);
                row.Controls.Add(cell2);
                row.Controls.Add(cell3);
                tbl.Controls.Add(row);

                if (!string.IsNullOrEmpty(strOriFileTitle) && !string.IsNullOrEmpty(strOriFileUpload))
                {
                    strOriFileTitle = strOriFileTitle.Substring(0, strOriFileTitle.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    strOriFileUpload = strOriFileUpload.Substring(0, strOriFileUpload.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                    LinkButton lnkbtn;
                    string[] fileTitleItems = strOriFileTitle.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    string[] fileUploadItems = strOriFileUpload.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                    for (int i = 0; i <= fileTitleItems.GetUpperBound(0); i++)
                    {
                        if (strDelFileUpload.IndexOf(fileUploadItems[i]) < 0)
                        {
                            intCount += 1;
                            var span = new HtmlGenericControl("span");
                            span.InnerHtml = (intCount).ToString();
                            cell1 = new HtmlTableCell();
                            cell1.Controls.Add(span);

                            lnkbtn = new LinkButton();
                            lnkbtn.CssClass = "lnkbtn lnkbtnEdit";
                            lnkbtn.ID = fileUploadItems[i] + "_" + i;
                            lnkbtn.Text = fileTitleItems[i];
                            lnkbtn.OnClientClick = "javascript:window.open('" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMHIGHFOLDER + "/" + clsAdmin.CONSTADMHIGHARTFOLDER + "/" + fileUploadItems[i] + "', 'art', 'toolbar=0, menubar=0, height=800px, width=800px, resizable=1'); return false;";
                            cell2 = new HtmlTableCell();
                            cell2.Attributes["style"] = "text-align: center;width:45%;";
                            cell2.Controls.Add(lnkbtn);

                            //spanSplitter = new HtmlGenericControl("span");
                            //spanSplitter.Attributes["class"] = "spanSplitter";
                            //spanSplitter.InnerHtml = " | ";
                            //cell2.Controls.Add(spanSplitter);

                            lnkbtn = new LinkButton();
                            lnkbtn.CssClass = "lnkbtn lnkbtnDelete";
                            lnkbtn.ID = fileUploadItems[i] + "_delete_" + i;
                            lnkbtn.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
                            lnkbtn.Command += new CommandEventHandler(oriFileUploadItems_OnDelete);
                            lnkbtn.CommandArgument = fileTitleItems[i] + clsAdmin.CONSTDEFAULTSEPERATOR + fileUploadItems[i];
                            cell3 = new HtmlTableCell();
                            cell3.Attributes["class"] = "tdAct";
                            cell3.Controls.Add(lnkbtn);

                            row = new HtmlTableRow();
                            row.Controls.Add(cell1);
                            row.Controls.Add(cell2);
                            row.Controls.Add(cell3);
                            tbl.Controls.Add(row);
                        }
                    }
                }

                if (strFileTitle != "" && strFileUpload != "")
                {
                    strFileTitle = strFileTitle.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strFileTitle.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    strFileTitle_zh = strFileTitle_zh.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strFileTitle_zh.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    strFileUpload = strFileUpload.Substring(0, strFileUpload.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                    LinkButton lnkbtn;
                    string[] fileTitleItems = strFileTitle.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    string[] fileTitleItems_zh = strFileTitle_zh.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    string[] fileUploadItems = strFileUpload.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                    for (int i = 0; i <= fileTitleItems.GetUpperBound(0); i++)
                    {
                        intCount += 1;

                        var span = new HtmlGenericControl("span");
                        span.InnerHtml = (intCount).ToString();
                        cell1 = new HtmlTableCell();
                        cell1.Controls.Add(span);

                        lnkbtn = new LinkButton();
                        lnkbtn.CssClass = "lnkbtn lnkbtnEdit";
                        lnkbtn.ID = fileUploadItems[i] + "_" + i;
                        lnkbtn.Text = fileTitleItems[i];
                        lnkbtn.OnClientClick = "javascript:window.open('" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMTEMPFOLDER + "/" + fileUploadItems[i] + "', 'art', 'toolbar=0, menubar=0, height=800px, width=800px, resizable=1'); return false;";
                        cell2 = new HtmlTableCell();
                        cell2.Attributes["style"] = "text-align: center;width:45%;";
                        cell2.Controls.Add(lnkbtn);

                        //spanSplitter = new HtmlGenericControl("span");
                        //spanSplitter.Attributes["class"] = "spanSplitter";
                        //spanSplitter.InnerHtml = " | ";
                        //cell2.Controls.Add(spanSplitter);

                        lnkbtn = new LinkButton();
                        lnkbtn.CssClass = "lnkbtn lnkbtnDelete";
                        lnkbtn.ID = fileUploadItems[i] + "_delete_" + i;
                        lnkbtn.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
                        lnkbtn.Command += new CommandEventHandler(fileUploadItems_OnDelete);
                        lnkbtn.CommandArgument = fileTitleItems[i] + clsAdmin.CONSTDEFAULTSEPERATOR + fileTitleItems_zh[i] + clsAdmin.CONSTDEFAULTSEPERATOR + fileUploadItems[i];
                        cell3 = new HtmlTableCell();
                        cell3.Attributes["class"] = "tdAct";
                        cell3.Controls.Add(lnkbtn);

                        row = new HtmlTableRow();
                        row.Controls.Add(cell1);
                        row.Controls.Add(cell2);
                        row.Controls.Add(cell3);
                        tbl.Controls.Add(row);
                    }
                }
            }

            pnlArticle.Style.Add("  padding-top", "10px;");
            litArticle.Visible = true;
            litArticle.Text = intCount + " uploaded article(s).";
            if (intCount > 0) { pnlArticle.Visible = true; }
            else { pnlArticle.Visible = false; }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message.ToString());
            throw ex;
        }
    }
    #endregion
}
