﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;
using System.Collections;

public partial class ctrl_ucAdmRelatedDeal : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected int _dealId = 0;
    protected DataTable _dtDealSel = null;
    protected string _selectedId = null;
    protected string _deletedId = null;
    protected string _pageListingURL = "";

    protected Boolean _boolRowClose = false;
    protected Boolean _boolSelRowClose = false;

    protected int intDealCount;
    protected int intDealCountInRow = 5;
    protected int intDealTotal;
    protected int intDealPageSize;

    protected int intDealCountSel;
    protected int intDealSelCountInRow = 5;
    protected int intDealSelTotal;
    protected int intDealSelPageSize;

    protected int _currentPage = 0;
    protected int _pageNo = 1;
    protected int _pageCount = 0;

    protected string strKeyword;
    protected Boolean _boolSearch;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int dealId
    {
        get
        {
            if (ViewState["DEALID"] == null)
            {
                return _dealId;
            }
            else
            {
                return int.Parse(ViewState["DEALID"].ToString());
            }
        }
        set { ViewState["DEALID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public DataTable dtDealSel
    {
        get
        {
            if (ViewState["DTDEALSEL"] == null)
            {
                return _dtDealSel;
            }
            else
            {
                return (DataTable)ViewState["DTDEALSEL"];
            }
        }
        set { ViewState["DTDEALSEL"] = value; }
    }

    public string selectedId
    {
        get
        {
            if (ViewState["SELECTEDID"] == null)
            {
                return _selectedId;
            }
            else
            {
                return ViewState["SELECTEDID"].ToString();
            }
        }
        set { ViewState["SELECTEDID"] = value; }
    }

    public string deletedId
    {
        get
        {
            if (ViewState["DELETEDID"] == null)
            {
                return _deletedId;
            }
            else
            {
                return ViewState["DELETEDID"].ToString();
            }
        }
        set { ViewState["DELETEDID"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if (ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }

    public Boolean boolSearch
    {
        get
        {
            if (ViewState["BOOLSEARCH"] == null)
            {
                return _boolSearch;
            }
            else
            {
                return (Boolean)ViewState["BOOLSEARCH"];
            }
        }
        set { ViewState["BOOLSEARCH"] = value; }
    }

    protected Boolean boolRowClose
    {
        get
        {
            if (ViewState["BOOLROWCLOSE"] == null)
            {
                return _boolRowClose;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLROWCLOSE"]);
            }
        }
        set { ViewState["BOOLROWCLOSE"] = value; }
    }

    protected Boolean boolSelRowClose
    {
        get
        {
            if (ViewState["BOOLSELROWCLOSE"] == null)
            {
                return _boolSelRowClose;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLSELROWCLOSE"]);
            }
        }
        set { ViewState["BOOLSELROWCLOSE"] = value; }
    }

    protected int currentPage
    {
        get
        {
            if (ViewState["CURRENTPAGE"] == null)
            {
                return _currentPage;
            }
            else
            {
                return Convert.ToInt16(ViewState["CURRENTPAGE"]);
            }
        }
        set { ViewState["CURRENTPAGE"] = value; }
    }

    protected int pageNo
    {
        get
        {
            if (ViewState["PAGENO"] == null)
            {
                return _pageNo;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGENO"]);
            }
        }
        set { ViewState["PAGENO"] = value; }
    }

    protected int pageCount
    {
        get
        {
            if (ViewState["PAGECOUNT"] == null)
            {
                return _pageCount;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGECOUNT"]);
            }
        }
        set { ViewState["PAGECOUNT"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        hypHideShow.Attributes["onclick"] = "hideshow('" + pnlSeachForm.ClientID + "','" + hypHideShow.ClientID + "');";

        txtKeyword.Attributes.Add("onkeydown", "javascript:if(event.keyCode == 13){document.getElementById('" + lnkbtnSearch.ClientID + "').click(); return false;}");
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        boolSearch = true;

        bindRptDealData();
    }

    protected void rptPagination_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intPage = Convert.ToInt16(e.Item.DataItem);

            LinkButton lnkbtnPage = (LinkButton)e.Item.FindControl("lnkbtnPage");

            if (intPage == pageNo)
            {
                lnkbtnPage.CssClass = "linkPageSel";
                lnkbtnPage.Enabled = false;
            }
        }
    }

    protected void rptPagination_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdPage")
        {
            pageNo = Convert.ToInt16(e.CommandArgument);

            bindRptDealData();
        }
    }

    protected void rptRelDeal_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strDealId = DataBinder.Eval(e.Item.DataItem, "DEAL_ID").ToString();
            string strDealName = DataBinder.Eval(e.Item.DataItem, "DEAL_NAME").ToString();
            string strDealImage = DataBinder.Eval(e.Item.DataItem, "DEAL_IMAGE").ToString();

            LinkButton lnkbtnRelDealImg = (LinkButton)e.Item.FindControl("lnkbtnRelDealImg");
            Image imgRelDeal = (Image)e.Item.FindControl("imgRelDeal");

            if (string.IsNullOrEmpty(strDealImage))
            {
                clsConfig config = new clsConfig();
                imgRelDeal.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + "/" + config.defaultGroupImg + "&w=100&h=100&f=1";
            }
            else
            {
                imgRelDeal.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMDEALFOLDER + "/" + strDealImage + "&w=100&h=100&f=1";
            }

            lnkbtnRelDealImg.ToolTip = strDealName;
            imgRelDeal.AlternateText = strDealName;

            Literal litRowOpen = (Literal)e.Item.FindControl("litRowOpen");
            Literal litRowClose = (Literal)e.Item.FindControl("litRowClose");

            intDealCount += 1;
            if (intDealCount % intDealCountInRow == 1)
            {
                litRowOpen.Text = "<tr>";
                litRowOpen.Visible = true;
                boolRowClose = false;
            }
            else if (intDealCount % intDealCountInRow == 0)
            {
                Panel pnlRelDealItem = (Panel)e.Item.FindControl("pnlRelDealItem");
                pnlRelDealItem.CssClass = "divRelDealItemLast";

                litRowClose.Text = "</tr>";
                litRowClose.Visible = true;
                boolRowClose = true;
            }

            if (intDealCount == intDealTotal || intDealCount == intDealPageSize)
            {
                if (!boolRowClose)
                {
                    int intRemainder = intDealCount % intDealCountInRow;

                    if (intRemainder > 0)
                    {
                        string strIds = "";

                        for (int i = 0; i < (intDealCountInRow = intRemainder); i++)
                        {
                            strIds += "<td></td>";
                        }

                        litRowClose.Text = strIds + "</tr>";
                        litRowClose.Visible = true;
                        boolRowClose = true;
                    }
                }
            }
        }
        else if(e.Item.ItemType == ListItemType.Footer)
        {
            if (!boolRowClose)
            {
                int intRemainder = intDealCount % intDealCountInRow;

                if (intRemainder > 0)
                {
                    string strIds = "";

                    for (int i = 0; i < (intDealCountInRow - intRemainder); i++)
                    {
                        strIds += "<td></td>";
                    }

                    Literal litRowClose2 = (Literal)e.Item.FindControl("litRowClose2");

                    litRowClose2.Text = strIds + "</tr>";
                    litRowClose2.Visible = true;
                    boolRowClose = true;
                }
            }
        }
    }

    protected void rptRelDeal_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdSelect")
        {
            string strArg = e.CommandArgument.ToString();
            string[] strArgSplit = strArg.Split((Char)'|');

            int intSelectedId = int.Parse(strArgSplit[0]);
            string strSelProdName = strArgSplit[1].ToString();
            string strSelProdImg = strArgSplit[2].ToString();

            updateDeletedList(intSelectedId);
            addSelection(intSelectedId, strSelProdName, strSelProdImg);

            bindRptDealData();
            bindRptDealSelData();
        }
    }

    protected void rptRelDealSel_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strDealId = DataBinder.Eval(e.Item.DataItem, "RELDEAL_ID").ToString();
            string strDealName = DataBinder.Eval(e.Item.DataItem, "RELDEAL_NAME").ToString();
            string strDealImage = DataBinder.Eval(e.Item.DataItem, "RELDEAL_IMAGE").ToString();

            LinkButton lnkbtnRelDealSelImg = (LinkButton)e.Item.FindControl("lnkbtnRelDealSelImg");
            Image imgRelImageSel = (Image)e.Item.FindControl("imgRelImageSel");

            if (string.IsNullOrEmpty(strDealImage))
            {
                clsConfig config = new clsConfig();
                imgRelImageSel.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + "/" + config.defaultGroupImg + "&w=100&h=100&f=1";
            }
            else
            {
                imgRelImageSel.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMDEALFOLDER + "/" + strDealImage + "&w=100&h=100&f=1";
            }

            lnkbtnRelDealSelImg.ToolTip = strDealName;
            imgRelImageSel.AlternateText = strDealName;

            intDealCountSel += 1;
            Literal litRowOpen = (Literal)e.Item.FindControl("litRowOpen");
            Literal litRowClose = (Literal)e.Item.FindControl("litRowClose");

            if (intDealCountSel % intDealSelCountInRow == 1)
            {
                litRowOpen.Text = "<tr>";
                litRowOpen.Visible = true;
                boolRowClose = false;
            }
            else if (intDealCountSel % intDealSelCountInRow == 0)
            {
                Panel pnlDealSelContainer = (Panel)e.Item.FindControl("pnlDealSelContainer");
                pnlDealSelContainer.CssClass = "divRelDealItemLast";

                HtmlTableCell tdRelDealItem = (HtmlTableCell)e.Item.FindControl("tdRelDealItem");
                tdRelDealItem.Attributes["class"] = "tdRelDealItemLast";

                litRowClose.Text = "</tr>";
                litRowClose.Visible = true;
                boolRowClose = true;
            }

            if (intDealCountSel == intDealSelTotal || intDealCountSel == intDealSelPageSize)
            {
                if (!boolSelRowClose)
                {
                    int intRemainder = intDealCountSel % intDealSelCountInRow;

                    if (intRemainder > 0)
                    {
                        string strTds = "";

                        for (int i = 0; i < (intDealSelCountInRow - intRemainder); i++)
                        {
                            strTds += "<td></td>";
                        }

                        litRowClose.Text = strTds + "</tr>";
                        litRowClose.Visible = true;
                        boolSelRowClose = true;
                    }
                }
            }
        }
        else if (e.Item.ItemType == ListItemType.Footer)
        {
            if (!boolSelRowClose)
            {
                int intRemainder = intDealCountSel % intDealSelCountInRow;

                if (intRemainder > 0)
                {
                    string strTds = "";

                    for (int i = 0; i < (intDealSelCountInRow - intRemainder); i++)
                    {
                        strTds += "<td></td>";
                    }

                    Literal litRowClose2 = (Literal)e.Item.FindControl("litRowClose2");

                    litRowClose2.Text = strTds + "</tr>";
                    litRowClose2.Visible = true;
                    boolSelRowClose = true;
                }
            }
        }
    }

    protected void rptRelDealSel_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdRemove")
        {
            if (selectedId != null && selectedId != "")
            {
                int intSelectedId = int.Parse(e.CommandArgument.ToString());
                string strSelectedId = selectedId.Substring(0, selectedId.Length - 1);
                string[] strSelectedSplit = strSelectedId.Split((char)',');
                string strNewSelected = "";

                try
                {
                    for (int i = 0; i < strSelectedSplit.Length; i++)
                    {
                        if (int.Parse(strSelectedSplit[i]) != intSelectedId)
                        {
                            strNewSelected += strSelectedSplit[i] + clsAdmin.CONSTCOMMASEPERATORSTRING;
                        }
                    }
                }
                catch (Exception ex) { }

                selectedId = null;
                selectedId = strNewSelected;
                deletedId += intSelectedId + clsAdmin.CONSTCOMMASEPERATORSTRING;

                DataTable dt = new DataTable();
                dt = dtDealSel;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (int.Parse(dt.Rows[i]["RELDEAL_ID"].ToString()) == intSelectedId)
                    {
                        dt.Rows[i].Delete();
                    }
                }

                dt.AcceptChanges();
                dtDealSel = dt;

                if (dt.Rows.Count <= clsAdmin.CONSTMAXDEALRELATED) { pnlAck.Visible = false; }

                bindRptDealData();
                bindRptDealSelData();
            }
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Boolean boolEdited = false;
            Boolean boolSuccess = true;

            clsDeal deal = new clsDeal();
            int intDealId;
            int intRecordAffected;

            if (deletedId != null && deletedId != "")
            {
                string strDeletedId = deletedId.ToString().Substring(0, deletedId.ToString().Length - 1);
                string[] strDeletedIdSplit = strDeletedId.Split((char)',');

                for (int i = 0; i < strDeletedIdSplit.Length; i++)
                {
                    intDealId = int.Parse(strDeletedIdSplit[i]);

                    intRecordAffected = deal.deletedRelatedDeal(dealId, intDealId);

                    if (intRecordAffected == 1) { boolEdited = true; }
                }
            }

            if (selectedId != null && selectedId != "")
            {
                string strSelectedId = selectedId.ToString().Substring(0, selectedId.ToString().Length - 1);
                string[] strSelectedIdSplit = strSelectedId.Split((char)',');

                for (int i = 0; i < strSelectedIdSplit.Length; i++)
                {
                    intDealId = int.Parse(strSelectedIdSplit[i]);

                    if (!deal.isExactSameRelDealSet(dealId, intDealId))
                    {
                        intRecordAffected = deal.addReletedDeal(dealId, intDealId);

                        if (intRecordAffected == 1) { boolEdited = true; }
                    }
                }
            }

            if (boolEdited)
            {
                Session["EDITDEALID"] = dealId;
                Response.Redirect(Request.Url.ToString());
            }
            else
            {
                Session["EDITDEALID"] = dealId;
                Session["NOCHANGE"] = 1;
                Response.Redirect(Request.Url.ToString());
            }
        }
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            clsDeal deal = new clsDeal();
            if (deal.extractDealById(dealId, 0))
            {
                setPageProperties();

                if (mode == 2)
                {
                    fillForm();
                }

                bindRptDealData();
                bindRptDealSelData();
            }
            else
            {
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
                Response.Redirect(currentPageName);
            }
        }

        lnkbtnCancel.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
    }

    protected void setPageProperties()
    {
        
    }

    protected void fillForm()
    {
        dtDealSel = null;
        selectedId = null;
        deletedId = null;

        clsDeal deal = new clsDeal();
        DataSet ds = deal.getRelatedDealListById(dealId, 0);
        dtDealSel = ds.Tables[0];

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            selectedId += row["RELDEAL_ID"] + clsAdmin.CONSTCOMMASEPERATORSTRING;
        }
    }

    protected void bindRptDealData()
    {
        clsDeal deal = new clsDeal();
        DataSet dsDeal = deal.getDealList(1);
        DataView dvDeal = new DataView(dsDeal.Tables[0]);

        string strSelectedId = dealId.ToString();

        if (selectedId != null && selectedId != "")
        {
            strSelectedId += clsAdmin.CONSTCOMMASEPERATORSTRING + selectedId.ToString().Substring(0, selectedId.ToString().Length - 1);
        }

        dvDeal.RowFilter = "DEAL_ID NOT IN (" + strSelectedId + ")";

        if (boolSearch)
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                strKeyword = txtKeyword.Text.Trim();
                dvDeal.RowFilter += " AND (DEAL_CODE LIKE '%" + strKeyword + "%' OR DEAL_NAME LIKE '%" + strKeyword + "%')";

                pnlSeachForm.CssClass = "divSearchFormShow";
                hypHideShow.CssClass = "linkHide";
            }
            else
            {
                pnlSeachForm.CssClass = "divSearchForm";
                hypHideShow.CssClass = "linkShow";
            }
        }

        intDealCount = 0;
        intDealPageSize = clsAdmin.CONSTADMPAGESIZE;
        intDealTotal = dvDeal.Count;

        if (dvDeal.Count > 0)
        {
            PagedDataSource pdsDeal;
            pdsDeal = new PagedDataSource();
            pdsDeal.DataSource = dvDeal;
            pdsDeal.AllowPaging = true;
            pdsDeal.PageSize = clsAdmin.CONSTADMPAGESIZE;
            pageCount = pdsDeal.PageCount;

            currentPage = pageNo > 0 ? (pageNo > pageCount ? pageCount - 1 : pageNo - 1) : 0;

            pdsDeal.CurrentPageIndex = currentPage;

            rptRelDeal.DataSource = pdsDeal;
            rptRelDeal.DataBind();

            pnlDealRelNoFound.Visible = false;
            pnlRelDeal.Visible = true;

            int intCurrentPage = currentPage + 1;
            int intStart;
            int intEnd;
            int intPaginationSize = 9;
            int intPaginationSizeMid = Convert.ToInt16(intPaginationSize / 2);

            if (intCurrentPage <= intPaginationSizeMid)
            {
                intStart = 1;
                if (pageCount > intPaginationSize) { intEnd = intPaginationSize; }
                else { intEnd = pageCount; }
            }
            else if (intCurrentPage >= pageCount - intPaginationSizeMid)
            {
                intStart = pageCount - intPaginationSize + 1;
                if (intStart < 1) { intStart = 1; }
                intEnd = pageCount;
            }
            else
            {
                intStart = intCurrentPage - intPaginationSizeMid;
                intEnd = intCurrentPage + intPaginationSizeMid;
            }

            ArrayList pages = new ArrayList();
            for (int i = intStart; i <= intEnd; i++)
            {
                pages.Add(i);
            }

            rptPaginationTop.DataSource = pages;
            rptPaginationTop.DataBind();

            rptPaginationBtm.DataSource = pages;
            rptPaginationBtm.DataBind();
        }
        else
        {
            pnlDealRelNoFound.Visible = true;
            pnlRelDeal.Visible = false;
        }
    }

    protected void bindRptDealSelData()
    {
        if (dtDealSel != null)
        {
            DataTable dt = dtDealSel;
            DataView dvDealSel = new DataView(dt);

            intDealCountSel = 0;
            intDealSelPageSize = clsAdmin.CONSTMAXDEALRELATED;
            intDealSelTotal = dvDealSel.Count;

            if (dvDealSel.Count > 0)
            {
                PagedDataSource pdsDealSel;
                pdsDealSel = new PagedDataSource();
                pdsDealSel.DataSource = dvDealSel;
                pdsDealSel.AllowPaging = false;
                pdsDealSel.PageSize = clsAdmin.CONSTMAXDEALRELATED;
                rptRelDealSel.DataSource = pdsDealSel;
                rptRelDealSel.DataBind();

                pnlDealSelListing.Visible = true;
                pnlDealSelNoFound.Visible = false;
            }
            else
            {
                pnlDealSelListing.Visible = false;
                pnlDealSelNoFound.Visible = true;
            }
        }
    }

    protected DataTable createDataTable()
    {
        DataTable dtSel = new DataTable();
        DataColumn col;

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.Int16");
        col.ColumnName = "RELDEAL_ID";
        dtSel.Columns.Add(col);

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.String");
        col.ColumnName = "RELDEAL_NAME";
        dtSel.Columns.Add(col);

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.String");
        col.ColumnName = "RELDEAL_IMAGE";
        dtSel.Columns.Add(col);

        return dtSel;
    }

    protected void addSelection(int intSelectedId, string strSelDealName, string strSelDealImg)
    {
        DataTable dt = new DataTable();

        if (dtDealSel != null)
        {
            dt = dtDealSel;
        }
        else
        {
            dt = createDataTable();
        }

        int intSelectedCount = 0;

        if (selectedId != null && selectedId != "")
        {
            string strSelectedId = selectedId;
            string[] strSelectedIdSplit = strSelectedId.Split((char)clsAdmin.CONSTCOMMASEPARATOR);
            intSelectedCount = strSelectedIdSplit.Length;
        }

        if (intSelectedCount <= clsAdmin.CONSTMAXDEALRELATED)
        {
            DataRow[] foundRow = dt.Select("RELDEAL_ID = '" + intSelectedId + "'");

            if (foundRow.Length <= 0)
            {
                DataRow row;
                row = dt.NewRow();
                row["RELDEAL_ID"] = intSelectedId;
                row["RELDEAL_NAME"] = strSelDealName;
                row["RELDEAL_IMAGE"] = strSelDealImg;
                dt.Rows.Add(row);
                selectedId += intSelectedId + clsAdmin.CONSTCOMMASEPERATORSTRING;
            }

            dtDealSel = dt;
        }
        else
        {
            pnlAck.Visible = true;
            litAck.Text = "<div class=\"errmsg\">Maximum " + clsAdmin.CONSTMAXDEALRELATED + " related deals.</div>";
        }
    }

    protected void updateDeletedList(int intRelDealId)
    {
        if (deletedId != null && deletedId != "")
        {
            string strDeletedId = deletedId;
            string[] strDeletedIdSplit = strDeletedId.Split((char)clsAdmin.CONSTCOMMASEPARATOR);

            strDeletedId = "";

            for (int i = 0; i < strDeletedIdSplit.Length - 1; i++)
            {
                if (int.Parse(strDeletedIdSplit[i]) != intRelDealId)
                {
                    strDeletedId += strDeletedIdSplit[i] + clsAdmin.CONSTCOMMASEPERATORSTRING;
                }
            }

            deletedId = strDeletedId;
        }
    }
    #endregion
}
