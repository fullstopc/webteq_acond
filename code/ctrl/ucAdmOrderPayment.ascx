﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmOrderPayment.ascx.cs" Inherits="ctrl_ucAdmOrderPayment" %>

<asp:Panel ID="pnlPaymentDetails" runat="server" CssClass="divPaymentDetails">
    <div class="divPaymentHeader">Payment</div>
    <table cellpadding="0" cellspacing="0" class="tblPaymentDetails">
        <thead>
        <tr>
            <td colspan="2"><span class="">Payment Details</span></td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="tdLabelNor">Payment Method:</td>
            <td class="tdItem"><asp:Literal ID="litMethod" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabelNor">Transaction ID:</td>
            <td class="tdItem"><asp:Literal ID="litPaymentRemarks" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabelNor">Payment Amount:</td>
            <td class="tdItem"><asp:Literal ID="litPaymentAmount" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabelNor">Payment Date:</td>
            <td class="tdItem"><asp:Literal ID="litPaymentDate" runat="server"></asp:Literal></td>
        </tr>
        <tr id="trBank" runat="server" visible="false">
            <td class="tdLabelNor">Bank:</td>
            <td class="tdItem"><asp:Literal ID="litBank" runat="server"></asp:Literal></td>
        </tr>
        <tr id="trAccount" runat="server" visible="false">
            <td class="tdLabelNor">Bank Account No.:</td>
            <td class="tdItem"><asp:Literal ID="litAccount" runat="server"></asp:Literal></td>
        </tr>
        <tr id="trTransId" runat="server" visible="false">
            <td class="tdLabelNor">Trans. ID:</td>
            <td class="tdItem"><asp:Literal ID="litTransId" runat="server"></asp:Literal></td>
        </tr>
        <tr id="trPayDate" runat="server" visible="false">
            <td class="tdLabelNor">Payment Date:</td>
            <td class="tdItem"><asp:Literal ID="litPayDate" runat="server"></asp:Literal></td>
        </tr>
        </tbody>
    </table>
</asp:Panel>
