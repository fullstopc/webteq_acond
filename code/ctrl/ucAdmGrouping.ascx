﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmGrouping.ascx.cs" Inherits="ctrl_ucAdmGrouping" %>
<script type="text/javascript">

</script>

<asp:Panel ID="pnlAdmCMSControlPanel" runat="server" CssClass="divAdmCMSControlPanel">
    <asp:Panel ID="pnlFormDetails" runat="server" CssClass="">
        <div id="divBankTransfer">
            <table class="tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Group Menu Setting</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel nobr">Group Menu Name:</td>
                        <td>
                            <asp:TextBox ID="txtGroupingName" runat="server" CssClass="text" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel nobr">Active:</td>
                        <td><asp:CheckBox ID="chkboxActive" runat="server" /></td>
                    </tr>
                    <tr><td height="10"></td><td></td></tr>
                    <tr>
                        <td></td>
                        <td><asp:LinkButton ID="lnkbtnSaveArea" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSaveGrouping_Click"></asp:LinkButton></td>
                    </tr>
                    <tr><td height="50"></td><td></td></tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2">
                            <asp:Repeater ID="rptGrouping" runat="server" OnItemDataBound="rptGrouping_ItemDataBound" OnItemCommand="rptGrouping_ItemCommand">
                            <HeaderTemplate>
                                <table cellpadding="0" cellspacing="0" border="0" class="dataTbl">
                                    <colgroup>
                                        <col width="40px" />
                                    </colgroup>
                                    <thead>
                                        <th>No.</th>
                                        <th>Grouping Name</th>
                                        <th class="td_medium" style="text-align:left;">Active</th>
                                        <th style="text-align:center;">Action</th>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><asp:Literal ID="litNo" runat="server"></asp:Literal></td>
                                    <td><asp:Literal ID="litItem" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LIST_NAME") %>'></asp:Literal></td>
                                    <td><asp:Literal ID="litActive" runat="server" Text='<%#clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "LIST_ACTIVE"))) %>'></asp:Literal></td>
                                    <td class="tdAct2">
                                        <asp:LinkButton ID="lnkbtnUp" runat="server" CssClass="lnkbtn lnkbtnUp" CommandName="cmdUp" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "LIST_ID") + "|" + DataBinder.Eval(Container.DataItem, "LIST_ORDER") %>'></asp:LinkButton>   
                                        <span class="spanSplitter">|</span>
                                        <asp:LinkButton ID="lnkbtnDown" runat="server" CssClass="lnkbtn lnkbtnDown" CommandName="cmdDown" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "LIST_ID") + "|" + DataBinder.Eval(Container.DataItem, "LIST_ORDER") %>'></asp:LinkButton>
                                        <span class="spanSplitter">|</span>
                                        <asp:HyperLink ID="hypEdit" runat="server" CssClass="lnkbtn lnkbtnEdit"></asp:HyperLink> 
                                        <span class="spanSplitter">|</span>
                                        <asp:LinkButton ID="lnkbtnDelete" runat="server" CssClass="lnkbtn lnkbtnDelete" CommandName="cmdDelete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "LIST_ID") %>'></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate></table></FooterTemplate>
                        </asp:Repeater>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="" runat="server"  visible="false">
            <table id="tblAction" class="formTbl tblData" cellpadding="0" cellspacing="0" style="padding-top: 0;">
                <tr>
                    <td class="tdLabel"></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated" style="padding-top:0px;">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    
</asp:Panel>
