﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmSmsSetup.ascx.cs" Inherits="ctrl_ucAdmSmsSetup" %>

<asp:Panel ID="pnlAdmCMSControlPanel" runat="server" CssClass="divAdmCMSControlPanel">
    <asp:Panel ID="pnlFormDetails" runat="server" CssClass="divEmailSetupContainer">
        <div>
            <table class="formTbl" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">SMS Settings</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel">
                            <asp:Label ID="lblSenderUrl" runat="server"></asp:Label>:</td>
                        <td>
                            <asp:TextBox ID="txtSenderUrl" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">
                            <asp:Label ID="lblSenderAPIKey" runat="server"></asp:Label>:</td>
                        <td>
                            <asp:TextBox ID="txtSenderAPIKey" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">
                            <asp:Label ID="lblUserName" runat="server"></asp:Label>:</td>
                        <td>
                            <asp:TextBox ID="txtUserName" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel nobr">
                            <asp:Label ID="lblSenderPwd" runat="server"></asp:Label>:</td>
                        <td>
                            <asp:TextBox ID="txtSenderPwd" runat="server" CssClass="text_fullwidth" MaxLength="250" TextMode="Password"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">
                            <asp:Label ID="lblUserId" runat="server"></asp:Label>:</td>
                        <td>
                            <asp:TextBox ID="txtUserId" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">
                            <asp:Label ID="lblSMSDefaultRecipient" runat="server"></asp:Label>:</td>
                        <td>
                            <asp:TextBox ID="txtSMSDefaultRecipient" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table class="formTbl" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">SMS Content</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="2" class="tdLabel">1 SMS supports 160 characters. Extra SMS credit will be used for SMS with more than 160 characters.</td>
                    </tr>
                    <tr>
                        <td class="tdLabel">
                            <asp:Label ID="lblEnquirySms" runat="server"></asp:Label>:</td>
                        <td>
                            <asp:TextBox ID="txtEnquirySms" runat="server" TextMode="MultiLine" Rows="8" CssClass="text_fullwidth"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">
                            <asp:Label ID="lblSendEnquirySms" runat="server"></asp:Label>:</td>
                        <td class="tdLabel">
                            <asp:CheckBox ID="chkboxSendEnquirySms" runat="server"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel">
                            <asp:Label ID="lblNewOrderSms" runat="server"></asp:Label>:</td>
                        <td>
                            <asp:TextBox ID="txtNewOrderSms" runat="server" TextMode="MultiLine" Rows="8" CssClass="text_fullwidth"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">
                            <asp:Label ID="lblSendNewOrderSms" runat="server"></asp:Label>:</td>
                        <td class="tdLabel">
                            <asp:CheckBox ID="chkboxSendNewOrderSms" runat="server"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdLabel">
                            <asp:Label ID="lblPaymentSms" runat="server"></asp:Label>:</td>
                        <td>
                            <asp:TextBox ID="txtPaymentSms" runat="server" TextMode="MultiLine" Rows="8" CssClass="text_fullwidth"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">
                            <asp:Label ID="lblSendPaymentSms" runat="server"></asp:Label>:</td>
                        <td class="tdLabel">
                            <asp:CheckBox ID="chkboxSendPaymentSms" runat="server"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                </tbody>
            </table>

            <table class="formTbl" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>

            </table>
            <div>
                <table class="infoBox" cellpadding="0" cellspacing="0" style="width: 97%;">
                    <colgroup>
                        <col width="33%" />
                        <col width="33%" />
                        <%--<col width="33%" />--%>
                    </colgroup>
                    <thead>
                        <tr>
                            <th colspan="2">Annotation
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2">Some useful syntax to use in SMS content. 
                            </th>
                        </tr>
                    </thead>
                    <tr>
                        <td>
                            <table cellpadding="10" class="tblEmailSyntax">
                                <colgroup>
                                    <col width="30%" />
                                    <col width="70%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td class="th">[EnquiryLink]</td>
                                        <td>Enquiry Email Content URL</td>
                                    </tr>
                                    <tr>
                                        <td class="th">[ORDNO]</td>
                                        <td>Order Number</td>
                                    </tr>
                                  
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table cellpadding="10" class="tblEmailSyntax">
                                <colgroup>
                                    <col width="30%" />
                                    <col width="70%" />
                                </colgroup>
                                <tbody>
                                     <tr>
                                        <td class="th">[PAYMENTTYPE]</td>
                                        <td>Type of Payment</td>
                                    </tr>
                                    <tr>
                                        <td class="th">[WEBSITENAME]</td>
                                        <td>Website Name</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
    </asp:Panel>

</asp:Panel>
