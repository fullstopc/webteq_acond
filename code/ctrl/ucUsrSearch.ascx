<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrSearch.ascx.cs" Inherits="ctrl_ucUsrSearch" %>

<script type="text/javascript">

</script>

<asp:Panel ID="pnlSearch" runat="server">
    <asp:Panel ID="pnlSearchDesc" runat="server" CssClass="divSearchDesc">
        <asp:Literal ID="litSearchDesc" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlDDLSearch" runat="server">
        <asp:Panel ID="pnlSearchInner" runat="server" CssClass="styled-select">
            <asp:Panel ID="pnlDDL" runat="server" CssClass="select">
                <asp:DropDownList ID="ddlSearch" runat="server" CssClass="divDDL">
                </asp:DropDownList>
            </asp:Panel>
        </asp:Panel>
        <asp:ImageButton ID="imgbtnSearchIcon" runat="server" OnClick="imgbtnSearch_Click"
            CssClass="imgbtnSearch" />
    </asp:Panel>
</asp:Panel>
