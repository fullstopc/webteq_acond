<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrMemberProfile.ascx.cs" Inherits="ctrl_ucUsrMemberLogin" %>
<script type="text/javascript">
        function validatePassword_client(source, args) {
            if(args.Value.length < <% =clsSetting.CONSTADMPASSWORDMINLENGTH %>)
            {
                args.IsValid = false;
                source.errormessage = 'Min. length for password is 6.';
                source.innerHTML =  'Min. length for password is 6.';
            }            
        }

        function validateSamePassword_client(source, args) {
            var txtCurrentPwd = document.getElementById("<%= txtCurrentPwd.ClientID %>");
            var txtNewPwd = document.getElementById("<%= txtNewPwd.ClientID %>");

            if ($.trim(txtCurrentPwd.value) != '' && $.trim(txtNewPwd.value) != '') {
                console.log($.trim(txtCurrentPwd.value)+":" + $.trim(txtNewPwd.value) );

                var index = $.trim(txtCurrentPwd.value).localeCompare($.trim(txtNewPwd.value));

                if(index == 0)
                {
                    args.IsValid = false;

                    source.errormessage = 'Current Password cannot same with old password.';
                    source.innerHTML =  'Current Password cannot same with old password.';
                }
            }           
        }
        
        function validateCurrentPwd_client(source, args) {
            var txtCurrentPwd = document.getElementById("<%= txtCurrentPwd.ClientID %>");
            var txtNewPwd = document.getElementById("<%= txtNewPwd.ClientID %>");
            var txtConfirmPwd = document.getElementById("<%= txtConfirmPwd.ClientID %>");
            
            var boolValid = false;

            source.errormessage = 'Please enter current password.'; 
            source.innerHTML = 'Please enter current password.'; 

            if ($.trim(txtCurrentPwd.value) != '') {
                args.IsValid = true;
            }
            else {
                if ($.trim(txtNewPwd.value) != '') {
                    boolValid = false;
                }
                else {
                    if ($.trim(txtConfirmPwd.value) != '') {
                        boolValid = false;
                    }
                    else {
                        boolValid = true;
                    }
                }

                args.IsValid = boolValid;
            }
        }

        function validateNewPwd_client(source, args) {
            var txtCurrentPwd = document.getElementById("<%= txtCurrentPwd.ClientID %>");
            var txtNewPwd = document.getElementById("<%= txtNewPwd.ClientID %>");
            var txtConfirmPwd = document.getElementById("<%= txtConfirmPwd.ClientID %>");

            var boolValid = false;

            source.errormessage = 'Please enter new password.';
            source.innerHTML =  'Please enter new password.';

            if ($.trim(txtNewPwd.value) != '') {
                args.IsValid = true;
            }
            else {
                if ($.trim(txtCurrentPwd.value) != '') {
                    boolValid = false;
                }
                else {
                    if ($.trim(txtConfirmPwd.value) != '') {
                        boolValid = false;
                    }
                    else {
                        boolValid = true;
                    }
                }

                args.IsValid = boolValid;
            }
        }

        function validateConfirmPwd_client(source, args) {
            var txtCurrentPwd = document.getElementById("<%= txtCurrentPwd.ClientID %>");
            var txtNewPwd = document.getElementById("<%= txtNewPwd.ClientID %>");
            var txtConfirmPwd = document.getElementById("<%= txtConfirmPwd.ClientID %>");

            var boolValid = false;

            source.errormessage = 'Please enter confirm password.';
            source.innerHTML =  'Please enter confirm password.';

            if ($.trim(txtConfirmPwd.value) != '') {
                args.IsValid = true;
            }
            else {
                if ($.trim(txtNewPwd.value) != '') {
                    boolValid = false;
                }
                else {
                    if ($.trim(txtCurrentPwd.value) != '') {
                        boolValid = false;
                    }
                    else {
                        boolValid = true;
                    }
                }

                args.IsValid = boolValid;
            }
        }
</script>

<div class="container-fluid userprofile-form">
    <asp:Panel CssClass="ack-container ack-container--margin-btm" runat="server" ID="pnlAck" visible="false">
        <div class="ack-msg">
            <asp:Literal ID="litAck" runat="server"></asp:Literal>
        </div>
    </asp:Panel>
    <div class="form__container form__container--frame">
        <div class="form__section">
        <div class="form__section__title">
            <i class="material-icons">account_circle</i>
            <span>Profile</span>
        </div>
        <div class="form__section__content">
            <div class="row">
                <div class="col-lg-6">Name</div>
                <div class="col-lg-6">
                    <asp:TextBox runat="server" ID="txtName" placeholder=""></asp:TextBox>
                    <div class="form-error">
                            <asp:RequiredFieldValidator ID="rvName" 
                            runat="server" 
                            ErrorMessage="Please enter name." 
                            Display="Dynamic" 
                            CssClass="errmsg" 
                            ControlToValidate="txtName"
                            ValidationGroup="vgProfile"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="row" runat="server" visible="false">
                <div class="col-lg-6">Address Unit</div>
                <div class="col-lg-6">
                    <asp:TextBox runat="server" ID="txtAddrUnit" placeholder=""></asp:TextBox>
                    <div class="form-error">
                            <asp:RequiredFieldValidator ID="rvAddrUnit" 
                            runat="server" 
                            ErrorMessage="Please enter address unit." 
                            Display="Dynamic" 
                            CssClass="errmsg" 
                            ControlToValidate="txtAddrUnit"
                            ValidationGroup="vgProfile"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">Contact No</div>
                <div class="col-lg-6">
                    <div style="display:flex;align-items:center;justify-content:center;">
                        <asp:TextBox runat="server" ID="txtContactTelPrefix" placeholder="" style="flex:0 0 30%;margin-right:20px;"></asp:TextBox>
                        <asp:TextBox runat="server" ID="txtContactTel" placeholder=""></asp:TextBox>
                    </div>
                    <div class="form-error">
                            <asp:RequiredFieldValidator ID="rvContactTel" 
                            runat="server" 
                            ErrorMessage="Please enter contact no." 
                            Display="Dynamic" 
                            CssClass="errmsg" 
                            ControlToValidate="txtContactTel"
                            ValidationGroup="vgProfile"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <asp:LinkButton runat="server" ID="lnkbtnSaveComp" ToolTip="<%$ Resources:LabelResource, lbl_submit %>" CssClass="button button-full button-round" OnClick="lnkbtnSaveComp_Click" ValidationGroup="vgCompany">
                        <i class="material-icons">save</i><span><%= GetGlobalResourceObject("LabelResource","lbl_submit") %></span>
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
        <div class="form__section">
            <div class="form__section__title">
                <i class="material-icons">phonelink_lock</i>
                <span>Change Password</span>
            </div>
            <div class="form__section__content">
                <div class="row">
                    <div class="col-lg-6">Current Password</div>
                    <div class="col-lg-6">
                        <asp:TextBox ID="txtCurrentPwd" runat="server" MaxLength="20" CssClass="text_medium" TextMode="Password"></asp:TextBox>
                        <div class="form__error">
                            <asp:HiddenField ID="hdnPassword" runat="server" />
                            <asp:HiddenField ID="hdnNewPassword" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvCurrentPwd" runat="server" ControlToValidate="txtCurrentPwd" Display="Dynamic" ValidationGroup="vgChangePwd" CssClass="errmsg" ErrorMessage="Please enter current password."></asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="cvCurrentPwd" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtCurrentPwd" ClientValidationFunction="validateCurrentPwd_client" OnServerValidate="validateCurrentPwd_server" OnPreRender="cvCurrentPwd_PreRender" ValidateEmptyText="true" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                            <asp:CustomValidator ID="cvCurrentPwd2" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtCurrentPwd" ClientValidationFunction="validatePassword_client" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                            <asp:CustomValidator ID="cvCurrentPwdSame" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtCurrentPwd" ClientValidationFunction="validateSamePassword_client" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">New Password</div>
                    <div class="col-lg-6">
                        <asp:TextBox ID="txtNewPwd" runat="server" MaxLength="20" CssClass="text_medium" TextMode="Password"></asp:TextBox>
                        <div class="form__error">
                            <asp:CustomValidator ID="cvNewPwd" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtNewPwd" ClientValidationFunction="validateNewPwd_client" ValidateEmptyText="true" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                            <asp:CustomValidator ID="cvNewPwd2" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtNewPwd" ClientValidationFunction="validatePassword_client" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                            <asp:CustomValidator ID="cvNewPwdSame" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtNewPwd" ClientValidationFunction="validateSamePassword_client" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">Confirm New Password</div>
                    <div class="col-lg-6">
                        <asp:TextBox ID="txtConfirmPwd" runat="server" MaxLength="20" CssClass="text_medium" TextMode="Password"></asp:TextBox>
                        <div class="form__error">
                            <asp:CustomValidator ID="cvConfirmPwd" runat="server" Display="Dynamic" ControlToValidate="txtConfirmPwd" CssClass="errmsg" ClientValidationFunction="validateConfirmPwd_client" ValidateEmptyText="true" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                            <asp:CustomValidator ID="cvConfirmPwd2" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtConfirmPwd" ClientValidationFunction="validatePassword_client" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                            <asp:CompareValidator ID="cvConfirmPwd3" runat="server" Display="Dynamic" CssClass="errmsg" ControlToCompare="txtNewPwd" ControlToValidate="txtConfirmPwd" ErrorMessage="Confirm Password Not Match." ClientValidationFunction="validateConfirmPwd_client" ValidationGroup="vgChangePwd"></asp:CompareValidator>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <asp:LinkButton runat="server" ID="lnkbtnSubmitPassword" ToolTip="<%$ Resources:LabelResource, lbl_submit %>" CssClass="button button-full button-round" OnClick="lnkbtnSubmitPassword_Click" ValidationGroup="vgChangePwd">
                            <i class="material-icons">save</i><span><%= GetGlobalResourceObject("LabelResource","lbl_submit") %></span>
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
   <script type="text/javascript">
      $(function () {


      })
   </script>