﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmGoogleSettings.ascx.cs" Inherits="ctrl_ucAdmGoogleSettings" %>

<script type="text/javascript">

</script>

<asp:Panel ID="pnlAdmCMSControlPanel" runat="server" CssClass="divAdmCMSControlPanel">
    <asp:Panel ID="pnlFormDetails" runat="server" CssClass="">
        <div>
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Google Settings</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblGoogleAnalytic" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtGoogleAnalytic" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="10" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblAutoScriptTag" runat="server"></asp:Label>:</td>
                        <td class="tdLabel"><asp:CheckBox ID="chkboxAutoScriptTag" runat="server" Checked="true" /></td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblGoogleAnalyticViewId" runat="server"></asp:Label>:</td>
                        <td class="tdLabel"><asp:TextBox ID="txtGoogleAnalyticViewId" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblConversionCodeEnquiry" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtConversionCodeEnquiry" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="10" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblConversionCodeOrder" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtConversionCodeOrder" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="10" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblConversionCodeAsk" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtConversionCodeAsk" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="10" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblConversionCodeShare" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtConversionCodeShare" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="10" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblConversionCodeCall" runat="server"></asp:Label>:</td>
                        <td><asp:TextBox ID="txtConversionCodeCall" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="10" ></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="">
            <table id="tblAction" class="formTbl tblData" cellpadding="0" cellspacing="0" style="padding-top: 0;">
                <tr>
                    <td class="tdLabel"></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated" style="padding-top:0px;">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Panel>
