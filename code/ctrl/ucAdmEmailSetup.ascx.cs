﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

public partial class ctrl_ucAdmEmailSetup : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _shipCompId = 0;
    protected int _shipMethodId = 0;
    protected int _areaId = 0;
    protected int _mode = 1;
    protected int _currId = 0;
    protected int itemCount = 0;
    protected int itemTotal = 0;
    protected DataSet _dsCurrency = new DataSet();
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int shipCompId
    {
        get { return _shipCompId; }
        set { _shipCompId = value; }
    }

    public int shipMethodId
    {
        get { return _shipMethodId; }
        set { _shipMethodId = value; }
    }

    public int areaId
    {
        get { return _areaId; }
        set { _areaId = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int currId
    {
        get
        {
            if (ViewState["currId"] == null)
            {
                return _currId;
            }
            else
            {
                return int.Parse(ViewState["currId"].ToString());
            }
        }
        set { ViewState["currId"] = value; }
    }

    protected DataSet dsCurrency
    {
        get
        {
            if (ViewState["DSCURRENCY"] == null)
            {
                return _dsCurrency;
            }
            else
            {
                return (DataSet)ViewState["DSCURRENCY"];
            }
        }
        set { ViewState["DSCURRENCY"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
    }
   #endregion

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        clsConfig config = new clsConfig();
        int intRecordAffected = 0;

        Boolean boolSuccess = true;
        Boolean boolEdited = false;
       
        // Email Settings
        string strNameSmtpServer = lblSmtpServer.Text;
        string strNameDefaultSenderName = lblDefaultSenderName.Text;
        string strNameDefaultSender = lblDefaultSender.Text;
        string strNameDefaultSenderPwd = lblDefaultSenderPwd.Text;
        string strNameDefaultRecipient = lblDefaultRecipient.Text;
        string strNameUseGmailSmtp = lblUseGmailSmtp.Text;
        string strNamePort = lblPort.Text;
        string strNameEnableSSL = lblEnableSSL.Text;
        string strEmailPrefix = lblEmailPrefix.Text;
        string strEmailSignature = lblEmailSignature.Text;

        string strNameSmtpServerValue = txtSmtpServer.Text.Trim();
        string strNameDefaultSenderNameValue = txtDefaultSenderName.Text.Trim();
        string strNameDefaultSenderValue = txtDefaultSender.Text.Trim();
        string strNameDefaultSenderPwdValue = txtDefaultSenderPwd.Text.Trim();
        string strNameDefaultRecipientValue = txtDefaultRecipient.Text.Trim();

        string strNameUseGmailSmtpValue = chkboxGmailSmtp.Checked ? clsConfig.CONSTTRUE : clsConfig.CONSTFALSE;
        string strNamePortValue = txtPort.Text.Trim();
        string strNameEnableSSLValue = chkboxEnableSSL.Checked ? clsConfig.CONSTTRUE : clsConfig.CONSTFALSE;
        string strEmailPrefixValue = txtEmailPrefix.Text;
        string strEmailSignatureValue = txtEmailSignature.Text;

        if (boolSuccess && !config.isItemExist(strNameSmtpServer, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.addItem(strNameSmtpServer, strNameSmtpServerValue, clsConfig.CONSTGROUPEMAILSETTING, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNameSmtpServer, strNameSmtpServerValue, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPEMAILSETTING, strNameSmtpServer, strNameSmtpServerValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNameDefaultSenderName, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.addItem(strNameDefaultSenderName, strNameDefaultSenderNameValue, clsConfig.CONSTGROUPEMAILSETTING, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNameDefaultSenderName, strNameDefaultSenderNameValue, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPEMAILSETTING, strNameDefaultSenderName, strNameDefaultSenderNameValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNameDefaultSender, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.addItem(strNameDefaultSender, strNameDefaultSenderValue, clsConfig.CONSTGROUPEMAILSETTING, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNameDefaultSender, strNameDefaultSenderValue, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPEMAILSETTING, strNameDefaultSender, strNameDefaultSenderValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNameDefaultSenderPwd, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.addItem(strNameDefaultSenderPwd, strNameDefaultSenderPwdValue, clsConfig.CONSTGROUPEMAILSETTING, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNameDefaultSenderPwd, strNameDefaultSenderPwdValue, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPEMAILSETTING, strNameDefaultSenderPwd, strNameDefaultSenderPwdValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNameDefaultRecipient, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.addItem(strNameDefaultRecipient, strNameDefaultRecipientValue, clsConfig.CONSTGROUPEMAILSETTING, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNameDefaultRecipient, strNameDefaultRecipientValue, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPEMAILSETTING, strNameDefaultRecipient, strNameDefaultRecipientValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNameUseGmailSmtp, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.addItem(strNameUseGmailSmtp, strNameUseGmailSmtpValue, clsConfig.CONSTGROUPEMAILSETTING, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNameUseGmailSmtp, strNameUseGmailSmtpValue, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPEMAILSETTING, strNameUseGmailSmtp, strNameUseGmailSmtpValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNamePort, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.addItem(strNamePort, strNamePortValue, clsConfig.CONSTGROUPEMAILSETTING, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNamePort, strNamePortValue, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPEMAILSETTING, strNamePort, strNamePortValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNameEnableSSL, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.addItem(strNameEnableSSL, strNameEnableSSLValue, clsConfig.CONSTGROUPEMAILSETTING, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNameEnableSSL, strNameEnableSSLValue, clsConfig.CONSTGROUPEMAILSETTING))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPEMAILSETTING, strNameEnableSSL, strNameEnableSSLValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strEmailPrefix, clsConfig.CONSTGROUPEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strEmailPrefix, strEmailPrefixValue, clsConfig.CONSTGROUPEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strEmailPrefix, strEmailPrefixValue, clsConfig.CONSTGROUPEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPEMAILCONTENT, strEmailPrefix, strEmailPrefixValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strEmailSignature, clsConfig.CONSTGROUPEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strEmailSignature, strEmailSignatureValue, clsConfig.CONSTGROUPEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strEmailSignature, strEmailSignatureValue, clsConfig.CONSTGROUPEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPEMAILCONTENT, strEmailSignature, strEmailSignatureValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Email Settings

        // Enquiry Email Content
        string strAdminEmailSubject = lblAdminEmailSubject.Text;
        string strAdminEmailContent = lblAdminEmailContent.Text;
        string strUserEmailSubject = lblUserEmailSubject.Text;
        string strUserEmailContent = lblUserEmailContent.Text;

        string strAdminEmailSubjectValue = txtAdminEmailSubject.Text;
        string strAdminEmailContentValue = txtAdminEmailContent.Text.Trim();
        string strUserEmailSubjectValue = txtUserEmailSubject.Text;
        string strUserEmailContentValue = txtUserEmailContent.Text.Trim();

        if (boolSuccess && !config.isItemExist(strAdminEmailSubject, clsConfig.CONSTGROUPEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strAdminEmailSubject, strAdminEmailSubjectValue, clsConfig.CONSTGROUPEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strAdminEmailSubject, strAdminEmailSubjectValue, clsConfig.CONSTGROUPEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPEMAILCONTENT, strAdminEmailSubject, strAdminEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strAdminEmailContent, clsConfig.CONSTGROUPEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strAdminEmailContent, strAdminEmailContentValue, clsConfig.CONSTGROUPEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strAdminEmailContent, strAdminEmailContentValue, clsConfig.CONSTGROUPEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPEMAILCONTENT, strAdminEmailContent, strAdminEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strUserEmailSubject, clsConfig.CONSTGROUPEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strUserEmailSubject, strUserEmailSubjectValue, clsConfig.CONSTGROUPEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strUserEmailSubject, strUserEmailSubjectValue, clsConfig.CONSTGROUPEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPEMAILCONTENT, strUserEmailSubject, strUserEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strUserEmailContent, clsConfig.CONSTGROUPEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strUserEmailContent, strUserEmailContentValue, clsConfig.CONSTGROUPEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strUserEmailContent, strUserEmailContentValue, clsConfig.CONSTGROUPEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPEMAILCONTENT, strUserEmailContent, strUserEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Enquiry Email Content

        // Share to Friend Email Content
        string strSTFEmailSubject = lblSTFEmailSubject.Text;
        string strSTFUserEmailContent = lblSTFUserEmailContent.Text;

        string strSTFEmailSubjectValue = txtSTFEmailSubject.Text;
        string strSTFUserEmailContentValue = txtSTFUserEmailContent.Text.Trim();

        if (boolSuccess && !config.isItemExist(strSTFEmailSubject, clsConfig.CONSTGROUPSTFEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strSTFEmailSubject, strSTFEmailSubjectValue, clsConfig.CONSTGROUPSTFEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strSTFEmailSubject, strSTFEmailSubjectValue, clsConfig.CONSTGROUPSTFEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPSTFEMAILCONTENT, strSTFEmailSubject, strSTFEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strSTFUserEmailContent, clsConfig.CONSTGROUPSTFEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strSTFUserEmailContent, strSTFUserEmailContentValue, clsConfig.CONSTGROUPSTFEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strSTFUserEmailContent, strSTFUserEmailContentValue, clsConfig.CONSTGROUPSTFEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPSTFEMAILCONTENT, strSTFUserEmailContent, strSTFUserEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Share to Friend Email Content

        // Ask Email Content
        string strAskEmailSubject = lblAskEmailSubject.Text;
        string strAskAdminEmailContent = lblAskAdminEmailContent.Text;
        string strAskUserEmailContent = lblAskUserEmailContent.Text;

        string strAskEmailSubjectValue = txtAskEmailSubject.Text;
        string strAskAdminEmailContentValue = txtAskAdminEmailContent.Text.Trim();
        string strAskUserEmailContentValue = txtAskUserEmailContent.Text.Trim();

        if (boolSuccess && !config.isItemExist(strAskEmailSubject, clsConfig.CONSTGROUPASKEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strAskEmailSubject, strAskEmailSubjectValue, clsConfig.CONSTGROUPASKEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strAskEmailSubject, strAskEmailSubjectValue, clsConfig.CONSTGROUPASKEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPASKEMAILCONTENT, strAskEmailSubject, strAskEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strAskAdminEmailContent, clsConfig.CONSTGROUPASKEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strAskAdminEmailContent, strAskAdminEmailContentValue, clsConfig.CONSTGROUPASKEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strAskAdminEmailContent, strAskAdminEmailContentValue, clsConfig.CONSTGROUPASKEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPASKEMAILCONTENT, strAskAdminEmailContent, strAskAdminEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strAskUserEmailContent, clsConfig.CONSTGROUPASKEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strAskUserEmailContent, strAskUserEmailContentValue, clsConfig.CONSTGROUPASKEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strAskUserEmailContent, strAskUserEmailContentValue, clsConfig.CONSTGROUPASKEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPASKEMAILCONTENT, strAskUserEmailContent, strAskUserEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Ask Email Content

        // New Order Email Content
        string strNewOrdEmailSubject = lblNewOrdEmailSubject.Text;
        string strNewOrdAdminEmailContent = lblNewOrdAdminEmailContent.Text;
        string strNewOrdUserEmailContent = lblNewOrdUserEmailContent.Text;

        string strNewOrdEmailSubjectValue = txtNewOrdEmailSubject.Text;
        string strNewOrdAdminEmailContentValue = txtNewOrdAdminEmailContent.Text.Trim();
        string strNewOrdUserEmailContentValue = txtNewOrdUserEmailContent.Text.Trim();

        if (boolSuccess && !config.isItemExist(strNewOrdEmailSubject, clsConfig.CONSTGROUPNEWORDEREMAILCONTENT))
        {
            intRecordAffected = config.addItem(strNewOrdEmailSubject, strNewOrdEmailSubjectValue, clsConfig.CONSTGROUPNEWORDEREMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strNewOrdEmailSubject, strNewOrdEmailSubjectValue, clsConfig.CONSTGROUPNEWORDEREMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPNEWORDEREMAILCONTENT, strNewOrdEmailSubject, strNewOrdEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNewOrdAdminEmailContent, clsConfig.CONSTGROUPNEWORDEREMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strNewOrdAdminEmailContent, strNewOrdAdminEmailContentValue, clsConfig.CONSTGROUPNEWORDEREMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strNewOrdAdminEmailContent, strNewOrdAdminEmailContentValue, clsConfig.CONSTGROUPNEWORDEREMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPNEWORDEREMAILCONTENT, strNewOrdAdminEmailContent, strNewOrdAdminEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strNewOrdUserEmailContent, clsConfig.CONSTGROUPNEWORDEREMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strNewOrdUserEmailContent, strNewOrdUserEmailContentValue, clsConfig.CONSTGROUPNEWORDEREMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strNewOrdUserEmailContent, strNewOrdUserEmailContentValue, clsConfig.CONSTGROUPNEWORDEREMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPNEWORDEREMAILCONTENT, strNewOrdUserEmailContent, strNewOrdUserEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of New Order Email Content

        // Payment Receive (Bank Transfer) Email Content
        string strPayRecBTEmailSubject = lblPayRecBTEmailSubject.Text;
        string strPayRecBTAdminEmailContent = lblPayRecBTAdminEmailContent.Text;
        string strPayRecBTUserEmailContent = lblPayRecBTUserEmailContent.Text;

        string strPayRecBTEmailSubjectValue = txtPayRecBTEmailSubject.Text;
        string strPayRecBTAdminEmailContentValue = txtPayRecBTAdminEmailContent.Text.Trim();
        string strPayRecBTUserEmailContentValue = txtPayRecBTUserEmailContent.Text.Trim();

        if (boolSuccess && !config.isItemExist(strPayRecBTEmailSubject, clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strPayRecBTEmailSubject, strPayRecBTEmailSubjectValue, clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strPayRecBTEmailSubject, strPayRecBTEmailSubjectValue, clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT, strPayRecBTEmailSubject, strPayRecBTEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strPayRecBTAdminEmailContent, clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strPayRecBTAdminEmailContent, strPayRecBTAdminEmailContentValue, clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strPayRecBTAdminEmailContent, strPayRecBTAdminEmailContentValue, clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT, strPayRecBTAdminEmailContent, strPayRecBTAdminEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strPayRecBTUserEmailContent, clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strPayRecBTUserEmailContent, strPayRecBTUserEmailContentValue, clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strPayRecBTUserEmailContent, strPayRecBTUserEmailContentValue, clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT, strPayRecBTUserEmailContent, strPayRecBTUserEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Payment Receive (Bank Transfer) Email Content

        // Payment Receive (Online Payment) Email Content
        string strPayRecOPEmailSubject = lblPayRecOPEmailSubject.Text;
        string strPayRecOPAdminEmailContent = lblPayRecOPAdminEmailContent.Text;
        string strPayRecOPUserEmailContent = lblPayRecOPUserEmailContent.Text;

        string strPayRecOPEmailSubjectValue = txtPayRecOPEmailSubject.Text;
        string strPayRecOPAdminEmailContentValue = txtPayRecOPAdminEmailContent.Text.Trim();
        string strPayRecOPUserEmailContentValue = txtPayRecOPUserEmailContent.Text.Trim();

        if (boolSuccess && !config.isItemExist(strPayRecOPEmailSubject, clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strPayRecOPEmailSubject, strPayRecOPEmailSubjectValue, clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strPayRecOPEmailSubject, strPayRecOPEmailSubjectValue, clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT, strPayRecOPEmailSubject, strPayRecOPEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strPayRecOPAdminEmailContent, clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strPayRecOPAdminEmailContent, strPayRecOPAdminEmailContentValue, clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strPayRecOPAdminEmailContent, strPayRecOPAdminEmailContentValue, clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT, strPayRecOPAdminEmailContent, strPayRecOPAdminEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strPayRecOPUserEmailContent, clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strPayRecOPUserEmailContent, strPayRecOPUserEmailContentValue, clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strPayRecOPUserEmailContent, strPayRecOPUserEmailContentValue, clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT, strPayRecOPUserEmailContent, strPayRecOPUserEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Payment Receive (Online Payment) Email Content

        // Payment Error Email Content
        string strPayErrorEmailSubject = lblPayErrorEmailSubject.Text;
        string strPayErrorAdminEmailContent = lblPayErrorAdminEmailContent.Text;
        string strPayErrorUserEmailContent = lblPayErrorUserEmailContent.Text;

        string strPayErrorEmailSubjectValue = txtPayErrorEmailSubject.Text;
        string strPayErrorAdminEmailContentValue = txtPayErrorAdminEmailContent.Text.Trim();
        string strPayErrorUserEmailContentValue = txtPayErrorUserEmailContent.Text.Trim();

        if (boolSuccess && !config.isItemExist(strPayErrorEmailSubject, clsConfig.CONSTGROUPPAYERROREMAILCONTENT))
        {
            intRecordAffected = config.addItem(strPayErrorEmailSubject, strPayErrorEmailSubjectValue, clsConfig.CONSTGROUPPAYERROREMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strPayErrorEmailSubject, strPayErrorEmailSubjectValue, clsConfig.CONSTGROUPPAYERROREMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPPAYERROREMAILCONTENT, strPayErrorEmailSubject, strPayErrorEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strPayErrorAdminEmailContent, clsConfig.CONSTGROUPPAYERROREMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strPayErrorAdminEmailContent, strPayErrorAdminEmailContentValue, clsConfig.CONSTGROUPPAYERROREMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strPayErrorAdminEmailContent, strPayErrorAdminEmailContentValue, clsConfig.CONSTGROUPPAYERROREMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPPAYERROREMAILCONTENT, strPayErrorAdminEmailContent, strPayErrorAdminEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strPayErrorUserEmailContent, clsConfig.CONSTGROUPPAYERROREMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strPayErrorUserEmailContent, strPayErrorUserEmailContentValue, clsConfig.CONSTGROUPPAYERROREMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strPayErrorUserEmailContent, strPayErrorUserEmailContentValue, clsConfig.CONSTGROUPPAYERROREMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPPAYERROREMAILCONTENT, strPayErrorUserEmailContent, strPayErrorUserEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Payment Error Email Content

        // Payment Reject Email Content
        string strPayRejectEmailSubject = lblPayRejectEmailSubject.Text;
        string strPayRejectAdminEmailContent = lblPayRejectAdminEmailContent.Text;
        string strPayRejectUserEmailContent = lblPayRejectUserEmailContent.Text;

        string strPayRejectEmailSubjectValue = txtPayRejectEmailSubject.Text;
        string strPayRejectAdminEmailContentValue = txtPayRejectAdminEmailContent.Text.Trim();
        string strPayRejectUserEmailContentValue = txtPayRejectUserEmailContent.Text.Trim();

        if (boolSuccess && !config.isItemExist(strPayRejectEmailSubject, clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strPayRejectEmailSubject, strPayRejectEmailSubjectValue, clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strPayRejectEmailSubject, strPayRejectEmailSubjectValue, clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT, strPayRejectEmailSubject, strPayRejectEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strPayRejectAdminEmailContent, clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strPayRejectAdminEmailContent, strPayRejectAdminEmailContentValue, clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strPayRejectAdminEmailContent, strPayRejectAdminEmailContentValue, clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT, strPayRejectAdminEmailContent, strPayRejectAdminEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strPayRejectUserEmailContent, clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strPayRejectUserEmailContent, strPayRejectUserEmailContentValue, clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strPayRejectUserEmailContent, strPayRejectUserEmailContentValue, clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT, strPayRejectUserEmailContent, strPayRejectUserEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Payment Reject Email Content

        // Payment Refund Email Content
        string strPayRefundEmailSubject = lblPayRefundEmailSubject.Text;
        string strPayRefundAdminEmailContent = lblPayRefundAdminEmailContent.Text;
        string strPayRefundUserEmailContent = lblPayRefundUserEmailContent.Text;

        string strPayRefundEmailSubjectValue = txtPayRefundEmailSubject.Text;
        string strPayRefundAdminEmailContentValue = txtPayRefundAdminEmailContent.Text.Trim();
        string strPayRefundUserEmailContentValue = txtPayRefundUserEmailContent.Text.Trim();

        if (boolSuccess && !config.isItemExist(strPayRefundEmailSubject, clsConfig.CONSTGROUPREFUNDEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strPayRefundEmailSubject, strPayRefundEmailSubjectValue, clsConfig.CONSTGROUPREFUNDEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strPayRefundEmailSubject, strPayRefundEmailSubjectValue, clsConfig.CONSTGROUPREFUNDEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPREFUNDEMAILCONTENT, strPayRefundEmailSubject, strPayRefundEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strPayRefundAdminEmailContent, clsConfig.CONSTGROUPREFUNDEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strPayRefundAdminEmailContent, strPayRefundAdminEmailContentValue, clsConfig.CONSTGROUPREFUNDEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strPayRefundAdminEmailContent, strPayRefundAdminEmailContentValue, clsConfig.CONSTGROUPREFUNDEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPREFUNDEMAILCONTENT, strPayRefundAdminEmailContent, strPayRefundAdminEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strPayRefundUserEmailContent, clsConfig.CONSTGROUPREFUNDEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strPayRefundUserEmailContent, strPayRefundUserEmailContentValue, clsConfig.CONSTGROUPREFUNDEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strPayRefundUserEmailContent, strPayRefundUserEmailContentValue, clsConfig.CONSTGROUPREFUNDEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPREFUNDEMAILCONTENT, strPayRefundUserEmailContent, strPayRefundUserEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Payment Refund Email Content

        // Order Processing Email Content
        string strOrdProEmailSubject = lblOrdProEmailSubject.Text;
        string strOrdProAdminEmailContent = lblOrdProAdminEmailContent.Text;
        string strOrdProUserEmailContent = lblOrdProUserEmailContent.Text;

        string strOrdProEmailSubjectValue = txtOrdProEmailSubject.Text;
        string strOrdProAdminEmailContentValue = txtOrdProAdminEmailContent.Text.Trim();
        string strOrdProUserEmailContentValue = txtOrdProUserEmailContent.Text.Trim();

        if (boolSuccess && !config.isItemExist(strOrdProEmailSubject, clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strOrdProEmailSubject, strOrdProEmailSubjectValue, clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strOrdProEmailSubject, strOrdProEmailSubjectValue, clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT, strOrdProEmailSubject, strOrdProEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strOrdProAdminEmailContent, clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strOrdProAdminEmailContent, strOrdProAdminEmailContentValue, clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strOrdProAdminEmailContent, strOrdProAdminEmailContentValue, clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT, strOrdProAdminEmailContent, strOrdProAdminEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strOrdProUserEmailContent, clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strOrdProUserEmailContent, strOrdProUserEmailContentValue, clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strOrdProUserEmailContent, strOrdProUserEmailContentValue, clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT, strOrdProUserEmailContent, strOrdProUserEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Order Processing Email Content

        // Order Delivery Email Content
        string strOrdDelEmailSubject = lblOrdDelEmailSubject.Text;
        string strOrdDelAdminEmailContent = lblOrdDelAdminEmailContent.Text;
        string strOrdDelUserEmailContent = lblOrdDelUserEmailContent.Text;

        string strOrdDelEmailSubjectValue = txtOrdDelEmailSubject.Text;
        string strOrdDelAdminEmailContentValue = txtOrdDelAdminEmailContent.Text.Trim();
        string strOrdDelUserEmailContentValue = txtOrdDelUserEmailContent.Text.Trim();

        if (boolSuccess && !config.isItemExist(strOrdDelEmailSubject, clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strOrdDelEmailSubject, strOrdDelEmailSubjectValue, clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strOrdDelEmailSubject, strOrdDelEmailSubjectValue, clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT, strOrdDelEmailSubject, strOrdDelEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strOrdDelAdminEmailContent, clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strOrdDelAdminEmailContent, strOrdDelAdminEmailContentValue, clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strOrdDelAdminEmailContent, strOrdDelAdminEmailContentValue, clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT, strOrdDelAdminEmailContent, strOrdDelAdminEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strOrdDelUserEmailContent, clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strOrdDelUserEmailContent, strOrdDelUserEmailContentValue, clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strOrdDelUserEmailContent, strOrdDelUserEmailContentValue, clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT, strOrdDelUserEmailContent, strOrdDelUserEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Order Delivery Email Content

        // Order Done Email Content
        string strOrdDoneEmailSubject = lblOrdDoneEmailSubject.Text;
        string strOrdDoneAdminEmailContent = lblOrdDoneAdminEmailContent.Text;
        string strOrdDoneUserEmailContent = lblOrdDoneUserEmailContent.Text;

        string strOrdDoneEmailSubjectValue = txtOrdDoneEmailSubject.Text;
        string strOrdDoneAdminEmailContentValue = txtOrdDoneAdminEmailContent.Text.Trim();
        string strOrdDoneUserEmailContentValue = txtOrdDoneUserEmailContent.Text.Trim();

        if (boolSuccess && !config.isItemExist(strOrdDoneEmailSubject, clsConfig.CONSTGROUPORDDONEEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strOrdDoneEmailSubject, strOrdDoneEmailSubjectValue, clsConfig.CONSTGROUPORDDONEEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strOrdDoneEmailSubject, strOrdDoneEmailSubjectValue, clsConfig.CONSTGROUPORDDONEEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPORDDONEEMAILCONTENT, strOrdDoneEmailSubject, strOrdDoneEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strOrdDoneAdminEmailContent, clsConfig.CONSTGROUPORDDONEEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strOrdDoneAdminEmailContent, strOrdDoneAdminEmailContentValue, clsConfig.CONSTGROUPORDDONEEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strOrdDoneAdminEmailContent, strOrdDoneAdminEmailContentValue, clsConfig.CONSTGROUPORDDONEEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPORDDONEEMAILCONTENT, strOrdDoneAdminEmailContent, strOrdDoneAdminEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strOrdDoneUserEmailContent, clsConfig.CONSTGROUPORDDONEEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strOrdDoneUserEmailContent, strOrdDoneUserEmailContentValue, clsConfig.CONSTGROUPORDDONEEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strOrdDoneUserEmailContent, strOrdDoneUserEmailContentValue, clsConfig.CONSTGROUPORDDONEEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPORDDONEEMAILCONTENT, strOrdDoneUserEmailContent, strOrdDoneUserEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Order Done Email Content

        // Order Cancel Email Content
        string strOrdCancelEmailSubject = lblOrdCancelEmailSubject.Text;
        string strOrdCancelAdminEmailContent = lblOrdCancelAdminEmailContent.Text;
        string strOrdCancelUserEmailContent = lblOrdCancelUserEmailContent.Text;

        string strOrdCancelEmailSubjectValue = txtOrdCancelEmailSubject.Text;
        string strOrdCancelAdminEmailContentValue = txtOrdCancelAdminEmailContent.Text.Trim();
        string strOrdCancelUserEmailContentValue = txtOrdCancelUserEmailContent.Text.Trim();

        if (boolSuccess && !config.isItemExist(strOrdCancelEmailSubject, clsConfig.CONSTGROUPORDCANCELEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strOrdCancelEmailSubject, strOrdCancelEmailSubjectValue, clsConfig.CONSTGROUPORDCANCELEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strOrdCancelEmailSubject, strOrdCancelEmailSubjectValue, clsConfig.CONSTGROUPORDCANCELEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPORDCANCELEMAILCONTENT, strOrdCancelEmailSubject, strOrdCancelEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strOrdCancelAdminEmailContent, clsConfig.CONSTGROUPORDCANCELEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strOrdCancelAdminEmailContent, strOrdCancelAdminEmailContentValue, clsConfig.CONSTGROUPORDCANCELEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strOrdCancelAdminEmailContent, strOrdCancelAdminEmailContentValue, clsConfig.CONSTGROUPORDCANCELEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPORDCANCELEMAILCONTENT, strOrdCancelAdminEmailContent, strOrdCancelAdminEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strOrdCancelUserEmailContent, clsConfig.CONSTGROUPORDCANCELEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strOrdCancelUserEmailContent, strOrdCancelUserEmailContentValue, clsConfig.CONSTGROUPORDCANCELEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strOrdCancelUserEmailContent, strOrdCancelUserEmailContentValue, clsConfig.CONSTGROUPORDCANCELEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPORDCANCELEMAILCONTENT, strOrdCancelUserEmailContent, strOrdCancelUserEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Order Cancel Email Content

        // Forgot Password Email Content
        string strForgotPwdEmailSubject = lblForgotPwdEmailSubject.Text;
        string strForgotPwdUserEmailContent = lblForgotPwdUserEmailContent.Text;

        string strForgotPwdEmailSubjectValue = txtForgotPwdEmailSubject.Text;
        string strForgotPwdUserEmailContentValue = txtForgotPwdUserEmailContent.Text.Trim();

        if (boolSuccess && !config.isItemExist(strForgotPwdEmailSubject, clsConfig.CONSTGROUPFORGOTPWDEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strForgotPwdEmailSubject, strForgotPwdEmailSubjectValue, clsConfig.CONSTGROUPFORGOTPWDEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strForgotPwdEmailSubject, strForgotPwdEmailSubjectValue, clsConfig.CONSTGROUPFORGOTPWDEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPFORGOTPWDEMAILCONTENT, strForgotPwdEmailSubject, strForgotPwdEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strForgotPwdUserEmailContent, clsConfig.CONSTGROUPFORGOTPWDEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strForgotPwdUserEmailContent, strForgotPwdUserEmailContentValue, clsConfig.CONSTGROUPFORGOTPWDEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strForgotPwdUserEmailContent, strForgotPwdUserEmailContentValue, clsConfig.CONSTGROUPFORGOTPWDEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPFORGOTPWDEMAILCONTENT, strForgotPwdUserEmailContent, strForgotPwdUserEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Forgot Password Email Content

        // Page Authorization Email Content
        string strPgAuthAdminEmailSubject = lblPgAuthAdminEmailSubject.Text;
        string strPgAuthAdminEmailContent = lblPgAuthAdminEmailContent.Text;
        string strPgAuthUserEmailSubject = lblCompanyEmailSubject.Text;
        string strPgAuthUserEmailContent = lblCompanyEmailContent.Text;

        string strPgAuthAdminEmailSubjectValue = txtPgAuthAdminEmailSubject.Text;
        string strPgAuthAdminEmailContentValue = txtPgAuthAdminEmailContent.Text.Trim();
        string strPgAuthUserEmailSubjectValue = txtCompanyEmailSubject.Text;
        string strPgAuthUserEmailContentValue = txtCompanyEmailContent.Text.Trim();

        if (boolSuccess && !config.isItemExist(strPgAuthAdminEmailSubject, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strPgAuthAdminEmailSubject, strPgAuthAdminEmailSubjectValue, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strPgAuthAdminEmailSubject, strPgAuthAdminEmailSubjectValue, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT, strPgAuthAdminEmailSubject, strPgAuthAdminEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strPgAuthAdminEmailContent, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strPgAuthAdminEmailContent, strPgAuthAdminEmailContentValue, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strPgAuthAdminEmailContent, strPgAuthAdminEmailContentValue, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT, strPgAuthAdminEmailContent, strPgAuthAdminEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strPgAuthUserEmailSubject, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT))
        {
            intRecordAffected = config.addItem(strPgAuthUserEmailSubject, strPgAuthUserEmailSubjectValue, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strPgAuthUserEmailSubject, strPgAuthUserEmailSubjectValue, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT, strPgAuthUserEmailSubject, strPgAuthUserEmailSubjectValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strPgAuthUserEmailContent, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT))
        {
            intRecordAffected = config.addItem2(strPgAuthUserEmailContent, strPgAuthUserEmailContentValue, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strPgAuthUserEmailContent, strPgAuthUserEmailContentValue, clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT, strPgAuthUserEmailContent, strPgAuthUserEmailContentValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        // End of Page Authorization Email Content

        if (!boolSuccess)
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to update email settings. Please try again.</div>";

            Response.Redirect(currentPageName);
        }
        else
        {
            if (boolEdited)
            {
                Session["EDITEDEMAIL"] = 1;
            }
            else
            {
                Session["EDITEDEMAIL"] = 1;
                Session["NOCHANGE"] = 1;
            }

            Response.Redirect(currentPageName);
        }
    }

    #region "Methods"
    public void fill()
    {
        registerCKEditorScript();

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                mode = 2;
            }

            setPageProperties();
        }
    }

    protected void registerCKEditorScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR")))
        {
            int intFullPath = Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFULLPATH]) == 1 ? clsAdmin.CONSTFULLPATHACTIVE : clsAdmin.CONSTFULLPATHDEFAULT;
            int intType = int.Parse(Session[clsAdmin.CONSTPROJECTTYPE].ToString());
            string strJS = null;
            strJS = "<script type=\"text/javascript\">" +
                     "CKEDITOR.replace('" + txtEmailSignature.ClientID + "'," +
                     "{" +
                     "width:'99%'," +
                     "height:'300'," +
                     "toolbar: 'User3'," +
                     "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                     "}" +
                     ");" +    
                     "CKEDITOR.replace('" + txtAdminEmailContent.ClientID + "'," +
                     "{" +
                     "width:'99%'," +
                     "height:'300'," +
                     "toolbar: 'User3'," +
                     "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                     "}" +
                     ");" +
                     "CKEDITOR.replace('" + txtUserEmailContent.ClientID + "'," +
                     "{" +
                     "width:'99%'," +
                     "height:'300'," +
                     "toolbar: 'User3'," +
                     "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            if (intType == clsAdmin.CONSTTYPEeCATALOG || intType == clsAdmin.CONSTTYPEeCATALOGLITE || intType == clsAdmin.CONSTTYPEeCATALOGLITEEVENTMANAGER || intType == clsAdmin.CONSTTYPEeCATALOGEVENTMANAGER || intType == clsAdmin.CONSTTYPEeSHOP || intType == clsAdmin.CONSTTYPEeSHOPLITE || intType == clsAdmin.CONSTTYPEESHOPINVOICE || intType == clsAdmin.CONSTTYPEESHOPLITEINVOICE || intType == clsAdmin.CONSTTYPEeSHOPEVENTMANAGER)
            {
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtSTFUserEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtAskAdminEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtAskUserEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            }

            if (intType == clsAdmin.CONSTTYPEeSHOP || intType == clsAdmin.CONSTTYPEeSHOPLITE || intType == clsAdmin.CONSTTYPEESHOPINVOICE || intType == clsAdmin.CONSTTYPEESHOPLITEINVOICE || intType == clsAdmin.CONSTTYPEeSHOPEVENTMANAGER)
            {
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtNewOrdAdminEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtNewOrdUserEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtPayRecBTAdminEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtPayRecBTUserEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtPayRecOPAdminEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtPayRecOPUserEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtPayErrorAdminEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtPayErrorUserEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtPayRejectAdminEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtPayRejectUserEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtPayRefundAdminEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                          "CKEDITOR.replace('" + txtPayRefundUserEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtOrdProAdminEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtOrdProUserEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtOrdDelAdminEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtOrdDelUserEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtOrdDoneAdminEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtOrdDoneUserEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtOrdCancelAdminEmailContent.ClientID + "'," +
                          "{" +
                          "width:'99%'," +
                          "height:'300'," +
                          "toolbar: 'User3'," +
                          "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtOrdCancelUserEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            }
            if (intType == clsAdmin.CONSTTYPEeSHOP || intType == clsAdmin.CONSTTYPEeSHOPLITE || intType == clsAdmin.CONSTTYPEESHOPINVOICE || intType == clsAdmin.CONSTTYPEESHOPLITEINVOICE || intType == clsAdmin.CONSTTYPEeSHOPEVENTMANAGER)
            {
                strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                         "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                         "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                         "}" +
                         ");" +
                         "CKEDITOR.replace('" + txtForgotPwdUserEmailContent.ClientID + "'," +
                         "{" +
                         "width:'99%'," +
                         "height:'300'," +
                         "toolbar: 'User3'," +
                         "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            }

            if (Session[clsAdmin.CONSTPROJECTPGAUTHORIZATION] != null && Session[clsAdmin.CONSTPROJECTPGAUTHORIZATION] != "")
            {
                if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTPGAUTHORIZATION]) != 0)
                {
                    if (Session[clsAdmin.CONSTPROJECTINDIVIDUALLOGIN] != null && Session[clsAdmin.CONSTPROJECTINDIVIDUALLOGIN] != "")
                    {
                        if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTINDIVIDUALLOGIN]) != 0)
                        {
                            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                                     "}" +
                                     ");" +
                                     "CKEDITOR.replace('" + txtPgAuthAdminEmailContent.ClientID + "'," +
                                     "{" +
                                     "width:'99%'," +
                                     "height:'300'," +
                                     "toolbar: 'User3'," +
                                     "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                                     "}" +
                                     ");" +
                                     "CKEDITOR.replace('" + txtCompanyEmailContent.ClientID + "'," +
                                     "{" +
                                     "width:'99%'," +
                                     "height:'300'," +
                                     "toolbar: 'User3'," +
                                     "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
                        }

                    }
                }

            }
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                     "}" +
                     ");" +
                     "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR", strJS, false);
        }
    }

    protected void setPageProperties()
    {
        if (Session[clsAdmin.CONSTPROJECTTYPE] != null && Session[clsAdmin.CONSTPROJECTTYPE] != "")
        {
            int intType = int.Parse(Session[clsAdmin.CONSTPROJECTTYPE].ToString());

            if (intType == clsAdmin.CONSTTYPEeCATALOG || intType == clsAdmin.CONSTTYPEeCATALOGLITE || intType == clsAdmin.CONSTTYPEeCATALOGLITEEVENTMANAGER || intType == clsAdmin.CONSTTYPEeCATALOGEVENTMANAGER || intType == clsAdmin.CONSTTYPEeSHOP || intType == clsAdmin.CONSTTYPEeSHOPLITE || intType == clsAdmin.CONSTTYPEESHOPINVOICE || intType == clsAdmin.CONSTTYPEESHOPLITEINVOICE || intType == clsAdmin.CONSTTYPEeSHOPEVENTMANAGER)
            {
                divProductContainer.Visible = true;
                trProduct.Visible = true;
            }

            if (intType == clsAdmin.CONSTTYPEeSHOP || intType == clsAdmin.CONSTTYPEeSHOPLITE || intType == clsAdmin.CONSTTYPEESHOPINVOICE || intType == clsAdmin.CONSTTYPEESHOPLITEINVOICE || intType == clsAdmin.CONSTTYPEeSHOPEVENTMANAGER)
            {
                divMemberContainer.Visible = true;
                divOrderContainer.Visible = true;
                divProductContainer.Visible = true;
                trOrder.Visible = true;
            }
        }

        if (Session[clsAdmin.CONSTPROJECTPGAUTHORIZATION] != null && Session[clsAdmin.CONSTPROJECTPGAUTHORIZATION] != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTPGAUTHORIZATION]) != 0)
            {
                if (Session[clsAdmin.CONSTPROJECTINDIVIDUALLOGIN] != null && Session[clsAdmin.CONSTPROJECTINDIVIDUALLOGIN] != "")
                {
                    if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTINDIVIDUALLOGIN]) != 0)
                    {
                        divPageAuthorization.Visible = true;
                        trPageAuthLogin.Visible = true;
                        trPageAuthPwd.Visible = true;
                        trPageAuthContPerson.Visible = true;
                    }
                }
            }
        }

        lblEmailPrefix.Text = clsConfig.CONSTNAMEEMAILPREFIX;
        lblEmailSignature.Text = clsConfig.CONSTNAMEEMAILSIGNATURE;
        lblAdminEmailSubject.Text = clsConfig.CONSTNAMEADMINEMAILSUBJECT;
        lblAdminEmailContent.Text = clsConfig.CONSTNAMEADMINEMAILCONTENT;
        lblUserEmailSubject.Text = clsConfig.CONSTNAMEUSEREMAILSUBJECT;
        lblUserEmailContent.Text = clsConfig.CONSTNAMEUSEREMAILCONTENT;

        lblSmtpServer.Text = clsConfig.CONSTNAMESMTPSERVER;
        lblDefaultSenderName.Text = clsConfig.CONSTNAMEDEFAULTSENDERNAME;
        lblDefaultSender.Text = clsConfig.CONSTNAMEDEFAULTSENDER;
        lblDefaultSenderPwd.Text = clsConfig.CONSTNAMEDEFAULTSENDERPWD;
        lblDefaultRecipient.Text = clsConfig.CONSTNAMEDEFAULTRECIPIENT;
        lblUseGmailSmtp.Text = clsConfig.CONSTNAMEUSERGMAILSMTP;
        lblPort.Text = clsConfig.CONSTNAMEPORT;
        txtPort.Text = clsConfig.CONSTDEFAULTVALUEPORT;
        lblEnableSSL.Text = clsConfig.CONSTNAMEENABLESSL;

        lblSTFEmailSubject.Text = clsConfig.CONSTNAMESTFEMAILSUBJECT;
        lblSTFUserEmailContent.Text = clsConfig.CONSTNAMESTFUSEREMAILCONTENT;

        lblAskEmailSubject.Text = clsConfig.CONSTNAMEASKEMAILSUBJECT;
        lblAskAdminEmailContent.Text = clsConfig.CONSTNAMEASKADMINEMAILCONTENT;
        lblAskUserEmailContent.Text = clsConfig.CONSTNAMEASKUSEREMAILCONTENT;

        lblNewOrdEmailSubject.Text = clsConfig.CONSTNAMENEWORDEMAILSUBJECT;
        lblNewOrdAdminEmailContent.Text = clsConfig.CONSTNAMENEWORDADMINEMAILCONTENT;
        lblNewOrdUserEmailContent.Text = clsConfig.CONSTNAMENEWORDUSEREMAILCONTENT;

        lblPayRecBTEmailSubject.Text = clsConfig.CONSTNAMEPAYRECBTEMAILSUBJECT;
        lblPayRecBTAdminEmailContent.Text = clsConfig.CONSTNAMEPAYRECBTADMINEMAILCONTENT;
        lblPayRecBTUserEmailContent.Text = clsConfig.CONSTNAMEPAYRECBTUSEREMAILCONTENT;

        lblPayRecOPEmailSubject.Text = clsConfig.CONSTNAMEPAYRECOPEMAILSUBJECT;
        lblPayRecOPAdminEmailContent.Text = clsConfig.CONSTNAMEPAYRECOPADMINEMAILCONTENT;
        lblPayRecOPUserEmailContent.Text = clsConfig.CONSTNAMEPAYRECOPUSEREMAILCONTENT;

        lblPayErrorEmailSubject.Text = clsConfig.CONSTNAMEPAYERROREMAILSUBJECT;
        lblPayErrorAdminEmailContent.Text = clsConfig.CONSTNAMEPAYERRORADMINEMAILCONTENT;
        lblPayErrorUserEmailContent.Text = clsConfig.CONSTNAMEPAYERRORUSEREMAILCONTENT;

        lblPayRejectEmailSubject.Text = clsConfig.CONSTNAMEPAYREJECTEMAILSUBJECT;
        lblPayRejectAdminEmailContent.Text = clsConfig.CONSTNAMEPAYREJECTADMINEMAILCONTENT;
        lblPayRejectUserEmailContent.Text = clsConfig.CONSTNAMEPAYREJECTUSEREMAILCONTENT;

        lblPayRefundEmailSubject.Text = clsConfig.CONSTNAMEREFUNDEMAILSUBJECT;
        lblPayRefundAdminEmailContent.Text = clsConfig.CONSTNAMEREFUNDADMINEMAILCONTENT;
        lblPayRefundUserEmailContent.Text = clsConfig.CONSTNAMEREFUNDUSEREMAILCONTENT;

        lblOrdProEmailSubject.Text = clsConfig.CONSTNAMEORDPROEMAILSUBJECT;
        lblOrdProAdminEmailContent.Text = clsConfig.CONSTNAMEORDPROADMINEMAILCONTENT;
        lblOrdProUserEmailContent.Text = clsConfig.CONSTNAMEORDPROUSEREMAILCONTENT;

        lblOrdDelEmailSubject.Text = clsConfig.CONSTNAMEORDDELEMAILSUBJECT;
        lblOrdDelAdminEmailContent.Text = clsConfig.CONSTNAMEORDDELADMINEMAILCONTENT;
        lblOrdDelUserEmailContent.Text = clsConfig.CONSTNAMEORDDELUSEREMAILCONTENT;

        lblOrdDoneEmailSubject.Text = clsConfig.CONSTNAMEORDDONEEMAILSUBJECT;
        lblOrdDoneAdminEmailContent.Text = clsConfig.CONSTNAMEORDDONEADMINEMAILCONTENT;
        lblOrdDoneUserEmailContent.Text = clsConfig.CONSTNAMEORDDONEUSEREMAILCONTENT;

        lblOrdCancelEmailSubject.Text = clsConfig.CONSTNAMEORDCANCELEMAILSUBJECT;
        lblOrdCancelAdminEmailContent.Text = clsConfig.CONSTNAMEORDCANCELADMINEMAILCONTENT;
        lblOrdCancelUserEmailContent.Text = clsConfig.CONSTNAMEORDCANCELUSEREMAILCONTENT;

        lblForgotPwdEmailSubject.Text = clsConfig.CONSTNAMEFORGOTPWDEMAILSUBJECT;
        lblForgotPwdUserEmailContent.Text = clsConfig.CONSTNAMEFORGOTPWDUSEREMAILCONTENT;

        lblPgAuthAdminEmailSubject.Text = clsConfig.CONSTNAMEPGAUTHORIZATIONADMINEMAILSUBJECT;
        lblPgAuthAdminEmailContent.Text = clsConfig.CONSTNAMEPGAUTHORIZATIONADMINEMAILCONTENT;
        lblCompanyEmailSubject.Text = clsConfig.CONSTNAMEPGAUTHORIZATIONCOMPEMAILSUBJECT;
        lblCompanyEmailContent.Text = clsConfig.CONSTNAMEPGAUTHORIZATIONCOMPEMAILCONTENT;

        fillForm();
    }

    protected void fillForm()
    {
        clsConfig config = new clsConfig();

        DataSet ds = config.getItemList(1);
        DataRow[] foundRow;

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPEMAILSETTING + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMESMTPSERVER, true) == 0) { txtSmtpServer.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEDEFAULTSENDERNAME, true) == 0) { txtDefaultSenderName.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEDEFAULTSENDER, true) == 0) { txtDefaultSender.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEDEFAULTSENDERPWD, true) == 0)
            {
                txtDefaultSenderPwd.Text = row["CONF_VALUE"].ToString();
                txtDefaultSenderPwd.Attributes["value"] = txtDefaultSenderPwd.Text;
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEDEFAULTRECIPIENT, true) == 0) { txtDefaultRecipient.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEUSERGMAILSMTP, true) == 0) { chkboxGmailSmtp.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTTRUE; }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPORT, true) == 0) { txtPort.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEENABLESSL, true) == 0) { chkboxEnableSSL.Checked = row["CONF_VALUE"].ToString() == clsConfig.CONSTTRUE; }
        }

        if (!chkboxGmailSmtp.Checked)
        {
            txtPort.Text = "";
            txtPort.Enabled = false;

            chkboxEnableSSL.Checked = false;
            chkboxEnableSSL.Enabled = false;
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPEMAILCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEMAILPREFIX, true) == 0) { txtEmailPrefix.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEMAILSIGNATURE, true) == 0) { txtEmailSignature.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEADMINEMAILSUBJECT, true) == 0) { txtAdminEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEADMINEMAILCONTENT, true) == 0) { txtAdminEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEUSEREMAILSUBJECT, true) == 0) { txtUserEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEUSEREMAILCONTENT, true) == 0) { txtUserEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPSTFEMAILCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMESTFEMAILSUBJECT, true) == 0) { txtSTFEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMESTFUSEREMAILCONTENT, true) == 0) { txtSTFUserEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPASKEMAILCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEASKEMAILSUBJECT, true) == 0) { txtAskEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEASKADMINEMAILCONTENT, true) == 0) { txtAskAdminEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEASKUSEREMAILCONTENT, true) == 0) { txtAskUserEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPNEWORDEREMAILCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMENEWORDEMAILSUBJECT, true) == 0) { txtNewOrdEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMENEWORDADMINEMAILCONTENT, true) == 0) { txtNewOrdAdminEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMENEWORDUSEREMAILCONTENT, true) == 0) { txtNewOrdUserEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYRECBTEMAILSUBJECT, true) == 0) { txtPayRecBTEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYRECBTADMINEMAILCONTENT, true) == 0) { txtPayRecBTAdminEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYRECBTUSEREMAILCONTENT, true) == 0) { txtPayRecBTUserEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPPAYRECOPEMAILCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYRECOPEMAILSUBJECT, true) == 0) { txtPayRecOPEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYRECOPADMINEMAILCONTENT, true) == 0) { txtPayRecOPAdminEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYRECOPUSEREMAILCONTENT, true) == 0) { txtPayRecOPUserEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPPAYERROREMAILCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYERROREMAILSUBJECT, true) == 0) { txtPayErrorEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYERRORADMINEMAILCONTENT, true) == 0) { txtPayErrorAdminEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYERRORUSEREMAILCONTENT, true) == 0) { txtPayErrorUserEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYREJECTEMAILSUBJECT, true) == 0) { txtPayRejectEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYREJECTADMINEMAILCONTENT, true) == 0) { txtPayRejectAdminEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYREJECTUSEREMAILCONTENT, true) == 0) { txtPayRejectUserEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPREFUNDEMAILCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEREFUNDEMAILSUBJECT, true) == 0) { txtPayRefundEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEREFUNDADMINEMAILCONTENT, true) == 0) { txtPayRefundAdminEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEREFUNDUSEREMAILCONTENT, true) == 0) { txtPayRefundUserEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDPROEMAILSUBJECT, true) == 0) { txtOrdProEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDPROADMINEMAILCONTENT, true) == 0) { txtOrdProAdminEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDPROUSEREMAILCONTENT, true) == 0) { txtOrdProUserEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDDELEMAILSUBJECT, true) == 0) { txtOrdDelEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDDELADMINEMAILCONTENT, true) == 0) { txtOrdDelAdminEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDDELUSEREMAILCONTENT, true) == 0) { txtOrdDelUserEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPORDDONEEMAILCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDDONEEMAILSUBJECT, true) == 0) { txtOrdDoneEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDDONEADMINEMAILCONTENT, true) == 0) { txtOrdDoneAdminEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDDONEUSEREMAILCONTENT, true) == 0) { txtOrdDoneUserEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPORDCANCELEMAILCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDCANCELEMAILSUBJECT, true) == 0) { txtOrdCancelEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDCANCELADMINEMAILCONTENT, true) == 0) { txtOrdCancelAdminEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDCANCELUSEREMAILCONTENT, true) == 0) { txtOrdCancelUserEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPFORGOTPWDEMAILCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFORGOTPWDEMAILSUBJECT, true) == 0) { txtForgotPwdEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFORGOTPWDUSEREMAILCONTENT, true) == 0) { txtForgotPwdUserEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPPGAUTHORIZATIONEMAILCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPGAUTHORIZATIONADMINEMAILSUBJECT, true) == 0) { txtPgAuthAdminEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPGAUTHORIZATIONADMINEMAILCONTENT, true) == 0) { txtPgAuthAdminEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPGAUTHORIZATIONCOMPEMAILSUBJECT, true) == 0) { txtCompanyEmailSubject.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPGAUTHORIZATIONCOMPEMAILCONTENT, true) == 0) { txtCompanyEmailContent.Text = row["CONF_LONGVALUE"].ToString(); }
        }
    }  
    #endregion
}