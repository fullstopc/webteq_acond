﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.Collections;

public partial class ctrl_ucAdmAdjust : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected string _pageListingURL = "";
    protected int _adId = 0;

    protected int intItemCount = 0;
    protected Boolean boolItems = false;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int mode
    {
        get { return _mode; }
        set { _mode = value; }
    }

    public string pageListingURL
    {
        get { return _pageListingURL; }
        set { _pageListingURL = value; }
    }

    public int adId
    {
        get { return _adId; }
        set { _adId = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void imgbtnPrint_Click(object sender, EventArgs e)
    {
        clsAdjust ad = new clsAdjust();
        ad.extractItemById2(adId, 0);
        //clsMis.addAuditTrail(int.Parse(Session["ADMID"].ToString()), GetGlobalResourceObject("GlobalResource", "admPrintAdjustment.Text").ToString() + " (" + ad.adCode + ")");
    }

    protected void validateCode_server(object source, ServerValidateEventArgs args)
    {
        string strCode = txtNo.Text.Trim();

        if (!string.IsNullOrEmpty(strCode))
        {
            clsAdjust ad = new clsAdjust();
            if (ad.isCodeExist(strCode, adId))
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }
    }

    protected void rptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            intItemCount += 1;

            HtmlTableRow trForm = (HtmlTableRow)e.Item.FindControl("trForm");

            if (intItemCount == 1)
            {
                hypAdd.Attributes["onclick"] = "javascript:cloneForm('" + trForm.ClientID + "');";
            }

            TextBox txtProdCode = (TextBox)e.Item.FindControl("txtProdCode");

            if (!Page.ClientScript.IsStartupScriptRegistered("autocomplete" + txtProdCode.ClientID))
            {
                string strJS = "<script type=\"text/javascript\">";
                strJS += "setAutocomplete('" + txtProdCode.ClientID + "');";
                strJS += "</script>";

                Page.ClientScript.RegisterStartupScript(Page.GetType(), "autocomplete" + txtProdCode.ClientID, strJS, false);
            }

            
            if (boolItems)
            {
                txtProdCode.Text = DataBinder.Eval(e.Item.DataItem, "AI_PRODCODE").ToString() +
                                   clsSetting.CONSTDEFAULTSEPERATOR +
                                   DataBinder.Eval(e.Item.DataItem, "AI_PRODNAME").ToString();

                HiddenField hdnId2 = (HiddenField)e.Item.FindControl("hdnId2");
                hdnId2.Value = DataBinder.Eval(e.Item.DataItem, "AI_ID").ToString();

                int intQty = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "AI_PRODQTY"));
                TextBox txtQty = (TextBox)e.Item.FindControl("txtQty");
                txtQty.Text = intQty != 0 ? intQty.ToString() : "";

                int intProdId = int.Parse(DataBinder.Eval(e.Item.DataItem, "AI_PRODID").ToString());

                HiddenField hdnId = (HiddenField)e.Item.FindControl("hdnId");
                hdnId.Value = intProdId.ToString();

                HiddenField hdnIdOld = (HiddenField)e.Item.FindControl("hdnIdOld");
                hdnIdOld.Value = intProdId.ToString() + clsSetting.CONSTDEFAULTSEPERATOR.ToString() + 
                                 intQty.ToString();

                HtmlGenericControl spanUOM = (HtmlGenericControl)e.Item.FindControl("spanUOM");
                spanUOM.InnerHtml = "unit";

                // extract UOM
                clsProduct prod = new clsProduct();
                if (prod.extractProdById(intProdId, 1))
                {
                    spanUOM.InnerHtml = prod.prodUOM;
                }

            }
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strCode = txtNo.Text.Trim();

            string strDate = txtDate.Text.Trim();
            DateTime dtDate = DateTime.MaxValue;
            DateTime dtTryParse;
            if (!string.IsNullOrEmpty(strDate))
            {
                if (DateTime.TryParse(clsMis.formatDateDDMMtoMMDD(strDate), out dtTryParse))
                {
                    dtDate = dtTryParse;
                }
            }

            string strTitle = txtTitle.Text.Trim();
            string strAdjustBy = txtAdjustBy.Text.Trim();

            clsAdjust ad = new clsAdjust();
            int intRecordAffected = 0;
            Boolean boolSuccess = true;
            Boolean boolEdited = false;
            Boolean boolAdded = false;

            if (mode == 1)
            {
                intRecordAffected = ad.addItem(strCode, dtDate, strTitle, strAdjustBy, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1)
                {
                    adId = ad.adId;
                    boolAdded = true;
                }
                else
                {
                    boolSuccess = false;
                }
            }
            else if (mode == 2)
            {
                if (!ad.isExactSameDataSet(adId, strCode, dtDate, strTitle, strAdjustBy))
                {
                    intRecordAffected = ad.updateItemById(adId, strCode, dtDate, strTitle, strAdjustBy, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1)
                    {
                        adId = ad.adId;
                        boolEdited = true;
                    }
                    else
                    {
                        boolSuccess = false;
                    }
                }
            }

            if (boolSuccess)
            {
                int intDetailId = 0;

                int intItemIdOld = 0;
                int intItemQtyOld = 0;

                int intItemCount = 0;
                int intItemId = 0;
                string strItemCode = "";
                string strItemName = "";
                int intItemQty = 0;

                int intCounter = 0;
                string strProductIdId = "";
                string strProductCodeId = "";
                string strProductNameId = "";
                string strProductQtyId = "";

                clsProduct prod = new clsProduct();

                foreach (RepeaterItem i in rptItems.Items)
                {
                    intItemCount += 1;

                    TextBox txtProdCode = (TextBox)i.FindControl("txtProdCode");

                    strItemCode = "";
                    strItemName = "";
                    if (!string.IsNullOrEmpty(txtProdCode.Text.Trim()) && txtProdCode.Text.Trim().Contains(clsSetting.CONSTDEFAULTSEPERATOR))
                    {
                        string[] strProdCodeAndName = (txtProdCode.Text.Trim()).Split(clsSetting.CONSTDEFAULTSEPERATOR);
                        strItemCode = strProdCodeAndName[0];
                        strItemName = strProdCodeAndName[1];
                    }

                    HiddenField hdnId2 = (HiddenField)i.FindControl("hdnId2");
                    intDetailId = !string.IsNullOrEmpty(hdnId2.Value.Trim()) ? int.Parse(hdnId2.Value.Trim()) : 0;

                    HiddenField hdnIdOld = (HiddenField)i.FindControl("hdnIdOld");
                    string strIdOld = hdnIdOld.Value.Trim();
                    if (!string.IsNullOrEmpty(strIdOld))
                    {
                        string[] strIdOldSplit = strIdOld.Split(clsSetting.CONSTDEFAULTSEPERATOR);
                        intItemIdOld = int.Parse(strIdOldSplit[0]);
                        intItemQtyOld = Convert.ToInt16(strIdOldSplit[1]);
                    }

                    HiddenField hdnId = (HiddenField)i.FindControl("hdnId");
                    intItemId = !string.IsNullOrEmpty(hdnId.Value.Trim()) ? int.Parse(hdnId.Value.Trim()) : 0;

                    intItemQty = 0;
                    TextBox txtQty = (TextBox)i.FindControl("txtQty");
                    if (!string.IsNullOrEmpty(txtQty.Text.Trim()))
                    {
                        int intTryParse;
                        if (int.TryParse(txtQty.Text.Trim(), out intTryParse))
                        {
                            intItemQty = intTryParse;
                        }
                    }

                    if (intItemCount == 1)
                    {
                        strProductIdId = hdnId.ClientID;
                        strProductCodeId = txtProdCode.ClientID;
                        strProductQtyId = txtQty.ClientID;
                    }

                    if (intItemId != 0)
                    {
                        if (mode == 1)
                        {
                            if (intItemQty != 0)
                            {
                                intRecordAffected = ad.addItemItem(adId, intItemId, strItemCode, strItemName, intItemQty, int.Parse(Session["ADMID"].ToString()));

                                if (intRecordAffected == 1)
                                {
                                    boolAdded = true;
                                }
                                else
                                {
                                    boolSuccess = false;
                                }
                            }
                        }
                        else if (mode == 2)
                        {
                            if (intItemQty != 0)
                            {
                                if (intDetailId != 0)
                                {
                                    if (!ad.isExactSameDataSet(intDetailId, intItemId, strItemCode, strItemName, intItemQty))
                                    {
                                        intRecordAffected = ad.updateItemItemById(intDetailId, intItemId, strItemCode, strItemName, intItemQty, int.Parse(Session["ADMID"].ToString()));

                                        if (intRecordAffected == 1)
                                        {
                                            boolEdited = true;
                                        }
                                        else
                                        {
                                            boolSuccess = false;
                                        }
                                    }
                                }
                                else
                                {
                                    intRecordAffected = ad.addItemItem(adId, intItemId, strItemCode, strItemName,  intItemQty, int.Parse(Session["ADMID"].ToString()));

                                    if (intRecordAffected == 1)
                                    {
                                        boolEdited = true;
                                    }
                                    else
                                    {
                                        boolSuccess = false;
                                    }
                                }
                            }
                            else
                            {
                                intRecordAffected = ad.deleteItemItemById(intDetailId);

                                if (intRecordAffected == 1)
                                {
                                    boolEdited = true;
                                }
                                else
                                {
                                    boolSuccess = false;
                                }
                            }
                        }

                        if (boolSuccess)
                        {

                        }
                    }
                }

                while (Request[strProductIdId + intCounter.ToString()] != null)
                {
                    intItemId = !string.IsNullOrEmpty(Request[strProductIdId + intCounter.ToString()].ToString().Trim()) ? int.Parse(Request[strProductIdId + intCounter.ToString()].ToString().Trim()) : 0;
                    string strItemCodeAndName = Request[strProductCodeId + intCounter.ToString()] != null ?
                                                Request[strProductCodeId + intCounter.ToString()].ToString().Trim() : "";

                    strItemCode = "";
                    strItemName = "";
                    if (!string.IsNullOrEmpty(strItemCodeAndName) && strItemCodeAndName.Contains(clsSetting.CONSTDEFAULTSEPERATOR))
                    {
                        strItemCode = strItemCodeAndName.Split(clsSetting.CONSTDEFAULTSEPERATOR)[0];
                        strItemName = strItemCodeAndName.Split(clsSetting.CONSTDEFAULTSEPERATOR)[1];
                    }

                    intItemQty = 0;
                    if (Request[strProductQtyId + intCounter.ToString()] != null)
                    {
                        string strQty = Request[strProductQtyId + intCounter.ToString()].ToString().Trim();
                        if (!string.IsNullOrEmpty(strQty))
                        {
                            int intTryParse;
                            if (int.TryParse(strQty, out intTryParse))
                            {
                                intItemQty = intTryParse;
                            }
                        }
                    }

                    if (intItemQty != 0 && intItemId != 0)
                    {
                        intRecordAffected = ad.addItemItem(adId, intItemId, strItemCode, strItemName, intItemQty, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1)
                        {

                            boolEdited = true;
                        }
                        else
                        {
                            boolSuccess = false;
                        }
                    }

                    intCounter += 1;
                }
            }

            if (boolAdded)
            {
                Session["NEWREID"] = adId;
                Response.Redirect(currentPageName + "?id=" + adId);
            }
            else if (boolEdited)
            {
                Session["EDITREID"] = adId;
            }
            else
            {
                if (boolSuccess)
                {
                    Session["EDITREID"] = adId;
                    Session["NOCHANGE"] = 1;
                }
                else
                {
                    if (mode == 1)
                    {
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text").ToString() + "<br />Failed to add new adjustment. Please try again.</div>";
                    }
                    else if (mode == 2)
                    {
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text").ToString() + "<br />Failed to edit adjustment. Please try again.</div>";
                    }
                }
            }

            Response.Redirect(Request.Url.ToString());
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string strDeletedNo = "";

        clsMis.performDeleteAdjustment(adId, ref strDeletedNo);
        Session["DELETEDRENO"] = strDeletedNo;

        Response.Redirect(currentPageName);
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        registerScript();

        if (!IsPostBack)
        {
            bindRptData();

            if (mode == 2)
            {
                fillForm();

                lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteAdjustment.Text").ToString() + "');";
                lnkbtnDelete.Visible = true;

                pnlActionPrint.Visible = true;
                imgbtnPrint.OnClientClick = "javascript:window.open('admPrintAdjust.aspx?id=" + adId + "','_blank','toolbar=false,menubar=false,scrollbars,width=900px','false');";
            }
        }

        lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
    }

    protected void registerScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("FORMSECT3")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "initSect3();";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "FORMSECT3", strJS, false);
        }
    }

    protected void fillForm()
    {
        clsAdjust ad = new clsAdjust();
        if (ad.extractItemById(adId, 0))
        {
            txtNo.Text = ad.adCode;

            if (ad.adDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
            {
                txtDate.Text = clsMis.formatDateMMDDtoDDMM(ad.adDate.ToShortDateString());
            }

            txtTitle.Text = ad.adTitle;
            txtAdjustBy.Text = ad.adAdjustBy;
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }

    protected void bindRptData()
    {
        if (mode == 1)
        {
            ArrayList al = new ArrayList();
            al.Add(1);

            intItemCount = 0;
            rptItems.DataSource = al;
            rptItems.DataBind();
        }
        else if (mode == 2)
        {
            clsAdjust ad = new clsAdjust();
            DataSet ds = new DataSet();
            ds = ad.getItemItemListById(adId);

            DataView dv = new DataView(ds.Tables[0]);
            intItemCount = 0;

            if (dv.Count > 0)
            {
                boolItems = true;
                rptItems.DataSource = dv;
                rptItems.DataBind();
            }
            else
            {
                ArrayList al = new ArrayList();
                al.Add(1);

                rptItems.DataSource = al;
                rptItems.DataBind();
            }
        }
    }
    #endregion
}
