﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmSideMenu.ascx.cs" Inherits="ctrl_ucAdmSideMenu" %>

<asp:Panel ID="pnlSideMenu" runat="server" CssClass="divAdmSideMenu">
    <asp:Panel ID="pnlSideMenuInner" runat="server" CssClass="divAdmSideMenuInner">
        <asp:Panel ID="pnlSideMenuParent" runat="server" CssClass="divAdmSideMenuParent" Visible="false">
            <asp:HyperLink ID="hypParent" runat="server" CssClass="sidemenuLink"></asp:HyperLink>
        </asp:Panel>
        <center class="divAdmSideMenuItemsCenter">
            <asp:Panel ID="pnlSideMenuItems" runat="server" CssClass="divAdmSideMenuItems">
                <asp:Repeater ID="rptItems" runat="server" OnItemDataBound="rptItems_ItemDataBound">
                    <ItemTemplate>
                        <asp:Panel ID="pnlSideMenuItem" runat="server" CssClass="divAdmSideMenuItem">
                            <asp:Panel ID="pnlSideMenuItemInner" runat="server" CssClass="divAdmSideMenuItemInner">
                                <asp:HyperLink ID="hypItem" runat="server" CssClass="sidemenuSubLink" ToolTip='<%# DataBinder.Eval(Container.DataItem, "ADMPAGE_NAME") %>'></asp:HyperLink>
                            </asp:Panel>
                            <asp:Panel ID="pnlSideMenuSubItems" runat="server" CssClass="divAdmSideMenuSubItems" Visible="false">
                                <asp:Repeater ID="rptSubItems" runat="server" OnItemDataBound="rptSubItems_ItemDataBound">
                                      <ItemTemplate>
                                         <asp:Panel ID="pnlSideMenuSubItemInner" runat="server" CssClass="divAdmSideMenuSubItemInner">
                                            <asp:HyperLink ID="hypSubItem" runat="server" CssClass="sidemenuSubLink" ToolTip='<%# DataBinder.Eval(Container.DataItem, "ADMPAGE_NAME") %>'></asp:HyperLink>
                                         </asp:Panel>
                                       </ItemTemplate>
                                 </asp:Repeater>
                            </asp:Panel>
                        </asp:Panel>
                    </ItemTemplate>
                </asp:Repeater>
            </asp:Panel>
        </center>
        <asp:Panel ID="pnlSideMenuBottom" runat="server" CssClass="divAdmSideMenuBottom">
        </asp:Panel>
    </asp:Panel>
</asp:Panel>
