﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucUsrAdBanner : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _pageSize = 0;
    protected int intBannerCount = 0;
    protected int intBannerNoInRow = 5;
    #endregion


    #region "Property Methods"
    public int pageSize
    {
        get
        {
            if (ViewState["PAGESIZE"] == null)
            {
                return _pageSize;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGESIZE"]);
            }
        }
        set { ViewState["PAGESIZE"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.LoadComplete += new EventHandler(Page_LoadComplete);

        if (!IsPostBack)
        {
            imgBannerHdr.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "usr/recently-discount.gif";
            hypBannerHdr.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/pagesub.gif";
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        bindRptData();
    }

    protected void rptAdBanner_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strAdName = DataBinder.Eval(e.Item.DataItem, "AD_NAME").ToString();
            string strAdURL = DataBinder.Eval(e.Item.DataItem, "AD_URL").ToString();
            string strAdBannerName = DataBinder.Eval(e.Item.DataItem, "AD_BANNER").ToString();

            HyperLink hypAdBanner = (HyperLink)e.Item.FindControl("hypAdBanner");
            hypAdBanner.ToolTip = strAdName;

            if (strAdURL.StartsWith("http") || strAdURL.StartsWith("https"))
            {
                hypAdBanner.NavigateUrl = strAdURL;
            }
            else if (strAdURL.StartsWith("www."))
            {
                hypAdBanner.NavigateUrl = "http://" + strAdURL;
            }
            else
            {
                hypAdBanner.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strAdURL;
            }

            string strAdBanner = ConfigurationManager.AppSettings["uplBase"] + clsAdmin.CONSTADMADFOLDER + "/" + strAdBannerName;

            Image resultImage = new Image();
            try
            {
                if (!string.IsNullOrEmpty(strAdBanner))
                {
                    string strImagePath = Server.MapPath(strAdBanner);
                    System.Drawing.Image objImage = System.Drawing.Image.FromFile(strImagePath);
                    resultImage = clsMis.getImageResize2(resultImage, clsAdmin.CONSTUSRADBANNERWIDTH, clsAdmin.CONSTUSRADBANNERHEIGHT, objImage.Width, objImage.Height);
                    objImage = null;
                }
            }
            catch (Exception ex)
            {
            }

            Image imgAdBanner = (Image)e.Item.FindControl("imgAdBanner");
            imgAdBanner.ImageUrl = strAdBanner;
            imgAdBanner.AlternateText = strAdName;
            imgAdBanner.ToolTip = strAdName;
            imgAdBanner.Width = resultImage.Width;
            imgAdBanner.Height = resultImage.Height;

            imgAdBanner.Attributes["style"] = "top:" + (clsAdmin.CONSTUSRADBANNERHEIGHT - resultImage.Height.Value) / 2 + "px;";
            imgAdBanner.Attributes["style"] += "left:" + (clsAdmin.CONSTUSRADBANNERWIDTH - resultImage.Width.Value) / 2 + "px;";
            resultImage.Dispose();
            resultImage = null;

            intBannerCount += 1;
            if (intBannerCount % intBannerNoInRow == 0)
            {
                Panel pnlIndAdBanner = (Panel)e.Item.FindControl("pnlIndAdBanner");
                pnlIndAdBanner.CssClass = "divIndAdBannerLast";
            }
        }
    }
    #endregion

    #region "Methods"
    protected void bindRptData()
    {
        if (Application["ADBANNERLIST"] == null)
        {
            clsAd ad = new clsAd();
            Application["ADBANNERLIST"] = ad.getAdList();
        }

        DataSet ds = new DataSet();
        ds = (DataSet)Application["ADBANNERLIST"];
        
        DataView dv = new DataView(ds.Tables[0]);
        dv.RowFilter = "(AD_STARTDATE <= #" + DateTime.Now.ToString("yyyy-MM-dd") + "# AND AD_ENDDATE >= #" + DateTime.Now.ToString("yyyy-MM-dd") + "#) OR (AD_STARTDATE >= #" + DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss") + "# OR AD_ENDDATE >=#" + DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss") + "#)";

        DataTable dtTemp = new DataTable();
        dtTemp = dv.Table.Clone();
        if (dv.Count > 0)
        {
            int intNoOfBanner = clsAdmin.CONSTUSRADBANNERSIZE;
            if (intNoOfBanner > dv.Count) { intNoOfBanner = dv.Count; }

            Random rand = new Random();
            int intRandNum = 0;
            int i = 0;

            do
            {
                intRandNum = rand.Next(0, dv.Count);
                DataRow row = dv[intRandNum].Row;
                int intAdId = Convert.ToInt16(row["AD_ID"]);

                if (dtTemp.Rows.Count > 0)
                {
                    DataRow[] foundRow = dtTemp.Select("AD_ID = " + intAdId);

                    if (foundRow.Length > 0) { continue; }
                }

                dtTemp.ImportRow(row);
                i++;

            } while (i < intNoOfBanner);
        }

        DataView dvTemp = new DataView(dtTemp);

        PagedDataSource pds = new PagedDataSource();
        pds.DataSource = dvTemp;
        //pds.AllowPaging = true;

        //if (pageSize > 0)
        //{
        //    pds.PageSize = pageSize;
        //}

        rptAdBanner.DataSource = pds;
        rptAdBanner.DataBind();
    }
    #endregion
}