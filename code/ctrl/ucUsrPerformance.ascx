﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrPerformance.ascx.cs" Inherits="ctrl_ucUsrPerformance" %>
<style type="text/css">
    .performance{
        overflow-x: auto;
    }

     #tblPerformance.tblContent td.center,
     #tblPerformance.tblContent th.center {text-align:center;}
     #tblPerformance.tblContent td.right,
     #tblPerformance.tblContent th.right {text-align:right;}
     #tblPerformance.tblContent td.left,
     #tblPerformance.tblContent th.left {text-align:left;}

     #tblPerformance.tblContent td.odd {background-color:#fff;}
     #tblPerformance.tblContent td.even {background-color:#f3f3f3;}

     #tblPerformance.tblContent tfoot h3 {color: #dc153c;margin-bottom:0;}
</style>

<div class="divFitContainer divFitPadding performance">
<table class="tblContent" id="tblPerformance" cellspacing="0" cellpadding="0"></table>
</div>
<script type="text/javascript">
    Number.prototype.formatMoney = function(c, d, t){
        var n = this, 
            c = isNaN(c = Math.abs(c)) ? 2 : c, 
            d = d == undefined ? "." : d, 
            t = t == undefined ? "," : t, 
            s = n < 0 ? "-" : "", 
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    $(function () {
        var data = <%= Newtonsoft.Json.JsonConvert.SerializeObject(returnValue) %>;
        var contest = data.contest

        contest.schools = data.schools.map(function(school){
            school.contestants = data.contestants.filter(function(contestant){
                return contestant.schoolID == school.ID;
            }).map(function (x) {
                x.results = data.results.filter(function (y) {
                    return x.ID == y.contestantID;
                });

                x.profit = x.results.reduce(function (prev, curr) {
                    return prev + (curr.sales - curr.expense);
                }, 0);
                x.sales = x.results.reduce(function (prev, curr) {
                    return prev + (curr.sales);
                }, 0);
                return x;
            });

            return school;
        })
        
        var dates = data.results.map(function(x){ return moment( x.date); });
        var minDate = moment.min(dates);
        var maxDate = moment.max(dates);
        var diffMonth = Math.ceil(maxDate.diff(minDate, 'months', true));

        var dateStart = minDate;
        var dateEnd = maxDate;
        var months = [];

        while (dateEnd > dateStart) {
            months.push({
                date : dateStart.toDate(),
                amount : data.results.filter(function(result){
                    return moment(result.date).isBefore(moment(dateStart.toDate()).add(1,'month'));
                }).reduce(function(prev,curr){
                    return prev + curr.sales;
                },0),
            });
            dateStart.add(1,'month');
        }

        var $thead = "<thead><tr>";
        $thead += "<th>Sekolah</th>";
        $thead += "<th>Syarikat</th>";
        $thead += months.map(function(x){
            return "<th class='center'>"+moment(x.date).format('YYYY MMM')+"</th>";
        });
        $thead += "</tr></thead>"

        var $body = contest.schools.map(function(x, schoolIndex){

            var contentRow = "", contentRowAfter = "";

            if(x.contestants.length > 0){
                contentRow += "<tr>";
                contentRow += "<td class='' rowspan='"+x.contestants.length+"'>"+x.name+"</td>";
                contentRow += x.contestants.filter(function(y,i){
                    return i == 0;
                }).map(function(y){
                    var name = "<td>"+y.companyName+"</td>";
                    var sales = months.map(function(month){
                        var date = moment(month.date);
                        date.add(1,'month');

                        var amount = y.results.filter(function(result){
                            return moment(result.date).isBefore(date);
                        }).reduce(function(prev,curr){
                            return prev + curr.sales;
                        },0);
                        return "<td class='center'>" + (amount == 0 ? '-' : amount.formatMoney(2)) + "</td>";
                    }).join("");
                    return name + sales;
                });
                if(x.contestants.length == 0){
                    contentRow += "<td class='center'>-</td>";
                    contentRow += months.map(function(month){
                        return "<td class='center'>-</td>";
                    }).join("");
                }
                contentRow += "</tr>";

                contentRowAfter += x.contestants.filter(function(y,i){
                    return i > 0;
                }).map(function(y){
                    var name = "<td class='odd'>"+y.companyName+"</td>";
                    var sales = months.map(function(month){
                        var date = moment(month.date);
                        date.add(1,'month');

                        var amount = y.results.filter(function(result){
                            return moment(result.date).isBefore(date);
                        }).reduce(function(prev,curr){
                            return prev + curr.sales;
                        },0);

                        return "<td class='center odd'>" + (amount == 0 ? '-' : amount.formatMoney(2)) + "</td>";
                    }).join("");
                    return "<tr>" + name + sales + "</tr>";
                }).join("");
            }


            

            

            return contentRow + contentRowAfter;
        });

        var $tfoot = "<tfoot><tr>";
        $tfoot += "<td colspan='2'><h3>Total</h3></th>";
        $tfoot += months.map(function(month){
            return "<td class='center'><h3>" + month.amount.formatMoney(2) + "</h3></th>";
        }).join("");
        $tfoot += "</tr>";
        $tfoot += "<tr><td colspan='2' class='even'><h3>Grand Total</h3></th>";
        $tfoot += "<td colspan='"+months.length+"' class='center even'><h3>"+ 
            months.reduce(function(prev,curr){ return prev + curr.amount; },0).formatMoney(2) +
            "</h3></th>";
        $tfoot += "</tr></tfoot>";

        $("#tblPerformance").append($thead + $tfoot + $body);
    });
</script>