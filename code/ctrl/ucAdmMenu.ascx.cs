﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucAdmMenu : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _sectId = 1;
    protected int _type = 1;
    #endregion


    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int type
    {
        get { return _type; }
        set { _type = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "admmenu.css' rel='Stylesheet' type='text/css' />";
        litCSS.Text += "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "nav_private.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            bindMenuItems();
        }
    }

    protected void rptMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intPageId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "ADMPAGE_ID"));
            string strPageName = DataBinder.Eval(e.Item.DataItem, "ADMPAGE_NAME").ToString();
            string strURL = DataBinder.Eval(e.Item.DataItem, "ADMPAGE_URL").ToString();

            
            DataSet ds = new DataSet();
            DataView dv;
            clsAdmin adm = new clsAdmin();
            Boolean boolShowEvent = false;
            Boolean boolShowCouponManager = false;
            Boolean boolShowInventory = false;
            Boolean boolShowGroupMenu = false;
            Boolean boolShowPageAuthorization = false;

            ds = adm.getAdminPageList(1);
            dv = new DataView(ds.Tables[0]);

            dv.RowFilter = "ADMPAGE_PARENT = " + intPageId + " AND ADMPAGE_ACTIVE = 1 AND ADMPAGE_SHOWINMENU = 1 AND ADMPAGE_SHOWINTOPMENU = 1 AND (V_TYPE = " + type + " OR V_TYPE IS NULL) AND ADMPAGE_MOBILE = 0";

            boolShowEvent = type == clsAdmin.CONSTTYPEePROFILELITE || type == clsAdmin.CONSTTYPEeCATALOGLITEEVENTMANAGER || type == clsAdmin.CONSTTYPEeDIRECTORYADMANAGER || type == clsAdmin.CONSTTYPEeSHOPEVENTMANAGER || type == clsAdmin.CONSTTYPEeCATALOGEVENTMANAGER;

            if (Session[clsAdmin.CONSTPROJECTEVENTMANAGER] != null && Session[clsAdmin.CONSTPROJECTEVENTMANAGER] != "")
            {
                boolShowEvent = boolShowEvent ? true : Convert.ToInt16(Session[clsAdmin.CONSTPROJECTEVENTMANAGER]) != 0;
            }

            if (Session[clsAdmin.CONSTPROJECTCOUPONMANAGER] != null && Session[clsAdmin.CONSTPROJECTCOUPONMANAGER] != "")
            {
                boolShowCouponManager = boolShowCouponManager ? true : Convert.ToInt16(Session[clsAdmin.CONSTPROJECTCOUPONMANAGER]) != 0;
            }

            if (Session[clsAdmin.CONSTPROJECTGROUPMENU] != null && Session[clsAdmin.CONSTPROJECTGROUPMENU] != "")
            {
                boolShowGroupMenu = boolShowGroupMenu ? true : Convert.ToInt16(Session[clsAdmin.CONSTPROJECTGROUPMENU]) != 0;
            }

            if (Session[clsAdmin.CONSTPROJECTPGAUTHORIZATION] != null && Session[clsAdmin.CONSTPROJECTPGAUTHORIZATION] != "")
            {
                boolShowPageAuthorization = boolShowPageAuthorization ? true : Convert.ToInt16(Session[clsAdmin.CONSTPROJECTPGAUTHORIZATION]) != 0;
            }

            if (!boolShowEvent)
            {
                dv.RowFilter += " AND ADMPAGE_NAME <> '" + clsAdmin.CONSTADMEVENTMANAGERNAME + "'";
            }

            if (!boolShowCouponManager)
            {
                dv.RowFilter += " AND ADMPAGE_NAME <> '" + clsAdmin.CONSTADMCOUPONMANAGERNAME + "'";
            }

            if (!boolShowGroupMenu)
            {
                dv.RowFilter += " AND ADMPAGE_NAME <> '" + clsAdmin.CONSTADMGROUPMENU + "'";
            }

            if (!boolShowPageAuthorization)
            {
                dv.RowFilter += " AND ADMPAGE_NAME <> '" + clsAdmin.CONSTADMPGAUTHORIZATION + "'";
            }

            dv.Sort = "ADMPAGE_ORDER ASC";

            if (dv.Count > 0)
            {
                Repeater rptSubMenu = (Repeater)e.Item.FindControl("rptSubMenu");

                rptSubMenu.DataSource = dv;
                rptSubMenu.DataBind();

                Panel pnlSubMenu = (Panel)e.Item.FindControl("pnlSubMenu");
                pnlSubMenu.Visible = true;

          
            }

            HyperLink hypMenu = (HyperLink)e.Item.FindControl("hypMenu");
            hypMenu.Text = strPageName + ((dv.Count > 0 ? "<i></i>" : ""));
            hypMenu.ToolTip = strPageName;

            if (sectId == intPageId)
            {
                hypMenu.CssClass = hypMenu.CssClass.Replace("menuLink", "menuLinkSel");
                HtmlGenericControl liMenu = (HtmlGenericControl)e.Item.FindControl("liMenu");
                liMenu.Attributes["class"] = "selected";
            }

            hypMenu.CssClass += (" " + clsMis.getImageIcon(strPageName));

            if (!string.IsNullOrEmpty(strURL)) { hypMenu.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + strURL; }

        }
    }

    protected void rptSubMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intPageId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "ADMPAGE_ID"));
            string strPageName = DataBinder.Eval(e.Item.DataItem, "ADMPAGE_NAME").ToString();
            string strURL = DataBinder.Eval(e.Item.DataItem, "ADMPAGE_URL").ToString();

            HyperLink hypSub = (HyperLink)e.Item.FindControl("hypSub");
            hypSub.Text = "<i class='bullet'></i>" + strPageName;
            hypSub.ToolTip = strPageName;

            if (sectId == intPageId)
            {
                hypSub.CssClass = "submenuLinkSel";

                HyperLink hypMenu = (HyperLink)e.Item.NamingContainer.NamingContainer.FindControl("hypMenu");
                hypMenu.CssClass = hypMenu.CssClass.Replace("menuLink", "menuLinkSel");

                HtmlGenericControl liMenu = (HtmlGenericControl)e.Item.NamingContainer.NamingContainer.FindControl("liMenu");
                liMenu.Attributes["class"] = "selected";
            }

            if (!string.IsNullOrEmpty(strURL)) { hypSub.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + strURL; }


            DataSet ds = new DataSet();
            DataView dv;
            clsAdmin adm = new clsAdmin();
            Boolean boolShowEvent = false;
            Boolean boolShowCouponManager = false;
            ds = adm.getAdminPageList(1);
            dv = new DataView(ds.Tables[0]);

            dv.RowFilter = "ADMPAGE_PARENT = " + intPageId + " AND ADMPAGE_ACTIVE = 1 AND ADMPAGE_SHOWINMENU = 1 AND ADMPAGE_SHOWINTOPMENU = 1 AND (V_TYPE = " + type + " OR V_TYPE IS NULL) AND ADMPAGE_MOBILE = 0";

            boolShowEvent = type == clsAdmin.CONSTTYPEePROFILELITE || type == clsAdmin.CONSTTYPEeCATALOGLITEEVENTMANAGER || type == clsAdmin.CONSTTYPEeDIRECTORYADMANAGER || type == clsAdmin.CONSTTYPEeSHOPEVENTMANAGER || type == clsAdmin.CONSTTYPEeCATALOGEVENTMANAGER;

            dv.Sort = "ADMPAGE_ORDER ASC";

            if (dv.Count > 0)
            {
                Repeater rptSubSubMenu = (Repeater)e.Item.FindControl("rptSubSubMenu");

                rptSubSubMenu.DataSource = dv;
                rptSubSubMenu.DataBind();

                Panel pnlSubSubMenu = (Panel)e.Item.FindControl("pnlSubSubMenu");
                pnlSubSubMenu.Visible = true;
            }

        }
    }

    protected void rptSubSubMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intPageId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "ADMPAGE_ID"));
            int intParentPageId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "ADMPAGE_PARENT"));
            string strPageName = DataBinder.Eval(e.Item.DataItem, "ADMPAGE_NAME").ToString();
            string strURL = DataBinder.Eval(e.Item.DataItem, "ADMPAGE_URL").ToString();

            HyperLink hypSubSub = (HyperLink)e.Item.FindControl("hypSubSub");
            hypSubSub.Text = strPageName;
            hypSubSub.ToolTip = strPageName;

            
            

            if (sectId == intPageId || sectId == intParentPageId)
            {
                if (sectId == intPageId) hypSubSub.CssClass = "subsubmenuLinkSel";

                HyperLink hypMenu = (HyperLink)e.Item.NamingContainer.NamingContainer.NamingContainer.NamingContainer.FindControl("hypMenu");
                hypMenu.CssClass = hypMenu.CssClass.Replace("menuLink", "menuLinkSel");

                HtmlGenericControl liSub = (HtmlGenericControl)e.Item.NamingContainer.NamingContainer.FindControl("liSub");
                liSub.Attributes["class"] = "selected";

            }
            
            if (!string.IsNullOrEmpty(strURL)) { hypSubSub.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + strURL; }
        
        
        }
    }
    #endregion


    #region "Methods"
    protected void bindMenuItems()
    {
        clsAdmin adm = new clsAdmin();

        DataSet ds = new DataSet();
        DataView dv = new DataView();

        ds = adm.getAdminPageList(1);
        dv = new DataView(ds.Tables[0]);

        dv.RowFilter = "ADMPAGE_PARENT = 0 AND ADMPAGE_ACTIVE = 1 AND ADMPAGE_SHOWINMENU = 1 AND ADMPAGE_SHOWINTOPMENU = 1 AND (V_TYPE = " + type + " OR V_TYPE IS NULL) AND ADMPAGE_MOBILE = 0";

        if (Session["ADMROOT"] != null && Session["ADMROOT"] != "")
        {
            if (Session["CTRLTYPE"] != null && Session["CTRLTYPE"] != "")
            {
                if (Convert.ToInt16(Session["CTRLTYPE"]) == 2 || Convert.ToInt16(Session["CTRLTYPE"]) == 3)
                {
                    dv.RowFilter += " AND ADMPAGE_NAME = '" + clsAdmin.CONSTADMUSERMANAGERNAME + "'";
                }
            }
        }

        if (Session[clsAdmin.CONSTPROJECTFAQ] != null && Session[clsAdmin.CONSTPROJECTFAQ] != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFAQ]) == 0)
            {
                dv.RowFilter += " AND ADMPAGE_NAME <> '" + clsAdmin.CONSTADMFAQNAME + "' AND ADMPAGE_NAME <> '" + clsAdmin.CONSTADMFAQMANAGERNAME + "'";
            }
        }

        if (Session[clsAdmin.CONSTPROJECTPROGRAMMANAGER] != null && Session[clsAdmin.CONSTPROJECTPROGRAMMANAGER] != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTPROGRAMMANAGER]) == 0)
            {
                dv.RowFilter += " AND ADMPAGE_NAME <> '" + clsAdmin.CONSTADMPROGRAMNAME + "'";
            }
        }

        // add by sh.chong 09Mar2015
        if (Session[clsAdmin.CONSTPROJECTINVENTORY] != null && Session[clsAdmin.CONSTPROJECTINVENTORY] != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTINVENTORY]) == 0)
            {
                dv.RowFilter += " AND ADMPAGE_NAME <> '" + clsAdmin.CONSTADMINVENTORYNAME + "'";
            }
        }

        // add by subi 17Aug2015
        if (Session[clsAdmin.CONSTPROJECTTRAINING] != null && Session[clsAdmin.CONSTPROJECTTRAINING] != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTTRAINING]) == 0)
            {
                dv.RowFilter += " AND ADMPAGE_NAME <> '" + clsAdmin.CONSTADMTRAINING + "'";
            }
        }

        dv.Sort = "ADMPAGE_ORDER ASC";

        if (dv.Count > 0)
        {
            rptMenu.DataSource = dv;
            rptMenu.DataBind();
        }
    }
    #endregion
}
