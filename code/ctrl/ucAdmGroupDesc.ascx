﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmGroupDesc.ascx.cs" Inherits="ctrl_ucAdmGroupDesc" %>

<asp:Panel ID="pnlGroupDesc" runat="server">
    <asp:Panel ID="pnlGroupDescInner" runat="server">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td colspan="2" class="tdSectionHdr"><asp:Literal ID="litGroupContent" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td class="tdSpacer"></td>
                <td class="tdSpacer"></td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblGroupDesc" runat="server"></asp:Label>:</td>
                <td><asp:TextBox ID="txtCatDesc" runat="server" CssClass="text_big" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
        <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn" OnClick="lnkbtnSave_Click"></asp:LinkButton>
        <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn" CausesValidation="false"></asp:LinkButton>
    </asp:Panel>
</asp:Panel>
