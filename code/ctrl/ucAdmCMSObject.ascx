﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmCMSObject.ascx.cs" Inherits="ctrl_ucAdmCMSObject" %>

<asp:Panel ID="pnlForm" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table id="tblObjectContent" cellpadding="0" cellspacing="0" class="formTbl tblData">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Object Content</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel">Object Title<span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtCMSTitle" runat="server" class="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvCMSTitle" runat="server" ErrorMessage="<br />Please enter title." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtCMSTitle"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel">Object Content:</td>
                    <td>
                        <asp:UpdatePanel runat="server" ID="upnlBackup" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:LinkButton ID="lnkbtnBackup" runat="server" OnClick="lnkbtnBackup_Click" ToolTip="Backup This Version" CssClass="btn btnSave">Backup this</asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnRestore" runat="server" ToolTip="Restore" CssClass="thickbox btn btnBack">Restore</asp:LinkButton>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="lnkbtnBackup" />
                                <%--<asp:PostBackTrigger ControlID="lnkbtnRestore" />--%>
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:TextBox ID="txtCMSContent" runat="server" class="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox>
                    </td>
                </tr>
            </tbody>
        </table>
        <table id="tblObjectSettings" cellpadding="0" cellspacing="0" class="formTbl tblData noBorderBtm">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Object Settings</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr id="trType" runat="server">
                    <td class="tdLabel">Type<span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:DropDownList ID="ddlType" runat="server" CssClass="compulsory"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvType" runat="server" ErrorMessage="<br />Please select one." Display="Dynamic" CssClass="errmsg" ControlToValidate="ddlType"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel">Active:</td>
                    <td class="tdLabel"><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" class="btn btnCancel" Visible="false" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
        
    </asp:Panel>
    
</asp:Panel>
