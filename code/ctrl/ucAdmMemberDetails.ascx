﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmMemberDetails.ascx.cs" Inherits="ctrl_ucAdmMemberDetails" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>
<script type="text/javascript">
    function validatePassword_client(source, args) {
        if(args.Value.length < <% =clsAdmin.CONSTADMUSERNAMEMINLENGTH %>)
        {
            args.IsValid = false;
        }            
    } 

    function generateRandomPassword() {
        var length = 6,
            charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        document.getElementById("<% =txtLoginPassword.ClientID %>").value = retVal;
        document.getElementById("<% =txtLoginPasswordRepeat.ClientID %>").value = retVal;
    }

    function getContactNo() {
        var prefix = document.getElementById("<% =txtContactPrefix.ClientID %>").value;
        var contactno = document.getElementById("<% =txtContactNo.ClientID %>").value;
        document.getElementById("<% =txtLoginUsername.ClientID %>").value = prefix + contactno;
    }

    var $datatable;
    var propertySaveSuccess = function (msg) {
        $.magnificPopup.close();
        $(".resident-property .ack-container .ack-msg").html(msg);
        $(".resident-property .ack-container").removeClass("hide");
        $datatable.ajax.reload();
    };

    var initProperty = function () {
        var columns = [
            { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
            { "data": "property_area_name", "label": "Area", "order": 1,  },
            { "data": "property_unitno", "label": "Unit No.", "order": 2, },
            { "data": "property_type_name", "label": "Type", "order": 3, },
            { "data": "action", "bSortable": false, "bSearchable": false, "label": '<a class="popup" href="admResidentPropertyForm.aspx?memid=<%= memId %>"><i class="fa fa-plus" aria-hidden="true"></i></a>', "order": 4 },
            { "data": "member_id", "order": 5, "visible": false },
        ];
        var columnSort = columns.reduce(function (prev, curr) {
            prev[curr.data] = curr.order;
            return prev;
        }, {});
        var columnDefs = [{
            "render": function (data, type, row) {
                var lnkbtnEdit = "<a class='lnkbtn lnkbtnEdit popup' href='admResidentPropertyForm.aspx?id=" + row.property_id + "&memid=" + row.member_id + "' title='Edit'></a>";
                var lnkbtnDelete = "<a class='lnkbtn lnkbtnDelete lnkbtnDeleteProperty' title='Delete' data-id='" + row.property_id + "'></a>";

                return lnkbtnEdit + lnkbtnDelete;
            },
            "className": "dt-center",
            "targets": columnSort["action"]
        }];
        var properties = {
            columns: columns,
            columnDefs: columnDefs,
            columnSort: columnSort,
            func: "getResidentProperty",
            name: "property",
            dom: 'ZBrti',
            elementID: "tblProperty",
            dataSerializeFunc: function (d) {
                //console.log(d, this.func);
                d.searchCustom = null;
                d.func = "getResidentProperty";
                d.columns[columnSort["member_id"]].search.value = <%= memId %>;

                return JSON.stringify(d);
            },
            callback: function () {
                $('#tblProperty .popup').magnificPopup({
                    type: 'iframe',
                    mainClass: 'timeslot-frame',
                    iframe: {
                        markup: `
                                <div class="mfp-iframe-scaler">
                                    <div class="mfp-close"></div>
                                    <iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>
                                </div>`
                    }
                });
            }
        };

        $datatable = callDataTables(properties);

    }

    $(document).on("click", ".lnkbtnDeleteProperty", function () {
        if (confirm("<%= HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteProperty.Text") %>")) {
             var objID = $(this).attr("data-id");
             document.getElementById('<%= hdnPropertyID.ClientID %>').value = objID;
             document.getElementById('<%= lnkbtnDeleteProperty.ClientID %>').click();
         }
     })

    $(function () {
        initProperty();
    })
</script>   
<asp:Panel ID="pnlFormSect1" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server" CssClass="container-fluid">
        <div class="form__container form__container--action-floated">
            <!-- Start of Resident Details -->
            <div class="form__section">
                <div class="form__section__title">
                    <span>Resident Details</span>
                </div>
                <div class="form__section__content form__section__content-leftpad">
                    <asp:Panel ID="pnlLastLogin" runat="server" Visible="false">
                        <div style="float: right">
                            <span class="form__tag">
                                Last Login<br />
                                <asp:Label runat="server" ID="lblLastLogin" CssClass="last-action-date"></asp:Label><br />
                                <br />
                                Last Updated<br />
                                <asp:Label runat="server" ID="lblLastUpdated" CssClass="last-action-date"></asp:Label><br />
                                <br />
                                Registered<br />
                                <asp:Label runat="server" ID="lblCreation" CssClass="last-action-date"></asp:Label><br />
                            </span>
                        </div>
                    </asp:Panel>
                    <div class="row">
                        <div class="col-md-3 form__label">Name :<span class="attention_compulsory">*</span></div>
                        <div class="col-md-9 form__control">
                            <asp:TextBox ID="txtName" runat="server" MaxLength="250" CssClass="text_fullwidth"></asp:TextBox>
                            <div class="form__error">
                                <asp:RequiredFieldValidator ID="rvName" 
                                    runat="server" 
                                    ErrorMessage="Please enter name." 
                                    Display="Dynamic" 
                                    ControlToValidate="txtName" 
                                    CssClass="errmsg"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-3 form__label">
                            Contact No.:<span class="attention_compulsory">*</span>
                        </div>
                        <div class="col-md-9" style="margin-top:5px; padding: 0">
                            <div class="col-md-4">
                                <asp:TextBox ID="txtContactPrefix" runat="server" MaxLength="20" CssClass="text_fullwidth uniq" placeholder="+6016"></asp:TextBox>
                            </div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtContactNo" runat="server" MaxLength="20" CssClass="text_fullwidth uniq"></asp:TextBox>
                            </div>
                            <div class="col-md-12 form__error">
                                <asp:RequiredFieldValidator ID="rvContactNoPrefix"
                                    runat="server"
                                    ErrorMessage="Please enter contact no. prefix."
                                    Display="Dynamic"
                                    ControlToValidate="txtContactPrefix"
                                    CssClass="errmsg"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="rvContactNo"
                                    runat="server"
                                    ErrorMessage="Please enter contact no."
                                    Display="Dynamic"
                                    ControlToValidate="txtContactNo"
                                    CssClass="errmsg"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revContactNoPrefix" runat="server" ErrorMessage="<br />Please enter correct contact no." Display="Dynamic" ControlToValidate="txtContactPrefix" CssClass="errmsg" ValidationExpression="\+\d{1,5}"></asp:RegularExpressionValidator>
                                <asp:CustomValidator ID="cvContactNo" runat="server" ControlToValidate="txtContactNo" Display="Dynamic" OnServerValidate="validateContactNo_server" OnPreRender="cvContactNo_PreRender" CssClass="errmsg" ErrorMessage="This contact no already in used."></asp:CustomValidator>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-3 form__label">
                            Email:<span class="attention_unique">*</span>
                        </div>
                        <div class="col-md-9 form__control">
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="250" cssclass="text_fullwidth uniq"></asp:TextBox>
                            <asp:HiddenField ID="hdnEmail" runat="server" />
                            <asp:HiddenField ID="hdnPassword" runat="server" />
                            <%-- <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="<br />Please enter Email." Display="Dynamic" ControlToValidate="txtEmail" CssClass="errmsg"></asp:RequiredFieldValidator> --%>
                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="<br />Please enter valid Email." Display="Dynamic" ControlToValidate="txtEmail" CssClass="errmsg" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="cvEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" OnServerValidate="validateEmail_server" OnPreRender="cvEmail_PreRender" CssClass="errmsg" ErrorMessage="This email already in used."></asp:CustomValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 form__label">Active: </div>
                        <div class="col-sm-9 form__control">
                            <asp:CheckBox ID="chkActive" runat="server" Checked="true" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Resident Details -->

            <!-- Start of Resident Login -->
            <div class="form__section">
                <div class="form__section__title">
                    Resident Login
                </div>
                <div class="form__section__content form__section__content-leftpad">
                    <div class="row">
                        <div class="col-md-3 form__label"> Login ID: <span class="attention_unique">*</span></div>
                        <div class="col-md-9 form__control">
                            <asp:TextBox ID="txtLoginUsername" runat="server" MaxLength="250" cssclass="text_fullwidth uniq"></asp:TextBox>
                            <a href="javascript:void(0);" onclick="copyValue('<% =txtEmail.ClientID %>','<% =txtLoginUsername.ClientID %>');" title="Same as Email.">Same as Email.</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="getContactNo()" title="Same as Contact No.">Same as Contact No.</a>
                            <div class="form__error">
                                <asp:RequiredFieldValidator ID="rvLoginUsername" runat="server" ErrorMessage="Please enter username." Display="Dynamic" ControlToValidate="txtLoginUsername" CssClass="errmsg"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvLoginUsername" runat="server" ControlToValidate="txtLoginUsername" Display="Dynamic" OnServerValidate="cvLoginUsername_ServerValidate" OnPreRender="cvLoginUsername_PreRender" CssClass="errmsg"></asp:CustomValidator>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 form__label"> Password: <span class="attention_compulsory">*</span></div>
                        <div class="col-md-9 form__control">
                            <asp:TextBox ID="txtLoginPassword" runat="server" MaxLength="250" cssclass="text_fullwidth"></asp:TextBox>
                            <a href="javascript:void(0);" onclick="generateRandomPassword()" title="Random Password">Random Password</a>
                            <div class="form__error">
                                <asp:CustomValidator ID="cvUserPwd" runat="server" CssClass="errmsg" ControlToValidate="txtLoginPassword" Display="Dynamic" ClientValidationFunction="validatePassword_client" SetFocusOnError="true"></asp:CustomValidator>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 form__label"> Confirm Password: <span class="attention_compulsory">*</span></div>
                        <div class="col-md-9 form__control">
                            <asp:TextBox ID="txtLoginPasswordRepeat" runat="server" MaxLength="250" cssclass="text_fullwidth"></asp:TextBox><a href="javascript:void(0);" onclick="copyValue('<% =txtLoginPassword.ClientID %>','<% =txtLoginPasswordRepeat.ClientID %>');" title="Same as Password">Same as Password</a>
                            <div class="form__error">
                                <asp:CompareValidator ID="cmvPassword" runat="server" ErrorMessage="Password does not match." ControlToCompare="txtLoginPassword" ControlToValidate="txtLoginPasswordRepeat" Display="Dynamic" class="errmsg"></asp:CompareValidator>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Resident Login -->

            <!-- Start of Resident Properties -->
            <% if (mode == 2) { %>
            <div class="form__section resident-property">
                <div class="ack-container success hide">
                    <div class="ack-msg"></div>
                </div>
                <div class="form__section__title">
                    Properties
                </div>
                <div class="form__section__content">
                    <uc:CmnDatatable ID="ucCmnDatatable_Property" runat="server" />
                    <asp:HiddenField runat="server" ID="hdnPropertyID" />
                    <asp:LinkButton ID="lnkbtnDeleteProperty" runat="server" OnClick="lnkbtnDeleteProperty_Click" Style="display: none;"></asp:LinkButton>
                    <table id="tblProperty"></table>
                </div>
            </div>
            <% } %>
            <!-- End of Resident Properties -->
        </div>

        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                <asp:Hyperlink ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack" CausesValidation="false" NavigateUrl="../adm/admMember01.aspx"></asp:Hyperlink>
            </asp:Panel>
            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                <asp:LinkButton ID="lnkbtnDelete" 
                                runat="server" 
                                Text="Delete" 
                                ToolTip="Delete" 
                                CssClass="btn btnCancel" 
                                OnClick="lnkbtnDelete_Click"
                                OnClientClick="return confirm('Are you sure you want to delete?')"></asp:LinkButton>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Panel>
