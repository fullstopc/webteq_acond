﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucUsrLoginLinks : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _formSect = 0;
    protected string[] loginTemplates = new string[4] { "memberprofile.aspx", "memberresult.aspx", "changepassword.aspx", "holders.aspx" };
    public bool isMobile = false;
    #endregion


    #region "Property Methods"
    public int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set { ViewState["FORMSECT"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {



        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        

        litUpdateProfile.Text = hypUpdateProfile.ToolTip = "Kemaskini Profil";
        litChangePwd.Text = hypChangePwd.ToolTip = "Tukar Kata Laluan";
        litCheckResult.Text = hypCheckResult.ToolTip = "Mengambil Bahagian";
        litStudents.Text = hypStudents.ToolTip = "Pemegang";
        litLogout.Text = hypLogout.ToolTip = "Log Keluar";

        if (Application["PAGELIST"] == null)
        {
            Application["PAGELIST"] = new clsPage().getPageList(1);
        }
        DataSet ds = (DataSet)Application["PAGELIST"];
        DataTable dt = ds.Tables[0];

        var rows = dt.AsEnumerable().Where(x => x["page_template"].ToString() == "memberprofile.aspx");
        if (rows.Any()) { litUpdateProfile.Text = hypUpdateProfile.ToolTip = rows.First()["page_displayname"].ToString(); }

        rows = dt.AsEnumerable().Where(x => x["page_template"].ToString() == "changepassword.aspx");
        if (rows.Any()) { litChangePwd.Text = hypChangePwd.ToolTip = rows.First()["page_displayname"].ToString(); }

        rows = dt.AsEnumerable().Where(x => x["page_template"].ToString() == "memberresult.aspx");
        if (rows.Any()) { litCheckResult.Text = hypCheckResult.ToolTip = rows.First()["page_displayname"].ToString(); }

        rows = dt.AsEnumerable().Where(x => x["page_template"].ToString() == "holders.aspx");
        if (rows.Any()) { litStudents.Text = hypStudents.ToolTip = rows.First()["page_displayname"].ToString(); }


        ulLoginLinks.Visible = true;
        hypUpdateProfile.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRPROFILE;
        hypChangePwd.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/changepassword.aspx";
        hypCheckResult.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/memberresult.aspx";
        hypLogout.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRLOGIN;
        hypStudents.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/holders.aspx";

        if (isMobile)
        {
            hypUpdateProfile.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + "memberprofilemb.aspx";
            hypChangePwd.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + "changepasswordmb.aspx";
            hypCheckResult.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + "memberresultmb.aspx";
            hypLogout.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + "loginmb.aspx";
            hypStudents.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + "holdersmb.aspx";
        }

        if (formSect == 0) { hypUpdateProfile.CssClass += " selected"; }
        else if (formSect == 1) { hypCheckResult.CssClass += " selected"; }
        else if (formSect == 2) { hypChangePwd.CssClass += " selected"; }
        else if (formSect == 3) { hypStudents.CssClass += " selected"; }
    }

    public void fillTopMenu(int currentPageID, HyperLink hypLogin, Panel pnlLoginLinks)
    {
        // bind loginlinks
        int pageID = clsPage.getPageIdByTemplate("login.aspx", "", 1);
        if (currentPageID == pageID)
        {
            hypLogin.CssClass = hypLogin.CssClass.Replace("active", "") + " selected";
        }

        clsPage page = new clsPage();
        if (page.extractPageById(pageID, 1))
        {
            hypLogin.Text = page.displayName;
            hypLogin.CssClass += " " + page.cssClass;
            hypLogin.ToolTip = page.title;
            hypLogin.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/login.aspx";
        }

        if (Session["MEM_ID"] != null)
        {
            hypLogin.NavigateUrl = "";
            hypLogin.Text = "Hi, " + Session["MEM_NAME"];
            pnlLoginLinks.Visible = true;

            if (Application["PAGELIST"] == null)
            {
                Application["PAGELIST"] = page.getPageList(1);
            }

            
            DataSet ds = (DataSet)Application["PAGELIST"];
            DataView dv = new DataView(ds.Tables[0]);
            dv.RowFilter = "PAGE_OTHER = 0";
            //dv.RowFilter += " AND PAGE_PARENT = " + clsSetting.CONSTPAGEPARENTROOT;
            dv.RowFilter += " AND PAGE_ID = '" + currentPageID + "' AND PAGE_TEMPLATE in (" + string.Join(",", loginTemplates.Select(x => "'" + x + "'").ToArray()) + ")";

            if (dv.Count == 1)
            {
                hypLogin.CssClass = hypLogin.CssClass.Replace("active", "") + " selected";

                string template = dv[0]["PAGE_TEMPLATE"].ToString();
                formSect = Array.IndexOf(loginTemplates, template);
            }

            fill();
        }
    }
    public void fillSideMenu(int currentPageID, HyperLink hypLogin, Literal litLogin)
    {
        int pageID = clsPage.getPageIdByTemplate("login.aspx", "", 1);
        if (currentPageID == pageID)
        {
            hypLogin.CssClass = hypLogin.CssClass.Replace("active", "") + " selected";
        }

        clsPage page = new clsPage();
        if (page.extractPageById(pageID, 1))
        {
            litLogin.Text = page.displayName;
            hypLogin.CssClass += " " + page.cssClass;
            hypLogin.ToolTip = page.title;
            hypLogin.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/login.aspx";
        }

        if (Session["MEM_ID"] != null)
        {
            hypLogin.NavigateUrl = "";
            litLogin.Text = "Hi, " + Session["MEM_NAME"];

            if (Application["PAGELIST"] == null)
            {
                Application["PAGELIST"] = page.getPageList(1);
            }

            DataSet ds = (DataSet)Application["PAGELIST"];
            DataView dv = new DataView(ds.Tables[0]);
            dv.RowFilter = "PAGE_OTHER = 0";
            //dv.RowFilter += " AND PAGE_PARENT = " + clsSetting.CONSTPAGEPARENTROOT;
            dv.RowFilter += " AND PAGE_ID = '" + currentPageID + "' AND PAGE_TEMPLATE in (" + string.Join(",", loginTemplates.Select(x => "'" + x + "'").ToArray()) + ")";

            hypLogin.CssClass = hypLogin.CssClass.Replace("parent", "") + " parent";
            if (dv.Count == 1)
            {
                hypLogin.CssClass = hypLogin.CssClass.Replace("active", "").Replace("expanded", "") + " selected expanded";

                string template = dv[0]["PAGE_TEMPLATE"].ToString();
                formSect = Array.IndexOf(loginTemplates, template);
            }

            fill();
        }
    }
    public void fillMobileTopeMenu(int currentPageID, HyperLink hypLogin, Literal litLogin)
    {
        int pageID = clsPage.getPageIdByTemplate("login.aspx", "", 1);
        if (currentPageID == pageID)
        {
            hypLogin.CssClass = hypLogin.CssClass.Replace("active", "") + " selected";
        }

        clsPage page = new clsPage();
        if (page.extractPageById(pageID, 1))
        {
            litLogin.Text = page.displayName;
            hypLogin.CssClass += " " + page.cssClass;
            hypLogin.ToolTip = page.title;
            hypLogin.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + "loginmb.aspx";
        }

        if (Session["MEM_ID"] != null)
        {
            hypLogin.NavigateUrl = "";
            hypLogin.CssClass += hypLogin.CssClass.Replace("parent", "") + " parent";
            litLogin.Text = "Hi, " + Session["MEM_NAME"];

            if (Application["PAGELIST"] == null)
            {
                Application["PAGELIST"] = page.getPageList(1);
            }

            DataSet ds = (DataSet)Application["PAGELIST"];
            DataView dv = new DataView(ds.Tables[0]);
            dv.RowFilter = "PAGE_OTHER = 0";
            //dv.RowFilter += " AND PAGE_PARENT = " + clsSetting.CONSTPAGEPARENTROOT;
            dv.RowFilter += " AND PAGE_ID = '" + currentPageID + "' AND PAGE_TEMPLATE in (" + string.Join(",", loginTemplates.Select(x => "'" + x + "'").ToArray()) + ")";

            
            if (dv.Count == 1)
            {
                hypLogin.CssClass = hypLogin.CssClass.Replace("active", "").Replace("expanded", "") + " selected expanded";

                string template = dv[0]["PAGE_TEMPLATE"].ToString();
                formSect = Array.IndexOf(loginTemplates, template);
            }

            fill();
        }
    }
    #endregion
}
