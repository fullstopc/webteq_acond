﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Linq;
public partial class ctrl_ucAdmSlideGroup : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected int _groupId = 0;
    protected int _formSect = 1;
    protected string _pageListingURL = "";
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int groupId
    {
        get
        {
            if (ViewState["groupId"] == null)
            {
                return _groupId;
            }
            else
            {
                return int.Parse(ViewState["groupId"].ToString());
            }
        }
        set { ViewState["groupId"] = value; }
    }

    public int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set { ViewState["FORMSECT"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if (ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void validateName_server(object source, ServerValidateEventArgs args)
    {
        txtName.Text = txtName.Text.Trim();

        clsMasthead grp = new clsMasthead();
        if (grp.isGrpNameExist(txtName.Text, groupId))
        {
            args.IsValid = false;
            cvName.ErrorMessage = "<br />This group name is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvName_PreRender(object source, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("catname"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtName.ClientID + "', document.getElementById('" + cvName.ClientID + "'));";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "catname", strJS, false);
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        int intMovedProdNo = 0;
        string strDeletedGrpName = "";

        clsMasthead grp = new clsMasthead();
        clsMis.performDeleteGroup2(groupId, grp.otherGrpId, ref intMovedProdNo, ref strDeletedGrpName);

        Session["MOVEDPRODNO"] = intMovedProdNo;
        Session["DELETEDGRPNAME"] = strDeletedGrpName;

        Response.Redirect(currentPageName);
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strName = txtName.Text.Trim();
            string strDName = txtDName.Text.Trim();

            string strSliderType = ddlSliderType.SelectedValue;
            string strSliderOption = "";
            string strSliderTrans = "";
            switch(strSliderType){
                case clsAdmin.CONSTLISTVALUE_DEFAULT:
                {
                    strSliderOption = "data-cycle-fx:" + ddlTransitionType_default.SelectedValue + ",";
                    strSliderOption += "data-cycle-random:" + (chkboxRandom_default.Checked ? "true" : "false") + ",";
                    strSliderOption += "data-cycle-reverse:" + (chkboxReverse_default.Checked ? "true" : "false") + ",";
                    strSliderOption += "data-cycle-speed:" + hdnSpeed_default.Value + ",";

                    if (ddlTransitionType_default.SelectedValue == "tileSlide" ||
                       ddlTransitionType_default.SelectedValue == "tileBlind")
                    {
                        strSliderOption += "data-cycle-tile-count:" + hdnTileCount_default.Value + ",";
                        strSliderOption += "data-cycle-tile-vertical:" + (chkTileVert_default.Checked ? "true" : "false") + ",";
                    }
                    strSliderTrans = ddlTransitionType_default.SelectedValue;
                }
                break;

                case clsAdmin.CONSTLISTVALUE_FULLSCREEN:
                {
                    strSliderOption = "transition:" + ddlTransitionType_fullscreen.SelectedValue + ",";
                    strSliderOption += "transition_speed:" + hdnTransitionSpeed_fullscreen.Value + ",";
                    strSliderOption += "slide_interval:" + hdnSlideInterval_fullscreen.Value + ",";
                    strSliderOption += "random:" + (chkboxRandom_fullscreen.Checked ? 1 : 0) + ",";
                    strSliderOption += "autoplay:" + (chkboxAutoPlay_fullscreen.Checked ? 1 : 0) + ",";
                    strSliderTrans = ddlTransitionType_fullscreen.SelectedValue;
                }
                break;

                case clsAdmin.CONSTLISTVALUE_MOBILE:
                {
                    strSliderOption = clsMasthead.dictMobileTransition[ddlTransitionType_mobile.SelectedValue.ToString()]["option"];
                    if (!string.IsNullOrEmpty(hdnCol_mobile.Value)) { strSliderOption = strSliderOption.Replace("[$cols]", hdnCol_mobile.Value); }
                    else { strSliderOption = strSliderOption.Replace("[$cols]", "5"); }

                    if (!string.IsNullOrEmpty(hdnRow_mobile.Value)) { strSliderOption = strSliderOption.Replace("[$rows]", hdnCol_mobile.Value); }
                    else { strSliderOption = strSliderOption.Replace("[$rows]", "5"); }

                    if (!string.IsNullOrEmpty(hdnDuration_mobile.Value)) { strSliderOption = strSliderOption.Replace("[$duration]", hdnDuration_mobile.Value); }
                    else { strSliderOption = strSliderOption.Replace("[$duration]", "1000"); }

                    strSliderTrans = ddlTransitionType_mobile.SelectedValue;
                }
                break;
            }
            int intShowTop = chkboxShowTop.Checked ? 1 : 0;
            int intActive = chkboxActive.Checked ? 1 : 0;

            if (string.IsNullOrEmpty(ddlSliderType.SelectedValue))
            {
                strSliderType = "1";
            }

            clsMasthead grp = new clsMasthead();
            int intRecordAffected = 0;

            if (mode == 1)
            {
                intRecordAffected = grp.addGroup(strName, strDName, strSliderType,strSliderOption, strSliderTrans,
                                                 intShowTop, intActive, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1)
                {
                    Session["NEWGRPID"] = grp.grpId;
                    Response.Redirect(currentPageName + "?id=" + grp.grpId);
                }
                else
                {
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new group. Please try again.</div>";
                    Response.Redirect(currentPageName);
                }
            }
            else if (mode == 2)
            {
                Boolean boolSuccess = true;
                Boolean boolEdited = false;

                if (!grp.isExactSameGrp(groupId, strName, strDName,strSliderType, strSliderTrans,
                                        strSliderOption, intShowTop, intActive))
                {
                    intRecordAffected = grp.updateGroupById(groupId, strName, strDName,strSliderType, strSliderTrans,
                                                            strSliderOption, intShowTop, intActive, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; }
                }

                if (boolEdited)
                {
                    Session["EDITGRPID"] = grp.grpId;
                }
                else
                {
                    if (!boolSuccess)
                    {
                        grp.extractGrpById(groupId, 0);
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit group (" + grp.grpName + "). Please try again.</div>";
                    }
                    else
                    {
                        Session["EDITGRPID"] = groupId;
                        Session["NOCHANGE"] = 1;
                    }
                }

                Response.Redirect(Request.Url.ToString());
            }
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        litGroupDetails.Text = "Group Details";
        lblGroupName.Text = "Group Name";
        lblGroupDName.Text = "Group Display Name";
        litGroupSetting.Text = "Group Settings";

        hypPreview.Attributes["href"] = ConfigurationManager.AppSettings["scriptBase"] +"adm/admMastheadTransitionDemo.aspx?id="+groupId;
        if (!IsPostBack)
        {
            fillSliderEffect();
            if (mode == 2)
            {
                lnkbtnDelete.Visible = true;

                if (Session["DELETEDGRPNAME"] == null) { fillForm(); }

                lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteGroup.Text").ToString().Replace(clsAdmin.CONSTGROUPTAG, "Group") + "'); return false;";
            }

            lnkbtnCancel.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
        }
    }

    protected void fillForm()
    {
        clsMasthead mast = new clsMasthead();
        if (mast.extractGrpById(groupId, 0))
        {
            txtName.Text = mast.grpName;
            txtDName.Text = mast.grpDName;

            if (mast.grpShowTop == 1) { chkboxShowTop.Checked = true; }
            else { chkboxShowTop.Checked = false; }

            if (mast.grpActive == 1) { chkboxActive.Checked = true; }
            else { chkboxActive.Checked = false; }

            if (groupId == mast.otherGrpId) { lnkbtnDelete.Visible = false; }

            if (Session[clsAdmin.CONSTPROJECT_SLIDER_ACTIVE] != null &&
                Session[clsAdmin.CONSTPROJECT_SLIDER_DEFAULT] != null &&
                Session[clsAdmin.CONSTPROJECT_SLIDER_FULLSCREEN] != null)
            {
                if ((int)Session[clsAdmin.CONSTPROJECT_SLIDER_ACTIVE] == 1)
                {
                    ddlSliderType.SelectedValue = mast.grpSliderType;

                    

                    switch (mast.grpSliderType)
                    {
                        case clsAdmin.CONSTLISTVALUE_DEFAULT:
                        { 
                            Dictionary<string, string> dictSliderOption = mast.grpSliderOption.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                                                                                                .Select(part => part.Split(':'))
                                                                                                .ToDictionary(split => split[0], split => split[1]);
                            if (dictSliderOption.ContainsKey("data-cycle-fx")) { ddlTransitionType_default.SelectedValue = dictSliderOption["data-cycle-fx"].Replace("'", ""); }
                            if (dictSliderOption.ContainsKey("data-cycle-random")) { chkboxRandom_default.Checked = dictSliderOption["data-cycle-random"] == "true" ? true : false; }
                            if (dictSliderOption.ContainsKey("data-cycle-reverse")) { chkboxReverse_default.Checked = dictSliderOption["data-cycle-reverse"] == "true" ? true : false; }
                            if (dictSliderOption.ContainsKey("data-cycle-speed")) { hdnSpeed_default.Value = dictSliderOption["data-cycle-speed"]; }

                            if (dictSliderOption.ContainsKey("data-cycle-tile-count")) { hdnTileCount_default.Value = dictSliderOption["data-cycle-tile-count"]; }
                            if (dictSliderOption.ContainsKey("data-cycle-tile-vertical")) { chkTileVert_default.Checked = (dictSliderOption["data-cycle-tile-vertical"] == "true" ? true : false); }
                        }
                        break;
                        case clsAdmin.CONSTLISTVALUE_FULLSCREEN:
                        { 
                            Dictionary<string, string> dictSliderOption = mast.grpSliderOption.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                                                                                                    .Select(part => part.Split(':'))
                                                                                                    .ToDictionary(split => split[0], split => split[1]);
                            if (dictSliderOption.ContainsKey("transition")) { ddlTransitionType_fullscreen.SelectedValue = dictSliderOption["transition"]; }
                            if (dictSliderOption.ContainsKey("transition_speed")) { hdnTransitionSpeed_fullscreen.Value = dictSliderOption["transition_speed"]; }
                            if (dictSliderOption.ContainsKey("slide_interval")) { hdnSlideInterval_fullscreen.Value = dictSliderOption["slide_interval"]; }
                            if (dictSliderOption.ContainsKey("random")) { chkboxRandom_fullscreen.Checked = dictSliderOption["random"] == "1" ? true : false; }
                            if (dictSliderOption.ContainsKey("autoplay")) { chkboxAutoPlay_fullscreen.Checked = dictSliderOption["autoplay"] == "1" ? true : false; }
                        }
                        break;
                        case clsAdmin.CONSTLISTVALUE_MOBILE:
                        {
                                List<string> output = mast.grpSliderOption.Split(new[] { '{', '}' }, StringSplitOptions.RemoveEmptyEntries)
                                            .Select(x => "{" + x + "}")
                                            .ToList();

                                Dictionary<string, string> dictSliderOption = output[0].Replace("{","").Replace("}","").Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                                                                                                .Select(part => part.Split(':'))
                                                                                                .ToDictionary(split => split[0], split => split[1]);
                            

                            if (dictSliderOption.ContainsKey("$Rows")) { hdnRow_mobile.Value = dictSliderOption["$Rows"]; }
                            if (dictSliderOption.ContainsKey("$Cols")) { hdnCol_mobile.Value = dictSliderOption["$Cols"]; }
                            if (dictSliderOption.ContainsKey("$Duration")) { hdnDuration_mobile.Value = dictSliderOption["$Duration"]; }
                            ddlTransitionType_mobile.SelectedValue = mast.grpSliderTrans;
                        }
                        break;
                    }
                }
            }

        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
        }
    }

    private void fillSliderEffect()
    {
        if (Session[clsAdmin.CONSTPROJECT_SLIDER_ACTIVE] != null &&
            Session[clsAdmin.CONSTPROJECT_SLIDER_DEFAULT] != null &&
            Session[clsAdmin.CONSTPROJECT_SLIDER_FULLSCREEN] != null)
        {
            if ((int)Session[clsAdmin.CONSTPROJECT_SLIDER_ACTIVE] == 1)
            {
                divSliderEffect.Visible = true;
                litSliderEffect.Text = "Slider Effect";

                clsMis mis = new clsMis();
                DataView dvSliderType = new DataView(mis.getListByListGrp(clsAdmin.CONSTLISTGROUP_SLIDERTYPE).Tables[0]);

                bool boolSliderDefault = Session[clsAdmin.CONSTPROJECT_SLIDER_DEFAULT] != null && Session[clsAdmin.CONSTPROJECT_SLIDER_DEFAULT].ToString() == "1";
                bool boolSliderFullscreen = Session[clsAdmin.CONSTPROJECT_SLIDER_FULLSCREEN] != null && Session[clsAdmin.CONSTPROJECT_SLIDER_FULLSCREEN].ToString() == "1";
                bool boolSliderMobile = Session[clsAdmin.CONSTPROJECT_SLIDER_MOBILE] != null && Session[clsAdmin.CONSTPROJECT_SLIDER_MOBILE].ToString() == "1";

                foreach (DataRow row in dvSliderType.Table.Rows)
                {
                    if (clsAdmin.CONSTLISTVALUE_DEFAULT == row["LIST_VALUE"].ToString() && boolSliderDefault) // && Session[clsAdmin.CONSTPROJECT_SLIDER_DEFAULT].ToString() == "1"
                    {
                        ddlSliderType.Items.Add(new ListItem(row["LIST_NAME"].ToString(), row["LIST_VALUE"].ToString()));

                        DataView dvTransitionDefault = new DataView(mis.getListByListGrp(clsAdmin.CONSTLISTGROUP_TRANSITIONTYPEDEFAULT).Tables[0]);
                        ddlTransitionType_default.DataSource = dvTransitionDefault;
                        ddlTransitionType_default.DataTextField = "LIST_NAME";
                        ddlTransitionType_default.DataValueField = "LIST_VALUE";
                        ddlTransitionType_default.DataBind();
                    }
                    if (clsAdmin.CONSTLISTVALUE_FULLSCREEN == row["LIST_VALUE"].ToString() && boolSliderFullscreen) // && Session[clsAdmin.CONSTPROJECT_SLIDER_FULLSCREEN].ToString() == "1"
                    {
                        ddlSliderType.Items.Add(new ListItem(row["LIST_NAME"].ToString(), row["LIST_VALUE"].ToString()));

                        DataView dvTransitionFullscreen = new DataView(mis.getListByListGrp(clsAdmin.CONSTLISTGROUP_TRANSITIONTYPEFULLSCREEN).Tables[0]);
                        ddlTransitionType_fullscreen.DataSource = dvTransitionFullscreen;
                        ddlTransitionType_fullscreen.DataTextField = "LIST_NAME";
                        ddlTransitionType_fullscreen.DataValueField = "LIST_VALUE";
                        ddlTransitionType_fullscreen.DataBind();
                    }
                    if (clsAdmin.CONSTLISTVALUE_MOBILE == row["LIST_VALUE"].ToString() && boolSliderMobile) // && Session[clsAdmin.CONSTPROJECT_SLIDER_FULLSCREEN].ToString() == "1"
                    {
                        ddlSliderType.Items.Add(new ListItem(row["LIST_NAME"].ToString(), row["LIST_VALUE"].ToString()));

                        DataView dvTransitionMobile = new DataView(mis.getListByListGrp(clsAdmin.CONSTLISTGROUP_TRANSITIONTYPEMOBILE).Tables[0]);
                        ddlTransitionType_mobile.DataSource = dvTransitionMobile;
                        ddlTransitionType_mobile.DataTextField = "LIST_NAME";
                        ddlTransitionType_mobile.DataValueField = "LIST_VALUE";
                        ddlTransitionType_mobile.DataBind();

                    }

                }

            }
        }

    }


    #endregion
}
