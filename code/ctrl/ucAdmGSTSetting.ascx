﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmGSTSetting.ascx.cs"
    Inherits="ctrl_ucAdmGSTSetting" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>

<script type="text/javascript">


    function validatePercentage_client(source, args) {
        var txtGstPercent = document.getElementById("<%= txtGstPercent.ClientID %>");
        var percentageRE = /<%= clsAdmin.CONSTNUMERICRE %>/;

        if (txtGstPercent.value.match(percentageRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "<br />Please enter only numeric.";
            source.innerHTML = "<br />Please enter only numeric.";
        }
    }


</script>

<asp:Panel ID="pnlAdmCMSControlPanel" runat="server" CssClass="divAdmCMSControlPanel">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table cellpadding="0" cellspacing="0" class="formTbl tblData" id="tblGSTSettings">
            <tr>
                <td class="tdLabel tdGstName">
                    <asp:Label ID="lblGstActivation" runat="server"></asp:Label>:
                </td>
                <td class="tdLabel2">
                    <asp:DropDownList ID="ddlGstActivation" runat="server" CssClass="ddl_fullwidth" AutoPostBack="true" DataTextField="LIST_NAME" DataValueField="LIST_VALUE" OnSelectedIndexChanged="ddlGstActivation_OnSelectedChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 0;">
                    <asp:UpdatePanel ID="upnlGstSetting" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:UpdateProgress ID="uprgMember" runat="server" AssociatedUpdatePanelID="upnlGstSetting"
                                DynamicLayout="true">
                                <ProgressTemplate>
                                    <uc:AdmLoading ID="ucAdmLoadingMember" runat="server" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:MultiView runat="server" ID="mvGSTSetting" ActiveViewIndex="0">
                                <asp:View ID="mvvNotAvailable" runat="server"></asp:View>
                                <asp:View ID="mvvGstInclusive" runat="server">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td class="tdLabel tdGstName">
                                                <asp:Label ID="lblGstInclusiveIcon" runat="server"></asp:Label>:
                                            </td>
                                            <td class="tdLabel2">
                                                <asp:Panel ID="pnlFileUpload" runat="server" CssClass="divFileUpload">
                                                    <asp:Panel ID="pnlFileUploadAction" runat="server" CssClass="divFileUploadAction">
                                                        <asp:FileUpload ID="fileGstIcon" runat="server" />
                                                    </asp:Panel>
                                                    <span class="spanTooltip">
                                                        <span class="icon"></span>
                                                        <span class="msg">
                                                            <asp:Literal ID="litUploadRule" runat="server"></asp:Literal>
                                                        </span>
                                                    </span>
                                                </asp:Panel>
                                                <asp:HiddenField ID="hdnGstIcon" runat="server" />
                                                <asp:HiddenField ID="hdnGstIconRef" runat="server" />
                                                <asp:CustomValidator ID="cvGstIcon" runat="server" ControlToValidate="fileGstIcon" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateGstIcon_server" OnPreRender="cvGstIcon_PreRender" ></asp:CustomValidator>
                                                <asp:Panel ID="pnlGstIcon" runat="server" Visible="false">
                                                    <br /><asp:LinkButton ID="lnkbtnDeleteGstIcon" runat="server" Text="Delete" ToolTip="Delete" CausesValidation="false" OnClick="lnkbtnDeleteGstIcon_Click"></asp:LinkButton>
                                                    <br /><asp:Image ID="imgGstIcon" runat="server" />
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="mvvGstApplied" runat="server">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col style="width: 20%;min-width: 150px;" />
                                            <col style="width: 80%;"/>
                                        </colgroup>
                                        <tr>
                                            <td class="tdLabel tdGstName">
                                                <asp:Label ID="lblGstPercent" runat="server"></asp:Label>:
                                            </td>
                                            <td  valign="middle">
                                                <asp:TextBox ID="txtGstPercent" runat="server" CssClass="text_medium"></asp:TextBox><span class="spanFieldDesc"><asp:Literal ID="litPercentage" runat="server" meta:resourcekey="litPercentage">></asp:Literal></span>
                                               <span class="spanFieldDesc"><asp:CustomValidator ID="cvPercentage" runat="server" ControlToValidate="txtGstPercent" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validatePercentage_client"></asp:CustomValidator></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdLabel">
                                                <asp:Label ID="lblGstProduct" runat="server" ></asp:Label>:
                                            </td>
                                            <td class="">
                                                <asp:DropDownList ID="ddlGstProduct" runat="server" CssClass="ddl_fullwidth" DataTextField="LIST_NAME" DataValueField="LIST_VALUE"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdLabel">
                                                <asp:Label ID="lblShowItemized" runat="server"></asp:Label>:
                                            </td>
                                            <td class="tdCheckbox">
                                                <asp:CheckBox ID="chkShowItemized" runat="server" Checked="true"  />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </asp:MultiView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>           
            <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave"
                                OnClick="lnkbtnSave_Click"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>
