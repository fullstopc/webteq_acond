﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;


public partial class ctrl_ucAdmOrder : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _transId = 0;
    protected int _formSect = 1;
    protected int _cancel = 1;
    protected int _type = 1;
    protected int _status = 0;
    protected int _lastStatus = 0;
    protected decimal _decShipping = Convert.ToDecimal("0.00");
    protected decimal _decTotal = Convert.ToDecimal("0.00");
    protected int _courierId;
    protected string _courier;
    protected string _consignmentNo;
    protected int _paymentReject = 0;
    protected string _paymentRejectRemarks;
    protected int _paymentId = 0;
    protected string _pageListingURL = "";

    protected int intCurStatus;
    protected int intItemCount = 0;
    protected string strItems = "";
    clsOrder ord = new clsOrder();
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int transId
    {
        get
        {
            if (ViewState["TRANSID"] == null)
            {
                return _transId;
            }
            else
            {
                return int.Parse(ViewState["TRANSID"].ToString());
            }
        }
        set { ViewState["TRANSID"] = value; }
    }

    public int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set { ViewState["FORMSECT"] = value; }
    }

    protected int cancel
    {
        get
        {
            if (ViewState["CANCEL"] == null)
            {
                return _cancel;
            }
            else
            {
                return Convert.ToInt16(ViewState["CANCEL"]);
            }
        }
        set { ViewState["CANCEL"] = value; }
    }

    protected int type
    {
        get
        {
            if (ViewState["TYPE"] == null)
            {
                return _type;
            }
            else
            {
                return Convert.ToInt16(ViewState["TYPE"]);
            }
        }
        set { ViewState["TYPE"] = value; }
    }

    protected int status
    {
        get
        {
            if (ViewState["STATUS"] == null)
            {
                return _status;
            }
            else
            {
                return Convert.ToInt16(ViewState["STATUS"]);
            }
        }
        set { ViewState["STATUS"] = value; }
    }

    protected int lastStatus
    {
        get
        {
            if (ViewState["LASTSTATUS"] == null)
            {
                return _lastStatus;
            }
            else
            {
                return Convert.ToInt16(ViewState["LASTSTATUS"]);
            }
        }
        set { ViewState["LASTSTATUS"] = value; }
    }

    public decimal decShipping
    {
        get
        {
            if (ViewState["SHIPPING"] == null)
            {
                return _decShipping;
            }
            else
            {
                return Convert.ToDecimal(ViewState["SHIPPING"]);
            }
        }
        set { ViewState["SHIPPING"] = value; }
    }

    public decimal decTotal
    {
        get
        {
            if (ViewState["DECTOTAL"] == null)
            {
                return _decTotal;
            }
            else
            {
                return Convert.ToDecimal(ViewState["DECTOTAL"]);
            }
        }
        set { ViewState["DECTOTAL"] = value; }
    }

    protected int courierId
    {
        get
        {
            if (ViewState["COURIERID"] == null)
            {
                return _courierId;
            }
            else
            {
                return Convert.ToInt16(ViewState["COURIERID"]);
            }
        }
        set { ViewState["COURIERID"] = value; }
    }

    protected string courier
    {
        get { return ViewState["COURIER"] as string ?? _courier; }
        set { ViewState["COURIER"] = value; }
    }

    protected string consignmentNo
    {
        get { return ViewState["CONSIGNMENTNO"] as string ?? _consignmentNo; }
        set { ViewState["CONSIGNMENTNO"] = value; }
    }

    protected int paymentReject
    {
        get
        {
            if (ViewState["PAYMENTREJECT"] == null)
            {
                return _paymentReject;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAYMENTREJECT"]);
            }
        }
        set { ViewState["PAYMENTREJECT"] = value; }
    }

    protected string paymentRejectRemarks
    {
        get { return ViewState["PAYMENTREJECTREMARKS"] as string ?? _paymentRejectRemarks; }
        set { ViewState["PAYMENTREJECTREMARKS"] = value; }
    }

    protected int paymentId
    {
        get
        {
            if (ViewState["PAYMENTID"] == null)
            {
                return _paymentId;
            }
            else
            {
                return int.Parse(ViewState["PAYMENTID"].ToString());
            }
        }
        set { ViewState["PAYMENTID"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if (ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }

    protected void rptStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Literal litStatus = (Literal)e.Item.FindControl("litStatus");
            string strDateTime = DataBinder.Eval(e.Item.DataItem, "STATUS_DATE").ToString();
            
            if (!string.IsNullOrEmpty(strDateTime))
            {
                DateTime dtStatusDate = Convert.ToDateTime(strDateTime);
                litStatus.Text = dtStatusDate.ToString("dd MMM yyyy");
            }
            else { litStatus.Text = "<span class=\"spanItalic\">" + GetGlobalResourceObject("GlobalResource", "statusDefaultMessage.Text").ToString() + "</span>"; }

            int intStatus = int.Parse(DataBinder.Eval(e.Item.DataItem, "ORDERSTATUS_ID").ToString());
            int intStatusOrder = int.Parse(DataBinder.Eval(e.Item.DataItem, "ORDERSTATUS_ORDER").ToString());

            LinkButton lnkbtnStatus = (LinkButton)e.Item.FindControl("lnkbtnStatus");
            HiddenField hdnStatus = (HiddenField)e.Item.FindControl("hdnStatus");

            //if (status > 0)
            //{
                //@ payment
                if (intStatus == clsAdmin.CONSTORDERSTATUSPAY)
                {
                    if (paymentReject <= 0)
                    {
                        HtmlTableRow trPaymentAmount = (HtmlTableRow)e.Item.FindControl("trPaymentAmount");
                        trPaymentAmount.Visible = true;

                        HtmlTableRow trPaymentRemarks = (HtmlTableRow)e.Item.FindControl("trPaymentRemarks");

                        //@ current status is new
                        if (status == intStatus - 1)
                        {
                            HtmlTableCell tdPaymentAmount = (HtmlTableCell)e.Item.FindControl("tdPaymentAmount");
                            tdPaymentAmount.Attributes["class"] = "tdStatusLabelActive";

                            HtmlTableCell tdPaymentAmountDetails = (HtmlTableCell)e.Item.FindControl("tdPaymentAmountDetails");
                            tdPaymentAmountDetails.Attributes["class"] = "tdStatusActive";

                            HtmlTableCell tdPaymentRemarks = (HtmlTableCell)e.Item.FindControl("tdPaymentRemarks");
                            tdPaymentRemarks.Attributes["class"] = "tdStatusLabelActive";

                            HtmlTableCell tdPaymentRemarksDetails = (HtmlTableCell)e.Item.FindControl("tdPaymentRemarksDetails");
                            tdPaymentRemarksDetails.Attributes["class"] = "tdStatusActive";

                            HtmlTableRow trPaymentAction = (HtmlTableRow)e.Item.FindControl("trPaymentAction");
                            trPaymentAction.Visible = true;

                            Panel pnlPayAmount = (Panel)e.Item.FindControl("pnlPayAmount");
                            Panel pnlPayRemarks = (Panel)e.Item.FindControl("pnlPayRemarks");

                            pnlPayAmount.Visible = true;
                            pnlPayRemarks.Visible = true;
                            trPaymentRemarks.Visible = true;


                            ////@ reject field
                            //HtmlTableRow trPaymentReject = (HtmlTableRow)e.Item.FindControl("trPaymentReject");
                            //trPaymentReject.Visible = true;

                            //HtmlTableCell tdPaymentRejectRemarks = (HtmlTableCell)e.Item.FindControl("tdPaymentRejectRemarks");
                            //tdPaymentRejectRemarks.Attributes["class"] = "tdStatusLabelActivePadding";

                            //HtmlTableCell tdPaymentRejectRemarksDetails = (HtmlTableCell)e.Item.FindControl("tdPaymentRejectRemarksDetails");
                            //tdPaymentRejectRemarksDetails.Attributes["class"] = "tdStatusActivePadding";

                            //Panel pnlPayRejectRemarks = (Panel)e.Item.FindControl("pnlPayRejectRemarks");
                            //pnlPayRejectRemarks.Visible = true;

                            //HtmlTableRow trPaymentRejectAction = (HtmlTableRow)e.Item.FindControl("trPaymentRejectAction");
                            //trPaymentRejectAction.Visible = true;
                        }
                        else //if (status >= intStatus)   // current status is PAID or above
                        {
                            Panel pnlPayAmountTxt = (Panel)e.Item.FindControl("pnlPayAmountTxt");
                            Literal litPayAmount = (Literal)e.Item.FindControl("litPayAmount");

                            Panel pnlPayRemarksText = (Panel)e.Item.FindControl("pnlPayRemarksText");
                            Literal litPayRemarks = (Literal)e.Item.FindControl("litPayRemarks");

                            litPayAmount.Text = hdnPayAmount.Value;
                            litPayRemarks.Text = hdnPayRemarks.Value.Replace("\n", "<br />");

                            pnlPayAmountTxt.Visible = true;
                            pnlPayRemarksText.Visible = true;
                            trPaymentRemarks.Visible = true;
                        }
                    }
                    else
                    {
                        HtmlTableRow trPaymentReject = (HtmlTableRow)e.Item.FindControl("trPaymentReject");
                        trPaymentReject.Visible = true;

                        Panel pnlPayRejectRemarksText = (Panel)e.Item.FindControl("pnlPayRejectRemarksText");
                        pnlPayRejectRemarksText.Visible = true;

                        Literal litPayRejectRemarks = (Literal)e.Item.FindControl("litPayRejectRemarks");
                        litPayRejectRemarks.Text = paymentRejectRemarks.Replace("\n", "<br />");
                    }
                }

                //@ delivery
                if (intStatus == clsAdmin.CONSTORDERSTATUSDELIVERY)
                {
                    HtmlTableRow trDeliveryCourier = (HtmlTableRow)e.Item.FindControl("trDeliveryCourier");
                    HtmlTableRow trDeliveryConsignment = (HtmlTableRow)e.Item.FindControl("trDeliveryConsignment");

                    if (status == intStatus - 1)
                    {
                        HtmlTableCell tdDeliveryCourier = (HtmlTableCell)e.Item.FindControl("tdDeliveryCourier");
                        tdDeliveryCourier.Attributes["class"] = "tdStatusLabelActive";

                        HtmlTableCell tdDeliveryCourierDetails = (HtmlTableCell)e.Item.FindControl("tdDeliveryCourierDetails");
                        tdDeliveryCourierDetails.Attributes["class"] = "tdStatusActive";

                        HtmlTableCell tdDeliveryConsignment = (HtmlTableCell)e.Item.FindControl("tdDeliveryConsignment");
                        tdDeliveryConsignment.Attributes["class"] = "tdStatusLabelActive";

                        HtmlTableCell tdDeliveryConsignmentDetails = (HtmlTableCell)e.Item.FindControl("tdDeliveryConsignmentDetails");
                        tdDeliveryConsignmentDetails.Attributes["class"] = "tdStatusActive";

                        //Panel pnlDeliveryCourier = (Panel)e.Item.FindControl("pnlDeliveryCourier");
                        Panel pnlDeliveryConsignment = (Panel)e.Item.FindControl("pnlDeliveryConsignment");
                        Panel pnlDeliveryCourierCompany = (Panel)e.Item.FindControl("pnlDeliveryCourierCompany");

                        //pnlDeliveryCourier.Visible = true;
                        pnlDeliveryConsignment.Visible = true;
                        pnlDeliveryCourierCompany.Visible = true;

                        trDeliveryCourier.Visible = true;
                        trDeliveryConsignment.Visible = true;

                        clsMis mis = new clsMis();
                        DataSet ds = new DataSet();
                        ds = mis.getListByListGrp("SHIPPING COMPANY", 1);

                        DataView dv = new DataView(ds.Tables[0]);
                        dv.Sort = "LIST_ORDER ASC";

                        DropDownList ddlDeliveryCourierCompany = (DropDownList)e.Item.FindControl("ddlDeliveryCourierCompany");
                        ddlDeliveryCourierCompany.DataSource = dv;
                        ddlDeliveryCourierCompany.DataTextField = "LIST_NAME";
                        ddlDeliveryCourierCompany.DataValueField = "LIST_VALUE";
                        ddlDeliveryCourierCompany.DataBind();

                        ListItem ddlDeliveryCourierCompanyDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                        ddlDeliveryCourierCompany.Items.Insert(0, ddlDeliveryCourierCompanyDefaultItem);
                    }
                    else //if (status >= intStatus)
                    {
                        Panel pnlDeliveryCourierText = (Panel)e.Item.FindControl("pnlDeliveryCourierText");
                        Literal litDeliveryCourier = (Literal)e.Item.FindControl("litDeliveryCourier");

                        Panel pnlDeliveryConsignmentText = (Panel)e.Item.FindControl("pnlDeliveryConsignmentText");
                        Literal litDeliveryConsignment = (Literal)e.Item.FindControl("litDeliveryConsignment");

                        litDeliveryCourier.Text = courier;
                        litDeliveryConsignment.Text = consignmentNo.Replace("\n", "<br />");

                        pnlDeliveryCourierText.Visible = true;
                        pnlDeliveryConsignmentText.Visible = true;

                        trDeliveryCourier.Visible = true;
                        trDeliveryConsignment.Visible = true;
                    }
                }

                //@ current status
                if (intStatus == status)
                {
                    ////@ for undo action
                    //if (intStatus != clsAdmin.CONSTORDERSTATUSNEW && intStatus != clsAdmin.CONSTORDERSTATUSDONE)
                    //{
                    //    HyperLink hypUndo = (HyperLink)e.Item.FindControl("hypUndo");
                    //    hypUndo.Text = GetGlobalResourceObject("GlobalResource", "contentAdmUndo.Text").ToString();
                    //    hypUndo.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/undoStatus.aspx?id=" + transId + "&" + clsAdmin.CONSTUSRARTICLETHICKBOX;

                    //    Panel pnlStatusAct = (Panel)e.Item.FindControl("pnlStatusAct");
                    //    pnlStatusAct.Visible = true;
                    //}
                    //// end of undo status

                    lnkbtnStatus.CssClass = "btnStatusSel";
                    lnkbtnStatus.OnClientClick = "javascript:return false;";
                    intCurStatus = int.Parse(hdnStatus.Value);
                }
                
                //@ after current status
                if (intCurStatus > 0)
                {
                    if (intCurStatus != lastStatus)
                    {
                        if (intStatusOrder == intCurStatus + 1)
                        {
                            if (paymentReject <= 0)
                            {
                                HtmlTableCell tdStatusAct = (HtmlTableCell)e.Item.FindControl("tdStatusAct");
                                tdStatusAct.Attributes["class"] = "tdLabelGrey";

                                HtmlTableCell tdStatusDetails = (HtmlTableCell)e.Item.FindControl("tdStatusDetails");
                                tdStatusDetails.Attributes["class"] = "tdStatusActive";
                            }

                            if (intStatusOrder != clsAdmin.CONSTORDERSTATUSPAY) { lnkbtnStatus.CssClass = "btnConfirm"; }
                            else
                            {
                                lnkbtnStatus.Text = "Pay:";
                                lnkbtnStatus.ToolTip = "Pay";
                                lnkbtnStatus.OnClientClick = "javascript:return false;";
                            }

                            litStatus.Text = "";
                        }
                        else if (intStatusOrder > intCurStatus + 1) { lnkbtnStatus.Enabled = false; }
                    }
                    else
                    {
                        //@ for sales type = "uploaded" use, there are two status for final stage
                        if (intStatus != status) { lnkbtnStatus.Visible = false; }

                        lnkbtnStatus.OnClientClick = "javascript:return false;";
                    }
                }
                else { lnkbtnStatus.OnClientClick = "javascript:return false;"; }   //@ before current status
            //}
        }
    }

    protected void rptStatus_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (Page.IsValid)
        {
            
            int intRecordAffected = 0;
            Boolean boolEdited = false;
            Boolean boolSuccess = true;

            if (e.CommandName == "cmdStatus")
            {
                int intStatus = Convert.ToInt16(e.CommandArgument);
                switch (intStatus)
                {
                    case clsAdmin.CONSTORDERSTATUSPAY:
                        TextBox txtPayRemarks = (TextBox)e.Item.FindControl("txtPayRemarks");
                        TextBox txtPayAmount = (TextBox)e.Item.FindControl("txtPayAmount");

                        if (!string.IsNullOrEmpty(txtPayRemarks.Text.Trim()))
                        {
                            decimal decPayAmount = Convert.ToDecimal("0.00");
                            
                            if (!string.IsNullOrEmpty(txtPayAmount.Text.Trim()))
                            {
                                decimal decTryParse = Convert.ToDecimal("0.00");
                                if (decimal.TryParse(txtPayAmount.Text.Trim(), out decTryParse)) { decPayAmount = decTryParse; }
                            }
                            
                            if (decPayAmount >= decTotal)
                            {
                                intRecordAffected = ord.addOrderPayment(transId, 2, decPayAmount, txtPayRemarks.Text.Trim());

                                if (intRecordAffected == 1)
                                {
                                    ord.setOrderPayment();
                                    boolEdited = true;
                                }
                                else { boolSuccess = false; }

                                if (boolSuccess)
                                {
                                    intRecordAffected = clsMis.performUpdateOrderStatus(transId, intStatus, true);

                                    if (intRecordAffected == 1)
                                    {
                                        boolEdited = true;

                                        if (ord.extractOrderById(transId))
                                        {
                                            clsConfig config = new clsConfig();
                                            //string strCurrency = config.currency;
                                            string strCurrency = "";

                                            if (ord.extractConvertionRateUnitById(transId))
                                            {
                                                if (!string.IsNullOrEmpty(ord.ordConvUnit))
                                                {
                                                    strCurrency = ord.ordConvUnit;
                                                }
                                                else
                                                {
                                                    strCurrency = config.currency;
                                                }
                                            }

                                            string strOrdNo = ord.ordNo;
                                            decimal decOrdTotalPrice = ord.ordTotal;
                                            string strName = ord.memName;
                                            string strEmail = ord.memEmail;

                                            if (string.IsNullOrEmpty(strName))
                                            {
                                                string[] strEmailSplit = strEmail.Split((char)'@');
                                                strName = strEmailSplit[0];
                                            }

                                            string strPersonDetails = "";

                                            strPersonDetails = "Name: " + strName + "<br />Email: " + strEmail;

                                            string strPaymentDetails = "Payment Type: " + ord.ordPayGateway + "<br />Remarks: " + ord.ordPayRemarks + "<br />Paid Amount: " + strCurrency + " " + ord.ordPayTotal + "<br />" + strPersonDetails;

                                            string strBillingDetails = "Contact Person: " + ord.name + "<br />Address: " + ord.add + "<br />Postal Code: " + ord.poscode + "<br />Country: " + ord.countryName + (!string.IsNullOrEmpty(ord.state) ? "<br />State: " + ord.state : "") + (!string.IsNullOrEmpty(ord.city) ? "<br />City: " + ord.city : "") + "<br />Contact No. " + ord.contactNo;

                                            string strDeliveryDetails = "Contact Person: " + ord.shippingName + "<br />Address: " + ord.shippingAdd + "<br />Postal Code: " + ord.shippingPoscode + "<br />Country: " + ord.shippingCountryName + (!string.IsNullOrEmpty(ord.shippingState) ? "<br />State: " + ord.shippingState : "") + (!string.IsNullOrEmpty(ord.shippingCity) ? "<br />City: " + ord.shippingCity : "") + "<br />Contact No. " + ord.shippingContactNo;
                                            
                                            EmailDetails(strCurrency, ord.ordGST, ord.ordTotalGST);

                                            sendPaymentDetails(strName, strEmail, transId, strOrdNo, ord.ordTotalItem, decOrdTotalPrice, strBillingDetails, strDeliveryDetails, strItems, strPaymentDetails, strCurrency);
                                        }
                                    }
                                    else { boolSuccess = false; }
                                }
                            }
                            else
                            {
                                Session["ERRMSG"] = "<div class=\"errmsg\">Amount paid is less than transaction amount.</div>";
                                Response.Redirect(Request.Url.ToString());
                            }
                        }
                        break;
                    case clsAdmin.CONSTORDERSTATUSPROCESSING:
                        intRecordAffected = clsMis.performUpdateOrderStatus(transId, intStatus, true);

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;

                            ord.extractOrderById(transId);
                            string strEmail = ord.memEmail;
                            string strName = ord.memName;

                            if (string.IsNullOrEmpty(strName))
                            {
                                string[] strEmailSplit = strEmail.Split((char)'@');
                                strName = strEmailSplit[0];
                            }

                            clsConfig config = new clsConfig();
                            string strCurrency = "";

                            if (ord.extractConvertionRateUnitById(transId))
                            {
                                if (!string.IsNullOrEmpty(ord.ordConvUnit))
                                {
                                    strCurrency = ord.ordConvUnit;
                                }
                                else
                                {
                                    strCurrency = config.currency;
                                }
                            }

                            string strBillingDetails = "Contact Person: " + ord.name + "<br />Address: " + ord.add + "<br />Postal Code: " + ord.poscode + "<br />Country: " + ord.countryName + (!string.IsNullOrEmpty(ord.state) ? "<br />State: " + ord.state : "") + (!string.IsNullOrEmpty(ord.city) ? "<br />City: " + ord.city : "") + "<br />Contact No. " + ord.contactNo;

                            string strDeliveryDetails = "Contact Person: " + ord.shippingName + "<br />Address: " + ord.shippingAdd + "<br />Postal Code: " + ord.shippingPoscode + "<br />Country: " + ord.shippingCountryName + (!string.IsNullOrEmpty(ord.shippingState) ? "<br />State: " + ord.shippingState : "") + (!string.IsNullOrEmpty(ord.shippingCity) ? "<br />City: " + ord.shippingCity : "") + "<br />Contact No. " + ord.shippingContactNo;

                            EmailDetails(strCurrency, ord.ordGST, ord.ordTotalGST);

                            sendOrdProcessingDetail(strEmail, strName, ord.ordNo, ord.ordTotalItem, ord.ordTotal, strBillingDetails, strDeliveryDetails, strItems,  strCurrency);

                        }
                        else { boolSuccess = false; }
                        break;
                    case clsAdmin.CONSTORDERSTATUSDELIVERY:
                        intRecordAffected = clsMis.performUpdateOrderStatus(transId, intStatus, true);

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;

                            //TextBox txtDeliveryCourier = (TextBox)e.Item.FindControl("txtDeliveryCourier");
                            TextBox txtDeliveryConsignment = (TextBox)e.Item.FindControl("txtDeliveryConsignment");
                            DropDownList ddlDeliveryCourierCompany = (DropDownList)e.Item.FindControl("ddlDeliveryCourierCompany");

                            //string strCourier = txtDeliveryCourier.Text.Trim();
                            string strConsignment = txtDeliveryConsignment.Text.Trim();
                            int intShippingCompany = !string.IsNullOrEmpty(ddlDeliveryCourierCompany.SelectedValue) ? Convert.ToInt16(ddlDeliveryCourierCompany.SelectedValue) : 0;
                            string strCourier = ddlDeliveryCourierCompany.SelectedItem.Text;

                            intRecordAffected = ord.updateDeliveryDetails(transId, intShippingCompany, strCourier, strConsignment);

                            if (intRecordAffected == 1)
                            {
                                ord.extractOrderById(transId);
                                string strEmail = ord.memEmail;
                                string strName = ord.memName;

                                if (string.IsNullOrEmpty(strName))
                                {
                                    string[] strEmailSplit = strEmail.Split((char)'@');
                                    strName = strEmailSplit[0];
                                }

                                clsConfig config = new clsConfig();
                                string strCurrency = "";

                                if (ord.extractConvertionRateUnitById(transId))
                                {
                                    if (!string.IsNullOrEmpty(ord.ordConvUnit))
                                    {
                                        strCurrency = ord.ordConvUnit;
                                    }
                                    else
                                    {
                                        strCurrency = config.currency;
                                    }
                                }

                                string strBillingDetails = "Contact Person: " + ord.name + "<br />Address: " + ord.add + "<br />Postal Code: " + ord.poscode + "<br />Country: " + ord.countryName + (!string.IsNullOrEmpty(ord.state) ? "<br />State: " + ord.state : "") + (!string.IsNullOrEmpty(ord.city) ? "<br />City: " + ord.city : "") + "<br />Contact No. " + ord.contactNo;

                                string strDeliveryDetails = "Contact Person: " + ord.shippingName + "<br />Address: " + ord.shippingAdd + "<br />Postal Code: " + ord.shippingPoscode + "<br />Country: " + ord.shippingCountryName + (!string.IsNullOrEmpty(ord.shippingState) ? "<br />State: " + ord.shippingState : "") + (!string.IsNullOrEmpty(ord.shippingCity) ? "<br />City: " + ord.shippingCity : "") + "<br />Contact No. " + ord.shippingContactNo;

                                EmailDetails(strCurrency, ord.ordGST, ord.ordTotalGST);

                                sendOrdDeliveryDetail(strEmail, strName, ord.ordNo, ord.ordTotalItem, ord.ordTotal, strBillingDetails, strDeliveryDetails, strItems, ord.shippingCompanyName, strConsignment, strCurrency);

                                if (!string.IsNullOrEmpty(ord.contactNo))
                                {
                                    clsSms sms = new clsSms();
                                    if (sms.extractSmsFlagById(clsAdmin.CONSTSMSORDERSTATUS, clsAdmin.CONSTSMSORDERSTATUSDELIVERY, 1))
                                    {
                                        sendSMS(ord.memName, ord.ordNo, ord.shippingCompanyName, ord.shippingTrackCode, ord.contactNo);
                                    }
                                }
                            }
                            else { boolSuccess = false; }
                        }
                        else { boolSuccess = false; }
                        break;
                    case clsAdmin.CONSTORDERSTATUSDONE:
                        intRecordAffected = clsMis.performUpdateOrderStatus(transId, intStatus, true);

                        if (intRecordAffected == 1)
                        {
                            ord.extractOrderById(transId);
                            string strEmail = ord.memEmail;
                            string strName = ord.memName;

                            clsConfig config = new clsConfig();
                            string strCurrency = "";

                            if (ord.extractConvertionRateUnitById(transId))
                            {
                                if (!string.IsNullOrEmpty(ord.ordConvUnit))
                                {
                                    strCurrency = ord.ordConvUnit;
                                }
                                else
                                {
                                    strCurrency = config.currency;
                                }
                            }

                            ord.setDone(transId);

                            string strBillingDetails = "Contact Person: " + ord.name + "<br />Address: " + ord.add + "<br />Postal Code: " + ord.poscode + "<br />Country: " + ord.countryName + (!string.IsNullOrEmpty(ord.state) ? "<br />State: " + ord.state : "") + (!string.IsNullOrEmpty(ord.city) ? "<br />City: " + ord.city : "") + "<br />Contact No. " + ord.contactNo;

                            string strDeliveryDetails = "Contact Person: " + ord.shippingName + "<br />Address: " + ord.shippingAdd + "<br />Postal Code: " + ord.shippingPoscode + "<br />Country: " + ord.shippingCountryName + (!string.IsNullOrEmpty(ord.shippingState) ? "<br />State: " + ord.shippingState : "") + (!string.IsNullOrEmpty(ord.shippingCity) ? "<br />City: " + ord.shippingCity : "") + "<br />Contact No. " + ord.shippingContactNo;

                            EmailDetails(strCurrency, ord.ordGST, ord.ordTotalGST);

                            sendOrdDoneDetail(strEmail, strName, ord.ordNo, ord.ordTotalItem, strBillingDetails, strDeliveryDetails, strItems, strCurrency);

                            boolEdited = true;
                        }
                        else { boolSuccess = false; }
                        break;
                }
            }
            else if (e.CommandName == "cmdReject")
            {
                TextBox txtPayRejectRemarks = (TextBox)e.Item.FindControl("txtPayRejectRemarks");

                intRecordAffected = ord.updatePaymentReject(transId, paymentId, txtPayRejectRemarks.Text.Trim());

                if (intRecordAffected == 1)
                {
                    boolEdited = true;

                    ord.extractOrderById(transId);

                    string strEmail = ord.memEmail;
                    string strName = ord.memName;

                    if (string.IsNullOrEmpty(strName))
                    {
                        string[] strEmailSplit = strEmail.Split((char)'@');
                        strName = strEmailSplit[0];
                    }

                    clsConfig config = new clsConfig();
                    string strCurrency = "";

                    if (ord.extractConvertionRateUnitById(transId))
                    {
                        if (!string.IsNullOrEmpty(ord.ordConvUnit))
                        {
                            strCurrency = ord.ordConvUnit;
                        }
                        else
                        {
                            strCurrency = config.currency;
                        }
                    }

                    string strBillingDetails = "Contact Person: " + ord.name + "<br />Address: " + ord.add + "<br />Postal Code: " + ord.poscode + "<br />Country: " + ord.countryName + (!string.IsNullOrEmpty(ord.state) ? "<br />State: " + ord.state : "") + (!string.IsNullOrEmpty(ord.city) ? "<br />City: " + ord.city : "") + "<br />Contact No. " + ord.contactNo;

                    string strDeliveryDetails = "Contact Person: " + ord.shippingName + "<br />Address: " + ord.shippingAdd + "<br />Postal Code: " + ord.shippingPoscode + "<br />Country: " + ord.shippingCountryName + (!string.IsNullOrEmpty(ord.shippingState) ? "<br />State: " + ord.shippingState : "") + (!string.IsNullOrEmpty(ord.shippingCity) ? "<br />City: " + ord.shippingCity : "") + "<br />Contact No. " + ord.shippingContactNo;

                    EmailDetails(strCurrency, ord.ordGST, ord.ordTotalGST);

                    sendPaymentRejectDetail(ord.memEmail, strName, ord.ordNo, ord.ordTotalItem, ord.ordTotal, strBillingDetails, strDeliveryDetails, strItems, ord.ordPayRejectRemarks, strCurrency);
                }
                else { boolSuccess = false; }
            }

            if (boolEdited) { Session["EDITEDORDID"] = transId; }
            else
            {
                if (!boolSuccess)
                {
                    ord.extractOrderById(transId);

                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text").ToString() + "<br />Failed to edit order (" + ord.ordNo + ").Please try again.</div>";
                }
                else
                {
                    Session["EDITEDORDID"] = transId;
                    Session["NOCHANGE"] = 1;
                }
            }

            Response.Redirect(Request.Url.ToString());
        }
    }

    protected void lnkbtnSaveRemarks_Click(object sender, EventArgs e)
    {
        string strRemarks = txtRemarks.Text.Trim();

        
        int intRecordAffected = 0;
        Boolean boolEdited = false;
        Boolean boolSuccess = true;

        if (string.Compare(strRemarks, hdnRemarks.Value, false) != 0)
        {
            intRecordAffected = ord.updateOrderRemarks(transId, strRemarks);

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolEdited) { Session["EDITEDTRANSID"] = ord.ordId; }
        else
        {
            if (!boolSuccess)
            {
                ord.extractOrderById(transId);

                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text").ToString() + "<br />Failed to edit order (" + ord.ordNo + ").Please try again.</div>";
            }
            else
            {
                Session["EDITEDTRANSID"] = transId;
                Session["NOCHANGE"] = 1;
            }
        }

        Response.Redirect(Request.Url.ToString());
    }


    #endregion


    #region "Methods"
    public void fill()
    {

        if (!IsPostBack)
        {
            setPageProperties();
            bindRptData();
        }

    }

    protected void setPageProperties()
    {
        lnkbtnBackToList.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";

        if (transId > 0)
        {
            //lnkbtnPrint.OnClientClick = "javascript:window.open('admPrintOrder.aspx?id=" + transId + "','_blank','toolbar=false,menubar=false,scrollbars,width=900px','false'); return false";
            lnkbtnPrint.Attributes["href"] = ConfigurationManager.AppSettings["scriptBase"] + "adm/admPrintOrder.aspx?id=" + transId + "&" + clsAdmin.CONSTUSRSALEORDERTHICKBOX;
            hypCancel.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admCancelTrans.aspx?id=" + transId + "&" + clsAdmin.CONSTUSRARTICLETHICKBOX;
            hypDelete.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admDeleteTrans.aspx?id=" + transId + "&" + clsAdmin.CONSTUSRARTICLETHICKBOX;

            fillForm();


                    ucAdmOrderItems.decShipping = decShipping;
                    ucAdmOrderItems.cancel = cancel;
                    ucAdmOrderItems.status = status;
                    ucAdmOrderItems.transId = transId;
                    ucAdmOrderItems.type = type;
                    ucAdmOrderItems.ord = ord;
                    ucAdmOrderItems.fill();
   
                    ucAdmOrderDetails.transId = transId;
                    ucAdmOrderDetails.status = status;
                    ucAdmOrderDetails.cancel = cancel;
                    ucAdmOrderDetails.fill();
             
         
                    ucAdmOrderPayment.transId = transId;
                    ucAdmOrderPayment.status = status;
                    ucAdmOrderPayment.cancel = cancel;
                    ucAdmOrderPayment.ord = ord;
                    ucAdmOrderPayment.fill();
            
                    pnlItems.Visible = true;
                    pnlDetails.Visible = true;
                    pnlPayment.Visible = true;
                
            
        }
        else
        {
            lnkbtnSaveRemarks.CssClass = "btnDisabled";
            lnkbtnPrint.CssClass = "btn2 btnPrint btnDisabled";
            hypCancel.CssClass = "btnDisabled";

            lnkbtnSaveRemarks.Enabled = false;
            lnkbtnPrint.Enabled = false;
            hypCancel.Enabled = false;
            pnlStatus.Visible = false;
        }

    }

    protected void fillForm()
    {
        
        clsConfig config = new clsConfig();

        if (ord.extractOrderByIdFromTB(transId))
        {
            string strCurrency = "";

            if (ord.extractConvertionRateUnitById(transId))
            {
                if (!string.IsNullOrEmpty(ord.ordConvUnit))
                {
                    strCurrency = ord.ordConvUnit;
                }
                else
                {
                    strCurrency = config.currency;
                }
            }

            if (ord.ordRead == 0)
            {
                ord.updateOrderRead(transId);
            }

            decShipping = ord.ordShipping;
            cancel = ord.ordCancel;
            status = ord.ordStatus;
            type = ord.ordType;
            paymentReject = ord.ordPayReject;
            paymentRejectRemarks = ord.ordPayRejectRemarks;
            paymentId = ord.ordPaymentId;
            // amount to be paid
            decTotal = ord.ordTotal;

            //sales detail
            if (type == 1)
            {
                if (ord.memId > 0)
                {
                    litMember.Text = "<a title='" + ord.memEmail + "'>" + ord.memEmail + "</a>";
                }
            }
            else
            {
                litMember.Text = "N/A";
            }

            litOrdDate.Text = ord.ordCreation.ToString("dd MMM yyyy");

            litOrdAmount.Text = strCurrency + " " + clsMis.formatPrice(ord.ordTotal);
            litMsgBuyer.Text = !string.IsNullOrEmpty(ord.ordMsgBuyer) ? ord.ordMsgBuyer.Replace("\n", "<br />") : "-";
            txtRemarks.Text = ord.ordRemarks;
            hdnRemarks.Value = ord.ordRemarks;

            if (ord.ordConvRate > 0)
            {
                trConvRate.Visible = true;
                litConvRate.Text = ord.ordConvRate.ToString();
            }

            //sales is cancelled
            if (ord.ordCancel == 1)
            {
                litOrdNo.Text = ord.ordNo + " (Cancel)";

                lnkbtnSaveRemarks.Visible = false;
                hypCancel.Visible = false;
            }
            else
            {
                //status is set
                if (status != 0)
                {
                    litOrdNo.Text = ord.ordNo + " (" + ord.ordStatusName + ")";
                }

                if (ord.ordDone == 1)
                {
                    lnkbtnSaveRemarks.Visible = false;
                    hypCancel.Visible = false;
                }
            }
            lnkbtnPrint.Attributes["title"] = "";
            lnkbtnPrint.Attributes["name"] = "Sales Order - " + ord.ordNo + "";
            lnkbtnPrint.Attributes["name"] += "<div id='TB_ajaxWindowAction'><button class='btn' id='btnPrint' onclick='print();'>Print</button></div>";
            //payment detail for status
            hdnPayGateway.Value = ord.ordPayGateway;
            hdnPayRemarks.Value = ord.ordPayRemarks;
            hdnPayAmount.Value = ord.ordPayTotal.ToString();
            courierId = ord.shippingCompany;
            courier = ord.shippingCompanyName;
            consignmentNo = ord.shippingTrackCode;

            if (ord.ordPayment.ToShortDateString() != DateTime.MaxValue.ToShortDateString()) { hdnPayDate.Value = ord.ordPayment.ToString("dd MMM yyyy"); }
            else { hdnPayDate.Value = ""; }

            hypCancel.ToolTip += " - " + ord.ordNo + " - " + ord.memEmail + " - " + strCurrency + " " + ord.ordTotal;
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";

            Response.Redirect(currentPageName);
        }

        
    }

    protected void bindRptData()
    {

        
        DataSet ds = new DataSet();
        DataView dv;

        ds = ord.getOrderStatus(transId);

        DataRow[] foundRow = ds.Tables[0].Select("ORDERSTATUS_ID > 0", "ORDERSTATUS_ORDER DESC");
        if (foundRow.Length > 0) { lastStatus = Convert.ToInt16(foundRow[0]["ORDERSTATUS_ID"]); }

        dv = new DataView(ds.Tables[0]);

        dv.RowFilter = "ORDERSTATUS_ID > 0 AND ORDERSTATUS_ACTIVE = 1 AND ORDERSTATUS_PARENTVALUE = " + type;
        dv.Sort = "ORDERSTATUS_ORDER ASC";

        PagedDataSource pdsStatus = new PagedDataSource();
        pdsStatus.DataSource = dv;
        intCurStatus = -1;
        rptStatus.DataSource = pdsStatus;
        rptStatus.DataBind();

    }

    protected void EmailDetails(string strCurrency , int intordGST, decimal decTotalGST)
    {
        
        clsConfig config = new clsConfig();
        //Order Item Details
        DataSet ds = new DataSet();
        ds = ord.getOrderItemsByOrderId(transId);

        strItems = "";

        strItems += "<table cellpadding=\"0\" cellspacing=\"0\" style=\"width:500px; color:#4a4a4a; border:solid 1px #000000;\"\">";
        strItems += "<tr>";
        strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:center; width:150px; padding-top:10px; padding-bottom:10px; font-weight:bold; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">Item</td>";
        strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:center; width:123px; padding-top:10px; padding-bottom:10px; font-weight:bold; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">Price</td>";
        strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:center; width:123px; padding-top:10px; padding-bottom:10px; font-weight:bold; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">Quantity</td>";
        strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:center; width:123px; padding-top:10px; padding-bottom:10px; font-weight:bold; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">Item Total</td>";
        strItems += "</tr>";

        decimal decSubTotal = Convert.ToDecimal("0.00");
        decimal decTotalGSTAmount = Convert.ToDecimal("0.00");

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            int intDetailId = Convert.ToInt16(row["ORDDETAIL_ID"]);
            int intItemId = Convert.ToInt16(row["ORDDETAIL_PRODID"]);
            decSubTotal += Convert.ToDecimal(row["ORDDETAIL_PRODTOTAL"]);
            decimal decProdPrice = Convert.ToDecimal(row["ORDDETAIL_PRODPRICE"]);
            decimal decProdPricing = Convert.ToDecimal(row["ORDDETAIL_PRODPRICING"]);
            int intQuantity = Convert.ToInt32(row["ORDDETAIL_PRODQTY"]);

            strItems += "<tr>";
            strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:left; width:150px; padding-left:10px; padding-top:10px; padding-bottom:10px; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + row["ORDDETAIL_PRODCODE"].ToString() + "<br />" + row["ORDDETAIL_PRODDNAME"].ToString() + "</td>";
            strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:center; width:123px; padding-right:10px; padding-top:10px; padding-bottom:10px; vertical-align:top; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strCurrency + " " + row["ORDDETAIL_PRODPRICE"].ToString() + "</td>";
            strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:center; width:123px; padding-top:10px; padding-bottom:10px; vertical-align:top; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + row["ORDDETAIL_PRODQTY"].ToString() + "</td>";
            strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:right; width:113px; padding-right:10px; padding-top:10px; padding-bottom:10px; vertical-align:top; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strCurrency + " " + row["ORDDETAIL_PRODTOTAL"].ToString() + "</td>";
            strItems += "</tr>";
        }

        
            strItems += "<tr>";
            strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\" colspan=\"3\">Subtotal</td>";
            strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strCurrency + " " + clsMis.formatPrice(decSubTotal) + "</td>";
            strItems += "</tr>";

            if (decTotalGST > 0)
            {
                strItems += "<tr>";
                strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\" colspan=\"3\">Total GST " + intordGST + "%</td>";
                strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strCurrency + " " + decTotalGST + "</td>";
                strItems += "</tr>";
            }

            strItems += "<tr>";
            strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\" colspan=\"3\">Shipping</td>";
            strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strCurrency + " " + decShipping + "</td>";
            strItems += "</tr>";

            strItems += "<tr>";
            strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-weight:bold; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\" colspan=\"3\">Order Total</td>";
            strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-weight:bold; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strCurrency + " " + clsMis.formatPrice(decTotal) + "</td>";
            strItems += "</tr>";
            strItems += "</table>";
    }

    //Payment Received Email
    protected void sendPaymentDetails(string strUserName, string strUserEmail, int intId, string strOrdNo, int intItem,  decimal decOrdTotal, string strBillingDetails, string strDeliveryDetails, string strItems,  string strPaymentDetails, string strCurrency)
    {
        string strSubjectPrefix;
        string strSignature;
        string strSubject;
        string strBody;
        string strUserBody;
        string strSenderName;

        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYRECBTEMAILSUBJECT, clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT, 1);
        strSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYRECBTADMINEMAILCONTENT, clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT, 1);
        strBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYRECBTUSEREMAILCONTENT, clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;

        strBody = strBody.Replace("[NAME]", strUserName);
        strBody = strBody.Replace("[ORDNO]", strOrdNo);
        strBody = strBody.Replace("[ORDITEM]", intItem.ToString());
        strBody = strBody.Replace("[CUR]", strCurrency);
        strBody = strBody.Replace("[ORDTOTAL]", decOrdTotal.ToString());
        strBody = strBody.Replace("[BILLINGDETAILS]", strBillingDetails);
        strBody = strBody.Replace("[DELIVERYDETAILS]", strDeliveryDetails);
        strBody = strBody.Replace("[ORDERDETAILS]", strItems);
        strBody = strBody.Replace("[PAYDETAILS]", strPaymentDetails);
        strBody = strBody.Replace("[SIGNATURE]", strSignature);

        strUserBody = strUserBody.Replace("[NAME]", strUserName);
        strUserBody = strUserBody.Replace("[ORDNO]", strOrdNo);
        strUserBody = strUserBody.Replace("[ORDITEM]", intItem.ToString());
        strUserBody = strUserBody.Replace("[CUR]", strCurrency);
        strUserBody = strUserBody.Replace("[ORDTOTAL]", decOrdTotal.ToString());
        strUserBody = strUserBody.Replace("[BILLINGDETAILS]", strBillingDetails);
        strUserBody = strUserBody.Replace("[DELIVERYDETAILS]", strDeliveryDetails);
        strUserBody = strUserBody.Replace("[ORDERDETAILS]", strItems);
        strUserBody = strUserBody.Replace("[PAYDETAILS]", strPaymentDetails);
        strUserBody = strUserBody.Replace("[SIGNATURE]", strSignature);

        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strSubject = strSubjectPrefix + " " + strSubject; }

        strSubject = strSubject.Replace("ORDNO", strOrdNo);

        if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        {
            if (clsEmail.send(strSenderName, "", strUserEmail, "", "", "", strSubject, strUserBody, 1))
            {

            }
            else
            {
                clsLog.logErroMsg("Failed to send order details.");
            }
        }
        else
        {
            clsLog.logErroMsg("Failed to send order details.");
        }

        //strSubject = strSubjectPrefix + " Payment Received";
        //strBody = "Dear Admin, <br /><br />Payment from " + strUserName + " has been made.<br /><br />The order details as below:<br />";
        //strBody += "OrderID: " + strOrdNo + "<br />";
        //strBody += "Order Total: " + strCurrency + " " + decOrdTotal + "<br />";

        //strBody += "<br />Here is the payment details:<br />";
        //strBody += strPaymentDetails;
        //strBody += "<br /><br />Thank you.<br /><br />";
        //strBody += strSignature;
        //strBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strBody + "</font>";
        ////strBody = strBody.Replace(Environment.NewLine, "<br />");

        //strUserSubject = strSubjectPrefix + " Payment Received";
        //strUserBody = "Dear " + strUserName + ",";
        //strUserBody += "<br /><br /><b>Your order " + strOrdNo + " history</b>";
        //strUserBody += "<br /><br />Thank you for your payment.";
        //strUserBody += "<br /><br />Your payment has been verified and we will proceed to pack and deliver your package within the next 2 working days.";
        //strUserBody += "<br /><br />Another email will be sent to you once your order is being processed.";
        ////strUserBody += "<br /><br />You can review and download your invoice from the <a href='" + strOrderStatusUrl + "'>Check Order Status</a> of your account on our Website.";
        //strUserBody += "<br /><br />Thank you.<br /><br />";
        //strUserBody += strSignature;
        //strUserBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strUserBody + "</font>";
        ////strUserBody = strUserBody.Replace(Environment.NewLine, "<br />");

        ////Response.Write(strBody + "<br /><br />" + strUserBody);

        //if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        //{
        //    if (clsEmail.send(strSenderName, "", strUserEmail, "", "", "", strUserSubject, strUserBody, 1))
        //    {

        //    }
        //    else
        //    {
        //        clsLog.logErroMsg("Failed to send order details.");
        //    }
        //}
        //else
        //{
        //    clsLog.logErroMsg("Failed to send order details.");
        //}
    }

    //Payment Reject Email
    protected Boolean sendPaymentRejectDetail(string strEmail, string strName, string strNo, int intItem, decimal decTotal, string strBillingDetails, string strDeliveryDetails, string strItems, string strReject, string strCurrency)
    {
        Boolean boolSuccess = true;

        string strSubjectPrefix;
        string strSignature;
        string strSubject;
        string strBody;
        string strUserBody;
        string strSenderName;

        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYREJECTEMAILSUBJECT, clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT, 1);
        strSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYREJECTADMINEMAILCONTENT, clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT, 1);
        strBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYREJECTUSEREMAILCONTENT, clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;

        strBody = strBody.Replace("[NAME]", strName);
        strBody = strBody.Replace("[ORDNO]", strNo);
        strBody = strBody.Replace("[ORDITEM]", intItem.ToString());
        strBody = strBody.Replace("[CUR]", strCurrency);
        strBody = strBody.Replace("[ORDTOTAL]", decTotal.ToString());
        strBody = strBody.Replace("[BILLINGDETAILS]", strBillingDetails);
        strBody = strBody.Replace("[DELIVERYDETAILS]", strDeliveryDetails);
        strBody = strBody.Replace("[ORDERDETAILS]", strItems);
        strBody = strBody.Replace("[REJECTMSG]", strReject);
        strBody = strBody.Replace("[SIGNATURE]", strSignature);

        strUserBody = strUserBody.Replace("[NAME]", strName);
        strUserBody = strUserBody.Replace("[ORDNO]", strNo);
        strUserBody = strUserBody.Replace("[ORDITEM]", intItem.ToString());
        strUserBody = strUserBody.Replace("[CUR]", strCurrency);
        strUserBody = strUserBody.Replace("[ORDTOTAL]", decTotal.ToString());
        strUserBody = strUserBody.Replace("[BILLINGDETAILS]", strBillingDetails);
        strUserBody = strUserBody.Replace("[DELIVERYDETAILS]", strDeliveryDetails);
        strUserBody = strUserBody.Replace("[ORDERDETAILS]", strItems);
        strUserBody = strUserBody.Replace("[REJECTMSG]", strReject);
        strUserBody = strUserBody.Replace("[SIGNATURE]", strSignature);

        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strSubject = strSubjectPrefix + " " + strSubject; }

        strSubject = strSubject.Replace("ORDNO", strNo);

        if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        {
            if (clsEmail.send(strSenderName, "", strEmail, "", "", "", strSubject, strUserBody, 1))
            {
                boolSuccess = true;
            }
            else
            {
                boolSuccess = false;
            }
        }

        return boolSuccess;

        //Boolean boolSuccess = true;

        //string strSubject;
        //string strBody;

        //string strUserSubject;
        //string strUserBody;
        //string strUserEmail;
        //string strUserName;
        //string strSenderName = GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString();

        //clsConfig config = new clsConfig();
        ////string strCurrency = config.currency;
        //config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        //string strSubjectPrefix = config.value;// config.emailSubject;
        //string strSignature = "";// config.emailSignature;

        //strUserEmail = strEmail;
        //strUserName = strName;
        //strSubject = strSubjectPrefix + " Payment Reject";
        //strBody = "Dear Admin, <br /><br />Please note that there is an error in the transaction details for an order from " + strUserName + " .<br /><br />The order details as below:<br />";
        //strBody += "OrderID: " + strNo + "<br />";
        //strBody += "Order Total: " + strCurrency + " " + decTotal + "<br />";
        //strBody += "Reject Remarks: " + strReject + "<br />";
        //strBody += "<br />Thank you.<br /><br />";
        //strBody += strSignature;
        //strBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strBody + "</font>";
        ////strBody = strBody.Replace(Environment.NewLine, "<br />");

        //strUserSubject = strSubjectPrefix + " Payment Reject";
        //strUserBody = "Dear " + strUserName + ",";
        //strUserBody += "<br /><br /><b>Your order " + strNo + " history</b>";
        //strUserBody += "<br /><br />Please note that there is an error in the transaction details.";
        //strUserBody += "<br /><br />" + strReject;
        //strUserBody += "<br /><br />Please kindly make the amendments and update the form under the order details with the correct information.";
        ////strUserBody += "<br /><br />Here are the order details:<br />";
        ////strUserBody += "OrderID: " + strNo + "<br />";
        ////strUserBody += "Order Item: " + intItem + "<br />";
        ////strUserBody += "Order Total: " + strCurrency + " " + decTotal + "<br />";
        ////strUserBody += "Courier: " + strCourier + "<br />";
        ////strUserBody += "Consignment No.: " + strConsignment + "<br />";
        ////strUserBody += "<br /><br />In order to know the status of your order, please login into <a href='" + ConfigurationManager.AppSettings["fullBase"] + "' title='" + GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString() + "'>" + GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString() + "</a> and check under Profile > Outstanding Order.";
        //strUserBody += "<br /><br />Thank you.<br /><br />";
        //strUserBody += strSignature;
        //strUserBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strUserBody + "</font>";
        ////Response.Write(strBody + "<br />" + strUserBody);
        //if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        //{
        //    if (clsEmail.send(strSenderName, "", strUserEmail, "", "", "", strUserSubject, strUserBody, 1))
        //    {
        //        boolSuccess = true;
        //    }
        //    else
        //    {
        //        boolSuccess = false;
        //    }
        //}

        //return boolSuccess;
    }

    //Order Processing Email
    protected Boolean sendOrdProcessingDetail(string strEmail, string strName, string strNo, int intItem, decimal decTotal, string strBillingDetails, string strDeliveryDetails, string strItems, string strCurrency)
    {
        Boolean boolSuccess = true;

        string strSubjectPrefix;
        string strSignature;
        string strSubject;
        string strBody;
        string strUserBody;
        string strSenderName;

        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDPROEMAILSUBJECT, clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT, 1);
        strSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDPROADMINEMAILCONTENT, clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT, 1);
        strBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDPROUSEREMAILCONTENT, clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;

        strBody = strBody.Replace("[NAME]", strName);
        strBody = strBody.Replace("[ORDNO]", strNo);
        strBody = strBody.Replace("[ORDITEM]", intItem.ToString());
        strBody = strBody.Replace("[CUR]", strCurrency);
        strBody = strBody.Replace("[ORDTOTAL]", decTotal.ToString());
        strBody = strBody.Replace("[BILLINGDETAILS]", strBillingDetails);
        strBody = strBody.Replace("[DELIVERYDETAILS]", strDeliveryDetails);
        strBody = strBody.Replace("[ORDERDETAILS]", strItems);
        strBody = strBody.Replace("[SIGNATURE]", strSignature);

        strUserBody = strUserBody.Replace("[NAME]", strName);
        strUserBody = strUserBody.Replace("[ORDNO]", strNo);
        strUserBody = strUserBody.Replace("[ORDITEM]", intItem.ToString());
        strUserBody = strUserBody.Replace("[CUR]", strCurrency);
        strUserBody = strUserBody.Replace("[ORDTOTAL]", decTotal.ToString());
        strUserBody = strUserBody.Replace("[BILLINGDETAILS]", strBillingDetails);
        strUserBody = strUserBody.Replace("[DELIVERYDETAILS]", strDeliveryDetails);
        strUserBody = strUserBody.Replace("[ORDERDETAILS]", strItems);
        strUserBody = strUserBody.Replace("[SIGNATURE]", strSignature);

        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strSubject = strSubjectPrefix + " " + strSubject; }

        strSubject = strSubject.Replace("ORDNO", strNo);

        if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        {
            if (clsEmail.send(strSenderName, "", strEmail, "", "", "", strSubject, strUserBody, 1))
            {
                boolSuccess = true;
            }
            else
            {
                boolSuccess = false;
            }
        }

        return boolSuccess;

        //Boolean boolSuccess = true;

        //string strSubject;
        //string strBody;

        //string strUserSubject;
        //string strUserBody;
        //string strUserEmail;
        //string strUserName;
        //string strSenderName = GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString();

        //clsConfig config = new clsConfig();
        ////string strCurrency = config.currency;
        //config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        //string strSubjectPrefix = config.value;// config.emailSubject;
        //string strSignature = "";// config.emailSignature;

        //strUserEmail = strEmail;
        //strUserName = strName;

        //strSubject = strSubjectPrefix + " Processed";
        //strBody = "Dear Admin, <br /><br />An order from " + strUserName + " is being processed.<br /><br />The order details as below:<br />";
        //strBody += "OrderID: " + strNo + "<br />";
        //strBody += "Order Item: " + intItem + "<br />";
        //strBody += "Order Total: " + strCurrency + " " + decTotal + "<br />";
        //strBody += "<br />Thank you.<br /><br />";
        //strBody += strSignature;
        //strBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strBody + "</font>";
        ////strBody = strBody.Replace(Environment.NewLine, "<br />");

        //strUserEmail = strEmail;
        //strUserName = strName;
        //strUserSubject = strSubjectPrefix + " Processed";
        //strUserBody = "Dear " + strUserName + ",";
        //strUserBody += "<br /><br /><b>Your order " + strNo + " history</b>";
        //strUserBody += "<br /><br />Your order is being processed.";
        //strUserBody += "<br /><br />Another email will be sent to you once your package has been delivered.";
        //strUserBody += "<br /><br />Thank you.<br /><br />";
        //strUserBody += strSignature;
        //strUserBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strUserBody + "</font>";
        ////Response.Write(strBody + "<br />" + strUserBody);
        //if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        //{
        //    if (clsEmail.send(strSenderName, "", strUserEmail, "", "", "", strUserSubject, strUserBody, 1))
        //    {
        //        boolSuccess = true;
        //    }
        //    else
        //    {
        //        boolSuccess = false;
        //    }
        //}

        //return boolSuccess;
    }

    //Order Delivered Email
    protected Boolean sendOrdDeliveryDetail(string strEmail, string strName, string strNo, int intItem, decimal decTotal, string strBillingDetails, string strDeliveryDetails, string strItems, string strCourier, string strConsignment, string strCurrency)
    {
        Boolean boolSuccess = true;

        string strSubjectPrefix;
        string strSignature;
        string strSubject;
        string strBody;
        string strUserBody;
        string strSenderName;

        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDDELEMAILSUBJECT, clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT, 1);
        strSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDDELADMINEMAILCONTENT, clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT, 1);
        strBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDDELUSEREMAILCONTENT, clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;

        strBody = strBody.Replace("[NAME]", strName);
        strBody = strBody.Replace("[ORDNO]", strNo);
        strBody = strBody.Replace("[ORDITEM]", intItem.ToString());
        strBody = strBody.Replace("[CUR]", strCurrency);
        strBody = strBody.Replace("[ORDTOTAL]", decTotal.ToString());
        strBody = strBody.Replace("[BILLINGDETAILS]", strBillingDetails);
        strBody = strBody.Replace("[DELIVERYDETAILS]", strDeliveryDetails);
        strBody = strBody.Replace("[ORDERDETAILS]", strItems);
        strBody = strBody.Replace("[COURIER]", strCourier);
        strBody = strBody.Replace("[CONSIGNMENTNO]", strConsignment);
        strBody = strBody.Replace("[SIGNATURE]", strSignature);

        strUserBody = strUserBody.Replace("[NAME]", strName);
        strUserBody = strUserBody.Replace("[ORDNO]", strNo);
        strUserBody = strUserBody.Replace("[ORDITEM]", intItem.ToString());
        strUserBody = strUserBody.Replace("[CUR]", strCurrency);
        strUserBody = strUserBody.Replace("[ORDTOTAL]", decTotal.ToString());
        strUserBody = strUserBody.Replace("[BILLINGDETAILS]", strBillingDetails);
        strUserBody = strUserBody.Replace("[DELIVERYDETAILS]", strDeliveryDetails);
        strUserBody = strUserBody.Replace("[ORDERDETAILS]", strItems);
        strUserBody = strUserBody.Replace("[COURIER]", strCourier);
        strUserBody = strUserBody.Replace("[CONSIGNMENTNO]", strConsignment);
        strUserBody = strUserBody.Replace("[SIGNATURE]", strSignature);

        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strSubject = strSubjectPrefix + " " + strSubject; }

        strSubject = strSubject.Replace("ORDNO", strNo);

        if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        {
            if (clsEmail.send(strSenderName, "", strEmail, "", "", "", strSubject, strUserBody, 1))
            {
                boolSuccess = true;
            }
            else
            {
                boolSuccess = false;
            }
        }

        return boolSuccess;

        //Boolean boolSuccess = true;

        //string strSubject;
        //string strBody;

        //string strUserSubject;
        //string strUserBody;
        //string strUserEmail;
        //string strUserName;
        //string strSenderName = GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString();

        //clsConfig config = new clsConfig();
        ////string strCurrency = config.currency;
        //config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        //string strSubjectPrefix = config.value;// config.emailSubject;
        //string strSignature = "";// config.emailSignature;

        //strUserEmail = strEmail;
        //strUserName = strName;

        //strSubject = strSubjectPrefix + " Delivered";
        //strBody = "Dear Admin, <br /><br />An order from " + strUserName + " has been delivered.<br /><br />The order details as below:<br />";
        //strBody += "OrderID: " + strNo + "<br />";
        //strBody += "Order Item: " + intItem + "<br />";
        //strBody += "Order Total: " + strCurrency + " " + decTotal + "<br />";
        //strBody += "Shipping Company: " + strCourier + "<br />";
        //strBody += "Consignment No.: " + strConsignment + "<br />";
        //strBody += "<br />Thank you.<br /><br />";
        //strBody += strSignature;
        //strBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strBody + "</font>";
        ////strBody = strBody.Replace(Environment.NewLine, "<br />");

        //strUserEmail = strEmail;
        //strUserName = strName;
        //strUserSubject = strSubjectPrefix + " Delivered";
        //strUserBody = "Dear " + strUserName + ",";
        //strUserBody += "<br /><br /><b>Your order " + strNo + " history</b>";
        //strUserBody += "<br /><br />Your order has been processed for delivery.";
        //strUserBody += "<br /><br />Here are the shipping details:<br />";
        //strUserBody += "Shipping Company: " + strCourier + "<br />";
        //strUserBody += "Consignment No.: " + strConsignment;
        //strUserBody += "<br /><br />Please allow <b>3 - 4 working days</b> for your package to reach you.";
        ////strUserBody += "<br /><br />Here are the order details:<br />";
        ////strUserBody += "OrderID: " + strNo + "<br />";
        ////strUserBody += "Order Item: " + intItem + "<br />";
        ////strUserBody += "Order Total: " + strCurrency + " " + decTotal + "<br />";
        ////strUserBody += "Courier: " + strCourier + "<br />";
        ////strUserBody += "Consignment No.: " + strConsignment + "<br />";
        ////strUserBody += "<br /><br />In order to know the status of your order, please login into <a href='" + ConfigurationManager.AppSettings["fullBase"] + "' title='" + GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString() + "'>" + GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString() + "</a> and check under Profile > Outstanding Order.";
        //strUserBody += "<br /><br />Thank you.<br /><br />";
        //strUserBody += strSignature;
        //strUserBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strUserBody + "</font>";
        ////Response.Write(strBody + "<br /><br />" + strUserBody);
        //if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        //{
        //    if (clsEmail.send(strSenderName, "", strUserEmail, "", "", "", strUserSubject, strUserBody, 1))
        //    {
        //        boolSuccess = true;
        //    }
        //    else
        //    {
        //        boolSuccess = false;
        //    }
        //}

        //return boolSuccess;
    }

    //Order Done Email
    protected Boolean sendOrdDoneDetail(string strEmail, string strName, string strNo, int intItem, string strBillingDetails, string strDeliveryDetails, string strItems, string strCurrency)
    {
        Boolean boolSuccess = true;

        string strSubjectPrefix;
        string strSignature;
        string strSubject;
        string strBody;
        string strUserBody;
        string strSenderName;

        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDDONEEMAILSUBJECT, clsConfig.CONSTGROUPORDDONEEMAILCONTENT, 1);
        strSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDDONEADMINEMAILCONTENT, clsConfig.CONSTGROUPORDDONEEMAILCONTENT, 1);
        strBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDDONEUSEREMAILCONTENT, clsConfig.CONSTGROUPORDDONEEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;

        strBody = strBody.Replace("[NAME]", strName);
        strBody = strBody.Replace("[ORDNO]", strNo);
        strBody = strBody.Replace("[ORDITEM]", intItem.ToString());
        strBody = strBody.Replace("[CUR]", strCurrency);
        strBody = strBody.Replace("[ORDTOTAL]", decTotal.ToString());
        strBody = strBody.Replace("[BILLINGDETAILS]", strBillingDetails);
        strBody = strBody.Replace("[DELIVERYDETAILS]", strDeliveryDetails);
        strBody = strBody.Replace("[ORDERDETAILS]", strItems);
        strBody = strBody.Replace("[SIGNATURE]", strSignature);

        strUserBody = strUserBody.Replace("[NAME]", strName);
        strUserBody = strUserBody.Replace("[ORDNO]", strNo);
        strUserBody = strUserBody.Replace("[ORDITEM]", intItem.ToString());
        strUserBody = strUserBody.Replace("[CUR]", strCurrency);
        strUserBody = strUserBody.Replace("[ORDTOTAL]", decTotal.ToString());
        strUserBody = strUserBody.Replace("[BILLINGDETAILS]", strBillingDetails);
        strUserBody = strUserBody.Replace("[DELIVERYDETAILS]", strDeliveryDetails);
        strUserBody = strUserBody.Replace("[ORDERDETAILS]", strItems);
        strUserBody = strUserBody.Replace("[SIGNATURE]", strSignature);

        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strSubject = strSubjectPrefix + " " + strSubject; }

        strSubject = strSubject.Replace("ORDNO", strNo);

        if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        {
            if (clsEmail.send(strSenderName, "", strEmail, "", "", "", strSubject, strUserBody, 1))
            {
                boolSuccess = true;
            }
            else
            {
                boolSuccess = false;
            }
        }

        return boolSuccess;
    }

    protected void sendSMS(string strMemName, string strOrdNo, string strShippingCompany, string strConsignmentNo, string strContactNo)
    {
        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        string defaultSenderName = config.value;

        string strMessage;

        if (!string.IsNullOrEmpty(strContactNo))
        {
            strContactNo = strContactNo.Replace("-", "");
            strContactNo = strContactNo.Replace(" ", "");

            strMessage = "Your order(" + strOrdNo + ") has been delivered to your Shipping Address, ";
            strMessage += "thru " + strShippingCompany + ", consignment no. is " + strConsignmentNo + ". ";
            strMessage += "From " + defaultSenderName + ". ";

            string strResult = clsSms.sendSMS(strContactNo, strMessage);
        }
    }
    #endregion
}
