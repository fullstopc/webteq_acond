﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;
using System.Linq;

public partial class ctrl_ucAdmGrouping : System.Web.UI.UserControl
{
    #region "Properties"
    protected clsGroup grouping = new clsGroup();

    protected bool boolSuccess = false;
    protected bool boolEdited = false;
    protected int intRecordAffected = 0;
    protected int intGrpingId = 0;
    protected int intGrpingCount = 0;
    protected int intGrpingTotal = 0;
     
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setPageProperties();
            fill();
        }
    }
    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            boolEdited = false;
            boolSuccess = true;
            intRecordAffected = 0;

            
            if (!boolSuccess)
            {
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to update Payment gateway. Please try again.</div>";
                Response.Redirect(currentPageName);
            }
            else
            {
                if (boolEdited)
                {
                    Session["EDITED_PAYMENTGATEWAY"] = 1;
                }
                else
                {
                    Session["EDITED_PAYMENTGATEWAY"] = 1;
                    Session["NOCHANGE"] = 1;
                }

                Response.Redirect(currentPageName);
            }
        }
    }

    protected void lnkbtnSaveGrouping_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int intRecordAffected = 0;
            bool boolSuccess = true;
            bool boolEdited = false;

            string strGrpingName = txtGroupingName.Text.Trim();
            int intGrpingActive = chkboxActive.Checked ? 1 : 0;

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;
                if (int.TryParse(Request["id"], out intTryParse))
                {
                    intGrpingId = intTryParse;
                }
            }

            if (!grouping.isGrpingExist(strGrpingName, intGrpingId))
            {
                intRecordAffected = grouping.addGrouping(strGrpingName, clsAdmin.CONSTLISTGROUPINGGROUP, intGrpingActive);

                if (intRecordAffected > 0) { boolEdited = true; }
                else { boolSuccess = false; }

                if (!boolSuccess)
                {
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add grouping. Please try again.</div>";

                    Response.Redirect(currentPageName);
                }
                else
                {
                    if (boolEdited)
                    {
                        Session["EDITED_GROUPING"] = 1;
                    }
                    else
                    {
                        Session["EDITED_GROUPING"] = 1;
                        Session["NOCHANGE"] = 1;
                    }

                    Response.Redirect(currentPageName);
                }
            }
            else
            {
                if (intGrpingId != 0)
                {
                    intRecordAffected = grouping.updateGroupingById(intGrpingId, strGrpingName, intGrpingActive);

                    if (intRecordAffected > 0) { boolEdited = true; }
                    else { boolSuccess = false; }

                    if (!boolSuccess)
                    {
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to Update grouping. Please try again.</div>";

                        Response.Redirect(currentPageName);
                    }
                    else
                    {
                        if (boolEdited)
                        {
                            Session["EDITED_GROUPING"] = 1;
                        }
                        else
                        {
                            Session["EDITED_GROUPING"] = 1;
                            Session["NOCHANGE"] = 1;
                        }

                        Response.Redirect(currentPageName);
                    }
                }
            }
        }
    }
    protected void rptGrouping_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            intGrpingCount++;
            int intGrpingId = int.Parse(DataBinder.Eval(e.Item.DataItem, "LIST_ID").ToString());
            int intGrpingActive = int.Parse(DataBinder.Eval(e.Item.DataItem, "LIST_ACTIVE").ToString());

            HyperLink hypEdit = (HyperLink)e.Item.FindControl("hypEdit");
            hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.NavigateUrl = currentPageName + "?id=" + intGrpingId + "#grouping";

            LinkButton lnkbtnDelete = (LinkButton)e.Item.FindControl("lnkbtnDelete");
            lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteGrouping.Text") + "');";

            LinkButton lnkbtnUp = (LinkButton)e.Item.FindControl("lnkbtnUp");
            //lnkbtnUp.Text = GetGlobalResourceObject("GlobalResource", "contentAdmUp.Text").ToString();

            LinkButton lnkbtnDown = (LinkButton)e.Item.FindControl("lnkbtnDown");
            //lnkbtnDown.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDown.Text").ToString();

            Literal litNo = (Literal)e.Item.FindControl("litNo");
            litNo.Text = (e.Item.ItemIndex + 1).ToString();

            if (intGrpingCount == 1)
            {
                lnkbtnUp.Enabled = false;
            }

            else if (intGrpingCount == intGrpingTotal)
            {
                lnkbtnDown.Enabled = false;
            }
        }
    }

    protected void rptGrouping_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdDelete")
        {
            int intId = int.Parse(e.CommandArgument.ToString());
            int intRecordAffected = 0;
            intRecordAffected = grouping.deleteGrouping(intId);

            if (intRecordAffected == 1)
            {
                bindRptData();
                fillGrouping();
            }
        }
        if (e.CommandName == "cmdUp")
        {
            string strArg = e.CommandArgument.ToString();
            string[] strArgSplit = strArg.Split((char)'|');

            int intId = Convert.ToInt16(strArgSplit[0]);
            int intOrder = Convert.ToInt16(strArgSplit[1]);

            int intPrevOrder = 0;
            int intPrevId = 0;

            if (grouping.extractPrevGrouping(intOrder, 0))
            {
                intPrevOrder = grouping.grpingOrder;
                intPrevId = grouping.grpingId;
                grouping.updateListOrderById(intId, intPrevOrder);
                grouping.updateListOrderById(intPrevId, intOrder);

                //clsMis.resetListing();
                bindRptData();
                fillGrouping();
            }
        }
        else if (e.CommandName == "cmdDown")
        {
            string strArg = e.CommandArgument.ToString();
            string[] strArgSplit = strArg.Split((char)'|');

            int intId = Convert.ToInt16(strArgSplit[0]);
            int intOrder = Convert.ToInt16(strArgSplit[1]);

            int intNextOrder = 0;
            int intNextId = 0;

            if (grouping.extractNextGrouping(intOrder, 0))
            {
                intNextOrder = grouping.grpingOrder;
                intNextId = grouping.grpingId;

                grouping.updateListOrderById(intId, intNextOrder);
                grouping.updateListOrderById(intNextId, intOrder);

                //clsMis.resetListing();
                bindRptData();
                fillGrouping();
            }
        }
    }

    
    #endregion

    
    #region "Methods"
    public void fill()
    {
        fillGrouping();
    }
    protected void setPageProperties()
    {
        bindRptData();
    }

    protected void fillGrouping()
    {
        DataView dvGrping = new DataView(grouping.getGrpingList().Tables[0]);
        dvGrping.Sort = "LIST_ORDER ASC";

        intGrpingTotal = dvGrping.Count;

        if (dvGrping.Count > 0)
        {
            rptGrouping.DataSource = dvGrping;
            rptGrouping.DataBind();
        }
        else
        {
            rptGrouping.DataSource = null;
            rptGrouping.DataBind();
        }
    }

    protected void bindRptData()
    {
        if (!string.IsNullOrEmpty(Request["id"]))
        {
            int intTryParse;
            if (int.TryParse(Request["id"], out intTryParse))
            {
                intGrpingId = intTryParse;
            }
        }

        if (intGrpingId != 0)
        {
            if(grouping.extractGrpingById(intGrpingId))
            {
                txtGroupingName.Text = grouping.grpingName;

                if (grouping.grpingActive == 1) { chkboxActive.Checked = true; }
                else { chkboxActive.Checked = false; }
            }
        }
        else
        {
            txtGroupingName.Text = "";
        }
    }

    #endregion
}