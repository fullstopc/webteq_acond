﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Linq;
using Newtonsoft.Json;

public partial class ctrl_ucUsrPerformance : System.Web.UI.UserControl
{
    protected Dictionary<string, object> returnValue;
    public bool isMobile;

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion


    #region "Methods"
    public void fill()
    {


        string defaultImg = ConfigurationManager.AppSettings["scriptBase"] + new clsConfig().defaultGroupImg;
        int contestID = 0;

        clsContest contest = new clsContest();
        if (contest.extractData("contest_active", 1))
        {
            contestID = contest.ID;
        }

        List<clsSchool> schools = new clsSchool().getDataTable()
            .AsEnumerable()
            .Where(x => Convert.ToInt32(x["school_active"]) == 1)
            .Select(x => new clsSchool() { row = x }).ToList();

        List<clsContestant> contestants = new clsContestant().getDataTable()
            .AsEnumerable()
            .Where(x => Convert.ToInt32(x["contest_id"]) == contestID)
            .Select(x => new clsContestant()
            {
                row = x,
                companyLogoUrl = !string.IsNullOrEmpty(x["contestant_comp_logo"].ToString()) ?
                                  ConfigurationManager.AppSettings["uplBase"] + "company/" + Convert.ToString(x["contestant_comp_logo"]) :
                                 defaultImg,
            }).ToList();

        List<clsContestantResult> results = new clsContestantResult().getDataTable()
            .AsEnumerable()
            .Where(x => Convert.ToInt32(x["contest_id"]) == contestID)
            .Select(x => new clsContestantResult() { row = x, }).ToList();


        returnValue = new Dictionary<string, object>()
        {
            { "contest" , contest },
            { "contestants", contestants },
            { "results", results },
            { "schools", schools }
        };
    }
    #endregion
}
