﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmMastheadGallery.ascx.cs" Inherits="ctrl_ucAdmMastheadGallery" Debug="true" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>

<asp:Panel ID="pnlFormSect1" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table border="0" id="tblAddMember1" cellpadding="0" cellspacing="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr">Slide Show Image Details</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Image Name<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax"><asp:TextBox ID="txtMastName" runat="server" class="text_fullwidth" MaxLength="250"></asp:TextBox>
                    <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvProdName" runat="server" ErrorMessage="<br />Please enter Image Name." ControlToValidate="txtMastName" Display="Dynamic" CssClass="errmsg" ValidationGroup="grpProd"></asp:RequiredFieldValidator></span>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Image:</td>
                <td class="tdLabel">
                    <asp:Panel ID="pnlFileUpload" runat="server" CssClass="divFileUpload">
                        <asp:Panel ID="pnlFileUploadAction" runat="server" CssClass="divFileUploadAction">
                            <asp:FileUpload ID="fileProdImage" runat="server" ValidationGroup="grpProd" />
                        </asp:Panel>
                        <span class="spanTooltip">
                            <span class="icon"></span>
                            <span class="msg">
                                <asp:Literal ID="litUploadRule" runat="server"></asp:Literal>
                                
                            </span>
                        </span>
                    </asp:Panel>
                    <asp:HiddenField ID="hdnProdImage" runat="server" />
                    <asp:HiddenField ID="hdnProdImageRef" runat="server" />
                    <asp:CustomValidator ID="cvProdImage" runat="server" ControlToValidate="fileProdImage" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateProdImage_server" OnPreRender="cvProdImage_PreRender" ValidationGroup="grpProd"></asp:CustomValidator>
                    <asp:Panel ID="pnlProdImage" runat="server" Visible="false">
                        <br /><asp:LinkButton ID="lnkbtnDeleteImage" runat="server" Text="Delete" ToolTip="Delete" CausesValidation="false" OnClick="lnkbtnDeleteImage_Click"></asp:LinkButton>
                        <br /><asp:Image ID="imgProdImage" runat="server" />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Thumb Image:</td>
                <td class="tdLabel" style="padding-bottom: 30px;">
                    <asp:Panel ID="pnlFileUpload2" runat="server" CssClass="divFileUpload">
                        <asp:Panel ID="pnlFileUploadAction2" runat="server" CssClass="divFileUploadAction">
                            <asp:FileUpload ID="fileThumbImage" runat="server" ValidationGroup="grpProd" />
                            </asp:Panel>
                        <span class="spanTooltip">
                            <span class="icon"></span>
                            <span class="msg">
                                <asp:Literal ID="litUploadRule2" runat="server"></asp:Literal> 
                            </span>
                        </span>
                    </asp:Panel>
                    <asp:HiddenField ID="hdnThumbImage" runat="server" />
                    <asp:HiddenField ID="hdnThumbImageRef" runat="server" />
                    <asp:CustomValidator ID="cvThumbImage" runat="server" ControlToValidate="fileThumbImage" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateThumbImage_server" OnPreRender="cvThumbImage_PreRender" ValidationGroup="grpProd"></asp:CustomValidator>
                    <asp:Panel ID="pnlThumbImage" runat="server" Visible="false">
                        <br /><asp:LinkButton ID="LinkButton1" runat="server" Text="Delete" ToolTip="Delete" CausesValidation="false" OnClick="lnkbtnDeleteImage2_Click"></asp:LinkButton>
                        <br /><asp:Image ID="imgThumbImage" runat="server" />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Short Description:</td>
                <td><asp:TextBox ID="txtShortDesc" runat="server" MaxLength="250" CssClass="text_fullwidth"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel">Tagline 1:</td>
                <td><asp:TextBox ID="txtMastTagline1" runat="server" TextMode="MultiLine" Rows="5" MaxLength="1000" CssClass="text_fullwidth"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel">Tagline 2:</td>
                <td><asp:TextBox ID="txtMastTagline2" runat="server" TextMode="MultiLine" Rows="5" MaxLength="1000" CssClass="text_fullwidth"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Group<span class="attention_compulsory">*</span>:</td>
                <td>
                    <asp:DropDownList ID="ddlGroup" runat="server" CssClass="ddl_fullwidth"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvGroup" runat="server" ErrorMessage="<br />Please select Group." ControlToValidate="ddlGroup" Display="Dynamic" CssClass="errmsg" ValidationGroup="grpProd"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Order:</td>
                <td><asp:TextBox ID="txtOrder" runat="server" MaxLength="9" CssClass="text_medium"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel">Clickable</td>
                <td class="tdLabel"><asp:CheckBox ID="chkboxClickable" runat="server" OnCheckedChanged="chkboxClickable_CheckedChanged" AutoPostBack="true" /></td>
            </tr>
            <tr id="trClickable" runat="server" visible="false">
                <td></td>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td class="tdSpacer">&nbsp;</td>
                            <td class="tdSpacer">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="tdLabel">URL</td>
                            <td>
                                <asp:TextBox ID="txtURL" runat="server" CssClass="text_fullwidth"></asp:TextBox><br />
                                <asp:Literal ID="litUrlRule" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel">Target</td>
                            <td><asp:DropDownList ID="ddlSlideTarget" runat="server" CssClass="ddl"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td class="tdSpacer">&nbsp;</td>
                            <td class="tdSpacer">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Active:</td>
                <td class="tdLabel"><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tfoot>
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="grpProd"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CssClass="btn btnCancel" Visible="false" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
       </table>
    </asp:Panel>
    
</asp:Panel>
