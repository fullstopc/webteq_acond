﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmShipping2.ascx.cs" Inherits="ctrl_ucAdmShipping2" %>

<script type="text/javascript">
    function validateShippingFee_client(source, args) {
        var txtSubSeqFee = document.getElementById("<%= txtSubSeqFee.ClientID %>");
        var currRE = /<%= clsSetting.CONSTBIGCURRENCYRE %>/;
        

        if (Trim(args.Value) != '') {
            if (args.Value.match(currRE)) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
                source.errormessage = "<br />Please enter only currency.";
                source.innerHTML = "<br />Please enter only currency.";
            }   
        }
    }

    function validateShippingWeight_client(source, args) {
        var txtFirstWeight = document.getElementById("<%= txtFirstWeight.ClientID %>");
        var currWeight = /<%= clsSetting.CONSTBIGCURRENCYRE %>/;

        if (Trim(args.Value) != '') {
            if (args.Value.match(currWeight)) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
                source.errormessage = "<br/>Please enter only number.";
                source.innerHTML = "<br/>Please enter only number.";
            }
        }
    }
</script>

<asp:Panel ID="pnlAck" runat="server" Visible="false">
    <asp:Literal ID="litAck" runat="server"></asp:Literal>
    <asp:Panel ID="pnlAckBtn" runat="server" class="divAckBtn">
        <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" CssClass="btn" CausesValidation="false"></asp:LinkButton>
    </asp:Panel>
</asp:Panel>
<asp:Panel ID="pnlShippingDetails" runat="server">
    <table id="tblAddMember1" cellpadding="0" cellspacing="0" class="formTbl">
        <tr>
            <td colspan="3" class="tdSectionHdr">Shipping Details</td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td class="tdLabel">Country<span class="attention_compulsory">*</span>:</td>
            <td colspan="2">
                <asp:DropDownList ID="ddlCountry" runat="server" CssClass="ddl" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ErrorMessage="<br />Please select one country." Display="Dynamic" CssClass="errmsg" ControlToValidate="ddlCountry"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr id="trState" runat="server" visible="false">
            <td class="tdLabel">State:</td>
            <td colspan="2"><asp:TextBox ID="txtState" runat="server" CssClass="text_small"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Literal ID="litFirstFee" runat="server"></asp:Literal><span class="attention_compulsory">*</span>:</td> 
            <td>
                <asp:TextBox ID="txtFirstWeight" runat="server" CssClass="text_extraSmall"></asp:TextBox> kg
                <asp:RequiredFieldValidator ID="rfvFirstWeight" runat="server" ErrorMessage="<br/>Please enter first block weight." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtFirstWeight"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvFirstWeight" runat="server" ControlToValidate="txtFirstWeight" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateShippingWeight_client"></asp:CustomValidator>
            </td>           
            <td>
                RM: <asp:TextBox ID="txtFirstFee" runat="server" MaxLength="20" CssClass="text_small"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvFirstFee" runat="server" ErrorMessage="<br />Please enter shipping fee." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtFirstFee"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvFirstFee" runat="server" ControlToValidate="txtFirstFee" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateShippingFee_client"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Literal ID="litSubSeqFee" runat="server"></asp:Literal><span class="attention_compulsory">*</span>:</td>
            <td>
                <asp:TextBox ID="txtSubSeqWeight" runat="server" CssClass="text_extraSmall"></asp:TextBox> kg
                <asp:RequiredFieldValidator ID="rfvSubSeqWeight" runat="server" ErrorMessage="<br/>Please enter subsequent block weight." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtSubSeqWeight"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvSubSeqWeight" runat="server" ControlToValidate="txtSubSeqWeight" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateShippingWeight_client"></asp:CustomValidator>
            </td>
            <td>
                RM: <asp:TextBox ID="txtSubSeqFee" runat="server" MaxLength="20" CssClass="text_small"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvSubSeqFee" runat="server" ErrorMessage="<br />Please enter shipping fee." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtSubSeqFee"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvSubSeqFee" runat="server" ControlToValidate="txtSubSeqFee" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateShippingFee_client"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td><asp:Literal ID="litFreeShippingFee" runat="server"></asp:Literal><span class="attention_compulsory">*</span>:</td>
            <td>
                <asp:TextBox ID="txtFreeShippingWeight" runat="server" CssClass="text_extraSmall"></asp:TextBox> kg
                <asp:RequiredFieldValidator ID="rfvFreeShippingWeight" runat="server" ErrorMessage="<br/>Please enter weight." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtFreeShippingWeight"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvFreeShippingWeight" runat="server" ControlToValidate="txtFreeShippingWeight" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateShippingWeight_client"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" class="tdSectionHdr">Shipping Settings</td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td class="tdLabel">Active:</td>
            <td colspan="2"><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" class="tdLinkBtn">
                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btnNor" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Back" ToolTip="Back" CssClass="btn" CausesValidation="false"></asp:LinkButton>
                <div class="tdDelete">
                    <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CssClass="btnDelete" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>
