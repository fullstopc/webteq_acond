﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucUsrFacebookLike : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _prodId;
    #endregion

    #region "Property Methods"
    public int prodId
    {
        get
        {
            if (ViewState["PRODID"] == null)
            {
                return _prodId;
            }
            else
            {
                return Convert.ToInt16(ViewState["PRODID"]);
            }
        }
        set { ViewState["PRODID"] = value; }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        clsProduct prod = new clsProduct();
        prod.extractProdById(prodId, 1);
        int intProdId = prod.prodId;

        string strUrl = ConfigurationManager.AppSettings["uplBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/product.aspx?id=" + intProdId;

        litFacebookLike.Text = "<a href='" + strUrl + "'></a>";
    }
}
