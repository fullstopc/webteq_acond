﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Collections;
using System.Net;
using System.IO;

public partial class ctrl_ucUsr : System.Web.UI.UserControl
{
    #region "Property"
    
    #endregion

    #region "Properties Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    #endregion
    
    public void fill()
    {
        if (!string.IsNullOrEmpty(Request["tx"]))
        {
            Reservation reserve = Session[clsKey.SESSION_RESERVATION] as Reservation;
            if (reserve != null)
            {
                litServiceDate.Text = reserve.config.serviceDatetimeFrom.ToString("d MMM yyyy");
                litServiceTime.Text = reserve.config.serviceDatetimeFrom.ToString("h:mm tt");
                litServiceAddrUnit.Text = reserve.config.propertyUnitno;
                litServiceItemQty.Text = reserve.config.serviceItemQty.ToString();
                litServiceAmount.Text = clsMis.formatPrice(reserve.config.serviceChargeAmount);
                litServiceRemark.Text = (!string.IsNullOrEmpty(reserve.config.serviceRemark)) ? reserve.config.serviceRemark : "-";
                litServiceContactTel.Text = reserve.config.serviceContactTel;

                litMemberEmail.Text = Session[clsKey.SESSION_MEMBER_EMAIL].ToString();
                litMemberName.Text = Session[clsKey.SESSION_MEMBER_NAME].ToString();

                hypMemberEmail.NavigateUrl = "mailto:" + Session[clsKey.SESSION_MEMBER_EMAIL].ToString();
            }

            Paypal paypal = new Paypal();
            if (paypal.PDTValidate(Request["tx"]))
            {
                litAckMsg.Text = "Your booking is done successfully.";
            }
            else
            {
                litAckMsg.Text = string.Format(@"Oops, I'm sorry. Your online payment has failed. Please contact us if you need our assistance.");
            }
        }

        
        
    }

}
