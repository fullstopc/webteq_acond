﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ctrl_ucAdmShippingCompany : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _shipId = 0;
    protected int _mode = 1;
    protected string _pageListingURL = "";
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int shipId
    {
        get
        {
            if (ViewState["SHIPID"] == null)
            {
                return _shipId;
            }
            else
            {
                return int.Parse(ViewState["SHIPID"].ToString());
            }
        }
        set { ViewState["SHIPID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtState.Text = "";

        if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
        {
            string countryId = ddlCountry.SelectedValue;

            clsCountry country = new clsCountry();
            country.extractCountryById(countryId, 1);

            if (country.showState == 1) { trState.Visible = true; }
            else { trState.Visible = false; }
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int shipCompany = 0;
            int shipMethod = 0;

            if (!string.IsNullOrEmpty(ddlShipComp.SelectedValue))
            {
                int intTryParse;
                if (int.TryParse(ddlShipComp.SelectedValue, out intTryParse)) { shipCompany = intTryParse; }
            }

            if (!string.IsNullOrEmpty(ddlShipMethod.SelectedValue))
            {
                int intTryParse;
                if (int.TryParse(ddlShipMethod.SelectedValue, out intTryParse)) { shipMethod = intTryParse; }
            }

            string country = ddlCountry.SelectedValue;
            string state = txtState.Text.Trim();
            decimal shipWeightFirst = Convert.ToDecimal("0.00");
            decimal shipFeeFirst = Convert.ToDecimal("0.00");
            decimal shipWeightSub = Convert.ToDecimal("0.00");
            decimal shipFeeSub = Convert.ToDecimal("0.00");

            if (!string.IsNullOrEmpty(txtFirstBlockWeight.Text.Trim())) { shipWeightFirst = Convert.ToDecimal(txtFirstBlockWeight.Text.Trim()); }
            if (!string.IsNullOrEmpty(txtFirstBlockPrice.Text.Trim())) { shipFeeFirst = Convert.ToDecimal(txtFirstBlockPrice.Text.Trim()); }
            if (!string.IsNullOrEmpty(txtSubBlockWeight.Text.Trim())) { shipWeightSub = Convert.ToDecimal(txtSubBlockWeight.Text.Trim()); }
            if (!string.IsNullOrEmpty(txtSubBlockPrice.Text.Trim())) { shipFeeSub = Convert.ToDecimal(txtSubBlockPrice.Text.Trim()); }
            int active = chkboxActive.Checked ? 1 : 0;

            clsShipping ship = new clsShipping();
            int intRecordAffected = 0;

            switch (mode)
            {
                case 1:
                    if (!ship.isShippingExist2(shipCompany, shipMethod, country, state))
                    {
                        intRecordAffected = ship.addShipping2(shipCompany, shipMethod, country, state, shipWeightFirst, shipFeeFirst, shipWeightSub, shipFeeSub, active, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1)
                        {
                            Session["NEWSHIPID"] = ship.shipId;
                            Response.Redirect(currentPageName + "?id=" + ship.shipId);
                        }
                        else
                        {
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new shipping details. Please try again.</div>";
                            Response.Redirect(currentPageName);
                        }
                    }
                    else
                    {
                        Session["ERRMSG"] = "<div class=\"errmsg\">This shipping details already exists.</div>";
                        Response.Redirect(currentPageName);
                    }
                    break;
                case 2:
                    Boolean boolSuccess = true;
                    Boolean boolEdited = false;

                    if (!ship.isExactSameSet2(shipId, shipCompany, shipMethod, country, state, shipWeightFirst, shipFeeFirst, shipWeightSub, shipFeeSub, active))
                    {
                        intRecordAffected = ship.updateShippingById2(shipId, shipCompany, shipMethod, country, state, shipWeightFirst, shipFeeFirst, shipWeightSub, shipFeeSub, active, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;
                        }
                        else
                        {
                            boolSuccess = false;
                        }
                    }

                    if (boolEdited)
                    {
                        Session["EDITEDSHIPID"] = ship.shipId;
                    }
                    else
                    {
                        if (!boolSuccess)
                        {
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit shipping details. Please try again.</div>";
                        }
                        else
                        {
                            Session["EDITEDSHIPID"] = shipId;
                            Session["NOCHANGE"] = 1;
                        }
                    }

                    Response.Redirect(Request.Url.ToString());
                    break;
            }
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        int intDeletedShipId = 0;

        clsMis.performDeleteShipping(shipId, ref intDeletedShipId);

        Session["DELETEDSHIPID"] = intDeletedShipId;
        Response.Redirect(currentPageName);
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void setPageProperties()
    {
        lblShipComp.Text = "Shipping Company";
        lblShipMethod.Text = "Shipping Method";
        lblCountry.Text = "Country";
        lblState.Text = "State";
        lblFirstBlock.Text = "First block";
        lblSubBlock.Text = "Subsequent block";
        lblActive.Text = "Active";

        clsConfig config = new clsConfig();
        clsCountry country = new clsCountry();
        DataSet ds = new DataSet();
        DataView dv = new DataView();

        ds = config.getItemsByGroup(clsConfig.CONSTGROUPSHIPPINGCOMPANY, 1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "CONF_NAME ASC";

        ddlShipComp.DataSource = dv;
        ddlShipComp.DataTextField = "CONF_NAME";
        ddlShipComp.DataValueField = "CONF_ID";
        ddlShipComp.DataBind();

        ListItem ddlShipCompDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString());
        ddlShipComp.Items.Insert(0, ddlShipCompDefaultItem);

        ds = config.getItemsByGroup(clsConfig.CONSTGROUPSHIPPINGMETHOD, 1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "CONF_NAME ASC";

        ddlShipMethod.DataSource = dv;
        ddlShipMethod.DataTextField = "CONF_NAME";
        ddlShipMethod.DataValueField = "CONF_ID";
        ddlShipMethod.DataBind();

        ListItem ddlShipMethodDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString());
        ddlShipMethod.Items.Insert(0, ddlShipMethodDefaultItem);

        ds = country.getCountryList(1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "COUNTRY_NAME ASC";

        ddlCountry.DataSource = dv;
        ddlCountry.DataTextField = "COUNTRY_NAME";
        ddlCountry.DataValueField = "COUNTRY_CODE";
        ddlCountry.DataBind();

        ListItem ddlCountryDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString());
        ddlCountry.Items.Insert(0, ddlCountryDefaultItem);

        string shippingUnit = config.shippingUnit;
        string shippingCurrency = config.currency;

        litFirstBlockWeightUnit.Text = litSubBlockWeightUnit.Text = shippingUnit;
        litFirstBlockPriceUnit.Text = litSubBlockPriceUnit.Text = shippingCurrency;

        switch (mode)
        {
            case 1:
                lnkbtnDelete.Visible = false;
                break;
            case 2:
                lnkbtnDelete.Visible = true;
                break;
        }

        if (mode == 2)
        {
            fillForm();
        }

        lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteShipping.Text").ToString() + "'); return false;";
        lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
    }

    protected void fillForm()
    {
        clsShipping ship = new clsShipping();

        if (ship.extractShippingById(shipId, 0))
        {
            if (ship.shipComp > 0) { ddlShipComp.SelectedValue = ship.shipComp.ToString(); }
            if (ship.shipMethod > 0) { ddlShipMethod.SelectedValue = ship.shipMethod.ToString(); }

            if (ship.shipDefault != 0)
            {
                ListItem ddlCountryAllDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString());
                ddlCountry.Items.Insert(0, ddlCountryAllDefaultItem);

                ddlCountry.SelectedValue = ship.shipCountry;

                ddlCountry.Enabled = false;
                txtState.Enabled = false;
                chkboxActive.Enabled = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(ship.shipCountry)) { ddlCountry.SelectedValue = ship.shipCountry; }
                else { ddlCountry.SelectedValue = "MY"; }
            }

            if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
            {
                string countryId = ddlCountry.SelectedValue;

                clsCountry country = new clsCountry();
                country.extractCountryById(countryId, 1);

                if (country.showState == 1)
                {
                    trState.Visible = true;
                    txtState.Text = ship.shipState;
                }
                else
                {
                    trState.Visible = false;
                }
            }

            txtFirstBlockWeight.Text = ship.shipKgFirst.ToString();
            txtFirstBlockPrice.Text = ship.shipFeeFirst.ToString();
            txtSubBlockWeight.Text = ship.shipKg.ToString();
            txtSubBlockPrice.Text = ship.shipFee.ToString();

            if (ship.shipActive == 1) { chkboxActive.Checked = true; }
            else { chkboxActive.Checked = false; }
        }
    }

    //protected string checkData()
    //{
    //    clsShipping ship = new clsShipping();


    //}
    #endregion
}
