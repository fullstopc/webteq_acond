﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ctrl_ucAdmGroupDesc : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected int _grpingId = clsAdmin.CONSTLISTGROUPINGCAT;
    protected string _grpingName = clsAdmin.CONSTLISRGROUPINGCATNAME;
    protected int _grpId = 0;
    protected string _pageListingURL = "";

    protected string grpDesc;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName2(true); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int grpingId
    {
        get
        {
            if ((Session["GRPINGID"] == null) || (Session["GRPINGID"] == "")) { return _grpingId; }
            else { return Convert.ToInt16(Session["GRPINGID"]); }
        }
        set { Session["GRPINGID"] = value; }
    }

    public string grpingName
    {
        get { return Session["GRPINGNAME"] as string ?? _grpingName; }
        set { Session["GRPINGNAME"] = value; }
    }

    public int grpId
    {
        get
        {
            if (ViewState["GROUPID"] == null)
            {
                return _grpId;
            }
            else
            {
                return Convert.ToInt16(ViewState["GROUPID"]);
            }
        }
        set { ViewState["GROUPID"] = value; }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (grpId == 0)
            {
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit category. Please select a valid category.</div>";
                Response.Redirect(currentPageName);
            }

            if (mode == 2)
            {
                Boolean bEdited = false;
                Boolean bSuccess = true;
                clsGroup grp = new clsGroup();
                int intRecordAffected = 0;

                grpDesc = txtCatDesc.Text.Trim();

                if (bEdited || !grp.isExactSameGrpSet(grpId, grpDesc))
                {
                    intRecordAffected = grp.updateGrpDescById(grpId, grpDesc, Convert.ToInt16(Session["ADMID"]));

                    if (intRecordAffected == 1)
                    {
                        bEdited = true;
                    }
                    else { bSuccess = false; }
                }

                if (bEdited)
                {
                    Session["EDITGRPID"] = grp.grpId;
                }
                else
                {
                    if (!bSuccess)
                    {
                        grp.extractGrpById(grpId, 0);
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit category (" + grp.grpDName + "). Please try again.</div>";
                    }
                    else
                    {
                        Session["EDITGRPID"] = grpId;
                        Session["NOCHANGE"] = 1;
                    }
                }

                Response.Redirect(Request.Url.ToString());
            }
        }
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }

        registerCKEditorScript();
    }

    protected void setPageProperties()
    {
        if (!IsPostBack)
        {
            if (mode == 2)
            {
                fillForm();
            }

            litGroupContent.Text = grpingName + " Content";
            lblGroupDesc.Text = grpingName + " Description";

            lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
        }
    }

    public void registerCKEditorScript()
    {
        Session["CMSPATH"] = "";
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">" +
                    "CKEDITOR.replace('" + txtCatDesc.ClientID + "'," +
                    "{" +
                    "height:'300'," +
                    "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSGRPVIEW + "&command=QuickUpload&type=Files&path=2'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSGRPVIEW + "&command=QuickUpload&type=Images&path=2'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSGRPVIEW + "&command=QuickUpload&type=Flash&path=2'" +
                     "}" +
                     ");";

            strJS += "CKEDITOR.add;";

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMEEDITORSTYLE, clsConfig.CONSTGROUPWEBSITE, 1);
            string strEditorStyle = config.value;
            if (!string.IsNullOrEmpty(strEditorStyle))
            {
                strJS += "CKEDITOR.config.contentsCss = 'body {" + strEditorStyle + "}';";
            }

            strJS += "CKEDITOR.config.stylesCombo_stylesSet= '" + clsAdmin.CONSTADMSTYLESET + ":" + System.Configuration.ConfigurationManager.AppSettings["jsBase"] + clsAdmin.CONSTADMSTYLESETFILE + "';" +
                     "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR", strJS, false);
        }
    }

    protected void fillForm()
    {
        clsGroup grp = new clsGroup();

        if (grp.extractGrpById(grpId, 0))
        {
            txtCatDesc.Text = grp.grpDesc;
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }
    #endregion
}
