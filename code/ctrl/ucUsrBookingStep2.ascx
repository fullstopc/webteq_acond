﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrBookingStep2.ascx.cs" Inherits="ctrl_ucUsr" %>
<script>
    function validateDatetime_client(source, args) {
        var value = $("#hdnServiceConfig").val();
        if (value) {
            args.IsValid = true;
            $(".ack-container").addClass("hide");
        } else {
            args.IsValid = false;
            $(".ack-container").removeClass("hide");
        }
        
    }
</script>

<div class="bookingform__headline">
    <h3>
        Unit No: <asp:Literal runat="server" ID="litAddrUnit"></asp:Literal>,
        <asp:Literal runat="server" ID="litItemQty"></asp:Literal> Aircond
    </h3>
</div>
<div class="bookingform__info">
    <span class="">When do you need the aircond cleaning service?</span>
</div>
<div class="ack-container error hide">
    <asp:CustomValidator
        Display="Dynamic"
        runat="server"
        ID="cvServiceDatetime"
        ClientValidationFunction="validateDatetime_client"
        ErrorMessage="Please select the datetime"
        ValidationGroup="vg"
        ></asp:CustomValidator>
</div>
<div class="calendar">
    <div class="calendar__container"></div>
    <div class="calendar__lookup">
        <div class="calendar__lookup__item calendar__lookup__item-selected">
            <i></i><span>Selected</span>
        </div>
        <div class="calendar__lookup__item calendar__lookup__item-today">
            <i></i><span>Today</span>
        </div>
        <div class="calendar__lookup__item calendar__lookup__item-occupied">
            <i></i><span>Occupied</span>
        </div>
        <div class="calendar__lookup__item calendar__lookup__item-holiday">
            <i></i><span>Holiday</span>
        </div>
    </div>
</div>

<asp:LinkButton ID="lnkbtnSubmit" runat="server" 
    CssClass="button button-full button-round button-icon-right button-edge display hide" 
    OnClick="lnkbtnSubmit_Click" 
    ValidationGroup="vg" 
    ToolTip="Next">
    <span>Next</span>
    <i class="material-icons">keyboard_arrow_right</i>
</asp:LinkButton>
<asp:HiddenField runat="server" ID="hdnServiceConfig" ClientIDMode="Static"/>

<script>
    function Calendar(data) {
        var self = this;
        this.parent = $(".bookingform__step2");
        this.container = $(".calendar__container");
        this.monthCount = 3;
        this.reservation = data.reservation.config;
        this.reserved_list = data.reserved_list;
        this.config = data.config;
        this.holidays = data.holidays;
        this.leaves = data.leaves;
        this.agents = data.agents.map(function(agent){
            var leaves = data.leaves.filter(function(leave){
                return leave.employeeID == agent.ID;
            });
            agent.leaves = leaves;
            return agent;
        });
        this.week = data.week.filter(function (day) {
            return day.dayChecked == 0;
        });

        
        var serviceTotalHour = this.reservation.serviceDurationHour;
        var timeslotPerUnit = <%= clsKey.CONFIG__TIMESLOT__DURATION %>;
        var timeslotTotalUnit = serviceTotalHour / timeslotPerUnit;
        
        this.timeslots = data.timeslots.map(function (timeslot, index) {
            if(timeslotTotalUnit > 1 && data.timeslots.length > index + timeslotTotalUnit){
                var timeslotLeftCount = Math.min(timeslotTotalUnit - 1, data.timeslots.length -  index + 1);
                timeslot.timeTo = data.timeslots[index + timeslotLeftCount].timeTo;
            }
            return timeslot;
        }).sort(function (a, b) {
            return a.timeFrom > b.timeFrom ? 1 : -1;
        });
        this.template = {
            container: '<div class ="calendar__header"> <div class ="calendar__days"> <div class ="calendar__day__row"> <span class ="calendar__day"> <span class ="calendar__day__value">M</span> </span> <span class ="calendar__day"> <span class ="calendar__day__value">T</span> </span> <span class ="calendar__day"> <span class ="calendar__day__value">W</span> </span> <span class ="calendar__day"> <span class ="calendar__day__value">T</span> </span> <span class ="calendar__day"> <span class ="calendar__day__value">F</span> </span> <span class ="calendar__day"> <span class ="calendar__day__value">S</span> </span> <span class ="calendar__day"> <span class ="calendar__day__value">S</span> </span> </div> </div> </div> <div class ="calendar__body"> <div class ="calendar__body__inner"> {{months}} </div> </div>',
            month: '<div class ="calendar__month">{{month}}</div><div class ="calendar__days">{{days}}</div>',
            day: '<div class="calendar__day {{class}}" data-date="{{date}}"> <div class ="calendar__day__info"> <span class ="calendar__day__value">{{day}}</span> <span class ="calendar__day__background"></span> </div> {{timeslot.picker}} </div>',
            timepicker: '<div class ="calendar_timeslot"> <div class ="timerpicker"> <div class ="timerpicker__action"> <div class ="timerpicker__date"></div> <div class ="timerpicker__close"> <i class ="material-icons">close</i> </div> </div> <div class ="timerpicker__info">{{timeslot.info}}</div><div class ="timerpicker__options">{{timeslot.list}}</div> <div class ="row"> <div class ="col"> <a title="Next" class ="timerpicker__submit button button-full button-round button-icon-right button-edge"> <span>Next</span> <i class ="material-icons">keyboard_arrow_right</i> </a> </div> </div> </div> </div>',
            timeslot: '<div class="timerpicker__item {{timeslot.occupied}}" data-timeslot-from="{{timeslot.from}}" data-timeslot-to="{{timeslot.to}}" data-agents={{agents}}>{{timeslot.from}}</div>'
        }

        this.init = function () {
            var months = [moment().startOf('month')];
            for (var i = 1; i < this.monthCount; i++) {
                months.push(moment().add(i, 'month').startOf('month'));
            }
            bindCalendarData(months, this);
            //bindTimeslotData(this);
            bindClick(this);
        }
        var bindCalendarData = function (months, self) {
            var template = self.template;
            var reservation = self.reservation;
            var reserved_list = self.reserved_list;
            var config = self.config;
            var timeslots = self.timeslots;
            var agents = self.agents;
            var leaves = self.leaves;
            var templateMonths = months.map(function(month){
                var days = month.daysInMonth();
                var templateDays = [];

                // add days prefix
                var monthPrefix = Math.abs(1 - month.startOf('day').weekday());
                for (var i = 1; i <= monthPrefix; i++) {
                    templateDays.push(template.day
                        .replace("{{class}}", "empty")
                        .replace("{{day}}", "")
                        .replace("{{timeslot.picker}}", ""));
                }

                // add days content
                for (var i = 1; i <= days; i++) {
                    var past_current_future = "";
                    var date = month.format("YYYY-MM-" + pad(i, 2));
                    if (moment().isSame(date, 'day')) past_current_future = "today";
                    else if (moment().isAfter(date)) past_current_future = "past";
                    else if (moment().isBefore(date)) past_current_future = "future";

                    // check is holiday
                    var holidays = self.holidays.filter(function (holiday) {
                        return moment(holiday.date).isSame(date, 'day');
                    });

                    // check week
                    var week = self.week.filter(function (day) {
                        return day.ID == moment(date).day();
                    });

                    var serviceTotalHour = reservation.serviceDurationHour;
                    var timeslotPerUnit = <%= clsKey.CONFIG__TIMESLOT__DURATION %>;
                    var timeslotTotalUnit = serviceTotalHour / timeslotPerUnit;

                    // timeslots mapping
                    var timeslotsNew = timeslots.filter(function (timeslot, index) {
                        // remove last few timeslot
                        return index < timeslots.length - timeslotTotalUnit + 1;

                    }).map(function (timeslot) {
                        /* Check for available agent */
                        var agentsOccupied = reserved_list.filter(function (reserved) {
                            var dateThis = moment(date);
                            var timeFrom = moment(timeslot.timeFrom)
                                .year(dateThis.year())
                                .month(dateThis.month())
                                .date(dateThis.date());
                            var timeTo = moment(timeslot.timeTo)
                                .year(dateThis.year())
                                .month(dateThis.month())
                                .date(dateThis.date());
                            var reservedTimeFrom = moment(reserved.serviceDatetimeFrom);
                            var reservedTimeTo = moment(reserved.serviceDatetimeTo);

                            return timeFrom.isSameOrBefore(reservedTimeFrom) && timeTo.isAfter(reservedTimeFrom) ||
                                   timeFrom.isBefore(reservedTimeTo) && timeTo.isSameOrAfter(reservedTimeTo) ||
                                   timeFrom.isSameOrBefore(reservedTimeFrom) &&  timeTo.isSameOrAfter(reservedTimeTo) || 
                                   timeFrom.isSameOrAfter(reservedTimeFrom) &&  timeTo.isSameOrBefore(reservedTimeTo)
                        }).map(function (agent) {
                            return agent.employeeID;
                        });
                        
                        var agentsLeave = leaves.filter(function(leave){
                            return moment(leave.start).isSameOrBefore(date,'day') &&
                                   moment(leave.end).isSameOrAfter(date,'day');
                        }).map(function (leave) {
                            return leave.employeeID;
                        });

                        // combine leave and occupied
                        agentsOccupied = agentsOccupied.concat(agentsLeave).filter(function(item, pos, self) {
                            return self.indexOf(item) == pos;
                        })
                        
                        timeslot.agent = {
                            free: agents.filter(function (agent) {
                                return agentsOccupied.indexOf(agent.ID) < 0;
                            }).map(function (agent) {
                                return agent.ID;
                            }),
                            occupied: agentsOccupied
                        };

                        return timeslot;


                    });

                    var timeslotsHTML = timeslotsNew.map(function (timeslot) {
                        return template.timeslot.replace(/{{timeslot.from}}/g, moment(timeslot.timeFrom).format("hh:mm A"))
                                                .replace(/{{timeslot.to}}/g, moment(timeslot.timeTo).format("hh:mm A"))
                                                .replace(/{{agents}}/g, timeslot.agent.free.join(",") || "")
                                                .replace(/{{timeslot.occupied}}/g, (timeslot.agent.free && timeslot.agent.free.length > 0 ? "" : "occupied"))

                    });
                    
                    var isDayOccupeid = [].concat.apply([], timeslotsNew.map(function (timeslot) {
                        return timeslot.agent.free;
                    })).length <= 0;


                    var css = [past_current_future];
                    if (isDayOccupeid) {
                        css.push("occupied");
                    }
                    else {
                        css.push("free");
                    }

                    

                    if (past_current_future == "past") {
                        css = ["past"]; // if past day, overwrite all css class
                    }
                    else if (holidays.length > 0 || week.length > 0) {
                        css = ["holiday"]; // if holiday, overwrite all css class
                    }
                    
                    css = css.join(" ");


                    templateDays.push(template.day
                        .replace("{{class}}", css)
                        .replace("{{date}}", date)
                        .replace("{{day}}", i)
                        .replace("{{timeslot.picker}}",
                        template.timepicker
                            .replace("{{timeslot.list}}", timeslotsHTML.join(""))
                            .replace("{{timeslot.info}}", "")));
                }

                // add days suffix
                var monthSuffix = Math.abs(7 - month.add(1, 'month').subtract(1, 'day').weekday());
                for (var i = 1; i <= monthSuffix; i++) {
                    templateDays.push(template.day
                        .replace("{{class}}", "empty")
                        .replace("{{day}}", "")
                        .replace("{{timeslot.picker}}", ""));
                }

                // add row
                templateDays = templateDays.reduce(function (prev, curr, index) {
                    if (index % 7 == 0) { prev.push("<div class='calendar__day__row'>");}
                    prev.push(curr);
                    if (index % 7 == 6) { prev.push("</div>"); }
                    return prev;

                }, []);

                return template.month
                    .replace("{{days}}", templateDays.join(""))
                    .replace("{{month}}", month.format("MMMM YYYY"));
            });

            self.container.append(template
                .container
                .replace("{{months}}", templateMonths.join("")));
        }

        var bindClick = function (self) {
            var config = self.config;
            var reservation = self.reservation;
            var parent = self.parent;

            $(".calendar__day.free").on("click", function (e) {
                self.reset();

                var $thisDayContainer = $(this),
                    $thisTimeContainer = $thisDayContainer.find(".timepicker"),
                    $thisRowContainer = $thisDayContainer.parent(),
                    date = moment($thisDayContainer.attr("data-date")).format("DD MMM YYYY");


                parent.addClass("day-selected");

                $thisRowContainer.addClass("selected");
                $thisDayContainer.addClass("selected");

                $thisTimeContainer.addClass("active");
                $thisTimeContainer.find(".timerpicker__date").html(date);

                e.preventDefault();
                e.stopPropagation();
            });
            $(".calendar__day.occupied").on("click", function (e) {
                var date = moment($(this).attr("data-date")).format("DD MMM YYYY");
                new Noty({
                    text: date + ' timeslot is full.',
                    closeWith: ['click', 'button'],
                    timeout: 2000,
                    layout: 'bottomRight',
                    theme: 'sunset',
                }).show();
            });
            $(".calendar__day.holiday").on("click", function (e) {
                var date = moment($(this).attr("data-date")).format("DD MMM YYYY");
                new Noty({
                    text: "We don't work at " + date + ".",
                    closeWith: ['click', 'button'],
                    timeout: 2000,
                    layout: 'bottomRight',
                    theme: 'sunset',
                }).show();
            });

            $(".timerpicker__submit").on("click", function (e) {
                var href = $("#<%= lnkbtnSubmit.ClientID %>").attr("href");
                eval(href);
                e.preventDefault();
                e.stopPropagation();
            });

            $(".timerpicker__close").on("click", function (e) {
                self.reset();
                e.preventDefault();
                e.stopPropagation();
            });

            $(".timerpicker__item").on("click", function (e) {
                var $this = $(this);

                if($this.hasClass("occupied")){
                    new Noty({
                        text: 'timeslot is full.',
                        closeWith: ['click', 'button'],
                        timeout: 2000,
                        layout: 'bottomRight',
                        theme: 'sunset',
                    }).show();
                    return ;
                }

                $(".timerpicker__item").removeClass("selected");
                $this.addClass("selected");

                var date = $(".calendar__day.selected").attr("data-date"),
                    timeFrom = moment(date + " " + $this.attr("data-timeslot-from"), "YYYY-MM-DD HH:mm A"),
                    timeTo = moment(date + " " + $this.attr("data-timeslot-to"), "YYYY-MM-DD HH:mm A"),
                    agents = $this.attr("data-agents");

                var ServiceConfig = {
                    from: timeFrom.format("DD MMM YYYY HH:mm:ss"),
                    to: timeTo.format("DD MMM YYYY HH:mm:ss"),
                    agents: agents.split(',').filter(function (x) { return (x); })
                };
                //console.log(ServiceConfig);
                $("#hdnServiceConfig").val(JSON.stringify(ServiceConfig));
                $(".timerpicker__info").html(timeFrom.format("hh:mm A") + " to " + timeTo.format("hh:mm A"));

                $(".calendar__day[data-date='" + date+"'] .timerpicker__item").each(function () {
                    var $item = $(this);

                    var itemTimeFrom = moment(date + " " + $item.attr("data-timeslot-from"), "YYYY-MM-DD HH:mm A");
                    var itemTimeTo = moment(date + " " + $item.attr("data-timeslot-to"), "YYYY-MM-DD HH:mm A");
                    if (timeFrom.isSameOrBefore(itemTimeFrom) && timeTo.isAfter(itemTimeFrom)) {
                        $item.addClass("selected");
                    }
                });

                e.preventDefault();
                e.stopPropagation();
            });



        }
    }

    Calendar.prototype.reset = function () {
        // selected datetime
        $("#hdnServiceConfig").val("");

        // timepicker
        $(".timerpicker__item").removeClass("selected");
        $(".timerpicker__date").html("");

        // datepicker
        this.container.find(".calendar__day__row").removeClass("selected");
        this.container.find(".calendar__day").removeClass("selected");

        // container
        this.parent.removeClass("day-selected");
    };

    $(function () {
        var calendar = new Calendar(<%= Newtonsoft.Json.JsonConvert.SerializeObject(dictReturn) %>);
        calendar.init();
    });
</script>