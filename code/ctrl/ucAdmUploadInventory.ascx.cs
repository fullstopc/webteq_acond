﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Drawing.Imaging;

public partial class ctrl_ucAdmUploadInventory : System.Web.UI.UserControl
{
     #region "Properties"
    protected int _mode = 1;
    protected string _pageListingURL = "";
    protected int _invUploadId = 0;
    protected string InventoryFile;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int mode
    {
        get { return _mode; }
        set { _mode = value; }
    }

    public string pageListingURL
    {
        get { return _pageListingURL; }
        set { _pageListingURL = value; }
    }

    public int invUploadId
    {
        get { return _invUploadId; }
        set { _invUploadId = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void validateInventory_server(object source, ServerValidateEventArgs args)
    {
        if (fileInventory.HasFile)
        {
            Match matchFile = Regex.Match(fileInventory.FileName, clsAdmin.CONSTADMINVENTORYALLOWEDEXT, RegexOptions.IgnoreCase);
            if (matchFile.Success)
            {
                args.IsValid = true;
            }
            else
            {
                cvInventory.ErrorMessage = "<br />Please enter valid file. (" + clsMis.formatAllowedFileExt(clsAdmin.CONSTADMINVENTORYALLOWEDEXT) + ").";
                args.IsValid = false;
            }
        }
    }

    protected void cvInventory_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("ProdInventory")))
        {
            string strJS = null;
            strJS = "<script type = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + fileInventory.ClientID + "', document.all['" + cvInventory.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "ProdInventory", strJS, false);
        }
    }

    protected void lnkbtnProcess_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            litFileName.Text = "";
            litUploadResult.Text = "Importing..";

            HttpPostedFile postedFile = fileInventory.PostedFile;
            string fileName = Path.GetFileName(postedFile.FileName);
            string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTINVENTORYFOLDER + "/";
            clsMis.createFolder(uploadPath);
            string strNewFile = uploadPath + fileName;
            postedFile.SaveAs(strNewFile);
            InventoryFile = fileName;

            int intUpdateMethod = !string.IsNullOrEmpty(ddlUpdateMethod.SelectedValue) ? int.Parse(ddlUpdateMethod.SelectedValue) : 0;

            clsInventory invt = new clsInventory();

            if (fileInventory.HasFile)
            {
                int intLine = 0;
                int intLineInsert = 0;
                int intLineUpdate = 0;
                int intInvalidLine = 0;
                string strInvalidText = "";
                string strInvalidInsertText = "";
                string strInvalidUpdateText = "";
                string strRemarks = "";
                string strRecord = "";
                string[] strRecordSplit;

                string strItemCode = "";
                string strProdCode = "";
                string strSize = "";
                string strQty = "";
                int intTryParse;

                Boolean boolProdExist = false;
                int intProdId = 0;
                int intColorId = 0;
                int intSizeId = 0;

                string strInsert = "";
                string strUpdate = "";

                clsProduct prod = new clsProduct();
                int intRecordAffected = 0;

                // read file
                StreamReader sr = new StreamReader(strNewFile);
                while ((strRecord = sr.ReadLine()) != null)
                {
                    intLine += 1;

                    strRecordSplit = strRecord.Split(',');

                    if (strRecordSplit.Length >= 2)
                    {
                        strItemCode = strRecordSplit[0];

                        //if (int.TryParse(strRecordSplit[1], out intTryParse))
                        //{
                            //intQty = intTryParse;
                        strQty = strRecordSplit[1];

                            try
                            {
                                //First 4 char is product code
                                strProdCode = !string.IsNullOrEmpty(strItemCode) ? strItemCode.Substring(0, strItemCode.Length - 2) : strItemCode;

                                strItemCode = HttpUtility.HtmlEncode(strItemCode);
                                strProdCode = HttpUtility.HtmlEncode(strProdCode);

                                boolProdExist = prod.isProdCodeItemCodeExist(strItemCode, strProdCode);

                                if (boolProdExist)
                                {
                                    intProdId = prod.prodId;

                                    if (!invt.isInventoryExist(intProdId))
                                    {
                                        intLineInsert += 1;
                                        strInvalidInsertText += "Row " + intLine + ", ";
                                        strInsert += "(" + intProdId + "," + strQty.ToString() + ",SYSDATE()),";
                                    }
                                    else
                                    {
                                        if (intUpdateMethod == 1)
                                        {
                                            intLineUpdate += 1;
                                            strInvalidUpdateText += "Row " + intLine + ", ";
                                            strUpdate += "INV_QTY = IF(PROD_ID = " + intProdId + " ," + strQty.ToString() + ", INV_QTY),INV_LASTUPDATE =IF(PROD_ID = " + intProdId + " ,sysdate(), INV_LASTUPDATE),";
                                            
                                        }
                                        else if (intUpdateMethod == 2)
                                        {
                                            invt.extractProdQty(intProdId);

                                            int intOldQty = int.Parse(invt.invQty);
                                            int intNewQty = 0;
                                            intNewQty = intOldQty + (int.Parse(strQty));

                                            intLineUpdate += 1;
                                            strInvalidUpdateText += "Row " + intLine + ", ";
                                            strUpdate += "INV_QTY = IF(PROD_ID = " + intProdId + " ," + intNewQty.ToString() + ", INV_QTY),INV_LASTUPDATE =IF(PROD_ID = " + intProdId + " ,sysdate(), INV_LASTUPDATE),";
                                           
                                        }
                                    }
                                }
                                else
                                {
                                    intInvalidLine += 1;
                                    strInvalidText += "Row " + intLine + ", ";
                                }
                            }
                            catch (Exception ex)
                            {
                                intInvalidLine += 1;
                                strInvalidText += "Row " + intLine + ", ";
                            }
                        //}
                        //else
                        //{
                        //    intInvalidLine += 1;
                        //    strInvalidText += "Row " + intLine + ", ";
                        //}
                    }
                    else
                    {
                        intInvalidLine += 1;
                        strInvalidText += "Row " + intLine + ", ";
                    }
                }

                if (strInsert != "")
                {
                    intRecordAffected = invt.addInventoryMulti(strInsert, intLineInsert);
                    if (intRecordAffected != intLineInsert)
                    {
                        strInvalidText += strInvalidInsertText;
                        intInvalidLine += intLineInsert;
                        intLineInsert = 0;
                    }
                }

                if (strUpdate != "")
                {
                    intRecordAffected = invt.updateInventoryMulti(strUpdate, intLineUpdate);
                    if (intRecordAffected <= 0)
                    {
                        strInvalidText += strInvalidUpdateText;
                        intInvalidLine += intLineUpdate;
                        intLineUpdate = 0;
                    }
                }

                sr.Close();

                
                string strImportResult = "Total " + intLine + " record(s), " + intLineInsert + " new record(s), " + intLineUpdate + " existing record(s), " + intInvalidLine + " invalid record(s).<br /><br />";
                string strImportRemarks = strRemarks;
                string strImportFailedRemarks = strInvalidText.Trim().Length > 0 ? strInvalidText.Remove(strInvalidText.Trim().Length - 1) : strInvalidText.Trim();

                invt.addInventoryImport(InventoryFile, strImportResult, strImportFailedRemarks, intUpdateMethod);

                litFileName.Text = InventoryFile + "(" + ddlUpdateMethod.Text + ")";
                litUploadResult.Text = "Total Records: " + intLine + "<br/> Successful: " + (intLineInsert + intLineUpdate) + "<br/> Failed: " + strImportFailedRemarks;
            }
        }
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        clsMis mis = new clsMis();
        DataSet dsUpdateMethod = new DataSet();
        dsUpdateMethod = mis.getListByListGrp(clsAdmin.CONSTADMUPLOADMETHOD, 1);

        DataView dvUpdateMethod = new DataView(dsUpdateMethod.Tables[0]);

        ddlUpdateMethod.DataSource = dvUpdateMethod;
        ddlUpdateMethod.DataTextField = "LIST_NAME";
        ddlUpdateMethod.DataValueField = "LIST_VALUE";
        ddlUpdateMethod.DataBind();

        ListItem ddlUpdateMethodDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlUpdateMethod.Items.Insert(0, ddlUpdateMethodDefaultItem);

        litUploadRule.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMINVENTORYALLOWEDEXT);
    }
    #endregion
}