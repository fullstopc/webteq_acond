﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmShippingWithWeight.ascx.cs" Inherits="ctrl_ucAdmShippingWithWeight" %>

<script type="text/javascript">
    function validateShippingFee_client(source, args) {
        var txtSubSeqFee = document.getElementById("<%= txtSubSeqFee.ClientID %>");
        var currRE = /<%= clsAdmin.CONSTCURRENCYRE %>/;
        

        if (Trim(args.Value) != '') {
            if (args.Value.match(currRE)) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
                source.errormessage = "<br />Please enter only currency.";
                source.innerHTML = "<br />Please enter only currency.";
            }   
        }
    }

    function validateShippingWeight_client(source, args) {
        var txtFirstWeight = document.getElementById("<%= txtFirstWeight.ClientID %>");
        var currWeight = /<%= clsAdmin.CONSTCURRENCYRE %>/;

        if (Trim(args.Value) != '') {
            if (args.Value.match(currWeight)) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
                source.errormessage = "<br/>Please enter only number.";
                source.innerHTML = "<br/>Please enter only number.";
            }
        }
    }
</script>

<asp:Panel ID="pnlShippingDetails" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
            <thead>
                <tr>
                    <td colspan="4" class="tdSectionHdr">Shipping Details</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="4" class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel">Country<span class="attention_compulsory">*</span>:</td>
                    <td id="tdCountryDdl" runat="server">
                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="ddl compulsory" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ErrorMessage="<br />Please select one country." Display="Dynamic" CssClass="errmsg" ControlToValidate="ddlCountry"></asp:RequiredFieldValidator>
                    </td>
                    <td id="tdCountryChkbox" runat="server" class="tdWithCheckbox" visible="false"><asp:CheckBox ID="chkboxCountry" runat="server" Text="Show Textbox" /></td>
                </tr>
                <tr id="trState" runat="server" visible="false">
                    <td class="tdLabel">State:</td>
                    <td id="tdStateDdl" runat="server" visible="false"><asp:DropDownList ID="ddlState" runat="server" CssClass="ddl"></asp:DropDownList></td>
                    <td id="tdStatetxtbox" runat="server"><asp:TextBox ID="txtState" runat="server" CssClass="text"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvState" runat="server" ErrorMessage="<br/>Please enter state." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtState"></asp:RequiredFieldValidator></td>
                    <td id="tdStateChkbox" colspan="2" runat="server" class="tdWithCheckbox" visible="false"><asp:CheckBox ID="chkboxState" runat="server" Text="Show Textbox" /></td>
                </tr>
                <tr id="trCity" runat="server" visible="false">
                    <td class="tdLabel">City:</td>
                    <td><asp:TextBox ID="txtCity" runat="server" CssClass="text"></asp:TextBox></td>
                    <%--<asp:RequiredFieldValidator ID="rfvCity" runat="server" ErrorMessage="<br/>Please enter city." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtCity"></asp:RequiredFieldValidator>--%>
                    <td colspan="2" class="tdWithCheckbox"><asp:CheckBox ID="chkboxCity" runat="server" Text="Show Textbox" /></td>
                </tr>
                <tr id="trLeadTime" runat="server" visible="false">
                    <td class="tdLabel nobr">Lead Time<span class="attention_compulsory">*</span>:</td> 
                    <td colspan="3">
                        <asp:TextBox ID="txtLeadTime" runat="server" CssClass="text_item" MaxLength="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvLeadTime" runat="server" ErrorMessage="<br/>Please enter lead time." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtLeadTime"></asp:RequiredFieldValidator>
                        <asp:Label ID="lblLeadTimeUnit" runat="server"></asp:Label>
                    </td>           
                </tr>
                <tr id="trShippingFee" runat="server" visible="false">
                    <td class="tdLabel nobr">Shipping Cost Per Item<span class="attention_compulsory">*</span>:</td> 
                    <td colspan="3">
                        <asp:Label ID="lblShippingFee" runat="server"></asp:Label>
                        <asp:TextBox ID="txtShippingFee" runat="server" CssClass="text_item" MaxLength="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvShippingFee" runat="server" ErrorMessage="<br/>Please enter shipping cost per item." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtShippingFee"></asp:RequiredFieldValidator>
                        
                    </td>           
                </tr>
                <tr id="trFreeAfterCost" runat="server" visible="false">
                    <td class="tdLabel nobr">Free After<span class="attention_compulsory">*</span>:</td> 
                    <td colspan="3">
                        <asp:Label ID="lblFreeAfter" runat="server"></asp:Label>
                        <asp:TextBox ID="txtFreeAfter" runat="server" CssClass="text_item" MaxLength="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvFreeAfter" runat="server" ErrorMessage="<br/>Please enter free after." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtFreeAfter"></asp:RequiredFieldValidator>
                    </td>           
                </tr>
                <tr id="trFirstFee" runat="server">
                    <td class="tdLabel"><asp:Literal ID="litFirstFee" runat="server"></asp:Literal><span class="attention_compulsory">*</span>:</td> 
                    <td>
                        <asp:TextBox ID="txtFirstWeight" runat="server" CssClass="text_medium compulsory" MaxLength="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvFirstWeight" runat="server" ErrorMessage="<br/>Please enter first block value." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtFirstWeight"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cvFirstWeight" runat="server" ControlToValidate="txtFirstWeight" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateShippingWeight_client"></asp:CustomValidator>
                    </td>           
                    <td class="tdLabel">
                        <asp:Literal ID="litFirstFeeUnit" runat="server"></asp:Literal>
                    </td>
                    <td><asp:TextBox ID="txtFirstFee" runat="server" MaxLength="10" CssClass="text_medium compulsory" onchange="formatCurrency(this.value)"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvFirstFee" runat="server" ErrorMessage="<br />Please enter shipping fee." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtFirstFee"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cvFirstFee" runat="server" ControlToValidate="txtFirstFee" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateShippingFee_client"></asp:CustomValidator>
                    </td>
                </tr>
                <tr id="trSubSeqFee" runat="server">
                    <td class="tdLabel nobr"><asp:Literal ID="litSubSeqFee" runat="server"></asp:Literal><span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtSubSeqWeight" runat="server" CssClass="text_medium compulsory" MaxLength="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvSubSeqWeight" runat="server" ErrorMessage="<br/>Please enter subsequent block value." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtSubSeqWeight"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cvSubSeqWeight" runat="server" ControlToValidate="txtSubSeqWeight" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateShippingWeight_client"></asp:CustomValidator>
                    </td>
                    <td class="tdLabel">
                        <asp:Literal ID="litSubSeqFeeUnit" runat="server"></asp:Literal>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSubSeqFee" runat="server" MaxLength="10" CssClass="text_medium compulsory" onchange="formatCurrency(this.value)"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvSubSeqFee" runat="server" ErrorMessage="<br />Please enter shipping fee." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtSubSeqFee"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cvSubSeqFee" runat="server" ControlToValidate="txtSubSeqFee" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateShippingFee_client"></asp:CustomValidator>
                    </td>
                </tr>
                <tr id="trFreeAfter" runat="server" visible="false">
                    <td><asp:Literal ID="litFreeShippingFee" runat="server"></asp:Literal>:</td>
                    <td colspan="3">
                        <span style="padding-right: 10px;"><asp:Literal ID="litCurrencyCode" runat="server"></asp:Literal></span>
                        <asp:TextBox ID="txtFreeShippingWeight" runat="server" MaxLength="10" CssClass="text_medium"></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="rfvFreeShippingWeight" runat="server" ErrorMessage="<br/>Please enter value." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtFreeShippingWeight"></asp:RequiredFieldValidator>--%>
                        <asp:CustomValidator ID="cvFreeShippingWeight" runat="server" ControlToValidate="txtFreeShippingWeight" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateShippingWeight_client"></asp:CustomValidator>
                    </td>
                </tr>
                <tr id="trSpacer" runat="server">
                    <td colspan="4" class="tdSpacer">&nbsp;</td>
                </tr>
            </tbody>
        <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
            <thead>
                <tr id="trSectionHeader" runat="server">
                    <td colspan="4" class="tdSectionHdr">Shipping Settings</td>
                </tr>
            </thead>
            <tbody>
                <tr id="trSpacer2" runat="server">
                    <td colspan="4" class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel">Active:</td>
                    <td colspan="3" class="tdWithCheckbox"><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
                </tr>
                <tr>
                    <td colspan="4" class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4" class="tdSpacer">&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
        <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
            <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack" CausesValidation="false"></asp:LinkButton>
        </asp:Panel>
        <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
            <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CssClass="btn btnCancel" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
        </asp:Panel>
    </asp:Panel>
</asp:Panel>
