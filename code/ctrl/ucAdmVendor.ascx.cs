﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ctrl_ucAdmVendor : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected string _pageListingURL = "";
    protected int _memId = 0;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int mode
    {
        get { return _mode; }
        set { _mode = value; }
    }

    public string pageListingURL
    {
        get { return _pageListingURL; }
        set { _pageListingURL = value; }
    }

    public int memId
    {
        get { return _memId; }
        set { _memId = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void validateProdCode_server(object source, ServerValidateEventArgs args)
    {
        string strCode = txtProdCode.Text.Trim();
        if (!string.IsNullOrEmpty(strCode))
        {
            clsVendor vendor = new clsVendor();
            if (vendor.isCodeExist(strCode, memId))
            {
                args.IsValid = false;
                cvProdCode.ErrorMessage = "<br />This code is in use.";
            }
            else
            {
                args.IsValid = true;
            }
        }
    }

    protected void validateEmail_server(object source, ServerValidateEventArgs args)
    {
        string strEmail = txtEmail.Text.Trim();
        if (!string.IsNullOrEmpty(strEmail))
        {
            clsVendor vendor = new clsVendor();
            if (vendor.isEmailExist(strEmail, memId))
            {
                args.IsValid = false;
                cvEmail.ErrorMessage = "<br />This email is in use.";
            }
            else
            {
                args.IsValid = true;
            }
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strCode = txtProdCode.Text.Trim();
            string strName = txtName.Text.Trim();
            string strEmail = txtEmail.Text.Trim();
            string strContactNo = txtContactNo.Text.Trim();
            string strFax = txtFax.Text.Trim();
            string strAdd = txtAddress.Text.Trim();
            int intActive = chkboxActive.Checked ? 1 : 0;

            clsVendor vendor = new clsVendor();
            int intRecordAffected = 0;

            if (mode == 1)
            {
                intRecordAffected = vendor.addMember(strCode, strName, strEmail, strContactNo, strFax, strAdd, intActive, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1)
                {
                    Session["NEWMEMID"] = vendor.vendorId;
                    Response.Redirect(currentPageName + "?id=" + vendor.vendorId);
                }
                else
                {
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new vendor. Please try again.</div>";
                    Response.Redirect(currentPageName);
                }
            }
            else if (mode == 2)
            {
                Boolean boolSuccess = true;
                Boolean boolEdited = false;

                if (!vendor.isExactSameMemberSet(memId, strCode, strName, strEmail, strContactNo, strFax, strAdd, intActive))
                {
                    intRecordAffected = vendor.updateMemberById(memId, strCode, strName, strEmail, strContactNo, strFax, strAdd, intActive, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; }
                }

                if (boolEdited)
                {
                    Session["EDITMEMID"] = vendor.vendorId;
                }
                else
                {
                    if (!boolSuccess)
                    {
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text").ToString() + "<br />Failed to edit vendor ('" + strName + "'). Please try again.</div>";
                    }
                    else
                    {
                        Session["EDITMEMID"] = memId;
                        Session["NOCHANGE"] = 1;
                    }
                }

                Response.Redirect(Request.Url.ToString());
            }
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string strDeletedMemEmail = "";

        clsMis.performDeleteVendor(memId, ref strDeletedMemEmail);
        Session["DELETEDMEMEMAIL"] = strDeletedMemEmail;

        Response.Redirect(currentPageName);
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void setPageProperties()
    {
        if (mode == 2)
        {
            fillForm();

            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteVendor.Text").ToString() + "');";
            lnkbtnDelete.Visible = true;
        }

        lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
    }

    protected void fillForm()
    {
        clsVendor vendor = new clsVendor();
        if (vendor.extractMemberById(memId, 0))
        {
            txtProdCode.Text = vendor.vendorCode;
            txtName.Text = vendor.vendorName;
            txtEmail.Text = vendor.vendorEmail;
            txtContactNo.Text = vendor.vendorContactNo;
            txtFax.Text = vendor.vendorFax;
            txtAddress.Text = vendor.vendorAddress;
            chkboxActive.Checked = vendor.vendorActive != 0;
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }
    #endregion
}
