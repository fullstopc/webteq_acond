﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using System.Data;

public partial class ctrl_ucAdmProductDesc : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected int _prodId = 0;
    protected string _pageListingURL = "";

    protected string prodSnapshot;
    protected string prodSnapshotJp;
    protected string prodSnapshotMs;
    protected string prodSnapshotZh;

    /* Added By Soo Shin -- 2013-12-17 */
    protected string prodRemarks;
    protected string prodRemarksJp;
    protected string prodRemarksMs;
    protected string prodRemarksZh;
    /* END */

    protected string prodDesc;
    protected string prodDescJp;
    protected string prodDescMs;
    protected string prodDescZh;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName2(true); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int prodId
    {
        get
        {
            if (ViewState["PRODID"] == null)
            {
                return _prodId;
            }
            else
            {
                return int.Parse(ViewState["PRODID"].ToString());
            }
        }
        set { ViewState["PRODID"] = value; }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void lnkbtnRelArt_OnClick(object sender, EventArgs e)
    {
    }

    protected void fileUploadItems_OnDelete(object sender, CommandEventArgs e)
    {
        string strFileUploadItemToDelete = e.CommandArgument.ToString();
        string[] strSplit = strFileUploadItemToDelete.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

        Session["FILETITLE"] = Session["FILETITLE"].ToString().Replace(strSplit[0] + clsAdmin.CONSTDEFAULTSEPERATOR, "");
        Session["FILEUPLOAD"] = Session["FILEUPLOAD"].ToString().Replace(strSplit[1] + clsAdmin.CONSTDEFAULTSEPERATOR, "");
        File.Delete(Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMTEMPFOLDER + "/" + strSplit[1]);

        populateFileUpload();
    }

    protected void oriFileUploadItems_OnDelete(object sender, CommandEventArgs e)
    {
        string strFileUploadItemToDelete = e.CommandArgument.ToString();
        string[] strSplit = strFileUploadItemToDelete.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

        Session["DELFILETITLE"] += strSplit[0] + clsAdmin.CONSTDEFAULTSEPERATOR;
        Session["DELFILEUPLOAD"] += strSplit[1] + clsAdmin.CONSTDEFAULTSEPERATOR;

        populateFileUpload();
    }

    #region "Product Description List"
    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int intProdDescId = !string.IsNullOrEmpty(hdnProdDesc.Value.Trim()) ? Convert.ToInt16(hdnProdDesc.Value.Trim()) : 0;
            string strProdDescTitle = txtProdDescTitle.Text.Trim();
            string strProdDescContent = txtProdDescContent.Text.Trim();

            if(intProdDescId == 0)
            {
                addItem(0, strProdDescTitle, strProdDescContent);
            }
            else
            {
                updateItemById(intProdDescId, strProdDescTitle, strProdDescContent);
            }

            hdnProdDesc.Value = "";
            txtProdDescTitle.Text = "";
            txtProdDescContent.Text = "";
            lnkbtnAdd.Text = lnkbtnAdd.ToolTip = "Add";

            bindRptData();
            if (txtProdDescContent.Visible == true)
            {
                registerCKEditorScript(txtProdDescContent.ClientID);
            }
        }
    }

    protected void rptProdDesc_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strProdDescTitle = DataBinder.Eval(e.Item.DataItem, "PRODDESC_TITLE").ToString();
            string strProdDescContent = DataBinder.Eval(e.Item.DataItem, "PRODDESC_DESC").ToString();

            LinkButton lnkbtnEdit = (LinkButton)e.Item.FindControl("lnkbtnEdit");
            lnkbtnEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();

            LinkButton lnkbtnDelete = (LinkButton)e.Item.FindControl("lnkbtnDelete");
            lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
        }
    }

    protected void rptProdDesc_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdEdit")
        {
            int intProdDescTempId = Convert.ToInt16(e.CommandArgument);
            extractItemById(intProdDescTempId);

            if (txtProdDescContent.Visible == true)
            {
                registerCKEditorScript(txtProdDescContent.ClientID);
            }
        }
        else if (e.CommandName == "cmdDelete")
        {
            int intProdDescTempId = Convert.ToInt16(e.CommandArgument);
            setDelete(intProdDescTempId);

            bindRptData();
        }
    }
    #endregion

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (mode == 2)
            {
                Boolean bEdited = false;
                Boolean bSuccess = true;
                clsProduct prod = new clsProduct();
                int intRecordAffected = 0;

                try
                {
                    string strFileTitle = Session["FILETITLE"].ToString();
                    string strFileUpload = Session["FILEUPLOAD"].ToString();
                    if (strFileTitle != "" && strFileUpload != "")
                    {
                        strFileTitle = strFileTitle.Substring(0, strFileTitle.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                        strFileUpload = strFileUpload.Substring(0, strFileUpload.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                        string[] fileTitleItems = strFileTitle.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                        string[] fileUploadItems = strFileUpload.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                        string strTempPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMTEMPFOLDER + "/";
                        string strNewPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPRODFOLDER + "/" + clsAdmin.CONSTADMPRODARTICLEFOLDER + "/";

                        for (int i = 0; i <= fileTitleItems.GetUpperBound(0); i++)
                        {
                            //add related article
                            intRecordAffected = prod.addProdArticle(prodId, fileTitleItems[i], fileUploadItems[i]);
                            if (intRecordAffected == 1)
                            {
                                File.Move(strTempPath + fileUploadItems[i], strNewPath + fileUploadItems[i]);
                                Session["FILETITLE"] = Session["FILETITLE"].ToString().Replace(fileTitleItems[i] + clsAdmin.CONSTDEFAULTSEPERATOR, "");
                                Session["FILEUPLOAD"] = Session["FILEUPLOAD"].ToString().Replace(fileUploadItems[i] + clsAdmin.CONSTDEFAULTSEPERATOR, "");
                                bEdited = true;
                            }
                        }
                    }
                }
                catch (Exception ex) { throw ex; }

                try
                {
                    string strDelFileTitle = Session["DELFILETITLE"].ToString();
                    string strDelFileUpload = Session["DELFILEUPLOAD"].ToString();
                    if (strDelFileTitle != "" && strDelFileUpload != "")
                    {
                        strDelFileTitle = strDelFileTitle.Substring(0, strDelFileTitle.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                        strDelFileUpload = strDelFileUpload.Substring(0, strDelFileUpload.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                        string[] delFileTitleItems = strDelFileTitle.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                        string[] delFileUploadItems = strDelFileUpload.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                        string strDelPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPRODFOLDER + "/" + clsAdmin.CONSTADMPRODARTICLEFOLDER + "/";

                        for (int i = 0; i <= delFileTitleItems.GetUpperBound(0); i++)
                        {
                            //add related article
                            intRecordAffected = prod.deleteProdArticle(prodId, delFileTitleItems[i], delFileUploadItems[i]);
                            if (intRecordAffected == 1)
                            {
                                File.Delete(strDelPath + delFileUploadItems[i]);
                                Session["DELFILETITLE"] = Session["DELFILETITLE"].ToString().Replace(delFileTitleItems[i] + clsAdmin.CONSTDEFAULTSEPERATOR, "");
                                Session["DELFILEUPLOAD"] = Session["DELFILEUPLOAD"].ToString().Replace(delFileUploadItems[i] + clsAdmin.CONSTDEFAULTSEPERATOR, "");
                                bEdited = true;
                            }
                        }
                    }
                }
                catch (Exception ex) { throw ex; }

                prodSnapshot = txtProdSnapshot.Text.Trim();
                prodSnapshotJp = txtProdSnapshotJp.Text.Trim();
                prodSnapshotMs = txtProdSnapshotMs.Text.Trim();
                prodSnapshotZh = txtProdSnapshotZh.Text.Trim();

                /* Added by Soo Shin -- 2013-12-17 */
                prodRemarks = txtProdRemarks.Text.Trim();
                prodRemarksJp = txtProdRemarksJp.Text.Trim();
                prodRemarksMs = txtProdRemarksMs.Text.Trim();
                prodRemarksZh = txtProdRemarksZh.Text.Trim();
                /* END */

                prodDesc = txtProdDesc.Text.Trim();
                prodDescJp = txtProdDescJp.Text.Trim();
                prodDescMs = txtProdDescMs.Text.Trim();
                prodDescZh = txtProdDescZh.Text.Trim();

                if (bEdited || !prod.isExactSameProdSet2(prodId, prodSnapshot, prodSnapshotJp, prodSnapshotMs, prodSnapshotZh, prodRemarks, prodRemarksJp, prodRemarksMs, prodRemarksZh, prodDesc, prodDescJp, prodDescMs, prodDescZh))
                {
                    intRecordAffected = prod.updateProdDescById(prodId, prodSnapshot, prodSnapshotJp, prodSnapshotMs, prodSnapshotZh, prodRemarks, prodRemarksJp, prodRemarksMs, prodRemarksZh, prodDesc, prodDescJp, prodDescMs, prodDescZh, int.Parse(Session["ADMID"].ToString()));
                    if (intRecordAffected == 1)
                    {
                        bEdited = true;
                    }
                    else
                    {
                        bSuccess = false;
                    }
                }

                #region "Product Description"
                if(Session[clsAdmin.CONSTPROJECTPRODUCTDESCRIPTION] != null && Session[clsAdmin.CONSTPROJECTPRODUCTDESCRIPTION] != "")
                {
                    if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTPRODUCTDESCRIPTION]) == 1)
                    {
                        if (bSuccess)
                        {
                            DataTable dt;

                            if (Session["TABLEDESC"] != null)
                            {
                                dt = (DataTable)Session["TABLEDESC"];

                                DataRow[] foundRow = dt.Select("PRODDESC_ID <> 0 AND PRODDESC_DELETED = 1");
                                foreach (DataRow row in foundRow)
                                {
                                    int intDescId = int.Parse(row["PRODDESC_ID"].ToString());
                                    intRecordAffected = prod.deleteProdDescItemById(intDescId);

                                    if (intRecordAffected == 1) { bEdited = true; }
                                    else
                                    {
                                        bSuccess = false;
                                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to delete product description. Please try again.</div>";
                                        break;
                                    }
                                }

                                if (bSuccess)
                                {
                                    foundRow = dt.Select("PRODDESC_ID <> 0 AND PRODDESC_EDITED = 1");
                                    foreach (DataRow row in foundRow)
                                    {
                                        if (!prod.isExactProdDescSameData(int.Parse(row["PRODDESC_ID"].ToString()), prodId, row["PRODDESC_TITLE"].ToString(), row["PRODDESC_DESC"].ToString()))
                                        {
                                            intRecordAffected = prod.updateProdDescItemById(int.Parse(row["PRODDESC_ID"].ToString()), prodId, row["PRODDESC_TITLE"].ToString(), row["PRODDESC_DESC"].ToString());

                                            if (intRecordAffected == 1) { bEdited = true; }
                                            else
                                            {
                                                bSuccess = false;
                                                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit product description. Please try again.</div>";
                                                break;
                                            }
                                        }
                                    }
                                }

                                if (bSuccess)
                                {
                                    foundRow = dt.Select("PRODDESC_ID = 0 AND PRODDESC_DELETED = 0");
                                    foreach (DataRow row in foundRow)
                                    {
                                        intRecordAffected = prod.addProdDescItem(prodId, row["PRODDESC_TITLE"].ToString(), row["PRODDESC_DESC"].ToString());

                                        if (intRecordAffected == 1) { bEdited = true; }
                                        else
                                        {
                                            bSuccess = false;
                                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add product description. Please try again.</div>";
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                if (bEdited)
                {
                    Session["EDITPRODID"] = prod.prodId;
                }
                else
                {
                    if (!bSuccess)
                    {
                        prod.extractProdById(prodId, 0);
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit product (" + prod.prodName + "). Please try again.</div>";
                    }
                    else
                    {
                        Session["EDITPRODID"] = prodId;
                        Session["NOCHANGE"] = 1;
                    }
                }

                Response.Redirect(Request.Url.ToString());
            }
        }
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack) 
        {
            if (mode == 2)
            {
                fillForm();
                bindRptData();
            }

            if (txtProdDescContent.Visible == true)
            {
                registerCKEditorScript(txtProdDescContent.ClientID);
            }
        }

        lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";

        checkLanguage();
        registerCKEditorScript(txtProdDesc.ClientID);
        
        populateFileUpload();
    }

    public void registerCKEditorScript(string strId)
    {
        Session["CMSPATH"] = "";
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR" + strId)))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">" +
                    "CKEDITOR.replace('" + strId + "'," +
                    "{" +
                    "height:'300'," +
                    "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSPRODVIEW + "&command=QuickUpload&type=Files&path=2'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSPRODVIEW + "&command=QuickUpload&type=Images&path=2'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSPRODVIEW + "&command=QuickUpload&type=Flash&path=2'" +
                     "}" +
                     ");";

            strJS += "CKEDITOR.add;";
            
            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMEEDITORSTYLE, clsConfig.CONSTGROUPWEBSITE, 1);

            string strEditorStyle = config.value;
            if (!string.IsNullOrEmpty(strEditorStyle))
            {
                strJS += "CKEDITOR.config.contentsCss = 'body {" + strEditorStyle + "}';";
            }
                
            strJS += "CKEDITOR.config.stylesCombo_stylesSet= '" + clsAdmin.CONSTADMSTYLESET + ":" + System.Configuration.ConfigurationManager.AppSettings["jsBase"] + clsAdmin.CONSTADMSTYLESETFILE + "';" +
                 "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR" + strId, strJS, false);
        }
    }

    //public void registerCKEditorScript2(string strId)
    //{
    //    Session["CMSPATH"] = "";
    //    string strJS = null;
    //    strJS = "<script type=\"text/javascript\">" +
    //            "var instance = CKEDITOR.instances['" + strId + "'];" +
    //            "var instanceData = CKEDITOR.instances['" + strId + "'].getData();" +
    //            "alert(instanceData);" +
    //            "if(instance)" +
    //            "{" +
    //            "    CKEDITOR.remove(instance);" +
    //            "}" +
    //            "CKEDITOR.replace('" + strId + "'," +
    //            "{" +
    //            "height:'300'," +
    //            "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
    //    strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSPRODVIEW + "&command=QuickUpload&type=Files&path=2'," +
    //             "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSPRODVIEW + "&command=QuickUpload&type=Images&path=2'," +
    //             "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSPRODVIEW + "&command=QuickUpload&type=Flash&path=2'" +
    //             "}" +
    //             ");";

    //    strJS += "CKEDITOR.add;";

    //    clsConfig config = new clsConfig();
    //    config.extractItemByNameGroup(clsConfig.CONSTNAMEEDITORSTYLE, clsConfig.CONSTGROUPWEBSITE, 1);
    //    string strEditorStyle = config.value;
    //    if (!string.IsNullOrEmpty(strEditorStyle))
    //    {
    //        strJS += "CKEDITOR.config.contentsCss = 'body {" + strEditorStyle + "}';";
    //    }

    //    strJS += "CKEDITOR.config.stylesCombo_stylesSet= '" + clsAdmin.CONSTADMSTYLESET + ":" + System.Configuration.ConfigurationManager.AppSettings["jsBase"] + clsAdmin.CONSTADMSTYLESETFILE + "';" +
    //         "</script>";

    //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "CKEDITOR" + strId, strJS, false);
    //}

    protected void populateFileUpload()
    {
        //System.Threading.Thread.Sleep(2000);
        pnlRelArt.Controls.Clear();
        litRelArt.Visible = false;
        litRelArt.Text = string.Empty;
        int intCount = 0;

        try
        {
            string strOriFileTitle = "";
            if (Session["ORIFILETITLE"] != null) { strOriFileTitle = Session["ORIFILETITLE"].ToString(); }

            string strOriFileUpload = "";
            if (Session["ORIFILEUPLOAD"] != null) { strOriFileUpload = Session["ORIFILEUPLOAD"].ToString(); }

            string strFileTitle = "";
            if (Session["FILETITLE"] != null) { strFileTitle = Session["FILETITLE"].ToString(); }

            string strFileUpload = "";
            if (Session["FILEUPLOAD"] != null) { strFileUpload = Session["FILEUPLOAD"].ToString(); }

            string strDelFileUpload = "";
            if (Session["DELFILEUPLOAD"] != null) { strDelFileUpload = Session["DELFILEUPLOAD"].ToString(); }

            if ((strOriFileTitle != "" && strOriFileUpload != "") || (strFileTitle != "" && strFileUpload != ""))
            {
                HtmlTable tbl = new HtmlTable();
                tbl.CellPadding = 0;
                tbl.CellSpacing = 0;
                tbl.Attributes.Add("class", "dataTbl");
                pnlRelArt.Controls.Add(tbl);
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cell1 = new HtmlTableCell("th");
                HtmlTableCell cell2 = new HtmlTableCell("th");
                HtmlTableCell cell3 = new HtmlTableCell("th");
                //HtmlGenericControl spanSplitter;

                // for table header
                cell1.Controls.Add(new LiteralControl("No."));
                cell2.Controls.Add(new LiteralControl("Article"));
                cell3.Controls.Add(new LiteralControl("Action"));

                cell1.Attributes["class"] = "tdNo";
                cell2.Attributes["style"] = "text-align:center;";
                cell3.Attributes["style"] = "text-align:center;";

                row.Controls.Add(cell1);
                row.Controls.Add(cell2);
                row.Controls.Add(cell3);
                tbl.Controls.Add(row);

                if (strOriFileTitle != "" && strOriFileUpload != "")
                {
                    strOriFileTitle = strOriFileTitle.Substring(0, strOriFileTitle.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    strOriFileUpload = strOriFileUpload.Substring(0, strOriFileUpload.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                    LinkButton lnkbtn;
                    string[] fileTitleItems = strOriFileTitle.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    string[] fileUploadItems = strOriFileUpload.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    for (int i = 0; i <= fileTitleItems.GetUpperBound(0); i++)
                    {
                        if (strDelFileUpload.IndexOf(fileUploadItems[i]) < 0)
                        {
                            intCount += 1;
                            var span = new HtmlGenericControl("span");
                            span.InnerHtml = (intCount).ToString();
                            cell1 = new HtmlTableCell();
                            cell1.Controls.Add(span);

                            lnkbtn = new LinkButton();
                            lnkbtn.CssClass = "lnkbtn lnkbtnEdit";
                            lnkbtn.ID = fileUploadItems[i] + "_" + i;
                            lnkbtn.Text = fileTitleItems[i];
                            lnkbtn.ToolTip = fileTitleItems[i];
                            lnkbtn.OnClientClick = "javascript:window.open('" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPRODFOLDER + "/" + clsAdmin.CONSTADMPRODARTICLEFOLDER + "/" + fileUploadItems[i] + "', 'relart', 'toolbar=0,menubar=0,height=800px,width=800px,resizable=1'); return false;";
                            cell2 = new HtmlTableCell();
                            cell2.Attributes["style"] = "text-align: center;width:45%;";
                            cell2.Controls.Add(lnkbtn);

                            //spanSplitter = new HtmlGenericControl("span");
                            //spanSplitter.Attributes["class"] = "spanSplitter";
                            //spanSplitter.InnerHtml = " | ";
                            //cell2.Controls.Add(spanSplitter);

                            lnkbtn = new LinkButton();
                            lnkbtn.CssClass = "lnkbtn lnkbtnDelete";
                            lnkbtn.ID = fileUploadItems[i] + "_delete_" + i;
                            lnkbtn.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
                            lnkbtn.ToolTip = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
                            lnkbtn.Command += new CommandEventHandler(oriFileUploadItems_OnDelete);
                            lnkbtn.CommandArgument = fileTitleItems[i] + clsAdmin.CONSTDEFAULTSEPERATOR + fileUploadItems[i];
                            cell3 = new HtmlTableCell();
                            cell3.Attributes["class"] = "tdAct";
                            cell3.Controls.Add(lnkbtn);

                            row = new HtmlTableRow();
                            row.Controls.Add(cell1);
                            row.Controls.Add(cell2);
                            row.Controls.Add(cell3);
                            tbl.Controls.Add(row);
                        }
                    }
                }

                if (strFileTitle != "" && strFileUpload != "")
                {
                    strFileTitle = strFileTitle.Substring(0, strFileTitle.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    strFileUpload = strFileUpload.Substring(0, strFileUpload.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                    LinkButton lnkbtn;
                    string[] fileTitleItems = strFileTitle.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    string[] fileUploadItems = strFileUpload.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                    for (int i = 0; i <= fileTitleItems.GetUpperBound(0); i++)
                    {
                        intCount += 1;

                        var span = new HtmlGenericControl("span");
                        span.InnerHtml = (intCount).ToString();
                        cell1 = new HtmlTableCell();
                        cell1.Controls.Add(span);

                        lnkbtn = new LinkButton();
                        lnkbtn.CssClass = "lnkbtn lnkbtnEdit";
                        lnkbtn.ID = fileUploadItems[i] + "_" + i;
                        lnkbtn.Text = fileTitleItems[i];
                        lnkbtn.OnClientClick = "javascript:window.open('" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMTEMPFOLDER + "/" + fileUploadItems[i] + "', 'relart', 'toolbar=0,menubar=0,height=800px,width=800px,resizable=1'); return false;";
                        cell2 = new HtmlTableCell();
                        cell2.Attributes["style"] = "text-align: center;width:45%;";
                        cell2.Controls.Add(lnkbtn);

                        //spanSplitter = new HtmlGenericControl("span");
                        //spanSplitter.Attributes["class"] = "spanSplitter";
                        //spanSplitter.InnerHtml = " | ";
                        //cell2.Controls.Add(spanSplitter);

                        lnkbtn = new LinkButton();
                        lnkbtn.CssClass = "lnkbtn lnkbtnDelete";
                        lnkbtn.ID = fileUploadItems[i] + "_delete_" + i;
                        lnkbtn.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
                        lnkbtn.Command += new CommandEventHandler(fileUploadItems_OnDelete);
                        lnkbtn.CommandArgument = fileTitleItems[i] + clsAdmin.CONSTDEFAULTSEPERATOR + fileUploadItems[i];
                        cell3 = new HtmlTableCell();
                        cell3.Attributes["class"] = "tdAct";
                        cell3.Controls.Add(lnkbtn);

                        row = new HtmlTableRow();
                        row.Controls.Add(cell1);
                        row.Controls.Add(cell2);
                        row.Controls.Add(cell3);
                        tbl.Controls.Add(row);
                    }
                }
                if (intCount > 0)
                {
                    pnlRelArt.Style.Add("  padding-top", "10px;");
                    pnlRelArt.Visible = litRelArt.Visible = true;
                    litRelArt.Text = intCount + " uploaded file(s):";
                }
                else
                {
                    pnlRelArt.Visible = litRelArt.Visible = false;
                }
            }
        }
        catch (Exception ex) { throw ex; }
    }

    protected void fillForm()
    {
        if (Session[clsAdmin.CONSTPROJECTPRODUCTDESCRIPTION] != null && Session[clsAdmin.CONSTPROJECTPRODUCTDESCRIPTION] != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTPRODUCTDESCRIPTION]) == 1)
            {
                trProdDescList.Visible = true;
                trProdDesc.Visible = false;
            }
        }

        clsProduct prod = new clsProduct();

        if (prod.extractProdById2(prodId, 0))
        {
            txtProdSnapshot.Text = prod.prodSnapshot;
            txtProdSnapshotJp.Text = prod.prodSnapshotJp;
            txtProdSnapshotMs.Text = prod.prodSnapshotMs;
            txtProdSnapshotZh.Text = prod.prodSnapshotZh;
            txtProdRemarks.Text = prod.prodRemarks;
            txtProdRemarksJp.Text = prod.prodRemarksJp;
            txtProdRemarksMs.Text = prod.prodRemarksMs;
            txtProdRemarksZh.Text = prod.prodRemarksZh;
            txtProdDesc.Text = prod.prodDesc;
            txtProdDescJp.Text = prod.prodDescJp;
            txtProdDescMs.Text = prod.prodDescMs;
            txtProdDescZh.Text = prod.prodDescZh;

            Session["ORIFILETITLE"] = "";
            Session["ORIFILEUPLOAD"] = "";
            DataSet dsProdArticle = prod.getProdArticleByProdId(prodId);
            foreach (DataRow row in dsProdArticle.Tables[0].Rows)
            {
                Session["ORIFILETITLE"] += row["PRODART_TITLE"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR;
                Session["ORIFILEUPLOAD"] += row["PRODART_FILE"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR;
            }

            /*Product Description List*/
            if (Session[clsAdmin.CONSTPROJECTPRODUCTDESCRIPTION] != null && Session[clsAdmin.CONSTPROJECTPRODUCTDESCRIPTION] != "")
            {
                if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTPRODUCTDESCRIPTION]) == 1)
                {
                    DataSet ds = new DataSet();
                    ds = prod.getProdDescItemListByProdId(prodId);
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        addItem(int.Parse(row["PRODDESC_ID"].ToString()), row["PRODDESC_TITLE"].ToString(), row["PRODDESC_DESC"].ToString());
                    }
                }
            }
            /*End of Product Description List*/
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }

    protected void checkLanguage()
    {
        if (clsMis.isLangExist(clsAdmin.CONSTLANGJAPANESE))
        {
            trSnapshotJp.Visible = true;
            trDescJp.Visible = true;

            registerCKEditorScript(txtProdDescJp.ClientID);
        }
        if (clsMis.isLangExist(clsAdmin.CONSTLANGMALAY))
        {
            trSnapshotMs.Visible = true;
            trDescMs.Visible = true;

            registerCKEditorScript(txtProdDescMs.ClientID);
        }
        if (clsMis.isLangExist(clsAdmin.CONSTLANGCHINESE))
        {
            trSnapshotZh.Visible = true;
            trDescZh.Visible = true;

            registerCKEditorScript(txtProdDescZh.ClientID);
        }
    }

    #region "Product Description List"
    protected DataTable createDataTable()
    {
        DataTable dt = new DataTable();
        DataColumn col;
        DataColumn[] keys = new DataColumn[1];

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.Int16");
        col.ColumnName = "PRODDESC_TEMPID";
        col.AutoIncrement = true;
        col.AutoIncrementSeed = 1;
        col.AutoIncrementStep = 1;
        dt.Columns.Add(col);

        keys[0] = col;

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.Int16");
        col.ColumnName = "PRODDESC_EDITED";
        col.DefaultValue = 0;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.Int16");
        col.ColumnName = "PRODDESC_DELETED";
        col.DefaultValue = 0;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.Int16");
        col.ColumnName = "PRODDESC_ID";
        col.DefaultValue = 0;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.String");
        col.ColumnName = "PRODDESC_TITLE";
        dt.Columns.Add(col);

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.String");
        col.ColumnName = "PRODDESC_DESC";
        dt.Columns.Add(col);

        dt.PrimaryKey = keys;
        return dt;
    }

    protected void addItem(int intDescId, string strDescTitle, string strDescContent)
    {
        DataTable dt;
        if (Session["TABLEDESC"] != null)
        {
            dt = (DataTable)Session["TABLEDESC"];
        }
        else
        {
            dt = createDataTable();
        }

        DataRow row;
        row = dt.NewRow();
        row["PRODDESC_ID"] = intDescId;
        row["PRODDESC_TITLE"] = strDescTitle;
        row["PRODDESC_DESC"] = strDescContent;

        dt.Rows.Add(row);
        Session["TABLEDESC"] = dt;
    }

    protected void extractItemById(int intDescId)
    {
        if (Session["TABLEDESC"] != null)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["TABLEDESC"];
            object[] keyVals = new object[] { intDescId };
            DataRow foundRow = dt.Rows.Find(keyVals);

            if (foundRow != null)
            {
                hdnProdDesc.Value = intDescId.ToString();
                txtProdDescTitle.Text = foundRow["PRODDESC_TITLE"].ToString();
                txtProdDescContent.Text = foundRow["PRODDESC_DESC"].ToString();
                lnkbtnAdd.Text = lnkbtnAdd.ToolTip = "Update";
            }
        }
    }

    protected void updateItemById(int intDescId, string strDescTitle, string strDescContent)
    {
        if (Session["TABLEDESC"] != null)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["TABLEDESC"];

            object[] keyVals = new object[] { intDescId };
            DataRow foundRow = dt.Rows.Find(keyVals);

            if (foundRow != null)
            {
                DataRow[] foundRowValid = dt.Select("PRODDESC_TEMPID <> " + intDescId);

                if (strDescTitle != foundRow["PRODDESC_TITLE"].ToString() || strDescContent != foundRow["PRODDESC_DESC"].ToString())
                {
                    foundRow["PRODDESC_TITLE"] = strDescTitle;
                    foundRow["PRODDESC_DESC"] = strDescContent;
                    foundRow["PRODDESC_EDITED"] = 1;

                    Session["TABLEDESC"] = dt;
                }
            }
        }
    }

    protected void setDelete(int intDescId)
    {
        if (Session["TABLEDESC"] != null)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["TABLEDESC"];

            object[] keyVals = new object[] { intDescId };
            DataRow foundRow = dt.Rows.Find(keyVals);

            if (foundRow != null)
            {
                foundRow["PRODDESC_DELETED"] = 1;

                DataRow[] foundRowValid = dt.Select("PRODDESC_DELETED = 0");
                if (foundRowValid.Length > 0)
                {
                    foundRowValid[0]["PRODDESC_EDITED"] = 1;
                }

                Session["TABLEDESC"] = dt;
            }
        }
    }

    protected void bindRptData()
    {
        if (Session["TABLEDESC"] != null)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["TABLEDESC"];

            DataView dv = new DataView(dt);
            dv.RowFilter = "PRODDESC_DELETED = 0";

            rptProdDesc.DataSource = dv;
            rptProdDesc.DataBind();
        }
    }
    #endregion
    #endregion
}
