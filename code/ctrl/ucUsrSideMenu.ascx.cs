﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucUsrSideMenu : System.Web.UI.UserControl
{
    #region "Properties"
    protected string _lang = "";
    protected int _pageId = 0;
    protected int _sectId = 1;
    protected int _parentId = 0;
    protected int parentId2 = 0;
    #endregion


    #region "Property Methods"
    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }

    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public int parentId
    {
        get { return _parentId; }
        set { _parentId = value; }
    }

    public int sectId
    {
        get
        {
            if (ViewState["SECTID"] == null)
            {
                return _sectId;
            }
            else
            {
                return Convert.ToInt16(ViewState["SECTID"]);
            }
        }
        set { ViewState["SECTID"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.LoadComplete += new EventHandler(Page_LoadComplete);

        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "sidemenu.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            //if (!string.IsNullOrEmpty(Request["pgid"]))
            //{ 
            //clsPage page = new clsPage();
            //litSubMenuHdr.Text = page.parentName;
            //Application["PAGELIST"] = page.getPageList(1);
            //}

        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        bindRptCatData();
    }

    protected void rptSubMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ID"));
            string strDisplayName = DataBinder.Eval(e.Item.DataItem, "PAGE_DISPLAYNAME").ToString();
            string strTitle = DataBinder.Eval(e.Item.DataItem, "PAGE_TITLE").ToString();
            string strCSSClass = DataBinder.Eval(e.Item.DataItem, "PAGE_CSSCLASS").ToString();
            string strLang = DataBinder.Eval(e.Item.DataItem, "PAGE_LANG").ToString();
            string strTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATE").ToString();
            int intRedirect = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECT"));
            int intParent = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_PARENT"));
            int intParentTemplate = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATEPARENT"));
            string strParentTemplate = DataBinder.Eval(e.Item.DataItem, "V_TEMPLATEPARENT").ToString();
            string strParentCSSClass = DataBinder.Eval(e.Item.DataItem, "V_PARENTCSSCLASS").ToString();
            int intEnableLink = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ENABLELINK"));
            string strRedirectLink = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTLINK").ToString();
            string strRedirectTarget = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTTARGET").ToString();
            string strParentName = DataBinder.Eval(e.Item.DataItem, "V_PARENTNAME").ToString();

            //litSubMenuHdr.Text = strParentName;
            litSubMenuHdr.Text = "LATEST UPDATE";

            HyperLink hypSubMenu = (HyperLink)e.Item.FindControl("hypSubMenu");
            hypSubMenu.Text = strDisplayName;
            hypSubMenu.ToolTip = strDisplayName;

            if (intId == pageId || intId == parentId2) 
            { 
                hypSubMenu.CssClass = "hypSubMenuLeftSel"; 
            }

            if (intEnableLink == 0)
            {
                hypSubMenu.NavigateUrl = string.Empty;
                hypSubMenu.Attributes.Remove("href");
                hypSubMenu.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "default";
            }
            else
            {
                if (intRedirect > 0)
                {
                    //DataRow[] foundRow = dsMenu.Tables[0].Select("PAGE_ID = " + intRedirect + " AND PAGE_ACTIVE = 1 AND PAGE_OTHER = 0");

                    //if (foundRow.Length > 0)
                    //{
                    //    int intRParent = Convert.ToInt16(foundRow[0]["PAGE_PARENT"]);
                    //    int intRTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                    //    string strRTemplateParent = foundRow[0]["V_TEMPLATEPARENT"].ToString();
                    //    string strRTemplate = intRParent > 0 ? (intRTemplateParent > 0 ? strRTemplateParent : strTemplate) : strTemplate;

                    //    if (!string.IsNullOrEmpty(strRTemplate))
                    //    {
                    //        if (!string.IsNullOrEmpty(lang)) { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strRTemplate + "?pgid=" + intRedirect + "&lang=" + strLang; }
                    //        else { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strRTemplate + "?pgid=" + intRedirect; }
                    //    }
                    //}

                    if (intRedirect == clsSetting.CONSTEXTERNALLINK)
                    {
                        if (!string.IsNullOrEmpty(strRedirectLink))
                        {
                            string strLink = strRedirectLink;

                            if (strRedirectLink.StartsWith("http://"))
                            {
                                hypSubMenu.NavigateUrl = strLink;
                            }
                            else
                            {
                                hypSubMenu.NavigateUrl = "http://" + strLink;
                            }

                            //hypSubMenu.NavigateUrl = strLink;
                            hypSubMenu.Target = strRedirectTarget;
                        }
                    }
                    else
                    {
                        if (Application["PAGELIST"] == null)
                        {
                            clsPage page = new clsPage();
                            Application["PAGELIST"] = page.getPageList(1);
                        }

                        DataSet ds = new DataSet();
                        ds = (DataSet)Application["PAGELIST"];

                        DataRow[] foundRow = ds.Tables[0].Select("PAGE_ID = " + intRedirect + " AND PAGE_ACTIVE = 1 AND PAGE_OTHER = 0");

                        if (foundRow.Length > 0)
                        {
                            int intRParent = Convert.ToInt16(foundRow[0]["PAGE_PARENT"]);
                            int intRTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                            string strRTemplateParent = foundRow[0]["V_TEMPLATEPARENT"].ToString();
                            string strRTemplate = intRParent > 0 ? (intRTemplateParent > 0 ? strRTemplateParent : strTemplate) : strTemplate;

                            if (!string.IsNullOrEmpty(strRTemplate))
                            {
                                if (!string.IsNullOrEmpty(lang)) { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strRTemplate + "?pgid=" + intRedirect + "&lang=" + strLang; }
                                else { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strRTemplate + "?pgid=" + intRedirect; }
                            }
                        }
                    }
                }
                else
                {
                    //string strTemplateUse = intParentTemplate > 0 ? strParentTemplate : strTemplate;

                    //if (!string.IsNullOrEmpty(strTemplateUse))
                    //{
                    //    if (!string.IsNullOrEmpty(lang)) { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateUse + "?pgid=" + intId + "&lang=" + strLang; }
                    //    else { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateUse + "?pgid=" + intId; }
                    //}

                    if (!string.IsNullOrEmpty(strTemplate))
                    {
                        if (!string.IsNullOrEmpty(lang)) { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + intId + "&lang=" + strLang; }
                        else { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + intId; }
                    }
                }
            }

            DataSet dsMenu = new DataSet();
            //dsMenu = (DataSet)Application["PAGELISTLV2"];
            //DataView dv = new DataView(dsMenu.Tables[0]);

            clsPage pageSub = new clsPage();
            Application["PAGELISTLV2"] = pageSub.getPageList(1);


            dsMenu = (DataSet)Application["PAGELISTLV2"];
            DataView dv = new DataView(dsMenu.Tables[0]);
            dv.RowFilter = "PAGE_PARENT = " + intId + " AND PAGE_SHOWINMENU = 1 AND PAGE_ACTIVE = 1 AND PAGE_OTHER = 0";
            dv.Sort = "PAGE_ORDER ASC";

            Repeater rptSubMenuLv2 = (Repeater)e.Item.FindControl("rptSubMenuLv2");
            Panel pnlSubMenuLv2 = (Panel)e.Item.FindControl("pnlSubMenuLv2");
            Panel pnlSubMenuLv1 = (Panel)e.Item.FindControl("pnlSubMenuLv1");

            if (dv.Count > 0)
            {
                pnlSubMenuLv2.Visible = true;
                rptSubMenuLv2.DataSource = dv;
                rptSubMenuLv2.DataBind();
            }
            else
            {
                pnlSubMenuLv2.Visible = false;
            }

            if (pnlSubMenuLv2.Visible)
            {
                if (intId != pageId && intId != parentId2)
                {
                    if (!Page.ClientScript.IsStartupScriptRegistered("SubMenuLv2" + intId))
                    {
                        string strJS = "<script type=\"text/javascript\">";
                        strJS += "initMenu('" + hypSubMenu.ClientID + "','" + pnlSubMenuLv1.ClientID + "','" + pnlSubMenuLv2.ClientID + "');";
                        strJS += "</script>";

                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "group" + intId, strJS);
                    }
                }
            }
        }
    }

    protected void rptSubMenuLv2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ID"));
            string strDisplayName = DataBinder.Eval(e.Item.DataItem, "PAGE_DISPLAYNAME").ToString();
            string strTitle = DataBinder.Eval(e.Item.DataItem, "PAGE_TITLE").ToString();
            string strCSSClass = DataBinder.Eval(e.Item.DataItem, "PAGE_CSSCLASS").ToString();
            string strLang = DataBinder.Eval(e.Item.DataItem, "PAGE_LANG").ToString();
            string strTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATE").ToString();
            int intRedirect = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECT"));
            int intParent = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_PARENT"));
            int intParentTemplate = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATEPARENT"));
            string strParentTemplate = DataBinder.Eval(e.Item.DataItem, "V_TEMPLATEPARENT").ToString();
            string strParentCSSClass = DataBinder.Eval(e.Item.DataItem, "V_PARENTCSSCLASS").ToString();
            int intEnableLink = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ENABLELINK"));
            string strRedirectLink = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTLINK").ToString();
            string strRedirectTarget = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTTARGET").ToString();
            string strParentName = DataBinder.Eval(e.Item.DataItem, "V_PARENTNAME").ToString();

            HyperLink hypSubMenuLv2 = (HyperLink)e.Item.FindControl("hypSubMenuLv2");
            hypSubMenuLv2.Text = strDisplayName;
            hypSubMenuLv2.ToolTip = strDisplayName;

            if (intId == pageId) { hypSubMenuLv2.CssClass = "hypSubMenuLv2Sel"; }

            if (intEnableLink == 0)
            {
                hypSubMenuLv2.NavigateUrl = string.Empty;
                hypSubMenuLv2.Attributes.Remove("href");
                hypSubMenuLv2.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "default";
            }
            else
            {
                if (intRedirect > 0)
                {
                    if (intRedirect == clsSetting.CONSTEXTERNALLINK)
                    {
                        if (!string.IsNullOrEmpty(strRedirectLink))
                        {
                            string strLink = strRedirectLink;

                            if (strRedirectLink.StartsWith("http://"))
                            {
                                hypSubMenuLv2.NavigateUrl = strLink;
                            }
                            else
                            {
                                hypSubMenuLv2.NavigateUrl = "http://" + strLink;
                            }

                            //hypSubMenu.NavigateUrl = strLink;
                            hypSubMenuLv2.Target = strRedirectTarget;
                        }
                    }
                }
                else
                {
                    string strTemplateUse = intParentTemplate > 0 ? strParentTemplate : strTemplate;

                    if (!string.IsNullOrEmpty(strTemplateUse))
                    {
                        if (!string.IsNullOrEmpty(lang)) { hypSubMenuLv2.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateUse + "?pgid=" + intId + "&lang=" + strLang; }
                        else { hypSubMenuLv2.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateUse + "?pgid=" + intId; }
                    }
                }
            }
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        //sectId = intSectId;
    }

    protected void bindRptCatData()
    {
        if (!string.IsNullOrEmpty(Request["pgid"]))
        {
            int intTryParse;
            if (int.TryParse(Request["pgid"], out intTryParse))
            {
                int intId = intTryParse;
            }
        }

        DataSet dsMenu = new DataSet();
        dsMenu = (DataSet)Application["PAGELIST"];
        DataView dv = new DataView(dsMenu.Tables[0]);

        clsPage page = new clsPage();
        int intParentId = page.getParentId(parentId);

        parentId2 = parentId;
        if (intParentId > 0)
        {
            parentId = intParentId;
        }

        //if (parentId > 0)
        //{
        //    if (Application["PAGELIST"] == null)
        //    {
        //        Application["PAGELIST"] = page.getPageList(1);
        //    }

        //    dsMenu = (DataSet)Application["PAGELIST"];
        //    dv = new DataView(dsMenu.Tables[0]);
        //    dv.RowFilter = "PAGE_PARENT = " + parentId + " AND PAGE_SHOWINMENU = 1 AND PAGE_ACTIVE = 1 AND PAGE_OTHER = 0";
        //    dv.Sort = "PAGE_ORDER ASC";
        //}
        //else
        //{
        //    if (Application["PAGELIST"] == null)
        //    {
        //        Application["PAGELIST"] = page.getPageList(1);
        //    }

        //    dsMenu = (DataSet)Application["PAGELIST"];
        //    dv = new DataView(dsMenu.Tables[0]);
        //    dv.RowFilter = "PAGE_PARENT = " + intId + " AND PAGE_SHOWINMENU = 1 AND PAGE_ACTIVE = 1 AND PAGE_OTHER = 0";
        //    dv.Sort = "PAGE_ORDER ASC";
        //}

        if (Application["PAGELIST"] == null)
        {
            Application["PAGELIST"] = page.getPageList(1);
        }

        dsMenu = (DataSet)Application["PAGELIST"];
        dv = new DataView(dsMenu.Tables[0]);
        dv.RowFilter = "PAGE_PARENT = 0 AND PAGE_SHOWINMENU = 1 AND PAGE_ACTIVE = 1 AND PAGE_OTHER = 0  AND PAGE_ID <> 2";
        dv.Sort = "PAGE_ORDER ASC";

        if (dv.Count > 0)
        {
            rptSubMenu.DataSource = dv;
            rptSubMenu.DataBind();
        }
        else
        {
            pnlSubMenuOuter.Visible = false;
        }
    }
    #endregion
}
