﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class ctrl_ucUsrHeader : System.Web.UI.UserControl
{
    public int pageId { get; set; }

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion


    #region "Methods"
    public void fill()
    {
        ucUsrSideMenuNav.pageId = pageId;
        ucUsrSideMenuNav.fill();

        ucUsrQuickLinks.fill();

        fillTopmenu();
    }

    private void fillTopmenu()
    {
        ucUsrTopTopMenu.pgid = pageId;
        ucUsrTopTopMenu.fill();

        ucUsrTopMenu.pgid = pageId;
        ucUsrTopMenu.fill();
    }
    #endregion
}
