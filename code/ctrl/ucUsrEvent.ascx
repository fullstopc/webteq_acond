﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrEvent.ascx.cs" Inherits="ctrl_ucUsrEvent" %>

<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>lazysizes.min.js" type="text/javascript"></script>
<script>
    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.expand = 9;
</script>

<asp:UpdatePanel ID="upnlProdGroup" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlEventList" runat="server" CssClass="divEventList">
            <asp:Panel ID="pnlEventContent" runat="server" CssClass="divEventContent">
                <asp:Panel ID="pnlEventHeader" runat="server" CssClass="" Visible="false">
                    <h1><asp:Literal ID="litEventHdr" runat="server"></asp:Literal></h1>
                </asp:Panel>
                <asp:Panel ID="pnlListPaginationTop" runat="server" CssClass="divListPaginationTop" Visible="false">
                    <%--<div class="divListPaginationInner">
            <div class="divLastButton">
                <asp:HyperLink ID="lnkbtnLastTop" runat="server" meta:resourcekey="lnkbtnLast" CssClass="lnkbtnLast" OnClick="lnkbtnLast_Click">End</asp:HyperLink></div>
            <div class="divPagination">
                <asp:Repeater ID="rptPaginationTop" runat="server" OnItemDataBound="rptPagination_ItemDataBound" OnItemCommand="rptPagination_ItemCommand">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkbtnPageNo" runat="server" CommandName="cmdPage" CommandArgument="<% #Container.DataItem %>" Text="<% #Container.DataItem %>" ToolTip="<% #Container.DataItem %>" CssClass="btnPagination"></asp:LinkButton>
                    </ItemTemplate>
                </asp:Repeater>
                </div>
                <div class="divFirstButton">
            <asp:HyperLink ID="lnkbtnFirstTop" runat="server" meta:resourcekey="lnkbtnFirst" CssClass="lnkbtnFirst" OnClick="lnkbtnFirst_Click">Start</asp:HyperLink></div> 
            </div> --%>
                    <asp:Panel ID="pnlListTopInner" runat="server" class="divListTopInner">
                        <asp:ImageButton ID="imgbtnFirstTop" runat="server" Width="8" Height="8" meta:resourcekey="imgbtnFirst" CssClass="imgbtnPageFirstTop" OnClick="lnkbtnFirst_Click" />
                        <span class="spanPagination">
                            <asp:Repeater ID="rptPaginationTop" runat="server" OnItemDataBound="rptPagination_ItemDataBound" OnItemCommand="rptPagination_ItemCommand">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtnPageNo" runat="server" CommandName="cmdPage" CommandArgument="<% #Container.DataItem%>" Text="<% #Container.DataItem%>" ToolTip="<% #Container.DataItem%>" CssClass="btnPagination"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:Repeater>
                        </span>
                        <asp:ImageButton ID="imgbtnLastTop" runat="server" Width="8" Height="8" meta:resourcekey="imgbtnLast" CssClass="imgbtnPageLastTop" OnClick="lnkbtnLast_Click" />
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="pnlDdlFilter" runat="server" CssClass="divDdlSort" Visible="true">
                    <asp:Literal ID="litShow" runat="server"></asp:Literal>
                    <asp:DropDownList ID="ddlFilter" runat="server" AutoPostBack="true" CssClass="ddl ddl_filter" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged"></asp:DropDownList>
                </asp:Panel>
                <asp:Panel ID="pnlDdlSort" runat="server" CssClass="divDdlSort" Visible="true">
                    <asp:Literal ID="litSort" runat="server"></asp:Literal>
                    <asp:DropDownList ID="ddlSort" runat="server" AutoPostBack="true" CssClass="ddl ddl_sort" OnSelectedIndexChanged="ddlSort_SelectedIndexChanged"></asp:DropDownList>
                </asp:Panel>
                <asp:Panel ID="pnlEventItemsContainer" runat="server" CssClass="divEventItemsContainer">
                    <asp:Repeater ID="rptEvent" runat="server" OnItemDataBound="rptEvent_ItemDataBound">
                        <ItemTemplate>
                            <asp:Panel ID="pnlEventItemOuter" runat="server" CssClass="divEventItemOuter">
                                <asp:Panel ID="pnlEventItem" runat="server" CssClass="divEventItem">
                                    <asp:Panel ID="pnlEventImage" runat="server" CssClass="divEventImage">
                                        <asp:Panel ID="pnlEventImg" runat="server" CssClass="divEventImg">
                                            <div class="divEventImgInner">
                                                <asp:HyperLink ID="hypEventImg" runat="server">
                                                    <asp:Image ID="imgEvent" runat="server" BorderWidth="0" /></asp:HyperLink>
                                            </div>
                                        </asp:Panel>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEventDetails" runat="server" CssClass="divEventDetails">
                                        <div class="divEventName">
                                            <div class="icon">
                                                <i class="fa fa-info"></i>
                                            </div>
                                            <asp:HyperLink ID="hypEventName" runat="server" CssClass="hypEventName"></asp:HyperLink>
                                        </div>
                                        <asp:Panel CssClass="divEventStartDate" runat="server" id="pnlEventStartDate" Visible="false">
                                            <div class="icon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <h4><asp:Literal ID="litEventStartDate" runat="server"></asp:Literal></asp:Panel></h4>
                                        <%--<div class="divEventDesc"><asp:Literal ID="litEventDesc" runat="server"></asp:Literal></div>--%>
                                        <asp:Panel ID="pnlEventRead" runat="server" CssClass="divEventRead" Visible="false">
                                            <asp:HyperLink ID="hypEvent" runat="server" CssClass="hypReadEvent"></asp:HyperLink>
                                        </asp:Panel>
                                        <%--<asp:Panel ID="pnlEventTopDesc" runat="server" Visible="false">
                                            <asp:Panel ID="pnlEventCount" runat="server" CssClass="divEventCount">
                                                <div class="divEventCountInnerTop">
                                                    <div class="divEvtCommentCount">
                                                        <asp:Literal ID="litEvtCommentCount" runat="server"></asp:Literal></div>
                                                    <div class="divEventCommentCount">
                                                        <asp:Image ID="imgComment" runat="server" /></div>
                                                    <div class="divEvtVideoCount">
                                                        <asp:Literal ID="litEvtVideoCount" runat="server"></asp:Literal></div>
                                                    <div class="divEventVideoCount">
                                                        <asp:Image ID="imgVideo" runat="server" /></div>
                                                    <div class="divEvtGalleryCount">
                                                        <asp:Literal ID="litEvtGallCount" runat="server"></asp:Literal></div>
                                                    <div class="divEventGalleryCount">
                                                        <asp:Image ID="imgGallery" runat="server" /></div>
                                                </div>
                                            </asp:Panel>
                                        </asp:Panel>--%>
                                    </asp:Panel>
                                </asp:Panel>
                                <%--<asp:Panel ID="pnlEventGallery" runat="server" CssClass="divEventGallery" Visible="false">
                            <asp:Repeater ID="rptEventGallery" runat="server" OnItemDataBound="rptEventGallery_ItemDataBound">
                                <ItemTemplate>
                                    <asp:Panel ID="pnlEventGalleryItems" runat="server" CssClass="divEventGalleryItems">
                                        <asp:Panel ID="pnlEventGalleryImg" runat="server" CssClass="divEventGalleryImg">
                                            <div class="divEventGalleryImgInner"><asp:HyperLink ID="hypEventGalleryImg" runat="server" class="thickbox"><asp:Image ID="imgEventGalleryImg" runat="server" BorderWidth="0" /></asp:HyperLink></div>
                                        </asp:Panel>
                                    </asp:Panel>
                                </ItemTemplate>
                            </asp:Repeater>
                        </asp:Panel>--%>
                            </asp:Panel>
                        </ItemTemplate>
                        <SeparatorTemplate>
                            <%--<div class="divEventSpliter" visible="true"></div>--%>
                        </SeparatorTemplate>
                    </asp:Repeater>
                </asp:Panel>
                <asp:Panel ID="pnlListPaginationBottom" runat="server" CssClass="divListPaginationBottom" Visible="false">
                    <%--  <div class="divListPaginationInner">
           <div class="divLastButton">
                <asp:HyperLink ID="lnkbtnLastBottom" runat="server" meta:resourcekey="lnkbtnLast" CssClass="lnkbtnLast" OnClick="lnkbtnLast_Click">End</asp:HyperLink></div>
            <div class="divPagination">
                <asp:Repeater ID="rptPaginationBottom" runat="server" OnItemDataBound="rptPagination_ItemDataBound" OnItemCommand="rptPagination_ItemCommand">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkbtnPageNo" runat="server" CommandName="cmdPage" CommandArgument="<% #Container.DataItem %>" Text="<% #Container.DataItem %>" ToolTip="<% #Container.DataItem %>" CssClass="btnPagination"></asp:LinkButton>
                    </ItemTemplate>
                </asp:Repeater></div>
                <div class="divFirstButton">
            <asp:HyperLink ID="lnkbtnFirstBottom" runat="server" meta:resourcekey="lnkbtnFirst" CssClass="lnkbtnFirst" OnClick="lnkbtnFirst_Click">Start</asp:HyperLink></div>
            </div>--%>
                    <asp:Panel ID="pnlListBottomPagination" runat="server" CssClass="divListPagination">
                        <asp:ImageButton ID="imgbtnFirstBottom" runat="server" Width="8" Height="8" meta:resourcekey="imgbtnFirst" CssClass="imgbtnPageFirst" OnClick="lnkbtnFirst_Click" />
                        <span class="spanPagination">
                            <asp:Repeater ID="rptPaginationBottom" runat="server" OnItemDataBound="rptPagination_ItemDataBound" OnItemCommand="rptPagination_ItemCommand">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtnPageNo" runat="server" CommandName="cmdPage" CommandArgument="<% #Container.DataItem%>" Text="<% #Container.DataItem%>" ToolTip="<% #Container.DataItem%>" CssClass="btnPaginationBottom"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:Repeater>
                        </span>
                        <asp:ImageButton ID="imgbtnLastBottom" runat="server" Width="8" Height="8" meta:resourcekey="imgbtnLast" CssClass="imgbtnPageLast" OnClick="lnkbtnLast_Click" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
