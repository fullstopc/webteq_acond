﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmControllerDetails.ascx.cs" Inherits="ctrl_ucAdmControllerDetails" %>

<script type="text/javascript">
        function validatePassword_client(source, args) {
            if(args.Value.length < <% =clsSetting.CONSTADMPASSWORDMINLENGTH %>)
            {
                args.IsValid = false;
                
                source.errormessage = "<br />Min. length for password is 6.";
                source.innerHTML = "<br />Min. length for password is 6.";
            }            
        }
        
        function validatePwd_client(source, args) {
            var txtPassword = document.getElementById("<%= txtPassword.ClientID %>");
            var hdnPwd = document.getElementById("<%= hdnPwd.ClientID %>");
            
            var boolValid = false;

            source.errormessage = "<br />Please enter current password.";
            source.innerHTML = "<br />Please enter current password.";

            if ((Trim(txtPassword.value) != '') || (Trim(hdnPwd.value) != '' )) {
                args.IsValid = true;
            }
            else {
                args.IsValid = boolValid;
            }
        }
        
        function validateConfirmPwd_client(source, args) {
            var txtPassword = document.getElementById("<%= txtPassword.ClientID %>");
            var txtConfirmPwd = document.getElementById("<%= txtConfirmPwd.ClientID %>");

            var boolValid = false;

            source.errormessage = "<br />Please enter password to confirm.";
            source.innerHTML = "<br />Please enter password to confirm.";

            if (Trim(txtConfirmPwd.value) != '') {
                args.IsValid = true;
            }
            else {
                if (Trim(txtPassword.value) != '') {
                    boolValid = false;
                }
                else {
                    boolValid = true;
                }

                args.IsValid = boolValid;
            }
        }
</script>
<asp:Panel ID="pnlForm" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table id="tblController" cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr">CONTROLLER DETAILS</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Roles<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:DropDownList ID="ddlRole" runat="server" class="ddl_big compulsory"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvRole" runat="server" ErrorMessage="<br />Please select Roles." ControlToValidate="ddlRole" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgController"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Controller Name<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtControllerName" runat="server" CssClass="text_big2 uniq" MaxLength="250"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvControllerName" runat="server" ErrorMessage="<br />Please enter Controller Name." ControlToValidate="txtControllerName" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgController"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Controller Email<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtControllerEmail" runat="server" CssClass="text_big2 compulsory" MaxLength="250"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvControllerEmail" runat="server" ErrorMessage="<br />Please enter Controller Email." ControlToValidate="txtControllerEmail" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgController"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revControllerEmail" runat="server" CssClass="errmsg" ControlToValidate="txtControllerEmail" Display="Dynamic" ErrorMessage="<br />Please enter valid Email." ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Password<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtPassword" runat="server" CssClass="text_big compulsory" MaxLength="50" TextMode="Password" ValidationGroup="vgController"></asp:TextBox>
                    <asp:HiddenField ID="hdnPwd" runat="server" />
                    <asp:CustomValidator ID="cvPwd" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtPassword" ClientValidationFunction="validatePwd_client" ValidateEmptyText="true" ValidationGroup="vgController"></asp:CustomValidator>
                <asp:CustomValidator ID="cvPwd2" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtPassword" ClientValidationFunction="validatePassword_client" ValidationGroup="vgController"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Confirm Password<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtConfirmPwd" runat="server" CssClass="text_big compulsory" MaxLength="50" TextMode="Password"></asp:TextBox>
                    <asp:CustomValidator ID="cvConfirmPwd" runat="server" Display="Dynamic" ControlToValidate="txtConfirmPwd" CssClass="errmsg" ClientValidationFunction="validateConfirmPwd_client" ValidateEmptyText="true" ValidationGroup="vgController"></asp:CustomValidator>
                <asp:CustomValidator ID="cvConfirmPwd2" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtConfirmPwd" ClientValidationFunction="validatePassword_client" ValidationGroup="vgController"></asp:CustomValidator>
                    <asp:CompareValidator ID="cmvPassword" runat="server" ErrorMessage="<br />Password does not match." ControlToCompare="txtPassword" ControlToValidate="txtConfirmPwd" Display="Dynamic" class="errmsg"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Active:</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
            </tr>
            <tfoot>
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="vgController"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" class="btn btnCancel" Visible="false" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
</asp:Panel>
