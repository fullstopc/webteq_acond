﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucUsrBtmMenu : System.Web.UI.UserControl
{
    #region "Properties"
    protected string _lang = "";
    protected int _pgid = 0;
    protected int _parentId = 0;
    //protected MenuType _menuType = MenuType.Link;
    protected DataSet _dsMenu = new DataSet();
    #endregion

    #region "Property Methods"
    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }

    public int pgid
    {
        get { return _pgid; }
        set { _pgid = value; }
    }

    public int parentId
    {
        get { return _parentId; }
        set { _parentId = value; }
    }

    protected DataSet dsMenu
    {
        get
        {
            if (ViewState["DSMENU"] == null)
            {
                return _dsMenu;
            }
            else
            {
                return (DataSet)ViewState["DSMENU"];
            }
        }
        set { ViewState["DSMENU"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "btmmenu.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);
    }

    protected void rptBtmMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ID"));
            string strDisplayName = DataBinder.Eval(e.Item.DataItem, "PAGE_DISPLAYNAME").ToString();
            string strTitle = DataBinder.Eval(e.Item.DataItem, "PAGE_TITLE").ToString();
            string strCSSClass = DataBinder.Eval(e.Item.DataItem, "PAGE_CSSCLASS").ToString();
            string strLang = DataBinder.Eval(e.Item.DataItem, "PAGE_LANG").ToString();
            string strTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATE").ToString();
            int intRedirect = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECT"));
            string strRedirectLink = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTLINK").ToString();
            string strTarget = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTTARGET").ToString();
            int intEnableLink = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ENABLELINK"));

            HyperLink hypBtmMenu = (HyperLink)e.Item.FindControl("hypBtmMenu");
            hypBtmMenu.Text = strDisplayName;
            hypBtmMenu.ToolTip = strDisplayName;

            Panel pnlBtmMenuItem = (Panel)e.Item.FindControl("pnlBtmMenuItem");

            if (intId == pgid || intId == parentId)
            {
                hypBtmMenu.CssClass = "btmMenuItemSel";
            }

            if (intEnableLink == 0)
            {
                hypBtmMenu.NavigateUrl = string.Empty;
                hypBtmMenu.Attributes.Remove("href");
                hypBtmMenu.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "default";
            }
            else
            {
                if (intRedirect > 0)
                {
                    if (intRedirect == clsSetting.CONSTEXTERNALLINK)
                    {
                        if (!string.IsNullOrEmpty(strRedirectLink))
                        {
                            string strLink = strRedirectLink;

                            if (strRedirectLink.StartsWith("http://") || strRedirectLink.StartsWith("https://"))
                            {
                                hypBtmMenu.NavigateUrl = strLink;
                            }
                            else
                            {
                                hypBtmMenu.NavigateUrl = "http://" + strLink;
                            }

                            //hypMenu.NavigateUrl = strLink;
                            hypBtmMenu.Target = strTarget;
                        }
                    }
                    else
                    {
                        DataRow[] foundRow = dsMenu.Tables[0].Select("PAGE_ID = " + intRedirect + " AND PAGE_ACTIVE = 1 AND PAGE_OTHER = 0");

                        if (foundRow.Length > 0)
                        {
                            int intRParent = Convert.ToInt16(foundRow[0]["PAGE_PARENT"]);
                            int intRTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                            string strRTemplateParent = foundRow[0]["V_TEMPLATEPARENT"].ToString();
                            string strRTemplate = intRParent > 0 ? (intRTemplateParent > 0 ? strRTemplateParent : strTemplate) : strTemplate;

                            if (!string.IsNullOrEmpty(strRTemplate))
                            {
                                if (!string.IsNullOrEmpty(lang)) { hypBtmMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strRTemplate + "?pgid=" + intRedirect + "&lang=" + strLang; }
                                else { hypBtmMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strRTemplate + "?pgid=" + intRedirect; }
                            }
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(strTemplate))
                    {
                        if (!string.IsNullOrEmpty(lang)) { hypBtmMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + intId + "&lang=" + strLang; }
                        else { hypBtmMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + intId; }
                    }
                }
            }
        }
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            bindRptData();
        }
    }

    protected void bindRptData()
    {
        if (Application["PAGELIST"] == null)
        {
            clsPage page = new clsPage();
            Application["PAGELIST"] = page.getPageList(1);
        }

        DataSet ds = new DataSet();
        ds = (DataSet)Application["PAGELIST"];
        dsMenu = ds;

        DataView dv = new DataView(ds.Tables[0]);

        dv.RowFilter = "PAGE_PARENT <= 0 AND PAGE_SHOWINMENU = 1 AND PAGE_OTHER = 0 AND PAGE_SHOWBOTTOM = 1";

        if (!string.IsNullOrEmpty(lang)) { dv.RowFilter += " AND PAGE_LANG = '" + lang + "'"; }

        dv.Sort = "PAGE_ORDER ASC";

        if (dv.Count > 0)
        {
            rptBtmMenu.DataSource = dv;
            rptBtmMenu.DataBind();
        }
    }
    #endregion
}
