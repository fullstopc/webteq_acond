﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmCMS.ascx.cs" Inherits="ctrl_ucAdmCMS" %>
<%@ Register Src="~/ctrl/ucCmnPageDetails.ascx" TagName="CmnPageDetails" TagPrefix="uc" %>
<script type="text/javascript">
        function BtnClick() {
            var val = Page_ClientValidate();
            if (!val) {
                var i = 0;
                for (; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $("#" + Page_Validators[i].controltovalidate).addClass("focus");
                    }
                }
            }            
            return val;
        }
    </script>


<asp:Panel ID="pnlAdmCMS" runat="server" CssClass="divAdmCMS">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr">PAGE DETAILS</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr id="trMessage" runat="server" visible="false">
                <td colspan="2"><asp:Literal ID="litMsg" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="withNestedTable">
                    <uc:CmnPageDetails id="ucCmnPageDetails" runat="server" viewType="Admin" modeType="Edit" />
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData noBorderBtm">
            <tr>
                <td class="tdSectionHdr">
                    PAGE CONTENT
                    <asp:HyperLink ID="hypPreview" runat="server" CssClass="btnPreview" Width="13px" Height="12px" Visible="false"><asp:Image ID="imgPreview" runat="server" /></asp:HyperLink></td>
                <td class="tdAction">
                    <div style="float:left;">
                        <asp:UpdatePanel runat="server" ID="upnlBackup" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:LinkButton ID="lnkbtnBackup" runat="server" OnClick="lnkbtnBackup_Click" ToolTip="Backup This Version" CssClass="btn btnSave">Backup this</asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnRestore" runat="server" ToolTip="Restore" CssClass="thickbox btn btnBack">Restore</asp:LinkButton>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="lnkbtnBackup" />
                                <%--<asp:PostBackTrigger ControlID="lnkbtnRestore" />--%>
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress runat="server" ID="upgBackup" DisplayAfter="50" AssociatedUpdatePanelID="upnlBackup">
                            <ProgressTemplate>
                                <div class="overlay-container">
                                    <div class="overlay-background"></div>
                                    <div class="overlay-content">
                                        <div class="overlay-title">
                                            Saving . . .
                                        </div>
                                    </div>
                                </div>
                                        
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                    <div style="float:left;padding:18px;">
                        <asp:CheckBox runat="server" ID="chkboxContentParent" Text="Apply Same Content to Mobile View" />
                    </div>
                    
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData" style="padding-top:0;">
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="txtContent" runat="server" class="text_big" TextMode="MultiLine" Rows="5"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <% if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTCKEDITORTOPMENU]) == 1) { %>
                <tr>
                     <td colspan="2" class="tdSectionHdr">TOP MENU CONTENT</td>
                </tr>
                 <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtTopMenuContent" runat="server" class="text_big" TextMode="MultiLine" Rows="5"></asp:TextBox>
                    </td>
                </tr>
            <%} %>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tfoot>
                <tr>
                    <%--<td></td>--%>
                    <td colspan="2">
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" 
                                                ToolTip="Save" 
                                                class="btn btnSave" 
                                                OnClick="lnkbtnSave_Click" 
                                                ValidationGroup="grpPageDetail"
                                                OnClientClick="return submitForm(this);">Save</asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnPreview" runat="server" Text="Preview" ToolTip="Preview" class="btn" CausesValidation="false" OnClick="lnkbtnPreview_Click" Visible="false"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" class="btn btnCancel" Visible="false" CausesValidation="false" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>
