﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmUserView.ascx.cs" Inherits="ctrl_ucAdmUserView" %>

<asp:HyperLink ID="hypUserView" ToolTip="<%$ Resources:hypUserView.Text %>" runat="server" CssClass="header__action__button header__action__button--manageUserView" Visible="false" Target="_blank">
    <i class="material-icons">settings</i>
    <span><%= GetLocalResourceObject("hypUserView.Text") %></span>
</asp:HyperLink>
<asp:HyperLink ID="hypUserViewManage" ToolTip="<%$ Resources:hypUserViewManage.Text %>" runat="server" CssClass="header__action__button header__action__button--gotoUserView" Visible="false" Target="_blank">
    <i class="material-icons">remove_red_eye</i>
    <span><%= GetLocalResourceObject("hypUserViewManage.Text") %></span>
</asp:HyperLink>
<asp:HyperLink ID="hypUpdateProfile" ToolTip="<%$ Resources:hypUpdateProfile.Text %>" runat="server" CssClass="header__action__button header__action__button--updateProfile" Visible="false">
    <i class="material-icons">person</i>
    <span><%= GetLocalResourceObject("hypUpdateProfile.Text") %></span>
</asp:HyperLink>