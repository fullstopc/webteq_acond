﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.IO;
using System.Configuration;
using System.Text.RegularExpressions;

public partial class ctrl_ucAdmControllerDetails : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _ctrlId = 0;
    protected int _mode = 1;
    protected string _pageListingURL = "";
    #endregion

    #region "Property Method"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int ctrlId
    {
        get
        {
            if (ViewState["CTRLID"] == null)
            {
                return _ctrlId;
            }
            else
            {
                return int.Parse(ViewState["CTRLID"].ToString());
            }
        }
        set { ViewState["CTRLID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int intRole = 0;
            if (!string.IsNullOrEmpty(ddlRole.SelectedValue)) { intRole = Convert.ToInt16(ddlRole.SelectedValue); }

            string strControllerName = txtControllerName.Text.Trim();
            string strControllerEmail = txtControllerEmail.Text.Trim();
            string strPwd = txtPassword.Text.Trim();
            int intActive = 0;
            if (chkboxActive.Checked) { intActive = 1; }

            clsController ctrl = new clsController();
            int intRecordAffected = 0;

            clsMD5 md5 = new clsMD5();
            string encryptPwd = md5.encrypt(strPwd);

            Boolean boolAdded = false;
            Boolean boolEdited = false;
            Boolean boolSuccess = true;

            if (mode == 1)
            {
                intRecordAffected = ctrl.addController(intRole, strControllerName, strControllerEmail, encryptPwd, intActive, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1)
                {
                    ctrlId = ctrl.ctrlId;
                    boolAdded = true;
                }
                else { boolSuccess = false; }
            }
            else if (mode == 2)
            {
                if (!ctrl.isExactSameSetController(ctrlId, intRole, strControllerName, strControllerEmail, intActive))
                {
                    intRecordAffected = ctrl.updateControllerById(ctrlId, intRole, strControllerName, strControllerEmail, intActive);

                    if (intRecordAffected == 1)
                    {
                        boolEdited = true;
                    }
                    else
                    {
                        boolSuccess = false;
                    }
                }

                if (boolSuccess && !string.IsNullOrEmpty(strPwd))
                {
                    if (string.Compare(hdnPwd.Value.Trim(), encryptPwd) != 0)
                    {
                        intRecordAffected = ctrl.updateControllerById(ctrlId, encryptPwd);

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;
                        }
                        else
                        {
                            boolSuccess = false;
                        }
                    }
                }
            }
           
            if (mode == 1)
            {
                if (boolAdded)
                {
                    Session["NEWCONTROLLERID"] = ctrl.ctrlId;

                    Response.Redirect(currentPageName + "?id=" + ctrl.ctrlId);
                }
                else
                {
                    if (!boolSuccess)
                    {
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new Controller. Please try again.</div>";

                        Response.Redirect(currentPageName);
                    }
                }
            }
            else if (mode == 2)
            {
                if (boolEdited)
                {
                    Session["EDITEDCONTROLLERID"] = ctrlId;

                    Response.Redirect(Request.Url.ToString());
                }
                else
                {
                    if (boolSuccess)
                    {
                        Session["EDITEDCONTROLLERID"] = ctrlId;
                        Session["NOCHANGE"] = 1;

                        Response.Redirect(Request.Url.ToString());
                    }
                    else
                    {
                        ctrl.extractControllerById(ctrlId, 0);

                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit Controller (" + ctrl.ctrlName + "). Please try again.</div>";

                        Response.Redirect(currentPageName);
                    }
                }
            }
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string strDeletedControllerName = "";
        clsMis.performDeleteController(ctrlId, ref strDeletedControllerName);

        Session["DELETEDCONTROLLERNAME"] = strDeletedControllerName;
        Response.Redirect(currentPageName);
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void setPageProperties()
    {
        clsMis mis = new clsMis();
        DataSet ds = new DataSet();
        ds = mis.getListByListGrp("CONTROLLER ROLE", 0);
        DataView dv = new DataView(ds.Tables[0]);

        ddlRole.DataSource = dv;
        ddlRole.DataTextField = "LIST_NAME";
        ddlRole.DataValueField = "LIST_VALUE";
        ddlRole.DataBind();

        ListItem ddlRoleDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlRole.Items.Insert(0, ddlRoleDefaultItem);

        lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";

        hdnPwd.Value = "";

        switch (mode)
        {
            case 1:
                break;
            case 2:
                lnkbtnDelete.Visible = true;
                lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteController.Text").ToString() + "'); return false;";

                if (ctrlId > 0) { fillForm(); }
                break;
        }
    }

    protected void fillForm()
    {
        clsController ctrl = new clsController();

        if (ctrl.extractControllerById(ctrlId, 0))
        {
            if (ctrl.ctrlRole != 0)
            {
                ddlRole.SelectedValue = ctrl.ctrlRole.ToString();
            }

            txtControllerName.Text = ctrl.ctrlName;
            txtControllerEmail.Text = ctrl.ctrlEmail;

            hdnPwd.Value = ctrl.ctrlPassword;

            if (ctrl.ctrlActive != 0) { chkboxActive.Checked = true; }
            else { chkboxActive.Checked = false; }
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }
    #endregion

}
