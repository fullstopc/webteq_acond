﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;

public partial class ctrl_ucAdmProductPrice : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected string _pageListingURL = "";
    protected int _prodId = 0;

    protected decimal prodCost;
    protected decimal prodPrice;
    protected decimal prodPromPrice;
    protected DateTime prodPromStartDate = DateTime.MaxValue;
    protected DateTime prodPromEndDate = DateTime.MaxValue;

    protected int intItemCount = 0;
    protected int intItemTotal = 0;
    public bool boolSubContent = false;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName2(true); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }

    public int prodId
    {
        get
        {
            if (ViewState["PRODID"] == null)
            {
                return _prodId;
            }
            else
            {
                return int.Parse(ViewState["PRODID"].ToString());
            }
        }
        set { ViewState["PRODID"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (save())
            {
                Session["EDITPRODID"] = prodId;
            }
            else
            {
                Session["ERRMSG"] = "div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to assign product pricing details. Please try again.</div>";
            }

            Response.Redirect(Request.Url.ToString());
        }
    }

    protected void rptPricing_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            intItemCount += 1;

            string strName1 = DataBinder.Eval(e.Item.DataItem, "PRICING_NAME1").ToString();
            string strName2 = DataBinder.Eval(e.Item.DataItem, "PRICING_NAME2").ToString();
            decimal decPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "PRICING_PRICE"));
            int intEquivalent = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PRICING_EQUIVALENT"));

            Label lblName = (Label)e.Item.FindControl("lblName");
            lblName.Text = strName1;

            Label lblName2 = (Label)e.Item.FindControl("lblName2");
            lblName2.Text = strName2;
            
            Label lblPrice = (Label)e.Item.FindControl("lblPrice");
            lblPrice.Text = decPrice.ToString();//GetGlobalResourceObject("GlobalResource", "prodCurrency.Text").ToString() + decPrice.ToString();

            Label lblEquivalent = (Label)e.Item.FindControl("lblEquivalent");
            lblEquivalent.Text = "Equivalent to "+intEquivalent.ToString();

            Label lblNo = (Label)e.Item.FindControl("lblNo");
            lblNo.Text = intItemCount.ToString();

            clsProduct prod = new clsProduct();
            if (prod.extractProdById(prodId, 1)) { lblEquivalent.Text += " "+prod.prodUOM; }

            if (intItemCount == 1)
            {
                LinkButton lnkbtnUp = (LinkButton)e.Item.FindControl("lnkbtnUp");
                lnkbtnUp.Enabled = false;
                lnkbtnUp.CssClass = "actLinkDisabled2";
            }

            if (intItemCount == intItemTotal)
            {
                LinkButton lnkbtnDown = (LinkButton)e.Item.FindControl("lnkbtnDown");
                lnkbtnDown.Enabled = false;
                lnkbtnDown.CssClass = "actLinkDisabled2";
            }

            int intActive = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PRICING_ACTIVE"));

            if (intActive != 0)
            {
                LinkButton lnkbtnDeactivate = (LinkButton)e.Item.FindControl("lnkbtnDeactivate");
                lnkbtnDeactivate.Visible = true;
            }
            else
            {
                LinkButton lnkbtnActivate = (LinkButton)e.Item.FindControl("lnkbtnActivate");
                lnkbtnActivate.Visible = true;
            }

            LinkButton lnkbtnDelete = (LinkButton)e.Item.FindControl("lnkbtnDelete");
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteProductPricing.Text").ToString() + "'); return false;";
        }
    }

    protected void rptPricing_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        string strArg = e.CommandArgument.ToString();
        string[] strArgSplit = strArg.Split((char)'|');

        int intId = int.Parse(strArgSplit[0]);

        if (e.CommandName == "cmdUp")
        {
            int intOrder = Convert.ToInt16(strArgSplit[1]);
            int intPrevOrder = 0;
            int intPrevId = 0;

            clsProduct prod = new clsProduct();
            DataSet ds = new DataSet();
            ds = prod.getPricingListById(prodId, 0);
            DataRow[] foundRow;
            foundRow = ds.Tables[0].Select("PRICING_ORDER < " + intOrder + " AND PROD_ID = " + prodId, "PRICING_ORDER DESC");

            if (foundRow.Length > 0)
            {
                intPrevOrder = Convert.ToInt16(foundRow[0]["PRICING_ORDER"]);
                intPrevId = int.Parse(foundRow[0]["PRICING_ID"].ToString());
            }

            prod.updatePricingOrderById(intId, intPrevOrder);
            prod.updatePricingOrderById(intPrevId, intOrder);

            clsMis.resetListing();
            bindRptData();
        }
        else if (e.CommandName == "cmdDown")
        {
            int intOrder = Convert.ToInt16(strArgSplit[1]);
            int intNextOrder = 0;
            int intNextId = 0;

            clsPage page = new clsPage();
            page.updateOrderById2(intId, intNextOrder);
            page.updateOrderById2(intNextId, intOrder);

            bindRptData();

            clsProduct prod = new clsProduct();
            DataSet ds = new DataSet();
            ds = prod.getPricingListById(prodId, 0);
            DataRow[] foundRow;
            foundRow = ds.Tables[0].Select("PRICING_ORDER > " + intOrder + " AND PROD_ID = " + prodId, "PRICING_ORDER ASC");

            if (foundRow.Length > 0)
            {
                intNextOrder = Convert.ToInt16(foundRow[0]["PRICING_ORDER"]);
                intNextId = int.Parse(foundRow[0]["PRICING_ID"].ToString());
            }

            prod.updatePricingOrderById(intId, intNextOrder);
            prod.updatePricingOrderById(intNextId, intOrder);

            clsMis.resetListing();
            bindRptData();
        }
        else if (e.CommandName == "cmdActive")
        {
            clsProduct prod = new clsProduct();
            prod.updatePricingActiveById(intId, 1);

            clsMis.resetListing();
            bindRptData();
        }
        else if (e.CommandName == "cmdDeactive")
        {
            clsProduct prod = new clsProduct();
            prod.updatePricingActiveById(intId, 0);

            clsMis.resetListing();
            bindRptData();
        }
        else if (e.CommandName == "cmdEdit")
        {
            int pricingID = Convert.ToInt32(e.CommandArgument);

            clsProduct prod = new clsProduct();
            DataTable dt = prod.getPricingListById(prodId, 1).Tables[0];
            DataRow[] row = dt.Select("PRICING_ID = " + pricingID);

            if(row.Length == 1)
            {
                txtPriceName1.Text = row[0]["pricing_name1"].ToString();
                txtPriceName2.Text = row[0]["pricing_name2"].ToString();
                txtPricing.Text = row[0]["pricing_price"].ToString();
                txtEquivalent.Text = row[0]["pricing_equivalent"].ToString();
                hdnPricingID.Value = pricingID.ToString();

                lnkbtnEditPricing.Style.Add("display", "inline-block");
                lnkbtnCancelPricing.Style.Add("display", "inline-block");
                lnkbtnAddPricing.Style.Add("display", "none");
            }
            

        }
        else if (e.CommandName == "cmdDelete")
        {
            int intOrder = Convert.ToInt16(strArgSplit[1]);

            clsProduct prod = new clsProduct();
            int intRecordAffected = 0;

            intRecordAffected = prod.deletePricingById(intId);

            if (intRecordAffected == 1)
            {
                prod.updatePricingOrder(prodId, intOrder);

                clsMis.resetListing();
                bindRptData();
            }
        }
    }
    protected void rptPromo_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        int promoID = Convert.ToInt32(e.CommandArgument);
        clsProduct prod = new clsProduct();
        switch (e.CommandName)
        {
            case "cmdDelete":
                if(prod.deletePromo(promoID) == 1)
                {
                    bindRptPromo();
                }
                break;
            case "cmdEdit":
                editingPromo();
                hdnPromoID.Value = promoID.ToString();

                RepeaterItem rptItem = rptPromo.Items[e.Item.ItemIndex];
                Literal litPrice = rptItem.FindControl("litPrice") as Literal;
                txtProdPromPrice.Text = litPrice.Text;

                Literal litFrom = rptItem.FindControl("litFrom") as Literal;
                txtProdPromStartDate.Text = litFrom.Text;

                Literal litTo = rptItem.FindControl("litTo") as Literal;
                txtProdPromEndDate.Text = litTo.Text;

                lnkbtnUpdatePromo.Attributes["data-promoid"] = e.Item.ItemIndex.ToString();
                break;
        }
        
    }
    protected void lnkbtnAddPricing_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strName1 = txtPriceName1.Text.Trim();
            string strName2 = txtPriceName2.Text.Trim();
            int intEquivalent = 0;
            int intTryParse;
            decimal decPrice = Convert.ToDecimal("0.00");
            decimal decTryParse;
            if (decimal.TryParse(txtPricing.Text.Trim(), out decTryParse)) { decPrice = decTryParse; }
            if (string.IsNullOrEmpty(txtEquivalent.Text.Trim())) { intEquivalent = 1; }
            else if (int.TryParse(txtEquivalent.Text.Trim(), out intTryParse)) { intEquivalent = intTryParse; }

            clsProduct prod = new clsProduct();
            int intRecordAffected = 0;

            if (!prod.isPricingExist(prodId, strName1, strName2, decPrice, intEquivalent))
            {
                intRecordAffected = prod.addPricing(prodId, strName1, strName2, decPrice, intEquivalent, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1)
                {
                    Session["EDITPRODID"] = prodId;
                    Response.Redirect(Request.Url.ToString());
                }
                else
                {
                    //pnlAck.Visible = true;
                    //litAck.Text = "div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to assign product pricing details. Please try again.</div>";
                }
            }
        }
    }
    protected void lnkbtnEditPricing_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int intID = Convert.ToInt32(hdnPricingID.Value);

            string strName1 = txtPriceName1.Text.Trim();
            string strName2 = txtPriceName2.Text.Trim();
            int intEquivalent = 0;
            int intTryParse;
            decimal decPrice = Convert.ToDecimal("0.00");
            decimal decTryParse;
            if (decimal.TryParse(txtPricing.Text.Trim(), out decTryParse)) { decPrice = decTryParse; }
            if (string.IsNullOrEmpty(txtEquivalent.Text.Trim())) { intEquivalent = 1; }
            else if (int.TryParse(txtEquivalent.Text.Trim(), out intTryParse)) { intEquivalent = intTryParse; }

            clsProduct prod = new clsProduct();
            int intRecordAffected = prod.updatePricingById(intID, strName1, strName2, decPrice, intEquivalent, int.Parse(Session["ADMID"].ToString()));
            if (intRecordAffected == 1)
            {
                Session["EDITPRODID"] = prodId;
                Response.Redirect(Request.Url.ToString());
            }
        }
    }
    
    protected void lnkbtnAddPromo_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            clsProduct prod = new clsProduct();
            prod.promoName = "";
            prod.promoProdID = prodId;
            prod.promoPrice = Convert.ToDecimal(txtProdPromPrice.Text.Trim());
            prod.promoFrom = Convert.ToDateTime(txtProdPromStartDate.Text.Trim());
            prod.promoTo = Convert.ToDateTime(txtProdPromEndDate.Text.Trim());
            prod.promoCreatedBy = Convert.ToInt32(Session["ADMID"]);

            int intRecordAffected = prod.addPromo();

            if (intRecordAffected == 1)
            {
                Session["EDITPRODID"] = prodId;
                Response.Redirect(Request.Url.ToString());
            }
            else
            {
                //pnlAck.Visible = true;
                //litAck.Text = "div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to assign product pricing details. Please try again.</div>";
            }
            
        }
    }

    protected void lnkbtnUpdatePromo_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            clsProduct prod = new clsProduct();
            prod.promoName = "";
            prod.promoID = Convert.ToInt32(hdnPromoID.Value);
            prod.promoPrice = Convert.ToDecimal(txtProdPromPrice.Text.Trim());
            prod.promoFrom = Convert.ToDateTime(txtProdPromStartDate.Text.Trim());
            prod.promoTo = Convert.ToDateTime(txtProdPromEndDate.Text.Trim());
            prod.promoUpdatedBy = Convert.ToInt32(Session["ADMID"]);

            int intRecordAffected = prod.updatePromo();

            if (intRecordAffected == 1)
            {
                Session["EDITPRODID"] = prodId;
                Response.Redirect(Request.Url.ToString());
            }
            else
            {
                //pnlAck.Visible = true;
                //litAck.Text = "div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to assign product pricing details. Please try again.</div>";
            }

        }
    }
    
    #endregion

    #region "Methods"
    public bool save()
    {
        decimal decTryParse;

        if (decimal.TryParse(txtProdCost.Text.Trim(), out decTryParse))
        {
            prodCost = decTryParse;
        }

        if (decimal.TryParse(txtProdPrice.Text.Trim(), out decTryParse))
        {
            prodPrice = decTryParse;
        }

        if (decimal.TryParse(txtProdPromPrice.Text.Trim(), out decTryParse))
        {
            prodPromPrice = decTryParse;
        }

        if (!string.IsNullOrEmpty(txtProdPromStartDate.Text.Trim()) && !string.IsNullOrEmpty(txtProdPromEndDate.Text.Trim()))
        {
            DateTime dtTryParse;

            if (DateTime.TryParse(txtProdPromStartDate.Text.Trim(), out dtTryParse))
            {
                prodPromStartDate = dtTryParse;
            }

            if (DateTime.TryParse(txtProdPromEndDate.Text.Trim(), out dtTryParse))
            {
                prodPromEndDate = dtTryParse;
            }
        }

        // if multiple promo set, let the data empty
        if (Session[clsAdmin.CONSTPROJECTPRODPROMO] != null &&
            Convert.ToString(Session[clsAdmin.CONSTPROJECTPRODPROMO]) != "" &&
            Convert.ToInt16(Session[clsAdmin.CONSTPROJECTPRODPROMO]) == 1)
        {
            prodPromPrice = 0;
            prodPromStartDate = DateTime.MaxValue;
            prodPromEndDate = DateTime.MaxValue;
        }


        clsProduct prod = new clsProduct();
        int intRecordAffected = 0;

        if (!prod.isProdPriceExist(prodId))
        {
            intRecordAffected = prod.addProdPrice(prodId, prodCost, prodPrice, prodPromPrice, prodPromStartDate, prodPromEndDate);

            if (intRecordAffected == 1)
            {
                return true;
            }
        }
        else
        {
            if (!prod.isExactSameProdSet3(prodId, prodCost, prodPrice, prodPromPrice, prodPromStartDate, prodPromEndDate))
            {
                intRecordAffected = prod.updateProdPrice(prodId, prodCost, prodPrice, prodPromPrice, prodPromStartDate, prodPromEndDate);
                if (intRecordAffected == 1)
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        return false;
    }
    public void registerScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("FORMSECT3")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "initSect3();";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "FORMSECT3", strJS, false);
        }
    }

    public void fill()
    {
        if (!IsPostBack)
        {
            // check is single or multiple promo set group.
            if (Session[clsAdmin.CONSTPROJECTPRODPROMO] != null && 
                Convert.ToString(Session[clsAdmin.CONSTPROJECTPRODPROMO]) != "" &&
                Convert.ToInt16(Session[clsAdmin.CONSTPROJECTPRODPROMO]) == 1)
            {
                multiPromoSet();
            }
            else
            {
                singlePromoSet();
            }

            clsConfig config = new clsConfig();
            string strCurrency = config.currency;
            litProdCostCurr.Text = strCurrency;
            litProdPriceCurr.Text = strCurrency;
            litProdPromPriceCurr.Text = strCurrency;
            litPricingUnit.Text = strCurrency;

            // extract UOM
            spanUOM.InnerHtml = "unit";
            clsProduct prod = new clsProduct();
            if (prod.extractProdById(prodId, 1)){spanUOM.InnerHtml = prod.prodUOM;}


            if (mode == 2)
            {
                fillForm();
            }

            // if sub content
            if (boolSubContent)
            {
                trSingleAction.Visible = false;
                tblPriceSet.Visible = false;
            }
        }

        registerScript();

        lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
    }

    protected void fillForm()
    {
        clsProduct prod = new clsProduct();
        if (prod.extractProdById3(prodId, 0))
        {
            txtProdCost.Text = prod.prodCost > 0 ? prod.prodCost.ToString() : "";
            txtProdPrice.Text = prod.prodPrice > 0 ? prod.prodPrice.ToString() : "";
            txtProdPromPrice.Text = prod.prodPromPrice > 0 ? prod.prodPromPrice.ToString() : "";

            if (prod.prodPromStartDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
            {
                txtProdPromStartDate.Text = prod.prodPromStartDate.ToString("dd MMM yyyy HH:mm tt");
            }

            if (prod.prodPromEndDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
            {
                txtProdPromEndDate.Text = prod.prodPromEndDate.ToString("dd MMM yyyy HH:mm tt");
            }

            bindRptData();
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }

    protected void bindRptData()
    {
        bindRptPricing();
        bindRptPromo();
    }
    private void bindRptPricing()
    {
        clsProduct prod = new clsProduct();
        DataSet ds = new DataSet();
        ds = prod.getPricingListById(prodId, 0);
        DataView dv = new DataView(ds.Tables[0]);
        dv.Sort = "PRICING_ORDER ASC";

        if (dv.Count > 0)
        {
            intItemCount = 0;
            intItemTotal = dv.Count;
            rptPricing.DataSource = dv;
            rptPricing.DataBind();
        }
        else
        {
            trPricingNoRecord.Visible = true;
            trPricingList.Visible = false;
        }
    }
    private void bindRptPromo()
    {
        clsProduct prod = new clsProduct();
        DataTable dt = prod.getPromoDataTableByProdID(prodId);

        if (dt != null && dt.Rows.Count > 0)
        {
            rptPromo.DataSource = dt;
            rptPromo.DataBind();
        }
    }
    private void singlePromoSet()
    {
        tbodyMultiProdPromo.Visible = false;
        trMultiAction.Visible = false;
        trProdPromoAdd.Visible = false;
        cvProdPromPeriod2.Enabled = false;
        rvProdPromPrice.Enabled = false;

        trSingleAction.Visible = true;
    }

    private void multiPromoSet()
    {
        tbodyMultiProdPromo.Visible = true;
        trMultiAction.Visible = true;
        trProdPromoAdd.Visible = true;
        cvProdPromPeriod2.Enabled = true;
        rvProdPromPrice.Enabled = true;

        trSingleAction.Visible = false;
    }

    private void editingPromo()
    {
        lnkbtnAddPromo.Visible = false;
        lnkbtnUpdatePromo.Visible = true;
    }

    private void editedPromo()
    {
        lnkbtnAddPromo.Visible = true;
        lnkbtnUpdatePromo.Visible = false;

        lnkbtnUpdatePromo.Attributes["data-promoid"] = "-1";
        txtProdPromEndDate.Text = "";
        txtProdPromStartDate.Text = "";
        txtProdPrice.Text = "";
        hdnPromoID.Value = "";
    }
    #endregion
}
