﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrBanner.ascx.cs" Inherits="ctrl_ucUsrBanner" %>

<asp:Panel ID="pnlBannerHome" runat="server" Visible="true">
    <%--<asp:ImageButton ID="imgbtnFacebookHome" runat="server" Width="197" Height="143" CssClass="imgbtnFacebookHome" meta:resourcekey="imgbtnFacebook" />
    <asp:ImageButton ID="imgbtnPaymentHome" runat="server" Width="184" Height="143" CssClass="imgbtnPaymentHome" meta:resourcekey="imgbtnPayment" />
    <asp:ImageButton ID="imgbtnContactUsHome" runat="server" Width="245" Height="143" CssClass="imgbtnContactUsHome" meta:resourcekey="imgbtnContactUs" OnClick="imgbtnContactUs_Click" />
    <asp:Image ID="ImgPro" runat="server" Width="164" Height="200" />
    <asp:Image ID="ImgAccess" runat="server" Width="164" Height="200" />
    <asp:Image ID="ImgPaypal" runat="server" Width="164" Height="200" />
    <asp:ImageButton ID="imgbtnFaqHome" runat="server" Width="164" Height="200" CssClass="imgbtnFaqHome" meta:resourcekey="imgbtnFaq" OnClick="imgbtnFaq_Click" />
    <asp:ImageButton ID="imgbtnContactUsHome" runat="server" Width="164" Height="200" CssClass="imgbtnContactUsHome" meta:resourcekey="imgbtnContactUs" OnClick="imgbtnContactUs_Click" />--%>
    <asp:ImageButton ID="imgbtnProductHome" runat="server" Width="164" Height="200" CssClass="imgbtnProductHome" meta:resourcekey="imgbtnProduct" title="New Arrival" OnClick="imgbtnProductHome_Click"/>
    <asp:ImageButton ID="imgbtnAccessoryHome" runat="server" Width="164" Height="200" CssClass="imgbtnAccessoryHome" meta:resourcekey="imgbtnAccessory" OnClick="imgbtnProduct_Click" title="Product"/>
    <asp:ImageButton ID="imgbtnPaypalHome" runat="server" Width="164" Height="200" CssClass="imgbtnPaypalHome" meta:resourcekey="imgbtnPaypal" title="Paypal"/>
    <asp:ImageButton ID="imgbtnFaqHome" runat="server" Width="164" Height="200" CssClass="imgbtnFaqHome" meta:resourcekey="imgbtnFaq" OnClick="imgbtnFaq_Click" title="FAQ"/>
    <asp:ImageButton ID="imgbtnContactUsHome" runat="server" Width="164" Height="200" CssClass="imgbtnContactUsHome" meta:resourcekey="imgbtnContactUs" OnClick="imgbtnContactUs_Click" />
</asp:Panel>
<asp:Panel ID="pnlBannerSub" runat="server" Visible="true">
    <asp:ImageButton ID="imgbtnProductSub" runat="server" Width="164" Height="200" CssClass="imgbtnProductSub" meta:resourcekey="imgbtnProduct" title="New Arrival" OnClick="imgbtnProductHome_Click"/>
    <asp:ImageButton ID="imgbtnAccessorySub" runat="server" Width="164" Height="200" CssClass="imgbtnAccessorySub" meta:resourcekey="imgbtnAccessory" OnClick="imgbtnProduct_Click" title="Product"/>
    <asp:ImageButton ID="imgbtnPaypalSub" runat="server" Width="164" Height="200" CssClass="imgbtnPaypalSub" meta:resourcekey="imgbtnPaypal" title="Paypal"/>
    <asp:ImageButton ID="imgbtnFaqSub" runat="server" Width="164" Height="200" CssClass="imgbtnFaqSub" meta:resourcekey="imgbtnFaq" OnClick="imgbtnFaq_Click" title="FAQ"/>
    <asp:ImageButton ID="imgbtnContactUsSub" runat="server" Width="164" Height="200" CssClass="imgbtnContactUsSub" meta:resourcekey="imgbtnContactUs" OnClick="imgbtnContactUs_Click" />
</asp:Panel>