﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmPaymentGateway.ascx.cs" Inherits="ctrl_ucAdmPaymentGateway" %>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>watermark.js" type="text/javascript"></script>

<script type="text/javascript">

</script>

<asp:Panel ID="pnlAdmCMSControlPanel" runat="server" CssClass="divAdmCMSControlPanel">
    <asp:Panel ID="pnlFormDetails" runat="server" CssClass="">
        <div id="divBankTransfer">
            <table class="tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td class="tdSectionHdr" colspan="2">
                            Bank Transfer</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel">Bank Selection<span class="attention_compulsory"> *</span></td>
                        <td>
                            <asp:DropDownList ID="ddlBankName" runat="server" CssClass="ddl_fullwidth compulsory"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvBankSel" runat="server" ErrorMessage="<br />Please select Bank." ControlToValidate="ddlBankName" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgBankTrans"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Bank Name<span class="attention_compulsory"> *</span></td>
                        <td>
                        <asp:TextBox ID="txtBankName" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ErrorMessage="<br />Please enter Bank Name." ControlToValidate="txtBankName" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgBankTrans"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Bank Account Name<span class="attention_compulsory"> *</span></td>
                        <td>
                        <asp:TextBox ID="txtBankAccName" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvBankAccName" runat="server" ErrorMessage="<br />Please enter Bank Account Name." ControlToValidate="txtBankAccName" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgBankTrans"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Bank Account Number<span class="attention_compulsory"> *</span></td>
                        <td>
                        <asp:TextBox ID="txtBankAccNum" runat="server" CssClass="text_fullwidth compulsory"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvBankAccNum" runat="server" ErrorMessage="<br />Please enter Bank Account Number." ControlToValidate="txtBankAccNum" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgBankTrans"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel"></td>
                        <td><asp:LinkButton ID="lnkbtnSaveBankTransfer" runat="server" Text="Save" ToolTip="Save" CssClass="btnConfirm" OnClick="lnkbtnSaveBankTransfer_Click" ValidationGroup="vgBankTrans"></asp:LinkButton></td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2">
                            <asp:Repeater ID="rptBankTrans" runat="server" OnItemDataBound="rptBankTrans_ItemDataBound" OnItemCommand="rptBankTrans_ItemCommand">
                                <HeaderTemplate>
                                    <table cellpadding="0" cellspacing="0" border="0" class="dataTbl">
                                        <thead>
                                            <th>No.</th>
                                            <th>Bank</th>
                                            <th>Bank Name</th>
                                            <th>Bank Account Name</th>
                                            <th>Bank Account Number</th>
                                            <th style="">Action</th>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><asp:Literal ID="litNo" runat="server"></asp:Literal></td>
                                        <td><asp:Literal ID="litBankSel" runat="server"></asp:Literal></td>
                                        <td><asp:Literal ID="litBankName" runat="server"></asp:Literal></td>
                                        <td><asp:Literal ID="litBankAccName" runat="server"></asp:Literal></td>
                                        <td><asp:Literal ID="litBankAccNum" runat="server"></asp:Literal></td>
                                        <td class="tdAct">
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CssClass="lnkbtn lnkbtnDelete" CommandName="cmdDelete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "BT_ID") %>'></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="borderSplitter" runat="server" id="divSplitPaypal"></div>
        <div id="divPayPal" runat="server">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td class="tdSectionHdr" colspan="2">
                            PayPal Detail</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel">Active</td>
                        <td class="tdLabel"><asp:CheckBox ID="chkPaypalActive" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Name</td>
                        <td><asp:TextBox ID="txtPaypalName" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Use SandBox</td>
                        <td><asp:DropDownList ID="ddlPaypalUseSandBox" runat="server" CssClass="ddl_fullwidth compulsory"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Paypal SandBox URL</td>
                        <td><asp:TextBox ID="txtPaypalSandboxUrl" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">PayPal URL</td>
                        <td><asp:TextBox ID="txtPaypalUrl" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Currency Code</td>
                        <td><asp:TextBox ID="txtPaypalCurrency" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">CMD</td>
                        <td><asp:TextBox ID="txtPaypalCmd" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Business Email</td>
                        <td><asp:TextBox ID="txtPaypalBusinessEmail" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Return URL(Cancel)</td>
                        <td><asp:TextBox ID="txtPaypalReturnUrl" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">PDT Token</td>
                        <td><asp:TextBox ID="txtPaypalPDTToken" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="borderSplitter" runat="server" id="divSplitMolpay"></div>
        <div id="divMolpay" runat="server">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td class="tdSectionHdr" colspan="2">
                            MolPay Detail</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel">Active</td>
                        <td class="tdLabel"><asp:CheckBox ID="chkMolpayActive" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Name</td>
                        <td><asp:TextBox ID="txtMolpayName" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">molpay</td>
                        <td><asp:TextBox ID="txtMolpay" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">merchantID</td>
                        <td><asp:TextBox ID="txtMolpayMerchantID" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">verifykey</td>
                        <td><asp:TextBox ID="txtMolpayVerifyKey" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">cur</td>
                        <td><asp:TextBox ID="txtMolpayCur" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">country</td>
                        <td><asp:TextBox ID="txtMolpayCountry" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">langcode</td>
                        <td><asp:TextBox ID="txtMolpayLangCode" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">return</td>
                        <td><asp:TextBox ID="txtMolpayReturn" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">returnpin</td>
                        <td><asp:TextBox ID="txtMolpayReturnPin" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="borderSplitter" runat="server" id="divSplitIPay88"></div>
        <div id="divIPay88" runat="server">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td class="tdSectionHdr" colspan="2">
                            iPay88 Detail</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel">Active</td>
                        <td class="tdLabel"><asp:CheckBox ID="chkIPay88Active" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Name</td>
                        <td><asp:TextBox ID="txtIPay88Name" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">iPay88EntryPage</td>
                        <td><asp:TextBox ID="txtIPay88EntryPage" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">iPay88EnquiryPage</td>
                        <td><asp:TextBox ID="txtIPay88EnquiryPage" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">responseUrl</td>
                        <td><asp:TextBox ID="txtIPay88ResUrl" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">requestUrl</td>
                        <td><asp:TextBox ID="txtIPay88ReqUrl" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">merchantKey</td>
                        <td><asp:TextBox ID="txtIPay88MerchantKey" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">merchantCode</td>
                        <td><asp:TextBox ID="txtIPay88MerchantCode" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">paymentId</td>
                        <td><asp:TextBox ID="txtIPay88PayId" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">currency</td>
                        <td><asp:TextBox ID="txtIPay88Cur" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">prodDesc</td>
                        <td><asp:TextBox ID="txtIPay88ProdDesc" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">backendUrl</td>
                        <td><asp:TextBox ID="txtIPay88BackendUrl" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="borderSplitter" runat="server" id="divSplitIPay88ENets"></div>
        <div id="divIPay88_eNets" runat="server">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td class="tdSectionHdr" colspan="2">
                            iPay88 eNets Detail</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel">Active</td>
                        <td class="tdLabel"><asp:CheckBox ID="chkIPay88ENetsActive" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Name</td>
                        <td><asp:TextBox ID="txtIPay88ENetsName" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">iPay88EntryPage</td>
                        <td><asp:TextBox ID="txtIPay88ENetsEntryPage" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">iPay88EnquiryPage</td>
                        <td><asp:TextBox ID="txtIPay88ENetsEnquiryPage" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">responseUrl</td>
                        <td><asp:TextBox ID="txtIPay88ENetsResUrl" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">requestUrl</td>
                        <td><asp:TextBox ID="txtIPay88ENetsReqUrl" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">merchantKey</td>
                        <td><asp:TextBox ID="txtIPay88ENetsMerchantKey" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">merchantCode</td>
                        <td><asp:TextBox ID="txtIPay88ENetsMerchantCode" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">paymentId</td>
                        <td><asp:TextBox ID="txtIPay88ENetsPayId" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">currency</td>
                        <td><asp:TextBox ID="txtIPay88ENetsCur" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">prodDesc</td>
                        <td><asp:TextBox ID="txtIPay88ENetsProdDesc" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">backendUrl</td>
                        <td><asp:TextBox ID="txtIPay88ENetsBackendUrl" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="borderSplitter" runat="server" id="divSplitEGhl"></div>
        <div id="divEGhl" runat="server">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td class="tdSectionHdr" colspan="2">
                            eGHL Detail</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel">Active</td>
                        <td class="tdLabel"><asp:CheckBox ID="chkEGhlActive" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Name</td>
                        <td><asp:TextBox ID="txtEGhlName" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">useTestUrl</td>
                        <td><asp:TextBox ID="txtEGhlUseTestUrl" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">testPaymentUrl</td>
                        <td><asp:TextBox ID="txtEGhlTestPayUrl" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">paymentUrl</td>
                        <td><asp:TextBox ID="txtEGhlPayUrl" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">transactionType</td>
                        <td><asp:TextBox ID="txtEGhlTransType" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">paymentMethod</td>
                        <td><asp:TextBox ID="txtEGhlPayMethod" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">merchantID</td>
                        <td><asp:TextBox ID="txtEGhlMerchantId" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">merchantPwd</td>
                        <td><asp:TextBox ID="txtEGhlMerchantPwd" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">merchantName</td>
                        <td><asp:TextBox ID="txtEGhlMerchantName" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">returnUrl</td>
                        <td><asp:TextBox ID="txtEGhlReturnUrl" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">callBackUrl</td>
                        <td><asp:TextBox ID="txtEGhlCallBackUrl" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">currencyCode</td>
                        <td><asp:TextBox ID="txtEGhlCurrencyCode" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">pageTimeout</td>
                        <td><asp:TextBox ID="txtEGhlTimeout" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="">
            <table id="tblAction" class="formTbl tblData" cellpadding="0" cellspacing="0" style="padding-top: 0;">
                <tr>
                    <td class="tdLabel"></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated" style="padding-top:0px;">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    
</asp:Panel>
