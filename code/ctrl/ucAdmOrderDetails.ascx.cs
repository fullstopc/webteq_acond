﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class ctrl_ucAdmOrderDetails : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _transId = 0;
    protected int _cancel = 1;
    protected int _status = 0;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int transId
    {
        get
        {
            if (ViewState["TRANSID"] == null)
            {
                return _transId;
            }
            else
            {
                return int.Parse(ViewState["TRANSID"].ToString());
            }
        }
        set { ViewState["TRANSID"] = value; }
    }

    public int cancel
    {
        get
        {
            if (ViewState["CANCEL"] == null)
            {
                return _cancel;
            }
            else
            {
                return Convert.ToInt16(ViewState["CANCEL"]);
            }
        }
        set { ViewState["CANCEL"] = value; }
    }

    public int status
    {
        get
        {
            if (ViewState["STATUS"] == null)
            {
                return _status;
            }
            else
            {
                return Convert.ToInt16(ViewState["STATUS"]);
            }
        }
        set { ViewState["STATUS"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ddlDelivery_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlDelivery.SelectedValue))
        {
            int intDelivery = Convert.ToInt16(ddlDelivery.SelectedValue);

            if (intDelivery == 1)
            {
                lnkbtnSame.Visible = true;
                trDeliveryDetails.Visible = true;
            }
            else if (intDelivery == 2)
            {
                lnkbtnSame.Visible = false;
                trDeliveryDetails.Visible = false;
            }
        }
    }

    protected void lnkbtnSame_Click(object sender, EventArgs e)
    {
        clsShipping shipping = new clsShipping();
        txtCompNameShip.Text = txtCompNameBill.Text.Trim();
        txtNameShip.Text = txtNameBill.Text.Trim();
        txtAddShip.Text = txtAddBill.Text.Trim();
        txtPoscodeShip.Text = txtPoscodeBill.Text.Trim();
        txtCityShip.Text = txtCityBill.Text.Trim();
        txtContactNoShip.Text = txtContactNoBill.Text.Trim();
        ddlCountryShip.SelectedValue = ddlCountryBill.SelectedValue;
        txtEmailShip.Text = txtEmailBill.Text.Trim();

        shipping.extractShippingCountry(ddlCountryBill.SelectedValue, 1);
        if (shipping.shipChkCountry == 1)
        {
            tdCountryShipInput.Visible = true;
            tdCountryShipTxt.Visible = true;
            tdCountryShipInput.ColSpan = 0;
            txtCountryShip.Text = txtCountryBill.Text;
        }
        else
        {
            tdCountryShipInput.Visible = true;
            tdCountryShipTxt.Visible = false;
            tdCountryShipInput.ColSpan = 2;
        }

        if (!string.IsNullOrEmpty(ddlCountryBill.SelectedValue))
        {
            clsCountry country = new clsCountry();

            DataSet dsState = new DataSet();
            
            clsConfig config = new clsConfig();
            if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
            {
                dsState = shipping.getShippingStateList(1);
                DataView dvState;

                if (dsState.Tables.Count > 0)
                {
                    dvState = new DataView(dsState.Tables[0]);

                    dvState.RowFilter = "SHIP_COUNTRY ='" + ddlCountryBill.SelectedValue + "'";

                    if (dvState.Count > 0)
                    {
                        ddlStateShip.DataSource = dvState;
                        ddlStateShip.DataTextField = "SHIP_STATE";
                        ddlStateShip.DataValueField = "SHIP_STATE";
                        ddlStateShip.DataBind();

                        ListItem ddlStateShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                        ddlStateShip.Items.Insert(0, ddlStateShipDefaultItem);

                        shipping.extractShippingStatebyCountry(ddlCountryBill.SelectedValue, ddlStateBill.SelectedValue, 1);
                        if (shipping.shipChkState == 1)
                        {
                            tdStateShipDdl.Visible = true;
                            tdStateShipTxt.Visible = true;
                            tdStateShipDdl.ColSpan = 0;
                            txtStateShip.Text = txtStateBill.Text.Trim();
                        }
                        else
                        {
                            tdStateShipDdl.Visible = true;
                            tdStateShipTxt.Visible = false;
                            tdStateShipDdl.ColSpan = 2;
                        }

                        ddlStateShip.SelectedValue = ddlStateBill.SelectedValue;
                    }
                    else
                    {
                        txtStateShip.Text = txtStateBill.Text.Trim();

                        tdStateShipDdl.Visible = false;
                        tdStateShipTxt.Visible = true;
                    }
                }
                else
                {
                    txtStateShip.Text = txtStateBill.Text.Trim();

                    tdStateShipDdl.Visible = false;
                    tdStateShipTxt.Visible = true;
                }

                if (!string.IsNullOrEmpty(ddlStateBill.SelectedValue))
                {
                    DataSet dsCity = new DataSet();
                    dsCity = shipping.getShippingCityByState(ddlCountryBill.SelectedValue, ddlStateBill.SelectedValue, 1);
                    DataRow[] foundRow;
                    DataView dvCity = new DataView(dsCity.Tables[0]);
                    dvCity.RowFilter = "SHIP_STATE = '" + ddlStateBill.SelectedValue + "' AND SHIP_CITY IS NOT NULL AND SHIP_CITY <> ''";

                    if (dvCity.Count > 0)
                    {
                        ddlCityShip.DataSource = dvCity;
                        ddlCityShip.DataTextField = "SHIP_CITY";
                        ddlCityShip.DataValueField = "SHIP_CITY";
                        ddlCityShip.DataBind();

                        ListItem ddlCityShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                        ddlCityShip.Items.Insert(0, ddlCityShipDefaultItem);

                        shipping.extractShippingCitybyState(ddlCountryBill.SelectedValue, ddlStateBill.SelectedValue, ddlCityBill.SelectedValue, 1);
                        if (shipping.shipChkCity == 1)
                        {
                            tdCityShipDdl.Visible = true;
                            tdCityShipInput.Visible = true;
                            tdCityShipDdl.ColSpan = 0;
                            txtCityShip.Text = txtCityBill.Text.Trim();
                        }
                        else
                        {
                            tdCityShipDdl.Visible = true;
                            tdCityShipInput.Visible = false;
                            tdCityShipDdl.ColSpan = 2;
                        }

                        ddlCityShip.SelectedValue = ddlCityBill.SelectedValue;
                    }
                    else
                    {
                        txtCityShip.Text = txtCityBill.Text.Trim();

                        tdCityShipDdl.Visible = false;
                        tdCityShipInput.Visible = true;
                    }

                }
                else
                {
                    ddlCityShip.Items.Clear();

                    ListItem ddlCityShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                    ddlCityShip.Items.Insert(0, ddlCityShipDefaultItem);

                    tdCityShipDdl.Visible = true;
                    tdCityShipInput.Visible = false;
                }
            }
            else
            {
                dsState = country.getStateList(1);
                DataView dvState;

                if (dsState.Tables.Count > 0)
                {
                    dvState = new DataView(dsState.Tables[0]);

                    dvState.RowFilter = "STATE_DEFAULT = 0 AND STATE_COUNTRY = '" + ddlCountryShip.SelectedValue + "' AND STATE_CODE IS NOT NULL AND STATE_CODE <> ''";

                    if (dvState.Count > 0)
                    {
                        ddlStateShip.DataSource = dvState;
                        ddlStateShip.DataTextField = "STATE_CODE";
                        ddlStateShip.DataValueField = "STATE_CODE";
                        ddlStateShip.DataBind();

                        ListItem ddlStateShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                        ddlStateShip.Items.Insert(0, ddlStateShipDefaultItem);

                        ddlStateShip.SelectedValue = ddlStateBill.SelectedValue;

                        tdStateShipDdl.Visible = true;
                        tdStateShipTxt.Visible = false;
                    }
                    else
                    {
                        txtStateShip.Text = txtStateBill.Text.Trim();

                        tdStateShipDdl.Visible = false;
                        tdStateShipTxt.Visible = true;
                    }
                }
                else
                {
                    txtStateShip.Text = txtStateBill.Text.Trim();

                    tdStateShipDdl.Visible = false;
                    tdStateShipTxt.Visible = true;
                }
            }
        }
        else
        {
            ddlStateShip.Items.Clear();

            ListItem ddlStateShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
            ddlStateShip.Items.Insert(0, ddlStateShipDefaultItem);

            tdStateShipDdl.Visible = true;
            tdStateShipTxt.Visible = false;
        }
    }

    protected void ddlCountryBill_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlCountryBill.SelectedValue))
        {
            clsConfig config = new clsConfig();
            if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
            {
                ddlStateBill.Items.Clear();
                ddlCityBill.Items.Clear();
                txtCityBill.Text = "";

                ListItem ddlCityBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlCityBill.Items.Insert(0, ddlCityBillDefaultItem);

                clsShipping shipping = new clsShipping();
                DataSet dsState = new DataSet();
                dsState = shipping.getShippingStateList(1);
                DataView dvState;

                string strCountry = ddlCountryBill.SelectedValue;

                if (dsState.Tables.Count > 0)
                {
                    dvState = new DataView(dsState.Tables[0]);

                    dvState.RowFilter = "SHIP_COUNTRY = '" + ddlCountryBill.SelectedValue + "'";

                    if (dvState.Count > 0)
                    {
                        ddlStateBill.DataSource = dvState;
                        ddlStateBill.DataTextField = "SHIP_STATE";
                        ddlStateBill.DataValueField = "SHIP_STATE";
                        ddlStateBill.DataBind();

                        ListItem ddlStateBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                        ddlStateBill.Items.Insert(0, ddlStateBillDefaultItem);

                        shipping.extractShippingCountry(ddlCountryBill.SelectedValue, 1);
                        if (shipping.shipChkCountry == 1)
                        {
                            tdCountryBillInput.Visible = true;
                            tdCountryBillTxt.Visible = true;
                            tdCountryBillInput.ColSpan = 0;
                            txtCountryBill.Text = "";
                        }
                        else
                        {
                            tdCountryBillInput.Visible = true;
                            tdCountryBillTxt.Visible = false;
                            tdCountryBillInput.ColSpan = 2;
                        }

                        tdStateBillDdl.Visible = true;
                        tdStateBillTxt.Visible = false;
                    }
                    else
                    {
                        txtStateBill.Text = "";

                        tdStateBillDdl.Visible = false;
                        tdStateBillTxt.Visible = true;
                    }
                }
                else
                {
                    txtStateBill.Text = "";

                    tdStateBillDdl.Visible = false;
                    tdStateBillTxt.Visible = true;
                }
            }
            else
            {
                clsCountry country = new clsCountry();

                DataSet dsState = new DataSet();
                dsState = country.getStateList(1);
                DataView dvState;

                if (dsState.Tables.Count > 0)
                {
                    dvState = new DataView(dsState.Tables[0]);

                    dvState.RowFilter = "STATE_DEFAULT = 0 AND STATE_COUNTRY = '" + ddlCountryBill.SelectedValue + "' AND STATE_CODE IS NOT NULL AND STATE_CODE <> ''";

                    if (dvState.Count > 0)
                    {
                        ddlStateBill.DataSource = dvState;
                        ddlStateBill.DataTextField = "STATE_CODE";
                        ddlStateBill.DataValueField = "STATE_CODE";
                        ddlStateBill.DataBind();

                        ListItem ddlStateBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                        ddlStateBill.Items.Insert(0, ddlStateBillDefaultItem);

                        tdStateBillDdl.Visible = true;
                        tdStateBillTxt.Visible = false;
                    }
                    else
                    {
                        txtStateBill.Text = "";

                        tdStateBillDdl.Visible = false;
                        tdStateBillTxt.Visible = true;
                    }
                }
                else
                {
                    txtStateBill.Text = "";

                    tdStateBillDdl.Visible = false;
                    tdStateBillTxt.Visible = true;
                }
            }
        }
        else
        {
            ddlStateBill.Items.Clear();

            ListItem ddlStateBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
            ddlStateBill.Items.Insert(0, ddlStateBillDefaultItem);

            tdStateBillDdl.Visible = true;
            tdStateBillTxt.Visible = false;
        }
    }

    protected void ddlCountryShip_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlCountryShip.SelectedValue))
        {
            clsConfig config = new clsConfig();
            if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
            {
                ddlStateShip.Items.Clear();
                ddlCityShip.Items.Clear();
                txtCityShip.Text = "";

                ListItem ddlCityShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlCityShip.Items.Insert(0, ddlCityShipDefaultItem);

                clsShipping ship = new clsShipping();
                DataSet dsState = new DataSet();
                dsState = ship.getShippingStateList(1);
                DataView dvState;

                string countryCode = ddlCountryShip.SelectedValue;
                //mis.extractCountryById(countryCode, 1);

                //if (mis.showState != 1)
                //{
                //    trStateShipping.Visible = false;
                //}

                if (dsState.Tables.Count > 0)
                {
                    dvState = new DataView(dsState.Tables[0]);

                    dvState.RowFilter = "SHIP_COUNTRY ='" + ddlCountryShip.SelectedValue + "'";

                    if (dvState.Count > 0)
                    {
                        ddlStateShip.DataSource = dvState;
                        ddlStateShip.DataTextField = "SHIP_STATE";
                        ddlStateShip.DataValueField = "SHIP_STATE";
                        ddlStateShip.DataBind();

                        ListItem ddlStateShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                        ddlStateShip.Items.Insert(0, ddlStateShipDefaultItem);

                        ship.extractShippingCountry(ddlCountryShip.SelectedValue, 1);
                        if (ship.shipChkCountry == 1)
                        {
                            tdCountryShipInput.Visible = true;
                            tdCountryShipTxt.Visible = true;
                            tdCountryShipInput.ColSpan = 0;
                            txtCountryShip.Text = "";
                        }
                        else
                        {
                            tdCountryShipInput.Visible = true;
                            tdCountryShipTxt.Visible = false;
                            tdCountryShipInput.ColSpan = 2;
                        }

                        tdStateShipDdl.Visible = true;
                        tdStateShipTxt.Visible = false;
                    }
                    else
                    {
                        txtStateShip.Text = "";

                        tdStateShipDdl.Visible = false;
                        tdStateShipTxt.Visible = true;
                    }
                }
                else
                {
                    txtStateShip.Text = "";

                    tdStateShipDdl.Visible = false;
                    tdStateShipTxt.Visible = true;
                }
            }
            else
            {
                clsCountry country = new clsCountry();

                DataSet dsState = new DataSet();
                dsState = country.getStateList(1);
                DataView dvState;

                if (dsState.Tables.Count > 0)
                {
                    dvState = new DataView(dsState.Tables[0]);

                    dvState.RowFilter = "STATE_DEFAULT = 0 AND STATE_COUNTRY = '" + ddlCountryShip.SelectedValue + "' AND STATE_CODE IS NOT NULL AND STATE_CODE <> ''";

                    if (dvState.Count > 0)
                    {
                        ddlStateShip.DataSource = dvState;
                        ddlStateShip.DataTextField = "STATE_CODE";
                        ddlStateShip.DataValueField = "STATE_CODE";
                        ddlStateShip.DataBind();

                        ListItem ddlStateShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                        ddlStateShip.Items.Insert(0, ddlStateShipDefaultItem);

                        tdStateShipDdl.Visible = true;
                        tdStateShipTxt.Visible = false;
                    }
                    else
                    {
                        txtStateShip.Text = "";

                        tdStateShipDdl.Visible = false;
                        tdStateShipTxt.Visible = true;
                    }
                }
                else
                {
                    txtStateShip.Text = "";

                    tdStateShipDdl.Visible = false;
                    tdStateShipTxt.Visible = true;
                }
            }
        }
        else
        {
            ddlStateShip.Items.Clear();

            ListItem ddlStateShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
            ddlStateShip.Items.Insert(0, ddlStateShipDefaultItem);

            tdStateShipDdl.Visible = true;
            tdStateShipTxt.Visible = false;
        }
    }

    protected void ddlStateBill_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlCountryBill.SelectedValue))
        {
            clsConfig config = new clsConfig();
            if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
            {
                ddlCityBill.Items.Clear();

                clsShipping ship = new clsShipping();

                DataSet dsCity = new DataSet();
                dsCity = ship.getShippingCityByState(ddlCountryBill.SelectedValue, ddlStateBill.SelectedValue, 1);
                DataView dvCity;

                if (dsCity.Tables.Count > 0)
                {
                    dvCity = new DataView(dsCity.Tables[0]);

                    dvCity.RowFilter = "SHIP_STATE ='" + ddlStateBill.SelectedValue + "' AND SHIP_CITY IS NOT NULL AND SHIP_CITY <> ''";

                    ship.extractShippingStatebyCountry(ddlCountryBill.SelectedValue, ddlStateBill.SelectedValue, 1);
                    if (dvCity.Count > 0)
                    {
                        ddlCityBill.DataSource = dvCity;
                        ddlCityBill.DataTextField = "SHIP_CITY";
                        ddlCityBill.DataValueField = "SHIP_CITY";
                        ddlCityBill.DataBind();

                        ListItem ddlCityBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                        ddlCityBill.Items.Insert(0, ddlCityBillDefaultItem);

                        if (ship.shipChkState == 1)
                        {
                            tdStateBillTxt.Visible = true;
                            tdStateBillDdl.Visible = true;
                            tdStateBillDdl.ColSpan = 0;
                            txtStateBill.Text = "";

                        }
                        else
                        {
                             tdStateBillDdl.Visible = true;
                            tdStateBillTxt.Visible = false;
                            tdStateBillDdl.ColSpan = 2;
                        }

                        tdCityBillInput.Visible = false;
                        tdCityBillDdl.Visible = true;
                        tdCityBillDdl.ColSpan = 2;
                    }
                    else
                    {
                        if (ship.shipChkState == 1)
                        {
                            tdStateBillTxt.Visible = true;
                            tdStateBillDdl.Visible = true;
                            tdStateBillDdl.ColSpan = 0;
                            txtStateBill.Text = "";
                        }
                        else
                        {
                            tdStateBillDdl.Visible = true;
                            tdStateBillTxt.Visible = false;
                            tdStateBillDdl.ColSpan = 2;
                        }

                        txtCityBill.Text = "";
                        tdCityBillInput.Visible = true;
                        tdCityBillDdl.Visible = false;
                        txtStateBill.Text = "";
                    }
                }
            }
        }
    }

    protected void ddlStateShip_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlCountryBill.SelectedValue))
        {
            clsConfig config = new clsConfig();
            if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
            {
                ddlCityShip.Items.Clear();

                clsShipping ship = new clsShipping();

                DataSet dsCity = new DataSet();
                dsCity = ship.getShippingCityByState(ddlCountryShip.SelectedValue, ddlStateShip.SelectedValue, 1);
                DataView dvCity;

                if (dsCity.Tables.Count > 0)
                {
                    dvCity = new DataView(dsCity.Tables[0]);

                    dvCity.RowFilter = "SHIP_STATE ='" + ddlStateShip.SelectedValue + "' AND SHIP_CITY IS NOT NULL AND SHIP_CITY <> ''";

                    ship.extractShippingStatebyCountry(ddlCountryShip.SelectedValue, ddlStateShip.SelectedValue, 1);
                    if (dvCity.Count > 0)
                    {
                        ddlCityShip.DataSource = dvCity;
                        ddlCityShip.DataTextField = "SHIP_CITY";
                        ddlCityShip.DataValueField = "SHIP_CITY";
                        ddlCityShip.DataBind();

                        ListItem ddlCityShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                        ddlCityShip.Items.Insert(0, ddlCityShipDefaultItem);

                        if (ship.shipChkState == 1)
                        {
                            tdStateShipTxt.Visible = true;
                            tdStateShipDdl.Visible = true;
                            tdStateShipDdl.ColSpan = 0;
                            txtStateShip.Text = "";

                        }
                        else
                        {
                            tdStateShipDdl.Visible = true;
                            tdStateShipTxt.Visible = false;
                            tdStateShipDdl.ColSpan = 2;
                        }

                        tdCityShipInput.Visible = false;
                        tdCityShipDdl.Visible = true;
                        tdCityShipDdl.ColSpan = 2;
                    }
                    else
                    {
                        if (ship.shipChkState == 1)
                        {
                            tdStateShipTxt.Visible = true;
                            tdStateShipDdl.Visible = true;
                            tdStateShipDdl.ColSpan = 0;
                            txtStateShip.Text = "";

                        }
                        else
                        {
                            tdStateShipDdl.Visible = true;
                            tdStateShipTxt.Visible = false;
                            tdStateShipDdl.ColSpan = 2;
                        }

                        txtCityShip.Text = "";
                        tdCityShipInput.Visible = true;
                        tdCityShipDdl.Visible = false;
                        txtStateShip.Text = "";
                    }
                }
            }
        }
     }

    protected void ddlCityBill_SelectedIndexChanged(object sender, EventArgs e)
    {
        clsShipping shipping = new clsShipping();
        shipping.extractShippingCitybyState(ddlCountryBill.SelectedValue, ddlStateBill.SelectedValue, ddlCityBill.SelectedValue, 1);
        if (shipping.shipChkCity == 1)
        {
            tdCityBillInput.Visible = true;
            tdCityBillDdl.Visible = true;
            tdCityBillDdl.ColSpan = 0;
            txtCityBill.Text = "";
        }
        else
        {
            tdCityBillInput.Visible = false;
            tdCityBillDdl.Visible = true;
            tdCityBillDdl.ColSpan = 2;
        }
    }

    protected void ddlCityShip_SelectedIndexChanged(object sender, EventArgs e)
    {
        clsShipping shipping = new clsShipping();
        shipping.extractShippingCitybyState(ddlCountryShip.SelectedValue, ddlStateShip.SelectedValue, ddlCityShip.SelectedValue, 1);
        if (shipping.shipChkCity == 1)
        {
            tdCityShipInput.Visible = true;
            tdCityShipDdl.Visible = true;
            tdCityShipDdl.ColSpan = 0;
            txtCityShip.Text = "";
        }
        else
        {
            tdCityShipInput.Visible = false;
            tdCityShipDdl.Visible = true;
            tdCityShipDdl.ColSpan = 2;
        }
    }

    protected void lnkbtnRevert_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.Url.ToString());
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strCompNameBill = "";
            string strNameBill = txtNameBill.Text.Trim();
            string strAddBill = txtAddBill.Text.Trim();
            string strPoscodeBill = txtPoscodeBill.Text.Trim();
            //string strCityBill = txtCityBill.Text.Trim();
            string strContactNoBill = txtContactNoBill.Text.Trim();
            //string strCountryBill = ddlCountryBill.SelectedValue;
            string strCityBill = "";
            string strCountryBill = "";
            string strStateBill = "";
            string strCityBillOther = "";
            string strStateBillOther = "";
            string strCountryBillOther = "";
            string strEmailBill = "";

            //if (tdStateBillDdl.Visible) { strStateBill = ddlStateBill.SelectedValue; }
            //else if (tdStateBillTxt.Visible) { strStateBill = txtStateBill.Text.Trim(); }

            if (tdCountryBillInput.Visible && tdCountryBillTxt.Visible) { strCountryBill = ddlCountryBill.SelectedValue; strCountryBillOther = txtCountryBill.Text.Trim(); }
            else if (tdCountryBillInput.Visible) { strCountryBill = ddlCountryBill.SelectedValue; }
            else if (tdCountryBillTxt.Visible) { strCountryBill = txtCountryBill.Text.Trim(); }
            else { strCountryBill = ""; }

            if (tdStateBillDdl.Visible && tdStateBillTxt.Visible) { strStateBill = ddlStateBill.SelectedValue; strStateBillOther = txtStateBill.Text.Trim(); }
            else if (tdStateBillDdl.Visible) { strStateBill = ddlStateBill.SelectedValue; }
            else if (tdStateBillTxt.Visible) { strStateBill = txtStateBill.Text.Trim(); }
            else { strStateBill = ""; }

            if (tdCityBillDdl.Visible && tdCityBillInput.Visible) { strCityBill = ddlCityBill.SelectedValue; strCityBillOther = txtCityBill.Text.Trim(); }
            else if (tdCityBillDdl.Visible) { strCityBill = ddlCityBill.SelectedValue; }
            else if (tdCityBillInput.Visible) { strCityBill = txtCityBill.Text.Trim(); }
            else { strCityBill = ""; }

            Boolean boolEdited = false;
            Boolean boolSuccess = true;
            clsOrder ord = new clsOrder();
            clsConfig config = new clsConfig();
            int intRecordAffected = 0;

            string strCompNameShip = "";
            string strNameShip = "";
            string strAddShip = "";
            string strPoscodeShip = "";
            string strCityShip = "";
            string strContactNoShip = "";
            string strRemark = "";
            strRemark = txtRemark.Text.Trim();
            string strCountryShip = "";
            string strStateShip = "";
            DateTime dtCollectionDateShip = DateTime.MaxValue;
            string strEmailShip = "";

            strNameShip = txtNameShip.Text.Trim();
            strAddShip = txtAddShip.Text.Trim();
            strPoscodeShip = txtPoscodeShip.Text.Trim();
            strCityShip = txtCityShip.Text.Trim();
            strContactNoShip = txtContactNoShip.Text.Trim();
            strCountryShip = ddlCountryShip.SelectedValue;
            strStateShip = "";
            
            string strCityShipOther = "";
            string strStateShipOther = "";
            string strCountryShipOther = "";

            //if (tdStateShipDdl.Visible) { strStateShip = ddlStateShip.SelectedValue; }
            //else if (tdStateShipTxt.Visible) { strStateShip = txtStateShip.Text.Trim(); }

            if (tdCountryShipInput.Visible && tdCountryShipTxt.Visible) { strCountryShip = ddlCountryShip.SelectedValue; strCountryShipOther = txtCountryShip.Text.Trim(); }
            else if (tdCountryShipInput.Visible) { strCountryShip = ddlCountryShip.SelectedValue; }
            else if (tdCountryShipTxt.Visible) { strCountryShip = txtCountryShip.Text.Trim(); }
            else { strCountryShip = ""; }

            if (tdStateShipDdl.Visible && tdStateShipTxt.Visible) { strStateShip = ddlStateShip.SelectedValue; strStateShipOther = txtStateShip.Text.Trim(); }
            else if (tdStateShipDdl.Visible) { strStateShip = ddlStateShip.SelectedValue; }
            else if (tdStateShipTxt.Visible) { strStateShip = txtStateShip.Text.Trim(); }
            else { strStateShip = ""; }

            if (tdCityShipDdl.Visible && tdCityShipInput.Visible) { strCityShip = ddlCityShip.SelectedValue; strCityShipOther = txtCityShip.Text.Trim(); }
            else if (tdCityShipDdl.Visible) { strCityShip = ddlCityShip.SelectedValue; }
            else if (tdCityShipInput.Visible) { strCityShip = txtCityShip.Text.Trim(); }
            else { strCityShip = ""; }

    
            if (!string.IsNullOrEmpty(txtCollectionDate.Text.Trim()))
            {
                string strCollectionDateShip = txtCollectionDate.Text.Trim();
                dtCollectionDateShip = Convert.ToDateTime(strCollectionDateShip);
            }

            //    if (!string.IsNullOrEmpty(txtCollectionDate.Text.Trim()) && !string.IsNullOrEmpty(hdnCollectionDate.Value.Trim()))
            //    {
            //        strCollectionDateShip = clsMis.formatDateDDMMtoMMDD(hdnCollectionDate.Value.Trim());
            //        dtCollectionDateShip = Convert.ToDateTime(strCollectionDateShip);
            //    }


            if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
            {
                strCompNameBill = txtCompNameBill.Text.Trim();
                strCompNameShip = txtCompNameShip.Text.Trim();
                strEmailBill = txtEmailBill.Text.Trim();
                strEmailShip = txtEmailShip.Text.Trim();

                if (!ord.isExactSameOrderSet3(transId, strCompNameBill, strNameBill, strAddBill, strPoscodeBill, strCityBill, strCityBillOther, strStateBill, strStateBillOther, strCountryBill, strCountryBillOther, strContactNoBill, strEmailBill, strCompNameShip, strNameShip, strAddShip, strPoscodeShip, strCityShip, strCityShipOther, strStateShip, strStateShipOther, strCountryShip, strCountryShipOther, strContactNoShip, dtCollectionDateShip, strEmailShip))
                {
                    intRecordAffected = ord.updateOrderDelivery2(transId, strCompNameBill, strNameBill, strAddBill, strPoscodeBill, strCityBill, strCityBillOther, strStateBill, strStateBillOther, strCountryBill, strCountryBillOther, strContactNoBill, strEmailBill, strCompNameShip, strNameShip, strAddShip, strPoscodeShip, strCityShip, strCityShipOther, strStateShip, strStateShipOther, strCountryShip, strCountryShipOther, strContactNoShip, dtCollectionDateShip, strEmailShip, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1)
                    {
                        boolEdited = true;
                    }
                    else { boolSuccess = false; }
                }
            }
            else
            {
                if (!ord.isExactSameOrderSet(transId, strNameBill, strAddBill, strPoscodeBill, strCityBill, strStateBill, strCountryBill, strContactNoBill, strNameShip, strAddShip, strPoscodeShip, strCityShip, strStateShip, strCountryShip, strContactNoShip, dtCollectionDateShip))
                {
                    intRecordAffected = ord.updateOrderDelivery(transId, strNameBill, strAddBill, strPoscodeBill, strCityBill, strStateBill, strCountryBill, strContactNoBill, strNameShip, strAddShip, strPoscodeShip, strCityShip, strStateShip, strCountryShip, strContactNoShip, dtCollectionDateShip, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1)
                    {
                        boolEdited = true;
                    }
                    else { boolSuccess = false; }
                }
            }

            if (boolEdited)
            {
                Session["EDITEDORDID"] = ord.ordId;

                ord.extractOrderById4(transId);

                decimal decShippingNew = Convert.ToDecimal("0.00");

                int intShippingType = !string.IsNullOrEmpty(config.shippingType) ? Convert.ToInt16(config.shippingType) : clsAdmin.CONSTSHIPPINGTYPE1;
                
                if (intShippingType == clsAdmin.CONSTSHIPPINGTYPE1)
                {
                    decShippingNew = clsMis.getShippingFee(ord.shippingCountry, ord.shippingState, ord.ordTotalWeight, ord.ordSubtotal);
                }
                else if (intShippingType == clsAdmin.CONSTSHIPPINGTYPE2)
                {
                    decShippingNew = clsMis.getShippingFee2(ord.shippingCountry, ord.shippingState, ord.ordTotalWeight, ord.ordSubtotal);
                }
                else if (intShippingType == clsAdmin.CONSTSHIPPINGTYPE3)
                {
                    decShippingNew = clsMis.getShippingFee3(ord.shippingCountry, ord.shippingState, ord.ordTotalWeight, ord.ordSubtotal);
                }

                ord.updateOrderShipping(transId, decShippingNew);
            }
            else
            {
                if (!boolSuccess)
                {
                    ord.extractOrderById4(transId);
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text").ToString() + "<br />Failed to edit transaction (" + ord.ordNo + ").Please try again.</div>";
                }
                else
                {
                    Session["EDITEDORDID"] = transId;
                    Session["NOCHANGE"] = 1;
                }
            }

            Response.Redirect(Request.Url.ToString());
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack) { setPageProperties(); }

        registerScript();
    }

    protected void registerScript()
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("FORMSECT"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "initSect3();";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "FORMSECT", strJS, false);
        }
    }

    protected void setPageProperties()
    {
        DataSet ds = new DataSet();
        DataView dv;

        clsConfig config = new clsConfig();
        clsShipping ship = new clsShipping();
        clsCountry country = new clsCountry();
        ds = new DataSet();

        if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
        {
            ds = ship.getShippingCountryList(1);

            dv = new DataView(ds.Tables[0]);
            dv.RowFilter = "SHIP_DEFAULT = 0";
            dv.Sort = "V_SHIPCOUNTRYNAME ASC";

            ddlCountryShip.DataSource = dv;
            ddlCountryShip.DataTextField = "V_SHIPCOUNTRYNAME";
            ddlCountryShip.DataValueField = "SHIP_COUNTRY";
            ddlCountryShip.DataBind();

            ddlCountryBill.DataSource = dv;
            ddlCountryBill.DataTextField = "V_SHIPCOUNTRYNAME";
            ddlCountryBill.DataValueField = "SHIP_COUNTRY";
            ddlCountryBill.DataBind();
        }
        else
        {
            ds = country.getCountryList(1);

            dv = new DataView(ds.Tables[0]);
            dv.RowFilter = "COUNTRY_DEFAULT = 0";
            dv.Sort = "COUNTRY_NAME ASC";

            ddlCountryShip.DataSource = dv;
            ddlCountryShip.DataTextField = "COUNTRY_NAME";
            ddlCountryShip.DataValueField = "COUNTRY_CODE";
            ddlCountryShip.DataBind();

            ddlCountryBill.DataSource = dv;
            ddlCountryBill.DataTextField = "COUNTRY_NAME";
            ddlCountryBill.DataValueField = "COUNTRY_CODE";
            ddlCountryBill.DataBind();
        }

        ListItem ddlCountryShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlCountryShip.Items.Insert(0, ddlCountryShipDefaultItem);

        ListItem ddlCountryBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlCountryBill.Items.Insert(0, ddlCountryBillDefaultItem);

        ListItem ddlStateShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlStateShip.Items.Insert(0, ddlStateShipDefaultItem);

        ListItem ddlStateBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlStateBill.Items.Insert(0, ddlStateBillDefaultItem);

        if (transId > 0) { fillForm(); }

        if (Session[clsAdmin.CONSTPROJECTDELIVERYMSG] != null && clsAdmin.CONSTPROJECTDELIVERYMSG != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTDELIVERYMSG]) == 0)
            {
                pnlDeliveryMessageDetails.Visible = false;
            }
        }

        //Customization for YSHamper Project - start
        if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
        {
            if (transId > 0) { fillForm2(); }
        }
        //Customization for YSHamper Project - end
    }

    protected void fillForm()
    {
        clsOrder ord = new clsOrder();
        if (ord.extractOrderById4(transId))
        {
            clsConfig config = new clsConfig();

            if (ord.ordStatus >= clsAdmin.CONSTORDERSTATUSPAY || cancel > 0)
            {
                pnlButton.Visible = false;
                lnkbtnSame.Visible = false;

                tdNameBill.Attributes["class"] = "tdLabelNor2 nobr";
                tdAddBill.Attributes["class"] = "tdLabelNor2";
                tdPoscodeBill.Attributes["class"] = "tdLabelNor2 nobr";
                tdCityBill.Attributes["class"] = "tdLabelNor2";
                tdCountryBill.Attributes["class"] = "tdLabelNor2";
                tdStateBill.Attributes["class"] = "tdLabelNor2";
                tdContactBill.Attributes["class"] = "tdLabelNor2 nobr";

                tdNameShip.Attributes["class"] = "tdLabelNor2 nobr";
                tdAddShip.Attributes["class"] = "tdLabelNor2";
                tdPoscodeShip.Attributes["class"] = "tdLabelNor2 nobr";
                tdCityShip.Attributes["class"] = "tdLabelNor2";
                tdCountryShip.Attributes["class"] = "tdLabelNor2";
                tdStateShip.Attributes["class"] = "tdLabelNor2";
                tdContactShip.Attributes["class"] = "tdLabelNor2 nobr";
                tdCollectDate.Attributes["class"] = "tdLabelNor2 nobr";
                tdRemarkShip.Attributes["class"] = "tdLabelNor2";

                tdNameBillReadOnly.Visible = true;
                tdAddBillReadOnly.Visible = true;
                tdPoscodeBillReadOnly.Visible = true;
                tdCityBillReadOnly.Visible = true;
                tdCountryBillReadOnly.Visible = true;
                tdStateBillReadOnly.Visible = true;
                tdContactBillReadOnly.Visible = true;

                tdNameShipReadOnly.Visible = true;
                tdAddShipReadOnly.Visible = true;
                tdPoscodeShipReadOnly.Visible = true;
                tdCityShipReadOnly.Visible = true;
                tdCountryShipReadOnly.Visible = true;
                tdStateShipReadOnly.Visible = true;
                tdContactShipReadOnly.Visible = true;
                tdCollectDateInputReadOnly.Visible = true;
                tdRemarkShipReadOnly.Visible = true;

                litNameBill.Text = ord.name;
                litAddBill.Text = ord.add;
                litPoscodeBill.Text = ord.poscode;
                litCityBill.Text = ord.city;
                litCountryBill.Text = ord.countryName;
                litStateBill.Text = ord.state;
                litContactNoBill.Text = ord.contactNo;

                litNameShip.Text = ord.shippingName;
                litAddShip.Text = ord.shippingAdd;
                litPoscodeShip.Text = ord.shippingPoscode;
                litCityShip.Text = ord.shippingCity;
                litCountryShip.Text = ord.shippingCountryName;
                litStateShip.Text = ord.shippingState;
                litContactNoShip.Text = ord.shippingContactNo;
                litCollectionDate.Text = ord.shippingCollectionDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString() ? ord.shippingCollectionDate.ToString("dd MMM yyyy") : " - ";

                litRemark.Text = !string.IsNullOrEmpty(ord.shippingRemarks) ? ord.shippingRemarks : "-";

                if (ord.deliveryDate != DateTime.MaxValue)
                {
                    litDeliveryDate.Text = ord.deliveryDate.ToString("dd MMM yyyy");
                }
                else { litDeliveryDate.Text = ""; }
                litDeliveryTime.Text = !string.IsNullOrEmpty(ord.deliveryTime) ? ord.deliveryTime : "-";
                litDeliveryRecipient.Text = ord.deliveryRecipient;
                litDeliverySender.Text = ord.deliverySender;
                litDeliveryMsg.Text = ord.deliveryMessage;
            }
            else
            {
                tdNameBillInput.Visible = true;
                tdAddBillInput.Visible = true;
                tdPoscodeBillInput.Visible = true;
                tdCityBillInput.Visible = true;
                tdCountryBillInput.Visible = true;
                tdContactBillInput.Visible = true;

                tdNameShipInput.Visible = true;
                tdAddShipInput.Visible = true;
                tdPoscodeShipInput.Visible = true;
                tdCityShipInput.Visible = true;
                tdCountryShipInput.Visible = true;
                tdContactShipInput.Visible = true;
                tdCollectDateInput.Visible = true;
                tdRemarkShipInput.Visible = true;

                txtNameBill.Text = ord.name;
                txtAddBill.Text = ord.add;
                txtPoscodeBill.Text = ord.poscode;
                txtCityBill.Text = ord.city;
                txtContactNoBill.Text = ord.contactNo;

                if (ord.deliveryDate != DateTime.MaxValue)
                {
                    litDeliveryDate.Text = ord.deliveryDate.ToString("dd MMM yyyy");
                }
                else { litDeliveryDate.Text = ""; }
                litDeliveryTime.Text = !string.IsNullOrEmpty(ord.deliveryTime) ? ord.deliveryTime : "-";
                litDeliveryRecipient.Text = ord.deliveryRecipient;
                litDeliverySender.Text = ord.deliverySender;
                litDeliveryMsg.Text = ord.deliveryMessage;

                ddlCountryBill.SelectedValue = ord.country;

                if (!string.IsNullOrEmpty(ord.country))
                {
                    clsCountry country = new clsCountry();

                    DataSet dsState = new DataSet();
                    dsState = country.getStateList(1);
                    DataView dvState;

                    if (dsState.Tables.Count > 0)
                    {
                        dvState = new DataView(dsState.Tables[0]);

                        dvState.RowFilter = "STATE_DEFAULT = 0 AND STATE_COUNTRY = '" + ord.country + "' AND STATE_CODE IS NOT NULL AND STATE_CODE <> ''";

                        if (dvState.Count > 0)
                        {
                            ddlStateBill.DataSource = dvState;
                            ddlStateBill.DataTextField = "STATE_CODE";
                            ddlStateBill.DataValueField = "STATE_CODE";
                            ddlStateBill.DataBind();

                            ListItem ddlStateBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                            ddlStateBill.Items.Insert(0, ddlStateBillDefaultItem);

                            ddlStateBill.SelectedValue = ord.state;

                            tdStateBillDdl.Visible = true;
                            tdStateBillTxt.Visible = false;
                        }
                        else
                        {
                            txtStateBill.Text = ord.state;

                            tdStateBillDdl.Visible = false;
                            tdStateBillTxt.Visible = true;
                        }
                    }
                    else
                    {
                        txtStateBill.Text = ord.state;

                        tdStateBillDdl.Visible = false;
                        tdStateBillTxt.Visible = true;
                    }
                }
                else
                {
                    ddlStateBill.Items.Clear();

                    ListItem ddlStateBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                    ddlStateBill.Items.Insert(0, ddlStateBillDefaultItem);

                    tdStateBillDdl.Visible = true;
                    tdStateBillTxt.Visible = false;
                }

                txtNameShip.Text = ord.shippingName;
                txtAddShip.Text = ord.shippingAdd;
                txtPoscodeShip.Text = ord.shippingPoscode;
                txtCityShip.Text = ord.shippingCity;
                txtContactNoShip.Text = ord.shippingContactNo;
                txtRemark.Text = ord.shippingRemarks;

                if (ord.shippingCollectionDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
                {
                    if (config.isGoodyFlagExist(clsConfig.CONSTNAMEGOODYFLAG, clsConfig.CONSTGROUPWEBSITE))
                    {
                        txtCollectionDate.Text = ord.shippingCollectionDate.ToString("dd MMM yyyy hh:mm:ss tt");
                    }
                    else
                    {
                        txtCollectionDate.Text = ord.shippingCollectionDate.ToString("dd MMM yyyy");
                    }  
                }

                ddlCountryShip.SelectedValue = ord.shippingCountry;

                if (!string.IsNullOrEmpty(ord.shippingCountry))
                {
                    clsCountry country = new clsCountry();

                    DataSet dsState = new DataSet();
                    dsState = country.getStateList(1);
                    DataView dvState;

                    if (dsState.Tables.Count > 0)
                    {
                        dvState = new DataView(dsState.Tables[0]);

                        dvState.RowFilter = "STATE_DEFAULT = 0 AND STATE_COUNTRY = '" + ord.shippingCountry + "' AND STATE_CODE IS NOT NULL AND STATE_CODE <> ''";

                        if (dvState.Count > 0)
                        {
                            ddlStateShip.DataSource = dvState;
                            ddlStateShip.DataTextField = "STATE_CODE";
                            ddlStateShip.DataValueField = "STATE_CODE";
                            ddlStateShip.DataBind();

                            ListItem ddlStateShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                            ddlStateShip.Items.Insert(0, ddlStateShipDefaultItem);

                            ddlStateShip.SelectedValue = ord.shippingState;

                            tdStateShipDdl.Visible = true;
                            tdStateShipTxt.Visible = false;
                        }
                        else
                        {
                            txtStateShip.Text = ord.shippingState;

                            tdStateShipDdl.Visible = false;
                            tdStateShipTxt.Visible = true;
                        }
                    }
                    else
                    {
                        txtStateShip.Text = ord.shippingState;

                        tdStateShipDdl.Visible = false;
                        tdStateShipTxt.Visible = true;
                    }
                }
                else
                {
                    ddlStateShip.Items.Clear();

                    ListItem ddlStateBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                    ddlStateShip.Items.Insert(0, ddlStateBillDefaultItem);

                    tdStateShipDdl.Visible = true;
                    tdStateShipTxt.Visible = false;
                }
            }

            trNeedShipping.Visible = (ord.ordDlvService == 1);
        }
    }

    protected void fillForm2()
    {
        clsOrder ord = new clsOrder();
        if (ord.extractOrderById5(transId))
        {
            clsConfig config = new clsConfig();

            if (ord.ordStatus >= clsAdmin.CONSTORDERSTATUSPAY || cancel > 0)
            {
                pnlButton.Visible = false;
                lnkbtnSame.Visible = false;

                tdCompNameBill.Attributes["class"] = "tdLabelNor2 nobr";
                tdNameBill.Attributes["class"] = "tdLabelNor2 nobr";
                tdAddBill.Attributes["class"] = "tdLabelNor2";
                tdPoscodeBill.Attributes["class"] = "tdLabelNor2 nobr";
                tdCityBill.Attributes["class"] = "tdLabelNor2";
                tdCountryBill.Attributes["class"] = "tdLabelNor2";
                tdStateBill.Attributes["class"] = "tdLabelNor2";
                tdContactBill.Attributes["class"] = "tdLabelNor2 nobr";
                tdEmailBill.Attributes["class"] = "tdLabelNor2";

                tdCompNameShip.Attributes["class"] = "tdLabelNor2 nobr";
                tdNameShip.Attributes["class"] = "tdLabelNor2 nobr";
                tdAddShip.Attributes["class"] = "tdLabelNor2";
                tdPoscodeShip.Attributes["class"] = "tdLabelNor2 nobr";
                tdCityShip.Attributes["class"] = "tdLabelNor2";
                tdCountryShip.Attributes["class"] = "tdLabelNor2";
                tdStateShip.Attributes["class"] = "tdLabelNor2";
                tdContactShip.Attributes["class"] = "tdLabelNor2 nobr";
                tdCollectDate.Attributes["class"] = "tdLabelNor2 nobr";
                tdRemarkShip.Attributes["class"] = "tdLabelNor2";
                tdEmailShip.Attributes["class"] = "tdLabelNor2";

                tdCompNameBillReadOnly.Visible = true;
                tdNameBillReadOnly.Visible = true;
                tdAddBillReadOnly.Visible = true;
                tdPoscodeBillReadOnly.Visible = true;
                tdCityBillReadOnly.Visible = true;
                tdCountryBillReadOnly.Visible = true;
                tdStateBillReadOnly.Visible = true;
                tdContactBillReadOnly.Visible = true;
                tdEmailBillReadOnly.Visible = true;

                tdCompNameShipReadOnly.Visible = true;
                tdNameShipReadOnly.Visible = true;
                tdAddShipReadOnly.Visible = true;
                tdPoscodeShipReadOnly.Visible = true;
                tdCityShipReadOnly.Visible = true;
                tdCountryShipReadOnly.Visible = true;
                tdStateShipReadOnly.Visible = true;
                tdContactShipReadOnly.Visible = true;
                tdCollectDateInputReadOnly.Visible = true;
                tdRemarkShipReadOnly.Visible = true;
                tdEmailShipReadOnly.Visible = true;

                litCompNameBill.Text = ord.company;
                litNameBill.Text = ord.name;
                litAddBill.Text = ord.add;
                litPoscodeBill.Text = ord.poscode;

                if (!string.IsNullOrEmpty(ord.cityOther))
                {
                    litCityBill.Text = ord.city + " " + ord.cityOther;
                }
                else
                {
                    litCityBill.Text = ord.city;
                }

                if (!string.IsNullOrEmpty(ord.countryOther))
                {
                    litCountryBill.Text = ord.countryName + " " + ord.countryOther;
                }
                else
                {
                    litCountryBill.Text = ord.countryName;
                }

                if (!string.IsNullOrEmpty(ord.stateOther))
                {
                    litStateBill.Text = ord.state + " " + ord.stateOther;
                }
                else
                {
                    litStateBill.Text = ord.state;
                }

                litContactNoBill.Text = ord.contactNo;
                litEmailBill.Text = ord.email;

                litCompNameShip.Text = ord.shippingCustCompany;
                litNameShip.Text = ord.shippingName;
                litAddShip.Text = ord.shippingAdd;
                litPoscodeShip.Text = ord.shippingPoscode;
                
                litCityShip.Text = ord.shippingCity;

                if (!string.IsNullOrEmpty(ord.shippingCityOther))
                {
                    litCityShip.Text = ord.shippingCity + " " + ord.shippingCityOther;
                }
                else
                {
                    litCityShip.Text = ord.shippingCity;
                }

                if (!string.IsNullOrEmpty(ord.shippingCountryOther))
                {
                    litCountryShip.Text = ord.shippingCountryName + " " + ord.shippingCountryOther;
                }
                else
                {
                    litCountryShip.Text = ord.shippingCountryName;
                }

                if (!string.IsNullOrEmpty(ord.shippingStateOther))
                {
                    litStateShip.Text = ord.shippingState + " " + ord.shippingStateOther;
                }
                else
                {
                    litStateShip.Text = ord.shippingState;
                }

                litContactNoShip.Text = ord.shippingContactNo;
                litCollectionDate.Text = ord.shippingCollectionDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString() ? ord.shippingCollectionDate.ToString("dd MMM yyyy") : " - ";
                litEmailShip.Text = ord.shippingEmail;

                litRemark.Text = !string.IsNullOrEmpty(ord.shippingRemarks) ? ord.shippingRemarks : "-";
            }
            else
            {
                tdCompNameBillInput.Visible = true;
                tdNameBillInput.Visible = true;
                tdAddBillInput.Visible = true;
                tdPoscodeBillInput.Visible = true;                
                tdContactBillInput.Visible = true;
                tdEmailBillInput.Visible = true;

                tdCompNameShipInput.Visible = true;
                tdNameShipInput.Visible = true;
                tdAddShipInput.Visible = true;
                tdPoscodeShipInput.Visible = true;
                tdContactShipInput.Visible = true;
                tdCollectDateInput.Visible = true;
                tdRemarkShipInput.Visible = true;
                tdEmailShipInput.Visible = true;

                txtCompNameBill.Text = ord.company;
                txtNameBill.Text = ord.name;
                txtAddBill.Text = ord.add;
                txtPoscodeBill.Text = ord.poscode;
                txtContactNoBill.Text = ord.contactNo;
                txtEmailBill.Text = ord.email; 

                clsShipping shipping = new clsShipping();
                try
                {
                    if (!string.IsNullOrEmpty(ord.country))
                    {
                        ddlCountryBill.SelectedValue = ord.country;
                    }

                    if (!string.IsNullOrEmpty(ord.countryOther))
                    {
                        txtCountryBill.Text = ord.countryOther;
                    }
                }
                catch (Exception ex) { clsLog.logErroMsg(ex.Message.ToString()); }

                shipping.extractShippingCountry(ddlCountryBill.SelectedValue, 1);
                if (shipping.shipChkCountry == 1)
                {
                    tdCountryBillInput.Visible = true;
                    tdCountryBillTxt.Visible = true;
                    tdCountryBillInput.ColSpan = 0;
                }
                else
                {
                    tdCountryBillInput.Visible = true;
                    tdCountryBillTxt.Visible = false;
                    tdCountryBillInput.ColSpan = 2;
                }
               
                if (!string.IsNullOrEmpty(ord.country))
                {
                    clsCountry country = new clsCountry();

                    DataSet dsState = new DataSet();
                    dsState = shipping.getShippingStateList(1);
                    DataView dvState;

                    if (dsState.Tables.Count > 0)
                    {
                        dvState = new DataView(dsState.Tables[0]);

                        dvState.RowFilter = "SHIP_COUNTRY ='" + ddlCountryBill.SelectedValue + "'";

                        if (dvState.Count > 0)
                        {
                            ddlStateBill.DataSource = dvState;
                            ddlStateBill.DataTextField = "SHIP_STATE";
                            ddlStateBill.DataValueField = "SHIP_STATE";
                            ddlStateBill.DataBind();

                            ListItem ddlStateBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                            ddlStateBill.Items.Insert(0, ddlStateBillDefaultItem);

                            try
                            {
                                ddlStateBill.SelectedValue = ord.state;

                                if (!string.IsNullOrEmpty(ord.stateOther))
                                {
                                    txtStateBill.Text = ord.stateOther;
                                }
                            }
                            catch (Exception ex) { clsLog.logErroMsg(ex.Message.ToString()); }

                            shipping.extractShippingStatebyCountry(ddlCountryBill.SelectedValue, ddlStateBill.SelectedValue, 1);
                            if (shipping.shipChkState == 1)
                            {
                                tdStateBillTxt.Visible = true;
                                tdStateBillDdl.Visible = true;
                                tdStateBillDdl.ColSpan = 0;
                                txtCityBill.Text = "";
                            }
                            else
                            {
                                tdStateBillDdl.Visible = true;
                                tdStateBillTxt.Visible = false;
                                tdCityBillInput.Visible = true;
                                tdCityBillDdl.Visible = false;
                                txtCityBill.Text = "";
                                tdStateBillDdl.ColSpan = 2;
                                ddlStateBill.SelectedValue = ord.state;
                            }
                        }
                        else
                        {
                            txtStateBill.Text = ord.stateOther;

                            tdStateBillDdl.Visible = false;
                            tdStateBillTxt.Visible = true;
                            tdStateBillTxt.ColSpan = 2;
                        }
                    }
                    else
                    {
                        txtStateBill.Text = ord.stateOther;

                        tdStateBillDdl.Visible = false;
                        tdStateBillTxt.Visible = true;
                        tdStateBillTxt.ColSpan = 2;
                    }
                }
                else
                {
                    ddlStateBill.Items.Clear();

                    ListItem ddlStateBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                    ddlStateBill.Items.Insert(0, ddlStateBillDefaultItem);

                    tdStateBillDdl.Visible = true;
                    tdStateBillTxt.Visible = false;
                    tdStateBillTxt.ColSpan = 2;
                }

                if (!string.IsNullOrEmpty(ord.city))
                {
                    ddlCityBill.Items.Clear();

                    DataSet dsCity = new DataSet();
                    dsCity = shipping.getShippingCityByState(ddlCountryBill.SelectedValue, ddlStateBill.SelectedValue, 1);
                    DataRow[] foundRow;
                    DataView dvCity = new DataView(dsCity.Tables[0]);
                    dvCity.RowFilter = "SHIP_STATE = '" + ddlStateBill.SelectedValue + "' AND SHIP_CITY IS NOT NULL AND SHIP_CITY <> ''";

                    if (dvCity.Count > 0)
                    {
                        ddlCityBill.DataSource = dvCity;
                        ddlCityBill.DataTextField = "SHIP_CITY";
                        ddlCityBill.DataValueField = "SHIP_CITY";
                        ddlCityBill.DataBind();

                        try
                        {
                            ddlCityBill.SelectedValue = ord.city;

                            if (!string.IsNullOrEmpty(ord.cityOther))
                            {
                                txtCityBill.Text = ord.cityOther;
                            }
                        }
                        catch (Exception ex) { clsLog.logErroMsg(ex.Message.ToString()); }

                        shipping.extractShippingCitybyState(ddlCountryBill.SelectedValue, ddlStateBill.SelectedValue, ddlCityBill.SelectedValue, 1);
                        if (shipping.shipChkCity == 1)
                        {
                            tdCityBillInput.Visible = true;
                            tdCityBillDdl.Visible = true;
                            tdCityBillDdl.ColSpan = 0;
                        }
                        else
                        {
                            tdCityBillInput.Visible = false;
                            tdCityBillDdl.Visible = true;
                            ddlCityBill.SelectedValue = ord.city;
                            tdCityBillDdl.ColSpan = 2;
                        }

                        ListItem ddlCityBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                        ddlCityBill.Items.Insert(0, ddlCityBillDefaultItem);

                    }
                    else
                    {
                        txtCityBill.Text = ord.cityOther;
                        tdCityBillInput.Visible = true;
                        tdCityBillDdl.Visible = false;
                        tdCityBillInput.ColSpan = 2;
                    }
                }
                else
                {
                    ddlCityBill.Items.Clear();

                    ListItem ddlCityBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                    ddlCityBill.Items.Insert(0, ddlCityBillDefaultItem);

                    tdCityBillInput.Visible = true;
                    tdCityBillDdl.Visible = false;
                    txtCityBill.Text = ord.cityOther;
                    tdCityBillInput.ColSpan = 2;
                }

                txtCompNameShip.Text = ord.shippingCustCompany;
                txtNameShip.Text = ord.shippingName;
                txtAddShip.Text = ord.shippingAdd;
                txtPoscodeShip.Text = ord.shippingPoscode;
                txtContactNoShip.Text = ord.shippingContactNo;
                txtRemark.Text = ord.shippingRemarks;
                txtEmailShip.Text = ord.shippingEmail;

                if (ord.shippingCollectionDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
                {
                    txtCollectionDate.Text = ord.shippingCollectionDate.ToString("dd MMM yyyy");
                }

                try
                {
                    if (!string.IsNullOrEmpty(ord.shippingCountry))
                    {
                        ddlCountryShip.SelectedValue = ord.shippingCountry;
                    }

                    if (!string.IsNullOrEmpty(ord.shippingCountryOther))
                    {
                        txtCountryShip.Text = ord.shippingCountryOther;
                    }
                }
                catch (Exception ex) { clsLog.logErroMsg(ex.Message.ToString()); }

                shipping.extractShippingCountry(ddlCountryShip.SelectedValue, 1);
                if (shipping.shipChkCountry == 1)
                {
                    tdCountryShipInput.Visible = true;
                    tdCountryShipTxt.Visible = true;
                    tdCountryShipInput.ColSpan = 0;
                }
                else
                {
                    tdCountryShipInput.Visible = true;
                    tdCountryShipTxt.Visible = false;
                }

                if (!string.IsNullOrEmpty(ord.shippingCountry))
                {
                    clsCountry country = new clsCountry();

                    DataSet dsState = new DataSet();
                    dsState = shipping.getShippingStateList(1);
                    DataView dvState;

                    if (dsState.Tables.Count > 0)
                    {
                        dvState = new DataView(dsState.Tables[0]);

                        dvState.RowFilter = "SHIP_COUNTRY ='" + ddlCountryShip.SelectedValue + "'";

                        if (dvState.Count > 0)
                        {
                            ddlStateShip.DataSource = dvState;
                            ddlStateShip.DataTextField = "SHIP_STATE";
                            ddlStateShip.DataValueField = "SHIP_STATE";
                            ddlStateShip.DataBind();

                            ListItem ddlStateShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                            ddlStateShip.Items.Insert(0, ddlStateShipDefaultItem);

                            try
                            {
                                ddlStateShip.SelectedValue = ord.shippingState;

                                if (!string.IsNullOrEmpty(ord.shippingStateOther))
                                {
                                    txtStateShip.Text = ord.shippingStateOther;
                                }
                            }
                            catch (Exception ex) { clsLog.logErroMsg(ex.Message.ToString()); }

                            shipping.extractShippingStatebyCountry(ddlCountryShip.SelectedValue, ddlStateShip.SelectedValue, 1);
                            if (shipping.shipChkState == 1)
                            {
                                tdStateShipTxt.Visible = true;
                                tdStateShipDdl.Visible = true;
                                tdStateShipDdl.ColSpan = 0;
                                txtCityShip.Text = "";
                            }
                            else
                            {
                                tdStateShipDdl.Visible = true;
                                tdStateShipTxt.Visible = false;
                                tdCityShipInput.Visible = true;
                                tdCityShipDdl.Visible = false;
                                txtCityShip.Text = "";
                                ddlStateShip.SelectedValue = ord.shippingState;
                                tdStateShipDdl.ColSpan = 2;
                            }
                        }
                        else
                        {
                            txtStateShip.Text = ord.shippingStateOther;

                            tdStateShipDdl.Visible = false;
                            tdStateShipTxt.Visible = true;
                            tdStateShipTxt.ColSpan = 2;
                        }
                    }
                    else
                    {
                        txtStateShip.Text = ord.shippingStateOther;

                        tdStateShipDdl.Visible = false;
                        tdStateShipTxt.Visible = true;
                        tdStateShipTxt.ColSpan = 2;
                    }
                }
                else
                {
                    ddlStateShip.Items.Clear();

                    ListItem ddlStateBillDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                    ddlStateShip.Items.Insert(0, ddlStateBillDefaultItem);

                    tdStateShipDdl.Visible = true;
                    tdStateShipTxt.Visible = false;
                    tdStateShipTxt.ColSpan = 2;
                }

                if (!string.IsNullOrEmpty(ord.shippingCity))
                {
                    ddlCityShip.Items.Clear();

                    DataSet dsCity = new DataSet();
                    dsCity = shipping.getShippingCityByState(ddlCountryShip.SelectedValue, ddlStateShip.SelectedValue, 1);
                    DataRow[] foundRow;
                    DataView dvCity = new DataView(dsCity.Tables[0]);
                    dvCity.RowFilter = "SHIP_STATE = '" + ddlStateShip.SelectedValue + "' AND SHIP_CITY IS NOT NULL AND SHIP_CITY <> ''";

                    if (dvCity.Count > 0)
                    {
                        ddlCityShip.DataSource = dvCity;
                        ddlCityShip.DataTextField = "SHIP_CITY";
                        ddlCityShip.DataValueField = "SHIP_CITY";
                        ddlCityShip.DataBind();

                        try
                        {
                            ddlCityShip.SelectedValue = ord.shippingCity;

                            if (!string.IsNullOrEmpty(ord.shippingCityOther))
                            {
                                txtCityShip.Text = ord.shippingCityOther;
                            }
                        }
                        catch (Exception ex) { clsLog.logErroMsg(ex.Message.ToString()); }

                        shipping.extractShippingCitybyState(ddlCountryShip.SelectedValue, ddlStateShip.SelectedValue, ddlCityShip.SelectedValue, 1);
                        if (shipping.shipChkCity == 1)
                        {
                            tdCityShipInput.Visible = true;
                            tdCityShipDdl.Visible = true;
                            tdCityShipDdl.ColSpan = 0;
                        }
                        else
                        {
                            tdCityShipInput.Visible = false;
                            tdCityShipDdl.Visible = true;
                            ddlCityShip.SelectedValue = ord.shippingCity;
                            tdCityShipDdl.ColSpan = 2;
                        }

                        ListItem ddlCityShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                        ddlCityShip.Items.Insert(0, ddlCityShipDefaultItem);

                    }
                    else
                    {
                        txtCityShip.Text = ord.shippingCityOther;
                        tdCityShipInput.Visible = true;
                        tdCityShipDdl.Visible = false;
                        tdCityShipInput.ColSpan = 2;
                    }
                }
                else
                {
                    ddlCityShip.Items.Clear();

                    ListItem ddlCityShipDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                    ddlCityShip.Items.Insert(0, ddlCityShipDefaultItem);

                    tdCityShipInput.Visible = true;
                    tdCityShipDdl.Visible = false;
                    txtCityShip.Text = ord.shippingCityOther;
                    tdCityShipInput.ColSpan = 2;
                }
            }

            trNeedShipping.Visible = (ord.ordDlvService == 1);
            trCompNameBill.Visible = true;
            trCompNameShip.Visible = true;
            trEmailBill.Visible = true;
            trEmailShip.Visible = true;

            pnlOrderDeliveryMessage.Visible = true;

            litMsgTo.Text = ord.ordTo;
            litMsgContent.Text = ord.ordMessage;
            litMsgFrom.Text = ord.ordFrom;
            litSpecialInstruction.Text = ord.ordMsgInstruction;
        }
    }
    #endregion
}
