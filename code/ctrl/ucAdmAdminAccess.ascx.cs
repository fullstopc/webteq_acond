﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucAdmAdminAccess : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _admId = 0;
    protected int _mode = 1;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int admId
    {
        get
        {
            if (ViewState["ADMID"] == null)
            {
                return _admId;
            }
            else
            {
                return Convert.ToInt16(ViewState["ADMID"]);
            }
        }
        set { ViewState["ADMID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cvLoginId.ErrorMessage = "<br />Login ID cannot be less than " + clsSetting.CONSTADMUSERNAMEMINLENGTH + " characters.";
            cvPassword.ErrorMessage = "<br />Password cannot be less than " + clsSetting.CONSTADMPASSWORDMINLENGTH + " characters.";
        }
    }

    protected void validateLoginId_server(object source, ServerValidateEventArgs args)
    {
        string strUserName = txtLoginId.Text.Trim();

        clsAdmin adm = new clsAdmin();
        if (string.Compare(strUserName, hdnLoginId.Value.Trim()) != 0)
        {
            if (adm.isAdminExist(strUserName))
            {
                args.IsValid = false;
                cvLoginId.ErrorMessage = "<br />This login id is in use.";
            }
            else
            {
                args.IsValid = true;
            }
        }
    }

    protected void cvLoginId_PreRender(object source, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("admloginid"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtLoginId.ClientID + "', document.all['" + cvLoginId.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "admloginid", strJS, false);
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strUserName = txtLoginId.Text.Trim();
            string strName = txtName.Text.Trim();
            string strPwd = txtNewPwd.Text.Trim();
            int intActive = 0;

            if (chkActive.Checked) { intActive = 1; }

            clsMD5 md5 = new clsMD5();
            string encryptPwd = md5.encrypt(strPwd);

            clsAdmin adm = new clsAdmin();
            int intRecordAffected = 0;
            Boolean boolSuccess = true;
            Boolean boolAdded = false;
            Boolean boolEdited = false;

            if (mode == 1)
            {
                intRecordAffected = adm.addAdmin(strUserName, encryptPwd, strName, intActive, Convert.ToInt16(Session["ADMID"]));

                if (intRecordAffected == 1)
                {
                    boolAdded = true;
                    admId = adm.admId;
                }
                else
                {
                    boolSuccess = false;

                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new admin. Please try again.</div>";
                }
            }
            else if (mode == 2)
            {
                if (!string.IsNullOrEmpty(hdnPassword.Value) && !string.IsNullOrEmpty(strPwd))
                {
                    if (string.Compare(encryptPwd, hdnPassword.Value.Trim()) != 0)
                    {
                        intRecordAffected = adm.updatePasswordById(admId, encryptPwd);

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;
                            admId = adm.admId;
                        }
                        else
                        {
                            boolSuccess = false;

                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to update password. Please try again.</div>";
                        }
                    }
                }

                if (boolSuccess)
                {
                    if (!adm.isExactSameData(admId, strName, intActive))
                    {
                        intRecordAffected = adm.updateAdminById(admId, strName, intActive, Convert.ToInt16(Session["ADMID"]));

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;
                            admId = adm.admId;
                        }
                        else
                        {
                            boolSuccess = false;

                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit admin. Please try again.</div>";
                        }
                    }
                }
            }

            if (boolSuccess)
            {
                clsAdmPage admPage = new clsAdmPage();
                DataSet ds = new DataSet();
                DataSet dsAL = new DataSet();

                ds = admPage.getAdminPageList(false,0);

                foreach (DataRow drRow in ds.Tables[0].Rows)
                {
                    CheckBox chkboxAccess = (CheckBox)pnlAdminAccessLevel.FindControl(drRow["ADMPAGE_VALUE"].ToString());
                    int intPageId = Convert.ToInt16(drRow["ADMPAGE_VALUE"]);

                    if (chkboxAccess.Checked)
                    {
                        if (!adm.isAdminAccessExist(admId, intPageId))
                        {
                            intRecordAffected = adm.addAdminAccess(admId, intPageId);

                            if (intRecordAffected == 1)
                            {
                                if (mode == 2) { boolEdited = true; }

                                admId = adm.admId;
                            }
                            else
                            {
                                boolSuccess = false;

                                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to update admin access level. Please try again.</div>";
                            }
                        }
                    }
                    else
                    {
                        if (adm.isAdminAccessExist(admId, intPageId))
                        {
                            intRecordAffected = adm.deleteAdminAccessById(admId, intPageId);

                            if (intRecordAffected == 1)
                            {
                                if (mode == 2) { boolEdited = true; }

                                admId = adm.admId;
                            }
                            else
                            {
                                boolSuccess = false;

                                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to update admin access level. Please try again.</div>";
                            }
                        }
                    }
                }
            }

            if (boolSuccess)
            {
                if (boolAdded)
                {
                    Session["NEWADMID"] = admId;
                }
                else if (boolEdited)
                {
                    Session["EDITEDADMID"] = admId;
                }
                else if (!boolEdited)
                {
                    Session["EDITEDADMID"] = admId;
                    Session["NOCHANGE"] = 1;
                }
            }

            Response.Redirect(Request.Url.ToString());
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        clsAdmin adm = new clsAdmin();
        int intRecordAffected = 0;
        string strDeletedAdminName = "";

        adm.extractAdminById(admId, 0);

        intRecordAffected = adm.deleteAdminById(admId);

        if (intRecordAffected == 1)
        {
            adm.deleteAdminAccessByAdmId(admId);

            strDeletedAdminName = adm.name;

            Session["DELETEDADMNAME"] = strDeletedAdminName;

            Response.Redirect(currentPageName);
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }

        populatePage();
    }

    protected void setPageProperties()
    {
        lnkbtnCancel.OnClientClick = "javascript:document.location.href='admProfile.aspx'; return false;";
        lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteAdmin.Text").ToString() + "');";

        if (mode == 2)
        {
            lnkbtnDelete.Visible = true;
            fillForm();
        }
    }

    protected void fillForm()
    {
        clsAdmin adm = new clsAdmin();

        if (adm.extractAdminById(admId, 0))
        {
            txtLoginId.Text = adm.userName;
            txtLoginId.Enabled = false;
            hdnLoginId.Value = adm.userName;

            txtName.Text = adm.name;
            hdnPassword.Value = adm.password;

            if (adm.active != 0) { chkActive.Checked = true; }
            else { chkActive.Checked = false; }
        }
    }

    protected void populatePage()
    {
        HtmlTable tbl = new HtmlTable();
        tbl.CellPadding = 0;
        tbl.CellSpacing = 0;
        tbl.Border = 0;
        tbl.Attributes.Add("class", "innerTbl");
        pnlAdminAccessLevel.Controls.Add(tbl);

        HtmlTableRow row;
        HtmlTableCell cell1;
        HtmlTableCell cell2;

        clsAdmPage admPage = new clsAdmPage();
        clsAdmin adm = new clsAdmin();

        DataSet ds = new DataSet();
        DataSet dsAL = new DataSet();

        ds = admPage.getAdminPageList(false,0);
        dsAL = adm.getAdminAccessList(admId);

        Literal litPage;
        CheckBox chkboxAccess;

        DataRow[] foundRow;

        foreach (DataRow drRow in ds.Tables[0].Rows)
        {
            litPage = new Literal();
            chkboxAccess = new CheckBox();

            litPage.Text = drRow["ADMPAGE_NAME"].ToString();

            int intPageId = Convert.ToInt16(drRow["ADMPAGE_VALUE"].ToString());
            chkboxAccess.ID = intPageId.ToString();
            chkboxAccess.Checked = true;

            if (mode == 2)
            {
                foundRow = dsAL.Tables[0].Select("PAGE_ID = " + intPageId);

                if (foundRow.Length > 0) { chkboxAccess.Checked = true; }
                else { chkboxAccess.Checked = false; }
            }

            cell1 = new HtmlTableCell();
            cell2 = new HtmlTableCell();

            cell1.Controls.Add(litPage);
            cell1.Attributes["class"] = "tdAdmAccess";

            cell2.Controls.Add(chkboxAccess);

            row = new HtmlTableRow();
            row.Controls.Add(cell1);
            row.Controls.Add(cell2);

            tbl.Controls.Add(row);
        }
    }
    #endregion
}
