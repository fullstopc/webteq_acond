﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmGroupDetails.ascx.cs" Inherits="ctrl_ucAdmGroupDetails" %>

<script type="text/javascript">
    function validateCatCode_client(source, args) {
        var txtCatCode = document.getElementById("<%= txtCatCode.ClientID %>");
        var catCodeRE = /<%= clsAdmin.CONSTADMCATCODERE %>/;

        if (txtCatCode.value.match(catCodeRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "Please enter only alphanumeric.";
            source.innerHTML = "Please enter only alphanumeric.";
        }
    }

    function validatePageTitleFriendlyUrl_client(source, args) {
        var txtCatPageTitleFriendlyUrl = document.getElementById("<%= txtCatPageTitleFriendlyUrl.ClientID %>");
        var pageTitleFriendlyUrlRE = /<%= clsAdmin.CONSTADMPAGETITLEFRIENDLYURL %>/;
        if (txtCatPageTitleFriendlyUrl.value.match(pageTitleFriendlyUrlRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "Please enter only valid format.";
            source.innerHTML = "Please enter only valid format.";
        }
    }
</script>

<asp:Panel ID="pnlForm" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table cellpadding="0" cellspacing="0" class="formTbl tblData">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr"><asp:Literal ID="litGroupDetails" runat="server"></asp:Literal></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr id="trParent" runat="server" visible="false">
                    <td class="tdLabelBig"><asp:Label ID="lblParent" runat="server"></asp:Label>:</td>
                    <td>
                        <asp:DropDownList ID="ddlParent" runat="server" CssClass="ddl"></asp:DropDownList>
                        <asp:HiddenField ID="hdnParent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblGroupCode" runat="server"></asp:Label><span class="attention_unique">*</span>:</td>
                    <td class="tdMax"><asp:TextBox ID="txtCatCode" runat="server" class="text uniq" MaxLength="20"></asp:TextBox>
                        <span class="spanFieldDesc">
                        <asp:RequiredFieldValidator ID="rfvCatCode" runat="server" ErrorMessage="Please enter Category Code." ControlToValidate="txtCatCode" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cvCatCode" runat="server" ControlToValidate="txtCatCode" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateCatCode_client" OnServerValidate="validateCatCode_server" OnPreRender="cvCatCode_PreRender"></asp:CustomValidator></span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblGroupName" runat="server"></asp:Label><span class="attention_unique">*</span>:</td>
                    <td><asp:TextBox ID="txtCatName" runat="server" class="text_big uniq" MaxLength="250"></asp:TextBox>
                        <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvCatName" runat="server" ErrorMessage="<br />Please enter Category Name." ControlToValidate="txtCatName" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator>
                        <%--<asp:CustomValidator ID="cvCatName" runat="server" ControlToValidate="txtCatName" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateCatName_server" OnPreRender="cvCatName_PreRender"></asp:CustomValidator>--%></span>
                    </td>
                </tr>
                <tr id="trNameJp" runat="server" visible="false">
                    <td class="tdLabel"><asp:Label ID="lblGroupJpName" runat="server"></asp:Label><span class="attention_unique">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtCatJpName" runat="server" class="text_big uniq" MaxLength="250"></asp:TextBox>
                        <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvCatJpName" runat="server" ErrorMessage="<br/>Please enter Category Name.(Japanese)" ControlToValidate="txtCatJpName" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="cvCatJpName" runat="server" ControlToValidate="txtCatJpName" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateCatNameJp_server" OnPreRender="cvCatNameJp_PreRender"></asp:CustomValidator></span>
                    </td>
                </tr>
                <tr id="trNameMs" runat="server" visible="false">
                    <td class="tdLabel"><asp:Label ID="lblGroupMsName" runat="server"></asp:Label><span class="attention_unique">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtCatMsName" runat="server" class="text_big uniq" MaxLength="250"></asp:TextBox>
                        <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvCatMsName" runat="server" ErrorMessage="<br/>Please enter Category Name.(Malay)" ControlToValidate="txtCatMsName" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="cvCatMsName" runat="server" ControlToValidate="txtCatMsName" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateCatNameMs_server" OnPreRender="cvCatNameMs_PreRender"></asp:CustomValidator></span>
                    </td>
                </tr>
                <tr id="trNameZh" runat="server" visible="false">
                    <td class="tdLabel"><asp:Label ID="lblGroupZhName" runat="server"></asp:Label><span class="attention_unique">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtCatZhName" runat="server" class="text_big uniq" MaxLength="250"></asp:TextBox>
                        <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvCatZhName" runat="server" ErrorMessage="<br/>Please enter Category Name.(Chinese)" ControlToValidate="txtCatZhName" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="cvCatZhName" runat="server" ControlToValidate="txtCatZhName" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateCatNameZh_server" OnPreRender="cvCatNameZh_PreRender"></asp:CustomValidator></span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblGroupDName" runat="server"></asp:Label><span class="attention_compulsory">*</span>:</td>
                    <td><asp:TextBox ID="txtCatDName" runat="server" class="text_big compulsory" MaxLength="250"></asp:TextBox><span class="spanFieldDesc"><a href="javascript:void(0);" onclick="copyValue('<% =txtCatName.ClientID %>','<% =txtCatDName.ClientID %>');" title="same as Name">same as Name</a></span>
                        <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvDName" runat="server" ErrorMessage="<br />Please enter Category Display Name." ControlToValidate="txtCatDName" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator></span>
                    </td>
                </tr>
                <tr id="trDNameJp" runat="server" visible="false">
                    <td class="tdLabel nobr"><asp:Label ID="lblGroupDNameJp" runat="server"></asp:Label><span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtCatDNameJp" runat="server" class="text_big compulsory" MaxLength="250"></asp:TextBox><span class="spanFieldDesc"><a href="javascript:void(0);" onclick="copyValue('<% =txtCatJpName.ClientID %>','<% =txtCatDNameJp.ClientID %>');" title="same as Name (Japanese)">same as Name (Japanese)</a></span>
                        <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvCatDNameJp" runat="server" ErrorMessage="<br/>Please enter Category Display Name (Japanese)" ControlToValidate="txtCatDNameJp" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator></span>
                    </td>
                </tr>
                <tr id="trDNameMs" runat="server" visible="false">
                    <td class="tdLabel nobr"><asp:Label ID="lblGroupDNameMs" runat="server"></asp:Label><span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtCatDNameMs" runat="server" class="text_big compulsory" MaxLength="250"></asp:TextBox><span class="spanFieldDesc"><a href="javascript:void(0);" onclick="copyValue('<% =txtCatMsName.ClientID %>','<% =txtCatDNameMs.ClientID %>');" title="same as Name (Malay)">same as Name (Malay)</a></span>
                        <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvCatDNameMs" runat="server" ErrorMessage="<br/>Please enter Category Display Name (Malay)" ControlToValidate="txtCatDNameMs" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator></span>
                    </td>
                </tr>
                <tr id="trDNameZh" runat="server" visible="false">
                    <td class="tdLabel nobr"><asp:Label ID="lblGroupDNameZh" runat="server"></asp:Label><span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtCatDNameZh" runat="server" class="text_big compulsory" MaxLength="250"></asp:TextBox><span class="spanFieldDesc"><a href="javascript:void(0);" onclick="copyValue('<% =txtCatZhName.ClientID %>','<% =txtCatDNameZh.ClientID %>');" title="same as Name (Chinese)">same as Name (Chinese)</a></span>
                        <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvCatDNameZh" runat="server" ErrorMessage="<br/>Please enter Category Display Name (Chinese)" ControlToValidate="txtCatDNameZh" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator></span>
                    </td>
                </tr>
                <tr  id="trFriendlyURL" runat="server">
                    <td class="tdLabel nobr"><asp:Label ID="lblGroupPageTitleFriendlyUrl" runat="server" Text="Label"></asp:Label>:<span class="attention_unique">*</span></td>
                    <td>
                        <asp:TextBox ID="txtCatPageTitleFriendlyUrl" runat="server" CssClass="text_big uniq" MaxLength="250"></asp:TextBox>
                        <span class="spanFieldDesc"><asp:CustomValidator ID="cvCatPageTitleFriendlyUrl" runat="server" ControlToValidate="txtCatPageTitleFriendlyUrl" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validatePageTitleFriendlyUrl_client" OnServerValidate="validateCatPageTitleFriendlyUrl_server" OnPreRender="cvCatPageTitleFriendlyUrl_PreRender"></asp:CustomValidator></span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblGroupImage" runat="server"></asp:Label>:</td>
                    <td>
                        <asp:Panel ID="pnlFileUpload" runat="server" CssClass="divFileUpload">
                            <asp:Panel ID="pnlFileUploadAction" runat="server" CssClass="divFileUploadAction">
                                <asp:FileUpload ID="fileCatImage" runat="server" />
                            </asp:Panel>
                            <span class="spanTooltip">
                                <span class="icon"></span>
                                <span class="msg">
                                    <asp:Literal ID="litUploadRule" runat="server"></asp:Literal>
                                </span>
                            </span>
                            
                        </asp:Panel>
                        <asp:HiddenField ID="hdnCatImage" runat="server" />
                        <asp:HiddenField ID="hdnCatImageRef" runat="server" />
                        <asp:CustomValidator ID="cvCatImage" runat="server" ControlToValidate="fileCatImage" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateCatImage_server" OnPreRender="cvCatImage_PreRender"></asp:CustomValidator>
                        <asp:Panel ID="pnlCatImage" runat="server" Visible="false">
                            <br /><asp:LinkButton ID="lnkbtnDeleteImage" runat="server" Text="Delete" Tooltip="Delete" CausesValidation="false" OnClick="lnkbtnDeleteImage_Click"></asp:LinkButton>
                            <br /><asp:Image ID="imgCatImage" runat="server" />
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <table cellpadding="0" cellspacing="0" class="formTbl tblData">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr"><asp:Literal ID="litGroupSetting" runat="server"></asp:Literal></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel">Show on Top:</td>
                    <td><asp:CheckBox ID="chkboxShowTop" runat="server" /></td>
                </tr>
                <tr>
                    <td class="tdLabel">Active:</td>
                    <td><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr id="trDesc" runat="server" visible="false">
                    <td colspan="2">
                        <table cellpadding="0" cellspacing="0" class="formTbl">
                            <tr>
                                <td colspan="2" class="tdSectionHdr">Brand Description</td>
                            </tr>
                            <tr>
                                <td class="tdSpacer">&nbsp;</td>
                                <td class="tdSpacer">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="tdLabel">Product Brand Snapshot:</td>
                                <td><asp:TextBox ID="txtCatSnapshot" runat="server" class="text_big" MaxLength="100" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="tdSpacer">&nbsp;</td>
                                <td class="tdSpacer">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="tdLabel">Product Brand Detail:</td>
                                <td>
                                    <asp:TextBox ID="txtCatDesc" runat="server" class="text_big" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdSpacer">&nbsp;</td>
                                <td class="tdSpacer">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnSaveBack" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false" OnClick="lnkbtnCancel_Click"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" class="btn btnCancel" Visible="false" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>
