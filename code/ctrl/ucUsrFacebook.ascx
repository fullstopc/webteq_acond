﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrFacebook.ascx.cs" Inherits="ctrl_ucUsrFacebook" %>
<%@ Register Src="~/ctrl/ucUsrFacebookThumbnail.ascx" TagName="UsrFacebookThumbnail" TagPrefix="uc" %>

<asp:Panel ID="pnlFacebookScript" runat="server" Visible="false" CssClass="divFacebookScript">
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        } (document, 'script', 'facebook-jssdk'));
    </script>
</asp:Panel>

<asp:Panel ID="pnlFacebook" runat="server" CssClass="">
    <uc:UsrFacebookThumbnail ID="ucUsrFacebookThumbnail" runat="server" />
    <div id="fb-root"></div>
    <asp:Panel ID="pnlFacebookLike" runat="server" CssClass="divFacebookLike">
        <asp:Literal ID="litLike" runat="server"></asp:Literal>
    </asp:Panel>
</asp:Panel>
