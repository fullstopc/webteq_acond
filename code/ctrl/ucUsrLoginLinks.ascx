﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrLoginLinks.ascx.cs" Inherits="ctrl_ucUsrLoginLinks" %>
<ul class="nav nav-two loginlinks" runat="server" visible="false" id="ulLoginLinks">
    <li runat="server">
        <asp:HyperLink ID="hypUpdateProfile" runat="server" CssClass="profile">
            <i class="fa fa-1x fa-user" aria-hidden="true"></i>
            <span><asp:Literal runat="server" ID="litUpdateProfile"></asp:Literal></span>
        </asp:HyperLink>
    </li>
    <li>
        <asp:HyperLink ID="hypChangePwd" runat="server" CssClass="password">
            <i class="fa fa-1x fa-unlock-alt" aria-hidden="true"></i>
            <span><asp:Literal runat="server" ID="litChangePwd"></asp:Literal></span>
        </asp:HyperLink>
    </li>
    <li>
        <asp:HyperLink ID="hypCheckResult" runat="server" CssClass="result">
            <i class="fa fa-1x fa-plus-circle" aria-hidden="true"></i>
            <span><asp:Literal runat="server" ID="litCheckResult"></asp:Literal></span>
        </asp:HyperLink>
    </li>
    <li>
        <asp:HyperLink ID="hypStudents" runat="server" CssClass="result">
            <i class="fa fa-1x fa-users" aria-hidden="true"></i>
            <span><asp:Literal runat="server" ID="litStudents"></asp:Literal></span>
        </asp:HyperLink>
    </li>
    <li>
        <asp:HyperLink ID="hypLogout" runat="server" CssClass="logout">
            <i class="fa fa-1x fa-sign-out" aria-hidden="true"></i>
            <span><asp:Literal runat="server" ID="litLogout"></asp:Literal></span>
        </asp:HyperLink>
    </li>
</ul>