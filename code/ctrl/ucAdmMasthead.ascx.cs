﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctlr_ucAdmMasthead : System.Web.UI.UserControl
{
    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "admmasthead.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            imgLogo.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "adm/logo_admin.gif";
        }
    }
    #endregion
}
