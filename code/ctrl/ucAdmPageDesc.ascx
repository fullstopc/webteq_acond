﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmPageDesc.ascx.cs" Inherits="ctrl_ucAdmPageDesc" %>

<asp:Panel ID="pnlPageDesc" runat="server" CssClass="pagedesc">
    <span class="pagedesc-item"><asp:Literal runat="server" ID="litDesc"></asp:Literal></span>
    <div class="pagedesc-item pagedesc-right">
        <span class="attention_uniq_msg"><asp:Literal ID="litUnique" runat="server"></asp:Literal></span>
        <span class="attention_compulsory_msg"><asp:Literal ID="litCompulsory" runat="server"></asp:Literal></span> 
    </div>
</asp:Panel>