﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;
using System.Linq;

public partial class ctrl_ucAdmOrderSlipSetup : System.Web.UI.UserControl
{
    #region "Properties"
    protected clsConfig config = new clsConfig();
    protected DataView dvConfig = null;
    protected DataSet dsConfig = null;
    protected bool boolGst = false;

    protected string strConfGroupName = "";
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setPageProperties();
            fill();
        }
    }
    protected void validateImage_server(object source, ServerValidateEventArgs args)
    {
        string strControlValidator = ((CustomValidator)source).ControlToValidate;
        FileUpload fileUpload = new FileUpload();
        CustomValidator customValidator = new CustomValidator();
        if (strControlValidator == fileCompanyLogo.ID)
        {
            fileUpload = fileCompanyLogo;
            customValidator = cvCompanyLogo;
        }

        if (fileUpload.HasFile)
        {
            int intImgMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            //if (fileUpload.PostedFile.ContentLength > clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH)
            //{
            //    customValidator.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH, "kb") + ".";
            //    args.IsValid = false;
            //}

            if (fileUpload.PostedFile.ContentLength > intImgMaxSize)
            {
                customValidator.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + ".";
                args.IsValid = false;
            }
            else
            {
                Match matchImage = Regex.Match(fileUpload.FileName, clsAdmin.CONSTADMIMGALLOWEDEXT, RegexOptions.IgnoreCase);
                if (matchImage.Success)
                {
                    args.IsValid = true;
                }
                else
                {
                    customValidator.ErrorMessage = "<br />Please enter valid image. (" + clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + ").";
                    args.IsValid = false;
                }
            }
        }
    }
    protected void cvImage_PreRender(object sender, EventArgs e)
    {
        string strControlValidator = ((CustomValidator)sender).ControlToValidate;
        if (strControlValidator == fileCompanyLogo.ID)
        {
            if ((!Page.ClientScript.IsStartupScriptRegistered("CompanyLogo")))
            {
                string strJS = null;
                strJS = "<script type = \"text/javascript\">";
                strJS += "ValidatorHookupControlID('" + fileCompanyLogo.ClientID + "', document.all['" + cvCompanyLogo.ClientID + "']);";
                strJS += "</script>";

                Page.ClientScript.RegisterStartupScript(Page.GetType(), "CompanyLogo", strJS, false);
            }
        }
    }
    protected void lnkbtnDelete_Click(object sender, CommandEventArgs e)
    {
        if (e.CommandArgument.ToString() == "cmaCompanyLogo")
        {
            pnlCompanyLogo.Visible = false;
            hdnCompanyLogo.Value = "";
        }
    }
    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {

            Boolean boolSuccess = true,
                    boolEdited = false;

            int intRecordAffected = 0;

            string confGroup = clsConfig.CONSTGROUPORDERSLIPSETUP;

            List<string[]> confList = new List<string[]>();
            confList.Add(new string[] { clsConfig.CONSTNAMEORDERSLIP_COMPANYLOGO, ""});
            confList.Add(new string[] { clsConfig.CONSTNAMEORDERSLIP_COMPANYNAME , txtCompanyName.Text.Trim() });
            confList.Add(new string[] { clsConfig.CONSTNAMEORDERSLIP_ADDR1 , txtAddr1.Text.Trim() });
            confList.Add(new string[] { clsConfig.CONSTNAMEORDERSLIP_ADDR2 , txtAddr2.Text.Trim() });
            confList.Add(new string[] { clsConfig.CONSTNAMEORDERSLIP_TEL , txtTel.Text.Trim()});
            confList.Add(new string[] { clsConfig.CONSTNAMEORDERSLIP_FAX , txtFax.Text.Trim() });
            confList.Add(new string[] { clsConfig.CONSTNAMEORDERSLIP_EMAIL , txtEmail.Text.Trim() });
            confList.Add(new string[] { clsConfig.CONSTNAMEORDERSLIP_WEBSITE , txtWebsite.Text.Trim() });
            confList.Add(new string[] { clsConfig.CONSTNAMEORDERSLIP_LINE1 , txtLine1.Text.Trim() });
            confList.Add(new string[] { clsConfig.CONSTNAMEORDERSLIP_LINE2 , txtLine2.Text.Trim() });
            confList.Add(new string[] { clsConfig.CONSTNAMEORDERSLIP_LINE3 , txtLine3.Text.Trim() });

            if (tbodyGST.Visible)
            {
                confList.Add(new string[] { clsConfig.CONSTNAMEORDERSLIP_COMPANYLREGNO, txtCompanyRegNo.Text.Trim() });
                confList.Add(new string[] { clsConfig.CONSTNAMEORDERSLIP_GSTREGNO, txtGstRegNo.Text.Trim() });
            }

            // get image link
            if (fileCompanyLogo.HasFile)
            {
                try
                {
                    HttpPostedFile postedFile = fileCompanyLogo.PostedFile;
                    string fileName = Path.GetFileName(postedFile.FileName);
                    string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCTRLPNLADMFOLDER + "/";
                    clsMis.createFolder(uploadPath);

                    string newFilename = clsMis.GetUniqueFilename(fileName, uploadPath);
                    postedFile.SaveAs(uploadPath + newFilename);
                    confList.ElementAt(0)[1] = newFilename;
                }
                catch (Exception ex)
                {
                    clsLog.logErroMsg(ex.Message.ToString());
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />" + ex.Message.ToString() + "</div>";

                    throw ex;
                }
            }
            else { confList.ElementAt(0)[1] = hdnCompanyLogo.Value; }



            for (int i = 0; i < confList.Count; i++)
            {
                string confName = confList.ElementAt(i)[0];
                string confValue = confList.ElementAt(i)[1];


                if (boolSuccess && !config.isItemExist(confName, confGroup))
                {
                    intRecordAffected = config.addItem(confName, confValue, confGroup, 1, int.Parse(Session["ADMID"].ToString()));
                }
                else if (!config.isExactSameSetData(confName, confValue, confGroup))
                {
                    intRecordAffected = config.updateItem(confGroup, confName, confValue, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else { boolSuccess = false; break; }
                }
            }

            if (!boolSuccess)
            {
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to update control panel. Please try again.</div>";
                Response.Redirect(currentPageName);
            }
            else
            {
                if (boolEdited)
                {
                    Session["EDITED_ORDERSLIP"] = 1;
                }
                else
                {
                    Session["EDITED_ORDERSLIP"] = 1;
                    Session["NOCHANGE"] = 1;
                }

                Response.Redirect(currentPageName);
            }
        }
    }
    #endregion

    
    #region "Methods"
    public void fill()
    {
        DataRow[] foundRow = dsConfig.Tables[0].Select("CONF_GROUP = '" + strConfGroupName + "'");
        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_COMPANYLOGO, true) == 0) 
            {
                string strImageUrl = row["CONF_VALUE"].ToString();
                if (!string.IsNullOrEmpty(strImageUrl))
                {
                    pnlCompanyLogo.Visible = true;
                    imgCompanyLogo.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCTRLPNLADMFOLDER + "/" + strImageUrl + "&f=1";
                    hdnCompanyLogo.Value = hdnCompanyLogoRef.Value = strImageUrl;
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_COMPANYNAME, true) == 0) { txtCompanyName.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_ADDR1, true) == 0) { txtAddr1.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_ADDR2, true) == 0) { txtAddr2.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_TEL, true) == 0) { txtTel.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_FAX, true) == 0) { txtFax.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_EMAIL, true) == 0) { txtEmail.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_WEBSITE, true) == 0) { txtWebsite.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_LINE1, true) == 0) { txtLine1.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_LINE2, true) == 0) { txtLine2.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_LINE3, true) == 0) { txtLine3.Text = row["CONF_VALUE"].ToString(); }

            if (tbodyGST.Visible)
            {
                if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_COMPANYLREGNO, true) == 0) { txtCompanyRegNo.Text = row["CONF_VALUE"].ToString(); }
                if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_GSTREGNO, true) == 0) { txtGstRegNo.Text = row["CONF_VALUE"].ToString(); }
            }
            
        }
    }
    protected void setPageProperties()
    {
        if (Session[clsAdmin.CONSTPROJECTTYPE] != null && Session[clsAdmin.CONSTPROJECTTYPE] != "")
        {
            int intImgMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            //litUploadRule.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH, "kb") + " | " + clsMis.formatAllowedFileDimension(clsAdmin.CONSTADMCURIMGALLOWEDWIDTH, clsAdmin.CONSTADMCURIMGALLOWEDHEIGHT, "px");
            litUploadRule.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + " | " + clsMis.formatAllowedFileDimension(clsAdmin.CONSTADMCURIMGALLOWEDWIDTH, clsAdmin.CONSTADMCURIMGALLOWEDHEIGHT, "px");
            
            tbodyGST.Visible = false;
            if (Session[clsAdmin.CONSTPROJECTGST] != null && clsAdmin.CONSTPROJECTGST != "")
            {
                if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTGST]) == 1)
                {
                        tbodyGST.Visible = true;
                }
            }
            bindRptData();

        }
    }
    protected void bindRptData()
    {
        strConfGroupName = clsConfig.CONSTGROUPORDERSLIPSETUP;
        dsConfig = config.getItemsByGroup(strConfGroupName,1);
        dvConfig = new DataView(dsConfig.Tables[0]);
    }
    #endregion
}