﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Drawing.Imaging;

public partial class ctrl_ucAdmProductInventory : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected int _prodId = 0;
    protected string _pageListingURL = "";
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int prodId
    {
        get
        {
            if (ViewState["PRODID"] == null)
            {
                return _prodId;
            }
            else
            {
                return int.Parse(ViewState["PRODID"].ToString());
            }
        }
        set
        {
            ViewState["PRODID"] = value;
        }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }

    protected void validateProdInv_server(object source, ServerValidateEventArgs args)
    {
        bool isValid = false;

        if (!string.IsNullOrEmpty(txtProdInventory.Text))
        {
            int intTryParse;
            if (int.TryParse(txtProdInventory.Text.Trim(), out intTryParse))
            {
                Match matchQty = Regex.Match(txtProdInventory.Text.Trim(), clsAdmin.CONSTNUMERICRE2);

                if (matchQty.Success) { isValid = true; }
                else { isValid = false; }
            }
        }

        if (isValid)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
            cvProdInv.ErrorMessage = "Please enter valid value";
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Boolean bEdited = false;
            Boolean bSuccess = true;

            string prodSupplier = txtSupplierName.Text.Trim();
            string prodInventory = txtProdInventory.Text.Trim();
            int prodAllowNegative = chkboxAllowNegative.Checked ? 1 : 0;

            clsProduct prod = new clsProduct();
            clsInventory inv = new clsInventory();

            int intRecordAffected = 0;
            int intRecordAffected2 = 0;

            if (!prod.isExactSameProdSet5(prodId, prodSupplier, prodAllowNegative))
            {
                intRecordAffected = prod.updateProdInventoryById(prodId, prodSupplier, prodAllowNegative, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1)
                {
                    bEdited = true;
                }
                else
                {
                    bSuccess = false;
                }
            }

            if (!inv.isInventoryExist(prodId))
            {
                intRecordAffected2 = inv.addProdInvQty(prodId, prodInventory);
                if (intRecordAffected2 == 1)
                {
                    bEdited = true;
                }
            }
            else
            {
                intRecordAffected2 = inv.updateProdInvQtyById(prodId, prodInventory);                
                {
                    if (intRecordAffected2 == 1)
                    {
                        bEdited = true;
                    }
                    else
                    {
                        bSuccess = false;
                    }
                }
            }

            if (bEdited)
            {
                Session["EDITPRODID"] = prod.prodId;
            }
            else
            {
                if (!bSuccess)
                {
                    prod.extractProdById(prodId, 0);
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit product (" + prod.prodName + "). Please try again.</div>";
                }
                else
                {
                    Session["EDITPRODID"] = prodId;
                    Session["NOCHANGE"] = 1;
                }
            }

            Response.Redirect(Request.Url.ToString());
        }
    }

    #endregion

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void setPageProperties()
    {
        switch (mode)
        {
            case 1://Add Product

                break;
            case 2://Edit Product

                fillForm();

                break;
            default:
                break;
        }

        lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
    }

    public void fillForm()
    {
        clsProduct prod = new clsProduct();
        clsConfig config = new clsConfig();
        clsInventory inv = new clsInventory();

        if (inv.extractProdQty(prodId))
        {
            txtProdInventory.Text = inv.invQty.ToString();
        }

        if (prod.extractProdById6(prodId, 0))
        {
            txtSupplierName.Text = prod.prodSupplier;

            if (prod.prodInvAllowNegative == 1) { chkboxAllowNegative.Checked = true; }
            else { chkboxAllowNegative.Checked = false; } 
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }
    #endregion
}