﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

public partial class ctrl_ucCmnIcon : System.Web.UI.UserControl
{
    #region "Properties"
    protected string _iconFileName = "";
    #endregion


    #region "Property Methods"
    public string iconFileName
    {
        get { return ViewState["ICONFILENAME"] as string ?? _iconFileName; }
        set { ViewState["ICONFILENAME"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        HtmlControl htmlctrlIcon = null;
        htmlctrlIcon = this.Page.Header.FindControl("Icon") as HtmlControl;

        if (htmlctrlIcon == null)
        {
            HtmlLink iconLink = new HtmlLink();
            //clsConfig config = new clsConfig();
            iconLink.ID = "Icon";
            //iconLink.Href = ConfigurationManager.AppSettings["uplBase"] + "admctrlpnl/" + config.defaultFavIconImg;
            iconLink.Href = ConfigurationManager.AppSettings["imageBase"] + "usr/" + "favicon-logo.ico";
            iconLink.Attributes.Add("rel", "shortcut icon");

            this.Page.Header.Controls.Add(iconLink);
        }
    }
    #endregion
}
