﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucAdmSideMenu : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _sectId = 0;
    protected int _subSectId = 0;
    protected int _subSubSectId = 0;
    protected int _idParent = 0;
    protected int _id = 0;
    protected int _id2 = 0;
    protected int _eId = 0;
    protected int _type = 1;
    protected Boolean _boolShowSibling = true;
    protected DataSet _dsSideMenu = new DataSet();
    #endregion


    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int subSectId
    {
        get { return _subSectId; }
        set { _subSectId = value; }
    }

    public int subSubSectId
    {
        get { return _subSubSectId; }
        set { _subSubSectId = value; }
    }

    public int idParent
    {
        get { return _idParent; }
        set { _idParent = value; }
    }

    public int itemId
    {
        get { return _id; }
        set { _id = value; }
    }

    public int id2
    {
        get { return _id2; }
        set { _id2 = value; }
    }

    public int eId
    {
        get { return _eId; }
        set { _eId = value; }
    }

    public int type
    {
        get { return _type; }
        set { _type = value; }
    }

    public Boolean boolShowSibling
    {
        get { return _boolShowSibling; }
        set { _boolShowSibling = value; }
    }

    protected DataSet dsSideMenu
    {
        get
        {
            if (ViewState["DSSIDEMENU"] == null)
            {
                return _dsSideMenu;
            }
            else
            {
                return (DataSet)ViewState["DSSIDEMENU"];
            }
        }
        set { ViewState["DSSIDEMENU"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "admsidemenu.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            bindMenuItems();
        }
    }

    protected void rptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "ADMPAGE_ID"));
            int intParentId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem,"ADMPAGE_PARENT"));
            string strURL = DataBinder.Eval(e.Item.DataItem, "ADMPAGE_URL").ToString();
            string strTitle = DataBinder.Eval(e.Item.DataItem, "ADMPAGE_NAME").ToString();

            HyperLink hypItem = (HyperLink)e.Item.FindControl("hypItem");
            hypItem.Text = strTitle;

            if (itemId > 0)
            {
                strURL = strURL.Replace(clsAdmin.CONSTIDTAG, itemId.ToString());
                strURL = strURL.Replace(clsAdmin.CONSTID2TAG, id2.ToString());
                strURL = strURL.Replace(clsAdmin.CONSTEIDTAG, eId.ToString());

                hypItem.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + strURL;
            }
            else 
            { 
                hypItem.CssClass = "sidemenuSubLinkDisabled";

                // if inside page manager page
                if (intParentId == clsAdmin.CONSTADMPAGEMANAGERID)
                {
                    if (strTitle == clsAdmin.CONSTADMPAGEMANAGER_ADDNEWPAGE || strTitle == clsAdmin.CONSTADMPAGEMANAGER_REPLICATEPAGE)
                    {
                        Panel pnlSideMenuItem = (Panel)e.Item.FindControl("pnlSideMenuItem");
                        pnlSideMenuItem.Visible = false;
                    }

                }
            }

            if (subSectId == intId)
            {
                hypItem.CssClass = "sidemenuSubLinkSel";

                Panel pnlSideMenuItemInner = (Panel)e.Item.FindControl("pnlSideMenuItemInner");
                pnlSideMenuItemInner.CssClass = "divAdmSideMenuItemInnerSel";
            }

            // Added by LM 20150804
            if (subSubSectId > 0)
            {
                DataView dv = new DataView(dsSideMenu.Tables[0]);
                dv.RowFilter = "ADMPAGE_PARENT = " + intId + " AND ADMPAGE_SHOWINMENU = 1 AND ADMPAGE_SHOWINSIDEMENU = 1 AND (V_TYPE = " + type + " OR V_TYPE IS NULL)";

                if (dv.Count > 0)
                {
                    Repeater rptSubItems = (Repeater)e.Item.FindControl("rptSubItems");
                    rptSubItems.DataSource = dv;
                    rptSubItems.DataBind();

                    Panel pnlSideMenuSubItems = (Panel)e.Item.FindControl("pnlSideMenuSubItems");
                    pnlSideMenuSubItems.Visible = true;
                }
            }
            // End Added by LM

            // add icon - sh.chong 21 oct 2015
            hypItem.CssClass += " "+clsMis.getSideMenuImageIcon(strTitle);
            // end add icon
        }
    }

    protected void rptSubItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "ADMPAGE_ID"));
            int intParentId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "ADMPAGE_PARENT"));
            string strURL = DataBinder.Eval(e.Item.DataItem, "ADMPAGE_URL").ToString();
            string strTitle = DataBinder.Eval(e.Item.DataItem, "ADMPAGE_NAME").ToString();

            HyperLink hypSubItem = (HyperLink)e.Item.FindControl("hypSubItem");
            hypSubItem.Text = strTitle;

            if (itemId > 0)
            {
                strURL = strURL.Replace(clsAdmin.CONSTIDTAG, itemId.ToString());
                strURL = strURL.Replace(clsAdmin.CONSTID2TAG, id2.ToString());
                strURL = strURL.Replace(clsAdmin.CONSTEIDTAG, eId.ToString());

                hypSubItem.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + strURL;
            }
            else
            {
                hypSubItem.CssClass = "sidemenuSubLinkDisabled";
            }

            if (subSubSectId == intId)
            {
                hypSubItem.CssClass = "sidemenuSubLinkSel";

                Panel pnlSideMenuSubItemInner = (Panel)e.Item.FindControl("pnlSideMenuSubItemInner");
                pnlSideMenuSubItemInner.CssClass = "divAdmSideMenuSubItemInnerSel";
            }
        }
    }
    #endregion


    #region "Methods"
    protected void bindMenuItems()
    {
        clsAdmin adm = new clsAdmin();

        DataSet ds = new DataSet();
        DataRow[] foundRow;
        DataView dv;

        ds = adm.getAdminPageList(1);
        dsSideMenu = ds;

        foundRow = ds.Tables[0].Select("ADMPAGE_ID = " + sectId + " AND (V_TYPE = " + type + " OR V_TYPE IS NULL)");

        dv = new DataView(ds.Tables[0]);
        dv.RowFilter = "ADMPAGE_PARENT = " + sectId + " AND ADMPAGE_SHOWINMENU = 1 AND ADMPAGE_SHOWINSIDEMENU = 1 AND (V_TYPE = " + type + " OR V_TYPE IS NULL)";

        if (Session[clsAdmin.CONSTPROJECTEVENTMANAGER] != null && clsAdmin.CONSTPROJECTEVENTMANAGER != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTEVENTMANAGER]) == 0)
            {
                dv.RowFilter += " AND ADMPAGE_NAME <> '" + clsAdmin.CONSTADMEVENTDESCRIPTIONNAME + "' AND ADMPAGE_NAME <> '" + clsAdmin.CONSTADMEVENTVIDEONAME +
                                "' AND ADMPAGE_NAME <> '" + clsAdmin.CONSTADMEVENTARTICLENAME + "' AND ADMPAGE_NAME <> '" + clsAdmin.CONSTADMEVENTCOMMENTNAME + "'";
            }
        }

        if (Session[clsAdmin.CONSTPROJECTMOBILEVIEW] != null && clsAdmin.CONSTPROJECTMOBILEVIEW != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTMOBILEVIEW]) == 0)
            {
                dv.RowFilter += " AND ADMPAGE_NAME <> '" + clsAdmin.CONSTADMMOBILEVIEW + "'";
            }
        }

        if (Session[clsAdmin.CONSTPROJECTINVENTORY] != null && Session[clsAdmin.CONSTPROJECTINVENTORY] != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTINVENTORY]) == 0)
            {
                dv.RowFilter += " AND ADMPAGE_NAME <> '" + clsAdmin.CONSTADMPRODINVENTORY + "'";
            }
        }
                    
        if (!boolShowSibling)
        {
            dv.RowFilter += " AND ADMPAGE_ID = " + subSectId;
        }
        
        if (dv.Count > 0 && foundRow.Length > 0)
        {
            int intId = Convert.ToInt16(foundRow[0]["ADMPAGE_ID"]);

            hypParent.Text = foundRow[0]["ADMPAGE_NAME"].ToString();
            hypParent.ToolTip = foundRow[0]["ADMPAGE_NAME"].ToString();

            string strURL = foundRow[0]["ADMPAGE_URL"].ToString();

            //Added by LM (28 JUL 2015)
            if (itemId > 0)
            {
                strURL = strURL.Replace(clsAdmin.CONSTIDTAG, itemId.ToString());
                strURL = strURL.Replace(clsAdmin.CONSTID2TAG, id2.ToString());
            }
            //End Added by LM

            if (idParent > 0) { strURL = strURL.Replace(clsAdmin.CONSTIDTAG, idParent.ToString()); }

            hypParent.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + strURL;

            if (intId == subSectId) { hypParent.CssClass = "sidemenuLinkSel"; }

            rptItems.DataSource = dv;
            rptItems.DataBind();
        }
        else
        {
            pnlSideMenu.Visible = false;
        }
    }
    #endregion
}
