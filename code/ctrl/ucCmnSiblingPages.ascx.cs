﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucCmnSiblingPages : System.Web.UI.UserControl
{
    #region "Enum"
    public enum View
    {
        Admin = 1,
        User = 2
    }
    #endregion


    #region "Properties"
    protected View _viewType = View.Admin;
    protected Boolean _enableLanguage = true;
    protected int _id = 0;
    protected string _templateParent = "";
    protected string _editPageURL = "";
    protected string _pageListingURL = "";
    protected int _parent = 0;

    protected int itemCount = 0;
    protected int itemTotal = 0;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public View viewType
    {
        get
        {
            if (ViewState["VIEW"] == null)
            {
                return _viewType;
            }
            else
            {
                return (View)ViewState["VIEW"];
            }
        }
        set { ViewState["VIEW"] = value; }
    }

    public Boolean enableLanguage
    {
        get
        {
            if (ViewState["ENABLELANGUAGE"] == null)
            {
                return _enableLanguage;
            }
            else
            {
                return Convert.ToBoolean(ViewState["ENABLELANGUAGE"]);
            }
        }
        set { ViewState["ENABLELANGUAGE"] = value; }
    }

    public int pageId
    {
        get
        {
            if (ViewState["ID"] == null)
            {
                return _id;
            }
            else
            {
                return Convert.ToInt16(ViewState["ID"]);
            }
        }
        set { ViewState["ID"] = value; }
    }

    protected string templateParent
    {
        get
        {
            if (ViewState["TEMPLATEPARENT"] == null)
            {
                return _templateParent;
            }
            else
            {
                return ViewState["TEMPLATEPARENT"].ToString();
            }
        }
        set { ViewState["TEMPLATEPARENT"] = value; }
    }

    public string editPageURL
    {
        get
        {
            if (ViewState["EDITPAGEURL"] == null)
            {
                return _editPageURL;
            }
            else
            {
                return ViewState["EDITPAGEURL"].ToString();
            }
        }
        set { ViewState["EDITPAGEURL"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if(ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }

    protected int parent
    {
        get
        {
            if (ViewState["PARENT"] == null)
            {
                return _parent;
            }
            else
            {
                return Convert.ToInt16(ViewState["PARENT"]);
            }
        }
        set { ViewState["PARENT"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "siblingpages.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (viewType == View.User)
        {
            lblSiblingPages.CssClass = "lblUsrTitle";
        }
        
        litNo.Text = GetLocalResourceObject("litNo.Text").ToString();
        litPageName.Text = GetLocalResourceObject("litPageName.Text").ToString();
        litLink.Text = GetLocalResourceObject("litLink.Text").ToString();
        litAction.Text = GetLocalResourceObject("litAction.Text").ToString();
    }

    protected void rptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            itemCount += 1;

            int intId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ID"));
            string strTitle = DataBinder.Eval(e.Item.DataItem, "PAGE_DISPLAYNAME").ToString();
            string strLang = DataBinder.Eval(e.Item.DataItem, "PAGE_LANG").ToString();
            string strTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATE").ToString();
            int intTemplateParent = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATEPARENT"));
            int intRedirect = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECT"));
            string strRedirectLink = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTLINK").ToString();

            string strURL = "";
            string strDURL = "";
            string strTemplateUse = "";

            if (intRedirect > 0)
            {
                if (intRedirect == clsAdmin.CONSTEXTERNALLINK)
                {
                    strURL = !strRedirectLink.StartsWith("http") ? "http://" + strRedirectLink : strRedirectLink;
                    strDURL = strURL;
                }
                else
                {
                    clsPage page = new clsPage();
                    if (page.extractPageById(intRedirect, 0)) { strTemplateUse = page.template; }
                }
            }
            else
            {
                strTemplateUse = intTemplateParent > 0 && !string.IsNullOrEmpty(templateParent) ? templateParent : strTemplate;
            }

            if (!string.IsNullOrEmpty(strTemplateUse))
            {
                strURL = ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateUse + "?pgid=" + intId + (enableLanguage ? "&lang=" + strLang : "");
                strDURL = Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/usr/" + strTemplateUse + "?pgid=" + intId + (enableLanguage ? "&lang=" + strLang : "");
            }

            HyperLink hypSiblingPages = (HyperLink)e.Item.FindControl("hypSiblingPages");
            hypSiblingPages.Text = strDURL;
            hypSiblingPages.ToolTip = strTitle;

            if (viewType == View.Admin)
            {
                hypSiblingPages.NavigateUrl = editPageURL + "?id=" + intId;

                HtmlTableCell tdAct = (HtmlTableCell)e.Item.FindControl("tdAct");
                tdAct.Visible = true;
            }
            else if (viewType == View.User)
            {
                hypSiblingPages.NavigateUrl = strURL;
            }

            if (itemCount == 1)
            {
                LinkButton lnkbtnUp = (LinkButton)e.Item.FindControl("lnkbtnUp");
                lnkbtnUp.Enabled = false;
                //lnkbtnUp.CssClass = "btnSPDisabled";
                lnkbtnUp.CssClass += " disabled";
            }
            else if (itemCount == itemTotal)
            {
                LinkButton lnkbtnDown = (LinkButton)e.Item.FindControl("lnkbtnDown");
                lnkbtnDown.Enabled = false;
                //lnkbtnDown.CssClass = "btnSPDisabled";
                lnkbtnDown.CssClass += " disabled";
            }

            Literal litPageNo = (Literal)e.Item.FindControl("litPageNo");
            litPageNo.Text = itemCount.ToString();

            Literal litPageTitle = (Literal)e.Item.FindControl("litPageTitle");

            //clsConfig config = new clsConfig();
            //strTitle = strTitle.Replace(config.separator, " ");

            if (pageId == intId)
            {
                hypSiblingPages.CssClass = "btnSPBold";
                litPageNo.Text = "<span class=\"boldmsg\">" + itemCount.ToString() + "</span>";
                litPageTitle.Text = "<span class=\"boldmsg\">" + strTitle + " (Current Page)</span>";
            }
            else
            {
                litPageTitle.Text = strTitle;
            }
        }
    }

    protected void rptItems_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        DataRow[] foundRow;

        string strArg = e.CommandArgument.ToString();
        string[] strArgSplit = strArg.Split((char)'|');

        int intId = Convert.ToInt16(strArgSplit[0]);
        int intOrder = Convert.ToInt16(strArgSplit[1]);

        if (e.CommandName == "cmdUp")
        {
            int intPrevOrder = 0;
            int intPrevId = 0;

            clsPage page = new clsPage();
            if (page.extractPrevPage(parent, intOrder, 0))
            {
                intPrevOrder = page.order;
                intPrevId = page.id;
                page.updateOrderById2(intId, intPrevOrder);
                page.updateOrderById2(intPrevId, intOrder);

                clsMis.resetListing();
                bindRptData();
            }
        }
        else if (e.CommandName == "cmdDown")
        {
            int intNextOrder = 0;
            int intNextId = 0;

            clsPage page = new clsPage();
            if (page.extractNextPage(parent, intOrder, 0))
            {
                intNextOrder = page.order;
                intNextId = page.id;

                page.updateOrderById2(intId, intNextOrder);
                page.updateOrderById2(intNextId, intOrder);

                clsMis.resetListing();
                bindRptData();
            }
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();

            if (pageId > 0) { bindRptData(); }
            else
            {
                if (viewType == View.Admin)
                {
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
                    Response.Redirect(currentPageName);
                }
            }
        }
    }

    protected void setPageProperties()
    {
        if (viewType == View.Admin)
        {
            lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";

            trBtn.Visible = true;
        }
    }

    protected void bindRptData()
    {
        clsPage page = new clsPage();

        if (page.extractPageById(pageId, 0))
        {
            if (viewType == View.Admin)
            {
                if (page.other > 0)
                {
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
                    Response.Redirect(currentPageName);
                }
            }

            parent = page.parent;

            if (page.extractPageById(parent, 0)) { templateParent = page.template; }

            DataSet ds = new DataSet();
            DataView dv;

            ds = page.getPageList(0);
            dv = new DataView(ds.Tables[0]);
            dv.RowFilter = "PAGE_PARENT = " + parent;
            dv.Sort = "PAGE_ORDER ASC";

            if (dv.Count > 0)
            {
                itemCount = 0;
                itemTotal = dv.Count;

                rptItems.DataSource = dv;
                rptItems.DataBind();

                tdItems.Visible = true;
            }
            else
            {
                tdNoItem.Visible = true;
            }

            string strFound = GetLocalResourceObject("litFound.Text").ToString();
            litFound.Text = strFound.Replace(clsAdmin.CONSTTOTALTAG, dv.Count.ToString());
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }
    #endregion
}
