﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmJobDetail.ascx.cs" Inherits="ctrl_ucAdmJobDetail" %>
<script type="text/javascript">
</script>
<asp:Panel ID="pnlFormSect1" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server" CssClass="container-fluid">
        <div class="form__container">
            <div class="ack-container success hide">
                <div class="ack-msg"></div>
            </div>
        </div>

        <!-- Start of Right Hand Side Status Panel -->
        <asp:Panel ID="pnlLastLogin" runat="server">
            <div class="payment-status-panel">
                <span class="form__tag__block">
                    Status:
                    <asp:Label runat="server" ID="lblStatus" CssClass="last-action-date" style="line-height: 35px;"></asp:Label>
                    <asp:LinkButton ID="lnkbtnCancelJob" runat="server" Text="Cancel Job" ToolTip="Cancel Job" CssClass="btn btn-danger pull-right"  OnClick="lnkbtnCancelJob_Click" CausesValidation="false"></asp:LinkButton>
                </span>
                <span class="form__tag__block">
                    <asp:Repeater runat="server" ID="rptPayments">
                        <HeaderTemplate></HeaderTemplate>
                        <FooterTemplate></FooterTemplate>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPaymentOrRefund" Text='<%# Eval("StatusInText") %>'></asp:Label><br />
                            <asp:Label runat="server" ID="lblPaymentGateway" CssClass="last-action-date"  Text='<%# Eval("GatewayDescription") %>'></asp:Label> - 
                            <asp:Label runat="server" ID="lblPaymentTransId" CssClass="last-action-date"  Text='<%# Eval("transID") %>'></asp:Label><br />
                            <asp:Label runat="server" ID="lblPaymentDate" CssClass="last-action-date"  Text='<%# Eval("PayDate") %>'></asp:Label><br />
                            <asp:Label runat="server" ID="lblPaymentAmount" CssClass="last-action-date"  Text='<%# Eval("Amount") %>'></asp:Label><br />
                            <br />
                        </ItemTemplate>
                    </asp:Repeater>
                </span>
                <span class="form__tag__block">
                    <span runat="server" visible="false">
                        Team<br />
                        <asp:Label runat="server" ID="lblTeam" CssClass="last-action-date"></asp:Label>
                        <asp:DropDownList ID="ddlTeam" runat="server" CssClass="form-control col-sm-5"></asp:DropDownList>&nbsp;
                        <asp:LinkButton ID="lnkbtnAssign" runat="server" Text="Assign" ToolTip="Assign Team" CssClass="btn btn-info" CausesValidation="false" OnClick="lnkbtnAssign_Click"></asp:LinkButton>
                        <br />
                        <br />
                    </span>
                    <span>
                        Agent<br />
                        <asp:Label runat="server" ID="lblAgent" CssClass="last-action-date"></asp:Label>
                        <asp:DropDownList ID="ddlAgent" runat="server" CssClass="form-control col-sm-5"></asp:DropDownList>&nbsp;
                        <asp:LinkButton ID="lnkbtnAssignAgent" runat="server" Text="Assign" ToolTip="Assign Team" CssClass="btn btn-info" CausesValidation="false" OnClick="lnkbtnAssignAgent_Click"></asp:LinkButton>
                        <br />
                        <br />
                    </span>

                    Assign On<br />
                    <asp:Label runat="server" ID="lblServiceAssignTime" CssClass="last-action-date"></asp:Label><br />
                    <br />
                    Service Status<br />
                    <asp:Label runat="server" ID="lblServiceStatus" CssClass="last-action-date"></asp:Label>
                    <asp:LinkButton ID="lnkbtnMarkasComplete" runat="server" Text="Mark as Completed" ToolTip="Mark as Completed" CssClass="btn btn-success pull-right" CausesValidation="false"  OnClick="lnkbtnMarkasComplete_Click"></asp:LinkButton><br />
                    <asp:Label runat="server" ID="lblServiceCompleteTime" CssClass="last-action-date"></asp:Label><br />
                </span>
            </div>
        </asp:Panel>
        <!-- End of Right Hand Side Status Panel -->

        <!-- Start of Left Hand Side -->
        <div class="form__container form__container--action-floated" style="width: 80%">
            <!-- Start of Job Details -->
            <div class="form__section">
                
                <div class="form__section__title">
                    <span>Job Details</span>
                </div>
                <div class="form__section__content form__section__content-leftpad">
                    
                    <div class="row">
                        <div class="col-md-3 ">Order ID:</div>
                        <div class="col-md-9 ">
                            <asp:Literal ID="litOrderID" runat="server"></asp:Literal>&nbsp;
                            <asp:HyperLink ID="hypPrintInvoice" runat="server">Print Invoice</asp:HyperLink> | <asp:HyperLink ID="hypPrintReceipt" runat="server">Print Receipt</asp:HyperLink>
                        </div>

                        <div class="col-md-3 ">Order Date:</div>
                        <div class="col-md-9 ">
                            <asp:Literal ID="litOrderDate" runat="server"></asp:Literal>
                        </div>

                        <div class="col-md-3 ">Service Date Time:</div>
                        <div class="col-md-9 ">
                            <asp:Literal ID="litServiceDateTime" runat="server"></asp:Literal>
                        </div>

                        <div class="col-md-3 ">Area + Unit No:</div>
                        <div class="col-md-9 ">
                            <asp:Literal ID="litAreaUnit" runat="server"></asp:Literal>
                        </div>

                        <div class="col-md-3 ">No. of Aircond:</div>
                        <div class="col-md-9 ">
                            <asp:Literal ID="litNoACond" runat="server"></asp:Literal>
                        </div>

                        <div class="col-md-3 ">Special Request:</div>
                        <div class="col-md-9 ">
                            <asp:Literal ID="litSpecialRequest" runat="server"></asp:Literal>
                        </div>

                        <div class="col-md-3 ">Urgent Contact:</div>
                        <div class="col-md-9 ">
                            <asp:Literal ID="litUrgentContact" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Job Details -->

            <!-- Start of Resident Details -->
            <div class="form__section">
                <div class="form__section__title">
                    <span>Resident Details</span>
                </div>
                <div class="form__section__content form__section__content-leftpad">
                    <div class="row">
                        <div class="col-md-3 ">Name:</div>
                        <div class="col-md-9 ">
                            <asp:Literal ID="litResidentName" runat="server"></asp:Literal>
                        </div>

                        <div class="col-md-3 ">Contact No:</div>
                        <div class="col-md-9 ">
                            <asp:Literal ID="litContactNo" runat="server"></asp:Literal>
                        </div>

                        <div class="col-md-3 ">Email:</div>
                        <div class="col-md-9 ">
                            <asp:Literal ID="litEmail" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Resident Details -->
        </div>
        <!-- End of Left Hand Side -->

        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                <asp:HyperLink ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack" CausesValidation="false" NavigateUrl="../adm/admJob01.aspx"></asp:HyperLink>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>

</asp:Panel>

<script type="text/javascript">
    var SaveSuccess = function (msg) {
        $(".ack-container .ack-msg").html(msg);
        $(".ack-container").removeClass("hide");
    };
</script>