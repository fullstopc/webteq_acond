﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;
using System.Linq;

public partial class ctrl_ucUsrTopMenu : System.Web.UI.UserControl
{
    #region "Enum"
    public enum MenuType
    {
        Link = 1,
        ImageLink = 2
    }

    public enum TopMenuType
    {
        TopTopMenu = 1,
        TopMenu = 2
    }

    public enum PageType
    {
        PageType = 1,
        subPageType = 2
    }
    #endregion

    #region "Properties"
    protected string _lang = "";
    protected int _pgid = 0;
    protected int _eid = 0;
    protected MenuType _menuType = MenuType.Link;
    protected TopMenuType _topMenuType = TopMenuType.TopMenu;
    protected PageType _pageType = PageType.PageType;
    protected DataSet _dsMenu = new DataSet();

    protected Boolean boolPage = true;
    #endregion

    #region "Property Methods"
    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }

    public int pgid
    {
        get { return _pgid; }
        set { _pgid = value; }
    }

    public int eid
    {
        get { return _eid; }
        set { _eid = value; }
    }

    public MenuType menuType
    {
        get
        {
            if (ViewState["MENUTYPE"] == null)
            {
                return _menuType;
            }
            else
            {
                return (MenuType)ViewState["MENUTYPE"];
            }
        }
        set { ViewState["MENUTYPE"] = value; }
    }

    public TopMenuType topMenuType
    {
        get
        {
            if (ViewState["TOPMENUTYPE"] == null)
            {
                return _topMenuType;
            }
            else
            {
                return (TopMenuType)ViewState["TOPMENUTYPE"];
            }
        }
        set { ViewState["TOPMENUTYPE"] = value; }
    }

    public PageType pageType
    {
        get
        {
            if (ViewState["PAGETYPE"] == null)
            {
                return _pageType;
            }
            else
            {
                return (PageType)ViewState["PAGETYPE"];
            }
        }
        set { ViewState["PAGETYPE"] = value; }
    }

    protected DataSet dsMenu
    {
        get
        {
            if (ViewState["DSMENU"] == null)
            {
                return _dsMenu;
            }
            else
            {
                return (DataSet)ViewState["DSMENU"];
            }
        }
        set { ViewState["DSMENU"] = value; }
    }
    #endregion

    #region "page Events"
    protected void Page_Load(object sender, EventArgs e){}

    protected void rptMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ID"));
            string strDisplayName = DataBinder.Eval(e.Item.DataItem, "PAGE_DISPLAYNAME").ToString();
            string strContent = DataBinder.Eval(e.Item.DataItem, "PAGE_TOPMENUCONTENT").ToString();
            string strCSSClass = DataBinder.Eval(e.Item.DataItem, "PAGE_CSSCLASS").ToString();
            string strLang = DataBinder.Eval(e.Item.DataItem, "PAGE_LANG").ToString();
            string strTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATE").ToString();
            int intRedirect = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECT"));
            string strRedirectLink = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTLINK").ToString();
            string strTarget = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTTARGET").ToString();

            clsConfig config = new clsConfig();
            int intEnableLink = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ENABLELINK"));

            Image imgMenu = (Image)e.Item.FindControl("imgMenu");
            imgMenu.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";

            HyperLink hypMenu = (HyperLink)e.Item.FindControl("hypMenu");
            hypMenu.ToolTip = strDisplayName;

            if (menuType == MenuType.Link)
            {
                imgMenu.Visible = false;
                hypMenu.Text = "<span>"+ strDisplayName + "</span>";
            }

            if (pageType == PageType.subPageType)
            {
                strCSSClass += " topMenuSub";
            }

            if (!string.IsNullOrEmpty(strCSSClass)) { hypMenu.CssClass = strCSSClass; }

            if (intId == pgid )
            {
                hypMenu.CssClass = (!string.IsNullOrEmpty(strCSSClass) ? strCSSClass : hypMenu.CssClass) + " selected";
            }

            if (intEnableLink == 0)
            {
                hypMenu.NavigateUrl = string.Empty;
                hypMenu.Attributes.Remove("href");
                hypMenu.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "default";
            }
            else
            {
                if (intRedirect > 0)
                {
                    if (intRedirect == clsSetting.CONSTEXTERNALLINK)
                    {
                        if (!string.IsNullOrEmpty(strRedirectLink))
                        {
                            string strLink = strRedirectLink;

                            if (strRedirectLink.StartsWith("http://") || strRedirectLink.StartsWith("https://"))
                            {
                                hypMenu.NavigateUrl = strLink;
                            }
                            else
                            {
                                hypMenu.NavigateUrl = "http://" + strLink;
                            }

                            //hypMenu.NavigateUrl = strLink;
                            hypMenu.Target = strTarget;
                        }
                    }
                    else
                    {
                        DataRow[] foundRow = dsMenu.Tables[0].Select("PAGE_ID = " + intRedirect + " AND PAGE_ACTIVE = 1 AND PAGE_OTHER = 0");

                        if (foundRow.Length > 0)
                        {
                            int intRParent = Convert.ToInt16(foundRow[0]["PAGE_PARENT"]);
                            int intRTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                            string strRTemplateParent = foundRow[0]["V_TEMPLATEPARENT"].ToString();
                            string strRTemplate = intRParent > 0 ? (intRTemplateParent > 0 ? strRTemplateParent : strTemplate) : strTemplate;

                            if (!string.IsNullOrEmpty(strRTemplate))
                            {
                                if (!string.IsNullOrEmpty(lang)) { hypMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strRTemplate + "?pgid=" + intRedirect + "&lang=" + strLang; }
                                else { hypMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strRTemplate + "?pgid=" + intRedirect; }
                            }
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(strTemplate))
                    {
                        if (!string.IsNullOrEmpty(lang)) { hypMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + intId + "&lang=" + strLang; }
                        else { hypMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + intId; }
                    }
                }
            }

            if (!string.IsNullOrEmpty(strContent))
            {
                Panel pnlMenu = (Panel)e.Item.FindControl("pnlMenu");
                Panel pnlDropDownContainer = (Panel)e.Item.FindControl("pnlDropDownContainer");
                Panel pnlMenuContent = (Panel)e.Item.FindControl("pnlMenuContent");

                registerScriptShowDropDownSub(pnlTopMenuContainer.ClientID, pnlMenuContent.ClientID, hypMenu.ClientID, pnlDropDownContainer.ClientID);
                //registerScriptShowDropDown(pnlMenu.ClientID, pnlDropDownContainer.ClientID, hypMenu.ClientID);
                registerScriptShowDropDown(pnlMenu.ClientID, pnlDropDownContainer.ClientID, pnlMenuContent.ClientID);
                pnlDropDownContainer.Visible = true;
                pnlMenuContent.Visible = true;

                Literal litMenuContent = (Literal)e.Item.FindControl("litMenuContent");
                litMenuContent.Text = strContent;
            }

            //DataView dv = new DataView(dsMenu.Tables[0]);
            //dv.RowFilter = "PAGE_PARENT = " + intId + " AND PAGE_SHOWINMENU = 1 AND PAGE_ACTIVE = 1 AND PAGE_OTHER = 0 AND PAGE_SHOWTOP = 1";
            //dv.Sort = "PAGE_ORDER ASC";

            //if (dv.Count > 0)
            //{
            //    Repeater rptSubMenu = (Repeater)e.Item.FindControl("rptSubMenu");

            //    rptSubMenu.DataSource = dv;
            //    rptSubMenu.DataBind();
            //}


            //Add Gallery DropDown
            boolPage = true;
            string strName = DataBinder.Eval(e.Item.DataItem, "PAGE_NAME").ToString();
            //DataSet dsMenu = new DataSet();
            DataView dv = new DataView();

            if (string.Compare(strTemplate, clsSetting.CONSTUSRGALLERYPAGE, true) != 0)
            {
                boolPage = false;
            }

            if (boolPage)
            {
                int intLimit = 10;

                clsHighlight highlight = new clsHighlight();
                //Application["HIGHLIGHT"] = highlight.getHighDropDown(1);
                highlight.getHighDropDown(1);

                DataSet dsMenu1 = highlight.getHighDropDown(1);
                DataView dvMenu1 = new DataView(dsMenu1.Tables[0]);

                dvMenu1.Sort = "HIGH_ORDER ASC";
                dvMenu1.RowFilter = "HIGH_VISIBLE = 1";

                DataTable dataTable = dvMenu1.ToTable();
                while (dataTable.Rows.Count > intLimit)
                {
                    dataTable = dataTable.AsEnumerable().Skip(0).Take(intLimit).CopyToDataTable();
                }
                dvMenu1 = new DataView(dataTable);
               
                if (dvMenu1.Count > 0)
                {

                    Repeater rptGallery = (Repeater)e.Item.FindControl("rptGallery");

                    rptGallery.DataSource = dvMenu1;
                    rptGallery.DataBind();
                }
            }

            else
            {
                DataView dvSubMenu = new DataView(dsMenu.Tables[0]);
                dvSubMenu.RowFilter = "PAGE_PARENT = " + intId + " AND PAGE_SHOWINMENU = 1 AND PAGE_ACTIVE = 1 AND PAGE_OTHER = 0";
                dvSubMenu.Sort = "PAGE_ORDER ASC";

                if (dvSubMenu.Count > 0)
                {
                    Repeater rptSubMenu = (Repeater)e.Item.FindControl("rptSubMenu");

                    rptSubMenu.DataSource = dvSubMenu;
                    rptSubMenu.DataBind();
                }
            }
            //end add gallery dropdown

        }
        else if (e.Item.ItemType == ListItemType.Separator)
        {

        }
    }

    protected void rptSubMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ID"));
            string strDisplayName = DataBinder.Eval(e.Item.DataItem, "PAGE_DISPLAYNAME").ToString();
            string strTitle = DataBinder.Eval(e.Item.DataItem, "PAGE_TITLE").ToString();
            string strCSSClass = DataBinder.Eval(e.Item.DataItem, "PAGE_CSSCLASS").ToString();
            string strLang = DataBinder.Eval(e.Item.DataItem, "PAGE_LANG").ToString();
            string strTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATE").ToString();
            int intRedirect = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECT"));
            int intParent = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_PARENT"));
            int intParentTemplate = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATEPARENT"));
            string strParentTemplate = DataBinder.Eval(e.Item.DataItem, "V_TEMPLATEPARENT").ToString();
            string strParentCSSClass = DataBinder.Eval(e.Item.DataItem, "V_PARENTCSSCLASS").ToString();
            int intEnableLink = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ENABLELINK"));
            string strRedirectLink = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTLINK").ToString();
            string strRedirectTarget = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTTARGET").ToString();

            HyperLink hypSubMenu = (HyperLink)e.Item.FindControl("hypSubMenu");
            hypSubMenu.Text = strDisplayName;
            hypSubMenu.ToolTip = strDisplayName;

            if (!string.IsNullOrEmpty(strCSSClass)) { hypSubMenu.CssClass = strCSSClass; }

            clsPage page = new clsPage();
            int intParentId = page.getParentId(pgid);

            if (intId == pgid || intParentId == intId)
            {
                HyperLink hypMenu = (HyperLink)e.Item.NamingContainer.NamingContainer.FindControl("hypMenu");
                hypMenu.CssClass = (!string.IsNullOrEmpty(strParentCSSClass) ? strParentCSSClass : hypMenu.CssClass) + " selected";

                HtmlGenericControl control = (HtmlGenericControl)e.Item.FindControl("liSubMenu");
                control.Attributes["class"] = "sel";

                hypSubMenu.CssClass = hypSubMenu.CssClass.Replace("selected","") + " selected";
            }

            if (intEnableLink == 0)
            {
                hypSubMenu.NavigateUrl = string.Empty;
                hypSubMenu.Attributes.Remove("href");
                hypSubMenu.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "default";
            }
            else
            {
                if (intRedirect > 0)
                {
                    if (intRedirect == clsSetting.CONSTEXTERNALLINK)
                    {
                        if (!string.IsNullOrEmpty(strRedirectLink))
                        {
                            string strLink = strRedirectLink;

                            if (strRedirectLink.StartsWith("http://") || strRedirectLink.StartsWith("https://"))
                            {
                                hypSubMenu.NavigateUrl = strLink;
                            }
                            else
                            {
                                hypSubMenu.NavigateUrl = "http://" + strLink;
                            }

                            //hypSubMenu.NavigateUrl = strLink;
                            hypSubMenu.Target = strRedirectTarget;
                        }
                    }
                    else
                    {
                        if (Application["PAGELIST"] == null)
                        {
                            //clsPage page = new clsPage();
                            Application["PAGELIST"] = page.getPageList(1);
                        }

                        DataSet ds = new DataSet();
                        ds = (DataSet)Application["PAGELIST"];

                        DataRow[] foundRow = ds.Tables[0].Select("PAGE_ID = " + intRedirect + " AND PAGE_ACTIVE = 1 AND PAGE_OTHER = 0");

                        if (foundRow.Length > 0)
                        {
                            int intRParent = Convert.ToInt16(foundRow[0]["PAGE_PARENT"]);
                            int intRTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                            string strRTemplateParent = foundRow[0]["V_TEMPLATEPARENT"].ToString();
                            string strRTemplate = intRParent > 0 ? (intRTemplateParent > 0 ? strRTemplateParent : strTemplate) : strTemplate;

                            if (!string.IsNullOrEmpty(strRTemplate))
                            {
                                if (!string.IsNullOrEmpty(lang)) { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strRTemplate + "?pgid=" + intRedirect + "&lang=" + strLang; }
                                else { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strRTemplate + "?pgid=" + intRedirect; }
                            }
                        }
                    }
                }
                else
                {
                    string strTemplateUse = intParentTemplate > 0 ? strParentTemplate : strTemplate;

                    if (!string.IsNullOrEmpty(strTemplateUse))
                    {
                        if (!string.IsNullOrEmpty(lang)) { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateUse + "?pgid=" + intId + "&lang=" + strLang; }
                        else { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateUse + "?pgid=" + intId; }
                    }
                }
            }

            DataView dv = new DataView(dsMenu.Tables[0]);
            dv.RowFilter = "PAGE_PARENT = " + intId + " AND PAGE_SHOWINMENU = 1 AND PAGE_ACTIVE = 1 AND PAGE_OTHER = 0";
            dv.Sort = "PAGE_ORDER ASC";

            if (dv.Count > 0)
            {
                Repeater rptSubMenuLv2 = (Repeater)e.Item.FindControl("rptSubMenuLv2");

                rptSubMenuLv2.DataSource = dv;
                rptSubMenuLv2.DataBind();

                hypSubMenu.CssClass = "SubMenuParent";
            }
        }
    }

    protected void rptSubMenuLv2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ID"));
            string strDisplayName = DataBinder.Eval(e.Item.DataItem, "PAGE_DISPLAYNAME").ToString();
            string strTitle = DataBinder.Eval(e.Item.DataItem, "PAGE_TITLE").ToString();
            string strCSSClass = DataBinder.Eval(e.Item.DataItem, "PAGE_CSSCLASS").ToString();
            string strLang = DataBinder.Eval(e.Item.DataItem, "PAGE_LANG").ToString();
            string strTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATE").ToString();
            int intRedirect = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECT"));
            int intParent = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_PARENT"));
            int intParentTemplate = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATEPARENT"));
            string strParentTemplate = DataBinder.Eval(e.Item.DataItem, "V_TEMPLATEPARENT").ToString();
            string strParentCSSClass = DataBinder.Eval(e.Item.DataItem, "V_PARENTCSSCLASS").ToString();
            int intEnableLink = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ENABLELINK"));
            string strRedirectLink = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTLINK").ToString();
            string strRedirectTarget = DataBinder.Eval(e.Item.DataItem, "PAGE_REDIRECTTARGET").ToString();

            HyperLink hypSubMenuLv2 = (HyperLink)e.Item.FindControl("hypSubMenuLv2");
            hypSubMenuLv2.Text = strDisplayName;
            hypSubMenuLv2.ToolTip = strDisplayName;

            if (!string.IsNullOrEmpty(strCSSClass)) { hypSubMenuLv2.CssClass = strCSSClass; }

            if (intId == pgid)
            {
                HtmlGenericControl control = (HtmlGenericControl)e.Item.FindControl("liSubMenuLv2");
                control.Attributes["class"] = "sel";

                hypSubMenuLv2.CssClass = "hypTopSubMenuLv2Sel";
                //hypSubMenu.CssClass = !string.IsNullOrEmpty(strCSSClass) ? strCSSClass + "Sel" : "sel";
            }

            if (intEnableLink == 0)
            {
                hypSubMenuLv2.NavigateUrl = string.Empty;
                hypSubMenuLv2.Attributes.Remove("href");
                hypSubMenuLv2.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "default";
            }
            else
            {
                if (intRedirect > 0)
                {
                    if (intRedirect == clsSetting.CONSTEXTERNALLINK)
                    {
                        if (!string.IsNullOrEmpty(strRedirectLink))
                        {
                            string strLink = strRedirectLink;

                            if (strRedirectLink.StartsWith("http://") || strRedirectLink.StartsWith("https://"))
                            {
                                hypSubMenuLv2.NavigateUrl = strLink;
                            }
                            else
                            {
                                hypSubMenuLv2.NavigateUrl = "http://" + strLink;
                            }

                            //hypSubMenu.NavigateUrl = strLink;
                            hypSubMenuLv2.Target = strRedirectTarget;
                        }
                    }
                }
                else
                {
                    string strTemplateUse = intParentTemplate > 0 ? strParentTemplate : strTemplate;

                    if (!string.IsNullOrEmpty(strTemplateUse))
                    {
                        if (!string.IsNullOrEmpty(lang)) { hypSubMenuLv2.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateUse + "?pgid=" + intId + "&lang=" + strLang; }
                        else { hypSubMenuLv2.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateUse + "?pgid=" + intId; }
                    }
                }
            }
        }
    }

    protected void rptGallery_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            int highId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "HIGH_ID"));
            string strHighTitle = DataBinder.Eval(e.Item.DataItem, "HIGH_TITLE").ToString();

            HyperLink hypGalleryMenu = (HyperLink)e.Item.FindControl("hypGalleryMenu");
            hypGalleryMenu.Text = strHighTitle;
            hypGalleryMenu.ToolTip = strHighTitle;

            clsPage page = new clsPage();
            int intPgId = page.getFirstPageByTemplate(clsSetting.CONSTUSRGALLERYPAGE, 1);

            hypGalleryMenu.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/" + clsSetting.CONSTUSRGALLERYPAGE + "?pgid=" + intPgId + "&eid=" + highId;

            if (highId == eid)
            {
                HtmlGenericControl control = (HtmlGenericControl)e.Item.FindControl("liSubMenu");
                control.Attributes["class"] = "sel";
                hypGalleryMenu.CssClass = " selected";
            }
        }

    }
    #endregion

    #region "methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            bindRptData();
        }
    }

    protected void bindRptData()
    {
        if (Application["PAGELIST"] == null)
        {
            clsPage page = new clsPage();
            Application["PAGELIST"] = page.getPageList(1);
        }

        DataSet ds = new DataSet();
        ds = (DataSet)Application["PAGELIST"];
        dsMenu = ds;

        DataView dv = new DataView(ds.Tables[0]);

        dv.RowFilter = "PAGE_OTHER = 0 AND PAGE_SHOWINMENU = 1 AND PAGE_SHOWTOP = 1 ";
       

        if (topMenuType == TopMenuType.TopMenu)
        {
            dv.RowFilter += " AND PAGE_PARENT = " + clsSetting.CONSTPAGEPARENTROOT;
        }
        else if (topMenuType == TopMenuType.TopTopMenu)
        {
            dv.RowFilter += " AND PAGE_PARENT = " + clsSetting.CONSTPAGEPARENTROOT2;
        }

        if (!string.IsNullOrEmpty(lang)) { dv.RowFilter += " AND PAGE_LANG = '" + lang + "'"; }

        dv.Sort = "PAGE_ORDER ASC";

        if (dv.Count > 0)
        {
            rptMenu.DataSource = dv;
            rptMenu.DataBind();
        }
    }

    protected void registerScriptShowDropDownSub(string container, string strFormId, string button, string arrow)
    {
        string strJS = null;
        strJS = "<script type=\"text/javascript\">";
        strJS += "setDropDownSub2('" + container + "','" + strFormId + "','" + button + "','" + arrow + "');";
        strJS += "</script>";

        Page.ClientScript.RegisterStartupScript(Page.GetType(), "FORM" + strFormId, strJS, false);
    }

    protected void registerScriptShowDropDown(string container, string strFormId, string button)
    {
        string strJS = null;
        strJS = "<script type=\"text/javascript\">";
        strJS += "setDropDown('" + container + "','" + strFormId + "','" + button + "');";
        strJS += "</script>";

        Page.ClientScript.RegisterStartupScript(Page.GetType(), "FORM" + strFormId, strJS, false);
    }
    #endregion
}
