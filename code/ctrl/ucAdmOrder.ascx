﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmOrder.ascx.cs" Inherits="ctrl_ucAdmOrder" %>
<%@ Register Src="~/ctrl/ucAdmOrderItems.ascx" TagName="AdmOrderItems" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmOrderDetails.ascx" TagName="AdmOrderDetails" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmOrderPayment.ascx" TagName="AdmOrderPayment" TagPrefix="uc" %>

<script type="text/javascript">
    function validateCurrency_client(source, args) {
        var currRE = /<%= clsAdmin.CONSTCURRENCYRE %>/;

        if (args.Value.match(currRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "<br />Only currency is allowed.";
            source.innerHTML = "<br />Only currency is allowed.";
        }
    }

    var isPostback = false;
    jQuery(document).ready(function() {
        $('.lnkbtnConfirm').click(function() {
            if (!Page_ClientValidate()) { }
            else {
                if (!isPostback) {
                    isPostback = true;
                    eval($(this).attr("href"));
                }
                $(this).attr("href", "#");
                return false;
            }
        });

    });

    function print() {
        document.getElementById('TB_iframeContent').contentWindow.print();
    }
</script>
<style>
#TB_window{top:25%;}
</style>
<asp:Panel ID="pnlOrder" runat="server" CssClass="divSalesDetail">
    <asp:Panel ID="pnlTop" runat="server" CssClass="divTop">
        <asp:Panel ID="pnlActionPrint" runat="server" CssClass="divActionPrint">
            <asp:LinkButton ID="lnkbtnPrint" runat="server" AlternateText="Print" Text="Print" CssClass="btn2 btnPrint thickbox" />
        </asp:Panel>
        <asp:Panel ID="pnlSalesSummary" runat="server" CssClass="divSalesSummary">
            <table id="tblSalesSummary" cellpadding="0" cellspacing="0" border="0" class="formTbl">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Sales Summary</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Sales No.:</td>
                        <td><span class="boldmsg"><asp:Literal ID="litOrdNo" runat="server"></asp:Literal></span></td>
                    </tr>
                    <tr>
                        <td>Sales Date:</td>
                        <td><asp:Literal ID="litOrdDate" runat="server"></asp:Literal></td>
                    </tr>
                    <tr id="trType" runat="server" visible="false">
                        <td>Sales Type:</td>
                        <td><asp:Literal ID="litOrdType" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Member:</td>
                        <td><asp:Literal ID="litMember" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td class="nobr">Sales Amount:</td>
                        <td>
                            <asp:Literal ID="litOrdAmount" runat="server"></asp:Literal>
                            <asp:HiddenField ID="hdnOrdAmount" runat="server" />
                            <asp:HiddenField ID="hdnPayAmount" runat="server" />
                            <asp:HiddenField ID="hdnPayGateway" runat="server" />
                            <asp:HiddenField ID="hdnPayDate" runat="server" />
                            <asp:HiddenField ID="hdnPayRemarks" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>Message To Seller:</td>
                        <td><asp:Literal ID="litMsgBuyer" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Remarks:</td>
                        <td>
                            <asp:TextBox ID="txtRemarks" runat="server" MaxLength="1000" TextMode="MultiLine" Rows="5" CssClass="text_big"></asp:TextBox>
                            <asp:HiddenField ID="hdnRemarks" runat="server" />
                        </td>
                    </tr>
                    <tr id="trConvRate" runat="server" visible="false">
                        <td class="nobr">Convertion Rate:</td>
                        <td><asp:Literal ID="litConvRate" runat="server"></asp:Literal></td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td>
                            <asp:LinkButton ID="lnkbtnSaveRemarks" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSaveRemarks_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnBackToList" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack"></asp:LinkButton>
                        </td>
                    </tr>
                </tfoot>
                
            </table>
        </asp:Panel>
        
    </asp:Panel>
    <div class="divLeft">
        <asp:Panel ID="pnlDetails" runat="server" CssClass="divDetails">
            <uc:AdmOrderDetails ID="ucAdmOrderDetails" runat="server" />
        </asp:Panel>
    </div>
    <div class="divRight">
        <asp:Panel ID="pnlStatus" runat="server" CssClass="divStatus" data-beforeleave="0">
                <div class="divStatusHeader">Order Status</div>
                <asp:Repeater ID="rptStatus" runat="server" OnItemDataBound="rptStatus_ItemDataBound" OnItemCommand="rptStatus_ItemCommand">
                    <HeaderTemplate>
                        <table cellpadding="0" cellspacing="0" border="0" class="tblStatus">
                            <thead>
                                <tr>
                                    <td colspan="2" class="tdSectionHdr"></td>
                                </tr>
                            </thead>
                            
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tbody>
                            <tr>
                                <td id="tdStatusAct" runat="server"><asp:LinkButton ID="lnkbtnStatus" runat="server" CssClass="btnStatus" Text='<%# DataBinder.Eval(Container.DataItem, "ORDERSTATUS_NAME") + ":"  %>' ToolTip='<%# DataBinder.Eval(Container.DataItem, "ORDERSTATUS_NAME") %>' CommandName="cmdStatus" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ORDERSTATUS_ID") %>' ValidationGroup="vgOrderStatus"></asp:LinkButton></td>
                                <td id="tdStatusDetails" runat="server" class="tdStatus">
                                    <div class="divLeft"><asp:Literal ID="litStatus" runat="server"></asp:Literal></div>
                                    <asp:Panel ID="pnlStatusAct" runat="server" CssClass="divStatusActRight" Visible="false">
                                        <asp:HyperLink ID="hypUndo" runat="server" CssClass="thickbox" ToolTip="Undo Status"></asp:HyperLink>
                                    </asp:Panel>
                                    <asp:HiddenField ID="hdnStatus" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ORDERSTATUS_ORDER") %>' />
                                </td>
                            </tr>
                            <tr id="trPaymentAmount" runat="server" class="trItalic" visible="false">
                                <td id="tdPaymentAmount" runat="server" class="">Payment Amount:</td>
                                <td id="tdPaymentAmountDetails" runat="server" class="tdStatus">
                                    <asp:Panel ID="pnlPayAmountTxt" runat="server" Visible="false">
                                        <asp:Literal ID="litPayAmount" runat="server"></asp:Literal>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlPayAmount" runat="server" CssClass="divPayAmount" Visible="false">
                                        <asp:TextBox ID="txtPayAmount" runat="server" MaxLength="1000" CssClass="text" ValidationGroup="vgOrderStatus"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPayAmount" runat="server" ErrorMessage="<br />Please enter payment amount." ControlToValidate="txtPayAmount" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgOrderStatus"></asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="cvPayAmount" runat="server" ControlToValidate="txtPayAmount" CssClass="errmsg" Display="Dynamic" ClientValidationFunction="validateCurrency_client" ValidationGroup="vgOrderStatus"></asp:CustomValidator>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trPaymentRemarks" runat="server" visible="false" class="trItalic">
                                <td id="tdPaymentRemarks" runat="server" class="">Payment Remarks:</td>
                                <td id="tdPaymentRemarksDetails" runat="server" class="tdStatus">
                                    <asp:Panel ID="pnlPayRemarksText" runat="server" Visible="false">
                                        <asp:Literal ID="litPayRemarks" runat="server"></asp:Literal>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlPayRemarks" runat="server" CssClass="divPayRemarks" Visible="false">
                                        <asp:TextBox ID="txtPayRemarks" runat="server" CssClass="text" MaxLength="1000" ValidationGroup="vgOrderStatus"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPayRemarks" runat="server" ErrorMessage="<br />Please enter payment remarks." ControlToValidate="txtPayRemarks" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgOrderStatus"></asp:RequiredFieldValidator>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trPaymentAction" runat="server" visible="false" class="trPaymentAction">
                                <td class="tdStatusLabelActive"></td>
                                <td class="tdStatusActive"><asp:LinkButton ID="lnkbtnConfirm" runat="server" Text="Confirm" ToolTip="Confirm" ValidationGroup="vgOrderStatus" CssClass="btnConfirm lnkbtnConfirm" CommandName="cmdStatus" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ORDERSTATUS_ID") %>'></asp:LinkButton></td>
                            </tr>
                            <tr id="trPaymentReject" runat="server" visible="false" class="trItalic">
                                <td id="tdPaymentRejectRemarks" runat="server" class="">Reject Remarks:</td>
                                <td id="tdPaymentRejectRemarksDetails" runat="server" class="tdStatus">
                                    <asp:Panel ID="pnlPayRejectRemarksText" runat="server" Visible="false">
                                        <asp:Literal ID="litPayRejectRemarks" runat="server"></asp:Literal>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlPayRejectRemarks" runat="server" CssClass="divPayRejectRemarks" Visible="false">
                                        <asp:TextBox ID="txtPayRejectRemarks" runat="server" CssClass="text" MaxLength="1000" Rows="2" TextMode="MultiLine" ValidationGroup="vgPayReject"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPayRejectRemarks" runat="server" ErrorMessage="<br />Please enter reject remarks." ValidationGroup="vgPayReject" CssClass="errmsg" Display="Dynamic" ControlToValidate="txtPayRejectRemarks"></asp:RequiredFieldValidator>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trPaymentRejectAction" runat="server" visible="false">
                                <td class="tdStatusLabelActive"></td>
                                <td class="tdStatusActive"><asp:LinkButton ID="lnkbtnReject" runat="server" Text="Reject" ToolTip="Reject" ValidationGroup="vgPayReject" CssClass="btnConfirm" CommandName="cmdReject"></asp:LinkButton></td>
                            </tr>
                            <tr id="trDeliveryCourier" runat="server" visible="false" class="trItalic">
                                <td id="tdDeliveryCourier" runat="server" class="">Shipping Company:</td>
                                <td id="tdDeliveryCourierDetails" runat="server" class="tdStatus">
                                    <asp:Panel ID="pnlDeliveryCourierText" runat="server" Visible="false">
                                        <asp:Literal ID="litDeliveryCourier" runat="server"></asp:Literal>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlDeliveryCourier" runat="server" CssClass="divDeliveryCourier" Visible="false">
                                        <asp:TextBox ID="txtDeliveryCourier" runat="server" CssClass="text" ValidationGroup="vgOrderStatus"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvDeliveryCourier" runat="server" ErrorMessage="<br />Please enter delivery courier." ControlToValidate="txtDeliveryCourier" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgOrderStatus"></asp:RequiredFieldValidator>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlDeliveryCourierCompany" runat="server" CssClass="divDeliveryCourierCompany" Visible="false">
                                        <asp:DropDownList ID="ddlDeliveryCourierCompany" runat="server" CssClass="ddl"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvDeliveryCourierCompany" runat="server" ErrorMessage="<br />Please select one shipping company." ControlToValidate="ddlDeliveryCourierCompany" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgOrderStatus"></asp:RequiredFieldValidator>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trDeliveryConsignment" runat="server" visible="false" class="trItalic">
                                <td id="tdDeliveryConsignment" runat="server" class="">Consignment No.:</td>
                                <td id="tdDeliveryConsignmentDetails" runat="server" class="tdStatus">
                                    <asp:Panel ID="pnlDeliveryConsignmentText" runat="server" Visible="false">
                                        <asp:Literal ID="litDeliveryConsignment" runat="server"></asp:Literal>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlDeliveryConsignment" runat="server" CssClass="divDeliveryConsignment" Visible="false">
                                        <asp:TextBox ID="txtDeliveryConsignment" runat="server" CssClass="text" ValidationGroup="vgOrderStatus"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvDeliveryConsignment" runat="server" ErrorMessage="<br />Please enter consignment no.." ControlToValidate="txtDeliveryConsignment" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgOrderStatus"></asp:RequiredFieldValidator>
                                    </asp:Panel>
                                </td>
                            </tr>
                         </tbody>
                    </ItemTemplate>
                    <SeparatorTemplate><%--<tr><td colspan="2" class="tdSpacer10"></td></tr>--%></SeparatorTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:Panel ID="pnlAction" runat="server" CssClass="">
                    <asp:HyperLink ID="hypCancel" runat="server" Text="Cancel Order" ToolTip="Cancel Order" CssClass="thickbox btn btnCancel"></asp:HyperLink>
                    <asp:HyperLink ID="hypDelete" runat="server" Text="Delete Order" ToolTip="Delete Order" CssClass="thickbox btn btnCancel" Visible="false"></asp:HyperLink>
                </asp:Panel>
            </asp:Panel>
        <asp:Panel ID="pnlPayment" runat="server" CssClass="divPayment">
            <uc:AdmOrderPayment ID="ucAdmOrderPayment" runat="server" />
        </asp:Panel>
    </div>
    
    <asp:Panel ID="pnlBottom" runat="server" CssClass="divBottomItems">
        <div class="divItemsHeader">Items</div>
        <asp:Panel ID="pnlItems" runat="server">
            <uc:AdmOrderItems ID="ucAdmOrderItems" runat="server" />
        </asp:Panel>
    </asp:Panel>
</asp:Panel>
