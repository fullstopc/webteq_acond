﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Collections;
using System.Net;
using System.IO;
using System.Collections.Specialized;

public partial class ctrl_ucUsr : System.Web.UI.UserControl
{
    #region "Property"
    protected Dictionary<string, object> data;
    #endregion

    #region "Properties Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    #endregion
    
    public void fill()
    {

        int memberID = Convert.ToInt32(Session[clsKey.SESSION_MEMBER_ID]);
        clsMember member = new clsMember();
        List<clsProperty> properties = new List<clsProperty>();

        if (member.extractByID(memberID))
        {
            litMemberName.Text = member.name;
            properties = member.getProperties();
        }

        data = new Dictionary<string, object>();
        data["properties"] = properties;
        data["serviceType"] = new clsConfigServiceType().getDataTable().AsEnumerable().Select(x => new {
            name = x["type_name"].ToString(),
            ID = x["type_id"].ToInteger()
        });
        data["airconds"] = new clsConfigPropertyTypeAircond().getDataTable().AsEnumerable()
                                                            .Select(x => new
                                                            {
                                                                name = x["aircond_name"].ToString(),
                                                                typeID = x["type_id"].ToInteger(),
                                                                ID = x["aircond_id"].ToInteger()
                                                            });
    }

    protected void lnkbtnSubmit_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            var aircondIDs = hdnAircondIDs.Value.Split(',');

            // get property info
            var propertyID = hdnPropertyID.Value.ToInteger();
            var property = new clsProperty();
            var propertyUnitno = "";
            var propertyArea = 0;
            var propertyAreaName = "";
            var propertyType = 0;
            var propertyTypeName = "";
            if (property.extractByID(propertyID))
            {
                propertyUnitno = property.unitno;
                propertyArea = property.area;
                propertyType = property.type;

                var area = new clsConfigPropertyArea();
                if (area.extractByID(property.area))
                {
                    propertyAreaName = area.name;
                }

                var type = new clsConfigPropertyType();
                if (type.extractByID(property.type))
                {
                    propertyTypeName = type.name;
                }
            }

            // get service type info
            var serviceTypeID = hdnServiceTypeID.Value.ToInteger();
            var serviceType = new clsConfigServiceType();
            var serviceTypeDuration = 0;
            decimal serviceTypeCharge = 0;
            if (serviceType.extractByID(serviceTypeID))
            {
                serviceTypeDuration = serviceType.duration.ToInteger();
                serviceTypeCharge = serviceType.charge.ToDecimal();
            }

            // reservation data
            Reservation reservation = new Reservation();
            reservation.config.code = System.Guid.NewGuid().ToString();
            reservation.config.propertyUnitno = propertyUnitno;
            reservation.config.memID = Convert.ToInt32(Session[clsKey.SESSION_MEMBER_ID]);
            reservation.config.propertyArea = propertyArea;
            reservation.config.propertyAreaName = propertyAreaName;
            reservation.config.propertyType = propertyType;
            reservation.config.propertyTypeName = propertyTypeName;
            reservation.step = 2;

            if(serviceTypeID == clsKey.CONFIG__SERVICE_TYPE__CLEAN)
            {
                reservation.config.serviceDurationHour = serviceTypeDuration;
                reservation.config.serviceChargeAmount = serviceTypeCharge;
                reservation.config.serviceItemQty = 0;
            }
            else if(serviceTypeID == clsKey.CONFIG__SERVICE_TYPE__AIRCOND)
            {
                var airconds = new clsConfigPropertyTypeAircond().getDataTable().AsEnumerable().Select(x => new
                {
                    ID = x["aircond_id"].ToInteger(),
                    name = x["aircond_name"].ToString(),
                    charge = x["aircond_charge"].ToDecimal()
                });
                reservation.config.serviceDurationHour = aircondIDs.Length * serviceTypeDuration;
                reservation.config.serviceItemQty = aircondIDs.Length;
                reservation.config.airconds = aircondIDs.Select(x => new clsReservationAircond()
                {
                    aircondID = x.ToInteger(),
                    aircondName = airconds.Where(k => k.ID == x.ToInteger()).First().name,
                    aircondCharge = airconds.Where(k => k.ID == x.ToInteger()).First().charge
                }).ToList();
                reservation.config.serviceChargeAmount = reservation.config.airconds.Select(x => x.aircondCharge).Sum();
            }

            Session[clsKey.SESSION_RESERVATION] = reservation;

            NameValueCollection parameters = HttpUtility.ParseQueryString(Request.QueryString.ToString());
            parameters["step"] = "2";
            string url = Request.Url.AbsolutePath + "?" + parameters.ToString();
            Response.Redirect(url);
        }
    }
}
