﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class ctrl_ucUsrFooter : System.Web.UI.UserControl
{
    public int pageId { get; set; }

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion


    #region "Methods"
    public void fill()
    {
        ucUsrCMSBtmBanner.pageId = pageId;
        ucUsrCMSBtmBanner.fill();

        fillFacebook();
        fillBtmmenu();
    }

    private void fillBtmmenu()
    {
        ucUsrBtmMenu.pgid = pageId;
        ucUsrBtmMenu.fill();
    }

    private void fillFacebook()
    {
        clsConfig config = new clsConfig();
        string facebookUrl = config.extractItemByNameGroup(clsConfig.CONSTNAMEFBPAGEURL, clsConfig.CONSTGROUPFB, 1) ? config.value : "";

        if (!string.IsNullOrEmpty(facebookUrl))
        {
            pnlFacebookPage.Visible = true;
            hypFacebookPage.NavigateUrl = facebookUrl;
            hypFacebookPage.Target = "_blank";
            hypFacebookPage.ToolTip = config.extractItemByNameGroup(clsConfig.CONSTNAMEFBLIKETITLE, clsConfig.CONSTGROUPFB, 1) ? config.value : "";
        }

        ucUsrFacebook.entityTitle = config.extractItemByNameGroup(clsConfig.CONSTNAMEFBLIKETITLE, clsConfig.CONSTGROUPFB, 1) ? config.value : "";
        ucUsrFacebook.entityType = config.extractItemByNameGroup(clsConfig.CONSTNAMEFBLIKETYPE, clsConfig.CONSTGROUPFB, 1) ? config.value : "";
        ucUsrFacebook.urlToLike = config.extractItemByNameGroup(clsConfig.CONSTNAMEFBLIKEURL, clsConfig.CONSTGROUPFB, 1) ? config.value : "";
        ucUsrFacebook.imageFile = config.extractItemByNameGroup(clsConfig.CONSTNAMEFBLIKEIMAGE, clsConfig.CONSTGROUPFB, 1) ? config.value : "";
        ucUsrFacebook.siteName = config.extractItemByNameGroup(clsConfig.CONSTNAMEFBLIKESITENAME, clsConfig.CONSTGROUPFB, 1) ? config.value : "";
        ucUsrFacebook.entityDesc = config.extractItemByNameGroup(clsConfig.CONSTNAMEFBLIKEDESC, clsConfig.CONSTGROUPFB, 1) ? config.value : "";

        pnlFacebookLike.Visible = !string.IsNullOrEmpty(ucUsrFacebook.urlToLike);
        pnlFbPageContainer.Visible = !string.IsNullOrEmpty(facebookUrl) || !string.IsNullOrEmpty(ucUsrFacebook.urlToLike);
        ucUsrFacebook.fill();
    }
    #endregion
}
