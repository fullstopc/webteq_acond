﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrLoading.ascx.cs" Inherits="ctrl_ucUsrLoading" %>

<asp:Panel ID="pnlLoading" runat="server" class="divLoading">
    <asp:Image ID="imgLoading" runat="server" /><br />
    <asp:Label ID="lblLoading" runat="server" meta:ResourceKey="lblLoading"></asp:Label>
</asp:Panel>  
