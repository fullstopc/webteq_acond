﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;

public partial class ctrl_ucUsrCMS : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 2;
    protected int _pageId = 0;
    protected Boolean _firstCtrl = true;

    protected int _cmsHeight = 0;
    protected int _cmsWidth = 0;
    protected string _cmsCSS = "";
    protected string _stylesSetFile = "";
    protected string _stylesSetName = "";
    protected string _toolBarName = "";

    protected string _lang = "";
    protected Boolean _viewMode = true;
    protected Boolean _allowAdd = true;
    protected int _totalPages = 0;
    protected int _limitPages = 0;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int pageId
    {
        get
        {
            if (ViewState["PAGEID"] == null)
            {
                return _pageId;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGEID"]);
            }
        }
        set { ViewState["PAGEID"] = value; }
    }

    public Boolean firstCtrl
    {
        get
        {
            if (ViewState["FIRSTCTRL"] == null)
            {
                return _firstCtrl;
            }
            else
            {
                return Convert.ToBoolean(ViewState["FIRSTCTRL"]);
            }
        }
        set { ViewState["FIRSTCTRL"] = value; }
    }

    public int cmsHeight
    {
        get
        {
            if (ViewState["CMSHEIGHT"] == null)
            {
                return _cmsHeight;
            }
            else
            {
                return Convert.ToInt16(ViewState["CMSHEIGHT"]);
            }
        }
        set { ViewState["CMSHEIGHT"] = value; }
    }

    public int cmsWidth
    {
        get
        {
            if (ViewState["CMSWIDTH"] == null)
            {
                return _cmsWidth;
            }
            else
            {
                return Convert.ToInt16(ViewState["CMSWIDTH"]);
            }
        }
        set { ViewState["CMSWIDTH"] = value; }
    }

    public string cmsCSS
    {
        get
        {
            if (ViewState["CMSCSS"] == null)
            {
                return _cmsCSS;
            }
            else
            {
                return ViewState["CMSCSS"].ToString();
            }
        }
        set { ViewState["CMSCSS"] = value; }
    }

    public string styleSetFile
    {
        get
        {
            if (ViewState["STYLESETFILE"] == null)
            {
                return _stylesSetFile;
            }
            else
            {
                return ViewState["STYLESETFILE"].ToString();
            }
        }
        set { ViewState["STYLESETFILE"] = value; }
    }

    public string styleSetName
    {
        get
        {
            if (ViewState["STYLESETNAME"] == null)
            {
                return _stylesSetName;
            }
            else
            {
                return ViewState["STYLESETNAME"].ToString();
            }
        }
        set { ViewState["STYLESETNAME"] = value; }
    }

    public string toolBarName
    {
        get
        {
            if (ViewState["TOOLBARNAME"] == null)
            {
                return _toolBarName;
            }
            else
            {
                return ViewState["TOOLBARNAME"].ToString();
            }
        }
        set { ViewState["TOOLBARNAME"] = value; }
    }

    public string lang
    {
        get
        {
            if (ViewState["LANG"] == null)
            {
                return _lang;
            }
            else
            {
                return ViewState["LANG"].ToString();
            }
        }
        set { ViewState["LANG"] = value; }
    }

    protected Boolean viewMode
    {
        get
        {
            if (ViewState["VIEWMODE"] == null)
            {
                return _viewMode;
            }
            else
            {
                return Convert.ToBoolean(ViewState["VIEWMODE"]);
            }
        }
        set { ViewState["VIEWMODE"] = value; }
    }

    public Boolean allowAdd
    {
        get
        {
            if (ViewState["ALLOWADD"] == null)
            {
                return _allowAdd;
            }
            else
            {
                return Convert.ToBoolean(ViewState["ALLOWADD"]);
            }
        }
        set { ViewState["ALLOWADD"] = value; }
    }

    protected int totalPages
    {
        get
        {
            if (ViewState["TOTALPAGES"] == null)
            {
                return _totalPages;
            }
            else
            {
                return Convert.ToInt16(ViewState["TOTALPAGES"]);
            }
        }
        set { ViewState["TOTALPAGES"] = value; }
    }

    protected int limitPages
    {
        get
        {
            if (ViewState["LIMITPAGES"] == null)
            {
                return _limitPages;
            }
            else
            {
                return Convert.ToInt16(ViewState["LIMITPAGES"]);
            }
        }
        set { ViewState["LIMITPAGES"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.LoadComplete += new EventHandler(Page_LoadComplete);

        if (firstCtrl)
        {
            Literal litCkEditorJS = new Literal();
            litCkEditorJS.Text = "<script type='text/javascript' src='" + ConfigurationManager.AppSettings["jsBase"] + "ckeditor_3.5/ckeditor.js'></script>";
            this.Page.Header.Controls.Add(litCkEditorJS);

            Literal litCSS = new Literal();
            litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "cms.css' rel='Stylesheet' type='text/css' />";
            this.Page.Header.Controls.Add(litCSS);
        }

        if (!IsPostBack)
        {
            setPageProperties();
            fillForm();
        }

        replaceTag();

        if (!viewMode) { loadCkEditor(); }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        clsConfig config = new clsConfig();
        string strDefaultKeyword = config.defaultKeyword;
        string strDefaultDesc = config.defaultDescription;
        string strDefaultTitle = config.defaultTitle;

        string strKeyword = "";
        string strDesc = "";
        string strTitle = "";
        Boolean boolDefaultKeyword = true;
        Boolean boolDefaultDesc = true;
        Boolean boolShowTitle = false;

        clsPage page = new clsPage();
        if (page.extractPageById(pageId, 0))
        {
            strKeyword = page.keyword;
            strDesc = page.desc;
            strTitle = page.title;

            boolShowTitle = page.showPageTitle > 0 ? true : false;
            boolDefaultKeyword = page.defaultKeyword > 0 ? true : false;
            boolDefaultDesc = page.defaultDesc > 0 ? true : false;
        }

        //clsConfig config = new clsConfig();

        //strTitle = strTitle.Replace(config.separator, " ");
        if (boolShowTitle)
        {
            if (Session["ADMID"] != null && Session["ADMID"] != "")
            {
                Page.Title += clsMis.formatPageTitle((!string.IsNullOrEmpty(strTitle) ? strTitle : strDefaultTitle), true);
            }
            else
            {
                Page.Title += clsMis.formatPageTitle((!string.IsNullOrEmpty(strTitle) ? strTitle : strDefaultTitle), false);
            }
        }
        else
        {
            if (Session["ADMID"] != null && Session["ADMID"] != "")
            {
                Page.Title += clsMis.formatPageTitle(strDefaultTitle, true);
            }
            else
            {
                Page.Title += clsMis.formatPageTitle(strDefaultTitle, false);
            }
        }

        Literal litKeyword = new Literal();
        string strAppliedKeyword = "";
        strAppliedKeyword = boolDefaultKeyword ? strDefaultKeyword : strKeyword;

        if (!string.IsNullOrEmpty(strAppliedKeyword))
        {
            litKeyword.Text = "<meta name='keywords' content='" + strAppliedKeyword + "' />";
            this.Page.Header.Controls.Add(litKeyword);
        }

        Literal litDesc = new Literal();
        string strAppliedDesc = "";
        strAppliedDesc = boolDefaultDesc ? strDefaultDesc : strDesc;

        if (!string.IsNullOrEmpty(strAppliedDesc))
        {
            litDesc.Text = "<meta name='description' content='" + strAppliedDesc + "' />";
            this.Page.Header.Controls.Add(litDesc);
        }
        
        ucCmnSiblingPages.pageId = pageId;
        ucCmnSiblingPages.fill();
    }

    protected void lnkbtnAddNew_OnClick(object sender, EventArgs e)
    {
        pnlEditModeAck.Visible = false;

        txtContent.Text = "";

        viewMode = false;
        mode = 1;

        setPageProperties();
        fillForm();
    }

    protected void lnkbtnUpdate_OnClick(object sender, EventArgs e)
    {
        pnlEditModeAck.Visible = false;

        viewMode = false;
        mode = 2;

        setPageProperties();
        fillForm();
    }

    protected void lnkbtnSave_OnClick(object sender, EventArgs e)
    {
        if (Session["ADMID"] != null && Session["ADMID"] != "")
        {
            if (string.IsNullOrEmpty(checkRequiredData()))
            {
                string strName = ucCmnPageDetailsEdit.pageName;
                string strDisplayName = ucCmnPageDetailsEdit.pageDisplayName;
                string strTitle = ucCmnPageDetailsEdit.pageTitle;
                int intParent = ucCmnPageDetailsEdit.parent;
                string strTemplate = ucCmnPageDetailsEdit.template;
                int intTemplate = ucCmnPageDetailsEdit.templateParent ? 1 : 0;
                int intShowPageTitle = ucCmnPageDetailsEdit.showPageTitle ? 1 : 0;
                int intShowInMenu = ucCmnPageDetailsEdit.showInMenu ? 1 : 0;
                int intShowTop = intShowInMenu > 0 ? (ucCmnPageDetailsEdit.showTop ? 1 : 0) : 0;
                int intShowBottom = intShowInMenu > 0 ? (ucCmnPageDetailsEdit.showBottom ? 1 : 0) : 0;
                int intShowInBreadcrumb = ucCmnPageDetailsEdit.showInBreadcrumb ? 1 : 0;
                int intUserEditable = ucCmnPageDetailsEdit.userEditable ? 1 : 0;
                int intEnableLink = ucCmnPageDetailsEdit.enableLink ? 1 : 0;
                string strCSSClass = ucCmnPageDetailsEdit.cssClass;
                string strKeyword = ucCmnPageDetailsEdit.keyword;
                int intKeyword = ucCmnPageDetailsEdit.defaultKeyword ? 1 : 0;
                string strDesc = ucCmnPageDetailsEdit.desc;
                int intDesc = ucCmnPageDetailsEdit.defaultDesc ? 1 : 0;
                int intActive = ucCmnPageDetailsEdit.active ? 1 : 0;
                int intRedirect = ucCmnPageDetailsEdit.redirect;
                string strRedirectLink = ucCmnPageDetailsEdit.externalLink;
                string strTarget = ucCmnPageDetailsEdit.target;

                string strContent = txtContent.Text.Trim();

                clsPage page = new clsPage();
                int intRecordAffected = 0;

                if (mode == 1)
                {
                    //add cms
                    Boolean boolValid = limitPages > 0 ? (totalPages < limitPages ? true : false) : true;

                    if (boolValid)
                    {
                        if (!string.IsNullOrEmpty(lang)) { intRecordAffected = page.addPage(strName, strDisplayName, strTitle, strTemplate, intTemplate, intParent, lang, intShowPageTitle, intShowInMenu, intShowTop, intShowBottom, intShowInBreadcrumb, intUserEditable, intEnableLink, strCSSClass, strKeyword, intKeyword, strDesc, intDesc, intRedirect, strRedirectLink, strTarget, intActive, strContent, Convert.ToInt16(Session["ADMID"])); }
                        else { intRecordAffected = page.addPage2(strName, strDisplayName, strTitle, strTemplate, intTemplate, intParent, intShowPageTitle, intShowInMenu, intShowTop, intShowBottom, intShowInBreadcrumb, intUserEditable, intEnableLink, strCSSClass, strKeyword, intKeyword, strDesc, intDesc, intRedirect, strRedirectLink, strTarget, intActive, strContent, Convert.ToInt16(Session["ADMID"])); }
                        
                        if (intRecordAffected == 1)
                        {
                            Session["EDITEDPAGEID"] = page.id;
                            clsMis.resetListing();

                            page.extractPageById(page.id, 0);

                            string strTemplateUse = page.parent > 0 ? (page.templateParent > 0 ? page.templateParentName : page.template) : page.template;

                            if (!string.IsNullOrEmpty(lang)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateUse + "?pgid=" + page.id + "&lang=" + lang); }
                            else { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateUse + "?pgid=" + page.id); }
                        }
                        else
                        {
                            litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "userErrAddContent.Text") + "</div>";

                            pnlAck.Visible = true;
                        }
                    }
                    else
                    {
                        litAck.Text = "<div class=\"errmsg\">limit reached</div>";

                        pnlAck.Visible = true;
                        pnlButtonAck.Visible = true;
                        pnlContentSection.Visible = false;
                    }
                }
                else if (mode == 2)
                {
                    Boolean boolSame = true;

                    page.extractPageById(pageId, 1);

                    if (!string.IsNullOrEmpty(lang)) { boolSame = page.isExactSameDataSet(pageId, strName, strDisplayName, strTitle, strContent, intShowPageTitle, intShowInMenu, intShowTop, intShowBottom, intShowInBreadcrumb, intUserEditable, intEnableLink, strCSSClass, lang, strTemplate, intTemplate, intParent, strKeyword, intKeyword, strDesc, intDesc, intRedirect, strRedirectLink, strTarget, intActive); }
                    else { boolSame = page.isExactSameDataSet(pageId, strName, strDisplayName, strTitle, strContent, intShowPageTitle, intShowInMenu, intShowTop, intShowBottom, intShowInBreadcrumb, intUserEditable, intEnableLink, strCSSClass, strTemplate, intTemplate, intParent, strKeyword, intKeyword, strDesc, intDesc, intRedirect, strRedirectLink, strTarget, intActive); }

                    if (!boolSame)
                    {
                        //edit cms content
                        if (Session["ADMID"] != null && Session["ADMID"] != "")
                        {
                            if (!string.IsNullOrEmpty(lang)) { intRecordAffected = page.updatePageById(pageId, strName, strDisplayName, strTitle, intShowPageTitle, intShowInMenu, intShowTop, intShowBottom, intShowInBreadcrumb, intUserEditable, intEnableLink, strCSSClass, lang, strTemplate, intTemplate, intParent, strKeyword, intKeyword, strDesc, intDesc, intRedirect, strRedirectLink, strTarget, intActive, strContent, Convert.ToInt16(Session["ADMID"])); }
                            else { intRecordAffected = page.updatePageById(pageId, strName, strDisplayName, strTitle, intShowPageTitle, intShowInMenu, intShowTop, intShowBottom, intShowInBreadcrumb, intUserEditable, intEnableLink, strCSSClass, strTemplate, intTemplate, intParent, strKeyword, intKeyword, strDesc, intDesc, intRedirect, strRedirectLink, strTarget, intActive, strContent, Convert.ToInt16(Session["ADMID"])); }

                            if (intRecordAffected == 1)
                            {
                                Session["EDITEDPAGEID"] = pageId;
                                clsMis.resetListing();

                                page.extractPageById(pageId, 0);

                                string strTemplateUse = page.parent > 0 ? (page.templateParent > 0 ? page.templateParentName : page.template) : page.template;

                                if (!string.IsNullOrEmpty(lang)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateUse + "?pgid=" + page.id + "&lang=" + lang); }
                                else { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateUse + "?pgid=" + page.id); }
                            }
                            else
                            {
                                pnlAck.Visible = true;
                                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "userErrUpdateContent.Text") + "</div>";
                            }
                        }
                    }
                    else
                    {
                        //no change
                        Session["EDITEDPAGEID"] = pageId;
                        Session["NOCHANGE"] = 1;

                        Response.Redirect(Request.Url.ToString());
                    }
                }
            }
            else
            {
                litEditModeAck.Text = "<div class=\"errmsg\">" + checkRequiredData() + "</div>";

                pnlEditModeAck.Visible = true;
            }
        }
        else
        {
            //unauthorised access   
            Session["UNAUTHORIZED"] = 1;
            Response.Redirect(Request.Url.ToString());
        }
    }

    protected void lnkbtnCancel_OnClick(object sender, EventArgs e)
    {
        viewMode = true;
        setPageProperties();

        if (pageId > 0)
        {
            mode = 2;
            fillForm();
        }
    }

    protected void lnkbtnAckCancel_OnClick(object sender, EventArgs e)
    {
        if (Application["PAGELIST"] == null)
        {
            clsPage page = new clsPage();
            Application["PAGELIST"] = page.getPageList(1);
        }

        DataSet ds = (DataSet)Application["PAGELIST"];
        DataRow[] foundRow;

        // get first page
        if (pageId > 0)
        {
            clsPage page = new clsPage();
            if (page.extractPageById(pageId, 1))
            {
                int intParent = page.parent;

                // get first page under same parent
                if (!string.IsNullOrEmpty(lang)) { foundRow = ds.Tables[0].Select("PAGE_PARENT = " + intParent + " AND PAGE_LANG = '" + lang + "'", "PAGE_ORDER ASC"); }
                else { foundRow = ds.Tables[0].Select("PAGE_PARENT = " + intParent, "PAGE_ORDER ASC"); }

                if (foundRow.Length > 0)
                {
                    int intTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                    string strTemplate = intParent > 0 ? (intTemplateParent > 0 ? foundRow[0]["V_TEMPLATEPARENT"].ToString() : foundRow[0]["PAGE_TEMPLATE"].ToString()) : foundRow[0]["PAGE_TEMPLATE"].ToString();

                    if (!string.IsNullOrEmpty(lang)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString() + "&lang=" + lang); }
                    else { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString()); }
                }
                else
                {
                    // get first page under same parent, ignore language
                    foundRow = ds.Tables[0].Select("PAGE_PARENT = " + intParent, "PAGE_ORDER ASC");

                    if (foundRow.Length > 0)
                    {
                        int intTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                        string strTemplate = intParent > 0 ? (intTemplateParent > 0 ? foundRow[0]["V_TEMPLATEPARENT"].ToString() : foundRow[0]["PAGE_TEMPLATE"].ToString()) : foundRow[0]["PAGE_TEMPLATE"].ToString();

                        if (!string.IsNullOrEmpty(lang)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString() + "&lang=" + foundRow[0]["PAGE_LANG"].ToString()); }
                        else { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString()); }
                    }
                    else
                    {
                        // get the very first page, ignore parent and language
                        foundRow = ds.Tables[0].Select("", "PAGE_PARENT ASC, PAGE_ORDER ASC");

                        if (foundRow.Length > 0)
                        {
                            intParent = Convert.ToInt16(foundRow[0]["PAGE_PARENT"]);
                            int intTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                            string strTemplate = intParent > 0 ? (intTemplateParent > 0 ? foundRow[0]["V_TEMPLATEPARENT"].ToString() : foundRow[0]["PAGE_TEMPLATE"].ToString()) : foundRow[0]["PAGE_TEMPLATE"].ToString();

                            if (!string.IsNullOrEmpty(lang)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString() + "&lang=" + foundRow[0]["PAGE_LANG"].ToString()); }
                            else { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString()); }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(lang)) { Response.Redirect(currentPageName + "?lang=" + lang); }
                            else { Response.Redirect(currentPageName); }
                        }
                    }
                }
            }
            else
            {
                // get first page
                if (!string.IsNullOrEmpty(lang)) { foundRow = ds.Tables[0].Select("PAGE_LANG = '" + lang + "'", "PAGE_PARENT ASC, PAGE_ORDER ASC"); }
                else { foundRow = ds.Tables[0].Select("", "PAGE_PARENT ASC, PAGE_ORDER ASC"); }

                if (foundRow.Length > 0)
                {
                    int intParent = Convert.ToInt16(foundRow[0]["PAGE_PARENT"]);
                    int intTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                    string strTemplate = intParent > 0 ? (intTemplateParent > 0 ? foundRow[0]["V_TEMPLATEPARENT"].ToString() : foundRow[0]["PAGE_TEMPLATE"].ToString()) : foundRow[0]["PAGE_TEMPLATE"].ToString();

                    if (!string.IsNullOrEmpty(lang)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString() + "&lang=" + foundRow[0]["PAGE_LANG"].ToString()); }
                    else { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString()); }
                }
                else
                {
                    // get the very first page, ignore language
                    foundRow = ds.Tables[0].Select("", "PAGE_PARENT ASC, PAGE_ORDER ASC");

                    if (foundRow.Length > 0)
                    {
                        int intParent = Convert.ToInt16(foundRow[0]["PAGE_PARENT"]);
                        int intTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                        string strTemplate = intParent > 0 ? (intTemplateParent > 0 ? foundRow[0]["V_TEMPLATEPARENT"].ToString() : foundRow[0]["PAGE_TEMPLATE"].ToString()) : foundRow[0]["PAGE_TEMPLATE"].ToString();

                        if (!string.IsNullOrEmpty(lang)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString() + "&lang=" + foundRow[0]["PAGE_LANG"].ToString()); }
                        else { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString()); }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(lang)) { Response.Redirect(currentPageName + "?lang=" + lang); }
                        else { Response.Redirect(currentPageName); }
                    }
                }
            }
        }
        else
        {
            // get first page
            if (!string.IsNullOrEmpty(lang)) { foundRow = ds.Tables[0].Select("PAGE_LANG = '" + lang + "'", "PAGE_PARENT ASC, PAGE_ORDER ASC"); }
            else { foundRow = ds.Tables[0].Select("", "PAGE_PARENT ASC, PAGE_ORDER ASC"); }

            if (foundRow.Length > 0)
            {
                int intParent = Convert.ToInt16(foundRow[0]["PAGE_PARENT"]);
                int intTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                string strTemplate = intParent > 0 ? (intTemplateParent > 0 ? foundRow[0]["V_TEMPLATEPARENT"].ToString() : foundRow[0]["PAGE_TEMPLATE"].ToString()) : foundRow[0]["PAGE_TEMPLATE"].ToString();

                if (!string.IsNullOrEmpty(lang)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString() + "&lang=" + foundRow[0]["PAGE_LANG"].ToString()); }
                else { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString()); }
            }
            else
            {
                // get the very first page, ignore language
                foundRow = ds.Tables[0].Select("", "PAGE_PARENT ASC, PAGE_ORDER ASC");

                if (foundRow.Length > 0)
                {
                    int intParent = Convert.ToInt16(foundRow[0]["PAGE_PARENT"]);
                    int intTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                    string strTemplate = intParent > 0 ? (intTemplateParent > 0 ? foundRow[0]["V_TEMPLATEPARENT"].ToString() : foundRow[0]["PAGE_TEMPLATE"].ToString()) : foundRow[0]["PAGE_TEMPLATE"].ToString();

                    if (!string.IsNullOrEmpty(lang)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString() + "&lang=" + foundRow[0]["PAGE_LANG"].ToString()); }
                    else { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString()); }
                }
                else
                {
                    if (!string.IsNullOrEmpty(lang)) { Response.Redirect(currentPageName + "?lang=" + lang); }
                    else { Response.Redirect(currentPageName); }
                }
            }
        }
    }
    #endregion


    #region "Methods"
    protected void loadCkEditor()
    {        
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR" + this.ID)))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">" +
                "CKEDITOR.replace('" + txtContent.ClientID + "'," +
                "{";

            if (cmsHeight > 0)
            {
                strJS += "   height:'" + cmsHeight + "',";
            }
            else
            {
                strJS += "   height:'300',";
            }

            if (cmsWidth > 0)
            {
                strJS += "   width:'" + cmsWidth + "',";
            }

            strJS += "   toolbar: '" + toolBarName + "'," +
                "   filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Files'," +
                "   filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Images'," +
                "   filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Flash'" +
                "}" +
                ");";

            if (cmsCSS != "")
            {
                strJS += "CKEDITOR.add;" +
                "CKEDITOR.config.contentsCss = '" + System.Configuration.ConfigurationManager.AppSettings["cssBase"] + cmsCSS + "';";
            }

            if (styleSetName != "" && styleSetFile != "")
            {
                strJS += "CKEDITOR.config.stylesCombo_stylesSet= '" + styleSetName + ":" + System.Configuration.ConfigurationManager.AppSettings["jsBase"] + styleSetFile + "';";
            }

            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR", strJS, false);
        }
    }

    protected void setPageProperties()
    {
        if (Session["ADMID"] != null && Session["ADMID"] != "")
        {
            clsPage page = new clsPage();
            totalPages = page.getTotalPages(0, 0);

            if (Session["ADMPAGELIMIT"] != null && Session["ADMPAGELIMIT"] != "")
            {
                limitPages = Convert.ToInt16(Session["ADMPAGELIMIT"]);
            }

            pnlAdminAction.Visible = true;
            pnlEditorStart.Visible = true;
            
            if(viewMode)
            {
                if (allowAdd)
                {
                    lnkbtnAddNew.Visible = limitPages > 0 ? (totalPages < limitPages ? true : false) : true;
                }
                else { lnkbtnAddNew.Visible = false; }

                ucCmnPageDetailsView.pageId = pageId;
                ucCmnPageDetailsView.fill();

                pnlContent.Visible = true;
                pnlViewMode.Visible = true;
                pnlEditMode.Visible = false;
            }
            else
            {
                if (mode == 2) { ucCmnPageDetailsEdit.pageId = pageId; }
                
                ucCmnPageDetailsEdit.fill();

                pnlContent.Visible = false;
                pnlViewMode.Visible = false;
                pnlEditMode.Visible = true;

                loadCkEditor();
            }

            if (Session["EDITEDPAGEID"] != null)
            {
                pnlAck.Visible = true;

                if (Session["NOCHANGE"] != null)
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "userMsgNoChange.Text") + "</div>";
                }
                else
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "userMsgContentUpdated.Text") + "</div>";
                    clsMis.resetListing();
                }

                Session["EDITEDPAGEID"] = null;
                Session["NOCHANGE"] = null;
            }
            else
            {
                pnlAck.Visible = false;
                litAck.Text = "";
            }
        }
        else
        {
            if (Session["UNAUTHORIZED"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "userErrUnauthorized.Text") + "</div>";
                Session["UNAUTHORIZED"] = null;
            }
            else
            {
                pnlAck.Visible = false;
                litAck.Text = "";
            }

            pnlAdminAction.Visible = false;
            pnlContent.Visible = true;
            pnlEditorStart.Visible = false;
        }
    }

    protected void fillForm()
    {
        clsPage page = new clsPage();

        if (Application["PAGELIST"] == null)
        {
            Application["PAGELIST"] = page.getPageList(1);
        }

        DataSet dsPage = new DataSet();
        dsPage = (DataSet)Application["PAGELIST"];
        DataView dvPage = new DataView(dsPage.Tables[0]);
        DataRow[] foundRow;
        
        if (pageId > 0)
        {
            if (page.extractPageById(pageId, 1))
            {
                if (Session["ADMID"] != null && Session["ADMID"] != "")
                {
                    if (Convert.ToInt16(Session["ADMTYPE"]) == clsAdmin.CONSTUSERADMTYPE && page.userEditable == 0)
                    {
                        pnlAdminAction.Visible = false;
                        pnlContent.Visible = true;
                        pnlEditorStart.Visible = false;
                    }
                }

                string strTemplate = page.parent > 0 ? (page.templateParent > 0 ? page.templateParentName : page.template) : page.template;

                if (page.redirect > 0)
                {
                    if (page.redirect == clsAdmin.CONSTEXTERNALLINK)
                    {
                        if (!string.IsNullOrEmpty(page.redirectLink))
                        {
                            string strRedirectLink = page.redirectLink;

                            if ((!strRedirectLink.StartsWith("http://")) && (!strRedirectLink.StartsWith("https://"))) { strRedirectLink = "http://" + strRedirectLink; }

                            Response.Redirect(strRedirectLink);
                        }
                    }
                    else if (page.extractPageById(page.redirect, 0))
                    {
                        string strTemplateRedirect = page.parent > 0 ? (page.templateParent > 0 ? page.templateParentName : page.template) : page.template;
                        Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplateRedirect + "?pgid=" + page.id + (!string.IsNullOrEmpty(lang) ? "&lang=" + page.lang : ""));
                    }
                }
                else if (string.Compare(Path.GetFileName(Request.PhysicalPath), strTemplate, true) != 0)
                {
                    Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + page.id + (!string.IsNullOrEmpty(lang) ? "&lang=" + page.lang : ""));
                }

                if (mode == 2)
                {
                    string strDefaultKeyword = "";
                    string strDefaultDesc = "";

                    if (page.defaultKeyword > 0)
                    {
                        clsConfig config = new clsConfig();
                        strDefaultKeyword = config.defaultKeyword;
                    }

                    if (page.defaultDesc > 0)
                    {
                        clsConfig config = new clsConfig();
                        strDefaultDesc = config.defaultDescription;
                    }


                    //view
                    ucCmnPageDetailsView.pageId = page.id;
                    ucCmnPageDetailsView.pageName = page.name;
                    ucCmnPageDetailsView.pageDisplayName = page.displayName;
                    ucCmnPageDetailsView.pageTitle = page.title;
                    ucCmnPageDetailsView.showPageTitle = page.showPageTitle > 0 ? true : false;
                    ucCmnPageDetailsView.showInMenu = page.showInMenu > 0 ? true : false;
                    ucCmnPageDetailsView.showTop = page.showTop > 0 ? true : false;
                    ucCmnPageDetailsView.showBottom = page.showBottom > 0 ? true : false;
                    ucCmnPageDetailsView.showInBreadcrumb = page.showInBreadcrumb > 0 ? true : false;
                    ucCmnPageDetailsView.userEditable = page.userEditable > 0 ? true : false;
                    ucCmnPageDetailsView.cssClass = page.cssClass;
                    ucCmnPageDetailsView.keyword = page.defaultKeyword > 0 ? strDefaultKeyword : page.keyword;
                    ucCmnPageDetailsView.desc = page.defaultDesc > 0 ? strDefaultDesc : page.desc;
                    ucCmnPageDetailsView.active = page.active > 0 ? true : false;
                    ucCmnPageDetailsView.template = page.templateParent > 0 ? page.templateParentName : page.template;
                    ucCmnPageDetailsView.parentText = page.parentName;
                    ucCmnPageDetailsView.redirectText = page.redirectName;


                    //edit
                    ucCmnPageDetailsEdit.pageId = page.id;
                    ucCmnPageDetailsEdit.pageName = page.name;
                    ucCmnPageDetailsEdit.pageDisplayName = page.displayName;
                    ucCmnPageDetailsEdit.pageTitle = page.title;
                    ucCmnPageDetailsEdit.showPageTitle = page.showPageTitle > 0 ? true : false;
                    ucCmnPageDetailsEdit.showInMenu = page.showInMenu > 0 ? true : false;
                    ucCmnPageDetailsEdit.showTop = page.showTop > 0 ? true : false;
                    ucCmnPageDetailsEdit.showBottom = page.showBottom > 0 ? true : false;
                    ucCmnPageDetailsEdit.showInBreadcrumb = page.showInBreadcrumb > 0 ? true : false;
                    ucCmnPageDetailsEdit.userEditable = page.userEditable > 0 ? true : false;
                    ucCmnPageDetailsEdit.cssClass = page.cssClass;
                    ucCmnPageDetailsEdit.keyword = page.keyword;
                    ucCmnPageDetailsEdit.defaultKeyword = page.defaultKeyword > 0 ? true : false;
                    ucCmnPageDetailsEdit.desc = page.desc;
                    ucCmnPageDetailsEdit.defaultDesc = page.defaultDesc > 0 ? true : false;
                    ucCmnPageDetailsEdit.active = page.active > 0 ? true : false;
                    ucCmnPageDetailsEdit.templateParent = page.templateParent > 0 ? true : false;
                    ucCmnPageDetailsEdit.externalLink = page.redirectLink;
                    ucCmnPageDetailsEdit.parent = page.parent;

                    if (!string.IsNullOrEmpty(page.template)) { ucCmnPageDetailsEdit.template = page.template; }
                    if (page.redirect > 0) { ucCmnPageDetailsEdit.redirect = page.redirect; }

                    txtContent.Text = page.content;

                    if (page.active == 0 && (Session["ADMID"] == null || Session["ADMID"] == ""))
                    {
                        litContent.Text = "Page content cannot be found.";
                    }
                    else
                    {
                        litContent.Text = page.content;
                    }
                }
                else if (mode == 1)
                {
                    //edit
                    ucCmnPageDetailsEdit.pageId = 0;
                    ucCmnPageDetailsEdit.pageName = "";
                    ucCmnPageDetailsEdit.pageDisplayName = "";
                    ucCmnPageDetailsEdit.pageTitle = "";
                    ucCmnPageDetailsEdit.showPageTitle = true;
                    ucCmnPageDetailsEdit.showInMenu = true;
                    ucCmnPageDetailsEdit.showTop = false;
                    ucCmnPageDetailsEdit.showBottom = false;
                    ucCmnPageDetailsEdit.showInBreadcrumb = true;
                    ucCmnPageDetailsEdit.userEditable = true;
                    ucCmnPageDetailsEdit.cssClass = "";
                    ucCmnPageDetailsEdit.keyword = "";
                    ucCmnPageDetailsEdit.defaultKeyword = true;
                    ucCmnPageDetailsEdit.desc = "";
                    ucCmnPageDetailsEdit.defaultDesc = true;
                    ucCmnPageDetailsEdit.active = true;
                    ucCmnPageDetailsEdit.templateParent = true;
                    ucCmnPageDetailsEdit.template = "";
                    ucCmnPageDetailsEdit.parent = 0;
                    ucCmnPageDetailsEdit.redirect = 0;
                    ucCmnPageDetailsEdit.externalLink = "";
                    ucCmnPageDetailsEdit.redirectTarget = "";

                    txtContent.Text = "";
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(lang)) { foundRow = dsPage.Tables[0].Select("PAGE_PARENT = " + pageId + " AND PAGE_LANG = '" + lang + "'", "PAGE_ORDER ASC"); }
                else { foundRow = dsPage.Tables[0].Select("PAGE_PARENT = " + pageId, "PAGE_ORDER ASC"); }

                if (foundRow.Length > 0)
                {
                    int intTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                    string strTemplate = intTemplateParent > 0 ? foundRow[0]["V_TEMPLATEPARENT"].ToString() : foundRow[0]["PAGE_TEMPLATE"].ToString();

                    if (!string.IsNullOrEmpty(lang)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString() + "&lang=" + lang); }
                    else { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString()); }
                }
                else
                {
                    if (!string.IsNullOrEmpty(lang)) { Response.Redirect(currentPageName + "?lang=" + lang); }
                    else { Response.Redirect(currentPageName); }
                }
            }
        }
        else
        {
            string strCurTemplate = Path.GetFileName(Request.PhysicalPath);

            foundRow = dsPage.Tables[0].Select("PAGE_PARENT = 0 AND PAGE_TEMPLATE = '" + strCurTemplate + "'" + (!string.IsNullOrEmpty(lang) ? " AND PAGE_LANG = '" + lang + "'" : ""), "PAGE_ORDER ASC");

            if (foundRow.Length > 0)
            {
                string strTemplate = foundRow[0]["PAGE_TEMPLATE"].ToString();
                
                if (!string.IsNullOrEmpty(lang)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString() + "&lang=" + lang); }
                else { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strTemplate + "?pgid=" + foundRow[0]["PAGE_ID"].ToString()); }
            }
            else
            {
                mode = 1;
                lnkbtnUpdate.Visible = false;
            }
        }
    }

    protected void replaceTag()
    {
        string strContent = txtContent.Text;

        if (Session["ADMID"] != null && Session["ADMID"] != "")
        {
        }
        else
        {
            //@ for redirection
            clsConfig conf = new clsConfig();
            if (conf.extractItemByNameGroup(ConfigurationManager.AppSettings["cmsRedirectTag"], ConfigurationManager.AppSettings["cmsTagGroup"], 1))
            {
                if (strContent.IndexOf(clsAdmin.CONSTTAGREDIRECT) >= 0)
                {
                    string strURL = strContent.Replace(clsAdmin.CONSTTAGREDIRECT, "");
                    strURL = System.Text.RegularExpressions.Regex.Replace(strURL, "<[^>]*>", string.Empty);
                    strURL = strURL.Replace(System.Environment.NewLine, "");
                    strURL = strURL.Trim();
                    Response.Redirect(strURL);
                }
            }

            //@ for gmap
            try
            {
                string strGmapKey = "";
                if (conf.extractItemByNameGroup(ConfigurationManager.AppSettings["gmapKey"],"", 1))
                {
                    strGmapKey = conf.value;
                }

                if (conf.extractItemByNameGroup(ConfigurationManager.AppSettings["cmsGmapTag"], ConfigurationManager.AppSettings["cmsTagGroup"], 1))
                {
                    string strGmapTag = conf.value;
                    string strCmsTagClose = ConfigurationManager.AppSettings["cmsTagClose"];
                    string strCmsTagSplit = ConfigurationManager.AppSettings["cmsTagSplit"];
                    strGmapTag = strGmapTag.Substring(0, strGmapTag.Length - strCmsTagClose.Length);

                    int intCount = 0;

                    while ((strContent.IndexOf(strGmapTag + strCmsTagClose) >= 0) || (strContent.IndexOf(strGmapTag + strCmsTagSplit) >= 0))
                    {
                        intCount += 1;
                        int intTargetGmapTagStartIndex = 0;
                        int intTargetGmapTagEndIndex = 0;
                        int intTargetGmapAttrStartIndex = 0;
                        if (strContent.IndexOf(strGmapTag + strCmsTagClose) >= 0)
                        {
                            intTargetGmapTagStartIndex = strContent.IndexOf(strGmapTag + strCmsTagClose);
                        }
                        if (strContent.IndexOf(strGmapTag + strCmsTagSplit) >= 0)
                        {
                            intTargetGmapTagStartIndex = strContent.IndexOf(strGmapTag + strCmsTagSplit);
                        }

                        intTargetGmapTagEndIndex = strContent.IndexOf(strCmsTagClose, intTargetGmapTagStartIndex);
                        intTargetGmapAttrStartIndex = strContent.IndexOf(strCmsTagSplit, intTargetGmapTagStartIndex);

                        string strTargetGmapTag = strContent.Substring(intTargetGmapTagStartIndex, intTargetGmapTagEndIndex - intTargetGmapTagStartIndex + 1);
                        string strTargetGmapAttr = strContent.Substring(intTargetGmapAttrStartIndex + 1, intTargetGmapTagEndIndex - intTargetGmapAttrStartIndex - 1);

                        string[] arrTargetGmapAttr = strTargetGmapAttr.Split(char.Parse(strCmsTagSplit));
                        string strGmapLat = arrTargetGmapAttr[0];
                        string strGmapLong = arrTargetGmapAttr[1];
                        string strGmapZoom = arrTargetGmapAttr[2];
                        string strGmapId = arrTargetGmapAttr[3];

                        string strGmapJS = "";
                        if (Session["GMAPJS"] != null && Session["GMAPJS"] != "")
                        {
                            if (!Page.ClientScript.IsStartupScriptRegistered("GMAPCMS" + intCount))
                            {
                                string strJS = "<script type=\"text/javascript\">";
                                strJS += "addLoadEvent(load('" + strGmapId + "', '" + strGmapLat + "', '" + strGmapLong + "', " + strGmapZoom + "));";
                                strJS += "</script>";

                                Page.ClientScript.RegisterStartupScript(Page.GetType(), "GMAPCMS" + intCount, strJS, false);
                            }

                            strGmapJS = "<div id='" + strGmapId + "' class='divGMapCMS'></div>";
                        }
                        else
                        {
                            strGmapJS = "<script src='http://maps.google.com/maps?file=api&amp;v=2&amp;key=" + strGmapKey + "' type='text/javascript'></script>";
                            strGmapJS += "<script src='" + ConfigurationManager.AppSettings["jsBase"] + "common.js' type='text/javascript'></script>";
                            strGmapJS += "<script src='" + ConfigurationManager.AppSettings["jsBase"] + "loadmap.js' type='text/javascript'></script><div id='" + strGmapId + "' class='divGMapCMS'></div>";

                            HtmlGenericControl MasterPageBodyTag = new HtmlGenericControl();
                            MasterPageBodyTag = (HtmlGenericControl)Page.Master.FindControl("BodyID");
                            MasterPageBodyTag.Attributes.Add("onunload", "GUnload()");
                            MasterPageBodyTag.Attributes.Add("onload", "addLoadEvent(load('" + strGmapId + "', '" + strGmapLat + "', '" + strGmapLong + "', " + strGmapZoom + "))");

                            Session["GMAPJS"] = 1;
                        }

                        strContent = strContent.Replace(strTargetGmapTag, strGmapJS);
                    }
                }
            }
            catch (Exception ex)
            { }

            //if (strContent.IndexOf(clsAdmin.CONSTTAGENQUIRY) >= 0)
            //{
            //    UserControl UsrEnquiry = (UserControl)Page.LoadControl(ConfigurationManager.AppSettings["scriptBase"] + "ctrl/ucUsrEnquiry.ascx");
            //    phCMS.Controls.Add(UsrEnquiry);
            //    strContent = strContent.Replace(clsAdmin.CONSTTAGENQUIRY, "");
            //}
        }

        litContent.Text = strContent;
    }

    protected string checkRequiredData()
    {
        string strMessage = "<div class=\"errmsg\">Please enter all required fields (*).</div>";
        
        if (string.IsNullOrEmpty(ucCmnPageDetailsEdit.pageTitle) || (ucCmnPageDetailsEdit.parent <= 0 && string.IsNullOrEmpty(ucCmnPageDetailsEdit.template)) || (string.IsNullOrEmpty(ucCmnPageDetailsEdit.template) && !ucCmnPageDetailsEdit.templateParent))
        {
            return strMessage;
        }
        else { return ""; }
    }
    #endregion
}
