﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmSlideGroup.ascx.cs" Inherits="ctrl_ucAdmSlideGroup" %>
<link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>fancybox/jquery.fancybox.css" rel="Stylesheet" />
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jquery.fancybox.js" type="text/javascript"></script>
<asp:Panel ID="pnlForm" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <div>
        <div class="borderSplitter"></div>
        <table cellpadding="0" cellspacing="0" class="formTbl tblData">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr"><asp:Literal ID="litGroupDetails" runat="server"></asp:Literal></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblGroupName" runat="server"></asp:Label><span class="attention_unique">*</span>:</td>
                    <td class="tdMax"><asp:TextBox ID="txtName" runat="server" class="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                        <span class=""><asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="<br />Please enter Name." ControlToValidate="txtName" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cvName" runat="server" ControlToValidate="txtName" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateName_server" OnPreRender="cvName_PreRender"></asp:CustomValidator></span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblGroupDName" runat="server"></asp:Label><span class="attention_compulsory">*</span>:</td>
                    <td class="tdMax"><asp:TextBox ID="txtDName" runat="server" class="text_fullwidth uniq" MaxLength="250"></asp:TextBox><span class="spanFieldDesc"><a href="javascript:void(0);" onclick="copyValue('<% =txtName.ClientID %>','<% =txtDName.ClientID %>');" title="same as Name">same as Name</a></span>
                        <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvDName" runat="server" ErrorMessage="<br />Please enter Display Name." ControlToValidate="txtDName" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator></span>
                    </td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
            </tbody>
        </table>
        </div>
        <div>
        <div class="borderSplitter"></div>
        <table cellpadding="0" cellspacing="0" class="formTbl tblData">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr"><asp:Literal ID="litGroupSetting" runat="server"></asp:Literal></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel">Show on Top:</td>
                    <td class="tdLabel"><asp:CheckBox ID="chkboxShowTop" runat="server" /></td>
                </tr>
                <tr>
                    <td class="tdLabel">Active:</td>
                    <td class="tdLabel"><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
            </tbody>
        </table>
        </div>
        <div id="divSliderEffect" runat="server" visible="false" class="divSliderEffect">
            <div class="borderSplitter"></div>
            <table cellpadding="0" cellspacing="0" class="formTbl tblData">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">
                            <asp:Literal ID="litSliderEffect" runat="server"></asp:Literal>
                            <a href="#" class="fancybox hypPreview" runat="server" id="hypPreview">Preview</a>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel">Slider Type:</td>
                        <td class="tdLabel"><asp:DropDownList ID="ddlSliderType" runat="server" onchange="changeSliderTypeOption(this);" CssClass="ddl_fullwidth"></asp:DropDownList></td>
                    </tr>
                </tbody>
                <tbody id="tbodyFullScreen" style="display:none;">
                    <tr>
                        <td class="tdLabel">Transition Type</td>
                        <td class="tdLabel"><asp:DropDownList ID="ddlTransitionType_fullscreen" runat="server" CssClass="ddl_fullwidth"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Transition Speed</td>
                        <td class="tdLabel">
                            <div class="divSlider second" data-max="5" data-min="1"></div>
                            <div class="divSliderLabel"><label>1</label> seconds</div>
                            <asp:HiddenField ID="hdnTransitionSpeed_fullscreen" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Image Duration</td>
                        <td class="tdLabel">
                            <div class="divSlider second" data-max="5" data-min="1"></div>
                            <div class="divSliderLabel"><label>1</label> seconds</div>
                            <asp:HiddenField ID="hdnSlideInterval_fullscreen" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Random:</td>
                        <td class="tdLabel"><asp:CheckBox ID="chkboxRandom_fullscreen" runat="server" Checked="true" /></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Auto Play:</td>
                        <td class="tdLabel"><asp:CheckBox ID="chkboxAutoPlay_fullscreen" runat="server" Checked="true" /></td>
                    </tr>
                </tbody>
                <tbody id="tbodyDefault" style="display:none;">
                    <tr>
                        <td class="tdLabel">Transition Type</td>
                        <td class="tdLabel"><asp:DropDownList ID="ddlTransitionType_default" runat="server" CssClass="ddl_fullwidth"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Speed</td>
                        <td class="tdLabel">
                            <div class="divSlider second" data-max="5" data-min="1"></div>
                            <div class="divSliderLabel"><label>1</label> seconds</div>
                            <asp:HiddenField ID="hdnSpeed_default" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Random</td>
                        <td class="tdLabel"><asp:CheckBox ID="chkboxRandom_default" runat="server" Checked="true" /></td>
                    </tr>
                    <tr>
                        <td class="tdLabel">Reverse</td>
                        <td class="tdLabel"><asp:CheckBox ID="chkboxReverse_default" runat="server" Checked="true" /></td>
                    </tr>
                    <tr data-type="trTile">
                        <td class="tdLabel">Tile Vertical</td>
                        <td class="tdLabel"><asp:CheckBox ID="chkTileVert_default" runat="server" Checked="true" /></td>
                    </tr>
                    <tr data-type="trTile">
                        <td class="tdLabel">Tile Count</td>
                        <td class="tdLabel">
                            <div class="divSlider piece" data-max="10" data-min="1"></div>
                            <div class="divSliderLabel"><label>1</label> piece</div>
                            <asp:HiddenField ID="hdnTileCount_default" runat="server" />
                        </td>
                    </tr>
                </tbody>
                <tbody id="tbodyMobile" style="display:none;">
                    <tr>
                        <td class="tdLabel">Transition Type</td>
                        <td class="tdLabel"><asp:DropDownList ID="ddlTransitionType_mobile" runat="server" CssClass="ddl_fullwidth"></asp:DropDownList></td>
                    </tr>
                    <tr data-type="trCol">
                        <td class="tdLabel">Columns</td>
                        <td class="tdLabel">
                            <div class="divSlider cols" data-max="20" data-min="1"></div>
                            <div class="divSliderLabel"><label>1</label> columns</div>
                            <asp:HiddenField ID="hdnCol_mobile" runat="server" />
                        </td>
                    </tr>
                    <tr data-type="trRow">
                        <td class="tdLabel">Rows</td>
                        <td class="tdLabel">
                            <div class="divSlider rows" data-max="20" data-min="1"></div>
                            <div class="divSliderLabel"><label>1</label> rows</div>
                            <asp:HiddenField ID="hdnRow_mobile" runat="server" />
                        </td>
                    </tr>
<%--                    <tr>
                        <td class="tdLabel">Slider Speed</td>
                        <td class="tdLabel">
                            <div class="divSlider second" data-max="5" data-min="1"></div>
                            <div class="divSliderLabel"><label>1</label> seconds</div>
                            <asp:HiddenField ID="hdnIdle_mobile" runat="server" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="tdLabel">Transition Speed</td>
                        <td class="tdLabel">
                            <div class="divSlider second" data-max="5" data-min="1"></div>
                            <div class="divSliderLabel"><label>1</label> seconds</div>
                            <asp:HiddenField ID="hdnDuration_mobile" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div>
        <table cellpadding="0" cellspacing="0" class="formTbl tblData">
            <tfoot>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                                <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                    <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                                </asp:Panel>
                                <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                    <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" class="btn btnCancel" Visible="false" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
                                </asp:Panel>
                            </asp:Panel>
                        </td>
                    </tr>
                </tfoot>
        </table>
        </div>
    </asp:Panel>
</asp:Panel>
<script>
    function changeSliderTypeOption(element) {
        var selectedOption = element.options[element.selectedIndex].value;

        $("#tbodyDefault").hide();
        $("#tbodyFullScreen").hide();
        $("#tbodyMobile").hide();
        if (selectedOption == "<%= clsAdmin.CONSTLISTVALUE_DEFAULT %>") {
            $("#tbodyDefault").show();
        } else if (selectedOption == "<%= clsAdmin.CONSTLISTVALUE_FULLSCREEN %>") {
            $("#tbodyFullScreen").show();
        } else if (selectedOption == "<%= clsAdmin.CONSTLISTVALUE_MOBILE %>") {
            $("#tbodyMobile").show();
        }
    }
    function initTile() {
        var sliderType = $("#<%= ddlSliderType.ClientID %>").val();
        var sliderTransition = $("#<%= ddlTransitionType_default.ClientID %>").val();
        $("tr[data-type=trTile]").hide();
        if (sliderType == "<%= clsAdmin.CONSTLISTVALUE_DEFAULT %>" &&
            (sliderTransition == "tileSlide" || sliderTransition == "tileBlind")) {
            $("tr[data-type='trTile']").show();
        }
    }
    function initMobile() {
        var sliderType = $("#<%= ddlSliderType.ClientID %>").val();
        var sliderTransition = $("#<%= ddlTransitionType_mobile.ClientID %>").val();

        $("tr[data-type=trRow]").show();
        $("tr[data-type=trCol]").show();
        if (sliderType == "<%= clsAdmin.CONSTLISTVALUE_MOBILE %>") {
            
            if (sliderTransition == "6" || sliderTransition == "7") {
                $("tr[data-type=trCol]").hide();
            }
            else if (sliderTransition == "11" || sliderTransition == "12" || sliderTransition == "13") {
                $("tr[data-type=trRow]").hide();
            }
        }
    }
    function getSupersizedOption() {
        var supersizedOption = "option=";
        supersizedOption += "transition:"+$("#<%= ddlTransitionType_fullscreen.ClientID %>").val()+",";
        supersizedOption += "transition_speed:" + $("#<%= hdnTransitionSpeed_fullscreen.ClientID %>").val() + ",";
        supersizedOption += "slide_interval:" + $("#<%= hdnSlideInterval_fullscreen.ClientID %>").val() + ",";
        supersizedOption += "random:" + ($("#<%= chkboxRandom_fullscreen.ClientID %>").is(':checked') ? 1 : 0) + ",";
        supersizedOption += "autoplay:" + ($("#<%= chkboxAutoPlay_fullscreen.ClientID %>").is(':checked') ? 1 : 0) + ",";
        return supersizedOption;
    }
    function getCycle2Option() {
        var cycle2Option = "option=";
        cycle2Option += "data-cycle-fx:" + $("#<%= ddlTransitionType_default.ClientID %>").val() + ",";
        cycle2Option += "data-cycle-speed:" + $("#<%= hdnSpeed_default.ClientID %>").val() + ",";
        cycle2Option += "data-cycle-random:" + ($("#<%= chkboxRandom_fullscreen.ClientID %>").is(':checked') ? "true" : "false") + ",";
        cycle2Option += "data-cycle-reverse:" + ($("#<%= chkboxAutoPlay_fullscreen.ClientID %>").is(':checked') ? "true" : "false") + ",";
        
        if ($("#<%= ddlTransitionType_default.ClientID %>").val() == "tileSlide" ||
            $("#<%= ddlTransitionType_default.ClientID %>").val() == "tileBlind") { 
            cycle2Option += "data-cycle-tile-count:" + $("#<%= hdnTileCount_default.ClientID %>").val() + ","; 
            cycle2Option += "data-cycle-tile-vertical:" + ($("#<%= chkTileVert_default.ClientID %>").is(':checked') ? "true" : "false") + ",";
        }

        return cycle2Option;
    }
    function getJssorOption() {
        var jsonTransition = {};
        <% foreach(var item in clsMasthead.dictMobileTransition) { %>
            <% System.Collections.Generic.Dictionary<string,string> dict = (System.Collections.Generic.Dictionary<string,string>)item.Value; %>
            jsonTransition["<%= item.Key %>"] = "<%= dict["option"] %>";
        <%}%>

        var options = jsonTransition[$("#<%= ddlTransitionType_mobile.ClientID %>").val()];
        options = options.replace("[$cols]",$("#<%= hdnCol_mobile.ClientID %>").val());
        options = options.replace("[$rows]",$("#<%= hdnRow_mobile.ClientID %>").val());
        options = options.replace("[$duration]",$("#<%= hdnDuration_mobile.ClientID %>").val());

        var jssorOption = "option="+options;

        

        return jssorOption;
    }
    $(function() {
        changeSliderTypeOption($("#<%= ddlSliderType.ClientID %>").get(0));
        initTile();
        initMobile()
        var $fancybox =  $(".fancybox").fancybox({
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'speedIn': 600,
            'speedOut': 200,
            'type': 'iframe',
            'autoResize':false,
            'autoSize': false,
            'fitToView': false,

            'preload': true,
            'afterShow': function () {
                this.width = $(".fancybox-iframe").contents().find(".sliderSize").width();
                this.height = $(".fancybox-iframe").contents().find(".sliderSize").height();
            },
            'onUpdate': function () {
                $.fancybox.update();
            }

        });
        <% if(mode == 1) { %>
            $("#<%= hypPreview.ClientID %>").hide();
        <%}%>
        $("#<%= hypPreview.ClientID %>").click(function () {
            var selectedOption = $("#<%= ddlSliderType.ClientID %>").val();
            var url = "<%= ConfigurationManager.AppSettings["scriptBase"] %>adm/admMastheadTransitionDemo.aspx?id=<%= groupId %>";
            if (selectedOption == "<%= clsAdmin.CONSTLISTVALUE_DEFAULT %>") {
                $(this).attr("href", url + "&" + getCycle2Option());
            } else if (selectedOption == "<%= clsAdmin.CONSTLISTVALUE_FULLSCREEN %>") {
                $(this).attr("href", url + "&" + getSupersizedOption());

            } else if (selectedOption == "<%= clsAdmin.CONSTLISTVALUE_MOBILE %>") {
                $(this).attr("href", url + "&" + getJssorOption());
            }
        });
        $(".divSlider").each(function() {
            var $this = $(this);
            var value = 1;
            var isSecond = $this.hasClass("second");
            var isPiece = $this.hasClass("piece");
            var isRowOrCol = $this.hasClass("rows") || $this.hasClass("cols");
            if (isSecond) {
                if ($this.parent().find("input[type=hidden]").val() > 0) {
                    value = $this.parent().find("input[type=hidden]").val() / 1000; 
                } else {
                    $this.parent().find("input[type=hidden]").val(1000);
                }
            }
            else if (isPiece || isRowOrCol) {
                if ($this.parent().find("input[type=hidden]").val() > 0) {
                    value = $this.parent().find("input[type=hidden]").val();
                } else {
                    $this.parent().find("input[type=hidden]").val(1);
                }
            }
            $this.parent().find("label").text(value);
            
            $this.slider({
                value: value,
                min: $this.data("min"),
                max: $this.data("max"),
                slide: function(event, ui) {
                    $this.parent().find("label").text(ui.value);
                    if (isSecond) {
                        $this.parent().find("input[type=hidden]").val(ui.value * 1000);
                    }
                    else if (isPiece || isRowOrCol) {
                        $this.parent().find("input[type=hidden]").val(ui.value);
                    }
                }
            });
        });


        $("#<%= ddlTransitionType_default.ClientID %>").change(function() {
            if ($(this).val() == "tileSlide" || $(this).val() == "tileSlide") {
                $("tr[data-type=trTile]").show();
            }
            else {
                $("tr[data-type=trTile]").hide();
            }
        });
        $("#<%= ddlTransitionType_mobile.ClientID %>").change(function () {
            $("tr[data-type=trRow]").show();
            $("tr[data-type=trCol]").show();
            if ($(this).val() == "6" || $(this).val() == "7") {
                $("tr[data-type=trCol]").hide();
            }
            else if ($(this).val() == "11" || $(this).val() == "12" || $(this).val() == "13") {
                $("tr[data-type=trRow]").hide();
            }
        });
    });

</script>