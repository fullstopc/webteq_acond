﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ctrl_ucUsrFacebookThumbnail : System.Web.UI.UserControl
{
    #region "Properties"
    protected string _urlToLike = "";
    protected string _imageFile = "";
    protected string _entityTitle = "";
    protected string _entityType = "website";
    protected string _siteName = "";
    protected string _entityDesc = "";
    #endregion


    #region "Property Methods"
    public string urlToLike
    {
        get { return ViewState["URLTOLIKE"] as string ?? _urlToLike; }
        set { ViewState["URLTOLIKE"] = value; }
    }

    public string imageFile
    {
        get { return ViewState["IMAGEFILE"] as string ?? _imageFile; }
        set { ViewState["IMAGEFILE"] = value; }
    }

    public string entityTitle
    {
        get { return ViewState["ENTITYTITLE"] as string ?? _entityTitle; }
        set { ViewState["ENTITYTITLE"] = value; }
    }

    public string entityType
    {
        get { return ViewState["ENTITYTYPE"] as string ?? _entityType; }
        set { ViewState["ENTITYTYPE"] = value; }
    }

    public string siteName
    {
        get { return ViewState["SITENAME"] as string ?? _siteName; }
        set { ViewState["SITENAME"] = value; }
    }

    public string entityDesc
    {
        get { return ViewState["ENTITYDESC"] as string ?? _entityDesc; }
        set { ViewState["ENTITYDESC"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        Literal litMetaTag = new Literal();
        litMetaTag.Text = !string.IsNullOrEmpty(entityTitle) ? "<meta property=\"og:title\" content=\"" + entityTitle + "\" />" : "";
        litMetaTag.Text += "<meta property=\"og:type\" content=\"" + entityType + "\" />";
        litMetaTag.Text += "<meta property=\"og:url\" content=\"" + urlToLike + "\" />";
        litMetaTag.Text += "<meta property=\"og:image\" content=\"" + imageFile + "\" />";
        litMetaTag.Text += "<meta property=\"og:site_name\" content=\"" + siteName + "\" />";
        litMetaTag.Text += !string.IsNullOrEmpty(entityDesc) ? "<meta property=\"og:description\" content=\"" + entityDesc + "\" />" : "";
        this.Page.Header.Controls.Add(litMetaTag);
    }
    #endregion
}
