﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmSocialMedia.ascx.cs" Inherits="ctrl_ucAdmSocialMedia" %>

<script type="text/javascript">

</script>

<asp:Panel ID="pnlAdmCMSControlPanel" runat="server" CssClass="divAdmCMSControlPanel">
    <asp:Panel ID="pnlFormDetails" runat="server" CssClass="">
        <div id="divFacebookLike" runat="server">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td class="tdSectionHdr" colspan="2">Facebook Settings</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblFbPageUrl" runat="server"></asp:Label>:</td>
                        <td>
                            <div class="input-group">
                                <asp:TextBox ID="txtFbPageUrl" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox>
                                <span class="spanTooltip">
                                    <span class="icon"></span>
                                    <span class="msg">
                                        For eg: http://www.facebook.com/WebteqSolution
                                    </span>
                                </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblFBLikeTitle" runat="server"></asp:Label>:</td>
                        <td>
                        <div class="input-group-no-icon"><asp:TextBox ID="txtFBLikeTitle" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblFBLikeType" runat="server"></asp:Label>:</td>
                        <td>
                            <div class="input-group">
                            <asp:TextBox ID="txtFBLikeType" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox>
                            <span class="spanTooltip">
                                <span class="icon"></span>
                                <span class="msg">
                                    For eg: website, blog, article. <a href="http://developers.facebook.com/docs/opengraphprotocol/#types" target="_blank">view reference</a>
                                    
                                </span>
                            </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblFBLikeUrl" runat="server"></asp:Label>:</td>
                        <td>
                            <div class="input-group">
                            <asp:TextBox ID="txtFBLikeUrl" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox>
                            <span class="spanTooltip">
                                <span class="icon"></span>
                                <span class="msg">
                                    For eg: <asp:Literal ID="litFBLikeUrlSample" runat="server"></asp:Literal>
                                    
                                </span>
                            </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblFBLikeImage" runat="server"></asp:Label>:</td>
                        <td>
                            <div class="input-group">
                            <asp:TextBox ID="txtFBLikeImage" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox>
                            <span class="spanTooltip">
                                <span class="icon"></span>
                                <span class="msg">
                                    <a href="javascript:void();" onclick="if(<% =txtFBLikeImage.ClientID %>.value != ''){window.open(<% =txtFBLikeImage.ClientID %>.value);}else{alert('Please specify an image.')}" title="view image">view image</a>
                                    
                                </span>
                            </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblFBLikeSitename" runat="server"></asp:Label>:</td>
                        <td>
                        
                            <div class="input-group-no-icon"><asp:TextBox ID="txtFBLikeSitename" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox>
                        </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblFBLikeDesc" runat="server"></asp:Label>:</td>
                        <td><div class="input-group-no-icon"><asp:TextBox ID="txtFBLikeDesc" runat="server" CssClass="text_fullwidth" MaxLength="1000"></asp:TextBox></div></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="borderSplitter"></div>
        <div id="divFacebookLogin" runat="server">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Facebook Login Settings</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblFbLoginActive" runat="server"></asp:Label>:</td>
                        <td class="tdLabel">
                            <asp:CheckBox ID="chkboxFbLoginActive" runat="server" />
                        </td>
                    </tr> 
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblFbLoginApiKey" runat="server"></asp:Label>:</td>
                        <td>
                            <div class="input-group-no-icon"><asp:TextBox ID="txtFbLoginApiKey" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></div>
                        </td>
                    </tr> 
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblFbLoginSecret" runat="server"></asp:Label>:</td>
                        <td>
                            <div class="input-group-no-icon"><asp:TextBox ID="txtFbLoginSecret" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblFbLoginCallback" runat="server"></asp:Label>:</td>
                        <td>
                            <div class="input-group-no-icon"><asp:TextBox ID="txtFbLoginCallback" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></div>
                        </td>
                    </tr> 
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblFbLoginSuffix" runat="server"></asp:Label>:</td>
                        <td>
                            <div class="input-group-no-icon"><asp:TextBox ID="txtFbLoginSuffix" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></div>
                        </td>
                    </tr>  
                </tbody>
            </table>
        </div>
        <div class="borderSplitter"></div>
        <div id="divGooglePlus" runat="server">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Google+ Settings</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblGooglePlusUrl" runat="server"></asp:Label>:</td>
                        <td><div class="input-group-no-icon"><asp:TextBox ID="txtGoogleURL" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></div></td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblGPLikeUrl" runat="server"></asp:Label>:</td>
                        <td>
                            <div class="input-group">
                            <asp:TextBox ID="txtGPLikeUrl" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox>
                            <span class="spanTooltip">
                                <span class="icon"></span>
                                <span class="msg">
                                   For eg: <asp:Literal ID="litGPLikeUrlSample" runat="server"></asp:Literal>
                                   
                                </span>
                            </span>
                            </div>
                        </td>
                    </tr> 
                </tbody>
            </table>
        </div>
        <div class="borderSplitter"></div>
        <div id="divLinkedIn" runat="server">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">LinkedIn Settings</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblLinkedInUrl" runat="server"></asp:Label>:</td>
                        <td><div class="input-group-no-icon"><asp:TextBox ID="txtLinkedInURL" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></div></td>
                    </tr>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblLIShareUrl" runat="server"></asp:Label>:</td>
                        <td><div class="input-group-no-icon"><asp:TextBox ID="txtLIShareUrl" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></div></td>
                    </tr> 
                </tbody>
            </table>
        </div>
        <div class="borderSplitter"></div>
        <div id="divInstagram" runat="server">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Instagram Settings</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblInstagramUrl" runat="server"></asp:Label>:</td>
                        <td><div class="input-group-no-icon"><asp:TextBox ID="txtInstagramUrl" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></div></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="borderSplitter"></div>
        <div id="divTwitter" runat="server">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Twitter Settings</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblTwitterUrl" runat="server"></asp:Label>:</td>
                        <td><div class="input-group-no-icon"><asp:TextBox ID="txtTwitterUrl" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></div></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="borderSplitter"></div>
        <div id="divYoutube" runat="server">
            <table class="formTbl tblData" cellpadding="0" cellspacing="0">        
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Youtube Settings</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdLabel"><asp:Label ID="lblYoutubeUrl" runat="server"></asp:Label>:</td>
                        <td><div class="input-group-no-icon"><asp:TextBox ID="txtYoutubeUrl" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></div></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="">
            <table id="tblAction" class="formTbl tblData" cellpadding="0" cellspacing="0" style="padding-top: 0;">
                <tr>
                    <td class="tdLabel"></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated" style="padding-top:0px;">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    
</asp:Panel>
