﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ctrl_ucAdmUserDetails : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _userId = 0;
    protected int _mode = 1;
    protected string _pageListingURL = "";
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int userId
    {
        get
        {
            if (ViewState["USERID"] == null)
            {
                return _userId;
            }
            else
            {
                return int.Parse(ViewState["USERID"].ToString());
            }
        }
        set { ViewState["USERID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if (ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void cvUserEmail_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("UserEmail")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtUserEmail.ClientID + "', document.getElementById('" + cvUserEmail.ClientID + "'));";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "UserEmail", strJS, false);
        }
    }

    protected void validateUserEmail_server(object source, ServerValidateEventArgs args)
    {
        string strEmail = txtUserEmail.Text.Trim();

        clsUser usr = new clsUser();
        if (!usr.isUserExist(userId, strEmail))
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
            cvUserEmail.ErrorMessage = "<br />Email is in used. Please try another Email.";
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strEmail = txtUserEmail.Text.Trim();
            int intType = 0;
            string strPwd = txtUserPwd.Text.Trim();
            string strConfirmPwd = txtConfirmPwd.Text.Trim();
            int intProject = 0;
            int intActive = 0;

            if (!string.IsNullOrEmpty(ddlUserType.SelectedValue)) { intType = Convert.ToInt16(ddlUserType.SelectedValue); }
            if (!string.IsNullOrEmpty(ddlUserProject.SelectedValue)) { intProject = int.Parse(ddlUserProject.SelectedValue); }
            if (chkboxActive.Checked) { intActive = 1; }

            clsUser user = new clsUser();
            int intRecordAffected = 0;

            clsMD5 md5 = new clsMD5();
            string encryptPwd = md5.encrypt(strPwd);

            if (mode == 1)
            {
                intRecordAffected = user.addUser(strEmail, intType, encryptPwd, intProject, intActive, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1)
                {
                    Session["NEWUSERID"] = user.userId;

                    Response.Redirect(currentPageName + "?id=" + user.userId);
                }
                else
                {
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new User. Please try again.</div>";

                    Response.Redirect(currentPageName);
                }
            }
            else if (mode == 2)
            {
                Boolean boolEdited = false;
                Boolean boolSuccess = true;

                if (!user.isExactSameSetUser(userId, strEmail, intType, intProject, intActive))
                {
                    intRecordAffected = user.updateUserById(userId, strEmail, intType, intProject, intActive, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1)
                    {
                        boolEdited = true;
                    }
                    else
                    {
                        boolSuccess = false;
                    }
                }

                if (boolSuccess && !string.IsNullOrEmpty(strPwd))
                {
                    if (string.Compare(hdnPwd.Value.Trim(), encryptPwd) != 0)
                    {
                        intRecordAffected = user.updateUserById(userId, encryptPwd, int.Parse(Session["ADMID"].ToString()));

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;
                        }
                        else
                        {
                            boolSuccess = false;
                        }
                    }
                }

                if (boolEdited)
                {
                    Session["EDITEDUSERID"] = userId;

                    Response.Redirect(Request.Url.ToString());
                }
                else
                {
                    if (boolSuccess)
                    {
                        Session["EDITEDUSERID"] = userId;
                        Session["NOCHANGE"] = 1;

                        Response.Redirect(Request.Url.ToString());
                    }
                    else
                    {
                        user.extractUserById(userId, 0);

                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit User (" + user.userEmail + "). Please try again.</div>";

                        Response.Redirect(Request.Url.ToString());
                    }
                }
            }
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string strDeletedUserEmail = "";

        clsUser user = new clsUser();
        int intRecordAffected = 0;

        user.extractUserById(userId, 0);
        strDeletedUserEmail = user.userEmail;

        intRecordAffected = user.deleteUserById(userId);

        if (intRecordAffected == 1)
        {
            Session["DELETEDUSEREMAIL"] = strDeletedUserEmail;
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to delete User (" + strDeletedUserEmail + "). Please try again.</div>";
        }

        Response.Redirect(currentPageName);
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void setPageProperties()
    {
        DataSet ds = new DataSet();
        DataView dv = new DataView();

        clsProject proj = new clsProject();
        ds = proj.getProjectList(0);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "PROJ_NAME ASC";

        ddlUserProject.DataSource = dv;
        ddlUserProject.DataTextField = "PROJ_NAME";
        ddlUserProject.DataValueField = "PROJ_ID";
        ddlUserProject.DataBind();

        ListItem ddlUserProjectDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlUserProject.Items.Insert(0, ddlUserProjectDefaultItem);

        clsMis mis = new clsMis();
        ds = new DataSet();
        ds = mis.getListByListGrp("ADMIN TYPE", 1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "LIST_ORDER ASC";

        ddlUserType.DataSource = dv;
        ddlUserType.DataTextField = "LIST_NAME";
        ddlUserType.DataValueField = "LIST_VALUE";
        ddlUserType.DataBind();

        ListItem ddlUserTypeDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlUserType.Items.Insert(0, ddlUserTypeDefaultItem);

        lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";

        if(mode == 2)
        {
            lnkbtnDelete.Visible = true;
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteUser.Text").ToString() + "'); return false;";

            if (userId > 0) { fillForm(); }
        }
    }

    protected void fillForm()
    {
        clsUser user = new clsUser();
        if (user.extractUserById(userId, 0))
        {
            txtUserEmail.Text = user.userEmail;
            hdnPwd.Value = user.userPwd;

            if (user.userType != 0)
            {
                ddlUserType.SelectedValue = user.userType.ToString();
            }

            if (user.userProject != 0)
            {
                ddlUserProject.SelectedValue = user.userProject.ToString();
            }

            if (user.userActive != 0) { chkboxActive.Checked = true; }
            else { chkboxActive.Checked = false; }

            if (user.userCreation != DateTime.MaxValue) { spanCreatedAt.Visible = true; litCreatedAt.Text = user.userCreation.ToString("dd MMM yyyy hh:mm:ss tt"); }
            if (user.userLastUpdate != DateTime.MaxValue) { spanUpdatedAt.Visible = true; litUpdatedAt.Text = user.userLastUpdate.ToString("dd MMM yyyy hh:mm:ss tt"); }

            hypLogin.Attributes["href"] = System.Configuration.ConfigurationManager.AppSettings["scriptBase"] +  "adm/admlogin.aspx?id=" + userId + "&type=" + 1;
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";

            Response.Redirect(currentPageName);
        }
    }
    #endregion
}
