﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmDetail.ascx.cs" Inherits="ctrl_ucAdmDetail" %>

<asp:Panel ID="pnlForm" runat="server">
    <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData" id="tblEventDetail">
        <tr id="trHeader" runat="server" visible="false">
            <td class="tdSectionHdr" colspan="2"><asp:Literal ID="litHeader" runat="server"></asp:Literal></td>
        </tr>
        <tr id="trHeaderSpace" runat="server" visible="false">
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td class="">Title:</td>
            <td class=""><span class="boldmsg"><asp:Literal ID="litTitle" runat="server"></asp:Literal></span></td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr id="trTitleJp" runat="server" visible="false">
            <td>Title (Japanese):</td>
            <td class=""><span class="boldmsg"><asp:Literal ID="litTitleJp" runat="server"></asp:Literal></span></td>
        </tr>
        <tr id="trTitleMs" runat="server" visible="false">
            <td>Title (Malay):</td>
            <td class=""><span class="boldmsg"><asp:Literal ID="litTitleMs" runat="server"></asp:Literal></span></td>
        </tr>
        <tr id="trTitleZh" runat="server" visible="false">
            <td>Title (Chinese):</td>
            <td class=""><span class="boldmsg"><asp:Literal ID="litTitleZh" runat="server"></asp:Literal></span></td>
        </tr>
        <tr id="trShortDesc" runat="server">
            <td>Short Description:</td>
            <td class=""><asp:Literal ID="litShortDesc" runat="server"></asp:Literal></td>
        </tr>
        <tr id="trShortDescJp" runat="server" visible="false">
            <td>Short Description (Japanese):</td>
            <td class=""><asp:Literal ID="litShortDescJp" runat="server"></asp:Literal></td>
        </tr>
        <tr id="trShortDescMs" runat="server" visible="false">
            <td>Short Description (Malay):</td>
            <td class=""><asp:Literal ID="litShortDescMs" runat="server"></asp:Literal></td>
        </tr>
        <tr id="trShortDescZh" runat="server" visible="false">
            <td>Short Description (Chinese):</td>
            <td class=""><asp:Literal ID="litShortDescZh" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
    </table>
</asp:Panel>