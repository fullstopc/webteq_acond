﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ctrl_ucAdmEnquiry : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _id;
    protected string strLat = "";
    protected string strLng = "";
    #endregion


    #region "Property Methods"
    public int id
    {
        get
        {
            if (ViewState["ID"] == null)
            {
                return _id;
            }
            else
            {
                return Convert.ToInt16(ViewState["ID"]);
            }
        }
        set { ViewState["ID"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lnkbtnBack.OnClientClick = "javascript:document.location.href='admEnquiry01.aspx'; return false;";
        }
    }

    protected void lnkbtnResend_Click(object sender, EventArgs e)
    {
        string strSubjectPrefix;
        string strSubject;
        string strBody;
        string strUserSalutation;
        string strUserName;
        string strUserEmail;
        string strUserSubject;
        string strUserBody;
        string strUserContact;
        string strUserCompany;
        string strUserMessage;
        string strAck;
        string strSenderName;
        string strWebsiteName;
        string strSignature;
        string strGeoLocation = "";
        Boolean boolSuccess = false;

        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEADMINEMAILSUBJECT, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEADMINEMAILCONTENT, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEUSEREMAILSUBJECT, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strUserSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEUSEREMAILCONTENT, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEWEBSITENAME, clsConfig.CONSTGROUPWEBSITE, 1);
        strWebsiteName = config.value;
        
        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strSubject = strSubjectPrefix + " " + strSubject + " - Resend"; }
        
        clsEnquiry enquiry = new clsEnquiry();
        clsMis mis = new clsMis();
        if (enquiry.extractEnquiryById(id, 0))
        {
            strUserSalutation = (enquiry.enqSalutation > 0) ? mis.getListNameByListValue(enquiry.enqSalutation.ToString(), "SALUTATION", 1, 1) : "-";
            strUserName = enquiry.enqName;
            strUserCompany = (enquiry.enqCompanyName != "") ? enquiry.enqCompanyName : "-";
            strUserContact = (enquiry.enqContactNo != "") ? enquiry.enqContactNo : "-";
            strUserEmail = (enquiry.enqEmailSender != "") ? enquiry.enqEmailSender : "-";
            strUserSubject = (enquiry.enqSubject != "") ? enquiry.enqSubject : "-";
            strUserMessage = (enquiry.enqMessage != "") ? enquiry.enqMessage : "-";

            strBody = strBody.Replace("[SALUTATION]", strUserSalutation);
            strBody = strBody.Replace("[NAME]", strUserName);
            strBody = strBody.Replace("[COMPANY]", strUserCompany);
            strBody = strBody.Replace("[CONTACT]", strUserContact);
            strBody = strBody.Replace("[EMAIL]", strUserEmail);
            strBody = strBody.Replace("[SUBJECT]", strUserSubject);
            strBody = strBody.Replace("[MESSAGE]", strUserMessage);
            strBody = strBody.Replace("[WEBSITENAME]", strWebsiteName);
            strBody = strBody.Replace("[SIGNATURE]", strSignature);
            
            //@ Send email to admin
            if (clsEmail.send(strSenderName, "", "", "", "", strUserEmail, strSubject, strBody, 1))
            {
                boolSuccess = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(strSubject)) { strSubject = strSubject + " using Webteq";}
                if (clsEmail.sendByGroup(strSenderName, "", "", "", "", strUserEmail, strSubject, strBody, "", 1, clsConfig.CONSTGROUPWEBTEQEMAILSETTING))
                {
                    boolSuccess = true;
                }
                else
                {
                    boolSuccess = false;
                }
            }
            
            if (!boolSuccess)
            {
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to send email. Please try again.</div>";
                Response.Redirect(Request.Url.ToString());
            }
            else
            {
                enquiry.updateEnquiryResend();
                Session["DATA_SUCCESS"] = 1;
                Response.Redirect(Request.Url.ToString());
            }
        }
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        clsEnquiry enquiry = new clsEnquiry();
        clsMis mis = new clsMis();

        if (enquiry.extractEnquiryById(id, 0))
        {
            litRecipientEmail.Text = enquiry.enqEmailRecipient;
            litDomainURL.Text = enquiry.enqURL;
            litSalutation.Text = (enquiry.enqSalutation > 0)? mis.getListNameByListValue(enquiry.enqSalutation.ToString(), "SALUTATION", 1, 1) : "-";
            litName.Text = enquiry.enqName;
            litCompanyName.Text = (enquiry.enqCompanyName != "") ? enquiry.enqCompanyName : "-";
            litContact.Text = (enquiry.enqContactNo != "") ? enquiry.enqContactNo : "-";
            litEmail.Text = (enquiry.enqEmailSender != "") ? enquiry.enqEmailSender : "-";
            litSubject.Text = (enquiry.enqSubject != "") ? enquiry.enqSubject : "-";
            litMessage.Text = (enquiry.enqMessage != "") ? enquiry.enqMessage : "-";
            litSentDate.Text = enquiry.enqCreation.ToString("dd MMM yyyy hh:mm:ss tt");

            if(enquiry.enqResendCount > 0)
            {
                litResend.Text = " (Last Resend: " + enquiry.enqLastResend.ToString("dd MMM yyyy hh:mm:ss tt") + ")";
            }

            if (!string.IsNullOrEmpty(enquiry.engGeoLocation))
            {
                string[] strLatAndLng = enquiry.engGeoLocation.Split(',');
                if (strLatAndLng.Length == 2)
                {
                    trLocation.Visible = true;
                    strLat = strLatAndLng[0];
                    strLng = strLatAndLng[1];
                }
            }

        }
    }
    #endregion
}