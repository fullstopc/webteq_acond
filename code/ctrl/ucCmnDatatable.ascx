﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCmnDatatable.ascx.cs" Inherits="ctrl_ucCmnDataTable" EnableViewState="false" %>
    <table id="datatable" runat="server"></table>
    <script>

        var setStorage = function(data){
            localStorage.setItem("datatable_properties", data);
        }
        var getStorage = function(name, columns){
            var properties = localStorage.getItem("datatable_properties");
            if(properties !== null){
                properties = $.parseJSON(properties);
            }

            if(properties === null || typeof(properties[name]) === 'undefined'){
                if(properties === null){
                    properties = {};
                }
                properties[name] = {
                    "columns" : columns.map(function(x){
                        var obj = {};
                        obj.name = x.data;
                        obj.visible = x.visible;
                        obj.width = -1;

                        return obj
                    })
                };
            }
            return properties;
        }

        var convertActive = function (data) {
            return (data == "1") ? "Yes" : "No";
        }
        var convertDatetime = function (data,format) {
            if(!data) return '';

            format = format ||  "D MMM YYYY h:mm A";
            return moment(data).format(format);
        }
        var convertPrice = function(data, currency){
            currency = currency || "";
            return currency + " " + parseFloat(data).toFixed(2);
        }


        var callDataTables = function (properties) {
            // local storage
            
            var name = properties.name;
            var func = properties.func;
            var dom = properties.dom || 'Z<"dataTables_top"ip<"dropdown-container colshow"><"clear">>Brti<"bottom"p>';
            var url = properties.url || "getData";
            var callback = properties.callback || function(){}
            var order = properties.order || [[1, 'asc']];
            var columns = properties.columns;
            var columnDefs = properties.columnDefs;
            var columnSort = properties.columnSort;
            var pageLength = properties.pageLength || <%= intPageLength %>;
            var elementID = properties.elementID || "<%= datatable.ClientID %>"
            var dataSerializeFunc = properties.dataSerializeFunc || function (d) {
                //console.log(d);
                d.searchCustom = null,
                d.func = func
                return JSON.stringify(d);
            };

            var datatable_properties = getStorage(name,columns);

            //repopulate visible
            columns = columns.map(function(x,index){
                x.visible = datatable_properties[name].columns[index].visible;
                if(datatable_properties[name].columns[index].width > 0){
                    x.width = datatable_properties[name].columns[index].width;
                }
                return x;
            });
            
            // update table ID
            var $table = $("#" + elementID);

            // header
            var tableHeader = columns.reduce(function (prevItem, currItem) {
                return prevItem + "<th>" + currItem.label + "</th>";
            }, "");
            //$table.append("<thead><tr>" + tableHeader + "</tr></thead>");

            var tableInput = columns.reduce(function (prevItem, currItem) {
                var textbox = '';
                if (typeof (currItem.bSearchable) === 'undefined') { currItem.bSearchable = true; }
                if (currItem.bSearchable) {
                    textbox = '<input type="text" placeholder="' + currItem.label + '" class="form-control" />';
                }
                return prevItem + '<td class="input-filter">' + (textbox) + '</td>';
            }, "");
            $table.append("<thead><tr>" + tableHeader + "</tr><tr>" + tableInput + "</tr></thead>");

            // footer
            //var tableFooter = columns.reduce(function (prevItem, currItem) {
            //    var textbox = '';
            //    if (typeof (currItem.bSearchable) === 'undefined') { currItem.bSearchable = true; }
            //    if (currItem.bSearchable) {
            //        textbox = '<input type="text" placeholder="' + currItem.label + '" class="form-control" />';
            //    }
            //    return prevItem + '<td>' + (textbox) + '</td>';
            // }, "");
            //$table.append("<thead><tr>" + tableFooter + "</tr></thead>");
            //$table.append("<tfoot><tr>" + tableFooter + "</tr></tfoot>");

            // convert columnDefs
            var convertColumnDef = columns.filter(function (x) {
                return typeof (x.convert) !== 'undefined' || x.convert == 'active' || x.convert == 'datetime'
            }).map(function (x) {
                return {
                    "render": function (data, type, row) {
                        switch (x.convert) {
                            case "active":
                                return convertActive(data);
                            case "datetime":
                                return convertDatetime(data, x.format);
                            case "price":
                                return convertPrice(data, x.currency);

                        }
                        return data;
                    },
                    "targets": x.order
                }
            })

            
            var $datatable = $table.DataTable({
                "processing": true, // for show progress bar
                "serverSide": true, // for process server side
                "iDisplayLength": pageLength,
                "bSortCellsTop": true,
                "ajax": {
                    "url": wsBase + "wsDataTable.asmx/" + url,
                    "type": "POST",
                    "datatype": "json",
                    "contentType": "application/json; charset=utf-8",
                    "data": dataSerializeFunc,
                    "dataSrc": function (json) {
                        if (json.d) { json = json.d; }
                        //console.log($.parseJSON(json.data));
                        return $.parseJSON(json.data);
                    },
                },
                "bAutoWidth": false,
                "dom": dom,
                "buttons": ['copy', 'excel', 'csv', 'pdf', 'print' ],
                "colResize": {
                    "handleWidth": 10,
                    "resizeCallback": function(column) {
                        datatable_properties[name].columns[column.order].width = column.width;
                        setStorage(JSON.stringify(datatable_properties));
                    }
                },
                "columns": columns,
                "columnDefs": $.merge(columnDefs, convertColumnDef),
                "order": order,
                "fnDrawCallback": callback
            });

            // Apply the search
            $.each($('.input-filter', $datatable.table().header()), function () {
                var column = $datatable.column($(this).index());

                $('input', this).on('keyup change', function () {
                    if (column.search() !== this.value) {
                        column
                            .search(this.value)
                            .draw();
                    }
                });
            });
            //$datatable.columns().every(function () {
            //    var that = this;
            //    $('input', this.header()).on('keyup change', function () {
            //        if (that.search() !== this.value) {
            //            that.search(this.value).draw();
            //        }
                    
            //    });
            //});

            $('.txtKeyword').keyup(function () {
                $datatable.search($(this).val()).draw();
                console.log($datatable.rows().data())
            }).keydown(function(e){
                if(e.keyCode == 13) {
                    e.preventDefault();
                    return false;
                }
            });
            
            // show hide column
            var columnList = "<ul class='dropdown-list'>"
            columns.forEach(function (item, index) {
                var isVisible = (typeof(item.visible) !== 'undefined') ? item.visible : true;
                columnList += "<li><label><input type='checkbox' data-column='" + index + "' "+(isVisible ? "checked":"")+"/>" + item.label + "</label></li>";
            });
            columnList += "</ul>";

            $("#"+elementID+"")
                .parent()
                .find(".dropdown-container")
                .html("<div class='dropdown-label'>Show/Hide<i class='material-icons'>play_arrow</i></div>" + columnList);
            $(".dropdown-container").unbind("click").click(function (e) {
                var $this = $(this);
                $this.toggleClass("open");

                $('html').one('click', function (e) {
                    $this.removeClass("open");
                });
                e.stopPropagation();
            });

            $(".dropdown-container input[type='checkbox']").change(function () {

                // Get the column API object
                var column = $datatable.column($(this).attr('data-column'));
                
                // Toggle the visibility
                column.visible(!column.visible());

                datatable_properties[name].columns[$(this).attr('data-column')].visible = column.visible();
                setStorage(JSON.stringify(datatable_properties));
            });

            // asp.net modify
            $table.on('xhr.dt', function (e, settings, json) {
                if (json.d) {
                    var data = json.d;
                    json.d = undefined;
                    $.extend(json, data);
                }
            });

            return $datatable;
        }
    </script>