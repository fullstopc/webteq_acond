﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;

public partial class ctrl_ucUsrMemberLogin : System.Web.UI.UserControl
{
    #region "Properties"
    protected int memId;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #endregion

    #region "Methods"
    public void fill()
    {
        memId = Convert.ToInt32(Session[clsKey.SESSION_MEMBER_ID]);


        clsMember member = new clsMember();
        if (member.extractByID(memId))
        {
            txtContactTel.Text = member.contactTel;
            txtContactTelPrefix.Text = member.contactPrefix;
            txtName.Text = member.name;
            //txtAddrUnit.Text = member.addrUnit;

            hdnPassword.Value = member.loginPassword;

        }

        new Acknowledge(Page)
        {
            container = pnlAck,
            msg = litAck
        }.init();
    }

    protected void lnkbtnSaveComp_Click(object sender, EventArgs e)
    {
        if (Page.IsValid && Session[clsKey.SESSION_MEMBER_ID] != null)
        {
            memId = Convert.ToInt32(Session[clsKey.SESSION_MEMBER_ID]);

            clsMember member = new clsMember();
            member.ID = memId;
            member.contactTel = txtContactTel.Text.Trim();
            member.contactPrefix = txtContactTelPrefix.Text.Trim();
            //member.addrUnit = txtAddrUnit.Text.Trim();
            member.name = txtName.Text.Trim();

            bool isSuccess = true;
            bool isChange = false;

            if (!member.isSame())
            {
                if (member.update())
                {
                    isChange = true;
                }
                else
                {
                    isSuccess = false;
                }
            }

            if (!isSuccess)
            {
                Session[clsKey.SESSION_ERROR] = 1;
                Session[clsKey.SESSION_MSG] = "Something wrong happen. Please try again later.";
            }
            else if (isChange)
            {
                Session[clsKey.SESSION_EDITED] = 1;
                Session[clsKey.SESSION_MSG] = "Update successfully";
            }
            else if (!isChange && isSuccess)
            {
                Session[clsKey.SESSION_NOCHANGE] = 1;
                Session[clsKey.SESSION_MSG] = "No change.";
            }

            Response.Redirect(clsMis.getCurrentPageName2(true));




        }
    }

    #endregion

    #region Validation
    protected void cvEmail_ServerValidate(object source, ServerValidateEventArgs args)
    {
        memId = Convert.ToInt32(Session[clsKey.SESSION_MEMBER_ID]);
        
        CustomValidator cv = source as CustomValidator;
        TextBox txt = cv.NamingContainer.FindControl(cv.ControlToValidate) as TextBox;
        string value = txt.Text.Trim();

        DataTable members = new clsMember().getDataTable();
        var member = members.AsEnumerable()
                        .Where(x => Convert.ToString(x["member_email"]) == value &&
                                    Convert.ToInt32(x["member_id"]) != memId);

        args.IsValid = !member.Any();
    }
    protected void cvEmail_PreRender(object source, EventArgs e)
    {
        CustomValidator cv = source as CustomValidator;
        TextBox txt = cv.NamingContainer.FindControl(cv.ControlToValidate) as TextBox;

        if ((!Page.ClientScript.IsStartupScriptRegistered("Email")))
        {
            string strJS = null;
            strJS = "<script = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txt.ClientID + "', document.all['" + cv.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "Email", strJS, false);
        }
    }

    #endregion


    #region Password
    protected void lnkbtnSubmitPassword_Click(object sender, EventArgs e)
    {
        if (Page.IsValid && Session[clsKey.SESSION_MEMBER_ID] != null)
        {
            memId = Convert.ToInt32(Session[clsKey.SESSION_MEMBER_ID]);



            string password = hdnPassword.Value;
            string passwordNew = new clsMD5().encrypt(txtNewPwd.Text);
            bool isSame = !(string.Compare(password, passwordNew) != 0);
            bool isChange = false;
            bool isSuccess = true;

            if (!isSame)
            {
                clsMember member = new clsMember();
                member.ID = memId;
                member.loginPassword = passwordNew;
                member.updatedby = memId;

                if (member.update())
                {
                    isChange = true;
                }
                else
                {
                    isSuccess = false;
                }
            }

            if (!isSuccess)
            {
                Session[clsKey.SESSION_ERROR] = 1;
                Session[clsKey.SESSION_MSG] = "Something wrong happen. Please try again later.";
            }
            else if (isChange)
            {
                Session[clsKey.SESSION_EDITED] = 1;
                Session[clsKey.SESSION_MSG] = "Update successfully";
            }
            else if (!isChange && isSuccess)
            {
                Session[clsKey.SESSION_NOCHANGE] = 1;
                Session[clsKey.SESSION_MSG] = "No change.";
            }

            Response.Redirect(clsMis.getCurrentPageName2(true));

        }
    }

    protected void cvCurrentPwd_PreRender(object sender, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("usrmempwd"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtCurrentPwd.Text.Trim() + "', document.all['" + cvCurrentPwd.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "usrmempwd", strJS, false);
        }
    }

    protected void validateCurrentPwd_server(object source, ServerValidateEventArgs args)
    {
        if (!string.IsNullOrEmpty(txtCurrentPwd.Text.Trim()))
        {
            clsMD5 md5 = new clsMD5();
            string strCurPwd = md5.encrypt(txtCurrentPwd.Text.Trim());

            if (string.Compare(strCurPwd, hdnPassword.Value) != 0)
            {
                args.IsValid = false;

                cvCurrentPwd.ErrorMessage = "Current password does not match.";
            }
            else
            {
                args.IsValid = true;
            }
        }
    }

    #endregion Password
}
