﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrSideMenu.ascx.cs" Inherits="ctrl_ucUsrSideMenu" %>

<script type="text/javascript">
    function initMenu(targetMenu, targetMenuContainer, targetContainer) {
        if (!$('#' + targetMenu).hasClass('hypGrpNameCatSel')) {
            $('#' + targetContainer).hide();
            slideActionCat(targetMenuContainer, targetContainer);
        }
    }
</script>


<asp:Panel ID="pnlSubMenuOuter" runat="server" CssClass="divSubMenuOuter">
<asp:Panel ID="pnlSubMenuHdr" runat="server" CssClass="divSubMenuHdr" Visible="false">
    <asp:Literal ID="litSubMenuHdr" runat="server"></asp:Literal>
</asp:Panel>
<asp:Panel ID="pnlSubMenuList" runat="server" CssClass="divSubMenuList">
    <asp:Repeater ID="rptSubMenu" runat="server" OnItemDataBound="rptSubMenu_ItemDataBound">
        <ItemTemplate>
            <asp:Panel ID="pnlSubMenuLv1" runat="server">
                <asp:HyperLink ID="hypSubMenu" runat="server" CssClass="hypSubMenuLeft"></asp:HyperLink>
            <asp:Panel ID="pnlSubMenuLv2" runat="server" CssClass="divSubMenuHdrLv2" Visible="false">
                <asp:Repeater ID="rptSubMenuLv2" runat="server" OnItemDataBound="rptSubMenuLv2_ItemDataBound">
                    <ItemTemplate>
                        <div class="divSubMenuLv2">
                            <asp:HyperLink ID="hypSubMenuLv2" runat="server" CssClass="hypSubMenuLv2"></asp:HyperLink></div>
                    </ItemTemplate>
                    <SeparatorTemplate><span class="divSubMenuLv2Splitter"></span></SeparatorTemplate>
                </asp:Repeater>
                <span class="divSubMenuLv2Splitter"></span>
            </asp:Panel>
                </asp:Panel>
        </ItemTemplate>
        <%--<SeparatorTemplate>
            <asp:Panel ID="pnlSubMenuSpliter" runat="server" CssClass="divSubMenuSpliter"></asp:Panel>
        </SeparatorTemplate>--%>
    </asp:Repeater>
</asp:Panel>
    <%--<asp:Panel ID="pnlSubMenuSpliter" runat="server" CssClass="divSubMenuRightSpliter"></asp:Panel>--%>
</asp:Panel>
