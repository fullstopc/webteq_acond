﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmOrderDetails.ascx.cs" Inherits="ctrl_ucAdmOrderDetails" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>

<link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>jquery.datetimepicker.css" rel="Stylesheet" />
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jquery.js" type="text/javascript"></script>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jquery.datetimepicker.js" type="text/javascript"></script>

<script type="text/javascript">
    var txtCollectionDate;
    var hdnCollectionDate;

    function pageLoad() {
        initSect3();    
    }

    function initSect3() {
        txtCollectionDate = document.getElementById("<%= txtCollectionDate.ClientID %>");
        hdnCollectionDate = document.getElementById("<%= hdnCollectionDate.ClientID %>");

        $(function() {

            var date = new Date();
            var currentMonth = date.getMonth();
            var currentDate = date.getDate();
            var currentYear = date.getFullYear();

            $("#<% =txtCollectionDate.ClientID %>").datetimepicker({ format: 'd M Y  h:i a' });
            $("#<% =txtCollectionDate.ClientID %>").datetimepicker({ changeMonth: true });
            $("#<% =txtCollectionDate.ClientID %>").datetimepicker({ changeYear: true });

            var changeMonth = $("#<% =txtCollectionDate.ClientID %>").datetimepicker('option', 'changeMonth');
            var changeYear = $("#<% =txtCollectionDate.ClientID %>").datetimepicker('option', 'changeYear');

            $("#<% =txtCollectionDate.ClientID %>").datetimepicker('option', 'changeMonth', true);
            $("#<% =txtCollectionDate.ClientID %>").datetimepicker('option', 'changeYear', true);
            $("#<% =txtCollectionDate.ClientID %>").datetimepicker('option', 'changeYear', true);
        });
    }
</script>

<asp:Panel ID="pnlDelivery" runat="server" CssClass="divLoadingContainer">
    <asp:UpdatePanel ID="upnlDelivery" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="uprgDelivery" runat="server" AssociatedUpdatePanelID="upnlDelivery" DynamicLayout="true">
                <ProgressTemplate>
                    <uc:AdmLoading ID="ucAdmLoadingDelivery" runat="server" />
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="divDeliveryHeader">Delivery</div>
            <asp:Panel ID="pnlBillingDetails" runat="server" CssClass="divBillingDetails">
                <table cellpadding="0" cellspacing="0" class="formTbl2">
                    <thead>
                        <tr>
                            <td colspan="2"><span class="">Billing Details</span></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="trCompNameBill" runat="server" visible="false">
                            <td id="tdCompNameBill" runat="server" class="tdLabel nobr">Company Name:</td>
                            <td colspan="2" id="tdCompNameBillInput" runat="server" visible="false" class="tdMax">
                                <asp:TextBox ID="txtCompNameBill" runat="server" MaxLength="250" CssClass="text" ValidationGroup="delivery"></asp:TextBox><br />
                            </td>
                            <td id="tdCompNameBillReadOnly" runat="server" visible="false" class="tdMax">
                                <asp:Literal ID="litCompNameBill" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td id="tdNameBill" runat="server" class="tdLabel nobr">Contact Person:<span class="attention_compulsory">*</span></td>
                            <td colspan="2" id="tdNameBillInput" runat="server" visible="false" class="tdMax">
                                <asp:TextBox ID="txtNameBill" runat="server" MaxLength="250" CssClass="text" ValidationGroup="delivery"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="tfvNameBill" runat="server" ErrorMessage="Please enter contact person." ControlToValidate="txtNameBill" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>
                            </td>
                            <td id="tdNameBillReadOnly" runat="server" visible="false" class="tdMax">
                                <asp:Literal ID="litNameBill" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td id="tdAddBill" runat="server" class="tdLabel">Address:<span class="attention_compulsory">*</span></td>
                            <td colspan="2" id="tdAddBillInput" runat="server" visible="false">
                                <asp:TextBox ID="txtAddBill" runat="server" MaxLength="250" CssClass="text_big" ValidationGroup="delivery"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="rfvAddBill" runat="server" ErrorMessage="Please enter address." ControlToValidate="txtAddBill" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>
                            </td>
                            <td id="tdAddBillReadOnly" runat="server" visible="false">
                                <asp:Literal ID="litAddBill" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td id="tdPoscodeBill" runat="server" class="tdLabel nobr">Postal Code:<span class="attention_compulsory">*</span></td>
                            <td colspan="2" id="tdPoscodeBillInput" runat="server" visible="false">
                                <asp:TextBox ID="txtPoscodeBill" runat="server" MaxLength="20" CssClass="text" ValidationGroup="delivery"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="rfvPoscodeBill" runat="server" ErrorMessage="Please enter postal code." ControlToValidate="txtPoscodeBill" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>
                            </td>
                            <td id="tdPoscodeBillReadOnly" runat="server" visible="false">
                                <asp:Literal ID="litPoscodeBill" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td id="tdCityBill" runat="server" class="tdLabel">City:</td>
                            <td colspan="2" id="tdCityBillDdl" runat="server" visible="false"><asp:DropDownList ID="ddlCityBill" runat="server" CssClass="ddl" ValidationGroup="delivery" AutoPostBack="true" OnSelectedIndexChanged="ddlCityBill_SelectedIndexChanged"></asp:DropDownList></td>
                            <td id="tdCityBillInput" runat="server" visible="false"><asp:TextBox ID="txtCityBill" runat="server" MaxLength="250" CssClass="text" ValidationGroup="delivery"></asp:TextBox></td>
                            <td id="tdCityBillReadOnly" runat="server" visible="false"><asp:Literal ID="litCityBill" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td id="tdCountryBill" runat="server" class="tdLabel">Country:<span class="attention_compulsory">*</span></td>
                            <td colspan="2" id="tdCountryBillInput" runat="server" visible="false">
                                <asp:DropDownList ID="ddlCountryBill" runat="server" CssClass="ddl" ValidationGroup="delivery" AutoPostBack="true" OnSelectedIndexChanged="ddlCountryBill_SelectedIndexChanged"></asp:DropDownList><br />
                                <asp:RequiredFieldValidator ID="rfvCountryBill" runat="server" ErrorMessage="Please select one country." ControlToValidate="ddlCountryBill" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>
                            </td>
                            <td id="tdCountryBillTxt" runat="server" visible="false">
                                <asp:TextBox ID="txtCountryBill" runat="server" CssClass="text" MaxLength="250" ValidationGroup="delivery"></asp:TextBox><br />
                            </td>
                            <td id="tdCountryBillReadOnly" runat="server" visible="false"><asp:Literal ID="litCountryBill" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td id="tdStateBill" runat="server" class="tdLabel">State:<span class="attention_compulsory">*</span></td>
                            <td colspan="2" id="tdStateBillDdl" runat="server" visible="false">
                                <asp:DropDownList ID="ddlStateBill" runat="server" CssClass="ddl" ValidationGroup="delivery" AutoPostBack="true" OnSelectedIndexChanged="ddlStateBill_SelectedIndexChanged"></asp:DropDownList><br />
                                <asp:RequiredFieldValidator ID="rfvStateBillDll" runat="server" ErrorMessage="Please select one state." ControlToValidate="ddlStateBill" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>
                            </td>
                            <td id="tdStateBillTxt" runat="server" visible="false">
                                <asp:TextBox ID="txtStateBill" runat="server" CssClass="text" MaxLength="250" ValidationGroup="delivery"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="rfvStateBillTxt" runat="server" ErrorMessage="Please enter value." ControlToValidate="txtStateBill" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>
                            </td>
                            <td id="tdStateBillReadOnly" runat="server" visible="false"><asp:Literal ID="litStateBill" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td id="tdContactBill" runat="server" class="tdLabel">Contact No.:<span class="attention_compulsory">*</span></td>
                            <td colspan="2" id="tdContactBillInput" runat="server" visible="false">
                                <asp:TextBox ID="txtContactNoBill" runat="server" MaxLength="20" CssClass="text" ValidationGroup="delivery"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="rfvContactNoBill" runat="server" ErrorMessage="Please enter contact no." ControlToValidate="txtContactNoBill" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>
                            </td>
                            <td id="tdContactBillReadOnly" runat="server" visible="false">
                                <asp:Literal ID="litContactNoBill" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr id="trEmailBill" runat="server" visible="false">
                            <td id="tdEmailBill" runat="server" class="tdLabel">Email:<span class="attention_compulsory">*</span></td>
                            <td colspan="2" id="tdEmailBillInput" runat="server" visible="false">
                                <asp:TextBox ID="txtEmailBill" runat="server" MaxLength="250" CssClass="text" ValidationGroup="delivery"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="rfvEmailBill" runat="server" ErrorMessage="Please enter email" ControlToValidate="txtEmailBill" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>
                            </td>
                            <td id="tdEmailBillReadOnly" runat="server" visible="false">
                                <asp:Literal ID="litEmailBill" runat="server"></asp:Literal>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlDeliveryDetails" runat="server" CssClass="divDeliveryDetails">
                <table id="tblDelivery" cellpadding="0" cellspacing="0" border="0" class="formTbl2">
                    <thead>
                        <tr>
                            <td class="tdLabelNor2 nobr"><span class="">Delivery Details</span></td>
                            <td class="tdMax">
                                <asp:DropDownList ID="ddlDelivery" runat="server" CssClass="ddl" AutoPostBack="true" OnSelectedIndexChanged="ddlDelivery_SelectedIndexChanged" Visible="false"></asp:DropDownList> 
                                <asp:LinkButton ID="lnkbtnSame" runat="server" Text="Same as billing details" ToolTip="Same as billing details" OnClick="lnkbtnSame_Click" CausesValidation="false"></asp:LinkButton>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="trNeedShipping" runat="server" visible="false">
                            <td colspan="2" class="tdShipNeed">I need shipping.</td>
                        </tr>
                    </tbody>
                    <tbody id="trDeliveryDetails" runat="server">
                        <tr id="trCompNameShip" runat="server" visible="false">
                            <td id="tdCompNameShip" runat="server" class="tdLabel nobr">Company Name:</td>
                            <td colspan="2" id="tdCompNameShipInput" runat="server" visible="false" class="tdMax">
                                <asp:TextBox ID="txtCompNameShip" runat="server" MaxLength="250" CssClass="text" ValidationGroup="delivery"></asp:TextBox>
                            </td>
                            <td id="tdCompNameShipReadOnly" runat="server" visible="false" class="tdMax"><asp:Literal ID="litCompNameShip" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td id="tdNameShip" runat="server" class="tdLabel nobr">Contact Person:<span class="attention_compulsory">*</span></td>
                            <td colspan="2" id="tdNameShipInput" runat="server" visible="false" class="tdMax">
                                <asp:TextBox ID="txtNameShip" runat="server" MaxLength="250" CssClass="text" ValidationGroup="delivery"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvNameShip" runat="server" ErrorMessage="<br />Please enter contact person." ControlToValidate="txtNameShip" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>
                            </td>
                            <td id="tdNameShipReadOnly" runat="server" visible="false" class="tdMax"><asp:Literal ID="litNameShip" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td id="tdAddShip" runat="server" class="tdLabel">Address:<span class="attention_compulsory">*</span></td>
                            <td colspan="2" id="tdAddShipInput" runat="server" visible="false">
                                <asp:TextBox ID="txtAddShip" runat="server" MaxLength="250" CssClass="text_big" ValidationGroup="delivery"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvAddShip" runat="server" ErrorMessage="<br />Please enter address." ControlToValidate="txtAddShip" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>
                            </td>
                            <td id="tdAddShipReadOnly" runat="server" visible="false"><asp:Literal ID="litAddShip" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td id="tdPoscodeShip" runat="server" class="tdLabel nobr">Postal Code:<span class="attention_compulsory">*</span></td>
                            <td colspan="2" id="tdPoscodeShipInput" runat="server" visible="false">
                                <asp:TextBox ID="txtPoscodeShip" runat="server" MaxLength="20" CssClass="text" ValidationGroup="delivery"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvPoscodeShip" runat="server" ErrorMessage="<br />Please enter postal code." ControlToValidate="txtPoscodeShip" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>
                            </td>
                            <td id="tdPoscodeShipReadOnly" runat="server" visible="false"><asp:Literal ID="litPoscodeShip" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td id="tdCityShip" runat="server" class="tdLabel">City:</td>
                            <td colspan="2" id="tdCityShipDdl" runat="server" visible="false"><asp:DropDownList ID="ddlCityShip" runat="server" CssClass="ddl" ValidationGroup="delivery" AutoPostBack="true" OnSelectedIndexChanged="ddlCityShip_SelectedIndexChanged"></asp:DropDownList></td>
                            <td id="tdCityShipInput" runat="server" visible="false">
                                <asp:TextBox ID="txtCityShip" runat="server" MaxLength="50" CssClass="text" ValidationGroup="delivery"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="rfvCityShip" runat="server" ErrorMessage="<br />Please enter city." ControlToValidate="txtCityShip" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>--%>
                            </td>
                            <td id="tdCityShipReadOnly" runat="server" visible="false"><asp:Literal ID="litCityShip" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td id="tdCountryShip" runat="server" class="tdLabel">Country:<span class="attention_compulsory">*</span></td>
                            <td colspan="2" id="tdCountryShipInput" runat="server" visible="false">
                                <asp:DropDownList ID="ddlCountryShip" runat="server" CssClass="ddl" AutoPostBack="true" OnSelectedIndexChanged="ddlCountryShip_SelectedIndexChanged" ValidationGroup="delivery"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvCountryShip" runat="server" ErrorMessage="<br />Please select one country." ControlToValidate="ddlCountryShip" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>
                            </td>
                            <td id="tdCountryShipTxt" runat="server" visible="false">
                                <asp:TextBox ID="txtCountryShip" runat="server" CssClass="text" MaxLength="250"></asp:TextBox>
                            </td>
                            <td id="tdCountryShipReadOnly" runat="server" visible="false"><asp:Literal ID="litCountryShip" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td id="tdStateShip" runat="server" class="tdLabel">State:<span class="attention_compulsory">*</span></td>
                            <td colspan="2" id="tdStateShipDdl" runat="server" visible="false">
                                <asp:DropDownList ID="ddlStateShip" runat="server" CssClass="ddl" AutoPostBack="true" OnSelectedIndexChanged="ddlStateShip_SelectedIndexChanged"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvStateDdlShip" runat="server" ErrorMessage="<br />Please select one state." ControlToValidate="ddlStateShip" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>
                            </td>
                            <td id="tdStateShipTxt" runat="server" visible="false">
                                <asp:TextBox ID="txtStateShip" runat="server" CssClass="text" MaxLength="250"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvStateTxtShip" runat="server" ErrorMessage="<br />Please enter state." ControlToValidate="txtStateShip" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>
                            </td>
                            <td id="tdStateShipReadOnly" runat="server" visible="false"><asp:Literal ID="litStateShip" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td id="tdContactShip" runat="server" class="tdLabel nobr">Contact No.:<span class="attention_compulsory">*</span></td>
                            <td colspan="2" id="tdContactShipInput" runat="server" visible="false">
                                <asp:TextBox ID="txtContactNoShip" runat="server" MaxLength="20" CssClass="text" ValidationGroup="delivery"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvContactNoShip" runat="server" ErrorMessage="<br />Please enter contact no." ControlToValidate="txtContactNoShip" Display="Dynamic" CssClass="errmsg" ValidationGroup="delivery"></asp:RequiredFieldValidator>
                            </td>
                            <td id="tdContactShipReadOnly" runat="server" visible="false"><asp:Literal ID="litContactNoShip" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td id="tdCollectDate" runat="server" class="tdLabel nobr">Collection Date:</td>
                            <td colspan="2" id="tdCollectDateInput" runat="server" class="tdMax" visible="false">
                                <asp:TextBox ID="txtCollectionDate" runat="server" CssClass="text"></asp:TextBox>
                                <asp:HiddenField ID="hdnCollectionDate" runat="server" />
                            </td>
                            <td class="" id="tdCollectDateInputReadOnly" runat="server" visible="false"><asp:Literal ID="litCollectionDate" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="trEmailShip" runat="server" visible="false">
                            <td id="tdEmailShip" runat="server" class="tdLabel">Email:</td>
                            <td colspan="2" id="tdEmailShipInput" runat="server" visible="false">
                                <asp:TextBox ID="txtEmailShip" runat="server" MaxLength="250" CssClass="text" ValidationGroup="delivery"></asp:TextBox><br />         
                                <asp:RegularExpressionValidator ID="revEmailShip" runat="server" ErrorMessage="<br />Please enter valid Email." Display="Dynamic" ControlToValidate="txtEmailShip" CssClass="errmsg" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$" ValidationGroup="delivery"></asp:RegularExpressionValidator>
                            </td>
                            <td id="tdEmailShipReadOnly" runat="server" visible="false"><asp:Literal ID="litEmailShip" runat="server"></asp:Literal></td>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr id="trRemarks" runat="server" visible="false">
                            <td id="tdRemarkShip" runat="server" class="tdLabel">Remark:</td>
                            <td id="tdRemarkShipInput" runat="server" visible="false" class="tdMax"><asp:TextBox ID="txtRemark" runat="server" MaxLength="1000" CssClass="text_big" ValidationGroup="delivery"></asp:TextBox></td>
                            <td id="tdRemarkShipReadOnly" runat="server" visible="false" class="tdMax"><asp:Literal ID="litRemark" runat="server"></asp:Literal></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlDeliveryMessageDetails" runat="server" CssClass="divBillingDetails">
                <table cellpadding="0" cellspacing="0" class="formTbl2">
                    <thead>
                        <tr>
                            <td colspan="2" class="nobr">Delivery Message Details</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="tdLabel nobr">Delivery Date:</td>
                            <td class="tdLabel">
                                <asp:Literal ID="litDeliveryDate" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel nobr">Delivery Time:</td>
                            <td class="tdLabel">
                                <asp:Literal ID="litDeliveryTime" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel nobr">Recipient Name (To):</td>
                            <td class="tdLabel">
                                <asp:Literal ID="litDeliveryRecipient" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel nobr">Sender Name (From):</td>
                            <td class="tdLabel">
                                <asp:Literal ID="litDeliverySender" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel nobr">Greeting Message:</td>
                            <td class="tdLabel">
                                <asp:Literal ID="litDeliveryMsg" runat="server"></asp:Literal>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlOrderDeliveryMessage" runat="server" CssClass="divBillingDetails" Visible="false">
                <table cellpadding="0" cellspacing="0" border="0" class="formTbl2">
                    <thead>
                        <tr class="nobr">
                            <td colspan="2" class="tdLabelNor2 nobr" style="padding-bottom:10px;"><span class="">Delivery Message Details</span></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="td1" runat="server" class="tdLabel nobr">Message From:</td>
                            <td id="td2" runat="server" class="tdLabel tdMax2">
                                <asp:Literal ID="litMsgFrom" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td id="td3" runat="server" class="tdLabel nobr">Message To:</td>
                            <td id="td4" runat="server" class="tdLabel tdMax2">
                                <asp:Literal ID="litMsgTo" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td id="td5" runat="server" class="tdLabel nobr">Message Content:</td>
                            <td id="td6" runat="server" class="tdLabel tdMax2">
                                <asp:Literal ID="litMsgContent" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td id="td7" runat="server" class="tdLabel nobr">Special Instruction:</td>
                            <td id="td8" runat="server" class="tdLabel tdMax2">
                                <asp:Literal ID="litSpecialInstruction" runat="server"></asp:Literal>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Panel ID="pnlButton" runat="server" CssClass="divFormBtnRight">
    <asp:LinkButton ID="lnkbtnRevert" runat="server" Text="Revert to Initial Input" ToolTip="Revert to Initial Input" CssClass="btn btnRevert" CausesValidation="false" OnClick="lnkbtnRevert_Click"></asp:LinkButton>
    <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="delivery"></asp:LinkButton>
</asp:Panel>