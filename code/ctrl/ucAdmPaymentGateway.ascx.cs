﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;
using System.Linq;

public partial class ctrl_ucAdmPaymentGateway : System.Web.UI.UserControl
{
    #region "Properties"
    protected clsConfig config = new clsConfig();
    protected clsMis mis = new clsMis();
    protected clsPaymentGateway paygateway = new clsPaymentGateway();
    protected DataView dvBankName = null;
    protected DataView dvConfig = null;
    protected DataSet dsConfig = null;
    protected ListItem ddlDefaultItem = null;

    protected bool boolSuccess = false;
    protected bool boolEdited = false;
    protected int intRecordAffected = 0;
     
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setPageProperties();
            fill();
        }
    }
    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            boolEdited = false;
            boolSuccess = true;
            intRecordAffected = 0;
            if (divPayPal.Visible) { savePaypal(); }
            if (divMolpay.Visible) { saveMolpay(); }
            if (divIPay88.Visible) { saveIpay88(); }
            if (divIPay88_eNets.Visible) { saveIpay88ENets(); }
            if (divEGhl.Visible) { saveEGhl(); }
            
            if (!boolSuccess)
            {
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to update Payment gateway. Please try again.</div>";
                Response.Redirect(currentPageName);
            }
            else
            {
                if (boolEdited)
                {
                    Session["EDITED_PAYMENTGATEWAY"] = 1;
                }
                else
                {
                    Session["EDITED_PAYMENTGATEWAY"] = 1;
                    Session["NOCHANGE"] = 1;
                }

                Response.Redirect(currentPageName);
            }
        }
    }
    protected void lnkbtnSaveBankTransfer_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strBankValue = ddlBankName.SelectedValue;
            string strBankName = txtBankName.Text.Trim();
            string strBankAccName = txtBankAccName.Text.Trim();
            string strBankAccNum = txtBankAccNum.Text.Trim();
            
            int intRecordAffected = paygateway.addBankTrans(strBankValue, strBankName,strBankAccName, strBankAccNum, int.Parse(Session["ADMID"].ToString()));
            bindRptData();
            fillBankTransfer();
        }
    }
    protected void rptBankTrans_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strBankSel = DataBinder.Eval(e.Item.DataItem, "LIST_NAME").ToString();
            string strBankName = DataBinder.Eval(e.Item.DataItem, "BT_NAME").ToString();
            string strBankAccName = DataBinder.Eval(e.Item.DataItem, "BT_BANKACC_NAME").ToString();
            string strBankAccNum = DataBinder.Eval(e.Item.DataItem, "BT_BANKACC_NUM").ToString();

            Literal litNo = (Literal)e.Item.FindControl("litNo");
            Literal litBankSel = (Literal)e.Item.FindControl("litBankSel");
            Literal litBankName = (Literal)e.Item.FindControl("litBankName");
            Literal litBankAccName = (Literal)e.Item.FindControl("litBankAccName");
            Literal litBankAccNum = (Literal)e.Item.FindControl("litBankAccNum");

            litNo.Text = (e.Item.ItemIndex + 1).ToString();
            litBankSel.Text = strBankSel;
            litBankName.Text = strBankName;
            litBankAccName.Text = strBankAccName;
            litBankAccNum.Text = strBankAccNum;

            LinkButton lnkbtnDelete = (LinkButton)e.Item.FindControl("lnkbtnDelete");
            lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteArea.Text") + "');";



        }
    }

    protected void rptBankTrans_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdDelete")
        {
            int intId = int.Parse(e.CommandArgument.ToString());
            int intRecordAffected = paygateway.deleteBankTransId(intId);
            bindRptData();
            fillBankTransfer();
        }
    }
    #endregion

    
    #region "Methods"
    public void fill()
    {
        fillBankTransfer();
        if (divPayPal.Visible) { fillPaypal(); }
        if (divMolpay.Visible) { fillMolpay(); }
        if (divIPay88.Visible) { fillIPay88(); }
        if (divIPay88_eNets.Visible) { fillIPay88ENets(); }
        if (divEGhl.Visible) { fillEGhl(); }

    }
    protected void setPageProperties()
    {
        divPayPal.Visible = false;
        divMolpay.Visible = false;
        divIPay88.Visible = false;
        divIPay88_eNets.Visible = false;
        divEGhl.Visible = false;

        divSplitPaypal.Visible = false;
        divSplitMolpay.Visible = false;
        divSplitIPay88.Visible = false;
        divSplitIPay88ENets.Visible = false;
        divSplitEGhl.Visible = false;

        if (Session[clsAdmin.CONSTPROJECTPAYMENTGATEWAY] != null && Session[clsAdmin.CONSTPROJECTPAYMENTGATEWAY] != "")
        {
            string[] strPaymentType = Session[clsAdmin.CONSTPROJECTPAYMENTGATEWAY].ToString().Split(clsAdmin.CONSTDEFAULTSEPERATOR);
            if (strPaymentType.Length > 0)
            {
                for (int i = 0; i < strPaymentType.Length; i++)
                {
                    switch (strPaymentType[i])
                    {
                        case clsConfig.CONSTNAMEPAYMENTTYPENAME_PAYPAL:
                            divPayPal.Visible = true;
                            divSplitPaypal.Visible = true;
                            break;
                        case clsConfig.CONSTNAMEPAYMENTTYPENAME_MOLPAY:
                            divMolpay.Visible = true;
                            divSplitMolpay.Visible = true;
                            break;
                        case clsConfig.CONSTNAMEPAYMENTTYPENAME_IPAY88:
                            divIPay88.Visible = true;
                            divSplitIPay88.Visible = true;
                            break;
                        case clsConfig.CONSTNAMEPAYMENTTYPENAME_IPAY88ENETS:
                            divIPay88_eNets.Visible = true;
                            divSplitIPay88ENets.Visible = true;
                            break;
                        case clsConfig.CONSTNAMEPAYMENTTYPENAME_EGHL:
                            divEGhl.Visible = true;
                            divSplitEGhl.Visible = true;
                            break;

                    }
                }

            }
        }
        
        bindRptData();
    }

    protected void fillBankTransfer()
    {
        dvBankName = new DataView(mis.getListByListGrp(clsConfig.CONSTLISTGRPBANKNAME,1).Tables[0]);

        ddlBankName.DataSource = dvBankName;
        ddlBankName.DataTextField = "LIST_NAME";
        ddlBankName.DataValueField = "LIST_ID";
        ddlBankName.DataBind();
        ddlBankName.Items.Insert(0, ddlDefaultItem);

        DataView dvBankTrans = new DataView(paygateway.getBankTransferList().Tables[0]);

        if (dvBankTrans.Count > 0)
        {
            rptBankTrans.DataSource = dvBankTrans;
            rptBankTrans.DataBind();
        }
        else
        {
            rptBankTrans.DataSource = null;
            rptBankTrans.DataBind();
        }
        

    }
    protected void fillMolpay()
    {
        DataRow[] foundRow = dsConfig.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPMOLPAYSETTINGS + "'");
        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYMENTGATEWAY_ACTIVE, true) == 0) { chkMolpayActive.Checked = Convert.ToBoolean(int.Parse(row["CONF_VALUE"].ToString())); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYMENTGATEWAY_NAME, true) == 0) { txtMolpayName.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEMOLPAY_MOLPAY, true) == 0) { txtMolpay.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEMOLPAY_MERCHANTID, true) == 0) { txtMolpayMerchantID.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEMOLPAY_VERIFYKEY, true) == 0) { txtMolpayVerifyKey.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEMOLPAY_CUR, true) == 0) { txtMolpayCur.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEMOLPAY_COUNTRY, true) == 0) { txtMolpayCountry.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEMOLPAY_LANGCODE, true) == 0) { txtMolpayLangCode.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEMOLPAY_RETURN, true) == 0) { txtMolpayReturn.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEMOLPAY_RETURNPIN, true) == 0) { txtMolpayReturnPin.Text = row["CONF_VALUE"].ToString(); }
        }
    }
    protected void fillPaypal()
    {
        ddlPaypalUseSandBox.Items.Insert(0, new ListItem("true", "true"));
        ddlPaypalUseSandBox.Items.Insert(1, new ListItem("false", "false"));
        ddlPaypalUseSandBox.SelectedValue = "false";

        DataRow[] foundRow = dsConfig.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPPAYPALSETTINGS + "'");


        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYMENTGATEWAY_ACTIVE, true) == 0) { chkPaypalActive.Checked = Convert.ToBoolean(int.Parse(row["CONF_VALUE"].ToString())); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYMENTGATEWAY_NAME, true) == 0) { txtPaypalName.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYPAL_USESANDBOX, true) == 0) { ddlPaypalUseSandBox.SelectedValue = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYPAL_PAYPALSANDBOX, true) == 0) { txtPaypalSandboxUrl.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYPAL_PAYPAL, true) == 0) { txtPaypalUrl.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYPAL_CURRENCYCODE, true) == 0) { txtPaypalCurrency.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYPAL_CMD, true) == 0) { txtPaypalCmd.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYPAL_BUSINESS, true) == 0) { txtPaypalBusinessEmail.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYPAL_CANCELRETURN, true) == 0) { txtPaypalReturnUrl.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYPAL_PDTTOKEN, true) == 0) { txtPaypalPDTToken.Text = row["CONF_VALUE"].ToString(); }
        }
    }
    protected void fillIPay88()
    {
        DataRow[] foundRow = dsConfig.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPIPAY88SETTINGS + "'");
        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYMENTGATEWAY_ACTIVE, true) == 0) { chkIPay88Active.Checked = Convert.ToBoolean(int.Parse(row["CONF_VALUE"].ToString())); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYMENTGATEWAY_NAME, true) == 0) { txtIPay88Name.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_IPAY88ENTRYPAGE, true) == 0) { txtIPay88EntryPage.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_IPAY88ENQUIRYPAGE, true) == 0) { txtIPay88EnquiryPage.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_RESPONSEURL, true) == 0) { txtIPay88ResUrl.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_REQUESTURL, true) == 0) { txtIPay88ReqUrl.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_MERCHANTKEY, true) == 0) { txtIPay88MerchantKey.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_MERCHANTCODE, true) == 0) { txtIPay88MerchantCode.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_PAYMENTID, true) == 0) { txtIPay88PayId.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_CURRENCY, true) == 0) { txtIPay88Cur.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_PRODDESC, true) == 0) { txtIPay88ProdDesc.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_BACKENDURL, true) == 0) { txtIPay88BackendUrl.Text = row["CONF_VALUE"].ToString(); }
        }
    }
    protected void fillIPay88ENets()
    {
        DataRow[] foundRow = dsConfig.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPIPAY88ENETSSETTINGS + "'");
        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYMENTGATEWAY_ACTIVE, true) == 0) { chkIPay88ENetsActive.Checked = Convert.ToBoolean(int.Parse(row["CONF_VALUE"].ToString())); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYMENTGATEWAY_NAME, true) == 0) { txtIPay88ENetsName.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_IPAY88ENTRYPAGE, true) == 0) { txtIPay88ENetsEntryPage.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_IPAY88ENQUIRYPAGE, true) == 0) { txtIPay88ENetsEnquiryPage.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_RESPONSEURL, true) == 0) { txtIPay88ENetsResUrl.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_REQUESTURL, true) == 0) { txtIPay88ENetsReqUrl.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_MERCHANTKEY, true) == 0) { txtIPay88ENetsMerchantKey.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_MERCHANTCODE, true) == 0) { txtIPay88ENetsMerchantCode.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_PAYMENTID, true) == 0) { txtIPay88ENetsPayId.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_CURRENCY, true) == 0) { txtIPay88ENetsCur.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_PRODDESC, true) == 0) { txtIPay88ENetsProdDesc.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEIPAY88_BACKENDURL, true) == 0) { txtIPay88ENetsBackendUrl.Text = row["CONF_VALUE"].ToString(); }
        }
    }
    protected void fillEGhl()
    {
        DataRow[] foundRow = dsConfig.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPEGHLSETTINGS + "'");
        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYMENTGATEWAY_ACTIVE, true) == 0) { chkEGhlActive.Checked = Convert.ToBoolean(int.Parse(row["CONF_VALUE"].ToString())); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYMENTGATEWAY_NAME, true) == 0) { txtEGhlName.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEGHL_USETESTURL, true) == 0) { txtEGhlUseTestUrl.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEGHL_TESTPAYMENTURL, true) == 0) { txtEGhlTestPayUrl.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEGHL_PAYMENTURL, true) == 0) { txtEGhlPayUrl.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEGHL_TRANSACTIONTYPE, true) == 0) { txtEGhlTransType.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEGHL_PAYMENTMETHOD, true) == 0) { txtEGhlPayMethod.Text = row["CONF_VALUE"].ToString();}
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEGHL_MERCHANTID, true) == 0) { txtEGhlMerchantId.Text = row["CONF_VALUE"].ToString();}
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEGHL_MERCHANTPWD, true) == 0) { txtEGhlMerchantPwd.Text = row["CONF_VALUE"].ToString();}
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEGHL_MERCHANTNAME, true) == 0) { txtEGhlMerchantName.Text = row["CONF_VALUE"].ToString();}
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEGHL_RETURNURL, true) == 0) { txtEGhlReturnUrl.Text = row["CONF_VALUE"].ToString();}
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEGHL_CALLBACKURL, true) == 0) { txtEGhlCallBackUrl.Text = row["CONF_VALUE"].ToString();}
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEGHL_CURRENCYCODE, true) == 0) { txtEGhlCurrencyCode.Text = row["CONF_VALUE"].ToString();}
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEEGHL_PAGETIMEOUT, true) == 0) { txtEGhlTimeout.Text = row["CONF_VALUE"].ToString();}
        }
    }

    protected void bindRptData()
    {
        txtBankAccNum.Text = "";
        txtBankAccName.Text = "";
        txtBankName.Text = "";

        dsConfig = config.getItemList(1);
        dvConfig = new DataView(dsConfig.Tables[0]);
        ddlDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
    }

    protected void savePaypal()
    {
        string confGroup = clsConfig.CONSTGROUPPAYPALSETTINGS;
        string[,] confArray = {
                                { clsConfig.CONSTNAMEPAYMENTGATEWAY_ACTIVE, (chkPaypalActive.Checked) ? "1" : "0"},
                                { clsConfig.CONSTNAMEPAYMENTGATEWAY_NAME , txtPaypalName.Text.Trim() } , 
                                { clsConfig.CONSTNAMEPAYPAL_USESANDBOX , ddlPaypalUseSandBox.SelectedValue } , 
                                { clsConfig.CONSTNAMEPAYPAL_PAYPALSANDBOX , txtPaypalSandboxUrl.Text.Trim() } , 
                                { clsConfig.CONSTNAMEPAYPAL_PAYPAL , txtPaypalUrl.Text.Trim()} , 
                                { clsConfig.CONSTNAMEPAYPAL_CURRENCYCODE , txtPaypalCurrency.Text.Trim() } , 
                                { clsConfig.CONSTNAMEPAYPAL_CMD , txtPaypalCmd.Text.Trim() } , 
                                { clsConfig.CONSTNAMEPAYPAL_BUSINESS , txtPaypalBusinessEmail.Text.Trim() } , 
                                { clsConfig.CONSTNAMEPAYPAL_CANCELRETURN , txtPaypalReturnUrl.Text.Trim() } , 
                                { clsConfig.CONSTNAMEPAYPAL_PDTTOKEN , txtPaypalPDTToken.Text.Trim() }
                              };

        saveConfig(confArray, confGroup);
    }
    protected void saveMolpay()
    {
        string confGroup = clsConfig.CONSTGROUPMOLPAYSETTINGS;
        string[,] confArray = {
                                { clsConfig.CONSTNAMEPAYMENTGATEWAY_ACTIVE, (chkMolpayActive.Checked) ? "1" : "0"},
                                { clsConfig.CONSTNAMEPAYMENTGATEWAY_NAME , txtMolpayName.Text.Trim() } , 
                                { clsConfig.CONSTNAMEMOLPAY_MOLPAY , txtMolpay.Text.Trim() } , 
                                { clsConfig.CONSTNAMEMOLPAY_MERCHANTID , txtMolpayMerchantID.Text.Trim() } , 
                                { clsConfig.CONSTNAMEMOLPAY_VERIFYKEY , txtMolpayVerifyKey.Text.Trim()} , 
                                { clsConfig.CONSTNAMEMOLPAY_CUR , txtMolpayCur.Text.Trim() } , 
                                { clsConfig.CONSTNAMEMOLPAY_COUNTRY , txtMolpayCountry.Text.Trim() } , 
                                { clsConfig.CONSTNAMEMOLPAY_LANGCODE , txtMolpayLangCode.Text.Trim() } , 
                                { clsConfig.CONSTNAMEMOLPAY_RETURN , txtMolpayReturn.Text.Trim() } , 
                                { clsConfig.CONSTNAMEMOLPAY_RETURNPIN , txtMolpayReturnPin.Text.Trim() }
                              };

        saveConfig(confArray, confGroup);
    }
    protected void saveIpay88()
    {
        string confGroup = clsConfig.CONSTGROUPIPAY88SETTINGS;
        string[,] confArray = {
                                { clsConfig.CONSTNAMEPAYMENTGATEWAY_ACTIVE, (chkIPay88Active.Checked) ? "1" : "0"},
                                { clsConfig.CONSTNAMEPAYMENTGATEWAY_NAME , txtIPay88Name.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_IPAY88ENTRYPAGE , txtIPay88EntryPage.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_IPAY88ENQUIRYPAGE , txtIPay88EnquiryPage.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_RESPONSEURL , txtIPay88ResUrl.Text.Trim()} , 
                                { clsConfig.CONSTNAMEIPAY88_REQUESTURL , txtIPay88ReqUrl.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_MERCHANTKEY , txtIPay88MerchantKey.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_MERCHANTCODE , txtIPay88MerchantCode.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_PAYMENTID , txtIPay88PayId.Text.Trim() } ,
                                { clsConfig.CONSTNAMEIPAY88_CURRENCY , txtIPay88Cur.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_PRODDESC , txtIPay88ProdDesc.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_BACKENDURL , txtIPay88BackendUrl.Text.Trim() }
                              };

        saveConfig(confArray, confGroup);
    }
    protected void saveIpay88ENets()
    {
        string confGroup = clsConfig.CONSTGROUPIPAY88ENETSSETTINGS;
        string[,] confArray = {
                                { clsConfig.CONSTNAMEPAYMENTGATEWAY_ACTIVE, (chkIPay88ENetsActive.Checked) ? "1" : "0"},
                                { clsConfig.CONSTNAMEPAYMENTGATEWAY_NAME , txtIPay88ENetsName.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_IPAY88ENTRYPAGE , txtIPay88ENetsEntryPage.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_IPAY88ENQUIRYPAGE , txtIPay88ENetsEnquiryPage.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_RESPONSEURL , txtIPay88ENetsResUrl.Text.Trim()} , 
                                { clsConfig.CONSTNAMEIPAY88_REQUESTURL , txtIPay88ENetsReqUrl.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_MERCHANTKEY , txtIPay88ENetsMerchantKey.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_MERCHANTCODE , txtIPay88ENetsMerchantCode.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_PAYMENTID , txtIPay88ENetsPayId.Text.Trim() } ,
                                { clsConfig.CONSTNAMEIPAY88_CURRENCY , txtIPay88ENetsCur.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_PRODDESC , txtIPay88ENetsProdDesc.Text.Trim() } , 
                                { clsConfig.CONSTNAMEIPAY88_BACKENDURL , txtIPay88ENetsBackendUrl.Text.Trim() }
                              };

        saveConfig(confArray, confGroup);
    }
    protected void saveEGhl()
    {
        string confGroup = clsConfig.CONSTGROUPEGHLSETTINGS;
        string[,] confArray = {
                                { clsConfig.CONSTNAMEPAYMENTGATEWAY_ACTIVE, (chkEGhlActive.Checked) ? "1" : "0"},
                                { clsConfig.CONSTNAMEPAYMENTGATEWAY_NAME , txtEGhlName.Text.Trim() } , 
                                { clsConfig.CONSTNAMEEGHL_USETESTURL , txtEGhlUseTestUrl.Text.Trim() } , 
                                { clsConfig.CONSTNAMEEGHL_TESTPAYMENTURL , txtEGhlTestPayUrl.Text.Trim() } , 
                                { clsConfig.CONSTNAMEEGHL_PAYMENTURL , txtEGhlPayUrl.Text.Trim()} , 
                                { clsConfig.CONSTNAMEEGHL_TRANSACTIONTYPE , txtEGhlTransType.Text.Trim() } , 
                                { clsConfig.CONSTNAMEEGHL_PAYMENTMETHOD , txtEGhlPayMethod.Text.Trim() } , 
                                { clsConfig.CONSTNAMEEGHL_MERCHANTID , txtEGhlMerchantId.Text.Trim() } , 
                                { clsConfig.CONSTNAMEEGHL_MERCHANTPWD , txtEGhlMerchantPwd.Text.Trim() } ,
                                { clsConfig.CONSTNAMEEGHL_MERCHANTNAME , txtEGhlMerchantName.Text.Trim() } , 
                                { clsConfig.CONSTNAMEEGHL_RETURNURL , txtEGhlReturnUrl.Text.Trim() } ,  
                                { clsConfig.CONSTNAMEEGHL_CALLBACKURL , txtEGhlCallBackUrl.Text.Trim() } ,
                                { clsConfig.CONSTNAMEEGHL_CURRENCYCODE , txtEGhlCurrencyCode.Text.Trim() } , 
                                { clsConfig.CONSTNAMEEGHL_PAGETIMEOUT , txtEGhlTimeout.Text.Trim() }
                              };

        saveConfig(confArray, confGroup);
    }
    protected void saveConfig(string[,] confArray,string confGroup)
    {
        for (int i = 0; i < confArray.GetLength(0); i++)
        {
            string confName = confArray[i, 0];
            string confValue = confArray[i, 1];
            if (boolSuccess && !config.isItemExist(confName, confGroup))
            {
                intRecordAffected = config.addItem(confName, confValue, confGroup, 1, int.Parse(Session["ADMID"].ToString()));
            }
            else if (!config.isExactSameSetData(confName, confValue, confGroup))
            {
                intRecordAffected = config.updateItem(confGroup, confName, confValue, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; break; }
            }
        }
    }
    #endregion
}