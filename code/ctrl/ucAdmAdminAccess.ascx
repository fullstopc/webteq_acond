﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmAdminAccess.ascx.cs" Inherits="ctrl_ucAdmAdminAccess" %>

<script type="text/javascript">
    function validateLoginId_client(source, args) {

        if(args.Value.length < <% =clsSetting.CONSTADMUSERNAMEMINLENGTH %>)
        {
            args.IsValid = false;
        }            
    }

    function validatePassword_client(source, args) {

        if(args.Value.length < <% =clsSetting.CONSTADMPASSWORDMINLENGTH %>)
        {
            args.IsValid = false;
        }            
    }
    
    function validateNewPwd_client(source, args) {
        var txtNewPwd = document.getElementById("<%= txtNewPwd.ClientID %>");
        var txtConfirmPwd = document.getElementById("<%= txtConfirmPwd.ClientID %>");

        var boolValid = false;

        source.errormessage = "<br />Please enter password";
        source.innerHTML = "<br />Please enter password";

        if (Trim(txtNewPwd.value) != '') {
            args.IsValid = true;
        }
        else {
            if (Trim(txtConfirmPwd.value) != '') {
                boolValid = false;
            }
            else {
                boolValid = true;
            }

            args.IsValid = boolValid;
        }
    }

    function validateConfirmPwd_client(source, args) {
        var txtNewPwd = document.getElementById("<%= txtNewPwd.ClientID %>");
        var txtConfirmPwd = document.getElementById("<%= txtConfirmPwd.ClientID %>");

        var boolValid = false;

        source.errormessage = "<br />Please confirm password";
        source.innerHTML = "<br />Please confirm password";

        if (Trim(txtConfirmPwd.value) != '') {
            args.IsValid = true;
        }
        else {
            if (Trim(txtNewPwd.value) != '') {
                boolValid = false;
            }
            else {
                boolValid = true;
            }
            
            args.IsValid = boolValid;
        }
    }
</script>
    
<asp:Panel ID="pnlAdminAccess" runat="server">
    <table cellpadding="0" cellspacing="0" class="formTbl">
        <tr>
            <td colspan="2" class="tdSectionHdr">Admin Access Details</td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td class="tdLabel">Login ID:<span class="attention_unique">*</span></td>
            <td>
                <asp:TextBox ID="txtLoginId" runat="server" class="text" MaxLength="20"></asp:TextBox>
                <asp:HiddenField ID="hdnLoginId" runat="server" />
                <asp:RequiredFieldValidator ID="rfvLoginId" runat="server" ErrorMessage="<br />Please enter Login ID." ControlToValidate="txtLoginId" class="errmsg" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvLoginId" runat="server" CssClass="errmsg" ControlToValidate="txtLoginId" Display="Dynamic" ClientValidationFunction="validateLoginId_client" OnServerValidate="validateLoginId_server" OnPreRender="cvLoginId_PreRender" SetFocusOnError="true"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="tdLabel">Name:<span class="attention_compulsory">*</span></td>
            <td>
                <asp:TextBox ID="txtName" runat="server" MaxLength="250" CssClass="text_big"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="<br />Please enter name." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtName"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td class="tdLabel">Password:</td>
            <td>
                <asp:TextBox ID="txtNewPwd" runat="server" class="text" TextMode="Password" MaxLength="20"></asp:TextBox>
                <asp:HiddenField ID="hdnPassword" runat="server" />
                <asp:CustomValidator ID="cvPassword" runat="server" CssClass="errmsg" ControlToValidate="txtNewPwd" Display="Dynamic" ClientValidationFunction="validatePassword_client" SetFocusOnError="true"></asp:CustomValidator>
                <asp:CustomValidator ID="cvNewPwd" runat="server" CssClass="errmsg" ControlToValidate="txtNewPwd" Display="Dynamic" ClientValidationFunction="validateNewPwd_client" SetFocusOnError="true" ValidateEmptyText="true"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="tdLabel">Confirm Password:</td>
            <td>
                <asp:TextBox ID="txtConfirmPwd" runat="server" class="text" TextMode="Password" MaxLength="20"></asp:TextBox>
                <asp:CompareValidator ID="cmvPassword" runat="server" ErrorMessage="<br />Password does not match." ControlToCompare="txtNewPwd" ControlToValidate="txtConfirmPwd" Display="Dynamic" class="errmsg"></asp:CompareValidator>
                <asp:CustomValidator ID="cvConfirmPwd" runat="server" CssClass="errmsg" ControlToValidate="txtConfirmPwd" Display="Dynamic" ClientValidationFunction="validateConfirmPwd_client" ValidateEmptyText="true"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="tdSectionHdr">Admin Access Level</td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="pnlAdminAccessLevel" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="tdSectionHdr">Admin Settings</td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td class="tdLabel">Active:</td>
            <td><asp:CheckBox ID="chkActive" runat="server" Checked="true" /></td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CssClass="btn" OnClick="lnkbtnDelete_Click" Visible="false"></asp:LinkButton>
                <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Cancel" ToolTip="Cancel" CssClass="btn" CausesValidation="false"></asp:LinkButton>
            </td>
        </tr>
    </table>
</asp:Panel>
