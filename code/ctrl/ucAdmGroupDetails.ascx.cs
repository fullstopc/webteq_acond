﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;

public partial class ctrl_ucAdmGroupDetails : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected int _grpingId = clsAdmin.CONSTLISTGROUPINGCAT;
    protected string _grpingName = clsAdmin.CONSTLISRGROUPINGCATNAME;
    protected int _groupId = 0;
    protected string _pageListingURL = "";
    protected int _parentId = 0;
    protected Boolean _boolShowDesc = false;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int grpingId
    {
        get
        {
            if ((Session["GRPINGID"] == null) || (Session["GRPINGID"] == "")) { return _grpingId; }
            else { return Convert.ToInt16(Session["GRPINGID"]); }
        }
        set { Session["GRPINGID"] = value; }
    }

    public string grpingName
    {
        get { return Session["GRPINGNAME"] as string ?? _grpingName; }
        set { Session["GRPINGNAME"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int groupId
    {
        get
        {
            if (ViewState["groupId"] == null)
            {
                return _groupId;
            }
            else
            {
                return int.Parse(ViewState["groupId"].ToString());
            }
        }
        set { ViewState["groupId"] = value; }
    }

    protected Boolean boolShowDesc
    {
        get
        {
            if (ViewState["BOOLSHOWDESC"] == null)
            {
                return _boolShowDesc;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLSHOWDESC"]);
            }
        }
        set { ViewState["BOOLSHOWDESC"] = value; }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }

    public int parentId
    {
        get
        {
            if (Session["PARENTID"] != null && Session["PARENTID"] != "")
            {
                return Convert.ToInt16(Session["PARENTID"]);
            }
            else
            {
                return _parentId;
            }
        }
        set { Session["PARENTID"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void validateCatCode_server(object source, ServerValidateEventArgs args)
    {
        txtCatCode.Text = txtCatCode.Text.Trim().ToUpper();

        clsGroup grp = new clsGroup();
        if (grp.isGrpCodeExist(txtCatCode.Text, grpingId, groupId))
        {
            args.IsValid = false;
            cvCatCode.ErrorMessage = "This " + grpingName.ToLower() + " code is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvCatCode_PreRender(object source, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("catcode")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtCatCode.ClientID + "', document.all['" + cvCatCode.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "catcode", strJS);
        }
    }

    //protected void validateCatName_server(object source, ServerValidateEventArgs args)
    //{
    //    txtCatName.Text = txtCatName.Text.Trim();

    //    clsGroup grp = new clsGroup();
    //    if (grp.isGrpNameExist(txtCatName.Text, grpingId, groupId))
    //    {
    //        args.IsValid = false;
    //        cvCatName.ErrorMessage = "<br />This " + grpingName.ToLower() + " name is in use.";
    //    }
    //    else
    //    {
    //        args.IsValid = true;
    //    }
    //}

    //protected void cvCatName_PreRender(object source, EventArgs e)
    //{
    //    if (!Page.ClientScript.IsStartupScriptRegistered("catname"))
    //    {
    //        string strJS = null;
    //        strJS = "<script type=\"text/javascript\">";
    //        strJS += "ValidatorHookupControlID('" + txtCatName.ClientID + "', document.all['" + cvCatName.ClientID + "']);";
    //        strJS += "</script>";

    //        Page.ClientScript.RegisterStartupScript(Page.GetType(), "catname", strJS, false);
    //    }
    //}

    protected void validateCatNameJp_server(object source, ServerValidateEventArgs args)
    {
        txtCatJpName.Text = txtCatJpName.Text.Trim();

        clsGroup grp = new clsGroup();
        if (grp.isGrpNameJpExist(txtCatJpName.Text, grpingId, groupId))
        {
            args.IsValid = false;
            cvCatJpName.ErrorMessage = "<br/>This " + grpingName.ToLower() + " name is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvCatNameJp_PreRender(object source, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("catnamejp"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtCatJpName.ClientID + "', document.getElementById('" + cvCatJpName.ClientID + "'));";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "catnamejp", strJS, false);
        }
    }

    protected void validateCatNameMs_server(object source, ServerValidateEventArgs args)
    {
        txtCatMsName.Text = txtCatMsName.Text.Trim();

        clsGroup grp = new clsGroup();
        if (grp.isGrpNameMsExist(txtCatMsName.Text, grpingId, groupId))
        {
            args.IsValid = false;
            cvCatMsName.ErrorMessage = "<br/>This " + grpingName.ToLower() + " name is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvCatNameMs_PreRender(object source, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("catnamems"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtCatMsName.ClientID + "', document.getElementById('" + cvCatMsName.ClientID + "'));";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "catnamems", strJS, false);
        }
    }

    protected void validateCatNameZh_server(object source, ServerValidateEventArgs args)
    {
        txtCatZhName.Text = txtCatZhName.Text.Trim();

        clsGroup grp = new clsGroup();
        if (grp.isGrpNameZhExist(txtCatZhName.Text, grpingId, groupId))
        {
            args.IsValid = false;
            cvCatZhName.ErrorMessage = "<br/>This " + grpingName.ToLower() + " name is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvCatNameZh_PreRender(object source, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("catnamezh"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtCatZhName.ClientID + "', document.getElementById('" + cvCatZhName.ClientID + "'));";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "catnamezh", strJS, false);
        }
    }

    protected void validateCatPageTitleFriendlyUrl_server(object source, ServerValidateEventArgs args)
    {
        clsGroup group = new clsGroup();
        if (group.isGrpPageTitleFriendlyUrlExist(txtCatPageTitleFriendlyUrl.Text, grpingId, groupId))
        {
            args.IsValid = false;
            cvCatPageTitleFriendlyUrl.ErrorMessage = "<br/>This " + txtCatPageTitleFriendlyUrl.Text + " is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvCatPageTitleFriendlyUrl_PreRender(object source, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("catpagetitlefriendlyurl")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtCatPageTitleFriendlyUrl.ClientID + "', document.all['" + cvCatPageTitleFriendlyUrl.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "catpagetitlefriendlyurl", strJS);
        }
    }

    protected void validateCatImage_server(object source, ServerValidateEventArgs args)
    {
        if (fileCatImage.HasFile)
        {
            int intImgMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            //if (fileCatImage.PostedFile.ContentLength > clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH)
            //{
            //    cvCatImage.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMIMGALLOWEDMAXLENGTH, "kb") + ".";
            //    args.IsValid = false;
            //}

            if (fileCatImage.PostedFile.ContentLength > intImgMaxSize)
            {
                cvCatImage.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + ".";
                args.IsValid = false;
            }
            else
            {
                Match matchImage = Regex.Match(fileCatImage.FileName, clsAdmin.CONSTADMIMGALLOWEDEXT, RegexOptions.IgnoreCase);
                if (matchImage.Success)
                {
                    args.IsValid = true;
                }
                else
                {
                    cvCatImage.ErrorMessage = "<br />Please enter valid " + grpingName.ToLower() + " image. (" + clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + ").";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cvCatImage_PreRender(object source, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("catimage")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + fileCatImage.ClientID + "', document.all['" + cvCatImage.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "catimage", strJS, false);
        }
    }

    protected void lnkbtnDeleteImage_Click(object sender, EventArgs e)
    {
        pnlCatImage.Visible = false;
        hdnCatImage.Value = "";
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMGRPFOLDER.ToString() + "/";
        int intMovedProdNo = 0;
        string strDeletedGrpName = "";

        clsGroup grp = new clsGroup();

        if (grpingId == clsAdmin.CONSTLISTGROUPINGCAT)
        {
            grp.extractGrpById2(groupId, 0);

            if (grp.getGrpNOC(grpingId, groupId, 0) > 0 || grp.getGrpNOP(groupId, 0) > 0)
            {

                Session["ERRMSG"] = "This " + grp.grpName + " cannot be deleted. It has sub categories or product(s).";
                //litAck.Text = "<div class=\"noticemsg\">This " + grp.grpName + " cannot be deleted. It has sub categories or product(s).</div>";
                //pnlAck.Visible = true;

                Response.Redirect(Request.Url.ToString());
            }
            else
            {
                int iRecordAffected = grp.deleteGroup(grp.grpId);

                if (iRecordAffected == 1)
                {
                    if (grp.grpImage != "") { File.Delete(uploadPath + grp.grpImage); }

                    Session["DELETEDGRPNAME"] = grp.grpName;

                    Response.Redirect(currentPageName);
                }
            }
        }

        //clsGroup grp = new clsGroup();
        //grp.grpingId = grpingId;
        //clsMis.performDeleteGroup(groupId, grp.otherGrpId, uploadPath, ref intMovedProdNo, ref strDeletedGrpName);

        //Session["MOVEDPRODNO"] = intMovedProdNo;
        //Session["DELETEDGRPNAME"] = strDeletedGrpName;

        //Response.Redirect(currentPageName);
    }

    protected void lnkbtnCancel_Click(object sender, EventArgs e)
    {
        Session["LISTING"] = 1;

        Response.Redirect(pageListingURL + (parentId > 0 ? "?id=" + parentId : ""));
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string catCode = txtCatCode.Text.Trim().ToUpper();
            string catName = txtCatName.Text.Trim();
            string catNameJp = txtCatJpName.Text.Trim();
            string catNameMs = txtCatMsName.Text.Trim();
            string catNameZh = txtCatZhName.Text.Trim();
            string catDName = txtCatDName.Text.Trim();
            string catDNameJp = txtCatDNameJp.Text.Trim();
            string catDNameMs = txtCatDNameMs.Text.Trim();
            string catDNameZh = txtCatDNameZh.Text.Trim();
            string catPageTitleFriendlyUrl = txtCatPageTitleFriendlyUrl.Text.Trim();
            string catDesc = txtCatDesc.Text.Trim();
            string catSnapshot = txtCatSnapshot.Text.Trim();
            string catImage = "";
            int catShowTop = 0;
            int catActive = 0;

            int intParent = 0;
            int intParentOld = 0;
            if (grpingId == clsAdmin.CONSTLISTGROUPINGCAT)
            {
                intParent = !string.IsNullOrEmpty(ddlParent.SelectedValue) ? Convert.ToInt16(ddlParent.SelectedValue) : 0;
                intParentOld = !string.IsNullOrEmpty(hdnParent.Value.Trim()) ? Convert.ToInt16(hdnParent.Value.Trim()) : 0;
            }

            if (chkboxShowTop.Checked) { catShowTop = 1; }
            if (chkboxActive.Checked) { catActive = 1; }

            if (fileCatImage.HasFile)
            {
                try
                {
                    HttpPostedFile postedFile = fileCatImage.PostedFile;
                    string filename = Path.GetFileName(postedFile.FileName);
                    string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMGRPFOLDER.ToString() + "/";
                    clsMis.createFolder(uploadPath);
                    string newFilename = clsMis.GetUniqueFilename(filename, uploadPath);
                    postedFile.SaveAs(uploadPath + newFilename);
                    catImage = newFilename;
                }
                catch (Exception ex)
                {
                    clsLog.logErroMsg(ex.Message.ToString());
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />" + ex.Message.ToString() + "</div>";

                    throw ex;
                }
            }
            else
            {
                if (mode == 1)
                {
                    catImage = "";
                }
                else
                {
                    catImage = hdnCatImage.Value;
                }
            }

            clsGroup grp = new clsGroup();
            int intRecordAffected = 0;

            switch (mode)
            {
                case 1: //Add
                    intRecordAffected = grp.addGroup2(catCode, catName, catNameJp, catNameMs, catNameZh, catDName, catDNameJp, catDNameMs, catDNameZh,
                                                      catPageTitleFriendlyUrl, grpingId, catImage, catShowTop, catActive, parentId, int.Parse(Session["ADMID"].ToString()));

                    if (intRecordAffected == 1)
                    {
                        if (parentId > 0)
                        {
                            grp.updateGroupById3(grp.grpId, parentId);
                        }

                        Session["NEWGRPID"] = grp.grpId;
                        Response.Redirect(currentPageName + "?id=" + grp.grpId);
                    }
                    else
                    {
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new " + grpingName.ToLower() + ". Please try again.</div>";
                        Response.Redirect(currentPageName);
                    }
                    break;

                case 2: //Edit
                    Boolean bEdited = false;
                    Boolean bSuccess = true;

                    if (!boolShowDesc)
                    {
                        if (!grp.isExactSameGrp2(groupId, catCode, catName, catNameJp, catNameMs, catNameZh, catDName, catDNameJp, catDNameMs, catDNameZh, catPageTitleFriendlyUrl,
                                                 grpingId, catImage, catShowTop, catActive))
                        {
                            intRecordAffected = grp.updateGroupById2(groupId, catCode, catName, catNameJp, catNameMs, catNameZh, catDName, catDNameJp, catDNameMs, catDNameZh, catPageTitleFriendlyUrl,
                                                                     catImage, catShowTop, catActive, int.Parse(Session["ADMID"].ToString()));

                            if (intRecordAffected == 1)
                            {
                                if (hdnCatImage.Value == "" && hdnCatImageRef.Value != "")
                                {
                                    string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMGRPFOLDER.ToString() + "/";
                                    if (File.Exists(uploadPath + hdnCatImageRef.Value)) { File.Delete(uploadPath + hdnCatImageRef.Value); }
                                }
                                bEdited = true;
                            }
                            else
                            {
                                bSuccess = false;
                            }
                        }

                        if (intParent != intParentOld)
                        {
                            intRecordAffected = grp.updateGroupById3(groupId, intParent);

                            if (intRecordAffected == 1)
                            {
                                bEdited = true;
                                parentId = intParent;
                            }
                            else { bSuccess = false; }
                        }

                        if (bEdited)
                        {
                            Session["EDITGRPID"] = grp.grpId;
                        }
                        else
                        {
                            if (!bSuccess)
                            {
                                grp.extractGrpById2(groupId, 0);
                                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit " + grpingName.ToLower() + " (" + grp.grpName + "). Please try again.</div>";
                            }
                            else
                            {
                                Session["EDITGRPID"] = groupId;
                                Session["NOCHANGE"] = 1;
                            }
                        }
                    }
                    else
                    {
                        if (!grp.isExactSameGrp(groupId, catCode, catName, catNameJp, catNameMs, catNameZh, catDName, catDNameJp, catDNameMs, catDNameZh, catPageTitleFriendlyUrl,
                                                catDesc, catSnapshot, grpingId, catImage, catShowTop, catActive))
                        {
                            intRecordAffected = grp.updateGroupById(groupId, catCode, catName, catNameJp, catNameMs, catNameZh, catDName, catDNameJp, catDNameMs, catDNameZh, catPageTitleFriendlyUrl,
                                                                    catDesc, catSnapshot, catImage, catShowTop, catActive, int.Parse(Session["ADMID"].ToString()));

                            if (intRecordAffected == 1)
                            {
                                string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMGRPFOLDER.ToString() + "/";
                                if (File.Exists(uploadPath + hdnCatImageRef.Value)) { File.Delete(uploadPath + hdnCatImageRef.Value); }
                                bEdited = true;
                            }
                            else
                            {
                                bSuccess = false;
                            }
                        }
                    }

                    if (bEdited)
                    {
                        Session["EDITGRPID"] = grp.grpId;
                    }
                    else
                    {
                        if (!bSuccess)
                        {
                            grp.extractGrpById(groupId, 0);
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit " + grpingName + " (" + grp.grpName + "). Please try again.</div>";
                        }
                        else
                        {
                            Session["EDITGRPID"] = groupId;
                            Session["NOCHANGE"] = 1;
                        }
                    }

                    Response.Redirect(Request.Url.ToString());
                    break;
                default:
                    break;
            }
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
            checkLanguage();
        }
    }

    protected void setPageProperties()
    {
        if (Session[clsAdmin.CONSTPROJECTFRIENDLYURL] != null && clsAdmin.CONSTPROJECTFRIENDLYURL != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFRIENDLYURL]) == 0)
            {
                trFriendlyURL.Visible = false;
            }
        }

        if (!IsPostBack)
        {
            if (parentId > 0)
            {
                trParent.Visible = true;
                clsGroup grp = new clsGroup();
                grp.extractGrpById2(parentId, 0);
                int intParent = grp.parentId;
                lblParent.Text = grp.parentId > 0 ? "2<span class=\"spanSuperscript\">nd</span> " + grpingName : "1<span class=\"spanSuperscript\">st</span> " + grpingName;

                DataSet ds = new DataSet();
                ds = grp.getGrpListByGrpingId2(grpingId, 0);

                DataView dv = new DataView(ds.Tables[0]);
                dv.RowFilter = "GRP_PARENT = " + intParent;

                ddlParent.DataSource = dv;
                ddlParent.DataTextField = "GRP_NAME";
                ddlParent.DataValueField = "GRP_ID";
                ddlParent.DataBind();

                ddlParent.SelectedValue = parentId.ToString();
            }

            switch (mode)
            {
                case 1:
                    break;
                case 2:
                    clsGroup grp = new clsGroup();
                    grp.grpingId = grpingId;

                    if (groupId != grp.otherGrpId)
                    {
                        lnkbtnDelete.Visible = true;
                    }

                    fillForm();
                    break;
                default:
                    break;
            }

            int intImgMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTIMGSETTING] != null && Session[clsAdmin.CONSTPROJECTIMGSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTIMGSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            litUploadRule.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + " | " + clsMis.formatAllowedFileDimension(clsAdmin.CONSTADMIMGALLOWEDWIDTH, clsAdmin.CONSTADMIMGALLOWEDHEIGHT, "px");

            litGroupDetails.Text = grpingName + " Details";
            lblGroupCode.Text = grpingName + " Code";
            lblGroupName.Text = grpingName + " Name";
            lblGroupJpName.Text = grpingName + " Name (Japanese)";
            lblGroupMsName.Text = grpingName + " Name (Malay)";
            lblGroupZhName.Text = grpingName + " NAME (Chinese)";
            lblGroupDName.Text = grpingName + " Display Name";
            lblGroupDNameJp.Text = grpingName + " Display Name (Japanese)";
            lblGroupDNameMs.Text = grpingName + " Display Name (Malay)";
            lblGroupDNameZh.Text = grpingName + " Display Name (Chinese)";
            lblGroupPageTitleFriendlyUrl.Text = "Page Title (Friendly URL)";
            lblGroupImage.Text = grpingName + " Image";
            litGroupSetting.Text = grpingName + " Settings";

            //lnkbtnSaveBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteGroup.Text").ToString().Replace("<!GROUP!>", grpingName.ToLower()) + "');";
        }
    }

    protected void fillForm()
    {
        clsGroup grp = new clsGroup();
        if (grp.extractGrpById(groupId, 0))
        {
            txtCatCode.Text = grp.grpCode;
            //txtCatCode.Enabled = false;
            txtCatName.Text = grp.grpName;
            txtCatJpName.Text = grp.grpNameJp;
            txtCatMsName.Text = grp.grpNameMs;
            txtCatZhName.Text = grp.grpNameZh;
            txtCatDName.Text = grp.grpDName;
            txtCatDNameJp.Text = grp.grpDNameJp;
            txtCatDNameMs.Text = grp.grpDNameMs;
            txtCatDNameZh.Text = grp.grpDNameZh;
            txtCatPageTitleFriendlyUrl.Text = grp.grpPageTitleFriendlyUrl;

            if (grp.grpShowTop == 1)
            {
                chkboxShowTop.Checked = true;
            }
            else
            {
                chkboxShowTop.Checked = false;
            }

            if (grp.grpActive == 1)
            {
                chkboxActive.Checked = true;
            }
            else
            {
                chkboxActive.Checked = false;
            }

            if (grp.grpImage != "")
            {
                pnlCatImage.Visible = true;
                imgCatImage.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMGRPFOLDER + "/" + grp.grpImage + "&w=" + clsAdmin.CONSTADMDEFAULTIMGWIDTH + "&h=" + clsAdmin.CONSTADMDEFAULTIMGHEIGHT + "&f=1";
                hdnCatImage.Value = grp.grpImage;
                hdnCatImageRef.Value = grp.grpImage;
            }

            boolShowDesc = grp.grpShowDesc > 0 ? true : false;

            if (boolShowDesc)
            {
                trDesc.Visible = true;
                txtCatDesc.Text = grp.grpDesc;
                txtCatSnapshot.Text = grp.grpSnapshot;
                registerCKEditorScript();
            }
            else
            {
                trDesc.Visible = false;
            }
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
        }
    }

    public void registerCKEditorScript()
    {
        Session["CMSPATH"] = "";
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">" +
                    "CKEDITOR.replace('" + txtCatDesc.ClientID + "'," +
                    "{" +
                    "height:'400'," +
                    "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSGRPVIEW + "&command=QuickUpload&type=Files&path=2'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSGRPVIEW + "&command=QuickUpload&type=Images&path=2'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSGRPVIEW + "&command=QuickUpload&type=Flash&path=2'" +
                     "}" +
                     ");";

            strJS += "CKEDITOR.add;" +
                     "CKEDITOR.config.contentsCss = '" + System.Configuration.ConfigurationManager.AppSettings["cssBase"] + clsAdmin.CONSTADMCMSCSS + "';";
            strJS += "CKEDITOR.config.stylesCombo_stylesSet= '" + clsAdmin.CONSTADMSTYLESET + ":" + System.Configuration.ConfigurationManager.AppSettings["jsBase"] + clsAdmin.CONSTADMSTYLESETFILE + "';" +
                     "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR", strJS, false);
        }
    }

    protected void checkLanguage()
    {
        if (clsMis.isLangExist(clsAdmin.CONSTLANGJAPANESE))
        {
            trNameJp.Visible = true;
            trDNameJp.Visible = true;
        }
        if (clsMis.isLangExist(clsAdmin.CONSTLANGMALAY))
        {
            trNameMs.Visible = true;
            trDNameMs.Visible = true;
        }
        if (clsMis.isLangExist(clsAdmin.CONSTLANGCHINESE))
        {
            trNameZh.Visible = true;
            trDNameZh.Visible = true;
        }
    }
    #endregion
}
