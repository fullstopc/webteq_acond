﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrSideMenuNav.ascx.cs" Inherits="ctrl_ucUsrSideMenu" %>
<%@ Register Src="~/ctrl/ucUsrLoginLinks.ascx" TagName="UsrLoginLinks" TagPrefix="uc" %>

<div class="sidebar-nav">
    <div class="sidebar-header"></div>
    <div class="sidebar-body">
        <div class="sidebar-list">
            <ul class="nav-one nav">
            <asp:Repeater ID="rptSubMenu" runat="server" OnItemDataBound="rptSubMenu_ItemDataBound">
                <HeaderTemplate></HeaderTemplate>
                <FooterTemplate></FooterTemplate>
                <ItemTemplate>
                    <li>
                        <asp:HyperLink ID="hypSubMenu" runat="server" CssClass="">
                            <span><asp:Literal runat="server" ID="litSubMenu"></asp:Literal></span>
                            <i class="material-icons add">add</i>
                            <i class="material-icons remove">remove</i>
                        </asp:HyperLink>
                        <asp:Repeater ID="rptSubMenuLv2" runat="server" OnItemDataBound="rptSubMenuLv2_ItemDataBound">
                            <HeaderTemplate><ul class="nav-two nav"></HeaderTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                            <ItemTemplate>
                                <li>
                                    <asp:HyperLink ID="hypSubMenuLv2" runat="server" CssClass=""></asp:HyperLink>
                                    <asp:Repeater ID="rptSubMenuLv3" runat="server" OnItemDataBound="rptSubMenuLv3_ItemDataBound">
                                        <HeaderTemplate><ul class="nav-three nav"></HeaderTemplate>
                                        <FooterTemplate></ul></FooterTemplate>
                                        <ItemTemplate>
                                            <li><asp:HyperLink ID="hypSubMenuLv3" runat="server" CssClass=""></asp:HyperLink></li>
                                        </ItemTemplate>
                                        <SeparatorTemplate></SeparatorTemplate>
                                    </asp:Repeater>
                                </li>
                            </ItemTemplate>
                            <SeparatorTemplate></SeparatorTemplate>
                        </asp:Repeater>
                    </li>
                </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </div>
    <div class="sidebar-footer"></div>
</div>
<script type="text/javascript">
    $(function () {
        $(".nav-one > li > a.parent.expanded").parent().find(".nav-two").show();
        $(".nav-one > li > a.parent").click(function () {
            var $this = $(this);
            var $child = $this.parent().find(".nav-two");
            $this.toggleClass("expanded");
            if ($this.hasClass("expanded")) {
                $child.slideDown();
            }
            else {
                $child.slideUp();
            }
            return false;
        });

        $(".sidebar-nav-toggle").click(function (e) {
            $("body").toggleClass("open");

            $('html').one('click', function (e) {
                $('body').removeClass("open");
            });
            e.stopPropagation();
        });
        $('.sidebar-nav').click(function (e) {
            e.stopPropagation();
        });
    });
</script>