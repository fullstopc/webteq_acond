﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;
using System.Linq;

public partial class ctrl_ucAdmSocialMedia : System.Web.UI.UserControl
{
    #region "Properties"
    protected clsConfig config = new clsConfig();
    protected DataView dvConfig = null;
    protected DataSet dsConfig = null;

    protected bool boolSuccess = false;
    protected bool boolEdited = false;
    protected int intRecordAffected = 0;
     
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setPageProperties();
            fill();
        }
    }
    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            boolEdited = false;
            boolSuccess = true;
            intRecordAffected = 0;

            saveFacebookLike();
            saveGooglePlus();
            saveLinkedIn();
            saveInstagram();
            saveTwitter();
            saveYoutube();
            if (divFacebookLogin.Visible) { saveFacebookLogin(); }
            
            if (!boolSuccess)
            {
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to update Payment gateway. Please try again.</div>";
                Response.Redirect(currentPageName);
            }
            else
            {
                if (boolEdited)
                {
                    Session["EDITED_SOCIALMEDIA"] = 1;
                }
                else
                {
                    Session["EDITED_SOCIALMEDIA"] = 1;
                    Session["NOCHANGE"] = 1;
                }

                Response.Redirect(currentPageName);
            }
        }
    }
    #endregion

    
    #region "Methods"
    
    
    protected void setPageProperties()
    {
        dsConfig = config.getItemList(1);
        dvConfig = new DataView(dsConfig.Tables[0]);
    }

   
    protected void saveConfig(string[,] confArray,string confGroup)
    {
        for (int i = 0; i < confArray.GetLength(0); i++)
        {
            string confName = confArray[i, 0];
            string confValue = confArray[i, 1];
            if (boolSuccess && !config.isItemExist(confName, confGroup))
            {
                intRecordAffected = config.addItem(confName, confValue, confGroup, 1, int.Parse(Session["ADMID"].ToString()));
            }
            else if (!config.isExactSameSetData(confName, confValue, confGroup))
            {
                intRecordAffected = config.updateItem(confGroup, confName, confValue, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; break; }
            }
        }
    }
    #endregion

    #region "fill method"
    public void fill()
    {
        fillFacebookLike();
        fillGooglePlus();
        fillLinkedIn();
        fillInstagram();
        fillTwitter();
        fillYoutube();

        // add by sh.chong 18May2015 - facebook login feature
        divFacebookLogin.Visible = false;
        if (Session[clsAdmin.CONSTPROJECTFBLOGIN] != null && Session[clsAdmin.CONSTPROJECTFBLOGIN] != "")
        {
            if (Session[clsAdmin.CONSTPROJECTFBLOGIN].ToString() == "1")
            {
                fillFacebookLogin();
                divFacebookLogin.Visible = true;
            }
        }

    }

    public void fillFacebookLike()
    {
        lblFbPageUrl.Text = clsConfig.CONSTNAMEFBPAGEURL;
        lblFBLikeTitle.Text = clsConfig.CONSTNAMEFBLIKETITLE;
        lblFBLikeType.Text = clsConfig.CONSTNAMEFBLIKETYPE;
        lblFBLikeUrl.Text = clsConfig.CONSTNAMEFBLIKEURL;
        lblFBLikeImage.Text = clsConfig.CONSTNAMEFBLIKEIMAGE;
        lblFBLikeSitename.Text = clsConfig.CONSTNAMEFBLIKESITENAME;
        lblFBLikeDesc.Text = clsConfig.CONSTNAMEFBLIKEDESC;
        litFBLikeUrlSample.Text = Session[clsAdmin.CONSTPROJECTUSERVIEWURL].ToString();

        DataRow[] foundRow = dsConfig.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPFB + "'");
        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFBPAGEURL, true) == 0) { txtFbPageUrl.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFBLIKETITLE, true) == 0) { txtFBLikeTitle.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFBLIKETYPE, true) == 0) { txtFBLikeType.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFBLIKEURL, true) == 0) { txtFBLikeUrl.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFBLIKEIMAGE, true) == 0) { txtFBLikeImage.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFBLIKESITENAME, true) == 0) { txtFBLikeSitename.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFBLIKEDESC, true) == 0) { txtFBLikeDesc.Text = row["CONF_VALUE"].ToString(); }
        }

        if (string.IsNullOrEmpty(txtFBLikeType.Text)) { txtFBLikeType.Text = clsConfig.CONSTFACEBOOKLIKETYPE; }
        if (string.IsNullOrEmpty(txtFbPageUrl.Text)) { txtFbPageUrl.Text = clsConfig.CONSTWEBSITEPREFIX; }
        if (string.IsNullOrEmpty(txtFBLikeUrl.Text)) { txtFBLikeUrl.Text = clsConfig.CONSTWEBSITEPREFIX; }
        if (string.IsNullOrEmpty(txtFBLikeImage.Text)) { txtFBLikeImage.Text = clsConfig.CONSTWEBSITEPREFIX; }

    }

    public void fillFacebookLogin()
    {
        lblFbLoginApiKey.Text = clsConfig.CONSTNAMEFBLOGINAPIKEY;
        lblFbLoginSecret.Text = clsConfig.CONSTNAMEFBLOGINSECRET;
        lblFbLoginActive.Text = clsConfig.CONSTNAMEFBLOGINACTIVE;
        lblFbLoginCallback.Text = clsConfig.CONSTNAMEFBLOGINCALLBACK;
        lblFbLoginSuffix.Text = clsConfig.CONSTNAMEFBLOGINSUFFIX;

        DataRow[] foundRow = dsConfig.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPFBLOGIN + "'");
        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFBLOGINACTIVE, true) == 0)
            {
                if (!string.IsNullOrEmpty(row["CONF_VALUE"].ToString()))
                {
                    if (row["CONF_VALUE"].ToString() == "1")
                    {
                        chkboxFbLoginActive.Checked = true;
                    }
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFBLOGINAPIKEY, true) == 0){txtFbLoginApiKey.Text = row["CONF_VALUE"].ToString();}
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFBLOGINSECRET, true) == 0){ txtFbLoginSecret.Text = row["CONF_VALUE"].ToString();}
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFBLOGINCALLBACK, true) == 0){txtFbLoginCallback.Text = row["CONF_VALUE"].ToString();}
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEFBLOGINSUFFIX, true) == 0){txtFbLoginSuffix.Text = row["CONF_VALUE"].ToString();}
        }
    }

    public void fillGooglePlus()
    {

        lblGooglePlusUrl.Text = clsConfig.CONSTNAMEGOOGLEPLUSURL;
        lblGPLikeUrl.Text = clsConfig.CONSTNAMEGPLIKEURL;
        litGPLikeUrlSample.Text = Session[clsAdmin.CONSTPROJECTUSERVIEWURL].ToString();

        DataRow[] foundRow = dsConfig.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPGOOGLEPLUS + "'");


        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEGOOGLEPLUSURL, true) == 0) { txtGoogleURL.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEGPLIKEURL, true) == 0) { txtGPLikeUrl.Text = row["CONF_VALUE"].ToString(); }
        }
        if (string.IsNullOrEmpty(txtGPLikeUrl.Text)) { txtGPLikeUrl.Text = clsConfig.CONSTWEBSITEPREFIX; }
        if (string.IsNullOrEmpty(txtGoogleURL.Text)) { txtGoogleURL.Text = clsConfig.CONSTWEBSITEPREFIX; }
        
    }

    public void fillLinkedIn()
    {
        lblLinkedInUrl.Text = clsConfig.CONSTNAMELINKEDINURL;
        lblLIShareUrl.Text = clsConfig.CONSTNAMELISHAREURL;

        DataRow[] foundRow = dsConfig.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPLINKEDIN + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMELINKEDINURL, true) == 0) { txtLinkedInURL.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMELISHAREURL, true) == 0) { txtLIShareUrl.Text = row["CONF_VALUE"].ToString(); }
        }
        if (string.IsNullOrEmpty(txtLinkedInURL.Text)) { txtLinkedInURL.Text = clsConfig.CONSTWEBSITEPREFIX; }
        if (string.IsNullOrEmpty(txtLIShareUrl.Text)) { txtLIShareUrl.Text = clsConfig.CONSTWEBSITEPREFIX; }
        
    }

    public void fillInstagram()
    {
        lblInstagramUrl.Text = clsConfig.CONSTNAMEINSTAGRAMURL;

        DataRow[] foundRow = dsConfig.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPINSTAGRAM + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEINSTAGRAMURL, true) == 0) { txtInstagramUrl.Text = row["CONF_VALUE"].ToString(); }
        }
        if (string.IsNullOrEmpty(txtInstagramUrl.Text)) { txtInstagramUrl.Text = clsConfig.CONSTWEBSITEPREFIX; }
        
    }

    public void fillTwitter()
    {
        lblTwitterUrl.Text = clsConfig.CONSTNAMETWITTERURL;

        DataRow[] foundRow = dsConfig.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPTWITTER + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMETWITTERURL, true) == 0) { txtTwitterUrl.Text = row["CONF_VALUE"].ToString(); }
        }
        if (string.IsNullOrEmpty(txtTwitterUrl.Text)) { txtTwitterUrl.Text = clsConfig.CONSTWEBSITEPREFIX; }

    }

    public void fillYoutube()
    {
        lblYoutubeUrl.Text = clsConfig.CONSTNAMEYOUTUBEURL;

        DataRow[] foundRow = dsConfig.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPYOUTUBE + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEYOUTUBEURL, true) == 0) { txtYoutubeUrl.Text = row["CONF_VALUE"].ToString(); }
        }
        if (string.IsNullOrEmpty(txtYoutubeUrl.Text)) { txtYoutubeUrl.Text = clsConfig.CONSTWEBSITEPREFIX; }

    }
    #endregion

    #region "save method"
    protected void saveFacebookLike()
    {
        string confGroup = clsConfig.CONSTGROUPFB;
        string[,] confArray = {
                                { clsConfig.CONSTNAMEFBPAGEURL, (txtFbPageUrl.Text.Trim() == clsConfig.CONSTWEBSITEPREFIX) ? "" : txtFbPageUrl.Text.Trim()},
                                { clsConfig.CONSTNAMEFBLIKETITLE , txtFBLikeTitle.Text } , 
                                { clsConfig.CONSTNAMEFBLIKETYPE , txtFBLikeType.Text.Trim() } , 
                                { clsConfig.CONSTNAMEFBLIKEURL , (txtFBLikeUrl.Text.Trim() == clsConfig.CONSTWEBSITEPREFIX) ? "" : txtFBLikeUrl.Text.Trim() } , 
                                { clsConfig.CONSTNAMEFBLIKEIMAGE , (txtFBLikeImage.Text.Trim() == clsConfig.CONSTWEBSITEPREFIX) ? "" : txtFBLikeImage.Text.Trim()}  , 
                                { clsConfig.CONSTNAMEFBLIKESITENAME , txtFBLikeSitename.Text}  , 
                                { clsConfig.CONSTNAMEFBLIKEDESC , txtFBLikeDesc.Text} 
                              };

        saveConfig(confArray, confGroup);
    }
    protected void saveFacebookLogin()
    {
        string confGroup = clsConfig.CONSTGROUPFBLOGIN;
        string[,] confArray = {
                                { clsConfig.CONSTNAMEFBLOGINACTIVE, (chkboxFbLoginActive.Checked)?"1":"0"},
                                { clsConfig.CONSTNAMEFBLOGINAPIKEY , txtFbLoginApiKey.Text.Trim() } , 
                                { clsConfig.CONSTNAMEFBLOGINSECRET , txtFbLoginSecret.Text.Trim() } , 
                                { clsConfig.CONSTNAMEFBLOGINCALLBACK , txtFbLoginCallback.Text.Trim() } , 
                                { clsConfig.CONSTNAMEFBLOGINSUFFIX , txtFbLoginSuffix.Text.Trim()} 
                              };

        saveConfig(confArray, confGroup);
    }
    protected void saveGooglePlus()
    {
        string confGroup = clsConfig.CONSTGROUPGOOGLEPLUS;
        string[,] confArray = {
                                { clsConfig.CONSTNAMEGOOGLEPLUSURL, (txtGoogleURL.Text.Trim() == clsConfig.CONSTWEBSITEPREFIX) ? "" : txtGoogleURL.Text.Trim()},
                                { clsConfig.CONSTNAMEGPLIKEURL , (txtGPLikeUrl.Text.Trim() == clsConfig.CONSTWEBSITEPREFIX) ? "" : txtGPLikeUrl.Text.Trim() } 
                              };

        saveConfig(confArray, confGroup);
    }
    protected void saveLinkedIn()
    {
        string confGroup = clsConfig.CONSTGROUPLINKEDIN;
        string[,] confArray = {
                                { clsConfig.CONSTNAMELINKEDINURL, (txtLinkedInURL.Text.Trim() == clsConfig.CONSTWEBSITEPREFIX) ? "" : txtLinkedInURL.Text.Trim()},
                                { clsConfig.CONSTNAMELISHAREURL , (txtLIShareUrl.Text.Trim() == clsConfig.CONSTWEBSITEPREFIX) ? "" : txtLIShareUrl.Text.Trim() } 
                              };

        saveConfig(confArray, confGroup);
    }
    protected void saveInstagram()
    {
        string confGroup = clsConfig.CONSTGROUPINSTAGRAM;
        string[,] confArray = {
                                { clsConfig.CONSTNAMEINSTAGRAMURL, (txtInstagramUrl.Text.Trim() == clsConfig.CONSTWEBSITEPREFIX) ? "" : txtInstagramUrl.Text.Trim()}
                              };
        saveConfig(confArray, confGroup);
    }
    protected void saveTwitter()
    {
        string confGroup = clsConfig.CONSTGROUPTWITTER;
        string[,] confArray = {
                                { clsConfig.CONSTNAMETWITTERURL, (txtTwitterUrl.Text.Trim() == clsConfig.CONSTWEBSITEPREFIX) ? "" : txtTwitterUrl.Text.Trim()}
                              };
        saveConfig(confArray, confGroup);
    }
    protected void saveYoutube()
    {
        string confGroup = clsConfig.CONSTGROUPYOUTUBE;
        string[,] confArray = {
                                { clsConfig.CONSTNAMEYOUTUBEURL, (txtYoutubeUrl.Text.Trim() == clsConfig.CONSTWEBSITEPREFIX) ? "" : txtYoutubeUrl.Text.Trim()}
                              };
        saveConfig(confArray, confGroup);
    }
    #endregion
}