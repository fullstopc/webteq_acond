﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmLoading.ascx.cs" Inherits="ctlr_ucAdmLoading" %>
<asp:Panel ID="pnlLoading" runat="server" class="divLoading">
    <asp:Image ID="imgLoading" runat="server" /><br />
    <asp:Label ID="lblLoading" runat="server" meta:ResourceKey="lblLoading"></asp:Label>
</asp:Panel>  
