﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;

public partial class ctrl_ucAdmOrderSales : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _id;
    protected int _memId;
    protected int oldOrdStatus;
    protected int _mode;
    protected int intItemCount = 0;
    protected DataTable _dtSales;
    protected string currency = "";
    protected decimal decSubTotal = Convert.ToDecimal("0.00");
    protected decimal decShipping = Convert.ToDecimal("0.00");
    protected decimal _decTotal = Convert.ToDecimal("0.00");
    protected int intShipCheck = 0;
    protected string salesNo;
    protected string salesDate;
    protected string customerNo;
    protected int salesCustId = 0;
    protected string salesCustomer = "";
    protected string salesCustomerCompName = "";
    protected string salesCustomerEmail = "";
    protected string salesInvoiceRemarks;
    protected string salesRemarks;
    protected int billShipSame = 0;

    protected int _type = 0;
    protected int _status = 0;
    protected int _lastStatus = 0;
    protected int _courierId;
    protected string _deliveryDate;
    protected string _courier;
    protected string _consignmentNo;
    protected int _paymentReject = 0;
    protected string _paymentRejectRemarks;
    protected int _paymentId = 0;
    protected int intCurStatus;

    protected string memCode = "";
    protected int memType;
    protected string billingCompany;
    protected string billingContactPerson;
    protected string billingEmail;
    protected string billingAddress;
    protected string billingPoscode;
    protected string billingCity;
    protected string billingCountry;
    protected string billingState;
    protected string billingTel1;
    protected string billingTel2;
    protected string billingFax;

    protected string shippingCompany;
    protected string shippingContactPerson;
    protected string shippingEmail;
    protected string shippingAddress;
    protected string shippingPoscode;
    protected string shippingCity;
    protected string shippingCountry;
    protected string shippingState;
    protected string shippingTel1;
    protected string shippingTel2;
    protected string shippingFax;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int id
    {
        get { return _id; }
        set { _id = value; }
    }

    public int memId
    {
        get { return _memId; }
        set { _memId = value; }
    }

    public int mode
    {
        get { return _mode; }
        set { _mode = value; }
    }

    public DataTable dtSales
    {
        get
        {
            if (ViewState["DTSALES"] == null)
            {
                return _dtSales;
            }
            else
            {
                return (DataTable)ViewState["DTSALES"];
            }
        }
        set { ViewState["DTSALES"] = value; }
    }

    protected int type
    {
        get
        {
            if (ViewState["TYPE"] == null)
            {
                return _type;
            }
            else
            {
                return Convert.ToInt16(ViewState["TYPE"]);
            }
        }
        set { ViewState["TYPE"] = value; }
    }

    protected int status
    {
        get
        {
            if (ViewState["STATUS"] == null)
            {
                return _status;
            }
            else
            {
                return Convert.ToInt16(ViewState["STATUS"]);
            }
        }
        set { ViewState["STATUS"] = value; }
    }

    protected int lastStatus
    {
        get
        {
            if (ViewState["LASTSTATUS"] == null)
            {
                return _lastStatus;
            }
            else
            {
                return Convert.ToInt16(ViewState["LASTSTATUS"]);
            }
        }
        set { ViewState["LASTSTATUS"] = value; }
    }

    protected int courierId
    {
        get
        {
            if (ViewState["COURIERID"] == null)
            {
                return _courierId;
            }
            else
            {
                return Convert.ToInt16(ViewState["COURIERID"]);
            }
        }
        set { ViewState["COURIERID"] = value; }
    }

    protected string deliveryDate
    {
        get { return ViewState["DELIVERYDATE"] as string ?? _deliveryDate; }
        set { ViewState["DELIVERYDATE"] = value; }
    }

    protected string courier
    {
        get { return ViewState["COURIER"] as string ?? _courier; }
        set { ViewState["COURIER"] = value; }
    }

    protected string consignmentNo
    {
        get { return ViewState["CONSIGNMENTNO"] as string ?? _consignmentNo; }
        set { ViewState["CONSIGNMENTNO"] = value; }
    }

    protected int paymentReject
    {
        get
        {
            if (ViewState["PAYMENTREJECT"] == null)
            {
                return _paymentReject;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAYMENTREJECT"]);
            }
        }
        set { ViewState["PAYMENTREJECT"] = value; }
    }

    protected string paymentRejectRemarks
    {
        get { return ViewState["PAYMENTREJECTREMARKS"] as string ?? _paymentRejectRemarks; }
        set { ViewState["PAYMENTREJECTREMARKS"] = value; }
    }

    protected int paymentId
    {
        get
        {
            if (ViewState["PAYMENTID"] == null)
            {
                return _paymentId;
            }
            else
            {
                return int.Parse(ViewState["PAYMENTID"].ToString());
            }
        }
        set { ViewState["PAYMENTID"] = value; }
    }

    public decimal decTotal
    {
        get
        {
            if (ViewState["DECTOTAL"] == null)
            {
                return _decTotal;
            }
            else
            {
                return Convert.ToDecimal(ViewState["DECTOTAL"]);
            }
        }
        set { ViewState["DECTOTAL"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void validateSalesNo_server(object source, ServerValidateEventArgs args)
    {
        string strSalesNo = txtSalesNo.Text.Trim();

        clsOrder ord = new clsOrder();
        if (ord.isOrderCodeExist(id, strSalesNo))
        {
            args.IsValid = false;
            cvSalesNo.ErrorMessage = "<br/>This Sales No is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvSalesNo_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("SalesNo")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtSalesNo.ClientID + "', document.all['" + cvSalesNo.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "SalesNo", strJS, false);
        }
    }

    /*Billing & Shipping*/
    protected void chkboxBillingDelivery_Checked(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string company = "";
            string contactPerson = "";
            string address = "";
            string email = "";
            string poscode = "";
            string tel1 = "";
            string tel2 = "";
            string fax = "";
            string city = "";
            string country = "";
            string state = "";

            if (chkboxBillingDelivery.Checked)
            {
                //if (string.IsNullOrEmpty(cboxCustomer.SelectedValue))
                //{
                //    Page.ClientScript.RegisterStartupScript(Page.GetType(), "alert", "alert('Please select customer.')", true);
                //}
                //else
                //{
                clsMember mem = new clsMember();

                //salesCustomer = cboxCustomer.SelectedItem.Text;
                salesCustomer = cboxCustomer.SelectedValue;

                if (!string.IsNullOrEmpty(cboxCustomer.SelectedValue))
                {
                    int intTryParse;
                    if (int.TryParse(cboxCustomer.SelectedValue, out intTryParse)) { memId = intTryParse; }
                }

                //if (!string.IsNullOrEmpty(salesCustomer))
                //{
                //    string[] salesCustomerSplit = salesCustomer.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                //    salesCustomer = salesCustomerSplit[0];

                //    if (salesCustomerSplit.Length > 1)
                //    {
                //        salesCustomerEmail = salesCustomerSplit[1];
                //    }
                //}

                if (mem.extractMemberById2(memId, 1))
                {
                    company = mem.company;
                    contactPerson = mem.name;
                    address = mem.add;
                    email = mem.email;
                    poscode = mem.poscode;
                    tel1 = mem.contactNo;
                    tel2 = mem.contactNo2;
                    fax = mem.fax;
                    city = mem.city;

                    if (!string.IsNullOrEmpty(mem.country)) { country = mem.country.ToString(); }
                    state = mem.state;
                }
                //}
            }
            else
            {
                clsOrder ord = new clsOrder();

                if (ord.extractOrderById3(id))
                {
                    company = ord.company;
                    contactPerson = ord.name;
                    address = ord.add;
                    email = ord.email;
                    poscode = ord.poscode;
                    tel1 = ord.contactNo;
                    tel2 = ord.contactNo2;
                    fax = ord.fax;
                    city = ord.city;

                    if (!string.IsNullOrEmpty(ord.country)) { country = ord.country.ToString(); }
                    state = ord.state;
                }
            }

            clsCountry cty = new clsCountry();
            DataSet dsState = new DataSet();
            dsState = cty.getStateList(1);

            txtBillingCompany.Text = company;
            txtBillingContactPerson.Text = contactPerson;
            txtBillingAddress.Text = address;
            txtBillingEmail.Text = email;
            txtBillingPoscode.Text = poscode;
            txtBillingTel1.Text = tel1;
            txtBillingTel2.Text = tel2;
            txtBillingFax.Text = fax;
            txtBillingCity.Text = city;

            if (!string.IsNullOrEmpty(country)) { ddlBillingCountry.SelectedValue = country.ToString(); }

            if (dsState.Tables.Count > 0)
            {
                DataView dvState = new DataView(dsState.Tables[0]);
                dvState.RowFilter = "STATE_COUNTRY = '" + ddlBillingCountry.SelectedValue + "'";

                if (dvState.Count > 0)
                {
                    ddlBillingState.DataSource = dvState;
                    ddlBillingState.DataTextField = "STATE_NAME";
                    ddlBillingState.DataValueField = "STATE_CODE";
                    ddlBillingState.DataBind();

                    ListItem ddlBillingStateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                    ddlBillingState.Items.Insert(0, ddlBillingStateDefaultItem);

                    try
                    {
                        if (!string.IsNullOrEmpty(state))
                        {
                            ddlBillingState.SelectedValue = state;
                        }
                    }
                    catch (Exception ex)
                    {
                        ddlBillingState.SelectedValue = "";
                    }

                    tdBillingStateDdl.Visible = true;
                    tdBillingStateTxt.Visible = false;
                }
                else
                {
                    txtBillingState.Text = state;

                    tdBillingStateDdl.Visible = false;
                    tdBillingStateTxt.Visible = true;
                }
            }
            else
            {
                txtBillingState.Text = state;

                tdBillingStateDdl.Visible = false;
                tdBillingStateTxt.Visible = true;
            }
        }
    }

    protected void ddlBillingCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlBillingCountry.SelectedValue))
        {
            clsCountry country = new clsCountry();

            DataSet dsState = new DataSet();
            dsState = country.getStateList(1);
            DataView dvState;

            string countryCode = ddlBillingCountry.SelectedValue;

            if (dsState.Tables.Count > 0)
            {
                dvState = new DataView(dsState.Tables[0]);

                dvState.RowFilter = "STATE_DEFAULT = 0 AND STATE_COUNTRY = '" + ddlBillingCountry.SelectedValue + "'";

                if (dvState.Count > 0)
                {
                    ddlBillingState.DataSource = dvState;
                    ddlBillingState.DataTextField = "STATE_CODE";
                    ddlBillingState.DataValueField = "STATE_CODE";
                    ddlBillingState.DataBind();

                    ListItem ddlBillingStateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                    ddlBillingState.Items.Insert(0, ddlBillingStateDefaultItem);

                    tdBillingStateDdl.Visible = true;
                    tdBillingStateTxt.Visible = false;
                }
                else
                {
                    txtBillingState.Text = "";

                    tdBillingStateDdl.Visible = false;
                    tdBillingStateTxt.Visible = true;
                }
            }
            else
            {
                txtBillingState.Text = "";

                tdBillingStateDdl.Visible = false;
                tdBillingStateTxt.Visible = true;
            }
        }
        else
        {
            ddlBillingState.Items.Clear();

            ListItem ddlBillingStateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
            ddlBillingState.Items.Insert(0, ddlBillingStateDefaultItem);

            tdBillingStateDdl.Visible = true;
            tdBillingStateTxt.Visible = false;
        }
    }

    protected void ddlDeliveryCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlDeliveryCountry.SelectedValue))
        {
            clsCountry country = new clsCountry();

            DataSet dsState = new DataSet();
            dsState = country.getStateList(1);
            DataView dvState;

            string countryCode = ddlDeliveryCountry.SelectedValue;

            if (dsState.Tables.Count > 0)
            {
                dvState = new DataView(dsState.Tables[0]);

                dvState.RowFilter = "STATE_DEFAULT = 0 AND STATE_COUNTRY = '" + countryCode + "'";

                if (dvState.Count > 0)
                {
                    ddlDeliveryState.DataSource = dvState;
                    ddlDeliveryState.DataTextField = "STATE_CODE";
                    ddlDeliveryState.DataValueField = "STATE_CODE";
                    ddlDeliveryState.DataBind();

                    ListItem ddlStateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                    ddlDeliveryState.Items.Insert(0, ddlStateDefaultItem);

                    tdDeliveryStateDdl.Visible = true;
                    tdDeliveryStateTxt.Visible = false;
                }
                else
                {
                    txtDeliveryState.Text = "";

                    tdDeliveryStateDdl.Visible = false;
                    tdDeliveryStateTxt.Visible = true;
                }
            }
            else
            {
                txtDeliveryState.Text = "";

                tdDeliveryStateDdl.Visible = false;
                tdDeliveryStateTxt.Visible = true;
            }
        }
        else
        {
            ddlDeliveryState.Items.Clear();

            ListItem ddlStateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
            ddlDeliveryState.Items.Insert(0, ddlStateDefaultItem);

            tdDeliveryStateDdl.Visible = true;
            tdDeliveryStateTxt.Visible = false;
        }
    }
    /*End of Billing & Shipping*/

    /*Sales Items*/
    protected void rptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intSalesId = int.Parse(DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_ID").ToString());
            int intSalesProdId = int.Parse(DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODID").ToString());
            int intSalesOri = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "SALES_ORI"));
            string strSalesProdName = DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODNAME").ToString();
            string strSalesProdDesc = DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODSNAPSHOT").ToString();
            decimal decSalesUnitPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODPRICE"));
            int intSalesProdQty = int.Parse(DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODQTY").ToString());
            decimal decSalesItemTotal = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODTOTAL"));

            intItemCount += 1;

            Literal litItemNo = (Literal)e.Item.FindControl("litItemNo");
            litItemNo.Text = intItemCount.ToString();

            Literal litItemName = (Literal)e.Item.FindControl("litItemName");
            litItemName.Text = strSalesProdName;

            Literal litItemDesc = (Literal)e.Item.FindControl("litItemDesc");
            strSalesProdDesc = strSalesProdDesc.Replace("\n", "<br/>");
            litItemDesc.Text = strSalesProdDesc;

            Literal litItemUnitPrice = (Literal)e.Item.FindControl("litItemUnitPrice");
            litItemUnitPrice.Text = String.Format("{0:N}", decSalesUnitPrice); //decSalesUnitPrice.ToString();

            TextBox txtItemQty = (TextBox)e.Item.FindControl("txtItemQty");
            HiddenField hdnProdQty = (HiddenField)e.Item.FindControl("hdnProdQty");
            txtItemQty.Text = String.Format("{0:#,0}", intSalesProdQty); // 5 5,000//String.Format("{0:g}", intSalesProdQty); //intSalesProdQty.ToString();
            hdnProdQty.Value = intSalesProdQty.ToString();

            decimal decSubItemsTotal = decSalesUnitPrice * intSalesProdQty;
            decSubTotal += decSubItemsTotal;

            Literal litItemTotal = (Literal)e.Item.FindControl("litItemTotal");
            litItemTotal.Text = String.Format("{0:N}", decSubItemsTotal); //decSubItemsTotal.ToString();

            HiddenField hdnSalesOri = (HiddenField)e.Item.FindControl("hdnSalesOri");
            hdnSalesOri.Value = intSalesOri.ToString();

            LinkButton lnkbtnDelete = (LinkButton)e.Item.FindControl("lnkbtnDelete");
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteOrdItem.Text").ToString() + "'); return false;";
            lnkbtnDelete.CommandArgument = intSalesId.ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + intSalesProdId.ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + strSalesProdName + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + strSalesProdDesc + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + decSalesUnitPrice.ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + intSalesProdQty.ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + decSalesItemTotal.ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + intSalesOri.ToString();
        }
        else if (e.Item.ItemType == ListItemType.Footer)
        {
            Literal litSubtotal = (Literal)e.Item.FindControl("litSubtotal");
            litSubtotal.Text = String.Format("{0:N}", decSubTotal); //decSubTotal.ToString();

            TextBox txtShipping = (TextBox)e.Item.FindControl("txtShipping");

            //decimal decTryParse;
            //if (decimal.TryParse(txtShipping.Text.Trim(), out decTryParse))
            //{
            //    decShipping = decTryParse;
            //}
            //decShipping = Convert.ToDecimal(txtShipping.Text);

            CheckBox chkboxShipping = (CheckBox)e.Item.FindControl("chkboxShipping");

            if (status >= clsAdmin.CONSTORDERSTATUSINVOICEPAY)
            {
                chkboxShipping.Visible = false;
                txtShipping.Enabled = false;
            }
            else
            {
                //if (type == 1)
                //{
                if (Session["SHIPPINGCHECK"] != null && Session["SHIPPINGCHECK"] != "")
                {
                    chkboxShipping.Checked = true;
                }
                else { chkboxShipping.Checked = false; }
                //}
                //chkboxShipping.Visible = false;
                //txtShipping.Enabled = false;
                //Session["SHIPPING"] = null; }
                //else if (type == 2)
                //{
                //    //chkboxShipping.Checked = true;

                //    if ((Session["SHIPPINGCHECK"] != null && Session["SHIPPINGCHECK"] != "") || intShipCheck == 1)
                //    {
                //        chkboxShipping.Checked = true;
                //    }
                //    else { chkboxShipping.Checked = false; }
                //}
            }
            //else
            //{
            chkboxShipping.Attributes["onclick"] = "javascript:enableTextbox('" + chkboxShipping.ClientID + "','" + txtShipping.ClientID + "');";

            if (Session["SHIPPINGCHECK"] != null && Session["SHIPPINGCHECK"] != "")
            {
                intShipCheck = Convert.ToInt16(Session["SHIPPINGCHECK"]);
            }

            if (Session["SHIPPING"] != null && Session["SHIPPING"] != "")
            {
                decShipping = Convert.ToDecimal(Session["SHIPPING"]);
            }
            //}

            if (!string.IsNullOrEmpty(decShipping.ToString()))
            {
                txtShipping.Text = String.Format("{0:N}", decShipping); //decShipping.ToString();
            }

            if (intShipCheck == 1)
            {
                chkboxShipping.Checked = true;
            }

            Literal litGrandTotal = (Literal)e.Item.FindControl("litGrandTotal");
            litGrandTotal.Text = String.Format("{0:N}", decSubTotal + decShipping); //(decSubTotal + decShipping).ToString();
        }

        if (e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //HtmlTableCell tdNo = (HtmlTableCell)e.Item.FindControl("tdNo");
            //HtmlTableCell tdItemName = (HtmlTableCell)e.Item.FindControl("tdItemName");
            //HtmlTableCell tdItemDesc = (HtmlTableCell)e.Item.FindControl("tdItemDesc");
            //HtmlTableCell tdItemUnitPrice = (HtmlTableCell)e.Item.FindControl("tdItemUnitPrice");
            //HtmlTableCell tdItemQty = (HtmlTableCell)e.Item.FindControl("tdItemQty");
            //HtmlTableCell tdItemTotal = (HtmlTableCell)e.Item.FindControl("tdItemTotal");
            //HtmlTableCell tdItemDelete = (HtmlTableCell)e.Item.FindControl("tdItemDelete");

            //tdNo.Attributes["class"] = "td_alt";
            //tdItemName.Attributes["class"] = "td_norLeft_alt";
            //tdItemDesc.Attributes["class"] = "td_norLeft_alt";
            //tdItemUnitPrice.Attributes["class"] = "td_norLeft_alt";
            //tdItemQty.Attributes["class"] = "td_alt";
            //tdItemTotal.Attributes["class"] = "td_Left_alt";
            //tdItemDelete.Attributes["class"] = "td_alt";
        }
    }

    protected void rptItems_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdDelete")
        {
            string strArgument = e.CommandArgument.ToString();
            string[] strArgumentAplit = strArgument.Split(new string[] { clsAdmin.CONSTDEFAULTSEPERATOR2 }, StringSplitOptions.None);

            int intSalesOri = int.Parse(strArgumentAplit[7]);

            if (intSalesOri == 1)
            {
                Session["DELSALES"] += strArgument + clsAdmin.CONSTSYMBOLSEPARATOR.ToString();
            }
            else if (intSalesOri == 0)
            {
                Session["SALES"] = Session["SALES"].ToString().Replace(clsAdmin.CONSTSYMBOLSEPARATOR.ToString() + strArgument + clsAdmin.CONSTSYMBOLSEPARATOR.ToString(), clsAdmin.CONSTSYMBOLSEPARATOR.ToString());
            }

            populateItems();
        }
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int intCounter = 0;
            int intItemId = 0;
            string strItemName;
            string strItemDesc;
            decimal decItemUnitPrice = Convert.ToDecimal("0.00");
            int intItemQty = 0;
            decimal decItemTotal = Convert.ToDecimal("0.00");
            int intSalesCounter = 1;

            decimal decTryParse;
            int intTryParse;

            if (Session["SALESCOUNTER"] != null && Session["SALESCOUNTER"] != "")
            {
                intSalesCounter = Convert.ToInt16(Session["SALESCOUNTER"]) + 1;
            }

            strItemName = txtProdDName.Text.Trim();

            if (int.TryParse(hdnId.Value, out intTryParse))
            {
                intItemId = intTryParse;
            }

            if (decimal.TryParse(txtProdUnitPrice.Text.Trim(), out decTryParse))
            {
                decItemUnitPrice = decTryParse;
            }

            if (int.TryParse(txtProdQty.Text.Trim(), out intTryParse))
            {
                intItemQty = intTryParse;
            }

            decItemTotal = decItemUnitPrice * intItemQty;

            Session["SALES"] += intSalesCounter.ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2;
            Session["SALES"] += intItemId.ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2;
            Session["SALES"] += txtProdDName.Text.Trim() + clsAdmin.CONSTDEFAULTSEPERATOR2;
            Session["SALES"] += txtProdDesc.Text.Trim() + clsAdmin.CONSTDEFAULTSEPERATOR2;
            Session["SALES"] += txtProdUnitPrice.Text.Trim() + clsAdmin.CONSTDEFAULTSEPERATOR2;
            Session["SALES"] += txtProdQty.Text.Trim() + clsAdmin.CONSTDEFAULTSEPERATOR2;
            Session["SALES"] += decItemTotal.ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2;
            Session["SALES"] += "0" + clsAdmin.CONSTSYMBOLSEPARATOR;
            Session["SALESCOUNTER"] = intSalesCounter.ToString();

            while (Request[hdnId.ClientID + intCounter.ToString()] != null)
            {
                intItemId = !string.IsNullOrEmpty(Request[hdnId.ClientID + intCounter.ToString()].ToString().Trim()) ? int.Parse(Request[hdnId.ClientID + intCounter.ToString()].ToString().Trim()) : 0;
                strItemName = Request[txtProdDName.ClientID + intCounter.ToString()] != null ? Request[txtProdDName.ClientID + intCounter.ToString()].ToString().Trim() : "";
                strItemDesc = Request[txtProdDesc.ClientID + intCounter.ToString()] != null ? Request[txtProdDesc.ClientID + intCounter.ToString()].ToString().Trim() : "";

                decItemUnitPrice = 0;
                if (Request[txtProdUnitPrice.ClientID + intCounter.ToString()] != null)
                {
                    string strUnitPrice = Request[txtProdUnitPrice.ClientID + intCounter.ToString()].ToString().Trim();
                    if (!string.IsNullOrEmpty(strUnitPrice))
                    {
                        if (decimal.TryParse(strUnitPrice, out decTryParse))
                        {
                            decItemUnitPrice = decTryParse;
                        }
                    }
                }

                intItemQty = 0;
                if (Request[txtProdQty.ClientID + intCounter.ToString()] != null)
                {
                    string strQty = Request[txtProdQty.ClientID + intCounter.ToString()].ToString().Trim();
                    if (!string.IsNullOrEmpty(strQty))
                    {
                        if (int.TryParse(strQty, out intTryParse))
                        {
                            intItemQty = intTryParse;
                        }
                    }
                }

                decItemTotal = 0;
                //if (Request[txtProdItemTotal.ClientID + intCounter.ToString()] != null)
                //{
                //    string strTotal = Request[txtProdItemTotal.ClientID + intCounter.ToString()].ToString().Trim();
                //    if (!string.IsNullOrEmpty(strTotal))
                //    {
                //        decimal decTryParse;
                //        if (decimal.TryParse(strTotal, out decTryParse))
                //        {
                //            decItemTotal = decTryParse;
                //        }
                //    }
                //}

                intCounter += 1;

                decItemTotal = decItemUnitPrice * intItemQty;

                Session["SALES"] += intSalesCounter.ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2;
                Session["SALES"] += intItemId.ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2;
                Session["SALES"] += strItemName + clsAdmin.CONSTDEFAULTSEPERATOR2;
                Session["SALES"] += strItemDesc + clsAdmin.CONSTDEFAULTSEPERATOR2;
                Session["SALES"] += decItemUnitPrice.ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2;
                Session["SALES"] += intItemQty.ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2;
                Session["SALES"] += decItemTotal.ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2;
                Session["SALES"] += "0" + clsAdmin.CONSTSYMBOLSEPARATOR;
                intSalesCounter += 1;
            }

            txtProdDName.Text = "";
            txtProdDesc.Text = "";
            txtProdUnitPrice.Text = "";
            txtProdQty.Text = "";
            txtProdItemTotal.Text = "";

            populateItems();
        }
    }
    /*End of Sales Items*/

    /*Sales Status*/
    protected void rptStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Literal litStatus = (Literal)e.Item.FindControl("litStatus");
            string strDateTime = DataBinder.Eval(e.Item.DataItem, "STATUS_DATE").ToString();

            if (!string.IsNullOrEmpty(strDateTime))
            {
                DateTime dtStatusDate = Convert.ToDateTime(strDateTime);
                litStatus.Text = dtStatusDate.ToString("dd MMM yyyy");
            }
            else { litStatus.Text = "<span class=\"spanItalic\">" + GetGlobalResourceObject("GlobalResource", "statusDefaultMessage.Text").ToString() + "</span>"; }

            int intStatus = int.Parse(DataBinder.Eval(e.Item.DataItem, "ORDERSTATUS_ID").ToString());
            int intStatusOrder = int.Parse(DataBinder.Eval(e.Item.DataItem, "ORDERSTATUS_ORDER").ToString());

            LinkButton lnkbtnStatus = (LinkButton)e.Item.FindControl("lnkbtnStatus");
            HiddenField hdnStatus = (HiddenField)e.Item.FindControl("hdnStatus");

            if (status > 0)
            {
                //@ payment
                if (intStatus == clsAdmin.CONSTORDERSTATUSINVOICEPAY)
                {
                    if (paymentReject <= 0)
                    {
                        HtmlTableRow trPaymentDate = (HtmlTableRow)e.Item.FindControl("trPaymentDate");
                        trPaymentDate.Visible = true;

                        HtmlTableRow trPaymentType = (HtmlTableRow)e.Item.FindControl("trPaymentType");
                        trPaymentType.Visible = true;

                        HtmlTableRow trPaymentAmount = (HtmlTableRow)e.Item.FindControl("trPaymentAmount");
                        trPaymentAmount.Visible = true;

                        HtmlTableRow trPaymentDesc = (HtmlTableRow)e.Item.FindControl("trPaymentDesc");

                        TextBox txtPayAmount = (TextBox)e.Item.FindControl("txtPayAmount");

                        //@ current status is new
                        if (status == intStatus - 1)
                        {
                            clsMis mis = new clsMis();
                            DataSet ds = new DataSet();
                            DataView dv = new DataView();
                            ds = mis.getListByListGrp("PAYMENT TYPE", 1);
                            dv = new DataView(ds.Tables[0]);

                            dv.RowFilter = "LIST_ACTIVE = 1";
                            dv.Sort = "LIST_ORDER ASC";

                            DropDownList ddlPayType = (DropDownList)e.Item.FindControl("ddlPayType");

                            ddlPayType.DataSource = dv;
                            ddlPayType.DataTextField = "LIST_NAME";
                            ddlPayType.DataValueField = "LIST_VALUE";
                            ddlPayType.DataBind();

                            ListItem ddlPayTypeDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                            ddlPayType.Items.Insert(0, ddlPayTypeDefaultItem);

                            txtPayAmount.Text = decTotal.ToString();

                            HtmlTableCell tdPaymentDate = (HtmlTableCell)e.Item.FindControl("tdPaymentDate");
                            tdPaymentDate.Attributes["class"] = "tdStatusLabelActive";

                            HtmlTableCell tdPaymentDateDetails = (HtmlTableCell)e.Item.FindControl("tdPaymentDateDetails");
                            tdPaymentDateDetails.Attributes["class"] = "tdStatusActive";

                            HtmlTableCell tdPaymentType = (HtmlTableCell)e.Item.FindControl("tdPaymentType");
                            tdPaymentType.Attributes["class"] = "tdStatusLabelActive";

                            HtmlTableCell tdPaymentTypeDetails = (HtmlTableCell)e.Item.FindControl("tdPaymentTypeDetails");
                            tdPaymentTypeDetails.Attributes["class"] = "tdStatusActive";

                            HtmlTableCell tdPaymentAmount = (HtmlTableCell)e.Item.FindControl("tdPaymentAmount");
                            tdPaymentAmount.Attributes["class"] = "tdStatusLabelActive";

                            HtmlTableCell tdPaymentAmountDetails = (HtmlTableCell)e.Item.FindControl("tdPaymentAmountDetails");
                            tdPaymentAmountDetails.Attributes["class"] = "tdStatusActive";

                            HtmlTableCell tdPaymentDesc = (HtmlTableCell)e.Item.FindControl("tdPaymentDesc");
                            tdPaymentDesc.Attributes["class"] = "tdStatusLabelActive";

                            HtmlTableCell tdPaymentDescDetails = (HtmlTableCell)e.Item.FindControl("tdPaymentDescDetails");
                            tdPaymentDescDetails.Attributes["class"] = "tdStatusActive";

                            HtmlTableRow trPaymentAction = (HtmlTableRow)e.Item.FindControl("trPaymentAction");
                            trPaymentAction.Visible = true;

                            Panel pnlPayDate = (Panel)e.Item.FindControl("pnlPayDate");
                            Panel pnlPayType = (Panel)e.Item.FindControl("pnlPayType");
                            Panel pnlPayAmount = (Panel)e.Item.FindControl("pnlPayAmount");
                            Panel pnlPayDesc = (Panel)e.Item.FindControl("pnlPayDesc");
                            TextBox txtPayDate = (TextBox)e.Item.FindControl("txtPayDate");

                            registerScript2(txtPayDate.ClientID);

                            pnlPayDate.Visible = true;
                            pnlPayType.Visible = true;
                            pnlPayAmount.Visible = true;
                            pnlPayDesc.Visible = true;
                            trPaymentDesc.Visible = true;
                        }
                        else if (status >= intStatus)   // current status is PAID or above
                        {
                            Panel pnlPayDateText = (Panel)e.Item.FindControl("pnlPayDateText");
                            Literal litPayDate = (Literal)e.Item.FindControl("litPayDate");

                            Panel pnlPayTypeText = (Panel)e.Item.FindControl("pnlPayTypeText");
                            Literal litPayType = (Literal)e.Item.FindControl("litPayType");

                            Panel pnlPayAmountTxt = (Panel)e.Item.FindControl("pnlPayAmountTxt");
                            Literal litPayAmount = (Literal)e.Item.FindControl("litPayAmount");

                            Panel pnlPayDescText = (Panel)e.Item.FindControl("pnlPayDescText");
                            Literal litPayDesc = (Literal)e.Item.FindControl("litPayDesc");

                            //litPayAmount.Text = hdnPayAmount.Value;
                            //litPayRemarks.Text = hdnPayRemarks.Value.Replace("\n", "<br />");

                            pnlPayDateText.Visible = true;
                            pnlPayTypeText.Visible = true;
                            pnlPayAmountTxt.Visible = true;
                            pnlPayDescText.Visible = true;
                            trPaymentDesc.Visible = true;

                            clsOrder ord = new clsOrder();
                            ord.extractPaymentById(paymentId, 1);

                            litStatus.Text = "";
                            litPayDate.Text = ord.ordPayment.ToString("dd MMM yyyy");
                            litPayType.Text = ord.ordPayGateway;
                            litPayAmount.Text = String.Format("{0:N}", ord.ordPayTotal); //ord.ordPayTotal.ToString();
                            litPayDesc.Text = ord.ordPayRemarks;
                        }
                    }
                    else
                    {
                        HtmlTableRow trPaymentReject = (HtmlTableRow)e.Item.FindControl("trPaymentReject");
                        trPaymentReject.Visible = true;

                        Panel pnlPayRejectRemarksText = (Panel)e.Item.FindControl("pnlPayRejectRemarksText");
                        pnlPayRejectRemarksText.Visible = true;

                        Literal litPayRejectRemarks = (Literal)e.Item.FindControl("litPayRejectRemarks");
                        litPayRejectRemarks.Text = paymentRejectRemarks.Replace("\n", "<br />");
                    }
                }

                //@ delivery
                if (intStatus == clsAdmin.CONSTORDERSTATUSINVOICEDELIVERY)
                {
                    HtmlTableRow trDeliveryDate = (HtmlTableRow)e.Item.FindControl("trDeliveryDate");
                    HtmlTableRow trDeliveryCourier = (HtmlTableRow)e.Item.FindControl("trDeliveryCourier");
                    HtmlTableRow trDeliveryConsignment = (HtmlTableRow)e.Item.FindControl("trDeliveryConsignment");

                    if (status == intStatus - 1)
                    {
                        HtmlTableCell tdDeliveryDate = (HtmlTableCell)e.Item.FindControl("tdDeliveryDate");
                        tdDeliveryDate.Attributes["class"] = "tdStatusLabelActive";

                        HtmlTableCell tdDeliveryDateDetails = (HtmlTableCell)e.Item.FindControl("tdDeliveryDateDetails");
                        tdDeliveryDateDetails.Attributes["class"] = "tdStatusActive";

                        HtmlTableCell tdDeliveryCourier = (HtmlTableCell)e.Item.FindControl("tdDeliveryCourier");
                        tdDeliveryCourier.Attributes["class"] = "tdStatusLabelActive";

                        HtmlTableCell tdDeliveryCourierDetails = (HtmlTableCell)e.Item.FindControl("tdDeliveryCourierDetails");
                        tdDeliveryCourierDetails.Attributes["class"] = "tdStatusActive";

                        HtmlTableCell tdDeliveryConsignment = (HtmlTableCell)e.Item.FindControl("tdDeliveryConsignment");
                        tdDeliveryConsignment.Attributes["class"] = "tdStatusLabelActive";

                        HtmlTableCell tdDeliveryConsignmentDetails = (HtmlTableCell)e.Item.FindControl("tdDeliveryConsignmentDetails");
                        tdDeliveryConsignmentDetails.Attributes["class"] = "tdStatusActive";

                        Panel pnlDeliveryDate = (Panel)e.Item.FindControl("pnlDeliveryDate");
                        //Panel pnlDeliveryCourier = (Panel)e.Item.FindControl("pnlDeliveryCourier");
                        Panel pnlDeliveryConsignment = (Panel)e.Item.FindControl("pnlDeliveryConsignment");
                        Panel pnlDeliveryCourierCompany = (Panel)e.Item.FindControl("pnlDeliveryCourierCompany");

                        pnlDeliveryDate.Visible = true;
                        //pnlDeliveryCourier.Visible = true;
                        pnlDeliveryConsignment.Visible = true;
                        pnlDeliveryCourierCompany.Visible = true;

                        trDeliveryDate.Visible = true;
                        trDeliveryCourier.Visible = true;
                        trDeliveryConsignment.Visible = true;

                        TextBox txtDeliveryDate = (TextBox)e.Item.FindControl("txtDeliveryDate");

                        registerScript2(txtDeliveryDate.ClientID);

                        clsMis mis = new clsMis();
                        DataSet ds = new DataSet();
                        ds = mis.getListByListGrp("SHIPPING COMPANY", 1);

                        DataView dv = new DataView(ds.Tables[0]);
                        dv.Sort = "LIST_ORDER ASC";

                        DropDownList ddlDeliveryCourierCompany = (DropDownList)e.Item.FindControl("ddlDeliveryCourierCompany");
                        ddlDeliveryCourierCompany.DataSource = dv;
                        ddlDeliveryCourierCompany.DataTextField = "LIST_NAME";
                        ddlDeliveryCourierCompany.DataValueField = "LIST_VALUE";
                        ddlDeliveryCourierCompany.DataBind();

                        ListItem ddlDeliveryCourierCompanyDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                        ddlDeliveryCourierCompany.Items.Insert(0, ddlDeliveryCourierCompanyDefaultItem);
                    }
                    else if (status >= intStatus)
                    {
                        Panel pnlDeliveryDateText = (Panel)e.Item.FindControl("pnlDeliveryDateText");
                        Literal litDeliveryDate = (Literal)e.Item.FindControl("litDeliveryDate");

                        Panel pnlDeliveryCourierText = (Panel)e.Item.FindControl("pnlDeliveryCourierText");
                        Literal litDeliveryCourier = (Literal)e.Item.FindControl("litDeliveryCourier");

                        Panel pnlDeliveryConsignmentText = (Panel)e.Item.FindControl("pnlDeliveryConsignmentText");
                        Literal litDeliveryConsignment = (Literal)e.Item.FindControl("litDeliveryConsignment");

                        litStatus.Text = "";
                        litDeliveryDate.Text = deliveryDate.ToString();

                        litDeliveryCourier.Text = courier;
                        litDeliveryConsignment.Text = consignmentNo.Replace("\n", "<br />");

                        pnlDeliveryDateText.Visible = true;
                        pnlDeliveryCourierText.Visible = true;
                        pnlDeliveryConsignmentText.Visible = true;

                        trDeliveryDate.Visible = true;
                        trDeliveryCourier.Visible = true;
                        trDeliveryConsignment.Visible = true;
                    }
                }

                //@ current status
                if (intStatus == status)
                {
                    ////@ for undo action
                    //if (intStatus != clsAdmin.CONSTORDERSTATUSINVOICENEW && intStatus != clsAdmin.CONSTORDERSTATUSINVOICEDONE)
                    //{
                    //    HyperLink hypUndo = (HyperLink)e.Item.FindControl("hypUndo");
                    //    hypUndo.Text = GetGlobalResourceObject("GlobalResource", "contentAdmUndo.Text").ToString();
                    //    hypUndo.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/undoStatus.aspx?id=" + id + "&" + clsAdmin.CONSTUSRARTICLETHICKBOX;

                    //    Panel pnlStatusAct = (Panel)e.Item.FindControl("pnlStatusAct");
                    //    pnlStatusAct.Visible = true;
                    //}
                    //// end of undo status

                    lnkbtnStatus.CssClass = "btnStatusSel";
                    lnkbtnStatus.OnClientClick = "javascript:return false;";
                    intCurStatus = int.Parse(hdnStatus.Value);
                }

                //@ after current status
                if (intCurStatus > 0)
                {
                    if (intCurStatus != lastStatus)
                    {
                        if (intStatusOrder == intCurStatus + 1)
                        {
                            if (paymentReject <= 0)
                            {
                                HtmlTableCell tdStatusAct = (HtmlTableCell)e.Item.FindControl("tdStatusAct");
                                tdStatusAct.Attributes["class"] = "tdLabelGrey";

                                HtmlTableCell tdStatusDetails = (HtmlTableCell)e.Item.FindControl("tdStatusDetails");
                                tdStatusDetails.Attributes["class"] = "tdStatusActive";
                            }

                            if (intStatusOrder != clsAdmin.CONSTORDERSTATUSINVOICEPAY) { lnkbtnStatus.CssClass = "btnConfirm"; }
                            else
                            {
                                lnkbtnStatus.Text = "Pay:";
                                lnkbtnStatus.ToolTip = "Pay";
                                lnkbtnStatus.OnClientClick = "javascript:return false;";
                            }

                            litStatus.Text = "";
                        }
                        else if (intStatusOrder > intCurStatus + 1) { lnkbtnStatus.Enabled = false; }
                    }
                    else
                    {
                        //@ for sales type = "uploaded" use, there are two status for final stage
                        if (intStatus != status) { lnkbtnStatus.Visible = false; }

                        lnkbtnStatus.OnClientClick = "javascript:return false;";
                    }
                }
                else { lnkbtnStatus.OnClientClick = "javascript:return false;"; }   //@ before current status
            }
        }
    }

    protected void rptStatus_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (Page.IsValid)
        {
            clsOrder ord = new clsOrder();
            int intRecordAffected = 0;
            Boolean boolEdited = false;
            Boolean boolSuccess = true;

            if (e.CommandName == "cmdStatus")
            {
                int intStatus = Convert.ToInt16(e.CommandArgument);

                switch (intStatus)
                {
                    case clsAdmin.CONSTORDERSTATUSINVOICEPAY:
                        TextBox txtPayDate = (TextBox)e.Item.FindControl("txtPayDate");
                        DropDownList ddlPayType = (DropDownList)e.Item.FindControl("ddlPayType");
                        TextBox txtPayAmount = (TextBox)e.Item.FindControl("txtPayAmount");
                        TextBox txtPayDesc = (TextBox)e.Item.FindControl("txtPayDesc");

                        //if (!string.IsNullOrEmpty(txtPayDesc.Text.Trim()))
                        //{
                        DateTime dtPayDate = DateTime.MaxValue;
                        int intPayType = 0;
                        decimal decPayAmount = Convert.ToDecimal("0.00");

                        if (!string.IsNullOrEmpty(txtPayDate.Text.Trim()))
                        {
                            DateTime dtTryParse = DateTime.MaxValue;
                            if (DateTime.TryParse(clsMis.formatDateDDMMtoMMDD(txtPayDate.Text.Trim()), out dtTryParse)) { dtPayDate = dtTryParse; }
                        }

                        if (!string.IsNullOrEmpty(ddlPayType.SelectedValue))
                        {
                            int intTryParse;
                            if (int.TryParse(ddlPayType.SelectedValue, out intTryParse)) { intPayType = intTryParse; }
                        }

                        if (!string.IsNullOrEmpty(txtPayAmount.Text.Trim()))
                        {
                            decimal decTryParse = Convert.ToDecimal("0.00");
                            if (decimal.TryParse(txtPayAmount.Text.Trim(), out decTryParse)) { decPayAmount = decTryParse; }
                        }

                        //if (decPayAmount >= decTotal)
                        //{

                        if (!ord.isPaymentExist(id))
                        {
                            intRecordAffected = ord.addOrderPayment2(id, dtPayDate, intPayType, decPayAmount, txtPayDesc.Text.Trim());

                            if (intRecordAffected == 1)
                            {
                                ord.setOrderPayment();
                                boolEdited = true;
                            }
                            else { boolSuccess = false; }

                            if (boolSuccess)
                            {
                                intRecordAffected = clsMis.performUpdateOrderStatus(id, intStatus, true);

                                if (intRecordAffected == 1)
                                {
                                    boolEdited = true;

                                    if (ord.extractOrderById(id))
                                    {
                                        clsConfig config = new clsConfig();
                                        string strCurrency = config.currency;

                                        string strOrdNo = ord.ordNo;
                                        decimal decOrdTotalPrice = ord.ordTotal;
                                        string strName = ord.memName;
                                        string strEmail = ord.memEmail;

                                        if (string.IsNullOrEmpty(strEmail))
                                        {
                                            strEmail = ord.email;
                                        }

                                        if (string.IsNullOrEmpty(strName))
                                        {
                                            string[] strEmailSplit = strEmail.Split((char)'@');
                                            strName = strEmailSplit[0];
                                        }

                                        string strPersonDetails = "";

                                        strPersonDetails = "Name: " + strName + "<br />Email: " + strEmail;

                                        string strPaymentDetails = "Payment Type: " + ord.ordPayGateway + "<br />Remarks: " + ord.ordPayRemarks + "<br />Paid Amount: " + strCurrency + " " + ord.ordPayTotal + "<br />" + strPersonDetails;

                                        config.extractItemByNameGroup(clsConfig.CONSTNAMEINVOICEPAYMENTEMAIL, clsConfig.CONSTGROUPINVOICE, 1);
                                        string sendPaymentEmail = config.value;
                                        if (sendPaymentEmail == clsAdmin.CONSTTRUE)
                                        {
                                            sendDetails(strName, strEmail, id, strOrdNo, decOrdTotalPrice, strPaymentDetails);
                                        }
                                    }
                                }
                                else { boolSuccess = false; }
                            }
                        }
                        //}
                        //else
                        //{
                        //    Session["ERRMSG"] = "<div class=\"errmsg\">Amount paid is less than transaction amount.</div>";
                        //    Response.Redirect(Request.Url.ToString());
                        //}
                        //}
                        break;
                    //case clsAdmin.CONSTORDERSTATUSPROCESSING:
                    //    intRecordAffected = clsMis.performUpdateOrderStatus(id, intStatus, true);

                    //    if (intRecordAffected == 1)
                    //    {
                    //        boolEdited = true;

                    //        ord.extractOrderById(id);
                    //        string strEmail = ord.memEmail;
                    //        string strName = ord.memName;

                    //        if (string.IsNullOrEmpty(strEmail))
                    //        {
                    //            strEmail = ord.email;
                    //        }

                    //        if (string.IsNullOrEmpty(strName))
                    //        {
                    //            string[] strEmailSplit = strEmail.Split((char)'@');
                    //            strName = strEmailSplit[0];
                    //        }

                    //        clsConfig config = new clsConfig();
                    //        config.extractItemByNameGroup(clsConfig.CONSTNAMEINVOICEPROCESSINGEMAIL, clsConfig.CONSTGROUPINVOICE, 1);
                    //        string sendProcessingEmail = config.value;
                    //        if (sendProcessingEmail == clsAdmin.CONSTTRUE)
                    //        {
                    //            sendDetail(strEmail, strName, ord.ordNo, ord.ordTotalItem, ord.ordTotal);
                    //        }
                    //    }
                    //    else { boolSuccess = false; }
                    //    break;
                    case clsAdmin.CONSTORDERSTATUSINVOICEDELIVERY:
                        if (!ord.isShippingExist(id))
                        {
                            intRecordAffected = clsMis.performUpdateOrderStatus(id, intStatus, true);

                            ord.extractOrderById3(id);

                            if (intRecordAffected == 1)
                            {
                                boolEdited = true;

                                TextBox txtDeliveryDate = (TextBox)e.Item.FindControl("txtDeliveryDate");
                                //TextBox txtDeliveryCourier = (TextBox)e.Item.FindControl("txtDeliveryCourier");
                                TextBox txtDeliveryConsignment = (TextBox)e.Item.FindControl("txtDeliveryConsignment");
                                DropDownList ddlDeliveryCourierCompany = (DropDownList)e.Item.FindControl("ddlDeliveryCourierCompany");

                                DateTime dtDeliveryDate = DateTime.MaxValue;

                                if (!string.IsNullOrEmpty(txtDeliveryDate.Text.Trim()))
                                {
                                    DateTime dtTryParse = DateTime.MaxValue;
                                    if (DateTime.TryParse(clsMis.formatDateDDMMtoMMDD(txtDeliveryDate.Text.Trim()), out dtTryParse)) { dtDeliveryDate = dtTryParse; }
                                }
                                else
                                { dtDeliveryDate = ord.ordCreation; }

                                //string strCourier = txtDeliveryCourier.Text.Trim();
                                string strConsignment = txtDeliveryConsignment.Text.Trim();
                                int intShippingCompany = !string.IsNullOrEmpty(ddlDeliveryCourierCompany.SelectedValue) ? Convert.ToInt16(ddlDeliveryCourierCompany.SelectedValue) : 0;
                                string strCourier = ddlDeliveryCourierCompany.SelectedItem.Text;

                                intRecordAffected = ord.updateDeliveryDetails(id, dtDeliveryDate, intShippingCompany, strCourier, strConsignment);

                                if (intRecordAffected == 1)
                                {
                                    ord.extractOrderById3(id);
                                    string strEmail = ord.memEmail;
                                    string strName = ord.memName;

                                    if (string.IsNullOrEmpty(strEmail))
                                    {
                                        strEmail = ord.email;
                                    }

                                    if (string.IsNullOrEmpty(strName))
                                    {
                                        string[] strEmailSplit = strEmail.Split((char)'@');
                                        strName = strEmailSplit[0];
                                    }

                                    clsConfig config = new clsConfig();
                                    config.extractItemByNameGroup(clsConfig.CONSTNAMEINVOICEDELIVERYEMAIL, clsConfig.CONSTGROUPINVOICE, 1);
                                    string sendDeliveryEmail = config.value;
                                    if (sendDeliveryEmail == clsAdmin.CONSTTRUE)
                                    {
                                        sendDetail(strEmail, strName, ord.ordNo, ord.ordTotalItem, ord.ordTotal, ord.shippingDate, ord.shippingCompanyName, strConsignment);
                                    }
                                }
                                else { boolSuccess = false; }
                            }
                            else { boolSuccess = false; }
                        }
                        break;
                    case clsAdmin.CONSTORDERSTATUSINVOICEDONE:
                        intRecordAffected = clsMis.performUpdateOrderStatus(id, intStatus, true);

                        if (intRecordAffected == 1)
                        {
                            ord.extractOrderById(id);
                            string strEmail = ord.memEmail;
                            string strName = ord.memName;

                            ord.setDone(id);

                            sendDetail(strEmail, strName, ord.ordNo);

                            boolEdited = true;
                        }
                        else { boolSuccess = false; }
                        break;
                }
            }
            else if (e.CommandName == "cmdReject")
            {
                TextBox txtPayRejectRemarks = (TextBox)e.Item.FindControl("txtPayRejectRemarks");

                intRecordAffected = ord.updatePaymentReject(id, paymentId, txtPayRejectRemarks.Text.Trim());

                if (intRecordAffected == 1)
                {
                    boolEdited = true;

                    ord.extractOrderById(id);

                    string strEmail = ord.memEmail;
                    string strName = ord.memName;

                    if (string.IsNullOrEmpty(strName))
                    {
                        string[] strEmailSplit = strEmail.Split((char)'@');
                        strName = strEmailSplit[0];
                    }

                    sendDetail(ord.memEmail, strName, ord.ordNo, ord.ordTotal, ord.ordPayRejectRemarks);
                }
                else { boolSuccess = false; }
            }

            if (boolEdited) { Session["EDITEDORDID"] = id; }
            else
            {
                if (!boolSuccess)
                {
                    ord.extractOrderById(id);

                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text").ToString() + "<br />Failed to edit order (" + ord.ordNo + ").Please try again.</div>";
                }
                else
                {
                    Session["EDITEDORDID"] = id;
                    Session["NOCHANGE"] = 1;
                }
            }

            Response.Redirect(Request.Url.ToString());
        }
    }
    /*End of Sales Status*/

    /*Remarks*/
    protected void lnkbtnAddRemarks_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Boolean boolExist = false;
            string strAddRemarks = txtSalesRemarks.Text.Trim() + clsAdmin.CONSTDEFAULTSEPERATOR;

            boolExist = false;

            if (!string.IsNullOrEmpty(strAddRemarks))
            {
                string strSalesAddRemarks = strAddRemarks + clsAdmin.CONSTDEFAULTSEPERATOR;

                if (Session["ORISALESREMARKS"] != null && Session["ORISALESREMARKS"] != "")
                {
                    if (Session["ORISALESREMARKS"].ToString().IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + strAddRemarks) >= 0) { boolExist = true; }
                }

                if (!boolExist && Session["SALESREMARKS"] != null && Session["SALESREMARKS"] != "")
                {
                    if (Session["SALESREMARKS"].ToString().IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + strAddRemarks) >= 0) { boolExist = true; }
                }
            }

            if (!boolExist)
            {
                Session["SALESREMARKS"] += strAddRemarks;
            }
            else if (boolExist)
            {
                if (Session["DELSALESREMARKS"] != null && Session["DELSALESREMARKS"] != "")
                {
                    if (Session["DELSALESREMARKS"].ToString().IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + strAddRemarks) >= 0) { Session["DELSALESREMARKS"].ToString().Replace(strAddRemarks, clsAdmin.CONSTDEFAULTSEPERATOR.ToString()); }
                }
            }

            txtSalesRemarks.Text = "";
        }

        populateRemarks();
    }
    /*End of Remarks*/

    protected void lnkbtnUpdate_Click(object sender, EventArgs e)
    {
        foreach (RepeaterItem rptItem in rptItems.Items)
        {
            HiddenField hdnDetailSalesId = (HiddenField)rptItem.FindControl("hdnDetailSalesId");
            HiddenField hdnProdId = (HiddenField)rptItem.FindControl("hdnProdId");
            TextBox txtItemQty = (TextBox)rptItem.FindControl("txtItemQty");
            HiddenField hdnProdQty = (HiddenField)rptItem.FindControl("hdnProdQty");

            int intDetailId = 0;
            int intSalesId = 0;
            decimal decUnitPrice = Convert.ToDecimal("0.00");
            decimal decTotal = Convert.ToDecimal("0.00");

            if (!string.IsNullOrEmpty(hdnDetailSalesId.Value))
            {
                string[] strDetailSalesIdSplit = hdnDetailSalesId.Value.Split(new string[] { clsAdmin.CONSTDEFAULTSEPERATOR2 }, StringSplitOptions.None);

                intDetailId = int.Parse(strDetailSalesIdSplit[0]);
                intSalesId = int.Parse(strDetailSalesIdSplit[1]);
            }

            int intProdId = int.Parse(hdnProdId.Value);

            int intProdQty;
            if (!string.IsNullOrEmpty(txtItemQty.Text.Trim())) { intProdQty = int.Parse(txtItemQty.Text.Trim(), System.Globalization.NumberStyles.AllowThousands); }//int.Parse(txtItemQty.Text); }
            else { intProdQty = 0; }

            int intProdQtyRef = int.Parse(hdnProdQty.Value);

            if (intProdQty != intProdQtyRef)
            {
                clsOrder ord = new clsOrder();
                DataRow[] row = dtSales.Select("ORDDETAIL_ID = " + intDetailId + " AND SALES_ORI = " + intSalesId);

                string strSalesItem = row[0]["ORDDETAIL_ID"] + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + row[0]["ORDDETAIL_PRODID"] + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + row[0]["ORDDETAIL_PRODNAME"] + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + row[0]["ORDDETAIL_PRODSNAPSHOT"] + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + row[0]["ORDDETAIL_PRODPRICE"] + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + row[0]["ORDDETAIL_PRODQTY"] + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + row[0]["ORDDETAIL_PRODTOTAL"] + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + row[0]["SALES_ORI"];

                if (row.Length > 0)
                {
                    if (intProdQty > 0)
                    {
                        decUnitPrice = Convert.ToDecimal(row[0]["ORDDETAIL_PRODPRICE"]);

                        decTotal = decUnitPrice * intProdQty;
                        decimal decSales = Convert.ToDecimal(row[0]["ORDDETAIL_PRODTOTAL"]);

                        string strSalesItemNew = strSalesItem.Replace(clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + intProdQtyRef + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + decSales.ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString(), clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + intProdQty + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString() + decTotal.ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2.ToString());

                        if (intSalesId == 0)
                        {
                            Session["SALES"] = Session["SALES"].ToString().Replace(clsAdmin.CONSTSYMBOLSEPARATOR.ToString() + strSalesItem + clsAdmin.CONSTSYMBOLSEPARATOR.ToString(), clsAdmin.CONSTSYMBOLSEPARATOR.ToString() + strSalesItemNew + clsAdmin.CONSTSYMBOLSEPARATOR.ToString());
                        }
                        else if (intSalesId == 1)
                        {
                            Session["ORISALES"] = Session["ORISALES"].ToString().Replace(clsAdmin.CONSTSYMBOLSEPARATOR.ToString() + strSalesItem + clsAdmin.CONSTSYMBOLSEPARATOR.ToString(), clsAdmin.CONSTSYMBOLSEPARATOR.ToString() + strSalesItemNew + clsAdmin.CONSTSYMBOLSEPARATOR.ToString());
                        }

                        //row[0]["ORDDETAIL_PRODQTY"] = intProdQty;
                    }
                    else
                    {
                        if (intSalesId == 0)
                        {
                            Session["SALES"] = Session["SALES"].ToString().Replace(clsAdmin.CONSTSYMBOLSEPARATOR.ToString() + strSalesItem + clsAdmin.CONSTSYMBOLSEPARATOR.ToString(), clsAdmin.CONSTSYMBOLSEPARATOR.ToString());
                        }
                        else if (intSalesId == 1)
                        {
                            Session["DELSALES"] += strSalesItem + clsAdmin.CONSTSYMBOLSEPARATOR.ToString();
                        }
                    }
                }
            }
        }

        /*Shipping*/
        decimal decShipping = Convert.ToDecimal("0.00");
        int intShipCheck = 0;

        TextBox txtShipping = (TextBox)rptItems.Controls[rptItems.Controls.Count - 1].Controls[0].FindControl("txtShipping");
        CheckBox chkboxShipping = (CheckBox)rptItems.Controls[rptItems.Controls.Count - 1].Controls[0].FindControl("chkboxShipping");

        if (chkboxShipping.Checked)
        {
            decimal decTryParse;
            if (decimal.TryParse(txtShipping.Text.Trim(), out decTryParse))
            {
                decShipping = decTryParse;
            }

            intShipCheck = 1;

            Session["SHIPPING"] = decShipping;
            Session["SHIPPINGCHECK"] = intShipCheck;
        }
        else
        {
            //intShipCheck = 0;

            //Session["SHIPPINGCHECK"] = intShipCheck;

            Session["SHIPPING"] = null;
            Session["SHIPPINGCHECK"] = null;
        }
        /*End of Shipping*/

        populateItems();
    }

    protected void lnkbtnQuickDone_Click(object sender, EventArgs e)
    {
        clsMis.performSalesQuickDone(id);

        Response.Redirect(Request.Url.ToString());
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            /*Sales Details*/
            salesNo = txtSalesNo.Text.Trim();
            salesCustomerCompName = cboxCustomer.SelectedItem.Text;
            if (!string.IsNullOrEmpty(cboxCustomer.SelectedValue))
            {
                int intTryParse;
                if (int.TryParse(cboxCustomer.SelectedValue, out intTryParse)) { memId = intTryParse; }
            }
            //salesCustomer = cboxCustomer.SelectedValue;
            customerNo = txtCustomerNo.Text.Trim();
            DateTime dtSalesDate = DateTime.MaxValue;
            salesDate = hdnSalesDate.Value.Trim();
            salesInvoiceRemarks = txtRemarksInvoice.Text.Trim();
            salesRemarks = txtSalesRemarks.Text.Trim();

            if (!string.IsNullOrEmpty(salesDate))
            {
                DateTime dtTryParse;
                if (DateTime.TryParse(clsMis.formatDateDDMMtoMMDD(salesDate), out dtTryParse)) { dtSalesDate = dtTryParse; }
            }
            else { dtSalesDate = DateTime.Now; }
            /*End of Sales Details*/

            /*Billing And Shipping*/
            billingCompany = txtBillingCompany.Text.Trim();
            billingContactPerson = txtBillingContactPerson.Text.Trim();
            billingEmail = txtBillingEmail.Text.Trim();
            billingAddress = txtBillingAddress.Text.Trim();
            billingPoscode = txtBillingPoscode.Text.Trim();
            billingCity = txtBillingCity.Text.Trim();
            billingCountry = ddlBillingCountry.SelectedValue;
            billingTel1 = txtBillingTel1.Text.Trim();
            billingTel2 = txtBillingTel2.Text.Trim();
            billingFax = txtBillingFax.Text.Trim();

            if (tdBillingStateDdl.Visible) { billingState = ddlBillingState.SelectedValue; }
            else if (tdBillingStateTxt.Visible) { billingState = txtBillingState.Text.Trim(); }
            else { billingState = ""; }

            if (chkboxDeliveryDetails.Checked)
            {
                shippingCompany = billingCompany;
                shippingContactPerson = billingContactPerson;
                shippingEmail = billingEmail;
                shippingAddress = billingAddress;
                shippingPoscode = billingPoscode;
                shippingCity = billingCity;
                shippingCountry = billingCountry;
                shippingTel1 = billingTel1;
                shippingTel2 = billingTel2;
                shippingFax = billingFax;
                shippingState = billingState;
            }
            else
            {
                shippingCompany = txtDeliveryCompany.Text.Trim();
                shippingContactPerson = txtDeliveryContactPerson.Text.Trim();
                shippingEmail = txtDeliveryEmail.Text.Trim();
                shippingAddress = txtDeliveryAddress.Text.Trim();
                shippingPoscode = txtDeliveryPoscode.Text.Trim();
                shippingCity = txtDeliveryCity.Text.Trim();
                shippingCountry = ddlDeliveryCountry.SelectedValue;
                shippingTel1 = txtDeliveryTel1.Text.Trim();
                shippingTel2 = txtDeliveryTel2.Text.Trim();
                shippingFax = txtDeliveryFax.Text.Trim();

                if (tdDeliveryStateDdl.Visible) { shippingState = ddlDeliveryState.SelectedValue; }
                else if (tdDeliveryStateTxt.Visible) { shippingState = txtDeliveryState.Text.Trim(); }
                else { shippingState = ""; }
            }

            /*End of Billing And Shipping*/

            clsOrder ord = new clsOrder();
            clsMember mem = new clsMember();
            clsProduct prod = new clsProduct();
            int intRecordAffected = 0;
            Boolean boolAdded = false;
            Boolean boolEdited = false;
            Boolean boolSuccess = true;

            /*Member*/
            if (memId == 0)
            {
                memCode = generateMemCode();

                intRecordAffected = mem.addMember(memCode, salesCustomerCompName, 2);

                memId = mem.memId;
            }

            //if (salesCustId != 0)
            //{
            //    mem.extractMemberById2(salesCustId, 1);

            //    salesCustomer = mem.name;
            //    salesCustomerCompName = mem.company;
            //    salesCustomerEmail = mem.email;
            //}
            //else
            //{
            //    memCode = generateMemCode();

            //    intRecordAffected = mem.addMember(memCode, salesCustomerCompName, 2);
            //}

            //if (!string.IsNullOrEmpty(salesCustomer))
            //{
            //    string[] salesCustomerSplit = salesCustomer.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

            //    salesCustomer = salesCustomerSplit[0];

            //    if (salesCustomerSplit.Length > 1)
            //    {
            //        salesCustomerEmail = salesCustomerSplit[1];
            //    }
            //}

            //if (!mem.isMemberCompanyExist(salesCustomer))
            //{
            //    memCode = generateMemCode();

            //    intRecordAffected = mem.addMember(memCode, salesCustomer, 2);
            //}
            //else { memCode = mem.memCode; }

            //if (memType == 1)
            //{
            //    salesCustomerEmail = salesCustomer;
            //    salesCustomer = "";
            //}

            if (chkboxCustomerDetails.Checked)
            {
                if (!mem.isExactSameMemberDataSet(memId, billingCompany, billingEmail, billingContactPerson, billingAddress, billingPoscode, billingCity, billingCountry, billingState, billingTel1, billingTel2, billingFax))
                {
                    mem.updateMemberById(memId, billingCompany, billingEmail, billingContactPerson, billingAddress, billingPoscode, billingCity, billingCountry, billingState, billingTel1, billingTel2, billingFax);
                }
            }

            if (memId != 0)
            {
                mem.extractMemberById2(memId, 1);

                memCode = mem.memCode;
                salesCustomer = mem.name;
                salesCustomerCompName = mem.company;
                salesCustomerEmail = mem.email;
                memType = mem.type;
            }
            /*End of Member*/

            /*Sales Items*/
            billShipSame = chkboxDeliveryDetails.Checked ? 1 : 0;

            switch (mode)
            {
                case 1:
                    //mem.extractMemberById(memId, 1);

                    intRecordAffected = ord.addSales(salesNo, dtSalesDate, customerNo, memId, memCode, salesCustomer, salesCustomerCompName, salesCustomerEmail, memType, salesInvoiceRemarks, 2, billingCompany, billingContactPerson, billingAddress, billingEmail, billingPoscode, billingCity, billingCountry, billingState, billingTel1, billingTel2, billingFax,
                                        shippingCompany, shippingContactPerson, shippingAddress, shippingEmail, shippingPoscode, shippingCity, shippingCountry, shippingState, shippingTel1, shippingTel2, shippingFax, billShipSame, decShipping);

                    if (intRecordAffected == 1)
                    {
                        id = ord.ordId;
                        boolAdded = true;

                        intRecordAffected = clsMis.performUpdateOrderStatus2(id, clsAdmin.CONSTORDERSTATUSINVOICENEW, dtSalesDate);
                    }
                    break;
                case 2:
                    if (Session["SALESREMARKS"] != null && Session["SALESREMARKS"] != "")
                    {
                        string strSalesRemarks = Session["SALESREMARKS"].ToString();

                        strSalesRemarks = strSalesRemarks.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strSalesRemarks.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                        if (!string.IsNullOrEmpty(strSalesRemarks))
                        {
                            strSalesRemarks = strSalesRemarks.Substring(0, strSalesRemarks.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                            string[] strSalesRemarksSplit = strSalesRemarks.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                            for (int i = 0; i <= strSalesRemarksSplit.GetUpperBound(0); i++)
                            {
                                salesRemarks = strSalesRemarksSplit[i];

                                intRecordAffected = ord.addOrderRemarks(id, salesRemarks);

                                if (intRecordAffected == 1)
                                {
                                    boolEdited = true;
                                }
                            }
                        }
                    }

                    if (boolSuccess && Session["DELSALESREMARKS"] != null && Session["DELSALESREMARKS"] != "")
                    {
                        string strDelSalesRemarks = Session["DELSALESREMARKS"].ToString();

                        strDelSalesRemarks = strDelSalesRemarks.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strDelSalesRemarks.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                        if (!string.IsNullOrEmpty(strDelSalesRemarks))
                        {
                            strDelSalesRemarks = strDelSalesRemarks.Substring(0, strDelSalesRemarks.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                            string[] strDelSalesRemarksSplit = strDelSalesRemarks.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                            for (int i = 0; i <= strDelSalesRemarksSplit.GetUpperBound(0); i++)
                            {
                                salesRemarks = strDelSalesRemarksSplit[i];

                                intRecordAffected = ord.deleteOrderRemarks(id, salesRemarks);

                                if (intRecordAffected == 1)
                                {
                                    boolEdited = true;
                                }
                            }
                        }
                    }

                    if (boolSuccess && Session["DELSALES"] != null && Session["DELSALES"] != "")
                    {
                        string strDelSales = Session["DELSALES"].ToString();

                        strDelSales = strDelSales.Substring(clsAdmin.CONSTSYMBOLSEPARATOR.ToString().Length, strDelSales.Length - clsAdmin.CONSTSYMBOLSEPARATOR.ToString().Length);

                        if (!string.IsNullOrEmpty(strDelSales))
                        {
                            strDelSales = strDelSales.Substring(0, strDelSales.Length - clsAdmin.CONSTSYMBOLSEPARATOR.ToString().Length);

                            string[] strDelSalesSplit = strDelSales.Split(clsAdmin.CONSTSYMBOLSEPARATOR);

                            for (int i = 0; i <= strDelSalesSplit.GetUpperBound(0); i++)
                            {
                                string[] strDelSalesItemSplit = strDelSalesSplit[i].Split(new string[] { clsAdmin.CONSTDEFAULTSEPERATOR2 }, StringSplitOptions.None);
                                int intDetailId = 0;
                                int intProdId = 0;

                                intDetailId = int.Parse(strDelSalesItemSplit[0]);
                                intProdId = int.Parse(strDelSalesItemSplit[1]);

                                intRecordAffected = ord.deleteOrderItem(id, intDetailId, intProdId);

                                if (intRecordAffected == 1)
                                {
                                    boolEdited = true;
                                }
                            }
                        }
                    }

                    if (boolSuccess && !ord.isExactSameOrderSet2(id, salesNo, dtSalesDate, customerNo, memId, memCode, salesCustomer, salesCustomerCompName, salesCustomerEmail, salesInvoiceRemarks, billingCompany, billingContactPerson, billingAddress, billingEmail, billingPoscode, billingCity, billingCountry, billingState, billingTel1, billingTel2, billingFax,
                                                                 shippingCompany, shippingContactPerson, shippingAddress, shippingEmail, shippingPoscode, shippingCity, shippingCountry, shippingState, shippingTel1, shippingTel2, shippingFax, billShipSame, decShipping))
                    {
                        intRecordAffected = ord.updateOrderById(id, salesNo, dtSalesDate, customerNo, memId, memCode, salesCustomer, salesCustomerCompName, salesCustomerEmail, salesInvoiceRemarks, billingCompany, billingContactPerson, billingAddress, billingEmail, billingPoscode, billingCity, billingCountry, billingState, billingTel1, billingTel2, billingFax,
                                                                shippingCompany, shippingContactPerson, shippingAddress, shippingEmail, shippingPoscode, shippingCity, shippingCountry, shippingState, shippingTel1, shippingTel2, shippingFax, billShipSame, decShipping, Convert.ToInt16(Session["ADMID"]));

                        if (intRecordAffected == 1)
                        {
                            boolEdited = true;
                        }
                    }
                    break;
            }

            foreach (RepeaterItem rptItem in rptItems.Items)
            {
                int intOri = 0;
                int intDetailId = 0;
                int intProdId = 0;
                string strProdCode = "";
                string strProdName = "";
                string strProdDesc = "";
                int intProdWeight = 0;
                decimal decProdPrice = Convert.ToDecimal("0.00");
                int intProdQty = 0;
                decimal decProdTotal = Convert.ToDecimal("0.00");

                HiddenField hdnSalesOri = (HiddenField)rptItem.FindControl("hdnSalesOri");
                HiddenField hdnDetailSalesId = (HiddenField)rptItem.FindControl("hdnDetailSalesId");
                HiddenField hdnProdId = (HiddenField)rptItem.FindControl("hdnProdId");
                //HiddenField hdnProdCode = (HiddenField)rptItem.FindControl("hdnProdCode");
                Literal litItemName = (Literal)rptItem.FindControl("litItemName");
                Literal litItemDesc = (Literal)rptItem.FindControl("litItemDesc");
                Literal litItemUnitPrice = (Literal)rptItem.FindControl("litItemUnitPrice");
                TextBox txtItemQty = (TextBox)rptItem.FindControl("txtItemQty");
                Literal litItemTotal = (Literal)rptItem.FindControl("litItemTotal");

                string[] strDetailIdSplit = hdnDetailSalesId.Value.Split(new string[] { clsAdmin.CONSTDEFAULTSEPERATOR2 }, StringSplitOptions.None);

                intOri = int.Parse(hdnSalesOri.Value);
                intDetailId = int.Parse(strDetailIdSplit[0]);
                intProdId = int.Parse(hdnProdId.Value);
                //strProdCode = hdnProdCode.Value;
                strProdName = litItemName.Text;
                strProdDesc = litItemDesc.Text;
                decProdPrice = Convert.ToDecimal(litItemUnitPrice.Text);

                if (!string.IsNullOrEmpty(txtItemQty.Text.Trim()))
                {
                    intProdQty = int.Parse(txtItemQty.Text.Trim(), System.Globalization.NumberStyles.AllowThousands);
                    //intProdQty = int.Parse(txtItemQty.Text.Trim());
                }
                else { intProdQty = 0; }

                decProdTotal = Convert.ToDecimal(litItemTotal.Text);

                if (prod.extractProdById4(intProdId, 0))
                {
                    strProdCode = prod.prodCode;
                    intProdWeight = prod.prodWeight;
                }

                if (intOri == 0)
                {
                    intRecordAffected = ord.addOrdItems(id, intProdId, strProdCode, strProdName, strProdDesc, intProdWeight, decProdPrice, intProdQty, decProdTotal);
                }
                else
                {
                    if (!ord.isExactSameDataSet(intDetailId, intProdId, intProdQty))
                    {
                        decProdTotal = intProdQty * decProdPrice;

                        intRecordAffected = ord.updateOrdItems(intDetailId, intProdQty, decProdTotal);
                    }
                }
            }

            /*Shipping*/
            //ord.extractOrderById3(id);
            //Session["SHIPPINGCHECK"] = ord.ordShippingCheck;

            if ((Session["SHIPPINGCHECK"] != null && Session["SHIPPINGCHECK"] != ""))
            {
                intShipCheck = Convert.ToInt16(Session["SHIPPINGCHECK"]);

                if (Session["SHIPPING"] != null && Session["SHIPPING"] != "")
                {
                    decShipping = Convert.ToDecimal(Session["SHIPPING"]);
                }
            }
            else
            {
                //intShipCheck = Convert.ToInt16(Session["SHIPPINGCHECK"]);

                //if (Session["SHIPPING"] == null && Session["SHIPPING"] == "")
                //{
                int intTotalWeight = ord.getProdWeightById(id);
                decimal decProdTotal = ord.getProdTotalById(id);

                clsConfig config = new clsConfig();
                int intShippingType = !string.IsNullOrEmpty(config.shippingType) ? Convert.ToInt16(config.shippingType) : clsAdmin.CONSTSHIPPINGTYPE1;

                if (intShippingType == clsAdmin.CONSTSHIPPINGTYPE1)
                {
                    decShipping = clsMis.getShippingFee(shippingCountry, shippingState, intTotalWeight, decProdTotal);
                }
                else if (intShippingType == clsAdmin.CONSTSHIPPINGTYPE2)
                {
                    decShipping = clsMis.getShippingFee2(shippingCountry, shippingState, intTotalWeight, decProdTotal);
                }
                else if (intShippingType == clsAdmin.CONSTSHIPPINGTYPE3)
                {
                    decShipping = clsMis.getShippingFee3(shippingCountry, shippingState, intTotalWeight, decProdTotal);
                }
                //}
            }

            ord.updateOrderShipping2(id, decShipping, intShipCheck);
            //if (type == 1)
            //{
            //    int intTotalWeight = ord.getProdWeightById(id);
            //    decimal decProdTotal = ord.getProdTotalById(id);

            //    clsConfig config = new clsConfig();
            //    int intShippingType = !string.IsNullOrEmpty(config.shippingType) ? Convert.ToInt16(config.shippingType) : clsAdmin.CONSTSHIPPINGTYPE1;

            //    if (intShippingType == clsAdmin.CONSTSHIPPINGTYPE1)
            //    {
            //        decShipping = clsMis.getShippingFee(shippingCountry, shippingState, intTotalWeight, decProdTotal);
            //    }
            //    else if (intShippingType == clsAdmin.CONSTSHIPPINGTYPE2)
            //    {
            //        decShipping = clsMis.getShippingFee2(shippingCountry, shippingState, intTotalWeight, decProdTotal);
            //    }
            //    else if (intShippingType == clsAdmin.CONSTSHIPPINGTYPE3)
            //    {
            //        decShipping = clsMis.getShippingFee3(shippingCountry, shippingState, decProdTotal);
            //    }
            //}
            //else
            //{
            //    if (Session["SHIPPING"] != null && Session["SHIPPING"] != "")
            //    {
            //        decShipping = Convert.ToDecimal(Session["SHIPPING"]);
            //    }
            //}
            /*End of Shipping*/

            if (boolAdded)
            {
                Session["NEWSALESID"] = id;
                Response.Redirect(currentPageName + "?id=" + id);
            }
            else if (boolEdited)
            {
                Session["EDITEDSALESID"] = id;
                Response.Redirect(currentPageName + "?id=" + id);
            }
            else
            {
                if (boolSuccess)
                {
                    Session["EDITEDSALESID"] = id;
                    Session["NOCHANGE"] = 1;
                    Response.Redirect(currentPageName + "?id=" + id);
                }
                else
                {
                    ord.extractOrderById2(id);
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit sales (" + ord.ordNo + "). Please try again.</div>";

                    Response.Redirect(currentPageName);
                }
            }
            /*End of Sales Items*/
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string strDeletedSalesNo = "";

        clsMis.performDeleteOrder(id, ref strDeletedSalesNo);

        Session["DELETEDSALESID"] = strDeletedSalesNo;
        Response.Redirect(ConfigurationManager.AppSettings["scriptBase"].ToString() + "adm/admOrder01.aspx");
    }

    protected void lnkbtnInvoicePdf_Click(object sender, EventArgs e)
    {
        clsMis mis = new clsMis();
        mis.printInvoice(id, 1);
    }

    protected void lnkbtnDeliveryPdf_Click(object sender, EventArgs e)
    {
        clsMis mis = new clsMis();
        mis.printInvoice(id, 2);
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        registerScript();
        registerCKEditorScript();

        if (!IsPostBack)
        {
            setPageProperties();

            switch (mode)
            {
                case 1://Add Sales
                    Session["SHIPPINGCHECK"] = 1;
                    break;
                case 2://Edit Sales
                    trSalesType.Visible = true;
                    trSalesAmount.Visible = true;
                    trSalesRemarks.Visible = true;
                    divSalesStatus.Visible = true;
                    lnkbtnQuickDone.Visible = true;
                    divInvoicePrint.Visible = true;

                    hypCancel.Visible = true;
                    hypDelete.Visible = true;

                    fillForm();
                    break;
                default:
                    break;
            }

            populateItems();

            lnkbtnInvoicePrint.OnClientClick = "javascript:window.open('admPrintInvoice.aspx?id=" + id + "&ptype=1&otype=" + type + "','_blank','toolbar=false,menubar=false,scrollbars,width=900,height=1000','false'); return false";
            lnkbtnDeliveryPrint.OnClientClick = "javascript:window.open('admPrintInvoice.aspx?id=" + id + "&ptype=2&otype=" + type + "','_blank','toolbar=false,menubar=false,scrollbars,width=900,height=1000','false'); return false";
        }

        populateRemarks();
    }

    public void registerScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("FORMSECT3")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "initSect3();";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "FORMSECT3", strJS, false);
        }
    }

    public void registerScript2(string strPayDateId)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("FORMSECT4")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "initSect4('" + strPayDateId + "');";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "FORMSECT4", strJS, false);
        }
    }

    public void registerScript3(string strDeliveryDateId)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("FORMSECT5")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "initSect5('" + strDeliveryDateId + "');";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "FORMSECT5", strJS, false);
        }
    }

    public void registerCKEditorScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">" +
                    "CKEDITOR.replace('" + txtRemarksInvoice.ClientID + "'," +
                    "{" +
                    "width:'99%'," +
                    "height:'300'," +
                    "toolbar: 'User3',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSPRODVIEW + "&command=QuickUpload&type=Files'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSPRODVIEW + "&command=QuickUpload&type=Images'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSPRODVIEW + "&command=QuickUpload&type=Flash'" +
                     "}" +
                     ");" +
                     "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR", strJS, false);
        }
    }

    protected string generateMemCode()
    {
        clsMember mem = new clsMember();
        string strMemCode = null;

        int intSequence = mem.generateSequenceNo();
        string strSequence = intSequence.ToString();
        strSequence = strSequence.PadLeft(4, '0');

        string strMonth = DateTime.Now.Month.ToString();
        strMonth = strMonth.PadLeft(2, '0');

        string strYear = DateTime.Now.Year.ToString();

        strMemCode = "MEM" + strYear + strMonth + strSequence;

        return strMemCode;
    }

    protected void setPageProperties()
    {
        lnkbtnQuickDone.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmQuickDone.Text").ToString() + "'); return false;";

        hypCancel.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admCancelTrans.aspx?id=" + id + "&" + clsAdmin.CONSTUSRARTICLETHICKBOX;
        hypDelete.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admDeleteTrans.aspx?id=" + id + "&" + clsAdmin.CONSTUSRARTICLETHICKBOX;

        clsOrder ord = new clsOrder();
        litLastSalesNo.Text = "(Last Sale No. " + ord.getLastOfflineOrd() + ")";

        txtSalesDate.Text = DateTime.Now.ToString("dd MMM yyyy");

        clsCountry country = new clsCountry();
        clsMember mem = new clsMember();
        DataSet ds = new DataSet();
        DataView dv;

        ds = new DataSet();
        ds = country.getCountryList(1);
        dv = new DataView(ds.Tables[0]);
        dv.RowFilter = "COUNTRY_DEFAULT = 0";
        dv.Sort = "COUNTRY_NAME ASC";

        ddlBillingCountry.DataSource = dv;
        ddlBillingCountry.DataTextField = "COUNTRY_NAME";
        ddlBillingCountry.DataValueField = "COUNTRY_CODE";
        ddlBillingCountry.DataBind();

        ddlDeliveryCountry.DataSource = dv;
        ddlDeliveryCountry.DataTextField = "COUNTRY_NAME";
        ddlDeliveryCountry.DataValueField = "COUNTRY_CODE";
        ddlDeliveryCountry.DataBind();

        ddlBillingCountry.SelectedValue = "MY";
        if (!string.IsNullOrEmpty(ddlBillingCountry.SelectedValue))
        {
            DataSet dsState = new DataSet();
            dsState = country.getStateList(1);
            DataView dvState;

            string countryCode = ddlBillingCountry.SelectedValue;

            if (dsState.Tables.Count > 0)
            {
                dvState = new DataView(dsState.Tables[0]);

                dvState.RowFilter = "STATE_DEFAULT = 0 AND STATE_COUNTRY = 'MY'";

                if (dvState.Count > 0)
                {
                    ddlBillingState.DataSource = dvState;
                    ddlBillingState.DataTextField = "STATE_CODE";
                    ddlBillingState.DataValueField = "STATE_CODE";
                    ddlBillingState.DataBind();

                    ListItem ddlBillingStateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                    ddlBillingState.Items.Insert(0, ddlBillingStateDefaultItem);

                    tdBillingStateDdl.Visible = true;
                    tdBillingStateTxt.Visible = false;
                }
                else
                {
                    txtBillingState.Text = "";

                    tdBillingStateDdl.Visible = false;
                    tdBillingStateTxt.Visible = true;
                }
            }
            else
            {
                txtBillingState.Text = "";

                tdBillingStateDdl.Visible = false;
                tdBillingStateTxt.Visible = true;
            }
        }
        else
        {
            ddlBillingState.Items.Clear();

            ListItem ddlBillingStateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
            ddlBillingState.Items.Insert(0, ddlBillingStateDefaultItem);

            tdBillingStateDdl.Visible = true;
            tdBillingStateTxt.Visible = false;
        }

        ListItem ddlBillingCountryDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlBillingCountry.Items.Insert(0, ddlBillingCountryDefaultItem);

        ddlDeliveryCountry.SelectedValue = "MY";
        if (!string.IsNullOrEmpty(ddlDeliveryCountry.SelectedValue))
        {
            DataSet dsState = new DataSet();
            dsState = country.getStateList(1);
            DataView dvState;

            //string countryCode = ddlDeliveryCountry.SelectedValue;

            if (dsState.Tables.Count > 0)
            {
                dvState = new DataView(dsState.Tables[0]);

                dvState.RowFilter = "STATE_DEFAULT = 0 AND STATE_COUNTRY = 'MY'";

                if (dvState.Count > 0)
                {
                    ddlDeliveryState.DataSource = dvState;
                    ddlDeliveryState.DataTextField = "STATE_CODE";
                    ddlDeliveryState.DataValueField = "STATE_CODE";
                    ddlDeliveryState.DataBind();

                    ListItem ddlStateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                    ddlDeliveryState.Items.Insert(0, ddlStateDefaultItem);

                    tdDeliveryStateDdl.Visible = true;
                    tdDeliveryStateTxt.Visible = false;
                }
                else
                {
                    txtDeliveryState.Text = "";

                    tdDeliveryStateDdl.Visible = false;
                    tdDeliveryStateTxt.Visible = true;
                }
            }
            else
            {
                txtDeliveryState.Text = "";

                tdDeliveryStateDdl.Visible = false;
                tdDeliveryStateTxt.Visible = true;
            }
        }
        else
        {
            ddlDeliveryState.Items.Clear();

            ListItem ddlStateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
            ddlDeliveryState.Items.Insert(0, ddlStateDefaultItem);

            tdDeliveryStateDdl.Visible = true;
            tdDeliveryStateTxt.Visible = false;
        }

        ListItem ddlDeliveryCountryDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlDeliveryCountry.Items.Insert(0, ddlDeliveryCountryDefaultItem);

        ds = mem.getMemberCompanyEmail(0);
        dv = new DataView(ds.Tables[0]);

        cboxCustomer.DataSource = dv;
        cboxCustomer.DataTextField = "MEM_COMPANYEMAIL";
        cboxCustomer.DataValueField = "MEM_ID";
        cboxCustomer.DataBind();
    }

    protected void fillForm()
    {
        clsOrder ord = new clsOrder();

        if (ord.extractOrderById3(id))
        {
            if (ord.ordCancel == 1 || ord.ordDone == 1)
            {
                lnkbtnQuickDone.Visible = false;
                trAddItems.Visible = false;
                lnkbtnSave.Visible = false;
                hypCancel.Visible = false;
                hypDelete.Visible = false;
            }

            if (ord.ordRead == 0)
            {
                ord.updateOrderRead(id);
            }

            if (ord.ordShippingCheck == 1)
            {
                Session["SHIPPINGCHECK"] = ord.ordShippingCheck;
            }
            else { Session["SHIPPINGCHECK"] = null; }

            status = ord.ordStatus;
            type = ord.ordType;
            paymentId = ord.ordPaymentId;

            if (!string.IsNullOrEmpty(ord.ordShipping.ToString()))
            {
                Session["SHIPPING"] = ord.ordShipping;
            }
            //decShipping = ord.ordShipping;

            decTotal = ord.ordTotal;

            String.Format("{0:C}", ord.ordTotal);

            deliveryDate = ord.shippingDate.ToString("dd MMM yyyy");
            consignmentNo = ord.shippingTrackCode;
            courier = ord.shippingCompanyName;
            chkboxDeliveryDetails.Checked = ord.ordBillShipSame == 1;

            if (ord.ordType == 1) { litType.Text = "Online Sales"; }
            else if (ord.ordType == 2) { litType.Text = "Offline Sales"; }
            txtSalesNo.Text = ord.ordNo;
            txtSalesDate.Text = ord.ordCreation.ToString("dd MMM yyyy");
            txtCustomerNo.Text = ord.customerNo;
            cboxCustomer.SelectedValue = ord.memId.ToString();
            litSalesAmount.Text = String.Format("{0:N}", ord.ordTotal);//ord.ordTotal.ToString();

            txtRemarksInvoice.Text = ord.ordRemarks;

            clsCountry country = new clsCountry();
            DataSet dsState = new DataSet();
            dsState = country.getStateList(1);
            DataView dvState;

            /*Billing Details*/
            txtBillingCompany.Text = ord.company;
            txtBillingContactPerson.Text = ord.name;
            txtBillingAddress.Text = ord.add;

            if (string.IsNullOrEmpty(ord.email))
            {
                txtBillingEmail.Text = ord.memEmail;
            }
            else { txtBillingEmail.Text = ord.email; }

            txtBillingPoscode.Text = ord.poscode;
            txtBillingTel1.Text = ord.contactNo;
            txtBillingTel2.Text = ord.contactNo2;
            txtBillingFax.Text = ord.fax;
            txtBillingCity.Text = ord.city;

            if (!string.IsNullOrEmpty(ord.country)) { ddlBillingCountry.SelectedValue = ord.country.ToString(); }

            if (dsState.Tables.Count > 0)
            {
                dvState = new DataView(dsState.Tables[0]);
                dvState.RowFilter = "STATE_COUNTRY = '" + ddlBillingCountry.SelectedValue + "'";

                if (dvState.Count > 0)
                {
                    ddlBillingState.DataSource = dvState;
                    ddlBillingState.DataTextField = "STATE_NAME";
                    ddlBillingState.DataValueField = "STATE_CODE";
                    ddlBillingState.DataBind();

                    ListItem ddlBillingStateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                    ddlBillingState.Items.Insert(0, ddlBillingStateDefaultItem);

                    try
                    {
                        if (!string.IsNullOrEmpty(ord.state))
                        {
                            ddlBillingState.SelectedValue = ord.state;
                        }
                    }
                    catch (Exception ex)
                    {
                        ddlBillingState.SelectedValue = "";
                    }

                    tdBillingStateDdl.Visible = true;
                    tdBillingStateTxt.Visible = false;
                }
                else
                {
                    txtBillingState.Text = ord.state;

                    tdBillingStateDdl.Visible = false;
                    tdBillingStateTxt.Visible = true;
                }
            }
            else
            {
                txtBillingState.Text = ord.state;

                tdBillingStateDdl.Visible = false;
                tdBillingStateTxt.Visible = true;
            }
            /*End of Billing Details*/

            /*Shipping Details*/
            txtDeliveryCompany.Text = ord.shippingCustCompany;
            txtDeliveryContactPerson.Text = ord.shippingName;
            txtDeliveryAddress.Text = ord.shippingAdd;
            txtDeliveryEmail.Text = ord.shippingEmail;
            txtDeliveryPoscode.Text = ord.shippingPoscode;
            txtDeliveryCity.Text = ord.shippingCity;
            txtDeliveryTel1.Text = ord.shippingContactNo;
            txtDeliveryTel2.Text = ord.shippingContactNo2;
            txtDeliveryFax.Text = ord.shippingFax;

            if (!string.IsNullOrEmpty(ord.shippingCountry)) { ddlDeliveryCountry.SelectedValue = ord.shippingCountry.ToString(); }

            if (dsState.Tables.Count > 0)
            {
                dvState = new DataView(dsState.Tables[0]);
                dvState.RowFilter = "STATE_COUNTRY = '" + ddlDeliveryCountry.SelectedValue + "'";

                if (dvState.Count > 0)
                {
                    ddlDeliveryState.DataSource = dvState;
                    ddlDeliveryState.DataTextField = "STATE_NAME";
                    ddlBillingState.DataValueField = "STATE_CODE";
                    ddlDeliveryState.DataBind();

                    ListItem ddlDeliveryStateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                    ddlDeliveryState.Items.Insert(0, ddlDeliveryStateDefaultItem);

                    try
                    {
                        if (!string.IsNullOrEmpty(ord.shippingState))
                        {
                            ddlDeliveryState.SelectedValue = ord.shippingState;
                        }
                    }
                    catch (Exception ex)
                    {
                        ddlDeliveryState.SelectedValue = "";
                    }

                    tdDeliveryStateDdl.Visible = true;
                    tdDeliveryStateTxt.Visible = false;
                }
                else
                {
                    txtDeliveryState.Text = ord.shippingState;

                    tdDeliveryStateDdl.Visible = false;
                    tdDeliveryStateTxt.Visible = true;
                }
            }
            else
            {
                txtDeliveryState.Text = ord.shippingState;

                tdDeliveryStateDdl.Visible = false;
                tdDeliveryStateTxt.Visible = true;
            }
            /*End of Shipping Details*/
        }

        DataSet ds = new DataSet();
        /*Remarks*/
        ds = ord.getOrdRemarksList(id);
        DataView dv = new DataView(ds.Tables[0]);

        foreach (DataRowView row in dv)
        {
            Session["ORISALESREMARKS"] += row["ORDREMARKS_REMARK"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR.ToString();
        }
        /*End of Remarks*/

        /*Items*/
        Session["ORISALES"] = null;

        //dtSales = null;

        ds = ord.getOrderItemsByOrderId2(id);

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            Session["ORISALES"] += row["ORDDETAIL_ID"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2;
            Session["ORISALES"] += row["ORDDETAIL_PRODID"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2;
            Session["ORISALES"] += row["ORDDETAIL_PRODNAME"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2;
            Session["ORISALES"] += row["ORDDETAIL_PRODSNAPSHOT"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2;
            Session["ORISALES"] += row["ORDDETAIL_PRODPRICE"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2;
            Session["ORISALES"] += row["ORDDETAIL_PRODQTY"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2;
            Session["ORISALES"] += row["ORDDETAIL_PRODTOTAL"].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR2;
            Session["ORISALES"] += "1" + clsAdmin.CONSTSYMBOLSEPARATOR;
        }
        /*End of Items*/
    }

    protected void bindRptData()
    {
        DataView dv;

        if (dtSales != null)
        {
            intItemCount = 0;
            DataTable dt = dtSales;
            dv = new DataView(dt);

            PagedDataSource pds = new PagedDataSource();
            pds.DataSource = dv;

            rptItems.DataSource = pds;
            rptItems.DataBind();
        }

        clsOrder ord = new clsOrder();
        DataSet ds = new DataSet();

        ds = ord.getOrderStatus(id);

        DataRow[] foundRow = ds.Tables[0].Select("ORDERSTATUS_ID > 0", "ORDERSTATUS_ORDER DESC");

        if (foundRow.Length > 0) { lastStatus = Convert.ToInt16(foundRow[0]["ORDERSTATUS_ID"]); }

        dv = new DataView(ds.Tables[0]);

        dv.RowFilter = "ORDERSTATUS_ID > 0 AND ORDERSTATUS_ACTIVE = 1 AND ORDERSTATUS_PARENTVALUE = 1";
        dv.Sort = "ORDERSTATUS_ORDER ASC";

        PagedDataSource pdsStatus = new PagedDataSource();
        pdsStatus.DataSource = dv;
        intCurStatus = -1;
        rptStatus.DataSource = pdsStatus;
        rptStatus.DataBind();
    }

    protected DataTable createDataTable()
    {
        DataTable dtSel = new DataTable();
        DataColumn col;

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.Int16");
        col.ColumnName = "ORDDETAIL_ID";
        col.DefaultValue = 0;
        dtSel.Columns.Add(col);

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.Int16");
        col.ColumnName = "ORDDETAIL_PRODID";
        col.DefaultValue = 0;
        dtSel.Columns.Add(col);

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.String");
        col.ColumnName = "ORDDETAIL_PRODNAME";
        dtSel.Columns.Add(col);

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.String");
        col.ColumnName = "ORDDETAIL_PRODSNAPSHOT";
        dtSel.Columns.Add(col);

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.Decimal");
        col.ColumnName = "ORDDETAIL_PRODPRICE";
        col.DefaultValue = 0;
        dtSel.Columns.Add(col);

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.Int32");
        col.ColumnName = "ORDDETAIL_PRODQTY";
        col.DefaultValue = 0;
        dtSel.Columns.Add(col);

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.Decimal");
        col.ColumnName = "ORDDETAIL_PRODTOTAL";
        col.DefaultValue = 0;
        dtSel.Columns.Add(col);

        col = new DataColumn();
        col.DataType = System.Type.GetType("System.Int16");
        col.ColumnName = "SALES_ORI";
        col.DefaultValue = 0;
        dtSel.Columns.Add(col);

        return dtSel;
    }

    protected void oriFileUploadItems_OnDelete(object sender, CommandEventArgs e)
    {
        string strFileUploadItemToDelete = e.CommandArgument.ToString();
        string[] strSplit = strFileUploadItemToDelete.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

        Session["DELSALESREMARKS"] += strSplit[0] + clsAdmin.CONSTDEFAULTSEPERATOR;
        populateRemarks();
    }

    protected void fileUploadItems1_OnDelete(object sender, CommandEventArgs e)
    {
        string strFileUploadItemToDelete = e.CommandArgument.ToString();
        string[] strSplit = strFileUploadItemToDelete.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

        Session["SALESREMARKS"] = Session["SALESREMARKS"].ToString().Replace(clsAdmin.CONSTDEFAULTSEPERATOR + strSplit[0] + clsAdmin.CONSTDEFAULTSEPERATOR, clsAdmin.CONSTDEFAULTSEPERATOR.ToString());
        populateRemarks();
    }

    protected void populateItems()
    {
        string strOriSales = "";
        if (Session["ORISALES"] != null)
        {
            if (!Session["ORISALES"].ToString().StartsWith(clsAdmin.CONSTSYMBOLSEPARATOR.ToString()))
            {
                Session["ORISALES"] = clsAdmin.CONSTSYMBOLSEPARATOR.ToString() + Session["ORISALES"];
            }

            strOriSales = Session["ORISALES"].ToString();
        }

        string strSales = "";
        if (Session["SALES"] != null)
        {
            if (!Session["SALES"].ToString().StartsWith(clsAdmin.CONSTSYMBOLSEPARATOR.ToString()))
            {
                Session["SALES"] = clsAdmin.CONSTSYMBOLSEPARATOR.ToString() + Session["SALES"];
            }

            strSales = Session["SALES"].ToString();
        }

        string strDelSales = "";
        if (Session["DELSALES"] != null)
        {
            if (!Session["DELSALES"].ToString().StartsWith(clsAdmin.CONSTSYMBOLSEPARATOR.ToString()))
            {
                Session["DELSALES"] = clsAdmin.CONSTSYMBOLSEPARATOR.ToString() + Session["DELSALES"];
            }

            strDelSales = Session["DELSALES"].ToString();
        }

        DataTable dt = new DataTable();
        dt = createDataTable();

        if (!string.IsNullOrEmpty(strOriSales))
        {
            strOriSales = strOriSales.Substring(0, strOriSales.Length - clsAdmin.CONSTSYMBOLSEPARATOR.ToString().Length);

            if (!string.IsNullOrEmpty(strOriSales))
            {
                strOriSales = strOriSales.Substring(clsAdmin.CONSTSYMBOLSEPARATOR.ToString().Length, strOriSales.Length - clsAdmin.CONSTSYMBOLSEPARATOR.ToString().Length);

                string[] fileSalesItems = strOriSales.Split(clsAdmin.CONSTSYMBOLSEPARATOR);

                for (int i = 0; i <= fileSalesItems.GetUpperBound(0); i++)
                {
                    if (strDelSales.IndexOf(clsAdmin.CONSTSYMBOLSEPARATOR.ToString() + fileSalesItems[i] + clsAdmin.CONSTSYMBOLSEPARATOR.ToString()) < 0)
                    {
                        string[] fileSalesItemsSplit = fileSalesItems[i].Split(new string[] { clsAdmin.CONSTDEFAULTSEPERATOR2 }, StringSplitOptions.None);

                        DataRow row;
                        row = dt.NewRow();

                        int intTryParse;

                        if (int.TryParse(fileSalesItemsSplit[0], out intTryParse))
                        {
                            row["ORDDETAIL_ID"] = intTryParse;
                        }

                        if (int.TryParse(fileSalesItemsSplit[1], out intTryParse))
                        {
                            row["ORDDETAIL_PRODID"] = intTryParse;
                        }

                        row["ORDDETAIL_PRODNAME"] = fileSalesItemsSplit[2];
                        row["ORDDETAIL_PRODSNAPSHOT"] = fileSalesItemsSplit[3];

                        decimal decTryParse;
                        if (decimal.TryParse(fileSalesItemsSplit[4], out decTryParse))
                        {
                            row["ORDDETAIL_PRODPRICE"] = decTryParse;
                        }

                        if (int.TryParse(fileSalesItemsSplit[5], out intTryParse))
                        {
                            row["ORDDETAIL_PRODQTY"] = intTryParse;
                        }

                        if (decimal.TryParse(fileSalesItemsSplit[6], out decTryParse))
                        {
                            row["ORDDETAIL_PRODTOTAL"] = decTryParse;
                        }

                        if (int.TryParse(fileSalesItemsSplit[7], out intTryParse))
                        {
                            row["SALES_ORI"] = intTryParse;
                        }

                        dt.Rows.Add(row);
                    }
                }
            }
        }

        if (!string.IsNullOrEmpty(strSales))
        {
            strSales = strSales.Substring(0, strSales.Length - clsAdmin.CONSTSYMBOLSEPARATOR.ToString().Length);

            if (!string.IsNullOrEmpty(strSales))
            {
                strSales = strSales.Substring(clsAdmin.CONSTSYMBOLSEPARATOR.ToString().Length, strSales.Length - clsAdmin.CONSTSYMBOLSEPARATOR.ToString().Length);

                string[] fileSalesItems = strSales.Split(clsAdmin.CONSTSYMBOLSEPARATOR);

                for (int i = 0; i <= fileSalesItems.GetUpperBound(0); i++)
                {
                    string[] fileSalesItemsSplit = fileSalesItems[i].Split(new string[] { clsAdmin.CONSTDEFAULTSEPERATOR2 }, StringSplitOptions.None);

                    DataRow row;
                    row = dt.NewRow();

                    int intTryParse;

                    if (int.TryParse(fileSalesItemsSplit[0], out intTryParse))
                    {
                        row["ORDDETAIL_ID"] = intTryParse;
                    }

                    if (int.TryParse(fileSalesItemsSplit[1], out intTryParse))
                    {
                        row["ORDDETAIL_PRODID"] = intTryParse;
                    }

                    row["ORDDETAIL_PRODNAME"] = fileSalesItemsSplit[2];
                    row["ORDDETAIL_PRODSNAPSHOT"] = fileSalesItemsSplit[3];

                    decimal decTryParse;
                    if (decimal.TryParse(fileSalesItemsSplit[4], out decTryParse))
                    {
                        row["ORDDETAIL_PRODPRICE"] = decTryParse;
                    }

                    if (int.TryParse(fileSalesItemsSplit[5], out intTryParse))
                    {
                        row["ORDDETAIL_PRODQTY"] = intTryParse;
                    }

                    if (decimal.TryParse(fileSalesItemsSplit[6], out decTryParse))
                    {
                        row["ORDDETAIL_PRODTOTAL"] = decTryParse;
                    }

                    if (int.TryParse(fileSalesItemsSplit[7], out intTryParse))
                    {
                        row["SALES_ORI"] = intTryParse;
                    }

                    dt.Rows.Add(row);
                }
            }
        }

        dtSales = dt;
        bindRptData();
    }

    protected void populateRemarks()
    {
        pnlSalesRemarksList.Controls.Clear();
        int intCount = 0;

        try
        {
            string strOriFileRemarks = "";
            if (Session["ORISALESREMARKS"] != null)
            {
                if (!Session["ORISALESREMARKS"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["ORISALESREMARKS"] = clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + Session["ORISALESREMARKS"]; }
                strOriFileRemarks = Session["ORISALESREMARKS"].ToString();
            }

            string strFileRemarks = "";
            if (Session["SALESREMARKS"] != null)
            {
                if (!Session["SALESREMARKS"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["SALESREMARKS"] = clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + Session["SALESREMARKS"]; }
                strFileRemarks = Session["SALESREMARKS"].ToString();
            }

            string strDelFileRemarks = "";
            if (Session["DELSALESREMARKS"] != null)
            {
                if (!Session["DELSALESREMARKS"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["DELSALESREMARKS"] = clsAdmin.CONSTDEFAULTSEPERATOR.ToString() + Session["DELSALESREMARKS"]; }
                strDelFileRemarks = Session["DELSALESREMARKS"].ToString();
            }

            if (!string.IsNullOrEmpty(strOriFileRemarks) || !string.IsNullOrEmpty(strFileRemarks))
            {
                HtmlTable tbl = new HtmlTable();
                tbl.CellPadding = 0;
                tbl.CellSpacing = 0;
                tbl.Attributes.Add("class", "dataTbl");
                pnlSalesRemarksList.Controls.Add(tbl);

                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cell1 = new HtmlTableCell("th");
                HtmlTableCell cell2 = new HtmlTableCell("th");
                HtmlTableCell cell3 = new HtmlTableCell("th");

                // for table header
                cell1.Controls.Add(new LiteralControl("No."));
                cell2.Controls.Add(new LiteralControl("Name"));
                cell3.Controls.Add(new LiteralControl("Action"));

                cell1.Attributes["class"] = "tdNo";
                cell2.Attributes["style"] = "width:90%;";
                cell3.Attributes["style"] = "text-align:center;";

                row.Controls.Add(cell1);
                row.Controls.Add(cell2);
                row.Controls.Add(cell3);
                tbl.Controls.Add(row);


                if (!string.IsNullOrEmpty(strOriFileRemarks))
                {
                    strOriFileRemarks = strOriFileRemarks.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strOriFileRemarks.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                }

                if (!string.IsNullOrEmpty(strOriFileRemarks))
                {
                    strOriFileRemarks = strOriFileRemarks.Substring(0, strOriFileRemarks.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                    LinkButton lnkbtn;
                    string[] fileItems = strOriFileRemarks.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                    for (int i = 0; i <= fileItems.GetUpperBound(0); i++)
                    {
                        if (strDelFileRemarks.IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + fileItems[i] + clsAdmin.CONSTDEFAULTSEPERATOR) < 0)
                        {
                            row = new HtmlTableRow();

                            intCount += 1;
                            cell1 = new HtmlTableCell();
                            cell2 = new HtmlTableCell();
                            cell3 = new HtmlTableCell();

                            cell1.Controls.Add(new LiteralControl(intCount.ToString() + "."));
                            cell2.Controls.Add(new LiteralControl(fileItems[i]));

                            lnkbtn = new LinkButton();
                            lnkbtn.CssClass = "lnkbtn lnkbtnDelete";
                            lnkbtn.ID = intCount + "_delete_" + i;
                            lnkbtn.Text = "delete";
                            lnkbtn.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteOrdRemark.Text").ToString() + "'); return false;";
                            lnkbtn.Command += new CommandEventHandler(oriFileUploadItems_OnDelete);
                            lnkbtn.CommandArgument = fileItems[i];
                            lnkbtn.CausesValidation = false;
                            cell3.Attributes["class"] = "tdAct";
                            cell3.Controls.Add(lnkbtn);

                            row.Controls.Add(cell1);
                            row.Controls.Add(cell2);
                            row.Controls.Add(cell3);
                            tbl.Controls.Add(row);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(strFileRemarks))
                {
                    strFileRemarks = strFileRemarks.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strFileRemarks.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                }

                if (!string.IsNullOrEmpty(strFileRemarks))
                {
                    strFileRemarks = strFileRemarks.Substring(0, strFileRemarks.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                    LinkButton lnkbtn;
                    string[] fileItems = strFileRemarks.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                    for (int i = 0; i <= fileItems.GetUpperBound(0); i++)
                    {
                        row = new HtmlTableRow();

                        intCount += 1;
                        cell1 = new HtmlTableCell();
                        cell2 = new HtmlTableCell();
                        cell3 = new HtmlTableCell();

                        cell1.Controls.Add(new LiteralControl(intCount.ToString() + "."));
                        cell2.Controls.Add(new LiteralControl(fileItems[i]));

                        lnkbtn = new LinkButton();
                        lnkbtn.CssClass = "lnkbtn lnkbtnDelete";
                        lnkbtn.ID = intCount + "_delete_" + i;
                        lnkbtn.Text = "delete" + "<br/>";
                        lnkbtn.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteOrdRemark.Text").ToString() + "'); return false;";
                        lnkbtn.Command += new CommandEventHandler(fileUploadItems1_OnDelete);
                        lnkbtn.CommandArgument = fileItems[i];
                        cell3.Attributes["class"] = "tdAct";
                        cell3.Controls.Add(lnkbtn);


                        row.Controls.Add(cell1);
                        row.Controls.Add(cell2);
                        row.Controls.Add(cell3);
                        tbl.Controls.Add(row);
                    }
                }

                //row.Controls.Add(cell);
                //tbl.Controls.Add(row);
            }
        }
        catch (Exception ex)
        {
            clsLog.logErroMsg(ex.Message.ToString());
            throw ex;
        }
    }

    /*Send Email*/
    //Payment Received Email
    protected void sendDetails(string strUserName, string strUserEmail, int intId, string strOrdNo, decimal decOrdTotal, string strPaymentDetails)
    {
        string strSubjectPrefix;
        string strSignature;
        string strSubject;
        string strBody;
        string strUserBody;
        string strSenderName;

        clsConfig config = new clsConfig();
        string strCurrency = config.currency;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYRECBTEMAILSUBJECT, clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT, 1);
        strSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYRECBTADMINEMAILCONTENT, clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT, 1);
        strBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYRECBTUSEREMAILCONTENT, clsConfig.CONSTGROUPPAYRECBTEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;

        strBody = strBody.Replace("[NAME]", strUserName);
        strBody = strBody.Replace("[ORDNO]", strOrdNo);
        strBody = strBody.Replace("[CUR]", strCurrency);
        strBody = strBody.Replace("[ORDTOTAL]", decOrdTotal.ToString());
        strBody = strBody.Replace("[PAYDETAILS]", strPaymentDetails);
        strBody = strBody.Replace("[SIGNATURE]", strSignature);

        strUserBody = strUserBody.Replace("[NAME]", strUserName);
        strUserBody = strUserBody.Replace("[ORDNO]", strOrdNo);
        strUserBody = strUserBody.Replace("[SIGNATURE]", strSignature);

        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strSubject = strSubjectPrefix + " " + strSubject; }

        if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        {
            if (clsEmail.send(strSenderName, "", strUserEmail, "", "", "", strSubject, strUserBody, 1))
            {

            }
            else
            {
                clsLog.logErroMsg("Failed to send order details.");
            }
        }
        else
        {
            clsLog.logErroMsg("Failed to send order details.");
        }

        //int intPageIdProfilePage = clsPage.getPageIdByTemplate(clsAdmin.CONSTUSRPROFILEPAGE, "", 1);
        //string strOrderStatusUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + clsAdmin.CONSTUSRPROFILEPAGE + "?pgid=" + intPageIdProfilePage + "&frm=" + clsAdmin.CONSTPROFILEORDERSTATUS;

        //string strSubject;
        //string strBody;

        //string strUserSubject;
        //string strUserBody;
        //string strSenderName = GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString();

        //clsConfig config = new clsConfig();
        //string strCurrency = config.currency;
        //string strSubjectPrefix = "";// config.emailSubject;

        //config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        //string strSignature = config.longValue; //config.emailSignature;

        //strSubject = strSubjectPrefix + " Payment Received";
        //strBody = "Dear Admin, <br /><br />Payment from " + GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString() + " has been made.<br /><br />The order details as below:<br />";
        //strBody += "OrderID: " + strOrdNo + "<br />";
        //strBody += "Order Total: " + strCurrency + " " + decOrdTotal + "<br />";

        //strBody += "<br />Here is the payment details:<br />";
        //strBody += strPaymentDetails;
        //strBody += "<br /><br />Thank you.<br /><br />";
        //strBody += strSignature;
        //strBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strBody + "</font>";
        ////strBody = strBody.Replace(Environment.NewLine, "<br />");

        //strUserSubject = strSubjectPrefix + " Payment Received";
        //strUserBody = "Dear " + strUserName + ",";
        //strUserBody += "<br /><br /><b>Your order " + strOrdNo + " history</b>";
        //strUserBody += "<br /><br />Thank you for your payment.";
        //strUserBody += "<br /><br />Your payment has been verified and we will proceed to pack and deliver your package within the next 2 working days.";
        //strUserBody += "<br /><br />Another email will be sent to you once your order is being processed.";
        ////strUserBody += "<br /><br />You can review and download your invoice from the <a href='" + strOrderStatusUrl + "'>Check Order Status</a> of your account on our Website.";
        //strUserBody += "<br /><br />Thank you.<br /><br />";
        //strUserBody += strSignature;
        //strUserBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strUserBody + "</font>";
        ////strUserBody = strUserBody.Replace(Environment.NewLine, "<br />");

        //if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        //{
        //    if (clsEmail.send(strSenderName, "", strUserEmail, "", "", "", strUserSubject, strUserBody, 1))
        //    {

        //    }
        //    else
        //    {
        //        clsLog.logErroMsg("Failed to send order details.");
        //    }
        //}
        //else
        //{
        //    clsLog.logErroMsg("Failed to send order details.");
        //}
    }

    //Payment Reject Email
    protected Boolean sendDetail(string strEmail, string strName, string strNo, decimal decTotal, string strReject)
    {
        Boolean boolSuccess = true;

        string strSubjectPrefix;
        string strSignature;
        string strSubject;
        string strBody;
        string strUserBody;
        string strSenderName;

        clsConfig config = new clsConfig();
        string strCurrency = config.currency;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYREJECTEMAILSUBJECT, clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT, 1);
        strSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYREJECTADMINEMAILCONTENT, clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT, 1);
        strBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEPAYREJECTUSEREMAILCONTENT, clsConfig.CONSTGROUPPAYREJECTEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;

        strBody = strBody.Replace("[NAME]", strName);
        strBody = strBody.Replace("[ORDNO]", strNo);
        strBody = strBody.Replace("[CUR]", strCurrency);
        strBody = strBody.Replace("[ORDTOTAL]", decTotal.ToString());
        strBody = strBody.Replace("[REJECTMSG]", strReject);
        strBody = strBody.Replace("[SIGNATURE]", strSignature);

        strUserBody = strUserBody.Replace("[NAME]", strName);
        strUserBody = strUserBody.Replace("[ORDNO]", strNo);
        strUserBody = strUserBody.Replace("[ORDNO]", strReject);
        strUserBody = strUserBody.Replace("[SIGNATURE]", strSignature);

        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strSubject = strSubjectPrefix + " " + strSubject; }

        if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        {
            if (clsEmail.send(strSenderName, "", strEmail, "", "", "", strSubject, strUserBody, 1))
            {
                boolSuccess = true;
            }
            else
            {
                boolSuccess = false;
            }
        }

        return boolSuccess;

        //int intPageIdProfilePage = clsPage.getPageIdByTemplate(clsAdmin.CONSTUSRPROFILEPAGE, "", 1);
        //string strOrderStatusUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + clsAdmin.CONSTUSRPROFILEPAGE + "?pgid=" + intPageIdProfilePage + "&frm=" + clsAdmin.CONSTPROFILEORDERSTATUS;

        //Boolean boolSuccess = true;

        //string strSubject;
        //string strBody;

        //string strUserSubject;
        //string strUserBody;
        //string strUserEmail;
        //string strUserName;
        //string strSenderName = GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString();

        //clsConfig config = new clsConfig();
        //string strCurrency = config.currency;
        //string strSubjectPrefix = "";// config.emailSubject;

        //config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        //string strSignature = config.longValue; //config.emailSignature;

        //strSubject = strSubjectPrefix + " Payment Reject";
        //strBody = "Dear Admin, <br /><br />Please note that there is an error in the transaction details for an order from " + GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString() + " website.<br /><br />The order details as below:<br />";
        //strBody += "OrderID: " + strNo + "<br />";
        //strBody += "Order Total: " + strCurrency + " " + decTotal + "<br />";
        //strBody += "Reject Remarks: " + strReject + "<br />";
        //strBody += "<br />Thank you.<br /><br />";
        //strBody += strSignature;
        //strBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strBody + "</font>";
        ////strBody = strBody.Replace(Environment.NewLine, "<br />");

        //strUserEmail = strEmail;
        //strUserName = strName;
        //strUserSubject = strSubjectPrefix + " Payment Reject";
        //strUserBody = "Dear " + strUserName + ",";
        //strUserBody += "<br /><br /><b>Your order " + strNo + " history</b>";
        //strUserBody += "<br /><br />Please note that there is an error in the transaction details.";
        //strUserBody += "<br /><br />" + strReject;
        //strUserBody += "<br /><br />Please kindly make the amendments and update the form under the order details with the correct information.";
        ////strUserBody += "<br /><br />Here are the order details:<br />";
        ////strUserBody += "OrderID: " + strNo + "<br />";
        ////strUserBody += "Order Item: " + intItem + "<br />";
        ////strUserBody += "Order Total: " + strCurrency + " " + decTotal + "<br />";
        ////strUserBody += "Courier: " + strCourier + "<br />";
        ////strUserBody += "Consignment No.: " + strConsignment + "<br />";
        ////strUserBody += "<br /><br />In order to know the status of your order, please login into <a href='" + ConfigurationManager.AppSettings["fullBase"] + "' title='" + GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString() + "'>" + GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString() + "</a> and check under Profile > Outstanding Order.";
        //strUserBody += "<br /><br />Thank you.<br /><br />";
        //strUserBody += strSignature;
        //strUserBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strUserBody + "</font>";
        ////Response.Write(strBody + "<br />" + strUserBody);
        //if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        //{
        //    if (clsEmail.send(strSenderName, "", strUserEmail, "", "", "", strUserSubject, strUserBody, 1))
        //    {
        //        boolSuccess = true;
        //    }
        //    else
        //    {
        //        boolSuccess = false;
        //    }
        //}

        //return boolSuccess;
    }

    //Order Processing Email
    protected Boolean sendDetail(string strEmail, string strName, string strNo, int intItem, decimal decTotal)
    {
        Boolean boolSuccess = true;

        string strSubjectPrefix;
        string strSignature;
        string strSubject;
        string strBody;
        string strUserBody;
        string strSenderName;

        clsConfig config = new clsConfig();
        string strCurrency = config.currency;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDPROEMAILSUBJECT, clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT, 1);
        strSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDPROADMINEMAILCONTENT, clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT, 1);
        strBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDPROUSEREMAILCONTENT, clsConfig.CONSTGROUPORDPROCESSINGEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;

        strBody = strBody.Replace("[NAME]", strName);
        strBody = strBody.Replace("[ORDNO]", strNo);
        strBody = strBody.Replace("[CUR]", strCurrency);
        strBody = strBody.Replace("[ORDTOTAL]", decTotal.ToString());
        strBody = strBody.Replace("[ORDITEM]", intItem.ToString());
        strBody = strBody.Replace("[SIGNATURE]", strSignature);

        strUserBody = strUserBody.Replace("[NAME]", strName);
        strUserBody = strUserBody.Replace("[ORDNO]", strNo);
        strUserBody = strUserBody.Replace("[SIGNATURE]", strSignature);

        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strSubject = strSubjectPrefix + " " + strSubject; }

        if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        {
            if (clsEmail.send(strSenderName, "", strEmail, "", "", "", strSubject, strUserBody, 1))
            {
                boolSuccess = true;
            }
            else
            {
                boolSuccess = false;
            }
        }

        return boolSuccess;

        //int intPageIdProfilePage = clsPage.getPageIdByTemplate(clsAdmin.CONSTUSRPROFILEPAGE, "", 1);
        //string strOrderStatusUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + clsAdmin.CONSTUSRPROFILEPAGE + "?pgid=" + intPageIdProfilePage + "&frm=" + clsAdmin.CONSTPROFILEORDERSTATUS;

        //Boolean boolSuccess = true;

        //string strSubject;
        //string strBody;

        //string strUserSubject;
        //string strUserBody;
        //string strUserEmail;
        //string strUserName;
        //string strSenderName = GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString();

        //clsConfig config = new clsConfig();
        //string strCurrency = config.currency;
        //string strSubjectPrefix = "";// config.emailSubject;
        
        //config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        //string strSignature = config.longValue; //config.emailSignature;

        //strSubject = strSubjectPrefix + " Processed";
        //strBody = "Dear Admin, <br /><br />An order from " + GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString() + " website is being processed.<br /><br />The order details as below:<br />";
        //strBody += "OrderID: " + strNo + "<br />";
        //strBody += "Order Item: " + intItem + "<br />";
        //strBody += "Order Total: " + strCurrency + " " + decTotal + "<br />";
        //strBody += "<br />Thank you.<br /><br />";
        //strBody += strSignature;
        //strBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strBody + "</font>";
        ////strBody = strBody.Replace(Environment.NewLine, "<br />");

        //strUserEmail = strEmail;
        //strUserName = strName;
        //strUserSubject = strSubjectPrefix + " Processed";
        //strUserBody = "Dear " + strUserName + ",";
        //strUserBody += "<br /><br /><b>Your order " + strNo + " history</b>";
        //strUserBody += "<br /><br />Your order is being processed.";
        //strUserBody += "<br /><br />Another email will be sent to you once your package has been delivered.";
        //strUserBody += "<br /><br />Thank you.<br /><br />";
        //strUserBody += strSignature;
        //strUserBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strUserBody + "</font>";
        ////Response.Write(strBody + "<br />" + strUserBody);
        //if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        //{
        //    if (clsEmail.send(strSenderName, "", strUserEmail, "", "", "", strUserSubject, strUserBody, 1))
        //    {
        //        boolSuccess = true;
        //    }
        //    else
        //    {
        //        boolSuccess = false;
        //    }
        //}

        //return boolSuccess;
    }

    //Order Delivered Email
    protected Boolean sendDetail(string strEmail, string strName, string strNo, int intItem, decimal decTotal, DateTime dtDate, string strCourier, string strConsignment)
    {
        Boolean boolSuccess = true;

        string strSubjectPrefix;
        string strSignature;
        string strSubject;
        string strBody;
        string strUserBody;
        string strSenderName;

        clsConfig config = new clsConfig();
        string strCurrency = config.currency;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDDELEMAILSUBJECT, clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT, 1);
        strSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDDELADMINEMAILCONTENT, clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT, 1);
        strBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDDELUSEREMAILCONTENT, clsConfig.CONSTGROUPORDDELIVERYEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;

        strBody = strBody.Replace("[NAME]", strName);
        strBody = strBody.Replace("[ORDNO]", strNo);
        strBody = strBody.Replace("[ORDITEM]", intItem.ToString());
        strBody = strBody.Replace("[CUR]", strCurrency);
        strBody = strBody.Replace("[ORDTOTAL]", decTotal.ToString());
        strBody = strBody.Replace("[SHIPPINGDATE]", dtDate.ToString());
        strBody = strBody.Replace("[COURIER]", strCourier);
        strBody = strBody.Replace("[CONSIGNMENTNO]", strConsignment);
        strBody = strBody.Replace("[SIGNATURE]", strSignature);

        strUserBody = strUserBody.Replace("[NAME]", strName);
        strUserBody = strUserBody.Replace("[ORDNO]", strNo);
        strUserBody = strUserBody.Replace("[COURIER]", strCourier);
        strUserBody = strUserBody.Replace("[CONSIGNMENTNO]", strConsignment);
        strUserBody = strUserBody.Replace("[SIGNATURE]", strSignature);

        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strSubject = strSubjectPrefix + " " + strSubject; }

        if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        {
            if (clsEmail.send(strSenderName, "", strEmail, "", "", "", strSubject, strUserBody, 1))
            {
                boolSuccess = true;
            }
            else
            {
                boolSuccess = false;
            }
        }

        return boolSuccess;

        //int intPageIdProfilePage = clsPage.getPageIdByTemplate(clsAdmin.CONSTUSRPROFILEPAGE, "", 1);
        //string strOrderStatusUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + clsAdmin.CONSTUSRPROFILEPAGE + "?pgid=" + intPageIdProfilePage + "&frm=" + clsAdmin.CONSTPROFILEORDERSTATUS;

        //Boolean boolSuccess = true;

        //string strSubject;
        //string strBody;

        //string strUserSubject;
        //string strUserBody;
        //string strUserEmail;
        //string strUserName;
        //string strSenderName = GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString();

        //clsConfig config = new clsConfig();
        //string strCurrency = config.currency;
        //string strSubjectPrefix = "";// config.emailSubject;

        //config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        //string strSignature = config.longValue; //config.emailSignature;

        //strSubject = strSubjectPrefix + " Delivered";
        //strBody = "Dear Admin, <br /><br />An order from " + GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString() + " website has been delivered.<br /><br />The order details as below:<br />";
        //strBody += "OrderID: " + strNo + "<br />";
        //strBody += "Order Item: " + intItem + "<br />";
        //strBody += "Order Total: " + strCurrency + " " + decTotal + "<br />";
        //strBody += "Shipping Date: " + dtDate + "<br/>";
        //strBody += "Shipping Company: " + strCourier + "<br />";
        //strBody += "Consignment No.: " + strConsignment + "<br />";
        //strBody += "<br />Thank you.<br /><br />";
        //strBody += strSignature;
        //strBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strBody + "</font>";
        ////strBody = strBody.Replace(Environment.NewLine, "<br />");

        //strUserEmail = strEmail;
        //strUserName = strName;
        //strUserSubject = strSubjectPrefix + " Delivered";
        //strUserBody = "Dear " + strUserName + ",";
        //strUserBody += "<br /><br /><b>Your order " + strNo + " history</b>";
        //strUserBody += "<br /><br />Your order has been processed for delivery.";
        //strUserBody += "<br /><br />Please allow <b>3 - 4 working days</b> for your package to reach you.";
        ////strUserBody += "<br /><br />Here are the order details:<br />";
        ////strUserBody += "OrderID: " + strNo + "<br />";
        ////strUserBody += "Order Item: " + intItem + "<br />";
        ////strUserBody += "Order Total: " + strCurrency + " " + decTotal + "<br />";
        ////strUserBody += "Courier: " + strCourier + "<br />";
        ////strUserBody += "Consignment No.: " + strConsignment + "<br />";
        ////strUserBody += "<br /><br />In order to know the status of your order, please login into <a href='" + ConfigurationManager.AppSettings["fullBase"] + "' title='" + GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString() + "'>" + GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString() + "</a> and check under Profile > Outstanding Order.";
        //strUserBody += "<br /><br />Thank you.<br /><br />";
        //strUserBody += strSignature;
        //strUserBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strUserBody + "</font>";
        ////Response.Write(strBody + "<br /><br />" + strUserBody);
        //if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        //{
        //    if (clsEmail.send(strSenderName, "", strUserEmail, "", "", "", strUserSubject, strUserBody, 1))
        //    {
        //        boolSuccess = true;
        //    }
        //    else
        //    {
        //        boolSuccess = false;
        //    }
        //}

        //return boolSuccess;
    }

    //Order Done Email
    protected Boolean sendDetail(string strEmail, string strName, string strNo)
    {
        Boolean boolSuccess = true;

        string strSubjectPrefix;
        string strSignature;
        string strSubject;
        string strBody;
        string strUserBody;
        string strSenderName;

        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDDONEEMAILSUBJECT, clsConfig.CONSTGROUPORDDONEEMAILCONTENT, 1);
        strSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDDONEADMINEMAILCONTENT, clsConfig.CONSTGROUPORDDONEEMAILCONTENT, 1);
        strBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDDONEUSEREMAILCONTENT, clsConfig.CONSTGROUPORDDONEEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;

        strBody = strBody.Replace("[NAME]", strName);
        strBody = strBody.Replace("[ORDNO]", strNo);
        strBody = strBody.Replace("[SIGNATURE]", strSignature);

        strUserBody = strUserBody.Replace("[NAME]", strName);
        strUserBody = strUserBody.Replace("[ORDNO]", strNo);
        strUserBody = strUserBody.Replace("[SIGNATURE]", strSignature);

        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strSubject = strSubjectPrefix + " " + strSubject; }

        if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        {
            if (clsEmail.send(strSenderName, "", strEmail, "", "", "", strSubject, strUserBody, 1))
            {
                boolSuccess = true;
            }
            else
            {
                boolSuccess = false;
            }
        }

        return boolSuccess;
    }
    /*End of Send Email*/
    #endregion
}
