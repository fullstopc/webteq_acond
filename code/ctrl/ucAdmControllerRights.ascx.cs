﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucAdmControllerRights : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _ctrlId = 0;
    protected int _mode = 1;
    protected string _pageListingURL = "";
    protected int _type = 0;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int ctrlId
    {
        get
        {
            if (ViewState["CTRLID"] == null)
            {
                return _ctrlId;
            }
            else
            {
                return int.Parse(ViewState["CTRLID"].ToString());
            }
        }
        set { ViewState["CTRLID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }

    protected int type
    {
        get
        {
            if (ViewState["TYPE"] == null)
            {
                return _type;
            }
            else
            {
                return int.Parse(ViewState["TYPE"].ToString());
            }
        }
        set { ViewState["TYPE"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lnkbtnNext_Click(object sender, EventArgs e)
    {
        for (int i = lstboxAllProjects.Items.Count - 1; i >= 0; i--)
        {
            if (lstboxAllProjects.Items[i].Selected == true)
            {
                lstboxSelectedProjects.Items.Add(lstboxAllProjects.Items[i]);
                ListItem li = lstboxAllProjects.Items[i];
                lstboxAllProjects.Items.Remove(li);
            }
        }
    }

    protected void lnkbtnPrev_Click(object sender, EventArgs e)
    {
        for (int i = lstboxSelectedProjects.Items.Count - 1; i >= 0; i--)
        {
            if (lstboxSelectedProjects.Items[i].Selected == true)
            {
                lstboxAllProjects.Items.Add(lstboxSelectedProjects.Items[i]);
                ListItem li = lstboxSelectedProjects.Items[i];
                lstboxSelectedProjects.Items.Remove(li);
            }
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            clsController ctrl = new clsController();
            int intRecordAffected = 0;
            Boolean boolSuccess = true;
            Boolean boolEdited = false;

            for (int i = lstboxAllProjects.Items.Count - 1; i >= 0; i--)
            {
                int intProjId = Convert.ToInt16(lstboxAllProjects.Items[i].Value);

                if (ctrl.isItemExist(ctrlId, intProjId))
                {
                    ctrl.deleteControllerProjects(ctrlId, intProjId);

                    boolEdited = true;
                }
            }

            for (int i = lstboxSelectedProjects.Items.Count - 1; i >= 0; i--)
            {
                int intProjId = Convert.ToInt16(lstboxSelectedProjects.Items[i].Value);

                if (!ctrl.isItemExist(ctrlId, intProjId)) 
                { 
                    ctrl.addControllerProjects(ctrlId, intProjId);

                    boolEdited = true;  
                }
            }
           
            if (boolEdited)
            {
                Session["EDITEDCONTROLLERID"] = ctrlId;

                Response.Redirect(Request.Url.ToString());
            }
            else
            {
                if (boolSuccess)
                {
                    Session["EDITEDCONTROLLERID"] = ctrlId;
                    Session["NOCHANGE"] = 1;

                    Response.Redirect(Request.Url.ToString());
                }
                else
                {
                    ctrl.extractControllerById(ctrlId, 0);

                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit Controller (" + ctrl.ctrlName + "). Please try again.</div>";

                    Response.Redirect(currentPageName);
                }
            }
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
            fillForm();
        }
    }

    protected void setPageProperties()
    {
        string strIds = "";

        clsController ctrl = new clsController();
        DataSet ds = new DataSet();
        ds = ctrl.getControllerProjectList(ctrlId);

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            strIds += row["PROJ_ID"].ToString() + clsSetting.CONSTCOMMASEPARATOR;
        }

        DataSet dsProj = new DataSet();
        dsProj = ctrl.getProjectList(0);
        DataView dv = new DataView(dsProj.Tables[0]);
        dv.RowFilter = (!string.IsNullOrEmpty(strIds) ? " PROJ_ID NOT IN (" + strIds.Substring(0, strIds.Length - clsSetting.CONSTCOMMASEPARATOR.ToString().Length) + ")" : "");
        dv.Sort = "PROJ_NAME ASC";

        lstboxAllProjects.DataSource = dv;
        lstboxAllProjects.DataTextField = "PROJ_NAME";
        lstboxAllProjects.DataValueField = "PROJ_ID";
        lstboxAllProjects.DataBind();

        lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
    }

    protected void fillForm()
    {
        clsController ctrl = new clsController();
        DataSet ds = new DataSet();
        ds = ctrl.getControllerProjectList(ctrlId);
        DataView dv = new DataView(ds.Tables[0]);
        dv.Sort = "V_PROJNAME ASC";

        lstboxSelectedProjects.DataSource = dv;
        lstboxSelectedProjects.DataTextField = "V_PROJNAME";
        lstboxSelectedProjects.DataValueField = "PROJ_ID";
        lstboxSelectedProjects.DataBind();
       
    }
    #endregion
}
