﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmRelatedDeal.ascx.cs" Inherits="ctrl_ucAdmRelatedDeal" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>

<asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
    <asp:Literal ID="litAck" runat="server"></asp:Literal>
</asp:Panel>
<asp:Panel ID="pnlForm4" runat="server">
    <div class="divFilterContainer">
        <div class="divFilter">
            <table class="tblSearch">
                <tr>
                    <td class="tdKeyword">
                        <div class="divKeyword">Keyword:</div>
                    </td>
                    <td>
                        <asp:TextBox ID="txtKeyword" runat="server" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
                    </td>
                    <td>
                        <%--<asp:LinkButton ID="lnkbtnSearch" runat="server" Text="Search" ToolTip="Search" CssClass="btn btnSave" OnClick="lnkbtnSearch_Click"></asp:LinkButton>--%>
                        <asp:Button ID="lnkbtnSearch" runat="server" Text="Search" ToolTip="Search" CssClass="btn btnSearch" OnClick="lnkbtnSearch_Click"/>
                    </td>
                </tr>
                <tr><td></td><td><span class="tdLabelBigGrey">(Code, Name)</span></td></tr>
            </table>            
        </div>
    </div>
    <div class="divSearch">
        <asp:Panel ID="pnlSeachHeader" runat="server" CssClass="divSearchHeader">
            <div class="divSectionTitleLeft">
                <asp:HyperLink ID="hypHideShow" runat="server" NavigateUrl="javascript:void(0);" CssClass="linkShow">search / filter</asp:HyperLink>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlSeachForm" runat="server" CssClass="divSearchForm" DefaultButton="lnkbtnSearch">
            <table class="tblSearch">
                <tr>
                    <td class="tdLabelItalic">Business Type:</td>
                    <td><asp:DropDownList ID="ddlBizType" runat="server" CssClass="ddl_medium"></asp:DropDownList></td>
                    <td class="tdLabelRightItalic">Merchant:</td>
                    <td><asp:DropDownList ID="ddlMerchant" runat="server" CssClass="ddl_medium"></asp:DropDownList></td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <asp:Panel ID="pnlDealListing" runat="server" CssClass="divListing">
        <asp:Panel ID="pnlDeal" runat="server">
            <asp:Panel ID="pnlDealRelNoFound" runat="server" CssClass="divDealRelNoFound">
                <asp:Literal ID="litNoFound" runat="server"></asp:Literal>
            </asp:Panel>
            <asp:Panel ID="pnlRelDeal" runat="server" CssClass="divRelDeal">
                <asp:Panel ID="pnlRelDealTotal" runat="server" CssClass="divRelDealTotal">
                    <asp:Literal ID="litRelDealTotal" runat="server"></asp:Literal>
                </asp:Panel>
                <asp:Panel ID="pnlRelDealPaginationTopContainer" runat="server" CssClass="divRelDealPaginationTopContainer">
                    <asp:Panel ID="pnlRelDealPaginationTopContainerInner" runat="server" CssClass="divRelDealPaginationTopContainerInner">
                        <div class="divPagingInner">
                            <span class="spanPagination">
                                <asp:Repeater ID="rptPaginationTop" runat="server" OnItemDataBound="rptPagination_ItemDataBound" OnItemCommand="rptPagination_ItemCommand">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkbtnPage" runat="server" CommandName="cmdPage" CommandArgument='<%# Container.DataItem %>' Text='<%# Container.DataItem %>' ToolTip='<%# Container.DataItem %>' CssClass="linkPage"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </span>
                        </div>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="pnlRelDealItemsContainer" runat="server" CssClass="divRelDealItemsContainer">
                    <asp:Repeater ID="rptRelDeal" runat="server" OnItemDataBound="rptRelDeal_ItemDataBound" OnItemCommand="rptRelDeal_ItemCommand">
                        <HeaderTemplate><table cellpadding="0" cellspacing="0" class="relDealTbl"></HeaderTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litRowOpen" runat="server" Visible="false"></asp:Literal>
                            <td id="tdRelDealItem" runat="server" class="tdRelDealItem">
                                <asp:Panel ID="pnlRelDealItem" runat="server" CssClass="divRelDealItem">
                                    <div class="divRelDealImg">
                                        <div class="divRelDealImgInner">
                                            <asp:LinkButton ID="lnkbtnRelDealImg" runat="server" CommandName="cmdSelect" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "DEAL_ID") + "|" + DataBinder.Eval(Container.DataItem, "DEAL_NAME") + "|" + DataBinder.Eval(Container.DataItem, "DEAL_IMAGE") %>'>
                                                <asp:Image ID="imgRelDeal" runat="server" />
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="divRelDealName"><asp:Literal ID="litRelDealName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DEAL_NAME") %>'></asp:Literal></div>
                                    <asp:LinkButton ID="lnkbtnSelect" runat="server" Text="select" ToolTip="select" CommandName="cmdSelect" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "DEAL_ID") + "|" + DataBinder.Eval(Container.DataItem, "DEAL_NAME") + "|" + DataBinder.Eval(Container.DataItem, "DEAL_IMAGE") %>'></asp:LinkButton>
                                </asp:Panel>
                            </td>
                            <asp:Literal ID="litRowClose" runat="server" Visible="false"></asp:Literal>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Literal ID="litRowClose2" runat="server" Visible="false"></asp:Literal>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </asp:Panel>
                <asp:Panel ID="pnlRelDealPaginationBtmContainer" runat="server" CssClass="divRelDealPaginationBtmContainer">
                    <asp:Panel ID="pnlRelDealPaginationBtmContainerInner" runat="server" CssClass="divRelDealPaginationBtmContainerInner">
                        <div class="divPagingInner">
                            <span class="spanPagination">
                                <asp:Repeater ID="rptPaginationBtm" runat="server" OnItemDataBound="rptPagination_ItemDataBound" OnItemCommand="rptPagination_ItemCommand">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkbtnPage" runat="server" CommandName="cmdPage" CommandArgument='<%# Container.DataItem %>' Text='<%# Container.DataItem %>' ToolTip='<%# Container.DataItem %>' CssClass="linkPage"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </span>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlSelectedRelDeal" runat="server" CssClass="divListing3">
        <div class="divSectionTitle">Selected Deal(s):</div>
        <asp:Panel ID="pnlDealSelContainer" runat="server" CssClass="divDealSelContainer">
            <asp:Panel ID="pnlDealSelNoFound" runat="server" CssClass="divDealSelNoFound" Visible="false">
                <asp:Literal ID="litSelNoFound" runat="server"></asp:Literal>
            </asp:Panel>
            <asp:Panel ID="pnlDealSelListing" runat="server" CssClass="divDealSelListing">
                <asp:Repeater ID="rptRelDealSel" runat="server" OnItemDataBound="rptRelDealSel_ItemDataBound" OnItemCommand="rptRelDealSel_ItemCommand">
                    <HeaderTemplate><table cellpadding="0" cellspacing="0" class="relDealTbl"></HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="litRowOpen" runat="server" Visible="false"></asp:Literal>
                        <td id="tdRelDealItem" runat="server" class="tdRelDealItem">
                            <asp:Panel ID="pnlRelDealSelItem" runat="server" CssClass="divRelDealItem">
                                <div class="divRelDealImg">
                                    <div class="divRelDealImgInner">
                                        <asp:LinkButton ID="lnkbtnRelDealSelImg" runat="server" CommandName="cmdRemove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "RELDEAL_ID") %>'>
                                            <asp:Image ID="imgRelImageSel" runat="server" />
                                        </asp:LinkButton>
                                    </div>
                                </div>
                                <div class="divRelDealName"><asp:Literal ID="litRelDealSelName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RELDEAL_NAME") %>'></asp:Literal></div>
                                <asp:LinkButton ID="lnkbtnSelect" runat="server" Text="remove" ToolTip="remove" CommandName="cmdRemove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "RELDEAL_ID") %>'></asp:LinkButton>
                            </asp:Panel>
                        </td>
                        <asp:Literal ID="litRowClose" runat="server" Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Literal ID="litRowClose2" runat="server" Visible="false"></asp:Literal>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated" style="padding-left:20px;">
        <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn" OnClick="lnkbtnSave_Click"></asp:LinkButton>
        <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Back" ToolTip="Back" class="btn" CausesValidation="false"></asp:LinkButton>
    </asp:Panel>
</asp:Panel>
