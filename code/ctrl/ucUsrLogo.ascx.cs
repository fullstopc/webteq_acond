﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucUsrLogo : System.Web.UI.UserControl
{
    #region "Enum"
    public enum ViewType
    {
        FullView = 1,
        MobileView = 2
    }
    #endregion

    #region "Properties"
    protected string _imageFilename;
    protected string _toolTip;
    protected string _alternateText;
    protected string _language;
    protected ViewType _viewType = ViewType.FullView;
    protected int _imageWidth = 0;
    protected int _imageHeight = 0;
    #endregion


    #region "Property Methods"
    public string imageFilename
    {
        get { return _imageFilename; }
        set { _imageFilename = value; }
    }

    public string toolTip
    {
        get { return _toolTip; }
        set { _toolTip = value; }
    }

    public string alternateText
    {
        get { return _alternateText; }
        set { _alternateText = value; }
    }

    public string language
    {
        get { return _language; }
        set { _language = value; }
    }

    public ViewType viewType
    {
        get { return _viewType; }
        set { _viewType = value; }
    }

    public int imageWidth
    {
        get { return _imageWidth; }
        set { _imageWidth = value; }
    }

    public int imageHeight
    {
        get { return _imageHeight; }
        set { _imageHeight = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        {
            clsConfig config = new clsConfig();
            string strImageLogo = config.extractItemByNameGroup(clsConfig.CONSTNAMELOGOCOMPANYLOGO, clsConfig.CONSTGROUPLOGOANDICONSETTINGS, 1) ? config.value : "";
            string strCompanyName = GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString();

            if (!string.IsNullOrEmpty(strImageLogo))
            {
                if (imageHeight > 0 && imageWidth > 0)
                {
                    imgLogo.Width = imageWidth;
                    imgLogo.Height = imageHeight;
                }

                imgLogo.ImageUrl = ConfigurationManager.AppSettings["uplBase"] + clsAdmin.CONSTCTRLPNLADMFOLDER + "/" + strImageLogo;
                imgLogo.ToolTip = strCompanyName;
                imgLogo.AlternateText = strCompanyName;
            }

            int intPageId = clsPage.getPageIdByTemplate("page.aspx", language, 1);
            string strUrl = "usr/page.aspx?pgid=" + intPageId;
            if (viewType == ViewType.MobileView)
            {
                //imgLogo.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + imageFilename;
                strUrl = clsSetting.CONSTUSRMOBILEDIRECTARY + clsSetting.CONSTUSRMOBILEPAGEPAGE;
            }

            hypLogo.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + strUrl;
            hypLogo.NavigateUrl = ""; // hardcode the logo url to none
        }
    }
    #endregion
}
