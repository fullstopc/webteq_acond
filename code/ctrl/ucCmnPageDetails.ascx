﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCmnPageDetails.ascx.cs" Inherits="ctrl_ucCmnPageDetails" %>
<%@ Register Src="~/ctrl/ucUsrLoading.ascx" TagName="UsrLoading" TagPrefix="uc" %>

<script type="text/javascript">
    function validatePageTitleFriendlyUrl_client(source, args) {
        var txtPageTitleFriendlyURL = document.getElementById("<%= txtPageTitleFriendlyURL.ClientID %>");
        var pageTitleFriendlyUrlRE = /<%= clsAdmin.CONSTADMPAGETITLEFRIENDLYURL %>/;        
        if (txtPageTitleFriendlyURL.value.match(pageTitleFriendlyUrlRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "Please enter only valid format.";
            source.innerHTML = "Please enter only valid format.";
        }
    }
</script>

<asp:Panel ID="pnlCmnPageDetailsEdit" runat="server" CssClass="divCmnPageDetailsEdit" Visible="false">
    <asp:UpdatePanel runat="server" ID="upnlPageDetail">
        <ContentTemplate>
        <table cellpadding="0" cellspacing="0" border="0" class="formTblNested">
        <tr>
            <td class="tdLabel"><asp:Label ID="lblPageNameEdit" runat="server" meta:resourcekey="lblPageName"></asp:Label>:<span class="attention_compulsory">*</span></td>
            <td>
                <asp:TextBox ID="txtPageName" runat="server" CssClass="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                <span class="fieldDesc"><asp:RequiredFieldValidator ID="rfvPageName" runat="server" meta:resourcekey="rfvPageName" SetFocusOnError="true" ControlToValidate="txtPageName" Display="Dynamic" CssClass="errmsg" ValidationGroup="grpPageDetail"></asp:RequiredFieldValidator></span>
            </td>
        </tr>
        <tr>
            <td class="tdLabel nobr"><asp:Label ID="lblPageDisplayNameEdit" runat="server" meta:resourcekey="lblPageDisplayName"></asp:Label>:<span class="attention_compulsory">*</span></td>
            <td>
                <asp:TextBox ID="txtPageDisplayName" runat="server" CssClass="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                <span class="spanFieldDesc"><a href="javascript:void(0);" onclick="copyValue('<% =txtPageName.ClientID %>','<% =txtPageDisplayName.ClientID %>');" title="same as Name">same as Name</a></span>
                <span class="fieldDesc"><asp:RequiredFieldValidator ID="rfvPageDisplayName" runat="server" meta:resourcekey="rfvPageDisplayName" SetFocusOnError="true" ControlToValidate="txtPageDisplayName" Display="Dynamic" CssClass="errmsg" ValidationGroup="grpPageDetail"></asp:RequiredFieldValidator></span>
            </td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblPageTitleEdit" runat="server" meta:resourcekey="lblPageTitle"></asp:Label>:<span class="attention_compulsory">*</span></td>
            <td>
                <asp:TextBox ID="txtPageTitle" runat="server" CssClass="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                <span class="spanFieldDesc"><a href="javascript:void(0);" onclick="copyValue('<% =txtPageName.ClientID %>','<% =txtPageTitle.ClientID %>');" title="same as Name">same as Name</a></span>
                <span class="fieldDesc"><asp:RequiredFieldValidator ID="rfvPageTitle" runat="server" meta:resourcekey="rfvPageTitle" SetFocusOnError="true" ControlToValidate="txtPageTitle" Display="Dynamic" CssClass="errmsg" ValidationGroup="grpPageDetail"></asp:RequiredFieldValidator></span>
            </td>
        </tr>
        <tr id="trFriendlyURL" runat="server">
            <td class="tdLabel nobr"><asp:Label ID="lblPageTitleFriendlyUrlEdit" runat="server" meta:resourcekey="lblPageTitleFriendlyUrl"></asp:Label>:<span class="attention_unique">*</span></td>
            <td>
                <asp:TextBox ID="txtPageTitleFriendlyURL" runat="server" CssClass="text_fullwidth uniq" MaxLength="250"></asp:TextBox>
                <span class="spanFieldDesc"><asp:CustomValidator ID="cvPageTitleFriendlyUrl" runat="server" ControlToValidate="txtPageTitleFriendlyURL" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validatePageTitleFriendlyUrl_client" OnServerValidate="validatePageTitleFriendlyUrl_server" OnPreRender="cvPageTitleFriendlyUrl_PreRender"></asp:CustomValidator></span>
            </td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblParentEdit" runat="server" meta:resourcekey="lblParent"></asp:Label>:</td>
            <td>
                <asp:Panel ID="pnlLoadingParent" runat="server" CssClass="divCmnPageDetailsLoadingContainer">
                    <asp:UpdatePanel ID="upnlParent" runat="server">
                        <ContentTemplate>
                            <asp:UpdateProgress ID="uprgParent" runat="server" AssociatedUpdatePanelID="upnlParent" DynamicLayout="true">
                                <ProgressTemplate>
                                    <uc:UsrLoading ID="ucUsrLoadingParent" runat="server" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:DropDownList ID="ddlParent" runat="server" CssClass="ddl_fullwidth" OnSelectedIndexChanged="ddlParent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            <asp:Literal ID="litParentRoot" runat="server" Visible="false"></asp:Literal>
                            </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblTemplateEdit" runat="server" meta:resourcekey="lblTemplate"></asp:Label>:<span class="attention_compulsory">*</span></td>
            <td>
                <asp:Panel ID="pnlLoadingTemplate" runat="server" CssClass="divCmnPageDetailsLoadingContainer">
                    <asp:UpdatePanel ID="upnlTemplate" runat="server">
                        <ContentTemplate>
                            <asp:UpdateProgress ID="uprgTemplate" runat="server" AssociatedUpdatePanelID="upnlTemplate" DynamicLayout="true">
                                <ProgressTemplate>
                                    <uc:UsrLoading ID="ucUsrLoadingTemplate" runat="server" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:DropDownList ID="ddlTemplate" runat="server" CssClass="ddl_fullwidth compulsory"></asp:DropDownList>  <asp:CheckBox ID="chkboxTemplate" runat="server" meta:resourcekey="chkboxTemplate" Checked="true" Visible="false" />
                            <span class="fieldDesc"><asp:RequiredFieldValidator ID="rfvTemplate" runat="server" meta:resourcekey="rfvTemplate" SetFocusOnError="true" ControlToValidate="ddlTemplate" Display="Dynamic" CssClass="errmsg" ValidationGroup="grpPageDetail"></asp:RequiredFieldValidator></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="tdSpacerPageDetails"></td>
            <td class="tdSpacerPageDetails"></td>
        </tr>
        <tr id="trLanguageEdit" runat="server" visible="false">
            <td class="tdLabel"><asp:Label ID="lblLanguageEdit" runat="server" meta:resourcekey="lblLanguage"></asp:Label>:</td>
            <td><asp:DropDownList ID="ddlLanguage" runat="server" CssClass="ddl_fullwidth"></asp:DropDownList></td>
        </tr>
        <tr id="trCorrespondEdit" runat="server" visible="false">
            <td class="tdLabel"><asp:Label ID="lblCorrespondEdit" runat="server" meta:resourcekey="lblCorrespond"></asp:Label>:</td>
            <td class="tdPageUpdatePanel">
                <asp:UpdatePanel ID="upnlCorrespondEdit" runat="server">
                    <ContentTemplate>
                        <asp:UpdateProgress ID="uprgCorrespondEdit" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="upnlCorrespondEdit">
                            <ProgressTemplate>
                                <uc:UsrLoading ID="ucUsrLoadingCorrespondEdit" runat="server" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:ListBox ID="lstboxPages" runat="server" OnSelectedIndexChanged="lstboxPages_SelectedIndexChanged" SelectionMode="Single" AutoPostBack="true" CssClass="ddl_page"></asp:ListBox> 
                        <asp:ListBox ID="lstboxPageCorrespond" runat="server" OnSelectedIndexChanged="lstboxPageCorrespond_SelectedIndexChanged" SelectionMode="Single" AutoPostBack="true" CssClass="ddl_page"></asp:ListBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tdSpacer"></td>
            <td class="tdSpacer"></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblCSSClassEdit" runat="server" meta:resourcekey="lblCSSClass"></asp:Label>:</td>
            <td>
                <asp:Panel ID="pnlLoadingCssClass" runat="server" CssClass="divCmnPageDetailsLoadingContainer">
                    <asp:UpdatePanel ID="upnlCssClass" runat="server">
                        <ContentTemplate>
                            <asp:UpdateProgress ID="uprgCssClass" runat="server" AssociatedUpdatePanelID="upnlCssClass" DynamicLayout="true">
                                <ProgressTemplate>
                                    <uc:UsrLoading ID="ucUsrLoadingCssClass" runat="server" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:DropDownList ID="ddlCssClass" runat="server" CssClass="ddl_fullwidth"></asp:DropDownList>
                            <asp:TextBox ID="txtCSSClass" runat="server" CssClass="text_fullwidth" MaxLength="250" TextMode="MultiLine" Rows="2" Visible="false"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </td>
        </tr>
        <tr>
             <td class="tdLabel" style="width:50px;"><asp:Label ID="lblSlideShowGroup" runat="server" text="Slide Show Group:"></asp:Label></td>
             <td>
                <asp:Panel ID="pnlLoadingSlideShowGroup" runat="server" CssClass="divCmnPageDetailsLoadingContainer">
                    <asp:DropDownList ID="ddlSlideShowGroup" runat="server" CssClass="ddl_fullwidth"></asp:DropDownList>
                    <asp:TextBox ID="txtSlideShowGroup" runat="server" CssClass="text_fullwidth" MaxLength="250" TextMode="MultiLine" Rows="2" Visible="false"></asp:TextBox>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblRedirectEdit" runat="server" meta:resourcekey="lblRedirect"></asp:Label>:</td>
            <td><asp:DropDownList ID="ddlRedirect" runat="server" CssClass="ddl_fullwidth" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlRedirect_SelectedIndexChanged"></asp:DropDownList></td>
        </tr>        
        <tr>
            <td></td>
            <td>
                <asp:Panel ID="pnlExtLink" runat="server" Visible="false">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td class="tdLabelExLink"><asp:Label ID="lblLinkEdit" runat="server" meta:resourcekey="lblLinkEdit"></asp:Label></td>
                            <td><asp:TextBox ID="txtLink" runat="server" CssClass="text_fullwidth" MaxLength="1000"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="tdLabelExLink"><asp:Label ID="lblTargetEdit" runat="server" meta:resourcekey="lblTargetEdit" ></asp:Label></td>
                            <td><asp:DropDownList ID="ddlTarget" runat="server" CssClass="ddl_fullwidth" AutoPostBack="false" CausesValidation="false" OnSelectedIndexChanged="ddlTarget_SelectedIndexChanged"></asp:DropDownList></td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="tdSpacerPageDetails"></td>
            <td class="tdSpacerPageDetails"></td>
        </tr>
        <tr>
            <td class="tdSpacer"></td>
            <td class="tdSpacer"></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblKeywordEdit" runat="server" meta:resourcekey="lblKeyword"></asp:Label>:</td>
            <td class="tdWithCheckbox"><asp:TextBox ID="txtKeyword" runat="server" CssClass="text_fullwidth" MaxLength="1000"></asp:TextBox> <asp:CheckBox ID="chkboxKeyword" runat="server" meta:resourcekey="chkboxKeyword" Checked="true" /></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblDescEdit" runat="server" meta:resourcekey="lblDesc"></asp:Label>:</td>
            <td><asp:TextBox ID="txtDesc" runat="server" CssClass="text_fullwidth" MaxLength="1000"></asp:TextBox> <asp:CheckBox ID="chkboxDesc" runat="server" meta:resourcekey="chkboxDesc" Checked="true" /></td>
        </tr>
        <tr>
            <td class="tdSpacerPageDetails"></td>
            <td class="tdSpacerPageDetails"></td>
        </tr>
        <tr>
            <td class="tdSpacer"></td>
            <td class="tdSpacer"></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblShowPageTitleEdit" runat="server" meta:resourcekey="lblShowPageTitle"></asp:Label>:</td>
            <td class="tdLabel"><asp:CheckBox ID="chkboxShowPageTitle" runat="server" /></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblShowInMenuEdit" runat="server" meta:resourcekey="lblShowInMenu"></asp:Label>:</td>
            <td class="tdLabel">
                <asp:Panel ID="pnlLoadingShowInMenu" runat="server" CssClass="divCmnPageDetailsLoadingContainer">
                    <asp:UpdatePanel ID="upnlShowInMenu" runat="server">
                        <ContentTemplate>
                            <asp:UpdateProgress ID="uprgShowInMenu" runat="server" AssociatedUpdatePanelID="upnlShowInMenu" DynamicLayout="true">
                                <ProgressTemplate>
                                    <uc:UsrLoading ID="ucUsrLoadingShowInMenu" runat="server" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:CheckBox ID="chkboxShowInMenu" runat="server" Checked="true" OnCheckedChanged="chkboxShowInMenu_CheckedChanged" AutoPostBack="true" />
                            <asp:Panel ID="pnlShowInMenu" runat="server" CssClass="divShowInMenu" Visible="false">
                                <asp:CheckBox ID="chkboxTopMenu" runat="server" Text="Top Menu" Checked="true"/>
                                <asp:CheckBox ID="chkboxBottomMenu" runat="server" Text="Bottom Menu" Checked="true" />
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblShowInBreadCrumbEdit" runat="server" meta:resourcekey="lblShowInBreadCrumb"></asp:Label>:</td>
            <td class="tdLabel"><asp:CheckBox ID="chkboxShowInBreadcrumb" runat="server" Checked="true" /></td>
        </tr>
        <tr id="trUserEditable" runat="server" visible="false">
            <td class="tdLabel"><asp:Label ID="lblUserEditableEdit" runat="server" meta:resourcekey="lblUserEditable"></asp:Label>:</td>
            <td class="tdLabel"><asp:CheckBox ID="chkboxUserEditable" runat="server" Checked="true" /></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblShowInSitemap" runat="server" meta:resourcekey="lblShowInSitemap"></asp:Label>:</td>
            <td class="tdLabel"><asp:CheckBox ID="chkboxShowInSitemap" runat="server" Checked="true" /></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblEnableLink" runat="server" meta:resourcekey="lblEnableLink"></asp:Label>:</td>
            <td class="tdLabel"><asp:CheckBox ID="chkboxEnableLink" runat="server" Checked="true" /></td>
        </tr>
        <tr id="trAuthorize" runat="server" visible="false">
            <td class="tdLabel"><asp:Label ID="lblAuthorize" runat="server" meta:resourcekey="lblAuthorize"></asp:Label>:</td>
            <td class="tdLabel"><asp:CheckBox ID="chkboxAuthorize" runat="server" /></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblActiveEdit" runat="server" meta:resourcekey="lblActive"></asp:Label></td>
            <td class="tdLabel"><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
        </tr>  
    </table>
        <</ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Panel ID="pnlCmnPageDetailsView" runat="server" CssClass="divCmnPageDetailsView" Visible="false">
    <table cellpadding="0" cellspacing="0" class="formTblNested">
        <tr>
            <td class="tdLabel"><asp:Label ID="lblPageNameView" runat="server" meta:resourcekey="lblPageName"></asp:Label></td>
            <td><asp:Literal ID="litPageName" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblPageDisplayNameView" runat="server" meta:resourcekey="lblPageDisplayName"></asp:Label></td>
            <td><asp:Literal ID="litPageDisplayName" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblPageTitleView" runat="server" meta:resourcekey="lblPageTitle"></asp:Label>:</td>
            <td>
                <asp:Literal ID="litPageTitle" runat="server"></asp:Literal> | 
                <asp:Label ID="lblShowPageTitleView" runat="server" meta:resourcekey="lblShowPageTitle"></asp:Label>: <asp:Literal ID="litShowPageTitle" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="tdLabel nobr"><asp:Label ID="lblPageTitleFriendlyUrlView" runat="server" meta:resourcekey="lblPageTitleFriendlyUrl"></asp:Label>:</td>
            <td><asp:Literal ID="litPageTitleFriendlyUrl" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblParentView" runat="server" meta:resourcekey="lblParent"></asp:Label>:</td>
            <td><asp:Literal ID="litParent" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblTemplateView" runat="server" meta:resourcekey="lblTemplate"></asp:Label>:</td>
            <td><asp:Literal ID="litTemplate" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblCSSClassView" runat="server" meta:resourcekey="lblCSSClass"></asp:Label>:</td>
            <td><asp:Literal ID="litCSSClass" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblSlideShowGroupView" runat="server" meta:resourcekey="lblSlideShowGroup"></asp:Label>:</td>
            <td><asp:Literal ID="litSlideShowGroup" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblLinkView" runat="server" meta:resourcekey="lblLink"></asp:Label>:</td>
            <td><asp:Literal ID="litLink" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblTargetView" runat="server" meta:resourcekey="lblTarget"></asp:Label></td>
            <td><asp:Literal ID="litTarget" runat="server"></asp:Literal></td>
        </tr>
        <tr id="trLanguageView" runat="server" visible="false">
            <td class="tdLabel"><asp:Label ID="lblLanguageView" runat="server" meta:resourcekey="lblLanguage"></asp:Label>:</td>
            <td><asp:Literal ID="litLanguage" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblShowInMenuView" runat="server" meta:resourcekey="lblShowInMenu"></asp:Label>:</td>
            <td><asp:Literal ID="litShowInMenu" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblShowInBreadcrumbView" runat="server" meta:resourcekey="lblShowInBreadCrumb"></asp:Label>:</td>
            <td><asp:Literal ID="litShowInBreadcrumb" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblUserEditableView" runat="server" meta:resourcekey="lblUserEditable"></asp:Label></td>
            <td><asp:Literal ID="litUserEditable" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblShowInSitemapView" runat="server" meta:resourcekey="lblShowInSitemap"></asp:Label></td>
            <td><asp:Literal ID="litShowInSitemap" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblEnableLinkView" runat="server" meta:resourcekey="lblEnableLink"></asp:Label></td>
            <td><asp:Literal ID="litEnableLink" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblAuthorizeView" runat="server" meta:resourcekey="lblAuthorize"></asp:Label></td>
            <td><asp:Literal ID="litAuthorize" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblKeywordView" runat="server" meta:resourcekey="lblKeyword"></asp:Label>:</td>
            <td><asp:Literal ID="litKeyword" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblDescView" runat="server" meta:resourcekey="lblDesc"></asp:Label>:</td>
            <td><asp:Literal ID="litDesc" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblActiveView" runat="server" meta:resourcekey="lblActive"></asp:Label>:</td>
            <td><asp:Literal ID="litActive" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td class="tdLabel"><asp:Label ID="lblRedirectView" runat="server" meta:resourcekey="lblRedirect"></asp:Label>:</td>
            <td><asp:Literal ID="litRedirect" runat="server"></asp:Literal></td>
        </tr>
    </table>
</asp:Panel>
