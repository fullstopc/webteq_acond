﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmHighlightDetails.ascx.cs" Inherits="ctrl_ucAdmHighlightDetails" %>
<link  href="<%= System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>cropper.min.css" rel="stylesheet">
<script src="<%= System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>cropper.min.js"></script>

<script type="text/javascript">
    var txtHltStartDate;
    var txtHltEndDate;

    function initSect3() {
        txtHltStartDate = document.getElementById("<%= txtHltStartDate.ClientID %>");
        txtHltEndDate = document.getElementById("<%= txtHltEndDate.ClientID %>");

        $(function() {
            $("#<% =txtHltStartDate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
            $("#<% =txtHltEndDate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });

            $("#<% =txtHltStartDate.ClientID %>").datepicker({ changeMonth: true });
            $("#<% =txtHltStartDate.ClientID %>").datepicker({ changeYear: true });
            $("#<% =txtHltEndDate.ClientID %>").datepicker({ changeMonth: true });
            $("#<% =txtHltEndDate.ClientID %>").datepicker({ changeYear: true });

            //getter
            var changeMonth = $("#<% =txtHltStartDate.ClientID %>").datepicker('option', 'changeMonth');
            var changeYear = $("#<% =txtHltStartDate.ClientID %>").datepicker('option', 'changeYear');
            var changeMonth = $("#<% =txtHltEndDate.ClientID %>").datepicker('option', 'changeMonth');
            var changeYear = $("#<% =txtHltEndDate.ClientID %>").datepicker('option', 'changeYear');

            //setter
            $("#<% =txtHltStartDate.ClientID %>").datepicker('option', 'changeMonth', true);
            $("#<% =txtHltStartDate.ClientID %>").datepicker('option', 'changeYear', true);
            $("#<% =txtHltEndDate.ClientID %>").datepicker('option', 'changeMonth', true);
            $("#<% =txtHltEndDate.ClientID %>").datepicker('option', 'changeYear', true);
        });
    }

    function validateHltPeriod_client(source, args) {
        var bValid = false;

        source.errormessage = "Please enter valid period.";
        source.innerHTML = "Please enter valid period.";

        if ((Trim(txtHltStartDate.value) == '') && (Trim(txtHltEndDate.value) == '')) {
            args.IsValid = true;
        }
        else {
            if (Trim(txtHltStartDate.value) != '') {
                if (validateDate(txtHltStartDate.value)) {
                    bValid = true;
                }
                else {
                    bValid = false;
                }
            }

            if ((bValid) && (Trim(txtHltEndDate.value) != '')) {
                if (validateDate(txtHltEndDate.value)) {
                    if (validateDateRange(txtHltStartDate.value, txtHltEndDate.value)) {
                        bValid = true;
                    }
                    else {
                        bValid = false;
                    }
                }
                else {
                    bValid = false;
                }
            }
            else {
                bValid = false;
            }

            args.IsValid = bValid;
        }
    }

    function validateOrder_Client(source, args) {
        var boolValid = false;
        var numRE = /<%= clsAdmin.CONSTNUMERICRE %>/;

        if (args.Value != '') {
            if (args.Value.match(numRE)) {
                boolValid = true;
            }
            else {
                boolValid = false;
                source.errormessage = "<br />Please enter only numeric.";
                source.innerHTML = "<br />Please enter only numeric.";
            }
        }

        args.IsValid = boolValid;
    }

    $(document).ready(function(){
         cropInit(<%= aspectRatioX %>,<%= aspectRatioY %>);
    });
</script>

<script type="text/javascript">
    function validatePageTitleFriendlyUrl_client(source, args) {
        var txtHltTitleFriendlyURL = document.getElementById("<%= txtHltTitleFriendlyURL.ClientID %>");
        var pageTitleFriendlyUrlRE = /<%= clsAdmin.CONSTADMPAGETITLEFRIENDLYURL %>/;        
        if (txtHltTitleFriendlyURL.value.match(pageTitleFriendlyUrlRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "Please enter only valid format.";
            source.innerHTML = "Please enter only valid format.";
        }
    }
</script>

<asp:Panel ID="pnlForm" runat="server">
    <table id="tblAddDetails" cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
        <tr>
            <td colspan="2" class="tdSectionHdr">Event Details</td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td class="tdLabel">Title<span class="attention_compulsory">*</span>:</td>
            <td>
                <asp:TextBox ID="txtHltTitle" runat="server" CssClass="text_big_perc" MaxLength="250"></asp:TextBox>
                <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvHltTitle" runat="server" ErrorMessage="<br />Please enter Title." ControlToValidate="txtHltTitle" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator></span>
            </td>
        </tr>
        <tr id="trTitleJp" runat="server" visible="false">
            <td>Title (Japanese)<span class="attention_compulsory">*</span>:</td>
            <td>
                <asp:TextBox ID="txtHltTitleJp" runat="server" class="text_big_perc" MaxLength="250"></asp:TextBox>
                <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvHltTitleJp" runat="server" ErrorMessage="<br/>Please enter Title(Japanese)." ControlToValidate="txtHltTitleJp" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator></span>
            </td>
        </tr>
        <tr id="trTitleMs" runat="server" visible="false">
            <td>Title (Malay)<span class="attention_compulsory">*</span>:</td>
            <td>
                <asp:TextBox ID="txtHltTitleMs" runat="server" class="text_big_perc" MaxLength="250"></asp:TextBox>
                <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvHltTitleMs" runat="server" ErrorMessage="<br/>Please enter Title(Malay)." ControlToValidate="txtHltTitleMs" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator></span>
            </td>
        </tr>
        <tr id="trTitleZh" runat="server" visible="false">
            <td>Title (Chinese)<span class="attention_compulsory">*</span>:</td>
            <td>
                <asp:TextBox ID="txtHltTitleZh" runat="server" class="text_big_perc" MaxLength="250"></asp:TextBox>
                <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvHltTitleZh" runat="server" ErrorMessage="<br/>Please enter Title(Chinese)." ControlToValidate="txtHltTitleZh" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator></span>
            </td>
        </tr>
        <tr id="trFriendlyURL" runat="server">
            <td class="tdLabel">Page Title (Friendly URL)<span class="attention_unique">*</span>:</td>
            <td>
                <asp:TextBox ID="txtHltTitleFriendlyURL" runat="server" CssClass="text_big_perc uniq" MaxLength="250"></asp:TextBox>
                <span class="spanFieldDesc"><asp:CustomValidator ID="cvPageTitleFriendlyUrl" runat="server" ControlToValidate="txtHltTitleFriendlyURL" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validatePageTitleFriendlyUrl_client" OnServerValidate="validatePageTitleFriendlyUrl_server" OnPreRender="cvPageTitleFriendlyUrl_PreRender"></asp:CustomValidator></span>
            </td>
        </tr>
        <tr>
            <td>Snapshot:</td>
            <td><asp:TextBox ID="txtSnapshot" runat="server" MaxLength="1000" CssClass="text_big_perc" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
        </tr>
        <tr id="trSnapshotJp" runat="server" visible="false">
            <td>Snapshot (Japanese):</td>
            <td><asp:TextBox ID="txtSnapshotJp" runat="server" MaxLength="1000" CssClass="text_big_perc" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
        </tr>
        <tr id="trSnapshotMs" runat="server" visible="false">
            <td>Snapshot (Malay):</td>
            <td><asp:TextBox ID="txtSnapshotMs" runat="server" MaxLength="1000" CssClass="text_big_perc" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
        </tr>
        <tr id="trSnapshotZh" runat="server" visible="false">
            <td>Snapshot (Chinese):</td>
            <td><asp:TextBox ID="txtSnapshotZh" runat="server" MaxLength="1000" CssClass="text_big_perc" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Short Description:</td>
            <td>
                <asp:TextBox ID="txtHltShortDesc" runat="server" CssClass="text_big" TextMode="MultiLine" Rows="5"></asp:TextBox>
                <%--<asp:CustomValidator ID="cvDesc" runat="server" ControlToValidate="txtHltShortDesc" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateDesc_server" OnPreRender="cvDesc_PreRender" ValidateEmptyText="true"></asp:CustomValidator>--%>
            </td>
        </tr>
        <tr id="trDescJp" runat="server" visible="false">
            <td>Short Description (Japanese)</td>
            <td>
                <asp:TextBox ID="txtHltShortDescJp" runat="server" CssClass="text_big" TextMode="MultiLine" Rows="5"></asp:TextBox>
            </td>
        </tr>
        <tr id="trDescMs" runat="server" visible="false">
            <td>Short Description (Malay)</td>
            <td>
                <asp:TextBox ID="txtHltShortDescMs" runat="server" CssClass="text_big" TextMode="MultiLine" Rows="5"></asp:TextBox>
            </td>
        </tr>
        <tr id="trDescZh" runat="server" visible="false">
            <td>Short Description (Chinese)</td>
            <td>
                <asp:TextBox ID="txtHltShortDescZh" runat="server" CssClass="text_big" TextMode="MultiLine" Rows="5"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tdLabel">Source Title:</td>
            <td>
                <asp:TextBox runat="server" ID="txtSourceTitle" MaxLength="250" CssClass="text_big_perc"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tr>
            <td class="tdLabel">Source Url:</td>
            <td>
                <div class="input-group left">
                    <span class="input-btn">
                        <a class="">http://</a>
                    </span>
                    <asp:TextBox runat="server" ID="txtSourceUrl" MaxLength="250" CssClass="text_big_perc input-control"></asp:TextBox>
                </div>
            </td>
        </tr>
        <tr>
            <td>Date:</td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" class="tblInnerTable">
                    <tr>
                        <td>From</td>
                        <td><asp:TextBox ID="txtHltStartDate" runat="server" CssClass="text_medium" MaxLength="10"></asp:TextBox></td>
                        <td>&nbsp;&nbsp;</td>
                        <td>To</td>
                        <td class="tdInnerTableLast"><asp:TextBox ID="txtHltEndDate" runat="server" CssClass="text_medium" MaxLength="10"></asp:TextBox></td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cvHltPeriod" runat="server" CssClass="errmsg" Display="Dynamic" ClientValidationFunction="validateHltPeriod_client"></asp:CustomValidator>
            </td>
        </tr>
        <tr id="trCategory" runat="server" visible="false">
            <td>Category<span class="attention_compulsory">*</span>:</td>
            <td>
                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="ddl"></asp:DropDownList>
                <asp:HiddenField ID="hdnCategory" runat="server" />
                <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvCategory" runat="server" ErrorMessage="<br />Please select one Category." ControlToValidate="ddlCategory" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator></span>
            </td>
        </tr>
        <tr>
            <td>Thumbnail:</span>
            </td>
            <td class="tdWithFileUpload">
<%--                <asp:Panel ID="pnlFileUpload" runat="server" CssClass="divFileUpload">
                    <asp:Panel ID="pnlFileUploadAction" runat="server" CssClass="divFileUploadAction">
                        
                    </asp:Panel>
                    <div class="divFileUploadRule">
                    </div>
                </asp:Panel>--%>
                <asp:Panel ID="pnlFileUpload" runat="server" CssClass="divFileUpload">
                    <asp:Panel ID="pnlFileUploadAction" runat="server" CssClass="divFileUploadAction">
                        <asp:FileUpload ID="fileHltImage" runat="server" CssClass="fileImg"/>
                    </asp:Panel>
                    <span class="spanTooltip">
                        <span class="icon"></span>
                        <span class="msg">
                            <asp:Literal ID="litUploadRule" runat="server"></asp:Literal>
                        </span>
                    </span>
                   <i class="material-icons icon-edit">crop</i>
                </asp:Panel>
                <table width="100%" id="tblCropImg" style="display:none;">
                    <thead>
                        <tr>
                            <td colspan="2">
                                <button type="button" class="btnDarkBlue" id="btnConfirmCrop">Crop</button>
                                <button type="button" class="btnDarkBlue" id="btnCancelCrop">Cancel</button>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="70%">
                                <div class="divImgContainer" style="width: 100%;">
                                    <img style="max-width:100%;width:100%;height:auto;"/>
                                </div>
                            </td>
                            <td width="30%">
                                <div class="divImgPreview" style="overflow:hidden;height:200px;width:200px;border:1px solid #ececec;">
                                </div>
                            </td>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <input type="hidden" id="hdnImgX" runat="server" data-id="hdnImgX"/>
                                <input type="hidden" id="hdnImgY" runat="server" data-id="hdnImgY"/>
                                <input type="hidden" id="hdnImgWidth" runat="server" data-id="hdnImgWidth"/>
                                <input type="hidden" id="hdnImgHeight" runat="server" data-id="hdnImgHeight"/>
                                <input type="hidden" id="hdnImgName" runat="server" data-id="hdnImgName"/>
                                <input type="hidden" id="hdnImgCropped" runat="server" data-id="hdnImgCropped"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <asp:HiddenField ID="hdnHltImage" runat="server" />
                <asp:HiddenField ID="hdnHltImageRef" runat="server" />
                <asp:CustomValidator ID="cvHltImage" runat="server" ControlToValidate="fileHltImage" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateHltImage_server" OnPreRender="cvHltImage_PreRender"></asp:CustomValidator>
                <asp:Panel ID="pnlHltImage" runat="server" Visible="false" CssClass="divSavedImageContainer">
                    <asp:LinkButton ID="lnkbtnDeleteImage" runat="server" Text="Delete" ToolTip="Delete" CausesValidation="false" OnClick="lnkbtnDeleteImage_Click"></asp:LinkButton>
                    <br />
                    <asp:Image ID="imgHltImage" runat="server" />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>Order:</td>
            <td>
                <asp:TextBox ID="txtOrder" runat="server" CssClass="text_small" MaxLength="5"></asp:TextBox>
                <asp:CustomValidator ID="cvOrder" runat="server" ControlToValidate="txtOrder" ClientValidationFunction="validateOrder_Client"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="nobr">Show on Top:</td>
            <td class="tdWithCheckbox"><asp:CheckBox ID="chkboxShowTop" runat="server" Checked="true" /></td>
        </tr>
        <tr>
            <td>Active:</td>
            <td class="tdWithCheckbox"><asp:CheckBox ID="chkboxVisible" runat="server" Checked="true" /></td>
        </tr>
        <tr id="trAllowComment" runat="server" visible="false">
            <td>Allow Comment:</td>
            <td class="tdWithCheckbox"><asp:CheckBox ID="chkboxComment" runat="server" /></td>
        </tr>
        <tr>
            <td class="tdSpacer">&nbsp;</td>
            <td class="tdSpacer">&nbsp;</td>
        </tr>
        <tfoot>
            <tr>
                <td></td>
                <td>
                    <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                        <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack" CausesValidation="false"></asp:LinkButton>
                        </asp:Panel>
                        <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                            <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CssClass="btn btnCancel" Visible="false" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
                        </asp:Panel>
                    </asp:Panel>
                </td>
            </tr>
        </tfoot>
    </table>
    
</asp:Panel>
