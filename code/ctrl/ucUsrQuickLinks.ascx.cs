﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucUsrQuickLinks : System.Web.UI.UserControl
{
    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void fill()
    {
        if (!IsPostBack)
        {
            clsConfig config = new clsConfig();

            string strQuickContactTel = config.extractItemByNameGroup(clsConfig.CONSTNAMEQUICKCONTACTTEL, clsConfig.CONSTGROUPQUICKCONTACT, 1) ? config.value : null;
            string strQuickContactEmail = config.extractItemByNameGroup(clsConfig.CONSTNAMEQUICKCONTACTEML, clsConfig.CONSTGROUPQUICKCONTACT, 1) ? config.value : null;


            if (!string.IsNullOrEmpty(strQuickContactTel))
            {
                litQuickContactNo.Text = strQuickContactTel;
            }

            if (!string.IsNullOrEmpty(strQuickContactEmail))
            {
                litQuickContactNoEmail.Text = "<a href=\"mailto:" + strQuickContactEmail + "\" title=\"contact us at " + strQuickContactEmail + "\" class=\"hypQuickEmail\">" + strQuickContactEmail + "</a>";
            }

            if (string.IsNullOrEmpty(strQuickContactTel) && string.IsNullOrEmpty(strQuickContactEmail))
            {
                pnlQuickLinksInner.Visible = false;
            }

        }
    }
    #endregion
}
