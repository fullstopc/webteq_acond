﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucUsrDirGroup : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _currentPage = 0;
    protected int _groupId = 0;
    protected int _sectId = 1;

    protected string _lang = "";
    protected Boolean _boolShowTop = false;
    protected int _pageSize = 0;

    protected int grpCount = 0;
    protected int _noOfGroupInRow = 0;

    protected int groupTotal = 0;
    protected int _pageIdTargetPage = 0;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }

    public int sectId
    {
        get
        {
            if (ViewState["SECTID"] == null)
            {
                return _sectId;
            }
            else
            {
                return Convert.ToInt16(ViewState["SECTID"]);
            }
        }
        set { ViewState["SECTID"] = value; }
    }

    public int groupId
    {
        get
        {
            if (ViewState["GROUPID"] == null)
            {
                return _groupId;
            }
            else
            {
                return Convert.ToInt16(ViewState["GROUPID"]);
            }
        }
        set { ViewState["GROUPID"] = value; }
    }

    public Boolean boolShowTop
    {
        get
        {
            if (ViewState["BOOLSHOWTOP"] == null)
            {
                return _boolShowTop;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLSHOWTOP"]);
            }
        }
        set { ViewState["BOOLSHOWTOP"] = value; }
    }

    public int pageSize
    {
        get
        {
            if (ViewState["PAGESIZE"] == null)
            {
                return _pageSize;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGESIZE"]);
            }
        }
        set { ViewState["PAGESIZE"] = value; }
    }

    public int noOfGroupInRow
    {
        get
        {
            if (ViewState["NOOFGROUPINROW"] == null)
            {
                return _noOfGroupInRow;
            }
            else
            {
                return Convert.ToInt16(ViewState["NOOFGROUPINROW"]);
            }
        }
        set { ViewState["NOOFGROUPINROW"] = value; }
    }

    public int currentPage
    {
        get
        {
            if (ViewState["CURRENTPAGE"] == null)
            {
                return _currentPage;
            }
            else
            {
                return Convert.ToInt16(ViewState["CURRENTPAGE"]);
            }
        }
        set { ViewState["CURRENTPAGE"] = value; }
    }

    public int pageIdTargetPage
    {
        get
        {
            if (ViewState["PAGEIDTARGETPAGE"] == null)
            {
                return _pageIdTargetPage;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGEIDTARGETPAGE"]);
            }
        }
        set { ViewState["PAGEIDTARGETPAGE"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        pageIdTargetPage = clsPage.getPageIdByTemplate(clsSetting.CONSTUSRDIRPAGE, lang, 1);

        Page.LoadComplete += new EventHandler(Page_LoadComplete);

        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "group.css' type='text/css' rel='Stylesheet' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        bindRptData();
    }

    protected void rptGroup_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intGrpId = int.Parse(DataBinder.Eval(e.Item.DataItem, "GRP_ID").ToString());
            string strGroupDName = DataBinder.Eval(e.Item.DataItem, "GRP_DNAME").ToString();

            Boolean boolFound = false;
            string strQuery = Request.Url.Query;

            if (!string.IsNullOrEmpty(strQuery))
            {
                string[] strQuerySplit = strQuery.Split((char)'&');
                strQuery = "";

                for (int i = 0; i <= strQuerySplit.Length - 1; i++)
                {
                    if (strQuerySplit[i].IndexOf("grpid=") >= 0)
                    {
                        boolFound = true;

                        if (string.IsNullOrEmpty(strQuery)) { strQuerySplit[i] = "?grpid=" + intGrpId; }
                        else { strQuerySplit[i] = "grpid=" + intGrpId; }
                    }
                    else if (strQuerySplit[i].IndexOf("id=") == 0)
                    {
                        strQuerySplit[i] = "";
                        if (i == (strQuerySplit.Length - 1))
                        {
                            if (!string.IsNullOrEmpty(strQuery)) { strQuery = strQuery.Substring(0, strQuery.Length - 1); }
                        }
                    }

                    if (!string.IsNullOrEmpty(strQuerySplit[i]))
                    {
                        if (i == (strQuerySplit.Length - 1))
                        {
                            strQuery += strQuerySplit[i];
                        }
                        else
                        {
                            strQuery += strQuerySplit[i] + "&";
                        }                    
                    }
                }
            }
            else
            {
                boolFound = true;
                strQuery = "?grpid=" + intGrpId;
            }

            if (!boolFound)
            {
                strQuery += "&grpid=" + intGrpId;
            }

            HyperLink hypGroupDName = (HyperLink)e.Item.FindControl("hypGroupDName");
            hypGroupDName.Text = strGroupDName;
            hypGroupDName.ToolTip = strGroupDName;
                        
            string strMerchantPage = clsSetting.CONSTUSRDIRPAGE + "?pgid=" + pageIdTargetPage;
            string strUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strMerchantPage + "&grpid=" + intGrpId;
            hypGroupDName.NavigateUrl = strUrl;

            if (intGrpId == groupId)
            {
                hypGroupDName.CssClass = "hypGroupDNameSel";

            }
        }
    }

    protected void rptGroup_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
    }
    #endregion


    #region "Methods"
    protected void bindRptData()
    {
        if (Application["BUSINESSTYPE"] == null)
        {
            clsDirGroup grp = new clsDirGroup();
            Application["BUSINESSTYPE"] = grp.getGrpListByGrpingId(clsAdmin.CONSTLISTGROUPINGBIZTYPE, 1);
        }

        DataSet ds = new DataSet();
        ds = (DataSet)Application["BUSINESSTYPE"];

        DataView dv = new DataView(ds.Tables[0]);

        Boolean boolFilter = false;

        if (boolShowTop)
        {
            if (boolFilter) { dv.RowFilter += " AND GRP_SHOWTOP = 1"; }
            else { dv.RowFilter = "GRP_SHOWTOP = 1"; }
        }

        dv.Sort = "GRP_DNAME ASC";

        PagedDataSource pds = new PagedDataSource();
        pds.DataSource = dv;

        if (pageSize > 0)
        {
            pds.AllowPaging = true;
            pds.PageSize = pageSize;
        }
        
        if (currentPage < 0) { currentPage = 0; }
        if (currentPage > pds.PageCount - 1) { currentPage = 0; }
        
        pds.CurrentPageIndex = currentPage;
        groupTotal = dv.Count;
        grpCount = 0;
        rptGroup.DataSource = pds;
        rptGroup.DataBind();

        if (pds.PageCount > 1)
        {
            if (currentPage == 0)
            {
                //lnkbtnPrev.Enabled = false;
                //lnkbtnPrev.CssClass = "btnGroupLinkDisabled";

                //imgbtnPrev.Enabled = false;
                //imgbtnPrev.CssClass = "imgbtnPrevDisabled";
            }
            else
            {
                //lnkbtnPrev.Enabled = true;
                //lnkbtnPrev.CssClass = "btnGroupLink";

                //imgbtnPrev.Enabled = true;
                //imgbtnPrev.CssClass = "imgbtnPrev";
            }

            if (currentPage == pds.PageCount - 1)
            {
                //lnkbtnNext.Enabled = false;
                //lnkbtnNext.CssClass = "btnGroupLinkDisabled";

                //imgbtnNext.Enabled = false;
                //imgbtnNext.CssClass = "imgbtnNextDisabled";
            }
            else
            {
                //lnkbtnNext.Enabled = true;
                //lnkbtnNext.CssClass = "btnGroupLink";

                //imgbtnNext.Enabled = true;
                //imgbtnNext.CssClass = "imgbtnNext";
            }
        }
        else
        {
            //lnkbtnPrev.Enabled = false;
            //lnkbtnNext.Enabled = false;
            //lnkbtnPrev.CssClass = "btnGroupLinkDisabled";
            //lnkbtnNext.CssClass = "btnGroupLinkDisabled";

            //imgbtnPrev.Enabled = false;
            //imgbtnNext.Enabled = false;
            //imgbtnPrev.CssClass = "imgbtnPrevDisabled";
            //imgbtnNext.CssClass = "imgbtnNextDisabled";
        }
    }
    #endregion
}