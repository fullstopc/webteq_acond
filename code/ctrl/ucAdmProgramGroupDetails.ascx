﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmProgramGroupDetails.ascx.cs" Inherits="ctrl_ucAdmProgramGroupDetails" %>

<script type="text/javascript">
    function validateProgCatCode_client(source, args) {
        var txtProgCatCode = document.getElementById("<%= txtProgCatCode.ClientID %>");
        var progCatCodeRE = /<%= clsAdmin.CONSTADMCATCODERE %>/;

        if (txtProgCatCode.value.match(progCatCodeRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "Please enter only alphanumeric.";
            source.innerHTML = "Please enter only alphanumeric.";
        }
    }

    function validateInteger_client(source, args) {
        var integerRE = /<%= clsAdmin.CONSTNUMERICRE %>/;

        if (args.Value.match(integerRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "<br />Please enter valid value.";
            source.innerHTML = "<br />Please enter valid value.";
        }
    }
</script>
    
<asp:Panel ID="pnlForm" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table cellpadding="0" cellspacing="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr"><asp:Literal ID="litProgGroupDetails" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr id="trAdmLink" runat="server" visible="false">
                <td colspan="2"><asp:HyperLink ID="hypUserViewLink" runat="server"></asp:HyperLink></td>
            </tr>
            <tr id="trAdmLinkSpacer" runat="server" visible="false">
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabelBig"><asp:Label ID="lblProgGroupCode" runat="server"></asp:Label><span class="attention_unique">*</span>:</td>
                <td><asp:TextBox ID="txtProgCatCode" runat="server" class="text_fullwidth uniq" MaxLength="20"></asp:TextBox>
                    <span class="spanFieldDesc">
                    <asp:RequiredFieldValidator ID="rfvProgCatCode" runat="server" ErrorMessage="<br />Please enter Program Code." ControlToValidate="txtProgCatCode" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvProgCatCode" runat="server" ControlToValidate="txtProgCatCode" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateProgCatCode_client" OnServerValidate="validateProgCatCode_server" OnPreRender="cvProgCatCode_PreRender"></asp:CustomValidator></span>
                </td>
            </tr>
            <tr>
                <td class="tdLabelBig"><asp:Label ID="lblProgGroupName" runat="server"></asp:Label><span class="attention_unique">*</span>:</td>
                <td><asp:TextBox ID="txtProgCatName" runat="server" class="text_fullwidth uniq" MaxLength="250"></asp:TextBox>
                    <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvProgCatName" runat="server" ErrorMessage="<br />Please enter Program Name." ControlToValidate="txtProgCatName" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvProgCatName" runat="server" ControlToValidate="txtProgCatName" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateProgCatName_server" OnPreRender="cvProgCatName_PreRender"></asp:CustomValidator></span>
                </td>
            </tr>
            <tr id="trProgGroupNameZh" runat="server" visible="false">
                <td class="tdLabelBig"><asp:Label ID="lblProgGroupName_zh" runat="server"></asp:Label><span class="attention_unique">*</span>:</td>
                <td><asp:TextBox ID="txtProgCatName_zh" runat="server" class="text_fullwidth uniq" MaxLength="250"></asp:TextBox>
                    <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvProgCatName_zh" runat="server" ErrorMessage="<br />Please enter Program Name (Chinese)." ControlToValidate="txtProgCatName_zh" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvProgCatName_zh" runat="server" ControlToValidate="txtProgCatName_zh" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateProgCatNameZH_server" OnPreRender="cvProgCatNameZH_PreRender"></asp:CustomValidator></span>
                </td>
            </tr>
            <tr>
                <td class="tdLabelBig"><asp:Label ID="lblProgGroupDName" runat="server"></asp:Label><span class="attention_compulsory">*</span>:</td>
                <td><asp:TextBox ID="txtProgCatDName" runat="server" class="text_fullwidth compulsory" MaxLength="250"></asp:TextBox><span class="spanFieldDesc"><a href="javascript:void(0);" onclick="copyValue('<% =txtProgCatName.ClientID %>','<% =txtProgCatDName.ClientID %>');" title="same as Name">same as Name</a></span>
                    <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvProgGroupDName" runat="server" ErrorMessage="<br />Please enter Display Program Name." ControlToValidate="txtProgCatDName" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator></span>
                </td>
            </tr>
            <tr id="trProgGroupDNameZh" runat="server" visible="false">
                <td class="tdLabelBig"><asp:Label ID="lblProgGroupDName_zh" runat="server"></asp:Label><span class="attention_compulsory">*</span>:</td>
                <td><asp:TextBox ID="txtProgCatDName_zh" runat="server" class="text_fullwidth compulsory" MaxLength="250"></asp:TextBox><span class="spanFieldDesc"><a href="javascript:void(0);" onclick="copyValue('<% =txtProgCatName_zh.ClientID %>','<% =txtProgCatDName_zh.ClientID %>');" title="same as Name (Chinese)">same as Name (Chinese)</a></span>
                    <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvProgGroupDName_zh" runat="server" ErrorMessage="<br />Please enter Display Program Name (Chinese)." ControlToValidate="txtProgCatDName_zh" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator></span>
                </td>
            </tr>
            <tr>
                <td class="tdLabelBig"><asp:Label ID="lblProgGroupImage" runat="server"></asp:Label>:</td>
                <td>
                    <asp:Panel ID="pnlFileUpload" runat="server" CssClass="divFileUpload">
                        <asp:Panel ID="pnlFileUploadAction" runat="server" CssClass="divFileUploadAction">
                            <asp:FileUpload ID="fileProgCatImage" runat="server" />
                        </asp:Panel>
                        <span class="spanTooltip">
                            <span class="icon"></span>
                            <span class="msg">
                                <asp:Literal ID="litUploadRule" runat="server"></asp:Literal>
                            </span>
                        </span>
                    </asp:Panel>
                    <asp:HiddenField ID="hdnProgCatImage" runat="server" />
                    <asp:HiddenField ID="hdnProgCatImageRef" runat="server" />
                    <asp:CustomValidator ID="cvProgCatImage" runat="server" ControlToValidate="fileProgCatImage" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateProgCatImage_server" OnPreRender="cvProgCatImage_PreRender"></asp:CustomValidator>
                    <asp:Panel ID="pnlProgCatImage" runat="server" Visible="false">
                        <br /><asp:LinkButton ID="lnkbtnDeleteImage" runat="server" Text="Delete" Tooltip="Delete" CausesValidation="false" OnClick="lnkbtnDeleteImage_Click"></asp:LinkButton>
                        <br /><span class="fieldDesc"><asp:Literal ID="litImage" runat="server"></asp:Literal></span>
                        <br /><asp:Image ID="imgProgCatImage" runat="server" />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
        </table>
 
         <table cellpadding="0" cellspacing="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr"><asp:Literal ID="litProgGroupSetting" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
        
            <tr>
                <td class="tdLabelBig">Active:</td>
                <td><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false" OnClick="lnkbtnCancel_Click"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" class="btn btnCancel" Visible="false" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>