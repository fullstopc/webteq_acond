﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

public partial class ctrl_ucAdmSmsSetup : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _shipCompId = 0;
    protected int _shipMethodId = 0;
    protected int _areaId = 0;
    protected int _mode = 1;
    protected int _currId = 0;
    protected int itemCount = 0;
    protected int itemTotal = 0;
    protected DataSet _dsCurrency = new DataSet();
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int shipCompId
    {
        get { return _shipCompId; }
        set { _shipCompId = value; }
    }

    public int shipMethodId
    {
        get { return _shipMethodId; }
        set { _shipMethodId = value; }
    }

    public int areaId
    {
        get { return _areaId; }
        set { _areaId = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int currId
    {
        get
        {
            if (ViewState["currId"] == null)
            {
                return _currId;
            }
            else
            {
                return int.Parse(ViewState["currId"].ToString());
            }
        }
        set { ViewState["currId"] = value; }
    }

    protected DataSet dsCurrency
    {
        get
        {
            if (ViewState["DSCURRENCY"] == null)
            {
                return _dsCurrency;
            }
            else
            {
                return (DataSet)ViewState["DSCURRENCY"];
            }
        }
        set { ViewState["DSCURRENCY"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
    }
   #endregion

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        clsConfig config = new clsConfig();
        int intRecordAffected = 0;

        Boolean boolSuccess = true;
        Boolean boolEdited = false;
       
        // SMS Settings
        string strSenderUrl = lblSenderUrl.Text;
        string strSenderAPIKey = lblSenderAPIKey.Text;
        string strUserName = lblUserName.Text;
        string strSenderPwd = lblSenderPwd.Text;
        string strUserId = lblUserId.Text;
        string strDefaultRecipient = lblSMSDefaultRecipient.Text;
        string strEnquirySms = lblEnquirySms.Text;
        string strSendEnquirySms = lblSendEnquirySms.Text;

        string strNewOrderSms = lblNewOrderSms.Text;
        string strSendNewOrderSms = lblSendNewOrderSms.Text;
        string strPaymentSms = lblPaymentSms.Text;
        string strSendPaymentSms = lblSendPaymentSms.Text;

        string strSenderUrlValue = txtSenderUrl.Text.Trim();
        string strSenderAPIKeyValue = txtSenderAPIKey.Text;
        string strUserNameValue = txtUserName.Text.Trim();
        string strSenderPwdValue = txtSenderPwd.Text.Trim();
        string strUserIdValue = txtUserId.Text.Trim();
        string strDefaultRecipientValue = txtSMSDefaultRecipient.Text.Trim();
        string strEnquirySmsValue = txtEnquirySms.Text.Trim();
        string strSendEnquirySmsValue = (chkboxSendEnquirySms.Checked)? "1" : "0";
        string strNewOrderSmsValue = txtNewOrderSms.Text.Trim();
        string strSendNewOrderSmsValue = (chkboxSendNewOrderSms.Checked) ? "1" : "0";
        string strPaymentSmsValue = txtPaymentSms.Text.Trim();
        string strSendPaymentSmsValue = (chkboxSendPaymentSms.Checked) ? "1" : "0";

        if (boolSuccess && !config.isItemExist(strSenderUrl, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.addItem(strSenderUrl, strSenderUrlValue, clsConfig.CONSTGROUPSMSSETTINGS, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strSenderUrl, strSenderUrlValue, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPSMSSETTINGS, strSenderUrl, strSenderUrlValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strSenderAPIKey, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.addItem(strSenderAPIKey, strSenderAPIKeyValue, clsConfig.CONSTGROUPSMSSETTINGS, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strSenderAPIKey, strSenderAPIKeyValue, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPSMSSETTINGS, strSenderAPIKey, strSenderAPIKeyValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strUserName, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.addItem(strUserName, strUserNameValue, clsConfig.CONSTGROUPSMSSETTINGS, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strUserName, strUserNameValue, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPSMSSETTINGS, strUserName, strUserNameValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strSenderPwd, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.addItem(strSenderPwd, strSenderPwdValue, clsConfig.CONSTGROUPSMSSETTINGS, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strSenderPwd, strSenderPwdValue, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPSMSSETTINGS, strSenderPwd, strSenderPwdValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strUserId, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.addItem(strUserId, strUserIdValue, clsConfig.CONSTGROUPSMSSETTINGS, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strUserId, strUserIdValue, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPSMSSETTINGS, strUserId, strUserIdValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strDefaultRecipient, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.addItem(strDefaultRecipient, strDefaultRecipientValue, clsConfig.CONSTGROUPSMSSETTINGS, 1, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData(strDefaultRecipient, strDefaultRecipientValue, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPSMSSETTINGS, strDefaultRecipient, strDefaultRecipientValue, Convert.ToInt16(Session["ADMID"]));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strEnquirySms, clsConfig.CONSTGROUPSMSCONTENT))
        {
            intRecordAffected = config.addItem2(strEnquirySms, strEnquirySmsValue, clsConfig.CONSTGROUPSMSCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strEnquirySms, strEnquirySmsValue, clsConfig.CONSTGROUPSMSCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPSMSCONTENT, strEnquirySms, strEnquirySmsValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strSendEnquirySms, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.addItem(strSendEnquirySms, strSendEnquirySmsValue, clsConfig.CONSTGROUPSMSSETTINGS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strSendEnquirySms, strSendEnquirySmsValue, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPSMSSETTINGS, strSendEnquirySms, strSendEnquirySmsValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        
        /*New Order SMS*/
        if (boolSuccess && !config.isItemExist(strNewOrderSms, clsConfig.CONSTGROUPSMSCONTENT))
        {
            intRecordAffected = config.addItem2(strNewOrderSms, strNewOrderSmsValue, clsConfig.CONSTGROUPSMSCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strNewOrderSms, strNewOrderSmsValue, clsConfig.CONSTGROUPSMSCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPSMSCONTENT, strNewOrderSms, strNewOrderSmsValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strSendNewOrderSms, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.addItem(strSendNewOrderSms, strSendNewOrderSmsValue, clsConfig.CONSTGROUPSMSSETTINGS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strSendNewOrderSms, strSendNewOrderSmsValue, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPSMSSETTINGS, strSendNewOrderSms, strSendNewOrderSmsValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        /*End New Order SMS*/

        /*New Payment SMS*/
        if (boolSuccess && !config.isItemExist(strPaymentSms, clsConfig.CONSTGROUPSMSCONTENT))
        {
            intRecordAffected = config.addItem2(strPaymentSms, strPaymentSmsValue, clsConfig.CONSTGROUPSMSCONTENT, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strPaymentSms, strPaymentSmsValue, clsConfig.CONSTGROUPSMSCONTENT))
        {
            intRecordAffected = config.updateItem2(clsConfig.CONSTGROUPSMSCONTENT, strPaymentSms, strPaymentSmsValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }

        if (boolSuccess && !config.isItemExist(strSendPaymentSms, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.addItem(strSendPaymentSms, strSendPaymentSmsValue, clsConfig.CONSTGROUPSMSSETTINGS, 1, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        else if (!config.isExactSameSetData2(strSendPaymentSms, strSendPaymentSmsValue, clsConfig.CONSTGROUPSMSSETTINGS))
        {
            intRecordAffected = config.updateItem(clsConfig.CONSTGROUPSMSSETTINGS, strSendPaymentSms, strSendPaymentSmsValue, int.Parse(Session["ADMID"].ToString()));

            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }
        }
        /*End New Payment SMS*/
        // End of SMS Settings

        if (!boolSuccess)
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to update control panel. Please try again.</div>";

            Response.Redirect(currentPageName);
        }
        else
        {
            if (boolEdited)
            {
                Session["EDITEDSMS"] = 1;
            }
            else
            {
                Session["EDITEDSMS"] = 1;
                Session["NOCHANGE"] = 1;
            }

            Response.Redirect(currentPageName);
        }
    }

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                mode = 2;
            }

            setPageProperties();
        }
    }

    protected void setPageProperties()
    {
        lblSenderUrl.Text = clsConfig.CONSTNAMESMSSENDERURL;
        lblSenderAPIKey.Text = clsConfig.CONSTNAMESMSSENDERAPIKEY;
        lblUserName.Text = clsConfig.CONSTNAMESMSSENDERUSERNAME;
        lblSenderPwd.Text = clsConfig.CONSTNAMESMSSENDERPWD;
        lblUserId.Text = clsConfig.CONSTNAMESMSSENDERUSERID;
        lblSMSDefaultRecipient.Text = clsConfig.CONSTNAMESMSDEFAULTRECIPIENT;

        lblEnquirySms.Text = clsConfig.CONSTNAMEENQUIRYSMSCONTENT;
        lblSendEnquirySms.Text = clsConfig.CONSTNAMESENDENQUIRYSMS;
        lblNewOrderSms.Text = clsConfig.CONSTNAMENEWORDERSMSCONTENT;
        lblSendNewOrderSms.Text = clsConfig.CONSTNAMESENDNEWORDERSMS;
        lblPaymentSms.Text = clsConfig.CONSTNAMEPAYMENTSMSCONTENT;
        lblSendPaymentSms.Text = clsConfig.CONSTNAMESENDPAYMENTSMS;

        fillForm();
    }

    protected void fillForm()
    {
        clsConfig config = new clsConfig();

        DataSet ds = config.getItemList(1);
        DataRow[] foundRow;

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPSMSSETTINGS + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMESMSSENDERURL, true) == 0) { txtSenderUrl.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMESMSSENDERAPIKEY, true) == 0) { txtSenderAPIKey.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMESMSSENDERUSERNAME, true) == 0) { txtUserName.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMESMSSENDERPWD, true) == 0)
            {
                txtSenderPwd.Text = row["CONF_VALUE"].ToString();
                txtSenderPwd.Attributes["value"] = txtSenderPwd.Text;
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMESMSSENDERUSERID, true) == 0) { txtUserId.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMESMSDEFAULTRECIPIENT, true) == 0) { txtSMSDefaultRecipient.Text = row["CONF_VALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMESENDENQUIRYSMS, true) == 0)
            {
                if (!string.IsNullOrEmpty(row["CONF_VALUE"].ToString()))
                {
                    chkboxSendEnquirySms.Checked = (row["CONF_VALUE"].ToString() == "1") ? true : false;
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMESENDNEWORDERSMS, true) == 0) { chkboxSendNewOrderSms.Checked = (row["CONF_VALUE"].ToString() == "1") ? true : false; }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMESENDPAYMENTSMS, true) == 0) { chkboxSendPaymentSms.Checked = (row["CONF_VALUE"].ToString() == "1") ? true : false; }
        }

        foundRow = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPSMSCONTENT + "'");

        foreach (DataRow row in foundRow)
        {
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEENQUIRYSMSCONTENT, true) == 0) { txtEnquirySms.Text = row["CONF_LONGVALUE"].ToString(); }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMENEWORDERSMSCONTENT, true) == 0) { txtNewOrderSms.Text = row["CONF_LONGVALUE"].ToString();}
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEPAYMENTSMSCONTENT, true) == 0) { txtPaymentSms.Text = row["CONF_LONGVALUE"].ToString(); }
        }
    }  
    #endregion
}