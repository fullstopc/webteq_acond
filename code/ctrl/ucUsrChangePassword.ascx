﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrChangePassword.ascx.cs" Inherits="ctrl_ucUsrChangePassword" %>

<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>strings.js" type="text/javascript"></script>
<script type="text/javascript">
        function validatePassword_client(source, args) {
            if(args.Value.length < <% =clsSetting.CONSTADMPASSWORDMINLENGTH %>)
            {
                args.IsValid = false;
                
                source.errormessage = '<%= GetGlobalResourceObject("GlobalResource", "cvPasswordMinLength").ToString()%>';
                source.innerHTML =  '<%= GetGlobalResourceObject("GlobalResource", "cvPasswordMinLength").ToString()%>';
            }            
        }

        function validateSamePassword_client(source, args) {
            var txtCurrentPwd = document.getElementById("<%= txtCurrentPwd.ClientID %>");
            var txtNewPwd = document.getElementById("<%= txtNewPwd.ClientID %>");

            if (Trim(txtCurrentPwd.value) != '' && Trim(txtNewPwd.value) != '') {
                console.log(Trim(txtCurrentPwd.value)+":" + Trim(txtNewPwd.value) );

                var index = Trim(txtCurrentPwd.value).localeCompare(Trim(txtNewPwd.value));

                if(index == 0)
                {
                    args.IsValid = false;

                    source.errormessage = '<%= GetGlobalResourceObject("GlobalResource", "cvPasswordCurNotMatch").ToString()%>';
                    source.innerHTML =  '<%= GetGlobalResourceObject("GlobalResource", "cvPasswordCurNotMatch").ToString()%>';
                }
            }           
        }
        
        function validateCurrentPwd_client(source, args) {
            var txtCurrentPwd = document.getElementById("<%= txtCurrentPwd.ClientID %>");
            var txtNewPwd = document.getElementById("<%= txtNewPwd.ClientID %>");
            var txtConfirmPwd = document.getElementById("<%= txtConfirmPwd.ClientID %>");
            
            var boolValid = false;

            source.errormessage = '<%= GetGlobalResourceObject("GlobalResource", "rfvEnterPasswordCurrent").ToString()%>'; 
            source.innerHTML = '<%= GetGlobalResourceObject("GlobalResource", "rfvEnterPasswordCurrent").ToString()%>'; 

            if (Trim(txtCurrentPwd.value) != '') {
                args.IsValid = true;
            }
            else {
                if (Trim(txtNewPwd.value) != '') {
                    boolValid = false;
                }
                else {
                    if (Trim(txtConfirmPwd.value) != '') {
                        boolValid = false;
                    }
                    else {
                        boolValid = true;
                    }
                }

                args.IsValid = boolValid;
            }
        }

        function validateNewPwd_client(source, args) {
            var txtCurrentPwd = document.getElementById("<%= txtCurrentPwd.ClientID %>");
            var txtNewPwd = document.getElementById("<%= txtNewPwd.ClientID %>");
            var txtConfirmPwd = document.getElementById("<%= txtConfirmPwd.ClientID %>");

            var boolValid = false;

            source.errormessage = '<%= GetGlobalResourceObject("GlobalResource", "rfvEnterPasswordNew").ToString()%>';
            source.innerHTML =  '<%= GetGlobalResourceObject("GlobalResource", "rfvEnterPasswordNew").ToString()%>';

            if (Trim(txtNewPwd.value) != '') {
                args.IsValid = true;
            }
            else {
                if (Trim(txtCurrentPwd.value) != '') {
                    boolValid = false;
                }
                else {
                    if (Trim(txtConfirmPwd.value) != '') {
                        boolValid = false;
                    }
                    else {
                        boolValid = true;
                    }
                }

                args.IsValid = boolValid;
            }
        }

        function validateConfirmPwd_client(source, args) {
            var txtCurrentPwd = document.getElementById("<%= txtCurrentPwd.ClientID %>");
            var txtNewPwd = document.getElementById("<%= txtNewPwd.ClientID %>");
            var txtConfirmPwd = document.getElementById("<%= txtConfirmPwd.ClientID %>");

            var boolValid = false;

            source.errormessage = '<%= GetGlobalResourceObject("GlobalResource", "rfvEnterPasswordConfirm").ToString()%>';
            source.innerHTML =  '<%= GetGlobalResourceObject("GlobalResource", "rfvEnterPasswordConfirm").ToString()%>';

            if (Trim(txtConfirmPwd.value) != '') {
                args.IsValid = true;
            }
            else {
                if (Trim(txtNewPwd.value) != '') {
                    boolValid = false;
                }
                else {
                    if (Trim(txtCurrentPwd.value) != '') {
                        boolValid = false;
                    }
                    else {
                        boolValid = true;
                    }
                }

                args.IsValid = boolValid;
            }
        }
</script>

<asp:Panel ID="pnlResetPassword" runat="server">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-label">
                    <asp:Label ID="lblCurrentPwd" runat="server" meta:resourcekey="lblCurrentPwd">Kata laluan Semasa:</asp:Label>:
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-input">
                    <asp:TextBox ID="txtCurrentPwd" runat="server" MaxLength="20" CssClass="text_medium" TextMode="Password"></asp:TextBox>
                </div>
                <div class="form-error">
                    <asp:HiddenField ID="hdnPassword" runat="server" />
                    <asp:HiddenField ID="hdnNewPassword" runat="server" />
                    <asp:RequiredFieldValidator ID="rfvCurrentPwd" runat="server" ControlToValidate="txtCurrentPwd" Display="Dynamic" ValidationGroup="vgChangePwd" CssClass="errmsg" ErrorMessage="<%$ Resources:GlobalResource, rfvEnterPasswordCurrent%>"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvCurrentPwd" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtCurrentPwd" ClientValidationFunction="validateCurrentPwd_client" OnServerValidate="validateCurrentPwd_server" OnPreRender="cvCurrentPwd_PreRender" ValidateEmptyText="true" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                    <asp:CustomValidator ID="cvCurrentPwd2" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtCurrentPwd" ClientValidationFunction="validatePassword_client" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                    <asp:CustomValidator ID="cvCurrentPwdSame" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtCurrentPwd" ClientValidationFunction="validateSamePassword_client" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-label">
                    <asp:Label ID="lblNewPwd" runat="server" meta:resourcekey="lblNewPwd">Kata laluan Baru:</asp:Label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-input">
                    <asp:TextBox ID="txtNewPwd" runat="server" MaxLength="20" CssClass="text_medium" TextMode="Password"></asp:TextBox>
                </div>
                <div class="form-error">
                    <asp:CustomValidator ID="cvNewPwd" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtNewPwd" ClientValidationFunction="validateNewPwd_client" ValidateEmptyText="true" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                <asp:CustomValidator ID="cvNewPwd2" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtNewPwd" ClientValidationFunction="validatePassword_client" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                <asp:CustomValidator ID="cvNewPwdSame" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtNewPwd" ClientValidationFunction="validateSamePassword_client" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-label">
                    <asp:Label ID="lblConfirmPwd" runat="server" meta:resourcekey="lblConfirmPwd">Sahkan kata laluan:</asp:Label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-input">
                    <asp:TextBox ID="txtConfirmPwd" runat="server" MaxLength="20" CssClass="text_medium" TextMode="Password"></asp:TextBox>
                </div>
                <div class="form-error">
                
                <asp:CustomValidator ID="cvConfirmPwd" runat="server" Display="Dynamic" ControlToValidate="txtConfirmPwd" CssClass="errmsg" ClientValidationFunction="validateConfirmPwd_client" ValidateEmptyText="true" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                <asp:CustomValidator ID="cvConfirmPwd2" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtConfirmPwd" ClientValidationFunction="validatePassword_client" ValidationGroup="vgChangePwd"></asp:CustomValidator>
                <asp:CompareValidator ID="cvConfirmPwd3" runat="server" Display="Dynamic" CssClass="errmsg" ControlToCompare="txtNewPwd" ControlToValidate="txtConfirmPwd" ErrorMessage="<%$ Resources:GlobalResource, cvConfirmPasswordNotMatch%>" ClientValidationFunction="validateConfirmPwd_client" ValidationGroup="vgChangePwd"></asp:CompareValidator>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12" style="text-align:center;">
                 <asp:LinkButton ID="lnkbtnChange" runat="server" CssClass="lnkbtn" Text="<%$ Resources:GlobalResource, btnChange %>" Tooltip="<%$ Resources:GlobalResource, btnChange %>" OnClick="lnkbtnChange_OnClick" ValidationGroup="vgChangePwd"></asp:LinkButton>
            </div>
        </div>
</asp:Panel>
