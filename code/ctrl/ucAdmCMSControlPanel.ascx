﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmCMSControlPanel.ascx.cs" Inherits="ctrl_ucAdmCMSControlPanel" %>

<script src="<%= System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jquery.fancybox.js"></script>
<link href="<%= System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>fancybox/jquery.fancybox.css" rel="stylesheet" />
<link href="<%= System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>fancybox/jquery.fancybox.custom.css" rel="stylesheet" />
<script type="text/javascript">
    function validateThumbWidth_Client(source, args) {
        var boolValid = false;
        var numRE = /<%= clsAdmin.CONSTNUMERICRE %>/;

        // if has value
        if (args.Value != '') {
            // if value is numeric
            if (args.Value.match(numRE)) {
                boolValid = true;
            }
            else {
                boolValid = false;
                source.errormessage = "<br />Please enter only numeric.";
                source.innerHTML = "<br />Please enter only numeric.";
            }
        }
        // if no value entered
        else {
            // if height has value
            if ($("#<%= txtThumbnailHeight.ClientID %>").val() != '') {
                boolValid = false;
                source.errormessage = "<br />Please enter thumbnail width.";
                source.innerHTML = "<br />Please enter thumbnail width.";
            }
            else {
                boolValid = true;
            }
        }

        args.IsValid = boolValid;
    }

    function validateThumbHeight_Client(source, args) {
        var boolValid = false;
        var numRE = /<%= clsAdmin.CONSTNUMERICRE %>/;

        // if has value
        if (args.Value != '') {
            // if value is numeric
            if (args.Value.match(numRE)) {
                boolValid = true;
            }
            else {
                boolValid = false;
                source.errormessage = "<br />Please enter only numeric.";
                source.innerHTML = "<br />Please enter only numeric.";
            }
        }
        // if no value entered
        else {
            // if width has value
            if ($("#<%= txtThumbnailWidth.ClientID %>").val() != '') {
                boolValid = false;
                source.errormessage = "<br />Please enter thumbnail height.";
                source.innerHTML = "<br />Please enter thumbnail height.";
            }
            else {
                boolValid = true;
            }
        }

        args.IsValid = boolValid;
    }

        var sortEnable = function($sortable){
            $sortable = $sortable || $(".sortable-list");
            var $items = $sortable.find('li');
            $items.find(".sortable-up,.sortable-down").removeClass("inactive");
            $items.first().find(".sortable-up").addClass("inactive");
            $items.last().find(".sortable-down").addClass("inactive");
        }

        var sortTextUpdate = function($sortable){
            $sortable = $sortable || $(".sortable-list");
            $sortable.find('li').each( function(e) {
                $(this).find(".txtOrder").text($(this).index() + 1);
                $(this).find("[name$='hdnListOrder']").val($(this).index() + 1);
            });
        }
        var sortDeliveryTimeUpdate = function($sortable, $item){
            if($sortable.hasClass("delivery-times")){
                if($item){
                    $item.addClass("deleted");
                }

                var deliverytimes = [];
                $(".delivery-times > li:visible .sortable-item-label").each(function(){
                    deliverytimes.push($(this).html());
                });
                $("#<%= hdnDeliveryTime.ClientID %>").val(deliverytimes.join("|"));
            }
        }
        var sortRecipientUpdate = function($sortable, $item){
            if($sortable.hasClass("recipients")){
                if($item){
                    $item.addClass("deleted");

                    var $input = $item.find("input:hidden")
                    var item = $.parseJSON($input.val());
                    item.deleted = 1;
                    $input.val(JSON.stringify(item));
                }

                var items = [];
                $(".recipients > li").each(function(index){
                    var $input = $(this).find("input:hidden");
                    var item = $.parseJSON($input.val());
                    item.order = (index + 1);
                    items.push(item);
                    $input.val(JSON.stringify(item));
                });
                $("#<%= hdnEnquiryRecipient.ClientID %>").val(JSON.stringify(items));

                
            }
        };
        var FormatPageDetail = function(data){
            var json = $.parseJSON(data);
            var areas = json.areas.sort(function(a,b){
                return a.order > b.order ? 1 : -1;
            });
            var products = json.products.sort(function(a,b){
                return a.order > b.order ? 1 : -1;
            });
            var company = json.company;
            var combine = areas.concat(...products);

            var productFirstImportant = null;
            var areaFirstImportant = null;

            var pageTitle = combine
                .filter(x => x.important == 1)
                .map(x => x.keyword);
            pageTitle.push(company.name);
            pageTitle.push(company.code);

            var pageTitlePrefix = combine.filter(x => x.important == 1)
            .reduce(function(prev, curr){
                if (!prev.some(x => x.group == 'AREA') && curr.group == 'AREA') {
                    areaFirstImportant = curr;
                    prev.push(curr);
                }
                else if (!prev.some(x => x.group == 'PRODUCT') && curr.group == 'PRODUCT') {
                    productFirstImportant = curr;
                    prev.push(curr);
                }
                return prev;
            },[]).map(x => x.keyword);
            pageTitlePrefix.push(company.name);
            pageTitlePrefix.push(company.code);

            var metaKeyword = [];
            metaKeyword.push(company.name);
            metaKeyword.push(company.code);
            var metaKeywordCount = 0;
            if(areas.length > products.length) {
                metaKeywordCount = products.length;
            }else{
                metaKeywordCount = areas.length;
            }
            for(var i=0;i<metaKeywordCount;i++){
                metaKeyword.push("["+products[i].keyword+" "+areas[i].keyword+"]");
            }

            var metaDesc = [];
            metaDesc.push(company.name);
            metaDesc.push("(" + company.code + ")");

            metaDesc = company.name + " (" + company.code + ") provides " + productFirstImportant.keyword + " at " + areaFirstImportant.keyword + ".";
            metaDesc += "We also provide " + products.map(x => x.keyword).join(",") + ".";
            metaDesc += "We cover " + areas.map(x => x.keyword).join(",") + ".";

            //console.log(pageTitle,pageTitlePrefix,metaKeyword,metaDesc);

            $("#<%= txtPrefix.ClientID %>").val(pageTitlePrefix.join(' | '));
            $("#<%= txtPageTitle.ClientID %>").val(pageTitle.join(' , '));
            $("#<%= txtKeyword.ClientID %>").val(metaKeyword.join(' , '));
            $("#<%= txtDesc.ClientID %>").val(metaDesc);
        }
        $.fn.sortableCustom = function() {
            var $this = this;
            $this.sortable({
            placeholder: "ui-state-highlight",
                items: "li:not(.ui-state-disabled)",
                create: function( event, ui ) {
            updateOrder();
            updateArrow();
        },
                update: function(event, ui) {
            updateOrder();
            updateArrow();
        }
        });
            $this.find(".sortable-remove").click(function(e){
            e.preventDefault();
            e.stopPropagation();

            var $item = $(this).parentsUntil("li").parent();
            if($item.find(".hdnDelete").length > 0){
                    $item.find(".hdnDelete").val("1");
            }

                $item.addClass("hidden").fadeOut(200, function(){
                updateOrder();
                updateArrow();
            });

        });

            function updateOrder(){
                $this.find('li:not(.hidden)').each( function(index) {
                    $(this).find(".txtOrder").text(index + 1);
                    $(this).find(".hdnOrder").val(index + 1);
            });
        }
        function updateArrow(){
            var $items = $this.find('li:not(.hidden)');
                $items.find(".sortable-up,.sortable-down").removeClass("inactive");
                $items.first().find(".sortable-up").addClass("inactive");
                $items.last().find(".sortable-down").addClass("inactive");
        }
            return $this;
        };
        

        $(function(){
            $( ".sortable-list" ).sortable({
        placeholder: "ui-state-highlight",
                items: "li:not(.ui-state-disabled)",
                create: function( event, ui ) {
                var $sortable = $(this);
                sortTextUpdate($sortable);
                sortEnable($sortable);
            },
                update: function(event, ui) {
                var $sortable = $(this);
                sortTextUpdate($sortable);
                sortDeliveryTimeUpdate($sortable);
                sortRecipientUpdate($sortable);
                sortEnable($sortable);
            }
        });

            $(".sortable-up").live("click", function(){   
                $el = $(this).parentsUntil("li").parent();
                $el.fadeOut(200, function(){
                    $el.insertBefore($el.prev());
                    $el.fadeIn(200,function(){
                    sortEnable($el.parent());
                });
                sortTextUpdate();
                sortDeliveryTimeUpdate($el.parent());
                sortRecipientUpdate($el.parent());

            });
        });
            $(".sortable-down").live("click", function(){ 
                $el = $(this).parentsUntil("li").parent();
                $el.fadeOut(200, function(){
                    $el.insertAfter($el.next());
                    $el.fadeIn(200,function(){
                    sortEnable($el.parent());
                });
                sortTextUpdate();
                sortDeliveryTimeUpdate($el.parent());
                sortRecipientUpdate($el.parent());
            });
        });
            $(".sortable-remove").live("click", function(){   
                $el = $(this).parentsUntil("li").parent();
            if($el.find(".hdnDelete").length > 0){
                    $el.find(".hdnDelete").val("1");
            }
                $el.fadeOut(200, function(){
                sortRecipientUpdate($el.parent(), $el);
                sortDeliveryTimeUpdate($el.parent(), $el);
                sortEnable();
            });
        });
            $("#<%= chkboxEnquiryRecaptcha.ClientID %>").change(function(){
                if($("#<%= chkboxEnquiryRecaptcha.ClientID %>").is(":checked")){
                    $(".GoogleRecaptcha").show();
                }else{
                    $(".GoogleRecaptcha").hide();
                }
            }).trigger('change');

            $("#<%= chklistDeliveryWeek.ClientID %> input:checkbox:first").change(function(){
                if ($("#<%= chklistDeliveryWeek.ClientID %> input:checkbox:first").is(":checked")) {
                    $("#<%= chklistDeliveryWeek.ClientID %>").find("tr:not(:first-child)").hide();
                }
                else {
                    $("#<%= chklistDeliveryWeek.ClientID %>").find("tr:not(:first-child)").show();
                    
                }
            }).trigger('change');

            $("#<%= chkboxDeliveryTimeActive.ClientID %>").change(function(){
                if ($("#<%= chkboxDeliveryTimeActive.ClientID %>").is(":checked")) {
                    $(".divDeliveryTimeDetail").hide();
                }
                else {
                    $(".divDeliveryTimeDetail").show();
                }
            }).trigger('change');

            
            $("#<%= chkboxEnquiryRecipient.ClientID %>").change(function(){
                if ($("#<%= chkboxEnquiryRecipient.ClientID %>").is(":checked")) {
                    $(".divEnquiryRecipient").show();
                }
                else {
                    $(".divEnquiryRecipient").hide();
                }
            }).trigger('change');

            
            $("#btnAddDeliveryTime").click(function(){
                var text = $("#<%= txtDeliveryTime.ClientID %>").val();
                var items = $("#<%= hdnDeliveryTime.ClientID %>").val().split('|').filter(function(x){
                    return x != '';
                });
                if(text.length > 0) { 
                    items.push(text);
                    $("#<%= hdnDeliveryTime.ClientID %>").val(items.join("|"));  
                }

                // clear text 
                $("#<%= txtDeliveryTime.ClientID %>").val("");

                var $ul = $(".delivery-times");
                $ul.empty()
                items.forEach(function(x, index){
                    var content = "<span class='sortable-item-label'>"+x+"</span>";
                    content += "<span class='sortable-right'>"; 
                    content += "<i class='material-icons sortable-up "+ (index==0 ? "inactive" : "")+"'>arrow_upward</i>";
                    content += "<i class='material-icons sortable-down "+ (index==items.length-1 ? "inactive" : "")+"'>arrow_downward</i>";
                    content += "<span class='text_small txtOrder'>"+(index + 1)+"</span>";
                    content += "<i class='material-icons sortable-remove'>delete_forever</i>";
                    content += "</span>";
                    content += "<div class='clear'></div>";

                    var $li = $('<li/>')
                   .html(content)
                   .addClass('ui-state-default')
                   .appendTo($ul);

                    $ul.sortable('refresh');
                })
            }).trigger('click');

            $("#btnAddRecipient").click(function(){
                var name = $("#<%= txtRecipientName.ClientID %>").val();
                var email = $("#<%= txtRecipientEmail.ClientID %>").val();
                var items = $.parseJSON($("#<%= hdnEnquiryRecipient.ClientID %>").val());

                if(name.length > 0 && email.length > 0) { 
                    var item = {
                        "ID" : 0,
                        "name" : name,
                        "email" : email,
                        "order" : items.length + 1,
                        "deleted":0,
                    };
                    items.push(item);
                    $("#<%= hdnEnquiryRecipient.ClientID %>").val(JSON.stringify(items));  
                }

                // clear text 
                $("#<%= txtRecipientName.ClientID %>").val("");
                $("#<%= txtRecipientEmail.ClientID %>").val("");

                var $ul = $(".recipients");
                $ul.empty()
                items.forEach(function(x, index){
                    var content = "<span class='sortable-item-label'><span class='name'>"+x.name +"</span>|<span class='email'>"+x.email +"</span></span>";
                    content += "<span class='sortable-right'>"; 
                    content += "<i class='material-icons sortable-up "+ (index==0 ? "inactive" : "")+"'>arrow_upward</i>";
                    content += "<i class='material-icons sortable-down "+ (index==items.length-1 ? "inactive" : "")+"'>arrow_downward</i>";
                    content += "<span class='text_small txtOrder'>"+(index + 1)+"</span>";
                    content += "<i class='material-icons sortable-remove'>delete_forever</i>";
                    content += "</span>";
                    content += "<input type='hidden' value='"+JSON.stringify(x)+"'/>";
                    content += "<div class='clear'></div>";

                    var $li = $("<li />")
                   .html(content)
                   .addClass('ui-state-default')
                   .appendTo($ul);
                });
                $ul.sortable('refresh');
            }).trigger('click');

            $(".fancybox").fancybox();
        })
</script>

<asp:Panel ID="pnlAdmCMSControlPanel" runat="server" CssClass="divAdmCMSControlPanel">
    <asp:Panel ID="pnlFormDetails" runat="server" CssClass="divControlPanelContainer">
        <table class="formTbl" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Page Details</td>
                </tr>
            </tbody>
            <tbody>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblPrefix" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtPrefix" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblPageTitle" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtPageTitle" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblKeyword" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtKeyword" runat="server" CssClass="text_fullwidth" MaxLength="1000"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblDesc" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtDesc" runat="server" CssClass="text_fullwidth" MaxLength="1000"></asp:TextBox></td>
                </tr>
            </tbody>
            <asp:Repeater runat="server" ID="rptPageDetail">
                <ItemTemplate>
                    <tbody>
                        <tr>
                            <td colspan="2" class="tdSectionHdr"><asp:Literal runat="server" ID="litTitle"></asp:Literal></td>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <td class="tdLabel"><asp:Label ID="lblPrefix" runat="server" Text="<%# clsConfig.CONSTNAMEPAGETITLEPREFIX %>"></asp:Label>:</td>
                            <td><asp:TextBox ID="txtPrefix" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="tdLabel"><asp:Label ID="lblPageTitle" runat="server" Text="<%# clsConfig.CONSTNAMEPAGETITLE %>"></asp:Label>:</td>
                            <td><asp:TextBox ID="txtPageTitle" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="tdLabel"><asp:Label ID="lblKeyword" runat="server" Text="<%# clsConfig.CONSTNAMEKEYWORD %>"></asp:Label>:</td>
                            <td><asp:TextBox ID="txtKeyword" runat="server" CssClass="text_fullwidth" MaxLength="1000"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="tdLabel"><asp:Label ID="lblDesc" runat="server" Text="<%# clsConfig.CONSTNAMEDESCRIPTION %>"></asp:Label>:</td>
                            <td><asp:TextBox ID="txtDesc" runat="server" CssClass="text_fullwidth" MaxLength="1000"></asp:TextBox></td>
                        </tr>
                    </tbody>
                </ItemTemplate>
            </asp:Repeater>
            <tr>
                <td>SEO Keyword Planing</td>
                <td><a class="btn3 fancybox fancybox.iframe" href="admSeoKeyword.aspx">Generate</a></td>
            </tr>
        </table>
        <table class="formTbl" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Quick Contact Settings</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblQuickContactTel" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtQuickContactTel" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblQuickContactEmail" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtQuickContactEmail" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox><span class="spanFieldDesc"><asp:HyperLink ID="hypQuickContactEmailSameAs" runat="server"></asp:HyperLink></span>
                    <asp:RegularExpressionValidator ID="revQuickContactEmail" runat="server" ErrorMessage="<br/>Please enter valid email." Display="Dynamic" ControlToValidate="txtQuickContactEmail" CssClass="errmsg" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
                </tr>
            </tbody>
        </table>
       <table class="formTbl" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Website Settings</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblEditorStyle" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtEditorStyle" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblDefaultImage" runat="server"></asp:Label>:</td>
                    <td>
                        <div class="inline text_fullwidth">
                        <div style="display:table;width:100%;">
                        <asp:Label ID="lblDefaultImagePath" runat="server" style="  display: table-cell;width: 178px;"></asp:Label>
                        <asp:TextBox ID="txtDefaultImage" runat="server" CssClass="text_big" MaxLength="250" style="display: table-cell; width: 100%;"></asp:TextBox>
                        </div>
                        </div>
                        <span class="spanTooltip" style="margin-top: -20px;">
                            <span class="icon"></span>
                            <span class="msg">
                                <a href="javascript:void();" onclick="if(<% =txtDefaultImage.ClientID %>.value != ''){window.open(<%= lblDefaultImagePath.ClientID %>.innerHTML + <%= txtDefaultImage.ClientID %>.value);}else{alert('Please specify an image.')}" title="view image">view image</a>
                               
                            </span>
                        </span> 
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblCopyright" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtCopyright" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel nobr">Thumbnail Dimension:</td>
                    <td>
                        <asp:TextBox ID="txtThumbnailWidth" runat="server" CssClass="text_small" MaxLength="10"></asp:TextBox>&nbsp;&nbsp;<span>px (width) &nbsp;x </span> 
                        <asp:TextBox ID="txtThumbnailHeight" runat="server" CssClass="text_small" MaxLength="10"></asp:TextBox>&nbsp;&nbsp;<span> px (height)</span> 
                        <asp:CustomValidator ID="cvThumbWidth" runat="server" ControlToValidate="txtThumbnailWidth" ValidateEmptyText="true" ClientValidationFunction="validateThumbWidth_Client" ValidationGroup="vgControlPanel"></asp:CustomValidator>
                        <asp:CustomValidator ID="cvThumbHeight" runat="server" ControlToValidate="txtThumbnailHeight" ValidateEmptyText="true" ClientValidationFunction="validateThumbHeight_Client" ValidationGroup="vgControlPanel"></asp:CustomValidator>
                    </td>
                </tr>
                <% if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTRIGHTCLICK]) == 1){ %>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblRightClickEnable" runat="server"></asp:Label>:</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlRightClickEnable">
                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <% } %>
            </tbody>
        </table>
        <table class="formTbl" cellpadding="0" cellspacing="0" runat="server" id="tblDefaultPageSettings" visible="false">
                <tr>
                    <td colspan="2" class="tdSectionHdr">Default Page Settings</td>
                </tr>
                <tr runat="server" id="trProductPage" visible="false">
                    <td class="tdLabel"><asp:Label ID="lblProductPage" runat="server"></asp:Label>:</td>
                    <td>
                        <asp:DropDownList ID="ddlProductPage" runat="server" CssClass="ddl_big" AutoPostBack="false"></asp:DropDownList>
                    </td>
                </tr>
                <tr runat="server" id="trEventPage" visible="false">
                    <td class="tdLabel"><asp:Label ID="lblEventPage" runat="server"></asp:Label>:</td>
                    <td>
                        <asp:DropDownList ID="ddlEventPage" runat="server" CssClass="ddl_big" AutoPostBack="false"></asp:DropDownList>
                    </td>
                </tr>
        </table>
        <table class="formTbl" cellpadding="0" cellspacing="0" id="trProductSettings" runat="server" visible="false">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Product Settings</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblProdDimension" runat="server"></asp:Label>:</td>
                    <td class="tdMax"><asp:DropDownList ID="ddlProdDimension" runat="server" CssClass="ddl" AutoPostBack="false" ></asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblProdWeight" runat="server"></asp:Label>:</td>
                    <td class="tdMax"><asp:DropDownList ID="ddlProdWeight" runat="server" CssClass="ddl" AutoPostBack="false" ></asp:DropDownList></td>
                </tr>
                <tr id="trInventory" runat="server">
                    <td class="tdLabel nobr"><asp:Label ID="lblProdInventory" runat="server"></asp:Label>:</td>
                    <td class="tdLabel"><asp:CheckBox ID="chkboxInventory" runat="server" /></td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblProdDName" runat="server"></asp:Label>:</td>
                    <td class="tdLabel"><asp:CheckBox ID="chkboxProdDName" runat="server" /></td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblProdCode" runat="server"></asp:Label>:</td>
                    <td class="tdLabel"><asp:CheckBox ID="chkboxProdCode" runat="server" /></td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblProdSnapshot" runat="server"></asp:Label>:</td>
                    <td class="tdLabel"><asp:CheckBox ID="chkboxProdSnapshot" runat="server" /></td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblProdPrice" runat="server"></asp:Label>:</td>
                    <td class="tdLabel"><asp:CheckBox ID="chkboxProdPrice" runat="server" /></td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblMaxQtyPerProduct" runat="server"></asp:Label>:</td>
                    <td class="">
                        <asp:TextBox ID="txtMaxQtyPerProduct" runat="server" CssClass="text_medium"></asp:TextBox>
                        <span class="spanTooltip">
                            <span class="icon"></span>
                            <span class="msg">
                                Set to zero for no limit
                            </span>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblMaxQtyPerPromoProduct" runat="server"></asp:Label>:</td>
                    <td class="">
                        <asp:TextBox ID="txtMaxQtyPerPromoProduct" runat="server" CssClass="text_medium"></asp:TextBox>
                        <span class="spanTooltip">
                            <span class="icon"></span>
                            <span class="msg">
                                Set to zero for no limit
                            </span>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel nobr">Sorting Option:</td>
                    <td>
                        <asp:Repeater runat="server" ID="rptProdSortOption">
                            <HeaderTemplate><ul class="sortable-list"></HeaderTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                            <ItemTemplate>
                                <li class="ui-state-default"><%--<%# Container.ItemIndex == 0 ? "ui-state-disabled" : "" %>--%>
                                    <span class="sortable-item-label">
                                        <asp:Label runat="server" ID="lblName" Text='<%# DataBinder.Eval(Container.DataItem,"LIST_NAME") %>'></asp:Label>
                                    </span>
                                    <span class="sortable-right">
                                        <i class="material-icons sortable-up">arrow_upward</i>
                                        <i class="material-icons sortable-down">arrow_downward</i>
                                        <asp:Label runat="server" ID="txtOrder" CssClass="text_small txtOrder" Text='<%# DataBinder.Eval(Container.DataItem,"LIST_ORDER") %>'></asp:Label>
                                        <asp:CheckBox runat="server" ID="chkboxActive" ToolTip="Active" Checked='<%# Convert.ToString(DataBinder.Eval(Container.DataItem,"LIST_ACTIVE")) == "1" ? true : false %>' />
                                        <asp:HiddenField ID="hdnListOrder" runat="server" />
                                    </span>
                                      <div class="clear"></div>
                                  </li>
                            </ItemTemplate>
                        </asp:Repeater>
 
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel nobr">Show Random Related Product:</td>
                    <td class="tdLabel">
                        <asp:CheckBox ID="chkboxRandomRelatedProdEnabled" runat="server" />
                        <span class="spanTooltip" style="margin-top: -5px;">
                            <span class="icon"></span>
                            <span class="msg">
                                When checked, it will show random related <br />
                                products when the releated product is empty.
                            </span>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel nobr">Display of Product Details:</td>
                    <td class="tdMax">
                        <asp:DropDownList ID="ddlProdDetailDisplayType" runat="server" CssClass="ddl" AutoPostBack="false" ></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel">Price Range:</td>
                    <td class="tdLabel">
                        <div><asp:CheckBox ID="chkboxPriceRange" runat="server"/></div>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="formTbl" cellpadding="0" cellspacing="0" id="trShippingType" runat="server" visible="false">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Shipping</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblShippingType" runat="server"></asp:Label>:</td>
                    <td class="tdMax">
                        <asp:UpdatePanel ID="upnlShippingType" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlShippingType" runat="server" CssClass="ddl" AutoPostBack="true" OnSelectedIndexChanged="ddlShippingType_OnSelectedChanged"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvShippingType" runat="server" ErrorMessage="<br />Please select Shipping Type." ControlToValidate="ddlShippingType" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgControlPanel"></asp:RequiredFieldValidator>  
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                 <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblVolumetricWeight" runat="server"></asp:Label>:</td>
                    <td class="tdCheckbox"><asp:CheckBox ID="chkboxVolumetricWeight" runat="server" /></td>
                </tr>
                <tr runat="server" visible="false" id="trDeliveryWeek">
                    <td class="tdLabel nobr">Delivery Week:</td>
                    <td class="tdMax">
                        <asp:CheckBoxList runat="server" ID="chklistDeliveryWeek" CssClass="chklist first"></asp:CheckBoxList>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="trDeliveryTime">
                    <td class="tdLabel nobr">Delivery Time:</td>
                    <td class="tdCheckbox">
                        <div class="">
                            <asp:CheckBox runat="server" ID="chkboxDeliveryTimeActive" Text="Any Time" ToolTip="Any Time" />
                        </div>
                        <div class="divDeliveryTimeDetail">
                            <div style="margin-bottom:10px;">
                                <asp:TextBox CssClass="text" runat="server" ID="txtDeliveryTime"></asp:TextBox>
                                <a class="btn3" id="btnAddDeliveryTime" style="height: 36px;line-height: 36px;">Add</a>
                            </div>
                            <div>
                                <ul class="delivery-times sortable-list">

                                </ul>
                                <asp:HiddenField runat="server" ID="hdnDeliveryTime" />
                            </div>
                        </div>
                        

                    </td>
                </tr>
            </tbody>
        </table>
        <table class="formTbl" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Enquiry Settings</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblEnquiryTitle" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtEnquiryTitle" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblEnquiryDesc" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtEnquiryDesc" runat="server" CssClass="txtEnquiryDesc" MaxLength="250" TextMode="MultiLine"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblEnquiryRecaptcha" runat="server"></asp:Label> (Google):</td>
                    <td class="tdLabel"><asp:CheckBox ID="chkboxEnquiryRecaptcha" runat="server"/></td>
                </tr>
                <tr class="GoogleRecaptcha">
                    <td class="tdLabel"><asp:Label ID="lblRecaptchaSiteKey" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtRecaptchaSiteKey" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                </tr>
                <tr class="GoogleRecaptcha">
                    <td class="tdLabel"><asp:Label ID="lblRecaptchaSecretKey" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtRecaptchaSecretKey" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblEnquiryRecipient" runat="server"></asp:Label>:</td>
                    <td class="tdLabel">
                        <div><asp:CheckBox ID="chkboxEnquiryRecipient" runat="server"/></div>
                        <div class="divEnquiryRecipient">
                            <div style="margin-bottom:10px;">
                                <asp:TextBox CssClass="text" runat="server" ID="txtRecipientName" placeholder="Recipient Name"></asp:TextBox>
                                <asp:TextBox CssClass="text" runat="server" ID="txtRecipientEmail" placeholder="Recipient Email"></asp:TextBox>
                                <a class="btn3" id="btnAddRecipient" style="height: 36px;line-height: 36px;">Add</a>
                            </div>
                            <div>
                                <ul class="recipients sortable-list" style="width:500px;">

                                </ul>
                                <asp:HiddenField runat="server" ID="hdnEnquiryRecipient" />
                            </div>
                        </div>
                    </td>
                </tr>
                <tr runat="server" id="trEnquiryField">
                    <td class="tdLabel">Enquiry Field Customize:</td>
                    <td><a class="btn3" href="admEnquiryField.aspx?iframe&width=600" rel="lightbox">Generate</a></td>
                </tr>
            </tbody>
        </table>
        <table class="formTbl" cellpadding="0" cellspacing="0" runat="server" id="tblEventSetting" visible="false">
                <tr>
                    <td colspan="2" class="tdSectionHdr">Event Settings</td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblEventSortOption" runat="server"></asp:Label>:</td>
                    <td>
                        <asp:Repeater runat="server" ID="rptEventSortOption">
                            <HeaderTemplate><ul class="sortable-list"></HeaderTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                            <ItemTemplate>
                                <li class="ui-state-default"><%--<%# Container.ItemIndex == 0 ? "ui-state-disabled" : "" %>--%>
                                    <span class="sortable-item-label">
                                        <asp:Label runat="server" ID="lblName" Text='<%# DataBinder.Eval(Container.DataItem,"LIST_NAME") %>'></asp:Label>
                                    </span>
                                    <span class="sortable-right">
                                        <i class="material-icons sortable-up">arrow_upward</i>
                                        <i class="material-icons sortable-down">arrow_downward</i>
                                        <asp:Label runat="server" ID="txtOrder" CssClass="text_small txtOrder" Text='<%# DataBinder.Eval(Container.DataItem,"LIST_ORDER") %>'></asp:Label>
                                        <asp:CheckBox runat="server" ID="chkboxActive" ToolTip="Active" Checked='<%# Convert.ToString(DataBinder.Eval(Container.DataItem,"LIST_ACTIVE")) == "1" ? true : false %>' />
                                        <asp:HiddenField ID="hdnListOrder" runat="server" />
                                    </span>
                                      <div class="clear"></div>
                                  </li>
                            </ItemTemplate>
                        </asp:Repeater>
 
                    </td>
                </tr>
        </table>
        <table class="formTbl" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Other Settings</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblListingSize" runat="server"></asp:Label>:</td>
                    <td class="tdMax"><asp:TextBox ID="txtListingSize" runat="server" CssClass="text_medium"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblSessionTimeout" runat="server"></asp:Label>:</td>
                    <td class="tdMax"><asp:TextBox ID="txtSessionTimeout" runat="server" CssClass="text_medium"></asp:TextBox></td>
                </tr>
                <tr id="trFriendlyURL" runat="server">
                    <td class="tdLabel nobr"><asp:Label ID="lblFriendlyURL" runat="server"></asp:Label>:</td>
                    <td class="tdLabel"><asp:CheckBox ID="chkboxFriendlyURL" runat="server" /></td>
                </tr>
                <tr id="trMobileView" runat="server">
                    <td class="tdLabel nobr"><asp:Label ID="lblMobileView" runat="server"></asp:Label>:</td>
                    <td class="tdLabel"><asp:CheckBox ID="chkboxMobileView" runat="server" /></td>
                </tr>
                <tr id="trDeliveryMsg" runat="server">
                    <td class="tdLabel nobr"><asp:Label ID="lblDeliveryMsg" runat="server"></asp:Label>:</td>
                    <td class="tdLabel"><asp:CheckBox ID="chkboxDeliveryMsg" runat="server" /></td>
                </tr>
                <tfoot>
                <tr>
                    <td colspan="2">

                    </td>
                </tr>
                </tfoot>
            </tbody>        
        </table>
        <asp:UpdatePanel runat="server" ID="upnlShipOption">
            <ContentTemplate>
        <table class="formTbl" cellpadding="0" cellspacing="0" id="trShippingComp" runat="server" visible="false">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Shipping Company</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblShipComp" runat="server" Text="Company"></asp:Label>:</td>
                    <td class="tdMax"><asp:TextBox ID="txtShipComp" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblShipCompOrder" runat="server" Text="Order"></asp:Label>:</td>
                    <td class="tdMax"><asp:TextBox ID="txtShipCompOrder" runat="server" CssClass="text_medium"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"></td>
                    <td class="tdMax"><asp:LinkButton ID="lnkbtnAddShipComp" runat="server" Text="Add" ToolTip="Add" CssClass="btn" OnClick="lnkbtnAddShipComp_Click"></asp:LinkButton></td>
                </tr>
                <tr id="trShipCompList" runat="server">
                    <td colspan="2">
                        <asp:Repeater ID="rptShipComp" runat="server" OnItemDataBound="rptShipComp_ItemDataBound" OnItemCommand="rptShipComp_ItemCommand">
                            <HeaderTemplate><table cellpadding="0" cellspacing="0"></HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="tdShippingLeftList"><asp:Label ID="lblShipCompName" runat="server"></asp:Label></td>
                                    <td class="tdShippingLeftList"><asp:Label ID="lblShipCompOrder" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="lnkbtnShipCompEdit" runat="server" Text="Edit" ToolTip="Edit" CommandName="cmdEdit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CONF_ID") %>'></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnShipCompDelete" runat="server" Text="Delete" ToolTip="Delete" CommandName="cmdDelete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CONF_ID") %>'></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate></table></FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="formTbl" cellpadding="0" cellspacing="0" id="trShipMethod" runat="server" visible="false">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Shipping Method</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblShipMethod" runat="server" Text="Method"></asp:Label>:</td>
                    <td class="tdMax"><asp:TextBox ID="txtShipMethod" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblShipMethodOrder" runat="server" Text="Order"></asp:Label></td>
                    <td class="tdMax"><asp:TextBox ID="txtShipMethodOrder" runat="server" CssClass="text_medium"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"></td>
                    <td class="tdMax"><asp:LinkButton ID="lnkbtnAddShipMethod" runat="server" Text="Add" ToolTip="Add" CssClass="btn" OnClick="lnkbtnAddShipMethod_Click"></asp:LinkButton></td>
                </tr>
                <tr id="trShipMethodList" runat="server">
                    <td colspan="2">
                        <asp:Repeater ID="rptShipMethod" runat="server" OnItemDataBound="rptShipMethod_ItemDataBound" OnItemCommand="rptShipMethod_ItemCommand">
                            <HeaderTemplate><table cellpadding="0" cellspacing="0"></HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="tdShippingLeftList"><asp:Label ID="lblShipMethodName" runat="server"></asp:Label></td>
                                    <td class="tdShippingLeftList"><asp:Label ID="lblShipMethodOrder" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="lnkbtnShipMethodEdit" runat="server" Text="Edit" ToolTip="Edit" CommandName="cmdEdit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CONF_ID") %>'></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnShipMethodDelete" runat="server" Text="Delete" ToolTip="Delete" CommandName="cmdDelete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CONF_ID") %>'></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate></table></FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </tbody>
        </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table class="formTbl" cellpadding="0" cellspacing="0" id="trCurrencyConversion" runat="server" visible="false">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Currency Conversion</td>
                </tr>
            </thead>
            <tbody>
                <%--<tr>
                    <td colspan="2">
                        <asp:Repeater ID="rptCurrency" runat="server" OnItemDataBound="rptCurrency_ItemDataBound">
                            <HeaderTemplate><table cellpadding="0" cellspacing="0" border="0" class="formTbl"></table></HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="tdLabel nobr"><asp:Label ID="lblDefaultCurrency" runat="server"></asp:Label><asp:HiddenField ID="hdnCurrId" runat="server" /></td>
                                    <td class="tdMax"><span style="padding-right:5px;"><asp:Label ID="lblCurrRate" runat="server"></asp:Label></span><asp:TextBox ID="txtCurrRate" runat="server" CssClass="text_medium"></asp:TextBox></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>--%>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblCurrName" runat="server" Text="Currency Name"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtCurrName" runat="server" CssClass="text_big compulsory"></asp:TextBox><span class="attention_compulsory"> *</span>
                    <asp:HiddenField ID="hdnCurr" runat="server" />
                    <asp:RequiredFieldValidator ID="rfvCurrName" runat="server" ErrorMessage="<br />Please enter Currency Name." ControlToValidate="txtCurrName" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgCurr"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblCurrValue" runat="server" Text="Currency Value"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtCurrValue" runat="server" CssClass="text_big compulsory"></asp:TextBox><span class="attention_compulsory"> *</span>
                    <asp:RequiredFieldValidator ID="rfvCurrValue" runat="server" ErrorMessage="<br />Please enter Currency Value." ControlToValidate="txtCurrValue" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgCurr"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblCurrConversionRate" runat="server" Text="ConversionRate"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtCurrConversionRate" runat="server" CssClass="text_big compulsory"></asp:TextBox><span class="attention_compulsory"> *</span>
                    <asp:RequiredFieldValidator ID="rfvCurrConversionRate" runat="server" ErrorMessage="<br />Please enter Convertion Rate." ControlToValidate="txtCurrConversionRate" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgCurr"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblCurrFlag" runat="server" Text="Flag"></asp:Label>:</td>
                    <td>
                        <asp:Panel ID="pnlCurrFlagFileUpload" runat="server" CssClass="divFileUpload">
                            <asp:Panel ID="pnlCurrFlagFileUploadAction" runat="server" CssClass="divFileUploadAction">
                                <asp:FileUpload ID="fileCurrFlagImage" runat="server" />
                                <span class="spanTooltip">
                                    <span class="icon"></span>
                                    <span class="msg">
                                        <asp:Literal ID="litUploadRuleCurr" runat="server"></asp:Literal>
                                    </span>
                                </span>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:HiddenField ID="hdnCurrFlagImage" runat="server" />
                        <asp:HiddenField ID="hdnCurrFlagImageRef" runat="server" />
                        <asp:CustomValidator ID="cvCurrFlagImage" runat="server" ControlToValidate="fileCurrFlagImage" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateFlagImage_server" OnPreRender="cvCurrFlagImage_PreRender"></asp:CustomValidator>
                        <asp:Panel ID="pnlFlagImage" runat="server" Visible="false">
                            <br /><asp:LinkButton ID="lnkbtnDeleteImageCurrFlag" runat="server" Text="Delete" ToolTip="Delete" CausesValidation="false" OnClick="lnkbtnDeleteImageCurrFlag_Click"></asp:LinkButton>
                            <br /><asp:Image ID="imgCurrFlagImage" runat="server" />
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblCurrOrder" runat="server" Text="Order"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtCurrOrder" runat="server" CssClass="text_big compulsory"></asp:TextBox><span class="attention_compulsory"> *</span>
                    <asp:RequiredFieldValidator ID="rfvCurrOrder" runat="server" ErrorMessage="<br />Please enter Currency Order." ControlToValidate="txtCurrOrder" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgCurr"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblCurrActive" runat="server" Text="Active"></asp:Label>:</td>
                    <td><asp:CheckBox ID="chkboxCurrActive" runat="server" Checked="true" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="tdbtn"><asp:LinkButton ID="lnkbtnSaveCurrency" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSaveCurrency_Click" ValidationGroup="vgCurr"></asp:LinkButton></td>
                </tr> 
                <tr>
                    <td colspan="2">
                        <div class="divCurrListing">
                            <asp:Repeater ID="rptCurrency" runat="server" OnItemDataBound="rptCurrency_ItemDataBound" OnItemCommand="rptCurrency_ItemCommand">
                                <HeaderTemplate>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataTbl">
                                        <tr>
 
                                            <th class="td_headerItemCart">Currency Name</th>
                                            <th class="td_headerPriceCart">Currency Value</th>
                                            <th class="td_headerQtyCart">Convertion Rate</th>
                                            <th class="td_headerTotalCart">Flag</th>
                                            <th class="td_headerTotalCart alignCenter">Order</th>
                                            <th class="td_headerTotalCart alignCenter">Active</th>
                                            <th class="td_headerTotalCart alignCenter">Action</th>
                                                   
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="trCurrency" runat="server">
                                        <td class="tdLabelNor5"><asp:Label ID="lblCurrName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CUR_NAME") %>'></asp:Label></td>
                                        <td class="tdLabelNorCur"><asp:Label ID="lblCurrValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CUR_VALUE2") %>'></asp:Label></td>
                                        <td class="tdLabelMedium"><asp:Label ID="lblCurConvrate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CUR_CONVRATE") %>'></asp:Label></td>
                                        <td class="tdLabelCurFlag"><asp:Label ID="lblCurFlag" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CUR_FLAG") %>'></asp:Label></td>
                                        <td class="tdLabelSmall alignCenter"><asp:Label ID="lblCurOrder" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CUR_ORDER") %>'></asp:Label></td>
                                        <td class="tdLabelActive alignCenter"><asp:Label ID="lblCurActive" runat="server"></asp:Label></td>
                                        <td class="tdAct">
                                            <asp:LinkButton ID="lnkbtnUp" CssClass="lnkbtn lnkbtnNoIcon" runat="server" Text="Move Up" ToolTip="Move Up" CommandName="cmdUp" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CUR_ID") + "|" + DataBinder.Eval(Container.DataItem, "CUR_ORDER") %>'></asp:LinkButton>   
                                            <span class="spanSplitter">|</span>
                                            <asp:LinkButton ID="lnkbtnDown" CssClass="lnkbtn lnkbtnNoIcon" runat="server" Text="Move Down" ToolTip="Move Down" CommandName="cmdDown" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CUR_ID") + "|" + DataBinder.Eval(Container.DataItem, "CUR_ORDER") %>'></asp:LinkButton>
                                            <span class="spanSplitter">|</span>
                                            <asp:HyperLink ID="hypEdit" CssClass="lnkbtn lnkbtnEdit" runat="server"></asp:HyperLink>
                                            <span class="spanSplitter">|</span>
                                            <asp:LinkButton ID="lnkbtnDelete" CssClass="lnkbtn lnkbtnDelete" runat="server" CommandName="cmdDelete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CUR_ID") %>'></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </td>
                </tr>                    
            </tbody>
        </table>
        <table class="formTbl" cellpadding="0" cellspacing="0" id="trInvoiceSettings" runat="server" visible="false">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Invoice Settings</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblInvoiceLogo" runat="server"></asp:Label>:</td>
                    <td>
                        <asp:Panel ID="pnlFileUpload" runat="server" CssClass="divFileUpload">
                            <asp:Panel ID="pnlFileUploadAction" runat="server" CssClass="divFileUploadAction">
                                <asp:FileUpload ID="fileInvoiceImage" runat="server" />
                            </asp:Panel>
                            <div class="divFileUploadRule">
                                <asp:HyperLink ID="hypInfo" runat="server" ToolTip="info" CssClass="hypUploadInfo" onmouseover="toggleDiv('divFileUploadInfo', true)" onmouseout="toggleDiv('divFileUploadInfo', false)"><asp:Image ID="imgInfo" runat="server" ToolTip="info" AlternateText="info" /></asp:HyperLink>
                                <div id="divFileUploadInfo" class="divFileUploadInfo"><span class="fieldDesc"><asp:Literal ID="litUploadRule" runat="server"></asp:Literal></span></div>
                            </div>
                        </asp:Panel>
                        <asp:HiddenField ID="hdnInvoiceImage" runat="server" />
                        <asp:HiddenField ID="hdnInvoiceImageRef" runat="server" />
                        <asp:CustomValidator ID="cvInvoiceImage" runat="server" ControlToValidate="fileInvoiceImage" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateInvoiceImage_server" OnPreRender="cvInvoiceImage_PreRender"></asp:CustomValidator>
                        <asp:Panel ID="pnlInvoiceImage" runat="server" Visible="false">
                            <br /><asp:LinkButton ID="lnkbtnDeleteImage" runat="server" Text="Delete" ToolTip="Delete" CausesValidation="false" OnClick="lnkbtnDeleteImage_Click"></asp:LinkButton>
                            <br /><asp:Image ID="imgInvoiceImage" runat="server" />
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblCompanyName" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtCompanyName" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblROC" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtROC" runat="server" CssClass="text"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblCompanyAddress" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtCompanyAddress" runat="server" CssClass="text_fullwidth"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblCompanyTel" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtCompanyTel" runat="server" CssClass="text"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblCompanyEmail" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtCompanyEmail" runat="server" CssClass="text"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblCompanyWebsite" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtCompanyWebsite" runat="server" CssClass="text"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblInvoiceRemarks" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtInvoiceRemarks" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblInvoiceRemarksPdf" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtInvoiceRemarksPdf" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblInvoiceSignature" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtInvoiceSig" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblInvoiceSignaturePdf" runat="server"></asp:Label>:</td>
                    <td><asp:TextBox ID="txtInvoiceSigPdf" runat="server" CssClass="text_fullwidth" TextMode="MultiLine" Rows="5"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblPaymentType" runat="server"></asp:Label>:</td>
                    <td><asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="ddl"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblShippingCom" runat="server"></asp:Label>:</td>
                    <td><asp:DropDownList ID="ddlShippingCompany" runat="server" CssClass="ddl"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblInvoicePayment" runat="server"></asp:Label>:</td>
                    <td><asp:CheckBox ID="chkboxPayment" runat="server" Checked="true" /></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblInvoiceProcessing" runat="server"></asp:Label>:</td>
                    <td><asp:CheckBox ID="chkboxProcessing" runat="server" Checked="true" /></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblInvoiceDelivery" runat="server"></asp:Label>:</td>
                    <td><asp:CheckBox ID="chkboxDelivery" runat="server" Checked="true" /></td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>
    
</asp:Panel>
<asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
    <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="vgControlPanel"></asp:LinkButton>
</asp:Panel>
