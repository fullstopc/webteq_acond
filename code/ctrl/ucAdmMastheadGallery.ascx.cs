﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;

public partial class ctrl_ucAdmMastheadGallery : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected int _mastId = 0;
    protected string _pageListingURL = "";

    protected string mastName;
    protected string mastShortDesc;
    protected string mastImage;
    protected string mastThumb;
    protected string mastTagline1;
    protected string mastTagline2;
    protected int mastOrder;
    protected int mastClickable;
    protected string mastUrl;
    protected string mastTarget;
    protected int mastActive;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int mastId
    {
        get
        {
            if (ViewState["MASTID"] == null)
            {
                return _mastId;
            }
            else
            {
                return int.Parse(ViewState["MASTID"].ToString());
            }
        }
        set
        {
            ViewState["MASTID"] = value;
        }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }

    protected void validateProdImage_server(object source, ServerValidateEventArgs args)
    {
        if (fileProdImage.HasFile)
        {
            int intFileMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTFILESETTING] != null && Session[clsAdmin.CONSTPROJECTFILESETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTFILESETTING]) > 0)
                {
                    intFileMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTFILESETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intFileMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            //if (fileProdImage.PostedFile.ContentLength > clsAdmin.CONSTADMDOCALLOWEDMAXLENGTH)
            //{
            //    cvProdImage.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMDOCALLOWEDMAXLENGTH, "kb") + ".";
            //    args.IsValid = false;
            //}

            if (fileProdImage.PostedFile.ContentLength > intFileMaxSize)
            {
                cvProdImage.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(intFileMaxSize, "kb") + ".";
                args.IsValid = false;
            }
            else
            {
                Match matchImage = Regex.Match(fileProdImage.FileName, clsAdmin.CONSTADMIMGALLOWEDEXT, RegexOptions.IgnoreCase);
                if (matchImage.Success)
                {
                    args.IsValid = true;
                }
                else
                {
                    cvProdImage.ErrorMessage = "<br />Please enter valid image. (" + clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + ").";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void validateThumbImage_server(object source, ServerValidateEventArgs args)
    {
        if (fileThumbImage.HasFile)
        {
            int intFileMaxSize = 0;

            if (Session[clsAdmin.CONSTPROJECTFILESETTING] != null && Session[clsAdmin.CONSTPROJECTFILESETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTFILESETTING]) > 0)
                {
                    intFileMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTFILESETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intFileMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }

            //if (fileThumbImage.PostedFile.ContentLength > clsAdmin.CONSTADMDOCALLOWEDMAXLENGTH)
            //{
            //    cvThumbImage.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMDOCALLOWEDMAXLENGTH, "kb") + ".";
            //    args.IsValid = false;
            //}

            if (fileThumbImage.PostedFile.ContentLength > intFileMaxSize)
            {
                cvThumbImage.ErrorMessage = "<br />File size exceeded " + clsMis.formatAllowedFileSize(intFileMaxSize, "kb") + ".";
                args.IsValid = false;
            }
            else
            {
                Match matchImage = Regex.Match(fileThumbImage.FileName, clsAdmin.CONSTADMIMGALLOWEDEXT, RegexOptions.IgnoreCase);
                if (matchImage.Success)
                {
                    args.IsValid = true;
                }
                else
                {
                    cvThumbImage.ErrorMessage = "<br />Please enter valid thumb image. (" + clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + ").";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cvProdImage_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("ProdImage")))
        {
            string strJS = null;
            strJS = "<script type = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + fileProdImage.ClientID + "', document.all['" + cvProdImage.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "ProdImage", strJS, false);
        }
    }

    protected void cvThumbImage_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("ThumbImage")))
        {
            string strJS = null;
            strJS = "<script type = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + fileThumbImage.ClientID + "', document.all['" + cvThumbImage.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "ThumbImage", strJS, false);
        }
    }

    protected void lnkbtnDeleteImage_Click(object sender, EventArgs e)
    {
        pnlProdImage.Visible = false;
        hdnProdImage.Value = "";
        pnlThumbImage.Visible = false;
        hdnThumbImage.Value = "";

    }

    protected void lnkbtnDeleteImage2_Click(object sender, EventArgs e)
    {
        pnlThumbImage.Visible = false;
        hdnThumbImage.Value = "";
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER + "/";
        string strDeletedMastName = "";

        clsMis.performDeleteMasthead(mastId, uploadPath, ref strDeletedMastName);

        Session["DELETEDMASTNAME"] = strDeletedMastName;
        Response.Redirect(currentPageName);
    }

    protected void chkboxClickable_CheckedChanged(object sender, EventArgs e)
    {
        trClickable.Visible = chkboxClickable.Checked;
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            clsMasthead mast = new clsMasthead();
            int intRecordAffected = 0;

            mastName = txtMastName.Text.Trim();
            mastShortDesc = txtShortDesc.Text.Trim();
            mastTagline1 = txtMastTagline1.Text.Trim();
            mastTagline2 = txtMastTagline2.Text.Trim();
            mastOrder = !string.IsNullOrEmpty(txtOrder.Text.Trim()) ? Convert.ToInt16(txtOrder.Text.Trim()) : 0;
            mastClickable = chkboxClickable.Checked ? 1 : 0;
            mastUrl = txtURL.Text.Trim();
            mastActive = chkboxActive.Checked ? 1 : 0;
            int intGrpId = !string.IsNullOrEmpty(ddlGroup.SelectedValue) ? int.Parse(ddlGroup.SelectedValue) : 0;

            if (!string.IsNullOrEmpty(ddlSlideTarget.SelectedValue)) { mastTarget = ddlSlideTarget.SelectedValue; }
            else { mastTarget = ""; }

            int intTotal = mast.getTotalRecord(intGrpId, 0);
            Boolean boolLimitReach = clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITSLIDESHOWIMAGE, intTotal);

            if (!boolLimitReach)
            {
                if (fileProdImage.HasFile)
                {
                    try
                    {
                        HttpPostedFile postedFile = fileProdImage.PostedFile;
                        string fileName = Path.GetFileName(postedFile.FileName);
                        string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER.ToString() + "/";
                        clsMis.createFolder(uploadPath);

                        string newFilename = clsMis.GetUniqueFilename(fileName, uploadPath);
                        postedFile.SaveAs(uploadPath + newFilename);
                        mastImage = newFilename;
                    }
                    catch (Exception ex)
                    {
                        clsLog.logErroMsg(ex.Message.ToString());
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />" + ex.Message.ToString() + "</div>";

                        throw ex;
                    }
                }
                else
                {
                    if (mode == 1)
                    {
                        mastImage = "";
                    }
                    else
                    {
                        mastImage = hdnProdImage.Value;
                    }
                }

                if (fileThumbImage.HasFile)
                {
                    try
                    {
                        HttpPostedFile postedFile2 = fileThumbImage.PostedFile;
                        string fileName2 = Path.GetFileName(postedFile2.FileName);
                        string uploadPath2 = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER.ToString() + "/";
                        clsMis.createFolder(uploadPath2);

                        string newFilename2 = clsMis.GetUniqueFilename(fileName2, uploadPath2);
                        postedFile2.SaveAs(uploadPath2 + newFilename2);
                        mastThumb = newFilename2;
                    }
                    catch (Exception ex)
                    {
                        clsLog.logErroMsg(ex.Message.ToString());
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />" + ex.Message.ToString() + "</div>";

                        throw ex;
                    }
                }
                else
                {
                    if (mode == 1)
                    {
                        mastThumb = "";
                    }
                    else
                    {
                        mastThumb = hdnThumbImage.Value;
                    }
                }

                switch (mode)
                {
                    case 1: //Add Product
                        intRecordAffected = mast.addMasthead(mastName, mastImage, mastActive, mastThumb, mastShortDesc, mastTagline1, mastTagline2, intGrpId, mastOrder, mastClickable, mastUrl, mastTarget);

                        if (intRecordAffected == 1)
                        {

                            Session["NEWMASTID"] = mast.mastId;
                            Response.Redirect(currentPageName + "?id=" + mast.mastId);
                        }
                        else
                        {
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new masthead slide show. Please try again.</div>";
                            Response.Redirect(currentPageName);
                        }
                        break;

                    case 2:
                        Boolean bEdited = false;
                        Boolean bSuccess = true;

                        if (!mast.isExactSameMastheadSet(mastId, mastName, mastImage, mastActive, mastThumb, mastShortDesc, mastTagline1, mastTagline2, intGrpId, mastOrder, mastClickable, mastUrl, mastTarget))
                        {
                            intRecordAffected = mast.updateMastheadById(mastId, mastName, mastImage, mastActive, mastThumb, mastShortDesc, mastTagline1, mastTagline2, intGrpId, mastOrder, mastClickable, mastUrl, mastTarget);

                            if (intRecordAffected == 1)
                            {
                                string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER.ToString() + "/";
                                if (hdnProdImageRef.Value != mastImage && hdnProdImageRef.Value != "")
                                {
                                    if (File.Exists(uploadPath + hdnProdImageRef.Value)) { File.Delete(uploadPath + hdnProdImageRef.Value); }
                                }

                                if (hdnThumbImageRef.Value != mastThumb && hdnThumbImageRef.Value != "")
                                {
                                    if (File.Exists(uploadPath + hdnThumbImageRef.Value)) { File.Delete(uploadPath + hdnThumbImageRef.Value); }
                                }
                                bEdited = true;
                            }

                            else
                            {
                                bSuccess = false;
                            }
                        }

                        if (bEdited)
                        {
                            Session["EDITMASTID"] = mast.mastId;
                        }
                        else
                        {
                            if (!bSuccess)
                            {
                                mast.extractMastheadById(mastId, 0);
                                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit masthead (" + mast.mastName + "). Please try again.</div>";
                            }
                            else
                            {
                                Session["EDITMASTID"] = mastId;
                                Session["NOCHANGE"] = 1;
                            }
                        }

                        Response.Redirect(Request.Url.ToString());
                        break;
                    default:
                        break;
                }
            }
            else
            {
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Limit reached.</div>";
                Response.Redirect(Request.Url.ToString());
            }
        }
    }
    #endregion

    #region "Methods"
    public void fill(int intMode, int intMastId)
    {
        registerCKEditorScript();

        if (!IsPostBack)
        {
            mastId = intMastId;
            mode = intMode;
            setPageProperties();
        }
    }

    protected void registerCKEditorScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR")))
        {
            int intFullPath = Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFULLPATH]) == 1 ? clsAdmin.CONSTFULLPATHACTIVE : clsAdmin.CONSTFULLPATHDEFAULT;
            string strJS = null;
            strJS = "<script type=\"text/javascript\">" +
                    "CKEDITOR.replace('" + txtMastTagline1.ClientID + "'," +
                    "{" +
                    "width:'99%'," +
                    "height:'300'," +
                    "toolbar: 'User3'," +
                    "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                     "}" +
                     ");" +
                     "CKEDITOR.replace('" + txtMastTagline2.ClientID + "'," +
                     "{" +
                     "width:'99%'," +
                     "height:'300'," +
                     "toolbar: 'User3'," +
                     "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTADMCTRLPNLVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                     "}" +
                     ");" +
                     "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR", strJS, false);
        }
    }

    protected void setPageProperties()
    {
        clsMasthead mast = new clsMasthead();
        DataSet dsGroup = new DataSet();
        dsGroup = mast.getGrpList(0);
        DataView dv = new DataView(dsGroup.Tables[0]);
        dv.Sort = "GRP_NAME ASC";

        ddlGroup.DataSource = dv;
        ddlGroup.DataTextField = "GRP_NAME";
        ddlGroup.DataValueField = "GRP_ID";
        ddlGroup.DataBind();

        ListItem ddlGroupDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlGroup.Items.Insert(0, ddlGroupDefaultItem);

        clsMis mis = new clsMis();
        DataSet dsTarget = new DataSet();
        dsTarget = mis.getListByListGrp("SLIDE SHOW TARGET", 1);
        DataView dvTarget = new DataView(dsTarget.Tables[0]);
        dvTarget.Sort = "LIST_ORDER ASC";

        ddlSlideTarget.DataSource = dvTarget;
        ddlSlideTarget.DataTextField = "LIST_NAME";
        ddlSlideTarget.DataValueField = "LIST_VALUE";
        ddlSlideTarget.DataBind();

        litUrlRule.Text = "<span class=\"noticemsg\">for external link: https://www.xxx.com/" + "</span>";

        if (mode == 2)
        {
            lnkbtnDelete.Visible = true;
            fillForm();
        }

        int intFileMaxSize = 0;

        if (Session[clsAdmin.CONSTPROJECTFILESETTING] != null && Session[clsAdmin.CONSTPROJECTFILESETTING] != "")
        {
            if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTFILESETTING]) > 0)
            {
                intFileMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTFILESETTING]) * 1024;
            }
            else
            {
                if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                {
                    if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                    {
                        intFileMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                    }
                }
            }
        }

        litUploadRule.Text = litUploadRule2.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(intFileMaxSize, "kb") + " | " + clsMis.formatAllowedFileDimension(clsAdmin.CONSTADMIMGALLOWEDWIDTH, clsAdmin.CONSTADMIMGALLOWEDHEIGHT, "px");
        //litUploadRule.Text = litUploadRule2.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(clsAdmin.CONSTADMDOCALLOWEDMAXLENGTH, "kb") + " | " + clsMis.formatAllowedFileDimension(clsAdmin.CONSTADMIMGALLOWEDWIDTH, clsAdmin.CONSTADMIMGALLOWEDHEIGHT, "px");
       
        lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
        lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteMasthead.Text").ToString() + "'); return false;";
    }

    public void fillForm()
    {
        clsMasthead mast = new clsMasthead();

        if (mast.extractMastheadById(mastId, 0))
        {
            txtMastName.Text = mast.mastName;
            txtShortDesc.Text = mast.mastShortDesc;
            txtMastTagline1.Text = mast.mastTagline1;
            txtMastTagline2.Text = mast.mastTagline2;
            txtURL.Text = mast.mastUrl;

            if (mast.mastClickable == 1) 
            { 
                chkboxClickable.Checked = true;
                trClickable.Visible = chkboxClickable.Checked;
            }
            else { chkboxClickable.Checked = false; }

            if (mast.mastActive == 1) { chkboxActive.Checked = true; }
            else { chkboxActive.Checked = false; }

            ddlSlideTarget.SelectedValue = mast.mastTarget;

            if (mast.mastImage != "")
            {
                pnlProdImage.Visible = true;
                imgProdImage.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + mast.mastImage + "&w=" + clsAdmin.CONSTADMDEFAULTIMGWIDTH + "&h=" + clsAdmin.CONSTADMDEFAULTIMGHEIGHT + "&f=1";
                hdnProdImage.Value = mast.mastImage;
                hdnProdImageRef.Value = mast.mastImage;
            }

            if (mast.mastThumb != "")
            {
                pnlThumbImage.Visible = true;
                imgThumbImage.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + mast.mastThumb + "&w=" + clsAdmin.CONSTADMDEFAULTIMGWIDTH + "&h=" + clsAdmin.CONSTADMDEFAULTIMGHEIGHT + "&f=1";
                hdnThumbImage.Value = mast.mastThumb;
                hdnThumbImageRef.Value = mast.mastThumb;
            }

            if (mast.grpId > 0) { ddlGroup.SelectedValue = mast.grpId.ToString(); }

            txtOrder.Text = mast.mastOrder.ToString();
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }

    #endregion
}
