﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmAgentDetails.ascx.cs" Inherits="ctrl_ucAdmAgentDetails" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>

<script type="text/javascript">
    function validatePassword_client(source, args) {
        if (args.Value.length < <% =clsAdmin.CONSTADMUSERNAMEMINLENGTH %>)
        {
            args.IsValid = false;
        }
    } 

    function generateRandomPassword() {
        var length = 6,
            charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        document.getElementById("<% =txtLoginPassword.ClientID %>").value = retVal;
        document.getElementById("<% =txtLoginPasswordRepeat.ClientID %>").value = retVal;
    }

    function getContactNo() {
        var prefix = document.getElementById("<% =txtContactPrefix.ClientID %>").value;
        var contactno = document.getElementById("<% =txtContactNo.ClientID %>").value;
        document.getElementById("<% =txtLoginUsername.ClientID %>").value = prefix + contactno;
    }

    var $datatable;
    var propertySaveSuccess = function (msg) {
        $.magnificPopup.close();
        $(".agent-leave .ack-container .ack-msg").html(msg);
        $(".agent-leave .ack-container").removeClass("hide");
        $datatable.ajax.reload();
    };

    var initAgentLeave = function () {
        var columns = [
            { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
            { "data": "leave_start", "label": "Start", "order": 1, "convert":"datetime", "format":"DD MMM YYYY" , },
            { "data": "leave_end", "label": "End", "order": 2, "convert": "datetime", "format": "DD MMM YYYY", },
            { "data": "leave_group", "label": "Group", "order": 3, },
            { "data": "leave_reason", "label": "Reason", "order": 4, },
            { "data": "action", "bSortable": false, "bSearchable": false, "label": '<a class="popup" href="admAgentLeaveForm.aspx?emp_id=<%= agentId %>"><i class="fa fa-plus" aria-hidden="true"></i></a>', "order": 5,  },
            { "data": "employee_id", "order": 6, "visible": false },
        ];
        var columnSort = columns.reduce(function (prev, curr) {
            prev[curr.data] = curr.order;
            return prev;
        }, {});
        var columnDefs = [{
            "render": function (data, type, row) {
                var lnkbtnEdit = "<a class='lnkbtn lnkbtnEdit popup' href='admAgentLeaveForm.aspx?id=" + row.leave_id + "&emp_id=" + row.employee_id + "' title='Edit'></a>";
                var lnkbtnDelete = "<a class='lnkbtn lnkbtnDelete' title='Delete' data-id='" + row.leave_id + "'></a>";

                return lnkbtnEdit + lnkbtnDelete;
            },
            "className": "dt-center",
            "targets": columnSort["action"]
        }];
        var properties = {
            columns: columns,
            columnDefs: columnDefs,
            columnSort: columnSort,
            func: "getAgentLeave",
            name: "property",
            dom: 'ZBrti',
            elementID: "tblAgentLeave",
            dataSerializeFunc: function (d) {
                //console.log(d, this.func);
                d.searchCustom = null;
                d.func = "getAgentLeave";
                d.columns[columnSort["employee_id"]].search.value = <%= agentId %>;

                return JSON.stringify(d);
            },
            callback: function () {
                $('#tblAgentLeave .popup').magnificPopup({
                    type: 'iframe',
                    mainClass: 'timeslot-frame',
                    iframe: {
                        markup: `
                                <div class="mfp-iframe-scaler">
                                    <div class="mfp-close"></div>
                                    <iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>
                                </div>`
                    }
                });
            }
        };

        $datatable = callDataTables(properties);

    }

    $(document).on("click", ".lnkbtnDeleteAgentLeave", function () {
        if (confirm("<%= HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteProperty.Text") %>")) {
             var objID = $(this).attr("data-id");
             document.getElementById('<%= hdnPropertyID.ClientID %>').value = objID;
             document.getElementById('<%= lnkbtnDeleteAgentLeave.ClientID %>').click();
         }
     })

    $(function () {
        initAgentLeave();
    })
</script>
<asp:Panel ID="pnlFormSect1" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server" CssClass="container-fluid">
        <div class="form__container form__container--action-floated">
            <div class="form__section">
                <div class="form__section__title">
                    <span>Agent Details</span>
                </div>
                <div class="form__section__content form__section__content-leftpad">
                    <asp:Panel ID="pnlLastLogin" runat="server" Visible="false">
                        <div style="float: right">
                            <span class="form__tag">
                                Last Login<br />
                                <asp:Label runat="server" ID="lblLastLogin" CssClass="last-action-date"></asp:Label><br />
                                <br />
                                Last Updated<br />
                                <asp:Label runat="server" ID="lblLastUpdated" CssClass="last-action-date"></asp:Label><br />
                                <br />
                                Registered<br />
                                <asp:Label runat="server" ID="lblCreation" CssClass="last-action-date"></asp:Label><br />
                            </span>
                        </div>
                    </asp:Panel>
                    <div class="row">
                        <div class="col-md-3 form__label">Name:<span class="attention_compulsory">*</span></div>
                        <div class="col-md-9 form__control">
                            <asp:TextBox ID="txtName" runat="server" MaxLength="250" CssClass="text_fullwidth"></asp:TextBox>
                            <div class="form__error">
                                <asp:RequiredFieldValidator ID="rvName"
                                    runat="server"
                                    ErrorMessage="Please enter name."
                                    Display="Dynamic"
                                    ControlToValidate="txtName"
                                    CssClass="errmsg"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 form__label">
                            Contact No.:<span class="attention_compulsory">*</span>
                        </div>
                        <div class="col-md-9" style="margin-top:5px; padding: 0">
                            <div class="col-md-4">
                                <asp:TextBox ID="txtContactPrefix" runat="server" MaxLength="20" CssClass="text_fullwidth uniq" placeholder="+6016"></asp:TextBox>
                            </div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtContactNo" runat="server" MaxLength="20" CssClass="text_fullwidth uniq"></asp:TextBox>
                            </div>
                            <div class="col-md-12 form__error">
                                <asp:RequiredFieldValidator ID="rvContactNoPrefix"
                                    runat="server"
                                    ErrorMessage="Please enter contact no. prefix."
                                    Display="Dynamic"
                                    ControlToValidate="txtContactPrefix"
                                    CssClass="errmsg"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="rvContactNo"
                                    runat="server"
                                    ErrorMessage="Please enter contact no."
                                    Display="Dynamic"
                                    ControlToValidate="txtContactNo"
                                    CssClass="errmsg"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revContactNoPrefix" runat="server" ErrorMessage="<br />Please enter correct contact no." Display="Dynamic" ControlToValidate="txtContactPrefix" CssClass="errmsg" ValidationExpression="\+60\d{1,3}"></asp:RegularExpressionValidator>
                                <asp:CustomValidator ID="cvContactNo" runat="server" ControlToValidate="txtContactNo" Display="Dynamic" OnServerValidate="validateContactNo_server" OnPreRender="cvContactNo_PreRender" CssClass="errmsg" ErrorMessage="This contact no already in used."></asp:CustomValidator>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 form__label">
                            Email:<span class="attention_unique">*</span>
                        </div>
                        <div class="col-md-9 form__control">
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="250" CssClass="text_fullwidth uniq" placeholder="example@example"></asp:TextBox>
                            <asp:HiddenField ID="hdnEmail" runat="server" />
                            <asp:HiddenField ID="hdnPassword" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="<br />Please enter Email." Display="Dynamic" ControlToValidate="txtEmail" CssClass="errmsg"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="<br />Please enter valid Email." Display="Dynamic" ControlToValidate="txtEmail" CssClass="errmsg" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="cvEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" OnServerValidate="validateEmail_server" OnPreRender="cvEmail_PreRender" CssClass="errmsg" ErrorMessage="This email already in used."></asp:CustomValidator>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 form__label">
                            Team:<span class="attention_compulsory">*</span>
                        </div>
                        <div class="col-md-9 form__control">
                            <asp:TextBox ID="txtTeam" runat="server" MaxLength="250" CssClass="text_fullwidth"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvTeam" runat="server" ErrorMessage="<br />Please enter Team." Display="Dynamic" ControlToValidate="txtTeam" CssClass="errmsg"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3 form__label">Active: </div>
                        <div class="col-sm-9 form__control">
                            <asp:CheckBox ID="chkActive" runat="server" Checked="true" />
                        </div>
                    </div>
                </div>

            </div>
            
            <div class="form__section">
                <div class="form__section__title">
                    Agent Login
                </div>
                <div class="form__section__content form__section__content-leftpad">
                    <div class="row">
                        <div class="col-md-3 form__label">
                            Login ID: <span class="attention_unique">*</span>
                        </div>
                        <div class="col-md-9 form__control">
                            <asp:TextBox ID="txtLoginUsername" runat="server" MaxLength="250" CssClass="text_fullwidth uniq"></asp:TextBox><a href="javascript:void(0);" onclick="getContactNo()" title="Same as Contact No.">Same as Contact No.</a>
                            <div class="form__error">
                                <asp:RequiredFieldValidator ID="rvLoginUsername" runat="server" ErrorMessage="Please enter login id." Display="Dynamic" ControlToValidate="txtLoginUsername" CssClass="errmsg"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvLoginUsername" runat="server" ControlToValidate="txtLoginUsername" Display="Dynamic" OnServerValidate="cvLoginUsername_ServerValidate" OnPreRender="cvLoginUsername_PreRender" CssClass="errmsg" ErrorMessage="This login ID already in used."></asp:CustomValidator>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 form__label">
                            Password: <span class="attention_compulsory">*</span>
                        </div>
                        <div class="col-md-9 form__control">
                            <asp:TextBox ID="txtLoginPassword" runat="server" MaxLength="250" CssClass="text_fullwidth"></asp:TextBox><a href="javascript:void(0);" onclick="generateRandomPassword()" title="Random Password">Random Password</a>
                            <div class="form__error">
                                <asp:RequiredFieldValidator ID="rvtxtLoginPassword" runat="server" ErrorMessage="Please enter password." Display="Dynamic" ControlToValidate="txtLoginPassword" CssClass="errmsg"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvUserPwd" runat="server" CssClass="errmsg" ControlToValidate="txtLoginPassword" Display="Dynamic" ClientValidationFunction="validatePassword_client" SetFocusOnError="true"></asp:CustomValidator>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 form__label">
                            Confirm Password: <span class="attention_compulsory">*</span>
                        </div>
                        <div class="col-md-9 form__control">
                            <asp:TextBox ID="txtLoginPasswordRepeat" runat="server" MaxLength="250" CssClass="text_fullwidth"></asp:TextBox><a href="javascript:void(0);" onclick="copyValue('<% =txtLoginPassword.ClientID %>','<% =txtLoginPasswordRepeat.ClientID %>');" title="Same as Password">Same as Password</a>
                            <div class="form__error">
                                <asp:RequiredFieldValidator ID="rvtxtLoginPasswordRepeat" runat="server" ErrorMessage="Please enter confirm password." Display="Dynamic" ControlToValidate="txtLoginPasswordRepeat" CssClass="errmsg"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cmvPassword" runat="server" ErrorMessage="Password does not match." ControlToCompare="txtLoginPassword" ControlToValidate="txtLoginPasswordRepeat" Display="Dynamic" class="errmsg"></asp:CompareValidator>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form__section agent-leave">
                <div class="ack-container success hide">
                    <div class="ack-msg"></div>
                </div>
                <div class="form__section__title">
                    Agent Leave
                </div>
                <div class="form__section__content form__section__content-leftpad">
                    <uc:CmnDatatable ID="ucCmnDatatable_Property" runat="server" />
                    <asp:HiddenField runat="server" ID="hdnPropertyID" />
                    <asp:LinkButton ID="lnkbtnDeleteAgentLeave" runat="server" OnClick="lnkbtnDeleteAgentLeave_Click" Style="display: none;"></asp:LinkButton>
                    <table id="tblAgentLeave"></table>
                      
                </div>
            </div>
        </div>
        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                <asp:Hyperlink ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack" CausesValidation="false" NavigateUrl="../adm/admAgent01.aspx"></asp:Hyperlink>
            </asp:Panel>
            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                <asp:LinkButton ID="lnkbtnDelete"
                    runat="server"
                    Text="Delete"
                    ToolTip="Delete"
                    CssClass="btn btnCancel"
                    OnClick="lnkbtnDelete_Click"
                    OnClientClick="return confirm('Are you sure you want to delete?')"></asp:LinkButton>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>

</asp:Panel>
