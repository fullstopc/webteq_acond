﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmVendor.ascx.cs" Inherits="ctrl_ucAdmVendor" %>

<script type="text/javascript">
    function validateCode_client(source, args) {
        var prodCodeRE = /<% =clsAdmin.CONSTADMPRODCODERE %>/;

        if (args.Value.match(prodCodeRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "<br />Please enter only alphanumeric.";
            source.innerHTML = "<br />Please enter only alphanumeric.";
        }
    }
</script>

<asp:Panel ID="pnlFormSect1" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table cellpadding="0" cellspacing="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr">Vendor Details</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Vendor Code<span class="attention_unique">*</span>:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtProdCode" runat="server" class="text_fullwidth uniq" MaxLength="20"></asp:TextBox>
                    <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvProdCode" runat="server" ErrorMessage="<br />Please enter Vendor Code." CssClass="errmsg" ControlToValidate="txtProdCode" Display="Dynamic" ValidationGroup="vgVendor"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvProdCode" runat="server" ControlToValidate="txtProdCode" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateCode_client" OnServerValidate="validateProdCode_server" ValidationGroup="vgVendor"></asp:CustomValidator></span>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Name<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtName" runat="server" MaxLength="250" CssClass="text_fullwidth compulsory"></asp:TextBox>
                    <span class="spanFieldDesc"><asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="<br />Please enter Vendor Name." ControlToValidate="txtName" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgVendor"></asp:RequiredFieldValidator></span>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Email<span class="attention_unique">*</span>:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="250" cssclass="text_fullwidth uniq"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="<br />Please enter valid Email." Display="Dynamic" ControlToValidate="txtEmail" CssClass="errmsg" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$" ValidationGroup="vgVendor"></asp:RegularExpressionValidator>
                    <asp:CustomValidator ID="cvEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" OnServerValidate="validateEmail_server" CssClass="errmsg" ValidationGroup="vgVendor"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Contact No.:</td>
                <td class="tdMax"><asp:TextBox ID="txtContactNo" runat="server" MaxLength="20" CssClass="text_fullwidth"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel">Fax:</td>
                <td class="tdMax"><asp:TextBox ID="txtFax" runat="server" MaxLength="20" CssClass="text_fullwidth"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Address:</td>
                <td class="tdMax"><asp:TextBox ID="txtAddress" runat="server" MaxLength="250" CssClass="text_fullwidth"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr">Vendor Settngs</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Active:</td>
                <td><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="vgVendor"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CssClass="btn btnCancel" OnClick="lnkbtnDelete_Click" Visible="false"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>
