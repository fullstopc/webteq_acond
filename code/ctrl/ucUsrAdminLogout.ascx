﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrAdminLogout.ascx.cs" Inherits="ctrl_ucUsrAdminLogout" %>

<asp:Panel ID="pnlAdminLogout" runat="server" CssClass="divAdminLogout" Visible="false">
    <asp:LinkButton ID="lnkbtnLogout" runat="server" Text="LOGOUT" ToolTip="LOGOUT" OnClick="lnkbtnLogout_Click"></asp:LinkButton>
</asp:Panel>
