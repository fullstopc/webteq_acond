﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucAdmHighlightDesc : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode;
    protected int _highId;
    protected string _pageListingURL = "";

    protected string highDesc;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int mode
    {
        get
        {
            if(ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int highId
    {
        get
        {
            if (ViewState["HIGHID"] == null)
            {
                return _highId;
            }
            else
            {
                return Convert.ToInt16(ViewState["HIGHID"]);
            }
        }
        set { ViewState["HIGHID"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if (ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        highDesc = txtHighDesc.Text.Trim();

        //string strHighDesc_zh = txtHighDesc_zh.Text.Trim();

        Boolean boolEdited = false;
        Boolean boolSuccess = true;
        int intRecordAffected = 0;

        clsHighlight high = new clsHighlight();

        if (!high.isExactSameHighlightDesc(highId, highDesc))
        {
            intRecordAffected = high.updateHighlightDesc(highId, highDesc);

            if (intRecordAffected == 1)
            {
                boolEdited = true;
            }
            else
            {
                boolSuccess = false;
            }
        }

        if (boolEdited)
        {
            Session["EDITEDHIGHID"] = high.highId;

            Response.Redirect(Request.Url.ToString());
        }
        else
        {
            if (!boolSuccess)
            {
                high.extractHighlightById(highId, 0);

                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit highlight (" + high.highTitle + "). Please try again.</div>";

                Response.Redirect(Request.Url.ToString());
            }
            else
            {
                Session["EDITEDHIGHID"] = highId;
                Session["NOCHANGE"] = 1;

                Response.Redirect(Request.Url.ToString());
            }
        }
    }
    #endregion


    #region "Methods"
    public void registerCKEditorScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR")))
        {
            int intFullPath = Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFULLPATH]) == 1 ? clsAdmin.CONSTFULLPATHACTIVE : clsAdmin.CONSTFULLPATHDEFAULT;
            string strJS = null;
            strJS = "<script type=\"text/javascript\">" +
                    "CKEDITOR.replace('" + txtHighDesc.ClientID + "'," +
                    "{" +
                    "width:'99%'," +
                    "height:'300'," +
                    "toolbar: 'User3'," +
                    "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSCMSVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                     "}" +
                     ");";

            strJS += "CKEDITOR.add;";

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMEEDITORSTYLE, clsConfig.CONSTGROUPWEBSITE, 1);
            string strEditorStyle = config.value;
            if (!string.IsNullOrEmpty(strEditorStyle))
            {
                strJS += "CKEDITOR.config.contentsCss = 'body {" + strEditorStyle + "}';";
            }
            
            strJS += "CKEDITOR.config.stylesCombo_stylesSet= '" + clsAdmin.CONSTADMSTYLESET + ":" + System.Configuration.ConfigurationManager.AppSettings["jsBase"] + clsAdmin.CONSTADMSTYLESETFILE + "';" +
                     "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR", strJS, false);
        }
    }


    public void fill(int intMode)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;

                if (int.TryParse(Request["id"], out intTryParse))
                {
                    highId = intTryParse;
                }
                else
                {
                    highId = int.MaxValue;
                }
            }

            mode = intMode;

            if (mode == 2)
            {
                if (Session["DELETEDHIGHNAME"] == null) { fillForm(); }
            }

            lnkbtnCancel.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
        }
    }

    protected void fillForm()
    {
        clsHighlight high = new clsHighlight();

        if(high.extractHighlightById(highId, 0))
        {
            txtHighDesc.Text = high.highDesc;
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }
    #endregion
}
