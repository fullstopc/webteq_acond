﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Configuration;

public partial class ctrl_ucAdmCMS : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _id;
    protected int _rid;
    protected int _mode = 1;
    protected Boolean _enableLanguage = false;
    protected int _oriParent = 0;
    protected int _oriOrder = 0;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int oriParent
    {
        get
        {
            if (ViewState["ORIPARENT"] == null)
            {
                return _oriParent;
            }
            else
            {
                return Convert.ToInt16(ViewState["ORIPARENT"]);
            }
        }
        set { ViewState["ORIPARENT"] = value; }
    }

    public int oriOrder
    {
        get
        {
            if (ViewState["ORIORDER"] == null)
            {
                return _oriOrder;
            }
            else
            {
                return Convert.ToInt16(ViewState["ORIORDER"]);
            }
        }
        set { ViewState["ORIORDER"] = value; }
    }

    public int pageId
    {
        get
        {
            if (ViewState["ID"] == null)
            {
                return _id;
            }
            else
            {
                return Convert.ToInt16(ViewState["ID"]);
            }
        }
        set { ViewState["ID"] = value; }
    }
    public int rid
    {
        get
        {
            if (ViewState["RID"] == null)
            {
                return _rid;
            }
            else
            {
                return Convert.ToInt16(ViewState["RID"]);
            }
        }
        set { ViewState["RID"] = value; }
    }
    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public Boolean enableLanguage
    {
        get
        {
            if (ViewState["ENABLELANGUAGE"] == null)
            {
                return _enableLanguage;
            }
            else
            {
                return Convert.ToBoolean(ViewState["ENABLELANGUAGE"]);
            }
        }
        set { ViewState["ENABLELANGUAGE"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //imgPreview.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";

        if (string.IsNullOrEmpty(checkRequiredData()))
        {
            clsPage page = new clsPage();
            int intRecordAffected = 0;

            string strName = ucCmnPageDetails.pageName;
            string strDisplayName = ucCmnPageDetails.pageDisplayName;
            string strTitle = ucCmnPageDetails.pageTitle;
            string strTitleFriendlyUrl = ucCmnPageDetails.pageTitleFriendlyUrl;
            string strTemplate = ucCmnPageDetails.template;
            int intTemplate = ucCmnPageDetails.templateParent ? 1 : 0;
            int intParent = ucCmnPageDetails.parent;

            if (intTemplate > 0 && intParent > 0)
            {
                page.extractPageById(intParent, 0);

                strTemplate = page.template;
            }
            
            string strLang = ucCmnPageDetails.language;
            int intShowPageTitle = ucCmnPageDetails.showPageTitle ? 1 : 0;
            int intShowInMenu = ucCmnPageDetails.showInMenu ? 1 : 0;
            int intShowTop = intShowInMenu > 0 ? (ucCmnPageDetails.showTop ? 1 : 0) : 0;
            int intShowBottom = intShowInMenu > 0 ? (ucCmnPageDetails.showBottom ? 1 : 0) : 0;
            int intShowInBreadcrumb = ucCmnPageDetails.showInBreadcrumb ? 1 : 0;
            int intUserEditable = ucCmnPageDetails.userEditable ? 1 : 0;
            int intEnableLink = ucCmnPageDetails.enableLink ? 1 : 0;
            string strCSSClass = ucCmnPageDetails.cssClass;
            string strKeyword = ucCmnPageDetails.keyword;
            string strDesc = ucCmnPageDetails.desc;
            int intActive = ucCmnPageDetails.active ? 1 : 0;
            int intRedirect = ucCmnPageDetails.redirect;
            string strRedirectLink = ucCmnPageDetails.externalLink;
            string strTarget = ucCmnPageDetails.target;
            string strSlideShowGroup = ucCmnPageDetails.slideShowGroup;
            int intKeyword = ucCmnPageDetails.defaultKeyword ? 1 : 0;
            int intDesc = ucCmnPageDetails.defaultDesc ? 1 : 0;
            int intShowInSitemap = ucCmnPageDetails.showInSitemap ? 1 : 0;
            int intPageAuthorize = ucCmnPageDetails.pageAuthorize ? 1 : 0;
            ArrayList alCorrespond = ucCmnPageDetails.alCorrespond;

            string strContent = txtContent.Text.Trim();
            string strTopMenuContent = txtTopMenuContent.Text.Trim();
            //Response.Write(ddlSlideShowGroup.SelectedValue);
            int intSlideShowGroup = string.IsNullOrEmpty(strSlideShowGroup) ? 0 : Convert.ToInt16(strSlideShowGroup);
            int intContentParent = chkboxContentParent.Checked ? 1 : 0;

            if (mode == 1)
            {
                if (enableLanguage) { intRecordAffected = page.addPage(strName, strDisplayName, strTitle, strTitleFriendlyUrl, strTemplate, 
                                                                        intTemplate, intParent, strLang, intShowPageTitle, intShowInMenu, 
                                                                        intShowTop, intShowBottom, intShowInBreadcrumb, intUserEditable, intEnableLink, 
                                                                        intShowInSitemap, strCSSClass, strKeyword, intKeyword, strDesc, 
                                                                        intDesc, intRedirect, strRedirectLink, strTarget, intActive, 
                                                                        strContent, Convert.ToInt16(Session["ADMID"]), strTopMenuContent, intSlideShowGroup, intPageAuthorize,
                                                                        intContentParent); }
                else { intRecordAffected = page.addPage2(strName, strDisplayName, strTitle, strTitleFriendlyUrl, strTemplate, 
                                                            intTemplate, intParent, intShowPageTitle, intShowInMenu, intShowTop, 
                                                            intShowBottom, intShowInBreadcrumb, intUserEditable, intEnableLink, intShowInSitemap, 
                                                            strCSSClass, strKeyword, intKeyword, strDesc, intDesc, 
                                                            intRedirect, strRedirectLink, strTarget, intActive, strContent, 
                                                            Convert.ToInt16(Session["ADMID"]), strTopMenuContent, intSlideShowGroup, intPageAuthorize, intContentParent); }

                if (intRecordAffected == 1)
                {
                    Session["NEWPAGEID"] = page.id;

                    if (enableLanguage)
                    {
                        for (int i = 0; i < alCorrespond.Count; i++)
                        {
                            int intSelId = Convert.ToInt16(alCorrespond[i]);

                            if (!page.isItemExist(page.id, intSelId)) { page.addPage(page.id, intSelId); }
                        }
                    }

                    Response.Redirect(currentPageName + "?id=" + page.id);
                }
                else
                {
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new page. Please try again.</div>";

                    Response.Redirect(currentPageName);
                }
            }
            else if (mode == 2)
            {
                ArrayList alRemain = ucCmnPageDetails.alRemain;

                Boolean boolSuccess = true;
                Boolean boolEdited = false;

                page.extractPageById(pageId, 0);

                if (Session["ADMID"] != null && Session["ADMID"] != "")
                {
                    //if (Convert.ToInt16(Session["ADMTYPE"]) == clsAdmin.CONSTUSERADMTYPE && page.parentName == "" && page.parent == 0)
                    //{
                    //    if (!page.isExactSameDataSetUserAdm(id, strName, strDisplayName, strTitle, strTitleFriendlyUrl, strContent, intShowPageTitle, intShowInMenu, intShowTop, intShowBottom, intShowInBreadcrumb, intUserEditable, intEnableLink, intShowInSitemap, strCSSClass, strLang, strTemplate, intTemplate, intParent, strKeyword, intKeyword, strDesc, intDesc, intRedirect, strRedirectLink, strTarget, intActive, strTopMenuContent, intSlideShowGroup))
                    //    {
                    //        intRecordAffected = page.updatePageByIdUserAdm(id, strName, strDisplayName, strTitle, strTitleFriendlyUrl, intShowPageTitle, intShowInMenu, intShowTop, intShowBottom, intShowInBreadcrumb, intUserEditable, intEnableLink, intShowInSitemap, strCSSClass, strLang, strTemplate, intTemplate, intParent, strKeyword, intKeyword, strDesc, intDesc, intRedirect, strRedirectLink, strTarget, intActive, strContent, Convert.ToInt16(Session["ADMID"]), strTopMenuContent, intSlideShowGroup);

                    //        if (intRecordAffected == 1) { boolEdited = true; }
                    //        else { boolSuccess = false; }
                    //    }
                    //}
                    //else if (enableLanguage)
                    if (enableLanguage)
                    {
                        if (!page.isExactSameDataSet(pageId, strName, strDisplayName, strTitle, strTitleFriendlyUrl, 
                            strContent, intShowPageTitle, intShowInMenu, intShowTop, intShowBottom, 
                            intShowInBreadcrumb, intUserEditable, intEnableLink, intShowInSitemap, strCSSClass, 
                            strLang, strTemplate, intTemplate, intParent, strKeyword, 
                            intKeyword, strDesc, intDesc, intRedirect, strRedirectLink, 
                            strTarget, intActive, strTopMenuContent, intSlideShowGroup, intPageAuthorize,
                            intContentParent))
                        {
                            intRecordAffected = page.updatePageById(pageId, strName, strDisplayName, strTitle, strTitleFriendlyUrl, 
                                intShowPageTitle, intShowInMenu, intShowTop, intShowBottom, intShowInBreadcrumb, 
                                intUserEditable, intEnableLink, intShowInSitemap, strCSSClass, strLang, 
                                strTemplate, intTemplate, intParent, strKeyword, intKeyword, 
                                strDesc, intDesc, intRedirect, strRedirectLink, strTarget, 
                                intActive, strContent, Convert.ToInt16(Session["ADMID"]), strTopMenuContent, intSlideShowGroup, 
                                intPageAuthorize, intContentParent);

                            if (intRecordAffected == 1) { boolEdited = true; }
                            else { boolSuccess = false; }
                        }

                        if (boolSuccess)
                        {
                            for (int i = 0; i < alCorrespond.Count; i++)
                            {
                                int intSelId = Convert.ToInt16(alCorrespond[i]);

                                if (!page.isItemExist(pageId, intSelId))
                                {
                                    page.addPage(pageId, intSelId);
                                    boolEdited = true;
                                }
                            }

                            for (int i = 0; i < alRemain.Count; i++)
                            {
                                int intRemainId = Convert.ToInt16(alRemain[i]);

                                intRecordAffected = page.deletePageById(pageId, intRemainId);

                                if (intRecordAffected == 1) { boolEdited = true; }
                            }
                        }
                    }
                    else
                    {
                        if (!page.isExactSameDataSet(pageId, strName, strDisplayName, strTitle, strTitleFriendlyUrl, 
                            strContent, intShowPageTitle, intShowInMenu, intShowTop, intShowBottom, 
                            intShowInBreadcrumb, intUserEditable, intEnableLink, intShowInSitemap, strCSSClass, 
                            strTemplate, intTemplate, intParent, strKeyword, intKeyword, 
                            strDesc, intDesc, intRedirect, strRedirectLink, strTarget, 
                            intActive, strTopMenuContent, intSlideShowGroup, intPageAuthorize, intContentParent))
                        {
                            intRecordAffected = page.updatePageById(pageId, strName, strDisplayName, strTitle, strTitleFriendlyUrl,
                                intShowPageTitle, intShowInMenu, intShowTop, intShowBottom, intShowInBreadcrumb, 
                                intUserEditable, intEnableLink, intShowInSitemap, strCSSClass, strTemplate, 
                                intTemplate, intParent, strKeyword, intKeyword, strDesc, 
                                intDesc, intRedirect, strRedirectLink, strTarget, intActive, 
                                strContent, Convert.ToInt16(Session["ADMID"]), strTopMenuContent, intSlideShowGroup, intPageAuthorize,
                                intContentParent);

                            if (intRecordAffected == 1) { boolEdited = true; }
                            else { boolSuccess = false; }
                        }
                    }
                }
                
                if (boolEdited)
                {
                    if (oriParent != intParent)
                    {
                        page.updateOrderById(pageId, intParent);
                        page.updateOrder(oriParent, oriOrder);
                    }

                    Session["EDITEDPAGEID"] = pageId;

                    Response.Redirect(Request.Url.ToString());
                }
                else
                {
                    if (boolSuccess)
                    {
                        Session["EDITEDPAGEID"] = pageId;
                        Session["NOCHANGE"] = 1;

                        Response.Redirect(Request.Url.ToString());
                    }
                    else
                    {
                        page.extractPageById(pageId, 0);

                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit page (" + page.title + "). Please try again.</div>";

                        Response.Redirect(Request.Url.ToString());
                    }
                }
            }
        }
        else
        {
            litMsg.Text = checkRequiredData();

            trMessage.Visible = true;
        }
    }

    protected void lnkbtnPreview_Click(object sender, EventArgs e)
    {

    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string strDeletedName = "";
        int intOldParent = 0;
        int intOldOrder = 0;

        clsPage page = new clsPage();
        int intRecordAffected = 0;

        page.extractPageById(pageId, 0);
        strDeletedName = page.title;
        intOldParent = page.parent;
        intOldOrder = page.order;

        intRecordAffected = page.deletePageById(pageId);

        if (intRecordAffected == 1)
        {
            int intNewParent = page.otherId;

            page.updateOrder(intOldParent, intOldOrder);
            page.updatePage(intNewParent, pageId, int.Parse(Session["ADMID"].ToString()));
            page.updatePage(pageId);

            Session["DELETEDPAGENAME"] = strDeletedName;
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to delete page (" + strDeletedName + "). Please try again.</div>";
        }

        Response.Redirect(currentPageName);
    }

    protected void lnkbtnBackup_Click(object sender, EventArgs e)
    {
        int intRecordAffected = 0;
        clsPage page = new clsPage();
        string message = "";
        if (page.extractPageById(pageId, 0))
        {
            Boolean boolSave = true;
            string strBackupCont = "";
            int intTotalBackupPages = page.getTotalBackupPages(pageId, clsAdmin.CONSTDESKTOPVIEW);
            if (intTotalBackupPages > 0)
            {
                strBackupCont = page.getLatestPageContent(pageId, clsAdmin.CONSTDESKTOPVIEW);
                if (strBackupCont.Equals(page.content))
                {
                    boolSave = false;
                }
            }

            if (boolSave == true)
            {
                if (intTotalBackupPages >= 50)
                {
                    intRecordAffected = page.deleteOldPageBackup(pageId, clsAdmin.CONSTDESKTOPVIEW);
                }
                else
                {
                    intRecordAffected = 1;
                }

                if (intRecordAffected == 1)
                {
                    intRecordAffected = 0;
                    intRecordAffected = page.addPageBackup(page.id, page.content, clsAdmin.CONSTDESKTOPVIEW, Convert.ToInt16(Session["ADMID"]));
                    message = "Successfully Backup.";
                }
                else
                {
                    message = "Cannot backup this version.";
                }
            }
            else
            {
                message = "This version was backup previously.";
            }
        }
        else
        {
            message = GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString();
        }

        ScriptManager.RegisterStartupScript(Page, typeof(Page), "PageBackupDone", "setTimeout(function(){alert('" + message + "');}, 100);", true);
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        registerCKEditorScript();

        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void registerCKEditorScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR")))
        {
            int intFullPath = Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFULLPATH]) == 1 ? clsAdmin.CONSTFULLPATHACTIVE : clsAdmin.CONSTFULLPATHDEFAULT; 
            string strJS = null;
            strJS = clsMis.formatCKEditor(txtContent.ClientID, "99%", "500");
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTCKEDITORTOPMENU]) == 1)
            {
                strJS += clsMis.formatCKEditor(txtTopMenuContent.ClientID, "99%", "300");
            }
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR", "<script type=\"text/javascript\">" + strJS + "</script>", false);
        }
    }

    protected void setPageProperties()
    {
        lnkbtnRestore.Attributes["href"] = ConfigurationManager.AppSettings["scriptBase"] + "adm/admPageBackupList.aspx?id=" + pageId + "&type=" + clsAdmin.CONSTDESKTOPVIEW + "&" + clsAdmin.CONSTUSRARTICLETHICKBOX;

        lnkbtnBack.OnClientClick = "javascript:document.location.href='admPage01.aspx'; return false;";

        ucCmnPageDetails.pageId = pageId;
        ucCmnPageDetails.enableLanguage = enableLanguage;
        ucCmnPageDetails.enableCorrespondPage = enableLanguage;
        ucCmnPageDetails.fill();

        

        if (mode == 1)
        {
            if (!string.IsNullOrEmpty(Request["rid"]))
            {
                try
                {
                    rid = Convert.ToInt16(Request["rid"]);
                    replicateForm();
                }
                catch (Exception ex) { }
                Literal litPageTitle = (Literal) Parent.FindControl("litPageTitle");
                litPageTitle.Text = clsAdmin.CONSTADMPAGEMANAGER_REPLICATEPAGE;
            }
        }
        else if (mode == 2)
        {

            if (Session["ADMID"] != null && Session["ADMID"] != "")
            {
                if (Convert.ToInt16(Session["ADMTYPE"]) == clsAdmin.CONSTUSERADMTYPE)
                {

                    lnkbtnDelete.Visible = false;
                    // hide usereditable column

                }
                else
                {
                    lnkbtnDelete.Visible = true;
                    lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeletePage.Text").ToString() + "'); return false;";
                }
            }

            if (pageId > 0) { fillForm(); }
        }
    }
    protected void replicateForm()
    {
        clsPage page = new clsPage();

        if (page.extractPageById(rid, 0))
        {
            if (page.other > 0) { lnkbtnDelete.Visible = false; }

            DataSet ds = new DataSet();
            ds = page.getPagePageListById(rid);

            ucCmnPageDetails.pageId = page.id;
            ucCmnPageDetails.pageName = page.name;
            ucCmnPageDetails.pageDisplayName = page.displayName;
            ucCmnPageDetails.pageTitle = page.title;
            ucCmnPageDetails.pageTitleFriendlyUrl = page.titleFriendlyUrl;
            ucCmnPageDetails.templateParent = page.templateParent > 0 ? true : false;
            ucCmnPageDetails.showPageTitle = page.showPageTitle > 0 ? true : false;
            ucCmnPageDetails.showInMenu = page.showInMenu > 0 ? true : false;
            ucCmnPageDetails.showTop = page.showTop > 0 ? true : false;
            ucCmnPageDetails.showBottom = page.showBottom > 0 ? true : false;
            ucCmnPageDetails.showInBreadcrumb = page.showInBreadcrumb > 0 ? true : false;
            ucCmnPageDetails.userEditable = page.userEditable > 0 ? true : false;
            ucCmnPageDetails.enableLink = page.enableLink > 0 ? true : false;
            ucCmnPageDetails.showInSitemap = page.showInSitemap > 0 ? true : false;
            ucCmnPageDetails.cssClass = page.cssClass;
            ucCmnPageDetails.keyword = page.keyword;
            ucCmnPageDetails.defaultKeyword = page.defaultKeyword > 0 ? true : false;
            ucCmnPageDetails.desc = page.desc;
            ucCmnPageDetails.defaultDesc = page.defaultDesc > 0 ? true : false;
            ucCmnPageDetails.active = page.active > 0 ? true : false;
            ucCmnPageDetails.externalLink = page.redirectLink;
            ucCmnPageDetails.target = page.redirectTarget;
            ucCmnPageDetails.parent = page.parent;
            ucCmnPageDetails.dsCorrespond = ds;
            ucCmnPageDetails.slideShowGroup = page.slideshowGroup.ToString();
            ucCmnPageDetails.pageAuthorize = page.pageAuthorize > 0 ? true : false;
            oriParent = page.parent;
            oriOrder = page.order;

            if (page.redirect > 0) { ucCmnPageDetails.redirect = page.redirect; }
            if (!string.IsNullOrEmpty(page.template)) { ucCmnPageDetails.template = page.template; }

            txtContent.Text = page.content;
            txtTopMenuContent.Text = page.topMenuContent;

            if (enableLanguage) { ucCmnPageDetails.language = page.lang; }
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }
    protected void fillForm()
    {
        clsPage page = new clsPage();

        if (page.extractPageById(pageId, 0))
        {
            if (page.other > 0) { lnkbtnDelete.Visible = false; }

            DataSet ds = new DataSet();
            ds = page.getPagePageListById(pageId);

            ucCmnPageDetails.pageId = page.id;
            ucCmnPageDetails.pageName = page.name;
            ucCmnPageDetails.pageDisplayName = page.displayName;
            ucCmnPageDetails.pageTitle = page.title;
            ucCmnPageDetails.pageTitleFriendlyUrl = page.titleFriendlyUrl;
            ucCmnPageDetails.templateParent = page.templateParent > 0 ? true : false;
            ucCmnPageDetails.showPageTitle = page.showPageTitle > 0 ? true : false;
            ucCmnPageDetails.showInMenu = page.showInMenu > 0 ? true : false;
            ucCmnPageDetails.showTop = page.showTop > 0 ? true : false;
            ucCmnPageDetails.showBottom = page.showBottom > 0 ? true : false;
            ucCmnPageDetails.showInBreadcrumb = page.showInBreadcrumb > 0 ? true : false;
            ucCmnPageDetails.userEditable = page.userEditable > 0 ? true : false;
            ucCmnPageDetails.enableLink = page.enableLink > 0 ? true : false;
            ucCmnPageDetails.showInSitemap = page.showInSitemap > 0 ? true : false;
            ucCmnPageDetails.cssClass = page.cssClass;
            ucCmnPageDetails.keyword = page.keyword;
            ucCmnPageDetails.defaultKeyword = page.defaultKeyword > 0 ? true : false;
            ucCmnPageDetails.desc = page.desc;
            ucCmnPageDetails.defaultDesc = page.defaultDesc > 0 ? true : false;
            ucCmnPageDetails.active = page.active > 0 ? true : false;
            ucCmnPageDetails.externalLink = page.redirectLink;
            ucCmnPageDetails.target = page.redirectTarget;
            ucCmnPageDetails.parent = page.parent;
            ucCmnPageDetails.dsCorrespond = ds;
            ucCmnPageDetails.slideShowGroup = page.slideshowGroup.ToString();
            ucCmnPageDetails.pageAuthorize = page.pageAuthorize > 0 ? true : false;
            oriParent = page.parent;
            oriOrder = page.order;

            if (page.redirect > 0) { ucCmnPageDetails.redirect = page.redirect; }
            if (!string.IsNullOrEmpty(page.template)) { ucCmnPageDetails.template = page.template; }

            txtContent.Text = page.content;
            txtTopMenuContent.Text = page.topMenuContent;
            chkboxContentParent.Checked = page.contentParent == 1;
            //ddlSlideShowGroup.SelectedValue = page.slideshowGroup.ToString();

            if (enableLanguage) { ucCmnPageDetails.language = page.lang; }

            if (Session["ADMID"] != null && Session["ADMID"] != "")
            {
                if (Convert.ToInt16(Session["ADMTYPE"]) == clsAdmin.CONSTUSERADMTYPE && page.userEditable == 0)
                {
                    lnkbtnSave.Visible = false;
                    lnkbtnDelete.Visible = false;
                }
            }

            lnkbtnRestore.ToolTip = "Restore page content - " + page.displayName;
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }

    protected string checkRequiredData()
    {
        string strMessage = "<div class=\"errmsg\">Please enter all required fields (*).</div>";

        if (string.IsNullOrEmpty(ucCmnPageDetails.pageName) || string.IsNullOrEmpty(ucCmnPageDetails.pageDisplayName) || string.IsNullOrEmpty(ucCmnPageDetails.pageTitle) || (ucCmnPageDetails.parent <= 0 && string.IsNullOrEmpty(ucCmnPageDetails.template)) || (string.IsNullOrEmpty(ucCmnPageDetails.template) && !ucCmnPageDetails.templateParent))
        {
            return strMessage;
        }
        else { return ""; }
    }
    #endregion
}
