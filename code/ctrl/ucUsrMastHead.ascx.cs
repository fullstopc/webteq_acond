﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;
using System.Collections;

public partial class ctrl_ucUsrMastHead : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _grpId = 0;
    #endregion


    #region "Property Methods"
    public int grpId
    {
        get
        {
            if (ViewState["GRPID"] == null)
            {
                return _grpId;
            }
            else
            {
                return Convert.ToInt16(ViewState["GRPID"]);
            }
        }
        set { ViewState["GRPID"] = value; }
    }
    #endregion


    #region "page_events"
    protected void Page_Load(object sender, EventArgs e)
    {
        HtmlControl htmlctrlCSS = null;
        htmlctrlCSS = this.Page.Header.FindControl("Masthead") as HtmlControl;

        if (htmlctrlCSS == null)
        {
            HtmlLink cssLink = new HtmlLink();
            cssLink.ID = "Masthead";
            cssLink.Href = ConfigurationManager.AppSettings["cssBase"] + "masthead.css";
            cssLink.Attributes.Add("rel", "stylesheet");
            cssLink.Attributes.Add("type", "text/css");

            this.Page.Header.Controls.Add(cssLink);
        }

        //registerScript();
        bindRptData();
    }

    protected void rptMasthead_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strMastheadTitle = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_NAME").ToString();
            string strMastheadId = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_ID").ToString();
            string strMastheadImage = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_IMAGE").ToString();
            string strMastTagline1 = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_TAGLINE1").ToString();
            string strMastTagline2 = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_TAGLINE2").ToString();
            int intMastheadClick = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "MASTHEAD_CLICKABLE"));
            string strMastheadUrl = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_URL").ToString();
            string strMastheadTarget = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_TARGET").ToString();

            strMastheadImage = ConfigurationManager.AppSettings["uplBase"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + strMastheadImage;

            System.Drawing.Image img = System.Drawing.Image.FromFile(Server.MapPath(strMastheadImage));
            int intHeight = img.Height;
            int intWidth = img.Width;
            img.Dispose();

            Image imgMasthead = (Image)e.Item.FindControl("imgMasthead");
            Image imgMastheadImage = (Image)e.Item.FindControl("imgMastheadImage");

            imgMasthead.ImageUrl = ConfigurationManager.AppSettings["imgBase"] + "cmn/trans.gif";
            imgMasthead.ImageUrl = strMastheadImage;
            imgMasthead.AlternateText = strMastheadTitle;
            imgMasthead.ToolTip = strMastheadTitle;
            imgMasthead.Width = intWidth;
            imgMasthead.Height = intHeight;

            imgMastheadImage.ImageUrl = ConfigurationManager.AppSettings["imgBase"] + "cmn/trans.gif";
            imgMastheadImage.ImageUrl = strMastheadImage;
            imgMastheadImage.AlternateText = strMastheadTitle;
            imgMastheadImage.ToolTip = strMastheadTitle;
            imgMastheadImage.Width = intWidth;
            imgMastheadImage.Height = intHeight;

            HyperLink hypMasthead = (HyperLink)e.Item.FindControl("hypMasthead");

            if (intMastheadClick > 0)
            {
                imgMasthead.Visible = false;

                hypMasthead.Visible = true;
                hypMasthead.ToolTip = strMastheadTitle;
                hypMasthead.Target = strMastheadTarget;

                if (strMastheadUrl.StartsWith("https://") || strMastheadUrl.StartsWith("http://"))
                {
                    hypMasthead.NavigateUrl = strMastheadUrl;
                }
                else
                {
                    hypMasthead.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strMastheadUrl;
                }
            }
            Literal litMastTagline1 = (Literal)e.Item.FindControl("litMastTagline1");
            litMastTagline1.Text = strMastTagline1;

            Literal litMastTagline2 = (Literal)e.Item.FindControl("litMastTagline2");
            litMastTagline2.Text = strMastTagline2;
        }
    }

    protected void rptThumb_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strThumbTitle = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_NAME").ToString();
            string strThumbId = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_ID").ToString();
            string strThumbImage = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_THUMB").ToString();
            strThumbImage = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + ConfigurationManager.AppSettings["uplBase"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + strThumbImage + "&w=" + clsAdmin.CONSTUSRSLIDESHOWTHUMBWIDTH + "&h=" + clsAdmin.CONSTUSRSLIDESHOWTHUMBHEIGHT;
            string strThumbDimension = "width:" + clsAdmin.CONSTUSRSLIDESHOWTHUMBWIDTH + "px; height:" + clsAdmin.CONSTUSRSLIDESHOWTHUMBHEIGHT + "px;";

            Literal litThumbUrl = (Literal)e.Item.FindControl("litThumbUrl");
            litThumbUrl.Text = strThumbImage;
            Literal litThumbDimension = (Literal)e.Item.FindControl("litThumbDimension");
            litThumbDimension.Text = strThumbDimension;

            Literal litGrpId = (Literal)e.Item.FindControl("litGrpId");
            litGrpId.Text = grpId.ToString();
        }
    }
    #endregion


    #region "Methods"
    protected void registerScript()
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("slideshowpaging" + grpId))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "  $('#" + pnlMastheadSlideShow.ClientID + "').cycle({";
            strJS += "      fx: 'fade',";
            strJS += "      speed: 5500,";
            strJS += "      timeout: 5000,";
            //strJS += "      next: pnlMastheadSlideShow.ClientID,";
            strJS += "      pause: 1,";
            strJS += "      pager: '#" +  pnlMastheadSlideShowPaging.ClientID + "', pagerAnchorBuilder: pagerFactory" + grpId;
            strJS += "  });";
            strJS += "  function pagerFactory" + grpId + "(idx, slide) {";
            strJS += "      return '<a href=\"#\"><img src=\"../img/cmn/trans.gif\" alt=\"\"  class=\"imgPage" + grpId + "' + idx + '\" border=\"0\" /></a>';";
            strJS += "  };";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "slideshowpaging" + grpId, strJS);
        }
    }

    protected void bindRptData()
    {
        clsMasthead masthead = new clsMasthead();

        if (Application["MASTHEAD"] == null)
        {
            Application["MASTHEAD"] = masthead.getMastheadImage2();
        }

        DataSet dsMasthead = (DataSet)Application["MASTHEAD"];
        DataView dvMasthead = new DataView(dsMasthead.Tables[0]);


        dvMasthead.RowFilter = "MASTHEAD_ACTIVE = 1 AND GRP_ID = " + grpId;
        dvMasthead.Sort = " MASTHEAD_ORDER ASC, MASTHEAD_NAME ASC";
        
        rptMasthead.DataSource = dvMasthead;
        rptMasthead.DataBind();
        rptThumb.DataSource = dvMasthead;
        rptThumb.DataBind();
    }
    #endregion
}
