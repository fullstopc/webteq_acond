﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections.Specialized;
using System.Data;

public partial class ctlr_ucAdmCheckAccess : System.Web.UI.UserControl
{
    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        checkAdmin();
    }

    protected void checkAdmin()
    {
        clsAES aes = new clsAES();
        if (Request.Cookies[aes.EncryptToString("CA_Admin")] != null && Session["ADMID"] == null)
        {
            NameValueCollection cookie = HttpUtility.ParseQueryString(aes.DecryptString(Request.Cookies[aes.EncryptToString("CA_Admin")].Value));

            Session["ADMID"] = cookie["ID"];
            Session["ADMTYPE"] = cookie["Type"];
            Session["ADMEMAIL"] = cookie["Email"];
            Session["ADMROOT"] = !string.IsNullOrEmpty(cookie["Root"]) ? cookie["Root"] : null;

            if (cookie["type"] == "1")
            {
                setAfterUserLogin(Convert.ToInt16(cookie["projID"]));
            }
        }

        if (Session["ADMID"] == null)
        {
            Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "adm/" + clsAdmin.CONSTADMLOGIN);
        }

    }

    private void setAfterUserLogin(int intProjId)
    {
        Session[clsAdmin.CONSTPROJECTNAME] = null;
        Session[clsAdmin.CONSTPROJECTDRIVER] = null;
        Session[clsAdmin.CONSTPROJECTSERVER] = null;
        Session[clsAdmin.CONSTPROJECTUSERID] = null;
        Session[clsAdmin.CONSTPROJECTPASSWORD] = null;
        Session[clsAdmin.CONSTPROJECTDATABASE] = null;
        Session[clsAdmin.CONSTPROJECTOPTION] = null;
        Session[clsAdmin.CONSTPROJECTPORT] = null;

        string strUploadPath = Server.MapPath(ConfigurationManager.AppSettings["uplBase"].ToString());
        strUploadPath = strUploadPath.TrimEnd('\\');
        strUploadPath = strUploadPath.Remove(strUploadPath.LastIndexOf('\\') + 1);

        Session[clsAdmin.CONSTPROJECTUPLOADPATH] = strUploadPath;
        Session[clsAdmin.CONSTPROJECTUSERVIEWURL] = ConfigurationManager.AppSettings["fullbase"] + ConfigurationManager.AppSettings["scriptBase"];
        Session[clsAdmin.CONSTPROJECTTYPE] = "4";
        Session[clsAdmin.CONSTPROJECTLANG] = null;
        Session[clsAdmin.CONSTPROJECTFULLPATH] = null;
        Session[clsAdmin.CONSTPROJECTFULLPATHVALUE] = null;
        Session[clsAdmin.CONSTPROJECTGROUPPARENTCHILD] = null;
        Session[clsAdmin.CONSTPROJECTFAQ] = "0";
        Session[clsAdmin.CONSTPROJECTPROGRAMMANAGER] = "0";
        Session[clsAdmin.CONSTPROJECTEVENTMANAGER] = null;
        Session[clsAdmin.CONSTPROJECTCOUPONMANAGER] = null;
        Session[clsAdmin.CONSTPROJECTMULTIPLECURRENCY] = null;
        Session[clsAdmin.CONSTPROJECTINVENTORY] = "0";
        Session[clsAdmin.CONSTPROJECTMOBILEVIEW] = null;
        Session[clsAdmin.CONSTPROJECTPRODUCTDESCRIPTION] = null;
        Session[clsAdmin.CONSTPROJECTPAYMENTGATEWAY] = "1|3";
        Session[clsAdmin.CONSTPROJECTGST] = null;
        Session[clsAdmin.CONSTPROJECTGSTNOTAVAILABLE] = null;
        Session[clsAdmin.CONSTPROJECTGSTINCLUSIVE] = null;
        Session[clsAdmin.CONSTPROJECTGSTAPPLIED] = null;
        Session[clsAdmin.CONSTPROJECTFBLOGIN] = null;

        Session[clsAdmin.CONSTPROJECTIMGSETTING] = "0";
        Session[clsAdmin.CONSTPROJECTFILESETTING] = "10000";
        Session[clsAdmin.CONSTPROJECTCKEDITORFILE] = null;
        Session[clsAdmin.CONSTPROJECTCKEDITORIMG] = null;
        Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] = "500";
        Session[clsAdmin.CONSTPROJECTEVENTTHUMBSETTING] = null;
        Session[clsAdmin.CONSTPROJECTEVENTGALLSETTING] = null;
        Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING] = null;
        Session[clsAdmin.CONSTPROJECTPRODGALLSETTING] = null;
        Session[clsAdmin.CONSTPROJECTTRAINING] = "0";
        Session[clsAdmin.CONSTPROJECTEVENTMAXIMAGEUPLOAD] = null;


        // config
        Session[clsAdmin.CONSTPROJECTCONFIGLIMITPAGE] = null;
        Session[clsAdmin.CONSTPROJECTCONFIGLIMITOBJECT] = null;
        Session[clsAdmin.CONSTPROJECTCONFIGLIMITSLIDESHOW] = null;
        Session[clsAdmin.CONSTPROJECTCONFIGLIMITSLIDESHOWIMAGE] = null;
        Session[clsAdmin.CONSTPROJECTCONFIGLIMITEVENT] = null;
        Session[clsAdmin.CONSTPROJECTCONFIGLIMITCATEGORY] = null;
        Session[clsAdmin.CONSTPROJECTCONFIGLIMITPRODUCT] = null;
        Session[clsAdmin.CONSTGOOGLEANALYTICSCLIENTID] = null;
        Session[clsAdmin.CONSTPROJECTWATERMARK] = null;
        Session[clsAdmin.CONSTPROJECTFRIENDLYURL] = null;
        Session[clsAdmin.CONSTPROJECTGROUPMENU] = "0";

        // slider
        Session[clsAdmin.CONSTPROJECT_SLIDER_ACTIVE] = null;
        Session[clsAdmin.CONSTPROJECT_SLIDER_DEFAULT] = null;
        Session[clsAdmin.CONSTPROJECT_SLIDER_FULLSCREEN] = null;

    }
    #endregion
}
