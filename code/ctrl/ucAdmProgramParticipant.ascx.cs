﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Configuration;

public partial class ctrl_ucAdmProgramParticipant : System.Web.UI.UserControl
{
    #region "Properties"
    protected int sectId = 3;
    protected int _parId = 0;
    protected int _progId;
    protected string _gvSortExpression = "PAR_REGISTEREDDATE";
    protected string _gvSortDirection = clsAdmin.CONSTSORTDESC;
    protected string _gvSort = "";

    protected string strRegDateFrom;
    protected string strRegDateTo;
    #endregion

    #region "Property Method"
    protected string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int parId
    {
        get
        {
            if (ViewState["PARTICIPANTID"] == null)
            {
                return _parId;
            }
            else
            {
                return Convert.ToInt16(ViewState["PARTICIPANTID"]);
            }
        }
        set { ViewState["PARTICIPANTID"] = value; }
    }

    public int progId
    {
        get
        {
            if (ViewState["PROGRAMID"] == null)
            {
                return _progId;
            }
            else
            {
                return Convert.ToInt16(ViewState["PROGRAMID"]);
            }
        }
        set { ViewState["PROGRAMID"] = value; }
    }

    protected string gvSortDirection
    {
        get { return ViewState["GVSORTDIRECTION"] as string ?? _gvSortDirection; }
        set { ViewState["GVSORTDIRECTION"] = value; }
    }

    protected string gvSortExpression
    {
        get { return ViewState["GVSORTEXPRESSION"] as string ?? _gvSortExpression; }
        set { ViewState["GVSORTEXPRESSION"] = value; }
    }

    protected string gvSort
    {
        get { return ViewState["GVSORT"] as string ?? _gvSort; }
        set { ViewState["GVSORT"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;
                if (int.TryParse(Request["id"], out intTryParse))
                {
                    progId = intTryParse;
                }
                else
                {
                    progId = 0;
                }
            }

            bindGV(gvParticipant, "PAR_NAME ASC");
        }

        if (Session["CANCELLEDPARNAME"] != null)
        {
            litAck.Text = "<div class=\"noticemsg\">Registration participant (" + Session["CANCELLEDPARNAME"].ToString() + ") has been cancelled successfully.</div>";
            pnlAck.Visible = true;

            Session["CANCELLEDPARNAME"] = null;
        }
    }

    protected void gvParticipant_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int intParId = Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "PAR_ID"));
            string strParFirstName = DataBinder.Eval(e.Row.DataItem, "PAR_FIRSTNAME").ToString();
            string strParLastName = DataBinder.Eval(e.Row.DataItem, "PAR_LASTNAME").ToString();

            Literal litRegDate = (Literal)e.Row.FindControl("litRegDate");
            litRegDate.Text = "-";
            if (!string.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "PAR_REGISTEREDDATE").ToString()))
            {
                DateTime dtRegDate = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "PAR_REGISTEREDDATE"));

                litRegDate.Text = dtRegDate.ToString("dd MMM yyyy");
            }

            Literal litProgName = (Literal)e.Row.FindControl("litProgName");
            litProgName.Text = strParFirstName + " " + strParLastName;

            Literal litProgDate = (Literal)e.Row.FindControl("litProgDate");
            litProgDate.Text = "-";
            if (!string.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "V_PROGSTARTDATE").ToString()) && !string.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "V_PROGENDDATE").ToString()))
            {
                DateTime dtStartDate = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "V_PROGSTARTDATE"));
                DateTime dtEndDate = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "V_PROGENDDATE"));

                if (dtStartDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString() && dtEndDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
                {
                    litProgDate.Text = dtStartDate.ToString("dd MMM yyyy") + " - " + dtEndDate.ToString("dd MMM yyyy");
                }
            }

            Literal litDOB = (Literal)e.Row.FindControl("litDOB");
            litDOB.Text = "-";
            if (!string.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "PAR_DOB").ToString()))
            {
                DateTime dtDOB = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "PAR_DOB"));

                if (dtDOB.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
                {
                    litDOB.Text = dtDOB.ToString("dd MMM yyyy");
                }
            }

            LinkButton lnkbtnView = (LinkButton)e.Row.FindControl("lnkbtnView");
            lnkbtnView.Text = "View";
            lnkbtnView.OnClientClick = "javascript:window.open('admRegistrationDetails.aspx?pid=" + intParId + "','_blank','toolbar=false,menubar=false,scrollbars,width=834px','false'); return false;";

            LinkButton lnkbtnCancel = (LinkButton)e.Row.FindControl("lnkbtnCancel");
            lnkbtnCancel.Text = "Cancel";
            lnkbtnCancel.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmCancelParticipant.Text").ToString() + "');";
        }
    }

    protected void gvParticipant_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "cmdCancel")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int intParId = Convert.ToInt16(gvParticipant.DataKeys[rowIndex].Value.ToString());

            string strCancelledParName = "";

            clsMis.performCancelRegistration(intParId, ref strCancelledParName);

            Session["CANCELLEDPARNAME"] = strCancelledParName;
            Response.Redirect(Request.Url.ToString());
        }
    }

    protected void gvParticipant_Sorting(object sender, GridViewSortEventArgs e)
    {
        string strSortExpDir = "";
        if (e.SortExpression != gvSortExpression)
        {
            gvSortExpression = e.SortExpression;
            gvSortDirection = clsAdmin.CONSTSORTDESC;
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }
        else
        {
            switch (gvSortDirection)
            {
                case clsAdmin.CONSTSORTASC:
                    gvSortDirection = clsAdmin.CONSTSORTDESC;
                    break;
                case clsAdmin.CONSTSORTDESC:
                    gvSortDirection = clsAdmin.CONSTSORTASC;
                    break;
            }

            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }

        gvSort = strSortExpDir;
        bindGV(gvParticipant, gvSort);
    }

    protected void gvParticipant_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvParticipant.PageIndex = e.NewPageIndex;
        bindGV(gvParticipant, gvSort);
    }

    protected void lnkbtnExcel_Click(object sender, EventArgs e)
    {
        ExportGV(gvParticipant, true);
    }
    #endregion

    #region "Method"
    protected void bindGV(GridView gvRef, string gvSortExpDir)
    {
        clsParticipant par = new clsParticipant();
        DataSet ds = new DataSet();
        DataView dv = new DataView();
        ds = par.getParticipantList(0);
        dv = new DataView(ds.Tables[0]);

        dv.RowFilter = "PAR_PROGRAM = " + progId;
        dv.Sort = "PAR_ID ASC";

        litParticipantFound.Text = dv.Count + " record(s) listed.";

        if (dv.Count > 0)
        {
            Session["PROGPARDV"] = dv;

            gvParticipant.DataSource = dv;
            gvParticipant.DataBind();

            lnkbtnExcel.CssClass = "btn2 btnExport";
            lnkbtnExcel.Enabled = true;
        }
        else
        {
            lnkbtnExcel.CssClass = "btn2 btnExport btnDisabled";
            lnkbtnExcel.Enabled = false;
        }

    }

    protected void ExportGV(GridView gvRef, Boolean boolHideAction)
    {
        DateTime dt = DateTime.Now;
        string strFileName = "PARTICIPANTLIST_" + dt.Day + dt.Month + dt.Year + ".xls";
        string strAttachment = "attachment; filename=" + strFileName;

        Response.ClearContent();
        Response.AddHeader("content-disposition", strAttachment);
        Response.ContentType = "application/vnd.xls";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        DataView dv = new DataView();
        dv = (DataView)Session["PROGPARDV"];

        gvRef.AllowPaging = false;
        gvRef.DataSource = dv;
        gvRef.DataBind();

        int intColHide = gvRef.Columns.Count - 1;

        for (int i = 0; i < gvRef.Columns.Count; i++)
        {
            if (gvRef.HeaderRow.Cells[i].CssClass == "hiddenCol")
            {
                gvRef.HeaderRow.Cells[i].CssClass = "showCol";
            }

            gvRef.HeaderRow.Cells[i].Enabled = false;
        }

        if (boolHideAction)
        {
            gvRef.HeaderRow.Cells[intColHide].Visible = false;
        }

        for (int i = 0; i < gvRef.Rows.Count; i++)
        {
            GridViewRow row = gvRef.Rows[i];

            for (int j = 0; j < gvRef.Columns.Count; j++)
            {
                if (row.Cells[j].CssClass == "hiddenCol")
                {
                    row.Cells[j].CssClass = "showCol";
                }
            }
        }

        if (boolHideAction)
        {
            gvRef.Columns[intColHide].Visible = false;
        }

        gvRef.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }
    #endregion
}
