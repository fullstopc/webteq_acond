﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucUsrIndMember : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _memId = 0;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int memId
    {
        get
        {
            if (ViewState["MEMID"] == null)
            {
                return _memId;
            }
            else
            {
                return Convert.ToInt16(ViewState["MEMID"]);
            }
        }
        set { ViewState["MEMID"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setPageProperties();
        }
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        //litWelcome.Text = "<div class=\"divWelcome\">" + GetLocalResourceObject("litWelcome.Text").ToString() + "</div>";

        fillForm();
    }

    protected void fillForm()
    {
        clsDirMerchant mem = new clsDirMerchant();

        if (mem.extractMemberById(memId, 0))
        {
            litIndMemContent.Text = mem.content;

            //int intSectId = 0; //Convert.ToInt16(clsSetting.CONSTUSRITEMPAGESECTID);

            //if (Application["PAGELIST"] == null)
            //{
            //    clsCMS cms = new clsCMS();
            //    Application["PAGELIST"] = cms.getPageList();
            //}

            //DataSet ds = new DataSet();
            //ds = (DataSet)Application["PAGELIST"];

            //DataTable dt = new DataTable();
            //dt = ds.Tables[0];

            //DataRow[] foundRow = dt.Select("SECT_ID = " + intSectId, "PAGE_DEFAULT DESC, PAGE_ORDER ASC");

            //if (foundRow.Length > 0)
            //{
            //    string strLandingPage;

            //    if (!string.IsNullOrEmpty(foundRow[0]["SECT_DEFAULT"].ToString()))
            //    {
            //        strLandingPage = foundRow[0]["SECT_DEFAULT"].ToString();
            //    }
            //    else
            //    {
            //        strLandingPage = foundRow[0]["SECT_TEMPLATE"].ToString();
            //    }

            //    int intMemPrevId = mem.getPrevMemberId(mem.name, memId, mem.bizType, 1);
            //    int intMemNextId = mem.getNextMemberId(mem.name, memId, mem.bizType, 1);

            //    if (intMemPrevId > 0)
            //    {
            //        imgbtnPrevTop.OnClientClick = "javascript:document.location.href='" + strLandingPage + "?pgid=" + foundRow[0]["PAGE_ID"] + "&id=" + intMemPrevId + "'; return false;";
            //        imgbtnPrevBottom.OnClientClick = "javascript:document.location.href='" + strLandingPage + "?pgid=" + foundRow[0]["PAGE_ID"] + "&id=" + intMemPrevId + "'; return false;";
            //    }
            //    else
            //    {
            //        imgbtnPrevTop.Enabled = false;
            //        imgbtnPrevTop.Visible = false;
            //        imgbtnPrevBottom.Enabled = false;
            //        imgbtnPrevBottom.Visible = false;
            //    }

            //    if (intMemNextId > 0)
            //    {
            //        imgbtnNextTop.OnClientClick = "javascript:document.location.href='" + strLandingPage + "?pgid=" + foundRow[0]["PAGE_ID"] + "&id=" + intMemNextId + "'; return false;";
            //        imgbtnNextBottom.OnClientClick = "javascript:document.location.href='" + strLandingPage + "?pgid=" + foundRow[0]["PAGE_ID"] + "&id=" + intMemNextId + "'; return false;";
            //    }
            //    else
            //    {
            //        imgbtnNextTop.Enabled = false;
            //        imgbtnNextTop.Visible = false;
            //        imgbtnNextBottom.Enabled = false;
            //        imgbtnNextBottom.Visible = false;
            //    }

            //    if (intMemPrevId == 0 && intMemNextId == 0)
            //    {
            //        pnlIndMemberActionTop.Visible = false;
            //        pnlIndMemberActionBottom.Visible = false;
            //    }
            //}
        }
    }
    #endregion
}
