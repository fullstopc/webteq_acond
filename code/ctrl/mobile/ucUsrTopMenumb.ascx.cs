﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;
using System.Globalization;

public partial class ctrl_mobile_ucUsrTopMenumb : System.Web.UI.UserControl
{
    #region "Properties"
    protected string _lang = "";
    protected int _pageId = 0;
    protected bool _boolFriendlyURL = false;
    protected DataSet _dsMenu = new DataSet();
    #endregion

    #region "Property Methods"
    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public Boolean boolFriendlyURL
    {
        get { return _boolFriendlyURL; }
        set { _boolFriendlyURL = value; }
    }

    protected DataSet dsMenu
    {
        get
        {
            if (ViewState["DSMENU"] == null)
            {
                return _dsMenu;
            }
            else
            {
                return (DataSet)ViewState["DSMENU"];
            }
        }
        set { ViewState["DSMENU"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void rptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "PAGE_ID"));
            int intParent = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "PAGE_PARENT"));
            string strPgTitle = DataBinder.Eval(e.Item.DataItem, "PAGE_DISPLAYNAME").ToString();
            string strTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATE").ToString();
            string strMobileTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILETEMPLATE").ToString();
            string strLang = DataBinder.Eval(e.Item.DataItem, "PAGE_LANG").ToString();
            string strPageTitleFURL = DataBinder.Eval(e.Item.DataItem, "PAGE_TITLEFURL").ToString();
            string strCSSClass = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILECSSCLASS").ToString();
            int intEnableLink = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ENABLELINK"));
            int intRedirect = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILEREDIRECT"));
            string strRedirectLink = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILEREDIRECTLINK").ToString();
            string strTarget = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILEREDIRECTTARGET").ToString();

            HyperLink hypMenu = (HyperLink)e.Item.FindControl("hypMenu");
            hypMenu.ToolTip = strPgTitle;

            Literal litMenu = (Literal)e.Item.FindControl("litMenu");
            litMenu.Text = strPgTitle;

            if (!string.IsNullOrEmpty(strCSSClass)) { hypMenu.CssClass = strCSSClass; }

            Panel pnlTopMenuItem = (Panel)e.Item.FindControl("pnlTopMenuItem");
            if (intId == pageId)
            {
                hypMenu.CssClass = (!string.IsNullOrEmpty(strCSSClass) ? strCSSClass : hypMenu.CssClass) + " selected";
                pnlTopMenuItem.CssClass = "divTopMenuItemSel";
            }

            DataView dv = new DataView(dsMenu.Tables[0]);
            dv.RowFilter = "PAGE_PARENT = " + intId;
            dv.Sort = "PAGE_ORDER ASC";

            if (dv.Count > 0)
            {
                Repeater rptSubMenu = (Repeater)e.Item.FindControl("rptSubMenu");

                rptSubMenu.DataSource = dv;
                rptSubMenu.DataBind();

                hypMenu.CssClass = hypMenu.CssClass.Replace("parent", "") + " parent";

                Panel pnlSubMenu = (Panel)e.Item.FindControl("pnlSubMenu");

            }
            else
            {
                if (intEnableLink == 0)
                {
                    hypMenu.NavigateUrl = string.Empty;
                    hypMenu.Attributes.Remove("href");
                    hypMenu.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "default";
                }
                else
                {
                    if (intRedirect > 0)
                    {
                        if (intRedirect == clsSetting.CONSTEXTERNALLINK)
                        {
                            if (!string.IsNullOrEmpty(strRedirectLink))
                            {
                                string strLink = strRedirectLink;

                                if (strRedirectLink.StartsWith("http://") || strRedirectLink.StartsWith("https://"))
                                {
                                    hypMenu.NavigateUrl = strLink;
                                }
                                else
                                {
                                    hypMenu.NavigateUrl = "http://" + strLink;
                                }

                                hypMenu.Target = strTarget;
                            }
                        }
                        else
                        {
                            DataRow[] foundRow = dsMenu.Tables[0].Select("PAGE_ID = " + intRedirect + " AND PAGE_OTHER = 0");

                            if (foundRow.Length > 0)
                            {
                                int intRParent = Convert.ToInt16(foundRow[0]["PAGE_PARENT"]);
                                int intRTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                                string strRTemplateParent = foundRow[0]["V_MOBILETEMPLATEPARENT"].ToString();
                                string strRTemplate = intRParent > 0 ? (intRTemplateParent > 0 ? strRTemplateParent : strMobileTemplate) : strMobileTemplate;

                                if (!string.IsNullOrEmpty(strRTemplate))
                                {
                                    if (!string.IsNullOrEmpty(lang)) { hypMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strRTemplate + "?pgid=" + intRedirect + "&lang=" + strLang; }
                                    else { hypMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strRTemplate + "?pgid=" + intRedirect; }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(strMobileTemplate))
                        {
                            if (!string.IsNullOrEmpty(lang)) { hypMenu.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strMobileTemplate + "?pgid=" + intId + "&lang=" + strLang; }
                            else { hypMenu.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strMobileTemplate + "?pgid=" + intId; }
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(lang)) { hypMenu.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strMobileTemplate + "?pgid=" + intId + "&lang=" + strLang; }
            else { hypMenu.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strMobileTemplate + "?pgid=" + intId; }
        }
    }

    protected void rptSubMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ID"));
            string strDisplayName = DataBinder.Eval(e.Item.DataItem, "PAGE_DISPLAYNAME").ToString();
            string strCSSClass = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILECSSCLASS").ToString();
            string strLang = DataBinder.Eval(e.Item.DataItem, "PAGE_LANG").ToString();
            string strTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATE").ToString();
            string strMobileTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILETEMPLATE").ToString();
            int intParent = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_PARENT"));
            int intParentTemplate = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATEPARENT"));
            string strParentTemplate = DataBinder.Eval(e.Item.DataItem, "V_MOBILETEMPLATEPARENT").ToString();
            int intEnableLink = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ENABLELINK"));
            int intRedirect = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILEREDIRECT"));
            string strRedirectLink = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILEREDIRECTLINK").ToString();
            string strTarget = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILEREDIRECTTARGET").ToString();

            HyperLink hypSubMenu = (HyperLink)e.Item.FindControl("hypSubMenu");
            hypSubMenu.Text = strDisplayName;
            hypSubMenu.ToolTip = strDisplayName;

            Panel pnlTopMenuSubItem = (Panel)e.Item.FindControl("pnlTopMenuSubItem");
            Panel pnlSubMenuLv2 = (Panel)e.Item.FindControl("pnlSubMenuLv2");

            if (intId == pageId)
            {
                HyperLink hypMenu = (HyperLink)e.Item.NamingContainer.NamingContainer.FindControl("hypMenu");
                //hypMenu.CssClass = hypMenu.CssClass + "Sel";
                hypMenu.CssClass = hypMenu.CssClass.Replace("parent", "").Replace("expand", "") + " parent expand";

                Panel pnlTopMenuItem = (Panel)e.Item.NamingContainer.NamingContainer.FindControl("pnlTopMenuItem");
                pnlTopMenuItem.CssClass = "divTopMenuItemSel";

                HtmlGenericControl control = (HtmlGenericControl)e.Item.FindControl("liSubMenu");
                control.Attributes["class"] = "sel";

                hypSubMenu.CssClass = hypSubMenu.CssClass.Replace("selected", "") + " selected";
            }

            DataView dv = new DataView(dsMenu.Tables[0]);
            dv.RowFilter = "PAGE_PARENT = " + intId + " AND PAGE_MOBILESHOWINMENU = 1 AND PAGE_OTHER = 0";
            dv.Sort = "PAGE_ORDER ASC";

            if (dv.Count > 0)
            {
                string strTemplateUse = intParentTemplate > 0 ? strParentTemplate : strMobileTemplate;

                if (!string.IsNullOrEmpty(strTemplateUse))
                {
                    if (!string.IsNullOrEmpty(lang)) { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strTemplateUse + "?pgid=" + intId + "&lang=" + strLang; }
                    else { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strTemplateUse + "?pgid=" + intId; }
                }

                //hypSubMenu.Text += "<span class='dropdown'></span>";
                hypSubMenu.CssClass += " menuSubLevel";
                Repeater rptSubMenuLv2 = (Repeater)e.Item.FindControl("rptSubMenuLv2");

                rptSubMenuLv2.DataSource = dv;
                rptSubMenuLv2.DataBind();
            }
            else
            {
                if (intEnableLink == 0)
                {
                    hypSubMenu.NavigateUrl = string.Empty;
                    hypSubMenu.Attributes.Remove("href");
                    hypSubMenu.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "default";
                }
                else
                {
                    if (intRedirect > 0)
                    {
                        if (intRedirect == clsSetting.CONSTEXTERNALLINK)
                        {
                            if (!string.IsNullOrEmpty(strRedirectLink))
                            {
                                string strLink = strRedirectLink;

                                if (strRedirectLink.StartsWith("http://") || strRedirectLink.StartsWith("https://"))
                                {
                                    hypSubMenu.NavigateUrl = strLink;
                                }
                                else
                                {
                                    hypSubMenu.NavigateUrl = "http://" + strLink;
                                }

                                hypSubMenu.Target = strTarget;
                            }
                        }
                        else
                        {
                            DataRow[] foundRow = dsMenu.Tables[0].Select("PAGE_ID = " + intRedirect + " AND PAGE_OTHER = 0");

                            if (foundRow.Length > 0)
                            {
                                int intRParent = Convert.ToInt16(foundRow[0]["PAGE_PARENT"]);
                                int intRTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                                string strRTemplateParent = foundRow[0]["V_MOBILETEMPLATEPARENT"].ToString();
                                string strRTemplate = intRParent > 0 ? (intRTemplateParent > 0 ? strRTemplateParent : strMobileTemplate) : strMobileTemplate;

                                if (!string.IsNullOrEmpty(strRTemplate))
                                {
                                    if (!string.IsNullOrEmpty(lang)) { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strRTemplate + "?pgid=" + intRedirect + "&lang=" + strLang; }
                                    else { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strRTemplate + "?pgid=" + intRedirect; }
                                }
                            }
                        }
                    }
                    else
                    {
                        string strTemplateUse = intParentTemplate > 0 ? strParentTemplate : strMobileTemplate;

                        if (!string.IsNullOrEmpty(strTemplateUse))
                        {
                            if (!string.IsNullOrEmpty(lang)) { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strTemplateUse + "?pgid=" + intId + "&lang=" + strLang; }
                            else { hypSubMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strTemplateUse + "?pgid=" + intId; }
                        }
                    }
                }
            }
        }
    }

    protected void rptSubMenuLv2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ID"));
            string strDisplayName = DataBinder.Eval(e.Item.DataItem, "PAGE_DISPLAYNAME").ToString();
            string strCSSClass = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILECSSCLASS").ToString();
            string strLang = DataBinder.Eval(e.Item.DataItem, "PAGE_LANG").ToString();
            string strTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILETEMPLATE").ToString();
            int intRedirect = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILEREDIRECT"));
            int intParent = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_PARENT"));
            int intParentTemplate = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATEPARENT"));
            string strParentTemplate = DataBinder.Eval(e.Item.DataItem, "V_MOBILETEMPLATEPARENT").ToString();
            int intEnableLink = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ENABLELINK"));
            string strRedirectLink = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILEREDIRECTLINK").ToString();
            string strRedirectTarget = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILEREDIRECTTARGET").ToString();

            HyperLink hypSubMenuLv2 = (HyperLink)e.Item.FindControl("hypSubMenuLv2");
            hypSubMenuLv2.Text = strDisplayName;
            hypSubMenuLv2.ToolTip = strDisplayName;

            if (intId == pageId)
            {
                HyperLink hypMenu = (HyperLink)e.Item.NamingContainer.NamingContainer.NamingContainer.NamingContainer.FindControl("hypMenu");
                hypMenu.CssClass = hypMenu.CssClass.Replace("parent", "").Replace("expand", "") + " parent expand selected";

                HtmlGenericControl liSubMenu = (HtmlGenericControl)e.Item.NamingContainer.NamingContainer.FindControl("liSubMenu");
                liSubMenu.Attributes["class"] = "sel";

                HyperLink hypSubMenu = (HyperLink)e.Item.NamingContainer.NamingContainer.FindControl("hypSubMenu");
                hypSubMenu.CssClass = hypSubMenu.CssClass.Replace("selected","") + " selected";

                HtmlGenericControl control = (HtmlGenericControl)e.Item.FindControl("liSubMenuLv2");
                control.Attributes["class"] = "sel";

                hypSubMenuLv2.CssClass = hypSubMenuLv2.CssClass.Replace("selected", "") + " selected";
            }

            if (intEnableLink == 0)
            {
                hypSubMenuLv2.NavigateUrl = string.Empty;
                hypSubMenuLv2.Attributes.Remove("href");
                hypSubMenuLv2.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "default";
            }
            else
            {
                if (intRedirect > 0)
                {
                    if (intRedirect == clsSetting.CONSTEXTERNALLINK)
                    {
                        if (!string.IsNullOrEmpty(strRedirectLink))
                        {
                            string strLink = strRedirectLink;

                            if (strRedirectLink.StartsWith("http://") || strRedirectLink.StartsWith("https://"))
                            {
                                hypSubMenuLv2.NavigateUrl = strLink;
                            }
                            else
                            {
                                hypSubMenuLv2.NavigateUrl = "http://" + strLink;
                            }

                            hypSubMenuLv2.Target = strRedirectTarget;
                        }
                    }
                    else
                    {
                        DataRow[] foundRow = dsMenu.Tables[0].Select("PAGE_ID = " + intRedirect + " AND PAGE_OTHER = 0");

                        if (foundRow.Length > 0)
                        {
                            int intRParent = Convert.ToInt16(foundRow[0]["PAGE_PARENT"]);
                            int intRTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                            string strRTemplateParent = foundRow[0]["V_MOBILETEMPLATEPARENT"].ToString();
                            string strRTemplate = intRParent > 0 ? (intRTemplateParent > 0 ? strRTemplateParent : strTemplate) : strTemplate;

                            if (!string.IsNullOrEmpty(strRTemplate))
                            {
                                if (!string.IsNullOrEmpty(lang)) { hypSubMenuLv2.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strRTemplate + "?pgid=" + intRedirect + "&lang=" + strLang; }
                                else { hypSubMenuLv2.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strRTemplate + "?pgid=" + intRedirect; }
                            }
                        }
                    }
                }
                else
                {
                    string strTemplateUse = intParentTemplate > 0 ? strParentTemplate : strTemplate;

                    if (!string.IsNullOrEmpty(strTemplateUse))
                    {
                        if (!string.IsNullOrEmpty(lang)) { hypSubMenuLv2.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strTemplateUse + "?pgid=" + intId + "&lang=" + strLang; }
                        else { hypSubMenuLv2.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strTemplateUse + "?pgid=" + intId; }
                    }
                }
            }
        }
    }

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            bindRptData();
        }
    }

    protected void bindRptData()
    {
        if (Application["MOBILEPAGELIST"] == null)
        {
            clsPage page = new clsPage();
            Application["MOBILEPAGELIST"] = page.getMobilePageList(1, 1);
        }

        DataSet ds = new DataSet();
        dsMenu = (DataSet)Application["MOBILEPAGELIST"];

        List<string> templates = new clsMis().getListByListGrp("MOBILE PAGE TEMPLATE", 1).Tables[0].AsEnumerable().Select(x => "'" + x["list_value"] + "'").ToList();
        clsLog.logErroMsg(string.Join(",", templates) + "::");
        DataView dvPage = new DataView(dsMenu.Tables[0]);
        dvPage.RowFilter = "PAGE_MOBILESHOWINMENU = 1 AND PAGE_PARENT = " + clsSetting.CONSTPAGEPARENTROOT + " AND PAGE_MOBILETEMPLATE IN (" + string.Join(",", templates) + ")";
        //dvPage.Sort = "PAGE_PARENT DESC, PAGE_ORDER ASC";
        dvPage.Sort = "PAGE_ORDER ASC";

        if (dvPage.Count > 0)
        {
            rptItems.DataSource = dvPage;
            rptItems.DataBind();
        }
    }
    #endregion
    #endregion
}