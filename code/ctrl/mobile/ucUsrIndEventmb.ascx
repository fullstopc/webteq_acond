﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrIndEventmb.ascx.cs" Inherits="ctrl_ucUsrIndEvent" %>

<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>lazysizes.min.js" type="text/javascript"></script>
<script type="text/javascript">
    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.expand = 9;

    function adjustPortfolio() {
        var width = 180; // <%= clsSetting.CONSTUSRPHOTOGALLERYIMGWIDTHINDE %> ;
        var container = $("div[event='galImg']").width();

        // ratio = 4:3
        var numEleRow = Math.round(container / width);
        var newWidth = (container / numEleRow) - 10;
        var newHeight = (newWidth / 4) * 3;

        $("div[event='galImg'] > div").width(newWidth).height(newHeight);
    }

    function getEventImgDimention() {
        var imgWidth = $("div[type='eventGallery']").width();
        var imgHeight = imgWidth * (3 / 4);
        $(".divEventGalleryImg").css({ 'height': imgHeight + 'px' });
        $(".divEventGalleryImg > div > a > img").each(function () {
            getImageDimention($(this), imgWidth, imgHeight);
        });

        $('.hypEventGalleryVid').css({ "height": $(".divEventGalleryImg").height() });
    }

    function getImageDimention(element, targetWidth, targetHeight) {
        var image = new Image();
        image.src = element.attr("src");

        var imageWidthRatio;
        var imageHeightRatio;
        var imageRealWidth = image.naturalWidth;
        var imageRealHeight = image.naturalHeight;
        var imageNewWidth;
        var imageNewHeight;
        var imageRatio;

        imageWidthRatio = imageRealWidth / targetWidth;
        imageHeightRatio = image.naturalHeight / targetHeight;

        if (imageWidthRatio >= imageHeightRatio) {
            imageRatio = imageHeightRatio;
            imageNewHeight = targetHeight;
            imageNewWidth = Math.round(imageRealWidth / imageRatio);
        }
        else {
            imageRatio = imageWidthRatio;
            imageNewWidth = targetWidth;
            imageNewHeight = Math.round(imageRealHeight / imageRatio);
        }

        element.width(imageNewWidth);
        element.height(imageNewHeight);
        element.css({ 'top': (targetHeight - imageNewHeight) / 2 + 'px', 'left': (targetWidth - imageNewWidth) / 2 + 'px' });
    }

    $(document).ready(function () {
        $('.swipebox').swipebox({
            useCSS: true, // false will force the use of jQuery for animations
            useSVG: false, // false to force the use of png for buttons
            initialIndexOnArray: 0, // which image index to init when a array is passed
            hideCloseButtonOnMobile: false, // true will hide the close button on mobile devices
            hideBarsDelay: 3000, // delay before hiding bars on desktop
            videoMaxWidth: 1140, // videos max width
            loopAtEnd: false // true will return to the first image after the last image is reached
        });

        //adjustPortfolio();
        getEventImgDimention();
    });


    $(window).resize(function () {
        //adjustPortfolio();
        getEventImgDimention();
    });
</script>

<asp:Panel ID="pnlIndEvent" runat="server" CssClass="divIndEvent">
    <div class="event-item">
        <asp:Panel ID="pnlIndEventTitle" runat="server" CssClass="divIndEventTitle">
            <h2>
                <asp:Literal ID="litIndEventTitle" runat="server"></asp:Literal>
            </h2>
        </asp:Panel>
        <asp:Panel ID="pnlIndEventDate" runat="server" CssClass="divIndEventDate">
            <h3><asp:Literal ID="litIndEventDate" runat="server"></asp:Literal></h3>
        </asp:Panel>
        <asp:Panel ID="pnlIndEventDesc" runat="server" CssClass="divIndEventDesc" Visible="false">
            <p><asp:Literal ID="litIndEventDesc" runat="server"></asp:Literal></p>
        </asp:Panel>
        <asp:Panel ID="pnlIndEventDesc2" runat="server" CssClass="divIndEventDesc" Visible="false">
            <p><asp:Literal ID="litIndEventDesc2" runat="server"></asp:Literal></p>
        </asp:Panel>
    </div>
    <asp:Panel ID="pnlIndEventArticle" runat="server" Visible="false" CssClass="event-item">
        <asp:Repeater ID="rptArticle" runat="server" OnItemDataBound="rptArticle_ItemDataBound" OnItemCommand="rptArticle_ItemCommand">
            <ItemTemplate>
                <asp:HyperLink ID="hypArticle" CssClass="lnkbtnArticle thickbox" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "HIGHART_TITLE") %>' ToolTip='<%# DataBinder.Eval(Container.DataItem, "HIGHART_TITLE") %>'></asp:HyperLink>
            </ItemTemplate>
            <SeparatorTemplate><span>|</span></SeparatorTemplate>
        </asp:Repeater>
    </asp:Panel>
   <%-- <asp:Panel ID="pnlEventGallList" runat="server" CssClass="divEventGallList" event="galImg">
        <asp:Repeater ID="rptEventGallery" runat="server" OnItemDataBound="rptEventGallery_ItemDataBound">
            <ItemTemplate>
                <asp:Panel ID="pnlEventGalleryItems" runat="server" CssClass="divEventGalleryItems">
                    <asp:HyperLink ID="hypEventGalleryImg" runat="server" class="swipebox"><asp:Image ID="imgEventGalleryImg" runat="server" BorderWidth="0" /></asp:HyperLink>
                </asp:Panel>
            </ItemTemplate>
        </asp:Repeater>
    </asp:Panel>--%>

    <asp:Panel ID="pnlEventGallList" runat="server" CssClass="divEventGallList event-item" Visible="false">
        <div class="event-title">
            <span class="fa-stack fa-lg icon">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-camera fa-stack-1x"></i>
            </span>
            <h2><asp:Literal ID="litPhotoHdr" runat="server"></asp:Literal></h2>
        </div>
        <asp:Repeater ID="rptEventGallery" runat="server" OnItemDataBound="rptEventGallery_ItemDataBound">
            <ItemTemplate>
                <asp:Literal ID="litDivOpen" runat="server"></asp:Literal>
                <asp:Panel ID="pnlEventGalleryItems" runat="server" CssClass="divEventGalleryItems two-columns">
                    <asp:Panel ID="pnlEventGalleryImg" runat="server" CssClass="divEventGalleryImg" type="eventGallery">
                        <div class="divEventGalleryImgInner"><asp:HyperLink ID="hypEventGalleryImg" runat="server" class="swipebox"><asp:Image ID="imgEventGalleryImg" runat="server" BorderWidth="0" /></asp:HyperLink></div>
                    </asp:Panel>
                </asp:Panel>
                <asp:Literal ID="litDivClose" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:Repeater>
         <asp:Literal ID="litDivClose2" runat="server" Visible="false"></asp:Literal>
    </asp:Panel>

    <asp:Panel ID="pnlEventVideoList" runat="server" CssClass="divEventGallList event-item" Visible="false">
        <div class="event-title">
            <span class="fa-stack fa-lg icon">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-video-camera fa-stack-1x"></i>
            </span>
            <h2><asp:Literal ID="litVideoHdr" runat="server"></asp:Literal></h2>
        </div>
        <asp:Repeater ID="rptEventVideo" runat="server" OnItemDataBound="rptEventVideo_ItemDataBound">
            <ItemTemplate>
                <asp:Literal ID="litDivOpen3" runat="server"></asp:Literal>
                <asp:Panel ID="pnlEventGalleryItems" runat="server" CssClass="divEventGalleryItems two-columns">
                    <asp:Panel ID="pnlEventGalleryImg" runat="server" CssClass="divEventGalleryImg" type="eventGallery">
                        <div class="divEventGalleryImgInner">
                            <asp:HyperLink ID="hypEventGalleryVid" runat="server" CssClass="hypEventGalleryVid">
                                <asp:Image ID="imgEventGalleryImg" runat="server" BorderWidth="0" />
                                <div class="divWaterMark">
                                    <asp:Image ID="imgVideoWatermark" runat="server" />
                                </div>
                            </asp:HyperLink>
                        </div>
                    </asp:Panel>
                </asp:Panel>
                <asp:Literal ID="litDivClose3" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:Repeater>
         <asp:Literal ID="litDivClose3" runat="server" Visible="false"></asp:Literal>
    </asp:Panel>

    <asp:Panel ID="pnlIndEventComment" runat="server" CssClass="event-comment-container event-item" Visible="false">
        <asp:Panel ID="pnlIndEventCommentHdrInner" runat="server" CssClass="divIndEventHdrInnerCmmt">
            <div class="event-title">
                <span class="icon fa-stack fa-lg icon">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-commenting fa-stack-1x"></i>
                </span>
                <h2><asp:Literal ID="litIndEvtCommentHdr" runat="server"></asp:Literal></h2>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlIndEventCommentItems" runat="server" CssClass="event-comments">
            <asp:Repeater ID="rptComment" runat="server" OnItemDataBound="rptComment_ItemDataBound">
                <ItemTemplate>
                    <div class="event-comment">
                        <div class="event-comment-author">
                            <div class="event-comment-author-name">
                                <h4><asp:Literal ID="litCommentPostedName" runat="server"></asp:Literal></h4>
                            </div>
                            <div class="event-comment-postdate">
                                <h4><asp:Literal ID="litCommentPostedDate" runat="server"></asp:Literal></h4>
                            </div>
                        </div>
                        <div class="event-comment-contet">
                            <p><asp:Literal ID="litCommentPosted" runat="server"></asp:Literal></p>
                        </div>                            
                        <asp:Panel runat="server" ID="pnlIndEvtReply" CssClass="event-comment-reply" Visible="false">
                            <i class="material-icons icon">reply</i>
                            <div><asp:Literal ID="litReplyPosted" runat="server"></asp:Literal></div>
                        </asp:Panel>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </asp:Panel>
    </asp:Panel>


    <asp:Panel ID="pnlIndEventPostCommentForm" runat="server" CssClass="divIndEventPostCommentForm">
        <asp:Panel ID="pnlIndEventPostCommentFormHdr" runat="server" CssClass="divIndEventCommentHdr">
            <h2>
                <asp:Literal ID="litPostCommentFormHdr" runat="server"></asp:Literal></h2>
        </asp:Panel>
        <asp:Panel ID="pnlPostCommentForm" runat="server" CssClass="event-comment-form">
            <table cellpadding="0" cellspacing="0" border="0" class="">
                <tr>
                    <%--<td class="tdLabelCommentForm">Name :</td>--%>
                    <td>
                        <asp:TextBox ID="txtPostCommName" runat="server" MaxLength="250" CssClass="text_big" placeholder="Name" TabIndex="1"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvPostCommName" runat="server" ErrorMessage="<br/>Please enter your name." ControlToValidate="txtPostCommName" CssClass="errmsg" Display="Dynamic" ValidationGroup="comment"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <%--<td class="tdLabelCommentForm">Email :</td>--%>
                    <td>
                        <asp:TextBox ID="txtPostCommEmail" runat="server" MaxLength="250" CssClass="text_big" placeholder="Email" TabIndex="2"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvPostCommEmail" runat="server" ErrorMessage="<br/>Please enter your email." ControlToValidate="txtPostCommEmail" CssClass="errmsg" Display="Dynamic" ValidationGroup="comment"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revPostCommEmail" runat="server" ErrorMessage="<br/>Please enter a valid email." ControlToValidate="txtPostCommEmail" CssClass="errmsg" Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="comment"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <%--<td class="tdLabelCommentForm">Comment :</td>--%>
                    <td>
                        <asp:TextBox ID="txtPostComment" runat="server" CssClass="text_big" TextMode="MultiLine" Rows="5" placeholder="Comment" TabIndex="3"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvPostComment" runat="server" ErrorMessage="<br/>Please enter your comment." ControlToValidate="txtPostComment" CssClass="errmsg" Display="Dynamic" ValidationGroup="comment"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <div class="divBtnComment">
                <asp:LinkButton ID="lnkbtnComment" runat="server" Text="Post Comment" TabIndex="4" ToolTip="Post Comment" CssClass="button button-full" OnClick="lnkbtnComment_Click" ValidationGroup="comment"></asp:LinkButton></td>
            </div>
        </asp:Panel>
    </asp:Panel>
</asp:Panel>
