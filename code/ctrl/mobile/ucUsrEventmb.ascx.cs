﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Collections;

public partial class ctrl_ucUsrEvent : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _sectId = 1;
    protected int _highId = 0;
    protected int _currentPage = 0;
    protected int _pageCount = 0;
    protected int _pageNo = 0;
    protected string _lang = "";
    protected int _pgid = 0;
    protected int _filter = 0;
    protected string _sort = "HIGH_ORDER ASC";

    protected string strSortConst = "HIGH_SHOWTOP DESC, HIGH_CREATION DESC, HIGH_ORDER ASC";
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int sectId
    {
        get
        {
            if (ViewState["SECTID"] == null)
            {
                return _sectId;
            }
            else
            {
                return Convert.ToInt16(ViewState["SECTID"]);
            }
        }
        set { ViewState["SECTID"] = value; }
    }

    public int highId
    {
        get
        {
            if (ViewState["HIGHID"] != "" && ViewState["HIGHID"] != null)
            {
                return Convert.ToInt16(ViewState["HIGHID"]);
            }
            else
            {
                return _highId;
            }
        }
        set { ViewState["HIGHID"] = value; }
    }

    public int currentPage
    {
        get
        {
            if (ViewState["CURRENTPAGE"] == null)
            {
                return _currentPage;
            }
            else
            {
                return Convert.ToInt16(ViewState["CURRENTPAGE"]);
            }
        }
        set
        {
            ViewState["CURRENTPAGE"] = value;
        }
    }

    public int pageCount
    {
        get
        {
            if (ViewState["PAGECOUNT"] == null)
            {
                return _pageCount;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGECOUNT"]);
            }
        }
        set { ViewState["PAGECOUNT"] = value; }
    }

    public int pageNo
    {
        get
        {
            if (ViewState["PAGENO"] == null)
            {
                return _pageNo;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGENO"]);
            }
        }
        set { ViewState["PAGENO"] = value; }
    }

    public int pgid
    {
        get
        {
            if (ViewState["PGID"] == null)
            {
                return _pgid;
            }
            else
            {
                return Convert.ToInt16(ViewState["PGID"]);
            }
        }
        set { ViewState["PGID"] = value; }
    }

    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }

    public int filter
    {
        get
        {
            if (ViewState["FILTER"] == null)
            {
                return _filter;
            }
            else
            {
                return Convert.ToInt16(ViewState["FILTER"]);
            }
        }
        set { ViewState["FILTER"] = value; }
    }

    public string sort
    {
        get { return ViewState["SORT"] as string ?? _sort; }
        set { ViewState["SORT"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "mobile/eventmb.css' type='text/css' rel='Stylesheet' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            setPageProperties();
            bindRptData();
        }
    }

    protected void ddlSort_SelectedIndexChanged(object sender, EventArgs e)
    {
        sort = ddlSort.SelectedValue;
        bindRptData();
    }

    protected void ddlFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        filter = Convert.ToInt16(ddlFilter.SelectedValue);
        bindRptData();
    }

    protected void rptPagination_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton lnkbtnPageNo = (LinkButton)e.Item.FindControl("lnkbtnPageNo");

            Boolean boolFound = false;
            string strQuery = Request.Url.Query;

            if (!string.IsNullOrEmpty(strQuery))
            {
                string[] strQuerySplit = strQuery.Split((char)'&');
                strQuery = "";

                for (int i = 0; i <= strQuerySplit.Length - 1; i++)
                {
                    if (strQuerySplit[i].IndexOf("pg=") >= 0)
                    {
                        boolFound = true;

                        if (i == 0) { strQuerySplit[i] = "?pg=" + Convert.ToInt16(e.Item.DataItem); }
                        else { strQuerySplit[i] = "pg=" + Convert.ToInt16(e.Item.DataItem); }
                    }

                    if (i == (strQuerySplit.Length - 1))
                    {
                        strQuery += strQuerySplit[i];
                    }
                    else
                    {
                        strQuery += strQuerySplit[i] + "&";
                    }
                }
            }
            else
            {
                boolFound = true;
                strQuery = "?pg=" + Convert.ToInt16(e.Item.DataItem);
            }

            if (!boolFound)
            {
                strQuery += "&pg=" + Convert.ToInt16(e.Item.DataItem);
            }

            lnkbtnPageNo.OnClientClick = "javascript:document.location.href='" + currentPageName + strQuery + "'; return false;";

            if (Convert.ToInt16(e.Item.DataItem) == currentPage + 1)
            {
                lnkbtnPageNo.CssClass = "btnPaginationSel";
                lnkbtnPageNo.OnClientClick = "javascript:return false;";
            }
        }
    }

    protected void rptPagination_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdPage")
        {
            currentPage = Convert.ToInt16(e.CommandArgument) - 1;
        }
    }

    protected void rptEvent_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strEventImg = DataBinder.Eval(e.Item.DataItem, "HIGH_IMAGE").ToString();
            string strEventTitle = DataBinder.Eval(e.Item.DataItem, "HIGH_TITLE").ToString();
            DateTime dtEventStartDate = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "HIGH_STARTDATE"));
            DateTime dtEventEndDate = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "HIGH_ENDDATE"));
            string strEventSnapshot = DataBinder.Eval(e.Item.DataItem, "HIGH_SNAPSHOT").ToString();
            int intHighId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "HIGH_ID").ToString());

            string strIndEventUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + "gallerymb.aspx?pgid=" + pgid + "&eid=" + intHighId;

            HyperLink hypEventImg = (HyperLink)e.Item.FindControl("hypEventImg");
            hypEventImg.NavigateUrl = strIndEventUrl;


            if (string.IsNullOrEmpty(strEventImg))
            {
                clsConfig config = new clsConfig();
                strEventImg = ConfigurationManager.AppSettings["scriptBase"] + config.defaultGroupImg;
            }
            else
            {
                strEventImg = ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMHIGHFOLDER + "/" + strEventImg;
            }
            
            Image resultImage = new Image();

            try
            {
                if (!string.IsNullOrEmpty(strEventImg))
                {
                    string strImagePath = Server.MapPath(strEventImg);
                    System.Drawing.Image objImage = System.Drawing.Image.FromFile(strImagePath);
                    resultImage = clsMis.getImageResize2(resultImage, clsSetting.CONSTUSRPRODUCTIMGWIDTH, clsSetting.CONSTUSRPRODUCTIMGHEIGHT, objImage.Width, objImage.Height);
                    objImage.Dispose();
                    objImage = null;
                }
            }
            catch (Exception ex)
            {
            }

            Image imgEvent = (Image)e.Item.FindControl("imgEvent");
            imgEvent.ImageUrl = strEventImg;
            imgEvent.AlternateText = strEventTitle;
            imgEvent.ToolTip = strEventTitle;
            imgEvent.Width = resultImage.Width;
            imgEvent.Height = resultImage.Height;
            imgEvent.Attributes["style"] = "top:" + (clsSetting.CONSTUSRPRODUCTIMGHEIGHT - resultImage.Height.Value) / 2 + "px;";
            imgEvent.Attributes["style"] += "left:" + (clsSetting.CONSTUSRPRODUCTIMGWIDTH - resultImage.Width.Value) / 2 + "px;";
            resultImage.Dispose();
            resultImage = null;

            
            HyperLink hypEventName = (HyperLink)e.Item.FindControl("hypEventName");
            hypEventName.Text = strEventTitle;
            hypEventName.ToolTip = strEventTitle;
            hypEventName.NavigateUrl = strIndEventUrl;

            Literal litEventDate = (Literal)e.Item.FindControl("litEventDate");
            Panel pnlEventDate = (Panel)e.Item.FindControl("pnlEventDate");

            if ((dtEventStartDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString()) && (dtEventEndDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString()))
            {
                if (dtEventStartDate.ToShortDateString() != dtEventEndDate.ToShortDateString())
                {
                    litEventDate.Text = dtEventStartDate.ToString("dd MMM yyyy") + " to " + dtEventEndDate.ToString("dd MMM yyyy");
                }
                else { litEventDate.Text = dtEventStartDate.ToString("dd MMM yyyy"); }
            }
            else
            {
                pnlEventDate.Visible = false;
                litEventDate.Text = "";
            }

            Literal litEventSnapshot = (Literal)e.Item.FindControl("litEventSnapshot");
            Panel pnlEventSnapshot = (Panel)e.Item.FindControl("pnlEventSnapshot");

            if (!string.IsNullOrEmpty(strEventSnapshot))
            {
                litEventSnapshot.Text = clsMis.limitString(strEventSnapshot, 350);
            }
            else
            {
                pnlEventSnapshot.Visible = true;
                litEventSnapshot.Text = "";
            }
           
        }
    }
    #endregion
 
    #region "Methods"
    protected void setPageProperties()
    {
        litShow.Text = "Show: ";
        litSort.Text = "Sort By: ";
        DataSet dsYear = new DataSet();
        DataView dvYear, dvYearCheck;
        Boolean boolStartDateNull = false;

        clsHighlight high = new clsHighlight();
        dsYear = high.getEventYear(1);
        dvYear = new DataView(dsYear.Tables[0]);

        dvYearCheck = new DataView(dsYear.Tables[0]);
        dvYearCheck.RowFilter = "HIGH_STARTDATE = '9999-12-31 11:59:59' OR HIGH_STARTDATE = null";

        if (dvYearCheck.Count > 0)
        {
            boolStartDateNull = true;
        }

        dvYear.RowFilter = "HIGH_STARTDATE <> '9999-12-31 11:59:59' OR HIGH_STARTDATE <> null";
        dvYear.Sort = "YEAR(HIGH_STARTDATE) DESC";

        ddlFilter.DataSource = dvYear;
        ddlFilter.DataTextField = "YEAR(HIGH_STARTDATE)";
        ddlFilter.DataValueField = "YEAR(HIGH_STARTDATE)";
        ddlFilter.DataBind();

        ListItem ddlYearDefaultItem = new ListItem("All", "0");
        ddlFilter.Items.Insert(0, ddlYearDefaultItem);

        if (boolStartDateNull && dvYear.Count > 0)
        {
            ddlFilter.Items.Add(new ListItem("Others", "9"));
        }

        ddlSort.Items.Add(new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), sort));
        ddlSort.Items.Add(new ListItem("Latest on Top", "HIGH_STARTDATE DESC, HIGH_SHOWTOP DESC, HIGH_CREATION DESC, HIGH_ORDER ASC"));
        ddlSort.Items.Add(new ListItem("Oldest on Top", "HIGH_STARTDATE ASC, HIGH_SHOWTOP DESC, HIGH_CREATION DESC, HIGH_ORDER ASC"));
        ddlSort.Items.Add(new ListItem("Title (A - Z)", "HIGH_TITLE ASC, " + strSortConst));
        ddlSort.Items.Add(new ListItem("Title (Z - A)", "HIGH_TITLE DESC, " + strSortConst));
    }

    protected void bindRptData()
    {
        int intPageSize = 0;

        clsHighlight high = new clsHighlight();
        DataSet ds = high.getHighlightList(0);
        DataView dv = new DataView(ds.Tables[0]);

        dv.RowFilter = "HIGH_VISIBLE = 1 AND HIGH_ACTIVE = 1";

        if (filter > 0)
        {
            ddlFilter.Items.FindByValue(filter.ToString()).Selected = true;

            if (filter == 9)
            {
                dv.RowFilter += " AND (HIGH_STARTDATE = '9999-12-31 11:59:59' OR HIGH_STARTDATE = null)";
            }
            else
            {
                dv.RowFilter += " AND (HIGH_STARTDATE >= #" + filter + "/01/01# AND HIGH_STARTDATE <= #" + filter + "/12/31#)";
            }
        }

        dv.Sort = sort;

        intPageSize = clsSetting.CONSTUSREVENTGALLERYSIZE;
        PagedDataSource pds = new PagedDataSource();
        pds.DataSource = dv;
        pds.AllowPaging = true;
        pds.PageSize = intPageSize;
        pageCount = pds.PageCount;

        if (pageNo != 0) { currentPage = pageNo - 1; }

        if (currentPage < 0) { currentPage = 0; }
        if (currentPage > pageCount - 1) { currentPage = 0; }

        pds.CurrentPageIndex = currentPage;

        rptEvent.DataSource = pds;
        rptEvent.DataBind();

        if (pageCount > 1)
        {
            pnlListPaginationTop.Visible = true;
            pnlListPaginationBottom.Visible = true;

            if (dv.Count > 0)
            {
                int intCurrentPage = currentPage + 1;
                int intStart;
                int intEnd;
                int intPaginationSize = 9;
                int intPaginationSizeMid = Convert.ToInt16(intPaginationSize / 2);

                if (intCurrentPage <= intPaginationSizeMid)
                {
                    intStart = 1;
                    if (pageCount > intPaginationSize) { intEnd = intPaginationSize; }
                    else { intEnd = pageCount; }
                }
                else if (intCurrentPage >= pageCount - intPaginationSizeMid)
                {
                    intStart = pageCount - intPaginationSize + 1;
                    if (intStart < 1) { intStart = 1; }
                    intEnd = pageCount;
                }
                else
                {
                    intStart = intCurrentPage - intPaginationSizeMid;
                    intEnd = intCurrentPage + intPaginationSizeMid;
                }

                ArrayList pages = new ArrayList();
                for (int i = intStart; i <= intEnd; i++)
                {
                    pages.Add(i);
                }

                rptPaginationTop.DataSource = pages;
                rptPaginationTop.DataBind();

                rptPaginationBottom.DataSource = pages;
                rptPaginationBottom.DataBind();
            }
        }
        else
        {
            pnlListPaginationTop.Visible = false;
            pnlListPaginationBottom.Visible = false;
        }
    }
    #endregion
}
