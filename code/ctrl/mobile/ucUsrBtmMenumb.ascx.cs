﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucUsrBtmMenumb : System.Web.UI.UserControl
{
    #region "Properties"
    protected string _lang = "";
    protected int _pgid = 0;
    protected int _parentId = 0;
    //protected MenuType _menuType = MenuType.Link;
    protected DataSet _dsMenu = new DataSet();
    #endregion

    #region "Property Methods"
    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }

    public int pgid
    {
        get { return _pgid; }
        set { _pgid = value; }
    }


    public int parentId
    {
        get { return _parentId; }
        set { _parentId = value; }
    }

    protected DataSet dsMenu
    {
        get
        {
            if (ViewState["DSMENU"] == null)
            {
                return _dsMenu;
            }
            else
            {
                return (DataSet)ViewState["DSMENU"];
            }
        }
        set { ViewState["DSMENU"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void rptBtmMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int intId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "PAGE_ID"));
            int intParent = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "PAGE_PARENT"));
            string strDisplayName = DataBinder.Eval(e.Item.DataItem, "PAGE_DISPLAYNAME").ToString();
            string strTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_TEMPLATE").ToString();
            string strMobileTemplate = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILETEMPLATE").ToString();
            string strLang = DataBinder.Eval(e.Item.DataItem, "PAGE_LANG").ToString();
            string strPageTitleFURL = DataBinder.Eval(e.Item.DataItem, "PAGE_TITLEFURL").ToString();
            string strCSSClass = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILECSSCLASS").ToString();
            int intEnableLink = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_ENABLELINK"));
            int intRedirect = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILEREDIRECT"));
            string strRedirectLink = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILEREDIRECTLINK").ToString();
            string strTarget = DataBinder.Eval(e.Item.DataItem, "PAGE_MOBILEREDIRECTTARGET").ToString();

            HyperLink hypBtmMenu = (HyperLink)e.Item.FindControl("hypBtmMenu");
            hypBtmMenu.ToolTip = strDisplayName;

            Literal litBtmMenu = (Literal)e.Item.FindControl("litBtmMenu");
            litBtmMenu.Text = strDisplayName.Replace("<br />", " ");

            Panel pnlBtmMenuItem = (Panel)e.Item.FindControl("pnlBtmMenuItem");

            hypBtmMenu.CssClass = !string.IsNullOrEmpty(strCSSClass)? hypBtmMenu.CssClass + " " + strCSSClass : hypBtmMenu.CssClass;
            if (intId == pgid || intId == parentId)
            {
                hypBtmMenu.CssClass = hypBtmMenu.CssClass + " selected";
            }

            if (intEnableLink == 0)
            {
                hypBtmMenu.NavigateUrl = string.Empty;
                hypBtmMenu.Attributes.Remove("href");
                hypBtmMenu.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "default";
            }
            else
            {
                if (intRedirect > 0)
                {
                    if (intRedirect == clsSetting.CONSTEXTERNALLINK)
                    {
                        if (!string.IsNullOrEmpty(strRedirectLink))
                        {
                            string strLink = strRedirectLink;

                            if (strRedirectLink.StartsWith("http://") || strRedirectLink.StartsWith("https://"))
                            {
                                hypBtmMenu.NavigateUrl = strLink;
                            }
                            else
                            {
                                hypBtmMenu.NavigateUrl = "http://" + strLink;
                            }

                            hypBtmMenu.Target = strTarget;
                        }
                    }
                    else
                    {
                        DataRow[] foundRow = dsMenu.Tables[0].Select("PAGE_ID = " + intRedirect + " AND PAGE_OTHER = 0");

                        if (foundRow.Length > 0)
                        {
                            int intRParent = Convert.ToInt16(foundRow[0]["PAGE_PARENT"]);
                            int intRTemplateParent = Convert.ToInt16(foundRow[0]["PAGE_TEMPLATEPARENT"]);
                            string strRTemplateParent = foundRow[0]["V_MOBILETEMPLATEPARENT"].ToString();
                            string strRTemplate = intRParent > 0 ? (intRTemplateParent > 0 ? strRTemplateParent : strMobileTemplate) : strMobileTemplate;

                            if (!string.IsNullOrEmpty(strRTemplate))
                            {
                                if (!string.IsNullOrEmpty(lang)) { hypBtmMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strRTemplate + "?pgid=" + intRedirect + "&lang=" + strLang; }
                                else { hypBtmMenu.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strRTemplate + "?pgid=" + intRedirect; }
                            }
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(strMobileTemplate))
                    {
                        if (!string.IsNullOrEmpty(lang)) { hypBtmMenu.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strMobileTemplate + "?pgid=" + intId + "&lang=" + strLang; }
                        else { hypBtmMenu.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strMobileTemplate + "?pgid=" + intId; }
                    }
                }
            }
            
        }
        if (e.Item.ItemType == ListItemType.Separator)
        {
        }
    }
    #endregion

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            bindRptData();
        }
    }

    protected void bindRptData()
    {

        DataSet ds = new clsPage().getMobilePageList(1, 1);
        DataView dv = new DataView(ds.Tables[0]);

        dsMenu = ds.Clone();

        dv.RowFilter = "PAGE_OTHER = 0 AND PAGE_MOBILESHOWBOTTOM = 1 AND PAGE_PARENT = 0";
        dv.Sort = "PAGE_ORDER ASC";
        
        if(Session[clsKey.SESSION_MEMBER_ID] == null)
        {
            dv.RowFilter += " AND PAGE_MOBILETEMPLATE <> 'memberprofilemb.aspx'";
        }
        else
        {
            dv.RowFilter += " AND PAGE_MOBILETEMPLATE <> 'loginmb.aspx'";
        }

        if (dv.Count > 0)
        {
            rptBtmMenu.DataSource = dv;
            rptBtmMenu.DataBind();
        }
    }
    #endregion
}
