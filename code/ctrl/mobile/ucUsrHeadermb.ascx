﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrHeadermb.ascx.cs" Inherits="ctrl_ucUsrHeader" %>
<%@ Register Src="~/ctrl/ucUsrCMSContainer.ascx" TagName="UsrCMSContainer" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/mobile/ucUsrSideMenumb.ascx" TagName="UsrSideMenu" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrLogo.ascx" TagName="UsrLogo" TagPrefix="uc" %>

<div class="sidebar-nav-container" runat="server" visible="false">
    <uc:UsrSideMenu ID="ucUsrSideMenu" runat="server" />
</div>
<asp:Panel ID="pnlBackToTopContainer" runat="server" CssClass="divBackToTopContainer">
    <asp:HyperLink ID="hypBackToTop" runat="server" onclick="goTop();" CssClass="hypBackToTop" ToolTip="Back to Top"></asp:HyperLink>
</asp:Panel>
<div class="header-inner">
    <div class="logo-container">
        <uc:UsrLogo ID="ucUsrLogo" runat="server" toolTip="Acond" imageFilename="logo.png" viewType="MobileView" />
    </div>
    <div class="contact-container" runat="server" visible="false">
        <asp:HyperLink ID="hypQuickContact" runat="server" CssClass="contact-info contact-tel" Visible="false"></asp:HyperLink>
    </div>
    <a class="sidebar-nav-toggle" runat="server" visible="false">
        <span></span>
        <span></span>
        <span></span>
    </a>
    <div class="header-title">
        <asp:Literal runat="server" ID="litHeaderText"></asp:Literal>
    </div>
</div>