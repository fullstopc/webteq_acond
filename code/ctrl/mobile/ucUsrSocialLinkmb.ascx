﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrSocialLinkmb.ascx.cs" Inherits="ctrl_ucUsrSocialLink" %>
<%@ Register Src="~/ctrl/ucUsrFacebook.ascx" TagName="UsrFacebook" TagPrefix="uc" %>

<asp:Panel ID="pnlFbContainer" runat="server" CssClass="fbpage-container" Visible="false">
    <asp:HyperLink ID="hypFacebook" runat="server" CssClass="hypFacebook">
        <i class="fa fa-facebook" aria-hidden="true"></i>
    </asp:HyperLink>
</asp:Panel>
<asp:Panel ID="pnlFbLike" runat="server" CssClass="fblike-container" Visible="false">
    <uc:UsrFacebook ID="ucUsrFacebook" runat="server" />
</asp:Panel>
<asp:Panel ID="pnlTwitterContainer" runat="server" CssClass="divTwitterContainer" Visible="false">
    <asp:HyperLink ID="hypTwitter" runat="server" CssClass="hypTwitter" Height="16" Width="16"></asp:HyperLink>
</asp:Panel>
<asp:Panel ID="pnlInstagramContainer" runat="server" CssClass="divInstagramContainer" Visible="false">
    <asp:HyperLink ID="hypInstagram" runat="server" CssClass="hypInstagram" Height="16" Width="16"></asp:HyperLink>
</asp:Panel>
<asp:Panel ID="pnlLinkedInContainer" runat="server" CssClass="divLinkedInContainer" Visible="false">
    <asp:HyperLink ID="hypLinkedIn" runat="server" CssClass="hypLinkedIn" Height="16" Width="16"></asp:HyperLink>
</asp:Panel>
<asp:Panel ID="pnlYouTubeContainer" runat="server" CssClass="divYouTubeContainer" Visible="false">
    <asp:HyperLink ID="hypYouTube" runat="server" CssClass="hypYouTube" Height="16" Width="16"></asp:HyperLink>
</asp:Panel>
<asp:Panel ID="pnlGooglePlusContainer" runat="server" CssClass="divGooglePlusContainer" Visible="false">
    <asp:HyperLink ID="hypGooglePlus" runat="server" CssClass="hypGooglePlus" Height="16" Width="16"></asp:HyperLink>
</asp:Panel>