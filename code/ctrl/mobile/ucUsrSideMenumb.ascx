﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrSideMenumb.ascx.cs" Inherits="ctrl_mobile_ucUsrSideMenumb" %>
<%@ Register Src="~/ctrl/mobile/ucUsrTopMenumb.ascx" TagName="UsrTopMenu" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/mobile/ucUsrSocialLinkmb.ascx" TagName="UsrSocialLink" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrCopy.ascx" TagName="UsrCopy" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrCMSContainer.ascx" TagName="UsrCMSContainer" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/mobile/ucUsrWebsiteViewmb.ascx" TagName="UsrWebsiteView" TagPrefix="uc" %>

<asp:Panel ID="pnlSideMenu" runat="server" CssClass="sidebar-nav-inner">
    <asp:Panel ID="pnlCtnBannerTop" runat="server" CssClass="banner-container">
        <uc:UsrCMSContainer ID="ucUsrContainerBannerTop" runat="server" cmsType="10" blockId="5" />
    </asp:Panel>
    <asp:Panel ID="pnlTopMenuContainer" runat="server" CssClass="sidemenu-container" >                                                                                        
        <uc:UsrTopMenu ID="ucUsrTopMenu" runat="server" />                      
    </asp:Panel>
     <asp:Panel ID="pnlSocialLinkContainer" runat="server" CssClass="socials-container">
        <uc:UsrSocialLink ID="ucUsrSocialLink" runat="server" />
    </asp:Panel>
    <asp:Panel ID="pnlSwitchDesktop" runat="server" CssClass="desktopview-container">
       <uc:UsrWebsiteView ID="UsrWebsiteView" runat="server" />
    </asp:Panel>
    <asp:Panel ID="pnlCopyrightContainer" runat="server" CssClass="copyright-container">
        <uc:UsrCopy ID="ucUsrCopy" runat="server" />
    </asp:Panel>
</asp:Panel>





