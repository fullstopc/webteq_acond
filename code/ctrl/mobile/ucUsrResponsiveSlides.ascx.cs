﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class ctrl_ucUsrResponsiveSlides : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _pageId = 0;
    protected int _height = 0;
    protected int _width = 0;
    protected int _grpId = 0;
    #endregion

    #region "Property Methods"
    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public int height
    {
        get { return _height; }
        set { _height = value; }
    }

    public int width
    {
        get { return _width; }
        set { _width = value; }
    }

    public int grpId
    {
        get { return _grpId; }
        set { _grpId = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "mobile/responsiveslides.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            clsPage page = new clsPage();
            if (page.extractPageById(pageId, 1))
            {
                grpId = page.mobileSlideShowGroup;
            }

            bindRptData();
        }
    }

    protected void rptHomeMasthead_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strMastheadTitle = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_NAME").ToString();
            string strMastheadId = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_ID").ToString();
            string strMastheadImage = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_IMAGE").ToString();
            int intMastheadClick = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "MASTHEAD_CLICKABLE"));
            string strMastheadUrl = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_URL").ToString();
            string strMastheadTarget = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_TARGET").ToString();
            string strtagline1 = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_TAGLINE1").ToString();
            string strtagline2 = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_TAGLINE2").ToString();

            strMastheadImage = ConfigurationManager.AppSettings["uplBase"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + strMastheadImage;

            System.Drawing.Image img = System.Drawing.Image.FromFile(Server.MapPath(strMastheadImage));
            int intHeight = img.Height;
            int intWidth = img.Width;
            img.Dispose();

            Image imgMasthead = (Image)e.Item.FindControl("imgMasthead");
            Image imgMastheadImage = (Image)e.Item.FindControl("imgMastheadImage");

            imgMasthead.ImageUrl = ConfigurationManager.AppSettings["imgBase"] + "cmn/trans.gif";
            imgMasthead.ImageUrl = strMastheadImage;
            imgMasthead.AlternateText = strMastheadTitle;
            imgMasthead.ToolTip = strMastheadTitle;
            //imgMasthead.Width = intWidth;
            //imgMasthead.Height = intHeight;
            pnlSliderContainer.Width = pnlMastheadSlider.Width = intWidth - 1;
            pnlSliderContainer.Height = pnlMastheadSlider.Height = intHeight - 1;

            Literal Littag1 = (Literal)e.Item.FindControl("Littag1");
            Littag1.Text = strtagline1;
            Literal Littag2 = (Literal)e.Item.FindControl("Littag2");
            Littag2.Text = strtagline2;

            HyperLink hypMasthead = (HyperLink)e.Item.FindControl("hypMasthead");
            if (intMastheadClick > 0)
            {
                hypMasthead.Enabled = true;
                hypMasthead.ToolTip = strMastheadTitle;
                hypMasthead.Target = strMastheadTarget;

                if (strMastheadUrl.StartsWith("https://") || strMastheadUrl.StartsWith("http://"))
                {
                    hypMasthead.NavigateUrl = strMastheadUrl;
                }
                else
                {
                    hypMasthead.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + strMastheadUrl;
                }
            }
        }
    }
    #endregion

    #region "Methods"
    protected void bindRptData()
    {
        clsMasthead masthead = new clsMasthead();

        if (Application["MASTHEAD"] == null)
        {
            Application["MASTHEAD"] = masthead.getMastheadImage2();
        }

        DataSet dsMasthead = (DataSet)Application["MASTHEAD"];
        DataView dvMasthead = new DataView(dsMasthead.Tables[0]);

        dvMasthead.RowFilter = "MASTHEAD_ACTIVE = 1 AND GRP_ID = " + grpId;
        dvMasthead.Sort = "MASTHEAD_NAME ASC";

        if(dvMasthead.Count > 0)
        {
            rptHomeMasthead.DataSource = dvMasthead;
            rptHomeMasthead.DataBind();
        }
        
    }
    #endregion
}
