﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucUsrSocialLink : System.Web.UI.UserControl
{
    #region "Properties"
    protected string _toolTip;
    #endregion

    #region "Property Methods"
    public string toolTip
    {
        get { return _toolTip; }
        set { _toolTip = value; }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "mobile/sociallinkmb.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        clsConfig config = new clsConfig();
        Boolean boolSocial = false;

        //Facebook
        Facebook facebook = new Facebook();
        facebook.setState();

        if (!string.IsNullOrEmpty(facebook.pageUrl))
        {
            pnlFbContainer.Visible= true;
            hypFacebook.NavigateUrl = facebook.pageUrl;
            hypFacebook.Target = "_blank";
            hypFacebook.ToolTip = "Find us on Facebook";
            boolSocial = true;
        }

        // Facebook Like
        if (!String.IsNullOrEmpty(facebook.type) && 
            !String.IsNullOrEmpty(facebook.websiteUrl) && 
            !String.IsNullOrEmpty(facebook.pageName))
        {

            ucUsrFacebook.entityTitle = facebook.title;
            ucUsrFacebook.entityType = facebook.type;
            ucUsrFacebook.urlToLike = facebook.websiteUrl;
            ucUsrFacebook.imageFile = facebook.imageUrl;
            ucUsrFacebook.siteName = facebook.pageName;
            ucUsrFacebook.entityDesc = facebook.desc;

            ucUsrFacebook.fill();

            pnlFbLike.Visible = true;
            boolSocial = true;
        }


        //Google Plus
        config.extractItemByNameGroup(clsConfig.CONSTNAMEGOOGLEPLUSURL, clsConfig.CONSTGROUPGOOGLEPLUS, 1);
        if (!string.IsNullOrEmpty(config.value))
        {
            pnlGooglePlusContainer.Visible = true;
            hypGooglePlus.NavigateUrl = config.value;
            hypGooglePlus.Target = "_blank";
            hypGooglePlus.ToolTip = "Find us on Google+";
            boolSocial = true;
        }

        //LinkedIn
        config.extractItemByNameGroup(clsConfig.CONSTNAMELINKEDINURL, clsConfig.CONSTGROUPLINKEDIN, 1);
        if (!string.IsNullOrEmpty(config.value))
        {
            pnlLinkedInContainer.Visible = true;
            hypLinkedIn.NavigateUrl = config.value;
            hypLinkedIn.Target = "_blank";
            hypLinkedIn.ToolTip = "Find us on LinkIn";
            boolSocial = true;
        }

        //Instagram
        config.extractItemByNameGroup(clsConfig.CONSTNAMEINSTAGRAMURL, clsConfig.CONSTGROUPINSTAGRAM, 1);
        if (!string.IsNullOrEmpty(config.value))
        {
            pnlInstagramContainer.Visible = true;
            hypInstagram.NavigateUrl = config.value;
            hypInstagram.Target = "_blank";
            hypInstagram.ToolTip = "Find us on Instagram";
            boolSocial = true;
        }

        //YouTube
        config.extractItemByNameGroup(clsConfig.CONSTNAMEYOUTUBEURL, clsConfig.CONSTGROUPYOUTUBE, 1);
        if (!string.IsNullOrEmpty(config.value))
        {
            pnlYouTubeContainer.Visible = true;
            hypYouTube.NavigateUrl = config.value;
            hypYouTube.Target = "_blank";
            hypYouTube.ToolTip = "Find us on YouTube";
            boolSocial = true;
        }

        //Twitter
        config.extractItemByNameGroup(clsConfig.CONSTNAMETWITTERURL, clsConfig.CONSTGROUPTWITTER, 1);
        if (!string.IsNullOrEmpty(config.value))
        {
            pnlTwitterContainer.Visible = true;
            hypTwitter.NavigateUrl = config.value;
            hypTwitter.Target = "_blank";
            hypTwitter.ToolTip = "Find us on Twitter";
            boolSocial = true;
        }

        //Hide the SideMenu Social Control if no Social Website
        if (!boolSocial)
        {
            Panel pnlSocialLinkContainer = (Panel)Parent.FindControl("pnlSocialLinkContainer");
            pnlSocialLinkContainer.Visible = false;
        }
    }
}