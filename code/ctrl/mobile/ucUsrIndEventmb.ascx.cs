﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class ctrl_ucUsrIndEvent : System.Web.UI.UserControl
{
    #region "Property"
    protected int _pageId = 0;
    protected int _intEventId = 0;
    protected string _lang = "";
    protected int intEventImgCount = 0;
    protected int intEventVidCount = 0;
    //xY
    protected int intEventImgNoInRow = 2;
    protected Boolean litdivClose = true;
    protected Boolean litdivClose3 = true;

    protected string strPostedName;
    protected string strPostedEmail;
    protected string strPostedComment;
    protected Boolean boolAllowComment = false;
    #endregion


    #region "Properties Methods"
    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }
    public int intEventId
    {
        get { return _intEventId; }
        set { _intEventId = value; }
    }
    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.LoadComplete += new EventHandler(Page_LoadComplete);

        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "mobile/indeventmb.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            setPageProperties();
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        bindRptData();
        if (litdivClose == false)
        {
            litDivClose2.Visible = true;
            litDivClose2.Text = "</div>";
        }

        if (litdivClose3 == false)
        {
            litDivClose3.Visible = true;
            litDivClose3.Text = "</div>";
        }
    }

    protected void rptEventGallery_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strHighTitle = DataBinder.Eval(e.Item.DataItem, "HIGHGALL_TITLE").ToString();
            string strHighTitleJp = DataBinder.Eval(e.Item.DataItem, "HIGHGALL_TITLE_JP").ToString();
            string strHighTitleZh = DataBinder.Eval(e.Item.DataItem, "HIGHGALL_TITLE_ZH").ToString();
            string strHighId = DataBinder.Eval(e.Item.DataItem, "HIGH_ID").ToString();
            string strHighImageName = DataBinder.Eval(e.Item.DataItem, "HIGHGALL_IMAGE").ToString();

            string strHighImage = ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMHIGHFOLDER + "/" + clsSetting.CONSTADMHIGHGALLERYFOLDER + "/" + strHighImageName;

            HyperLink hypEventGalleryImg = (HyperLink)e.Item.FindControl("hypEventGalleryImg");
            hypEventGalleryImg.NavigateUrl = strHighImage;

            if (lang == "ja")
            {
                hypEventGalleryImg.ToolTip = strHighTitleJp;
            }
            else if (lang == "zh")
            {
                hypEventGalleryImg.ToolTip = strHighTitleZh;
            }
            else
            {
                hypEventGalleryImg.ToolTip = strHighTitle;
            }

            hypEventGalleryImg.Attributes["rel"] = "swipebox[event" + strHighId + "]";

            Image resultImage = new Image();
            try
            {
                if (!string.IsNullOrEmpty(strHighImage))
                {
                    string strImagePath = Server.MapPath(strHighImage);
                    System.Drawing.Image objImage = System.Drawing.Image.FromFile(strImagePath);
                    resultImage = clsMis.getImageResize2(resultImage, clsSetting.CONSTUSRMOBILEPHOTOGALLERYIMGWIDTHINDE, clsSetting.CONSTUSRMOBILEPHOTOGALLERYIMGHEIGHTINDE, objImage.Width, objImage.Height);
                    objImage = null;
                }
            }
            catch (Exception ex)
            {
            }

            // create thumbmail
            string filePath = Server.MapPath("~/data/" + clsSetting.CONSTADMHIGHFOLDER + "/" + clsSetting.CONSTADMHIGHGALLERYFOLDER + "/thumb/");
            if (!System.IO.File.Exists(filePath + "thumb_" + strHighImageName))
            {
                System.Drawing.Bitmap bitThumb = GenerateThumbnails(new System.Drawing.Bitmap(Server.MapPath(strHighImage)), (int)resultImage.Width.Value, (int)resultImage.Height.Value);
                bitThumb.Save(filePath + "thumb_" + strHighImageName);
            }
            string strThumbImage = ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMHIGHFOLDER + "/" + clsSetting.CONSTADMHIGHGALLERYFOLDER + "/thumb/thumb_" + strHighImageName;
            //end by create thumbnail

            Image imgEventGalleryImg = (Image)e.Item.FindControl("imgEventGalleryImg");
            imgEventGalleryImg.ImageUrl = strHighImage;

            if (lang == "ja")
            {
                imgEventGalleryImg.AlternateText = strHighTitleJp;
                imgEventGalleryImg.ToolTip = strHighTitleJp;
            }
            else if (lang == "zh")
            {
                imgEventGalleryImg.AlternateText = strHighTitleZh;
                imgEventGalleryImg.ToolTip = strHighTitleZh;
            }
            else
            {
                imgEventGalleryImg.AlternateText = strHighTitle;
                imgEventGalleryImg.ToolTip = strHighTitle;
            }

            imgEventGalleryImg.Width = resultImage.Width;
            imgEventGalleryImg.Height = resultImage.Height;

            imgEventGalleryImg.Attributes["data-src"] = strThumbImage;
            imgEventGalleryImg.CssClass = "lazyload";

            imgEventGalleryImg.Attributes["style"] = "top:" + ((clsSetting.CONSTUSRPHOTOGALLERYIMGHEIGHTINDE - resultImage.Height.Value) / 2) + "px;";
            imgEventGalleryImg.Attributes["style"] += "left:" + ((clsSetting.CONSTUSRPHOTOGALLERYIMGWIDTHINDE - resultImage.Width.Value) / 2).ToString().Replace(",", ".") + "px;";
            resultImage.Dispose();
            resultImage = null;

            intEventImgCount += 1;
            Literal litDivOpen = (Literal)e.Item.FindControl("litDivOpen");
            Literal litDivClose = (Literal)e.Item.FindControl("litDivClose");
            switch (intEventImgCount % intEventImgNoInRow)
            {
                case 1:
                    litDivOpen.Text = "<div class=\"divEventRow\">";
                    litdivClose = false;
                    break;
                case 0:
                    litDivClose.Text = "</div>";
                    litdivClose = true;

                    Panel pnlEventGalleryItems = (Panel)e.Item.FindControl("pnlEventGalleryItems");
                    pnlEventGalleryItems.CssClass = "divEventGalleryItemsLast two-columns";
                    break;
            }
        }
    }

    protected void rptEventVideo_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strVideoImage = DataBinder.Eval(e.Item.DataItem, "HIGHVIDEO_THUMBNAIL").ToString();
            string strVideoLink = DataBinder.Eval(e.Item.DataItem, "HIGHVIDEO_VIDEO").ToString();

            HyperLink hypEventGalleryVid = (HyperLink)e.Item.FindControl("hypEventGalleryVid");
            Image imgEventGalleryImg = (Image)e.Item.FindControl("imgEventGalleryImg");

            imgEventGalleryImg.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
            hypEventGalleryVid.Target = "_blank";

            hypEventGalleryVid.NavigateUrl = strVideoLink;
            hypEventGalleryVid.Attributes["style"] = "background-image:url('" + strVideoImage + "')";

            Image imgVideoWatermark = (Image)e.Item.FindControl("imgVideoWatermark");
            imgVideoWatermark.ImageUrl = ConfigurationManager.AppSettings["imagebase"] + "cmn/trans.gif";

            intEventVidCount += 1;
            Literal litDivOpen3 = (Literal)e.Item.FindControl("litDivOpen3");
            Literal litDivClose3 = (Literal)e.Item.FindControl("litDivClose3");
            switch (intEventVidCount % intEventImgNoInRow)
            {
                case 1:
                    litDivOpen3.Text = "<div class=\"divEventRow\">";
                    litdivClose3 = false;
                    break;
                case 0:
                    litDivClose3.Text = "</div>";
                    litdivClose3 = true;

                    Panel pnlEventGalleryItems = (Panel)e.Item.FindControl("pnlEventGalleryItems");
                    pnlEventGalleryItems.CssClass = "divEventGalleryItemsLast two-columns";
                    break;
            }
        }
    }

    # region "Comment"
    protected void rptComment_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strCommPostedBy = DataBinder.Eval(e.Item.DataItem, "HIGHCOMM_POSTEDBYNAME").ToString();
            string strCommPosted = DataBinder.Eval(e.Item.DataItem, "HIGHCOMM_COMMENT").ToString();
            DateTime dtPostedDate = DataBinder.Eval(e.Item.DataItem, "HIGHCOMM_POSTEDDATE") != DBNull.Value ? DateTime.Parse(DataBinder.Eval(e.Item.DataItem, "HIGHCOMM_POSTEDDATE").ToString()) : DateTime.MaxValue;
            string strCommRepliedBy = DataBinder.Eval(e.Item.DataItem, "HIGHREPLY_REPLIEDBYNAME").ToString();
            string strCommReplied = DataBinder.Eval(e.Item.DataItem, "HIGHREPLY_COMMENT").ToString();
            DateTime dtRepliedDate = DataBinder.Eval(e.Item.DataItem, "HIGHREPLY_REPLIEDDATE") != DBNull.Value ? DateTime.Parse(DataBinder.Eval(e.Item.DataItem, "HIGHREPLY_REPLIEDDATE").ToString()) : DateTime.MaxValue;

            Literal litCommentPostedName = (Literal)e.Item.FindControl("litCommentPostedName");
            litCommentPostedName.Text = strCommPostedBy;

            if (dtPostedDate != DateTime.MaxValue)
            {
                Literal litCommentPostedDate = (Literal)e.Item.FindControl("litCommentPostedDate");
                litCommentPostedDate.Text = dtPostedDate.Day.ToString() + " " + dtPostedDate.ToString("MMM") + " " + dtPostedDate.Year.ToString();
            }

            Literal litCommentPosted = (Literal)e.Item.FindControl("litCommentPosted");
            litCommentPosted.Text = strCommPosted;

            if (!string.IsNullOrEmpty(strCommReplied))
            {
                Panel pnlIndEvtReply = (Panel)e.Item.FindControl("pnlIndEvtReply");
                pnlIndEvtReply.Visible = true;

                string[] strCommRepliedBySplit = strCommRepliedBy.Split((char)'@');

                //Literal litReplyPostedName = (Literal)e.Item.FindControl("litReplyPostedName");
                //litReplyPostedName.Text = strCommRepliedBySplit[0];

                //if (dtRepliedDate != DateTime.MaxValue)
                //{
                //    Literal litReplyPostedDate = (Literal)e.Item.FindControl("litReplyPostedDate");
                //    litReplyPostedDate.Text = dtRepliedDate.Day.ToString() + " " + dtRepliedDate.ToString("MMM") + " " + dtRepliedDate.Year.ToString();
                //}

                Literal litReplyPosted = (Literal)e.Item.FindControl("litReplyPosted");
                litReplyPosted.Text = strCommReplied;
            }
        }
    }

    protected void lnkbtnComment_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (!string.IsNullOrEmpty(txtPostCommName.Text.Trim())) { strPostedName = txtPostCommName.Text.Trim(); }
            else { strPostedName = "Anonymous"; }

            strPostedEmail = txtPostCommEmail.Text.Trim();
            strPostedComment = txtPostComment.Text.Trim();
            strPostedComment = strPostedComment.Replace(Environment.NewLine, "<br/>");

            clsHighlight high = new clsHighlight();
            int intRecordAffected = 0;
            intRecordAffected = high.addHighlightComment(intEventId, strPostedComment, 1, strPostedName, strPostedEmail);

            if (intRecordAffected == 1)
            {
                clsMis.resetListing();
                //trAck.Visible = true;
                //litAck.Text = "Your comment is posted.";
                Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + "gallerymb.aspx?pgid=" + pageId + "&eid=" + intEventId);
            }
        }
    }
    #endregion

    #region "Article"
    protected void rptArticle_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strArtTitle = DataBinder.Eval(e.Item.DataItem, "HIGHART_TITLE").ToString();
            string strArtFile = DataBinder.Eval(e.Item.DataItem, "HIGHART_FILE").ToString();

            HyperLink hypArticle = (HyperLink)e.Item.FindControl("hypArticle");
            hypArticle.NavigateUrl = ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMHIGHFOLDER + "/" + clsSetting.CONSTADMHIGHARTICLEFOLDER + "/" + strArtFile + "?" + clsSetting.CONSTUSRARTICLETHICKBOX;
        }
    }

    protected void rptArticle_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
    }
    #endregion
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        litPhotoHdr.Text = "Photo";
        litVideoHdr.Text = "Video";
        litIndEvtCommentHdr.Text = "Comment";
        litPostCommentFormHdr.Text = "Leave Your Comment";

        clsHighlight high = new clsHighlight();

        if (high.extractHighlightById(intEventId, 1))
        {
            litIndEventTitle.Text = high.highTitle;

            DateTime dtEventStartDate = high.highStartDate;
            DateTime dtEventEndDate = high.highEndDate;
            if ((dtEventStartDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString()) && (dtEventEndDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString()))
            {
                if (dtEventStartDate.ToShortDateString() != dtEventEndDate.ToShortDateString())
                {
                    litIndEventDate.Text = dtEventStartDate.ToString("dd MMM yyyy") + " to " + dtEventEndDate.ToString("dd MMM yyyy");
                }
                else { litIndEventDate.Text = dtEventStartDate.ToString("dd MMM yyyy"); }
            }
            else if (dtEventStartDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
            {
                litIndEventDate.Text = dtEventStartDate.ToString("MMM yyyy");
            }
            else
            {
                pnlIndEventDate.Visible = false;
                litIndEventDate.Text = "";
            }


            if (!string.IsNullOrEmpty(high.highShortDesc))
            {
                pnlIndEventDesc.Visible = true;
                litIndEventDesc.Text = high.highShortDesc;
            }

            if (!string.IsNullOrEmpty(high.highDesc))
            {
                pnlIndEventDesc2.Visible = true;
                litIndEventDesc2.Text = high.highDesc;
            }

            if (high.highAllowComment == 1)
            {
                pnlIndEventPostCommentForm.Visible = true;
                boolAllowComment = true;
            }
            else
            {
                pnlIndEventPostCommentForm.Visible = false;
            }
        }
    }

    protected void bindRptData()
    {
        clsHighlight high = new clsHighlight();
        DataSet ds = new DataSet();
        DataView dv = new DataView();

        //album
        if (Application["HIGHLIGHTGALLERY"] == null)
        {
            Application["HIGHLIGHTGALLERY"] = high.getHighlightGallery();
        }

        DataSet dsPhoto = (DataSet)Application["HIGHLIGHTGALLERY"];
        DataView dvPhoto = new DataView(dsPhoto.Tables[0]);

        if (intEventId > 0)
        {
            dvPhoto.RowFilter = "HIGH_ID = " + intEventId;
        }

        dvPhoto.Sort = "HIGHGALL_ORDER ASC";

        if (dvPhoto.Count > 0)
        {
            pnlEventGallList.Visible = true;
            rptEventGallery.DataSource = dvPhoto;
            rptEventGallery.DataBind();
        }

        //video
        if (Application["HIGHLIGHTVIDEOS"] == null)
        {
            Application["HIGHLIGHTVIDEOS"] = high.getHighlightVideos();
        }

        DataSet dsVideo = (DataSet)Application["HIGHLIGHTVIDEOS"];
        DataView dvVideo = new DataView(dsVideo.Tables[0]);

        if (intEventId > 0)
        {
            dvVideo.RowFilter = "HIGH_ID = " + intEventId;
        }

        dvVideo.Sort = "HIGHVIDEO_ID ASC";

        if (dvVideo.Count > 0)
        {
            pnlEventVideoList.Visible = true;
            rptEventVideo.DataSource = dvVideo;
            rptEventVideo.DataBind();
        }

        /*Highlight Comment*/
        if (Application["HIGHLIGHTCOMMENT"] == null)
        {
            Application["HIGHLIGHTCOMMENT"] = high.getHighlightComments();
        }

        ds = high.getHighlightComments();
        dv = new DataView(ds.Tables[0]);

        if (intEventId > 0)
        {
            dv.RowFilter = "HIGH_ID = " + intEventId;
        }

        dv.Sort = "HIGHCOMM_POSTEDDATE DESC";

        if (dv.Count > 0 && boolAllowComment)
        {
            pnlIndEventComment.Visible = true;

            rptComment.DataSource = dv;
            rptComment.DataBind();
        }
        /*End of Highlight Comment*/

        /*Highlight Article*/
        if (Application["HIGHLIGHTARTICLE"] == null)
        {
            Application["HIGHLIGHTARTICLE"] = high.getHighlightArticles();
        }

        ds = (DataSet)Application["HIGHLIGHTARTICLE"];
        dv = new DataView(ds.Tables[0]);

        if (intEventId > 0)
        {
            dv.RowFilter = "HIGH_ID = " + intEventId;
        }

        dv.Sort = "HIGHART_TITLE ASC";

        if (dv.Count > 0)
        {
            pnlIndEventArticle.Visible = true;

            rptArticle.DataSource = dv;
            rptArticle.DataBind();
        }
        /*End of Highlight Article*/
    }

    private System.Drawing.Bitmap GenerateThumbnails(System.Drawing.Bitmap srcImage, int width, int height)
    {

        int newWidth = width;
        int newHeight = height;
        System.Drawing.Bitmap newImage = new System.Drawing.Bitmap(newWidth, newHeight);

        using (System.Drawing.Graphics gr = System.Drawing.Graphics.FromImage(newImage))
        {
            gr.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            gr.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            gr.DrawImage(srcImage, new System.Drawing.Rectangle(0, 0, newWidth, newHeight));
        }
        return newImage;
    }
    #endregion
}
