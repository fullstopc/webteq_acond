﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrFootermb.ascx.cs" Inherits="ctrl_ucUsrFooter" %>

<%@ Register Src="~/ctrl/ucUsrCopy.ascx" TagName="UsrCopy" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/mobile/ucUsrQuickLinksmb.ascx" TagName="UsrQuickLinks" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/mobile/ucUsrWebsiteViewmb.ascx" TagName="UsrWebsiteView" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/mobile/ucUsrWebteqmb.ascx" TagName="UsrWebteq" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/mobile/ucUsrBtmMenumb.ascx" TagName="UsrBtmMenu" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrCMSContainer.ascx" TagName="UsrCMSContainer" TagPrefix="uc" %>

<div class="footer-bottommenu">
    <ul class="nav nav-one" runat="server" visible="false">
        <li><a class="booking" href="bookingmb.aspx"><i></i><span>Book A Job</span></a></li>
        <li><a class="history" href="historymb.aspx"><i></i><span>Book A Job</span></a></li>
        <li><a class="marked"><i></i><span>Book A Job</span></a></li>
        <li><a class="chat"><i></i><span>Book A Job</span></a></li>
        <li><a class="login"><i></i><span>Book A Job</span></a></li>
    </ul>
    <uc:UsrBtmMenu ID="ucUsrBtmMenu" runat="server" />
</div>
<asp:Panel ID="pnlCtnBanner" runat="server" CssClass="divCtnBanner" Visible="false">
    <uc:UsrCMSContainer ID="ucUsrCMSFooter" runat="server" cmsType="10" blockId="3" />
</asp:Panel>
<asp:Panel ID="pnlQuickInfoContainer" runat="server" CssClass="footer-contact" Visible="false">
    <uc:UsrQuickLinks ID="ucUsrQuickLinks" runat="server" />
</asp:Panel>
<asp:Panel ID="pnlCopyright" runat="server" CssClass="footer-copyright" Visible="false">
    <uc:UsrCopy ID="ucUsrCopy" runat="server" />
</asp:Panel>
<asp:Panel ID="pnlWebteqContainer" runat="server" CssClass="footer-webteq" Visible="false">
    <uc:UsrWebteq ID="ucUsrWebteq" runat="server" />
</asp:Panel>