﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCmnHeadermb.ascx.cs" Inherits="ctrl_ucAdmCMSControlPanel" %>
<meta name='viewport' content='initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no'>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

        <link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>reset.css" rel="Stylesheet" />
        <link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>bootstrap-grid.css" rel="Stylesheet" />

        <%--<link rel="stylesheet" href="<% =System.Configuration.ConfigurationManager.AppSettings["libBase"] %>jQueryMobile/jquery.mobile-1.4.5.css" />--%>
        <link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>on-off-switch.css" rel="Stylesheet" />
        <link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["libBase"] %>noty-master/noty.css" rel="Stylesheet" />
        <link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>mobile/navmb.css" rel="Stylesheet" />
        <link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>mobile/publicmb.css?ts=201705251620" rel="Stylesheet" />

        <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>moment.js" type="text/javascript"></script>
        <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>config.js" type="text/javascript"></script>
        <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jquery-1.11.1.min.js" type="text/javascript"></script>
        <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>common.js" type="text/javascript"></script>
        <script src="<% =System.Configuration.ConfigurationManager.AppSettings["libBase"] %>noty-master/noty.min.js"></script>
        <%--<script src="<% =System.Configuration.ConfigurationManager.AppSettings["libBase"] %>jQueryMobile/jquery.mobile-1.4.5.min.js"></script>--%>

        <script type="text/javascript">
            ////Viewport for mobile
            //if (navigator.userAgent.match(/(iPad)/g)) {
            //    document.write("<meta name='viewport' content='initial-scale=2.3, user-scalable=no'>"); // or whichever meta tags make sense for your site
            //} else {
            //    document.write("<meta name='viewport' content='initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no'>"); // again, which ever meta tags you need
            //}

            $(document).ready(function () {
                $(".sidebar-nav-toggle").click(function (e) {
                    $("body").toggleClass("open");

                    $('html').one('click', function (e) {
                        $('body').removeClass("open");
                    });
                    e.stopPropagation();
                });
                $('.sidebar-nav-container').click(function (e) {
                    e.stopPropagation();
                });


                //back to top button

                $('.divBackToTopContainer').hide();

                $(window).scroll(function () {
                    if ($(this).scrollTop() > 35) {
                        $('.divBackToTopContainer').fadeIn();
                    } else {
                        $('.divBackToTopContainer').fadeOut();
                    }
                });
            });
            function goTop() {
                $('html,body').animate({ scrollTop: 0 }, 1700, 'easeInOutExpo');
                return false;
            }
            //end back to top button
        </script>