﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrEventmb.ascx.cs" Inherits="ctrl_ucUsrEvent" %>

<script type="text/javascript">
    oB.settings.iframeWidth = "740";
    oB.settings.iframeHeight = "310";
    oB.settings.addThis = false;
</script>

<asp:Panel ID="pnlEventList" runat="server" CssClass="divEventList">
    <asp:Panel ID="pnlEventContent" runat="server" CssClass="divEventContent">
        
        <asp:Panel ID="pnlListPaginationTop" runat="server" CssClass="divListPaginationTop">
            <div class="divListPaginationInner">
                <asp:Repeater ID="rptPaginationTop" runat="server" OnItemDataBound="rptPagination_ItemDataBound" OnItemCommand="rptPagination_ItemCommand">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkbtnPageNo" runat="server" CommandName="cmdPage" CommandArgument="<% #Container.DataItem %>" Text="<% #Container.DataItem %>" ToolTip="<% #Container.DataItem %>" CssClass="btnPagination"></asp:LinkButton>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </asp:Panel>
         <asp:Panel ID="pnlDdlFilter" runat="server" CssClass="divDdlSort"  Visible="false">
            <asp:Literal ID="litShow" runat="server"></asp:Literal>
            <asp:DropDownList ID="ddlFilter" runat="server" AutoPostBack="true" CssClass="ddl_filter" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged"></asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="pnlDdlSort" runat="server" CssClass="divDdlSort" Visible="false">
            <asp:Literal ID="litSort" runat="server"></asp:Literal>
            <asp:DropDownList ID="ddlSort" runat="server" AutoPostBack="true" CssClass="ddl_sort" OnSelectedIndexChanged="ddlSort_SelectedIndexChanged"></asp:DropDownList>
        </asp:Panel>
        
        <asp:Repeater ID="rptEvent" runat="server" OnItemDataBound="rptEvent_ItemDataBound">
            <ItemTemplate>
                <asp:Panel ID="pnlEventItemOuter" runat="server" CssClass="divEventItemOuter">
                    <asp:Panel ID="pnlEventItem" runat="server" CssClass="divEventItem">
                        <asp:Panel ID="pnlEventImage" runat="server" CssClass="divEventImage" >
                            <asp:Panel ID="pnlEventImg" runat="server" CssClass="divEventImg">
                                <div class="divEventImgInner">
                                    <asp:HyperLink ID="hypEventImg" runat="server"><asp:Image ID="imgEvent" runat="server" BorderWidth="0" /></asp:HyperLink></div>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlEventDetails" runat="server" CssClass="divEventDetails">
                            <asp:Panel ID="pnlEventName" runat="server" CssClass="divEventName">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-stack-1x info">i</i>
                                </span>
                                <asp:HyperLink ID="hypEventName" runat="server" CssClass="hypEventName"></asp:HyperLink>
                            </asp:Panel>
                            <asp:Panel ID="pnlEventDate" runat="server" CssClass="divEventStartDate">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-calendar fa-stack-1x"></i>
                                </span>
                                <h4><asp:Literal ID="litEventDate" runat="server"></asp:Literal></h4>
                            </asp:Panel>
                            <asp:Panel ID="pnlEventSnapshot" runat="server" CssClass="divEventSnapshot" Visible="false"><asp:Literal ID="litEventSnapshot" runat="server"></asp:Literal></asp:Panel>
                        </asp:Panel>
                    </asp:Panel>    
                </asp:Panel>
              </ItemTemplate>
             <SeparatorTemplate ><div class="divEventSpliter"></div></SeparatorTemplate> 
        </asp:Repeater>
        
        <asp:Panel ID="pnlListPaginationBottom" runat="server" CssClass="divListPaginationBottom">
            <div class="divListPaginationInner">
                <asp:Repeater ID="rptPaginationBottom" runat="server" OnItemDataBound="rptPagination_ItemDataBound" OnItemCommand="rptPagination_ItemCommand">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkbtnPageNo" runat="server" CommandName="cmdPage" CommandArgument="<% #Container.DataItem %>" Text="<% #Container.DataItem %>" ToolTip="<% #Container.DataItem %>" CssClass="btnPagination"></asp:LinkButton>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </asp:Panel>
    </asp:Panel>
</asp:Panel>