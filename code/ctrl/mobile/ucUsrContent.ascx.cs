﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;

public partial class ctrl_mobile_ucUsrContent : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _pageId = 0;
    #endregion


    #region "Property Methods"
    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion


    #region "Methods"
    public void fill()
    {
        setPageProperties();
    }

    protected void setPageProperties()
    {
        string strCurTemplate = Path.GetFileName(Request.PhysicalPath);

        clsConfig config = new clsConfig();
        string strDefaultKeyword = config.defaultKeyword;
        string strDefaultDesc = config.defaultDescription;

        string strKeyword = "";
        string strDesc = "";
        Boolean boolDefaultKeyword = true;
        Boolean boolDefaultDesc = true;

        clsPage page = new clsPage();
        if (page.extractPageById(pageId, 0))
        {
            strKeyword = page.keyword;
            strDesc = page.desc;

            boolDefaultKeyword = page.defaultKeyword > 0 ? true : false;
            boolDefaultDesc = page.defaultDesc > 0 ? true : false;
        }

        Literal litKeyword = new Literal();
        string strAppliedKeyword = "";
        strAppliedKeyword = boolDefaultKeyword ? strDefaultKeyword : strKeyword;

        if (!string.IsNullOrEmpty(strAppliedKeyword))
        {
            litKeyword.Text = "<meta name='keywords' content='" + strAppliedKeyword + "' />";
            this.Page.Header.Controls.Add(litKeyword);
        }

        Literal litDesc = new Literal();
        string strAppliedDesc = "";
        strAppliedDesc = boolDefaultDesc ? strDefaultDesc : strDesc;

        if (!string.IsNullOrEmpty(strAppliedDesc))
        {
            litDesc.Text = "<meta name='description' content='" + strAppliedDesc + "' />";
            this.Page.Header.Controls.Add(litDesc);
        }

        if (pageId > 0)
        {
            if (page.extractMobilePageById(pageId, 1, 1))
            {
                string strTemplate = page.mobileTemplate;

                if (string.Compare(strCurTemplate, strTemplate, true) != 0)
                {
                    int intPageId = page.getMobileFirstPageByTemplate(strCurTemplate, 1, 1);
                    Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + "/" + strCurTemplate + "?pgid=" + intPageId);
                }
                else
                {
                    litContent.Text = page.mobileContent;
                    
                    string strDefaultTitle = config.defaultTitle;
                    Boolean boolShowTitle = page.showPageTitle > 0;
                    string strTitle = page.title;

                    if (boolShowTitle)
                    {
                        Page.Title += clsMis.formatPageTitle((!string.IsNullOrEmpty(strTitle) ? strTitle.Replace("<br />", " ").Replace("<br/>", " ") : strDefaultTitle.Replace("<br />", " ").Replace("<br/>", " ")), false);
                    }
                    else
                    {
                        Page.Title += clsMis.formatPageTitle(strDefaultTitle.Replace("<br />", " ").Replace("<br/>", " "), false);
                    }
                }
            }
            else
            {
                Response.Redirect(ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"]);
            }
        }
        else
        {
            int intPageId = page.getMobileFirstPageByTemplate(strCurTemplate, 1, 1);
            if (intPageId > 0)
            {
                Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRMOBILEDIRECTARY + "/" + strCurTemplate + "?pgid=" + intPageId);
            }
            else
            {
                Response.Redirect(ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"]);
            }
        }
    }
    #endregion
}
