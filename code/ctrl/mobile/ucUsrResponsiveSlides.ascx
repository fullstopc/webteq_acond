﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrResponsiveSlides.ascx.cs" Inherits="ctrl_ucUsrResponsiveSlides" %>   


<script type="text/javascript">
    jQuery(document).ready(function ($) {

        var options = {
            $FillMode: 2,                                       //[Optional] The way to fill image in slide, 0 stretch, 1 contain (keep aspect ratio and put all inside slide), 2 cover (keep aspect ratio and cover whole slide), 4 actual size, 5 contain for large image, actual size for small image, default value is 0
            $AutoPlay: false,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
            $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
            $PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

            $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
            $SlideEasing: $JssorEasing$.$EaseOutQuint,          //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
            $SlideDuration: 800,                               //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
            $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
            //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
            //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
            $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
            $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
            $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
            $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
            $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
            $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

            $BulletNavigatorOptions: {                          //[Optional] Options to specify and enable navigator or not
                $Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
                $ChanceToShow: 0,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                $SpacingX: 8,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                $SpacingY: 8,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                $Orientation: 1,                                //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                $Scale: false,                                  //Scales bullets navigator or not while slider scale
            },

            $ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
                $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                $ChanceToShow: 0,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
            }
        };
        if ($(".divMastheadSlider > div").length > 0) {
            var jssor_slider1 = new $JssorSlider$("<%= pnlSliderContainer.ClientID %>", options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var bodyWidth = document.body.clientWidth;
                if (bodyWidth)
                    jssor_slider1.$ScaleWidth(Math.min(bodyWidth, 1920));
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();
		    jssor_slider1.$Play();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
        }

        //responsive code end
    });
</script>

<!-- Jssor Slider Begin -->
<!-- You can move inline styles to css file or css block. -->
<asp:Panel ID="pnlSliderContainer" runat="server" CssClass="divSliderContainer">
    <!-- Slides Container -->
    <asp:Panel ID="pnlMastheadSlider" u="slides" runat="server" CssClass="divMastheadSlider">
        <asp:Repeater ID="rptHomeMasthead" runat="server" OnItemDataBound="rptHomeMasthead_ItemDataBound">
            <ItemTemplate>
                <div>
                    <asp:HyperLink ID="hypMasthead" runat="server" Enabled="false">
                        <asp:Panel ID="pnlImgMasthead" runat="server" CssClass="divImgMasthead">
                            <asp:Image ID="imgMasthead" runat="server" BorderWidth="0" />
                        </asp:Panel>
                        <asp:Panel ID="pnltagline" runat="server" CssClass="divtagline" >
                            <asp:Panel ID="pnlLittag1" runat="server" CssClass="divLittag1">
                                <asp:Literal ID="Littag1" runat="server"></asp:Literal>
                            </asp:Panel>
                            <asp:Panel ID="pnlLittag2" runat="server" CssClass="divLittag2">
                                <asp:Literal ID="Littag2" runat="server"></asp:Literal>
                            </asp:Panel>    
                        </asp:Panel>
                     </asp:HyperLink>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </asp:Panel>
    <!-- bullet navigator container -->
    <div u="navigator" class="jssorb21 divNavigator">
        <!-- bullet navigator item prototype -->
        <div u="prototype" class="divPrototype"></div>
    </div>
    <!-- End bullet navigator container -->
    <!-- Arrow navigator container -->
    <!-- Arrow Left -->
    <span u="arrowleft" class="jssora21l spanArrowLeft"></span>
    <!-- Arrow Right -->
    <span u="arrowright" class="jssora21r spanArrowRight"></span>
    <!-- End arrow navigator container -->
</asp:Panel>
<!-- Jssor Slider End -->

