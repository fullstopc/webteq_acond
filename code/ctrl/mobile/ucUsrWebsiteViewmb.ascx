﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrWebsiteViewmb.ascx.cs" Inherits="ctrl_ucUsrWebsiteViewmb" %>

<asp:LinkButton Visible="false" ID="lnkbtnDesktopView" runat="server" CssClass="hypDesktopView" OnClick="lnkbtnDesktopView_Click"></asp:LinkButton>
<asp:HyperLink Visible="false" ID="hypMobileView" runat="server" class="hypMobileView">Mobile View</asp:HyperLink>
    
 <asp:Panel ID="pnlDesktopView" runat="server" CssClass="desktopview-inner">
    <span class="desktopview-label" style="">Desktop View: </span>
    <div class="flip-toggle floated left" style="">
       <asp:CheckBox ID="chkboxBtmDesktopView" runat="server"  OnCheckedChanged="chkboxBtmDesktopView_changed" AutoPostBack="true"/>
    </div>
</asp:Panel>


<script type="text/javascript">
    $(function () {
        new DG.OnOffSwitch({
            el: '#<%=chkboxBtmDesktopView.ClientID%>',
            textOn: 'On',
            textOff: 'Off',
            listener: function (name, checked) {
                $('#<%=chkboxBtmDesktopView.ClientID%>').click();
                <%--if (checked) {
                    window.location.href = "<%= ConfigurationManager.AppSettings["scriptBase"] + "usr/page.aspx?pgid=2" %>";
                }--%>
            }
        });
    });
</script>