﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucUsrQuickLinks : System.Web.UI.UserControl
{
    protected int _pageId;

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "mobile/quicklinksmb.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMEQUICKCONTACTTEL, clsConfig.CONSTGROUPQUICKCONTACT, 1);
            string strQuickContactNo = config.value.Replace(";", "");

            config.extractItemByNameGroup(clsConfig.CONSTNAMEQUICKCONTACTEML, clsConfig.CONSTGROUPQUICKCONTACT, 1);
            string strQuickContactEmail = config.value;

            if (string.IsNullOrEmpty(strQuickContactNo) && string.IsNullOrEmpty(strQuickContactEmail))
            {
                pnlQuickLinksInner.Visible = false;
            }

            if (!string.IsNullOrEmpty(strQuickContactNo))
            {
                //lblQuickContactNo.Text = "Call Us Now";
                pnlQuickLinksContactNo.Visible = true;
                hypQuickContactNo.Text = strQuickContactNo;
                hypQuickContactNo.NavigateUrl = "tel:" + strQuickContactNo;
            }

            if (!string.IsNullOrEmpty(strQuickContactEmail))
            {
                pnlQuickLinksEmailFacebookLike.Visible = true;
                pnlQuickLinksEmail.Visible = true;
                //hypQuickEmail.Text = "<i class=\"material-icons dp18\" style=\"font-size:30px; color:#454545;\">&#xE0BE;</i>";
                hypQuickEmail.Text = strQuickContactEmail;
                hypQuickEmail.ToolTip = strQuickContactEmail;
                hypQuickEmail.NavigateUrl = "mailto:" + strQuickContactEmail;
            }

            //Facebook
            string strEntityFBUrl = "";
            string strEntityTitle = "";
            string strEntityType = "";
            string strUrlToLike = "";
            string strImageFile = "";
            string strSiteName = "";
            string strEntityDesc = "";

            config.extractItemByNameGroup(clsConfig.CONSTNAMEFBPAGEURL, clsConfig.CONSTGROUPFB, 1);
            strEntityFBUrl = config.value;
            config.extractItemByNameGroup(clsConfig.CONSTNAMEFBLIKETITLE, clsConfig.CONSTGROUPFB, 1);
            strEntityTitle = config.value;
            config.extractItemByNameGroup(clsConfig.CONSTNAMEFBLIKETYPE, clsConfig.CONSTGROUPFB, 1);
            strEntityType = config.value;
            config.extractItemByNameGroup(clsConfig.CONSTNAMEFBLIKEURL, clsConfig.CONSTGROUPFB, 1);
            strUrlToLike = config.value;
            config.extractItemByNameGroup(clsConfig.CONSTNAMEFBLIKESITENAME, clsConfig.CONSTGROUPFB, 1);
            strSiteName = config.value;

            if (!String.IsNullOrEmpty(strEntityFBUrl))
            {
                hypFbPage.NavigateUrl = strEntityFBUrl;
                imgFbPage.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/social/icon-facebook.png";
                hypFbPage.Target = "_blank";

                pnlFbPageContainer.Visible = true;
            }
            else
            {
                pnlFbPageContainer.Visible = false;
            }

            if (!String.IsNullOrEmpty(strEntityType) && !String.IsNullOrEmpty(strUrlToLike) && !String.IsNullOrEmpty(strSiteName))
            {
                config.extractItemByNameGroup(clsConfig.CONSTNAMEFBLIKEIMAGE, clsConfig.CONSTGROUPFB, 1);
                strImageFile = config.value;
                config.extractItemByNameGroup(clsConfig.CONSTNAMEFBLIKEDESC, clsConfig.CONSTGROUPFB, 1);
                strEntityDesc = config.value;

                ucUsrFacebook.entityTitle = strEntityTitle;
                ucUsrFacebook.entityType = strEntityType;
                ucUsrFacebook.urlToLike = strUrlToLike;
                ucUsrFacebook.imageFile = strImageFile;
                ucUsrFacebook.siteName = strSiteName;
                ucUsrFacebook.entityDesc = strEntityDesc;

                ucUsrFacebook.fill();

                pnlFbPageLikeContainer.Visible = true;
            }
            else
            {
                pnlFbPageLikeContainer.Visible = false;
            }

            ucUsrContainerBanner1.pageId = pageId;
            ucUsrContainerBanner1.fill();
        }
    }
    #endregion
}
