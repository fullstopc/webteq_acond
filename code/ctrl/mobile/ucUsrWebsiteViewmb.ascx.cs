﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucUsrWebsiteViewmb : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _pageId;
    #endregion

    #region "Property Methods"
    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //hypDesktopView.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/page.aspx?pgid=" + pageId;
            //hypDesktopView.ToolTip = "Desktop View";
            //hypDesktopView.Target = "_blank";

            lnkbtnDesktopView.Text = lnkbtnDesktopView.ToolTip = "Go to Desktop";

            hypMobileView.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/mobile/pagemb.aspx?pgid=" + pageId;
            hypMobileView.ToolTip = "Mobile View";
        }
    }

    protected void chkboxBtmDesktopView_changed(Object sender, EventArgs args)
    {
        Session["DESKTOPVIEW"] = true;

        string strURL = clsMis.formatWebsiteViewURL(clsSetting.CONSTWEBSITEVIEWDESKTOP, pageId);
        string strQuery = Request.Url.GetComponents(UriComponents.Query, UriFormat.SafeUnescaped);

        Response.Redirect(strURL + (!string.IsNullOrEmpty(strQuery) ? "?" + strQuery : ""));
    }

    protected void lnkbtnDesktopView_Click(Object sender, EventArgs e)
    {
        Session["DESKTOPVIEW"] = true;

        string strURL = clsMis.formatWebsiteViewURL(clsSetting.CONSTWEBSITEVIEWDESKTOP, pageId);
        string strQuery = Request.Url.GetComponents(UriComponents.Query, UriFormat.SafeUnescaped);

        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "DesktopView", "window.open('" + strURL + strQuery + "','_blank');", true);
        Response.Redirect(strURL + (!string.IsNullOrEmpty(strQuery) ? "?" + strQuery : ""));
    }
    #endregion
}
