﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class ctrl_ucUsrHeader : System.Web.UI.UserControl
{
    public int pageId { get; set; }
    public string headerText { get; set; }

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion


    #region "Methods"
    public void fill()
    {
        litHeaderText.Text = headerText;

        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEQUICKCONTACTTEL, clsConfig.CONSTGROUPQUICKCONTACT, 1);
        string strQuickContactNo = config.value.Replace(";", "");

        if (!string.IsNullOrEmpty(strQuickContactNo))
        {
            hypQuickContact.Text = "<i class=\"material-icons dp24\" >&#xE61D;</i>";
            hypQuickContact.NavigateUrl = "tel:" + strQuickContactNo;
            hypQuickContact.Visible = true;
        }

        ucUsrSideMenu.pageId = pageId;

    }

    #endregion
}
