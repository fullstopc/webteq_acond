﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucUsrEnquiry : System.Web.UI.UserControl
{
    #region "Properties"
    protected string _lang = "";
    protected int _pgid;
    protected string _subject = "";
    protected string strReCaptcha_Key = "";
    #endregion


    #region "Property Methods"
    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }

    public int pgid
    {
        get
        {
            if (ViewState["PGID"] == null)
            {
                return _pgid;
            }
            else
            {
                return Convert.ToInt16(ViewState["PGID"]);
            }
        }
        set { ViewState["PGID"] = value; }
    }

    public string subject
    {
        get { return _subject; }
        set { _subject = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "mobile/enquirymb.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            litSubmitTitle.Text = GetLocalResourceObject("litSubmitTitle.Text").ToString();
            ListItem listItem1 = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
            ddlSalutation.Items.Add(listItem1);
            ListItem listItem2 = new ListItem(GetLocalResourceObject("salutationMs.Text").ToString(), "1");
            ddlSalutation.Items.Add(listItem2);
            ListItem listItem3 = new ListItem(GetLocalResourceObject("salutationMr.Text").ToString(), "2");
            ddlSalutation.Items.Add(listItem3);
            ListItem listItem4 = new ListItem(GetLocalResourceObject("salutationMrs.Text").ToString(), "3");
            ddlSalutation.Items.Add(listItem4);
            
            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMEENQUIRYTITLE, clsConfig.CONSTGROUPENQUIRYSETTINGS, 1);
            litSubmitTitle.Text = config.value;

            config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTRECIPIENT, clsConfig.CONSTGROUPEMAILSETTING, 1);
            string strEmail = config.value;

            config.extractItemByNameGroup(clsConfig.CONSTNAMEENQUIRYDESC, clsConfig.CONSTGROUPENQUIRYSETTINGS, 1);
            litDesc.Text = config.longValue.Replace("[EMAIL]", strEmail);
            //litDesc.Text = GetLocalResourceObject("litDesc.Text").ToString().Replace(clsAdmin.CONSTEMAILTAG, strEmail);

            //Google Recaptcha
            config.extractItemByNameGroup(clsConfig.CONSTNAMEENQUIRYRECAPTCHA, clsConfig.CONSTGROUPENQUIRYSETTINGS, 1);
            tr_enqRecaptcha.Visible = (config.value == "1") ? true : false;

            config.extractItemByNameGroup(clsConfig.CONSTNAMERECAPTCHASITEKEY, clsConfig.CONSTGROUPGOOGLERECAPTCHASETTINGS, 1);
            strReCaptcha_Key = config.value;
            //End Google Recaptcha

            if (!string.IsNullOrEmpty(subject))
            {
                txtSubject.Text = subject;
            }
        }
    }

    protected void imgbtnOk_Click(object sender, EventArgs e)
    {
        Response.Redirect("pagemb.aspx?lang=" + lang);
    }

    protected void imgbtnBack_Click(object sender, EventArgs e)
    {
        pnlAck.Visible = false;
        pnlEnquiryForm.Visible = true;
        pnlEnquiryHeaderDesc.Visible = true;
        litSubmitTitle.Text = GetLocalResourceObject("litSubmitTitle.Text").ToString();
    }

    protected void imgbtnSubmit_Click(object sender, EventArgs e)
    {
        string strSubjectPrefix;
        string strSubject;
        string strBody;
        string strUserSalutation;
        string strUserName;
        string strUserEmail;
        string strUserSubject;
        string strUserBody;
        string strUserContact;
        string strUserCompany;
        string strUserMessage;
        string strAck;
        string strSenderName;
        string strWebsiteName;
        string strSignature;
        string strGeoLocation = (!string.IsNullOrEmpty(hdnGeoLocation.Value)) ? hdnGeoLocation.Value : "";
        Boolean boolSendEnquirySMS = false;
        string strSMSContent;

        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEADMINEMAILSUBJECT, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEADMINEMAILCONTENT, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEUSEREMAILSUBJECT, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strUserSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEUSEREMAILCONTENT, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEWEBSITENAME, clsConfig.CONSTGROUPWEBSITE, 1);
        strWebsiteName = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMESENDENQUIRYSMS, clsConfig.CONSTGROUPSMSSETTINGS, 1);
        boolSendEnquirySMS = (config.value == "1") ? true : false;

        strUserSalutation = !string.IsNullOrEmpty(ddlSalutation.SelectedValue) ? Server.HtmlEncode(ddlSalutation.SelectedItem.ToString()) : "";
        strUserName = Server.HtmlEncode(txtName.Text.Trim());
        strUserEmail = Server.HtmlEncode(txtEmail.Text.Trim());
        strUserSubject = Server.HtmlEncode(txtSubject.Text.Trim());
        strUserContact = Server.HtmlEncode(txtContactNo.Text.Trim());
        strUserCompany = Server.HtmlEncode(txtCompany.Text.Trim());
        strUserMessage = txtMessage.Text.Trim().Replace(Environment.NewLine, "<br />");

        strBody = strBody.Replace("[SALUTATION]", strUserSalutation);
        strBody = strBody.Replace("[NAME]", strUserName);
        strBody = strBody.Replace("[COMPANY]", strUserCompany);
        strBody = strBody.Replace("[CONTACT]", strUserContact);
        strBody = strBody.Replace("[EMAIL]", strUserEmail);
        strBody = strBody.Replace("[SUBJECT]", strUserSubject);
        strBody = strBody.Replace("[MESSAGE]", strUserMessage);
        strBody = strBody.Replace("[WEBSITENAME]", strWebsiteName);
        strBody = strBody.Replace("[SIGNATURE]", strSignature);

        strUserBody = strUserBody.Replace("[SALUTATION]", strUserSalutation);
        strUserBody = strUserBody.Replace("[NAME]", strUserName);
        strUserBody = strUserBody.Replace("[COMPANY]", strUserCompany);
        strUserBody = strUserBody.Replace("[CONTACT]", strUserContact);
        strUserBody = strUserBody.Replace("[EMAIL]", strUserEmail);
        strUserBody = strUserBody.Replace("[SUBJECT]", strUserSubject);
        strUserBody = strUserBody.Replace("[MESSAGE]", strUserMessage);
        strUserBody = strUserBody.Replace("[WEBSITENAME]", strWebsiteName);
        strUserBody = strUserBody.Replace("[SIGNATURE]", strSignature);

        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strSubject = strSubjectPrefix + " " + strSubject; }
        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strUserSubject = strSubjectPrefix + " " + strUserSubject; }

        //lm - Add enquiry to database (20150205)
        int intSalutation = 0;
        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTRECIPIENT, clsConfig.CONSTGROUPEMAILSETTING, 1);
        string defaultRecipient = config.value;
        string strCurURL = Request.Url.ToString();

        if (!string.IsNullOrEmpty(ddlSalutation.SelectedValue)) { intSalutation = Convert.ToInt16(ddlSalutation.SelectedValue); }

        Random generator = new Random();
        String strRandomNo = generator.Next(10, 1000000).ToString("D6");
        string strEnquiryURL = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/newenquiry.aspx?rId=" + strRandomNo;
        
        clsEnquiry enquire = new clsEnquiry();
        bool boolAddEnquiryToDB = (enquire.addEnquiry(defaultRecipient, strCurURL, intSalutation, strUserName, strUserCompany, strUserContact, strUserEmail, strUserSubject, strUserMessage, strGeoLocation, strRandomNo) == 1 ? true : false);
        //end lm 

        if (boolAddEnquiryToDB)
        {
            //@ Send email to admin
            if (clsEmail.send(strUserEmail, "", "", "", "", strUserEmail, strSubject, strBody, 1))
            {
                //@ Send email to user
                if (clsEmail.send(strSenderName, "", strUserEmail, "", "", "", strUserSubject, strUserBody, 1))
                {
                    strAck = "<p class=\"noticemsg2\"> " + GetLocalResourceObject("txtGreet.Text").ToString() + (!string.IsNullOrEmpty(ddlSalutation.SelectedValue) ? Server.HtmlEncode(ddlSalutation.SelectedItem.ToString()) : "") + " " + Server.HtmlEncode(txtName.Text.Trim()) + ",<br /><br />" + GetLocalResourceObject("msgSuccess.Text").ToString() + "</p>";
                    lnkbtnOk.Visible = true;
                    lnkbtnBack.Visible = false;
                    litSubmitTitle.Text = GetLocalResourceObject("successful.Text").ToString();
                }
                else
                {
                    strAck = "<p class=\"errmsgEnquiry\">" + GetLocalResourceObject("msgUnsuccess.Text").ToString() + "</p>";

                    lnkbtnOk.Visible = false;
                    lnkbtnBack.Visible = true;
                    litSubmitTitle.Text = GetLocalResourceObject("unsuccessful.Text").ToString();
                }
            }
            else
            {
                strAck = "<p class=\"errmsgEnquiry\">" + GetLocalResourceObject("msgUnsuccess.Text").ToString() + "</p>";
                lnkbtnOk.Visible = false;
                lnkbtnBack.Visible = true;
                litSubmitTitle.Text = GetLocalResourceObject("unsuccessful.Text").ToString();
            }

            //Send SMS to Admin
            if (boolSendEnquirySMS)
            {
                config.extractItemByNameGroup(clsConfig.CONSTNAMEENQUIRYSMSCONTENT, clsConfig.CONSTGROUPSMSCONTENT, 1);
                strSMSContent = config.longValue;

                strSMSContent = strSMSContent.Replace("[EnquiryLink]", strEnquiryURL);

                string strResult = clsMis.sendSMS("", strSMSContent);
                if (string.IsNullOrEmpty(strResult))
                {
                    clsLog.logErroMsg("Failed to send TAG by sms.");
                }
            }

            Page.ClientScript.RegisterStartupScript(this.GetType(), "anchor", "goToByScroll();", true);

            litAck.Text = strAck;
            pnlAck.Visible = true;
            pnlEnquiryForm.Visible = false;
            pnlEnquiryHeaderDesc.Visible = false;
        }
    }
    #endregion
}
