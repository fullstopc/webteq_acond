﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrTopMenumb.ascx.cs" Inherits="ctrl_mobile_ucUsrTopMenumb" %>
<%@ Register Src="~/ctrl/ucUsrLoginLinks.ascx" TagName="UsrLoginLinks" TagPrefix="uc" %>

<script type="text/javascript">
    $(function () {
        $(".nav-one > li > div > a.parent.expand").parent().find(".nav-two").show();
        $(".nav-one > li > div > a.parent > i").click(function () {
            var $this = $(this).parent();
            var $child = $this.parent().find(".nav-two");
            $this.toggleClass("expand");

            if ($this.hasClass("expand")) {
                $child.slideDown();
            }
            else {
                $child.slideUp();
            }
            return false;
        });
    });
</script>

<asp:Panel ID="pnlTopMenu" runat="server" CssClass="divTopMenu">
    <asp:Panel ID="pnlTopMenuItems" runat="server" CssClass="divTopMenuItems">
        <ul class="ulTopMenu folded nav nav-one">
            <asp:Repeater ID="rptItems" runat="server" OnItemDataBound="rptItems_ItemDataBound">
                <ItemTemplate>
                    <li id="liTopMenu" runat="server">
                        <asp:Panel ID="pnlTopMenuItem" runat="server" CssClass="divTopMenuItem">
                            <asp:HyperLink ID="hypMenu" runat="server">
                                <span><asp:Literal runat="server" ID="litMenu"></asp:Literal></span>
                                <i class="material-icons add">add</i>
                                <i class="material-icons remove">remove</i>
                            </asp:HyperLink>
                             <asp:Panel ID="pnlSubMenu" runat="server" CssClass="divTopMenuSub">
                                <asp:Repeater ID="rptSubMenu" runat="server" OnItemDataBound="rptSubMenu_ItemDataBound">
                                    <HeaderTemplate>
                                        <ul id="ulSubMenu" class="ulSubMenu nav nav-two">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li runat="server" id="liSubMenu">
                                            <asp:Panel ID="pnlTopMenuSubItem" runat="server" CssClass="divTopMenuSubItem">
                                                <asp:HyperLink ID="hypSubMenu" runat="server" CssClass="hypTopSubMenu"></asp:HyperLink>
                                                <asp:Panel ID="pnlSubMenuLv2" runat="server" CssClass="divSubMenuLv2">
                                                    <asp:Repeater ID="rptSubMenuLv2" runat="server" OnItemDataBound="rptSubMenuLv2_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <ul id="ulSubMenuLv2" class="ulSubMenuLv2 nav nav-three">
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <li runat="server" id="liSubMenuLv2">
                                                                <asp:HyperLink ID="hypSubMenuLv2" runat="server" CssClass="hypTopSubMenuLv2"></asp:HyperLink>
                                                            </li>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </ul>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </asp:Panel>
                                            </asp:Panel>
                                        </li>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                    </FooterTemplate>
                                    <SeparatorTemplate>
                                        <li class="liSubMenuSeperator"></li>
                                    </SeparatorTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                        </asp:Panel>
                    </li>
                </ItemTemplate>
                <SeparatorTemplate>
                    <li class="splitter"></li>
                </SeparatorTemplate>
            </asp:Repeater>
        </ul>
    </asp:Panel>
</asp:Panel>
