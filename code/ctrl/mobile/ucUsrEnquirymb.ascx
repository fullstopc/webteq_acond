<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrEnquirymb.ascx.cs" Inherits="ctrl_ucUsrEnquiry" %>

<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
<script type="text/javascript">
    $(document).ready(function () {
        // Select dropdowns
        if ($('select').length) {
            $.each($('select'), function (i, val) {

                var $el = $(val);
                if (!$el.val()) {
                    $el.addClass("not_chosen");
                }

                $el.change(function () {
                    if (!$el.val())
                        $el.addClass("not_chosen");
                    else
                        $el.removeClass("not_chosen");
                });
            });
        }

        //Location
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                alert("Geolocation is not supported by this browser.");
            }
        }
        function showPosition(position) {
            $("#<%= hdnGeoLocation.ClientID %>").val(position.coords.latitude + "," + position.coords.longitude);
        }

        $(function () { getLocation(); });
        //End Location

        //Set focus to Empty TextBox
       <%-- $('#' + "<%= lnkbtnSubmit.ClientID %>").click(function (e) {
            validateHTML();
        });

        function validateHTML() {
            var numberReg = /^[0-9]+$/;
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

            var email = $('#txtEmailInput').val();

            if (email == "") {
                $("#txtEmailInput").focus();
            }
            else if (!emailReg.test(email)) {
                $("#txtEmailInput").focus();
            }
        }--%>
    });
</script>
<script>
    var isPostback = false;
    jQuery(document).ready(function() {
        $('.lnkbtnSubmit').click(function() {
            if (!Page_ClientValidate()) { }
            else {
                if (!isPostback) {
                    isPostback = true;
                    eval($(this).attr("href"));
                }
                $(this).attr("href", "#");
                return false;
            }
        });

    });

    function goToByScroll() {
        $('html, body').animate({ scrollTop: $('#contact').offset().top }, 'fast');
    }

    //Recaptcha
    var onloadCallback = function() {
        grecaptcha.render('dvCaptcha', {
            'sitekey':  '<%=strReCaptcha_Key%>',
            'callback': function(response) {
                $.ajax({
                    type: "POST",
                    url: wsBase + "recaptcha.asmx/VerifyCaptcha",
                    data: "{response: '" + response + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(r) {
                        var captchaResponse = jQuery.parseJSON(r.d);

                        console.log("captchaResponse:" + captchaResponse.success);
                        if (captchaResponse.success) {
                            $("[id*=txtCaptcha]").val(captchaResponse.success);
                            $("[id*=rfvCaptcha]").hide();
                        } else {
                            $("[id*=txtCaptcha]").val("");
                            $("[id*=rfvCaptcha]").show();
                            var error = captchaResponse["error-codes"][0];
                            $("[id*=rfvCaptcha]").html("RECaptcha error. " + error);
                        }
                    }
                });
            }
        });
    };
</script>

<asp:Panel ID="pnlEnquiry" runat="server">
    <a id="contact" name="contact"></a>
    <asp:Panel ID="pnlSubmitTitle" runat="server">
        <h3><asp:Literal ID="litSubmitTitle" runat="server"></asp:Literal></h3>
    </asp:Panel>
    <asp:Panel ID="pnlEnquiryTop" runat="server" CssClass="divEnquiryTop">
        <asp:Panel ID="pnlEnquiryHeaderDesc" runat="server" CssClass="divEnquiryHeaderDesc">
            <asp:Literal ID="litDesc" runat="server"></asp:Literal>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divEnquiryAck">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
        <asp:Panel ID="pnlAckBtn" runat="server" CssClass="divAckBtn">
            <asp:LinkButton ID="lnkbtnOk" runat="server" CssClass="lnkbtn" OnClick="imgbtnOk_Click" meta:resourcekey="lnkbtnOk" CausesValidation="false" ></asp:LinkButton>
            <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="lnkbtn" OnClick="imgbtnBack_Click" meta:resourcekey="lnkbtnBack" CausesValidation="false" ></asp:LinkButton>                     
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlEnquiryForm" runat="server" DefaultButton="lnkbtnSubmit">
        <table id="tblEnquiry" cellpadding="0" cellspacing="0" border="0" class="enquiry-form">
            <tr>
                <%--<td class="tdLabelNorEnq"><asp:Label ID="lblSalutation" runat="server" meta:resourcekey="lblSalutation"></asp:Label></td>--%>
                <td><asp:DropDownList ID="ddlSalutation" runat="server" CssClass="ddl_enquiry"></asp:DropDownList></td>
            </tr>
            <tr>
                <%--<td class="tdLabelNorEnq"><asp:Label ID="lblName" runat="server" meta:resourcekey="lblName"></asp:Label> <span class="attention_compulsory">*</span></td>--%>
                <td>
                    <asp:TextBox ID="txtName" runat="server" MaxLength="250" CssClass="text_big" ValidationGroup="grpEnquiry" placeholder="Name*"></asp:TextBox>
                    <span class="fieldDesc"><asp:RequiredFieldValidator ID="rfvName" runat="server" meta:resourcekey="rfvName" ControlToValidate="txtName" Display="Dynamic" CssClass="errmsgEnquiry" ValidationGroup="grpEnquiry" SetFocusOnError="true"></asp:RequiredFieldValidator></span>
                </td>
            </tr>
            <tr>
                <%--<td class="tdLabelNorEnq nobr"><asp:Label ID="lblCompany" runat="server" meta:resourcekey="lblCompany"></asp:Label></td>--%>
                <td><asp:TextBox ID="txtCompany" runat="server" MaxLength="250" CssClass="text_big" placeholder="Company Name"></asp:TextBox></td>
            </tr>
            <tr>
                <%--<td class="tdLabelNorEnq"><asp:Label ID="lblContactNo" runat="server" meta:resourcekey="lblContactNo"></asp:Label></td>--%>
                <td>
                    <asp:TextBox ID="txtContactNo" runat="server" MaxLength="20" CssClass="text_big" placeholder="Contact No*"></asp:TextBox>
                    <%--<input id="txtContactNoInput" onblur="document.getElementById('<%=txtContactNo.ClientID %>').value = this.value" type="tel" class="text_big" placeholder="Contact No*" title="Contact No"/>--%>
                    <span class="fieldDesc"><asp:RequiredFieldValidator ID="rfvContactNo" meta:resourcekey="rfvContactNo" runat="server" ControlToValidate="txtContactNo" Display="Dynamic" CssClass="errmsgEnquiry" ValidationGroup="grpEnquiry" SetFocusOnError="true"></asp:RequiredFieldValidator></span>
                    <span class="fieldDesc"><asp:RegularExpressionValidator ID="revContactNo" meta:resourcekey="revContactNo" runat="server" ControlToValidate="txtContactNo" Display="Dynamic" CssClass="errmsgEnquiry" ValidationExpression="^\+?\d{2,5}\s?-?\s?\d{3,7}\s?\d{3,10}$" ValidationGroup="grpEnquiry" SetFocusOnError="true"></asp:RegularExpressionValidator></span>
                </td>
            </tr>
            <tr>
                <%--<td class="tdLabelNorEnq"><asp:Label ID="lblEmail" runat="server" meta:resourcekey="lblEmail"></asp:Label> <span class="attention_compulsory">*</span></td>--%>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="250" CssClass="text_big" ValidationGroup="grpEnquiry" placeholder="Email*"></asp:TextBox>
                    <%--<input id="txtEmailInput" onblur="document.getElementById('<%=txtEmail.ClientID %>').value = this.value" type="email" class="text_big" placeholder="Email*" title="Email"/>--%>
                    <span class="fieldDesc"><asp:RequiredFieldValidator ID="rfvMail" meta:resourcekey="rfvMail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" CssClass="errmsgEnquiry" ValidationGroup="grpEnquiry" SetFocusOnError="true"></asp:RequiredFieldValidator></span>
                    <span class="fieldDesc"><asp:RegularExpressionValidator ID="revMail" meta:resourcekey="revMail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" CssClass="errmsgEnquiry" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="grpEnquiry"></asp:RegularExpressionValidator></span>
                </td>
            </tr>
            <tr>
                <%--<td class="tdLabelNorEnq"><asp:Label ID="lblSubject" runat="server" meta:resourcekey="lblSubject"></asp:Label> <span class="attention_compulsory">*</span></td>--%>
                <td>
                    <asp:TextBox ID="txtSubject" runat="server" MaxLength="250" CssClass="text_big" placeholder="Subject*"></asp:TextBox>
                    <span class="fieldDesc"><asp:RequiredFieldValidator ID="rfvSubject" runat="server" meta:resourcekey="rfvSubject" ControlToValidate="txtSubject" Display="Dynamic" CssClass="errmsgEnquiry" ValidationGroup="grpEnquiry" SetFocusOnError="true"></asp:RequiredFieldValidator></span>
                </td>
            </tr>
            <tr>
                <%--<td class="tdLabelNorEnq"><asp:Label ID="lblMessage" runat="server" meta:resourcekey="lblMessage"></asp:Label> <span class="attention_compulsory">*</span></td>--%>
                <td>
                    <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Rows="8" CssClass="text_big" placeholder="Message*"></asp:TextBox>
                    <span class="fieldDesc"><asp:RequiredFieldValidator ID="rfvMessage" runat="server" meta:resourcekey="rfvMessage" ControlToValidate="txtMessage" Display="Dynamic" CssClass="errmsgEnquiry" ValidationGroup="grpEnquiry" SetFocusOnError="true"></asp:RequiredFieldValidator></span>
                </td>
            </tr>
            <%--<tr>
                <td></td>
                <td><span class="attention_compulsory"><asp:Literal ID="litCompulsory" runat="server" meta:resourcekey="litCompulsory"></asp:Literal></span></td>
            </tr>--%>
        </table>
        <div id="tr_enqRecaptcha" runat="server" visible ="false" class="recaptchaOuter">
            <div id="dvCaptcha"></div>
            <asp:TextBox ID="txtCaptcha" runat="server" Style="display: none"/><br />
            <span class="fieldDesc"><asp:RequiredFieldValidator ID = "rfvCaptcha" ErrorMessage="Captcha validation is required." ControlToValidate="txtCaptcha" runat="server" Display = "Dynamic" CssClass="errmsgEnquiry" ValidationGroup="grpEnquiry"/></span>
        </div>
        <asp:Panel ID="pnlBtnSubmit" runat="server" CssClass="divBtnSubmit">
            <asp:LinkButton ID="lnkbtnSubmit" runat="server" CssClass="button button-full lnkbtnSubmit" OnClick="imgbtnSubmit_Click" meta:resourcekey="lnkbtnSubmit" ValidationGroup="grpEnquiry"></asp:LinkButton>
            <asp:HiddenField runat="server" ID="hdnGeoLocation"/>
        </asp:Panel>
    </asp:Panel>
</asp:Panel>