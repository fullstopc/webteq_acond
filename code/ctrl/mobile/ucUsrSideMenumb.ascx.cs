﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;

public partial class ctrl_mobile_ucUsrSideMenumb : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _pageId = 0;
    protected int _memFormSect;
    protected string _currency = "";
    protected string _lang = "";
    protected bool _boolFriendlyURL = false;
    #endregion
    
    #region "Property Methods"
    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public int memFormSect
    {
        get { return _memFormSect; }
        set { _memFormSect = value; }
    }

    public string currency
    {
        get { return _currency; }
        set { _currency = value; }
    }

    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }

    public Boolean boolFriendlyURL
    {
        get { return _boolFriendlyURL; }
        set { _boolFriendlyURL = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "mobile/sidemenumb.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            ucUsrTopMenu.pageId = pageId;
            ucUsrTopMenu.fill();

            //bindRptData();

            ucUsrContainerBannerTop.pageId = pageId;
            ucUsrContainerBannerTop.fill();
        }
    }
    #endregion
}