﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrBtmMenumb.ascx.cs" Inherits="ctrl_ucUsrBtmMenumb" %>

<asp:Repeater ID="rptBtmMenu" runat="server" OnItemDataBound="rptBtmMenu_ItemDataBound">
    <HeaderTemplate><ul class="nav nav-one"></HeaderTemplate>
    <FooterTemplate></ul></FooterTemplate>
    <ItemTemplate>
        <li>
            <asp:HyperLink ID="hypBtmMenu" runat="server" CssClass="">
                <i></i>
                <span><asp:Literal runat="server" ID="litBtmMenu"></asp:Literal></span>
            </asp:HyperLink>
        </li>
    </ItemTemplate>
    <SeparatorTemplate></SeparatorTemplate>
</asp:Repeater>
