﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucUsrWebteq : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            imgWebteqLogo.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "usr/mobile/logo-webteq.gif";
            imgWebteqLogo.ToolTip = "Powered by Webteq Solution";
            hypWebteq.NavigateUrl = "http://www.webteq.com.my";
            hypWebteq.Target = "blank";
        }
    }
}
