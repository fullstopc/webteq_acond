﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class ctrl_ucUsrFooter : System.Web.UI.UserControl
{
    public int pageId { get; set; }

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion


    #region "Methods"
    public void fill()
    {
        ucUsrCMSFooter.pageId = pageId;
        ucUsrCMSFooter.fill();

        ucUsrBtmMenu.pgid = pageId;
        ucUsrBtmMenu.fill();
    }

    #endregion
}
