﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrQuickLinksmb.ascx.cs" Inherits="ctrl_ucUsrQuickLinks" %>
<%@ Register Src="~/ctrl/ucUsrFacebookThumbnail.ascx" TagName="UsrFacebookThumbnail" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrFacebook.ascx" TagName="UsrFacebook" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucUsrCMSContainer.ascx" TagName="UsrCMSContainer" TagPrefix="uc" %>

<div class="divQuickLinks">
    <asp:Panel ID="pnlQuickLinksInner" runat="server" CssClass="divQuickLinksInner">
        <asp:Panel ID="pnlQuickLinksContactNo" runat="server" CssClass="divQuickLinksContactNo" Visible="false">
            <div class="divhypContact"><asp:HyperLink ID="hypQuickContactNo" runat="server" CssClass="hypQuickContactNo"></asp:HyperLink></div>
        </asp:Panel>
        <asp:Panel ID="pnlQuickLinksEmailFacebookLike" runat="server" CssClass="divQuickLinksEmailFacebookLike" Visible="false">
            <asp:Panel ID="pnlEmailFacebookLike" runat="server" CssClass="divEmailFacebookLike">
                <asp:Panel ID="pnlQuickLinksEmail" runat="server" CssClass="divQuickLinksEmail"  Visible="false"><asp:HyperLink ID="hypQuickEmail" runat="server" CssClass="hypQuickEmail"></asp:HyperLink></asp:Panel>
                <div class="divFbBottom">
                <asp:Panel ID="pnlFbPageContainer" runat="server" CssClass="divFbPageContainer" Visible = "false">
                        <div class="divFacebookWebsitePage"><asp:HyperLink ID="hypFbPage" runat="server"><asp:Image ID="imgFbPage" runat="server" /></asp:HyperLink></div>
                        <%--<div class="divFacebookLikePage"><uc:UsrFacebook ID="ucUsrFacebook" runat="server" /></div>--%>
                </asp:Panel>
                <asp:Panel ID="pnlFbPageLikeContainer" runat="server" CssClass="divFbPageLikeContainer" Visible="false">
                    <div class="divFacebookLikePage">
                        <uc:UsrFacebook ID="ucUsrFacebook" runat="server" />
                    </div>
                </asp:Panel>
                </div>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlCtnBannerBottom" runat="server" CssClass="divCtnBannerBottom">
        <uc:UsrCMSContainer ID="ucUsrContainerBanner1" runat="server" cmsType="10" blockId="2" />
    </asp:Panel>
</div>
