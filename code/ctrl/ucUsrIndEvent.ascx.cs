﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Collections;
using System.Net;
using System.IO;

public partial class ctrl_ucUsrIndEvent : System.Web.UI.UserControl
{
    #region "Property"
    protected int _pageId;
    protected int _currentPage = 0;
    protected int _eid;

    protected int intGallCount = 0;
    protected int intGallInRow = 6;
    protected int intGallPgSize = 2;

    protected int _currentPageGall = 0;
    protected int _pageCountGall = 0;
    protected int _pageNoGall = 1;

    protected int intVideoCount = 0;
    protected int intVideoInRow = 4;
    protected int intVidPgSize = 1;

    protected int _currentPageVid = 0;
    protected int _pageCountVid = 0;
    protected int _pageNoVid = 1;

    protected string strPostedName;
    protected string strPostedEmail;
    protected string strPostedComment;
    protected Boolean boolAllowComment = false;
    #endregion

    #region "Properties Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int currentPage
    {
        get
        {
            if (ViewState["CURRENTPAGE"] == null)
            {
                return _currentPage;
            }
            else
            {
                return Convert.ToInt16(ViewState["CURRENTPAGE"]);
            }
        }
        set
        {
            ViewState["CURRENTPAGE"] = value;
        }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public int eid
    {
        get { return _eid; }
        set { _eid = value; }
    }

    public int  currentPageGall
    {
        get
        {
            if (ViewState["CURRENTPAGEGALL"] == null)
            {
                return _currentPageGall;
            }
            else
            {
                return Convert.ToInt16(ViewState["CURRENTPAGEGALL"]);
            }
        }
        set
        {
            ViewState["CURRENTPAGEGALL"] = value;
        }
    }

    public int pageCountGall
    {
        get
        {
            if (ViewState["PAGECOUNTGALL"] == null)
            {
                return _pageCountGall;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGECOUNTGALL"]);
            }
        }
        set { ViewState["PAGECOUNTGALL"] = value; }
    }

    public int pageNoGall
    {
        get
        {
            if (ViewState["PAGENOGALL"] == null)
            {
                return _pageNoGall;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGENOGALL"]);
            }
        }
        set { ViewState["PAGENOGALL"] = value; }
    }

    public int currentPageVid
    {
        get
        {
            if (ViewState["CURRENTPAGEVID"] == null)
            {
                return _currentPageVid;
            }
            else
            {
                return Convert.ToInt16(ViewState["CURRENTPAGEVID"]);
            }
        }
        set
        {
            ViewState["CURRENTPAGEVID"] = value;
        }
    }

    public int pageCountVid
    {
        get
        {
            if (ViewState["PAGECOUNTVID"] == null)
            {
                return _pageCountVid;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGECOUNTVID"]);
            }
        }
        set { ViewState["PAGECOUNTVID"] = value; }
    }

    public int pageNoVid
    {
        get
        {
            if (ViewState["PAGENOVID"] == null)
            {
                return _pageNoVid;
            }
            else
            {
                return Convert.ToInt16(ViewState["PAGENOVID"]);
            }
        }
        set { ViewState["PAGENOVID"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.LoadComplete += new EventHandler(Page_LoadComplete);

        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "indEvent.css' type='text/css' rel='Stylesheet' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            setPageProperties();

            //clsPage page = new clsPage();
            //page.extractPageById(pageId, 1);
            //litEventHdr.Text = page.displayName;
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        bindRptData();
    }
    #endregion

    #region "Gallery"
    protected void lnkbtnFirstGall_Click(object sender, EventArgs e)
    {
        pageNoGall = 1;

        string strCurQuery = Request.QueryString.ToString();
        string strNewQuery = clsMis.getNewQueryString(strCurQuery, "ppg", pageNoGall.ToString());

        Response.Redirect(currentPageName + strNewQuery);
    }

    protected void lnkbtnLastGall_Click(object sender, EventArgs e)
    {
        pageNoGall = pageCountGall;

        string strCurQuery = Request.QueryString.ToString();
        string strNewQuery = clsMis.getNewQueryString(strCurQuery, "ppg", pageNoGall.ToString());

        Response.Redirect(currentPageName + strNewQuery);
    }

    protected void lnkbtnPrevGall_Click(object sender, EventArgs e)
    {
        pageNoGall = pageNoGall - 1 > 0 ? (pageNoGall - 1) : 1;

        string strCurQuery = Request.QueryString.ToString();
        string strNewQuery = clsMis.getNewQueryString(strCurQuery, "ppg", pageNoGall.ToString());

        Response.Redirect(currentPageName + strNewQuery /*+ "#photo"*/);
    }

    protected void lnkbtnNextGall_Click(object sender, EventArgs e)
    {
        pageNoGall = pageNoGall + 1 > pageCountGall ? pageCountGall : (pageNoGall + 1);

        string strCurQuery = Request.QueryString.ToString();
        string strNewQuery = clsMis.getNewQueryString(strCurQuery, "ppg", pageNoGall.ToString());

        Response.Redirect(currentPageName + strNewQuery  /*+ "#photo"*/);
    }

    protected void rptAlbum_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strGallName = DataBinder.Eval(e.Item.DataItem, "HIGHGALL_TITLE").ToString();
            string strGallImage = DataBinder.Eval(e.Item.DataItem, "HIGHGALL_IMAGE").ToString();

            string strHighImage = ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMHIGHFOLDER + "/" + clsSetting.CONSTADMHIGHGALLERYFOLDER + "/" + strGallImage;

            HyperLink hypAlbumImg = (HyperLink)e.Item.FindControl("hypAlbumImg");
            //hypAlbumImg.NavigateUrl = ConfigurationManager.AppSettings["uplBase"] + clsAdmin.CONSTADMHIGHFOLDER + "/" + clsAdmin.CONSTADMHIGHGALLERYFOLDER + "/" + strGallImage + "?" + clsSetting.CONSTUSRPRODIMGTHICKBOX;
            hypAlbumImg.NavigateUrl = ConfigurationManager.AppSettings["uplBase"] + clsAdmin.CONSTADMHIGHFOLDER + "/" + clsAdmin.CONSTADMHIGHGALLERYFOLDER + "/" + strGallImage;
            hypAlbumImg.ToolTip = strGallName;
            //hypAlbumImg.Target = "_blank";
            hypAlbumImg.Attributes["rel"] = "lightbox[event" + eid + "]";

            Image imgAlbumImage = (Image)e.Item.FindControl("imgAlbumImage");

            try
            {
                if (!string.IsNullOrEmpty(strHighImage))
                {
                    Dictionary<string, decimal> imageRatio = new Dictionary<string, decimal>()
                    {
                        { "x",4 }, {"y",3 }
                    };
                    string strImagePath = Server.MapPath(strHighImage);
                    System.Drawing.Image objImage = System.Drawing.Image.FromFile(strImagePath);

                    bool containerLandscape = (imageRatio != null) ? (imageRatio["x"] > imageRatio["y"]) : true;
                    bool imageLandscape = objImage.Width > objImage.Height;

                    int scale = (int)(objImage.Width / imageRatio["x"]);
                    int containerHeight = (int)(scale * imageRatio["y"]);


                    if (!containerLandscape && imageLandscape ||
                        (containerLandscape && imageLandscape && containerHeight > objImage.Height) || // follow width
                        (!containerLandscape && !imageLandscape && containerHeight > objImage.Height))
                    {
                        imgEvent.CssClass += " potrait";
                    }

                    //create thumbmail
                    string filePath = Server.MapPath("~/data/" + clsSetting.CONSTADMHIGHFOLDER + "/" + clsSetting.CONSTADMHIGHGALLERYFOLDER + "/thumb/");
                    if (!System.IO.File.Exists(filePath + "thumb_" + strGallImage))
                    {
                        System.Drawing.Bitmap bitThumb = GenerateThumbnails(new System.Drawing.Bitmap(Server.MapPath(strHighImage)), (int)objImage.Width, (int)objImage.Height);
                        bitThumb.Save(filePath + "thumb_" + strGallImage);
                    }
                    string strThumbImage = ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMHIGHFOLDER + "/" + clsSetting.CONSTADMHIGHGALLERYFOLDER + "/thumb/thumb_" + strGallImage;
                    //end create thumbnail

                    //imgAlbumImage.ImageUrl = strHighImage;
                    imgAlbumImage.AlternateText = strGallName;
                    imgAlbumImage.ToolTip = strGallName;
                    imgAlbumImage.Attributes["data-src"] = strThumbImage;
                    imgAlbumImage.CssClass = "lazyload";

                    objImage.Dispose();
                    objImage = null;
                }
            }
            catch (Exception ex) { }

            //Image imgAlbumImage = (Image)e.Item.FindControl("imgAlbumImage");
            //imgAlbumImage.AlternateText = imgAlbumImage.ToolTip = strGallName;

            //strGallImage = ConfigurationManager.AppSettings["uplBase"] + clsAdmin.CONSTADMHIGHFOLDER + "/" + clsAdmin.CONSTADMHIGHGALLERYFOLDER + "/" + strGallImage;
            //imageResize(imgAlbumImage, strGallImage, clsSetting.CONSTUSRINDEVENTGALLWIDTH, clsSetting.CONSTUSRINDEVENTGALLHEIGHT, 0);

            Literal litIndEvtAlbumTitle = (Literal)e.Item.FindControl("litIndEvtAlbumTitle");
            litIndEvtAlbumTitle.Text = strGallName;
        }
    }

    protected void rptPaginationGall_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton lnkbtnPageNo = (LinkButton)e.Item.FindControl("lnkbtnPageNo");

            int intPageNo = Convert.ToInt16(e.Item.DataItem);

            string strCurQuery = Request.QueryString.ToString();
            string strNewQuery = clsMis.getNewQueryString(strCurQuery, "ppg", intPageNo.ToString());

            lnkbtnPageNo.OnClientClick = "javascript:document.location.href='" + currentPageName + strNewQuery + "'; return false;";

            if (Convert.ToInt16(e.Item.DataItem) == currentPageGall + 1)
            {
                lnkbtnPageNo.CssClass = "btnEventPaginationSel";
                lnkbtnPageNo.OnClientClick = "javascript:return false;";
            }
        }
    }

    protected void rptPaginationGall_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdPage")
        {
            currentPageGall = Convert.ToInt16(e.CommandArgument) - 1;
        }
    }
    #endregion

    # region "Video"
    protected void lnkbtnFirstVid_Click(object sender, EventArgs e)
    {
        pageNoVid = 1;

        string strCurQuery = Request.QueryString.ToString();
        string strNewQuery = clsMis.getNewQueryString(strCurQuery, "vpg", pageNoVid.ToString());

        Response.Redirect(currentPageName + strNewQuery);
    }

    protected void lnkbtnLastVid_Click(object sender, EventArgs e)
    {
        pageNoVid = pageCountVid;

        string strCurQuery = Request.QueryString.ToString();
        string strNewQuery = clsMis.getNewQueryString(strCurQuery, "vpg", pageNoVid.ToString());

        Response.Redirect(currentPageName + strNewQuery);
    }

    protected void lnkbtnPrevVid_Click(object sender, EventArgs e)
    {
        pageNoVid = pageNoVid - 1 > 0 ? (pageNoVid - 1) : 1;

        string strCurQuery = Request.QueryString.ToString();
        string strNewQuery = clsMis.getNewQueryString(strCurQuery, "vpg", pageNoVid.ToString());

        Response.Redirect(currentPageName + strNewQuery /*+ "#photo"*/);
    }

    protected void lnkbtnNextVid_Click(object sender, EventArgs e)
    {
        pageNoVid = pageNoVid + 1 > pageCountVid ? pageCountVid : (pageNoVid + 1);

        string strCurQuery = Request.QueryString.ToString();
        string strNewQuery = clsMis.getNewQueryString(strCurQuery, "vpg", pageNoVid.ToString());

        Response.Redirect(currentPageName + strNewQuery  /*+ "#photo"*/);
    }
   
    protected void rptVideo_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strVideoImage = DataBinder.Eval(e.Item.DataItem, "HIGHVIDEO_THUMBNAIL").ToString();
            string strVideoLink = DataBinder.Eval(e.Item.DataItem, "HIGHVIDEO_VIDEO").ToString();

            HyperLink hypVideoImg = (HyperLink)e.Item.FindControl("hypVideoImg");
            //hypVideoImg.NavigateUrl = strVideoLink + "?autoplay=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=398&amp;width=648";
            hypVideoImg.NavigateUrl = strVideoLink + "?iframe";
            hypVideoImg.Attributes["rel"] = "lightbox[video" + eid + "]";
            //hypVideoImg.NavigateUrl = "<object width='195' height='145'><param name='movie' value='" + strVideoLink + "'></param><param name='allowFullScreen' value='true'></param><param name='allowscriptaccess' value='always'></param><embed src='" + strVideoLink + "' type='application/x-shockwave-flash' allowscriptaccess='always' allowfullscreen='true' width='195' height='145'></embed></object>";

            Image imgVideoImage = (Image)e.Item.FindControl("imgVideoImage");
            //imgVideoImage.ImageUrl = strVideoImage;

            Image imgVideoWatermark = (Image)e.Item.FindControl("imgVideoWatermark");
            imgVideoWatermark.Visible = false;

            Image resultImage = new Image();
            try
            {
                if (!string.IsNullOrEmpty(strVideoImage))
                {
                    string strImagePath = Server.MapPath(strVideoImage);
                    System.Drawing.Image objImage = System.Drawing.Image.FromFile(strImagePath);
                    resultImage = clsMis.getImageResize2(resultImage, clsSetting.CONSTUSRINDEVENTVIDWIDTH, clsSetting.CONSTUSRINDEVENTVIDHEIGHT, objImage.Width, objImage.Height);
                    objImage.Dispose();
                    objImage = null;
                }
            }
            catch (Exception ex)
            {
            }

            //Image imgEvent = (Image)e.Item.FindControl("imgEvent");
            imgVideoImage.ImageUrl = strVideoImage;
            //imgVideoImage.AlternateText = strEventTitle;
            //imgVideoImage.ToolTip = strEventTitle;

            resultImage.Dispose();
            resultImage = null;

            imageResize(imgVideoImage, strVideoImage, clsSetting.CONSTUSRINDEVENTVIDWIDTH, clsSetting.CONSTUSRINDEVENTVIDHEIGHT, 1);

        }
    }

    protected void rptPaginationVid_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton lnkbtnPageNo = (LinkButton)e.Item.FindControl("lnkbtnPageNo");

            int intPageNo = Convert.ToInt16(e.Item.DataItem);

            string strCurQuery = Request.QueryString.ToString();
            string strNewQuery = clsMis.getNewQueryString(strCurQuery, "vpg", intPageNo.ToString());

            lnkbtnPageNo.OnClientClick = "javascript:document.location.href='" + currentPageName + strNewQuery + "'; return false;";

            if (Convert.ToInt16(e.Item.DataItem) == currentPageVid + 1)
            {
                lnkbtnPageNo.CssClass = "btnEventPaginationSel";
                lnkbtnPageNo.OnClientClick = "javascript:return false;";
            }
        }
    }

    protected void rptPaginationVid_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdPage")
        {
            currentPageVid = Convert.ToInt16(e.CommandArgument) - 1;
        }
    }
    #endregion

    # region "Comment"
    protected void rptComment_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strCommPostedBy = DataBinder.Eval(e.Item.DataItem, "HIGHCOMM_POSTEDBYNAME").ToString();
            string strCommPosted = DataBinder.Eval(e.Item.DataItem, "HIGHCOMM_COMMENT").ToString();
            DateTime dtPostedDate = DataBinder.Eval(e.Item.DataItem, "HIGHCOMM_POSTEDDATE") != DBNull.Value ? DateTime.Parse(DataBinder.Eval(e.Item.DataItem, "HIGHCOMM_POSTEDDATE").ToString()) : DateTime.MaxValue;
            string strCommRepliedBy = DataBinder.Eval(e.Item.DataItem, "HIGHREPLY_REPLIEDBYNAME").ToString();
            string strCommReplied = DataBinder.Eval(e.Item.DataItem, "HIGHREPLY_COMMENT").ToString();
            DateTime dtRepliedDate = DataBinder.Eval(e.Item.DataItem, "HIGHREPLY_REPLIEDDATE") != DBNull.Value ? DateTime.Parse(DataBinder.Eval(e.Item.DataItem, "HIGHREPLY_REPLIEDDATE").ToString()) : DateTime.MaxValue;

            Literal litCommentPostedName = (Literal)e.Item.FindControl("litCommentPostedName");
            litCommentPostedName.Text = strCommPostedBy;

            if (dtPostedDate != DateTime.MaxValue)
            {
                Literal litCommentPostedDate = (Literal)e.Item.FindControl("litCommentPostedDate");
                litCommentPostedDate.Text = dtPostedDate.Day.ToString() + " " + dtPostedDate.ToString("MMM") + " " + dtPostedDate.Year.ToString();
            }

            Literal litCommentPosted = (Literal)e.Item.FindControl("litCommentPosted");
            litCommentPosted.Text = strCommPosted;

            if (!string.IsNullOrEmpty(strCommReplied))
            {
                Panel pnlIndEvtReply = (Panel)e.Item.FindControl("pnlIndEvtReply");
                pnlIndEvtReply.Visible = true;

                string[] strCommRepliedBySplit = strCommRepliedBy.Split((char)'@');

                //Literal litReplyPostedName = (Literal)e.Item.FindControl("litReplyPostedName");
                //litReplyPostedName.Text = strCommRepliedBySplit[0];

                //if (dtRepliedDate != DateTime.MaxValue)
                //{
                //    Literal litReplyPostedDate = (Literal)e.Item.FindControl("litReplyPostedDate");
                //    litReplyPostedDate.Text = dtRepliedDate.Day.ToString() + " " + dtRepliedDate.ToString("MMM") + " " + dtRepliedDate.Year.ToString();
                //}

                Literal litReplyPosted = (Literal)e.Item.FindControl("litReplyPosted");
                litReplyPosted.Text = strCommReplied;
            }
        }
    }

    protected void lnkbtnComment_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (!string.IsNullOrEmpty(txtPostCommName.Text.Trim())) { strPostedName = txtPostCommName.Text.Trim(); }
            else { strPostedName = "Anonymous"; }

            strPostedEmail = txtPostCommEmail.Text.Trim();
            strPostedComment = txtPostComment.Text.Trim();
            strPostedComment = strPostedComment.Replace(Environment.NewLine, "<br/>");

            clsHighlight high = new clsHighlight();
            int intRecordAffected = 0;
            intRecordAffected = high.addHighlightComment(eid, strPostedComment, 1, strPostedName, strPostedEmail);

            if (intRecordAffected == 1)
            {
                clsMis.resetListing();
                trAck.Visible = true;
                litAck.Text = "Your comment is posted.";
                Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRDEFAULTPATH + clsSetting.CONSTUSRGALLPAGE + "?pgid=" + pageId + "&eid=" + eid);
            }
        }
    }
    #endregion

    #region "Article"
    protected void rptArticle_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strArtTitle = DataBinder.Eval(e.Item.DataItem, "HIGHART_TITLE").ToString();
            string strArtFile = DataBinder.Eval(e.Item.DataItem, "HIGHART_FILE").ToString();

            HyperLink hypArticle = (HyperLink)e.Item.FindControl("hypArticle");
            hypArticle.NavigateUrl = ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMHIGHFOLDER + "/" + clsSetting.CONSTADMHIGHARTICLEFOLDER + "/" + strArtFile + "?" + clsSetting.CONSTUSRARTICLETHICKBOX;
        }
    }

    protected void rptArticle_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        hypIndEvtBack.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + clsSetting.CONSTUSRDEFAULTPATH + clsSetting.CONSTUSRGALLPAGE + "?pgid=" + pageId;

        //litIndEvtAlbumHdr.Text = "Photo";
        //litIndEvtVideoHdr.Text = "Video";
        //litIndEvtCommentHdr.Text = "Comment";
        //litPostCommentFormHdr.Text = "Leave Your Comment";


        litIndEvtAlbumHdr.Text = "Photo";
        litIndEvtVideoHdr.Text = "Video";
        litIndEvtCommentHdr.Text = "Comment";
        litPostCommentFormHdr.Text = "Leave Your Comment";

        imgGallery.ImageUrl = ConfigurationManager.AppSettings["imagebase"] + "usr/icon-album.png";
        imgVideo.ImageUrl = ConfigurationManager.AppSettings["imagebase"] + "usr/icon-video.png";
        imgComment.ImageUrl = ConfigurationManager.AppSettings["imagebase"] + "usr/icon-comment.png";

        clsPage page = new clsPage();
        clsHighlight high = new clsHighlight();

        page.extractPageById(pageId, 1);

        if (high.extractHighlightById(eid, 1))
        {
            string strEventImg = high.highImage;
            litIndEventTitle.Text = high.highTitle;
            litIndEventSnapShot.Text = high.highShortDesc;
            litIndEventDesc.Text = high.highDesc;

            if (!string.IsNullOrEmpty(litIndEventDesc.Text))
            { pnlIndEventDetailsBtm.Visible = true; }
            else { pnlIndEventDetailsBtm.Visible = false; }
         
            int intPrev = high.getPreviousEventByIdInd(high.highStartDate, high.highId, 0, 1);
            int intNext = high.getNextEventByIdInd(high.highStartDate, high.highId, 0, 1);
            
            if (intPrev > 0)
            {
                lnkbtnIndEvtPrev.OnClientClick = "javascript:document.location.href='" + currentPageName + "?pgid=" + pageId + "&eid=" + intPrev + "'; return false;";
                lnkbtnIndEvtPrev.Enabled = true;
            }
            else
            {
                lnkbtnIndEvtPrev.Enabled = false;
                pnlIndEvtPrev.CssClass = "divIndEvtPrevDisabled";
                lnkbtnIndEvtPrev.CssClass = lnkbtnIndEvtPrev.CssClass + " disabled";
            }

            if (intNext > 0)
            {
                lnkbtnIndEvtNext.OnClientClick = "javascript:document.location.href='" + currentPageName + "?pgid=" + pageId + "&eid=" + intNext + "'; return false;";
                lnkbtnIndEvtNext.Enabled = true;
            }
            else
            {
                lnkbtnIndEvtNext.Enabled = false;
                pnlIndEvtNext.CssClass = "divIndEvtNextDisabled";
                lnkbtnIndEvtNext.CssClass = "btnIndEventDisabledNext";
            }

            DateTime dtEventStartDate = Convert.ToDateTime(high.highStartDate);
            DateTime dtEventEndDate = Convert.ToDateTime(high.highEndDate);

            if ((dtEventStartDate.ToShortDateString() == dtEventEndDate.ToShortDateString()) && (dtEventEndDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString()))
            {
                litEventDate.Text = dtEventStartDate.ToString("dd MMM yyyy");

            }
            else if ((dtEventStartDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString()) && (dtEventEndDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString()))
            {
                litEventDate.Text = dtEventStartDate.ToString("dd MMM yyyy") + " to " + dtEventEndDate.ToString("dd MMM yyyy");
            }
            else if (dtEventStartDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
            {
                litEventDate.Text = dtEventStartDate.ToString("MMMM yyyy");
            }
            else
            {
                pnlIndEventDate.Visible = false;
                litEventDate.Text = "";
            }

            strEventImg = ConfigurationManager.AppSettings["uplBase"] + clsSetting.CONSTADMHIGHFOLDER + "/" + strEventImg;
            imageResize(imgEvent, strEventImg, clsSetting.CONSTUSRINDEVENTIMGWIDTH, clsSetting.CONSTUSRINDEVENTIMGHEIGHT, 0);

            if (high.highAllowComment == 1)
            {
                pnlIndEventPostCommentForm.Visible = true;
                boolAllowComment = true;
            }
            else
            {
                pnlIndEventPostCommentForm.Visible = false;
            }
        }
    }

    protected void bindRptData()
    {
        clsHighlight high = new clsHighlight();
        DataSet ds = new DataSet();
        DataView dv = new DataView();

        /*Highlight Album*/
        if (Application["HIGHLIGHTGALLERY"] == null)
        {
            Application["HIGHLIGHTGALLERY"] = high.getHighlightGallery();
        }

        ds = (DataSet)Application["HIGHLIGHTGALLERY"];
        dv = new DataView(ds.Tables[0]);

        if (eid > 0)
        {
            dv.RowFilter = "HIGH_ID = " + eid;
        }

        dv.Sort = "HIGHGALL_TITLE ASC";

        PagedDataSource pdsGall = new PagedDataSource();
        pdsGall.DataSource = dv;
        pdsGall.AllowPaging = true;
        pdsGall.PageSize = clsSetting.CONSTUSRINDEVENTGALLERYSIZE; ;
        pageCountGall = pdsGall.PageCount;

        if (pageNoGall != 0) { currentPageGall = pageNoGall - 1; }
        if (currentPageGall < 0) { currentPageGall = 0; }
        if (currentPageGall > pageCountGall - 1) { currentPageGall = 0; }

        pdsGall.CurrentPageIndex = currentPageGall;

        rptAlbum.DataSource = pdsGall;
        rptAlbum.DataBind();

        if (pageCountGall > 1)
        {
            if (dv.Count > 0)
            {
                litEvtGallCount.Text = "(" + dv.Count.ToString() + ")";
                pnlIndEventAlbum.Visible = true;

                int intCurrentPage = currentPageGall + 1;
                int intStart;
                int intEnd;
                int intPaginationSize = 3;
                int intPaginationSizeMid = Convert.ToInt16(intPaginationSize / 2);

                if (intCurrentPage <= intPaginationSizeMid)
                {
                    intStart = 1;
                    if (pageCountGall > intPaginationSize) { intEnd = intPaginationSize; }
                    else { intEnd = pageCountGall; }
                }
                else if (intCurrentPage >= pageCountGall - intPaginationSizeMid)
                {
                    intStart = pageCountGall - intPaginationSize + 1;
                    if (intStart < 1) { intStart = 1; }
                    intEnd = pageCountGall;
                }
                else
                {
                    intStart = intCurrentPage - intPaginationSizeMid;
                    intEnd = intCurrentPage + intPaginationSizeMid;
                }

                ArrayList pages = new ArrayList();
                for (int i = intStart; i <= intEnd; i++)
                {
                    pages.Add(i);
                }

                rptPaginationGall.DataSource = pages;
                rptPaginationGall.DataBind();
            }
        }
        else
        {
            if (dv.Count > 0)
            {
                litEvtGallCount.Text = "(" + dv.Count.ToString() + ")";
                pnlIndEventAlbum.Visible = true;
            }
            pnlIndPaginationGall.Visible = false;
        }

        if ((currentPageGall + 1) == pageCountGall)
        {
            lnkbtnNextGallTop.Enabled = false;
            lnkbtnLastGallTop.Enabled = false;
            lnkbtnNextGallTop.CssClass = "lnkbtnDisabled";
            lnkbtnLastGallTop.CssClass = "lnkbtnLastDisabled";
        }
        else if (currentPageGall == 0)
        {
            lnkbtnPrevGallTop.Enabled = false;
            lnkbtnFirstGallTop.Enabled = false;
            lnkbtnFirstGallTop.CssClass = "lnkbtnFirstDisabled";
            lnkbtnPrevGallTop.CssClass = "lnkbtnDisabled";
        }
        /*End of Highlight Album*/

        /*Highlight Video*/
        //Application["HIGHLIGHTVIDEOS"] = null;

        if (Application["HIGHLIGHTVIDEOS"] == null)
        {
            Application["HIGHLIGHTVIDEOS"] = high.getHighlightVideos();
        }

        ds = (DataSet)Application["HIGHLIGHTVIDEOS"];
        dv = new DataView(ds.Tables[0]);

        if (eid > 0)
        {
            dv.RowFilter = "HIGH_ID = " + eid;
        }

        dv.Sort = "HIGHVIDEO_ID ASC";

        PagedDataSource pdsVid = new PagedDataSource();
        pdsVid.DataSource = dv;
        pdsVid.AllowPaging = true;
        pdsVid.PageSize = clsSetting.CONSTUSRINDEVENTVIDEOSIZE;
        pageCountVid = pdsVid.PageCount;

        if (pageNoVid != 0) { currentPageVid = pageNoVid - 1; }
        if (currentPageVid < 0) { currentPageVid = 0; }
        if (currentPageVid > pageCountVid - 1) { currentPageVid = 0; }

        pdsVid.CurrentPageIndex = currentPageVid;

        rptVideo.DataSource = dv;
        rptVideo.DataBind();

        if (pageCountVid > 1)
        {
            if (dv.Count > 0)
            {
                litEvtVideoCount.Text = "(" + dv.Count.ToString() + ")";
                pnlIndEventVideo.Visible = true;

                int intCurrentPage = currentPageVid + 1;
                int intStart;
                int intEnd;
                int intPaginationSize = 3;
                int intPaginationSizeMid = Convert.ToInt16(intPaginationSize / 2);

                if (intCurrentPage <= intPaginationSizeMid)
                {
                    intStart = 1;
                    if (pageCountVid > intPaginationSize) { intEnd = intPaginationSize; }
                    else { intEnd = pageCountVid; }
                }
                else if (intCurrentPage >= pageCountVid - intPaginationSizeMid)
                {
                    intStart = pageCountVid - intPaginationSize + 1;
                    if (intStart < 1) { intStart = 1; }
                    intEnd = pageCountVid;
                }
                else
                {
                    intStart = intCurrentPage - intPaginationSizeMid;
                    intEnd = intCurrentPage + intPaginationSizeMid;
                }

                ArrayList pages = new ArrayList();
                for (int i = intStart; i <= intEnd; i++)
                {
                    pages.Add(i);
                }

                rptPaginationVid.DataSource = pages;
                rptPaginationVid.DataBind();
            }
        }
        else
        {
            if (dv.Count > 0)
            {
                litEvtVideoCount.Text = "(" + dv.Count.ToString() + ")";
                pnlIndEventVideo.Visible = true;
            }
            pnlIndPaginationVideo.Visible = false;
        }

        if ((currentPageVid + 1) == pageCountVid)
        {
            lnkbtnNextVidTop.Enabled = false;
            lnkbtnLastVidTop.Enabled = false;
            lnkbtnNextVidTop.CssClass = "lnkbtnDisabled";
            lnkbtnLastVidTop.CssClass = "lnkbtnLastDisabled";
        }
        else if (currentPageVid == 0)
        {
            lnkbtnPrevVidTop.Enabled = false;
            lnkbtnFirstVidTop.Enabled = false;
            lnkbtnPrevVidTop.CssClass = "lnkbtnDisabled";
            lnkbtnFirstVidTop.CssClass = "lnkbtnFirstDisabled";
        }
        /*End of Highlight Video*/

        /*Highlight Comment*/
        if (Application["HIGHLIGHTCOMMENT"] == null)
        {
            Application["HIGHLIGHTCOMMENT"] = high.getHighlightComments();
        }

        ds = high.getHighlightComments();
        dv = new DataView(ds.Tables[0]);

        if (eid > 0)
        {
            dv.RowFilter = "HIGH_ID = " + eid;
        }

        dv.Sort = "HIGHCOMM_POSTEDDATE DESC";

        if (dv.Count > 0 && boolAllowComment)
        {
            litEvtCommentCount.Text = "(" + dv.Count.ToString() + ")";
            pnlIndEventComment.Visible = true;

            rptComment.DataSource = dv;
            rptComment.DataBind();
        }
        /*End of Highlight Comment*/

        /*Highlight Article*/
        if (Application["HIGHLIGHTARTICLE"] == null)
        {
            Application["HIGHLIGHTARTICLE"] = high.getHighlightArticles();
        }

        ds = (DataSet)Application["HIGHLIGHTARTICLE"];
        dv = new DataView(ds.Tables[0]);

        if (eid > 0)
        {
            dv.RowFilter = "HIGH_ID = " + eid;
        }

        dv.Sort = "HIGHART_TITLE ASC";

        if (dv.Count > 0)
        {
            pnlIndEventArticle.Visible = true;

            rptArticle.DataSource = dv;
            rptArticle.DataBind();
        }
        /*End of Highlight Article*/
    }

    protected void imageResize(Image imageIndEvent, string strImage, int width, int height, int typeOfImg)
    {
        Image resultImage = new Image();
        try
        {
            if (!string.IsNullOrEmpty(strImage))
            {
                if (typeOfImg > 0)
                {
                    WebRequest req = WebRequest.Create(strImage);        
                    WebResponse response2 = req.GetResponse();
                    Stream stream = response2.GetResponseStream();
                    System.Drawing.Image objImage = System.Drawing.Image.FromStream(stream);//System.Drawing.Image.FromFile(strImagePath);
                    resultImage = clsMis.getImageResize2(resultImage, width, height, objImage.Width, objImage.Height);
                    objImage = null;
                }
                else
                {
                    string strImagePath = Server.MapPath(strImage);
                    System.Drawing.Image objImage = System.Drawing.Image.FromFile(strImagePath);
                    resultImage = clsMis.getImageResize2(resultImage, width, height, objImage.Width, objImage.Height);
                    objImage = null;
                }
            }
        }
        catch (Exception ex)
        {
        }

        imageIndEvent.ImageUrl = strImage;
        //imageIndEvent.Width = resultImage.Width;
        //imageIndEvent.Height = resultImage.Height;
        //imageIndEvent.Attributes["style"] = "left:" + (width - resultImage.Width.Value) / 2 + "px;";
        //imageIndEvent.Attributes["style"] += "top:" + (height - resultImage.Height.Value) / 2 + "px;";
        //imageIndEvent.Attributes["style"] += "position:absolute";
        resultImage.Dispose();
        resultImage = null;
    }

    private System.Drawing.Bitmap GenerateThumbnails(System.Drawing.Bitmap srcImage, int width, int height)
    {

        int newWidth = width;
        int newHeight = height;
        System.Drawing.Bitmap newImage = new System.Drawing.Bitmap(newWidth, newHeight);

        using (System.Drawing.Graphics gr = System.Drawing.Graphics.FromImage(newImage))
        {
            gr.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            gr.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            gr.DrawImage(srcImage, new System.Drawing.Rectangle(0, 0, newWidth, newHeight));
        }
        return newImage;
    }
    #endregion
}
