﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmInfo.ascx.cs" Inherits="ctrl_ucAdmInfo" %>

<asp:Panel ID="pnlAdmInfo" runat="server" CssClass="divAdmInfo">
    <asp:Literal ID="litInfo" runat="server"></asp:Literal>
</asp:Panel>


<script>
    $(function () {
        var hostname = window.location.host;
        var isFound = false;
        var hostnamePiece = hostname.split(".");
        var adminVersion = "";
        $.each(hostnamePiece, function (index) {
            if (hostnamePiece[index].indexOf('admin') > -1) {
                adminVersion = hostnamePiece[index].replace("admin", "");
                isFound = true;
            }
        });

        if(isFound)
        {
            var $pnlAdmInfo = $("#<%= pnlAdmInfo.ClientID %>");
            $pnlAdmInfo.html($pnlAdmInfo.text() + "(" + adminVersion + ")");
        }
    })
</script>
