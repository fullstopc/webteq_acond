﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmUserDetails.ascx.cs" Inherits="ctrl_ucAdmUserDetails" %>

<script type="text/javascript">
    function validatePassword_client(source, args) {
        if(args.Value.length < <% =clsAdmin.CONSTADMUSERNAMEMINLENGTH %>)
        {
            args.IsValid = false;
        }            
    }    
</script>    

<asp:Panel ID="pnlForm" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table id="tblUser" cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr">
                    <span style="padding-right:20px;">User Details</span>
                    <span class="tag" runat="server" id="spanCreatedAt" visible="false"><b>Created on</b> : <asp:Literal runat="server" ID="litCreatedAt"></asp:Literal></span>
                    <span class="tag" runat="server" id="spanUpdatedAt" visible="false"><b>Last Updated</b> : <asp:Literal runat="server" ID="litUpdatedAt"></asp:Literal></span>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Email<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <div class="input-group right">
                        <asp:TextBox ID="txtUserEmail" runat="server" CssClass="text_fullwidth compulsory input-control" MaxLength="250"></asp:TextBox>
                        <span class="input-btn">
                            <a class="" runat="server" id="hypLogin">Login</a>
                        </span>
                    </div>
                    <asp:RequiredFieldValidator ID="rfvUserEmail" runat="server" ErrorMessage="<br />Please enter Email." CssClass="errmsg" ControlToValidate="txtUserEmail" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revUserEmail" runat="server" CssClass="errmsg" ControlToValidate="txtUserEmail" Display="Dynamic" ErrorMessage="<br />Please enter valid Email." ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    <asp:CustomValidator ID="cvUserEmail" runat="server" ControlToValidate="txtUserEmail" CssClass="errmsg" Display="Dynamic" OnServerValidate="validateUserEmail_server" OnPreRender="cvUserEmail_PreRender"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Type<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:DropDownList ID="ddlUserType" runat="server" class="ddl_fullwidth compulsory"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvUserType" runat="server" ErrorMessage="<br />Please select Type." ControlToValidate="ddlUserType" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Password:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtUserPwd" runat="server" CssClass="text_fullwidth" TextMode="Password" MaxLength="20"></asp:TextBox>
                    <asp:HiddenField ID="hdnPwd" runat="server" />
                    <asp:CustomValidator ID="cvUserPwd" runat="server" CssClass="errmsg" ControlToValidate="txtUserPwd" Display="Dynamic" ClientValidationFunction="validatePassword_client" SetFocusOnError="true"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Confirm Password:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtConfirmPwd" runat="server" CssClass="text_fullwidth" TextMode="Password" MaxLength="20"></asp:TextBox>
                    <asp:CompareValidator ID="cmvPassword" runat="server" ErrorMessage="<br />Password does not match." ControlToCompare="txtUserPwd" ControlToValidate="txtConfirmPwd" Display="Dynamic" class="errmsg"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr">USER SETTINGS</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Project:</td>
                <td class="tdMax">
                    <asp:DropDownList ID="ddlUserProject" runat="server" class="ddl_fullwidth"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvProject" runat="server" ErrorMessage="<br />Please select Project." ControlToValidate="ddlUserProject" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Active:</td>
                <td class="tdMax"><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
            <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" class="btn btnCancel" Visible="false" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>
