﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmDashboard.ascx.cs" Inherits="ctrl_ucAdmDashboard" %>

<link href="<% =System.Configuration.ConfigurationManager.AppSettings["libBase"] %>fullcalendar/dist/fullcalendar.min.css" rel="Stylesheet" />
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["libBase"] %>fullcalendar/dist/fullcalendar.min.js" type="text/javascript"></script>
<asp:Panel ID="pnlDashboard" runat="server" CssClass="divDashboard container-fluid">
    <div class="row">
        <div class="col">
            <div class="box">
                <div class="box-header">
                    <h2 class="title">Schedule</h2>
                </div>
                
                <div class="box-body">
                    <div class="schedule-weekly__container">
                        <div class="schedule-weekly__header">
                            <div class="schedule-weekly__action">
<%--                                <div class="schedule-weekly__filter">
                                    <div class="schedule-weekly__filter__label">
                                        <span>Agent Filter</span>
                                        <i class="material-icons">play_arrow</i>
                                    </div>
                                    <ul class="schedule-weekly__filter__list"></ul>
                                </div>--%>
                            </div>
                        </div>
                        <div class="schedule-weekly__body">
                            <div class="schedule-weekly__calendar"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col" id="divGoogleAnalytic" runat="server">
            <div class="box">
                <div class="box-header">
                    <h2 class="title">Google Analytic</h2>
                </div>
                
                <div class="box-body">
                    <div class="divGALoginDesc"><asp:Literal ID="litGALoginDesc" runat="server" ></asp:Literal></div>
                    <link rel="stylesheet" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>pikaday.css">
<%--				    <style>
				        #date-range-selector-container label {width: 75px;display: inline-block;}
				    </style>--%>
                    <script>
                        (function(w, d, s, g, js, fs) {
                            g = w.gapi || (w.gapi = {}); g.analytics = { q: [], ready: function(f) { this.q.push(f); } };
                            js = d.createElement(s); fs = d.getElementsByTagName(s)[0];
                            js.src = 'https://apis.google.com/js/platform.js';
                            fs.parentNode.insertBefore(js, fs); js.onload = function() { g.load('analytics'); };
                        } (window, document, 'script'));
                    </script>
                    <div id="embed-api-auth-container"></div>
	                <table>
	                <colgroup>
		                <col width="80%" />
		                <col width="20%" />
	                </colgroup>
	                <tr>
		                <td><div id="chart-container"></div></td>
		                <td>
			                <div id="date-range-selector-container" style="padding:20px;"></div>
			                <div id="metrics-checkbox-container" style="padding:5px 20px;"></div>
		                </td>
	                </tr>
	                </table>
                    <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>moment.js"></script>
                    <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>pikaday.js"></script>
					<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>GoogleAnalyticsComponent.js" type="text/javascript"></script>
                    <script>
                       <% clsConfig config = new clsConfig(); %>
                       <% if(config.extractItemByNameGroup(clsConfig.CONSTNAMEGOOGLEANALYTICVIEWID, clsConfig.CONSTGROUPGOOGLE, 1)) { %>
                        var ids = <%= string.IsNullOrEmpty(config.value) ? "null":config.value %>
                       <% }else{ %>
                        var ids = null;
                       <% } %>
                       if(ids != null){
                        gapi.analytics.ready(function() {

                        if(gapi.analytics.auth.isAuthorized())
                        {  $(".divGALoginDesc").show();}
                        else{   $(".divGALoginDesc").hide();}

                        gapi.analytics.auth.authorize({
                            container: 'embed-api-auth-container',
                            clientid: '<%= strGoogleAnalyticsClientId %>',
                        });
                        // LINE, COLUMN, BAR, TABLE, and GEO
                        var commonConfig = {
                            query: {
                                dimensions: 'ga:date',
                                ids:'ga:<%= config.value %>'
                            },
                            chart: {
                                container: 'chart-container',
                                options: {
                                    width: '100%',
                                    colors: ['red','blue','yellow','cyan','green','brown']
                                }
                            }
                        };

                        var chartOption = {
                            type:'LINE',
                        }
                        var queryOption = {
                            'metrics': 'ga:bounces,ga:sessions,ga:bounceRate,ga:sessionDuration,ga:searchSessions',
                            'start-date': '30daysAgo',
                            'end-date': 'yesterday'
                        }

                        var dateRangeSelector = new gapi.analytics.ext.DateRangeSelector({
                            container: 'date-range-selector-container'
                        }).set(queryOption).execute();



                        var dataChart = new gapi.analytics.googleCharts.DataChart(commonConfig)
                        .set({query: queryOption})
                        .set({chart: chartOption});

                        dataChart.execute();
                        dateRangeSelector.on('change', function(data) {
                            queryOption['start-date'] = data['start-date'];
                            queryOption['end-date'] = data['end-date'];
                            dataChart.set({query: queryOption}).execute();
                        });



                        var metricsSelector = new gapi.analytics.ext.MetricsSelector({
                            container: 'metrics-checkbox-container'
                        }).execute();
                        metricsSelector.on('change', function(data) {
                            queryOption["metrics"] = data["metrics"];
                            dataChart.set({query: queryOption}).execute();
                        });

                        /*var chartTypeSelector = new gapi.analytics.ext.ChartTypeSelector({
                            container: 'chart-option'
                        }).execute();
                        chartTypeSelector.on('change', function(data) {
                            dataChart = new gapi.analytics.googleCharts.DataChart(commonConfig)
                            .set({query: dateRange})
                            .set({chart: data});
                            dataChart.execute();
                        });*/

                        });
	                    }
	                    </script>
                </div>
               
                <div class="box-footer"></div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col" id="divSalesManager" runat="server">
            <div class="box">
                <div class="box-header">
                    <a class="title" href="<% =System.Configuration.ConfigurationManager.AppSettings["scriptBase"]+"adm/admOrder01.aspx" %>"><h2>Sales Manager</h2></a>
                </div>
                <div class="box-body">
                    <asp:Repeater ID="rptOrder" runat="server"  OnItemDataBound="rptOrder_ItemDataBound">
                        <HeaderTemplate>
                            <table ID="tblOrder" class="dataTbl" cellspacing="0" border="1" style="border-collapse:collapse;">
                                <thead>
                                    <th>No.</th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Sales Date</a></th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Sales No.</a></th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Sales Amount</a></th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Status</a></th>
                                    <th>Action</th>
                                </thead>
                        </HeaderTemplate>
                        <FooterTemplate></table></FooterTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><asp:Literal ID="litNo" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litSaleDate" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litSaleNo" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litSaleAmt" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litStatus" runat="server"></asp:Literal></td>
                            <td class="alignCenter"><asp:HyperLink ID="hypEdit" CssClass="lnkbtn lnkbtnEdit" runat="server"></asp:HyperLink></td></tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="box-footer"></div>
            </div>
        </div>
        <div class="col" id="divProductManager" runat="server">
            <div class="box">
                <div class="box-header">
                    <a class="title" href="<% =System.Configuration.ConfigurationManager.AppSettings["scriptBase"]+"adm/admProduct01.aspx" %>"><h2>Product Manager</h2></a>
                </div>
                <div class="box-body">
                    <asp:Repeater ID="rptProduct" runat="server"  OnItemDataBound="rptProduct_ItemDataBound">
                        <HeaderTemplate>
                            <table ID="tblProduct" class="dataTbl" cellspacing="0" border="1" style="border-collapse:collapse;">
                                <thead>
                                    <th>No.</th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Code</a></th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Name</a></th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Active</a></th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Last Updated</a></th>
                                    <th>Action</th>
                                </thead>
                        </HeaderTemplate>
                        <FooterTemplate></table></FooterTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><asp:Literal ID="litNo" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litCode" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litName" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litActive" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litLastUpdated" runat="server"></asp:Literal></td>
                            <td class="alignCenter"><asp:HyperLink ID="hypEdit" CssClass="lnkbtn lnkbtnEdit" runat="server"></asp:HyperLink></td></tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="box-footer"></div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col" id="divEnquiryManager" runat="server">
            <div class="box">
            <div class="box-header">
                <a class="title" href="<% =System.Configuration.ConfigurationManager.AppSettings["scriptBase"]+"adm/admEnquiry01.aspx" %>"><h2>enquiry manager</h2></a>
            </div>
            <div class="box-body">
                <asp:Repeater ID="rptEnquiry" runat="server"  OnItemDataBound="rptEnquiry_ItemDataBound">
                    <HeaderTemplate>
                        <table ID="tblEnquiry" class="dataTbl" cellspacing="0" border="1" style="border-collapse:collapse;">
                            <thead>
                                <th>No.</th>
                                <th data-sort="string"><a href="#" onclick="return false;">Send Date</a></th>
                                <th data-sort="string"><a href="#" onclick="return false;">Sender Name</a></th>
                                <th data-sort="string"><a href="#" onclick="return false;">Company Name</a></th>
                                <th data-sort="string"><a href="#" onclick="return false;">Contact No</a></th>
                                <th data-sort="string"><a href="#" onclick="return false;">Sender Email</a></th>
                                <th data-sort="string"><a href="#" onclick="return false;">Subject</a></th>
                                <th data-sort="string">email recipient</th>
                                <th>Action</th>
                            </thead>
                    </HeaderTemplate>
                    <FooterTemplate></table></FooterTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><asp:Literal ID="litNo" runat="server"></asp:Literal></td>
                            <td><asp:Literal ID="litCreation" runat="server"></asp:Literal></td>
                            <td><asp:Literal ID="litName" runat="server"></asp:Literal></td>
                            <td><asp:Literal ID="litCompanyName" runat="server"></asp:Literal></td>
                            <td><asp:Literal ID="litContactNo" runat="server"></asp:Literal></td>
                            <td><asp:Literal ID="litEmailSender" runat="server"></asp:Literal></td>
                            <td><asp:Literal ID="litSubject" runat="server"></asp:Literal></td>
                            <td><asp:Literal ID="litMessage" runat="server"></asp:Literal></td>
                            <td class="alignCenter"><asp:HyperLink ID="hypView" runat="server" CssClass="lnkbtn lnkbtnView"></asp:HyperLink></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="box-footer"></div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col" id="divPageManager" runat="server">
            <div class="box">
                <div class="box-header">
                    <a class="title" href="<% =System.Configuration.ConfigurationManager.AppSettings["scriptBase"]+"adm/admPage01.aspx" %>"><h2>page manager</h2></a>
                </div>
                <div class="box-body">
                <asp:Repeater ID="rptPage" runat="server"  OnItemDataBound="rptPage_ItemDataBound">
                    <HeaderTemplate>
                        <table ID="tblPage" class="dataTbl" cellspacing="0" border="1" style="border-collapse:collapse;">
                            <thead>
                                <th>No.</th>
                                <th data-sort="string"><a href="#" onclick="return false;">Page Name</a></th>
                                <th data-sort="string"><a href="#" onclick="return false;">Parent</a></th>
                                <th data-sort="string"><a href="#" onclick="return false;">Active</a></th>
                                <th data-sort="string"><a href="#" onclick="return false;">Last Updated Date</a></th>
                                <th>Action</th>
                            </thead>
                    </HeaderTemplate>
                    <FooterTemplate></table></FooterTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><asp:Literal ID="litNo" runat="server"></asp:Literal></td>
                            <td><asp:Literal ID="litPageName" runat="server"></asp:Literal></td>
                            <td><asp:Literal ID="litParent" runat="server"></asp:Literal></td>
                            <td><asp:Literal ID="litActive" runat="server"></asp:Literal></td>
                            <td><asp:Literal ID="litLastUpdated" runat="server"></asp:Literal></td>
                            <td class="alignCenter"><asp:HyperLink ID="hypEdit" CssClass="lnkbtn lnkbtnEdit" runat="server"></asp:HyperLink></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                </div>
                <div class="box-footer"></div>
            </div>
        </div>
        <div class="col" id="divObjectManager" runat="server">
            <div class="box">
                <div class="box-header">
                    <a class="title" href="<% =System.Configuration.ConfigurationManager.AppSettings["scriptBase"]+"adm/admCMS01.aspx" %>"><h2>object manager</h2></a>
                </div>
                <div class="box-body">
                    <asp:Repeater ID="rptObject" runat="server"  OnItemDataBound="rptObject_ItemDataBound">
                        <HeaderTemplate>
                            <table ID="tblObject" class="dataTbl" cellspacing="0" border="1" style="border-collapse:collapse;">
                                <thead>
                                    <th>No.</th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Title</a></th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Type</a></th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Active</a></th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Last Updated Date</a></th>
                                    <th>Action</th>
                                </thead>
                        </HeaderTemplate>
                        <FooterTemplate></table></FooterTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><asp:Literal ID="litNo" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litTitle" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litType" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litActive" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litLastUpdated" runat="server"></asp:Literal></td>
                                <td class="alignCenter"><asp:HyperLink ID="hypEdit" CssClass="lnkbtn lnkbtnEdit" runat="server"></asp:HyperLink></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="box-footer"></div>
            </div>
        
        </div>
    </div>
    
    <div class="row">
        <div class="col" id="divSlideShowManager" runat="server">
            <div class="box">
                <div class="box-header">
                    <a class="title" href="<% =System.Configuration.ConfigurationManager.AppSettings["scriptBase"]+"adm/admSlide01.aspx" %>"><h2>slide show manager</h2></a>
                </div>
                <div class="box-body divAblImg">
                <asp:Repeater ID="rptAblImg" runat="server" OnItemDataBound = "rptAblImg_ItemDataBound">
                    <ItemTemplate>
                        <div class="divAblImgContainer">
                            <div class="divImg">
                                <asp:HyperLink ID="hypAblImg" runat="server">
                                    <asp:Image ID="imgAblImg" runat="server" />
                                </asp:HyperLink>
                            </div>
                            <div class="divDetail">
                                <div class="divName">
                                    <asp:Literal ID="litName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MASTHEAD_NAME") %>'></asp:Literal>
                                </div>
                                <div class="divActive">
                                    Active : <asp:Literal ID="litActive" runat="server" Text='<%# clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "MASTHEAD_ACTIVE"))) %>'></asp:Literal>
                                </div>
                                <div class="divGroupName">
                                    Group Name : <asp:Literal ID="litGroupName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "V_GRPDNAME") %>'></asp:Literal>
                                </div>
                                <div class="divLastUpdated">
                                    Last Updated : <asp:Literal ID="litLastUpdated" runat="server" Text=''></asp:Literal>
                                </div>
                                <div class="form__action form__action--floated">
                                    <asp:HyperLink ID="hypEdit" CssClass="lnkbtn lnkbtnEdit" runat="server" ></asp:HyperLink>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                </div>
                <div class="box-footer"></div>
            </div>
        </div>
        <div class="col" id="divEventManager" runat="server">
            <div class="box">
                <div class="box-header">
                    <a class="title" href="<% =System.Configuration.ConfigurationManager.AppSettings["scriptBase"]+"adm/admHighlight01.aspx" %>"><h2>event manager</h2></a>
                </div>
                <div class="box-body">
                    <asp:Repeater ID="rptEvent" runat="server"  OnItemDataBound="rptEvent_ItemDataBound">
                        <HeaderTemplate>
                            <table ID="tblEvent" class="dataTbl" cellspacing="0" border="1" style="border-collapse:collapse;">
                                <thead>
                                    <th>No.</th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Title</a></th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Star Date</a></th>
                                    <th data-sort="string"><a href="#" onclick="return false;">End Date</a></th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Active</a></th>
                                    <th data-sort="string"><a href="#" onclick="return false;">Last Updated Date</a></th>
                                    <th>Action</th>
                                </thead>
                        </HeaderTemplate>
                        <FooterTemplate></table></FooterTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><asp:Literal ID="litNo" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litTitle" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litStartDate" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litEndDate" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litActive" runat="server"></asp:Literal></td>
                                <td><asp:Literal ID="litLastUpdated" runat="server"></asp:Literal></td>
                                <td class="alignCenter"><asp:HyperLink ID="hypEdit" CssClass="lnkbtn lnkbtnEdit" runat="server"></asp:HyperLink></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="box-footer"></div>
            </div>
        </div>
    </div>
</asp:Panel>
<script type="text/javascript">
function adjustPortfolio() {
    var tuning = 24;
    var width = <% =clsSetting.CONSTSLIDESHOWMANAGERIMGWIDTH %> + tuning;
    var container = $(".divAblImg").width();
     
    // ratio = 5:3
    var numEleRow = Math.ceil(container / width);
    var newWidth = (container/numEleRow) - tuning;
    var newHeight = (newWidth / 5) * 3;
    $(".divAblImgContainer").width(newWidth)
    $(".divAblImg .divImg").width(newWidth).height(newHeight);
         
}


    jQuery(document).ready(function() {

        adjustPortfolio();
        jQuery(window).resize(function() {
            adjustPortfolio();
        });
    });
    
</script>
<script type="text/javascript">
    $(function () {
        var data = <%= Newtonsoft.Json.JsonConvert.SerializeObject(dictReturn) %>;
        var agents = data.agents;

        //$(".schedule-weekly__filter__list").append(agents.map(function(agent){
        //    return `
        //        <li>
        //            <label>
        //                <input type='checkbox' data-id='${agent.ID}' checked/>
        //                <span>${agent.name}</span>
        //            </label>
        //        </li>`;
        //}).join(""));

        $(".schedule-weekly__calendar").fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
            },
            defaultView: 'agendaWeek',
            minTime: "07:00:00",
            maxTime: "19:00:00",
            allDaySlot: false,
            editable: false,
            events: function (start, end, timezone, callback) {
                $.ajax({
                    type: 'POST',
                    url: wsBase + 'wsFullcalendar.asmx/getReservedList',
                    data: "{}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    navLinks: true,
                    success: function (data) {
                        var dataNew = $.parseJSON(data.d);
                        var agents = dataNew.map(function(x){ return x.employeeID; })
                                            .filter(function(item, pos, self) {
                                                return self.indexOf(item) == pos;
                                            })
                                            .map(function(x){
                                                return {
                                                    color:'#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6),
                                                    employeeID : x
                                                };
                                            });

                        var json = $.parseJSON(data.d).map(function(reserved){
                            var color = agents.filter(function(x){
                                return x.employeeID == reserved.employeeID;
                            }).map(function(x){ return x.color; })[0];

                            return {
                                title: reserved.propertyAreaName + ", " + reserved.propertyUnitno + " " + reserved.serviceTeam,
                                start: reserved.serviceDatetimeFrom,
                                end: reserved.serviceDatetimeTo,
                                url: 'admJob0101.aspx?id=' + reserved.ID,
                                backgroundColor :color,
                                borderColor:color
                            }
                        });
                        callback(json);
                    }
                });
            },
        });
    });


</script>