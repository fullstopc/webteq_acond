﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmControllerRights.ascx.cs" Inherits="ctrl_ucAdmControllerRights" %>

<asp:Panel ID="pnlForm" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table cellpadding="0" cellspacing="0" class="formTbl tblData">
            <tr>
                <td colspan="3" class="tdSectionHdr">Projects Rights</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr data-beforeleave="0">
                <td class="tdlistbox">
                    <asp:ListBox ID="lstboxAllProjects" runat="server" SelectionMode="Multiple" CssClass="ddl3" Rows="10" Height="310px"></asp:ListBox>                
                </td>
                <td class="tdbtnNextPrev">
                    <asp:LinkButton ID="lnkbtnNext" runat="server" Text=">" ToolTip=">" class="btnNext" OnClick="lnkbtnNext_Click"></asp:LinkButton><br />
                    <asp:LinkButton ID="lnkbtnPrev" runat="server" Text="<" ToolTip="<" class="btnNext" OnClick="lnkbtnPrev_Click"></asp:LinkButton>
                </td>
                <td class="tdlistbox">
                    <asp:ListBox ID="lstboxSelectedProjects" runat="server" SelectionMode="Multiple" CssClass="ddl3" Rows="10" Height="310px"></asp:ListBox>      
                </td>
            </tr>
            <tfoot>
                <tr>
                    <td colspan="3">
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="vgConfig"></asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>
