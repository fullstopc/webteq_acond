﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucUsrCMSContainer : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _pageId = 0;
    protected int _customId = 0;
    protected int _customId2 = 0;
    protected int _blockId = 0;
    protected bool _adminView = false;
    protected int _cmsId = 0;
    protected int _width = 0;
    protected int _height = 0;
    protected int _cmsType = 0;
    protected int _cmsGroup = 0;
    protected string _lang = "en";
    protected Boolean _boolAllowUpdate = true;
    #endregion


    #region "Property Methods"
    public int pageId
    {
        get
        {
            if (ViewState["PAGEID"] == null) { return _pageId; }
            else { return Convert.ToInt16(ViewState["PAGEID"]); }
        }
        set { ViewState["PAGEID"] = value; }
    }

    public int customId
    {
        get
        {
            if (ViewState["CUSTOMID"] == null) { return _customId; }
            else { return Convert.ToInt16(ViewState["CUSTOMID"]); }
        }
        set { ViewState["CUSTOMID"] = value; }
    }

    public int customId2
    {
        get
        {
            if (ViewState["CUSTOMID2"] == null) { return _customId2; }
            else { return Convert.ToInt16(ViewState["CUSTOMID2"]); }
        }
        set { ViewState["CUSTOMID2"] = value; }
    }

    public int blockId
    {
        get
        {
            if (ViewState["BLOCKID"] == null) { return _blockId; }
            else { return Convert.ToInt16(ViewState["BLOCKID"]); }
        }
        set { ViewState["BLOCKID"] = value; }
    }

    public Boolean adminView
    {
        get
        {
            if (ViewState["ADMINVIEW"] == null) { return _adminView; }
            else { return Convert.ToBoolean(ViewState["ADMINVIEW"]); }
        }
        set { ViewState["ADMINVIEW"] = value; }
    }

    public int cmsId
    {
        get
        {
            if (ViewState["CMSID"] == null) { return _cmsId; }
            else { return Convert.ToInt16(ViewState["CMSID"]); }
        }
        set { ViewState["CMSID"] = value; }
    }

    public int width
    {
        get
        {
            if (ViewState["WIDTH"] == null) { return _width; }
            else { return Convert.ToInt16(ViewState["WIDTH"]); }
        }
        set { ViewState["WIDTH"] = value; }
    }

    public int height
    {
        get
        {
            if (ViewState["HEIGHT"] == null) { return _height; }
            else { return Convert.ToInt16(ViewState["HEIGHT"]); }
        }
        set { ViewState["HEIGHT"] = value; }
    }

    public int cmsType
    {
        get
        {
            if (ViewState["CMSTYPE"] == null) { return _cmsType; }
            else { return Convert.ToInt16(ViewState["CMSTYPE"]); }
        }
        set { ViewState["CMSTYPE"] = value; }
    }

    public int cmsGroup
    {
        get
        {
            if (ViewState["CMSGROUP"] == null)
            {
                return _cmsGroup;
            }
            else
            {
                return Convert.ToInt16(ViewState["CMSGROUP"]);
            }
        }
        set { ViewState["CMSGROUP"] = value; }
    }

    public string lang
    {
        get { return ViewState["LANG"] as string ?? _lang; }
        set { ViewState["LANG"] = value; }
    }

    public Boolean boolAllowUpdate
    {
        get
        {
            if (ViewState["BOOLALLOWUPDATE"] == null) { return _boolAllowUpdate; }
            else { return Convert.ToBoolean(ViewState["BOOLALLOWUPDATE"]); }
        }
        set { ViewState["BOOLALLOWUPDATE"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        HtmlControl htmlctrlCSS = null;
        htmlctrlCSS = this.Page.Header.FindControl("CMSContainer") as HtmlControl;

        if (htmlctrlCSS == null)
        {
            HtmlLink cssLink = new HtmlLink();
            cssLink.ID = "CMSContainer";
            cssLink.Href = ConfigurationManager.AppSettings["cssBase"] + "cmscontainer.css";
            cssLink.Attributes.Add("rel", "stylesheet");
            cssLink.Attributes.Add("type", "text/css");

            this.Page.Header.Controls.Add(cssLink);
        }
    }
    #endregion


    #region "Methods"
    public void fill()
    {
        if (!IsPostBack) { setPageProperties(); }
    }

    protected void setPageProperties()
    {
        pnlCMSContainerAction.Visible = true;

        if (width > 0)
        {
            pnlCMSContainerInner.Width = width;
        }
        
        if (Session["ADMID"] != null && Session["ADMID"] != "" && boolAllowUpdate)
        {
            pnlCMSContainerEdit.Visible = true;
            pnlCMSContainerAction.Visible = true;

            int intAdminView = 0;
            if (adminView) { intAdminView = 1; }

            hypAdd.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/addCMS.aspx?pgid=" + pageId + "&cid=" + customId + "&cid2=" + customId2 + "&bid=" + blockId + "&type=" + cmsType + "&adm=" + intAdminView + (!string.IsNullOrEmpty(lang) ? "&lang=" + lang : "") + "&" + clsAdmin.CONSTUSRARTICLETHICKBOX;
            
        }
        else
        {
            if (height > 0)
            {
                pnlCMSContainerInner.Height = height;
            }

            pnlCMSContainerEdit.Visible = false;
            pnlCMSContainerAction.Visible = false;
        }

        fillForm();
    }

    protected void fillForm()
    {
        DataSet ds = new DataSet();
        DataView dv = new DataView();

        if (Application["PAGEOBJECTLIST"] == null)
        {
            clsPageObject po = new clsPageObject();
            Application["PAGEOBJECTLIST"] = po.getItemList2();
        }

        ds = (DataSet)Application["PAGEOBJECTLIST"];
        dv = new DataView(ds.Tables[0]);

        int intAdminView = 0;
        if(adminView) { intAdminView = 1; }

        dv.RowFilter = "PAGE_ID = " + pageId + " AND CUSTOM_ID = " + customId + " AND CUSTOM_ID_2 = " + customId2 + " AND BLOCK_ID = " + blockId + " AND PAGECMS_TYPE = " + cmsType + " AND PAGECMS_ADMINVIEW = " + intAdminView + (!string.IsNullOrEmpty(lang) ? " AND PAGECMS_LANG = '" + lang + "'" : "") + " AND (CMS_ACTIVE = 1 OR GRP_ACTIVE = 1)";

        if (dv.Count > 0)
        {
            PagedDataSource pds = new PagedDataSource();
            pds.DataSource = dv;

            rptCMSContent.DataSource = pds;
            rptCMSContent.DataBind();
        }
    }

    protected int SetCMSCSSTypeId(int intCMSType)
    {
        clsMis mis = new clsMis();
        string strType = mis.getListNameByListValue(intCMSType.ToString(), "CMS TYPE", 1, 1);

        litTitle.Text = GetLocalResourceObject("litTitle.Text").ToString().Replace("<!TYPE!>", strType);

        return intCMSType;
    }

    protected Boolean setBoolAllowUpdate()
    {
        return boolAllowUpdate;
    }

    protected Boolean setIsAdminView()
    {
        return adminView;
    }

    protected int setHeight()
    {
        return height;
    }

    protected int setWidth()
    {
        return width;
    }
    #endregion
}
