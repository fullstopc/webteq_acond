<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrEnquiry.ascx.cs" Inherits="ctrl_ucUsrEnquiry" %>

<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>WaterMark.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        //Can add unlimited field and separate it by coma.
        $("[id*=txtName],[id*=txtEmail],[id*=txtSubject],[id*=txtCompany],[id*=txtContactNo],[id*=txtMessage]").WaterMark(
            {
                WaterMarkTextColor: '#292929'
            });
    });
</script>

<script type="text/javascript">
    //Location
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            alert("Geolocation is not supported by this browser.");
        }
    }
    function showPosition(position) {
        $("#<%= hdnGeoLocation.ClientID %>").val(position.coords.latitude + "," + position.coords.longitude);
    }

    $(function () { getLocation(); });
    //End Location

    function goToByScroll() {
        $('html, body').animate({ scrollTop: $('#contact').offset().top }, 'fast');
    }

    //Recaptcha
    var onloadCallback = function() {
        grecaptcha.render('dvCaptcha', {
            'sitekey':  '<%=strReCaptcha_Key%>',
            'callback': function(response) {
                $.ajax({
                    type: "POST",
                    url: wsBase + "recaptcha.asmx/VerifyCaptcha",
                    data: "{response: '" + response + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(r) {
                        var captchaResponse = jQuery.parseJSON(r.d);

                        //console.log("captchaResponse:" + captchaResponse.success);
                        if (captchaResponse.success) {
                            $("[id*=txtCaptcha]").val(captchaResponse.success);
                            $("[id*=rfvCaptcha]").hide();
                        } else {
                            $("[id*=txtCaptcha]").val("");
                            $("[id*=rfvCaptcha]").show();
                            var error = captchaResponse["error-codes"][0];
                            $("[id*=rfvCaptcha]").html("RECaptcha error. " + error);
                        }
                    }
                });
            }
        });
    };
</script>


<asp:Panel ID="pnlEnquiry" runat="server" CssClass="divEnquiry">
    <a id="contact" name="contact"></a>
    <asp:Panel ID="pnlSubmitTitle" runat="server" CssClass="divSubmitTitle">
        <h2>
            <asp:Literal ID="litSubmitTitle" runat="server"></asp:Literal></h2>
    </asp:Panel>
    <asp:Panel ID="pnlEnquiryHeaderDesc" runat="server" CssClass="divEnquiryHeaderDesc">
        <asp:Literal ID="litDesc" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divEnquiryAck">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
        <asp:Panel ID="pnlAckBtn" runat="server" CssClass="divAckBtn">
            <asp:LinkButton ID="lnkbtnOk" runat="server" CssClass="lnkbtn" OnClick="imgbtnOk_Click"
                CausesValidation="false">OK</asp:LinkButton>
            <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="lnkbtn" OnClick="imgbtnBack_Click"
                CausesValidation="false">BACK</asp:LinkButton>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlEnquiryForm" runat="server" DefaultButton="lnkbtnSubmit">

        <table id="tblEnquiry" cellpadding="0" cellspacing="0" border="0" class="enquiry-form">
            <colgroup>
                <col width="48%" />
                <col width="4%" />
                <col width="48%" />
            </colgroup>
            <tr>
                <td>
                    <%--<asp:Label ID="lblSalutation" runat="server" meta:resourcekey="lblSalutation"></asp:Label><br />--%>
                    <asp:DropDownList ID="ddlSalutation" runat="server" CssClass="ddl ddl_enquiry" TabIndex="1"></asp:DropDownList></td>
                <td rowspan="5" width="50"></td>
                <td>
                    <%--<asp:Label ID="lblSubject" runat="server" meta:resourcekey="lblSubject"></asp:Label><span class="attention_compulsory">*</span><br />--%>
                    <asp:TextBox ID="txtSubject" runat="server" MaxLength="250" CssClass="text text_enquiry" TabIndex="6" ToolTip="Subject *"></asp:TextBox>
                    <span class="fieldDesc">
                        <asp:RequiredFieldValidator ID="rfvSubject" meta:resourcekey="rfvSubject" runat="server" ControlToValidate="txtSubject" Display="Dynamic" CssClass="errmsg" ValidationGroup="grpEnquiry"></asp:RequiredFieldValidator></span>
                </td>
            </tr>
            <tr>
                <td>
                    <%--<asp:Label ID="lblName" runat="server" meta:resourcekey="lblName"></asp:Label><span class="attention_compulsory">*</span><br />--%>
                    <asp:TextBox ID="txtName" runat="server" MaxLength="250" CssClass="text text_enquiry" ToolTip="Name *" TabIndex="2" ValidationGroup="grpEnquiry"></asp:TextBox>
                    <span class="fieldDesc">
                        <asp:RequiredFieldValidator ID="rfvName" runat="server" meta:resourcekey="rfvName" ControlToValidate="txtName" Display="Dynamic" CssClass="errmsg" ValidationGroup="grpEnquiry"></asp:RequiredFieldValidator></span>
                </td>
                <td rowspan="4">
                    <%--<asp:Label ID="lblMessage" runat="server" meta:resourcekey="lblMessage"></asp:Label><span class="attention_compulsory">*</span><br />--%>
                    <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Rows="10" CssClass="text_EnquiryMessage" TabIndex="7" ToolTip="Message *"></asp:TextBox>
                    <span class="fieldDesc">
                        <asp:RequiredFieldValidator ID="rfvMessage" meta:resourcekey="rfvMessage" runat="server" ControlToValidate="txtMessage" Display="Dynamic" CssClass="errmsg" ValidationGroup="grpEnquiry"></asp:RequiredFieldValidator></span>
                </td>
            </tr>
            <tr>
                <td>
                    <%--<asp:Label ID="lblCompany" runat="server" meta:resourcekey="lblCompany"></asp:Label><br />--%>
                    <asp:TextBox ID="txtCompany" runat="server" MaxLength="250" CssClass="text text_enquiry" TabIndex="3" ToolTip="Company Name"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <%--<asp:Label ID="lblContactNo" runat="server" meta:resourcekey="lblContactNo"></asp:Label><br />--%>
                    <asp:TextBox ID="txtContactNo" runat="server" MaxLength="20" CssClass="text text_enquiry" TabIndex="4" ToolTip="Contact No *"></asp:TextBox>
                    <span class="fieldDesc"><asp:RequiredFieldValidator ID="rfvContactNo" meta:resourcekey="rfvContactNo" runat="server" ControlToValidate="txtContactNo" Display="Dynamic" CssClass="errmsg" ValidationGroup="grpEnquiry"></asp:RequiredFieldValidator></span>
                    <span class="fieldDesc"><asp:RegularExpressionValidator ID="revContactNo" meta:resourcekey="revContactNo" runat="server" ControlToValidate="txtContactNo" Display="Dynamic" CssClass="errmsg" ValidationExpression="^\+?\d{2,5}\s?-?\s?\d{3,7}\s?\d{3,10}$" ValidationGroup="grpEnquiry" SetFocusOnError="true"></asp:RegularExpressionValidator></span>                
                </td>
            </tr>
            <tr>
                <td>
                    <%--<asp:Label ID="lblEmail" runat="server" meta:resourcekey="lblEmail"></asp:Label><span class="attention_compulsory">*</span><br />--%>
                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="250" CssClass="text text_enquiry" ToolTip="Email *" TabIndex="5" ValidationGroup="grpEnquiry"></asp:TextBox>
                    <span class="fieldDesc">
                        <asp:RequiredFieldValidator ID="rfvMail" meta:resourcekey="rfvMail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" CssClass="errmsg" ValidationGroup="grpEnquiry"></asp:RequiredFieldValidator></span>
                    <span class="fieldDesc">
                        <asp:RegularExpressionValidator ID="revMail" meta:resourcekey="revMail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" CssClass="errmsg" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="grpEnquiry"></asp:RegularExpressionValidator></span>
                </td>
            </tr>
            <tr id="tr_enqRecaptcha" runat="server" visible ="false" >
                <td colspan="2" class="recaptchaOuter">
                    <div id="dvCaptcha"></div>
                    <asp:TextBox ID="txtCaptcha" runat="server" Style="display: none"/><br />
                    <span class="fieldDesc"><asp:RequiredFieldValidator ID = "rfvCaptcha" ErrorMessage="Captcha validation is required." ControlToValidate="txtCaptcha" runat="server" Display = "Dynamic" CssClass="errmsg" ValidationGroup="grpEnquiry"/></span>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="lnkbtnSubmit" runat="server" CssClass="button lnkbtnSubmit float right" TabIndex="8" OnClick="imgbtnSubmit_Click" ValidationGroup="grpEnquiry">Submit</asp:LinkButton>
                    <asp:HiddenField runat="server" ID="hdnGeoLocation"/>

                </td>
            </tr>
        </table>
    </asp:Panel>
    <script>
        var isPostback = false;
        jQuery(document).ready(function() {
        $('.lnkbtnSubmit').click(function() {
                if (!Page_ClientValidate()) {  }
                else {
                    if (!isPostback) {
                        isPostback = true;
                        eval($(this).attr("href"));
                    }
                    $(this).attr("href", "#");
                    return false;
                }
            });

        });
    </script>
</asp:Panel>
