﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrAdBanner.ascx.cs" Inherits="ctrl_ucUsrAdBanner" %>
<div class="divBannerHdr">
    <asp:HyperLink ID="hypBannerHdr" runat="server" meta:resourcekey="hypBannerHdr" Enabled="false"><asp:Image ID="imgBannerHdr" runat="server" meta:resourcekey="imgBannerHdr" CssClass="imgBannerHdr" /></asp:HyperLink>
</div>
<div class="divBannerContent">
    <asp:Repeater ID="rptAdBanner" runat="server" OnItemDataBound="rptAdBanner_ItemDataBound">
        <ItemTemplate>
            <asp:Panel ID="pnlIndAdBanner" runat="server" CssClass="divIndAdBanner">
                <div class="divIndAdBannerInner">
                    <asp:HyperLink ID="hypAdBanner" runat="server" Target="_blank"><asp:Image ID="imgAdBanner" runat="server" CssClass="imgAdBanner" /></asp:HyperLink>
                </div>
            </asp:Panel>
        </ItemTemplate>
        <SeparatorTemplate>
            <div class="divAdBannerSplit"></div>
        </SeparatorTemplate>
    </asp:Repeater>
</div>