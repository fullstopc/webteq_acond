﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmShippingCompany.ascx.cs" Inherits="ctrl_ucAdmShippingCompany" %>

<script type="text/javascript">
    function validateShippingFee_client(source, args) {
        var txtFirstBlockPrice = document.getElementById("<%= txtFirstBlockPrice.ClientID %>");
        var txtSubBlockPrice = document.getElementById("<%= txtSubBlockPrice.ClientID %>");
        var currRE = /<%= clsAdmin.CONSTCURRENCYRE %>/;
        

        if (Trim(args.Value) != '') {
            if (args.Value.match(currRE)) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
                source.errormessage = "<br />Please enter only currency.";
                source.innerHTML = "<br />Please enter only currency.";
            }   
        }
    }

    function validateShippingWeight_client(source, args) {
        var txtFirstBlockWeight = document.getElementById("<%= txtFirstBlockWeight.ClientID %>");
        var txtSubBlockWeight = document.getElementById("<%= txtSubBlockWeight.ClientID %>");
        var currWeight = /<%= clsAdmin.CONSTCURRENCYRE %>/;

        if (Trim(args.Value) != '') {
            if (args.Value.match(currWeight)) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
                source.errormessage = "<br/>Please enter only number.";
                source.innerHTML = "<br/>Please enter only number.";
            }
        }
    }
</script>

<asp:Panel ID="pnlShipCompContainer" runat="server" CssClass="divShipCompContainer">
    <asp:Panel ID="pnlShipCompDetailsContainer" runat="server" CssClass="divShipCompDetailsContainer">
        <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr">Shipping Details</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel nobr"><asp:Label ID="lblShipComp" runat="server"></asp:Label><span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:DropDownList ID="ddlShipComp" runat="server" CssClass="ddl"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvShipComp" runat="server" ErrorMessage="<br/>Please select one shipping company." ControlToValidate="ddlShipComp" Display="Dynamic" CssClass="errmsg" ValidationGroup="shipComp"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblShipMethod" runat="server"></asp:Label><span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:DropDownList ID="ddlShipMethod" runat="server" CssClass="ddl"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvShipMethod" runat="server" ErrorMessage="<br/>Please select one shipping method." ControlToValidate="ddlShipMethod" Display="Dynamic" CssClass="errmsg" ValidationGroup="shipComp"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblCountry" runat="server"></asp:Label><span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="ddl" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ErrorMessage="<br/>Please select one country." ControlToValidate="ddlCountry" Display="Dynamic" CssClass="errmsg" ValidationGroup="shipComp"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr id="trState" runat="server" visible="false">
                <td class="tdLabel"><asp:Label ID="lblState" runat="server"></asp:Label>:</td>
                <td class="tdMax"><asp:TextBox ID="txtState" runat="server" CssClass="text"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblFirstBlock" runat="server"></asp:Label><span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <table cellpadding="0" cellspacing="0" border="0" class="">
                        <tr>
                            <td class="tdLabel">
                                <asp:TextBox ID="txtFirstBlockWeight" runat="server" CssClass="text_medium" onchange="formatCurrency(this.value)"></asp:TextBox>
                                <asp:Literal ID="litFirstBlockWeightUnit" runat="server"></asp:Literal>
                                <asp:RequiredFieldValidator ID="rfvFirstBlockWeight" runat="server" ErrorMessage="<br/>Please enter first block value." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtFirstBlockWeight" ValidationGroup="shipComp"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvFirstBlockWeight" runat="server" ControlToValidate="txtFirstBlockWeight" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateShippingWeight_client"></asp:CustomValidator>
                            </td>
                            <td class="tdMax">
                                <asp:Literal ID="litFirstBlockPriceUnit" runat="server"></asp:Literal>
                                <asp:TextBox ID="txtFirstBlockPrice" runat="server" CssClass="text_medium" onchange="formatCurrency(this.value)"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvFirstBlockPrice" runat="server" ErrorMessage="<br/>Please enter shipping fee." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtFirstBlockPrice" ValidationGroup="shipComp"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvFirstBlockPrice" runat="server" ControlToValidate="txtFirstBlockPrice" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateShippingFee_client"></asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblSubBlock" runat="server"></asp:Label><span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <table cellpadding="0" cellspacing="0" border="0" class="">
                        <tr>
                            <td class="tdLabel">
                                <asp:TextBox ID="txtSubBlockWeight" runat="server" CssClass="text_medium" onchange="formatCurrency(this.value)"></asp:TextBox>
                                <asp:Literal ID="litSubBlockWeightUnit" runat="server"></asp:Literal>
                                <asp:RequiredFieldValidator ID="rfvSubBlockWeight" runat="server" ErrorMessage="<br/>Please enter subsequent block value." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtSubBlockWeight" ValidationGroup="shipComp"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvSubBlockWeight" runat="server" ControlToValidate="txtSubBlockWeight" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateShippingWeight_client"></asp:CustomValidator>
                            </td>
                            <td class="tdMax">
                                <asp:Literal ID="litSubBlockPriceUnit" runat="server"></asp:Literal>
                                <asp:TextBox ID="txtSubBlockPrice" runat="server" CssClass="text_medium" onchange="formatCurrency(this.value)"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvSubBlockPrice" runat="server" ErrorMessage="<br>Please enter shipping fee." Display="Dynamic" CssClass="errmsg" ControlToValidate="txtSubBlockPrice" ValidationGroup="shipComp"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvSubBlockPrice" runat="server" ControlToValidate="txtSubBlockPrice" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateShippingFee_client"></asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
        </table>

        <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr">Shipping Settings</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblActive" runat="server"></asp:Label>:</td>
                <td class="tdMax"><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tfoot>
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="shipComp"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CssClass="btn btnCancel" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>
