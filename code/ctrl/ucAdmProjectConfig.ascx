﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmProjectConfig.ascx.cs" Inherits="ctrl_ucAdmProjectConfig" %>

<script type="text/javascript">
    function validateNumeric_client(source, args) {
        var dimRE = /<%= clsAdmin.CONSTNUMERICRE %>/;

        if (args.Value.match(dimRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "<br />Please enter only numeric.";
            source.innerHTML = "<br />Please enter only numeric.";
        }
    }
</script>

<asp:Panel ID="pnlForm" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table cellpadding="0" cellspacing="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr">Limit</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblPageLimit" runat="server"></asp:Label>:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtPageLimit" runat="server" CssClass="text_small"></asp:TextBox>
                    <asp:CustomValidator ID="cvPageLimit" runat="server" ControlToValidate="txtPageLimit" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateNumeric_client" ValidationGroup="vgConfig"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel"><asp:Label ID="lblObjectLimit" runat="server"></asp:Label>:</td>
                <td>
                    <asp:TextBox ID="txtObjectLimit" runat="server" CssClass="text_small"></asp:TextBox>
                    <asp:CustomValidator ID="cvObjectLimit" runat="server" ControlToValidate="txtObjectLimit" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateNumeric_client" ValidationGroup="vgConfig"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel nobr"><asp:Label ID="lblSlideShowLimit" runat="server"></asp:Label>:</td>
                <td>
                    <asp:TextBox ID="txtSlideShowLimit" runat="server" CssClass="text_small"></asp:TextBox>
                    <asp:CustomValidator ID="cvSlideShowLimit" runat="server" ControlToValidate="txtSlideShowLimit" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateNumeric_client" ValidationGroup="vgConfig"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel nobr"><asp:Label ID="lblSlideShowImageLimit" runat="server"></asp:Label>:</td>
                <td>
                    <asp:TextBox ID="txtSlideShowImageLimit" runat="server" CssClass="text_small"></asp:TextBox>
                    <asp:CustomValidator ID="cvSlideShowImageLimit" runat="server" ControlToValidate="txtSlideShowImageLimit" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateNumeric_client" ValidationGroup="vgConfig"></asp:CustomValidator>
                </td>
            </tr>
            <tr id="trEvent" runat="server" visible="false">
                <td class="tdLabel"><asp:Label ID="lblEventLimit" runat="server"></asp:Label>:</td>
                <td>
                    <asp:TextBox ID="txtEventLimit" runat="server" CssClass="text_small"></asp:TextBox>
                    <asp:CustomValidator ID="cvEventLimit" runat="server" ControlToValidate="txtEventLimit" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateNumeric_client" ValidationGroup="vgConfig"></asp:CustomValidator>
                </td>
            </tr>
            <tr id="trCategory" runat="server" visible="false">
                <td class="tdLabel"><asp:Label ID="lblCategoryLimit" runat="server"></asp:Label>:</td>
                <td>
                    <asp:TextBox ID="txtCategoryLimit" runat="server" CssClass="text_small"></asp:TextBox>
                    <asp:CustomValidator ID="cvCategoryLimit" runat="server" ControlToValidate="txtCategoryLimit" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateNumeric_client" ValidationGroup="vgConfig"></asp:CustomValidator>
                </td>
            </tr>
            <tr id="trProduct" runat="server" visible="false">
                <td class="tdLabel"><asp:Label ID="lblProductLimit" runat="server"></asp:Label>:</td>
                <td>
                    <asp:TextBox ID="txtProductLimit" runat="server" CssClass="text_small"></asp:TextBox>
                    <asp:CustomValidator ID="cvProductLimit" runat="server" ControlToValidate="txtProductLimit" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateNumeric_client" ValidationGroup="vgConfig"></asp:CustomValidator>
                </td>
            </tr>
            <tr id="trProgGroup" runat="server" visible="false">
                <td class="tdLabel"><asp:Label ID="lblProgGroupLimit" runat="server"></asp:Label>:</td>
                <td>
                    <asp:TextBox ID="txtProgGroupLmit" runat="server" CssClass="text_small"></asp:TextBox>
                    <asp:CustomValidator ID="cvProgGroupLimit" runat="server" ControlToValidate="txtProgGroupLmit" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateNumeric_client" ValidationGroup="vgConfig"></asp:CustomValidator>
                </td>
            </tr>
            <tr id="trProgram" runat="server" visible="false">
                <td class="tdLabel"><asp:Label ID="lblProgramLimit" runat="server"></asp:Label>:</td>
                <td>
                    <asp:TextBox ID="txtProgramLimit" runat="server" CssClass="text_small"></asp:TextBox>
                    <asp:CustomValidator ID="cvProgramLimit" runat="server" ControlToValidate="txtProgramLimit" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateNumeric_client" ValidationGroup="vgConfig"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tfoot>
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="vgConfig"></asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>
