﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmHighlightDesc.ascx.cs" Inherits="ctrl_ucAdmHighlightDesc" %>

<asp:Panel ID="pnlForm" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table id="tblAddDesc" cellpadding="0" cellspacing="0" class="formTbl tblData">
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Description:</td>
                <td><asp:TextBox ID="txtHighDesc" runat="server" CssClass="text_big" TextMode="MultiLine"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="btn btnSave" Text="Save" ToolTip="Save" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnCancel" runat="server" CssClass="btn btnBack" Text="Cancel" ToolTip="Cancel" CausesValidation="false"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>