﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class ctrl_ucUsrCopy : System.Web.UI.UserControl
{
    #region "page_events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMECOPYRIGHT, clsConfig.CONSTGROUPWEBSITE, 1);
            litCopy.Text = config.value;
        }
    }
    #endregion
}
