using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Data;

public partial class ctrl_ucUsrSearch : System.Web.UI.UserControl
{
    #region "Properties"
    protected string _keywd = "";
    #endregion

    #region "Property Methods"
    public string keywd
    {
        get { return _keywd; }
        set { _keywd = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "search.css' rel='Stylesheet' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            //txtSearch.Attributes.Add("onfocus", "clearText()");
            //txtSearch.Attributes.Add("onblur", "resetText()");
            //txtSearch.Attributes.Add("onkeydown", "javascript: if (event.keyCode == 13) {document.getElementById('" + imgbtnSearchIcon.ClientID + "').click(); return false;}");
            ddlSearch.Items.Add(" The Bukit Course");
            ddlSearch.Items.Add(" The Bintang Course");

            imgbtnSearchIcon.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "cmn/trans.gif";
            imgbtnSearchIcon.ToolTip = "search";
            litSearchDesc.Text = "I would like to";

       

            if (!string.IsNullOrEmpty(keywd))
            {
                //txtSearch.Text = keywd;
            }
        }
    }

    protected void imgbtnSearch_Click(object sender, EventArgs e)
    {
        //if (String.Compare(txtSearch.Text.Trim(), "search", true) == 0 || string.IsNullOrEmpty(txtSearch.Text.Trim()))
        //{
        //}
        //else
        //{
        //    string strQuery = "?key=" + txtSearch.Text.Trim();
        //    Response.Redirect(clsSetting.CONSTUSRPRODUCTPAGE + strQuery);
        //}
    }
    #endregion
}