﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Drawing.Imaging;

public partial class ctrl_ucAdmProductDetails : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _mode = 1;
    protected int _prodId = 0;
    protected int _grpId = 0;
    protected int _prodgrpId = 0;
    protected string _pageListingURL = "";
    protected decimal decDimensionConvRate = Convert.ToDecimal("0.000");
    protected decimal decWeightConvRate = Convert.ToDecimal("0.000");

    protected int intProdCountSel;
    protected string prodCode;
    protected string prodName;
    protected string prodNameJp;
    protected string prodDName;
    protected string prodDNameJp;
    protected string pageTitleFriendlyUrl;
    protected string prodDesc;
    protected string prodSnapShot;
    protected string prodUOM;
    protected int prodWeight;
    protected int prodLength;
    protected int prodWidth;
    protected int prodHeight;
    protected int prodNew;
    protected int prodBest;
    protected int prodShowTop;
    protected int prodSold;
    protected int prodRent;
    protected int prodOutOfStock;
    protected int prodPreorder;
    protected int prodWithGst;
    protected int prodActive;
    protected int prodAddon;
    protected int prodSales;
    protected string prodImage;
    protected int prodNOV;
    protected int prodNOP;
    protected int prodCat1;
    protected int prodCat2;
    protected int prodCat3;
    protected int prodCat4;
    protected int prodCat5;
    protected int prodCat6;
    protected int prodCat7;
    protected int prodCat8;
    protected int prodCat9;
    protected int prodCat10;

    // crop image - sh.chong 22 Sep 2015
    protected decimal decImgWidth = Convert.ToDecimal("0.00");
    protected decimal decImgHeight = Convert.ToDecimal("0.00");
    protected decimal decImgX = Convert.ToDecimal("0.00");
    protected decimal decImgY = Convert.ToDecimal("0.00");
    protected string strImgCropped = "";
    protected string strImgName = "";
    protected string uploadPath = "";
    protected string aspectRatioX = "1";
    protected string aspectRatioY = "1";
    protected string aspectRatio = "1/1";
    // end crop image

    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int prodId
    {
        get
        {
            if (ViewState["PRODID"] == null)
            {
                return _prodId;
            }
            else
            {
                return int.Parse(ViewState["PRODID"].ToString());
            }
        }
        set
        {
            ViewState["PRODID"] = value;
        }
    }

    public int grpId
    {
        get
        {
            if (ViewState["GRPID"] == null)
            {
                return _grpId;
            }
            else
            {
                return int.Parse(ViewState["GRPID"].ToString());
            }
        }
        set
        {
            ViewState["GRPID"] = value;
        }
    }

    public int prodgrpId
    {
        get
        {
            if (ViewState["PRODGRPID"] == null)
            {
                return _prodgrpId;
            }
            else
            {
                return int.Parse(ViewState["PRODID"].ToString());
            }
        }
        set
        {
            ViewState["PRODGRPID"] = value;
        }
    }

    public string pageListingURL
    {
        get { return ViewState["PAGELISTINGURL"] as string ?? _pageListingURL; }
        set { ViewState["PAGELISTINGURL"] = value; }
    }

    protected int groupParentChild
    {
        get { return Convert.ToInt16(Session[clsAdmin.CONSTPROJECTGROUPPARENTCHILD]); }
        set { Session["GROUPPARENTCHILD"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Page.Form.Attributes.Add("enctype", "multipart/form-data");

            ucAdmProductPrice.mode = mode;
            ucAdmProductPrice.prodId = prodId;
            ucAdmProductPrice.boolSubContent = true;
            ucAdmProductPrice.fill();

            clsConfig config = new clsConfig();
            clsMis mis = new clsMis();

            //Customization for YSHamper Project - start
            if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
            {
                lblShowTop.Text = "Hot Deals:";
                trSold.Visible = false;
                trRent.Visible = false;
                trOutofStock.Visible = false;
                trPreOrder.Visible = false;
                trBestSeller.Visible = false;
                trStockStatus.Visible = true;
                trContainLiquid.Visible = true;

                
                DataSet dsStockStatus = new DataSet();
                dsStockStatus = mis.getListByListGrp(clsAdmin.CONSTLISTSTOCKSTATUS, 1);
                
                DataView dvStockStatus = new DataView(dsStockStatus.Tables[0]);

                ddlStockStatus.DataSource = dvStockStatus;
                ddlStockStatus.DataTextField = "LIST_NAME";
                ddlStockStatus.DataValueField = "LIST_VALUE";
                ddlStockStatus.DataBind();
            }
            else
            {
                lblShowTop.Text = "Show on Top:";
            }
            //Customization for YSHamper Project - end

            trAddon.Visible = Session[clsAdmin.CONSTPROJECTPRODADDON] != null && Convert.ToString(Session[clsAdmin.CONSTPROJECTPRODADDON]) == "1";

            clsMatrixConversion matrix = new clsMatrixConversion();
            string strDimensionUnit = !string.IsNullOrEmpty(config.dimensionUnit) ? config.dimensionUnit : matrix.getDefaultMatrixByGroup(clsConfig.CONSTGROUPPRODUCTDIMENSION); ;
            string strWeightUnit = !string.IsNullOrEmpty(config.weightUnit) ? config.weightUnit : matrix.getDefaultMatrixByGroup(clsConfig.CONSTGROUPPRODUCTWEIGHT); ;
            
            litLengthUnit.Text = strDimensionUnit;
            litWidthUnit.Text = strDimensionUnit;
            litHeightUnit.Text = strDimensionUnit;
            litWeightUnit.Text = strWeightUnit;
            litVolumatricWeightUnit.Text = " kg";
            lblNotInUse.Text = "(not in use)";

            DataSet dsGrping = new DataSet();

            if (groupParentChild == 0)
            {
                litCat1.Text = mis.getListNameByListValue("1", "GROUPING", 1, 1);
                litCat2.Text = mis.getListNameByListValue("2", "GROUPING", 1, 1);
                litCat3.Text = mis.getListNameByListValue("3", "GROUPING", 1, 1);
                litCat4.Text = mis.getListNameByListValue("4", "GROUPING", 1, 1);
                litCat5.Text = mis.getListNameByListValue("5", "GROUPING", 1, 1);
                litCat6.Text = mis.getListNameByListValue("6", "GROUPING", 1, 1);
                litCat7.Text = mis.getListNameByListValue("7", "GROUPING", 1, 1);
                litCat8.Text = mis.getListNameByListValue("8", "GROUPING", 1, 1);
                litCat9.Text = mis.getListNameByListValue("9", "GROUPING", 1, 1);
                litCat10.Text = mis.getListNameByListValue("10", "GROUPING", 1, 1);
            }
            else
            {
                litCat11.Text = mis.getListNameByListValue("1", "GROUPING", 1, 1);
            }


            /* Get Dimension Conversion Rate*/
            decDimensionConvRate = getMatrixConvRate(strDimensionUnit, clsConfig.CONSTGROUPPRODUCTDIMENSION);
            decWeightConvRate = getMatrixConvRate(strWeightUnit, clsConfig.CONSTGROUPPRODUCTWEIGHT);
            /* End Get Dimension Conversion Rate*/

            aspectRatio = config.aspectRatio;
            string[] aspectRatioXandY = aspectRatio.Split('/');
            aspectRatioX = aspectRatioXandY[0];
            aspectRatioY = aspectRatioXandY[1];
        }

        
    }

    protected void validateProdCode_server(object source, ServerValidateEventArgs args)
    {
        txtProdCode.Text = txtProdCode.Text.Trim().ToUpper();

        clsProduct prod = new clsProduct();
        if (prod.isProdCodeExist(txtProdCode.Text, prodId))
        {
            args.IsValid = false;
            cvProdCode.ErrorMessage = "<br />This product code is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvProdCode_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("ProdCode")))
        {
            string strJS = null;
            strJS = "<script = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtProdCode.ClientID + "', document.all['" + cvProdCode.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "ProdCode", strJS, false);
        }
    }

    protected void cvPageTitleFriendlyUrl_server(object source, ServerValidateEventArgs args)
    {
        clsProduct prod = new clsProduct();

        if (prod.isPageTitleFUrlExist(txtPageTitleFriendlyUrl.Text, prodId))
        {
            args.IsValid = false;
            cvPageTitleFriendlyUrl.ErrorMessage = "<br/>This " + txtPageTitleFriendlyUrl.Text + " is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvPageTitleFriendlyUrl_PreRender(object source, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("prodpagetitlefriendlyurl")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtPageTitleFriendlyUrl.ClientID + "', document.all['" + cvPageTitleFriendlyUrl.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "prodpagetitlefriendlyurl", strJS);
        }
    }

    protected void validateProdImage_server(object source, ServerValidateEventArgs args)
    {
        HttpFileCollection uploads = Request.Files;

        HttpPostedFile postedFile = HttpContext.Current.Request.Files["fileProdImage"];
        if (postedFile.ContentLength > 0)
        {
            int intImgMaxSize = 0;
            if (Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING] != null && Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }


            if (postedFile.ContentLength > intImgMaxSize)
            {
                cvProdImage.ErrorMessage = "File size exceeded " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + ".";
                args.IsValid = false;
            }
            else
            {
                Match matchImage = Regex.Match(postedFile.FileName, clsAdmin.CONSTADMIMGALLOWEDEXT, RegexOptions.IgnoreCase);
                if (matchImage.Success)
                {
                    args.IsValid = true;
                }
                else
                {
                    cvProdImage.ErrorMessage = "Please enter valid product image. (" + clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + ").";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cvProdImage_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("ProdImage")))
        {
            string strJS = null;
            strJS = "<script type = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('fileProdImage', document.all['" + cvProdImage.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "ProdImage", strJS, false);
        }
    }

    protected void lnkbtnDeleteImage_Click(object sender, EventArgs e)
    {
        pnlProdImage.Visible = false;
        hdnProdImage.Value = "";
    }

    protected void oriCat_OnDelete(object sender, CommandEventArgs e)
    {
        string strOriCatToDelete = e.CommandArgument.ToString();
        string[] strOriCatToDeleteSplit = strOriCatToDelete.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

        Session["DELCAT"] += strOriCatToDeleteSplit[0] + clsAdmin.CONSTDEFAULTSEPERATOR;
        Session["DELCATNAME"] += strOriCatToDeleteSplit[1] + clsAdmin.CONSTDEFAULTSEPERATOR;

        populateData();
    }

    protected void cat_OnDelete(object sender, CommandEventArgs e)
    {
        string strCatToDelete = e.CommandArgument.ToString();
        string[] strCatToDeleteSplit = strCatToDelete.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

        Session["CAT"] = Session["CAT"].ToString().Replace(clsAdmin.CONSTDEFAULTSEPERATOR + strCatToDeleteSplit[0] + clsAdmin.CONSTDEFAULTSEPERATOR, clsAdmin.CONSTDEFAULTSEPERATOR.ToString());
        Session["CATNAME"] = Session["CATNAME"].ToString().Replace(clsAdmin.CONSTDEFAULTSEPERATOR + strCatToDeleteSplit[1] + clsAdmin.CONSTDEFAULTSEPERATOR, clsAdmin.CONSTDEFAULTSEPERATOR.ToString());

        populateData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlCat.SelectedValue))
        {
            Boolean boolFound = false;
            string strCat = ddlCat.SelectedValue;
            string strCatName = ddlCat.SelectedItem.Text;

            if (!string.IsNullOrEmpty(ddlSubCat.SelectedValue))
            {
                strCat += clsAdmin.CONSTCOMMASEPARATOR + ddlSubCat.SelectedValue;
                strCatName += clsAdmin.CONSTCOMMASEPARATOR + ddlSubCat.SelectedItem.Text;

            }

            strCat += clsAdmin.CONSTDEFAULTSEPERATOR;
            strCatName += clsAdmin.CONSTDEFAULTSEPERATOR;

            if (Session["DELCAT"] != null && Session["DELCAT"] != "")
            {
                if (Session["DELCAT"].ToString().IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + strCat) >= 0)
                {
                    boolFound = true;
                    Session["DELCAT"] = Session["DELCAT"].ToString().Replace(clsAdmin.CONSTDEFAULTSEPERATOR + strCat, clsAdmin.CONSTDEFAULTSEPERATOR.ToString());
                    //Session["DELCATNAME"] = Session["DELCATNAME"].ToString().Replace(clsAdmin.CONSTDEFAULTSEPERATOR + strCatName, clsAdmin.CONSTDEFAULTSEPERATOR.ToString());
                }
            }

            if (Session["CAT"] != null && Session["CAT"] != "")
            {
                if (Session["CAT"].ToString().IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + strCat) >= 0) { boolFound = true; }
            }

            if (Session["ORICAT"] != null && Session["ORICAT"] != "")
            {
                if (Session["ORICAT"].ToString().IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + strCat) >= 0) { boolFound = true; }
            }

            if (!boolFound)
            {
                Session["CAT"] += strCat;
                Session["CATNAME"] += strCatName;
            }

            //ddlCat.SelectedValue = "";

            //ddlSubCat.Items.Clear();
            //ListItem ddlSubCatDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
            //ddlSubCat.Items.Insert(0, ddlSubCatDefaultItem);

            //ddlSubSubCat.Items.Clear();
            //ListItem ddlSubSubCatDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
            //ddlSubSubCat.Items.Insert(0, ddlSubSubCatDefaultItem);

            populateData();
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPRODFOLDER + "/";
        string strDeletedProdName = "";

        clsMis.performDeleteProduct(prodId, uploadPath, ref strDeletedProdName);

        Session["DELETEDPRODNAME"] = strDeletedProdName;
        Response.Redirect(ConfigurationManager.AppSettings["scriptBase"].ToString() + "adm/admProduct01.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            HttpPostedFile file = Request.Files["fileProdImage"];
            prodCode = txtProdCode.Text.Trim().ToUpper();
            prodName = txtProdName.Text.Trim();
            prodNameJp = txtProdNameJp.Text.Trim();
            string prodNameMs = txtProdNameMs.Text.Trim();
            string prodNameZh = txtProdNameZh.Text.Trim();
            prodDName = txtProdDName.Text.Trim();
            prodDNameJp = txtProdDNameJp.Text.Trim();
            string prodDNameMs = txtProdDNameMs.Text.Trim();
            string prodDNameZh = txtProdDNameZh.Text.Trim();
            pageTitleFriendlyUrl = txtPageTitleFriendlyUrl.Text.Trim();
            prodUOM = txtUOM.Text.Trim();
            prodWeight = txtWeight.Text != "" ? Convert.ToInt16(txtWeight.Text.Trim()) : 0;
            prodLength = txtLength.Text != "" ? Convert.ToInt16(txtLength.Text.Trim()) : 0;
            prodWidth = txtWidth.Text != "" ? Convert.ToInt16(txtWidth.Text.Trim()) : 0;
            prodHeight = txtHeight.Text != "" ? Convert.ToInt16(txtHeight.Text.Trim()) : 0;
            prodCat1 = !string.IsNullOrEmpty(ddlCat1.SelectedValue) ? int.Parse(ddlCat1.SelectedValue) : 0;
            prodCat2 = !string.IsNullOrEmpty(ddlCat2.SelectedValue) ? int.Parse(ddlCat2.SelectedValue) : 0;
            prodCat3 = !string.IsNullOrEmpty(ddlCat3.SelectedValue) ? int.Parse(ddlCat3.SelectedValue) : 0;
            prodCat4 = !string.IsNullOrEmpty(ddlCat4.SelectedValue) ? int.Parse(ddlCat4.SelectedValue) : 0;
            prodCat5 = !string.IsNullOrEmpty(ddlCat5.SelectedValue) ? int.Parse(ddlCat5.SelectedValue) : 0;
            prodCat6 = !string.IsNullOrEmpty(ddlCat6.SelectedValue) ? int.Parse(ddlCat6.SelectedValue) : 0;
            prodCat7 = !string.IsNullOrEmpty(ddlCat7.SelectedValue) ? int.Parse(ddlCat7.SelectedValue) : 0;
            prodCat8 = !string.IsNullOrEmpty(ddlCat8.SelectedValue) ? int.Parse(ddlCat8.SelectedValue) : 0;
            prodCat9 = !string.IsNullOrEmpty(ddlCat9.SelectedValue) ? int.Parse(ddlCat9.SelectedValue) : 0;
            prodCat10 = !string.IsNullOrEmpty(ddlCat10.SelectedValue) ? int.Parse(ddlCat10.SelectedValue) : 0;
            prodNew = chkboxNew.Checked ? 1 : 0;
            prodBest = chkboxBest.Checked ? 1 : 0;
            prodShowTop = chkboxShowTop.Checked ? 1 : 0;
            prodSold = chkboxSold.Checked ? 1 : 0;
            prodRent = chkboxRent.Checked ? 1 : 0;
            prodOutOfStock = chkboxOutOfStock.Checked ? 1 : 0;
            prodPreorder = chkboxPreorder.Checked ? 1 : 0;
            prodWithGst = chkboxWithGst.Checked ? 1 : 0;   
            prodActive = chkboxActive.Checked ? 1 : 0;
            prodAddon = chkboxAddon.Checked ? 1 : 0;
            prodSales = chkboxSales.Checked ? 1 : 0;

            //Customized for YSHamper Project
            clsConfig config = new clsConfig();
            int prodstockstatus = 0;
            int prodContainLiquid = 0;
            if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
            {
                prodstockstatus = !string.IsNullOrEmpty(ddlStockStatus.SelectedValue) ? int.Parse(ddlStockStatus.SelectedValue) : 0;
                prodContainLiquid = chkboxContainLiquid.Checked ? 1 : 0;
            }
            //End of Customized for YSHamper Project



            //  cropped image - sh.chong 22 Sep 2015
            uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPRODFOLDER + "/";
            bool boolHasCroppedImg = (!string.IsNullOrEmpty(hdnImgWidth.Value)) &&
                                     (!string.IsNullOrEmpty(hdnImgHeight.Value)) &&
                                     (!string.IsNullOrEmpty(hdnImgX.Value)) &&
                                     (!string.IsNullOrEmpty(hdnImgY.Value)) &&
                                     (!string.IsNullOrEmpty(hdnImgCropped.Value)) &&
                                     (!string.IsNullOrEmpty(hdnImgName.Value));


            bool boolHasFile = file.ContentLength > 0;

            //if (fileProdImage.HasFile)
            if (boolHasCroppedImg || boolHasFile)
            {
                try
                {
                    string newFilename = "";
                    if (boolHasCroppedImg)
                    {
                        decImgWidth = Convert.ToDecimal(hdnImgWidth.Value);
                        decImgHeight = Convert.ToDecimal(hdnImgHeight.Value);
                        decImgX = Convert.ToDecimal(hdnImgX.Value);
                        decImgY = Convert.ToDecimal(hdnImgY.Value);
                        strImgCropped = hdnImgCropped.Value;
                        strImgName = hdnImgName.Value;

                        newFilename = clsMis.GetUniqueFilename(strImgName, uploadPath);
                        string base64 = strImgCropped;
                        byte[] bytes = Convert.FromBase64String(base64.Split(',')[1]);
                        prodImage = newFilename;

                        using (FileStream fs = new FileStream(uploadPath + newFilename, FileMode.Create))
                        {
                            using (BinaryWriter bw = new BinaryWriter(fs)) { bw.Write(bytes); bw.Close(); }
                        }
                    }
                    else if (boolHasFile)
                    {
                        string fileName = Path.GetFileName(file.FileName);
                        clsMis.createFolder(uploadPath);
                        newFilename = clsMis.GetUniqueFilename(fileName, uploadPath);
                        file.SaveAs(uploadPath + newFilename);
                        prodImage = newFilename;
                    }




                    //Add by sh.chong 20Apr2015 for water mark purpose
                    clsMis mis = new clsMis();

                    DataSet ds = config.getItemList(1);
                    DataRow[] dwWatermarks = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPWATERMARKSETTINGS + "'");

                    string strWatermarkType = "0",
                           strWatermarkValue = "",
                           strWatermarkOpacity = "0.1";

                    if (dwWatermarks.Length > 0)
                    {
                        foreach (DataRow dwWatermark in dwWatermarks)
                        {
                            if (dwWatermark["CONF_NAME"].ToString() == clsConfig.CONSTNAMEWATERMARKTYPE)
                            {
                                if (!string.IsNullOrEmpty(dwWatermark["CONF_VALUE"].ToString()))
                                {
                                    strWatermarkType = dwWatermark["CONF_VALUE"].ToString();
                                }
                            }
                            else if (dwWatermark["CONF_NAME"].ToString() == clsConfig.CONSTNAMEWATERMARKVALUE)
                            {
                                strWatermarkValue = dwWatermark["CONF_VALUE"].ToString();
                            }
                            else if (dwWatermark["CONF_NAME"].ToString() == clsConfig.CONSTNAMEWATERMARKOPACITY)
                            {
                                strWatermarkOpacity = dwWatermark["CONF_VALUE"].ToString();
                            }
                        }

                    }
                    bool boolWatermarkImageExists = (strWatermarkType == clsAdmin.CONSTWATERMARKTYPEIMAGE && !string.IsNullOrEmpty(strWatermarkValue));
                    bool boolWatermarkTextExists = (strWatermarkType == clsAdmin.CONSTWATERMARKTYPETEXT && !string.IsNullOrEmpty(strWatermarkValue));

                    if (boolWatermarkImageExists)
                    {
                        string strWatermarkUploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCTRLPNLADMFOLDER + "/";

                        //Bitmap bitOriginal = new Bitmap(postedFile.InputStream, false);
                        Bitmap bitOriginal = new Bitmap(uploadPath + newFilename);
                        Bitmap bitWatermark = new Bitmap(strWatermarkUploadPath + strWatermarkValue, false);
                        float opacity = float.Parse(strWatermarkOpacity);

                        Bitmap bitNew = new Bitmap(mis.drawWatermarkImage(bitOriginal, bitWatermark, opacity));
                        bitOriginal.Dispose();
                        bitOriginal = null;
                        bitNew.Save(uploadPath + newFilename);
                    }
                    else if (boolWatermarkTextExists)
                    {
                        double dblOpacity = double.Parse(strWatermarkOpacity) * 255;
                        //Bitmap bitOriginal = new Bitmap(postedFile.InputStream, false);

                        Bitmap bitOriginal = new Bitmap(uploadPath + newFilename, true);
                        Bitmap bitNew = new Bitmap(mis.drawWatermarkText(bitOriginal, strWatermarkValue, (int)dblOpacity));
                        bitOriginal.Dispose();
                        bitOriginal = null;
                        bitNew.Save(uploadPath + newFilename);
                    }
                    //End by sh.chong 20Apr2015 for water mark purpose
                }
                catch (Exception ex)
                {
                    clsLog.logErroMsg(ex.Message.ToString());
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />" + ex.Message.ToString() + "</div>";

                    throw ex;
                }
            }
            else
            {
                if (mode == 1)
                {
                    prodImage = "";
                }
                else
                {
                    prodImage = hdnProdImage.Value;
                }
            }

            clsProduct prod = new clsProduct();
            int intRecordAffected = 0;

            switch (mode)
            {
                case 1: //Add Product
                    intRecordAffected = prod.addProduct(prodCode, prodName, prodNameJp, prodNameMs, prodNameZh, prodDName, prodDNameJp, prodDNameMs, prodDNameZh, pageTitleFriendlyUrl,
                                                        prodUOM, prodWeight, prodLength, prodWidth, prodHeight, prodNew, prodBest, prodActive, prodShowTop, prodSold, prodRent,
                                                        prodOutOfStock, prodPreorder, prodWithGst, prodImage, int.Parse(Session["ADMID"].ToString()), prodstockstatus, prodContainLiquid,
                                                        prodAddon, prodSales);

                    if (intRecordAffected == 1)
                    {
                        foreach (TreeNode node in tvCategory.CheckedNodes)
                        {
                            if (tvCategory.CheckedNodes.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(node.Value))
                                {
                                    prod.assignGrpToProd(prod.prodId, Convert.ToInt16(node.Value));
                                }
                            }
                        }

                        //if (prodCat1 != 0)
                        //{
                        //    prod.assignGrpToProd(prod.prodId, prodCat1);
                        //}
                        //if (prodCat2 != 0)
                        //{
                        //    prod.assignGrpToProd(prod.prodId, prodCat2);
                        //}

                        //if (prodCat3 != 0)
                        //{
                        //    prod.assignGrpToProd(prod.prodId, prodCat3);
                        //}

                        //if (prodCat4 != 0)
                        //{
                        //    prod.assignGrpToProd(prod.prodId, prodCat4);
                        //}

                        //if (prodCat5 != 0)
                        //{
                        //    prod.assignGrpToProd(prod.prodId, prodCat5);
                        //}

                        //if (prodCat6 != 0)
                        //{
                        //    prod.assignGrpToProd(prod.prodId, prodCat6);
                        //}

                        //if (prodCat7 != 0)
                        //{
                        //    prod.assignGrpToProd(prod.prodId, prodCat7);
                        //}

                        //if (prodCat8 != 0)
                        //{
                        //    prod.assignGrpToProd(prod.prodId, prodCat8);
                        //}

                        //if (prodCat9 != 0)
                        //{
                        //    prod.assignGrpToProd(prod.prodId, prodCat9);
                        //}

                        //if (prodCat10 != 0)
                        //{
                        //    prod.assignGrpToProd(prod.prodId, prodCat10);
                        //}
                        ucAdmProductPrice.save();
                        Session["NEWPRODID"] = prod.prodId;
                        Response.Redirect(currentPageName + "?id=" + prod.prodId);
                    }
                    else
                    {
                        Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new product. Please try again.</div>";
                        Response.Redirect(currentPageName);
                    }
                    break;
                case 2:

                    Boolean bEdited = false;
                    Boolean bSuccess = true;

                    string strCatNode = "";

                    foreach (TreeNode node in tvCategory.CheckedNodes)
                    {
                        strCatNode += node.Value + clsAdmin.CONSTCOMMASEPARATOR.ToString();

                        if (!prod.isProdGroupExist(prodId, Convert.ToInt16(node.Value)))
                        {
                            intRecordAffected = prod.assignGrpToProd(prodId, Convert.ToInt16(node.Value));
                        }

                        if (intRecordAffected == 1)
                        {
                            bEdited = true;
                        }
                    }

                    //if (groupParentChild == 0)
                    //{
                        //    int intCat1Old = !string.IsNullOrEmpty(hdnCat1.Value.Trim()) ? int.Parse(hdnCat1.Value.Trim()) : 0;
                        //    if (prodCat1 != intCat1Old)
                        //    {
                        //        if (intCat1Old == 0)
                        //        {
                        //            intRecordAffected = prod.assignGrpToProd(prodId, prodCat1);
                        //        }
                        //        else
                        //        {
                        //            intRecordAffected = prod.updateGrpToProd(prodId, prodCat1, intCat1Old);
                        //        }

                        //        if (intRecordAffected == 1)
                        //        {
                        //            bEdited = true;
                        //        }
                        //    }

                        //    int intCat2Old = !string.IsNullOrEmpty(hdnCat2.Value.Trim()) ? int.Parse(hdnCat2.Value.Trim()) : 0;
                        //    if (prodCat2 != intCat2Old)
                        //    {
                        //        if (intCat2Old == 0)
                        //        {
                        //            intRecordAffected = prod.assignGrpToProd(prodId, prodCat2);
                        //        }
                        //        else
                        //        {
                        //            intRecordAffected = prod.updateGrpToProd(prodId, prodCat2, intCat2Old);
                        //        }
                        //        if (intRecordAffected == 1)
                        //        {
                        //            bEdited = true;
                        //        }
                        //    }

                        //    int intCat3Old = !string.IsNullOrEmpty(hdnCat3.Value.Trim()) ? int.Parse(hdnCat3.Value.Trim()) : 0;
                        //    if (prodCat3 != intCat3Old)
                        //    {
                        //        if (intCat3Old == 0)
                        //        {
                        //            intRecordAffected = prod.assignGrpToProd(prodId, prodCat3);
                        //        }
                        //        else
                        //        {
                        //            intRecordAffected = prod.updateGrpToProd(prodId, prodCat3, intCat3Old);
                        //        }
                        //        if (intRecordAffected == 1)
                        //        {
                        //            bEdited = true;
                        //        }
                        //    }

                        //    int intCat4Old = !string.IsNullOrEmpty(hdnCat4.Value.Trim()) ? int.Parse(hdnCat4.Value.Trim()) : 0;
                        //    if (prodCat4 != intCat4Old)
                        //    {
                        //        if (intCat4Old == 0)
                        //        {
                        //            intRecordAffected = prod.assignGrpToProd(prodId, prodCat4);
                        //        }
                        //        else
                        //        {
                        //            intRecordAffected = prod.updateGrpToProd(prodId, prodCat4, intCat4Old);
                        //        }
                        //        if (intRecordAffected == 1)
                        //        {
                        //            bEdited = true;
                        //        }
                        //    }

                        //    int intCat5Old = !string.IsNullOrEmpty(hdnCat5.Value.Trim()) ? int.Parse(hdnCat5.Value.Trim()) : 0;
                        //    if (prodCat5 != intCat5Old)
                        //    {
                        //        if (intCat5Old == 0)
                        //        {
                        //            intRecordAffected = prod.assignGrpToProd(prodId, prodCat5);
                        //        }
                        //        else
                        //        {
                        //            intRecordAffected = prod.updateGrpToProd(prodId, prodCat5, intCat5Old);
                        //        }
                        //        if (intRecordAffected == 1)
                        //        {
                        //            bEdited = true;
                        //        }
                        //    }

                        //    int intCat6Old = !string.IsNullOrEmpty(hdnCat6.Value.Trim()) ? int.Parse(hdnCat6.Value.Trim()) : 0;
                        //    if (prodCat6 != intCat6Old)
                        //    {
                        //        if (intCat6Old == 0)
                        //        {
                        //            intRecordAffected = prod.assignGrpToProd(prodId, prodCat6);
                        //        }
                        //        else
                        //        {
                        //            intRecordAffected = prod.updateGrpToProd(prodId, prodCat6, intCat6Old);
                        //        }
                        //        if (intRecordAffected == 1)
                        //        {
                        //            bEdited = true;
                        //        }
                        //    }

                        //    int intCat7Old = !string.IsNullOrEmpty(hdnCat7.Value.Trim()) ? int.Parse(hdnCat7.Value.Trim()) : 0;
                        //    if (prodCat7 != intCat7Old)
                        //    {
                        //        if (intCat7Old == 0)
                        //        {
                        //            intRecordAffected = prod.assignGrpToProd(prodId, prodCat7);
                        //        }
                        //        else
                        //        {
                        //            intRecordAffected = prod.updateGrpToProd(prodId, prodCat7, intCat7Old);
                        //        }
                        //        if (intRecordAffected == 1)
                        //        {
                        //            bEdited = true;
                        //        }
                        //    }

                        //    int intCat8Old = !string.IsNullOrEmpty(hdnCat8.Value.Trim()) ? int.Parse(hdnCat8.Value.Trim()) : 0;
                        //    if (prodCat8 != intCat8Old)
                        //    {
                        //        if (intCat8Old == 0)
                        //        {
                        //            intRecordAffected = prod.assignGrpToProd(prodId, prodCat8);
                        //        }
                        //        else
                        //        {
                        //            intRecordAffected = prod.updateGrpToProd(prodId, prodCat8, intCat8Old);
                        //        }
                        //        if (intRecordAffected == 1)
                        //        {
                        //            bEdited = true;
                        //        }
                        //    }

                        //    int intCat9Old = !string.IsNullOrEmpty(hdnCat9.Value.Trim()) ? int.Parse(hdnCat9.Value.Trim()) : 0;
                        //    if (prodCat9 != intCat9Old)
                        //    {
                        //        if (intCat9Old == 0)
                        //        {
                        //            intRecordAffected = prod.assignGrpToProd(prodId, prodCat9);
                        //        }
                        //        else
                        //        {
                        //            intRecordAffected = prod.updateGrpToProd(prodId, prodCat9, intCat9Old);
                        //        }
                        //        if (intRecordAffected == 1)
                        //        {
                        //            bEdited = true;
                        //        }
                        //    }

                        //    int intCat10Old = !string.IsNullOrEmpty(hdnCat10.Value.Trim()) ? int.Parse(hdnCat10.Value.Trim()) : 0;
                        //    if (prodCat10 != intCat10Old)
                        //    {
                        //        if (intCat10Old == 0)
                        //        {
                        //            intRecordAffected = prod.assignGrpToProd(prodId, prodCat10);
                        //        }
                        //        else
                        //        {
                        //            intRecordAffected = prod.updateGrpToProd(prodId, prodCat10, intCat10Old);
                        //        }
                        //        if (intRecordAffected == 1)
                        //        {
                        //            bEdited = true;
                        //        }
                        //    }
                        //}
                    //}
                    
                    if (!prod.isExactSameProdSet1(prodId, prodCode, prodName, prodNameJp, prodNameMs, prodNameZh, prodDName, prodDNameJp, prodDNameMs, prodDNameZh, pageTitleFriendlyUrl,
                                                    prodUOM, prodWeight, prodLength, prodWidth, prodHeight, prodNew, prodBest, prodActive, prodShowTop, prodSold, prodRent,
                                                    prodOutOfStock, prodPreorder, prodWithGst, prodImage, prodstockstatus, prodContainLiquid, prodAddon, prodSales))
                    {
                        intRecordAffected = prod.updateProdById(prodId, prodCode, prodName, prodNameJp, prodNameMs, prodNameZh, prodDName, prodDNameJp, prodDNameMs, prodDNameZh, pageTitleFriendlyUrl,
                                                                prodUOM, prodWeight, prodLength, prodWidth, prodHeight, prodNew, prodBest, prodActive, prodShowTop, prodSold, prodRent,
                                                                prodOutOfStock, prodPreorder, prodWithGst, prodImage, int.Parse(Session["ADMID"].ToString()), prodstockstatus, prodContainLiquid,
                                                                prodAddon, prodSales);

                        if (intRecordAffected == 1)
                        {
                            if (hdnProdImageRef.Value != prodImage && hdnProdImageRef.Value != "")
                            {
                                if (File.Exists(uploadPath + hdnProdImageRef.Value)) { File.Delete(uploadPath + hdnProdImageRef.Value); }
                            }
                            bEdited = true;
                        }
                        else
                        {
                            bSuccess = false;
                        }
                    }

                    if (bSuccess)
                    {
                        if (!string.IsNullOrEmpty(strCatNode))
                        {
                            strCatNode = strCatNode.Substring(0, strCatNode.Length - clsAdmin.CONSTCOMMASEPARATOR.ToString().Length);

                            DataSet dsProdGrp = new DataSet();
                            dsProdGrp = prod.getProdGroupListById(prodId);
                            DataView dvProdGrp = new DataView(dsProdGrp.Tables[0]);
                            dvProdGrp.RowFilter = "GRP_ID NOT IN (" + strCatNode + ")";

                            foreach (DataRowView dr in dvProdGrp)
                            {
                                int intCatId = Convert.ToInt16(dr["GRP_ID"]);

                                intRecordAffected = prod.deleteProductGroup(prodId, intCatId);

                                if (intRecordAffected == 1) { bEdited = true; }
                                else
                                {
                                    bSuccess = false;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            intRecordAffected = prod.deleteProductGroup(prodId);

                            if (intRecordAffected >= 1) { bEdited = true; }
                            //else
                            //{
                            //    bSuccess = false;
                            //    break;
                            //}
                        }
                    }

                    /*Category And Sub-Category (Parent Group - Child)*/
                    //if (bSuccess && Session["DELCAT"] != null && Session["DELCAT"] != "")
                    //{
                    //    string strDelCat = Session["DELCAT"].ToString();
                    //    strDelCat = strDelCat.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strDelCat.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                    //    if (!string.IsNullOrEmpty(strDelCat))
                    //    {
                    //        strDelCat = strDelCat.Substring(0, strDelCat.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    //        string[] strDelCatSplit = strDelCat.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                    //        for (int i = 0; i <= strDelCatSplit.GetUpperBound(0); i++)
                    //        {
                    //            string[] strDelCatSplitSplit = strDelCatSplit[i].Split(clsAdmin.CONSTCOMMASEPARATOR);

                    //            // get lowest level Category Id only
                    //            int intCatId = Convert.ToInt16(strDelCatSplitSplit[strDelCatSplitSplit.Length - 1]);

                    //            intRecordAffected = prod.deleteProductGroup(prodId, intCatId);

                    //            if (intRecordAffected == 1) { bEdited = true; }
                    //            else
                    //            {
                    //                bSuccess = false;
                    //                break;
                    //            }
                    //        }
                    //    }
                    //}

                    //if (bSuccess && Session["CAT"] != null && Session["CAT"] != "")
                    //{
                    //    string strCat = Session["CAT"].ToString();
                    //    strCat = strCat.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strCat.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                    //    if (!string.IsNullOrEmpty(strCat))
                    //    {
                    //        strCat = strCat.Substring(0, strCat.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    //        string[] strCatSplit = strCat.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                    //        for (int i = 0; i <= strCatSplit.GetUpperBound(0); i++)
                    //        {
                    //            string[] strCatSplitSplit = strCatSplit[i].Split(clsAdmin.CONSTCOMMASEPARATOR);

                    //            // get lowest level Category Id only
                    //            int intCatId = Convert.ToInt16(strCatSplitSplit[strCatSplitSplit.Length - 1]);

                    //            intRecordAffected = prod.assignGrpToProd(prodId, intCatId);

                    //            if (intRecordAffected == 1) { bEdited = true; }
                    //            else
                    //            {
                    //                clsLog.logErroMsg("Failed Message when saving");
                    //                bSuccess = false;
                    //                break;
                    //            }
                    //        }
                    //    }
                    //}
                    ucAdmProductPrice.save();
                    if (bEdited)
                    {
                        Session["EDITPRODID"] = prod.prodId;
                    }
                    else
                    {
                        if (!bSuccess)
                        {
                            prod.extractProdById(prodId, 0);
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit product (" + prod.prodName + "). Please try again.</div>";
                        }
                        else
                        {
                            Session["EDITPRODID"] = prodId;
                            Session["NOCHANGE"] = 1;                                                                                                                     
                        }
                    }

                    Response.Redirect(Request.Url.ToString());
                    break;
                default:
                    break;
            }
        }
    }

    #endregion

    #region "Methods"
    public void fill()
    {
        if (!IsPostBack)
        {
            setPageProperties();
            fillGrouping();
            checkLanguage();
        }

        if (groupParentChild != 0)
        {
            populateData();
        }
    }

    protected void setPageProperties()
    {
        if (Session[clsAdmin.CONSTPROJECTFRIENDLYURL] != null && clsAdmin.CONSTPROJECTFRIENDLYURL != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFRIENDLYURL]) == 0)
            {
                trFriendlyURL.Visible = false;
            }
        }

        DataSet dsGroup = new DataSet();
        clsGroup grp = new clsGroup();
        dsGroup = grp.getGrpListByGrpingId2(0, 0);

        if (groupParentChild == 0)
        {
            DataView dvCat1 = new DataView(dsGroup.Tables[0]);
            dvCat1.RowFilter = "GRPING_ID = 1";
            dvCat1.Sort = "GRP_NAME ASC";
            ddlCat1.DataSource = dvCat1;
            ddlCat1.DataTextField = "GRP_NAME";
            ddlCat1.DataValueField = "GRP_ID";
            ddlCat1.DataBind();

            if (ddlCat1.Items.Count <= 0)
            {
                ddlCat1.Enabled = false;
            }
            else
            {
                ListItem ddlCatDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlCat1.Items.Insert(0, ddlCatDefaultItem);
            }

            DataView dvCat2 = new DataView(dsGroup.Tables[0]);
            dvCat2.RowFilter = "GRPING_ID = 2";
            dvCat2.Sort = "GRP_NAME ASC";
            ddlCat2.DataSource = dvCat2;
            ddlCat2.DataTextField = "GRP_NAME";
            ddlCat2.DataValueField = "GRP_ID";
            ddlCat2.DataBind();

            if (ddlCat2.Items.Count <= 0)
            {
                ddlCat2.Enabled = false;
            }
            else
            {
                ListItem ddlBrandDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlCat2.Items.Insert(0, ddlBrandDefaultItem);
            }

            DataView dvCat3 = new DataView(dsGroup.Tables[0]);
            dvCat3.RowFilter = "GRPING_ID = 3";
            dvCat3.Sort = "GRP_NAME ASC";
            ddlCat3.DataSource = dvCat3;
            ddlCat3.DataTextField = "GRP_NAME";
            ddlCat3.DataValueField = "GRP_ID";
            ddlCat3.DataBind();

            if (ddlCat3.Items.Count <= 0)
            {
                ddlCat3.Enabled = false;
            }
            else
            {
                ListItem ddlCat3DefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlCat3.Items.Insert(0, ddlCat3DefaultItem);
            }

            DataView dvCat4 = new DataView(dsGroup.Tables[0]);
            dvCat4.RowFilter = "GRPING_ID = 4";
            dvCat4.Sort = "GRP_NAME ASC";
            ddlCat4.DataSource = dvCat4;
            ddlCat4.DataTextField = "GRP_NAME";
            ddlCat4.DataValueField = "GRP_ID";
            ddlCat4.DataBind();

            if (ddlCat4.Items.Count <= 0)
            {
                ddlCat4.Enabled = false;
            }
            else
            {
                ListItem ddlCat4DefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlCat4.Items.Insert(0, ddlCat4DefaultItem);
            }

            DataView dvCat5 = new DataView(dsGroup.Tables[0]);
            dvCat5.RowFilter = "GRPING_ID = 5";
            dvCat5.Sort = "GRP_NAME ASC";
            ddlCat5.DataSource = dvCat5;
            ddlCat5.DataTextField = "GRP_NAME";
            ddlCat5.DataValueField = "GRP_ID";
            ddlCat5.DataBind();

            if (ddlCat5.Items.Count <= 0)
            {
                ddlCat5.Enabled = false;
            }
            else
            {
                ListItem ddlCat5DefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlCat5.Items.Insert(0, ddlCat5DefaultItem);
            }

            DataView dvCat6 = new DataView(dsGroup.Tables[0]);
            dvCat6.RowFilter = "GRPING_ID = 6";
            dvCat6.Sort = "GRP_NAME ASC";
            ddlCat6.DataSource = dvCat6;
            ddlCat6.DataTextField = "GRP_NAME";
            ddlCat6.DataValueField = "GRP_ID";
            ddlCat6.DataBind();

            if (ddlCat6.Items.Count <= 0)
            {
                ddlCat6.Enabled = false;
            }
            else
            {
                ListItem ddlCat6DefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlCat6.Items.Insert(0, ddlCat6DefaultItem);
            }

            DataView dvCat7 = new DataView(dsGroup.Tables[0]);
            dvCat7.RowFilter = "GRPING_ID = 7";
            dvCat7.Sort = "GRP_NAME ASC";
            ddlCat7.DataSource = dvCat7;
            ddlCat7.DataTextField = "GRP_NAME";
            ddlCat7.DataValueField = "GRP_ID";
            ddlCat7.DataBind();

            if (ddlCat7.Items.Count <= 0)
            {
                ddlCat7.Enabled = false;
            }
            else
            {
                ListItem ddlCat7DefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlCat7.Items.Insert(0, ddlCat7DefaultItem);
            }

            DataView dvCat8 = new DataView(dsGroup.Tables[0]);
            dvCat8.RowFilter = "GRPING_ID = 8";
            dvCat8.Sort = "GRP_NAME ASC";
            ddlCat8.DataSource = dvCat8;
            ddlCat8.DataTextField = "GRP_NAME";
            ddlCat8.DataValueField = "GRP_ID";
            ddlCat8.DataBind();

            if (ddlCat8.Items.Count <= 0)
            {
                ddlCat8.Enabled = false;
            }
            else
            {
                ListItem ddlCat8DefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlCat8.Items.Insert(0, ddlCat8DefaultItem);
            }

            DataView dvCat9 = new DataView(dsGroup.Tables[0]);
            dvCat9.RowFilter = "GRPING_ID = 9";
            dvCat9.Sort = "GRP_NAME ASC";
            ddlCat9.DataSource = dvCat9;
            ddlCat9.DataTextField = "GRP_NAME";
            ddlCat9.DataValueField = "GRP_ID";
            ddlCat9.DataBind();

            if (ddlCat9.Items.Count <= 0)
            {
                ddlCat9.Enabled = false;
            }
            else
            {
                ListItem ddlCat9DefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlCat9.Items.Insert(0, ddlCat9DefaultItem);
            }

            DataView dvCat10 = new DataView(dsGroup.Tables[0]);
            dvCat10.RowFilter = "GRPING_ID = 10";
            dvCat10.Sort = "GRP_NAME ASC";
            ddlCat10.DataSource = dvCat10;
            ddlCat10.DataTextField = "GRP_NAME";
            ddlCat10.DataValueField = "GRP_ID";
            ddlCat10.DataBind();

            if (ddlCat10.Items.Count <= 0)
            {
                ddlCat10.Enabled = false;
            }
            else
            {
                ListItem ddlCat10DefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlCat10.Items.Insert(0, ddlCat10DefaultItem);
            }
        }
        else
        {
            ddlCat1.Enabled = false; ddlCat1.Attributes.CssStyle.Add("Visibility", "Hidden");
            ddlCat2.Enabled = false; ddlCat2.Attributes.CssStyle.Add("Visibility", "Hidden");
            ddlCat3.Enabled = false; ddlCat3.Attributes.CssStyle.Add("Visibility", "Hidden");
            ddlCat4.Enabled = false; ddlCat4.Attributes.CssStyle.Add("Visibility", "Hidden");
            ddlCat5.Enabled = false; ddlCat5.Attributes.CssStyle.Add("Visibility", "Hidden");
            ddlCat6.Enabled = false; ddlCat6.Attributes.CssStyle.Add("Visibility", "Hidden");
            ddlCat7.Enabled = false; ddlCat7.Attributes.CssStyle.Add("Visibility", "Hidden");
            ddlCat8.Enabled = false; ddlCat8.Attributes.CssStyle.Add("Visibility", "Hidden");
            ddlCat9.Enabled = false; ddlCat9.Attributes.CssStyle.Add("Visibility", "Hidden");
            ddlCat10.Enabled = false; ddlCat10.Attributes.CssStyle.Add("Visibility", "Hidden");
            trDGroupParentChildCat.Visible = false;
            cddlCat.PromptText = GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString();
            cddlSubcat.PromptText = GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString();
        }

        switch (mode)
        {
            case 1://Add Product
                txtUOM.Text = GetGlobalResourceObject("GlobalResource", "defaultUOM.Text").ToString();
                break;
            case 2://Edit Product
                lnkbtnDelete.Visible = true;

                fillForm();

                break;
            default:
                break;
        }

        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEGSTACTIVATION, clsConfig.CONSTGROUPGSTSETTING, 1);
        if (config.value == clsConfig.CONSTNAMEGSTAPPLIED)
        {
            config.extractItemByNameGroup(clsConfig.CONSTNAMEGSTPRODUCTWITHGST, clsConfig.CONSTGROUPGSTSETTING, 1);
            if (config.value == clsConfig.CONSTNAMEGSTPARTIALLYAPPLIED)
            {
                trWithGst.Visible = true;
            }
        }

        int intImgMaxSize = 0;

        if (Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING] != null && Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING] != "")
        {
            if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING]) > 0)
            {
                intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING]) * 1024;
            }
            else
            {
                if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                {
                    if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                    {
                        intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                    }
                }
            }
        }

        litUploadRule.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + " | " + clsMis.formatAllowedFileDimension(clsAdmin.CONSTADMIMGALLOWEDWIDTH, clsAdmin.CONSTADMIMGALLOWEDHEIGHT, "px");
        lnkbtnBack.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
        lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteProduct.Text").ToString() + "'); return false;";
    }

    public void fillForm()
    {
        clsProduct prod = new clsProduct();
        clsConfig config = new clsConfig();

        if (prod.extractProdById(prodId, 0))
        {
            txtProdCode.Text = prod.prodCode;
            //txtProdCode.Enabled = false;
            txtProdName.Text = prod.prodName;
            txtProdNameJp.Text = prod.prodNameJp;
            txtProdNameMs.Text = prod.prodNameMs;
            txtProdNameZh.Text = prod.prodNameZh;
            txtProdDName.Text = prod.prodDName;
            txtProdDNameJp.Text = prod.prodDNameJp;
            txtProdDNameMs.Text = prod.prodDNameMs;
            txtProdDNameZh.Text = prod.prodDNameZh;
            txtPageTitleFriendlyUrl.Text = prod.prodPageTitleFUrl;
            txtUOM.Text = prod.prodUOM;
            txtWeight.Text = prod.prodWeight.ToString();
            txtLength.Text = prod.prodLength.ToString();
            txtWidth.Text = prod.prodWidth.ToString();
            txtHeight.Text = prod.prodHeight.ToString();

            chkboxNew.Checked = prod.prodNew == 1;
            chkboxShowTop.Checked = prod.prodShowTop == 1;
            chkboxSold.Checked = prod.prodSold == 1;
            chkboxRent.Checked = prod.prodRent == 1;
            chkboxOutOfStock.Checked = prod.prodOutOfStock == 1;
            chkboxPreorder.Checked = prod.prodPreorder == 1;
            chkboxBest.Checked = prod.prodBest == 1;
            chkboxActive.Checked = prod.prodActive == 1;
            chkboxWithGst.Checked = prod.prodWithGst == 1;
            chkboxAddon.Checked = prod.prodAddon == 1;
            chkboxSales.Checked = prod.prodSales == 1;

            if (prod.prodImage != "")
            {
                pnlProdImage.Visible = true;
                imgProdImage.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPRODFOLDER + "/" + prod.prodImage + "&w=" + clsAdmin.CONSTADMDEFAULTIMGWIDTH + "&h=" + clsAdmin.CONSTADMDEFAULTIMGHEIGHT + "&f=1";
                hdnProdImage.Value = prod.prodImage;
                hdnProdImageRef.Value = prod.prodImage;
            }

            if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
            {
                if (!string.IsNullOrEmpty(prod.prodStockStatus))
                {
                    ddlStockStatus.SelectedValue = prod.prodStockStatus;
                }

                if (prod.prodContainLiquid == 1) { chkboxContainLiquid.Checked = true; }
                else { chkboxContainLiquid.Checked = false; }            
            }
            
            // group
            DataSet dsProdGroup = new DataSet();

            if (groupParentChild == 0)
            {
                dsProdGroup = prod.getProdGroupListById(prodId);
                DataView dvProdGroup = new DataView(dsProdGroup.Tables[0]);
                dvProdGroup.RowFilter = "PROD_ID = " + prodId;
                //DataRow[] foundRowProdGroup;

                //foundRowProdGroup = dsProdGroup.Tables[0].Select();
                //if (foundRowProdGroup.Length > 0)
                //{
                //    TreeNode node = new TreeNode();
                //    int intCat1 = int.Parse(foundRowProdGroup[0]["GRP_ID"].ToString());

                //    if (intCat1 != 0) { node.Value = intCat1.ToString(); }
                //    //hdnCat1.Value = intCat1.ToString();
                //    //if (intCat1 != 0) { ddlCat1.SelectedValue = intCat1.ToString(); }
                //}


                //foreach (DataRowView dr in dvProdGroup)
                //{
                //    Response.Write("hello");

                //    //int grpId = dr["GRP_ID"].ToString();

                //    TreeNode node = new TreeNode();
                //    node.Value = dr["GRP_ID"].ToString();
                //    node.SelectAction = TreeNodeSelectAction.Select;
                //    node.Checked = true;
                //    node.Selected = true;

                //    if (node.Value == "3")
                //    {
                //        Response.Write("inin");
                //        node.Selected = true;
                //        //tvCategory.SelectedNode = node.Value;
                //        //tvCategory.Nodes.Add(node);
                //        //node.Checked = true;
                //    }

                //    Response.Write("value: " + node.Value);
                //    Response.Write("checked: " + node.Checked);
                //    Response.Write("<br/> selected: " + node.Selected);
                //    //node.Checked = dr["GRP_ID"].ToString();
                //}





                //dsProdGroup = prod.getProdGroupListById(prodId);
                //DataRow[] foundRowProdGroup;

                //foundRowProdGroup = dsProdGroup.Tables[0].Select("GRPING_ID = 1");
                //if (foundRowProdGroup.Length > 0)
                //{
                //    int intCat1 = int.Parse(foundRowProdGroup[0]["GRP_ID"].ToString());
                //    hdnCat1.Value = intCat1.ToString();
                //    if (intCat1 != 0) { ddlCat1.SelectedValue = intCat1.ToString(); }
                //}

                //foundRowProdGroup = dsProdGroup.Tables[0].Select("GRPING_ID = 2");
                //if (foundRowProdGroup.Length > 0)
                //{
                //    int intCat2 = int.Parse(foundRowProdGroup[0]["GRP_ID"].ToString());
                //    hdnCat2.Value = intCat2.ToString();
                //    if (intCat2 != 0) { ddlCat2.SelectedValue = intCat2.ToString(); }
                //}

                //foundRowProdGroup = dsProdGroup.Tables[0].Select("GRPING_ID = 3");
                //if (foundRowProdGroup.Length > 0)
                //{
                //    int intCat3 = int.Parse(foundRowProdGroup[0]["GRP_ID"].ToString());
                //    hdnCat3.Value = intCat3.ToString();
                //    if (intCat3 != 0) { ddlCat3.SelectedValue = intCat3.ToString(); }
                //}

                //foundRowProdGroup = dsProdGroup.Tables[0].Select("GRPING_ID = 4");
                //if (foundRowProdGroup.Length > 0)
                //{
                //    int intCat4 = int.Parse(foundRowProdGroup[0]["GRP_ID"].ToString());
                //    hdnCat4.Value = intCat4.ToString();
                //    if (intCat4 != 0) { ddlCat4.SelectedValue = intCat4.ToString(); }
                //}

                //foundRowProdGroup = dsProdGroup.Tables[0].Select("GRPING_ID = 5");
                //if (foundRowProdGroup.Length > 0)
                //{
                //    int intCat5 = int.Parse(foundRowProdGroup[0]["GRP_ID"].ToString());
                //    hdnCat5.Value = intCat5.ToString();
                //    if (intCat5 != 0) { ddlCat5.SelectedValue = intCat5.ToString(); }
                //}

                //foundRowProdGroup = dsProdGroup.Tables[0].Select("GRPING_ID = 6");
                //if (foundRowProdGroup.Length > 0)
                //{
                //    int intCat6 = int.Parse(foundRowProdGroup[0]["GRP_ID"].ToString());
                //    hdnCat6.Value = intCat6.ToString();
                //    if (intCat6 != 0) { ddlCat6.SelectedValue = intCat6.ToString(); }
                //}

                //foundRowProdGroup = dsProdGroup.Tables[0].Select("GRPING_ID = 7");
                //if (foundRowProdGroup.Length > 0)
                //{
                //    int intCat7 = int.Parse(foundRowProdGroup[0]["GRP_ID"].ToString());
                //    hdnCat7.Value = intCat7.ToString();
                //    if (intCat7 != 0) { ddlCat7.SelectedValue = intCat7.ToString(); }
                //}

                //foundRowProdGroup = dsProdGroup.Tables[0].Select("GRPING_ID = 8");
                //if (foundRowProdGroup.Length > 0)
                //{
                //    int intCat8 = int.Parse(foundRowProdGroup[0]["GRP_ID"].ToString());
                //    hdnCat8.Value = intCat8.ToString();
                //    if (intCat8 != 0) { ddlCat8.SelectedValue = intCat8.ToString(); }
                //}

                //foundRowProdGroup = dsProdGroup.Tables[0].Select("GRPING_ID = 9");
                //if (foundRowProdGroup.Length > 0)
                //{
                //    int intCat9 = int.Parse(foundRowProdGroup[0]["GRP_ID"].ToString());
                //    hdnCat9.Value = intCat9.ToString();
                //    if (intCat9 != 0) { ddlCat9.SelectedValue = intCat9.ToString(); }
                //}

                //foundRowProdGroup = dsProdGroup.Tables[0].Select("GRPING_ID = 10");
                //if (foundRowProdGroup.Length > 0)
                //{
                //    int intCat10 = int.Parse(foundRowProdGroup[0]["GRP_ID"].ToString());
                //    hdnCat10.Value = intCat10.ToString();
                //    if (intCat10 != 0) { ddlCat10.SelectedValue = intCat10.ToString(); }
                //}
                // end of group
            }
            else
            {
                dsProdGroup = prod.getProdGroupListById2(prodId);
                DataRow[] foundRow = dsProdGroup.Tables[0].Select("GRPING_ID = " + clsAdmin.CONSTLISTGROUPINGCAT);

                string strOriCat = "";
                string strOriCatName = "";

                foreach (DataRow row in foundRow)
                {
                    string strOriCatItem = "";
                    string strOriCatNameItem = "";

                    strOriCatItem += Convert.ToInt16(row["GRP_PARENT"]) > 0 ? (!string.IsNullOrEmpty(strOriCatItem) ? clsAdmin.CONSTCOMMASEPARATOR.ToString() : "") + row["GRP_PARENT"].ToString() : "";
                    strOriCatItem += Convert.ToInt16(row["GRP_ID"]) > 0 ? (!string.IsNullOrEmpty(strOriCatItem) ? clsAdmin.CONSTCOMMASEPARATOR.ToString() : "") + row["GRP_ID"].ToString() : "";

                    strOriCatNameItem += !string.IsNullOrEmpty(row["GRP_PARENTNAME"].ToString()) ? (!string.IsNullOrEmpty(strOriCatNameItem) ? clsAdmin.CONSTCOMMASEPARATOR.ToString() : "") + row["GRP_PARENTNAME"].ToString() : "";
                    strOriCatNameItem += !string.IsNullOrEmpty(row["GRP_NAME"].ToString()) ? (!string.IsNullOrEmpty(strOriCatNameItem) ? clsAdmin.CONSTCOMMASEPARATOR.ToString() : "") + row["GRP_NAME"].ToString() : "";

                    strOriCat += !string.IsNullOrEmpty(strOriCatItem) ? strOriCatItem + clsAdmin.CONSTDEFAULTSEPERATOR : "";
                    strOriCatName += !string.IsNullOrEmpty(strOriCatNameItem) ? strOriCatNameItem + clsAdmin.CONSTDEFAULTSEPERATOR : "";
                }

                Session["ORICAT"] = !string.IsNullOrEmpty(strOriCat) ? strOriCat : "";
                Session["ORICATNAME"] = !string.IsNullOrEmpty(strOriCatName) ? strOriCatName : "";
            }
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            Response.Redirect(currentPageName);
        }
    }

    protected void checkLanguage()
    {
        if (clsMis.isLangExist(clsAdmin.CONSTLANGJAPANESE))
        {
            trNameJp.Visible = true;
            trDNameJp.Visible = true;
        }
        if (clsMis.isLangExist(clsAdmin.CONSTLANGMALAY))
        {
            trNameMs.Visible = true;
            trDNameMs.Visible = true;
        }
        if (clsMis.isLangExist(clsAdmin.CONSTLANGCHINESE))
        {
            trNameZh.Visible = true;
            trDNameZh.Visible = true;
        }
    }

    protected void populateData()
    {
        pnlCat.Controls.Clear();
        int intCount = 0;

        try
        {
            string strOriCat = "";
            if (Session["ORICAT"] != null && Session["ORICAT"] != "")
            {
                if (!Session["ORICAT"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["ORICAT"] = clsAdmin.CONSTDEFAULTSEPERATOR + Session["ORICAT"].ToString(); }

                strOriCat = Session["ORICAT"].ToString();
            }

            string strOriCatName = "";
            if (Session["ORICATNAME"] != null && Session["ORICATNAME"] != "")
            {
                if (!Session["ORICATNAME"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["ORICATNAME"] = clsAdmin.CONSTDEFAULTSEPERATOR + Session["ORICATNAME"].ToString(); }

                strOriCatName = Session["ORICATNAME"].ToString();
            }

            string strCat = "";
            if (Session["CAT"] != null && Session["CAT"] != "")
            {
                if (!Session["CAT"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["CAT"] = clsAdmin.CONSTDEFAULTSEPERATOR + Session["CAT"].ToString(); }

                strCat = Session["CAT"].ToString();
            }

            string strCatName = "";
            if (Session["CATNAME"] != null && Session["CATNAME"] != "")
            {
                if (!Session["CATNAME"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["CATNAME"] = clsAdmin.CONSTDEFAULTSEPERATOR + Session["CATNAME"].ToString(); }

                strCatName = Session["CATNAME"].ToString();
            }

            string strDelCat = "";
            if (Session["DELCAT"] != null && Session["DELCAT"] != "")
            {
                if (!Session["DELCAT"].ToString().StartsWith(clsAdmin.CONSTDEFAULTSEPERATOR.ToString())) { Session["DELCAT"] = clsAdmin.CONSTDEFAULTSEPERATOR + Session["DELCAT"].ToString(); }

                strDelCat = Session["DELCAT"].ToString();
            }

            if ((!string.IsNullOrEmpty(strOriCat) && !string.IsNullOrEmpty(strOriCatName)) || (!string.IsNullOrEmpty(strCat) && !string.IsNullOrEmpty(strCatName)))
            {
                HtmlTable tbl = new HtmlTable();
                tbl.CellPadding = 0;
                tbl.CellSpacing = 0;
                tbl.Attributes.Add("class", "innerTbl");
                pnlCat.Controls.Add(tbl);

                HtmlTableRow row;
                HtmlTableCell cell;
                Literal litItem;
                LinkButton lnkbtn;

                // construct header
                litItem = new Literal();
                litItem.Text = "1<span class=\"spanSuperscript\">st</span> Category";

                row = new HtmlTableRow();
                cell = new HtmlTableCell();
                cell.Controls.Add(litItem);
                cell.Attributes.Add("class", "tdCatHeader");
                row.Controls.Add(cell);


                litItem = new Literal();
                litItem.Text = "2<span class=\"spanSuperscript\">nd</span> Category";

                cell = new HtmlTableCell();
                cell.Controls.Add(litItem);
                cell.Attributes.Add("class", "tdCatHeader");
                row.Controls.Add(cell);

                litItem = new Literal();
                litItem.Text = "Action";

                cell = new HtmlTableCell();
                cell.Controls.Add(litItem);
                cell.Attributes.Add("class", "tdCatHeader");
                row.Controls.Add(cell);

                tbl.Controls.Add(row);


                // populate items
                if (!string.IsNullOrEmpty(strOriCat) && !string.IsNullOrEmpty(strOriCatName))
                {
                    strOriCat = strOriCat.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strOriCat.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    strOriCatName = strOriCatName.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strOriCatName.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                    if (!string.IsNullOrEmpty(strOriCat) && !string.IsNullOrEmpty(strOriCatName))
                    {
                        strOriCat = strOriCat.Substring(0, strOriCat.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                        strOriCatName = strOriCatName.Substring(0, strOriCatName.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                        string[] strOriCatSplit = strOriCat.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                        string[] strOriCatNameSplit = strOriCatName.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                        for (int i = 0; i <= strOriCatSplit.GetUpperBound(0); i++)
                        {
                            if (strDelCat.IndexOf(clsAdmin.CONSTDEFAULTSEPERATOR + strOriCatSplit[i] + clsAdmin.CONSTDEFAULTSEPERATOR) < 0)
                            {
                                intCount += 1;

                                string[] strOriCatSplitSplit = strOriCatSplit[i].Split(clsAdmin.CONSTCOMMASEPARATOR);
                                string[] strOriCatNameSplitSplit = strOriCatNameSplit[i].Split(clsAdmin.CONSTCOMMASEPARATOR);

                                row = new HtmlTableRow();

                                for (int j = 0; j <= strOriCatSplitSplit.GetUpperBound(0); j++)
                                {
                                    litItem = new Literal();
                                    litItem.Text = strOriCatNameSplitSplit[j];

                                    cell = new HtmlTableCell();
                                    cell.Controls.Add(litItem);
                                    cell.Attributes.Add("class", "tdCatItem");
                                    row.Controls.Add(cell);
                                }

                                int intEmptyCell = 2 - strOriCatSplitSplit.Length;

                                for (int k = 0; k < intEmptyCell; k++)
                                {
                                    cell = new HtmlTableCell();
                                    cell.Attributes.Add("class", "tdCatItem");
                                    row.Controls.Add(cell);
                                }

                                lnkbtn = new LinkButton();
                                lnkbtn.ID = intCount.ToString();
                                lnkbtn.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
                                lnkbtn.Command += new CommandEventHandler(oriCat_OnDelete);
                                lnkbtn.CommandArgument = strOriCatSplit[i] + clsAdmin.CONSTDEFAULTSEPERATOR + strOriCatNameSplit[i];
                                lnkbtn.CausesValidation = false;

                                cell = new HtmlTableCell();
                                cell.Controls.Add(lnkbtn);
                                cell.Attributes.Add("class", "tdCatItem");
                                row.Controls.Add(cell);
                                tbl.Controls.Add(row);
                            }
                        }
                    }
                }


                if (!string.IsNullOrEmpty(strCat) && !string.IsNullOrEmpty(strCatName))
                {
                    strCat = strCat.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strCat.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                    strCatName = strCatName.Substring(clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length, strCatName.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                    if (!string.IsNullOrEmpty(strCat) && !string.IsNullOrEmpty(strCatName))
                    {
                        strCat = strCat.Substring(0, strCat.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);
                        strCatName = strCatName.Substring(0, strCatName.Length - clsAdmin.CONSTDEFAULTSEPERATOR.ToString().Length);

                        string[] strCatSplit = strCat.Split(clsAdmin.CONSTDEFAULTSEPERATOR);
                        string[] strCatNameSplit = strCatName.Split(clsAdmin.CONSTDEFAULTSEPERATOR);

                        for (int i = 0; i <= strCatSplit.GetUpperBound(0); i++)
                        {
                            intCount += 1;

                            string[] strCatSplitSplit = strCatSplit[i].Split(clsAdmin.CONSTCOMMASEPARATOR);
                            string[] strCatNameSplitSplit = strCatNameSplit[i].Split(clsAdmin.CONSTCOMMASEPARATOR);

                            row = new HtmlTableRow();

                            for (int j = 0; j <= strCatSplitSplit.GetUpperBound(0); j++)
                            {
                                litItem = new Literal();
                                litItem.Text = strCatNameSplitSplit[j];

                                cell = new HtmlTableCell();
                                cell.Controls.Add(litItem);
                                cell.Attributes.Add("class", "tdCatItem");
                                row.Controls.Add(cell);
                            }

                            int intEmptyCell = 2 - strCatSplitSplit.Length;

                            for (int k = 0; k < intEmptyCell; k++)
                            {
                                cell = new HtmlTableCell();
                                cell.Attributes.Add("class", "tdCatItem");
                                row.Controls.Add(cell);
                            }

                            lnkbtn = new LinkButton();
                            lnkbtn.ID = intCount.ToString();
                            lnkbtn.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
                            lnkbtn.Command += new CommandEventHandler(cat_OnDelete);
                            lnkbtn.CommandArgument = strCatSplit[i] + clsAdmin.CONSTDEFAULTSEPERATOR + strCatNameSplit[i];
                            lnkbtn.CausesValidation = true;

                            cell = new HtmlTableCell();
                            cell.Controls.Add(lnkbtn);
                            cell.Attributes.Add("class", "tdCatItem");
                            row.Controls.Add(cell);
                            tbl.Controls.Add(row);
                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {

        }
    }

    public void fillGrouping()
    {
        clsMis mis = new clsMis();
        DataSet dsGrouping = new DataSet();
        dsGrouping = mis.getListByListGrp(clsAdmin.CONSTLISTGROUPINGGROUP, 1);
        DataView dvGrouping = new DataView(dsGrouping.Tables[0]);

        clsGroup grp = new clsGroup();
        DataSet dsCat = new DataSet();
        dsCat = grp.getGrpList(1);
        DataView dvCat = new DataView(dsCat.Tables[0]);

        clsProduct prod = new clsProduct();
        DataSet dsProdGroup = new DataSet();
        dsProdGroup = prod.getProdGroupListById(prodId);
        DataView dvProdGroup = new DataView(dsProdGroup.Tables[0]);

        dvGrouping.Sort = "LIST_ORDER ASC";

        tvCategory.Nodes.Clear();

        foreach (DataRowView drGrouping in dvGrouping)
        {
            TreeNode tnGrouping = new TreeNode();
            tnGrouping.Text = drGrouping["LIST_NAME"].ToString();
            tnGrouping.Value = drGrouping["LIST_VALUE"].ToString();
            tnGrouping.PopulateOnDemand = true;
            tnGrouping.ShowCheckBox = false;
            tnGrouping.SelectAction = TreeNodeSelectAction.SelectExpand;
            tnGrouping.Expand();
            tnGrouping.Selected = true;
            tvCategory.Nodes.Add(tnGrouping);
            fillCategory(tnGrouping, tnGrouping.Value, 0, dvCat, dvProdGroup);
        }
    }

    public void fillCategory(TreeNode grouping, string grpingId, int intParent, DataView dvCat, DataView dvProdGroup)
    {
        dvCat.RowFilter = "GRP_PARENT = " + intParent + " AND GRPING_ID = " + grpingId;
        dvCat.Sort = "GRP_SHOWTOP DESC, GRP_ORDER ASC";

        grouping.ChildNodes.Clear();

        if (dvCat.Count == 0)
        {
            grouping.PopulateOnDemand = true;
        }
        else
        {
            foreach (DataRowView dr in dvCat)
            {
                TreeNode tnCat = new TreeNode();
                tnCat.Text = dr["GRP_NAME"].ToString().Trim();
                tnCat.Value = dr["GRP_ID"].ToString().Trim();

                if (prodId > 0)
                {
                    dvProdGroup.RowFilter = "PROD_ID = " + prodId;
                    dvProdGroup.RowFilter += " AND GRP_ID = " + tnCat.Value;

                    if (dvProdGroup.Count > 0)
                    {
                        tnCat.Checked = true;
                    }
                }

                tnCat.SelectAction = TreeNodeSelectAction.SelectExpand;
                tnCat.CollapseAll();
                grouping.ChildNodes.Add(tnCat);

                fillCategory(tnCat, grpingId, Convert.ToInt16(tnCat.Value), dvCat, dvProdGroup);
            }
        }
    }

    protected decimal getMatrixConvRate(string strMatrixUnit, string strMatrixGroup)
    {
        clsMatrixConversion matrix = new clsMatrixConversion();
        DataSet dsMatrix = new DataSet();
        dsMatrix = matrix.getMatrixConversionList(1);

        decimal decMatrixConvRate = Convert.ToDecimal("0.000");
        string strMatrixDefaultUnit = matrix.getDefaultMatrixByGroup(strMatrixGroup);
        string strMatrixCalUnit = !string.IsNullOrEmpty(strMatrixUnit) ? strMatrixUnit : strMatrixDefaultUnit;

        DataRow[] foundDimRow = dsMatrix.Tables[0].Select("MAT_GROUP = '" + strMatrixGroup + "' AND MAT_UNIT = '" + strMatrixCalUnit + "'");
        if (foundDimRow.Length > 0)
        {
            decMatrixConvRate = Convert.ToDecimal(foundDimRow[0]["MAT_CONVRATE"].ToString());
        }

        return decMatrixConvRate;
    }

    #endregion
}