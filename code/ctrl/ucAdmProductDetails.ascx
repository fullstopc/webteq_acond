﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmProductDetails.ascx.cs" Inherits="ctrl_ucAdmProductDetails" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmProductPrice.ascx" TagName="AdmProductPrice" TagPrefix="uc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<link  href="<%= System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>cropper.min.css" rel="stylesheet">
<script src="<%= System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>cropper.min.js"></script>
<script type="text/javascript">
    function validateProdCode_client(source, args) {
        var txtProdCode = document.getElementById("<% =txtProdCode.ClientID %>");
        var prodCodeRE = /<% =clsAdmin.CONSTADMPRODCODERE %>/;

        if (txtProdCode.value.match(prodCodeRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "<br />Please enter only alphanumeric.";
            source.innerHTML = "<br />Please enter only alphanumeric.";
        }
    }

    function validateWeight_client(source, args) {
        var txtWeight = document.getElementById("<%= txtWeight.ClientID %>");
        var weightRE = /<%= clsAdmin.CONSTNUMERICRE %>/;

        if (txtWeight.value.match(weightRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "<br />Please enter only numeric.";
            source.innerHTML = "<br />Please enter only numeric.";
        }
    }
    function validateProdImage_client(source,args){
        var filename = $("#fileProdImage").val();
        var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'ico'];
        if ($.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == -1 && filename.length > 0) {
            args.IsValid = false;
            source.innerHTML = "Only formats are allowed : "+fileExtension.join(', ');
        }
    }
    function validateDim_client(source, args) {
        var dimRE = /<%= clsAdmin.CONSTNUMERICRE %>/;

        if (args.Value.match(dimRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "<br />Please enter only numeric.";
            source.innerHTML = "<br />Please enter only numeric.";
        }
    }

    function validatePageTitleFriendlyUrl_client(source, args) {
        var txtPageTitleFriendlyUrl = document.getElementById("<%= txtPageTitleFriendlyUrl.ClientID %>");
        var pageTitleFriendlyUrlRE = /<%= clsAdmin.CONSTADMPAGETITLEFRIENDLYURL %>/;
        if (txtPageTitleFriendlyUrl.value.match(pageTitleFriendlyUrlRE)) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
            source.errormessage = "Please enter only valid format.";
            source.innerHTML = "Please enter only valid format.";
        }
    }

    function getVolumetricWeight() {
        var txtLength = document.getElementById("<%=txtLength.ClientID%>").value;
        var txtWidth = document.getElementById("<%=txtWidth.ClientID%>").value;
        var txtHeight = document.getElementById("<%=txtHeight.ClientID%>").value;
        var decDimensionConvRate = <%=decDimensionConvRate%>;
        var valVolWeight = 0;

        if (txtLength > 0 && txtWidth > 0 && txtHeight > 0)
        { 
             txtLength= txtLength/decDimensionConvRate;
             txtWidth= txtWidth/decDimensionConvRate;
             txtHeight = txtHeight/decDimensionConvRate;

             valVolWeight = (txtLength*txtWidth*txtHeight)/5000;

             document.getElementById("<%=lblVolumetricWeight.ClientID%>").innerText = valVolWeight;
             $('#trVolumetricWeight').show();
        }else{ $('#trVolumetricWeight').hide();}
    }
    
    $(document).ready(function(){
        getVolumetricWeight();
        cropInit(<%= aspectRatioX %>,<%= aspectRatioY %>);

        // file upload
        $(".file-group input[type='file']").change(function(){
            var $parent = $(this).parent();
            var $file = $(this);
            var $label = $parent.find(".filename");
            
            var filename = $file.val();
            if(filename.length <= 0){
                filename = "Choose a file";
            }

            $label.html(filename);
        });

        // Hide collapse expand
        $(".tvCategory > div table").each(function(){
            var $this = $(this);
            if(!$this.next().is("div")){
                $this.find("td:nth-last-child(2) > a").css({"visibility":"hidden"});
            }
        });
    });
</script>
<asp:Panel ID="pnlFormSect1" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table border="0" id="tblAddMember1" cellpadding="0" cellspacing="0" class="formTbl tblData">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Product Details</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel">Product Code<span class="attention_unique">*</span>:</td>
                    <td class="tdMax"><asp:TextBox ID="txtProdCode" runat="server" class="text_fullwidth uniq" MaxLength="50"></asp:TextBox>
                        <span class=""><asp:RequiredFieldValidator ID="rfvProdCode" runat="server" ErrorMessage="<br />Please enter Product Code." CssClass="errmsg" ControlToValidate="txtProdCode" Display="Dynamic" ValidationGroup="grpProd"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cvProdCode" runat="server" ControlToValidate="txtProdCode" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateProdCode_client" OnServerValidate="validateProdCode_server" OnPreRender="cvProdCode_PreRender" ValidationGroup="grpProd"></asp:CustomValidator></span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel">Product Name<span class="attention_compulsory">*</span>:</td>
                    <td><asp:TextBox ID="txtProdName" runat="server" class="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                        <span class=""><asp:RequiredFieldValidator ID="rfvProdName" runat="server" ErrorMessage="<br />Please enter Product Name." ControlToValidate="txtProdName" Display="Dynamic" CssClass="errmsg" ValidationGroup="grpProd"></asp:RequiredFieldValidator></span>
                    </td>
                </tr>
                <tr id="trNameJp" runat="server" visible="false">
                    <td class="tdLabel">Product Name (Japanese)<span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtProdNameJp" runat="server" class="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                        <span class=""><asp:RequiredFieldValidator ID="rfvProdNameJp" runat="server" ErrorMessage="<br/>Please enter Product Name (Japanese)" ControlToValidate="txtProdNameJp" Display="Dynamic" CssClass="errmsg" ValidationGroup="grpProd"></asp:RequiredFieldValidator></span>
                    </td>
                </tr>
                <tr id="trNameMs" runat="server" visible="false">
                    <td class="tdLabel">Product Name (Malay)<span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtProdNameMs" runat="server" class="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                        <span class=""><asp:RequiredFieldValidator ID="rfvProdNameMs" runat="server" ErrorMessage="<br/>Please enter Product Name (Malay)" ControlToValidate="txtProdNameMs" Display="Dynamic" CssClass="errmsg" ValidationGroup="grpProd"></asp:RequiredFieldValidator></span>
                    </td>
                </tr>
                <tr id="trNameZh" runat="server" visible="false">
                    <td class="tdLabel">Product Name (Chinese)<span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtProdNameZh" runat="server" class="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                        <span class=""><asp:RequiredFieldValidator ID="rfvProdNameZh" runat="server" ErrorMessage="<br/>Please enter Product Name (Chinese)" ControlToValidate="txtProdNameZh" Display="Dynamic" CssClass="errmsg" ValidationGroup="grpProd"></asp:RequiredFieldValidator></span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel nobr">Product Display Name<span class="attention_compulsory">*</span>:</td>
                    <td><asp:TextBox ID="txtProdDName" runat="server" class="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                        <span class=""><a href="javascript:void(0);" onclick="copyValue('<% =txtProdName.ClientID %>','<% =txtProdDName.ClientID %>');" title="same as Name">same as Name</a></span>
                        <span class=""><asp:RequiredFieldValidator ID="rfvProdDName" runat="server" ErrorMessage="<br />Please enter Product Display Name." ControlToValidate="txtProdDName" CssClass="errmsg" Display="Dynamic" ValidationGroup="grpProd"></asp:RequiredFieldValidator></span>
                    </td>
                </tr>
                <tr id="trDNameJp" runat="server" visible="false">
                    <td class="tdLabel nobr">Product Display Name (Japanese)<span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtProdDNameJp" runat="server" class="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                        <span class=""><a href="javascript:void(0);" onclick="copyValue('<% =txtProdNameJp.ClientID %>','<% =txtProdDNameJp.ClientID %>');" title="same as Name">same as Name</a></span>
                        <span class=""><asp:RequiredFieldValidator ID="rfvProdDNameJp" runat="server" ErrorMessage="<br/>Please enter Product Display Name (Japanese)" ControlToValidate="txtProdDNameJp" CssClass="errmsg" Display="Dynamic" ValidationGroup="grpProd"></asp:RequiredFieldValidator></span>
                    </td>
                </tr>
                <tr id="trDNameMs" runat="server" visible="false">
                    <td class="tdLabel nobr">Product Display Name (Malay)<span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtProdDNameMs" runat="server" class="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                        <span class=""><a href="javascript:void(0);" onclick="copyValue('<% =txtProdNameMs.ClientID %>','<% =txtProdDNameMs.ClientID %>');" title="same as Name">same as Name</a></span>
                        <span class=""><asp:RequiredFieldValidator ID="rfvProdDNameMs" runat="server" ErrorMessage="<br/>Please enter Product Display Name (Malay)" ControlToValidate="txtProdDNameMs" CssClass="errmsg" Display="Dynamic" ValidationGroup="grpProd"></asp:RequiredFieldValidator></span>
                    </td>
                </tr>
                <tr id="trDNameZh" runat="server" visible="false">
                    <td class="tdLabel nobr">Product Display Name (Chinese)<span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtProdDNameZh" runat="server" class="text_fullwidth compulsory" MaxLength="250"></asp:TextBox>
                        <span class=""><a href="javascript:void(0);" onclick="copyValue('<% =txtProdNameZh.ClientID %>','<% =txtProdDNameZh.ClientID %>');" title="same as Name">same as Name</a></span>
                        <span class=""><asp:RequiredFieldValidator ID="rfvProdDNameZh" runat="server" ErrorMessage="<br/>Please enter Product Display Name (Chinese)" ControlToValidate="txtProdDNameZh" CssClass="errmsg" Display="Dynamic" ValidationGroup="grpProd"></asp:RequiredFieldValidator></span>
                    </td>
                </tr>
                <tr id="trFriendlyURL" runat="server">
                    <td class="tdLabel nobr">Page Title (Friendly URL):<span class="attention_unique">*</span></td>
                    <td class="tdMax">
                        <asp:TextBox ID="txtPageTitleFriendlyUrl" runat="server" MaxLength="250" CssClass="text_fullwidth uniq"></asp:TextBox>
                        <span class=""><asp:CustomValidator ID="cvPageTitleFriendlyUrl" runat="server" ControlToValidate="txtPageTitleFriendlyUrl" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validatePageTitleFriendlyUrl_client" OnServerValidate="cvPageTitleFriendlyUrl_server" OnPreRender="cvPageTitleFriendlyUrl_PreRender"></asp:CustomValidator></span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel nobr">Product Image:</td>
                    <td class="tdLabel">
                        <asp:Panel ID="pnlFileUpload" runat="server" CssClass="">
                            <asp:Panel ID="pnlFileUploadAction" runat="server" CssClass="left">
                                <div class="file-group">
                                    <input type="file" name="fileProdImage" class="fileImg" id="fileProdImage" accept="image/*">
                                    <label for="fileProdImage">
                                        <i class="material-icons">file_upload</i>
                                        <span class="filename">Choose a file</span>
                                    </label>
                                </div>
                            </asp:Panel>
                            <i class="material-icons icon-edit">crop</i>
                            <span class="spanTooltip" style="margin-top: 8px;margin-left: 8px;">
                                <span class="icon"></span>
                                <span class="msg">
                                    <asp:Literal ID="litUploadRule" runat="server"></asp:Literal>
                                </span>
                            </span>
                            <div class="clear"></div>
                        </asp:Panel>
                        <table width="100%" id="tblCropImg" style="display:none;">
                            <thead>
                                <tr>
                                    <td colspan="2">
                                        <button type="button" class="btnDarkBlue" id="btnConfirmCrop">Crop</button>
                                        <button type="button" class="btnDarkBlue" id="btnCancelCrop">Cancel</button>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="70%">
                                        <div class="divImgContainer" style="width: 100%;">
                                            <img style="max-width:100%;width:100%;height:auto;"/>
                                        </div>
                                    </td>
                                    <td width="30%">
                                        <div class="divImgPreview" style="overflow:hidden;height:200px;width:200px;border:1px solid #ececec;">
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <tbody>
                                <tr>
                                    <td colspan="2">
                                        <input type="hidden" id="hdnImgX" runat="server" data-id="hdnImgX"/>
                                        <input type="hidden" id="hdnImgY" runat="server" data-id="hdnImgY"/>
                                        <input type="hidden" id="hdnImgWidth" runat="server" data-id="hdnImgWidth"/>
                                        <input type="hidden" id="hdnImgHeight" runat="server" data-id="hdnImgHeight"/>
                                        <input type="hidden" id="hdnImgName" runat="server" data-id="hdnImgName"/>
                                        <input type="hidden" id="hdnImgCropped" runat="server" data-id="hdnImgCropped"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                        <asp:HiddenField ID="hdnProdImage" runat="server" />
                        <asp:HiddenField ID="hdnProdImageRef" runat="server" />
                        <asp:CustomValidator ID="cvProdImage" runat="server" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateProdImage_server" ClientValidationFunction ="validateProdImage_client" OnPreRender="cvProdImage_PreRender" ValidationGroup="grpProd"></asp:CustomValidator>
                        <asp:Panel ID="pnlProdImage" runat="server" Visible="false" CssClass="divSavedImageContainer">
                            <asp:LinkButton ID="lnkbtnDeleteImage" runat="server" Text="Delete" ToolTip="Delete" CausesValidation="false" OnClick="lnkbtnDeleteImage_Click"></asp:LinkButton>
                            <br /><asp:Image ID="imgProdImage" runat="server" />
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" class="formTbl tblData borderBtm">
            <thead>
                <tr>
                    <td colspan="2" class="tdSectionHdr">Product Settings</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TreeView ID="tvCategory" runat="server" ShowCheckBoxes="All" CssClass="tvCategory"></asp:TreeView>
                    </td>
                </tr>
                <tr id="trDGroupParentChildCat" runat="server" visible="false">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td Cssclass="tdLabel"><asp:Literal ID="litCat11" runat="server"></asp:Literal></td> 
                                <td></td>
                            </tr>
                            <tr>
                                <td Cssclass="tdLabel">1<span class="spanSuperscript">st</span> Category:</td>
                                 <td><asp:DropDownList ID="ddlCat" runat="server" CssClass="ddl"></asp:DropDownList>
                                    <asp:HiddenField ID="hdnCat" runat="server" />
                                    <asp:CascadingDropDown ID="cddlCat" runat="server" Category="cat" TargetControlID="ddlCat" LoadingText="Loading categories..." ServiceMethod="BindCatDropdown" ServicePath="~/wsvc/category.asmx" />
                                </td>
                           </tr>
                           <tr>
                                <td Cssclass="tdLabel">2<span class="spanSuperscript">nd</span> Category:</td>
                                <td><asp:DropDownList ID="ddlSubCat" runat="server" CssClass="ddl"></asp:DropDownList>
                                    <asp:CascadingDropDown ID="cddlSubcat" runat="server" Category="subcat" TargetControlID="ddlSubCat" ParentControlID="ddlCat" LoadingText="Loading categories..." ServiceMethod="BindSubcatDropdown" ServicePath="~/wsvc/category.asmx" />
                                    </td>
                           </tr>
                           <tr>
                                <td>
                                </td>
                                <td>
                                   <asp:LinkButton ID="lnkbtnAdd" runat="server" Text="Add" ToolTip="Add" OnClick="lnkbtnAdd_Click" CssClass="btn" CausesValidation="false"></asp:LinkButton>
                                </td>
                           </tr>
                           <tr>
                                <td></td>
                                <td>
                                     <div class="divLoadingContainer">
                                     <asp:UpdatePanel ID="upnlCategory" runat="server">      
                                        <ContentTemplate>                            
                                            <asp:UpdateProgress ID="uprgCategory" runat="server" DynamicLayout="true">
                                                <ProgressTemplate>
                                                    <uc:AdmLoading ID="ucAdmLoading" runat="server" />
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>  
                                            <asp:Panel ID="pnlCat" runat="server"></asp:Panel>               
                                        </ContentTemplate>
                                         <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="lnkbtnAdd" />
                                        </Triggers>
                                        </asp:UpdatePanel>
                                     </div>
                                </td>
                           </tr>    
                        </table>
                    </td>     
                </tr> 
                
                <tr id="trCat" runat="server" visible="false">
                    <td colspan="2">
                        <table cellpadding="0" cellspacing="0" border="0" class="formTbl">
                            <tr>
                                <td class="tdLabel"><asp:Literal ID="litCat1" runat="server"></asp:Literal></td>
                                <td><asp:DropDownList ID="ddlCat1" runat="server" CssClass="ddl"></asp:DropDownList>
                                    <asp:HiddenField ID="hdnCat1" runat="server" />
                                </td>
                            </tr> 
                            <tr>
                                <td class="tdLabel"><asp:Literal ID="litCat2" runat="server"></asp:Literal></td>
                                <td><asp:DropDownList ID="ddlCat2" runat="server" CssClass="ddl"></asp:DropDownList>
                                    <asp:HiddenField ID="hdnCat2" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel"><asp:Literal ID="litCat3" runat="server"></asp:Literal></td>
                                <td>
                                    <asp:DropDownList ID="ddlCat3" runat="server" CssClass="ddl"></asp:DropDownList>
                                    <asp:HiddenField ID="hdnCat3" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel"><asp:Literal ID="litCat4" runat="server"></asp:Literal></td>
                                <td>
                                    <asp:DropDownList ID="ddlCat4" runat="server" CssClass="ddl"></asp:DropDownList>
                                    <asp:HiddenField ID="hdnCat4" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel"><asp:Literal ID="litCat5" runat="server"></asp:Literal></td>
                                <td>
                                    <asp:DropDownList ID="ddlCat5" runat="server" CssClass="ddl"></asp:DropDownList>
                                    <asp:HiddenField ID="hdnCat5" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel"><asp:Literal ID="litCat6" runat="server"></asp:Literal></td>
                                <td>
                                    <asp:DropDownList ID="ddlCat6" runat="server" CssClass="ddl"></asp:DropDownList>
                                    <asp:HiddenField ID="hdnCat6" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel"><asp:Literal ID="litCat7" runat="server"></asp:Literal></td>
                                <td>
                                    <asp:DropDownList ID="ddlCat7" runat="server" CssClass="ddl"></asp:DropDownList>
                                    <asp:HiddenField ID="hdnCat7" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel"><asp:Literal ID="litCat8" runat="server"></asp:Literal></td>
                                <td>
                                    <asp:DropDownList ID="ddlCat8" runat="server" CssClass="ddl"></asp:DropDownList>
                                    <asp:HiddenField ID="hdnCat8" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel"><asp:Literal ID="litCat9" runat="server"></asp:Literal></td>
                                <td>
                                    <asp:DropDownList ID="ddlCat9" runat="server" CssClass="ddl"></asp:DropDownList>
                                    <asp:HiddenField ID="hdnCat9" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel"><asp:Literal ID="litCat10" runat="server"></asp:Literal></td>
                                <td>
                                    <asp:DropDownList ID="ddlCat10" runat="server" CssClass="ddl"></asp:DropDownList>
                                    <asp:HiddenField ID="hdnCat10" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td class="tdLabel">UOM:</td>
                    <td><asp:TextBox ID="txtUOM" runat="server" class="text_small" MaxLength="10"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="tdLabel">Weight:</td>
                    <td><asp:TextBox ID="txtWeight" runat="server" class="text_small" MaxLength="10"></asp:TextBox> <span class=""><asp:Literal ID="litWeightUnit" runat="server"></asp:Literal></span>
                        <span class=""><asp:CustomValidator ID="cvWeight" runat="server" ControlToValidate="txtWeight" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateWeight_client"></asp:CustomValidator></span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel">Dimension:</td>
                    <td><asp:TextBox ID="txtLength" runat="server" onblur="getVolumetricWeight()" CssClass="text_small" MaxLength="10"></asp:TextBox> <span class=""><asp:Literal ID="litLengthUnit" runat="server"></asp:Literal> x </span>
                        <asp:TextBox ID="txtWidth" runat="server" onblur="getVolumetricWeight()" CssClass="text_small" MaxLength="10"></asp:TextBox> <span class=""><asp:Literal ID="litWidthUnit" runat="server"></asp:Literal> x</span>
                        <asp:TextBox ID="txtHeight" runat="server" onblur="getVolumetricWeight()" CssClass="text_small" MaxLength="10"></asp:TextBox> <span class=""><asp:Literal ID="litHeightUnit" runat="server"></asp:Literal></span>
                        <span class="">(Length x Width x Height)<asp:CustomValidator ID="cvLength" runat="server" ControlToValidate="txtLength" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateDim_client"></asp:CustomValidator>
                        <asp:CustomValidator ID="cvWidth" runat="server" ControlToValidate="txtWidth" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateDim_client"></asp:CustomValidator>
                        <asp:CustomValidator ID="cvheight" runat="server" ControlToValidate="txtHeight" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateDim_client"></asp:CustomValidator></span>
                        
                    </td>
                </tr>
                <tr id="trVolumetricWeight" >
                    <td class="tdLabel">Volumetric Weight:</td>
                    <td class="tdLabel"><asp:Label ID="lblVolumetricWeight" runat="server"></asp:Label><span class=""><asp:Literal ID="litVolumatricWeightUnit" runat="server"></asp:Literal></span></td>
                </tr>
                <tr>
                    <td class="tdLabel">New Product:</td>
                    <td class="tdCheckbox"><asp:CheckBox ID="chkboxNew" runat="server" /></td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblShowTop" runat="server"></asp:Label></td>
                    <%--<td class="tdLabel">Show on Top:</td>--%>
                    <td class="tdCheckbox"><asp:CheckBox ID="chkboxShowTop" runat="server" /></td>
                </tr>
                <tr id="trSold" runat="server">
                    <td class="tdLabel">Sold:</td>
                    <td class="tdCheckbox"><asp:CheckBox ID="chkboxSold" runat="server" /></td>
                </tr>
                <tr id="trRent" runat="server">
                    <td class="tdLabel">Rent:</td>
                    <td class="tdCheckbox"><asp:CheckBox ID="chkboxRent" runat="server" /></td>
                </tr>
                <tr id="trOutofStock" runat="server">
                    <td class="tdLabel">Out of Stock:</td>
                    <td class="tdCheckbox"><asp:CheckBox ID="chkboxOutOfStock" runat="server" /></td>
                </tr>
                <tr id="trPreOrder" runat="server">
                    <td class="tdLabel">Preorder:</td>
                    <td class="tdCheckbox"><asp:CheckBox ID="chkboxPreorder" runat="server" /></td>
                </tr>
                <tr id="trBestSeller" runat="server">
                    <td class="tdLabel">Best Seller:</td>
                    <td class="tdCheckbox"><asp:CheckBox ID="chkboxBest" runat="server" />
                        <asp:Label ID="lblNotInUse" runat="server" CssClass="errmsg"></asp:Label></td>
                </tr>
                 <tr id="trWithGst" runat="server" visible="false">
                    <td class="tdLabel">Product with GST:</td>
                    <td class="tdCheckbox"><asp:CheckBox ID="chkboxWithGst" runat="server" /></td>
                </tr>
                <tr id="trStockStatus" runat="server" visible="false">
                    <td class="tdLabel">Stock Status:</td>
                    <td><asp:DropDownList ID="ddlStockStatus" runat="server" CssClass="ddl"></asp:DropDownList></td>
                </tr>
                <tr id="trContainLiquid" runat="server" visible="false">
                    <td class="tdLabel">Contain Liquid?:</td>
                    <td class="tdCheckbox"><asp:CheckBox ID="chkboxContainLiquid" runat="server" /></td>
                </tr>
                <tr>
                    <td class="tdLabel">For Sales:</td>
                    <td class="tdCheckbox"><asp:CheckBox ID="chkboxSales" runat="server" /></td>
                </tr>
                <tr id="trAddon" runat="server" visible="false">
                    <td class="tdLabel">For Addon Product:</td>
                    <td class="tdCheckbox"><asp:CheckBox ID="chkboxAddon" runat="server" /></td>
                </tr>
                <tr>
                    <td class="tdLabel">Active:</td>
                    <td class="tdCheckbox"><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="grpProd"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CssClass="btn btnCancel" Visible="false" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>
<uc:AdmProductPrice ID="ucAdmProductPrice" runat="server" pageListingURL="admProduct01.aspx" />