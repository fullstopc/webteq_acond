﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmMenu.ascx.cs" Inherits="ctrl_ucAdmMenu" %>

<asp:Panel ID="pnlAdmMenu2" runat="server" CssClass="divAdmMenu">
    <ul id="nav-one" class="nav">
        <asp:Repeater ID="rptMenu" runat="server" OnItemDataBound="rptMenu_ItemDataBound">
            <ItemTemplate>
                <li runat="server" ID ="liMenu"><asp:HyperLink ID="hypMenu" runat="server" CssClass="menuLink"></asp:HyperLink>
                    <asp:Panel ID="pnlSubMenu" runat="server" Visible="false">
                        <asp:Repeater ID="rptSubMenu" runat="server" OnItemDataBound="rptSubMenu_ItemDataBound">
                            <HeaderTemplate><ul id="nav-two" class="nav"></HeaderTemplate>
                            <ItemTemplate>
                                <li runat="server" ID ="liSub">
                                    <asp:HyperLink ID="hypSub" runat="server" CssClass="submenuLink"></asp:HyperLink>
                                    <asp:Panel ID="pnlSubSubMenu" runat="server" Visible="false">
                                        <asp:Repeater ID="rptSubSubMenu" runat="server" OnItemDataBound="rptSubSubMenu_ItemDataBound">
                                            <HeaderTemplate><ul id="nav-three" class="nav" ></HeaderTemplate>
                                            <ItemTemplate>
                                                <li>
                                                    <asp:HyperLink ID="hypSubSub" runat="server" CssClass="subsubmenuLink"></asp:HyperLink>
                                                </li>
                                            </ItemTemplate>
                                            <FooterTemplate></ul></FooterTemplate>
                                        </asp:Repeater>
                                    </asp:Panel>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</asp:Panel>
    