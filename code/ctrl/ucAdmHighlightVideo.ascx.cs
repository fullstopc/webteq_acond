﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class ctrl_ucAdmHighlightVideo : System.Web.UI.UserControl
{
    #region "Properties"
    protected int _highId;
    protected string _pageListingURL = "";

    protected int intVideoInRow = 6;
    protected int intVideoCount = 0;

    protected string highVideo;
    protected string highThumbnail;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int highId
    {
        get
        {
            if (ViewState["HIGHID"] == null)
            {
                return _highId;
            }
            else
            {
                return Convert.ToInt16(ViewState["HIGHID"]);
            }
        }
        set { ViewState["HIGHID"] = value; }
    }

    public string pageListingURL
    {
        get
        {
            if (ViewState["PAGELISTINGURL"] == null)
            {
                return _pageListingURL;
            }
            else
            {
                return ViewState["PAGELISTINGURL"].ToString();
            }
        }
        set { ViewState["PAGELISTINGURL"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.LoadComplete += new EventHandler(Page_LoadComplete);
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        bindRpt();
    }

    protected void rptHighVideo_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strVideoLink = DataBinder.Eval(e.Item.DataItem, "HIGHVIDEO_VIDEO").ToString();

            ImageButton imgbtnHighVideo = (ImageButton)e.Item.FindControl("imgbtnHighVideo");
            imgbtnHighVideo.ImageUrl = DataBinder.Eval(e.Item.DataItem, "HIGHVIDEO_THUMBNAIL").ToString();
            imgbtnHighVideo.OnClientClick = "javascript:window.open('" + strVideoLink + "'); return false;";
            imgbtnHighVideo.ToolTip = strVideoLink;
            imgbtnHighVideo.AlternateText = strVideoLink;

            string addBackSlash = Session[clsAdmin.CONSTPROJECTUPLOADPATH].ToString().Replace(@"\", @"\\");
            imgbtnHighVideo.Attributes["onerror"] = "this.src='" + ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + addBackSlash + "/" + (new clsConfig()).defaultGroupImg + "&f=1" + "';";
            

            intVideoCount += 1;
            if (intVideoCount % intVideoInRow == 0)
            {
                Panel pnlHighVideo = (Panel)e.Item.FindControl("pnlHighVideo");
                pnlHighVideo.CssClass = "divIndVideoLast";
            }
        }
    }

    protected void rptHighVideo_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdRemove")
        {
            string strArgument = e.CommandArgument.ToString();

            clsHighlight high = new clsHighlight();

            int intRecordAffected = 0;

            intRecordAffected = high.deleteHighlightVideo(int.Parse(strArgument));

            if (intRecordAffected == 1)
            {
                Session["EDITEDHIGHID"] = highId;
                Response.Redirect(currentPageName);
            }
            else
            {
                high.extractHighlightById(highId, 0);
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit highlight (" + high.highTitle + "). Please try again.</div>";

                Response.Redirect(currentPageName);
            }
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Boolean boolEdited = false;
            Boolean boolSuccess = true;
            int intRecordAffected = 0;
            clsHighlight high = new clsHighlight();

            highVideo = txtHighVideo.Text.Trim();
            highThumbnail = txtHighThumbnail.Text.Trim();

            if (!string.IsNullOrEmpty(highVideo) && !string.IsNullOrEmpty(highThumbnail))
            {
                intRecordAffected = high.addHighlightVideo(highId, highVideo, highThumbnail);

                if (intRecordAffected == 1)
                {
                    boolEdited = true;
                }
                else
                {
                    boolSuccess = false;
                }
            }

            if (boolEdited)
            {
                Session["EDITEDHIGHID"] = highId;
                Response.Redirect(Request.Url.ToString());
            }
            else
            {
                if (!boolSuccess)
                {
                    high.extractHighlightById(highId, 0);
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit highlight (" + high.highTitle + "). Please try again.</div>";

                    Response.Redirect(Request.Url.ToString());
                }
                else
                {
                    Session["EDITEDHIGHID"] = highId;
                    Session["NOCHANGE"] = 1;

                    Response.Redirect(Request.Url.ToString());
                }
            }
        }
    }
    #endregion


    #region "Methods"
    public void fill(int intMode)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;

                if (int.TryParse(Request["id"], out intTryParse))
                {
                    highId = intTryParse;
                }
            }

            lnkbtnCancel.OnClientClick = "javascript:document.location.href='" + pageListingURL + "'; return false;";
        }
    }

    protected void bindRpt()
    {
        clsHighlight high = new clsHighlight();
        DataSet ds = new DataSet();

        ds = high.getHighlightVideosById(highId);

        intVideoCount = 0;
        DataView dv = new DataView(ds.Tables[0]);

        PagedDataSource pds = new PagedDataSource();
        pds.DataSource = dv;

        rptHighVideo.DataSource = pds;
        rptHighVideo.DataBind();

        litVideoFound.Text = dv.Count + " uploaded video(s).";
    }
    #endregion
}
