﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCmnHeader.ascx.cs" Inherits="ctrl_ucAdmCMSControlPanel" %>

<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css' />
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

<link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>reset.css" rel="Stylesheet" />
<link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>thickbox.css" rel="Stylesheet" />
<link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>magnific-popup.css" rel="Stylesheet" />
<link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>fancybox/jquery.fancybox.css" rel="Stylesheet" />
<link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>fancybox/jquery.fancybox.custom.css" rel="Stylesheet" />
<link href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>ui-lightness/jquery-ui-1.8.7.custom.css" rel="Stylesheet" type="text/css" />
<link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>animate.css" rel="Stylesheet" />

<link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>public.css" rel="Stylesheet" />
<link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>nav.css" rel="Stylesheet" />
<link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>sidebar.css" rel="Stylesheet" />
<link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>ScrollMagic/ScrollMagic.custom.css" rel="Stylesheet" />
<link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>responsive.css" rel="Stylesheet" />

<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>config.js" type="text/javascript"></script>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jquery.1.7.2.min.js" type="text/javascript"></script>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jquery.cycle.all.min.js" type="text/javascript"></script>

<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>moment.js" type="text/javascript"></script>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>thickbox.js" type="text/javascript"></script>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>thickbox_init.js" type="text/javascript"></script>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jquery.fancybox.js" type="text/javascript"></script>

<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jquery-ui-1.8.12.custom.min.js" type="text/javascript"></script>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>supersized.3.2.6.min.js" type="text/javascript"></script>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>supersized.shutter.min.js" type="text/javascript"></script>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>supersized-shuttle.js" type="text/javascript"></script>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>ScrollMagic/ScrollMagic.min.js" type="text/javascript"></script>
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>ScrollMagic/plugins/debug.addIndicators.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function () {
    $(".divBackToTopContainer").hide();

    $(window).scroll(function () {
        if ($(this).scrollTop() > 35) {
            $(".divBackToTopContainer").fadeIn();
            $("body").addClass("scrolled");

        } else {
            $(".divBackToTopContainer").fadeOut();
            $("body").removeClass("scrolled");

        }
    });
});
function goTop() {
    $('html,body').animate({ scrollTop: 0 }, 1700, 'easeInOutExpo');
    return false;
}
</script>