﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmUploadInventory.ascx.cs" Inherits="ctrl_ucAdmUploadInventory" %>

<asp:Panel ID="pnlFormSect1" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table cellpadding="0" cellspacing="0" class="formTbl tblData">
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Upload Inventory File:</td>
                <td class="tdLabel">
                    <asp:Panel ID="pnlFileUpload" runat="server" CssClass="divFileUpload">
                        <asp:Panel ID="pnlFileUploadAction" runat="server" CssClass="divFileUploadAction">
                            <asp:FileUpload ID="fileInventory" runat="server" ValidationGroup="grpInv" />
                        </asp:Panel>
                        <span class="spanTooltip">
                            <span class="icon"></span>
                            <span class="msg">
                                <asp:Literal ID="litUploadRule" runat="server"></asp:Literal>
                            </span>
                        </span>
                    </asp:Panel>
                    <asp:HiddenField ID="hdnInventory" runat="server" />
                    <asp:HiddenField ID="hdnInventoryRef" runat="server" />
                    <asp:RequiredFieldValidator ID="rfvInventory" runat="server" ErrorMessage="<br />Please select file to upload." CssClass="errmsg" ControlToValidate="fileInventory" Display="Dynamic" ValidationGroup="grpInv"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvInventory" runat="server" ControlToValidate="fileInventory" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateInventory_server" OnPreRender="cvInventory_PreRender" ValidationGroup="grpInv"></asp:CustomValidator>
                    <%--<asp:Panel ID="pnlInventory" runat="server" Visible="false">
                        <br /><asp:LinkButton ID="lnkbtnDeleteImage" runat="server" Text="Delete" ToolTip="Delete" CausesValidation="false" OnClick="lnkbtnDeleteImage_Click"></asp:LinkButton>
                        <br /><asp:Image ID="imgProdImage" runat="server" />
                    </asp:Panel>--%>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Update Method:</td>
                <td class="tdMax">
                    <asp:DropDownList ID="ddlUpdateMethod" runat="server" CssClass="ddl"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvUpdateMethod" runat="server" ErrorMessage="<br />Please select update method." CssClass="errmsg" ControlToValidate="ddlUpdateMethod" Display="Dynamic" ValidationGroup="grpInv"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                        <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                            <asp:LinkButton ID="lnkbtnProcess" runat="server" Text="Process" ToolTip="Process" CssClass="btn btnSave" OnClick="lnkbtnProcess_Click" ValidationGroup="grpInv"></asp:LinkButton>
                        </asp:Panel>
                    </asp:Panel>
                </td>
                <td></td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr">Upload Report</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">File Name:</td>
                <td class="tdLabel"><asp:Literal ID="litFileName" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td class="tdLabel">Upload Result:</td>
                <td class="tdLabel"><asp:Literal ID="litUploadResult" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tfoot>
                
            </tfoot>
        </table>
    </asp:Panel>
    
</asp:Panel>
