﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdmProjectDetails.ascx.cs" Inherits="ctrl_ucAdmProjectDetails" %>

<script type="text/javascript">
<!--

    function hideTextBox() {
        var chk = $('#<%= chkFullPath.ClientID %>');

        if (chk.is(':checked')) {
            $('#<%= txtFullPath.ClientID %>').show();
        }
        else {
            $('#<%= txtFullPath.ClientID %>').hide();
        }
    }

    window.onload = hideTextBox;
    
    function hideOrShowTextBox(elementRef) {
        
        if (elementRef.checked) {
           $('#<%= txtFullPath.ClientID %>').show();         
        }
        else {
            $('#<%= txtFullPath.ClientID %>').hide();
        }
    }

    function validateFullPath_client(source, args) {

        var chk = document.getElementById('<%= chkFullPath.ClientID %>');
        var strFullPath = document.getElementById('<%= txtFullPath.ClientID %>');

        if (chk.checked) {
            if (strFullPath.value.length <= 0) { args.IsValid = false; }
            else { args.IsValid = true; }
        }

        return;

    }
    
    function toggleGstSetting(element) {
        var parent = $("#<%= chklistGst.ClientID %>");
        var $this = $(element).find("input:checkbox");
        console.log($this.attr("checked"));
        if ($this.attr("checked")) {
            parent.find("tr:not(:first-child)").show();
        }
        else {
            parent.find("tr:not(:first-child)").hide();
        }
            
    }
    function toggleChkboxListSetting(element) {
        var parent = $(element).parentsUntil(".chklist").parent();
        var $this = $(element).find("input:checkbox");
        if ($this.attr("checked")) {
            parent.find("tr:not(:first-child)").show();
        }
        else {
            parent.find("tr:not(:first-child)").hide();
        }
    }

    function toggleAuthSetting() {
        if ($("#<%= chkboxPgAuthSetting.ClientID %>").attr("checked")) {
            $("#<%= rdoListLogin.ClientID %>").show();
        }
        else {
            $("#<%= rdoListLogin.ClientID %>").hide();
        }
    }
    
    $(document).ready(function() {
        toggleChkboxListSetting($("#<%= chklistGst.ClientID %>").find("tr:first-child").get(0));
        toggleChkboxListSetting($("#<%= chklistSlider.ClientID %>").find("tr:first-child").get(0));
        toggleAuthSetting();

        $(".datepicker").attr("readonly","").datepicker({
            dateFormat: 'dd M yy',
            changeMonth: true,
            changeYear : true,
        });
        $(".input-group-remove .icon").click(function () {
            $(this).parent().find("input").val("");
        });

        $("#<%= chkboxCouponManager.ClientID %>").change(function () {
            var checked = $(this).is(":checked");

            if (checked) { $("#<%= ddlCouponManagerPackage.ClientID %>").show(); }
            else { $("#<%= ddlCouponManagerPackage.ClientID %>").hide(); }
        }).trigger("change");
    });

    
// -->
</script>

<asp:Panel ID="pnlForm" runat="server">
    <asp:Panel ID="pnlFormDetails" runat="server">
        <table id="tblProject" cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">
            <tr>
                <td colspan="2" class="tdSectionHdr">PROJECT DETAILS</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel">Name<span class="attention_unique">*</span>:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtProjName" runat="server" CssClass="text_fullwidth uniq" MaxLength="250"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="<br />Please enter Name." ControlToValidate="txtProjName" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgProject"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvName" runat="server" ControlToValidate="txtProjName" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgProject" OnServerValidate="validateName_server" OnPreRender="cvName_PreRender"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Driver<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:DropDownList ID="ddlProjDriver" runat="server" class="ddl_fullwidth compulsory"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvDriver" runat="server" ErrorMessage="<br />Please select Driver." ControlToValidate="ddlProjDriver" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgProject"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Server<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtProjServer" runat="server" Text="localhost" CssClass="text_fullwidth compulsory" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvServer" runat="server" ErrorMessage="<br />Please enter Server." ControlToValidate="txtProjServer" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgProject"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">User Id<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtProjUserId" runat="server" CssClass="text_fullwidth compulsory" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvUserId" runat="server" ErrorMessage="<br />Please enter User Id." ControlToValidate="txtProjUserId" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgProject"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Password<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtProjPwd" runat="server" CssClass="text_fullwidth compulsory" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvPwd" runat="server" ErrorMessage="<br />Please enter Password." ControlToValidate="txtProjPwd" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgProject"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Database<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtProjDatabase" runat="server" CssClass="text_fullwidth compulsory" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDatabase" runat="server" ErrorMessage="<br />Please enter Database." ControlToValidate="txtProjDatabase" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgProject"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Option<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtProjOption" runat="server" Text="3" CssClass="text_fullwidth compulsory" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvOption" runat="server" ErrorMessage="<br />Please enter Option." ControlToValidate="txtProjOption" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgProject"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Port<span class="attention_compulsory">*</span>:</td>
                <td class="tdMax">
                    <asp:TextBox ID="txtProjPort" runat="server" Text="3306" CssClass="text_fullwidth compulsory" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvPort" runat="server" ErrorMessage="<br />Please enter Port." ControlToValidate="txtProjPort" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgProject"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData">    
            <tr>
                <td colspan="2" class="tdSectionHdr">PROJECT SETTINGS</td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Website<span class="attention_compulsory">*</span>:</td>
                <td>
                    <asp:TextBox ID="txtWebsite" runat="server" CssClass="text_fullwidth compulsory" MaxLength="1000"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvWebsite" runat="server" ErrorMessage="<br />Please enter Website." ControlToValidate="txtWebsite" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgProject"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Full Path:</td>
                <td class="tdLabel"> 
                    <asp:CheckBox ID="chkFullPath" runat="server" CssClass="chk" onclick="hideOrShowTextBox(this);"  />
                    <asp:TextBox ID="txtFullPath" runat="server" CssClass="text_fullwidth3" MaxLength="250" ></asp:TextBox>
                    <asp:CustomValidator ID="cvFullPath" runat="server"  ErrorMessage="<br />Please enter Full Path." ControlToValidate="txtFullPath"  ClientValidationFunction="validateFullPath_client" ValidateEmptyText="true" ValidationGroup="vgProject" ></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Upload Path<span class="attention_compulsory">*</span>:</td>
                <td>
                    <asp:TextBox ID="txtUploadPath" runat="server" CssClass="text_fullwidth compulsory" MaxLength="1000"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvUploadPath" runat="server" ErrorMessage="<br />Please enter Upload Path." ControlToValidate="txtUploadPath" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgProject"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Type<span class="attention_compulsory">*</span>:</td>
                <td>
                    <asp:DropDownList ID="ddlType" runat="server" CssClass="ddl_fullwidth compulsory"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvType" runat="server" ErrorMessage="<br />Please select Type." ControlToValidate="ddlType" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgProject"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Additional Language:</td>
                <td>
                    <asp:DropDownList ID="ddlLang" runat="server" CssClass="ddl"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:LinkButton ID="lnkbtnAddLang" runat="server" Text="Add" ToolTip="Add" OnClick="lnkbtnAddLang_Click" ValidationGroup="vgLang"></asp:LinkButton>
                    <asp:RequiredFieldValidator ID="rfvLang" runat="server" ErrorMessage="<br />Please select Language." ControlToValidate="ddlLang" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgLang"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Panel ID="pnlLang" runat="server">
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Top Menu Root<span class="attention_compulsory">*</span>:</td>
                <td>
                    <asp:DropDownList ID="ddlRootMenu" runat="server" CssClass="ddl compulsory"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RequiredFieldValidator ID="rfvRootMenu" runat="server" ErrorMessage="<br />Please top menu root." ControlToValidate="ddlRootMenu" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgProject"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Payment Gateway:</td>
                <td class="tdLabel">
                    <asp:CheckBoxList ID="chklistPaymentGateway" runat="server" CssClass="chklist" ></asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Gst Settings:</td>
                <td class="tdLabel">
                    <asp:CheckBoxList ID="chklistGst" runat="server" CssClass="chklist gst" >
                    </asp:CheckBoxList> 
                </td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Product Settings:</td>
                <td class="">
                    <table cellpadding="0" cellspacing="0" style="width:650px;" class="projfeature">
                        <colgroup>
                            <col width="50%" />
                            <col width="50%" />
                        </colgroup>
                        <tr>
                            <td>
                                <label>
                                    <asp:CheckBox ID="chkboxProdDesc" runat="server" />
                                    Product Description
                                </label>
                            </td>
                            <td>
                                <label>
                                    <asp:CheckBox ID="chkboxProdGall" runat="server" />
                                    Product Gallery
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <asp:CheckBox ID="chkboxProdPromo" runat="server"  />
                                    Product Promotion Multiple set
                                </label>
                            </td>
                            <td>
                                <label>
                                    <asp:CheckBox ID="chkboxProdAddon" runat="server" />
                                    Product Addon Feature
                                </label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Facebook Catalog Settings:</td>
                <td class="">
                    <table cellpadding="0" cellspacing="0" style="width:650px;" class="projfeature">
                        <colgroup>
                            <col width="20%" />
                            <col width="20%" />
                            <col width="60%" />
                        </colgroup>
                        <tr>
                            <td class="vcenter"> 
                                <label>
                                    <asp:CheckBox ID="chkboxFbCatalogActive" runat="server" />
                                    Active
                                </label>
                            </td>
                            <td class="vcenter"> 
                                <asp:Label runat="server" ID="lblRandId" CssClass="tag" Visible="true"></asp:Label>
                            </td>
                            <td>
                                <span>Expired Date</span>
                                <div class="input-group-remove">
                                    <asp:TextBox runat="server" ID="txtFbCatalogExpired" CssClass="datepicker"></asp:TextBox>
                                    <i class="icon remove">&#10006;</i>
                                </div>
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Enquiry Field Customize:</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxEnqField" runat="server" Checked="false" /></td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Group Menu:</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxGroupMenu" runat="server" Checked="false" /></td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Group (Parent - Child):</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkGroupParentChild" runat="server" Checked="false" /></td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Group Description:</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxGroupDesc" runat="server" /></td>
            </tr>
            <tr>
                <td class="tdLabel">FAQ:</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxFAQ" runat="server" Checked="false" /></td>
            </tr>
            <tr>
                <td class="tdLabel">Program Manager:</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxProgManager" runat="server" /></td>
            </tr>
            <tr>
                <td class="tdLabel">Event Manager (Video, Comment):</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxEventManager" runat="server" /></td>
            </tr>
            <tr>
                <td class="tdLabel">Coupon Manager:</td>
                <td class="tdMax tdCheckbox">
                    <asp:CheckBox ID="chkboxCouponManager" runat="server" />
                    <asp:DropDownList runat="server" ID="ddlCouponManagerPackage" CssClass="ddl"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Multiple Currency:</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxMultipleCurr" runat="server" /></td>
            </tr>
            <tr>
                <td class="tdLabel">Inventory:</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxInv" runat="server" AutoPostBack="true" OnCheckedChanged="chkboxInv_CheckedChanged"  />
                <asp:DropDownList ID="ddlInv" runat="server" Visible="false"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="tdLabel">Mobile View:</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxMobileView" runat="server"  /></td>
            </tr>
            <tr>
                <td class="tdLabel">Facebook Login:</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxFbLogin" runat="server" /></td>
            </tr>
            <tr>
                <td class="tdLabel">Images Watermark:</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxWatermark" runat="server"  /></td>
            </tr>
            <tr>
                <td class="tdLabel">Friendly URL:</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxFriendlyURL" runat="server"  /></td>
            </tr>
            <tr>
                <td class="tdLabel">Training:</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxTraining" runat="server"  /></td>
            </tr>
            <tr>
                <td class="tdLabel">Order Manager Delivery Message:</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxDeliveryMsg" runat="server"  /></td>
            </tr>
            <tr>
                <td class="tdLabel">Page Authorization Setting:</td>
                <td class="tdMax tdCheckbox">
                    <asp:CheckBox ID="chkboxPgAuthSetting" runat="server" />
                    <asp:RadioButtonList ID="rdoListLogin" runat="server" CssClass="chklist"></asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Stop right click:</td>
                <td class="tdMax tdCheckbox">
                    <asp:CheckBox ID="chkboxRightClick" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Webteq Logo Enable:</td>
                <td class="tdMax tdCheckbox">
                    <asp:CheckBox ID="chkboxWebteqLogo" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="tdLabel">CKEditor Top Menu Enable:</td>
                <td class="tdMax tdCheckbox">
                    <asp:CheckBox ID="chkboxCkeditorTopmenu" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Delivery Datetime:</td>
                <td class="tdMax tdCheckbox">
                    <asp:CheckBox ID="chkboxDeliveryDatetime" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="tdLabel">Active:</td>
                <td class="tdMax tdCheckbox"><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
            </tr>
            <tr>
                <td class="tdLabel">File Size Upload Setting :</td>
                <td class="tdMax tdCheckbox">
                    <table cellpadding="0" cellspacing="0" border="0" class="dataTbl2">    
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData2">    
                                    <tr>
                                        <td class="tdLabel">Images</td>
                                        <td class="tdLabel3">
                                            <asp:TextBox ID="txtImages" runat="server" CssClass="text_medium_2" MaxLength="20"></asp:TextBox>
                                            <asp:CustomValidator ID="cvImagesSize" runat="server" Display="Dynamic" CssClass="errmsgAgentSearch"  ErrorMessage="Invalid" OnServerValidate="validateImagesSize_server" ValidationGroup="vgProject"></asp:CustomValidator>
                                        </td>
                                        <td class="tdLabel"><asp:Label ID="lblImages" runat="server" Text="kb" CssClass="lblSize"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="tdLabel">File</td>
                                        <td class="tdLabel3">
                                            <asp:TextBox ID="txtFile" runat="server" CssClass="text_medium_2" MaxLength="20"></asp:TextBox>
                                            <asp:CustomValidator ID="cvFileSize" runat="server" Display="Dynamic" CssClass="errmsgAgentSearch"  ErrorMessage="Invalid" OnServerValidate="validateFileSize_server" ValidationGroup="vgProject"></asp:CustomValidator>
                                        </td>
                                        <td class="tdLabel"><asp:Label ID="lblFile" runat="server" Text="kb" CssClass="lblSize"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="tdLabel">Ckeditor File</td>
                                        <td class="tdLabel3">
                                            <asp:TextBox ID="txtCkeditorFile" runat="server" CssClass="text_medium_2" MaxLength="20"></asp:TextBox>
                                            <asp:CustomValidator ID="cvCkeditorFileSize" runat="server" Display="Dynamic" CssClass="errmsgAgentSearch"  ErrorMessage="Invalid" OnServerValidate="validateCkeditorFileSize_server" ValidationGroup="vgProject"></asp:CustomValidator>
                                        </td>
                                        <td class="tdLabel"><asp:Label ID="lblCkeditorFile" runat="server" Text="kb" CssClass="lblSize"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="tdLabel">Ckeditor Image</td>
                                        <td class="tdLabel3">
                                            <asp:TextBox ID="txtCkeditorImage" runat="server" CssClass="text_medium_2" MaxLength="20"></asp:TextBox>
                                            <asp:CustomValidator ID="cvCkeditorImagesSize" runat="server" Display="Dynamic" CssClass="errmsgAgentSearch"  ErrorMessage="Invalid" OnServerValidate="validateCkeditorImagesSize_server" ValidationGroup="vgProject"></asp:CustomValidator>
                                        </td>
                                        <td class="tdLabel"><asp:Label ID="lblCkeditorImage" runat="server" Text="kb" CssClass="lblSize"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="tdLabel">Default Setting</td>
                                        <td class="tdLabel3">
                                            <asp:TextBox ID="txtDefault" runat="server" CssClass="text_medium_2" MaxLength="20" Enabled="false"></asp:TextBox>
                                            <asp:CustomValidator ID="cvDefaultSize" runat="server" Display="Dynamic" CssClass="errmsgAgentSearch"  ErrorMessage="Invalid" OnServerValidate="validateDefaultSize_server" ValidationGroup="vgProject"></asp:CustomValidator>
                                        </td>
                                        <td class="tdLabel"><asp:Label ID="lblDefault" runat="server" Text="kb" CssClass="lblSize"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData2">    
                                    <tr>
                                        <td>Event Manager</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdLabel nobr">Event Thumb</td>
                                        <td class="tdLabel3">
                                            <asp:TextBox ID="txtEventThumb" runat="server" CssClass="text_medium_2" MaxLength="20"></asp:TextBox>
                                            <asp:CustomValidator ID="cvEventThumbSize" runat="server" Display="Dynamic" CssClass="errmsgAgentSearch"  ErrorMessage="Invalid" OnServerValidate="validateEventThumbSize_server" ValidationGroup="vgProject"></asp:CustomValidator>
                                        </td>
                                        <td class="tdLabel"><asp:Label ID="lblEventThumb" runat="server" Text="kb" CssClass="lblSize"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="tdLabel nobr">Event Gallery</td>
                                        <td class="tdLabel3">
                                            <asp:TextBox ID="txtEventGallery" runat="server" CssClass="text_medium_2" MaxLength="20"></asp:TextBox>
                                            <asp:CustomValidator ID="cvEventGallSize" runat="server" Display="Dynamic" CssClass="errmsgAgentSearch"  ErrorMessage="Invalid" OnServerValidate="validateventGallSize_server" ValidationGroup="vgProject"></asp:CustomValidator>
                                        </td>
                                        <td class="tdLabel"><asp:Label ID="lblSizeEventGall" runat="server" Text="kb" CssClass="lblSize"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0" class="formTbl tblData2">    
                                    <tr>
                                        <td>Product Manager</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdLabel nobr">Product Thumb</td>
                                        <td class="tdLabel3">
                                            <asp:TextBox ID="txtProductThumb" runat="server" CssClass="text_medium_2" MaxLength="20"></asp:TextBox>
                                            <asp:Label ID="lblSizeProdThumb" runat="server" Text="kb" CssClass="lblSize2"></asp:Label>
                                            <asp:CustomValidator ID="cvProdThumbSize" runat="server" Display="Dynamic" CssClass="errmsgAgentSearch"  ErrorMessage="Invalid" OnServerValidate="validateProdThumbSize_server" ValidationGroup="vgProject"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdLabel">Product Gallery</td>
                                        <td class="tdLabel3">
                                            <asp:TextBox ID="txtProductGallery" runat="server" CssClass="text_medium_2" MaxLength="20"></asp:TextBox>
                                            <asp:Label ID="lblSizeProdGall" runat="server" Text="kb" CssClass="lblSize2"></asp:Label>
                                            <asp:CustomValidator ID="cvProdGallSize" runat="server" Display="Dynamic" CssClass="errmsgAgentSearch"  ErrorMessage="Invalid" OnServerValidate="validateProdGallSize_server" ValidationGroup="vgProject"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="tdLabel nobr">Slider Settings:</td>
                <td class="tdLabel">
                    <asp:CheckBoxList ID="chklistSlider" runat="server" CssClass="chklist gst" >
                    </asp:CheckBoxList> 
                </td>
            </tr>
            <tr>
                <td class="tdLabel">SMS Setting:</td>
                <td class="tdMax tdCheckbox">
                    <asp:CheckBox ID="chkboxSMSSetting" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tfoot>
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                            <asp:Panel ID="pnlActionLeft" runat="server" CssClass="divActionLeft">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="vgProject"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlActionRight" runat="server" CssClass="floated right">
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" class="btn btnCancel" Visible="false" OnClick="lnkbtnDelete_Click"></asp:LinkButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </tfoot>
        </table>
    </asp:Panel>
</asp:Panel>
