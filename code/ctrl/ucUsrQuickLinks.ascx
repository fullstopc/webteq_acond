﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUsrQuickLinks.ascx.cs" Inherits="ctrl_ucUsrQuickLinks" %>

<asp:Panel runat="server" ID="pnlQuickLinksInner" CssClass="contact-inner">
<i class="icon material-icons">email</i>
<span class="contact-info contact-tel"><asp:Literal ID="litQuickContactNo" runat="server"></asp:Literal></span>
<span class="contact-splitter">/</span>
<span class="contact-info contact-email"><asp:Literal ID="litQuickContactNoEmail" runat="server"></asp:Literal></span>
</asp:Panel>

    
        