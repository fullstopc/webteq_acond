﻿<%@ WebHandler Language="C#" Class="FileUploadHandler" %>

using System;
using System.Web;
using System.Configuration;
using System.Web.SessionState;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Linq;
using System.Data;

public class FileUploadHandler : IHttpHandler,IRequiresSessionState {

    clsHighlight high = new clsHighlight();
    clsProduct prod = new clsProduct();
    JavaScriptSerializer json = new JavaScriptSerializer();


    public void ProcessRequest (HttpContext context)
    {
        string filepath = HttpContext.Current.Session[clsAdmin.CONSTPROJECTUPLOADPATH] +
                          ConfigurationManager.AppSettings["uplBase2"] +
                          clsAdmin.CONSTADMTEMPFOLDER.ToString() + "/";

        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            if (context.Request.Form["txtType"].ToString() == "typeProd")
            {
                //Product Gallery
                List<ProdGalleryDetail> listProdGalleryDetail = new List<ProdGalleryDetail>();
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string fname = clsMis.GetUniqueFilename(file.FileName,filepath);
                    file.SaveAs(filepath + fname);

                    ProdGalleryDetail prodGalleryDetail = new ProdGalleryDetail();
                    prodGalleryDetail.title = fname; //context.Request.Form["txtTitle"].ToString();
                    prodGalleryDetail.type = "session";
                    prodGalleryDetail.filename = fname;
                    prodGalleryDetail.filenameEncode = System.Uri.EscapeDataString(prodGalleryDetail.filename);
                    prod.addProdGallery(prodGalleryDetail);
                    prod.addProdGallerySort(prodGalleryDetail);
                    prod.addProdGalleryDefault(prodGalleryDetail);

                    listProdGalleryDetail.Add(prodGalleryDetail);
                    drawWaterMark(file,filepath, fname);
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("{\"success\":1,\"file\":" + json.Serialize(listProdGalleryDetail)+"}");
                }
                //End Product Gallery
            }
            else
            {
                //Event Gallery
                List<EventGalleryDetail> listEventGalleryDetail = new List<EventGalleryDetail>();
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string fname = clsMis.GetUniqueFilename(file.FileName, filepath);
                    file.SaveAs(filepath + fname);

                    EventGalleryDetail eventGalleryDetail = new EventGalleryDetail();
                    eventGalleryDetail.title = context.Request.Form["txtTitle"].ToString();
                    eventGalleryDetail.type = "session";
                    eventGalleryDetail.filename = fname;
                    high.addEventGallery(eventGalleryDetail);
                    high.addEventGallerySort(eventGalleryDetail);

                    listEventGalleryDetail.Add(eventGalleryDetail);
                    drawWaterMark(file, filepath, fname);

                }
                //End Event Gallery 
                context.Response.ContentType = "text/plain";
                context.Response.Write("{\"success\":1,\"file\":" + json.Serialize(listEventGalleryDetail) + "}");
            }
        }

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    private void drawWaterMark(HttpPostedFile file,string filepath, string filename)
    {
        clsMis mis = new clsMis();
        clsConfig config = new clsConfig();

        DataSet ds = config.getItemList(1);
        DataRow[] dwWatermarks = ds.Tables[0].Select("CONF_GROUP = '" + clsConfig.CONSTGROUPWATERMARKSETTINGS + "'");

        string strWatermarkType = "0",
                strWatermarkValue = "",
                strWatermarkOpacity = "0.1";

        if (dwWatermarks.Length > 0)
        {
            foreach (DataRow dwWatermark in dwWatermarks)
            {
                if (dwWatermark["CONF_NAME"].ToString() == clsConfig.CONSTNAMEWATERMARKTYPE)
                {
                    if (!string.IsNullOrEmpty(dwWatermark["CONF_VALUE"].ToString()))
                    {
                        strWatermarkType = dwWatermark["CONF_VALUE"].ToString();
                    }
                }
                else if (dwWatermark["CONF_NAME"].ToString() == clsConfig.CONSTNAMEWATERMARKVALUE)
                {
                    strWatermarkValue = dwWatermark["CONF_VALUE"].ToString();
                }
                else if (dwWatermark["CONF_NAME"].ToString() == clsConfig.CONSTNAMEWATERMARKOPACITY)
                {
                    strWatermarkOpacity = dwWatermark["CONF_VALUE"].ToString();
                }
            }

        }

        bool boolWatermarkImageExists = (strWatermarkType == clsAdmin.CONSTWATERMARKTYPEIMAGE && !string.IsNullOrEmpty(strWatermarkValue));
        bool boolWatermarkTextExists = (strWatermarkType == clsAdmin.CONSTWATERMARKTYPETEXT && !string.IsNullOrEmpty(strWatermarkValue));

        if (boolWatermarkImageExists)
        {
            string strWatermarkUploadPath = HttpContext.Current.Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCTRLPNLADMFOLDER + "/";

            System.Drawing.Bitmap bitOriginal = new System.Drawing.Bitmap(file.InputStream, false);
            System.Drawing.Bitmap bitWatermark = new System.Drawing.Bitmap(strWatermarkUploadPath + strWatermarkValue, false);
            float opacity = float.Parse(strWatermarkOpacity);

            System.Drawing.Imaging.ImageFormat format = System.Drawing.Imaging.ImageFormat.Jpeg;

            if (bitOriginal.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Jpeg)) format = System.Drawing.Imaging.ImageFormat.Jpeg;
            else if (bitOriginal.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Bmp)) format = System.Drawing.Imaging.ImageFormat.Bmp;
            else if (bitOriginal.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Png)) format = System.Drawing.Imaging.ImageFormat.Png;
            else if (bitOriginal.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif)) format = System.Drawing.Imaging.ImageFormat.Gif;
            else if (bitOriginal.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Icon)) format = System.Drawing.Imaging.ImageFormat.Icon;

            bitOriginal = mis.drawWatermarkImage(bitOriginal, bitWatermark, opacity);
            bitOriginal.Save(filepath + filename, format);
        }
        else if (boolWatermarkTextExists)
        {
            double dblOpacity = double.Parse(strWatermarkOpacity) * 255;
            System.Drawing.Bitmap bitOriginal = new System.Drawing.Bitmap(file.InputStream, false);

            System.Drawing.Imaging.ImageFormat format = System.Drawing.Imaging.ImageFormat.Jpeg;
            if (bitOriginal.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Jpeg)) format = System.Drawing.Imaging.ImageFormat.Jpeg;
            else if (bitOriginal.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Bmp)) format = System.Drawing.Imaging.ImageFormat.Bmp;
            else if (bitOriginal.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Png)) format = System.Drawing.Imaging.ImageFormat.Png;
            else if (bitOriginal.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif)) format = System.Drawing.Imaging.ImageFormat.Gif;
            else if (bitOriginal.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Icon)) format = System.Drawing.Imaging.ImageFormat.Icon;

            bitOriginal = mis.drawWatermarkText(bitOriginal, strWatermarkValue, (int)dblOpacity);
            bitOriginal.Save(filepath + filename, format);
        }
    }

}