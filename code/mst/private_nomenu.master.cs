﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mst_private_nomenu : System.Web.UI.MasterPage
{
    #region "Properties"
    protected string _message;
    #endregion


    #region "Property Methods"
    public string message
    {
        get { return _message; }
        set { _message = value; }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        ucAdmInfo.message = message;
    }
}
