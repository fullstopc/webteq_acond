﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class mst_public : System.Web.UI.MasterPage
{
    #region "Properties"
    protected int _sectId;
    protected int _pageId;
    protected int _parentId;
    protected int _grpingId;
    protected int _grpId;
    protected string _lang = "";
    #endregion

    #region "Property Methods"
    protected string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public int parentId
    {
        get { return _parentId; }
        set { _parentId = value; }
    }

    public int groupId
    {
        get { return _grpId; }
        set { _grpId = value; }
    }

    public int grpingId
    {
        get { return _grpingId; }
        set { _grpingId = value; }
    }

    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }
    #endregion
    
    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            ucUsrResponsiveSlides.pageId = pageId;
            ucUsrPageTitle.pageId = pageId;

            ucUsrContainerBanner.pageId = pageId;
            ucUsrContainerBanner.fill();

            ucUsrHeader.pageId = pageId;
            ucUsrHeader.fill();

            ucUsrFooter.pageId = pageId;
            ucUsrFooter.fill();

            //register Google Analytic Script
            clsMis mis = new clsMis();
            mis.registerGAScript(Page);
        }
    }
    #endregion
}
