﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class mst_public_sub : System.Web.UI.MasterPage
{
    #region "Properties"
    protected int _sectId;
    protected int _pageId;
    protected string _lang = "";
    #endregion


    #region "Property Methods"
    protected string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public string headerText { get; set; }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["GMAPJS"] = null;

        if (!IsPostBack)
        {
            ucUsrHeader.headerText = headerText;
            ucUsrHeader.pageId = pageId;
            ucUsrHeader.fill();

            ucUsrFooter.pageId = pageId;
            ucUsrFooter.fill();

            ucUsrCMSSubMastheadContainer.pageId = pageId;
            ucUsrCMSSubMastheadContainer.fill();

            ucUsrContainerBanner.pageId = pageId;
            ucUsrContainerBanner.fill();
            
            ucUsrPageTitle.pageId = pageId;

            //register Google Analytic Script
            clsMis mis = new clsMis();
            mis.registerGAScript(Page);
        }
    }
    #endregion
}
