﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class mst_public : System.Web.UI.MasterPage
{
    #region "Properties"
    protected int _sectId;
    protected int _pageId;
    protected string _lang = "en";
    #endregion

    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ucUsrHeader.pageId = pageId;
            ucUsrHeader.fill();

            ucUsrFooter.pageId = pageId;
            ucUsrFooter.fill();

            ucUsrCMSBanner.pageId = pageId;
            ucUsrCMSBanner.fill();

            clsConfig config = new clsConfig();

            //LM-force to mobile view 
            config.extractItemByNameGroup(clsConfig.CONSTNAMEMOBILEVIEW, clsConfig.CONSTGROUPOTHERSETTING, 1);
            int intMobileViewActive = Int32.Parse(config.value);

            if (!Convert.ToBoolean(Session["DESKTOPVIEW"]))
            {
                if (clsMis.isMobileBrowser() && intMobileViewActive == 1)
                {
                    string strURL = clsMis.formatWebsiteViewURL(clsSetting.CONSTWEBSITEVIEWMOBILE, pageId);
                    string strQuery = "?" + Request.Url.GetComponents(UriComponents.Query, UriFormat.SafeUnescaped);
                    Response.Redirect(strURL + strQuery);
                }
            }
            //end LM
            
            //register Google Analytic Script
            clsMis mis = new clsMis();
            mis.registerGAScript(Page);
        }

        ucUsrBackground.pageId = pageId;
    }
    #endregion
}
