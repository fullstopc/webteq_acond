﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class mst_public_sub : System.Web.UI.MasterPage
{
    #region "Properties"
    protected int _sectId;
    protected int _pageId;
    public int _parent;
    protected int _eid;
    public string _parentName;
    public string _parentTemplate;
    protected string _lang = "en";
    #endregion


    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }

     public int parent
    {
        get { return _parent; }
        set { _parent = value; }
    }

     public int eid
     {
         get { return _eid; }
         set { _eid = value; }
     }

    public string parentName
    {
        get { return _parentName; }
        set { _parentName = value; }
    }

    public string parentTemplate
    {
        get { return _parentTemplate; }
        set { _parentTemplate = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["GMAPJS"] = null;

        if (!IsPostBack)
        {
            ucUsrBackground.pageId = pageId;
            clsConfig config = new clsConfig();

            //LM-force to mobile view 
            config.extractItemByNameGroup(clsConfig.CONSTNAMEMOBILEVIEW, clsConfig.CONSTGROUPOTHERSETTING, 1);
            int intMobileViewActive = Int32.Parse(config.value);

            if (!Convert.ToBoolean(Session["DESKTOPVIEW"]))
            {
                if (clsMis.isMobileBrowser() && intMobileViewActive == 1)
                {
                    string strURL = clsMis.formatWebsiteViewURL(clsSetting.CONSTWEBSITEVIEWMOBILE, pageId);
                    string strQuery = "?" + Request.Url.GetComponents(UriComponents.Query, UriFormat.SafeUnescaped);
                    Response.Redirect(strURL + strQuery);
                }
            }
            //end LM

            ucUsrHeader.pageId = pageId;
            ucUsrHeader.fill();

            ucUsrFooter.pageId = pageId;
            ucUsrFooter.fill();

            clsPage pg = new clsPage();
            //int displayName = -1;
            if (pg.extractPageById2(pageId, 1))
            {
                litPageDName.Text = pg.displayName;
            }

            UsrSideMenu.fill();
            UsrSideMenu.pageId = pageId;
            UsrSideMenu.parentId = parent;

            UsrCMSRightBanner.pageId = pageId;
            UsrCMSRightBanner.lang = lang;
            UsrCMSRightBanner.fill();

            UsrCMSMiddleBanner.pageId = pageId;
            UsrCMSMiddleBanner.lang = lang;
            UsrCMSMiddleBanner.fill();

            ucUsrCMSContainerBannerBottom.pageId = pageId;
            ucUsrCMSContainerBannerBottom.lang = lang;
            ucUsrCMSContainerBannerBottom.fill();

            //register Google Analytic Script
            clsMis mis = new clsMis();
            mis.registerGAScript(Page);
        }
    }
    #endregion
}
