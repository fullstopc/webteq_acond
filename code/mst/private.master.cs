﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class mst_private : System.Web.UI.MasterPage
{
    #region "Properties"
    protected int _sectId;
    protected int _parentId;
    protected int _subSectId;
    protected int _subSubSectId;
    protected int _idParent;
    protected int _id;
    protected int _id2;
    protected int _eid;
    protected Boolean _showSideMenu = false;
    protected Boolean _boolShowSibling = true;
    protected string _moduleDesc = "Summaries of the online system.";
    protected int _adminview;
    #endregion


    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int parentId
    {
        get { return _parentId; }
        set { _parentId = value; }
    }

    public int subSectId
    {
        get { return _subSectId; }
        set { _subSectId = value; }
    }

    public int subSubSectId
    {
        get { return _subSubSectId; }
        set { _subSubSectId = value; }
    }

    public int idParent
    {
        get { return _idParent; }
        set { _idParent = value; }
    }

    public int id
    {
        get { return _id; }
        set { _id = value; }
    }

    public int id2
    {
        get { return _id2; }
        set { _id2 = value; }
    }

    public int eid
    {
        get { return _eid; }
        set { _eid = value; }
    }

    public Boolean showSideMenu
    {
        get { return _showSideMenu; }
        set { _showSideMenu = value; }
    }

    public Boolean boolShowSibling
    {
        get { return _boolShowSibling; }
        set { _boolShowSibling = value; }
    }

    public string moduleDesc
    {
        get { return _moduleDesc; }
        set { _moduleDesc = value; }
    }

    public int adminview
    {
        get { return _adminview; }
        set { _adminview = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Init(object sender, EventArgs e)
    {
        // check if session admin is expired
        if (ucAdmCheckAccess != null)
        {
            ucAdmCheckAccess.fill();
        }

        // check if page accessed is valid to access
        if (ucAdmCheckAccessPage != null)
        {
            ucAdmCheckAccessPage.fill();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //To prevent user access page by clicking on BACK button
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
        Response.Cache.SetNoStore();
        Response.AppendHeader("Pragma", "no-cache"); 

        if (!IsPostBack)
        {
            clsConfig config = new clsConfig();
            string image_url = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["imageBase"] + "adm/logo-webteq.gif";
            if(config.extractItemByNameGroup(clsConfig.CONSTNAMELOGOCALOGO, clsConfig.CONSTGROUPLOGOANDICONSETTINGS, 1) && !string.IsNullOrEmpty(config.value))
            {
                image_url = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCTRLPNLADMFOLDER + "/" + config.value + "&f=1";
            }


            imgLogo.Src = image_url;

            if (Session[clsAdmin.CONSTPROJECTUSERVIEWURL] != null && Session[clsAdmin.CONSTPROJECTUSERVIEWURL] != "")
            {
                clsProduct prod = new clsProduct();
                clsPage page = new clsPage();
                

                string strManageUserViewParams = "";
                string strUserViewParams = "";
                string strTemplate = "";
                string strParams = "";
                string strPageType = clsMis.getCurrentPageName().Split('/').Last();
              
                bool boolPageTypeExists = (strPageType == clsAdmin.CONSTADMPRODUCTDETAIL ||strPageType == clsAdmin.CONSTADMPAGEDETAIL ||strPageType == clsAdmin.CONSTADMEVENTDETAIL || strPageType == clsAdmin.CONSTADMPRODUCTLIST);
                bool boolFriendlyUrlFound = config.extractItemByNameGroup(clsConfig.CONSTNAMEFRIENDLYURL, clsConfig.CONSTGROUPOTHERSETTINGS, 1);
                bool boolFriendlyUrl = boolFriendlyUrlFound && (config.value == "1");

                // if page type is equal 'page' , 'product' or 'event'
                if (boolPageTypeExists)
                {
                    string strPageId = "0";
                    switch(strPageType)
                    {
                        case clsAdmin.CONSTADMPRODUCTLIST:
                            {
                                bool boolTemplateFound = config.extractItemByNameGroup(clsConfig.CONSTNAMEPRODUCTPAGE, clsConfig.CONSTGROUPDEFAULTPAGE, 1);
                                strPageId = boolTemplateFound ? !string.IsNullOrEmpty(config.value) ? config.value : "0" : "0";

                                bool boolPageExtract = boolTemplateFound && page.extractPageById(int.Parse(strPageId), 1);

                                if (boolTemplateFound && boolPageExtract)
                                {
                                    strTemplate = page.template;
                                    strParams = "pgid" + clsConfig.CONSTSPLITTER_COLON + strPageId;
                                    strManageUserViewParams = "?" + clsConfig.CONSTMANAGEUSERVIEW_TEMPLATE + "=" + strTemplate;
                                    strManageUserViewParams += "&" + clsConfig.CONSTMANAGEUSERVIEW_PARAMS + "=" + strParams;

                                    if (boolFriendlyUrl)
                                    {
                                        strManageUserViewParams += "&" + "title=" + page.titleFriendlyUrl;
                                    }
                                }
                            }
                            break;
                        case clsAdmin.CONSTADMPRODUCTDETAIL:
                        {
                            string strProdId = (Request["id"] != null) ? Request["id"].ToString() : null; // get the product id
                            int intProdId = 0;
                            if (!string.IsNullOrEmpty(strProdId)) // there is no product id in that page, which mean is new product. 
                            {
                                // product page has multiple parameter such as grping id, grp id, product id, page id. 
                                // So use a Dictionary variable to store all together is easy to readable. 
                                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                                bool boolTemplateFound = config.extractItemByNameGroup(clsConfig.CONSTNAMEPRODUCTPAGE, clsConfig.CONSTGROUPDEFAULTPAGE, 1);
                                strPageId = config.value;

                                bool boolPageIdEmpty = boolTemplateFound && !(string.IsNullOrEmpty(config.value))&&page.extractPageById(int.Parse(config.value), 1); ;

                                if (boolTemplateFound && boolPageIdEmpty)
                                {
                                    strTemplate = page.template;
                                    dictParams.Add("pgid", strPageId); // add page id

                                    bool boolInteger = int.TryParse(strProdId, out intProdId);
                                    bool boolProdFound = boolInteger && prod.getGrpingIdGrpIdByProdId(intProdId);

                                    if (boolProdFound) // if the product has grping id and grp id
                                    {
                                        dictParams.Add("id", strProdId);
                                        dictParams.Add("grpingid", prod.grpingId.ToString()); // add grping id
                                        dictParams.Add("grpid", prod.grpId.ToString()); // add grp id
                                    }
                             
                                    strParams = string.Join(clsConfig.CONSTSPLITTER_SEMICOLON, dictParams.Select(m => m.Key + clsConfig.CONSTSPLITTER_COLON + m.Value).ToArray());

                                    strManageUserViewParams = "?" + clsConfig.CONSTMANAGEUSERVIEW_TEMPLATE + "=" + strTemplate;
                                    strManageUserViewParams += "&" + clsConfig.CONSTMANAGEUSERVIEW_PARAMS + "=" + strParams;
                                    if (boolFriendlyUrl)
                                    {
                                        strManageUserViewParams += "&" + "title=" + page.titleFriendlyUrl;
                                        strManageUserViewParams += "&" + "isproduct=" + "1";

                                        // get group titleurl
                                        clsGroup grp = new clsGroup();
                                        grp.extractGrpById(prod.grpId, 1);
                                        strManageUserViewParams += "&" + "title_group=" + grp.grpPageTitleFriendlyUrl;

                                        // get product titleurl
                                        prod.extractProdById(intProdId, 1);
                                        strManageUserViewParams += "&" + "title_product=" + prod.prodPageTitleFUrl;
  
                                    }

                                }
                            }

                        }
                        break;
                        case clsAdmin.CONSTADMPAGEDETAIL:// if inside page manager
                        {
     
                            strPageId = (!string.IsNullOrEmpty(Request["id"])) ? Request["id"].ToString() : null; // get the page id

                            bool boolPageIDEmpty = string.IsNullOrEmpty(strPageId);
                            bool boolPageExtract = !boolPageIDEmpty && page.extractPageById(int.Parse(strPageId),1);

                            if (boolPageExtract && !boolPageIDEmpty)
                            {
                                strTemplate = page.template;
                                strParams = "pgid" + clsConfig.CONSTSPLITTER_COLON + strPageId;
                                

                                strManageUserViewParams = "?" + clsConfig.CONSTMANAGEUSERVIEW_TEMPLATE + "=" + strTemplate;
                                strManageUserViewParams += "&" + clsConfig.CONSTMANAGEUSERVIEW_PARAMS + "=" + strParams;
                                if (boolFriendlyUrl)
                                {
                                    strManageUserViewParams += "&" + "title=" + page.titleFriendlyUrl;
                                }
                                //string strUrl = "/usr/"+strTemplate+"?pgid="+strPageId;
                                //strManageUserViewParams = "?" + clsConfig.CONSTMANAGEUSERVIEW_URL + "=" + strUrl;

                            }

                        }
                        break;
                        case clsAdmin.CONSTADMEVENTDETAIL:// if inside event manager
                        {
                            bool boolTemplateFound = config.extractItemByNameGroup(clsConfig.CONSTNAMEEVENTPAGE, clsConfig.CONSTGROUPDEFAULTPAGE, 1);
                            bool boolPageIdEmpty = boolTemplateFound && !(string.IsNullOrEmpty(config.value)) &&page.extractPageById(int.Parse(config.value),1);
             
                            if (boolTemplateFound && boolPageIdEmpty) // extract page template
                            {
                                strTemplate = page.template;
                                strPageId = page.id.ToString();
                                strParams = "pgid" + clsConfig.CONSTSPLITTER_COLON + strPageId;
                                strManageUserViewParams = "?" + clsConfig.CONSTMANAGEUSERVIEW_TEMPLATE + "=" + strTemplate;
                                strManageUserViewParams += "&" + clsConfig.CONSTMANAGEUSERVIEW_PARAMS + "=" + strParams;

                                if (boolFriendlyUrl)
                                {
                                    strManageUserViewParams += "&" + "title=" + page.titleFriendlyUrl;
                                }
                            }
                                    
                        }
                        break;
                    }
                }

                if ((!string.IsNullOrEmpty(strTemplate)) && (!string.IsNullOrEmpty(strParams)))
                {
                    strUserViewParams = "/usr/" + strTemplate + "?" + strParams.Replace(";", "&").Replace(":", "=");
                }

                if (!string.IsNullOrEmpty(strManageUserViewParams)) { strManageUserViewParams += ("&friendlyUrl=" + (boolFriendlyUrl ? "1" : "0")); }
                
                ucAdmUserView.userViewUrl = Session[clsAdmin.CONSTPROJECTUSERVIEWURL].ToString() + strUserViewParams;
                ucAdmUserViewManage.userViewUrl = ConfigurationManager.AppSettings["scriptBase"] + clsAdmin.CONSTUSERVIEWMANAGEPAGE + strManageUserViewParams;

            }
            else
            {
                ucAdmUserView.Visible = false;
                ucAdmUserViewManage.Visible = false;
            }
            
            if (Session[clsAdmin.CONSTPROJECTTYPE] != null && Session[clsAdmin.CONSTPROJECTTYPE] != "")
            {
                int intType = int.Parse(Session[clsAdmin.CONSTPROJECTTYPE].ToString());

                ucAdmMenu.type = intType;
                ucAdmSideMenu.type = intType;
            }

            //ucAdmInfo.message = moduleDesc;
            ucAdmMenu.sectId = sectId;
            ucAdmSideMenu.Visible = showSideMenu;

            if (showSideMenu)
            {
                ucAdmSideMenu.sectId = parentId > 0 ? parentId : sectId;
                ucAdmSideMenu.subSectId = subSectId;
                ucAdmSideMenu.subSubSectId = subSubSectId;


                ucAdmSideMenu.idParent = idParent;
                ucAdmSideMenu.itemId = id;
                ucAdmSideMenu.id2 = id2;
                ucAdmSideMenu.eId = eid;
                ucAdmSideMenu.boolShowSibling = boolShowSibling;
                

                Panel pnlForm = (Panel)cphContent.FindControl("pnlForm");
                Panel pnlForm2 = (Panel)cphContent.FindControl("pnlForm2");
                Panel pnlForm3 = (Panel)cphContent.FindControl("pnlForm3");
                Panel pnlForm4 = (Panel)cphContent.FindControl("pnlForm4");
                Panel pnlForm5 = (Panel)cphContent.FindControl("pnlForm5");
                Panel pnlAck = (Panel)cphContent.FindControl("pnlAck");

                if (pnlForm != null){pnlForm.CssClass += " divFormWithLeftMenu";}
                if (pnlForm2 != null) { pnlForm2.CssClass += " divFormWithLeftMenu"; }
                if (pnlForm3 != null) { pnlForm3.CssClass += " divFormWithLeftMenu"; }
                if (pnlForm4 != null) { pnlForm4.CssClass += " divFormWithLeftMenu"; }
                if (pnlForm5 != null) { pnlForm5.CssClass += " divFormWithLeftMenu"; }
                if (pnlAck != null) { pnlAck.CssClass += " divFormWithLeftMenu"; }

            }

            pnlAdmUserProfile.Visible = !(clsMis.isSingleLogin());

            if (Session["ADMROOT"] != null)
            {
                adminview = 0;
                //lnkUserManual.Visible = false;
            }
            else
            {
                adminview = 1;
                //lnkUserManual.Visible = true;
            }
        }
    }
    #endregion
}
