﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class mst_public_sub2 : System.Web.UI.MasterPage
{
    #region "Properties"
    protected int _sectId;
    protected int _pageId;
    public int _parent;
    public string _parentName;
    public string _parentTemplate;
    protected string _lang = "en";
    #endregion


    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }

    public int parent
    {
        get { return _parent; }
        set { _parent = value; }
    }

    public string parentName
    {
        get { return _parentName; }
        set { _parentName = value; }
    }

    public string parentTemplate
    {
        get { return _parentTemplate; }
        set { _parentTemplate = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["GMAPJS"] = null;

        //if (Session["ADMID"] != null && Session["ADMID"] != "")
        //{
        //    pnlMainContainerBottom.CssClass = "divMainContainerBottomOutter divMainContainerBottom2";
        //}

        if (!IsPostBack)
        {
            ucUsrBackground.pageId = pageId;

            ucUsrHeader.pageId = pageId;
            ucUsrHeader.fill();

            ucUsrFooter.pageId = pageId;
            ucUsrFooter.fill();

            clsPage page = new clsPage();
            page.extractPageById(pageId, 1);

            if (!string.IsNullOrEmpty(page.mastheadImage) || !string.IsNullOrEmpty(page.mastheadTagline))
            {
                string strMastheadImage = ConfigurationManager.AppSettings["uplBase"].ToString() + clsSetting.CONSTMASTHEADFOLDER + "/" + page.mastheadImage;
                string strContent = page.mastheadTagline;

                //@ for redirection
                clsConfig conf = new clsConfig();
                if (conf.extractItemByNameGroup(ConfigurationManager.AppSettings["cmsRedirectTag"], ConfigurationManager.AppSettings["cmsTagGroup"], 1))
                {
                    if (strContent.IndexOf(clsAdmin.CONSTTAGREDIRECT) >= 0)
                    {
                        string strURL = strContent.Replace(clsAdmin.CONSTTAGREDIRECT, "");
                        strURL = System.Text.RegularExpressions.Regex.Replace(strURL, "<[^>]*>", string.Empty);
                        strURL = strURL.Replace(System.Environment.NewLine, "");
                        strURL = strURL.Trim();
                        Response.Redirect(strURL);
                    }
                }

                //GmapGenerate(strContent);
            }

            clsConfig config = new clsConfig();
            
            //LM-force to mobile view 
            config.extractItemByNameGroup(clsConfig.CONSTNAMEMOBILEVIEW, clsConfig.CONSTGROUPOTHERSETTING, 1);
            int intMobileViewActive = Int32.Parse(config.value);

            if (!Convert.ToBoolean(Session["DESKTOPVIEW"]))
            {
                if (clsMis.isMobileBrowser() && intMobileViewActive == 1)
                {
                    string strURL = clsMis.formatWebsiteViewURL(clsSetting.CONSTWEBSITEVIEWMOBILE, pageId);
                    string strQuery = "?" + Request.Url.GetComponents(UriComponents.Query, UriFormat.SafeUnescaped);
                    Response.Redirect(strURL + strQuery);
                }
            }
            //end LM

            ucUsrCMSBanner.pageId = pageId;
            ucUsrCMSBanner.lang = lang;
            ucUsrCMSBanner.fill();

            //register Google Analytic Script
            clsMis mis = new clsMis();
            mis.registerGAScript(Page);
        }
    }
    #endregion

    private void GmapGenerate(string strContent)
    {
        //@ for gmap
        try
        {
            clsConfig conf = new clsConfig();
            string strGmapKey = "";
            if (conf.extractItemByNameGroup(ConfigurationManager.AppSettings["gmapKey"], "", 1))
            {
                strGmapKey = conf.value;
            }

            if (conf.extractItemByNameGroup(ConfigurationManager.AppSettings["cmsGmapTag"], ConfigurationManager.AppSettings["cmsTagGroup"], 1))
            {
                string strGmapTag = conf.value;
                string strCmsTagClose = ConfigurationManager.AppSettings["cmsTagClose"];
                string strCmsTagSplit = ConfigurationManager.AppSettings["cmsTagSplit"];
                strGmapTag = strGmapTag.Substring(0, strGmapTag.Length - strCmsTagClose.Length);

                int intCount = 0;

                while ((strContent.IndexOf(strGmapTag + strCmsTagClose) >= 0) || (strContent.IndexOf(strGmapTag + strCmsTagSplit) >= 0))
                {
                    intCount += 1;
                    int intTargetGmapTagStartIndex = 0;
                    int intTargetGmapTagEndIndex = 0;
                    int intTargetGmapAttrStartIndex = 0;
                    if (strContent.IndexOf(strGmapTag + strCmsTagClose) >= 0)
                    {
                        intTargetGmapTagStartIndex = strContent.IndexOf(strGmapTag + strCmsTagClose);
                    }
                    if (strContent.IndexOf(strGmapTag + strCmsTagSplit) >= 0)
                    {
                        intTargetGmapTagStartIndex = strContent.IndexOf(strGmapTag + strCmsTagSplit);
                    }

                    intTargetGmapTagEndIndex = strContent.IndexOf(strCmsTagClose, intTargetGmapTagStartIndex);
                    intTargetGmapAttrStartIndex = strContent.IndexOf(strCmsTagSplit, intTargetGmapTagStartIndex);

                    string strTargetGmapTag = strContent.Substring(intTargetGmapTagStartIndex, intTargetGmapTagEndIndex - intTargetGmapTagStartIndex + 1);
                    string strTargetGmapAttr = strContent.Substring(intTargetGmapAttrStartIndex + 1, intTargetGmapTagEndIndex - intTargetGmapAttrStartIndex - 1);

                    string[] arrTargetGmapAttr = strTargetGmapAttr.Split(char.Parse(strCmsTagSplit));
                    string strGmapMarkers = arrTargetGmapAttr[0];
                    string strGmapZoom = arrTargetGmapAttr[1];
                    string strGmapId = arrTargetGmapAttr[2];

                    string[] strGmapMarkerPoint = strGmapMarkers.Split(clsSetting.CONSTDEFAULTSEPERATOR);

                    string strJS = "<script type=\"text/javascript\">";
                    strJS += "$(function(){ $(\"";
                    strJS += strGmapId + "\").goMap({ markers:[";
                    for (int i = 0; i < strGmapMarkerPoint.Length; i++)
                    {
                        string[] strGmapMarkerCoordinate = strGmapMarkerPoint[i].Split(clsSetting.CONSTCOMMASEPARATOR);
                        strJS += "{latitude:" + strGmapMarkerCoordinate[0] + "," +
                            "longitude:" + strGmapMarkerCoordinate[1] + "," +
                            //"icon:\"" + ConfigurationManager.AppSettings["imageBase"] + "usr/icon-" + (i + 1).ToString() + ".png\"";
                            "icon:\"" + ConfigurationManager.AppSettings["imageBase"] + "usr/icon.png\"";

                        if (i + 1 == strGmapMarkerPoint.Length) { strJS += "}"; }
                        else { strJS += "},"; }
                    }
                    strJS += "],zoom:" + strGmapZoom + ",maptype: 'ROADMAP' ,scaleControl: true, });});";
                    strJS += "</script>";
                    strContent = strContent.Replace(strTargetGmapTag, strJS);
                }
            }
        }
        catch (Exception ex)
        { }

        litTaglineSub.Text = strContent;
    }
}
