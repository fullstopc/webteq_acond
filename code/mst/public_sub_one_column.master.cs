﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class mst_public_sub : System.Web.UI.MasterPage
{
    #region "Properties"
    protected int _sectId;
    protected int _pageId;
    public int _parent;
    protected int _eid;
    public string _parentName;
    public string _parentTemplate;
    protected string _lang = "en";
    #endregion


    #region "Property Methods"
    public int sectId
    {
        get { return _sectId; }
        set { _sectId = value; }
    }

    public int pageId
    {
        get { return _pageId; }
        set { _pageId = value; }
    }

    public string lang
    {
        get { return _lang; }
        set { _lang = value; }
    }

     public int parent
    {
        get { return _parent; }
        set { _parent = value; }
    }

     public int eid
     {
         get { return _eid; }
         set { _eid = value; }
     }

    public string parentName
    {
        get { return _parentName; }
        set { _parentName = value; }
    }

    public string parentTemplate
    {
        get { return _parentTemplate; }
        set { _parentTemplate = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["GMAPJS"] = null;

        if (!IsPostBack)
        {
            ucUsrHeader.pageId = pageId;
            ucUsrHeader.fill();

            ucUsrFooter.pageId = pageId;
            ucUsrFooter.fill();

            clsPage pg = new clsPage();
            //int displayName = -1;
            if (pg.extractPageById2(pageId, 1))
            {
                litPageDName.Text = pg.displayName;
            }

            ucUsrCMSBanner.pageId = pageId;
            ucUsrCMSBanner.fill();

            clsPage page = new clsPage();
            page.extractPageById(pageId, 1);

            //register Google Analytic Script
            clsMis mis = new clsMis();
            mis.registerGAScript(Page);
        }
        ucUsrBackground.pageId = pageId;

    }
    #endregion
}
