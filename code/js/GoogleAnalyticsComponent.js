gapi.analytics.ready(function() {
	gapi.analytics.createComponent('MetricsSelector', {
		execute: function() {
			var e = this.get();
			
			
			this.container = ("string" == typeof e.container) ? document.getElementById(e.container) : e.container,
			e.template && (this.template = e.template), 
			this.container.innerHTML = this.template;
			
			
		
			e["metrics"] = e["metrics"] ||  "ga:bounces,ga:sessions,ga:bounceRate,ga:sessionDuration,ga:searchSessions";
	
			return this.setValues(),this.container.onchange = this.onChange.bind(this),this
		},
		onChange: function() {
			 this.setValues(),this.emit("change", {
				"metrics": this["metrics"],
			})
		},
		setValues: function() {
			var a = this.container.querySelectorAll("input")
			var checkboxesChecked = [];
			// loop over them all
			for (var i=0; i<a.length; i++) {
				if (a[i].checked) {
					checkboxesChecked.push(a[i].value);
				}
			}
			this["metrics"] = checkboxesChecked.join()
		},
		template: '<table class="chklist" border="0">'+
					'<tr>'+
					'<td><input type="checkbox" id="chkbox1" value="ga:bounces" checked><label for="chkbox1">Bounces</label></td>'+
					'</tr><tr>'+
					'<td><input type="checkbox" id="chkbox2" value="ga:sessions" checked><label for="chkbox2">Session</label></td>'+
					'</tr><tr>'+
					'<td><input type="checkbox" id="chkbox3" value="ga:bounceRate" checked><label for="chkbox3">Bounce Rate</label></td>'+
					'</tr><tr>'+
					'<td><input type="checkbox" id="chkbox4" value="ga:sessionDuration" checked><label for="chkbox4">Session Duration</label></td>'+
					'</tr><tr>'+
					'<td><input type="checkbox" id="chkbox5" value="ga:searchSessions" checked><label for="chkbox5">Session with Search</label></td>'+
					'</tr>'+
					'</table>'
	});
	gapi.analytics.createComponent('ChartTypeSelector', {
		execute: function() {
			var e = this.get();
			
			e["type"] = e["type"] || "LINE";
			
			this.container = ("string" == typeof e.container) ? document.getElementById(e.container) : e.container,
			e.template && (this.template = e.template), 
			this.container.innerHTML = this.template;
			
			var a = this.container.querySelectorAll("select")
			return this.chartType = a[0],this.chartType.value = e["type"],this.setValues(),this.container.onchange = this.onChange.bind(this),this
		},
		onChange: function() {
			 this.setValues(),this.emit("change", {
				"type": this["type"],
			})
		},
		setValues: function() {
			this["type"] = this.chartType.value
		},
		template: '<select id="sltChartType"><option value="LINE">LINE</option><option value="BAR">BAR</option><option value="COLUMN">COLUMN</option><option value="TABLE">TABLE</option></select>'
	});
	{
		function t(t) {
			if (n.test(t)) return t;
			var i = a.exec(t);
			if (i) return e(+i[1]);
			if ("today" == t) return e(0);
			if ("yesterday" == t) return e(1);
			throw new Error("Cannot convert date " + t)
		}

		function e(t) {
			var e = new Date;
			e.setDate(e.getDate() - t);
			var a = String(e.getMonth() + 1);
			a = 1 == a.length ? "0" + a : a;
			var n = String(e.getDate());
			return n = 1 == n.length ? "0" + n : n, e.getFullYear() + "-" + a + "-" + n
		}
		
		var a = /(\d+)daysAgo/,
			n = /\d{4}\-\d{2}\-\d{2}/,
			dateformat1='dd-M-yy',
			dateformat2='yy-mm-dd';
			//n = /([12]\d|0[1-9]|3[0-1])-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-(\d{4})/;
		gapi.analytics.createComponent("DateRangeSelector", {
			execute: function() {
				var $this = this;
				var e = this.get();
				e["start-date"] = e["start-date"] || "7daysAgo", 
				e["end-date"] = e["end-date"] || "yesterday", 
				this.container = "string" == typeof e.container ? document.getElementById(e.container) : e.container, e.template && (this.template = e.template), this.container.innerHTML = this.template;

	
				//jQuery(".datepicker").datepicker({ dateFormat: dateformat1,changeMonth: true,changeYear: true });
				document.getElementById('dateFrom').value = moment(t(e["start-date"])).format('DD/MMM/YYYY');
				document.getElementById('dateTo').value = moment(t(e["end-date"])).format('DD/MMM/YYYY');
				
				new Pikaday(
                {
                    field: document.getElementById('dateFrom'),
					format: 'DD/MMM/YYYY',
					position: 'bottom right',
					container:document.getElementById('dateFromContainer'),
					onClose:function(){
						var dateFrom = document.getElementById('dateFrom').value;
						document.getElementById('hdnDateFrom').value = moment(dateFrom).format('YYYY-MM-DD');
					    $this.onChange();
					}
                });
				new Pikaday(
                {
                    field: document.getElementById('dateTo'),
					format: 'DD/MMM/YYYY',
					position: 'bottom right',
					container:document.getElementById('dateToContainer'),
					onClose:function(){
						var dateFrom = document.getElementById('dateTo').value;
						document.getElementById('hdnDateTo').value = moment(dateFrom).format('YYYY-MM-DD');
					    $this.onChange();
					}
                });

            
				var a = this.container.querySelectorAll("input[type='hidden']");
				return this.startDateInput = a[0], this.startDateInput.value = t(e["start-date"]), this.endDateInput = a[1], this.endDateInput.value = t(e["end-date"]), this.setValues(), this.setMinMax(), this.container.onchange = this.onChange.bind(this), this
			},
			onChange: function() {
				this.setValues(), this.setMinMax(), this.emit("change", {
					"start-date": this["start-date"],
					"end-date": this["end-date"]
				})
			},
			setValues: function() {
				this["start-date"] = this.startDateInput.value, 
				this["end-date"] = this.endDateInput.value
			},
			setMinMax: function() {
				this.startDateInput.max = this.endDateInput.value, 
				this.endDateInput.min = this.startDateInput.value
			},
			template: '<div class="DateRangeSelector"><div class="DateRangeSelector-item" id="dateFromContainer"><label>Start Date</label><input class="datepicker" id="dateFrom"><input type="hidden" id="hdnDateFrom" /></div><div class="DateRangeSelector-item" id="dateToContainer"><label>End Date</label><input class="datepicker" id="dateTo"><input type="hidden" id="hdnDateTo" /></div></div>'
		})
	}
//# sourceMappingURL=date-range-selector.js.map
});