﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;

public partial class _Default : System.Web.UI.Page 
{
    #region "Properties"
    public bool isMobile = false;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        var files = Directory.GetFiles(HttpContext.Current.Session[clsAdmin.CONSTPROJECTUPLOADPATH] + "/data/cms/images/", "*.*", SearchOption.AllDirectories);

        List<object> imageFiles = new List<object>();
        foreach (string filename in files)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict["image"] = HttpContext.Current.Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/data/cms/images/" + Path.GetFileName(filename);
            imageFiles.Add(dict);
        }


        Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(imageFiles));
    }
}
