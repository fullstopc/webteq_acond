﻿CKEDITOR.stylesSet.add('set1',
[
	{name: 'Blue Title', element: 'h3', styles: { 'color': 'Blue'} },
	{name: 'Red Title', element: 'h3', styles: { 'color': 'Red'} },

	{ name: 'Header: Level1', element: 'span', styles: { 'font-weight': 'bold', 'font-size': '18px', 'color': '#ffffff'} },
	{ name: 'Header: Level2', element: 'span', styles: { 'font-weight': 'bold', 'font-size': '14px', 'color': '#b5d2e0'} },
    { name: 'Header: Level3', element: 'span', styles: { 'font-weight': 'bold', 'font-size': '12px', 'color': '#e8e8e8'} },
    { name: 'Header: Level4', element: 'span', styles: { 'font-weight': 'normal', 'font-size': '12px', 'color': '#e8e8e8', 'font-style': 'italic'} },

	{ name: 'Marker: Transparent', element: 'span', styles: { 'font-weight': 'bold', 'font-size': '105%', 'color': '#5d5e5e'} },
	{ name: 'Marker: Grey', element: 'span', styles: { 'font-weight': 'normal', 'font-size': '100%', 'color': '#75787a'} },
	{name: 'Marker: Yellow', element: 'span', styles: { 'background-color': 'Yellow'} },
	{name: 'Marker: Green', element: 'span', styles: { 'background-color': 'Lime'} },
    { name: 'Marker: Info', element: 'span', styles: { 'font-weight': 'normal', 'font-size': '100%', 'color': '#5d5e5e', 'font-style': 'italic'} },
    { name: 'Marker: Mandarin', element: 'span', styles: { 'letter-spacing': '1px'} },

	{ name: 'Background: Separator', element: 'div', styles: { 'background': 'url("../img/usr/dotted.gif") repeat-x', 'margin': '20px 0px 20px 0px'} },    
	
	{name: 'Big', element: 'big' },
	{name: 'Small', element: 'small' },
	{name: 'Typewriter', element: 'tt' },

	{name: 'Computer Code', element: 'code' },
	{name: 'Keyboard Phrase', element: 'kbd' },
	{name: 'Sample Text', element: 'samp' },
	{name: 'Variable', element: 'var' },

	{name: 'Deleted Text', element: 'del' },
	{name: 'Inserted Text', element: 'ins' },

	{name: 'Cited Work', element: 'cite' },
	{name: 'Inline Quotation', element: 'q' },

	{name: 'Language: RTL', element: 'span', attributes: { 'dir': 'rtl'} },
	{name: 'Language: LTR', element: 'span', attributes: { 'dir': 'ltr'} },

	{
	    name: 'Image on Left',
	    element: 'img',
	    attributes:
		{
		    'style': 'padding: 5px; margin-right: 5px',
		    'border': '2',
		    'align': 'left'
		}
    },

	{
	    name: 'Image on Right',
	    element: 'img',
	    attributes:
		{
		    'style': 'padding: 5px; margin-left: 5px',
		    'border': '2',
		    'align': 'right'
		}
	}
]);
