﻿
function callDropzone(maxSize, aspectRatioW, aspectRatioH) {
    //$(function () { 
    //    Dropzone.autoDiscover = false;
    //    $("#dZUpload").dropzone({
    //        url: "/centralizedAdminV1/wsvc/FileUploadHandler.ashx",
    //        addRemoveLinks: true,
    //        success: function (file, response) {
    //            var imgName = response;
    //            file.previewElement.classList.add("dz-success");
    //            console.log("Successfully uploaded :" + imgName);
    //        },
    //        error: function (file, response) {
    //            file.previewElement.classList.add("dz-error");
    //        }
    //    });
    //});
    // transform cropper dataURI output to a Blob which Dropzone accepts

    function dataURItoBlob(dataURI) {
        var byteString = atob(dataURI.split(',')[1]);
        var ab = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(ab);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ab], { type: 'image/jpeg' });
    }

    var previewNode = document.querySelector("#template");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);

    Dropzone.autoDiscover = false;
    var filelist = [];
    var resizeImgList = [];

    var myDropzone = new Dropzone("#previews", { // Make the whole body a dropzone
        url: wsBase + "/FileUploadHandler.ashx",
        thumbnailWidth: 100,
        thumbnailHeight: 100,
        parallelUploads: 20,
        previewTemplate: previewTemplate,
        autoQueue: false, // Make sure the files aren't queued until manually added
        previewsContainer: "#previews", // Define the container to display the previews
        clickable: ".fileinput-button", // Define the element that should be used as click trigger to select files.
        acceptedFiles: "image/*",
        maxFilesize: maxSize,
        dictDefaultMessage: "Drag & drop your files in this box to start the uploading",
        queuecomplete: function () {
            $("#actions .start").addClass("disabled");
            $("#actions .cancel").addClass("disabled");
        },
        reset: function () {
            $("#actions .start").addClass("disabled");
            $("#actions .cancel").addClass("disabled");
        }

    });

    myDropzone.on("addedfile", function (file) {
        $("#actions .start").removeClass("disabled");
        $("#actions .cancel").removeClass("disabled");

        // Hookup the start button
        file.previewElement.querySelector(".start").onclick = function () { myDropzone.enqueueFile(file); };
        file.previewElement.querySelector(".crop").onclick = function () {
            //if (file.cropped) {
            //    return false;
            //}

            var template = "";
            template += '<div id="divCropImgContainer"  style="display:none">';
            template += '<div class="divImgContainer" style="width: 500px;height:300px;">';
            template += '<img style="max-width:100%;width:100%;height:auto;"/>';
            template += '</div>';
            template += '<div class="divCropAction" style="margin-top:10px;float:right;">';
            template += '<a class="btn3 crop-upload"><i class="material-icons">crop</i>crop</a>';
            template += '</div>';
            template += '</div>';

            var $cropperModal = $(template);
            // 'Crop and Upload' button in a modal
            var $uploadCrop = $cropperModal.find('.crop-upload');
            var cachedFilename = file.name;
            var $img = $('<img />');
            var reader = new FileReader();
            reader.onloadend = function () {
                // add uploaded and read image to modal
                $cropperModal.find('.divImgContainer').html($img);
                $img.attr('src', reader.result);

                // initialize cropper for uploaded image
                console.log(aspectRatioW);
                console.log(aspectRatioH);
                $img.cropper({
                    aspectRatio: aspectRatioW / aspectRatioH,
                    autoCropArea: 1,
                    zoomable: false,
                    mouseWheelZoom: false,
                    movable: false,
                    cropBoxResizable: true,
                    minContainerWidth: 500,
                    minContainerHeight: 300,
                });

            };
            // read uploaded file (triggers code above)
            reader.readAsDataURL(file);
            var $this = $(this);
            $.fancybox(
                $cropperModal,
                {
                    'autoSize': false,
                    'autoResize': false,
                    'autoDimensions': false,
                    'fitToView' : false,
                    'width': 500,
                    'height': 350,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'topRatio': 0,
                    'afterShow': function () {
                        $(".fancybox-wrap").css({ "margin": "200px 0 0" });
                    }
                }
            );
  
            $uploadCrop.on('click', function () {
                // get cropped image data
                var blob = $img.cropper('getCroppedCanvas').toDataURL();
                // transform it to Blob object
                var newFile = dataURItoBlob(blob);
                // set 'cropped to true' (so that we don't get to that listener again)
                newFile.cropped = true;
                // assign original filename
                newFile.name = cachedFilename;

                // add cropped file to dropzone
                myDropzone.removeFile(file);
                myDropzone.addFile(newFile);
                // upload cropped file with dropzone
                //myDropzone.processQueue();
                $img.cropper("destroy");
                $.fancybox.close();
            });


            return false;
        };

        //if (file.cropped) {
        //    file.previewElement.querySelector(".crop").className = file.previewElement.querySelector(".crop").className + " disabled";
        //}
    });


    myDropzone.on("sending", function (file, xhr, formData) {
        var txtTitle = file.previewElement.querySelector('input[name=txtTitle]').value;
        formData.append("txtTitle", txtTitle);
        
        var eleType = file.previewElement.querySelector('input[name=txtType]');
        var txtType = "typeEvent";
        if (typeof (eleType) != 'undefined' && eleType != null) {
            txtType = file.previewElement.querySelector('input[name=txtType]').value;
        }
        formData.append("txtType", txtType);

        // And disable the start button
        var button = file.previewElement.querySelector(".start");
        button.setAttribute("disabled", "disabled");
        button.className = button.className + " disabled";
    });
    myDropzone.on("success", function (file, response) {
        var txtTitle = file.previewElement.querySelector('input[name=txtTitle]').value;
       
        var eleType = file.previewElement.querySelector('input[name=txtType]');
        if (typeof (eleType) != 'undefined' && eleType != null) {
            var txtType = file.previewElement.querySelector('input[name=txtType]').value;
        }

        var jsonReponse = jQuery.parseJSON(response);
        for (var i = 0; i < jsonReponse.file.length; i++) {
            var fileObj = {};
            fileObj.title = jsonReponse.file[i].title;
            fileObj.filename = jsonReponse.file[i].filename;
            fileObj.desc = fileObj.desc === undefined ? '' : fileObj.desc;
            addImage(fileObj)
        }
    });

    myDropzone.on("complete", function (file) {
        file.previewElement.className = file.previewElement.className + " done";
    });
    // listen to thumbnail event
    myDropzone.on('thumbnail', function (file) {

    });
    //myDropzone.on('dragover', function () {
    //    $(".dropzone").addClass("active");
    //});
    //myDropzone.on('dragenter', function () {

    //});
    //myDropzone.on('dragleave', function () {
    //    $(".dropzone").removeClass("active");
    //});
    //myDropzone.on('drop', function () {
    //    e.preventDefault();
    //});


    // Setup the buttons for all transfers
    // The "add files" button doesn't need to be setup because the config
    // `clickable` has already been specified.
    document.querySelector("#actions .start").onclick = function () {
        myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));

    };
    document.querySelector("#actions .cancel").onclick = function () {
        myDropzone.removeAllFiles(true);
    };

    $("#actions .start").addClass("disabled");
    $("#actions .cancel").addClass("disabled");
}