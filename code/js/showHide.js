﻿function hideshow(target, actionLink) {
    var pnlSearchForm = document.getElementById(target);
    var hypHideShow = document.getElementById(actionLink);

    if (pnlSearchForm.style.display == "none") {
        $(pnlSearchForm).show("slow");
        //pnlSearchForm.style.display = 'block';
        hypHideShow.className = "linkHide";
    }
    else if (pnlSearchForm.style.display == "block") {
        $(pnlSearchForm).hide("slow");
        //pnlSearchForm.style.display = 'none';
        hypHideShow.className = "linkShow";
    }
    else {
        if (pnlSearchForm.className == "divSearchForm") {
            $(pnlSearchForm).show("slow");
            //pnlSearchForm.style.display = 'block';
            hypHideShow.className = "linkHide";
        }
        else {
            $(pnlSearchForm).hide("slow");
            //pnlSearchForm.style.display = 'none';
            hypHideShow.className = "linkShow";
        }
    }
}

function hideshow2(target, actionLink, formList, actionList) {
    var pnlForm = document.getElementById(target);
    var pnlActionLink = document.getElementById(actionLink);
    
    if (pnlForm.style.display == "none") {
        pnlForm.style.display = 'block';
        pnlActionLink.className = "divPokerTabItemSel";
    }
    else {
        pnlForm.style.display = 'block';
        pnlActionLink.className = "divPokerTabItemSel";
    }

    hideAll(formList, actionList, target, actionLink);
}

function hideAll(formList, actionList, exceptForm, exceptAction) {
    var strFormSplit = formList.split(strDefaultSeparator);
    var strActionSplit = actionList.split(strDefaultSeparator);
    //alert(strFormSplit[3] + " " + strFormSplit[4]);
    for (var i = 0; i < strFormSplit.length; i++) {
        if (strFormSplit[i] != exceptForm) {
            //alert(strFormSplit[i] + " " + exceptForm);
            var pnlForm = document.getElementById(strFormSplit[i]);
            //alert(pnlForm);
            pnlForm.style.display = 'none';

            if (strActionSplit[i] != exceptAction) {
                var pnlActionLink = document.getElementById(strActionSplit[i]);
                pnlActionLink.className = "divPokerTabItem";
            }
        }
    }
}

function toggleFilterOption() {
    var $filterOption = $(".divFilterContainer .divFilterMore");
    var $btnDropdown = $(".divFilter .btnDropdown");
    
    $filterOption.toggleClass("active");
    $btnDropdown.toggleClass("active");
    return false;
}

function toggleSideMenu() {
    var $sidemenu = $(".divAdmSideMenu");
    $sidemenu.toggleClass("active");
    return false;
}
$(function () {
    var text = $(".sidemenuSubLinkSel").text();
    if(text) $(".hypListingAction.setting").text(text);
});