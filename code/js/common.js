
function copyValue(source, target) {

    var textbox1 = document.getElementById(source);
    var textbox2 = document.getElementById(target);

    textbox2.value = textbox1.value;
}

function toggleDiv(target, boolShow) {
    if(boolShow) {
        $("#" + target).show();
    }
    else {
        $("#" + target).hide();
    }
}

function addLoadEvent(func){
    var oldonload = window.onload;
    
    if(typeof window.onload != 'function'){
        window.onload = func;
    }
    else {
        window.onload  = function() {
            if(oldonload) {
                oldonload();
            }
            
            func();
        }
    }
}

function slideActionCat(targetContainer, targetSlideContainer) {
    $('#' + targetContainer).hover(
        function() {
            var obj = $(this);
            obj.addClass("active");
            setTimeout(function() {
                if (obj.hasClass('active')) {
                    $('#' + targetSlideContainer).slideDown(500);
                }
            }, 300);
        },
        function() {
            var obj = $(this);
            obj.removeClass("active");
            $('#' + targetSlideContainer).slideUp(500);
        }
    );
}

function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function submitForm(element){
    if (!Page_ClientValidate()) {  return false; }
  
    var $this = $(element);
    
    eval($this.attr("href"));
    $this.html("<span>"+$this.html()+"</span>")
         .attr("class","btn btnLoading")
         .attr("href", "javascript: void(0)");
}

function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

// crop image feature - sh.chong 28 Sep 2015
function cropActive(){
    $(".divFileUpload").hide();
    $(".divSavedImageContainer").hide();
    $("#tblCropImg").show();
    $("#canvas-temp").remove();
    $(".divImgPreview").show();
    $("input[type='hidden'][data-id='hdnImgName']").val($(".fileImg")[0].files[0].name);
    $(".icon-edit").removeClass("active");
}
    
function cropInActive(){
    $(".divFileUpload").show();
    $(".divSavedImageContainer").show();
    $("#tblCropImg").hide();
    $(".icon-edit").addClass("active");
        
}
    
function cropDone(){
    $(".divFileUpload").show();
    $(".divSavedImageContainer").hide();
    $("#tblCropImg").hide();
    $(".divImgPreview").hide();
    $(".icon-edit").addClass("active");
}
function cropReset(){
    $("input[type='hidden'][data-id='hdnImgX']").val("");
    $("input[type='hidden'][data-id='hdnImgY']").val("");
    $("input[type='hidden'][data-id='hdnImgWidth']").val("");
    $("input[type='hidden'][data-id='hdnImgHeight']").val("");
    $("input[type='hidden'][data-id='hdnImgName']").val("");
    $("input[type='hidden'][data-id='hdnImgCropped']").val("");
        
    $("#canvas-temp").remove();
    $("#tblCropImg").hide();
    $(".icon-edit").removeClass("active");
}

function cropInit(w,h){
    var $image = $(".divImgContainer > img");
    var $tblCropImg = $("#tblCropImg");
    var URL = window.URL || window.webkitURL;
    var blobURL;
    var $inputImage = $(".fileImg");

    var settings =  {
            zoomable : false,
            mouseWheelZoom : false, 
            movable : false,
            preview: '.divImgPreview',
            crop: function(e) {
                // Output the result data for cropping image.
                $("input[type='hidden'][data-id='hdnImgX']").val(e.x);
                $("input[type='hidden'][data-id='hdnImgY']").val(e.y);
                $("input[type='hidden'][data-id='hdnImgWidth']").val(e.width);
                $("input[type='hidden'][data-id='hdnImgHeight']").val(e.height);

            }
        };
    
    if((w) && (h)){
        settings.aspectRatio = w / h;
    }

    $image.cropper(settings);
        

    $inputImage.change(function(){
        cropReset();
        if($(this).val() != ""){
            $(".icon-edit").addClass("active");
        }
    });
        
    $(".icon-edit").click(function(){
        var files = $inputImage[0].files;
        var file;
 
        if (files && files.length) {
            file = files[0];

            if (/^image\/\w+$/.test(file.type)) {
                cropActive();
                blobURL = URL.createObjectURL(file);
                $image.one('built.cropper', function () {
                    URL.revokeObjectURL(blobURL); // Revoke when load complete
                }).cropper('reset').cropper('replace', blobURL);
            } else {
                alert("Please choose an image file.");
                $inputImage.val('');
            }
        }
              
    });
        
    $("#btnCancelCrop").click(function(){
        cropInActive();
    });
    $("#btnConfirmCrop").click(function(){
        cropDone();
        var width = $("input[type='hidden'][data-id='hdnImgWidth']").val(),
            height = $("input[type='hidden'][data-id='hdnImgHeight']").val();
            
        var cropImgToCanvas = $image.cropper('getCroppedCanvas');
        var $canvas = $(cropImgToCanvas);
            
        $canvas.attr("id","canvas-temp");
        $canvas.css({
            "max-width":"80%",
            "width":"30%",
            "height":"auto",
            "border":"1px solid black",
            "padding":"10px",
        });

        $canvas.insertAfter('.divFileUpload');
            
        $("input[type='hidden'][data-id='hdnImgCropped']").val(cropImgToCanvas.toDataURL());
           
    });
}
// end of crop image
//-->