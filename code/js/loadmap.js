﻿//<![CDATA[
function load(mapId, lat, lng, zoom) {
    if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById(mapId));
        map.addControl(new GSmallMapControl());

        var pointGW = new GLatLng(lat, lng);
        map.addOverlay(new GMarker(pointGW));
        map.setCenter(pointGW, zoom);
    }
}
//]]>