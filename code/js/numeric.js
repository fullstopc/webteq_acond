﻿function validateNumericRange(strStart, strEnd) {
    try {
        var boolSuccess = false;
        var decStart = parseFloat(strStart);
        var decEnd = parseFloat(strEnd);

        if (decStart <= decEnd) { boolSuccess = true; }
        return boolSuccess
    }
    catch (errC) { }
}

function formatCurrency(num) {
    num = isNaN(num) || num == '' || num == null ? num : num = parseFloat(num).toFixed(2);
    return num;
} 