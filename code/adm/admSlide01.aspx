﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admSlide01.aspx.cs" Inherits="adm_admSlide01" %>
<%@ MasterType VirtualPath="~/mst/private.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title"><asp:Literal ID="litPageTitle" runat="server" meta:resourcekey="litPageTitle"></asp:Literal></div>
        <div class="divListingSplitter"></div>
        <div class="action">
			<asp:Panel ID="pnlListingAction" runat="server" CssClass="divListingAction">
                <asp:HyperLink ID="hypAdd" runat="server" Text="Add Slide Show Image" ToolTip="Add Slide Show Image" CssClass="hypListingAction add" NavigateUrl="admSlide0101.aspx"></asp:HyperLink>
            </asp:Panel>
        </div>
        <div class="divListingSplitter"></div>
        <div class="filter">
            <div class="divFilterContainer">
                <div class="divFilter">
                    <asp:TextBox ID="txtKeyword" runat="server" placeholder="Keyword Search" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
                    <a href="#" class="btnDropdown" onclick="toggleFilterOption();"></a>
                    <asp:Literal ID="litKeywordRule" runat="server" meta:resourcekey="litKeywordRule" visible="false"></asp:Literal>
                </div>
                <div class="divFilterMore">
                    <div></div>
                    <div>
                        <table class="tblSearch">
                            <tr>
                                <td class="">
                                    <div class="divFilterLabel">Group:</div>
                                    <asp:DropDownList ID="ddlGroup" runat="server" CssClass="ddl"></asp:DropDownList>

                                </td>
                                <td class="">
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
            <div class="divFilterAction"><asp:Button ID="lnkbtnGo" runat="server" Text="" ToolTip="Search" CssClass="btn btnSearch" OnClick="lnkbtnGo_Click"/></div>
        </div>
    </div>
    <div class="divListing">
        <div class="divListingHdr">
            <div class="divListingHdrLeft"><asp:Literal ID="litItemFound" runat="server"></asp:Literal></div>
        </div>
        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
            <div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
        </asp:Panel>
        <div class="divListingData">
            <div class="divAblImg">
                <asp:Repeater ID="rptAblImg" runat="server" OnItemDataBound = "rptAblImg_ItemDataBound" OnItemCommand="rptAblImg_ItemCommand">
                    <ItemTemplate>
                        <div class="divAblImgContainer">
                            <div class="divImg">
                                <asp:HyperLink ID="hypAblImg" runat="server">
                                    <asp:Image ID="imgAblImg" runat="server" />
                                </asp:HyperLink>
                            </div>
                            <div class="divDetail">
                                <div class="divName">
                                    <asp:Literal ID="litName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MASTHEAD_NAME") %>'></asp:Literal>
                                </div>
                                <div class="divActive">
                                    Active : <asp:Literal ID="litActive" runat="server" Text='<%# clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "MASTHEAD_ACTIVE"))) %>'></asp:Literal>
                                </div>
                                <div class="divGroupName">
                                    Group Name : <asp:Literal ID="litGroupName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "V_GRPDNAME") %>'></asp:Literal>
                                </div>
                                <div class="divActionSlideShow">
                                    <asp:HyperLink ID="hypEdit" CssClass="lnkbtn lnkbtnEdit-slideshow" runat="server"></asp:HyperLink>
                                    <%--<asp:LinkButton ID="lnkbtnEdit" CssClass="lnkbtn lnkbtnEdit" runat="server" CommandName="cmdEdit" CommandArgument='<% #DataBinder.Eval(Container.DataItem, "MASTHEAD_ID")%>'></asp:LinkButton>--%>
                                    <%--<span id="" class="spanSplitter">|</span>--%>
                                    <asp:HyperLink ID="hypViewImage" CssClass="lnkbtn lnkbtnView-slideshow" runat="server" Target="_blank" Title="view image"></asp:HyperLink>
                                    <asp:LinkButton ID="lnkbtnDelete" CssClass="lnkbtn lnkbtnDelete-slideshow" runat="server" CommandName="cmdDelete" CommandArgument='<% #DataBinder.Eval(Container.DataItem, "MASTHEAD_ID")%>'></asp:LinkButton>
                                    
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
                <%--<asp:DataList ID="rptAblImg"  CssClass="tblAblImg" runat="server" RepeatLayout="Table" RepeatColumns="6" OnItemDataBound="rptAblImg_ItemDataBound" OnItemCommand="rptAblImg_ItemCommand">
                    <ItemTemplate>%>
                        <div class="divAblImgContainer">
                            <div class="divImg">
                                <asp:Image ID="imgAblImg" runat="server" />
                            </div>
                            <div class="divDetail">
                                <div class="divName">
                                    <asp:Literal ID="litName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MASTHEAD_NAME") %>'></asp:Literal>
                                </div>
                                <div class="divActive">
                                    Active : <asp:Literal ID="litActive" runat="server" Text='<%# clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "MASTHEAD_ACTIVE"))) %>'></asp:Literal>
                                </div>
                                <div class="divGroupName">
                                    Group Name : <asp:Literal ID="litGroupName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "V_GRPDNAME") %>'></asp:Literal>
                                </div>
                                <div class="form__action form__action--floated">
                                    <asp:LinkButton ID="lnkbtnEdit" CssClass="lnkbtn lnkbtnEdit" runat="server" CommandName="cmdEdit" CommandArgument='<% #DataBinder.Eval(Container.DataItem, "MASTHEAD_ID")%>'></asp:LinkButton>
                                    <span id="" class="spanSplitter">|</span>
                                    <asp:LinkButton ID="lnkbtnDelete" CssClass="lnkbtn lnkbtnDelete" runat="server" CommandName="cmdDelete" CommandArgument='<% #DataBinder.Eval(Container.DataItem, "MASTHEAD_ID")%>'></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <SeparatorTemplate>
                    <div style="width:20px;"></div>
                    </SeparatorTemplate>
                </asp:DataList>--%>
        </div>
    </div>
    <script>
        function adjustPortfolio() {
            var tuning = 24;
            var width = <% =clsSetting.CONSTSLIDESHOWMANAGERIMGWIDTH %> + tuning;
            var container = $(".divListingData").width();
             
            // ratio = 5:3
            var numEleRow = Math.ceil(container / width);
            var newWidth = (container/numEleRow) - tuning;
            var newHeight = (newWidth / 5) * 3;
            $(".divAblImgContainer").width(newWidth)
            $(".divListingData .divImg").width(newWidth).height(newHeight);
            
            
        }
        jQuery(function() {
            adjustPortfolio();
            jQuery(window).resize(function() {
                adjustPortfolio();
            });
        });
    </script>
</asp:Content>