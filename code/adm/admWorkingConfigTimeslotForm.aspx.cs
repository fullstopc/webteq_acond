﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;
public partial class adm_adm : System.Web.UI.Page
{


    #region "Properties"
    protected int _mode = 1;
    protected int _timeslotID = 0;
    #endregion

    #region "Property Methods"
    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    public int timeslotID
    {
        get
        {
            if (ViewState["TIMESLOT_ID"] == null)
            {
                return _timeslotID;
            }
            else
            {
                return int.Parse(ViewState["TIMESLOT_ID"].ToString());
            }
        }
        set
        {
            ViewState["TIMESLOT_ID"] = value;
        }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    timeslotID = int.Parse(Request["id"]);
                }
                catch (Exception ex)
                {
                    timeslotID = 0;
                }
            }
           
            if (timeslotID > 0)
            {
                mode = 2;
            }

            if(mode == 2)
            {
                fillform();
            }

        }

    }
    #endregion

    #region "Methods"
    private void fillform()
    {
        clsWorkingConfigTimeslot timeslot = new clsWorkingConfigTimeslot();
        if (timeslot.extractByID(timeslotID))
        {
            TextBox txtGroup = (TextBox)this.AdmWorkingTimeGroup.FindControl("txtWTGroup");
            txtGroup.Text = timeslot.group;
            txtWTFrom.Text = timeslot.timeFrom.ToString("h:mm A");
            txtWTTo.Text = timeslot.timeTo.ToString("h:mm A");
        }
    }
    #endregion

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if(Page.IsValid)
        {
            #region Model Validation
            bool isValidInput = true;
            DateTime timeFrom = DateTime.MinValue;
            DateTime timeTo = DateTime.MinValue;
            lblErrWT.Visible = false;
            lblErrWT.Text = "";

            TextBox txtGroup = (TextBox)this.AdmWorkingTimeGroup.FindControl("txtWTGroup");

            if (string.IsNullOrEmpty(txtWTFrom.Text) || string.IsNullOrEmpty(txtWTTo.Text) || string.IsNullOrEmpty(txtGroup.Text))
            {
                lblErrWT.Visible = true;
                lblErrWT.Text += "From/To/Group fields is required. ";
                lblErrWT.ForeColor = System.Drawing.Color.Red;
                isValidInput = false;
            }
            else
            {
                if (!DateTime.TryParse(txtWTFrom.Text, out timeFrom))
                {
                    lblErrWT.Visible = true;
                    lblErrWT.Text += "From Time Format is not valid.";
                    lblErrWT.ForeColor = System.Drawing.Color.Red;
                    isValidInput = false;
                }

                if (!DateTime.TryParse(txtWTTo.Text, out timeTo))
                {
                    lblErrWT.Visible = true;
                    lblErrWT.Text += "To Time Format is not valid.";
                    lblErrWT.ForeColor = System.Drawing.Color.Red;
                    isValidInput = false;
                }

                if (isValidInput)
                {
                    if (timeTo < timeFrom)
                    {
                        lblErrWT.Visible = true;
                        lblErrWT.Text += "From Time cannot greater than To Time.";
                        lblErrWT.ForeColor = System.Drawing.Color.Red;
                        isValidInput = false;
                    }
                }
            }
            #endregion

            #region Edit Time Slot
            int intAdmID = Convert.ToInt32(Session["ADMID"]);

            clsWorkingConfigTimeslot timeslot = new clsWorkingConfigTimeslot();
            timeslot.timeFrom = timeFrom;
            timeslot.timeTo = timeTo;
            timeslot.group = txtGroup.Text;
            timeslot.ID = mode == 2 ? timeslotID : -1;
            
            string msg = "";
            bool Success = true;
            bool Updated = false;

            if (mode == 1)
            {
                if (timeslot.add())
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();
                }
            }
            else if (mode == 2)
            {
                if (!timeslot.isSame() && !timeslot.isCrashTime())
                {
                    if (timeslot.update())
                    {
                        Updated = true;
                        msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();
                    }
                    else
                    {
                        Success = false;
                    }
                }

                if (Success && !Updated)
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString();
                }
            }

            if (msg == "")
            {
                msg = GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString();
            }



            Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.TimeslotSaveSuccess('" + msg + "')", true);
            #endregion
        }
    }
}