﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admEnquiry01.aspx.cs" Inherits="adm_admEnquiry01" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" Runat="Server">
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jquery.fancybox.js"></script>
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>fancybox/jquery.fancybox.css" rel="stylesheet" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>fancybox/jquery.fancybox.custom.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">

    <div class="main-content__header">
        <div class="title"><asp:Literal ID="litPageTitle" runat="server" meta:resourcekey="litPageTitle"></asp:Literal></div>
        <div class="divListingSplitter"></div>
        <div class="action">
			<asp:Panel ID="Panel1" runat="server" CssClass="divListingAction">
                <asp:LinkButton ID="lnkbtnExport" runat="server" Text="Export to Excel" ToolTip="Export to Excel" CssClass="hypListingAction export" OnClick="lnkbtnExport_Click"></asp:LinkButton>
            </asp:Panel>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    
    <div class="divListing">
        <div class="divListingHdr">
            <div class="divListingHdrLeft"><asp:Literal ID="litItemFound" runat="server"></asp:Literal></div>
        </div>
        <asp:Panel ID="pnlAck" runat="server" CssClass="divAck" Visible="false">
            <div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
        </asp:Panel>
        <uc:CmnDatatable ID="ucCmnDatatable" runat="server" />
        <asp:HiddenField runat="server" ID="hdnEnqID" />
        <asp:LinkButton ID="lnkbtnResend" runat="server" OnClick="lnkbtnResend_Click" style="display:none;"></asp:LinkButton>

        <%-- Visible false at 11 Oct 2016 --%>
        <div class="divListingData" runat="server" visible="false">
            <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom" CssClass="dataTbl" OnSorting="gvItems_Sorting" OnRowDataBound="gvItems_RowDataBound" OnPageIndexChanging="gvItems_PageIndexChanging" OnRowCommand="gvItems_RowCommand" DataKeyNames="ENQ_ID" EmptyDataText="No record found.">
                <PagerStyle CssClass="pg" HorizontalAlign="Right" />
                <AlternatingRowStyle CssClass="td_alt" />
                <Columns>
                    <asp:TemplateField HeaderText="no.">
                        <ItemStyle CssClass="tdNo" />
                        <ItemTemplate>
                            <asp:Literal ID="litNo" runat="server" Text="<%#Container.DataItemIndex + 1 %>"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="received date" DataField="ENQ_CREATION" SortExpression="ENQ_CREATION" HtmlEncode="false" DataFormatString="{0:dd MMM yyyy hh:mm:ss tt}"/>
                    
                    <asp:BoundField HeaderText="url" DataField="ENQ_URL" SortExpression="ENQ_URL" HeaderStyle-CssClass="hiddenCol" ItemStyle-CssClass="hiddenCol"/>
                    <asp:BoundField HeaderText="sender name" DataField="ENQ_NAME" SortExpression="ENQ_NAME" />
                    <asp:BoundField HeaderText="company name" DataField="ENQ_COMPANYNAME" SortExpression="ENQ_COMPANYNAME" />
                    <asp:BoundField HeaderText="contact no" DataField="ENQ_CONTACTNO" SortExpression="ENQ_CONTACTNO" />
                    <asp:BoundField HeaderText="sender email" DataField="ENQ_EMAILSENDER" SortExpression="ENQ_EMAILSENDER"/>
                    <asp:BoundField HeaderText="subject" DataField="ENQ_SUBJECT" SortExpression="ENQ_SUBJECT"/>
                    <asp:BoundField HeaderText="message" DataField="ENQ_MESSAGE" SortExpression="ENQ_MESSAGE" HeaderStyle-CssClass="hiddenCol" ItemStyle-CssClass="hiddenCol"/>
                    <asp:BoundField HeaderText="email recipient" DataField="ENQ_EMAILRECIPIENT" SortExpression="ENQ_EMAILRECIPIENT"/>
                    <asp:TemplateField HeaderText="active" SortExpression="ENQ_ACTIVE" HeaderStyle-CssClass="hiddenCol" >
                        <ItemStyle CssClass="td_small hiddenCol"/>
                        <ItemTemplate>
                            <asp:Literal ID="litActive" runat="server" Text='<%#clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "ENQ_ACTIVE"))) %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="action" HeaderStyle-CssClass="alignCenter">
                        <ItemStyle CssClass="tdAct alignCenter" />
                        <ItemTemplate>
                            <asp:HyperLink ID="hypView" CssClass="lnkbtn lnkbtnNoIcon" runat="server"></asp:HyperLink>
                            <%--<asp:LinkButton ID="lnkbtnView" CssClass="lnkbtn lnkbtnNoIcon" runat="server" CausesValidation="false" CommandName="cmdView"></asp:LinkButton>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>

    <script type="text/javascript">

        $(function () {
            var columns = [
                    { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                    { "data": "ENQ_CREATION", "bSearchable": false, "label": "Received Date", "order": 1,"convert":"datetime" },
                    { "data": "ENQ_NAME", "label": "Sender Name", "order": 2, },
                    { "data": "ENQ_COMPANYNAME", "label": "Company Name", "order": 3, },
                    { "data": "ENQ_CONTACTNO", "label": "Contact no", "order": 4, },
                    { "data": "ENQ_EMAILSENDER", "label": "Sender Email", "order": 5, },
                    { "data": "ENQ_SUBJECT", "bSearchable": false, "label": "Subject", "order": 6 },
                    { "data": "ENQ_EMAILRECIPIENT", "bSearchable": false, "label": "Email Receipient", "order": 7 },
                    { "data": "ENQ_LASTRESEND", "bSearchable": false, "label": "Last Resend", "order": 8, "convert": "datetime" },
                    { "data": "action", "bSortable": false, "bSearchable": false, "label": "Action", "order": 9, "exportable": false, "className": "dt-body-center" },
            ];
            var columnSort = columns.reduce(function (prev, curr) {
                prev[curr.data] = curr.order;
                return prev;
            }, {});

            var columnDefs = [{
                "render": function (data, type, row) {
                    var btnView = "<a class='lnkbtn lnkbtnView fancybox fancybox.iframe' rel='enquiry-group' href='admEnquiry0101.aspx?id=" + row.ENQ_ID + "' title='View'></a>";
                    var btnResend = "<a class='lnkbtn lnkbtnResend hypResend' title='Resend' href='#' data-id='" + row.ENQ_ID + "'></a>";
                    return btnView + btnResend;
                },
                "targets": columnSort["action"]
            }];
            var properties = {
                columns: columns,
                columnDefs: columnDefs,
                columnSort: columnSort,
                func: "getEnquiryData",
                name: "enquiry",
            };

            // Export
            $("#<%= lnkbtnExport.ClientID %>").click(function (e) {
                var $table = $('.dataTable').clone();
                $table.attr("border", "1")
                $table.find("th:last-child,td:last-child").remove();
                window.open('data:application/vnd.ms-excel,' + encodeURIComponent($table.get(0).outerHTML));

                e.preventDefault();
            });

            $(document).on("click", ".hypResend", function () {
                if (confirm("Are you sure you want to resend email?")) {
                    var pageID = $(this).attr("data-id");
                    document.getElementById('<%= hdnEnqID.ClientID %>').value= pageID;
                    document.getElementById('<%= lnkbtnResend.ClientID %>').click();
                }
                return false;
            })

            var $datatable = callDataTables(properties);
            $(".fancybox").fancybox();
            
        });
    </script>
</asp:Content>

