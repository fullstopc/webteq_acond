﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admGeneralSettings01.aspx.cs" Inherits="adm_admCMSControlPanel01" validateRequest="false" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmWorkingTimeGroup.ascx" TagName="AdmWorkingTimeGroup" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlForm" runat="server" CssClass="divForm">
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <div class="form__container">
            <div class="form__section property-area-section">
                <div class="ack-container success hide">
                    <div class="ack-msg"></div>
                </div>
                <div class="form__section__title">
                    Property Area
                </div>
                <div class="form__section__content">
                    <uc:CmnDatatable ID="ucCmnDatatable_PropertyArea" runat="server" />
                    <asp:HiddenField runat="server" ID="hdnPropertyAreaID" />
                    <table id="tblPropertyArea"></table>
                </div>
            </div>

            <!-- Start of Working Day !-->
            <div class="form__section property-type-section">
                <div class="ack-container success hide">
                    <div class="ack-msg"></div>
                </div>
                <div class="form__section__title">
                    Property Type
                </div>
                <div class="form__section__content">
                    <uc:CmnDatatable ID="ucCmnDatatable_PropertyType" runat="server" />
                    <asp:HiddenField runat="server" ID="hdnPropertyTypeID" />
                    <table id="tblPropertyType"></table>
                </div>
            </div>
            <!-- End of Working Day !-->
        </div>
    </asp:Panel>
    <script type="text/javascript">
        var $tableArea;
        var AreaSaveSuccess = function (msg) {
            $.magnificPopup.close();
            $(".property-area-section .ack-container .ack-msg").html(msg);
            $(".property-area-section .ack-container").removeClass("hide");
            $tableArea.ajax.reload();
        };

        var initArea = function () {
            var url = "";
            var columns = [
                    { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                    { "data": "area_name", "label": "Area Name", "order": 1,  },
                    { "data": "action", "bSortable": false, "bSearchable": false, "label": "<a class='popup' href='admConfigPropertyAreaForm.aspx'><i class='fa fa-plus' aria-hidden='true'></i></a>", "order": 2, "align": "center" }, // Add Label: <a class='button add popup' href='admConfigPropertyAreaForm.aspx'><i class='material-icons icon'>add</i><span>Add</span></a>
            ];
            var columnSort = columns.reduce(function (prev, curr) {
                prev[curr.data] = curr.order;
                return prev;
            }, {});

            var columnDefs = [{
                "render": function (data, type, row) {
                    var lnkbtnEdit = "<a class='lnkbtn lnkbtnEdit popup' href='admConfigPropertyAreaForm.aspx?id=" + row.area_id + "' title='Edit'></a>";
                    var lnkbtnDelete = "<a class='lnkbtn lnkbtnDelete lnkbtnDeleteWT' title='Delete' data-id='" + row.area_id + "'></a>";

                    return lnkbtnEdit + lnkbtnDelete;
                },
                "className": "dt-center",
                "targets": columnSort["action"]
            }];
            var properties = {
                columns: columns,
                columnDefs: columnDefs,
                columnSort: columnSort,
                dom: 'ZBrti',
                func: "getPropertyArea",
                name: "property_area",
                elementID: "tblPropertyArea",
                callback: function () {
                    $('#tblPropertyArea .popup').magnificPopup({
                        type: 'iframe',
                        mainClass: 'timeslot-frame',
                        iframe: {
                            markup: `
                                <div class="mfp-iframe-scaler">
                                    <div class="mfp-close"></div>
                                    <iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>
                                </div>`
                        }
                    });
                }
            };
            $tableArea = callDataTables(properties);
        }

        $(function () {
            initArea();
        });
    </script>
    <script type="text/javascript">
        var $tableType;
        var TypeSaveSuccess = function (msg) {
            $.magnificPopup.close();
            $(".property-type-section .ack-container .ack-msg").html(msg);
            $(".property-type-section .ack-container").removeClass("hide");
            $tableType.ajax.reload();
        };

        var initType = function () {
            var url = "";
            var columns = [
                    { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                    { "data": "type_name", "label": "Name", "order": 1, },
                    { "data": "type_airconds", "label": "Airconds", "order": 2, },
                    { "data": "action", "bSortable": false, "bSearchable": false, "label": "<a class='popup' href='admConfigPropertyTypeForm.aspx'><i class='fa fa-plus' aria-hidden='true'></i></a>", "order": 3, "align": "center" }, // Add Label: <a class='button add popup' href='admConfigPropertyTypeForm.aspx'><i class='material-icons icon'>add</i><span>Add</span></a>
            ];
            var columnSort = columns.reduce(function (prev, curr) {
                prev[curr.data] = curr.order;
                return prev;
            }, {});

            var columnDefs = [{
                "render": function (data, type, row) {
                    var lnkbtnEdit = "<a class='lnkbtn lnkbtnEdit popup' href='admConfigPropertyTypeForm.aspx?id=" + row.type_id + "' title='Edit'></a>";
                    var lnkbtnDelete = "<a class='lnkbtn lnkbtnDelete lnkbtnDeleteWT' title='Delete' data-id='" + row.type_id + "'></a>";

                    return lnkbtnEdit + lnkbtnDelete;
                },
                "className": "dt-center",
                "targets": columnSort["action"]
            }];
            var properties = {
                columns: columns,
                columnDefs: columnDefs,
                columnSort: columnSort,
                dom: 'ZBrti',
                func: "getPropertyType",
                name: "property_type",
                elementID: "tblPropertyType",
                callback: function () {
                    $('#tblPropertyType .popup').magnificPopup({
                        type: 'iframe',
                        mainClass: 'timeslot-frame',
                        iframe: {
                            markup: `
                                <div class="mfp-iframe-scaler">
                                    <div class="mfp-close"></div>
                                    <iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>
                                </div>`
                        }
                    });
                }
            };
            $tableType = callDataTables(properties);
        }

        $(function () {
            initType();
        });
    </script>
</asp:Content>

