﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admGST01.aspx.cs" Inherits="adm_admGST01" ValidateRequest="false"  %>

<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmGSTSetting.ascx" TagName="AdmGSTSetting" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="Server" >
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server" meta:resourcekey="litPageTitle"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlForm" runat="server" CssClass="divForm divGstSetting">
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <uc:AdmGSTSetting ID="AdmGSTSetting1" runat="server" />
    </asp:Panel>
</asp:Content>
