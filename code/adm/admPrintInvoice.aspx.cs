﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;

public partial class adm_admPrintInvoice : System.Web.UI.Page
{
    #region "Properties"
    protected int _ordId;
    protected int type;
    protected int orderType;
    protected int intProdCount;
    protected string currency;
    protected decimal decSubTotal = Convert.ToDecimal("0.00");
    protected decimal decShipping = Convert.ToDecimal("0.00");
    #endregion

    #region "Property Methods"
    public int ordId
    {
        get
        {
            if (ViewState["ORDID"] == null)
            {
                return _ordId;
            }
            else
            {
                return int.Parse(ViewState["ORDID"].ToString());
            }
        }
        set { ViewState["ORDID"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;
                if (int.TryParse(Request["id"], out intTryParse))
                {
                    ordId = intTryParse;
                }
                else
                {
                    ordId = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["ptype"]))
            {
                int intTryParse;
                if (int.TryParse(Request["ptype"], out intTryParse))
                {
                    type = intTryParse;
                }
                else
                {
                    type = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["otype"]))
            {
                int intTryParse;
                if (int.TryParse(Request["otype"], out intTryParse))
                {
                    orderType = intTryParse;
                }
                else
                {
                    orderType = 0;
                }
            }

            setPageProperties();
        }

        //registerCKEditorScript();
        setPageProperties();
        bindRptData();
    }

    protected void rptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            HtmlTableCell thUnitPrice = (HtmlTableCell)e.Item.FindControl("thUnitPrice");
            HtmlTableCell thAmount = (HtmlTableCell)e.Item.FindControl("thAmount");

            thUnitPrice.Visible = thAmount.Visible = type == 1;
        }
        else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strProdName = DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODNAME").ToString();
            string strProdSnapshot = DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODSNAPSHOT").ToString();
            int intProdQty = int.Parse(DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODQTY").ToString());
            decimal decProdUnitPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODPRICE"));
            decimal decProdTotal = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "ORDDETAIL_PRODTOTAL"));

            HtmlTableCell tdUnitPrice = (HtmlTableCell)e.Item.FindControl("tdUnitPrice");
            HtmlTableCell tdTotal = (HtmlTableCell)e.Item.FindControl("tdTotal");

            tdUnitPrice.Visible = tdTotal.Visible = type == 1;

            intProdCount += 1;

            Literal litNo = (Literal)e.Item.FindControl("litNo");
            litNo.Text = intProdCount.ToString();

            Literal litItem = (Literal)e.Item.FindControl("litItem");
            litItem.Text = strProdName + "</br>" + strProdSnapshot;

            Literal litQty = (Literal)e.Item.FindControl("litQty");
            litQty.Text = String.Format("{0:#,0}", intProdQty); //intProdQty.ToString();

            Literal litUnitPrice = (Literal)e.Item.FindControl("litUnitPrice");
            litUnitPrice.Text = String.Format("{0:N}", decProdUnitPrice); //decProdUnitPrice.ToString();

            decimal decItemsSubTotal = decProdUnitPrice * intProdQty;
            decSubTotal += decItemsSubTotal;

            Literal litItemTotal = (Literal)e.Item.FindControl("litItemTotal");
            litItemTotal.Text = String.Format("{0:N}", decProdTotal); //decProdTotal.ToString();
        }
        else if (e.Item.ItemType == ListItemType.Footer)
        {
            HtmlTableRow trSubtotal = (HtmlTableRow)e.Item.FindControl("trSubtotal");
            HtmlTableRow trShipping = (HtmlTableRow)e.Item.FindControl("trShipping");
            HtmlTableRow trTotal = (HtmlTableRow)e.Item.FindControl("trTotal");

            trSubtotal.Visible = trShipping.Visible = decShipping != 0 && type == 1;
            trTotal.Visible = type == 1;

            Literal litSubTotal = (Literal)e.Item.FindControl("litSubTotal");
            litSubTotal.Text = String.Format("{0:N}", decSubTotal); //decSubTotal.ToString();

            Literal litShipping = (Literal)e.Item.FindControl("litShipping");
            litShipping.Text = String.Format("{0:N}", decShipping); //decShipping.ToString();

            Literal litSubtotalCurrency = (Literal)e.Item.FindControl("litSubtotalCurrency");
            Literal litShippingCurrency = (Literal)e.Item.FindControl("litShippingCurrency");
            Literal litTotalCurrency = (Literal)e.Item.FindControl("litTotalCurrency");

            clsConfig config = new clsConfig();
            currency = config.currency;

            litSubtotalCurrency.Text = "(" + currency + ")";
            litShippingCurrency.Text = litTotalCurrency.Text = litSubtotalCurrency.Text;

            Literal litGrandTotal = (Literal)e.Item.FindControl("litGrandTotal");
            litGrandTotal.Text = String.Format("{0:N}", decSubTotal + decShipping); //(decSubTotal + decShipping).ToString();
        }

        if (e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlTableCell tdNo = (HtmlTableCell)e.Item.FindControl("tdNo");
            HtmlTableCell tdItem = (HtmlTableCell)e.Item.FindControl("tdItem");
            HtmlTableCell tdQty = (HtmlTableCell)e.Item.FindControl("tdQty");
            HtmlTableCell tdUnitPrice = (HtmlTableCell)e.Item.FindControl("tdUnitPrice");
            HtmlTableCell tdTotal = (HtmlTableCell)e.Item.FindControl("tdTotal");

            tdUnitPrice.Visible = tdTotal.Visible = type == 1;

            tdNo.Attributes["class"] = "tdNo_alt";
            tdItem.Attributes["class"] = "tdItem_alt";
            tdQty.Attributes["class"] = "tdQty_alt";
            tdUnitPrice.Attributes["class"] = "tdUnitPrice_alt";
            tdTotal.Attributes["class"] = "tdTotal_alt";
        }
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEINVOICELOGO, clsConfig.CONSTGROUPINVOICE, 1);
        string strLogo = config.value;

        if (!string.IsNullOrEmpty(strLogo))
        {
            string strImgPath = HttpContext.Current.Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTCMSFOLDER + "/" + clsAdmin.CONSTIMAGESFOLDER + "/" + config.value;
            string strFilePath = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + strImgPath + "&w=" + clsAdmin.CONSTADMINVOICELOGOWIDTH + "&h=" + clsAdmin.CONSTADMINVOICELOGOHEIGHT + "&f=1";
            imgInvoiceLogo.ImageUrl = strFilePath;
        }

        divPrintInvoiceHeaderLeft.Attributes["style"] = "width:" + clsAdmin.CONSTADMINVOICELOGOWIDTH + "; float:left; padding";

        config.extractItemByNameGroup(clsConfig.CONSTNAMECOMPANYNAME, clsConfig.CONSTGROUPINVOICE, 1);
        string companyName = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEROC, clsConfig.CONSTGROUPINVOICE, 1);
        string roc = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMECOMPANYADDRESS, clsConfig.CONSTGROUPINVOICE, 1);
        string companyAddress = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMECOMPANYTEL, clsConfig.CONSTGROUPINVOICE, 1);
        string companyTel = "Tel : " + config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMECOMPANYEMAIL, clsConfig.CONSTGROUPINVOICE, 1);
        string companyEmail = "Email : " + config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMECOMPANYWEBSITE, clsConfig.CONSTGROUPINVOICE, 1);
        string companyWebsite = "Website : " + config.value;

        litCompanyDetails.Text = companyName + "<span class='spanROC'>(" + roc + ")</span><br/>" + companyAddress + "<br/>" + companyTel + "<br/>" +
                                 companyEmail + "<br/>" + companyWebsite;

        litInvoiceHdr.Text = type == 1 ? "SALES ORDER" : "DELIVERY ORDER";
        pnlPrintInvoiceAllRemarks.Visible = type == 1;
        pnlPrintInvoiceSignature.Visible = type == 2;

        fillForm();
    }

    protected void fillForm()
    {
        clsOrder ord = new clsOrder();

        if (ord.extractOrderById4(ordId))
        {
            decShipping = ord.ordShipping;

            string name = "";
            string contactPerson = "";

            string company = ord.company;
            string email = ord.email;
            string address = ord.add;
            string poscode = ord.poscode;
            string city = ord.city;
            string state = ord.state;
            string country = ord.countryName;
            string tel1 = ord.contactNo;
            string tel2 = ord.contactNo2;
            string fax = ord.fax;

            string shippingCompany = ord.shippingCustCompany;
            string shippingEmail = ord.shippingEmail;
            string shippingAddress = ord.shippingAdd;
            string shippingPoscode = ord.shippingPoscode;
            string shippingCity = ord.shippingCity;
            string shippingState = ord.shippingState;
            string shippingCountry = ord.shippingCountryName;
            string shippingTel1 = ord.shippingContactNo;
            string shippingTel2 = ord.shippingContactNo2;
            string shippingFax = ord.shippingFax;

            litSalesDate.Text = ord.ordCreation.Day + " " + ord.ordCreation.ToString("MMM") + " " + ord.ordCreation.Year;

            if (type == 1)
            {
                if (!string.IsNullOrEmpty(company)) 
                { 
                    name = company; 
                    litBillDelName.Text = name + "<br/>";
                }

                contactPerson = ord.name;

                litTel1.Text = tel1;
                litTel2.Text = tel2;
                litFax.Text = fax;
                litAttnName.Text = contactPerson;

                litBillDeliveryDetails.Text = address + "<br/>" + poscode + "," + city + "<br/>" +
                                              state + ",<br/>" + country;

                lblInvoiceNo.Text = "SALES ORDER NO.";
            }
            else if (type == 2)
            {
                if (!string.IsNullOrEmpty(shippingCompany)) 
                { 
                    name = shippingCompany;
                    litBillDelName.Text = name + "<br/>";
                }

                contactPerson = ord.shippingName;

                litTel1.Text = shippingTel1;
                litTel2.Text = shippingTel2;
                litFax.Text = shippingFax;
                litAttnName.Text = contactPerson;

                litBillDeliveryDetails.Text = shippingAddress + "<br/>" + shippingPoscode + "," + shippingCity + "<br/>" +
                                              shippingState + ",<br/>" + shippingCountry;

                lblInvoiceNo.Text = "D/O NO.";

                if (ord.shippingDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
                {
                    litSalesDate.Text = ord.shippingDate.Day + " " + ord.shippingDate.ToString("MMM") + " " + ord.ordCreation.Year;
                }
            }

            lblDate.Text = "DATE";
            lblCustomerNo.Text = "CUSTOMER P/O NO.";

            litSalesNo.Text = ord.ordNo;
            litCustomerNo.Text = ord.customerNo;

            litInvoiceSalesRemarks.Text = ord.ordRemarks;

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMEINVOICEREMARKS, clsConfig.CONSTGROUPINVOICE, 1);
            litInvoiceRemarks.Text = config.longValue;

            config.extractItemByNameGroup(clsConfig.CONSTNAMEINVOICESIGNATURE, clsConfig.CONSTGROUPINVOICE, 1);
            litInvoiceSignature.Text = config.longValue;
        }
    }

    protected void bindRptData()
    {
        clsOrder ord = new clsOrder();
        DataSet ds = new DataSet();
        ds = ord.getOrderItemsByOrderId2(ordId);
        DataView dv = new DataView(ds.Tables[0]);

        rptItems.DataSource = dv;
        rptItems.DataBind();
    }
    #endregion
}
