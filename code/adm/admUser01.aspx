﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admUser01.aspx.cs" Inherits="adm_admUser01" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title"><asp:Literal ID="litPageTitle" runat="server" meta:resourcekey="litPageTitle"></asp:Literal></div>
        <div class="divListingSplitter"></div>
        <div class="action">
			
			<asp:Panel ID="pnlListingAction" runat="server" CssClass="divListingAction">
                <asp:HyperLink ID="hypAddUser" runat="server" Text="Add User" ToolTip="Add User" NavigateUrl="admUser0101.aspx" CssClass="hypListingAction add"></asp:HyperLink>
            </asp:Panel>
        </div>
        <div class="divListingSplitter"></div>
        <div class="filter">
            <div class="divFilterContainer">
                <div class="divFilter">
                    <asp:TextBox ID="txtKeyword" runat="server" placeholder="Keyword Search" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
                    <a href="#" class="btnDropdown" onclick="toggleFilterOption();"></a>
                    <asp:Literal ID="litKeywordRule" runat="server" meta:resourcekey="litKeywordRule" visible="false"></asp:Literal>
                </div>
                <div class="divFilterMore">
                    <div></div>
                    <div>
                        <table class="tblSearch">
							<tr>
                                <td class="">
									<div class="divFilterLabel">Type:</div>
                                    <asp:DropDownList ID="ddlType" runat="server" class="ddl_medium"></asp:DropDownList>
                                </td>
								<td>
									<div class="divFilterLabel">Active:</div>
                                    <asp:DropDownList ID="ddlActive" runat="server" class="ddl_medium"></asp:DropDownList>
								</td>
                            </tr>
							
                        </table>

                    </div>
                </div>
            </div>
            <div class="divFilterAction"><asp:Button ID="lnkbtnSearch" runat="server" Text="" ToolTip="Search" CssClass="btn btnSearch" Visible="false"/></div>
        </div>
    </div>
    <div class="divListing">
        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
            <div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
        </asp:Panel>


        </asp:UpdatePanel><uc:CmnDatatable ID="ucCmnDatatable" runat="server" />
        <asp:HiddenField runat="server" ID="hdnItemID" />
        <asp:LinkButton ID="lnkbtnDelete" runat="server" OnClick="lnkbtnDelete_Click" style="display:none;"></asp:LinkButton>
        <asp:LinkButton ID="lnkbtnAdminAccess" runat="server" OnClick="lnkbtnAdminAccess_Click" style="display:none;"></asp:LinkButton>
        <asp:LinkButton ID="lnkbtnUserAccess" runat="server" OnClick="lnkbtnUserAccess_Click" style="display:none;"></asp:LinkButton>
    </div>
    <script type="text/javascript">

        $(function () {
            var columns = [
                    { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                    { "data": "USR_EMAIL", "label": "Email", "order": 1, },
                    { "data": "V_USRTYPENAME", "label": "Type", "order": 2, },
                    { "data": "V_USRPROJNAME", "label": "Project", "order": 3, },
                    { "data": "USR_CREATION", "bSearchable": false, "label": "Creation", "order": 4, "convert": "datetime" },
                    { "data": "USR_LASTUPDATE", "bSearchable": false, "label": "Last Update", "order": 5, "convert": "datetime" },
                    { "data": "USR_ACTIVE", "bSearchable": false, "label": "Active", "order": 6, "convert": "active" },
                    { "data": "action", "bSortable": false, "bSearchable": false, "label": "Action", "order": 7 },
            ];
            var columnSort = columns.reduce(function (prev, curr) {
                prev[curr.data] = curr.order;
                return prev;
            }, {});

            var columnDefs = [{
                "render": function (data, type, row) {
                    var lnkbtn = "<div class='action'>";
                    <% if (!isSingleLogin) { %>
                    lnkbtn += "<a class='button lnkbtn lnkbtnEdit' href='admUser0101.aspx?id=" + row.USR_ID + "' title='Edit'></a>";
                    lnkbtn += "<a class='button lnkbtn lnkbtnDelete' title='Delete' data-id='" + row.USR_ID + "'></a>";
                    <%}%>
                    lnkbtn += "<a class='button lnkbtnAdminLock' title='Admin Access' data-id='" + row.USR_ID + "'><i class='material-icons'>lock</i></a>";
                    lnkbtn += "<a class='button lnkbtnUserLock' title='User Access' data-id='" + row.USR_ID + "'><i class='material-icons'>lock</i></a>";
                    lnkbtn += "</div>";
                    return lnkbtn;
                    return '';
                },
                "targets": columnSort["action"]
            }];
            var properties = {
                columns: columns,
                columnDefs: columnDefs,
                columnSort: columnSort,
                func: "getMasterUserData",
                name: "master_user",
            };
            var $datatable = callDataTables(properties);

            // Filter
            $("#<%= ddlActive.ClientID %>").change(function () {
                $datatable
               .columns(columnSort["USR_ACTIVE"])
               .search(this.value)
               .draw();
            });
            $("#<%= ddlType.ClientID %>").change(function () {
                $datatable
               .columns(columnSort["V_USRTYPENAME"])
               .search(this.value)
               .draw();
            });

             $(document).on("click", ".lnkbtnDelete", function () {
                if (confirm("<%= HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeletePage.Text") %>")) {
                    var itemID = $(this).attr("data-id");
                    document.getElementById('<%= hdnItemID.ClientID %>').value = itemID;
                    document.getElementById('<%= lnkbtnDelete.ClientID %>').click();
                }
             })
            $(document).on("click", ".lnkbtnAdminLock", function () {
                var itemID = $(this).attr("data-id");
                document.getElementById('<%= hdnItemID.ClientID %>').value = itemID;
                document.getElementById('<%= lnkbtnAdminAccess.ClientID %>').click();
                
            })
            $(document).on("click", ".lnkbtnUserLock", function () {
                var itemID = $(this).attr("data-id");
                document.getElementById('<%= hdnItemID.ClientID %>').value = itemID;
                document.getElementById('<%= lnkbtnUserAccess.ClientID %>').click();
  
             })

            
        });
    </script>
</asp:Content>
