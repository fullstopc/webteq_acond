﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admPrintAdjust.aspx.cs" Inherits="adm_admPrintAdjust" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <script type="text/javascript">
        window.print();
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="divPrintContainer">
        <div class="main-content__header">
            <div class="main-content__headerLeft"><asp:Image ID="imgLogo" runat="server" Visible="false" /></div>
            <div class="main-content__headerRight"><span class="spanTitleLarge"><asp:Literal ID="litPageHdr" runat="server" meta:resourcekey="litPageTitle"></asp:Literal></span></div>
        </div>
        <asp:Panel ID="pnlAck" runat="server" CssClass="divAck" Visible="false">
            <asp:Literal ID="litAck" runat="server"></asp:Literal>
        </asp:Panel>
        <div class="divListing">
            <table cellpadding="0" cellspacing="0" class="formTbl">
                <tr>
                    <td>
                        <asp:Literal ID="litDetails" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
        </div>
        <div class="divListing">
            <asp:Repeater ID="rptItems" runat="server" OnItemDataBound="rptItems_ItemDataBound">
                <HeaderTemplate>
                    <table cellpadding="0" cellspacing="0" class="listTbl">
                        <tr>
                            <th>No.</th>
                            <th>Item</th>
                            <th>Quantity</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                        <tr class="trItem">
                            <td><asp:Literal ID="litNo" runat="server"></asp:Literal></td>
                            <td class="td_norLeft"><asp:Literal ID="litItem" runat="server"></asp:Literal></td>
                            <td><asp:Literal ID="litQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AI_PRODQTY") %>'></asp:Literal></td>
                        </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>

