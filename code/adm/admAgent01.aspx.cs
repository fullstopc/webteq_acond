﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Linq;
using System.Configuration;

public partial class adm_adm : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 1002;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        string title = "Service Agent";
        Page.Title += clsMis.formatPageTitle(title, true);
        litPageTitle.Text = title;

        if (!IsPostBack)
        {

            Master.sectId = sectId;
            Master.moduleDesc = title;

            setPageProperties();

        }

        new Acknowledge(Page)
        {
            container = pnlAck,
            msg = litAck
        }.init();
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        int intItemID = Convert.ToInt16(hdnItemID.Value);
        string name = "";

        clsEmployee agent = new clsEmployee();
        agent.ID = intItemID;

        if (agent.extractByID(intItemID))
        {
            name = agent.name;
        }

        if (agent.delete() == 1)
        {
            Session[clsKey.SESSION_DELETED] = 1;
            Session[clsKey.SESSION_MSG] = "Agent " + name + " delete successfully.";
        }
        Response.Redirect(currentPageName);
    }

    #endregion
}