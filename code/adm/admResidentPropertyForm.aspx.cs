﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_adm : System.Web.UI.Page
{
    #region "Properties"
    protected int _mode = 1;
    protected int _propertyID = 0;
    protected int _memberID = 0;
    #endregion

    #region "Property Methods"
    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    public int propertyID
    {
        get
        {
            if (ViewState["PROPERTY_ID"] == null)
            {
                return _propertyID;
            }
            else
            {
                return int.Parse(ViewState["PROPERTY_ID"].ToString());
            }
        }
        set
        {
            ViewState["PROPERTY_ID"] = value;
        }
    }
    public int memberID
    {
        get
        {
            if (ViewState["MEMBER_ID"] == null)
            {
                return _memberID;
            }
            else
            {
                return int.Parse(ViewState["MEMBER_ID"].ToString());
            }
        }
        set
        {
            ViewState["MEMBER_ID"] = value;
        }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            propertyID = Request["id"].ToInteger();
            memberID = Request["memid"].ToInteger();

            ddlType.DataSource = new clsConfigPropertyType().getDataTable();
            ddlType.DataValueField = "type_id";
            ddlType.DataTextField = "type_name";
            ddlType.DataBind();
            ddlType.AddDefault();

            ddlArea.DataSource = new clsConfigPropertyArea().getDataTable();
            ddlArea.DataValueField = "area_id";
            ddlArea.DataTextField = "area_name";
            ddlArea.DataBind();
            ddlArea.AddDefault();

            if (propertyID > 0)
            {
                mode = 2;
            }

            if (mode == 2)
            {
                fillform();
            }

        }

    }
    #endregion

    #region "Methods"
    private void fillform()
    {
        clsProperty property = new clsProperty();
        if (property.extractByID(propertyID))
        {
            ddlArea.SelectedValue = property.area.ToString();
            ddlType.SelectedValue = property.type.ToString();
            txtUnit.Text = property.unitno;
            //txtArea.Text = property.area;
            //txtType.Text = property.type;
        }
    }
    #endregion

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int intAdmID = Convert.ToInt32(Session["ADMID"]);

            clsProperty property = new clsProperty();
            property.unitno = txtUnit.Text;
            property.type = ddlType.SelectedValue.ToInteger();
            property.area = ddlArea.SelectedValue.ToInteger();
            property.memberID = memberID;
            property.ID = mode == 2 ? propertyID : -1;

            string msg = "";
            bool Success = true;
            bool Updated = false;

            if (mode == 1)
            {
                if (property.add())
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();
                }
            }
            else if (mode == 2)
            {
                if (!property.isSame())
                {
                    if (property.update())
                    {
                        Updated = true;
                        msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();
                    }
                    else
                    {
                        Success = false;
                    }
                }

                if (Success && !Updated)
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString();
                }
            }

            if (msg == "")
            {
                msg = GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString();
            }

            Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.propertySaveSuccess('" + msg + "')", true);

        }
    }
}