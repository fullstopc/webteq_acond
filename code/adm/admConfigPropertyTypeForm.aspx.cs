﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class adm_adm : System.Web.UI.Page
{
    #region "Properties"
    protected int _mode = 1;
    protected int _typeID = 0;
    #endregion

    #region "Property Methods"
    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    public int typeID
    {
        get
        {
            if (ViewState["TYPE_ID"] == null)
            {
                return _typeID;
            }
            else
            {
                return int.Parse(ViewState["TYPE_ID"].ToString());
            }
        }
        set
        {
            ViewState["TYPE_ID"] = value;
        }
    }
   
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            typeID = Request["id"].ToInteger();

            if (typeID > 0)
            {
                mode = 2;
            }

            if (mode == 2)
            {
                fillform();
            }

        }

    }
    #endregion

    #region "Methods"
    private void fillform()
    {
        clsConfigPropertyType type = new clsConfigPropertyType();
        if (type.extractByID(typeID))
        {
            txtName.Text = type.name;

            var airconds = new clsConfigPropertyTypeAircond().getDataView();
            airconds.RowFilter = "type_id = '" + type.ID + "'";

            rptAirconds.DataSource = airconds;
            rptAirconds.DataBind();
        }
    }
    #endregion

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {

            int intAdmID = Session["ADMID"].ToInteger();

            clsConfigPropertyType type = new clsConfigPropertyType();
            type.name = txtName.Text;
            type.ID = mode == 2 ? typeID : -1;
            type.createdby = intAdmID;
            type.updatedby = intAdmID;
            type.airconds = JsonConvert.DeserializeObject<List<clsConfigPropertyTypeAircond>>(hdnAircondInfo.Value);

            string msg = "";
            bool Success = true;
            bool Updated = false;

            if (mode == 1)
            {
                if (type.add())
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();
                }
            }
            else if (mode == 2)
            {
                //if (!type.isSame())
                {
                    if (type.update())
                    {
                        Updated = true;
                        msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();
                    }
                    else
                    {
                        Success = false;
                    }
                }

                if (Success && !Updated)
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString();
                }
            }

            if (msg == "")
            {
                msg = GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString();
            }

            Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.TypeSaveSuccess('" + msg + "')", true);
        }
    }
}