﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Linq;
using System.Data;

public partial class adm_admSeoKeyword : System.Web.UI.Page
{
    #region "Properties"
    private DataTable dtSeoProduct;
    private DataTable dtSeoArea;
    #endregion


    #region "Property Methods"
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setPageProperties();
            bindSeoData();

            ViewState["dtSeoProduct"] = dtSeoProduct;
            ViewState["dtSeoArea"] = dtSeoArea;

            if(Session["success"] != null)
            {
                divAck.Visible = true;
                divAck.Attributes["class"] += " success";
            }
        }
        else
        {
            dtSeoProduct = ViewState["dtSeoProduct"] as DataTable;
            dtSeoArea = ViewState["dtSeoArea"] as DataTable;
        }
        Session["success"] = null;
       
    }

    protected void lnkbtnAddProduct_Click(object sender, EventArgs e)
    {
        DataRow rowNew = dtSeoProduct.NewRow();
        rowNew["type"] = "temp";
        rowNew["hidden"] = 0;
        rowNew["SEO_KEYWORD"] = txtProduct.Text;

        dtSeoProduct.Rows.Add(rowNew);
        bindSeoProduct(dtSeoProduct);

        txtProduct.Text = "";

    }

    protected void lnkbtnAddArea_Click(object sender, EventArgs e)
    {
        DataRow rowNew = dtSeoArea.NewRow();
        rowNew["type"] = "temp";
        rowNew["hidden"] = 0;
        rowNew["SEO_KEYWORD"] = txtArea.Text;

        dtSeoArea.Rows.Add(rowNew);
        bindSeoArea(dtSeoArea);

        txtArea.Text = "";

    }
    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        clsSeoKeyword seo = new clsSeoKeyword();

        string companyName = txtCompName.Text;
        string companyNameShort = txtCompNameShort.Text;
        List<clsSeoKeyword> products = new List<clsSeoKeyword>();
        foreach (RepeaterItem item in rptSeoProduct.Items)
        {
            HtmlInputHidden hdnIndex = (HtmlInputHidden)item.FindControl("hdnIndex");
            HtmlInputHidden hdnHidden= (HtmlInputHidden)item.FindControl("hdnHidden");
            Literal litKeyword = (Literal)item.FindControl("litKeyword");
            CheckBox chkboxImportant = (CheckBox)item.FindControl("chkboxImportant");

            if (hdnHidden.Value == "0")
            {
                products.Add(new clsSeoKeyword()
                {
                    group = "PRODUCT",
                    keyword = litKeyword.Text,
                    order = Convert.ToInt16(hdnIndex.Value),
                    createdBy = Convert.ToInt32(Session["ADMID"]),
                    important = chkboxImportant.Checked ? 1 : 0,

                });
            }
        }

        List<clsSeoKeyword> areas = new List<clsSeoKeyword>();
        foreach (RepeaterItem item in rptSeoArea.Items)
        {
            HtmlInputHidden hdnIndex = (HtmlInputHidden)item.FindControl("hdnIndex");
            HtmlInputHidden hdnHidden = (HtmlInputHidden)item.FindControl("hdnHidden");
            Literal litKeyword = (Literal)item.FindControl("litKeyword");
            CheckBox chkboxImportant = (CheckBox)item.FindControl("chkboxImportant");

            if (hdnHidden.Value == "0")
            {
                areas.Add(new clsSeoKeyword()
                {
                    group = "AREA",
                    keyword = litKeyword.Text,
                    order = Convert.ToInt16(hdnIndex.Value),
                    createdBy = Convert.ToInt32(Session["ADMID"]),
                    important = chkboxImportant.Checked ? 1 : 0,

                });
            }
        }
        seo.save(companyName, companyNameShort, products, areas, Convert.ToInt32(Session["ADMID"]));

        Dictionary<string, object> json = new Dictionary<string, object>();
        json["areas"] = areas;
        json["products"] = products;
        json["company"] = new Dictionary<string, string>()
        {
            { "name", companyName },
            { "code", companyNameShort }
        };

        Page.ClientScript.RegisterStartupScript(
            GetType(),
            "SeoGenerate",
            "parent.FormatPageDetail('"+Newtonsoft.Json.JsonConvert.SerializeObject(json)+ "');parent.$.fancybox.close();",
            true);
        //Session["success"] = 1;
        //Response.Redirect(clsMis.getCurrentPageName());

    }
    
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {

    }

    private void bindSeoData()
    {
        DataTable dtSeo = new clsSeoKeyword().getDataTable();
        if(dtSeo != null)
        {
            dtSeo.Columns.Add("type", typeof(string));
            dtSeo.Columns.Add(new DataColumn() {
                ColumnName = "hidden",
                DataType = typeof(int),
                DefaultValue = 0
            });

            for (int i = 0; i < dtSeo.Rows.Count; i++)
            {
                dtSeo.Rows[i]["type"] = "database";
                dtSeo.Rows[i]["hidden"] = 0;
            }

            dtSeoProduct = dtSeo.Clone();
            dtSeoArea = dtSeo.Clone();


            DataRow[] rowCompanyName = dtSeo.Select("SEO_GROUP = 'Company Name'");
            DataRow rowCompanyNameShow = dtSeo.Select("SEO_GROUP = 'Company Short Name'").FirstOrDefault();

            var rowProduct = dtSeo.Select("SEO_GROUP = 'PRODUCT'");
            if(rowProduct.Any()) { dtSeoProduct = rowProduct.CopyToDataTable(); }

            var rowArea = dtSeo.Select("SEO_GROUP = 'AREA'");
            if (rowArea.Any()) { dtSeoArea = rowArea.CopyToDataTable(); }

            // bind data
            txtCompName.Text = Convert.ToString(rowCompanyName[0]["SEO_KEYWORD"]);
            txtCompNameShort.Text = Convert.ToString(rowCompanyNameShow["SEO_KEYWORD"]);

            bindSeoProduct(dtSeoProduct);
            bindSeoArea(dtSeoArea);
        }
    }

    private void bindSeoProduct(DataTable dt)
    {
        if(rptSeoProduct.Items.Count > 0)
        {
            for(int i = 0; i < rptSeoProduct.Items.Count; i++)
            {
                HtmlInputHidden hdnHidden = (HtmlInputHidden)rptSeoProduct.Items[i].FindControl("hdnHidden");
                if(hdnHidden.Value == "1")
                {
                    dt.Rows[i]["hidden"] = 1;
                }
            }
        }


        DataView dv = new DataView(dt);
        dv.Sort = "SEO_ORDER";

        rptSeoProduct.DataSource = dv;
        rptSeoProduct.DataBind();
    }

    private void bindSeoArea(DataTable dt)
    {
        if (rptSeoArea.Items.Count > 0)
        {
            for (int i = 0; i < rptSeoArea.Items.Count; i++)
            {
                HtmlInputHidden hdnHidden = (HtmlInputHidden)rptSeoArea.Items[i].FindControl("hdnHidden");
                if (hdnHidden.Value == "1")
                {
                    dt.Rows[i]["hidden"] = 1;
                }
            }
        }

        DataView dv = new DataView(dt);
        dv.Sort = "SEO_ORDER";

        rptSeoArea.DataSource = dt;
        rptSeoArea.DataBind();
    }
    #endregion
}
