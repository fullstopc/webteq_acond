﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Linq;
using System.Data;

public partial class adm_admEnquiryField : System.Web.UI.Page
{
    #region "Properties"
    private DataTable dtEnqField;
    private DataTable dtEnqDropdownOptions;
    private int tempID = 1;

    protected bool BoxOpen = false;
    #endregion

    #region "Property Methods"
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setPageProperties();
            initEnqField();
            bindEnqField(dtEnqField);
            bindEnqGridster(dtEnqField);

            hdnParent.Value = tempID.ToString();
            ViewState["tempID"] = tempID;
            ViewState["dtEnqField"] = dtEnqField;
            ViewState["dtEnqDropdownOptions"] = dtEnqDropdownOptions;

            if (Session["success"] != null)
            {
                divAck.Visible = true;
                divAck.Attributes["class"] += " success";
            }
        }
        else
        {
            tempID = Convert.ToInt32(ViewState["tempID"]);
            dtEnqField = ViewState["dtEnqField"] as DataTable;
            dtEnqDropdownOptions = ViewState["dtEnqDropdownOptions"] as DataTable;
        }
        BoxOpen = false;
        Session["success"] = null;
       
    }
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        pnlOptionForm.Visible = ddlType.SelectedValue == "select";
        BoxOpen = true;
    }
    protected void lnkbtnAddField_Click(object sender, EventArgs e)
    {
        DataRow rowNew = dtEnqField.NewRow();
        rowNew["storage"] = "temp";
        rowNew["id"] = "T" + hdnParent.Value;
        rowNew["parent_id"] = 0;
        rowNew["index"] = dtEnqField.Rows.Count;

        rowNew["enqfield_parent"] = 0;
        rowNew["enqfield_active"] = 1;
        rowNew["enqfield_deletable"] = 1;
        rowNew["enqlabel_value"] = txtName.Text;
        rowNew["enqfield_order"] = dtEnqField.Rows.Count;
        rowNew["enqfield_required"] = ddlRequired.SelectedValue;
        rowNew["enqfield_validate"] = ddlValidation.SelectedValue;
        rowNew["enqfield_type"] = ddlType.SelectedValue;

        rowNew["enqfield_position_x"] = 99;
        rowNew["enqfield_position_y"] = 1;
        rowNew["enqfield_size_x"] = 12;
        rowNew["enqfield_size_y"] = 1;

        dtEnqField.Rows.Add(rowNew);
        bindEnqField(dtEnqField);
        bindEnqGridster(dtEnqField);

        // assign option
        if (ddlType.SelectedValue == "select")
        {
            foreach (DataRow row in dtEnqDropdownOptions.Rows)
            {
                row["index"] = dtEnqField.Rows.Count;
                dtEnqField.Rows.Add(row.ItemArray);
            }
            dtEnqDropdownOptions = dtEnqField.Clone();
        }


        ViewState["tempID"] = tempID += 1;
        resetField();
        resetOption();

        pnlOptionForm.Visible = false;
        rptOptions.DataSource = new string[0];
        rptOptions.DataBind();


        BoxOpen = true;
    }
    protected void lnkbtnCancelField_Click(object sender, EventArgs e)
    {

        txtName.Text = "";
        ddlType.SelectedValue ="input";
        ddlRequired.SelectedValue = "1";
        ddlValidation.SelectedValue = "";
        hdnFieldFormID.Value = "";

        lnkbtnAddField.Visible = true;
        lnkbtnCancelField.Visible = false;
        lnkbtnEditField.Visible = false;

        pnlOptionForm.Visible = false;
        rptOptions.DataSource = new string[0];
        rptOptions.DataBind();

        resetField();
        resetOption();

        BoxOpen = true;

    }
    protected void lnkbtnEditField_Click(object sender, EventArgs e)
    {
        string ID = hdnFieldFormID.Value;
        string storage = hdnFieldStorage.Value;

        DataRow[] rows = dtEnqField.Select("id = '" + ID + "' AND storage ='" + storage + "'");
        if(rows.Length == 1)
        {
            int index = Convert.ToInt16(rows[0]["index"]);
            dtEnqField.Rows[index]["edited"] = 1;
            dtEnqField.Rows[index]["enqlabel_value"] = txtName.Text;
            dtEnqField.Rows[index]["enqfield_order"] = dtEnqField.Rows.Count;
            dtEnqField.Rows[index]["enqfield_required"] = ddlRequired.SelectedValue;
            dtEnqField.Rows[index]["enqfield_validate"] = ddlValidation.SelectedValue;
            dtEnqField.Rows[index]["enqfield_type"] = ddlType.SelectedValue;

            // assign option
            if (ddlType.SelectedValue == "select")
            {
                foreach (DataRow row in dtEnqDropdownOptions.Rows)
                {
                    row["index"] = dtEnqField.Rows.Count;
                    dtEnqField.Rows.Add(row.ItemArray);
                }
                dtEnqDropdownOptions = dtEnqField.Clone();
            }
        }

        bindEnqField(dtEnqField);
        bindEnqGridster(dtEnqField);

        ViewState["tempID"] = tempID += 1;
        resetField();
        resetOption();

        pnlOptionForm.Visible = false;
        rptOptions.DataSource = new string[0];
        rptOptions.DataBind();

        BoxOpen = true;

    }

    protected void lnkbtnAddOption_Click(object sender, EventArgs e)
    {
        ViewState["tempID"] = tempID += 1;

        DataRow rowNew = dtEnqDropdownOptions.NewRow();
        rowNew["storage"] = "temp";
        rowNew["id"] = "T" + tempID;
        rowNew["parent_id"] = hdnParent.Value;

        
        rowNew["enqlabel_value"] = txtOptionText.Text;
        rowNew["enqattr_value"] = txtOptionValue.Text;
        rowNew["enqfield_parent"] = hdnParent.Value;
        rowNew["enqfield_required"] = -1;
        rowNew["enqfield_type"] = "option";

        dtEnqDropdownOptions.Rows.Add(rowNew);
        bindEnqDropdownOptions(dtEnqDropdownOptions, hdnParent.Value);
        resetOption();

        BoxOpen = true;
        
    }
    protected void lnkbtnEditOption_Click(object sender, EventArgs e)
    {
        string ID = hdnFieldOptionID.Value;
        string storage = hdnFieldOptionStorage.Value;

        DataRow[] rows = dtEnqField.Select("id = '" + ID + "' AND storage ='" + storage + "'");
        if (rows.Length == 1)
        {
            int index = Convert.ToInt16(rows[0]["index"]);
            dtEnqField.Rows[index]["edited"] = 1;
            dtEnqField.Rows[index]["enqlabel_value"] = txtOptionText.Text;
            dtEnqField.Rows[index]["enqattr_value"] = txtOptionValue.Text;

            DataRow[] rowsParent = dtEnqField.Select("id = '" + dtEnqField.Rows[index]["parent_id"] + "'");
            if(rowsParent.Length == 1)
            {
                int parentIndex = Convert.ToInt16(rowsParent[0]["index"]);
                dtEnqField.Rows[parentIndex]["edited"] = 1;
            }
        }
        bindEnqDropdownOptions(dtEnqField, hdnParent.Value);
        resetOption();
        BoxOpen = true;

    }
    protected void lnkbtnCancelOption_Click(object sender, EventArgs e)
    {

    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        clsEnquiryField enqField = new clsEnquiryField();
        List<clsEnquiryField> tempFields = new List<clsEnquiryField>();

        foreach (RepeaterItem item in rptEnqField.Items)
        {
            HtmlInputHidden hdnIndex = item.FindControl("hdnIndex") as HtmlInputHidden;
            HtmlInputHidden hdnHidden = item.FindControl("hdnHidden") as HtmlInputHidden;
            HtmlInputHidden hdnInfo = item.FindControl("hdnInfo") as HtmlInputHidden;
            CheckBox chkboxActive = item.FindControl("chkboxActive") as CheckBox;

            int index = Convert.ToInt16(hdnIndex.Value);
            Dictionary<string, string> info = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(hdnInfo.Value);
            DataRow row = dtEnqField.Rows[index];

            string type = row["enqfield_type"].ToString();
            int active = chkboxActive.Checked ? 1 : 0;
            int ID = Convert.ToInt32(row["id"].ToString().Replace("T", ""));
            bool hidden = hdnHidden.Value == "1";
            bool edited = Convert.ToInt16(row["edited"]) == 1;

            if (row["storage"].ToString() == "temp" && !hidden)
            {
                tempFields.Add(new clsEnquiryField()
                {
                    ID = ID,
                    labels = new List<clsEnquiryFieldLabel>() { new clsEnquiryFieldLabel() { value = row["enqlabel_value"].ToString(), fieldID = ID } },
                    type = row["enqfield_type"].ToString(),
                    parent = Convert.ToInt32(row["parent_id"].ToString().Replace("T", "")),
                    required = Convert.ToInt16(row["enqfield_required"]),
                    order = 99,
                    active = active,
                    posX = info["posX"],
                    posY = info["posY"],
                    sizeX = info["sizeX"],
                    sizeY = info["sizeY"],
                    attrs = null,
                    validate = Convert.ToString(row["enqfield_validate"]),
                    createdBy = Convert.ToInt16(Session["ADMID"])
                });

                if (type == "select")
                {
                    DataRow[] options = dtEnqField.Select("parent_id = '" + ID + "' AND storage = 'temp'");
                    foreach (DataRow option in options)
                    {
                        int optionID = Convert.ToInt32(option["id"].ToString().Replace("T", ""));

                        tempFields.Add(new clsEnquiryField()
                        {
                            ID = optionID,
                            labels = new List<clsEnquiryFieldLabel>() { new clsEnquiryFieldLabel() {
                                value = option["enqlabel_value"].ToString(),
                                fieldID = optionID
                            }},
                            type = option["enqfield_type"].ToString(),
                            parent = Convert.ToInt32(option["parent_id"].ToString().Replace("T", "")),
                            active = 1,
                            attrs = new List<clsEnquiryFieldAttr>() {
                            new clsEnquiryFieldAttr() {
                                value = option["enqattr_value"].ToString(),
                                name = "value",
                                fieldID = optionID
                            }},
                            createdBy = Convert.ToInt16(Session["ADMID"])
                        });
                    }
                }
            }
            else  if (row["storage"].ToString() == "database")
            {
                if (hidden)
                {
                    new clsEnquiryField().delete(Convert.ToInt32(row["enqfield_id"]));
                }
                else if(active != Convert.ToInt16(row["enqfield_active"]) ||
                        edited ||
                        info["posX"] != Convert.ToString(row["enqfield_position_x"]) || info["posY"] != Convert.ToString(row["enqfield_position_y"]) ||
                        info["sizeX"] != Convert.ToString(row["enqfield_size_x"]) || info["sizeY"] != Convert.ToString(row["enqfield_size_y"]))
                {
                    Dictionary<string, object> paramUpdate = new Dictionary<string, object>();
                    Dictionary<string, object> paramWhere = new Dictionary<string, object>() {
                        { "enqfield_id" , Convert.ToInt32(row["enqfield_id"]) }
                    };

                    if (active != Convert.ToInt16(row["enqfield_active"])) { paramUpdate["enqfield_active"] = active; }

                    if (info["posX"] != Convert.ToString(row["enqfield_position_x"])) { paramUpdate["enqfield_position_x"] = info["posX"]; }
                    if (info["posY"] != Convert.ToString(row["enqfield_position_y"])) { paramUpdate["enqfield_position_y"] = info["posY"]; }
                    if (info["sizeX"] != Convert.ToString(row["enqfield_size_x"])) { paramUpdate["enqfield_size_x"] = info["sizeX"]; }
                    if (info["sizeY"] != Convert.ToString(row["enqfield_size_y"])) { paramUpdate["enqfield_size_y"] = info["sizeY"]; }

                    if(type == "select")
                    {
                        DataRow[] options = dtEnqField.Select("parent_id = '" + ID + "'");
                        foreach(DataRow option in options)
                        {
                            bool optionEdited = Convert.ToInt16(option["edited"]) == 1;
                            bool optionTemp = Convert.ToString(option["storage"]) == "temp";
                            if (optionEdited)
                            {
                                Dictionary<string, object> paramOptionWhere = new Dictionary<string, object>() {
                                    { "enqfield_id" , Convert.ToInt32(option["enqfield_id"]) }
                                };

                                Dictionary<string, object> paramOptionAttrUpdate = new Dictionary<string, object>();
                                paramOptionAttrUpdate["enqattr_value"] = option["enqattr_value"].ToString();

                                Dictionary<string, object> paramOptionLabelUpdate = new Dictionary<string, object>();
                                paramOptionLabelUpdate["enqlabel_value"] = option["enqlabel_value"].ToString();

                                new clsEnquiryFieldLabel().update(paramOptionLabelUpdate, paramOptionWhere, Convert.ToInt16(Session["ADMID"]));
                                new clsEnquiryFieldAttr().update(paramOptionAttrUpdate, paramOptionWhere, Convert.ToInt16(Session["ADMID"]));
                            }
                            else if (optionTemp)
                            {
                                int optionID = Convert.ToInt32(option["id"].ToString().Replace("T", ""));
                                tempFields.Add(new clsEnquiryField()
                                {
                                    ID = 0,
                                    labels = new List<clsEnquiryFieldLabel>() { new clsEnquiryFieldLabel() {
                                        value = option["enqlabel_value"].ToString(),
                                        fieldID = optionID
                                    }},
                                    type = option["enqfield_type"].ToString(),
                                    parent = Convert.ToInt32(option["parent_id"].ToString().Replace("T", "")),
                                    active = 1,
                                    attrs = new List<clsEnquiryFieldAttr>() {
                                    new clsEnquiryFieldAttr() {
                                        value = option["enqattr_value"].ToString(),
                                        name = "value",
                                        fieldID = optionID
                                    }},
                                    createdBy = Convert.ToInt16(Session["ADMID"])
                                });
                            }
                        }
                    }

                    if (edited)
                    {
                        //paramUpdate["enqlabel_value"] = row["enqlabel_value"].ToString();
                        paramUpdate["enqfield_type"] = row["enqfield_type"].ToString();
                        paramUpdate["enqfield_required"] = row["enqfield_required"].ToString();
                        paramUpdate["enqfield_validate"] = row["enqfield_validate"].ToString();

                        Dictionary<string, object> paramLabelUpdate = new Dictionary<string, object>();
                        paramLabelUpdate["enqlabel_value"] = row["enqlabel_value"].ToString();

                        new clsEnquiryFieldLabel().update(paramLabelUpdate, paramWhere, Convert.ToInt16(Session["ADMID"]));
                    }

                    

                    new clsEnquiryField().update(paramUpdate, paramWhere, Convert.ToInt16(Session["ADMID"]));
                }
            }

        }

        
        if (tempFields.Count > 0)
        {
            new clsEnquiryField().addList(tempFields);
        }

        Session["success"] = 1;
        Response.Redirect(clsMis.getCurrentPageName());

    }
    #endregion


        #region "Methods"
    protected void setPageProperties()
    {
        ddlValidation.Items.Add(new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect2.Text").ToString(),""));
        ddlValidation.Items.Add(new ListItem("Phone Format", @"^\+?\d{2,5}\s?-?\s?\d{3,7}\s?\d{3,10}$"));
        ddlValidation.Items.Add(new ListItem("Email Format", @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"));

    }

    private void initEnqField()
    {
        DataTable dtOptions = new clsEnquiryField().getOptions();

        dtEnqField = new clsEnquiryField().getDataTable();
        if(dtEnqField != null)
        {
            dtEnqField.Columns.Add("storage", typeof(string));
            dtEnqField.Columns.Add("index", typeof(int));
            dtEnqField.Columns.Add("id", typeof(string));
            dtEnqField.Columns.Add("parent_id", typeof(string));
            dtEnqField.Columns.Add("enqattr_value", typeof(string));

            dtEnqField.Columns.Add(new DataColumn() { ColumnName = "edited", DataType = typeof(int), DefaultValue = 0 });
            dtEnqField.Columns.Add(new DataColumn() { ColumnName = "deleted", DataType = typeof(int), DefaultValue = 0 });

            for (int i = 0; i < dtEnqField.Rows.Count; i++)
            {
                dtEnqField.Rows[i]["storage"] = "database";
                dtEnqField.Rows[i]["id"] = dtEnqField.Rows[i]["enqfield_id"];
                dtEnqField.Rows[i]["parent_id"] = dtEnqField.Rows[i]["enqfield_parent"];
                dtEnqField.Rows[i]["index"] = i;

                if (dtEnqField.Rows[i]["enqfield_type"].ToString() == "option")
                {
                    dtEnqField.Rows[i]["enqattr_value"] = dtOptions.Select("enqfield_id = '" + dtEnqField.Rows[i]["enqfield_id"] +"'")[0]["enqattr_value"];
                }
            }
        }


        dtEnqDropdownOptions = dtEnqField.Clone();

    }

    private void bindEnqField(DataTable datatable)
    {
        DataView dv = new DataView(datatable);
        dv.RowFilter = "enqfield_parent = 0";

        rptEnqField.DataSource = dv;
        rptEnqField.DataBind();
    }

    private void bindEnqGridster(DataTable datatable)
    {
        DataView dv = new DataView(datatable);
        dv.RowFilter = "enqfield_parent = 0 AND enqfield_active = 1";
        dv.Sort = "enqfield_position_x asc";

        rptGridster.DataSource = dv;
        rptGridster.DataBind();
    }

    private void bindEnqDropdownOptions(DataTable dtOptions, string parentID)
    {

        DataView dv = new DataView(dtOptions);
        dv.RowFilter = "parent_id  = '"+ parentID + "' AND deleted = 0";

        rptOptions.DataSource = dv;
        rptOptions.DataBind();
    }
    
    private void resetField()
    {
        txtName.Text = "";
        ddlValidation.SelectedIndex = 0;
        ddlRequired.SelectedIndex = 0;
        ddlType.SelectedIndex = 0;
        hdnParent.Value = tempID.ToString();
    }

    private void resetOption()
    {
        txtOptionText.Text = "";
        txtOptionValue.Text = "";
    }
    #endregion

    protected void rptEnqField_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if(e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            string ID = DataBinder.Eval(e.Item.DataItem, "ID").ToString();

            DataView dv = new DataView(dtEnqField);
            dv.RowFilter = "parent_id = '" + ID + "'";
            if (dv.Count > 0)
            {
                Repeater rptFieldChilds = e.Item.FindControl("rptFieldChilds") as Repeater;
                rptFieldChilds.DataSource = dv;
                rptFieldChilds.DataBind();
            }
        }
    }
    
    protected void rptEnqField_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdEdit")
        {
            string[] arguments  = e.CommandArgument.ToString().Split('|');
            string ID = arguments[0];
            string storage = arguments[1];

            DataView dvField = new DataView(dtEnqField);
            dvField.RowFilter = "id = '" + ID + "' AND storage = '"+ storage + "'";

            if(dvField.Count == 1)
            {
                DataRowView row = dvField[0];

                txtName.Text = row["enqlabel_value"].ToString();
                ddlType.SelectedValue = row["enqfield_type"].ToString();
                ddlRequired.SelectedValue = row["enqfield_required"].ToString();
                ddlValidation.SelectedValue = row["enqfield_validate"].ToString();

                hdnParent.Value = ID;
                hdnFieldFormID.Value = ID;
                hdnFieldStorage.Value = storage;

                lnkbtnEditField.Visible = true;
                lnkbtnCancelField.Visible = true;
                lnkbtnAddField.Visible = false;

                BoxOpen = true;

                // for Dropdown Options
                pnlOptionForm.Visible = ddlType.SelectedValue == "select";
                bindEnqDropdownOptions(dtEnqField, ID);
            }
        }
    }

    protected void rptOptions_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string[] arguments = e.CommandArgument.ToString().Split('|');
        string ID = arguments[0];
        string parentID = arguments[1];
        string storage = arguments[2];

        DataRow[] rows = dtEnqField.Select("id = '" + ID + "' AND storage = '" + storage + "'");

        if (e.CommandName == "cmdDelete")
        {
            if(rows.Length == 1)
            {
                int index = Convert.ToInt16(rows[0]["index"]);
                if (storage == "temp")
                {
                    dtEnqField.Rows[index].Delete();
                }
                else
                {
                    dtEnqField.Rows[index]["deleted"] = 1;
                }
            }

            bindEnqDropdownOptions(dtEnqField, parentID);
            
        }
        else if(e.CommandName == "cmdEdit")
        {
            if(rows.Length == 1)
            {
                DataRow row = rows[0];
                txtOptionText.Text = row["enqlabel_value"].ToString();
                txtOptionValue.Text = row["enqattr_value"].ToString();

                hdnFieldOptionID.Value = ID;
                hdnFieldOptionStorage.Value = storage;
                hdnParent.Value = parentID;

                lnkbtnAddOption.Visible = false;
                lnkbtnEditOption.Visible = true;
                lnkbtnCancelOption.Visible = true;
            }
            
        }

        BoxOpen = true;
    }
}
