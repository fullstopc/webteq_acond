﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.IO;

public partial class adm_admDate01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 100;
    protected int _dateId = 0;
    protected int _mode = 1;
    protected string _gvSortExpression = "TD_STATUS ASC, TD_DATETIME ASC";
    protected string _gvSortDirection = clsAdmin.CONSTSORTASC;
    protected string _gvSort = "";
    #endregion


    #region "Property Methods"
    protected string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    protected int dateId
    {
        get
        {
            if (ViewState["DATEID"] == null)
            {
                return _dateId;
            }
            else
            {
                return Convert.ToInt16(ViewState["DATEID"]);
            }
        }
        set { ViewState["DATEID"] = value; }
    }

    protected int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    protected string gvSortExpression
    {
        get { return ViewState["GVSORTEXPRESSION"] as string ?? _gvSortExpression; }
        set { ViewState["GVSORTEXPRESSION"] = value; }
    }

    protected string gvSortDirection
    {
        get { return ViewState["GVSORTDIRECTION"] as string ?? _gvSortDirection; }
        set { ViewState["GVSORTDIRECTION"] = value; }
    }

    protected string gvSort
    {
        get { return ViewState["GVSORT"] as string ?? _gvSort; }
        set { ViewState["GVSORT"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        registerScript();

        if (!IsPostBack)
        {
            Master.sectId = sectId;

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;
                if (int.TryParse(Request["id"], out intTryParse))
                {
                    dateId = intTryParse;
                }
                else
                {
                    dateId = int.MaxValue;
                }
            }

            if(!string.IsNullOrEmpty(Request["id"]))
            {
                mode = 2;
            }

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMELISTINGSIZE, clsConfig.CONSTGROUPOTHERSETTINGS, 1);

            gvDate.PagerSettings.FirstPageText = GetGlobalResourceObject("GlobalResource", "contentAdmFirstPageText.Text").ToString();
            gvDate.PagerSettings.LastPageText = GetGlobalResourceObject("GlobalResource", "contentAdmLastPageText.Text").ToString();
            gvDate.PageSize = (!string.IsNullOrEmpty(config.value)) ? Convert.ToInt16(config.value) : clsAdmin.CONSTADMPAGESIZE;
            //gvDate.PageSize = clsAdmin.CONSTADMPAGESIZE;

            gvSort = gvSortExpression + " " + gvSortDirection;
            bindGV(gvDate, "TD_STATUS ASC, TD_DATETIME ASC");
            setPageProperties();
            
        }

        Session["NEWDATEID"] = null;
        Session["EDITEDDATEID"] = null;
        Session["NOCHANGE"] = null;
        Session["DELETEDDATEID"] = null;
        Session["DATELIST"] = null;
    }

    protected void validateDate_server(object source, ServerValidateEventArgs args)
    {
        clsTraining training = new clsTraining();

        string strTrainingDateTime;

        strTrainingDateTime = txtDate.Text.Trim();
        strTrainingDateTime += " " + txtTime.Text.Trim();

        DateTime dtTrainingDate = Convert.ToDateTime(strTrainingDateTime);

        if (training.isTrainingDateTimeExist(dtTrainingDate, dateId))
        {
            args.IsValid = false;
            cvDate.ErrorMessage = "<br />This date and time is in use.";
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cvDate_PreRender(object source, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("pagetitlefriendlyurl")))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtDate.ClientID + "', document.all['" + cvDate.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "pagetitlefriendlyurl", strJS);
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strTrainingDateTime;

            strTrainingDateTime = txtDate.Text.Trim();
            strTrainingDateTime += " " + txtTime.Text.Trim();

            DateTime dtTrainingDate = Convert.ToDateTime(strTrainingDateTime);

            int intStatus = 1; //Available

            clsTraining training = new clsTraining();
            int intRecordAffected = 0;

            if (mode == 1)
            {
                intRecordAffected = training.addTrainingDate(dtTrainingDate, intStatus, Convert.ToInt16(Session["ADMID"]));

                if (intRecordAffected == 1)
                {
                    Session["NEWDATEID"] = training.trainingDateId;

                    Response.Redirect(currentPageName);
                }
                else
                {
                    pnlAck.Visible = true;
                    litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new date. Please try again.</div>";
                }
            }
            else if (mode == 2)
            {
                Boolean boolSuccess = true;
                Boolean boolEdited = false;

                if (!training.isExactSameDate(dateId, dtTrainingDate))
                {
                    intRecordAffected = training.updateTrainingDateById(dateId, dtTrainingDate, Convert.ToInt16(Session["ADMID"]));

                    if (intRecordAffected == 1)
                    {
                        boolEdited = true;
                    }
                    else
                    {
                        boolSuccess = false;
                    }
                }

                if (boolEdited)
                {
                    Session["EDITEDDATEID"] = training.trainingDateId;

                    Response.Redirect(currentPageName);   
                }
                else
                {
                    if (!boolSuccess)
                    {
                        pnlAck.Visible = true;
                        litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit date. Please try again.</div>";
                    }
                    else
                    {
                        Session["EDITEDDATEID"] = training.trainingDateId;
                        Session["NOCHANGE"] = 1;

                        Response.Redirect(currentPageName);
                    }
                }
            }
        }
    }

    protected void gvDate_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell tc in e.Row.Cells)
            {
                if (tc.HasControls())
                {
                    LinkButton lnkbtn = (LinkButton)tc.Controls[0];

                    if (lnkbtn != null && gvSortExpression == lnkbtn.CommandArgument)
                    {
                        string strClass = (gvSortDirection == "DESC") ? "desc" : "asc";
                        tc.CssClass = strClass;
                    }
                }
            }
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkbtnEdit = (LinkButton)e.Row.FindControl("lnkbtnEdit");
            LinkButton lnkbtnDelete = (LinkButton)e.Row.FindControl("lnkbtnDelete");

            int trainingdateid = Convert.ToInt16(gvDate.DataKeys[e.Row.RowIndex].Value.ToString());
            clsTraining training = new clsTraining();
            training.extractTrainingDateById(trainingdateid);

            lnkbtnEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            //lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();

            if (training.trainingStatus == 2) //Occupied
            {
                lnkbtnDelete.Enabled = false;
                lnkbtnDelete.CssClass = "lnkbtn linkDeleteDisabled";
            }
            else
            {
                lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteTrainingDate.Text").ToString() + "');";
            }
        }
    }

    protected void gvDate_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "cmdEdit")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int dateid = Convert.ToInt16(gvDate.DataKeys[rowIndex].Value.ToString());
            Response.Redirect(currentPageName + "?id=" + dateid);
        }
        else if (e.CommandName == "cmdDelete")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int dateId = Convert.ToInt16(gvDate.DataKeys[rowIndex].Value.ToString());
            int intDeletedDate = 0;

            clsMis.performDeleteTrainingDate(dateId, ref intDeletedDate);

            Session["DELETEDDATEID"] = intDeletedDate;
            Session["DATELIST"] = 1;

            Response.Redirect(currentPageName);
        }
    }

    protected void gvDate_Sorting(object sender, GridViewSortEventArgs e)
    {
        string strSortExpDir = "";
        if (e.SortExpression != gvSortExpression)
        {
            gvSortExpression = e.SortExpression;
            gvSortDirection = clsAdmin.CONSTSORTASC;
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }
        else
        {
            switch (gvSortDirection)
            {
                case clsAdmin.CONSTSORTASC:
                    gvSortDirection = clsAdmin.CONSTSORTDESC;
                    break;
                case clsAdmin.CONSTSORTDESC:
                    gvSortDirection = clsAdmin.CONSTSORTASC;
                    break;
            }
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }

        gvSort = strSortExpDir;
        bindGV(gvDate, strSortExpDir);
    }

    protected void gvDate_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDate.PageIndex = e.NewPageIndex;
        bindGV(gvDate, gvSort);
    }

    public override void VerifyRenderingInServerForm(Control control)
    {

    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        litPageTitle.Text = GetLocalResourceObject("litPageTitle.Text").ToString();

        if (mode == 2)
        {
            if (Session["EDITEDDATEID"] == null && Session["DELETEDDATEID"] == null) { fillForm(); }
        }

        if (Session["NEWDATEID"] != null && Session["NEWDATEID"] != "")
        {
            pnlAck.Visible = true;
            litAck.Text = "<div class=\"noticemsg\">New date has been added successfully.</div>";

            clsMis.resetListing();
        }
        else if (Session["EDITEDDATEID"] != null && Session["EDITEDDATEID"] != "")
        {
            if (Session["NOCHANGE"] != null)
            {
                litAck.Text = "<div class=\"noticemsg\">No changes detected.</div>";
            }
            else
            {
                litAck.Text = "<div class=\"noticemsg\">Date has been edited successfully.</div>";

                clsMis.resetListing();
            }

            pnlAck.Visible = true;
        }
        else if (Session["DELETEDDATEID"] != null && Session["DELETEDDATEID"] != "")
        {
            string strAck = "Date has been deleted successfully.";

            strAck = "<div class=\"noticemsg\">" + strAck + "</div>";

            if (Session["DATELIST"] != null && Session["DATELIST"] != "")
            {
                litAck.Text = strAck;
                pnlAck.Visible = true;
            }
            else
            {
                litAck.Text = strAck;
                pnlAck.Visible = true;
            }

            clsMis.resetListing();
        }
    }

    protected void fillForm()
    {
        clsTraining training = new clsTraining();
        if (training.extractTrainingDateById(dateId))
        {
            txtDate.Text = training.trainingDateTime.ToString("dd MMM yyyy");
            txtTime.Text = training.trainingDateTime.ToString("hh:mm tt");
        }
        else
        {
            pnlAck.Visible = true;
            litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
        }
    }

    protected void bindGV(GridView gvRef, string gvSortExpDir)
    {
        clsTraining training = new clsTraining();
        DataSet ds = new DataSet();
        DataView dv = new DataView();
        ds = training.getTraingDateList();

        dv = new DataView(ds.Tables[0]);

        if (gvSortExpDir != "")
        {
            dv.Sort = gvSortExpDir;
        }

        //dv.Sort = "TD_STATUS ASC, TD_DATETIME ASC";

        Session["DATEDV"] = dv;

        gvRef.DataSource = dv;
        gvRef.DataBind();

        litDateFound.Text = dv.Count + " record(s) listed.";
    }

    protected void registerScript()
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("showDate"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "  initSect3();";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "showDate", strJS);
        }
    }
    #endregion
}