﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admMember01.aspx.cs" Inherits="adm_admMember01" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
	<div class="main-content__header">
		<div class="title"><asp:Literal ID="litPageTitle" runat="server"></asp:Literal></div>
		<div class="divListingSplitter"></div>
		<div class="action">
			<asp:Panel ID="pnlListingAction" runat="server" CssClass="divListingAction">
				<asp:LinkButton ID="lnkbtnExport" runat="server" Text="Export" ToolTip="Export" CssClass="hypListingAction export" Visible="false"></asp:LinkButton>
				<a href="admMember0101.aspx" class="hypListingAction add">Add Resident</a>
			</asp:Panel>
		</div>
		<div class="divListingSplitter"></div>
		<div class="filter">
			<div class="divFilterContainer">
				<div class="divFilter">
					<asp:TextBox ID="txtKeyword" runat="server" placeholder="Keyword Search" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
					<a href="#" class="btnDropdown" onclick="toggleFilterOption();"></a>
					<asp:Literal ID="litKeywordRule" runat="server" meta:resourcekey="litKeywordRule" visible="false"></asp:Literal>
				</div>
				<div class="divFilterMore">
					<div></div>
					<div>
						<table class="tblSearch">
	
						</table>

					</div>
				</div>
			</div>
			<div class="divFilterAction"><%--<asp:Button ID="lnkbtnSearch" runat="server" Text="" ToolTip="Search" CssClass="btn btnSearch" OnClick="lnkbtnSearch_Click"/>--%></div>
		</div>
	</div>
	<div class="divListing">
		<asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
			<div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
		</asp:Panel>
		<uc:CmnDatatable ID="ucCmnDatatable" runat="server" />
		<asp:HiddenField runat="server" ID="hdnItemID" />
        <table id="tblProperty"></table>
		<asp:LinkButton ID="lnkbtnDelete" runat="server" OnClick="lnkbtnDelete_Click" style="display:none;"></asp:LinkButton>
	</div>
	<script type="text/javascript">
		$(function () {
			var columns = [
					{ "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                    { "data": "mem_name", "label": "Name", "order": 1, },
                    { "data": "mem_contact_tel", "label": "Contact No.", "order": 2, render: function (data, type, full, meta) { return full.mem_tel_prefix + "-" + data; },},
					{ "data": "mem_email", "label": "Email", "order": 3, },
                    { "data": "mem_login_username", "label": "Login ID", "order": 4, },
                    { "data": "mem_property", "label": "P", "order": 5, },
                    { "data": "mem_active", "label": "Active", "order": 6, "convert": "active" },
                    { "data": "mem_login_timestamp", "label": "Last Login", "order": 7, "convert": "datetime", "format": "d MMM YYYY h:MM A" },
					{ "data": "action", "bSortable": false, "bSearchable": false, "label": "Action", "order": 8 },
			];
			var columnSort = columns.reduce(function (prev, curr) {
				prev[curr.data] = curr.order;
				return prev;
			}, {});

            var columnDefs = [{
                "render": function (data, type, row) {
                    var lnkbtnEdit = "<a class='lnkbtn lnkbtnEdit' href='admMember0101.aspx?id=" + row.mem_id + "' title='Edit'></a>";
                    var lnkbtnDelete = "<a class='lnkbtn lnkbtnDelete' title='Delete' data-id='" + row.mem_id + "'></a>";
                    return lnkbtnEdit + lnkbtnDelete;
                },
                "targets": columnSort["action"]
            },
            {
                "render": function (data, type, row) {
                    var memberID = row.mem_id;
                    var json = JSON.parse(data) || [];
                    var length = json.length;
                    var lnkbtn = "<a class='popup' href='admResidentProperty.aspx?id=" + memberID + "&frm=2'>" + length + "</a>";

                    return lnkbtn;
                },
                "targets": columnSort["mem_property"]
            },
            {
                "render": function (data, type, row) {
                    var lnkbtn = "<a href='admMember0101.aspx?id=" + row.mem_id + "' title='Edit'>" + data + "</a>";
                    return lnkbtn;
                },
                "targets": columnSort["mem_name"]
            }];
            var properties = {
				columns: columns,
				columnDefs: columnDefs,
				columnSort: columnSort,
				func: "getMemberData",
                name: "member",
                elementID: "tblProperty",
                callback: function () {
                    $('#tblProperty .popup').magnificPopup({
                        type: 'iframe',
                        mainClass: 'timeslot-frame',
                        iframe: {
                            markup: `
                                <div class="mfp-iframe-scaler">
                                    <div class="mfp-close"></div>
                                    <iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>
                                </div>`
                        }
                    });
                }
			};
			var $datatable = callDataTables(properties);


			 $(document).on("click", ".lnkbtnDelete", function () {
				if (confirm("Are you sure you want to delete this member?")) {
					var itemID = $(this).attr("data-id");
					document.getElementById('<%= hdnItemID.ClientID %>').value = itemID;
					document.getElementById('<%= lnkbtnDelete.ClientID %>').click();
				}
			 })

			$("#<%= lnkbtnExport.ClientID %>").click(function (e) {
				var $table = $('.dataTable').clone();
				$table.attr("border", "1").find("th:last-child,td:last-child").remove();
				window.open('data:application/vnd.ms-excel,' + encodeURIComponent($table.get(0).outerHTML));

				e.preventDefault();
			});
        });
    </script>
</asp:Content>