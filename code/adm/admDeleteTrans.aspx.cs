﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class adm_admDeleteTrans : System.Web.UI.Page
{
    #region "Properties"
    protected int _transId = 0;
    protected int _type = 1;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int transId
    {
        get
        {
            if (ViewState["TRANSID"] == null)
            {
                return _transId;
            }
            else
            {
                return Convert.ToInt16(ViewState["TRANSID"]);
            }
        }
        set { ViewState["TRANSID"] = value; }
    }

    protected int type
    {
        get
        {
            if (ViewState["TYPE"] == null)
            {
                return _type;
            }
            else
            {
                return Convert.ToInt16(ViewState["TYPE"]);
            }
        }
        set { ViewState["TYPE"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    transId = Convert.ToInt16(Request["id"]);
                }
                catch (Exception ex)
                {
                    transId = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["type"]))
            {
                try
                {
                    type = Convert.ToInt16(Request["type"]);
                }
                catch (Exception ex)
                {
                    type = 0;
                }
            }
        }

        setPageProperties();
    }

    protected void lnkbtnConfirm_Click(object sender, EventArgs e)
    {
        clsUser usr = new clsUser();

        string strEmail = txtEmail.Text.Trim();

        string password = txtPassword.Text.Trim();
        clsMD5 md5 = new clsMD5();
        string strPassword = md5.encrypt(password);

        //@ set flag to use back connection string for admin
        Session[clsAdmin.CONSTADMINCS] = 1;
        if (usr.isUserTypeValid(strEmail, strPassword, 1))
        {
            Session[clsAdmin.CONSTADMINCS] = null;
            clsOrder ord = new clsOrder();
            ord.extractOrderById(transId);

            string strDeletedName = "";

            clsMis.performDeleteTrans(transId, ref strDeletedName);

            if(!string.IsNullOrEmpty(strDeletedName)) { Session["DELETEDTRANSCODE"] = strDeletedName; }
            else { Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text").ToString() + "<br />Failed to delete order (" + ord.ordNo + ").Please try again.</div>"; }

            if (Session["DELETEDTRANSCODE"] != null)
            {
                litAck.Text = "Transaction (" + Session["DELETEDTRANSCODE"] + ") has been deleted successfully.";
                litAck.Text = "<div class=\"noticemsg\">" + litAck.Text + "</div>";

                pnlAck.Visible = true;
                lnkbtnBack.Visible = false;
                pnlForm.Visible = false;

                clsMis.resetListing();
            }
            else if (Session["ERRMSG"] != null)
            {
                litAck.Text = Session["ERRMSG"].ToString();

                pnlAck.Visible = true;
                lnkbtnContinue.Visible = false;
                pnlForm.Visible = false;
            }

            Session["DELETEDTRANSCODE"] = null;
            Session["ERRMSG"] = null;
        }
        else
        {
            litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmInvalid.Text").ToString();

            pnlAck.Visible = true;
            lnkbtnContinue.Visible = false;
            pnlForm.Visible = false;
        }
    }

    protected void lnkbtnContinue_Click(object sender, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("REFRESHPARENT"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "tb_remove();";
            strJS += "parent.location.href='" + ConfigurationManager.AppSettings["scriptBase"] + "adm/admOrder01.aspx" + "';";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "REFRESHPARENT", strJS, false);
        }
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        lnkbtnCancel.OnClientClick = "javascript:self.parent.tb_remove(); return false;";
        lnkbtnConfirm.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteTrans.Text").ToString() + "'); return false;";

        if (!IsPostBack)
        {
            lnkbtnBack.OnClientClick = "javascript:document.location.href='" + Request.Url.ToString() + "'; return false;";
        }
    }
    #endregion
}
