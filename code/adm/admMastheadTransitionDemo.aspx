﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admMastheadTransitionDemo.aspx.cs" Inherits="adm_admMastheadTransitionDemo" %>
<%@ MasterType VirtualPath="~/mst/blank.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jquery.1.7.2.min.js" type="text/javascript"></script>

<% if (fullscreen.Visible) { %>


<link type="text/css" href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>supersized.css" rel="Stylesheet" />
<script type="text/javascript">
    jQuery(function($) {
        $.supersized({
            // Functionality
            slideshow: 1,		            // Slideshow on/off
            start_slide: 1, 		        // Start slide (0 is random)
            stop_loop: 0,                 // Pauses slideshow on last slide
            new_window: 1, 		        // Image links open in new window/tab
            pause_hover: 0, 		        // Pause slideshow on hover
            keyboard_nav: 1, 		        // Keyboard navigation on/off
            performance: 2, 		        // 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
            image_protect: 1, 		    // Disables image dragging and right click with Javascript
            // Size & Position						   
            min_width: 0 ,    		    // Min width allowed (in pixels)
            min_height: 0,    		    // Min height allowed (in pixels)
            vertical_center: 0, 		    // Vertically center background
            horizontal_center: 1, 	    // Horizontally center background
            fit_always: 0, 		        // Image will never exceed browser width or height (Ignores min. dimensions)
            fit_portrait: 1, 		        // Portrait images will not exceed browser height
            fit_landscape: 0, 		    // Landscape images will not exceed browser width
            // Components							
            slide_links: 'blank',         // Individual links for each slide (Options: false, 'number', 'name', 'blank')
            thumb_links: 1, 		        // Individual thumb links for each slide
            thumbnail_navigation: 0,      // Thumbnail navigation
                    
            <%= supersizedOption%>
            progress_bar: 1, 		        // Timer for each slide							
            mouse_scrub: 1
        });
        $("#supersized").addClass("sliderSize");
    });
<%}%>
</script>
<div class="container" style="">
    <div id="transDefault" runat="server" visible="false">
        <asp:Panel ID="pnlMastheadSlideShow" runat="server" CssClass="divSlideShowMasthead">
            <asp:Repeater ID="rptMasthead" runat="server" OnItemDataBound="rptMasthead_ItemDataBound">
                <ItemTemplate>
                    <div class="divMastheadTaglineOuter">
                        <div class="divMastheadTagline" id="divMastheadTagline" runat="server" visible="false">
                            <asp:Literal ID="litMastTagline1" runat="server"></asp:Literal>
                            <asp:Literal ID="litMastTagline2" runat="server"></asp:Literal>
                        </div>
                        <asp:Image ID="imgMasthead" runat="server" BorderWidth="0" CssClass="sliderSize"/>
                        <asp:HyperLink ID="hypMasthead" runat="server" Visible="false"><asp:Image ID="imgMastheadImage" runat="server" BorderWidth="0" /></asp:HyperLink>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </asp:Panel>
        <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>cycle2/jquery.cycle2.js" type="text/javascript"></script>
        <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>cycle2/jquery.cycle2.flip.min.js" type="text/javascript"></script>
        <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>cycle2/jquery.cycle2.carousel.min.js" type="text/javascript"></script>
        <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>cycle2/jquery.cycle2.scrollVert.min.js" type="text/javascript"></script>
        <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>cycle2/jquery.cycle2.shuffle.min.js" type="text/javascript"></script>
        <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>cycle2/jquery.cycle2.tile.min.js" type="text/javascript"></script>
    </div>

    <div id="fullscreen" runat="server" visible="false">
        
    </div>
    <div id="mobile" runat="server" visible="false"  style="">
        <link href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>mobile/responsiveslides.css" rel="Stylesheet" />
        <asp:Panel ID="pnlSliderContainer" runat="server" CssClass="divSliderContainer" style="">
            <!-- Slides Container -->
            <asp:Panel ID="pnlMastheadSlider" u="slides" runat="server" CssClass="divMastheadSlider">
                <asp:Repeater ID="rptHomeMasthead" runat="server" OnItemDataBound="rptHomeMasthead_ItemDataBound">
                    <ItemTemplate>
                        <div class="sliderSize">
                            <asp:HyperLink ID="hypMasthead" runat="server" Enabled="false">
                                <asp:Panel ID="pnlImgMasthead" runat="server" CssClass="divImgMasthead">
                                    <asp:Image ID="imgMasthead" runat="server" BorderWidth="0" />
                                </asp:Panel>
                                <asp:Panel ID="pnltagline" runat="server" CssClass="divtagline" >
                                    <asp:Panel ID="pnlLittag1" runat="server" CssClass="divLittag1">
                                        <asp:Literal ID="Littag1" runat="server"></asp:Literal>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlLittag2" runat="server" CssClass="divLittag2">
                                        <asp:Literal ID="Littag2" runat="server"></asp:Literal>
                                    </asp:Panel>    
                                </asp:Panel>
                            </asp:HyperLink>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <!-- bullet navigator container -->
                <div u="navigator" class="jssorb21 divNavigator">
                    <!-- bullet navigator item prototype -->
                    <div u="prototype" class="divPrototype"></div>
                </div>
                <!-- End bullet navigator container -->
                <!-- Arrow navigator container -->
                <!-- Arrow Left -->
                <span u="arrowleft" class="jssora21l spanArrowLeft"></span>
                <!-- Arrow Right -->
                <span u="arrowright" class="jssora21r spanArrowRight"></span>
                <!-- End arrow navigator container -->
            </asp:Panel>
        </asp:Panel>
        
        <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jssor.js" type="text/javascript"></script>
        <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jssor.slider.js" type="text/javascript"></script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {

                var options = {
                    $FillMode: 2,                                       //[Optional] The way to fill image in slide, 0 stretch, 1 contain (keep aspect ratio and put all inside slide), 2 cover (keep aspect ratio and cover whole slide), 4 actual size, 5 contain for large image, actual size for small image, default value is 0
                    $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                    $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                    $PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                    $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                    $SlideEasing: $JssorEasing$.$EaseOutQuint,          //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                    $SlideDuration: 300,                               //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                    $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                    //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                    //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                    $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                    $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                    $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                    $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                    $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                    $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                    $BulletNavigatorOptions: {                          //[Optional] Options to specify and enable navigator or not
                        $Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
                        $ChanceToShow: 0,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                        $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                        $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                        $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                        $SpacingX: 8,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                        $SpacingY: 8,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                        $Orientation: 1,                                //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                        $Scale: false,                                  //Scales bullets navigator or not while slider scale
                    },

                    $ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
                        $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                        $ChanceToShow: 0,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                        $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                        $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                    },
                    <% if (!string.IsNullOrEmpty(jssorOption)){%>
                    $SlideshowOptions: {
                        $Class: $JssorSlideshowRunner$,
                        $Transitions: [<%= jssorOption%>]
                    }
                    <% } %>
                };

                var jssor_slider1 = new $JssorSlider$("<%= pnlSliderContainer.ClientID %>", options);
                function ScaleSlider() {
                    var bodyWidth = document.body.clientWidth;
                    if (bodyWidth)
                        jssor_slider1.$ScaleWidth(Math.min(bodyWidth, 1920));
                    else
                        window.setTimeout(ScaleSlider, 30);
                }
                ScaleSlider();

                $(window).bind("load", ScaleSlider);
                $(window).bind("resize", ScaleSlider);
                $(window).bind("orientationchange", ScaleSlider);
            });
        </script>
    </div>
</div>
</asp:Content>

