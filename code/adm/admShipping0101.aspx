﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admShipping0101.aspx.cs" Inherits="adm_admShipping0101" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmShipping.ascx" TagName="AdmShipping" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmShippingWithWeight.ascx" TagName="AdmShippingWithWeight" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmShippingCompany.ascx" TagName="AdmShippingCompany" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
        <div class="action">
            <a href="#" class="hypListingAction setting" onclick="toggleSideMenu();">Page Detail</a>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlForm" runat="server" CssClass="divForm">
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <asp:Panel ID="pnlShippingDetails" runat="server">
            <uc:AdmShipping ID="ucAdmShipping" runat="server" pageListingURL="admShipping01.aspx" />
        </asp:Panel>
        <asp:Panel ID="pnlShippingDetails2" runat="server" Visible="false">
            <uc:AdmShippingCompany ID="ucAdmShippingCompany" runat="server" pageListingURL="admShipping01.aspx" />
        </asp:Panel>
        <asp:Panel ID="pnlShippingWithWeightContainer" runat="server" Visible="false">
            <uc:AdmShippingWithWeight ID="ucAdmShippingWithWeight" runat="server" pageListingURL="admShipping01.aspx" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>