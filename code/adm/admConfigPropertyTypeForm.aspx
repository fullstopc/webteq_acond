﻿<%@ Page Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admConfigPropertyTypeForm.aspx.cs" Inherits="adm_adm"  ValidateRequest="false" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/mst/blank.Master" %>
<asp:Content ID="Content" ContentPlaceHolderID="cphHead" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <br />
    <div class="container-fluid">
        <div class="row">
            <label class="col-sm-1"><b>Room Type:</b></label>
            <div class="col-sm-3">
                <div class="form-group">
                    <asp:TextBox runat="server" ID="txtName" CssClass="form-control"></asp:TextBox>
               
                
                <asp:RequiredFieldValidator ID="rvName" 
                 ControlToValidate="txtName"
                 Display="Dynamic"
                 CssClass="errmsg"
                 ErrorMessage="Please enter Name."
                 ValidationGroup="vg"
                 runat="server"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-1"><b>Room Airconds:</b></label>
            <div class="col-sm-3">
                <div class="panel panel-aircond">
                    <div class="panel-heading"></div>
                </div>
                    <table id="tblAircond" class="dataTable" role="grid">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>Charge</th>
                                <th></th>
                            </tr>
                            <tr id="trAircondForm">
                                <td></td>
                                <td><asp:TextBox runat="server" ID="txtAircondName" CssClass="form-control"></asp:TextBox></td>
                                <td><asp:TextBox runat="server" ID="txtAircondCharge" CssClass="form-control"></asp:TextBox></td>
                                <td>
                                    <a class="btn btn-primary btnAddAircond">Add</a>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater runat="server" ID="rptAirconds">
                                <ItemTemplate>

                                <tr>
                                    <td ><%# Container.ItemIndex + 1 %></td>
                                    <td data-id="name"><%# Eval("aircond_name") %></td>
                                    <td data-id="charge"><%# Eval("aircond_charge") %></td>
                                    <td>
                                        <a class="lnkbtn lnkbtnDelete lnkbtnDeleteAircond"></a>
                                    </td>
                                </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                    <asp:HiddenField runat="server" ID="hdnAircondInfo" />
            </div>
        </div>
        <div class="row">
            <div class="col">
                <asp:LinkButton runat="server" ID="lnkbtnSave" OnClick="lnkbtnSave_Click" ToolTip="Save" CssClass="btn btnSave right" ValidationGroup="vg" style="margin:10px 0px;">Save</asp:LinkButton> 
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var ValidateAcond = function () {
            var name = $("#<%=txtAircondName.ClientID %>").val();
            var charge = $("#<%= txtAircondCharge.ClientID %>").val();
            var priceRegex = /^\d{0,8}(\.\d{1,2})?$/;
            // validtion
            if (!name || !charge || !priceRegex.test(charge)) {
                var msg = "name and charge need to be valid.";
                if (!name) { msg = "Name field is required."; }
                if (!charge) { msg = "Charge field is required."; }
                if (!priceRegex.test(charge)) { msg = "Please insert valid digit." }

                $(".panel-aircond").show().addClass("panel-danger");
                $(".panel-aircond .panel-heading").html(msg);
                return false;
            }

            $(".panel-aircond").hide().removeClass("panel-danger");
            $(".panel-aircond .panel-heading").html("");
            return true;
        }

        var GetAircondData = function () {
            var airconds = [];
            $("#tblAircond tbody tr").each(function () {
                var name = $(this).find("[data-id='name']").html();
                var charge = $(this).find("[data-id='charge']").html();

                airconds.push({
                    name: name,
                    charge: charge
                });
            });
            return airconds;
        };
        var BindAircondData = function (airconds) {
            $("#tblAircond tbody").empty();
            airconds.forEach(function (aircond, index) {
                $("#tblAircond tbody").append(`
                    <tr>
                        <td>${index+1}</td>
                        <td data-id='name'>${aircond.name}</td>
                        <td data-id='charge'>${aircond.charge}</td>
                        <td><a class ="lnkbtn lnkbtnDelete lnkbtnDeleteAircond"></a></td>
                    </tr>
                `);
            });
            $("#<%=txtAircondName.ClientID %>").val("");
            $("#<%= txtAircondCharge.ClientID %>").val("");
            $("#<%= hdnAircondInfo.ClientID %>").val(JSON.stringify(airconds));
        }
        $(function () {
            $(".panel-aircond").hide();
            $(".btnAddAircond").click(function () {
                var name = $("#<%=txtAircondName.ClientID %>").val();
                var charge = $("#<%= txtAircondCharge.ClientID %>").val();

                if (!ValidateAcond()) return;

                var airconds = GetAircondData(); 
                airconds.push({
                    name: name,
                    charge: charge
                });

                BindAircondData(airconds);
            });

            $(document).on("click", ".lnkbtnDeleteAircond", function () {
                var $tr = $(this).parentsUntil("tr").parent();
                var index = $("#tblAircond tbody tr").index($tr);

                var airconds = GetAircondData();
                airconds.splice(index, 1);

                BindAircondData(airconds);
            });
        })
    </script>
</asp:Content>