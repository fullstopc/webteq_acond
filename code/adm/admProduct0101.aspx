﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admProduct0101.aspx.cs" Inherits="adm_admProduct0101" ValidateRequest="false" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/mst/private.Master" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmProductDetails.ascx" TagName="AdmProductDetails" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmProductDesc.ascx" TagName="AdmProductDesc" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmProductPrice.ascx" TagName="AdmProductPrice" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmProductRelated.ascx" TagName="AdmProductRelated" TagPrefix="uc" %>
<%--<%@ Register Src="~/ctrl/ucAdmProductHotSales.ascx" TagName="AdmProductHotSales" TagPrefix="uc" %>--%>
<%@ Register Src="~/ctrl/ucAdmSupplierDetails.ascx" TagName="AdmSupplierDetails" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmProductGallery.ascx" TagName="AdmProductGallery" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmProductInventory.ascx" TagName="AdmProductInventory" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmProductAddon.ascx" TagName="AdmProductAddon" TagPrefix="uc" %>

<%--Register customized control - start--%>
<%@ Register Src="~/project/francisandjean/ctrl/ucAdmProductSpec.ascx" TagName="AdmProductSpec" TagPrefix="uc" %>
<%--Register customized control - end--%>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>jquery.datetimepicker.js" type="text/javascript"></script>
    <link href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>jquery.datetimepicker.css" rel="Stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
        <div class="action">
            <a href="#" class="hypListingAction setting" onclick="toggleSideMenu();">Page Detail</a>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlForm" runat="server" CssClass="divForm">
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <asp:Panel ID="pnlFormSect1" runat="server">
            <uc:AdmProductDetails ID="ucAdmProductDetails" runat="server" pageListingURL="admProduct01.aspx" />
        </asp:Panel>
        <asp:Panel ID="pnlFormSect2" runat="server" Visible="true">
            <uc:AdmProductDesc ID="ucAdmProductDesc" runat="server" pageListingURL="admProduct01.aspx" />
        </asp:Panel>
        <asp:Panel ID="pnlFormSect3" runat="server" Visible="false">
            <uc:AdmProductPrice ID="ucAdmProductPrice" runat="server" pageListingURL="admProduct01.aspx" />
        </asp:Panel>
        <asp:Panel ID="pnlFormSect4" runat="server" Visible="false" CssClass="tdLoading">
            <asp:UpdatePanel ID="upnlRelProd" runat="server">
                <ContentTemplate>
                    <asp:UpdateProgress ID="uprgAdmLoadingRelProd" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="upnlRelProd">
                        <ProgressTemplate>
                            <uc:AdmLoading ID="ucAdmLoadingRelProd" runat="server" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <uc:AdmProductRelated ID="ucAdmProductRelated" runat="server" pageListingURL="admProduct01.aspx" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Panel ID="pnlFormSect5" runat="server" Visible="false">
           <%-- <uc:AdmProductHotSales ID="ucAdmProductHotSales" runat="server" />--%>
        </asp:Panel>
        <asp:Panel ID="pnlFormSect6" runat="server" Visible="false">
            <uc:AdmSupplierDetails ID="ucAdmSupplierDetails" runat="server" />
        </asp:Panel>
        <asp:Panel ID="pnlFormSect7" runat="server" Visible="false">
            <uc:AdmProductGallery ID="ucAdmProductGallery" runat="server" />
        </asp:Panel>
        <asp:Panel ID="pnlFormSect8" runat="server" Visible="false">
            <uc:AdmProductInventory ID="ucAdmProductInventory" runat="server" pageListingURL="admProduct01.aspx" />
        </asp:Panel>
        <asp:Panel ID="pnlFormSect9" runat="server" Visible="false" CssClass="tdLoading">
            <asp:UpdatePanel ID="upnlProdAddon" runat="server">
                <ContentTemplate>
                    <asp:UpdateProgress ID="uprgAdmLoadingProdAddon" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="upnlProdAddon">
                        <ProgressTemplate>
                            <uc:AdmLoading ID="ucAdmLoadingProdAddon" runat="server" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <uc:AdmProductAddon ID="ucAdmProductAddon" runat="server" pageListingURL="admProduct01.aspx" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <%--Customized control - start--%>
        <asp:Panel ID="pnlFormSect100" runat="server" Visible="false">
            <uc:AdmProductSpec ID="ucAdmProductSpec" runat="server" />
        </asp:Panel>
        <%--Customized control - end--%>
        
    </asp:Panel>
    <script type="text/javascript">
        $(function () {
            <% if(mode == 2 && isAddon) { %> $(".product-addon").hide(); <%}%>
        })
    </script>
</asp:Content>