﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admWorking01.aspx.cs" Inherits="adm_admCMSControlPanel01" validateRequest="false" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmWorkingTimeGroup.ascx" TagName="AdmWorkingTimeGroup" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlForm" runat="server" CssClass="divForm">
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <div class="form__container workingform">
            
            <!-- Start of Working Time !-->
            <div class="form__section timeslot-section">
                <div class="ack-container success hide">
                    <div class="ack-msg"></div>
                </div>
                <div class="form__section__title">
                    Working Time
                </div>
                <div class="form__section__textbox">
                    <div class='col-sm-2'>
                        <div class="form-group">
                            <div class='input-group date' id='timepickerWTFrom'>
                                <asp:TextBox ID="txtWTFrom" runat="server" CssClass="form-control" MaxLength="250" placeholder="From"  ValidationGroup="grpWT"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group">
                            <div class='input-group date' id='timepickerWTTo'>
                                <asp:TextBox ID="txtWTTo" runat="server" CssClass="form-control" MaxLength="250" placeholder="To"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <uc:AdmWorkingTimeGroup ID="AdmWorkingTimeGroup" runat="server" />
                    </div>
                    <div class="col col-sm-1">
                        <asp:LinkButton runat="server" 
                            ID="btnSaveWT" 
                            Text="Save <span class='glyphicon glyphicon-ok'></span>" 
                            ToolTip="Save" 
                            CssClass="btn btn-primary"
                            OnClick="lnkbtnSaveWT_Click"></asp:LinkButton>
                    </div>
                    <asp:Label ID="lblErrWT" runat="server" Text="" Visible="false"></asp:Label>
                </div>
                <div class="form__section__content">
                    <uc:CmnDatatable ID="ucCmnDatatable_WorkingTime" runat="server" />
                    <asp:HiddenField runat="server" ID="hdnWorkingTimeID" />
                    <table id="tblWorkingTime"></table>
                </div>
            </div>
            <!-- End of Working Time !-->

            <!-- Start of Working Day !-->
            <div class="form__section weekdayform">
                <div class="ack-container success" runat="server" id="pnlWeekAck" visible="false">
                    <div class="ack-msg"><asp:Literal runat="server" ID="litWeekAckMsg"></asp:Literal></div>
                </div>
                <div class="form__section__title">
                    Working Day
                </div>
                <div class="form__section__content">
                    <div class="row">
                        <div class="col">
                            <asp:Repeater runat="server" ID="rptDays">
                                <HeaderTemplate><div class="weekday-list"></HeaderTemplate>
                                <FooterTemplate></div></FooterTemplate>
                                <ItemTemplate>
                                    <div class="weekday-timeslot">
                                        <div class="weekday-item">
                                            <asp:CheckBox runat="server"
                                                Checked='<%# Eval("week_checked").ToString() == "1" %>'
                                                ID="chkboxDay"
                                                Text='<%# Eval("week_day").ToString().Substring(0,3) %>' />
                                            <asp:HiddenField runat="server" ID="hdnWeekID" Value='<%# Eval("week_id") %>' />
                                        </div>
                                        <div class="weekday-timezone-selection col-sm-9">
                                            <asp:DropDownList ID="ddlTimeSlot" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="col col-sm-1">
                            <asp:LinkButton runat="server" 
                                ID="lnkbtnSaveWeek" 
                                Text="Save <span class='glyphicon glyphicon-ok'></span>" 
                                ToolTip="Save" 
                                CssClass="btn btn-primary"
                                OnClick="lnkbtnSaveWeek_Click"></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Working Day !-->

            <!-- Start of Off Day !-->
            <div class="form__section holiday-section">
                <div class="ack-container success hide">
                    <div class="ack-msg"></div>
                </div>
                <div class="form__section__title">Off Day</div>
                <div class="form__section__textbox">
                    <div class='col-sm-2'>
                        <div class="form-group">
                            <div class='input-group date' id='datepickerODFrom'>
                                <asp:TextBox ID="txtODFrom" runat="server" CssClass="form-control" MaxLength="250" placeholder="From"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group">
                            <div class='input-group date' id='datepickerODTo'>
                                <asp:TextBox ID="txtODTo" runat="server" CssClass="form-control" MaxLength="250" placeholder="To"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <asp:TextBox ID="txtODRemark" runat="server" CssClass="form-control" MaxLength="250" placeholder="Remark"></asp:TextBox>
                    </div>
                    <div class="col col-sm-1">
                        <asp:LinkButton runat="server" 
                            ID="btnSaveOD" 
                            Text="Save <span class='glyphicon glyphicon-ok'></span>" 
                            ToolTip="Save" 
                            CssClass="btn btn-primary"
                            OnClick="lnkbtnSaveOD_Click"></asp:LinkButton>
                    </div>
                    <asp:Label ID="lblErrOD" runat="server" Text="" Visible="false"></asp:Label>
                </div>
                <div class="form__section__content">
                    <uc:CmnDatatable ID="ucCmnDatatable_WorkingHoliday" runat="server" />
                    <asp:HiddenField runat="server" ID="hdnWTID" />
                    <asp:LinkButton ID="lnkbtnDeleteWT" runat="server" OnClick="lnkbtnDeleteWT_Click" style="display:none;"></asp:LinkButton>
                    <asp:HiddenField runat="server" ID="hdnODID" />
                    <asp:LinkButton ID="lnkbtnDeleteOD" runat="server" OnClick="lnkbtnDeleteOD_Click" style="display:none;"></asp:LinkButton>
                    <table id="tblWorkingHoliday"></table>
                </div>
            </div>
            <!-- End of Off Day !-->
        </div>
    </asp:Panel>
    <script type="text/javascript">
        var $tableTimeslot, $tableHoliday;
        var TimeslotSaveSuccess = function (msg) {
            $.magnificPopup.close();
            $(".timeslot-section .ack-container .ack-msg").html(msg);
            $(".timeslot-section .ack-container").removeClass("hide");
            $tableTimeslot.ajax.reload();
        };

        var HolidaySaveSuccess = function (msg) {
            $.magnificPopup.close();
            $(".holiday-section .ack-container .ack-msg").html(msg);
            $(".holiday-section .ack-container").removeClass("hide");
            $tableHoliday.ajax.reload();
        };

        var initTimeslot = function(){
            var url = "";
            var columns = [
                    { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                    { "data": "time_from", "label": "Time From", "order": 1, "convert": "datetime", "format": "h:mm A" },
                    { "data": "time_to", "label": "Time To", "order": 2, "convert": "datetime", "format": "h:mm A"},
                    { "data": "time_group", "bSortable": true, "label": "Group", "order": 3 },
                    { "data": "action", "bSortable": false, "bSearchable": false, "label": "Action", "order": 4, "align": "center" }, // Add Label: <a class='button add popup' href='admWorkingConfigTimeslotForm.aspx'><i class='material-icons icon'>add</i><span>Add</span></a>
            ];
            var columnSort = columns.reduce(function (prev, curr) {
                prev[curr.data] = curr.order;
                return prev;
            }, {});

            var columnDefs = [{
                "render": function (data, type, row) {
                    var lnkbtnEdit = "<a class='lnkbtn lnkbtnEdit popup' href='admWorkingConfigTimeslotForm.aspx?id=" + row.time_id + "' title='Edit'></a>";
                    var lnkbtnDelete = "<a class='lnkbtn lnkbtnDelete lnkbtnDeleteWT' title='Delete' data-id='" + row.time_id + "'></a>";

                    return lnkbtnEdit + lnkbtnDelete;
                },
                "className": "dt-center",
                "targets": columnSort["action"]
            }];
            var properties = {
                columns: columns,
                columnDefs: columnDefs,
                columnSort: columnSort,
                dom: 'ZBrti',
                func: "getWorkingConfigTime",
                name: "working_config_time",
                elementID: "tblWorkingTime",
                callback: function () {
                    $('#tblWorkingTime .popup').magnificPopup({
                        type: 'iframe',
                        mainClass: 'timeslot-frame',
                        iframe: {
                            markup: `
                                <div class="mfp-iframe-scaler">
                                    <div class="mfp-close"></div>
                                    <iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>
                                </div>`
                        }
                    });
                }
            };
            $tableTimeslot = callDataTables(properties);
        }
        var initHoliday = function () {
            var url = "";
            var columns = [
                    { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                    { "data": "holiday_date", "label": "From", "order": 1, "convert": "datetime", "format": "DD-MMM-YYYY"},
                    { "data": "holiday_date_to", "label": "To", "order": 2, "convert": "datetime", "format": "DD-MMM-YYYY"},
                    { "data": "holiday_remark", "label": "Remark", "order": 3},
                    { "data": "action", "bSortable": false, "bSearchable": false, "label": "Action", "order": 4, "align": "center" }, //Add Label: <a class='button add popup' href='admWorkingConfigHolidayForm.aspx'><i class='material-icons icon'>add</i><span>Add</span></a>
            ];
            var columnSort = columns.reduce(function (prev, curr) {
                prev[curr.data] = curr.order;
                return prev;
            }, {});

            var columnDefs = [{
                "render": function (data, type, row) {
                    var lnkbtnEdit = "<a class='lnkbtn lnkbtnEdit popup' href='admWorkingConfigHolidayForm.aspx?id=" + row.holiday_id + "' title='Edit'></a>";
                    var lnkbtnDelete = "<a class='lnkbtn lnkbtnDelete lnkbtnDeleteOD' title='Delete' data-id='" + row.holiday_id + "'></a>";

                    return lnkbtnEdit + lnkbtnDelete;
                },
                "className": "dt-center",
                "targets": columnSort["action"]
            }];
            var properties = {
                columns: columns,
                columnDefs: columnDefs,
                columnSort: columnSort,
                dom: 'ZBrti',
                func: "getWorkingConfigHoliday",
                name: "working_config_holiday",
                elementID: "tblWorkingHoliday",
                callback: function () {
                    $('#tblWorkingHoliday .popup').magnificPopup({
                        type: 'iframe',
                        mainClass: 'holiday-frame',
                        iframe: {
                            markup: `
                                <div class="mfp-iframe-scaler">
                                    <div class="mfp-close"></div>
                                    <iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>
                                </div>`
                        }

                    });
                }
            };
            $tableHoliday = callDataTables(properties);
        }

        $(document).on("click", ".lnkbtnDeleteWT", function () {
            if (confirm("<%= HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteTimeSlot.Text") %>")) {
                var objID = $(this).attr("data-id");
                document.getElementById('<%= hdnWTID.ClientID %>').value = objID;
                document.getElementById('<%= lnkbtnDeleteWT.ClientID %>').click();
            }
        })

        $(document).on("click", ".lnkbtnDeleteOD", function () {
            if (confirm("<%= HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteOffDay.Text") %>")) {
                var objID = $(this).attr("data-id");
                document.getElementById('<%= hdnODID.ClientID %>').value = objID;
                document.getElementById('<%= lnkbtnDeleteOD.ClientID %>').click();
            }
        })

        $(function () {
            initTimeslot();
            initHoliday();

            $('#timepickerWTFrom, #timepickerWTTo').datetimepicker({
                format: 'LT',
            });
            $('#datepickerODFrom, #datepickerODTo').datetimepicker({
                format: 'DD-MMM-YYYY',
            });
        })
    </script>
</asp:Content>

