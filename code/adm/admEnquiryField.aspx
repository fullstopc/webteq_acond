﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" EnableViewState="true" CodeFile="admEnquiryField.aspx.cs" Inherits="adm_admEnquiryField" %>
<%@ MasterType VirtualPath="~/mst/blank.master" %>
<%@ Register Src="~/ctrl/ucAdmCheckAccess.ascx" TagName="AdmCheckAccess" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" Runat="Server">
        <link href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>gridster/jquery.gridster.css" rel="Stylesheet" />
        <link href="<% =System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>gridster/jquery.gridster.custom.css" rel="Stylesheet" />
        <script src="<% =System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>gridster/jquery.gridster.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
   <asp:ScriptManager runat="server"></asp:ScriptManager>
    
   <div class="form">
       <div class="row">
            <div class="column">
                <div class="ack-container" runat="server" id="divAck" visible="false">
                    Save Succesfully.
                </div>
            </div>
        </div>
       <div class="row">
           <div class="column"><h1>Enquiry Field</h1></div>
       </div>

                <asp:UpdatePanel runat="server" ID="upnlProd">
                    <ContentTemplate>
                        <div class="enquiry-form enquiry-field <%= BoxOpen ? "active" : "" %>">
                        <div class="enquiry-form-header">
                            <div class="row">
                                <div class="title">Enquiry Form</div>
                                <div class="tools">
                                    <div class="arrow" onclick="$('.enquiry-field').toggleClass('active');$('.enquiry-field .enquiry-form-body').slideToggle();">▼</div>
                                </div>
                            </div>
                        </div>
                        <div class="enquiry-form-body" style="<%= BoxOpen ? "display:block;" : "display:none;" %>">
                            <div class="row">
                                <div class="column-4">
                                    <label>Field Name</label>
                                    <asp:TextBox runat="server" ID="txtName"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" 
                                        ID="rvName" 
                                        Display="Dynamic" 
                                        ErrorMessage="This field is required." 
                                        CssClass="errmsg"
                                        ValidationGroup="vgFieldForm"
                                        ControlToValidate="txtName"></asp:RequiredFieldValidator>
                                </div>
                                <div class="column-2">
                                    <label>Input Type</label>
                                    <asp:DropDownList runat="server" ID="ddlType" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                                        <asp:ListItem Value="input" Text="Input"></asp:ListItem>
                                        <asp:ListItem Value="select" Text="Select"></asp:ListItem>
                                        <asp:ListItem Value="textarea" Text="TextArea"></asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                                <div class="column-2">
                                    <label>Compulsory</label>
                                    <asp:DropDownList runat="server" ID="ddlRequired">
                                        <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="No"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="column-2">
                                    <label>Validation</label>
                                    <asp:DropDownList runat="server" ID="ddlValidation"></asp:DropDownList>
                                </div>
                                <div class="column-2">
                                    <label>&nbsp;</label>
                                    <div class="action flexbox">
                                        <asp:LinkButton runat="server" ID="lnkbtnAddField" CssClass="button" OnClick="lnkbtnAddField_Click" ValidationGroup="vgFieldForm">Add</asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lnkbtnEditField" CssClass="button" OnClick="lnkbtnEditField_Click" ValidationGroup="vgFieldForm" Visible="false">Save</asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lnkbtnCancelField" CssClass="button" OnClick="lnkbtnCancelField_Click" CausesValidation="false" Visible="false">Cancel</asp:LinkButton>
                                    </div>
                        
                                    <asp:HiddenField runat="server" ID="hdnFieldFormID" />
                                    <asp:HiddenField runat="server" ID="hdnFieldStorage" />
                                </div>
                            </div>
                            <asp:Panel ID="pnlOptionForm" runat="server" Visible="false" CssClass="enquiry-options-form">
                                <div class="row">
                                    <div class="column-4">&nbsp;</div>
                                    <div class="column-2"><label>Options</label><asp:TextBox runat="server" ID="txtOptionText" placeholder="Text"></asp:TextBox></div>
                                    <div class="column-2"><label>&nbsp;</label><asp:TextBox runat="server" ID="txtOptionValue" placeholder="Value"></asp:TextBox></div>
                                    <div class="column-2"><label>&nbsp;</label>
                                        <div class="action flexbox">
                                            <asp:LinkButton runat="server" ID="lnkbtnAddOption" CssClass="button" OnClick="lnkbtnAddOption_Click">Add</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkbtnEditOption" CssClass="button" OnClick="lnkbtnEditOption_Click" ValidationGroup="vgFieldForm" Visible="false">Save</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkbtnCancelOption" CssClass="button" OnClick="lnkbtnCancelOption_Click" CausesValidation="false" Visible="false">Cancel</asp:LinkButton>
                                        </div>
                                        
                                        <asp:HiddenField runat="server" ID="hdnFieldOptionID" />
                                        <asp:HiddenField runat="server" ID="hdnFieldOptionStorage" />
                                    </div>
                                    <div class="column-2"><asp:HiddenField runat="server" ID="hdnParent" Value="0" /></div>
                                </div>
                                <div class="row">
                                    <div class="column-4"></div>
                                    <div class="column-4">
                                        <asp:Repeater runat="server" ID="rptOptions" OnItemCommand="rptOptions_ItemCommand">
                                            <HeaderTemplate><ol class="option-list"></HeaderTemplate>
                                            <FooterTemplate></ol></FooterTemplate>
                                            <ItemTemplate>
                                                <li>
                                                    <%# Eval("enqlabel_value") %> - <%# Eval("enqattr_value") %>
                                                    <div class="action">
                                                        <asp:LinkButton runat="server" ID="lnkbtnEdit" CommandName="cmdEdit" CommandArgument='<%# Eval("id") + "|" + Eval("parent_id") + "|"  + Eval("storage") %>'>
                                                            <i class="icon material-icons" runat="server">edit</i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lnkbtnDelete" CommandName="cmdDelete" CommandArgument='<%# Eval("id") + "|" + Eval("parent_id") + "|"  + Eval("storage") %>'>
                                                            <i class="icon material-icons" runat="server">delete_forever</i>
                                                        </asp:LinkButton>
                                                        <asp:HiddenField runat="server" ID="hdnHidden" />
                                                    </div>
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <div class="column-4"></div>
                                </div>
                            </asp:Panel>
                            <div class="row">
                                <div class="column">
                                    <asp:Repeater runat="server" ID="rptEnqField" 
                                        OnItemDataBound="rptEnqField_ItemDataBound"
                                        OnItemCommand="rptEnqField_ItemCommand">
                                        <HeaderTemplate>
                                            <table cellpadding="0" cellspacing="0" class="enquiry-field-grid">
                                                <colgroup>
                                                    <col class="index" />
                                                    <col class="name" />
                                                    <col class="inputtype" />
                                                    <col class="compulsory" />
                                                    <col class="validation" />
                                                    <col class="action" />
                                                </colgroup>
                                                <thead>
                                                    <tr>
                                                        <td class="index">No.</td>
                                                        <td class="name">Field Name</td>
                                                        <td class="inputtype">Input Type</td>
                                                        <td class="compulsory">Compulsory</td>
                                                        <td class="validation">Validation</td>
                                                        <td class="action">&nbsp;</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <FooterTemplate></tbody></table></FooterTemplate>
                                        <ItemTemplate>
                                            <tr data-id="<%# Eval("id") %>">
                                                <td class="index"><%# Container.ItemIndex + 1 %>.</td>
                                                <td class="name">
                                                    <asp:Literal runat="server" ID="litName" Text='<%# Eval("enqlabel_value") %>'></asp:Literal>
                                                    <asp:Repeater runat="server" ID="rptFieldChilds">
                                                        <HeaderTemplate><ol></HeaderTemplate>
                                                        <FooterTemplate></ol></FooterTemplate>
                                                        <ItemTemplate>
                                                            <li><%# Eval("enqlabel_value") %></li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                                <td class="inputtype">
                                                    <asp:Literal runat="server" ID="litType" Text='<%# Eval("enqfield_type") %>'></asp:Literal>
                                                </td>
                                                <td class="compulsory"><asp:Literal runat="server" ID="litRequired" Text='<%# Convert.ToString(Eval("enqfield_required")) == "1" ? "Yes" : "No" %>'></asp:Literal></td>
                                                <td class="validation"><%# !string.IsNullOrEmpty(Eval("enqfield_validate").ToString()) ? 
                                                                                ddlValidation.Items.FindByValue(Eval("enqfield_validate").ToString()).Text : "" %></td>
                                                <td class="action">
                                                    <div class="tools">
                                                        <asp:LinkButton runat="server" ID="lnkbtnEdit" CommandName="cmdEdit" CommandArgument='<%# Eval("id") + "|" + Eval("storage") %>'>
                                                            <i class="icon material-icons" runat="server">edit</i>
                                                        </asp:LinkButton>
                                            
                                                        <i class="icon material-icons" onclick="sortHidden(this);" runat="server" visible='<%# Eval("enqfield_deletable").ToString() == "1" %>'>delete_forever</i>
                                            
                                                        <div class="checkbox-group">
                                                            <label class="chkbox_active">
                                                                <asp:CheckBox runat="server" ID="chkboxActive" Checked='<%# Convert.ToString(Eval("enqfield_active")) == "1" ? true : false %>' />
                                                                <span>Active</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                        
                                                    <input type="hidden" runat="server" id="hdnIndex" class="hdnIndex" value='<%# Eval("index") %>' />
                                                    <input type="hidden" runat="server" id="hdnHidden" class="hdnHidden" value="0" />
                                                    <input type="hidden" runat="server" id="hdnInfo" class="hdnInfo" value='<%#
                                                        Newtonsoft.Json.JsonConvert.SerializeObject(new System.Collections.Generic.Dictionary<string, object>(){
                                                            { "posX" , Eval("enqfield_position_x") },
                                                            { "posY" , Eval("enqfield_position_y") },
                                                            { "sizeX" , Eval("enqfield_size_x") },
                                                            { "sizeY" , Eval("enqfield_size_y") },
                                                            { "type", Eval("enqfield_type") },
                                                            { "label", Eval("enqlabel_value") },
                                                            { "ID" , Eval("ID") },
                                                        })
                                                    %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="uprgProd" AssociatedUpdatePanelID="upnlProd" runat="server" DisplayAfter="10">
                    <ProgressTemplate>
                        <uc:AdmLoading runat="server" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            <div class="enquiry-form ">
                <div class="enquiry-form-header">
                   <div class="row">
                       <div class="title">Enquiry Field Position</div>
                   </div>
                </div>
                <div class="enquiry-form-body active">
                    <div class="row">
           <div class="column">
               <asp:UpdatePanel runat="server" ID="upnlGridster">
                    <ContentTemplate>
                        <asp:Repeater runat="server" ID="rptGridster">
                            <HeaderTemplate><div class="gridster"><ul></HeaderTemplate>
                            <FooterTemplate></ul></div></FooterTemplate>
                            <ItemTemplate>
                                <li data-row="<%# Eval("enqfield_position_x").ToString() == "0" ? "99" : Eval("enqfield_position_x") %>" 
                                    data-col="<%# Eval("enqfield_position_y") %>" 
                                    data-sizex="<%# Eval("enqfield_size_x") %>" 
                                    data-sizey="<%# Eval("enqfield_size_y") %>" 
                                    data-id="<%# Eval("id") %>"
                                    class="gridster-item">
                                    <span><%# Eval("enqfield_type") %></span>
                                    <span>:</span> 
                                    <span><%# Eval("enqlabel_value") %></span>
                                    <span class="close">&#10006;</span>
                                    <input type="hidden" runat="server" id="hdnInfo" class="hdnInfo" />
                                    <input type="hidden" runat="server" id="hdnIndex" class="hdnIndex" value='<%# Eval("index") %>' />
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ContentTemplate>
                </asp:UpdatePanel>
                
            </div>
        </div>
                </div>
            </div>
                
        <div class="row">
            <div class="column-10"></div>
            <div class="column-2"><asp:LinkButton runat="server" ID="lnkbtnSave" CssClass="button" OnClick="lnkbtnSave_Click">Save</asp:LinkButton></div>
        </div>

   </div>
    <script>
        var $gridster;
        function sortInit() {
            $(".list.sortable").sortable({
                update: function (event, ui) {
                    var $sortable = $(this);
                    sortIndex($sortable);
                }
            });
        };

        function sortHidden(element) {
            $(element).parentsUntil("tr").parent().attr("data-hidden", 1);
            $(element).parentsUntil("tr").parent().find(".hdnHidden").val("1");
            sortIndex($(element).parentsUntil(".sortable.list").parent());
        }
        function sortUp(element) {
            var $parent = $(element).parentsUntil(".sortable.list").parent();
            var $this = $(element).parentsUntil(".item").parent();
            if ($parent.find(".item").index($this) > 0) {

                $this.fadeOut(200, function () {
                    $this.insertBefore($this.prev());
                    $this.fadeIn(200, function () {
                        sortIndex($parent);
                    });
                });
                sortIndex($parent);
            }

        }
        function sortDown(element) {
            var $parent = $(element).parentsUntil(".sortable.list").parent();
            var $this = $(element).parentsUntil(".item").parent();
            var length = $parent.find(".item:visible").length;
            if ($parent.find(".item").index($this) < length - 1) {

                $this.fadeOut(200, function () {
                    $this.insertAfter($this.next());
                    $this.fadeIn(200, function () {
                        sortIndex($parent);
                    });
                });
            }
        }
        function sortIndex($sortable) {
            $sortable.find(" > .item:visible ").each(function (i) {
                $(this).find(".index").html(i + 1);
                $(this).find(".hdnOrder").val(i + 1);
            });
        }

        function gridInit() {
            $gridster = $(".gridster ul").gridster({
                widget_base_dimensions: ['auto', 50],
                //autogenerate_stylesheet: true,
                //shift_widgets_up: false,
                //shift_larger_widgets_down: false,
                //collision: {
                //    wait_for_mouseup: true
                //},
                min_cols: 1,
                max_cols: 12,
                widget_margins: [10, 10],
                resize: {
                    enabled: true,
                    stop: function (e, ui, $widget) {
                        $(".gridster .gridster-item").each(function () {
                            var posX = $(this).attr("data-row");
                            var posY = $(this).attr("data-col");
                            var sizeX = $(this).attr("data-sizex");
                            var sizeY = $(this).attr("data-sizey");
                            var ID = $(this).attr("data-id");

                            var $hdn = $(".enquiry-field tr[data-id='" + ID + "'] .hdnInfo");
                            var info = JSON.parse($hdn.val());
                            info.posX = posX;
                            info.posY = posY;
                            info.sizeX = sizeX;
                            info.sizeY = sizeY;

                            $hdn.val(JSON.stringify(info));
                        });

                    }
                },
                draggable: {
                    stop: function (e, ui, $widget) {
                        $(".gridster .gridster-item").each(function () {
                            console.log($(this));
                            var posX = $(this).attr("data-row");
                            var posY = $(this).attr("data-col");
                            var sizeX = $(this).attr("data-sizex");
                            var sizeY = $(this).attr("data-sizey");
                            var ID = $(this).attr("data-id");

                            
                            var $hdn = $(".enquiry-field tr[data-id='" + ID + "'] .hdnInfo");
                            var info = JSON.parse($hdn.val());
                   
                            info.posX = posX;
                            info.posY = posY;
                            info.sizeX = sizeX;
                            info.sizeY = sizeY;

                            $hdn.val(JSON.stringify(info));
                        });
                        
                    }
                }
            }).data('gridster');
            //$gridster.disable().disable_resize();

            $(".chkbox_active input[type='checkbox']").change(function () {
                var $parent = $(this).parentsUntil(".action").parent();
                var $hdnInfo = $parent.find(".hdnInfo");

                var info = JSON.parse($hdnInfo.val())

                if($(this).is(':checked')){
                    var $item = "<li class='gridster-item' data-id='"+info.ID+"'>" + 
                                "<span>"+info.type+"</span>" + 
                                "<span>:</span> " + 
                                "<span>"+info.label+"</span> " + 
                                "</li>";
                    $gridster.add_widget($item, info.sizeX, info.sizeY, info.posY, info.posX);
                }else{
                    var $item = $("li[data-id='" + info.ID + "']");
                    $gridster.remove_widget($item);
                }
                

                

                
            });
        }
        $(function (){
            gridInit();

        })

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function (s, e) {
            gridInit();
        });
    </script>
</asp:Content>

