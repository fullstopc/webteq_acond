﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_admArea01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 90;
    protected int _mode = 1;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {

            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMAREASETTING, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.parentId = 1;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            setPageProperties();
        }


        Session["EDITED_AREASETTING"] = null;
        Session["NOCHANGE"] = null;
        Session["ERRMSG"] = null;
        Session["ADMPAGENAME"] = null;
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        if (Session["EDITED_AREASETTING"] != null)
        {
            if (Session["NOCHANGE"] != null)
            {
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
            }
            else
            {
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";
                clsMis.resetListing();
            }

            pnlAck.Visible = true;
        }
        else if (Session["ERRMSG"] != null)
        {
            pnlAck.Visible = true;
            litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
        }
    }
    #endregion
}
