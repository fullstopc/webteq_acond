﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admPage0101.aspx.cs" Inherits="adm_admPage0101" ValidateRequest="false" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmCMS.ascx" TagName="AdmCMS" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucCmnSiblingPages.ascx" TagName="CmnSiblingPages" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmMobileView.ascx" TagName="AdmMobileView" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
        <div class="action">
            <a href="#" class="hypListingAction setting" onclick="toggleSideMenu();">Page Detail</a>
        </div>
        <div class="divListingSplitter"></div>
        <div class="pageAction">
            <a href="#" class="hypListingAction" runat="server" id="hypPrevPage" visible="false" style="padding-left: 10px;"><i class="material-icons">keyboard_arrow_left</i>Previous Page</a>
            <div class="divListingSplitter"></div>
            <a href="#" class="hypListingAction" runat="server" id="hypNextPage" visible="false" style="padding-left: 10px;"><i class="material-icons">keyboard_arrow_right</i>Next Page</a>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlFormContainer" runat="server" CssClass="divForm">
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <asp:Panel ID="pnlForm" runat="server" Visible="false">
            <uc:AdmCMS ID="ucAdmCMS" runat="server" pageListingURL="admPage01.aspx" />
        </asp:Panel>
        <asp:Panel ID="pnlForm2" runat="server" Visible="false">
            <uc:CmnSiblingPages id="ucCmnSiblingPages" runat="server" viewType="Admin" pageListingURL="admPage01.aspx" />
        </asp:Panel>
        <asp:Panel ID="pnlForm3" runat="server" Visible="false">
            <uc:AdmMobileView ID="ucAdmMobileView" runat="server" pageListingURL="admPage01.aspx" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>

