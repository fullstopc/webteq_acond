﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admCMS01.aspx.cs" Inherits="adm_admCMS01" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title"><asp:Literal ID="litPageTitle" runat="server" meta:resourcekey="litPageTitle"></asp:Literal></div>
        <div class="divListingSplitter"></div>
        <div class="action">
            <asp:Panel ID="pnlListingAction" runat="server" CssClass="divListingAction">
                <asp:HyperLink ID="hypAddCMS" runat="server" Text="Add Object" ToolTip="Add Object" NavigateUrl="admCMS0101.aspx" CssClass="hypListingAction add"></asp:HyperLink>
            </asp:Panel>
        </div>
        <div class="divListingSplitter"></div>
        <div class="filter">
            <div class="divFilterContainer">
                <div class="divFilter">
                    <asp:TextBox ID="txtKeyword" runat="server" placeholder="Keyword Search" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
                    <a href="#" class="btnDropdown" onclick="toggleFilterOption();"></a>
                    <asp:Literal ID="litKeywordRule" runat="server" meta:resourcekey="litKeywordRule" visible="false"></asp:Literal>
                </div>
                <div class="divFilterMore">
                    <div></div>
                    <div>
                        <table class="tblSearch">
                            <tr>
                                <td class="">
                                    <div class="divFilterLabel">Active:</div>
                                    <asp:DropDownList ID="ddlActive" runat="server" CssClass="ddl_medium"></asp:DropDownList>

                                </td>
                                <td class="">
                                    

                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
            <div class="divFilterAction" runat="server" visible="false"><asp:Button ID="lnkbtnGo" runat="server" Text="" ToolTip="Search" CssClass="btn btnSearch" OnClick="lnkbtnGO_Click"/></div>
        </div>
    </div>
    <div class="divListing">
        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
            <div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
        </asp:Panel>
        <uc:CmnDatatable ID="ucCmnDatatable" runat="server" />

        <asp:HiddenField runat="server" ID="hdnObjectID" />
        <asp:LinkButton ID="lnkbtnDelete" runat="server" OnClick="lnkbtnDelete_Click" style="display:none;"></asp:LinkButton>
    </div>
     <script type="text/javascript">

        $(function () {
            var columns = [
                    { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                    { "data": "CMS_TITLE", "label": "Title", "order": 1, },
                    { "data": "V_CMSTYPENAME", "label": "Type", "order": 2, },
                    { "data": "CMS_ACTIVE", "bSearchable": false, "label": "Active", "order": 3, "convert": "active" },
                    { "data": "action", "bSortable": false, "bSearchable": false, "label": "Action", "order": 4 },
            ];
            var columnSort = columns.reduce(function (prev, curr) {
                prev[curr.data] = curr.order;
                return prev;
            }, {});

            var columnDefs = [{
                "render": function (data, type, row) {
                    var lnkbtnEdit = "<a class='lnkbtn lnkbtnEdit' href='admCMS0101.aspx?id=" + row.CMS_ID + "' title='Edit'></a>";
                    var lnkbtnDelete = "<a class='lnkbtn lnkbtnDelete' title='Delete' data-id='" + row.CMS_ID + "'></a>";
                    return lnkbtnEdit + lnkbtnDelete;
                },
                "targets": columnSort["action"]
            }];
            var properties = {
                columns: columns,
                columnDefs: columnDefs,
                columnSort: columnSort,
                func: "getObjectData",
                name: "object",
            };
            var $datatable = callDataTables(properties);

            // Filter
            $("#<%= ddlActive.ClientID %>").change(function () {
                $datatable
               .columns(columnSort["CMS_ACTIVE"])
               .search(this.value)
               .draw();
            });

            $(document).on("click", ".lnkbtnDelete", function () {
                if (confirm("<%= HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteCMS.Text") %>")) {
                    var objID = $(this).attr("data-id");
                    document.getElementById('<%= hdnObjectID.ClientID %>').value = objID;
                    document.getElementById('<%= lnkbtnDelete.ClientID %>').click();
                }
            })
            
        });
    </script>
</asp:Content>
