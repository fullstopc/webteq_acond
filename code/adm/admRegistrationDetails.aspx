﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admRegistrationDetails.aspx.cs" Inherits="adm_admRegistrationDetails" %>
<%@ MasterType VirtualPath="~/mst/blank.master" %>
<%@ Register Src="~/ctrl/ucAdmCheckAccess.ascx" TagName="AdmCheckAccess" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function validateCurrency_client(source, args) {
            var currRE = /<%= clsAdmin.CONSTBIGCURRENCYRE %>/;

            if (args.Value.match(currRE)) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
                source.errormessage = "<br />Please enter only currency.";
                source.innerHTML = "<br />Please enter only currency.";
            }
        }

        function validatePayment(source, args) {
            var txtPaymentMethod = document.getElementById("<%= txtPaymentMethod.ClientID %>");
            var txtTransID = document.getElementById("<%= txtTransID.ClientID %>");
            var txtPaymentAmount = document.getElementById("<%= txtPaymentAmount.ClientID %>");

            var boolValid = false;

            if (args.Value != '') {
                boolValid = true;
            }
            else {
                if (txtPaymentMethod.value != '' || txtTransID.value != '' || txtPaymentAmount.value != '') {
                    boolValid = false;
                }
                else {
                    boolValid = true;
                }
            }

            args.IsValid = boolValid;
        }

        var txtDOB;
        var hdnDOB;
        var currentTime = new Date();
        var day = currentTime.getDate() + 1;
        var month = currentTime.getMonth() + 1;
        var year = currentTime.getFullYear() - 10;

        var currentDate = parseInt(day, 10) + "/" + parseInt(month, 10) + "/" + year;

        function initSect3() {
            txtDOB = document.getElementById("<%= txtDOB.ClientID %>");
            hdnDOB = document.getElementById("<%= hdnDOB.ClientID %>");

            $(function() {
                $("#<% =txtDOB.ClientID %>").datepicker({ dateFormat: 'd M yy' });
                $("#<% =txtDOB.ClientID %>").datepicker({ changeMonth: true });
                $("#<% =txtDOB.ClientID %>").datepicker({ changeYear: true });
                $("#<% =txtDOB.ClientID %>").datepicker({ altField: "#<% =hdnDOB.ClientID %>" });
                $("#<% =txtDOB.ClientID %>").datepicker({ altFormat: "dd/mm/yy" });
                $("#<% =txtDOB.ClientID %>").datepicker("option", "yearRange", '-100:+0');

                //getter
                var changeMonth = $("#<% =txtDOB.ClientID %>").datepicker('option', 'changeMonth');
                var changeYear = $("#<% =txtDOB.ClientID %>").datepicker('option', 'changeYear');
                var altField = $("#<% =txtDOB.ClientID %>").datepicker("option", "altField");
                var altFormat = $("#<% =txtDOB.ClientID %>").datepicker("option", "altFormat");
                var yearRange = $("#<% =txtDOB.ClientID %>").datepicker("option", "yearRange");

                //setter
                $("#<% =txtDOB.ClientID %>").datepicker('option', 'changeMonth', true);
                $("#<% =txtDOB.ClientID %>").datepicker('option', 'changeYear', true);
                $("#<% =txtDOB.ClientID %>").datepicker('option', 'changeYear', true);
                $("#<% =txtDOB.ClientID %>").datepicker("option", "altField", '#<% =hdnDOB.ClientID %>');
                $("#<% =txtDOB.ClientID %>").datepicker("option", "altFormat", 'dd/mm/yy');
                $("#<% =txtDOB.ClientID %>").datepicker("option", "yearRange", '-100:+0');
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <asp:Panel ID="pnlRegistrationDetailsOuter" runat="server" CssClass="divRegistrationDetailsOuter">
        <asp:Panel ID="pnlRegistrationDetailsInner" runat="server" CssClass="divRegistrationDetailsInner">
            <uc:AdmCheckAccess ID="ucAdmCheckAccess" runat="server" />
            <asp:Panel ID="pnlRegistrationDetails" runat="server" CssClass="divRegistrationDetails">
                <asp:Panel ID="pnlRegistrationDetailsHdr" runat="server" CssClass="divRegistrationDetailsHdr">
                    <div class="divRegDetailsHdrLeft"><asp:Literal ID="litRegDetailsHdr" runat="server"></asp:Literal></div>
                    <div class="divRegDetailsHdrRight"><uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" /></div>
                </asp:Panel>
                <asp:Panel ID="pnlRegistrationDetailsAck" runat="server" CssClass="divRegistrationDetailsAck" Visible="false">
                    <div class="divAckGrey"><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
                </asp:Panel>
                <asp:Panel ID="pnlRegistrationDetailsContent" runat="server" CssClass="divRegistrationDetailsContent">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td class="tdRegDetailLabel">Full Name<span class="attention_compulsory">*</span>:</td>
                            <td>
                                <asp:Literal ID="litName" runat="server" Visible="false"></asp:Literal>
                                <asp:TextBox ID="txtFirstName" runat="server" CssClass="text_medium" MaxLength="250" Visible="false"></asp:TextBox>
                                <asp:TextBox ID="txtLastName" runat="server" CssClass="text_medium" MaxLength="250" Visible="false"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ErrorMessage="<br />Please enter your first name." ControlToValidate="txtFirstName" CssClass="errmsg" Display="Dynamic" ValidationGroup="vgRegister"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ErrorMessage="<br />Please enter your last name." ControlToValidate="txtLastName" CssClass="errmsg" Display="Dynamic" ValidationGroup="vgRegister"></asp:RequiredFieldValidator>
                            </td>
                            <%--<td class="tdRegDetailLabelNor2">Payment Status:</td>
                            <td><asp:Literal ID="litPaymentStatus" runat="server"></asp:Literal></td>--%>
                        </tr>
                        <%--<tr>
                            <td class="tdRegDetailLabel">NRIC Number (for SC/PR):</td>
                            <td>
                                <asp:Literal ID="litIcNo" runat="server" Visible="false"></asp:Literal>
                                <asp:TextBox ID="txtIcNo" runat="server" CssClass="text_medium" MaxLength="250" Visible="false"></asp:TextBox>
                            </td>
                        </tr>--%>
                        <tr>
                            <td class="tdRegDetailLabel">Email Address<span class="attention_compulsory">*</span>:</td>
                            <td>
                                <asp:Literal ID="litEmail" runat="server" Visible="false"></asp:Literal>
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="text" MaxLength="250" Visible="false"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="<br />Please enter Email." ControlToValidate="txtEmail" CssClass="errmsg" Display="Dynamic" ValidationGroup="vgRegister"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="<br/>Please enter a valid email." Display="Dynamic" ControlToValidate="txtEmail" CssClass="errmsg" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="vgRegister"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdRegDetailLabel">Contact Number<span class="attention_compulsory">*</span>:</td>
                            <td>
                                <asp:Literal ID="litContactNo" runat="server" Visible="false"></asp:Literal>
                                <asp:TextBox ID="txtContactNo" runat="server" CssClass="text_medium" MaxLength="20" Visible="false"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvContactNo" runat="server" ErrorMessage="<br />Please enter Contact No.." ControlToValidate="txtContactNo" CssClass="errmsg" Display="Dynamic" ValidationGroup="vgRegister"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="tdRegDetailLabel">Malaysian<span class="attention_compulsory">*</span>:</td>
                            <td>
                                <asp:Literal ID="litMalaysia" runat="server" Visible="false"></asp:Literal>
                                <asp:RadioButton ID="rdoYes" runat="server" GroupName="malaysian" Visible="false" />
                                <asp:RadioButton ID="rdoNo" runat="server" GroupName="malaysian" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdRegDetailLabel">Gender<span class="attention_compulsory">*</span>:</td>
                            <td>
                                <asp:Literal ID="litGender" runat="server" Visible="false"></asp:Literal>
                                <asp:RadioButton ID="rdoMale" runat="server" GroupName="gender" Visible="false" />
                                <asp:RadioButton ID="rdoFemale" runat="server" GroupName="gender" Visible="false" />
                            </td>
                        </tr>
                        <tr id="trDOB" runat="server">
                            <td class="tdRegDetailLabel">Date of Birth<span class="attention_compulsory">*</span>:</td>
                            <td>
                                <asp:Literal ID="litDOB" runat="server" Visible="false"></asp:Literal>
                                <asp:TextBox ID="txtDOB" runat="server" CssClass="text_medium" Visible="false"></asp:TextBox><asp:HiddenField ID="hdnDOB" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdRegDetailLabel">Highest Level of Education:</td>
                            <td>
                                <asp:Literal ID="litHighestEdu" runat="server" Visible="false"></asp:Literal>
                                <asp:DropDownList ID="ddlHighestEdu" runat="server" CssClass="ddl" Visible="false"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdRegDetailLabel">English (Speak):</td>
                            <td>
                                <asp:Literal ID="litEngSpeak" runat="server" Visible="false"></asp:Literal>
                                <asp:DropDownList ID="ddlEngSpeak" runat="server" CssClass="ddl" Visible="false"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdRegDetailLabel">English (Write):</td>
                            <td>
                                <asp:Literal ID="litEngWrite" runat="server" Visible="false"></asp:Literal>
                                <asp:DropDownList ID="ddlEngWrite" runat="server" CssClass="ddl" Visible="false"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="trReason" runat="server" visible="false">
                            <td class="tdRegDetailLabel">I'm suitable for this course because </td>
                            <td><asp:Literal ID="litReason" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="trResume" runat="server" visible="false">
                            <td class="tdRegDetailLabel">Resume:</td>
                            <td><asp:Literal ID="litResume" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="trLearnAbout" runat="server" visible="false">
                            <td class="tdRegDetailLabel">Learn about program from </td>
                            <td><asp:Literal ID="litLearnAbout" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="tdRegDetailLabel">Selected Program Category:</td>
                            <td><asp:Literal ID="litProgCat" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                           <td class="tdRegDetailLabel">Selected Program:</td>
                           <td><asp:Literal ID="litProgName" runat="server"></asp:Literal></td> 
                        </tr>
                        <tr>
                            <td class="tdRegDetailLabel">Program Date<span class="attention_compulsory">*</span>:</td>
                            <td>
                                <asp:Literal ID="litProgDate" runat="server" Visible="false"></asp:Literal>
                                <asp:DropDownList ID="ddlProgramDate" runat="server" CssClass="ddl" Visible="false"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvProgDate" runat="server" ErrorMessage="<br />Please select Program Date." ControlToValidate="ddlProgramDate" CssClass="errmsg" Display="Dynamic" ValidationGroup="vgRegister"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdRegDetailLabel">Recently applied:</td>
                            <td><asp:Literal ID="litRecentApplied" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="tdRegDetailLabel">Subscribe for IMCITDP updates</td>
                            <td><asp:Literal ID="litSubscribe" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="trProgFee" runat="server" visible="false">
                            <td class="tdRegDetailLabelNor">Program Fee:</td>
                            <td><asp:Literal ID="litProgramFee" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="tdRegDetailLabelNor">Registration Date:</td>
                            <td><asp:Literal ID="litRegistrationDate" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="trPromotionCode" runat="server" visible="false">
                            <td>Promotion Code:</td>
                            <td>
                                <asp:Literal ID="litPromotionCode" runat="server" Visible="false"></asp:Literal>
                                <asp:TextBox ID="txtPromotionCode" runat="server" CssClass="text_medium" Visible="false"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlPaymentDetails" runat="server" CssClass="divRegistrationPaymentDetails" Visible="false">
                <asp:Panel ID="pnlPaymentDetailsHdr" runat="server" CssClass="divPaymentDetailsHdr">
                    <asp:Literal ID="litPaymentDetailsHdr" runat="server"></asp:Literal>
                </asp:Panel>
                <asp:Panel ID="pnlPaymentDetailsContent" runat="server" CssClass="divPaymentDetailsContent">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td class="tdRegDetailLabel">Payment Method:</td>
                            <td>
                                <asp:Literal ID="litPaymentMethod" runat="server" Visible="false"></asp:Literal>
                                <asp:TextBox ID="txtPaymentMethod" runat="server" Visible="false" CssClass="text_small" MaxLength="250"></asp:TextBox>
                                <asp:CustomValidator ID="cvPaymentMethod" runat="server" ControlToValidate="txtPaymentMethod" CssClass="errmsg" Display="Dynamic" ValidationGroup="vgRegister"></asp:CustomValidator>
                                <asp:CustomValidator ID="cvPaymentMethod2" runat="server" ErrorMessage="<br />Please enter Payment Method." ControlToValidate="txtPaymentMethod" CssClass="errmsg" ClientValidationFunction="validatePayment" ValidateEmptyText="true" ValidationGroup="vgRegister" Display="Dynamic"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdRegDetailLabel">Transaction ID:</td>
                            <td>
                                <asp:Literal ID="litTransID" runat="server" Visible="false"></asp:Literal>
                                <asp:TextBox ID="txtTransID" runat="server" Visible="false" CssClass="text_small" MaxLength="250"></asp:TextBox>
                                <asp:CustomValidator ID="cvTransID" runat="server" ErrorMessage="<br />Please enter Transaction ID." ControlToValidate="txtTransID" CssClass="errmsg" ClientValidationFunction="validatePayment" ValidateEmptyText="true" ValidationGroup="vgRegister" Display="Dynamic"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdRegDetailLabel">Payment Amount:</td>
                            <td>
                                <asp:Literal ID="litPaymentAmount" runat="server" Visible="false"></asp:Literal>
                                <asp:TextBox ID="txtPaymentAmount" runat="server" Visible="false" CssClass="text_small" MaxLength="10"></asp:TextBox> <asp:Literal ID="litPayAmountCurrency" runat="server" Visible="false"></asp:Literal>
                                <asp:CustomValidator ID="cvPaymentAmount" runat="server" ClientValidationFunction="validateCurrency_client" ControlToValidate="txtPaymentAmount" CssClass="errmsg" ValidationGroup="vgRegister" Display="Dynamic"></asp:CustomValidator>
                                <asp:CustomValidator ID="cvPaymentAmount2" runat="server" ErrorMessage="<br />Please enter Payment Amount." ControlToValidate="txtPaymentAmount" CssClass="errmsg" ClientValidationFunction="validatePayment" OnServerValidate="validatePayment_Server" OnPreRender="cvPaymentAmount2_PreRender" ValidateEmptyText="true" ValidationGroup="vgRegister" Display="Dynamic"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr id="trPaymentDate" runat="server" visible="false">
                            <td class="tdRegDetailLabelNor">Payment Date:</td>
                            <td><asp:Literal ID="litPaymentDate" runat="server"></asp:Literal></td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlBtn" runat="server" CssClass="divAdmRegBtn">
                <div class="divBtnLeft">
                    <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="vgRegister"></asp:LinkButton>
                    <asp:HyperLink ID="hypClose" runat="server" Text="Close" ToolTip="Close" CssClass="btn btnBack"></asp:HyperLink>
                    <asp:LinkButton ID="lnkbtnPrint" runat="server" Text="Print" ToolTip="Print" CssClass="btn" Visible="false"></asp:LinkButton>
                </div>
                <div class="divBtnRight">
                    <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Cancel" ToolTip="Cancel" CssClass="btn btnCancel" OnClick="lnkbtnCancel_Click"></asp:LinkButton>
                </div>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
