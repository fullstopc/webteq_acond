﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class adm_admUserManual01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 6;
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.sectId = sectId;
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);
        AdmUserManual.fill();
    }
    #endregion
}
