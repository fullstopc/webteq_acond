﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class adm_admCMSControlPanel01 : AdminPage
{
    #region "Properties"
    protected int sectId = 1007;
    #endregion


    #region "Property Methods"
    public string currentPageName 
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        string pageTitle = "General Settings";
        Page.Title += clsMis.formatPageTitle(pageTitle, true);
        litPageTitle.Text = pageTitle;
        if (!IsPostBack)
        {
            Master.sectId = sectId;
            Master.parentId = 1;
            Master.moduleDesc = pageTitle;
        }
    }
    #endregion


    #region "Methods"
    #endregion


    #region "Button Events"
    #endregion
}
