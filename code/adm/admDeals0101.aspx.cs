﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_admDeals0101 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 59; //need change sectId and subSectId according to db
    protected string _currentPageName;
    protected int _mode = 1;
    protected int _dealId = 0;
    protected int _formSect = 1;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set { ViewState["FORMSECT"] = value; }
    }

    public int dealId
    {
        get
        {
            if (ViewState["DEALID"] == null)
            {
                return _dealId;
            }
            else
            {
                return int.Parse(ViewState["DEALID"].ToString());
            }
        }
        set
        {
            ViewState["DEALID"] = value;
        }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["id"]))
        {
            try
            {
                dealId = int.Parse(Request["id"]);
            }
            catch (Exception ex)
            {
                dealId = 0;
            }
        }
        else if (Session["NEWDEALID"] != null)
        {
            try
            {
                dealId = int.Parse(Session["NEWDEALID"].ToString());
            }
            catch (Exception ex)
            {
                dealId = 0;
            }
        }
        else if (Session["EDITDEALID"] != null)
        {
            try
            {
                dealId = int.Parse(Session["EDITDEALID"].ToString());
            }
            catch (Exception ex)
            {
                dealId = 0;
            }
        }

        if (!string.IsNullOrEmpty(Request["frm"]))
        {
            int intTryParse;
            if (int.TryParse(Request["frm"], out intTryParse))
            {
                formSect = intTryParse;
            }
            else
            {
                formSect = 1;
            }
        }

        if (Request["id"] != null || Session["EDITDEALID"] != null)
        {
            mode = 2;
        }

        clsAdmin adm = new clsAdmin();
        adm.extractAdminPageByName(clsAdmin.CONSTADMDEALSMANAGERNAME, 1);
        sectId = adm.pageId;

        Master.sectId = sectId;
        Master.parentId = sectId;
        Master.showSideMenu = true;
        Master.subSectId = clsMis.getSubSectId(sectId, formSect, 1);
        Master.id = dealId;
        Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

        if (!IsPostBack)
        {
            Session["ORIADDRESS"] = null;
            Session["ADDRESS"] = null;
            Session["DELADDRESS"] = null;
        }

        setPageProperties();

        Session["NEWDEALID"] = null;
        Session["EDITDEALID"] = null;
        Session["DELETEDDEALNAME"] = null;
        Session["NOCHANGE"] = null;
        Session["ERRMSG"] = null;
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        switch (mode)
        {
            case 1:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleAdd.Text").ToString(), true);
                break;
            case 2:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleEdit.Text").ToString(), true);
                break;
            default:
                break;
        }

        if (!IsPostBack)
        {
            switch (mode)
            {
                case 1: //Add Deal
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleAdd.Text").ToString();
                    break;
                case 2: // Edit Deal
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleEdit.Text").ToString();
                    break;
                default:
                    break;
            }

            if (Session["NEWDEALID"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";
                clsMis.resetListing();
            }
            else if (Session["EDITDEALID"] != null)
            {
                if (Session["NOCHANGE"] != null)
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
                }
                else
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";

                    clsMis.resetListing();
                }

                pnlAck.Visible = true;
            }
            else if (Session["DELETEDDEALNAME"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }
            else if (Session["ERRMSG"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
            }
        }

        switch (formSect)
        {
            case 1:
                ucAdmDealDetails.dealId = dealId;
                ucAdmDealDetails.mode = mode;
                ucAdmDealDetails.fill();

                pnlFormSect1.Visible = true;
                pnlFormSect2.Visible = false;
                pnlFormSect3.Visible = false;
                pnlFormSect4.Visible = false;
                break;
            case 2:

                pnlFormSect2.Visible = true;
                pnlFormSect1.Visible = false;
                pnlFormSect3.Visible = false;
                pnlFormSect4.Visible = false;
                break;
            case 3:
                ucAdmDealPrice.mode = mode;
                ucAdmDealPrice.dealId = dealId;
                ucAdmDealPrice.fill();

                pnlFormSect3.Visible = true;
                pnlFormSect1.Visible = false;
                pnlFormSect2.Visible = false;
                pnlFormSect4.Visible = false;
                break;
            case 4:
                ucAdmRelatedDeal.mode = mode;
                ucAdmRelatedDeal.dealId = dealId;
                ucAdmRelatedDeal.fill();

                pnlFormSect4.Visible = true;
                pnlFormSect1.Visible = false;
                pnlFormSect2.Visible = false;
                pnlFormSect3.Visible = false;
                break;
            default:
                break;
        }
    }
    #endregion
}
