﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class adm_userview : System.Web.UI.Page
{
    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["c"]))
            {
                string strCode = Request["c"];

                clsLogin login = new clsLogin();

                if (login.extractByCode(strCode))
                {
                    Session["ADMID"] = login.userId;
                    Session["ADMTYPE"] = login.userType;

                    string strSettings = login.userSettings;
                    if (!string.IsNullOrEmpty(strSettings))
                    {
                        try
                        {
                            strSettings = strSettings.Substring(0, strSettings.Length - clsLogin.CONSTDEFAULTSEPERATOR.ToString().Length);
                            string[] strSettingsSplit = strSettings.Split(clsLogin.CONSTDEFAULTSEPERATOR);

                            Session["ADMPAGELIMIT"] = strSettingsSplit[0];
                        }
                        catch (Exception ex)
                        {
                            clsLog.logErroMsg(ex.Message.ToString());
                        }
                    }

                    clsMis.resetListing();

                    login.deleteByCode(strCode);
                    login.resetAutoIncrement();
                }
            }
            // add by sh.chong 30 Apr 2015 - manage user view to specific page (product,event, page)
            string strParameter = "";
            if (!string.IsNullOrEmpty(Request["params"]) && !string.IsNullOrEmpty(Request["template"]))
            {
                string strParams = Request["params"].ToString().Replace(";","&").Replace(":","=");
                strParameter = "usr/"+Request["template"].ToString() + "?" + strParams;
            }
            // end by sh.chong 30 Apr 2015 - manage user view to specific page (product,event, page)
            Response.Redirect(ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + strParameter);
        }
    }
    #endregion
}
