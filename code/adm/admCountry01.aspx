﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admCountry01.aspx.cs" Inherits="adm_admCountry01" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header"><asp:Literal ID="litPageTitle" runat="server" meta:resourcekey="litPageTitle"></asp:Literal></div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlCountryDetailsContainer" runat="server" CssClass="divCountryDetailsContainer">
        <table cellpadding="0" cellspacing="0" border="0" class="formTbl">
            <tr>
                <td class="tdLabel nobr">Country Code:</td>
                <td class="tdMax"><asp:TextBox ID="txtCountryCode" runat="server" CssClass="text"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tdlabel">Country:</td>
                <td class="tdMax"><asp:TextBox ID="txtCountry" runat="server" CssClass="text_big_item"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Active:</td>
                <td><asp:CheckBox ID="chkboxActive" runat="server" Checked="true" /></td>
            </tr>
            <tr>
                <td class="tdSpacer">&nbsp;</td>
                <td class="tdSpacer">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:LinkButton ID="lnkbtnSaveCountry" runat="server" Text="Save" ToolTip="Save" CssClass="btn" OnClick="lnkbtnSaveCountry_Click"></asp:LinkButton>
                    <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CssClass="btnDelete" OnClick="lnkbtnDelete_Click" Visible="false"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlCountryListingContainer" runat="server" CssClass="divCountryDetailsContainer">
        <asp:GridView ID="gvCountry" runat="server" AutoGenerateColumns="false" AllowPaging="true" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom" CssClass="dataTbl" OnRowDataBound="gvCountry_RowDataBound" OnSorting="gvCountry_Sorting" OnPageIndexChanging="gvCountry_PageIndexChanging" OnRowCommand="gvCountry_RowCommand" DataKeyNames="COUNTRY_ID" EmptyDataText="No record found.">
            <PagerStyle CssClass="pg" HorizontalAlign="Right" />
            <AlternatingRowStyle CssClass="td_alt" />
            <Columns>
                <asp:TemplateField HeaderText="NO.">
                    <ItemStyle CssClass="tdNo" />
                    <ItemTemplate>
                        <asp:Literal ID="litNo" runat="server" Text="<%#Container.DataItemIndex + 1 %>"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Code" DataField="COUNTRY_CODE" SortExpression="COUNTRY_CODE"  />
                <asp:BoundField HeaderText="Country" DataField="COUNTRY_NAME" SortExpression="COUNTRY_NAME" />
                <asp:TemplateField HeaderText="Active">
                    <ItemStyle CssClass="td_small" />
                    <ItemTemplate>
                        <asp:Literal ID="litActive" runat="server" Text='<%#clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "COUNTRY_ACTIVE"))) %>'></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Order">
                    <ItemTemplate>
                        <asp:TextBox ID="txtCountryOrder" runat="server" CssClass="text_medium"></asp:TextBox>
                        <asp:HiddenField ID="hdnCountryOrder" runat="server" Value='<%#Convert.ToInt16(DataBinder.Eval(Container.DataItem, "COUNTRY_ORDER")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Action">
                    <ItemStyle CssClass="tdAct" />
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkbtnEdit" runat="server" CausesValidation="false" CommandName="cmdEdit"></asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnDelete" runat="server" CausesValidation="false" CommandName="cmdDelete"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pnlCountryButton" runat="server" CssClass="divCountryDetailsContainer">
        <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn" OnClick="lnkbtnSave_Click"></asp:LinkButton>
    </asp:Panel>
</asp:Content>