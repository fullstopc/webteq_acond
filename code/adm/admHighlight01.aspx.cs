﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.IO;

public partial class adm_admHighlight01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 30; //need change sectId according to db
    protected bool boolGrp = false;
    protected string _gvSortExpression = "HIGH_TITLE";
    protected string _gvSortDirection = clsAdmin.CONSTSORTASC;
    protected string _gvSort = "";

    protected Boolean _boolSearch = false;
    protected string strKeyword;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public Boolean boolSearch
    {
        get
        {
            if (ViewState["BOOLSEARCH"] == null)
            {
                return _boolSearch;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLSEARCH"]);
            }
        }
        set { ViewState["BOOLSEARCH"] = value; }
    }

    public string gvSortDirection
    {
        get { return ViewState["GVSORTDIRECTION"] as string ?? _gvSortDirection; }
        set { ViewState["GVSORTDIRECTION"] = value; }
    }

    public string gvSortExpression
    {
        get { return ViewState["GVSORTEXPRESSION"] as string ?? _gvSortExpression; }
        set { ViewState["GVSORTEXPRESSION"] = value; }
    }

    public string gvSort
    {
        get { return ViewState["GVSORT"] as string ?? _gvSort; }
        set { ViewState["GVSORT"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMEVENTMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            //txtKeyword.Attributes.Add("onkeydown", "javascript:if(event.keyCode == 13){document.getElementById('" + lnkbtnGo.ClientID + "').click(); return false;}");

            //clsConfig config = new clsConfig();
            //int pagesize = config.extractItemByNameGroup(clsConfig.CONSTNAMELISTINGSIZE, clsConfig.CONSTGROUPOTHERSETTINGS, 1) ? Convert.ToInt16(config.value) : clsAdmin.CONSTADMPAGESIZE;
            //gvHighlight.PagerSettings.FirstPageText = GetGlobalResourceObject("GlobalResource", "contentAdmFirstPageText.Text").ToString();
            //gvHighlight.PagerSettings.LastPageText = GetGlobalResourceObject("GlobalResource", "contentAdmLastPageText.Text").ToString();
            //gvHighlight.PageSize = pagesize;
            //bindGVData(gvHighlight, "HIGH_TITLE ASC");

            setPageProperties();

        }

        if (Session["DELETEDHIGHNAME"] != null)
        {
            pnlAck.Visible = true;
            litAck.Text = "Event (" + Session["DELETEDHIGHNAME"] + ") has been deleted successfully.";
            litAck.Text = "<div class=\"noticemsg\">" + litAck.Text + "</div>";

            clsMis.resetListing();

            Session["DELETEDHIGHNAME"] = null;
        }
        else
        {
            pnlAck.Visible = false;
            litAck.Text = "";
        }
    }

    protected void lnkbtnGo_Click(object sender, EventArgs e)
    {
        boolSearch = true;

        bindGVData(gvHighlight, "HIGH_TITLE ASC");
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        boolSearch = false;

        Response.Redirect(currentPageName);
    }

    protected void gvHighlight_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell tc in e.Row.Cells)
            {
                if (tc.HasControls())
                {
                    LinkButton lnkbtn = (LinkButton)tc.Controls[0];
                    //Image icon = new Image();
                    //icon.ImageUrl = "../img/adm/btn-cancel.gif";

                    if (lnkbtn != null && gvSortExpression == lnkbtn.CommandArgument)
                    {
                        //tc.Controls.Add(icon);
                        string strClass = (gvSortDirection == "DESC") ? "desc" : "asc";
                        tc.CssClass = strClass;
                    }


                }
            }
            if (boolGrp) { e.Row.Cells[4].CssClass = ""; }
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //LinkButton lnkbtnEdit = (LinkButton)e.Row.FindControl("lnkbtnEdit");
            LinkButton lnkbtnDelete = (LinkButton)e.Row.FindControl("lnkbtnDelete");
            HyperLink hypEdit = (HyperLink)e.Row.FindControl("hypEdit");

            int highId = int.Parse(gvHighlight.DataKeys[e.Row.RowIndex].Value.ToString());
            hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.NavigateUrl = "admHighlight0101.aspx?id=" + highId;

            //lnkbtnEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "admConfirmDeleteHighlight.Text").ToString() + "');";
            if (boolGrp) { e.Row.Cells[4].CssClass = ""; }
            
        }
        
        
    }

    protected void gvHighlight_Sorting(object sender, GridViewSortEventArgs e)
    {
        string strSortExpDir = "";

        if (e.SortExpression != gvSortExpression)
        {
            gvSortExpression = e.SortExpression;
            gvSortDirection = clsAdmin.CONSTSORTASC;
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }
        else
        {
            switch (gvSortDirection)
            {
                case clsAdmin.CONSTSORTASC:
                    gvSortDirection = clsAdmin.CONSTSORTDESC;
                    break;
                case clsAdmin.CONSTSORTDESC:
                    gvSortDirection = clsAdmin.CONSTSORTASC;
                    break;
            }

            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }

        gvSort = strSortExpDir;
        bindGVData(gvHighlight, strSortExpDir);
    }

    protected void gvHighlight_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvHighlight.PageIndex = e.NewPageIndex;

        if (gvSort != "") { bindGVData(gvHighlight, gvSort); }
        else { bindGVData(gvHighlight, "HIGH_TITLE ASC"); }
    }

    protected void gvHighlight_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "cmdEdit")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int highId = int.Parse(gvHighlight.DataKeys[rowIndex].Value.ToString());

            Response.Redirect("admHighlight0101.aspx?id=" + highId);
        }
        else if (e.CommandName == "cmdDelete")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int highId = int.Parse(gvHighlight.DataKeys[rowIndex].Value.ToString());

            string uploadPath = Server.MapPath(ConfigurationManager.AppSettings["uplBase"].ToString() + "/" + clsAdmin.CONSTADMHIGHFOLDER + "/");
            string strDeletedHighName = "";

            clsMis.performDeleteHighlight(highId, uploadPath, ref strDeletedHighName);

            Session["DELETEDHIGHNAME"] = strDeletedHighName;

            Response.Redirect(currentPageName);
        }
    }

    protected void lnkbtnExport_Click(object sender, EventArgs e)
    {
        ExportExcel();
        //ExportGV(gvHighlight, "EVENTLIST", (DataView)Session["HIGHDV"], true);
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
    }
    #endregion


    #region "Methods"
    protected void bindGVData(GridView gvHighRef, string gvSortExpDir)
    {
        clsHighlight high = new clsHighlight();
        DataSet ds = new DataSet();
        ds = high.getHighlightList(0);
        DataView dv = new DataView(ds.Tables[0]);
        dv.RowFilter = "1 = 1";
        Boolean boolFilter = false;

        if (boolSearch)
        {
            strKeyword = txtKeyword.Text.Trim();

            if (!string.IsNullOrEmpty(strKeyword))
            {
                dv.RowFilter += " AND HIGH_TITLE LIKE '%" + strKeyword + "%'";
                boolFilter = true;

                litItemFound.Text = "Search for <span class=\"SectKeyword\">" + strKeyword + "</span> | ";
            }
            else { litItemFound.Text = ""; }

            if (!string.IsNullOrEmpty(ddlCategory.SelectedValue))
            {
                dv.RowFilter += " AND V_GRP = " + ddlCategory.SelectedValue;
                boolFilter = true;
            }
        }

        if (boolFilter)
        {
            //pnlSearchForm.CssClass = "divSearchFormShow";
            //hypHideShow.CssClass = "linkHide";
        }
        else
        {
            //pnlSearchForm.CssClass = "divSearchForm";
            //hypHideShow.CssClass = "linkShow";
        }

        if (gvSortExpDir != "")
        {
            dv.Sort = gvSortExpDir;
        }
        else
        {
            dv.Sort = "HIGH_TITLE ASC";
        }

        boolGrp = (high.countHighlightList() > 0) ? true : false;

        Session["HIGHDV"] = dv;

        gvHighRef.DataSource = dv;
        gvHighRef.DataBind();

        if (boolSearch) { litItemFound.Text += dv.Count + " record(s) listed."; }
        else { litItemFound.Text = dv.Count + " record(s) listed."; }

        if (dv.Count > 0)
        {
            lnkbtnExport.Enabled = true;
        }
        else
        {
            lnkbtnExport.Enabled = false;
            lnkbtnExport.CssClass += " disabled";
        }

        if (clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITEVENT, dv.Count)) { hypAdd.Visible = false; }
    }

    protected void setPageProperties()
    {
        litPageTitle.Text = GetLocalResourceObject("litPageTitle.Text").ToString();

        DataSet dsCat = new DataSet();
        DataView dvCat;

        clsMis mis = new clsMis();
        dsCat = mis.getListByListGrp("HIGHLIGHT GROUP", 1);
        dvCat = new DataView(dsCat.Tables[0]);

        ddlCategory.DataSource = dvCat;
        ddlCategory.DataTextField = "LIST_NAME";
        ddlCategory.DataValueField = "LIST_VALUE";
        ddlCategory.DataBind();

        ListItem ddlCategoryDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlCategory.Items.Insert(0, ddlCategoryDefaultItem);
    }

    protected void ExportGV(GridView gvRef, string strFileTitle, DataView dvItems, Boolean boolHideLastColumn)
    {
        DateTime dt = DateTime.Now;
        string strFileName = strFileTitle + "_" + dt.Day + dt.Month + dt.Year + ".xls";
        string strAttachment = "attachment; filename=" + strFileName;

        Response.ClearContent();
        Response.AddHeader("content-disposition", strAttachment);
        Response.ContentType = "application/vnd.xls";
        Response.ContentEncoding = System.Text.Encoding.Unicode;
        Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        gvRef.AllowPaging = false;
        gvRef.DataSource = dvItems;
        gvRef.DataBind();

        for (int i = 0; i < gvRef.Columns.Count; i++)
        {
            if (gvRef.HeaderRow.Cells[i].CssClass == "hiddenCol")
            {
                gvRef.HeaderRow.Cells[i].CssClass = "showCol";
            }

            gvRef.HeaderRow.Cells[i].Enabled = false;
        }

        for (int i = 0; i < gvRef.Rows.Count; i++)
        {
            GridViewRow row = gvRef.Rows[i];

            for (int j = 0; j < gvRef.Columns.Count; j++)
            {
                if (row.Cells[j].CssClass == "hiddenCol")
                {
                    row.Cells[j].CssClass = "showCol";
                }
            }
        }

        if (boolHideLastColumn)
        {
            int intColHide = gvRef.Columns.Count - 1;

            gvRef.HeaderRow.Cells[intColHide].Visible = false;
            gvRef.Columns[intColHide].Visible = false;
        }

        gvRef.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        int intItemID = Convert.ToInt16(hdnItemID.Value);
        string uploadPath = Server.MapPath(ConfigurationManager.AppSettings["uplBase"].ToString() + "/" + clsAdmin.CONSTADMHIGHFOLDER + "/");
        string strDeletedHighName = "";

        clsMis.performDeleteHighlight(intItemID, uploadPath, ref strDeletedHighName);

        Session["DELETEDHIGHNAME"] = strDeletedHighName;

        Response.Redirect(currentPageName);
    }
    private void ExportExcel()
    {
        DateTime datetime = DateTime.Now;
        string filename = "EVENTLIST_" + datetime.Day + datetime.Month + datetime.Year + ".xls";
        string attachment = "attachment; filename=" + filename;

        DataTable dt = new clsHighlight().getHighlightList(0).Tables[0];

        GridView GridView1 = new GridView();
        GridView1.AllowPaging = false;
        GridView1.DataSource = dt;
        GridView1.DataBind();

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", attachment);
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel ";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);

        GridView1.RenderControl(hw);
        Response.Output.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }
    #endregion
}
