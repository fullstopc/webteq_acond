﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class adm_adm : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 1005;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        string title = "Job";
        Page.Title += clsMis.formatPageTitle(title, true);
        litPageTitle.Text = title;

        if (!IsPostBack)
        {

            Master.sectId = sectId;
            Master.moduleDesc = title;

            setPageProperties();

        }

        new Acknowledge(Page)
        {
            container = pnlAck,
            msg = litAck
        }.init();
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
    }
    #endregion

    #region "Button Events"
    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
    }
    #endregion
}