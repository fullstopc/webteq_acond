﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.IO;

public partial class adm_admPage01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 5; //need change sectId according to db
    protected string _gvSortExpression = "PAGE_TITLE";
    protected string _gvSortDirection = clsAdmin.CONSTSORTASC;
    protected string _gvSort = "";

    protected Boolean _boolSearch = false;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public Boolean boolSearch
    {
        get
        {
            if (ViewState["BOOLSEARCH"] == null)
            {
                return _boolSearch;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLSEARCH"]);
            }
        }
        set { ViewState["BOOLSEARCH"] = value; }
    }

    public string gvSortDirection
    {
        get { return ViewState["GVSORTDIRECTION"] as string ?? _gvSortDirection; }
        set { ViewState["GVSORTDIRECTION"] = value; }
    }

    public string gvSortExpression
    {
        get { return ViewState["GVSORTEXPRESSION"] as string ?? _gvSortExpression; }
        set { ViewState["GVSORTEXPRESSION"] = value; }
    }

    public string gvSort
    {
        get { return ViewState["GVSORT"] as string ?? _gvSort; }
        set { ViewState["GVSORT"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMPAGEMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            txtKeyword.Attributes.Add("onkeydown", "javascript:if(event.keyCode == 13){document.getElementById('" + lnkbtnGo.ClientID + "').click(); return false;}");

            setPageProperties();

         }

        if (Session["DELETEDPAGENAME"] != null)
        {
            pnlAck.Visible = true;
            litAck.Text = "<div class=\"noticemsg\">Page (" + Session["DELETEDPAGENAME"].ToString() + ") has been deleted successfully.</div>";

            clsMis.resetListing();

            Session["DELETEDPAGENAME"] = null;
        }
        else
        {
            litAck.Text = "";
            pnlAck.Visible = false;
        }
    }

    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        DataSet ds = new DataSet();
        DataView dv = new DataView();

        clsPage page = new clsPage();
        ds = page.getPageList(0);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "PAGE_DISPLAYNAME ASC";

        ddlParent.DataSource = dv;
        ddlParent.DataTextField = "PAGE_DISPLAYNAME";
        ddlParent.DataValueField = "PAGE_DISPLAYNAME";
        ddlParent.DataBind();

        ListItem ddlParentDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlParent.Items.Insert(0, ddlParentDefaultItem);

        ListItem ddlParentDefaultItem2 = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlRoot.Text").ToString(), "0");
        ddlParent.Items.Insert(1, ddlParentDefaultItem2);

        clsMis mis = new clsMis();
        ds = new DataSet();
        ds = mis.getListByListGrp("PAGE TEMPLATE", 1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "LIST_ORDER ASC";

        ddlTemplate.DataSource = dv;
        ddlTemplate.DataTextField = "LIST_NAME";
        ddlTemplate.DataValueField = "LIST_VALUE";
        ddlTemplate.DataBind();

        ListItem ddlTemplateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlTemplate.Items.Insert(0, ddlTemplateDefaultItem);


        ds = new DataSet();
        ds = mis.getListByListGrp("LANGUAGE", 1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "LIST_ORDER ASC";

        ddlLanguage.DataSource = dv;
        ddlLanguage.DataTextField = "LIST_NAME";
        ddlLanguage.DataValueField = "LIST_VALUE";
        ddlLanguage.DataBind();

        ListItem ddlLanguageDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlLanguage.Items.Insert(0, ddlLanguageDefaultItem);


        ddlActive.Items.Add(new ListItem("Yes", "1"));
        ddlActive.Items.Add(new ListItem("No", "0"));

        ListItem ddlActiveDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlActive.Items.Insert(0, ddlActiveDefaultItem);


        ddlShowPageTitle.Items.Add(new ListItem("Yes", "1"));
        ddlShowPageTitle.Items.Add(new ListItem("No", "0"));

        ListItem ddlShowPageTitleDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlShowPageTitle.Items.Insert(0, ddlShowPageTitleDefaultItem);


        ddlShowInMenu.Items.Add(new ListItem("Yes", "1"));
        ddlShowInMenu.Items.Add(new ListItem("No", "0"));

        ListItem ddlShowInMenuDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlShowInMenu.Items.Insert(0, ddlShowInMenuDefaultItem);

        // hide add button if exceed page limit
        int intPageCount = new clsPage().getPageList(0).Tables[0].Rows.Count;
        hypAdd.Visible = !clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITPAGE, intPageCount - 1);
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        int intPageID = Convert.ToInt16(hdnPageID.Value);

        string strDeletedName = "";

        int intOldParent = 0;
        int intOldOrder = 0;

        clsPage page = new clsPage();
        int intRecordAffected = 0;

        page.extractPageById(intPageID, 0);
        strDeletedName = page.title;
        intOldParent = page.parent;
        intOldOrder = page.order;

        intRecordAffected = page.deletePageById(intPageID);

        if (intRecordAffected == 1)
        {
            int intNewParent = page.otherId;

            page.updateOrder(intOldParent, intOldOrder);
            page.updatePage(intNewParent, intPageID, Convert.ToInt16(Session["ADMID"]));
            page.updatePage(intPageID);

            Session["DELETEDPAGENAME"] = strDeletedName;
        }
        else
        {
            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to delete page (" + strDeletedName + "). Please try again.</div>";
        }

        Response.Redirect(currentPageName);
    }
    #endregion
}
