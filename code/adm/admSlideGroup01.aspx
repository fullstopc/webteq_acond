﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admSlideGroup01.aspx.cs" Inherits="adm_admSlideGroup01" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmCheckAccess.ascx" TagName="AdmCheckAccess" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <uc:AdmCheckAccess ID="ucAdmCheckAccess4" runat="server" />
    <div class="main-content__header">
        <div class="title"><asp:Literal ID="litPageTitle" runat="server" meta:resourcekey="litPageTitle"></asp:Literal></div>
        <div class="divListingSplitter"></div>
        <div class="action">
			<asp:Panel ID="pnlListingAction" runat="server" CssClass="divListingAction">
                <asp:HyperLink ID="hypAdd" runat="server" NavigateUrl="admSlideGroup0101.aspx" CssClass="hypListingAction add"></asp:HyperLink>
                <div class="divListingSplitter"></div>
                <asp:LinkButton ID="lnkbtnExport" runat="server" Text="Export to Excel" ToolTip="Export to Excel" CssClass="hypListingAction export" OnClick="lnkbtnExport_Click"></asp:LinkButton>
            </asp:Panel>
        </div>
        <div class="divListingSplitter"></div>
        <div class="filter">
            <div class="divFilterContainer">
                <div class="divFilter">
                    <asp:TextBox ID="txtKeyword" runat="server" placeholder="Keyword Search" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
                    <a href="#" class="btnDropdown" onclick="toggleFilterOption();"></a>
                    <asp:Literal ID="litKeywordRule" runat="server" meta:resourcekey="litKeywordRule" visible="false"></asp:Literal>
                </div>
                <div class="divFilterMore">
                    <div></div>
                    <div>
                        <table class="tblSearch">
                            <tr>
                                <td class="">
                                    <div class="divFilterLabel">Active:</div>
                                    <asp:DropDownList ID="ddlActive" runat="server" CssClass="ddl"></asp:DropDownList>

                                </td>
                                <td class="">
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
            <div class="divFilterAction" runat="server" visible="false"><asp:Button ID="lnkbtnGo" runat="server" Text="" ToolTip="Search" CssClass="btn btnSearch" OnClick="lnkbtnGo_Click"/></div>
        </div>
    </div>
    <div class="divListing">
        <asp:Panel ID="pnlListingHdr" runat="server" CssClass="divListingHdr">
            <div class="divListingHdrLeft"><asp:Literal ID="litFound" runat="server"></asp:Literal></div>
        </asp:Panel>
        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
            <div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
        </asp:Panel>
        <uc:CmnDatatable ID="ucCmnDatatable" runat="server" />
        <asp:HiddenField runat="server" ID="hdnItemID" />
        <asp:LinkButton ID="lnkbtnDelete" runat="server" OnClick="lnkbtnDelete_Click" style="display:none;"></asp:LinkButton>

        <%-- Visible false at 11 Oct 2016 --%>
        <div class="divListingData" runat="server" visible="false">
            <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom" CssClass="dataTbl" OnRowDataBound="gvItems_RowDataBound" OnSorting="gvItems_Sorting" OnPageIndexChanging="gvItems_PageIndexChanging" OnRowCommand="gvItems_RowCommand" DataKeyNames="GRP_ID" EmptyDataText="No record found.">
                <PagerStyle CssClass="pg" HorizontalAlign="Right" />
                <AlternatingRowStyle CssClass="td_alt" />
                <Columns>
                    <asp:TemplateField HeaderText="no.">
                        <ItemStyle CssClass="tdNo" />
                        <ItemTemplate>
                            <asp:Literal ID="litNo" runat="server" Text="<%# Container.DataItemIndex + 1 %>"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="name" DataField="GRP_NAME" SortExpression="GRP_NAME" ItemStyle-CssClass="" />
                    <asp:BoundField HeaderText="display name" DataField="GRP_DNAME" SortExpression="GRP_DNAME" />
                    <asp:TemplateField HeaderText="show on top" SortExpression="GRP_SHOWTOP">
                        <ItemStyle CssClass="td_small" />
                        <ItemTemplate>
                            <asp:Literal ID="litShowTop" runat="server" Text='<%# clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "GRP_SHOWTOP"))) %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="active" SortExpression="GRP_ACTIVE">
                        <ItemStyle CssClass="td_small" />
                        <ItemTemplate>
                            <asp:Literal ID="litActive" runat="server" Text='<%# clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "GRP_ACTIVE"))) %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="action">
                        <ItemStyle CssClass="tdAct" />
                        <ItemTemplate>
                            <asp:HyperLink ID="hypEdit" CssClass="lnkbtn lnkbtnEdit" runat="server"></asp:HyperLink>
                            <%--<asp:LinkButton ID="lnkbtnEdit" runat="server" CausesValidation="false" CommandName="cmdEdit" CssClass="lnkbtn lnkbtnEdit"></asp:LinkButton>--%>
                            <%# (Convert.ToInt16(DataBinder.Eval(Container.DataItem, "GRP_OTHER")) > 0) ? string.Empty : "<span class='spanSplitter'>|</span>" %>
                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CausesValidation="false" CommandName="cmdDelete" CssClass="lnkbtn lnkbtnDelete"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>

    <script type="text/javascript">

        $(function () {
            var columns = [
                    { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                    { "data": "GRP_NAME", "label": "Name", "order": 1, },
                    { "data": "GRP_DNAME", "label": "Display Name", "order": 2, },
                    { "data": "GRP_SHOWTOP", "bSearchable": false, "label": "Show On Top", "order": 3, "convert": "active" },
                    { "data": "GRP_ACTIVE", "bSearchable": false, "label": "Active", "order": 4, "convert": "active" },
                    { "data": "action", "bSortable": false, "bSearchable": false, "label": "Action", "order": 5,"exportable":false },
            ];
            var columnSort = columns.reduce(function (prev, curr) {
                prev[curr.data] = curr.order;
                return prev;
            }, {});

            var columnDefs = [{
                "render": function (data, type, row) {
                    var lnkbtnEdit = "<a class='lnkbtn lnkbtnEdit' href='admSlideGroup0101.aspx?id=" + row.GRP_ID + "' title='Edit'></a>";
                    var lnkbtnDelete = "<a class='lnkbtn lnkbtnDelete' title='Delete' data-id='"+row.GRP_ID+"'></a>";
                    return lnkbtnEdit + lnkbtnDelete;
                },
                "targets": columnSort["action"]
            }];
            var properties = {
                columns: columns,
                columnDefs: columnDefs,
                columnSort: columnSort,
                func: "getSlideGroupData",
                name: "slide_group",
            };
            var $datatable = callDataTables(properties);

            // Filter
            $("#<%= ddlActive.ClientID %>").change(function () {
                $datatable
               .columns(columnSort["GRP_ACTIVE"])
               .search(this.value)
               .draw();
            });

            // Export
            $("#<%= lnkbtnExport.ClientID %>").click(function (e) {
                var $table = $('.dataTable').clone();
                $table.attr("border", "1");
                $table.find("th:last-child,td:last-child").remove();
                window.open('data:application/vnd.ms-excel,' + encodeURIComponent($table.get(0).outerHTML));

                e.preventDefault();
            });

            $(document).on("click", ".lnkbtnDelete", function () {
                if (confirm("<%= HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeletePage.Text") %>")) {
                    var pageID = $(this).attr("data-id");
                    document.getElementById('<%= hdnItemID.ClientID %>').value= pageID;
                    document.getElementById('<%= lnkbtnDelete.ClientID %>').click();
                }
            })
            
        });
    </script>
</asp:Content>
