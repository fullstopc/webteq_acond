﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.IO;
using System.Configuration;

public partial class adm_admMember01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 21;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        string title = "Resident";
        Page.Title += clsMis.formatPageTitle(title, true);
        litPageTitle.Text = title;

        if (!IsPostBack)
        {
            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMMEMBERMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.moduleDesc = title;

            setPageProperties();
        }

        new Acknowledge(Page) { container = pnlAck, msg = litAck }.init();
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        int intItemID = Convert.ToInt16(hdnItemID.Value);
        string name = "";

        clsMember member = new clsMember();
        member.ID = intItemID;

        if (member.extractByID(intItemID))
        {
            name = member.name;
        }

        if (member.delete() == 1)
        {
            Session[clsKey.SESSION_DELETED] = 1;
            Session[clsKey.SESSION_MSG] = "Member " + name + " delete successfully.";
        }
        Response.Redirect(currentPageName);
    }

    #endregion
}