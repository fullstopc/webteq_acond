﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="admDeals01.aspx.cs" Inherits="adm_admDeals01" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>

<%--Soo Shin -- 2013-11-20 --%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">

    <div class="main-content__header">
        <div class="title"><asp:Literal ID="litPageTitle" runat="server" meta:resourcekey="litPageTitle"></asp:Literal></div>
        <div class="divListingSplitter"></div>
        <div class="action">
			<asp:Panel ID="pnlListingAction" runat="server" CssClass="divListingAction">
                <asp:HyperLink ID="hypAddDeal" runat="server" Text="Add Deal" ToolTip="Add Deal" NavigateUrl="admDeals0101.aspx" CssClass="hypListingAction add"></asp:HyperLink>
				<div class="divListingSplitter"></div>
                <asp:LinkButton ID="lnkbtnExport" runat="server" Text="Export to Excel" ToolTip="Export to Excel" CssClass="hypListingAction export" OnClick="lnkbtnExport_Click"></asp:LinkButton>
            </asp:Panel>
        </div>
        <div class="divListingSplitter"></div>
        <div class="filter">
            <div class="divFilterContainer">
                <div class="divFilter">
                    <asp:TextBox ID="txtKeyword" runat="server" placeholder="Keyword Search" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
                    <a href="#" class="btnDropdown" onclick="toggleFilterOption();"></a>
                    <asp:Literal ID="litKeywordRule" runat="server" meta:resourcekey="litKeywordRule" visible="false"></asp:Literal>
                </div>
                <div class="divFilterMore">
                    <div></div>
                    <div>
                        <table class="tblSearch">
							<tr>
                                <td class="">
									<div class="divFilterLabel">Business Type:</div>
                                    <asp:DropDownList ID="ddlBizType" runat="server" class="ddl_medium"></asp:DropDownList>
                                </td>
								<td>
									<div class="divFilterLabel">Area:</div>
                                    <asp:DropDownList ID="ddlArea" runat="server" class="ddlArea"></asp:DropDownList>
								</td>
                            </tr>
							<tr>
                                <td class="">
									<div class="divFilterLabel">Show Top:</div>
                                    <asp:DropDownList ID="ddlShowTop" runat="server" class="ddl_medium"></asp:DropDownList>
                                </td>
								<td>
									<div class="divFilterLabel">Active:</div>
                                    <asp:DropDownList ID="ddlActive" runat="server" class="ddlArea"></asp:DropDownList>
								</td>
                            </tr>
							<tr>
                                <td class="">
									<div class="divFilterLabel">Merchant:</div>
                                    <asp:DropDownList ID="ddlMerchant" runat="server" class="ddl_medium"></asp:DropDownList>
                                </td>
								<td></td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
            <div class="divFilterAction"><asp:Button ID="lnkbtnSearch" runat="server" Text="" ToolTip="Search" CssClass="btn btnSearch" OnClick="lnkbtnSearch_Click"/></div>
        </div>
    </div>
    <div class="divListing">
        <div class="divListingHdr">
            <div class="divListingHdrLeft"><asp:Literal ID="litItemFound" runat="server"></asp:Literal></div>
            
        </div>
        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
            <div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
        </asp:Panel>
        <div class="divListingData">
            <asp:GridView ID="gvDeal" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom" CssClass="dataTbl" OnRowDataBound="gvDeal_RowDataBound" OnSorting="gvDeal_Sorting" OnPageIndexChanging="gvDeal_PageIndexChanging" OnRowCommand="gvDeal_RowCommand" DataKeyNames="DEAL_ID" EmptyDataText="No record found.">
                <PagerStyle CssClass="pg" HorizontalAlign="Right" />
                <AlternatingRowStyle CssClass="td_alt" />
                <Columns>
                    <asp:TemplateField HeaderText="no.">
                        <ItemStyle CssClass="tdNo" />
                        <ItemTemplate>
                            <asp:Literal ID="litNo" runat="server" Text="<%#Container.DataItemIndex + 1 %>"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="start date">
                        <ItemTemplate>
                            <asp:Literal ID="litStartDate" runat="server"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="end date">
                        <ItemTemplate>
                            <asp:Literal ID="litEndDate" runat="server"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="deal code" DataField="DEAL_CODE" SortExpression="DEAL_CODE" />
                    <asp:BoundField HeaderText="deal name" DataField="DEAL_NAME" SortExpression="DEAL_NAME" />
                    <asp:BoundField HeaderText="merchant name" />
                    <asp:BoundField HeaderText="business type" />
                    <asp:BoundField HeaderText="area" />
                    <asp:TemplateField HeaderText="action">
                        <ItemStyle CssClass="tdAct" />
                        <ItemTemplate>
                            <asp:HyperLink ID="hypEdit" CssClass="lnkbtn lnkbtnEdit" runat="server"></asp:HyperLink>
                            <span class="spanSplitter">|</span>
                            <asp:LinkButton ID="lnkbtnDelete" CssClass="lnkbtn lnkbtnDelete" runat="server" CausesValidation="false" CommandName="cmdDelete"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
<%--Soo Shin -- 2013-11-20 --%>