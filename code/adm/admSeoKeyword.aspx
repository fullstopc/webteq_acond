﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" EnableViewState="true" CodeFile="admSeoKeyword.aspx.cs" Inherits="adm_admSeoKeyword" %>
<%@ MasterType VirtualPath="~/mst/blank.master" %>
<%@ Register Src="~/ctrl/ucAdmCheckAccess.ascx" TagName="AdmCheckAccess" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
   <asp:ScriptManager runat="server"></asp:ScriptManager>
    
   <div class="form seokeyword">
       <div class="row">
            <div class="column">
                <div class="ack-container" runat="server" id="divAck" visible="false">
                    Save Succesfully.
                </div>
            </div>
        </div>
       <div class="row">
           <div class="column"><h1>Seo Keyword Planing</h1></div>
       </div>
       <asp:UpdatePanel runat="server" ID="upnlProd">
            <ContentTemplate>
                <div class="row">
                    <div class="column">
                        <h2>Main Products & Services</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="column-10"><asp:TextBox runat="server" ID="txtProduct"></asp:TextBox></div>
                    <div class="column-2"><asp:LinkButton runat="server" ID="lnkbtnAddProduct" CssClass="button" OnClick="lnkbtnAddProduct_Click">Add</asp:LinkButton></div>
                </div>
                <div class="row">
                    <div class="column">
                        <asp:Repeater runat="server" ID="rptSeoProduct">
                            <HeaderTemplate><div class="list sortable"></HeaderTemplate>
                            <FooterTemplate></div></FooterTemplate>
                            <ItemTemplate>
                                <asp:Panel CssClass="flexbox item" runat="server" ID="pnlItem" data-hidden='<%# Eval("hidden") %>'>
                                    <div class="index"><%# Container.ItemIndex + 1 %>.</div>
                                    <div class="name">
                                        <asp:Literal runat="server" ID="litKeyword" Text='<%# Eval("SEO_KEYWORD") %>'></asp:Literal>
                                    </div>
                                    <div class="action">
                                        <i class="icon material-icons" onclick="sortUp(this);">arrow_upward</i>
                                        <i class="icon material-icons" onclick="sortDown(this);">arrow_downward</i>
                                        <i class="icon material-icons" onclick="sortHidden(this);">delete_forever</i>
                                        <div class="checkbox-group">
                                            <label>
                                                <asp:CheckBox runat="server" ID="chkboxImportant" Checked='<%# Convert.ToString(Eval("SEO_IMPORTANT")) == "1" ? true : false %>' />
                                                <span>Important</span>
                                            </label>
                                        </div>
                                        <input type="hidden" runat="server" id="hdnIndex" class="hdnIndex" value='<%# Container.ItemIndex + 1 %>' />
                                        <input type="hidden" runat="server" id="hdnHidden" class="hdnHidden" value='<%# Eval("hidden") %>' />
                                    </div>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </ContentTemplate>
       </asp:UpdatePanel>
      <asp:UpdateProgress ID="uprgProd" AssociatedUpdatePanelID="upnlProd" runat="server" DisplayAfter="10">
          <ProgressTemplate>
              <uc:AdmLoading runat="server" />
          </ProgressTemplate>
      </asp:UpdateProgress>
        <asp:UpdatePanel runat="server" ID="upnlArea">
            <ContentTemplate>
                <div class="row">
                   <div class="column">
                        <h2>Area(s)</h2>
                   </div>
               </div>
                <div class="row">
                    <div class="column-10"><asp:TextBox runat="server" ID="txtArea"></asp:TextBox></div>
                    <div class="column-2"><asp:LinkButton runat="server" ID="lnkbtnAddArea" CssClass="button" OnClick="lnkbtnAddArea_Click">Add</asp:LinkButton></div>
                </div>
                <div class="row">
                    <div class="column">
                        <asp:Repeater runat="server" ID="rptSeoArea">
                            <HeaderTemplate><div class="list sortable"></HeaderTemplate>
                            <FooterTemplate></div></FooterTemplate>
                            <ItemTemplate>
                                <asp:Panel CssClass="flexbox item" runat="server" ID="pnlItem" data-hidden='<%# Eval("hidden") %>'>
                                    <div class="index"><%# Container.ItemIndex + 1 %>.</div>
                                    <div class="name">
                                        <asp:Literal runat="server" ID="litKeyword" Text='<%# Eval("SEO_KEYWORD") %>'></asp:Literal>
                                    </div>
                                    <div class="action">
                                        <i class="icon material-icons" onclick="sortUp(this);">arrow_upward</i>
                                        <i class="icon material-icons" onclick="sortDown(this);">arrow_downward</i>
                                        <i class="icon material-icons" onclick="sortHidden(this);">delete_forever</i>
                                        <div class="checkbox-group">
                                            <label>
                                                <asp:CheckBox runat="server" ID="chkboxImportant" Checked='<%# Convert.ToString(Eval("SEO_IMPORTANT")) == "1" ? true : false %>' />
                                                <span>Important</span>
                                            </label>
                                        </div>
                                        <input type="hidden" runat="server" id="hdnIndex" class="hdnIndex" value='<%# Container.ItemIndex + 1 %>' />
                                        <input type="hidden" runat="server" id="hdnHidden" class="hdnHidden" value='<%# Eval("hidden") %>' />
                                    </div>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </ContentTemplate>
       </asp:UpdatePanel>
      <asp:UpdateProgress ID="uprgArea" AssociatedUpdatePanelID="upnlArea" runat="server" DisplayAfter="10">
          <ProgressTemplate>
              <uc:AdmLoading runat="server" />
          </ProgressTemplate>
      </asp:UpdateProgress>
        <div class="row">
            <div class="column-6">
                <h2>Company Name</h2>
                <asp:TextBox runat="server" ID="txtCompName"></asp:TextBox>
            </div>
            <div class="column-6">
                <h2>Company Short Name</h2>
                <asp:TextBox runat="server" ID="txtCompNameShort"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <div class="column-10"></div>
            <div class="column-2"><asp:LinkButton runat="server" ID="lnkbtnSave" CssClass="button" OnClick="lnkbtnSave_Click">Save</asp:LinkButton></div>
        </div>
   </div>
    <script>
        function sortInit() {
            $(".list.sortable").sortable({
                update: function (event, ui) {
                    var $sortable = $(this);
                    sortIndex($sortable);
                }
            });
        };

        function sortHidden(element) {
            $(element).parentsUntil(".item").parent().attr("data-hidden", 1);
            $(element).parentsUntil(".item").parent().find(".hdnHidden").val("1");
            sortIndex($(element).parentsUntil(".sortable.list").parent());
        }
        function sortUp(element) {
            var $parent = $(element).parentsUntil(".sortable.list").parent();
            var $this = $(element).parentsUntil(".item").parent();
            if ($parent.find(".item").index($this) > 0) {

                $this.fadeOut(200, function () {
                    $this.insertBefore($this.prev());
                    $this.fadeIn(200, function () {
                        sortIndex($parent);
                    });
                });
                sortIndex($parent);
            }

        }
        function sortDown(element) {
            var $parent = $(element).parentsUntil(".sortable.list").parent();
            var $this = $(element).parentsUntil(".item").parent();
            var length = $parent.find(".item:visible").length;
            if ($parent.find(".item").index($this) < length - 1) {

                $this.fadeOut(200, function () {
                    $this.insertAfter($this.next());
                    $this.fadeIn(200, function () {
                        sortIndex($parent);
                    });
                });
            }
        }
        function sortIndex($sortable) {
            $sortable.find(" > .item:visible ").each(function (i) {
                $(this).find(".index").html(i + 1);
                $(this).find(".hdnIndex").val(i + 1);
            });
        }
        
        $(function (){
            sortInit();
        })

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function (s, e) {
            sortInit();
        });
    </script>
</asp:Content>

