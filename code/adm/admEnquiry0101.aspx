﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admEnquiry0101.aspx.cs" Inherits="adm_admEnquiry0101" %>
<%@ MasterType VirtualPath="~/mst/blank.master" %>
<%@ Register Src="~/ctrl/ucAdmEnquiry.ascx" TagName="AdmEnquiry" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlForm" runat="server" CssClass="divForm">
        <uc:AdmEnquiry ID="ucAdmEnquiry" runat="server" pageListingURL="admEnquiry01.aspx" />
    </asp:Panel>
</asp:Content>


