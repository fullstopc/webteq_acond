﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="undoStatus.aspx.cs" Inherits="adm_undoStatus" %>
<%@ MasterType VirtualPath="~/mst/blank.master" %>
<%@ Register Src="~/ctrl/ucAdmCheckAccess.ascx" TagName="AdmCheckAccess" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <asp:Panel ID="pnlUndoStatus" runat="server" CssClass="divUndoStatus">
        <asp:Panel ID="pnlUndoStatusInner" runat="server" CssClass="divUndoStatusInner">
            <table cellpadding="0" cellspacing="0" class="formTbl">
                <tr>
                    <td class="tdUndoStatus">
                        <asp:Panel ID="pnlUndoStatusForm" runat="server" CssClass="divUndoStatusForm">
                            <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
                                <asp:Literal ID="litAck" runat="server"></asp:Literal>
                                <asp:Panel ID="pnlAckBtn" runat="server" class="divAckBtn">
                                    <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn" CausesValidation="false"></asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnContinue" runat="server" Text="Continue" ToolTip="Continue" class="btn" CausesValidation="false" OnClick="lnkbtnContinue_Click"></asp:LinkButton>
                                </asp:Panel>
                            </asp:Panel>
                            <asp:Panel ID="pnlForm" runat="server">
                                <table cellpadding="0" cellspacing="0" id="tblLogin">
                                    <tr>
                                        <th colspan="2">UNDO STATUS:</th>
                                    </tr>
                                    <tr>
                                        <td class="tdLabel">Email:</td>
                                        <td>
                                            <asp:TextBox ID="txtEmail" runat="server" class="text" MaxLength="250" ValidationGroup="cancel"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="<br />Please enter Email." ControlToValidate="txtEmail" class="errmsg" Display="dynamic" ValidationGroup="cancel"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="<br />Please enter valid Email." Display="Dynamic" ControlToValidate="txtEmail" CssClass="errmsg" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$" ValidationGroup="cancel"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdLabel">Password:</td>
                                        <td>
                                            <asp:TextBox ID="txtPassword" runat="server" class="text" TextMode="Password" MaxLength="20" ValidationGroup="cancel"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="<br />Please enter Password." ControlToValidate="txtPassword" class="errmsg" Display="dynamic" ValidationGroup="cancel"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdSpacer">&nbsp;</td>
                                        <td class="tdSpacer">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnConfirm" runat="server" Text="Confirm Undo" ToolTip="Confirm Undo" class="btn" OnClick="lnkbtnConfirm_Click" ValidationGroup="cancel" />
                                            <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Back" ToolTip="Back" CssClass="btn"></asp:LinkButton>
                                        </td>
                                    </tr>          
                                </table> 
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>
</asp:Content>

