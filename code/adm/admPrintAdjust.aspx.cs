﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class adm_admPrintAdjust : System.Web.UI.Page
{
    #region "Properties"
    protected int _reId;

    protected int intItemCount = 0;
    #endregion


    #region "Property Methods"
    public int reId
    {
        get { return _reId; }
        set { _reId = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;
                if (int.TryParse(Request["id"], out intTryParse))
                {
                    reId = intTryParse;
                }
            }

            setPageProperties();
            fillForm();
            bindRptData();
        }
    }

    protected void rptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            intItemCount += 1;

            Literal litNo = (Literal)e.Item.FindControl("litNo");
            litNo.Text = intItemCount.ToString();

            string strItemCode = DataBinder.Eval(e.Item.DataItem, "AI_PRODCODE").ToString();
            string strItemName = DataBinder.Eval(e.Item.DataItem, "AI_PRODNAME").ToString();

            Literal litItem = (Literal)e.Item.FindControl("litItem");
            litItem.Text = strItemCode + " " + strItemName;
        }
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
    }

    protected void fillForm()
    {
        clsAdjust ad = new clsAdjust();
        if (ad.extractItemById(reId, 0))
        {
            litDetails.Text = ad.adTitle + "<br />" + ad.adDate.ToString("dd MMM yyyy") + "<br /><br />Adjusted by: " + ad.adAdjustBy;
        }
    }

    protected void bindRptData()
    {
        clsAdjust ad = new clsAdjust();
        DataSet ds = new DataSet();
        ds = ad.getItemItemListById(reId);

        DataView dv = new DataView(ds.Tables[0]);

        if (dv.Count > 0)
        {
            intItemCount = 0;
            rptItems.DataSource = dv;
            rptItems.DataBind();
        }
    }
    #endregion
}
