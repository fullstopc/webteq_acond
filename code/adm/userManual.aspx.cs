﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class adm_userManual : System.Web.UI.Page
{
    #region "Properties"
    public int adminview = 1;
    #endregion 
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "User Manual";

        AdmUserManual.adminview = adminview;
        AdmUserManual.fill();
    }
}
