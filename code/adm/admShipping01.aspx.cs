using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Configuration;

public partial class adm_admShipping01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 25;

    protected string _gvSortExpression = "SHIP_ID";
    protected string _gvSortDirection = clsAdmin.CONSTSORTDESC;
    protected string _gvSort = "";

    protected Boolean boolSearch;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public string gvSortDirection
    {
        get { return ViewState["GVSORTDIRECTION"] as string ?? _gvSortDirection; }
        set { ViewState["GVSORTDIRECTION"] = value; }
    }

    public string gvSortExpression
    {
        get { return ViewState["GVSORTEXPRESSION"] as string ?? _gvSortExpression; }
        set { ViewState["GVSORTEXPRESSION"] = value; }
    }

    public string gvSort
    {
        get { return ViewState["GVSORT"] as string ?? _gvSort; }
        set { ViewState["GVSORT"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMSHIPPINGMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            txtKeyword.Attributes.Add("onkeydown", "javascript:if(event.keyCode==13){document.getElementById('" + lnkbtnSearch.ClientID + "').click(); return false;}");

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMELISTINGSIZE, clsConfig.CONSTGROUPOTHERSETTINGS, 1);

            gvShipping.PagerSettings.FirstPageText = GetGlobalResourceObject("GlobalResource", "contentAdmFirstPageText.Text").ToString();
            gvShipping.PagerSettings.LastPageText = GetGlobalResourceObject("GlobalResource", "contentAdmLastPageText.Text").ToString();
            gvShipping.PageSize = (!string.IsNullOrEmpty(config.value)) ? Convert.ToInt16(config.value) : clsAdmin.CONSTADMPAGESIZE;
            //gvShipping.PageSize = clsAdmin.CONSTADMPAGESIZE;

            gvSort = gvSortExpression + " " + gvSortDirection;
            bindGV(gvShipping, "SHIP_DEFAULT DESC, V_SHIPCOUNTRYNAME ASC");
            bindGVWaiver(gvWaiver, "WAIVER_ID ASC");
            setPageProperties();

            
            if (Session["DELETEDSHIPID"] != null)
            {
                pnlAck.Visible = true;

                litAck.Text = "<div class=\"noticemsg\">Shipping details has been deleted successfully.</div>";

                Session["DELETEDSHIPID"] = null;

                clsMis.resetListing();
            }

            if (Session["DELETEDWAIVERID"] != null)
            {
                pnlAckWaiver.Visible = true;

                litAckWaiver.Text = "<div class=\"noticemsg\">Shipping waiver has been deleted successfully.</div>";

                Session["DELETEDWAIVERID"] = null;

                clsMis.resetListing();
            }
        }
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        boolSearch = true;

        bindGV(gvShipping, "SHIP_DEFAULT DESC");
    }

    protected void gvShipping_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell tc in e.Row.Cells)
            {
                if (tc.HasControls())
                {
                    LinkButton lnkbtn = (LinkButton)tc.Controls[0];
                    //Image icon = new Image();
                    //icon.ImageUrl = "../img/adm/btn-cancel.gif";

                    if (lnkbtn != null && gvSortExpression == lnkbtn.CommandArgument)
                    {
                        //tc.Controls.Add(icon);
                        string strClass = (gvSortDirection == "DESC") ? "desc" : "asc";
                        tc.CssClass = strClass;
                    }


                }
            }
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int intDefault = Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "SHIP_DEFAULT"));
            //LinkButton lnkbtnEdit = (LinkButton)e.Row.FindControl("lnkbtnEdit");
            LinkButton lnkbtnDelete = (LinkButton)e.Row.FindControl("lnkbtnDelete");
            HyperLink hypEdit = (HyperLink)e.Row.FindControl("hypEdit");

            int shipId = int.Parse(gvShipping.DataKeys[e.Row.RowIndex].Value.ToString());
            hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.NavigateUrl = "admShipping0101.aspx?id=" + shipId;

            //lnkbtnEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteShippingOption.Text") + "');";

            if (intDefault == 1)
            {
                lnkbtnDelete.Visible = false;
            }
        }
    }

    protected void gvShipping_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "cmdEdit")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int shipId = Convert.ToInt16(gvShipping.DataKeys[rowIndex].Value.ToString());

            Response.Redirect("admShipping0101.aspx?id=" + shipId);
        }
        else if (e.CommandName == "cmdDelete")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int shipId = Convert.ToInt16(gvShipping.DataKeys[rowIndex].Value.ToString());
            int intDeletedShipId = 0;

            clsMis.performDeleteShipping(shipId, ref intDeletedShipId);

            Session["DELETEDSHIPID"] = intDeletedShipId;

            Response.Redirect(currentPageName);
        }
    }

    protected void gvShipping_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvShipping.PageIndex = e.NewPageIndex;
        bindGV(gvShipping, gvSort);
    }

    protected void gvShipping_Sorting(object sender, GridViewSortEventArgs e)
    {
        string strSortExpDir = "";
        if (e.SortExpression != gvSortExpression)
        {
            gvSortExpression = e.SortExpression;
            gvSortDirection = clsAdmin.CONSTSORTASC;
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }
        else
        {
            switch (gvSortDirection)
            {
                case clsAdmin.CONSTSORTASC:
                    gvSortDirection = clsAdmin.CONSTSORTDESC;
                    break;
                case clsAdmin.CONSTSORTDESC:
                    gvSortDirection = clsAdmin.CONSTSORTASC;
                    break;
            }
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }

        gvSort = strSortExpDir;
        bindGV(gvShipping, gvSort);
    }

    protected void gvWaiver_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //LinkButton lnkbtnEdit = (LinkButton)e.Row.FindControl("lnkbtnEdit");
            LinkButton lnkbtnDelete = (LinkButton)e.Row.FindControl("lnkbtnDelete");
            HyperLink hypEdit = (HyperLink)e.Row.FindControl("hypEdit");

            int waiverId = int.Parse(gvWaiver.DataKeys[e.Row.RowIndex].Value.ToString());
            hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.NavigateUrl = "admShipping0102.aspx?id=" + waiverId;

            //lnkbtnEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteWaiver.Text") + "');";
        }
    }

    protected void gvWaiver_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "cmdEdit")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int waiverId = Convert.ToInt16(gvWaiver.DataKeys[rowIndex].Value.ToString());
            Response.Redirect("admShipping0102.aspx?id=" + waiverId);
        }
        else if (e.CommandName == "cmdDelete")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int waiverId = Convert.ToInt16(gvWaiver.DataKeys[rowIndex].Value.ToString());
            int intDeletedWaiverId = 0;

            clsMis.performDeleteWaiver(waiverId, ref intDeletedWaiverId);

            Session["DELETEDWAIVERID"] = intDeletedWaiverId;

            Response.Redirect(currentPageName);
        }
    }

    protected void gvWaiver_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvWaiver.PageIndex = e.NewPageIndex;
        bindGVWaiver(gvWaiver, gvSort);
    }

    protected void gvWaiver_Sorting(object sender, GridViewSortEventArgs e)
    {
        string strSortExpDir = "";
        if (e.SortExpression != gvSortExpression)
        {
            gvSortExpression = e.SortExpression;
            gvSortDirection = clsAdmin.CONSTSORTASC;
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }
        else
        {
            switch (gvSortDirection)
            {
                case clsAdmin.CONSTSORTASC:
                    gvSortDirection = clsAdmin.CONSTSORTDESC;
                    break;
                case clsAdmin.CONSTSORTDESC:
                    gvSortDirection = clsAdmin.CONSTSORTASC;
                    break;
            }
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }

        gvSort = strSortExpDir;
        bindGVWaiver(gvWaiver, gvSort);
    }

    protected void gvItems_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            clsConfig config = new clsConfig();
            string strCurrency = config.currency;
            string strCurrencyTag = config.currencyTag;
            string strShippingUnit = config.shippingUnit;
            string strShippingUnitTag = config.shippingUnitTag;

            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                string strHeader = Server.HtmlDecode(e.Row.Cells[i].Text);

                if (!string.IsNullOrEmpty(strHeader))
                {
                    strHeader = strHeader.Replace(strCurrencyTag, strCurrency);
                    strHeader = strHeader.Replace(strShippingUnitTag, strShippingUnit);

                    e.Row.Cells[i].Text = strHeader;
                }
                else
                {
                    strHeader = Server.HtmlDecode(((LinkButton)e.Row.Cells[i].Controls[0]).Text);

                    strHeader = strHeader.Replace(strCurrencyTag, strCurrency);
                    strHeader = strHeader.Replace(strShippingUnitTag, strShippingUnit);

                    ((LinkButton)e.Row.Cells[i].Controls[0]).Text = strHeader;
                }
            }
        }
    }

    protected void lnkbtnExport_Click(object sender, EventArgs e)
    {
        ExportGV(gvShipping, "SHIPPINGLIST", (DataView)Session["SHIPDV"], true);
    }

    protected void lnkbtnExportWaiver_Click(object sender, EventArgs e)
    {
        ExportGV(gvWaiver, "SHIPPINGWAIVERLIST", (DataView)Session["WAIVERDV"], true);
    }

    public override void VerifyRenderingInServerForm(Control control)
    {

    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        clsCountry country = new clsCountry();
        DataSet ds;
        DataView dv;

        ds = new DataSet();
        ds = country.getCountryList(1);
        dv = new DataView(ds.Tables[0]);
        dv.RowFilter = "COUNTRY_DEFAULT = 0";
        dv.Sort = "COUNTRY_NAME ASC";

        ddlCountry.DataSource = dv;
        ddlCountry.DataTextField = "COUNTRY_NAME";
        ddlCountry.DataValueField = "COUNTRY_CODE";
        ddlCountry.DataBind();

        ListItem ddlCountryDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlCountry.Items.Insert(0, ddlCountryDefaultItem);

        clsConfig config = new clsConfig();
        int intType = !string.IsNullOrEmpty(config.shippingType) ? Convert.ToInt16(config.shippingType) : 1;
        if (intType == clsAdmin.CONSTSHIPPINGTYPE1)
        {
            pnlWaiver.Visible = true;
            // hide Free After column
            gvShipping.Columns[9].Visible = false;
        }
        else if (intType == clsAdmin.CONSTSHIPPINGTYPE2 || intType == clsAdmin.CONSTSHIPPINGTYPE3)
        {

        }
        else if (intType == clsAdmin.CONSTSHIPPINGTYPE4)
        {
            hypAddShipping.Visible = false;
        }
    }

    protected void bindGV(GridView gvRef, string gvSortExpDir)
    {
        clsShipping ship = new clsShipping();
        DataSet ds = new DataSet();
        ds = ship.getShippingList2(0);
        DataView dv = new DataView(ds.Tables[0]);
        dv.RowFilter = "1 = 1";
        Boolean boolFilter = false;

        if (boolSearch)
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                string strKeyword = txtKeyword.Text.Trim();

                dv.RowFilter += " AND (V_SHIPCOUNTRYNAME LIKE '%" + strKeyword + "%' OR SHIP_STATE LIKE '%" + strKeyword + "%')";
                boolFilter = true;

                litFound.Text = "Search for <span class=\"SectKeyword\">" + strKeyword + "</span> | ";
            }
            else { litFound.Text = ""; }

            if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
            {
                dv.RowFilter += " AND SHIP_COUNTRY = '" + ddlCountry.SelectedValue + "'";
                boolFilter = true;
            }
        }

        if (boolFilter)
        {
            //pnlSearchForm.CssClass = "divSearchFormShow";
            //hypHideShow.CssClass = "linkHide";
        }
        else
        {
            //pnlSearchForm.CssClass = "divSearchForm";
            //hypHideShow.CssClass = "linkShow";
        }

        if (!string.IsNullOrEmpty(gvSortExpDir))
        {
            dv.Sort = gvSortExpDir;
        }

        Session["SHIPDV"] = dv;
        
        gvShipping.DataSource = dv;

        clsConfig config = new clsConfig();
        // default shipping type is 1
        int intType = !string.IsNullOrEmpty(config.shippingType) ? Convert.ToInt16(config.shippingType) : 1;

        if (intType == clsAdmin.CONSTSHIPPINGTYPE5)
        {
            gvShipping.Columns[1].Visible = true;
            gvShipping.Columns[2].Visible = true;
            gvShipping.Columns[12].Visible = false;
        }

        if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
        {
            gvShipping.Columns[6].Visible = true;
            gvShipping.Columns[7].Visible = false;
            gvShipping.Columns[8].Visible = false;
            gvShipping.Columns[9].Visible = false;
            gvShipping.Columns[10].Visible = false;
            gvShipping.Columns[11].Visible = true;
        }

        gvShipping.DataBind();

        if (boolSearch) { litFound.Text += dv.Count + " record(s) listed."; }
        else { litFound.Text = dv.Count + " record(s) listed."; }

        if (dv.Count > 0)
        {
            lnkbtnExport.Enabled = true;
            //lnkbtnExport.CssClass = "btn2 btnExport";
        }
        else
        {
            lnkbtnExport.Enabled = false;
            lnkbtnExport.CssClass += " disabled";
        }
    }

    protected void bindGVWaiver(GridView gvRef, string gvSortExpDir)
    {
        clsWaiver waiver = new clsWaiver();
        DataSet ds = new DataSet();
        ds = waiver.getWaiverList(1);
        DataView dv = new DataView(ds.Tables[0]);

        if (!string.IsNullOrEmpty(gvSortExpDir))
        {
            dv.Sort = gvSortExpDir;
        }

        Session["WAIVERDV"] = dv;

        gvWaiver.DataSource = dv;
        gvWaiver.DataBind();

        litFound2.Text = dv.Count + " record(s) listed.";

        if (dv.Count > 0)
        {
            lnkbtnExportWaiver.Enabled = true;
            lnkbtnExportWaiver.CssClass = "btn2 btnExport";
        }
        else
        {
            lnkbtnExportWaiver.Enabled = false;
            lnkbtnExportWaiver.CssClass = "btn2 btnExport btnDisabled";
        }
    }

    protected void ExportGV(GridView gvRef, string strFileTitle, DataView dvItems, Boolean boolHideLastColumn)
    {
        DateTime dt = DateTime.Now;
        string strFileName = strFileTitle + "_" + dt.Day + dt.Month + dt.Year + ".xls";
        string strAttachment = "attachment; filename=" + strFileName;

        Response.ClearContent();
        Response.AddHeader("content-disposition", strAttachment);
        Response.ContentType = "application/vnd.xls";
        Response.ContentEncoding = System.Text.Encoding.Unicode;
        Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        gvRef.AllowPaging = false;
        gvRef.DataSource = dvItems;
        gvRef.DataBind();

        for (int i = 0; i < gvRef.Columns.Count; i++)
        {
            if (gvRef.HeaderRow.Cells[i].CssClass == "hiddenCol")
            {
                gvRef.HeaderRow.Cells[i].CssClass = "showCol";
            }

            gvRef.HeaderRow.Cells[i].Enabled = false;
        }

        for (int i = 0; i < gvRef.Rows.Count; i++)
        {
            GridViewRow row = gvRef.Rows[i];

            for (int j = 0; j < gvRef.Columns.Count; j++)
            {
                if (row.Cells[j].CssClass == "hiddenCol")
                {
                    row.Cells[j].CssClass = "showCol";
                }
            }
        }

        if (boolHideLastColumn)
        {
            int intColHide = gvRef.Columns.Count - 1;

            gvRef.HeaderRow.Cells[intColHide].Visible = false;
            gvRef.Columns[intColHide].Visible = false;
        }

        gvRef.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }
    #endregion
}