﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.IO;

public partial class adm_admProduct01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 15;

    protected string strKeyword;
    protected int intActive;
    protected int intShowTop;
    protected int intNew;
    protected Boolean boolSearch;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMPRODUCTMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            setSearchProperties();
        }

        if (Session["DELETEDPRODNAME"] != null)
        {
            pnlAck.Visible = true;
            litAck.Text = "Product (" + Session["DELETEDPRODNAME"] + ") has been deleted successfully.";
            litAck.Text = "<div class=\"noticemsg\">" + litAck.Text + "</div>";

            Session["DELETEDPRODNAME"] = null;

            clsMis.resetListing();
        }
    }

    #endregion

    #region "Methods"

    protected void setSearchProperties()
    {
        ListItem ddlNewDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlNew.Items.Insert(0, ddlNewDefaultItem);
        ddlNew.Items.Add(new ListItem("Yes", "1"));
        ddlNew.Items.Add(new ListItem("No", "0"));

        ListItem ddlShowTopDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlShowTop.Items.Insert(0, ddlShowTopDefaultItem);
        ddlShowTop.Items.Add(new ListItem("Yes", "1"));
        ddlShowTop.Items.Add(new ListItem("No", "0"));

        ListItem ddlBestSellerDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlBestSeller.Items.Insert(0, ddlBestSellerDefaultItem);
        ddlBestSeller.Items.Add(new ListItem("Yes", "1"));
        ddlBestSeller.Items.Add(new ListItem("No", "0"));

        ListItem ddlActiveDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlActive.Items.Insert(0, ddlActiveDefaultItem);
        ddlActive.Items.Add(new ListItem("Yes", "1"));
        ddlActive.Items.Add(new ListItem("No", "0"));

        //ListItem ddlHotSalesDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        //ddlHotSales.Items.Insert(0, ddlHotSalesDefaultItem);
        //ddlHotSales.Items.Add(new ListItem("Yes", "1"));
        //ddlHotSales.Items.Add(new ListItem("No", "0"));

        if (Session[clsAdmin.CONSTPROJECTINVENTORY] != null && Session[clsAdmin.CONSTPROJECTINVENTORY] != "")
        {
            if (Convert.ToInt16(Session[clsAdmin.CONSTPROJECTINVENTORY]) == 1)
            {
                tdInventory.Visible = true;

                ListItem ddlInventoryDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
                ddlInventory.Items.Insert(0, ddlActiveDefaultItem);
                ddlInventory.Items.Add(new ListItem("With Stock", "1"));
                ddlInventory.Items.Add(new ListItem("Empty Stock", "2"));
            }
        } 
    }

    protected void ExportGV(GridView gvRef, string strFileTitle, DataView dvItems, Boolean boolHideLastColumn)
    {
        DateTime dt = DateTime.Now;
        string strFileName = strFileTitle + "_" + dt.Day + dt.Month + dt.Year + ".xls";
        string strAttachment = "attachment; filename=" + strFileName;

        Response.ClearContent();
        Response.AddHeader("content-disposition", strAttachment);
        Response.ContentType = "application/vnd.xls";
        Response.ContentEncoding = System.Text.Encoding.Unicode;
        Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        checkLanguage(gvRef);
        gvRef.AllowPaging = false;
        gvRef.DataSource = dvItems;
        gvRef.DataBind();

        for (int i = 0; i < gvRef.Columns.Count; i++)
        {
            if (gvRef.HeaderRow.Cells[i].CssClass == "hiddenCol")
            {
                gvRef.HeaderRow.Cells[i].CssClass = "showCol";
            }

            gvRef.HeaderRow.Cells[i].Enabled = false;
        }

        for (int i = 0; i < gvRef.Rows.Count; i++)
        {
            GridViewRow row = gvRef.Rows[i];

            for (int j = 0; j < gvRef.Columns.Count; j++)
            {
                if (row.Cells[j].CssClass == "hiddenCol")
                {
                    row.Cells[j].CssClass = "showCol";
                }
            }
        }

        if (boolHideLastColumn)
        {
            int intColHide = gvRef.Columns.Count - 1;

            gvRef.HeaderRow.Cells[intColHide].Visible = false;
            gvRef.Columns[intColHide].Visible = false;
        }

        gvRef.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    protected void checkLanguage(GridView gvRef)
    {
        if (clsMis.isLangExist(clsAdmin.CONSTLANGJAPANESE))
        {
            gvRef.Columns[3].Visible = true;
            gvRef.Columns[7].Visible = true;
        }
        if (clsMis.isLangExist(clsAdmin.CONSTLANGMALAY))
        {
            gvRef.Columns[4].Visible = true;
            gvRef.Columns[8].Visible = true;
        }
        if (clsMis.isLangExist(clsAdmin.CONSTLANGCHINESE))
        {
            gvRef.Columns[5].Visible = true;
            gvRef.Columns[9].Visible = true;
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        int intItemID = Convert.ToInt16(hdnItemID.Value);
        string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTADMPRODFOLDER + "/";
        string strDeletedProdName = "";

        clsMis.performDeleteProduct(intItemID, uploadPath, ref strDeletedProdName);

        Session["DELETEDPRODNAME"] = strDeletedProdName;
        Response.Redirect(currentPageName);

        Response.Redirect(currentPageName);
    }

    #endregion
}
