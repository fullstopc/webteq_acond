﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class adm_admCancelTrans : System.Web.UI.Page
{
    #region "Properties"
    protected int _transId = 0;
    protected int _type = 1;
    protected string strItems = "";
    protected decimal _decShipping = Convert.ToDecimal("0.00");
    protected decimal _decTotal = Convert.ToDecimal("0.00");
    clsOrder ord = new clsOrder();
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int transId
    {
        get
        {
            if (ViewState["TRANSID"] == null)
            {
                return _transId;
            }
            else
            {
                return int.Parse(ViewState["TRANSID"].ToString());
            }
        }
        set { ViewState["TRANSID"] = value; }
    }

    protected int type
    {
        get
        {
            if (ViewState["TYPE"] == null)
            {
                return _type;
            }
            else
            {
                return Convert.ToInt16(ViewState["TYPE"]);
            }
        }
        set { ViewState["TYPE"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    transId = int.Parse(Request["id"]);
                }
                catch (Exception ex)
                {
                    transId = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["type"]))
            {
                try
                {
                    type = Convert.ToInt16(Request["type"]);
                }
                catch (Exception ex)
                {
                    type = 0;
                }
            }
        }

        setPageProperties();
    }

    protected void lnkbtnConfirm_Click(object sender, EventArgs e)
    {
        clsUser usr = new clsUser();
        string strEmail = txtEmail.Text.Trim();
        string password = txtPassword.Text.Trim();
        clsMD5 md5 = new clsMD5();
        string strPassword = md5.encrypt(password);

        //@ set flag to use back connection string for admin
        Session[clsAdmin.CONSTADMINCS] = 1;
        if (usr.isUserTypeValid(strEmail, strPassword, 1) || usr.isUserTypeValid(strEmail, strPassword, 2))
        {
            Session[clsAdmin.CONSTADMINCS] = null;
            int intRecordAffected = 0;

            clsOrder ord = new clsOrder();
            DataSet dsOrder = ord.getTbOrderByOrderId("ORDER_NO",transId);
            string strOrderNo = "";
            if (dsOrder.Tables[0].Rows.Count > 0)
            {
                strOrderNo = dsOrder.Tables[0].Rows[0]["ORDER_NO"].ToString();
            }
            // ord.extractOrderById(transId);

            intRecordAffected = ord.setCancel(transId);

            if (intRecordAffected == 1)
            {
                clsMis.performUpdateOrderStatus(transId, clsAdmin.CONSTTRANSSTATUSCANCEL, true);
                Session["DELETEDTRANSCODE"] = strOrderNo;

                /*Inventory*/
                clsConfig conf = new clsConfig();
                conf.extractItemByNameGroup(clsConfig.CONSTNAMEINVENTORY, clsConfig.CONSTGROUPPRODUCT, 1);
                int intInventoryChk = Convert.ToInt16(conf.value);

                if (intInventoryChk == 1)
                {
                    DataSet dsItems = new DataSet();
                    dsItems = ord.getOrderItemsByOrderId(transId);
                    foreach (DataRow row in dsItems.Tables[0].Rows)
                    {
                        int intProdId = Convert.ToInt16(row["ORDDETAIL_PRODID"].ToString());
                        int intProdQty = Convert.ToInt16(row["ORDDETAIL_PRODQTY"].ToString());

                        clsInventory inv = new clsInventory();
                        if (inv.isInventoryExist(intProdId))
                        {
                            if (inv.extractProdQty(intProdId))
                            {
                                int intRecAffected = 0;
                                int intNewInvQty = int.Parse(inv.invQty) + intProdQty;

                                intRecAffected = inv.updateProdInvQtyById(intProdId, intNewInvQty.ToString());
                                if (intRecAffected == 1)
                                {
                                    inv.addInvTransLog(intProdId, ord.ordId, inv.invQty.ToString(), intProdQty.ToString(), intNewInvQty.ToString());
                                }
                            }
                        }
                    }
                }
                /*End of Inventory*/

                ord.extractOrderById(transId);

                clsConfig config = new clsConfig();
                string strCurrency = "";

                if (ord.extractConvertionRateUnitById(transId))
                {
                    if (!string.IsNullOrEmpty(ord.ordConvUnit))
                    {
                        strCurrency = ord.ordConvUnit;
                    }
                    else
                    {
                        strCurrency = config.currency;
                    }
                }

                string strPersonDetails = "";

                strPersonDetails = "Name: " + ord.memName + "<br />Email: " + ord.memEmail;

                string strPaymentDetails = "Payment Type: " + ord.ordPayGateway + "<br />Remarks: " + ord.ordPayRemarks + "<br />Paid Amount: " + strCurrency + " " + ord.ordPayTotal + "<br />" + strPersonDetails;

                string strBillingDetails = "Contact Person: " + ord.name + "<br />Address: " + ord.add + "<br />Postal Code: " + ord.poscode + "<br />Country: " + ord.countryName + (!string.IsNullOrEmpty(ord.state) ? "<br />State: " + ord.state : "") + (!string.IsNullOrEmpty(ord.city) ? "<br />City: " + ord.city : "") + "<br />Contact No. " + ord.contactNo;

                string strDeliveryDetails = "Contact Person: " + ord.shippingName + "<br />Address: " + ord.shippingAdd + "<br />Postal Code: " + ord.shippingPoscode + "<br />Country: " + ord.shippingCountryName + (!string.IsNullOrEmpty(ord.shippingState) ? "<br />State: " + ord.shippingState : "") + (!string.IsNullOrEmpty(ord.shippingCity) ? "<br />City: " + ord.shippingCity : "") + "<br />Contact No. " + ord.shippingContactNo;

                EmailDetails(strCurrency, ord.ordGST, ord.ordTotalGST, ord.ordShipping, ord.ordTotal);

                sendCancelOrder(ord.memName, ord.memEmail, ord.ordNo, ord.ordTotalItem, ord.ordTotal, strBillingDetails, strDeliveryDetails, strItems, strPaymentDetails, strCurrency);

            }
            else { Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text").ToString() + "<br />Failed to cancel order (" + strOrderNo + ").Please try again.</div>"; }

            if (Session["DELETEDTRANSCODE"] != null)
            {
                litAck.Text = "Order (" + Session["DELETEDTRANSCODE"] + ") has been cancelled.";
                litAck.Text = "<div class=\"noticemsg\">" + litAck.Text + "</div>";

                pnlAck.Visible = true;
                lnkbtnBack.Visible = false;
                pnlForm.Visible = false;

                clsMis.resetListing();
            }
            else if (Session["ERRMSG"] != null)
            {
                litAck.Text = Session["ERRMSG"].ToString();

                pnlAck.Visible = true;
                lnkbtnContinue.Visible = false;
                pnlForm.Visible = false;
            }

            Session["DELETEDTRANSCODE"] = null;
            Session["ERRMSG"] = null;
        }
        else
        {
            litAck.Text = "<div class=\"errmsg\">Sorry, you're not allowed to cancel this order.<br />You might need higher level of user access.</div>";

            pnlAck.Visible = true;
            lnkbtnContinue.Visible = false;
            pnlForm.Visible = false;

            Session[clsAdmin.CONSTADMINCS] = null;
        }
    }

    protected void lnkbtnContinue_Click(object sender, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("REFRESHPARENT"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "tb_close(true);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "REFRESHPARENT", strJS, false);
        }
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        lnkbtnCancel.OnClientClick = "javascript:self.parent.tb_remove(); return false;";
        lnkbtnConfirm.OnClientClick = "javascript:if (Page_ClientValidate()){return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmCancelTrans.Text").ToString() + "'); return false;}";

        if (!IsPostBack)
        {
            lnkbtnBack.OnClientClick = "javascript:document.location.href='" + Request.Url.ToString() + "'; return false;";
        }
    }

    protected void sendCancelOrder(string strUserName, string strUserEmail, string strOrdNo, int intItem, decimal decOrdTotal, string strBillingDetails, string strDeliveryDetails, string strItems, string strPaymentDetails, string strCurrency)
    {
        string strSubjectPrefix;
        string strSignature;
        string strSubject;
        string strBody;
        string strUserBody;
        string strSenderName;

        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDCANCELEMAILSUBJECT, clsConfig.CONSTGROUPORDCANCELEMAILCONTENT, 1);
        strSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDCANCELADMINEMAILCONTENT, clsConfig.CONSTGROUPORDCANCELEMAILCONTENT, 1);
        strBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEORDCANCELUSEREMAILCONTENT, clsConfig.CONSTGROUPORDCANCELEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;

        strBody = strBody.Replace("[NAME]", strUserName);
        strBody = strBody.Replace("[ORDNO]", strOrdNo);
        strBody = strBody.Replace("[ORDITEM]", intItem.ToString());
        strBody = strBody.Replace("[CUR]", strCurrency);
        strBody = strBody.Replace("[ORDTOTAL]", decOrdTotal.ToString());
        strBody = strBody.Replace("[BILLINGDETAILS]", strBillingDetails);
        strBody = strBody.Replace("[DELIVERYDETAILS]", strDeliveryDetails);
        strBody = strBody.Replace("[ORDERDETAILS]", strItems);
        strBody = strBody.Replace("[PAYDETAILS]", strPaymentDetails);
        strBody = strBody.Replace("[SIGNATURE]", strSignature);

        strUserBody = strUserBody.Replace("[NAME]", strUserName);
        strUserBody = strUserBody.Replace("[ORDNO]", strOrdNo);
        strUserBody = strUserBody.Replace("[ORDITEM]", intItem.ToString());
        strUserBody = strUserBody.Replace("[CUR]", strCurrency);
        strUserBody = strUserBody.Replace("[ORDTOTAL]", decOrdTotal.ToString());
        strUserBody = strUserBody.Replace("[BILLINGDETAILS]", strBillingDetails);
        strUserBody = strUserBody.Replace("[DELIVERYDETAILS]", strDeliveryDetails);
        strUserBody = strUserBody.Replace("[ORDERDETAILS]", strItems);
        strUserBody = strUserBody.Replace("[PAYDETAILS]", strPaymentDetails);
        strUserBody = strUserBody.Replace("[SIGNATURE]", strSignature);

        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strSubject = strSubjectPrefix + " " + strSubject; }

        strSubject = strSubject.Replace("ORDNO", strOrdNo);

        if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        {
            if (clsEmail.send(strSenderName, "", strUserEmail, "", "", "", strSubject, strUserBody, 1))
            {

            }
            else
            {
                clsLog.logErroMsg("Failed to send order cancel details.");
            }
        }
        else
        {
            clsLog.logErroMsg("Failed to send order cancel details.");
        }
    }

    protected void EmailDetails(string strCurrency, int intordGST, decimal decTotalGST, decimal decOrdShipping, decimal decOrdTotal)
    {

        clsConfig config = new clsConfig();
        //Order Item Details
        DataSet ds = new DataSet();
        ds = ord.getOrderItemsByOrderId(transId);

        strItems = "";

        strItems += "<table cellpadding=\"0\" cellspacing=\"0\" style=\"width:500px; color:#4a4a4a; border:solid 1px #000000;\"\">";
        strItems += "<tr>";
        strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:center; width:150px; padding-top:10px; padding-bottom:10px; font-weight:bold; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">Item</td>";
        strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:center; width:123px; padding-top:10px; padding-bottom:10px; font-weight:bold; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">Price</td>";
        strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:center; width:123px; padding-top:10px; padding-bottom:10px; font-weight:bold; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">Quantity</td>";
        strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:center; width:123px; padding-top:10px; padding-bottom:10px; font-weight:bold; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">Item Total</td>";
        strItems += "</tr>";

        decimal decSubTotal = Convert.ToDecimal("0.00");
        decimal decTotalGSTAmount = Convert.ToDecimal("0.00");

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            int intDetailId = Convert.ToInt16(row["ORDDETAIL_ID"]);
            int intItemId = Convert.ToInt16(row["ORDDETAIL_PRODID"]);
            decSubTotal += Convert.ToDecimal(row["ORDDETAIL_PRODTOTAL"]);
            decimal decProdPrice = Convert.ToDecimal(row["ORDDETAIL_PRODPRICE"]);
            decimal decProdPricing = Convert.ToDecimal(row["ORDDETAIL_PRODPRICING"]);
            int intQuantity = Convert.ToInt32(row["ORDDETAIL_PRODQTY"]);

            strItems += "<tr>";
            strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:left; width:150px; padding-left:10px; padding-top:10px; padding-bottom:10px; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + row["ORDDETAIL_PRODCODE"].ToString() + "<br />" + row["ORDDETAIL_PRODDNAME"].ToString() + "</td>";
            strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:center; width:123px; padding-right:10px; padding-top:10px; padding-bottom:10px; vertical-align:top; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strCurrency + " " + row["ORDDETAIL_PRODPRICE"].ToString() + "</td>";
            strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:center; width:123px; padding-top:10px; padding-bottom:10px; vertical-align:top; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + row["ORDDETAIL_PRODQTY"].ToString() + "</td>";
            strItems += "   <td style=\"border-bottom:solid 1px #000000; text-align:right; width:113px; padding-right:10px; padding-top:10px; padding-bottom:10px; vertical-align:top; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strCurrency + " " + row["ORDDETAIL_PRODTOTAL"].ToString() + "</td>";
            strItems += "</tr>";
        }


        strItems += "<tr>";
        strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\" colspan=\"3\">Subtotal</td>";
        strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strCurrency + " " + decSubTotal + "</td>";
        strItems += "</tr>";

        if (decTotalGST > 0)
        {
            strItems += "<tr>";
            strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\" colspan=\"3\">Total GST " + intordGST + "%</td>";
            strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strCurrency + " " + decTotalGST + "</td>";
            strItems += "</tr>";
        }

        strItems += "<tr>";
        strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\" colspan=\"3\">Shipping</td>";
        strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strCurrency + " " + decOrdShipping + "</td>";
        strItems += "</tr>";

        strItems += "<tr>";
        strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-weight:bold; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\" colspan=\"3\">Order Total</td>";
        strItems += "   <td style=\"text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px; font-weight:bold; font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strCurrency + " " + decOrdTotal + "</td>";
        strItems += "</tr>";
        strItems += "</table>";
    }
    #endregion
}
