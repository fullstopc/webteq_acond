﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class adm_admCMS0101 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 3;

    protected int _cmsId = 0;
    protected int _mode = 1;
    protected int _formSect = 1;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    public string strSavedMsg
    {
        get { return String.Format(GetGlobalResourceObject("GlobalResource", "admSavedMessageWithParams.Text").ToString(), "Object"); }
    }
    public int cmsId
    {
        get
        {
            if (ViewState["CMSID"] == null)
            {
                return _cmsId;
            }
            else
            {
                return int.Parse(ViewState["CMSID"].ToString());
            }
        }
        set { ViewState["CMSID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    protected int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set
        {
            ViewState["FORMSECT"] = value;
        }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    cmsId = int.Parse(Request["id"]);
                }
                catch (Exception ex)
                {
                    cmsId = 0;
                }
            }
            else if (Session["NEWCMSID"] != null)
            {
                try
                {
                    cmsId = int.Parse(Session["NEWCMSID"].ToString());
                }
                catch (Exception ex)
                {
                    cmsId = 0;
                }
            }
            else if (Session["EDITEDCMSID"] != null)
            {
                try
                {
                    cmsId = int.Parse(Session["EDITEDCMSID"].ToString());
                }
                catch (Exception ex)
                {
                    cmsId = 0;
                }
            }

            if (Request["id"] != null || Session["EDITEDCMSID"] != null)
            {
                mode = 2;
            }

            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMOBJECTMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.parentId = sectId;

            Master.subSectId = clsMis.getSubSectId(sectId, formSect, 1);
            Master.id = cmsId;
            Master.showSideMenu = true;

            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            ucAdmCMSObject.cmsId = cmsId;
            ucAdmCMSObject.mode = mode;
        }

        setPageProperties();
        Session["NEWCMSID"] = null;
        Session["EDITEDCMSID"] = null;
        Session["NOCHANGE"] = null;
        Session["DELETEDCMSTITLE"] = null;
        Session["ERRMSG"] = null;
        Session["ADMPAGENAME"] = null;
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        switch (mode)
        {
            case 1:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleAdd.Text").ToString(), true);
                break;
            case 2:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleEdit.Text").ToString(), true);
                break;
            default:
                break;
        }

        if (!IsPostBack)
        {
            switch (mode)
            {
                case 1:
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleAdd.Text").ToString();

                    clsCMSObject ob = new clsCMSObject();
                    int intTotal = ob.getTotalRecord(0);

                    if (clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITOBJECT, intTotal)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "adm/admCMS01.aspx"); }
                    break;
                case 2:
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleEdit.Text").ToString();
                    break;
                default:
                    break;
            }

            if (Session["NEWCMSID"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + strSavedMsg + "</div>";

                clsMis.resetListing();
            }
            else if (Session["EDITEDCMSID"] != null)
            {
                if (Session["NOCHANGE"] != null)
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
                }
                else
                {
                    int intTotal = new clsCMSObject().getTotalRecord(0);
                    string strUrlFormpage = "<a href='admCMS0101.aspx'>Add New</a>";
                    string strUrlListpage = "<a href='admCMS01.aspx'>Back to Listing</a>";

                    litAck.Text = "<div class=\"noticemsg\">" + strSavedMsg + "  " + strUrlListpage + "";
                    if (!clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITOBJECT, intTotal - 1))
                    {
                        litAck.Text += " & " + strUrlFormpage + "";
                    }
                    litAck.Text += "</div>";
                    clsMis.resetListing();
                }

                pnlAck.Visible = true;
            }
            else if (Session["DELETEDCMSTITLE"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }
            else if (Session["ERRMSG"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
            }
        }

        if (pnlForm.Visible)
        {
            ucAdmCMSObject.fill();
        }
    }
    #endregion
}
