﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_adm : System.Web.UI.Page
{
    #region "Properties"
    protected int _mode = 1;
    protected int _areaID = 0;
    #endregion

    #region "Property Methods"
    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    public int areaID
    {
        get
        {
            if (ViewState["AREA_ID"] == null)
            {
                return _areaID;
            }
            else
            {
                return int.Parse(ViewState["AREA_ID"].ToString());
            }
        }
        set
        {
            ViewState["AREA_ID"] = value;
        }
    }
   
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            areaID = Request["id"].ToInteger();

            if (areaID > 0)
            {
                mode = 2;
            }

            if (mode == 2)
            {
                fillform();
            }

        }

    }
    #endregion

    #region "Methods"
    private void fillform()
    {
        clsConfigPropertyArea area = new clsConfigPropertyArea();
        if (area.extractByID(areaID))
        {
            txtName.Text = area.name;
        }
    }
    #endregion

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {

            int intAdmID = Session["ADMID"].ToInteger();

            clsConfigPropertyArea area = new clsConfigPropertyArea();
            area.name = txtName.Text;
            area.ID = mode == 2 ? areaID : -1;
            area.createdby = intAdmID;
            area.updatedby = intAdmID;

            string msg = "";
            bool Success = true;
            bool Updated = false;

            if (mode == 1)
            {
                if (area.add())
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();
                }
            }
            else if (mode == 2)
            {
                if (!area.isSame())
                {
                    if (area.update())
                    {
                        Updated = true;
                        msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();
                    }
                    else
                    {
                        Success = false;
                    }
                }

                if (Success && !Updated)
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString();
                }
            }

            if (msg == "")
            {
                msg = GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString();
            }

            Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.AreaSaveSuccess('" + msg + "')", true);
        }
    }
}