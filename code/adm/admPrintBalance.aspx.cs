﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class adm_admPrintBalance : System.Web.UI.Page
{
    #region "Properties"
    protected string strKeyWord;
    protected int intGrpId = 0;
    protected int intGrpingId = 0;
    protected int intItemCount = 0;
    protected decimal decDiscount = 0;
    protected decimal decTotal = 0;
    #endregion


    #region "Property Methods"

    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["grpid"]))
            {
                int intTryParse;
                if (int.TryParse(Request["grpid"], out intTryParse))
                {
                    intGrpId = intTryParse;
                }
            }

            if (!string.IsNullOrEmpty(Request["grpingid"]))
            {
                int intTryParse;
                if (int.TryParse(Request["grpingid"], out intTryParse))
                {
                    intGrpingId = intTryParse;
                }
            }

            if (!string.IsNullOrEmpty(Request["key"]))
            {
                strKeyWord = Request["key"].ToString();
            }

            setPageProperties();
            fillForm();
            bindRptData();
        }
    }

    protected void rptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            intItemCount += 1;

            Literal litNo = (Literal)e.Item.FindControl("litNo");
            litNo.Text = intItemCount.ToString();
        }
        else if (e.Item.ItemType == ListItemType.Footer)
        {

        }
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
    }

    protected void fillForm()
    {

    }

    protected void bindRptData()
    {
        clsBalance balance = new clsBalance();
        DataSet ds = new DataSet();
        ds = balance.getBalanceList();
        
        DataView dv = new DataView(ds.Tables[0]);
        dv.RowFilter = "1 = 1";
        if (!string.IsNullOrEmpty(strKeyWord))
        {
            dv.RowFilter += " AND (PROD_CODE LIKE '%" + strKeyWord + "%'" +
                            " OR PROD_DNAME LIKE '%" + strKeyWord + "%')";

        }

        if (intGrpId > 0)
        {
            dv.RowFilter += " AND GRP_ID = " + intGrpId;
        }

        if (intGrpingId > 0)
        {
            dv.RowFilter += " AND GRP_ID = " + intGrpId;
        }

        dv = balance.GroupBy("PROD_ID", dv.ToTable());
        if (dv.Count > 0)
        {
            intItemCount = 0;
            rptItems.DataSource = dv;
            rptItems.DataBind();
        }
    }
    #endregion
}
