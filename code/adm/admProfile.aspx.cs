﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_admProfile : System.Web.UI.Page
{
    #region "Properties"
    protected string _currentPageName;

    protected int admId = 0;
    protected Boolean boolAdmRoot = false;
    protected Boolean boolController = false;

    protected string currentPwd;
    protected string newPwd;
    protected string confirmPwd;

    // Image Properties
    protected string profileImage = "";
    protected decimal decImgWidth = Convert.ToDecimal("0.00");
    protected decimal decImgHeight = Convert.ToDecimal("0.00");
    protected decimal decImgX = Convert.ToDecimal("0.00");
    protected decimal decImgY = Convert.ToDecimal("0.00");
    protected string strImgCropped = "";
    protected string strImgName = "";
    protected string uploadPath = "";
    protected string aspectRatioX = "1";
    protected string aspectRatioY = "1";
    protected string aspectRatio = "1/1";
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CTRLID"] != null && Session["CTRLID"] != "")
        {
            boolController = true;
        }
        else if (Session["ADMROOT"] != null && Session["ADMROOT"] != "")
        {
            boolAdmRoot = true;
        }

        if (Session["ADMID"] != null && Session["ADMID"] != "")
        {
            //@ set flag to use back connection string for admin update
            Session[clsAdmin.CONSTADMINCS] = 1;

            admId = int.Parse(Session["ADMID"].ToString());
            setPageProperties();
            Session["PWDCHANGED"] = null;
            Session["IMGCHANGED"] = null;
        }

        Session[clsAdmin.CONSTADMINCS] = null;

        if (!IsPostBack)
        {
            cvNewPwd2.ErrorMessage = "<br />Password cannot be less than " + clsAdmin.CONSTADMPASSWORDMINLENGTH.ToString() + " characters.";
            cvConfirmPwd2.ErrorMessage = "<br />Password cannot be less than " + clsAdmin.CONSTADMPASSWORDMINLENGTH.ToString() + " characters.";
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            //@ set flag to use back connection string for admin
            Session[clsAdmin.CONSTADMINCS] = 1;
            int intRecordAffected = 0;
            bool isPwdSaved = false;
            bool isImgSaved = false;


            #region Save Password
            clsMD5 md5 = new clsMD5();
            string currentp = txtCurrentPwd.Text.Trim();
            currentPwd = md5.encrypt(currentp);

            string newp = txtNewPwd.Text.Trim();
            newPwd = md5.encrypt(newp);

            intRecordAffected = 0;
            if (!string.IsNullOrEmpty(currentp))
            {
                if (boolAdmRoot)
                {
                    clsAdmin adm = new clsAdmin();
                    intRecordAffected = adm.changeAdminPwd(admId, currentPwd, newPwd);
                }
                else if (boolController)
                {
                    clsController ctrl = new clsController();
                    intRecordAffected = ctrl.changeCtrlPwd(admId, currentPwd, newPwd);
                }
                else
                {
                    clsUser usr = new clsUser();
                    intRecordAffected = usr.changeUserPwd(admId, currentPwd, newPwd);
                }
            }
            isPwdSaved = intRecordAffected == 1;
            #endregion Save Password

            #region Save Profile Image
            string profilePath = Server.MapPath("~/data/profile/");
            if (!Directory.Exists(profilePath)){ Directory.CreateDirectory(profilePath); }

            string tempPath = Server.MapPath("~/data/temp/");
            if (!Directory.Exists(tempPath)) { Directory.CreateDirectory(tempPath); }

            HttpPostedFile file = Request.Files["fileProdImage"];
            uploadPath = Server.MapPath("~/data/profile/");
            bool boolHasCroppedImg = (!string.IsNullOrEmpty(hdnImgWidth.Value)) &&
                                     (!string.IsNullOrEmpty(hdnImgHeight.Value)) &&
                                     (!string.IsNullOrEmpty(hdnImgX.Value)) &&
                                     (!string.IsNullOrEmpty(hdnImgY.Value)) &&
                                     (!string.IsNullOrEmpty(hdnImgCropped.Value)) &&
                                     (!string.IsNullOrEmpty(hdnImgName.Value));


            bool boolHasFile = file != null && file.ContentLength > 0;
            string newFilename = "";
            if (boolHasCroppedImg || boolHasFile || hdnImage.Value == "")
            {
                try
                {
                    if (boolHasCroppedImg)
                    {
                        decImgWidth = Convert.ToDecimal(hdnImgWidth.Value);
                        decImgHeight = Convert.ToDecimal(hdnImgHeight.Value);
                        decImgX = Convert.ToDecimal(hdnImgX.Value);
                        decImgY = Convert.ToDecimal(hdnImgY.Value);
                        strImgCropped = hdnImgCropped.Value;
                        strImgName = hdnImgName.Value;

                        newFilename = clsMis.GetUniqueFilename(strImgName, uploadPath);
                        string base64 = strImgCropped;
                        byte[] bytes = Convert.FromBase64String(base64.Split(',')[1]);
                        profileImage = newFilename;

                        using (FileStream fs = new FileStream(uploadPath + newFilename, FileMode.Create))
                        {
                            using (BinaryWriter bw = new BinaryWriter(fs)) { bw.Write(bytes); bw.Close(); }
                        }
                    }
                    else if (boolHasFile)
                    {
                        string fileName = Path.GetFileName(file.FileName);
                        clsMis.createFolder(uploadPath);
                        profileImage = newFilename = clsMis.GetUniqueFilename(fileName, uploadPath);
                        file.SaveAs(uploadPath + newFilename);
                    }
                    else if (hdnImage.Value == "")
                    {
                        if (File.Exists(uploadPath + hdnImageRef.Value))
                        {
                            File.Delete(uploadPath + hdnImageRef.Value);
                        }
                    }

                    intRecordAffected = 0;
                    if (boolAdmRoot)
                    {
                        clsAdmin adm = new clsAdmin();
                        intRecordAffected = adm.changeProfileImage(admId, profileImage);
                    }
                    else if (boolController)
                    {
                        clsController ctrl = new clsController();
                        intRecordAffected = ctrl.changeProfileImage(admId, profileImage);
                    }
                    else
                    {
                        clsUser usr = new clsUser();
                        intRecordAffected = usr.changeProfileImage(admId, profileImage);
                    }
                    isImgSaved = intRecordAffected == 1;
                }
                catch (Exception ex)
                {
                    clsLog.logErroMsg(ex.Message.ToString());
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />" + ex.Message.ToString() + "</div>";

                    throw ex;
                }
            }

            

            #endregion Save profile Image


            if (isImgSaved || isPwdSaved)
            {
                Session["PWDCHANGED"] = isPwdSaved;
                Session["IMGCHANGED"] = isImgSaved;
                Response.Redirect(currentPageName);
            }
            else
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to change password. Please try again.</div>";
            }
        }
    }

    protected void validateCurrentPwd_server(object source, ServerValidateEventArgs args)
    {
        if (!string.IsNullOrEmpty(txtCurrentPwd.Text.Trim()))
        {
            clsMD5 md5 = new clsMD5();
            string strCurPwd = md5.encrypt(txtCurrentPwd.Text.Trim());
            string strNewPwd = md5.encrypt(txtNewPwd.Text.Trim());

            if (string.Compare(strCurPwd, hdnCurrentPwd.Value) != 0)
            {
                args.IsValid = false;
                cvCurrentPwd.ErrorMessage = "<br />Current password is not match with present one.";
            }
            else
            {
                if (string.Compare(strCurPwd, strNewPwd) == 0)
                {
                    args.IsValid = false;
                    cvCurrentPwd.ErrorMessage = "<br />New password should not be same as present one.";
                }
                else
                {
                    args.IsValid = true;
                }                
            }
        }
    }

    protected void cvCurrentPwd_PreRender(object source, EventArgs e)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("admpwd"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtCurrentPwd.Text.Trim() + "', document.all['" + cvCurrentPwd.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "admpwd", strJS, false);
        }
    }

    protected void validateImage_server(object source, ServerValidateEventArgs args)
    {
        HttpFileCollection uploads = Request.Files;

        HttpPostedFile postedFile = HttpContext.Current.Request.Files["fileProdImage"];
        if (postedFile.ContentLength > 0)
        {
            int intImgMaxSize = 0;
            if (Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING] != null && Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }


            if (postedFile.ContentLength > intImgMaxSize)
            {
                cvImage.ErrorMessage = "File size exceeded " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + ".";
                args.IsValid = false;
            }
            else
            {
                Match matchImage = Regex.Match(postedFile.FileName, clsAdmin.CONSTADMIMGALLOWEDEXT, RegexOptions.IgnoreCase);
                if (matchImage.Success)
                {
                    args.IsValid = true;
                }
                else
                {
                    cvImage.ErrorMessage = "Please enter valid product image. (" + clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + ").";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cvImage_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("ProdImage")))
        {
            string strJS = null;
            strJS = "<script type = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('fileProdImage', document.all['" + cvImage.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "ProdImage", strJS, false);
        }
    }

    protected void lnkbtnDeleteImage_Click(object sender, EventArgs e)
    {
        pnlImage.Visible = false;
        hdnImage.Value = "";
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            litPageTitle.Text = GetLocalResourceObject("litPageTitle.Text").ToString();

            clsConfig config = new clsConfig();
            string strLandingPage = !string.IsNullOrEmpty(config.admLandingPage) ? config.admLandingPage : "adm00.aspx";

            lnkbtnCancel.OnClientClick = "javascript:document.location.href='" + strLandingPage + "'; return false;";

            string strUserEmail = "";
            string strUserPwd = "";
            string strUserImage = "";
            if (boolAdmRoot)
            {
                clsAdmin adm = new clsAdmin();
                if (adm.extractAdminById(admId, 0))
                {
                    strUserEmail = adm.email;
                    strUserPwd = adm.password;
                    strUserImage = adm.image;
                }
            }
            else if (boolController)
            {
                clsController ctrl = new clsController();
                if (ctrl.extractControllerById(admId, 0))
                {
                    strUserEmail = ctrl.ctrlEmail;
                    strUserPwd = ctrl.ctrlPassword;
                    strUserImage = ctrl.ctrlImage;

                }
            }
            else
            {
                clsUser usr = new clsUser();

                if (usr.extractUserById(admId, 0))
                {
                    strUserEmail = usr.userEmail;
                    strUserPwd = usr.userPwd;
                    strUserImage = usr.userImage;
                }
            }

            txtUserId.Text = strUserEmail;
            hdnCurrentPwd.Value = strUserPwd;
            if (strUserImage != "")
            {
                pnlImage.Visible = true;
                imgProfile.ImageUrl = ConfigurationManager.AppSettings["uplBase"] + "profile/" + strUserImage;
                hdnImage.Value = strUserImage;
                hdnImageRef.Value = strUserImage;
            }

            // Image properties
            Page.Form.Attributes.Add("enctype", "multipart/form-data");
            int intImgMaxSize = 0;
            if (Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING] != null && Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING] != "")
            {
                if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING]) > 0)
                {
                    intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING]) * 1024;
                }
                else
                {
                    if (Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != null && Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] != "")
                    {
                        if (Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) > 0)
                        {
                            intImgMaxSize = Convert.ToInt32(Session[clsAdmin.CONSTPROJECTDEFAULTSETTING]) * 1024;
                        }
                    }
                }
            }
            litUploadRule.Text = clsMis.formatAllowedFileExt(clsAdmin.CONSTADMIMGALLOWEDEXT) + " | " + clsMis.formatAllowedFileSize(intImgMaxSize, "kb") + " | " + clsMis.formatAllowedFileDimension(clsAdmin.CONSTADMIMGALLOWEDWIDTH, clsAdmin.CONSTADMIMGALLOWEDHEIGHT, "px");

            if (Session["PWDCHANGED"] != null || Session["IMGCHANGED"] != null)
            {
                pnlAck.Visible = true;
                //litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";

                litAck.Text = "<div class='noticemsg'>";
                litAck.Text += "Saved ";
                litAck.Text += Convert.ToBoolean(Session["PWDCHANGED"]) ? "Password" : "";
                litAck.Text += Convert.ToBoolean(Session["PWDCHANGED"]) && Convert.ToBoolean(Session["IMGCHANGED"]) ? " & " : "";
                litAck.Text += Convert.ToBoolean(Session["IMGCHANGED"]) ? "Profile Image" : "";
                litAck.Text += "</div>";
                clsMis.resetListing();
            }
        }
    }
    #endregion
}