﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admGroup0101.aspx.cs" Inherits="adm_admGroup0101" ValidateRequest="false"%>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmGroupDetails.ascx" TagName="AdmGroupDetails" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmGroupDesc.ascx" TagName="AdmGroupDesc" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
        <div class="action">
            <a href="#" class="hypListingAction setting" onclick="toggleSideMenu();">Page Detail</a>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlForm" runat="server" CssClass="divForm">
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <asp:Panel ID="pnlFormSect1" runat="server">
            <uc:AdmGroupDetails ID="ucAdmGroupDetails" runat="server" pageListingURL="admGroup01.aspx" />
        </asp:Panel>
        <asp:Panel ID="pnlFormSect2" runat="server" Visible="true">
            <uc:AdmGroupDesc ID="ucAdmGroupDesc" runat="server" pageListingURL="admGroup01.aspx" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>