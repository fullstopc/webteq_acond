﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class adm_admHighlight0101 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 28; //need change sectId according to db

    protected int _mode = 1;
    protected int _highId = 0;
    protected int _formSect = 1;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    public string strSavedMsg
    {
        get { return String.Format(GetGlobalResourceObject("GlobalResource", "admSavedMessageWithParams.Text").ToString(), "Event"); }
    }
    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set { ViewState["FORMSECT"] = value; }
    }

    public int highId
    {
        get
        {
            if (ViewState["HIGHID"] == null)
            {
                return _highId;
            }
            else
            {
                return int.Parse(ViewState["HIGHID"].ToString());
            }
        }
        set { ViewState["HIGHID"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;
                if (int.TryParse(Request["id"], out intTryParse))
                {
                    highId = intTryParse;
                }
                else
                {
                    highId = 0;
                }
            }
            else if (Session["NEWHIGHID"] != null)
            {
                try
                {
                    highId = int.Parse(Session["NEWHIGHID"].ToString());
                }
                catch (Exception ex)
                {
                    highId = 0;
                }
            }
            else if (Session["EDITEDHIGHID"] != null)
            {
                try
                {
                    highId = int.Parse(Session["EDITEDHIGHID"].ToString());
                }
                catch (Exception ex)
                {
                    highId = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["frm"]))
            {
                int intTryParse;

                if (int.TryParse(Request["frm"], out intTryParse))
                {
                    formSect = intTryParse;
                }
                else
                {
                    formSect = 1;
                }
            }

            if (Request["id"] != null || Session["EDITEDHIGHID"] != null)
            {
                mode = 2;
            }

            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMEVENTMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.parentId = sectId;
            Master.showSideMenu = true;
            Master.subSectId = clsMis.getSubSectId(sectId, formSect, 1);
            Master.id = highId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            Session["ORIIMGTITLE"] = "";
            Session["ORIIMGUPLOAD"] = "";
            Session["DELIMGTITLE"] = "";
            Session["DELIMGUPLOAD"] = "";

            Session["ORIFILETITLE"] = "";
            Session["ORIFILEUPLOAD"] = "";
            Session["DELFILETITLE"] = "";
            Session["DELFILEUPLOAD"] = "";

            Session["FILETITLE"] = "";
            Session["FILETITLE_ZH"] = "";
            Session["FILEUPLOAD"] = "";
        }

        setPageProperties();

        Session["NEWHIGHID"] = null;
        Session["EDITEDHIGHID"] = null;
        Session["NOCHANGE"] = null;
        Session["DELETEDHIGHNAME"] = null;
        Session["ERRMSG"] = null;
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        switch (mode)
        {
            case 1:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleAdd.Text").ToString(), true);
                break;
            case 2:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleEdit.Text").ToString(), true);
                break;
            default:
                break;
        }

        if (!IsPostBack)
        {
            switch (mode)
            {
                case 1:
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleAdd.Text").ToString();

                    clsHighlight high = new clsHighlight();
                    int intTotal = high.getTotalRecord(0);
                    if (clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITEVENT, intTotal)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "adm/admHighlight01.aspx"); }
                    break;
                case 2:
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleEdit.Text").ToString();
                    break;
                default:
                    break;
            }

            if (Session["NEWHIGHID"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + strSavedMsg + "</div>";

                clsMis.resetListing();
            }
            else if (Session["EDITEDHIGHID"] != null)
            {
                if (Session["NOCHANGE"] != null)
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
                }
                else
                {
                    int intTotal = new clsHighlight().getTotalRecord(0);
                    string strUrlFormpage = "<a href='admHighlight0101.aspx'>Add New</a>";
                    string strUrlListpage = "<a href='admHighlight01.aspx'>Back to Listing</a>";

                    litAck.Text = "<div class=\"noticemsg\">" + strSavedMsg + "  " + strUrlListpage + "";
                    if (!clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITEVENT, intTotal - 1))
                    {
                        litAck.Text += " & " + strUrlFormpage + "";
                    }
                    litAck.Text += "</div>";
                    clsMis.resetListing();
                }

                pnlAck.Visible = true;
            }
            else if (Session["DELETEDHIGHNAME"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }
            else if (Session["ERRMSG"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
            }
        }

        switch (formSect)
        {
            case 1:
                ucAdmHighlightDetails.registerScript();
                ucAdmHighlightDetails.mode = mode;
                ucAdmHighlightDetails.highId = highId;
                ucAdmHighlightDetails.fill();

                ucAdmDetail.header = "Event Details";
                ucAdmDetail.boolShowDesc = true;

                pnlFormDetails.Visible = true;
                pnlFormInner.Visible = false;
                break;
            case 2:
                ucAdmHighlightGallery.mode = mode;
                ucAdmHighlightGallery.highId = highId;
                ucAdmHighlightGallery.fill();

                ucAdmDetail.header = "Event Gallery";
                ucAdmDetail.boolShowDesc = false;

                pnlFormDetails.Visible = false;
                pnlFormInner.Visible = true;
                pnlFormGallery.Visible = true;
                pnlFormDesc.Visible = false;
                pnlFormVideo.Visible = false;
                pnlFormArticle.Visible = false;
                pnlFormComment.Visible = false;
                break;
            case 3:
                ucAdmHighlightDesc.registerCKEditorScript();
                ucAdmHighlightDesc.fill(mode);

                ucAdmDetail.header = "Event Description";
                ucAdmDetail.boolShowDesc = false;

                pnlFormDetails.Visible = false;
                pnlFormInner.Visible = true;
                pnlFormDesc.Visible = true;
                pnlFormGallery.Visible = false;
                pnlFormVideo.Visible = false;
                pnlFormArticle.Visible = false;
                pnlFormComment.Visible = false;
                break;
            case 4:
                ucAdmHighlightVideo.fill(mode);

                ucAdmDetail.header = "Event Video";
                ucAdmDetail.boolShowDesc = false;

                pnlFormDetails.Visible = false;
                pnlFormInner.Visible = true;
                pnlFormDesc.Visible = false;
                pnlFormGallery.Visible = false;
                pnlFormVideo.Visible = true;
                pnlFormArticle.Visible = false;
                pnlFormComment.Visible = false;
                break;
            case 5:
                ucAdmHighlightArticle.fill(mode);

                ucAdmDetail.header = "Event Article";
                ucAdmDetail.boolShowDesc = false;

                pnlFormDetails.Visible = false;
                pnlFormInner.Visible = true;
                pnlFormDesc.Visible = false;
                pnlFormGallery.Visible = false;
                pnlFormVideo.Visible = false;
                pnlFormArticle.Visible = true;
                pnlFormComment.Visible = false;
                break;
            case 6:
                ucAdmHighlightComment.fill(mode);

                ucAdmDetail.header = "Event Comment";
                ucAdmDetail.boolShowDesc = false;

                pnlFormDetails.Visible = false;
                pnlFormInner.Visible = true;
                pnlFormDesc.Visible = false;
                pnlFormGallery.Visible = false;
                pnlFormVideo.Visible = false;
                pnlFormArticle.Visible = false;
                pnlFormComment.Visible = true;
                break;
        }

        if (formSect > 1)
        {
            clsHighlight high = new clsHighlight();
            if (high.extractHighlightById(highId, 0))
            {
                ucAdmDetail.title = high.highTitle;
                ucAdmDetail.shortDesc = high.highShortDesc;

                ucAdmDetail.fill();
            }
        }
    }
    #endregion
}
