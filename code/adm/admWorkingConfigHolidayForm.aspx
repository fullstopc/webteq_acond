﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admWorkingConfigHolidayForm.aspx.cs" Inherits="adm_adm" ValidateRequest="false" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/mst/blank.Master" %>
<asp:Content ID="Content" ContentPlaceHolderID="cphHead" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="container-fluid holiday-form">
        <div class="row">
            <label class="col-sm-1"><b>Date From:</b></label>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class='input-group date' id='datepickerODFrom'>
                        <asp:TextBox runat="server" ID="txtODFrom" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <asp:RequiredFieldValidator ID="rvDate" 
                 ControlToValidate="txtODFrom"
                 Display="Dynamic"
                 CssClass="errmsg"
                 ErrorMessage="Please enter date from."
                 runat="server"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-1"><b>Date To:</b></label>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class='input-group date' id='datepickerODTo'>
                        <asp:TextBox runat="server" ID="txtODTo" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <asp:RequiredFieldValidator ID="rvDateTo" 
                 ControlToValidate="txtODTo"
                 Display="Dynamic"
                 CssClass="errmsg"
                 ErrorMessage="Please enter date to."
                 runat="server"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-1"><b>Remark :</b></label>
            <div class="col-sm-3">
                <asp:TextBox runat="server" ID="txtRemark" CssClass="text_fullwidth"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <asp:Label ID="lblErrOD" runat="server" Text="" Visible="false"></asp:Label>
        </div>
        <div class="row">
            <div class="col">
                <asp:LinkButton runat="server" ID="lnkbtnSave" OnClick="lnkbtnSave_Click" ToolTip="Save" CssClass="btn btnSave right">Save</asp:LinkButton>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#datepickerODFrom, #datepickerODTo').datetimepicker({
                format: 'DD-MMM-YYYY',
            });
        })
    </script>
</asp:Content>
