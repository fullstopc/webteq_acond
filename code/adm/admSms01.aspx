﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admSms01.aspx.cs" Inherits="adm_admSms01" validateRequest="false" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmSmsSetup.ascx" TagName="AdmSmsSetup" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server" meta:resourcekey="litPageTitle"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlForm" runat="server" CssClass="divForm">
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <uc:AdmSmsSetup ID="ucAdmSmsSetup" runat="server" />
    </asp:Panel>
</asp:Content>

