﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class adm_admCMSControlPanel01 : AdminPage
{
    #region "Properties"
    protected int sectId = 1001;
    #endregion


    #region "Property Methods"
    public string currentPageName 
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        string pageTitle = "Working Period";
        Page.Title += clsMis.formatPageTitle(pageTitle, true);
        litPageTitle.Text = pageTitle;
        if (!IsPostBack)
        {

            Master.sectId = sectId;
            Master.parentId = 1;
            Master.moduleDesc = pageTitle;

            bindWeekDayRepeater();
            fillInTimeSlotToDropDown();
        }
    }
    #endregion


    #region "Methods"
    protected void bindWeekDayRepeater()
    {
        DataView week = new clsWorkingConfigWeek().getDataView();
        rptDays.DataSource = week;
        rptDays.DataBind();
    }

    protected void fillInTimeSlotToDropDown()
    {
        foreach (RepeaterItem item in rptDays.Items)
        {
            DropDownList ddlTimeSlot = item.FindControl("ddlTimeSlot") as DropDownList;
            HiddenField hdnWeekID = item.FindControl("hdnWeekID") as HiddenField;

            #region 1. Fill in Dropdown Selections
            clsWorkingConfigTimeslot workingTimeSlot = new clsWorkingConfigTimeslot();
            var listTimeSlot = workingTimeSlot.getGroupName().ToList();

            List<ListItem> items = new List<ListItem>();
            foreach(var timeSlot in listTimeSlot)
            {
                items.Add(new ListItem(timeSlot, timeSlot));
            }
            ddlTimeSlot.Items.AddRange(items.ToArray());
            #endregion

            #region 2. Fill in Dropdown Value
            clsWorkingConfigWeek weekDay = new clsWorkingConfigWeek();
            var listValue = weekDay.getDayTimeSlot();
            ddlTimeSlot.SelectedValue = listValue.Where(x => x.WeekID.ToString() == hdnWeekID.Value)
                                                 .Select(x => x.WeekGroup)
                                                 .FirstOrDefault();
            #endregion
        }
    }
    #endregion


    #region "Button Events"
    protected void lnkbtnSaveWT_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            #region Model Validation
            bool isValidInput = true;
            DateTime timeFrom = DateTime.MinValue;
            DateTime timeTo = DateTime.MinValue;
            lblErrWT.Visible = false;
            lblErrWT.Text = "";

            TextBox txtGroup = (TextBox)this.AdmWorkingTimeGroup.FindControl("txtWTGroup");

            if (string.IsNullOrEmpty(txtWTFrom.Text) || string.IsNullOrEmpty(txtWTTo.Text) || string.IsNullOrEmpty(txtGroup.Text))
            {
                lblErrWT.Visible = true;
                lblErrWT.Text += "From/To/Group fields is required. ";
                lblErrWT.ForeColor = System.Drawing.Color.Red;
                isValidInput = false;
            }
            else
            {
                if (!DateTime.TryParse(txtWTFrom.Text, out timeFrom))
                {
                    lblErrWT.Visible = true;
                    lblErrWT.Text += "From Time Format is not valid.";
                    lblErrWT.ForeColor = System.Drawing.Color.Red;
                    isValidInput = false;
                }

                if (!DateTime.TryParse(txtWTTo.Text, out timeTo))
                {
                    lblErrWT.Visible = true;
                    lblErrWT.Text += "To Time Format is not valid.";
                    lblErrWT.ForeColor = System.Drawing.Color.Red;
                    isValidInput = false;
                }

                if (isValidInput)
                {
                    if (timeTo < timeFrom)
                    {
                        lblErrWT.Visible = true;
                        lblErrWT.Text += "From Time cannot greater than To Time.";
                        lblErrWT.ForeColor = System.Drawing.Color.Red;
                        isValidInput = false;
                    }
                }
            }
            #endregion

            #region Add Time Slot
            if (isValidInput)
            {
                clsWorkingConfigTimeslot timeslot = new clsWorkingConfigTimeslot();
                timeslot.timeFrom = Convert.ToDateTime("1900-01-01 " + txtWTFrom.Text);
                timeslot.timeTo = Convert.ToDateTime("1900-01-01 " + txtWTTo.Text);
                timeslot.group = txtGroup.Text;
                timeslot.ID = -1;

                if (timeslot.isCrashTime() || timeslot.isSame())
                {
                    lblErrWT.Visible = true;
                    lblErrWT.Text += "This time conflict with other time slot.";
                    lblErrWT.ForeColor = System.Drawing.Color.Red;
                    isValidInput = false;
                }
                else
                {
                    int intAdmID = Convert.ToInt32(Session["ADMID"]);
                    string msg = "";

                    if (timeslot.add())
                    {
                        msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();
                    }
                    else
                    { 
                        msg = GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString();
                    }

                    Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.TimeslotSaveSuccess('" + msg + "')", true);
                }
            }
            #endregion
        }
    }

    protected void lnkbtnSaveWeek_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            foreach(RepeaterItem item in rptDays.Items)
            {
                HiddenField hdnWeekID = item.FindControl("hdnWeekID") as HiddenField;
                CheckBox chkboxDay = item.FindControl("chkboxDay") as CheckBox;
                DropDownList ddlTimeSlot = item.FindControl("ddlTimeSlot") as DropDownList;

                clsWorkingConfigWeek config = new clsWorkingConfigWeek();
                config.ID = Convert.ToInt16(hdnWeekID.Value);
                config.group = ddlTimeSlot.Text;
                config.dayChecked = chkboxDay.Checked ? 1 : 0;
                config.day = chkboxDay.Text;
                config.update();
            }

            pnlWeekAck.Visible = true;
            litWeekAckMsg.Text = "Save successful.";
        }
    }

    protected void lnkbtnSaveOD_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            #region Model Validation
            bool isValidInput = true;
            DateTime dtFrom = DateTime.MinValue;
            DateTime dtTo = DateTime.MinValue;
            lblErrOD.Visible = false;
            lblErrOD.Text = "";

            if (string.IsNullOrEmpty(txtODFrom.Text) || string.IsNullOrEmpty(txtODTo.Text))
            {
                lblErrOD.Visible = true;
                lblErrOD.Text += "From/To fields is required. ";
                lblErrOD.ForeColor = System.Drawing.Color.Red;
                isValidInput = false;
            }
            else
            {
                if (!DateTime.TryParse(txtODFrom.Text, out dtFrom))
                {
                    lblErrOD.Visible = true;
                    lblErrOD.Text += "From Date Format is not valid.";
                    lblErrOD.ForeColor = System.Drawing.Color.Red;
                    isValidInput = false;
                }

                if (!DateTime.TryParse(txtODTo.Text, out dtTo))
                {
                    lblErrOD.Visible = true;
                    lblErrOD.Text += "To Date Format is not valid.";
                    lblErrOD.ForeColor = System.Drawing.Color.Red;
                    isValidInput = false;
                }

                if (isValidInput)
                {
                    if (dtTo < dtFrom)
                    {
                        lblErrOD.Visible = true;
                        lblErrOD.Text += "From Date cannot greater than To Date.";
                        lblErrOD.ForeColor = System.Drawing.Color.Red;
                        isValidInput = false;
                    }
                }
            }
            #endregion

            #region Add Off Day
            if (isValidInput)
            {
                clsWorkingConfigHoliday holiday = new clsWorkingConfigHoliday();
                holiday.date = Convert.ToDateTime(txtODFrom.Text);
                holiday.dateto = Convert.ToDateTime(txtODTo.Text);
                holiday.remark = txtODRemark.Text;
                holiday.ID = -1;

                if (holiday.isCrashDate() || holiday.isSame())
                {
                    lblErrWT.Visible = true;
                    lblErrWT.Text += "This time conflict with other time slot.";
                    lblErrWT.ForeColor = System.Drawing.Color.Red;
                    isValidInput = false;
                }
                else
                {
                    int intAdmID = Convert.ToInt32(Session["ADMID"]);
                    string msg = "";

                    if (holiday.add())
                    {
                        msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();
                    }
                    else
                    {
                        msg = GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString();
                    }

                    Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.HolidaySaveSuccess('" + msg + "')", true);
                }
            }
            #endregion
        }
    }

    protected void lnkbtnDeleteWT_Click(object sender, EventArgs e)
    {
        int objectID = int.Parse(hdnWTID.Value);
        clsWorkingConfigTimeslot timeslot = new clsWorkingConfigTimeslot();
        timeslot.extractByID(objectID);

        string msg = "";
        if (timeslot.delete() == 1)
        {
            msg = GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString();
        }
        else
        {
            msg = GetGlobalResourceObject("GlobalResource", "admFailedDelete.Text").ToString();
        }
        Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.TimeslotSaveSuccess('" + msg + "')", true);
    }

    protected void lnkbtnDeleteOD_Click(object sender, EventArgs e)
    {
        int objectID = int.Parse(hdnODID.Value);
        clsWorkingConfigHoliday holiday = new clsWorkingConfigHoliday();
        holiday.extractByID(objectID);

        string msg = "";
        if (holiday.delete() == 1)
        {
            msg = GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString();
        }
        else
        {
            msg = GetGlobalResourceObject("GlobalResource", "admFailedDelete.Text").ToString();
        }
        Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.HolidaySaveSuccess('" + msg + "')", true);
    }
    #endregion
}
