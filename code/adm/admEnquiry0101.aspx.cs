﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_admEnquiry0101 : System.Web.UI.Page
{
    #region "Properties"
    protected int _id = 0;
    protected int sectId = 70;
    #endregion

    #region " Property Methods"
    public int id
    {
        get
        {
            if (ViewState["ID"] == null)
            {
                return _id;
            }
            else
            {
                return Convert.ToInt16(ViewState["ID"]);
            }
        }
        set
        {
            ViewState["ID"] = value;
        }
    }
    #endregion

    #region "Page Event"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    id = Convert.ToInt16(Request["id"]);
                }
                catch (Exception ex)
                {
                    id = 0;
                }
            }

            if(Session["DATA_SUCCESS"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">Email resend successful.</div>";
            }
            if (Session["ERRMSG"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = Session["ERRMSG"].ToString();
            }

        }
        Session["ERRMSG"] = null;
        Session["DATA_SUCCESS"] = null;
        setPageProperties();
    }
    #endregion

    #region "Methods"
    public void setPageProperties()
    {
        ucAdmEnquiry.id = id;
        ucAdmEnquiry.fill();
    }

    #endregion
}