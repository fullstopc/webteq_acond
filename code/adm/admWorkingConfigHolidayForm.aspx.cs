﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;
public partial class adm_adm : System.Web.UI.Page
{
    #region "Properties"
    protected int _mode = 1;
    protected int _holidayID = 0;
    #endregion

    #region "Property Methods"
    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    public int holidayID
    {
        get
        {
            if (ViewState["HOLIDAY_ID"] == null)
            {
                return _holidayID;
            }
            else
            {
                return int.Parse(ViewState["HOLIDAY_ID"].ToString());
            }
        }
        set
        {
            ViewState["HOLIDAY_ID"] = value;
        }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    holidayID = int.Parse(Request["id"]);
                }
                catch (Exception ex)
                {
                    holidayID = 0;
                }
            }
           
            if (holidayID > 0)
            {
                mode = 2;
            }

            if(mode == 2)
            {
                fillform();
            }

        }

    }
    #endregion

    #region "Methods"
    private void fillform()
    {
        clsWorkingConfigHoliday holiday = new clsWorkingConfigHoliday();
        if (holiday.extractByID(holidayID))
        {
            txtODFrom.Text = holiday.date.ToString("dd-MMM-yyyy");
            txtODTo.Text = holiday.date.ToString("dd-MMM-yyyy");
            txtRemark.Text = holiday.remark;
        }
    }
    #endregion

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if(Page.IsValid)
        {
            #region Model Validation
            bool isValidInput = true;
            DateTime dtFrom = DateTime.MinValue;
            DateTime dtTo = DateTime.MinValue;
            lblErrOD.Visible = false;
            lblErrOD.Text = "";

            if (string.IsNullOrEmpty(txtODFrom.Text) || string.IsNullOrEmpty(txtODTo.Text))
            {
                lblErrOD.Visible = true;
                lblErrOD.Text += "From/To fields is required. ";
                lblErrOD.ForeColor = System.Drawing.Color.Red;
                isValidInput = false;
            }
            else
            {
                if (!DateTime.TryParse(txtODFrom.Text, out dtFrom))
                {
                    lblErrOD.Visible = true;
                    lblErrOD.Text += "From Date Format is not valid.";
                    lblErrOD.ForeColor = System.Drawing.Color.Red;
                    isValidInput = false;
                }

                if (!DateTime.TryParse(txtODTo.Text, out dtTo))
                {
                    lblErrOD.Visible = true;
                    lblErrOD.Text += "To Date Format is not valid.";
                    lblErrOD.ForeColor = System.Drawing.Color.Red;
                    isValidInput = false;
                }

                if (isValidInput)
                {
                    if (dtTo < dtFrom)
                    {
                        lblErrOD.Visible = true;
                        lblErrOD.Text += "From Date cannot greater than To Date.";
                        lblErrOD.ForeColor = System.Drawing.Color.Red;
                        isValidInput = false;
                    }
                }
            }
            #endregion

            #region Edit Off Day
            int intAdmID = Convert.ToInt32(Session["ADMID"]);

            clsWorkingConfigHoliday holiday = new clsWorkingConfigHoliday();
            holiday.date = dtFrom;
            holiday.dateto = dtTo;
            holiday.remark = txtRemark.Text;
            holiday.ID = mode == 2 ? holidayID : -1;
            
            string msg = "";
            bool Success = true;
            bool Updated = false;

            if (mode == 1)
            {
                if (holiday.add())
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();
                }
            }
            else if (mode == 2)
            {
                if (!holiday.isSame() && !holiday.isCrashDate())
                {
                    if (holiday.update())
                    {
                        Updated = true;
                        msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();
                    }
                    else
                    {
                        Success = false;
                    }
                }

                if (Success && !Updated)
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString();
                }
            }

            if (msg == "")
            {
                msg = GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString();
            }

            Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.HolidaySaveSuccess('" + msg + "')", true);
            #endregion
        }
    }
}