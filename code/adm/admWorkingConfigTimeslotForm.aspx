﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admWorkingConfigTimeslotForm.aspx.cs" Inherits="adm_adm" ValidateRequest="false" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/mst/blank.Master" %>
<%@ Register Src="~/ctrl/ucAdmWorkingTimeGroup.ascx" TagName="AdmWorkingTimeGroup" TagPrefix="uc" %>
<asp:Content ID="Content" ContentPlaceHolderID="cphHead" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="container-fluid timeslot-form">
        <div class="row">
            <label class="col-sm-1"><b>Time From:</b></label>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class='input-group date' id='timepickerWTFrom'>
                        <asp:TextBox runat="server" ID="txtWTFrom" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <asp:RequiredFieldValidator ID="rvTime" 
                 ControlToValidate="txtWTFrom"
                 Display="Dynamic"
                 CssClass="errmsg"
                 ErrorMessage="Please enter time from."
                 runat="server"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-1"><b>Date To:</b></label>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class='input-group date' id='timepickerWTTo'>
                        <asp:TextBox runat="server" ID="txtWTTo" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <asp:RequiredFieldValidator ID="rvTimeTo" 
                 ControlToValidate="txtWTTo"
                 Display="Dynamic"
                 CssClass="errmsg"
                 ErrorMessage="Please enter time to."
                 runat="server"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-1"><b>Group :</b></label>
            <div class="col-sm-3">
                <uc:AdmWorkingTimeGroup ID="AdmWorkingTimeGroup" runat="server" />
            </div>
        </div>
        <div class="row">
            <asp:Label ID="lblErrWT" runat="server" Text="" Visible="false"></asp:Label>
        </div>
        <div class="row">
            <div class="col">
                <asp:LinkButton runat="server" ID="lnkbtnSave" OnClick="lnkbtnSave_Click" ToolTip="Save" CssClass="btn btnSave right">Save</asp:LinkButton>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#timepickerWTFrom, #timepickerWTTo').datetimepicker({
                format: 'LT',
            });
        })
    </script>
</asp:Content>