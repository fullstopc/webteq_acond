﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admPageBackupList.aspx.cs" Inherits="adm_admPageBackupList" %>
<%@ MasterType VirtualPath="~/mst/blank.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="divListing">
        <div class="divListingHdr">
            <div class="divListingHdrLeft"><asp:Literal ID="litItemFound" runat="server"></asp:Literal></div>
        </div>
        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
            <div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
        </asp:Panel>
        <div class="divListingData divPageManagerGridContainer">
            <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom" CssClass="dataTbl" OnRowDataBound="gvItems_RowDataBound" OnRowCommand="gvItems_RowCommand" DataKeyNames="PAGEBACKUP_ID" EmptyDataText="No record found.">
                <PagerStyle CssClass="pg" HorizontalAlign="Right" />
                <AlternatingRowStyle CssClass="td_alt" />
                <Columns>
                    <asp:TemplateField HeaderText="no.">
                        <ItemStyle CssClass="tdNo" />
                        <ItemTemplate>
                            <asp:Literal ID="litNo" runat="server" Text="<%#Container.DataItemIndex + 1 %>"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="backup date" DataField="PAGEBACKUP_DATE" SortExpression="PAGEBACKUP_DATE"  ItemStyle-CssClass="tdCreation" DataFormatString="{0:dd MMM yyyy hh:mm:ss tt}"/>
                    
                    <asp:TemplateField HeaderText="action">
                        <ItemStyle CssClass="tdAct" />
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnRestore" runat="server" CommandName="cmdRestore">Restore</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>

