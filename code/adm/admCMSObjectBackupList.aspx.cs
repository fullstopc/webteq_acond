﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class adm_admCMSObjectBackupList : System.Web.UI.Page
{
    #region "Properties"
    protected int _pgId = 0;
    protected int _type = 0;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int cmsId
    {
        get
        {
            if (ViewState["CMSID"] == null)
            {
                return _pgId;
            }
            else
            {
                return int.Parse(ViewState["CMSID"].ToString());
            }
        }
        set { ViewState["CMSID"] = value; }
    }

    protected int type
    {
        get
        {
            if (ViewState["TYPE"] == null)
            {
                return _type;
            }
            else
            {
                return Convert.ToInt16(ViewState["TYPE"]);
            }
        }
        set { ViewState["TYPE"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    cmsId = int.Parse(Request["id"]);
                }
                catch (Exception ex)
                {
                    cmsId = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["type"]))
            {
                try
                {
                    type = Convert.ToInt16(Request["type"]);
                }
                catch (Exception ex)
                {
                    type = 0;
                }
            }

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMELISTINGSIZE, clsConfig.CONSTGROUPOTHERSETTINGS, 1);

            gvItems.PageSize = (!string.IsNullOrEmpty(config.value)) ? Convert.ToInt16(config.value) : clsAdmin.CONSTADMPAGESIZE;
            setPageProperties();
        }
    }

    protected void gvItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

    protected void gvItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "cmdRestore")
        {
            int cmsId = Convert.ToInt32(e.CommandArgument);

            clsCMSObject cms = new clsCMSObject();
            if (cms.extractCMSBackupByBackupId(cmsId))
            {
                int intRecordAffected = cms.updateCMSContentById(cms.cmsId, cms.cmsBackupContent);
                Response.Write("<script>parent.tb_remove(); self.parent.location.href='admCMS0101.aspx?id=" + cms.cmsId + "'; alert('Successfully Restore.');</script>");
            }
        }
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        DataSet ds = new DataSet();
        DataView dv = new DataView();

        clsCMSObject cms = new clsCMSObject();
        ds = cms.getCMSBackupDataset(cmsId);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "CMSBACKUP_DATE DESC";

        gvItems.DataSource = dv;
        gvItems.DataBind();
    }
    #endregion
}
