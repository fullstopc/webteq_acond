﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.IO;
using System.Configuration;

public partial class adm_admSlide01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 10;
    protected int intNumItemPerRow = 6;
    protected string _gvSortExpression = "MAST_ID";
    protected string _gvSortDirection = clsAdmin.CONSTSORTASC;
    protected string _gvSort = "";

    protected string strKeyword;
    protected Boolean boolSearch = false;
    #endregion

    #region "Property Methods"


    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    protected string gvSortExpression
    {
        get { return ViewState["GVSORTEXPRESSION"] as string ?? _gvSortExpression; }
        set { ViewState["GVSORTEXPRESSION"] = value; }
    }

    protected string gvSortDirection
    {
        get { return ViewState["GVSORTDIRECTION"] as string ?? _gvSortDirection; }
        set { ViewState["GVSORTDIRECTION"] = value; }
    }

    protected string gvSort
    {
        get { return ViewState["GVSORT"] as string ?? _gvSort; }
        set { ViewState["GVSORT"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMSLIDESHOWMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            txtKeyword.Attributes.Add("onkeydown", "javascript:if(event.keyCode == 13){document.getElementById('" + lnkbtnGo.ClientID + "').click(); return false;}");

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMELISTINGSIZE, clsConfig.CONSTGROUPOTHERSETTINGS, 1);


            setPageProperties();
            bindRptData();
            //bindGV(rptAblImg, "MASTHEAD_ID ASC");
 
        }

        if (Session["DELETEDMASTNAME"] != null)
        {
            pnlAck.Visible = true;
            litAck.Text = "Masthead Image (" + Session["DELETEDMASTNAME"] + ") has been deleted successfully.";
            litAck.Text = "<div class=\"noticemsg\">" + litAck.Text + "</div>";

            Session["DELETEDMASTNAME"] = null;

            clsMis.resetListing();
        }
    }

    protected void lnkbtnGo_Click(object sender, EventArgs e)
    {
        boolSearch = true;

        //bindGV(rptAblImg, "MASTHEAD_ID ASC");
    }

    //protected void rptAblImg_ItemDataBound(object sender, DataListItemEventArgs e)
    //{
    //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //    {
    //        string strMastheadName = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_NAME").ToString();
    //        string strMastheadId = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_ID").ToString();
    //        string strMastheadImg = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_IMAGE").ToString();

    //        LinkButton lnkbtnEdit = (LinkButton)e.Item.FindControl("lnkbtnEdit");
    //        LinkButton lnkbtnDelete = (LinkButton)e.Item.FindControl("lnkbtnDelete");

    //        lnkbtnEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
    //        lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
    //        lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteMasthead.Text") + "');";

    //        if (string.IsNullOrEmpty(strMastheadImg))
    //        {
    //            clsConfig config = new clsConfig();
    //            strMastheadImg = ConfigurationManager.AppSettings["imageBase"] + "cmn/" + config.defaultGroupImg;
    //        }

    //        else
    //        {
    //            strMastheadImg = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + strMastheadImg + "&f=1";
    //        }

    //        Image imgAblImg = (Image)e.Item.FindControl("imgAblImg");
    //        imgAblImg.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + strMastheadImg + "&w=263&h=198";
    //        imgAblImg.AlternateText = strMastheadName;
    //        imgAblImg.ToolTip = strMastheadName;
    //    }
    //}

    protected void rptAblImg_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdEdit")
        {
            int mastId = Convert.ToInt16(e.CommandArgument);
            Response.Redirect("admSlide0101.aspx?id=" + mastId);
        }
        else if (e.CommandName == "cmdDelete")
        {
            int mastId = Convert.ToInt16(e.CommandArgument);
            string uploadPath = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER + "/";
            string strDeletedMastheadName = "";
            clsMis.performDeleteMasthead(mastId, uploadPath, ref strDeletedMastheadName);
            Session["DELETEDMASTNAME"] = strDeletedMastheadName;

            Response.Redirect(currentPageName);
        }
    }

    protected void rptAblImg_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strMastheadName = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_NAME").ToString();
            string strMastheadId = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_ID").ToString();
            string strMastheadImg = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_IMAGE").ToString();

            //LinkButton lnkbtnEdit = (LinkButton)e.Item.FindControl("lnkbtnEdit");
            LinkButton lnkbtnDelete = (LinkButton)e.Item.FindControl("lnkbtnDelete");
            HyperLink hypAblImg = (HyperLink)e.Item.FindControl("hypAblImg");
            HyperLink hypEdit = (HyperLink)e.Item.FindControl("hypEdit");

            hypAblImg.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admSlide0101.aspx?id=" + strMastheadId;
            //hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.NavigateUrl = "admSlide0101.aspx?id=" + strMastheadId;

            //lnkbtnEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            //lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteMasthead.Text") + "');";

            string strFullPathImg = "";
            clsConfig config = new clsConfig();
            if (string.IsNullOrEmpty(strMastheadImg))
            {
                strFullPathImg = ConfigurationManager.AppSettings["imageBase"] + "cmn/" + config.defaultGroupImg;
            }

            else
            {
                strFullPathImg = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + strMastheadImg;
            }
            Image resultImage = new Image();
            try
            {
                if (!string.IsNullOrEmpty(strMastheadImg))
                {
                    System.Drawing.Image objImage = System.Drawing.Image.FromFile(strFullPathImg);
                    resultImage = clsMis.getImageResize2(resultImage, clsSetting.CONSTSLIDESHOWMANAGERIMGWIDTH, clsSetting.CONSTSLIDESHOWMANAGERIMGHEIGHT, objImage.Width, objImage.Height);
                    objImage.Dispose();
                    objImage = null;
                }
            }
            catch (Exception ex){}

            

            HyperLink hypViewImage = (HyperLink)e.Item.FindControl("hypViewImage");
            if (!string.IsNullOrEmpty(strMastheadImg))
            {
                Image imgAblImg = (Image)e.Item.FindControl("imgAblImg");
                imgAblImg.ImageUrl = ConfigurationManager.AppSettings["scriptBase"] + "cmn/showImage.aspx?img=" + strFullPathImg + "&f=1";
                imgAblImg.AlternateText = strMastheadName;
                imgAblImg.ToolTip = strMastheadName;

                imgAblImg.Height = resultImage.Height;
                imgAblImg.Width = resultImage.Width;
                imgAblImg.Attributes["style"] = "margin-top:-" + (resultImage.Height.Value / 2) + "px;";
                imgAblImg.Attributes["style"] += "margin-left:-" + (resultImage.Width.Value / 2) + "px;";
                resultImage.Dispose();
                resultImage = null;

                hypViewImage.NavigateUrl = Session[clsAdmin.CONSTPROJECTUSERVIEWURL].ToString() + "/" + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + strMastheadImg;
            }
            else
            {
                Image imgAblImg = (Image)e.Item.FindControl("imgAblImg");
                imgAblImg.ImageUrl = Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + config.defaultGroupImg;
                imgAblImg.AlternateText = strMastheadName;
                imgAblImg.ToolTip = strMastheadName;
                imgAblImg.Attributes["style"] = "width:100%;transform: translate(-50%, -50%);";
                resultImage.Dispose();
                resultImage = null;

                hypViewImage.NavigateUrl = Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + config.defaultGroupImg;
            }
        }
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        clsMasthead mast = new clsMasthead();
        DataSet ds = new DataSet();
        ds = mast.getGrpList(0);
        DataView dv = new DataView(ds.Tables[0]);
        dv.Sort = "GRP_NAME ASC";

        ddlGroup.DataSource = dv;
        ddlGroup.DataTextField = "GRP_NAME";
        ddlGroup.DataValueField = "GRP_ID";
        ddlGroup.DataBind();

        ListItem ddlGroupDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
        ddlGroup.Items.Insert(0, ddlGroupDefaultItem);
    }

    protected void bindGV(DataList rptAblImg, string gvSortExpDir)
    {
        clsMasthead masthead = new clsMasthead();
        DataSet ds = new DataSet();
        ds = masthead.getMastheadImage();

        DataView dv = new DataView(ds.Tables[0]);

        if (gvSortExpDir != "")
        {
            dv.Sort = gvSortExpDir;
        }

        string strKeyword = "";
        dv.RowFilter = "1 = 1";
        Boolean boolFilter = false;

        if (boolSearch)
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                boolFilter = true;
                strKeyword = txtKeyword.Text.Trim();

                dv.RowFilter += " AND (MASTHEAD_NAME LIKE '%" + strKeyword + "%')";

                litItemFound.Text = "Search for <span class=\"SectKeyword\">" + strKeyword + "</span> | ";
            }
            else { litItemFound.Text = ""; }

            if (!string.IsNullOrEmpty(ddlGroup.SelectedValue))
            {
                boolFilter = true;
                dv.RowFilter += " AND GRP_ID = " + ddlGroup.SelectedValue;
            }
        }

        if (boolFilter)
        {
            //pnlSearchForm.CssClass = "divSearchFormShow";
            //hypHideShow.CssClass = "linkHide";
        }
        else
        {
            //pnlSearchForm.CssClass = "divSearchForm";
            //hypHideShow.CssClass = "linkShow";
        }

        rptAblImg.DataSource = dv;
        rptAblImg.DataBind();


        if (boolSearch && !string.IsNullOrEmpty(strKeyword)) { litItemFound.Text = litItemFound.Text + dv.Count + " record(s) listed."; }
        else { litItemFound.Text = dv.Count + " record(s) listed."; }
    }

    protected void bindRptData()
    {
        clsMasthead masthead = new clsMasthead();
        DataSet ds = new DataSet();
        ds = masthead.getMastheadImage();

        DataView dv = new DataView(ds.Tables[0]);



        string strKeyword = "";
        dv.RowFilter = "1 = 1";
        Boolean boolFilter = false;

        if (boolSearch)
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                boolFilter = true;
                strKeyword = txtKeyword.Text.Trim();

                dv.RowFilter += " AND (MASTHEAD_NAME LIKE '%" + strKeyword + "%')";

                litItemFound.Text = "Search for <span class=\"SectKeyword\">" + strKeyword + "</span> | ";
            }
            else { litItemFound.Text = ""; }

            if (!string.IsNullOrEmpty(ddlGroup.SelectedValue))
            {
                boolFilter = true;
                dv.RowFilter += " AND GRP_ID = " + ddlGroup.SelectedValue;
            }
        }

        if (boolFilter)
        {
            //pnlSearchForm.CssClass = "divSearchFormShow";
            //hypHideShow.CssClass = "linkHide";
        }
        else
        {
            //pnlSearchForm.CssClass = "divSearchForm";
            //hypHideShow.CssClass = "linkShow";
        }

        rptAblImg.DataSource = dv;
        rptAblImg.DataBind();


        if (boolSearch && !string.IsNullOrEmpty(strKeyword)) { litItemFound.Text = litItemFound.Text + dv.Count + " record(s) listed."; }
        else { litItemFound.Text = dv.Count + " record(s) listed."; }
    }
    #endregion
}
