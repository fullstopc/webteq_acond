﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.IO;
using System.Configuration;

public partial class adm_admSlideGroup01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 8;

    protected string _gvSortExpression = "GRP_NAME";
    protected string _gvSortDirection = clsAdmin.CONSTSORTASC;
    protected string _gvSort = "";

    protected Boolean _boolSearch = false;
    #endregion


    #region "Property Methods"
    protected string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    protected string gvSortExpression
    {
        get { return ViewState["GVSORTEXPRESSION"] as string ?? _gvSortExpression; }
        set { ViewState["GVSORTEXPRESSION"] = value; }
    }

    protected string gvSortDirection
    {
        get { return ViewState["GVSORTDIRECTION"] as string ?? _gvSortDirection; }
        set { ViewState["GVSORTDIRECTION"] = value; }
    }

    protected string gvSort
    {
        get { return ViewState["GVSORT"] as string ?? _gvSort; }
        set { ViewState["GVSORT"] = value; }
    }

    protected Boolean boolSearch
    {
        get
        {
            if (ViewState["BOOLSEARCH"] == null)
            {
                return _boolSearch;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLSEARCH"]);
            }
        }
        set { ViewState["BOOLSEARCH"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMSLIDESHOWGROUPNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            txtKeyword.Attributes.Add("onkeydown", "javascript:if(event.keyCode == 13){document.getElementById('" + lnkbtnGo.ClientID + "').click(); return false;}");

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMELISTINGSIZE, clsConfig.CONSTGROUPOTHERSETTINGS, 1);

            //gvItems.PagerSettings.FirstPageText = GetGlobalResourceObject("GlobalResource", "contentAdmFirstPageText.Text").ToString();
            //gvItems.PagerSettings.LastPageText = GetGlobalResourceObject("GlobalResource", "contentAdmLastPageText.Text").ToString();
            //gvItems.PageSize = (!string.IsNullOrEmpty(config.value)) ? Convert.ToInt16(config.value) : clsAdmin.CONSTADMPAGESIZE;
            //gvSort = gvSortExpression + " " + gvSortDirection;
            //bindGV(gvItems, "GRP_OTHER DESC, GRP_NAME ASC");

            setPageProperties();

            if (Session["DELETEDGRPNAME"] != null)
            {
                clsMasthead grp = new clsMasthead();
                grp.extractGrpById(grp.otherGrpId, 0);

                pnlAck.Visible = true;
                litAck.Text = "Group (" + Session["DELETEDGRPNAME"] + ") has been deleted successfully.";

                if (Convert.ToInt16(Session["MOVEDPRODNO"]) > 1)
                {
                    litAck.Text = litAck.Text + "<br />" + Session["MOVEDPRODNO"] + " items have been moved to \"" + grp.grpName + "\".";
                }
                else
                {
                    litAck.Text = litAck.Text + "<br />" + Session["MOVEDPRODNO"] + " item has been moved to \"" + grp.grpName + "\".";
                }

                clsMis.resetListing();

                Session["DELETEDGRPNAME"] = null;
                Session["MOVEDPRODNO"] = 0;
            }

        }
    }

    protected void gvItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell tc in e.Row.Cells)
            {
                if (tc.HasControls())
                {
                    LinkButton lnkbtn = (LinkButton)tc.Controls[0];
                    //Image icon = new Image();
                    //icon.ImageUrl = "../img/adm/btn-cancel.gif";

                    if (lnkbtn != null && gvSortExpression == lnkbtn.CommandArgument)
                    {
                        //tc.Controls.Add(icon);
                        string strClass = (gvSortDirection == "DESC") ? "desc" : "asc";
                        tc.CssClass = strClass;
                    }


                }
            }
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //LinkButton lnkbtnEdit = (LinkButton)e.Row.FindControl("lnkbtnEdit");
            LinkButton lnkbtnDelete = (LinkButton)e.Row.FindControl("lnkbtnDelete");

            int grpid = Convert.ToInt16(gvItems.DataKeys[e.Row.RowIndex].Value.ToString());
            HyperLink hypEdit = (HyperLink)e.Row.FindControl("hypEdit");

            hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.NavigateUrl = "admSlideGroup0101.aspx?id=" + grpid;

            clsMasthead grp = new clsMasthead();

            if (grpid == grp.otherGrpId)
            {
                lnkbtnDelete.Visible = false;
            }

            //lnkbtnEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteGroup.Text").ToString().Replace(clsAdmin.CONSTGROUPTAG, "Group") + "');";
        }
    }

    protected void gvItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "cmdEdit")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int intId = Convert.ToInt16(gvItems.DataKeys[rowIndex].Value.ToString());
            Response.Redirect("admSlideGroup0101.aspx?id=" + intId);
        }
        else if (e.CommandName == "cmdDelete")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int intId = Convert.ToInt16(gvItems.DataKeys[rowIndex].Value.ToString());
            int intMovedProdNo = 0;
            string strDeletedGrpName = "";
            
            clsMasthead grp = new clsMasthead();
            clsMis.performDeleteGroup2(intId, grp.otherGrpId, ref intMovedProdNo, ref strDeletedGrpName);

            Session["MOVEDPRODNO"] = intMovedProdNo;
            Session["DELETEDGRPNAME"] = strDeletedGrpName;

            Response.Redirect(currentPageName);
        }
    }

    protected void gvItems_Sorting(object sender, GridViewSortEventArgs e)
    {
        string strSortExpDir = "";
        if (e.SortExpression != gvSortExpression)
        {
            gvSortExpression = e.SortExpression;
            gvSortDirection = clsAdmin.CONSTSORTASC;
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }
        else
        {
            switch (gvSortDirection)
            {
                case clsAdmin.CONSTSORTASC:
                    gvSortDirection = clsAdmin.CONSTSORTDESC;
                    break;
                case clsAdmin.CONSTSORTDESC:
                    gvSortDirection = clsAdmin.CONSTSORTASC;
                    break;
            }

            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }

        gvSort = strSortExpDir;
        bindGV(gvItems, gvSort);
    }

    protected void gvItems_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvItems.PageIndex = e.NewPageIndex;
        bindGV(gvItems, gvSort);
    }

    protected void lnkbtnGo_Click(object sender, EventArgs e)
    {
        boolSearch = true;

        bindGV(gvItems, "GRP_NAME ASC");
    }

    protected void lnkbtnExport_Click(object sender, EventArgs e)
    {
        ExportExcel();
        //ExportGV(gvItems, "SLIDESHOWGROUPLIST", (DataView)Session["DVSSGROUP"], true);
    }

    public override void VerifyRenderingInServerForm(Control control)
    {

    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        hypAdd.Text = "Add Group";
        hypAdd.ToolTip = "Add Group";

        ListItem ddlActiveDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlActive.Items.Insert(0, ddlActiveDefaultItem);
        ddlActive.Items.Add(new ListItem("Yes", "1"));
        ddlActive.Items.Add(new ListItem("No", "0"));
    }

    protected void bindGV(GridView gvRef, string gvSortExpDir)
    {
        clsMasthead grp = new clsMasthead();
        DataSet ds = new DataSet();
        ds = grp.getGrpList(0);

        DataView dv = new DataView(ds.Tables[0]);
        dv.RowFilter = "1 = 1";
        Boolean boolFilter = false;

        string strKeyword = "";

        if (boolSearch)
        {
            strKeyword = txtKeyword.Text.Trim();

            if (!string.IsNullOrEmpty(strKeyword))
            {
                boolFilter = true;
                dv.RowFilter += " AND (GRP_NAME LIKE '%" + strKeyword + "%' OR GRP_DNAME LIKE '%" + strKeyword + "%')";

                litFound.Text = "Search for <span class=\"SectKeyword\">" + strKeyword + "</span> | ";

                //pnlSearchForm.CssClass = "divSearchFormShow";
                //hypHideShow.CssClass = "linkHide";
            }
            else
            {
                litFound.Text = "";

                //pnlSearchForm.CssClass = "divSearchForm";
                //hypHideShow.CssClass = "linkShow";
            }

            if (!string.IsNullOrEmpty(ddlActive.SelectedValue))
            {
                boolFilter = true;
                dv.RowFilter += " AND GRP_ACTIVE = " + ddlActive.SelectedValue;
            }
        }

        if (boolFilter)
        {
            //pnlSearchForm.CssClass = "divSearchFormShow";
            //hypHideShow.CssClass = "linkHide";
        }
        else
        {
            //pnlSearchForm.CssClass = "divSearchForm";
           //hypHideShow.CssClass = "linkShow";
        }

        if (gvSortExpDir != "")
        {
            dv.Sort = gvSortExpDir;
        }
        else
        {
            dv.Sort = "GRP_OTHER DESC, GRP_NAME ASC";
        }

        Session["DVSSGROUP"] = dv;

        gvRef.DataSource = dv;
        gvRef.DataBind();

        if (boolSearch && !string.IsNullOrEmpty(strKeyword)) { litFound.Text = litFound.Text + dv.Count + " record(s) listed."; }
        else { litFound.Text = dv.Count + " record(s) listed."; }

        if (dv.Count == 0)
        {
            lnkbtnExport.Enabled = false;
            lnkbtnExport.CssClass += " disabled";
        }
        else
        {
            lnkbtnExport.Enabled = true;
            //lnkbtnExport.CssClass = "btn2 btnExport";
        }

        // exclude default group - Other
        if (clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITSLIDESHOW, (dv.Count - 1))) { hypAdd.Visible = false; }
    }

    protected void ExportGV(GridView gvRef, string strFileTitle, DataView dvItems, Boolean boolHideLastColumn)
    {
        DateTime dt = DateTime.Now;
        string strFileName = strFileTitle + "_" + dt.Day + dt.Month + dt.Year + ".xls";
        string strAttachment = "attachment; filename=" + strFileName;

        Response.ClearContent();
        Response.AddHeader("content-disposition", strAttachment);
        Response.ContentType = "application/vnd.xls";
        Response.ContentEncoding = System.Text.Encoding.Unicode;
        Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        gvRef.AllowPaging = false;
        gvRef.DataSource = dvItems;
        gvRef.DataBind();

        for (int i = 0; i < gvRef.Columns.Count; i++)
        {
            if (gvRef.HeaderRow.Cells[i].CssClass == "hiddenCol")
            {
                gvRef.HeaderRow.Cells[i].CssClass = "showCol";
            }

            gvRef.HeaderRow.Cells[i].Enabled = false;
        }

        for (int i = 0; i < gvRef.Rows.Count; i++)
        {
            GridViewRow row = gvRef.Rows[i];

            for (int j = 0; j < gvRef.Columns.Count; j++)
            {
                if (row.Cells[j].CssClass == "hiddenCol")
                {
                    row.Cells[j].CssClass = "showCol";
                }
            }
        }

        if (boolHideLastColumn)
        {
            int intColHide = gvRef.Columns.Count - 1;

            gvRef.HeaderRow.Cells[intColHide].Visible = false;
            gvRef.Columns[intColHide].Visible = false;
        }

        gvRef.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }
    private void ExportExcel()
    {
        DateTime datetime = DateTime.Now;
        string filename = "SLIDESHOWGROUPLIST_" + datetime.Day + datetime.Month + datetime.Year + ".xls";
        string attachment = "attachment; filename=" + filename;

        DataTable dt = new clsMasthead().getGrpList(0).Tables[0];

        GridView GridView1 = new GridView();
        GridView1.AllowPaging = false;
        GridView1.DataSource = dt;
        GridView1.DataBind();

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", attachment);
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel ";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);

        GridView1.RenderControl(hw);
        Response.Output.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }
    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        int intItemID = Convert.ToInt16(hdnItemID.Value);
        int intMovedProdNo = 0;
        string strDeletedGrpName = "";

        clsMasthead grp = new clsMasthead();
        clsMis.performDeleteGroup2(intItemID, grp.otherGrpId, ref intMovedProdNo, ref strDeletedGrpName);

        Session["MOVEDPRODNO"] = intMovedProdNo;
        Session["DELETEDGRPNAME"] = strDeletedGrpName;

        Response.Redirect(currentPageName);
    }
    #endregion
}
