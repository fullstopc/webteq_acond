﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class adm_adm00 : System.Web.UI.Page
{
    #region "Properties"
    protected string pageRedirect = clsAdmin.CONSTADMLANDINGPAGE;
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }

        if (Session["ADMACCESSDENIED"] != null && Session["ADMACCESSDENIED"] != "")
        {
            Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleDenied.Text").ToString(), true);

            pnlAccessDenied.Visible = true;
        }
        else
        {
            Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

            if (!string.IsNullOrEmpty(pageRedirect)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + pageRedirect); }
        }

        Session["ADMACCESSDENIED"] = null;

    }
    #endregion
}
