﻿<%@ Page Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admAgentLeaveCalendar.aspx.cs" Inherits="adm_adm"  ValidateRequest="false" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/mst/blank.Master" %>
<asp:Content ID="Content" ContentPlaceHolderID="cphHead" runat="server">
<link href="<% =System.Configuration.ConfigurationManager.AppSettings["libBase"] %>fullcalendar/dist/fullcalendar.min.css" rel="Stylesheet" />
<script src="<% =System.Configuration.ConfigurationManager.AppSettings["libBase"] %>fullcalendar/dist/fullcalendar.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    
    <div class="container-fluid" style="padding:20px;">
        <div id="onLeaveCalendar"></div>
    </div>
    <script type="text/javascript">
        $(function () {
            $("#onLeaveCalendar").fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month'
                },
                allDaySlot: false,
                editable: false,
                displayEventTime: false,
                events: function (start, end, timezone, callback) {
                    $.ajax({
                        type: 'POST',
                        url: wsBase + 'wsFullcalendar.asmx/getEmployeeLeaveList',
                        data: "{}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        navLinks: true,
                        success: function (data) {
                            var dataNew = $.parseJSON(data.d);
                            var agents = dataNew.map(function (x) { return x.employeeID; })
                                                .filter(function (item, pos, self) {
                                                    return self.indexOf(item) == pos;
                                                })
                                                .map(function (x) {
                                                    return {
                                                        color: '#' + (0x1000000 + (Math.random()) * 0xffffff).toString(16).substr(1, 6),
                                                        employeeID: x
                                                    };
                                                });

                            var json = $.parseJSON(data.d).map(function (leave) {
                                var color = agents.filter(function (x) {
                                    return x.employeeID == leave.employeeID;
                                }).map(function (x) { return x.color; })[0];
                                
                                return {
                                    title: leave.name +" - " + leave.reason,
                                    start: leave.start,
                                    end: leave.end,
                                    backgroundColor: color,
                                    borderColor: color
                                }
                            });
                            callback(json);
                        }
                    });
                },
            });
        })
    </script>
</asp:Content>