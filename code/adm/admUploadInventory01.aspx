﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admUploadInventory01.aspx.cs" Inherits="adm_admUploadInventory01" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title"><asp:Literal ID="litPageTitle" runat="server" meta:resourcekey="litPageTitle"></asp:Literal></div>
        <div class="divListingSplitter"></div>
        <div class="action">
            <asp:Panel ID="pnlListingAction" runat="server" CssClass="divListingAction">
                <asp:HyperLink ID="hypAdd" runat="server" Text="Upload New" ToolTip="Upload New" NavigateUrl="admUploadInventory0101.aspx" CssClass="hypListingAction add"></asp:HyperLink>
            </asp:Panel>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <div class="divListing">
        <div class="divListingHdr">
            <div class="divListingHdrLeft"><asp:Literal ID="litItemFound" runat="server"></asp:Literal></div>
            
        </div>
        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
            <div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
        </asp:Panel>
        <div class="divListingData">
            <asp:GridView ID="gvInventory" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom" CssClass="dataTbl" OnRowDataBound="gvInventory_RowDataBound" OnSorting="gvInventory_Sorting" OnPageIndexChanging="gvInventory_PageIndexChanging" DataKeyNames="INVUPLOAD_ID" EmptyDataText="No record found.">
                <PagerStyle CssClass="pg" HorizontalAlign="Right" />
                <AlternatingRowStyle CssClass="td_alt" />
                <Columns>
                    <asp:TemplateField HeaderText="no.">
                        <ItemStyle CssClass="tdNo" />
                        <ItemTemplate>
                            <asp:Literal ID="litNo" runat="server" Text="<%# Container.DataItemIndex + 1 %>"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Date" DataField="INVUPLOAD_DATE" SortExpression="INVUPLOAD_DATE" DataFormatString="{0:dd MMM yyyy hh:mm tt}"/>
                    <asp:BoundField HeaderText="File Name" DataField="INVUPLOAD_FILENAME" SortExpression="INVUPLOAD_FILENAME" />
                    <asp:BoundField HeaderText="Failed Records" DataField="INVUPLOAD_FAILEDRECORDS" SortExpression="INVUPLOAD_FAILEDRECORDS" />
                    <asp:BoundField HeaderText="Update Method" DataField="V_UPDATEMETHOD" SortExpression="V_UPDATEMETHOD" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
