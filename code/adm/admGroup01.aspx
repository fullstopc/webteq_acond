﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="admGroup01.aspx.cs" Inherits="adm_admGroup01" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title"><asp:Literal ID="litPageTitle" runat="server"></asp:Literal></div>
        <div class="divListingSplitter"></div>
        <div class="action">
			<asp:Panel ID="pnlListingAction" runat="server" CssClass="divListingAction">
                <asp:HyperLink ID="hypAddGroup" runat="server" NavigateUrl="admGroup0101.aspx" CssClass="hypListingAction add"></asp:HyperLink>
                <div class="divListingSplitter"></div>
                <asp:LinkButton ID="lnkbtnExport" runat="server" Text="Export to Excel" ToolTip="Export to Excel" CssClass="hypListingAction export" OnClick="lnkbtnExport_Click"></asp:LinkButton>
            </asp:Panel>
        </div>
        <div class="divListingSplitter"></div>
        <div class="filter">
            <div class="divFilterContainer">
                <div class="divFilter">
                    <asp:TextBox ID="txtKeyword" runat="server" placeholder="Keyword Search" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
                    <a href="#" class="btnDropdown" onclick="toggleFilterOption();"></a>
                    <asp:Literal ID="litKeywordRule" runat="server" meta:resourcekey="litKeywordRule" visible="false"></asp:Literal>
                </div>
                <div class="divFilterMore">
                    <div></div>
                    <div>
                        <table class="tblSearch">
                            <tr>
                                <td class="">
                                    <div class="divFilterLabel">Show on Top:</div>
                                    <asp:DropDownList ID="ddlShowTop" runat="server" CssClass="ddl_medium"></asp:DropDownList>
                                </td>
                                <td class="">
									<div class="divFilterLabel">Active:</div>
                                    <asp:DropDownList ID="ddlActive" runat="server" CssClass="ddl_medium"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
            <div class="divFilterAction"><asp:Button ID="lnkbtnSearch" runat="server" Text="" ToolTip="Search" CssClass="btn btnSearch" OnClick="lnkbtnSearch_Click"/></div>
        </div>
    </div>
    <div class="divListing">
        <div class="divListingHdr">
            <div class="divListingHdrLeft"><h2><asp:Literal ID="litGroupFound" runat="server"></asp:Literal></h2></div>
            <div class="divGroupingInner"> 
                <label style="padding-right:20px;">Category Group :</label><asp:DropDownList ID="ddlGrouping" runat="server" CssClass="ddl" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlGrouping_SelectedIndexChanged"></asp:DropDownList>
            </div>
        </div>
        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
            <div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
        </asp:Panel>
        <div class="divListingData">
            <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PagerSettings-Mode="NumericFirstLast" CssClass="dataTbl" OnRowDataBound="gvGroup_RowDataBound" OnSorting="gvGroup_Sorting" OnPageIndexChanging="gvGroup_PageIndexChanging" OnRowCommand="gvGroup_RowCommand" DataKeyNames="GRP_ID" EmptyDataText="No record found.">
                <PagerStyle CssClass="pg" HorizontalAlign="Right" />
                <AlternatingRowStyle CssClass="td_alt" />
                <Columns>
                    <asp:TemplateField HeaderText="no.">
                        <ItemStyle CssClass="tdNo" />
                        <ItemTemplate>
                            <asp:Literal ID="litNo" runat="server" Text="<%# Container.DataItemIndex + 1 %>"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="code" DataField="GRP_CODE" SortExpression="GRP_CODE" ItemStyle-CssClass="td" />
                    <asp:BoundField HeaderText="name" DataField="GRP_NAME" SortExpression="GRP_NAME" />
                    <asp:BoundField HeaderText="name (japanese)" DataField="GRP_NAME_JP" SortExpression="GRP_NAME_JP" Visible="false" />
                    <asp:BoundField HeaderText="name (malay)" DataField="GRP_NAME_MS" SortExpression="GRP_NAME_MS" Visible="false" />
                    <asp:BoundField HeaderText="name (chinese)" DataField="GRP_NAME_ZH" SortExpression="GRP_NAME_ZH" Visible="false" />
                    <asp:BoundField HeaderText="display name" DataField="GRP_DNAME" SortExpression="GRP_DNAME" />
                    <asp:BoundField HeaderText="display name (japanese)" DataField="GRP_DNAME_JP" SortExpression="GRP_DNAME_JP" Visible="false" />
                    <asp:BoundField HeaderText="display name (malay)" DataField="GRP_DNAME_MS" SortExpression="GRP_DNAME_MS" Visible="false" />
                    <asp:BoundField HeaderText="display name (chinese)" DataField="GRP_DNAME_ZH" SortExpression="GRP_DNAME_ZH" Visible="false" />
                    <asp:TemplateField HeaderText="top" SortExpression="GRP_SHOWTOP">
                        <ItemStyle CssClass="td_small" />
                        <ItemTemplate>
                            <asp:Literal ID="litShowTop" runat="server" Text='<%# clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "GRP_SHOWTOP"))) %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="active" SortExpression="GRP_ACTIVE">
                        <ItemStyle CssClass="td_small" />
                        <ItemTemplate>
                            <asp:Literal ID="litActive" runat="server" Text='<%# clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "GRP_ACTIVE"))) %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="action" HeaderStyle-CssClass="alignCenter tdAct2">
                        <ItemStyle CssClass="tdAct" />
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnUp" runat="server" CssClass="btnSP lnkbtn lnkbtnUp" CommandName="cmdUp" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "GRP_ID") + "|" + DataBinder.Eval(Container.DataItem, "GRP_ORDER") %>'></asp:LinkButton>   
                                 <span class="spanSplitter">|</span>
                            <asp:LinkButton ID="lnkbtnDown" runat="server" CssClass="btnSP lnkbtn lnkbtnDown" CommandName="cmdDown" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "GRP_ID") + "|" + DataBinder.Eval(Container.DataItem, "GRP_ORDER") %>'></asp:LinkButton>
                                 <span class="spanSplitter" runat="server" id="spanSplitter2">|</span>
                                 <asp:HyperLink ID="hypEdit" CssClass="lnkbtn lnkbtnEdit" runat="server"></asp:HyperLink>
                            <%--<asp:LinkButton ID="lnkbtnEdit" CssClass="lnkbtn lnkbtnEdit" runat="server" CausesValidation="false" CommandName="cmdEdit"></asp:LinkButton>--%>
                                <span class="spanSplitter" runat="server" id="spanSplitter">|</span>
                            <asp:LinkButton ID="lnkbtnDelete" CssClass="lnkbtn lnkbtnDelete" runat="server" CausesValidation="false" CommandName="cmdDelete"></asp:LinkButton>
                                <asp:Literal ID="litSplitter" runat="server" Visible="false"></asp:Literal>
                                <asp:HyperLink ID="hypbtnSubCategory" CssClass="lnkbtn lnkbtnNoIcon" runat="server"></asp:HyperLink>
                            <%--<asp:LinkButton ID="lnkbtnSubCategory" CssClass="lnkbtn lnkbtnNoIcon" runat="server" CausesValidation="false" CommandName="cmdSubCategory" Visible=""></asp:LinkButton>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>

