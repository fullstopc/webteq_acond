﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admCancelTrans.aspx.cs" Inherits="adm_admCancelTrans" %>
<%@ MasterType VirtualPath="~/mst/blank.master" %>
<%@ Register Src="~/ctrl/ucAdmCheckAccess.ascx" TagName="AdmCheckAccess" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    
    <asp:Panel ID="pnlCancelTrans" runat="server" CssClass="divCancelTrans">
        <asp:Panel ID="pnlCancelTransInner" runat="server" CssClass="divCancelTransInner">
            <table cellpadding="0" cellspacing="0" class="tblCancelTrans">
                <tr>
                    <td class="tdCancelTrans">
                        <asp:Panel ID="pnlCancelTransForm" runat="server" CssClass="divCancelTransForm">
                            <asp:Panel ID="pnlForm" runat="server">
                                <table cellpadding="0" cellspacing="0" border="0" class="tblCancelTransInner">
                                    <tr>
                                        <td colspan="2" class="tdHeader">Cancel Order:</td>
                                    </tr>
                                    <tr class="trHeader">
                                        <td colspan="2">Order cancellation only allowed for authorised personnel. Please re-login to confirm this cancellation.</td>
                                    </tr>
                                    <tr>
                                        <td>Email:</td>
                                        <td>
                                            <asp:TextBox ID="txtEmail" runat="server" class="text_fullwidth" MaxLength="250" ValidationGroup="cancel"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="<br />Please enter Email." ControlToValidate="txtEmail" class="errmsg" Display="dynamic" ValidationGroup="cancel"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="<br />Please enter valid Email." Display="Dynamic" ControlToValidate="txtEmail" CssClass="errmsg" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$" ValidationGroup="cancel"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Password:</td>
                                        <td>
                                            <asp:TextBox ID="txtPassword" runat="server" class="text_fullwidth" TextMode="Password" MaxLength="20" ValidationGroup="cancel"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="<br />Please enter Password." ControlToValidate="txtPassword" class="errmsg" Display="dynamic" ValidationGroup="cancel"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr class="trBtn">
                                        <td></td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnConfirm" runat="server" Text="Confirm Cancel" ToolTip="Confirm Cancel" class="btn btnSave" OnClick="lnkbtnConfirm_Click" ValidationGroup="cancel" />
                                            <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Back" ToolTip="Back" CssClass="btn btnBack"></asp:LinkButton>
                                        </td>
                                    </tr>          
                                </table> 
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
                <asp:Literal ID="litAck" runat="server"></asp:Literal>
                <asp:Panel ID="pnlAckBtn" runat="server" class="divAckBtn">
                    <asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                    <asp:LinkButton ID="lnkbtnContinue" runat="server" Text="Close" ToolTip="Close" class="btn btnBack" CausesValidation="false" OnClientClick="self.parent.tb_remove();parent.location.reload(); return false;"></asp:LinkButton>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>

