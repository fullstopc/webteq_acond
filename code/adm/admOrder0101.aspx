﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admOrder0101.aspx.cs" Inherits="adm_admOrder0101" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmOrder.ascx" TagName="AdmOrder" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
    </asp:Panel>
    <asp:Panel ID="pnlForm" runat="server" CssClass="divForm">
        <uc:AdmOrder ID="ucAdmOrder" runat="server" pageListingURL="admOrder01.aspx" />
    </asp:Panel>
    <asp:Panel ID="pnlCancelSales" runat="server" Visible="false">
        <asp:Panel ID="pnlCancelForm" runat="server">  
            <table cellpadding="0" cellspacing="0" id="tblLogin">
                <tr>
                    <th colspan="2">CANCEL SALES:</th>
                </tr>
                <tr>
                    <td class="tdLabel">User ID:</td>
                    <td><asp:TextBox ID="txtUserId" runat="server" class="text" MaxLength="20" ValidationGroup="cancel"></asp:TextBox><asp:RequiredFieldValidator ID="rfvUserId" runat="server" ErrorMessage="<br />Please enter User ID." ControlToValidate="txtUserId" class="errmsg" Display="dynamic" ValidationGroup="cancel"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="tdLabel">Password:</td>
                    <td><asp:TextBox ID="txtPassword" runat="server" class="text" TextMode="Password" MaxLength="20" ValidationGroup="cancel"></asp:TextBox><asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="<br />Please enter Password." ControlToValidate="txtPassword" class="errmsg" Display="dynamic" ValidationGroup="cancel"></asp:RequiredFieldValidator></td>
                </tr>  
                <tr>
                    <td></td>
                    <td>
                        <asp:LinkButton ID="lnkbtnConfirm" runat="server" Text="Confirm Cancel" ToolTip="Confirm Cancel" class="btn" OnClick="lnkbtnConfirm_Click" ValidationGroup="cancel" />
                        <asp:LinkButton ID="lnkbtnCancelDelete" runat="server" Text="Cancel" ToolTip="Cancel" CssClass="btn"></asp:LinkButton>
                    </td>
                </tr>          
            </table>
        </asp:Panel>  
    </asp:Panel>
</asp:Content>