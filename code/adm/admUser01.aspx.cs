﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;

public partial class adm_admUser01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 2;
    protected bool isSingleLogin;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);
        Page.ClientScript.RegisterClientScriptInclude("colresizeable", System.Configuration.ConfigurationManager.AppSettings["jsBase"] + "colResizable/colResizable-1.6.min.js");

        if (!IsPostBack)
        {
            if (Session["ADMROOT"] == null || Session["ADMROOT"] == "")
            {
                if (Session["CTRLID"] == null || Session["CTRLID"] == "")
                {
                    Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "adm/" + clsAdmin.CONSTADMLOGIN);
                }
                else // if ctrlID session still remain, login as controller again.
                {
                    resetSession();
                    clsController ctrl = new clsController();
                    if(ctrl.extractControllerById(Convert.ToInt32(Session["CTRLID"]),1))
                    {
                        Session["ADMID"] = ctrl.ctrlId;
                        Session["ADMEMAIL"] = ctrl.ctrlEmail;
                        Session["ADMROOT"] = 1;

                        Session["CTRLEMAIL"] = ctrl.ctrlEmail;
                        Session["CTRLTYPE"] = ctrl.ctrlRole;
                        Session["CTRLID"] = ctrl.ctrlId;
                        ctrl.setLastLogin();

                        Response.Redirect(currentPageName);
                    }
                    else
                    {
                        Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "adm/" + clsAdmin.CONSTADMLOGIN);
                    }
                }
            }

            Master.sectId = sectId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            setPageProperties();

            if (Session["DELETEDUSEREMAIL"] != null && Session["DELETEDUSEREMAIL"] != "")
            {
                pnlAck.Visible = true;

                litAck.Text = "<div class=\"noticemsg\">User (" + Session["DELETEDUSEREMAIL"].ToString() + ") has been deleted successfully.</div>";

                Session["DELETEDUSEREMAIL"] = null;

                clsMis.resetListing();
            }            
        }
    }
protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        int intItemID = Convert.ToInt16(hdnItemID.Value);
        string strDeletedUserEmail = "";

        clsUser user = new clsUser();
        int intRecordAffected = 0;

        user.extractUserById(intItemID, 0);
        strDeletedUserEmail = user.userEmail;

        intRecordAffected = user.deleteUserById(intItemID);

        if (intRecordAffected == 1)
        {
            Session["DELETEDUSEREMAIL"] = strDeletedUserEmail;

            Response.Redirect(currentPageName);
        }
        else
        {
            litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to delete User (" + strDeletedUserEmail + "). Please try again.</div>";

            pnlAck.Visible = true;
        }
    }
    protected void lnkbtnAdminAccess_Click(object sender, EventArgs e)
    {
        int intItemID = Convert.ToInt16(hdnItemID.Value);
        Response.Redirect("admlogin.aspx?id=" + intItemID + "&type=" + 1);
    }
    protected void lnkbtnUserAccess_Click(object sender, EventArgs e)
    {
        int intItemID = Convert.ToInt16(hdnItemID.Value);
        Response.Redirect("admlogin.aspx?id=" + intItemID + "&type=" + 1);
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        clsMis mis = new clsMis();
        DataSet ds = new DataSet();
        ds = mis.getListByListGrp("ADMIN TYPE", 1);
        DataView dv = new DataView(ds.Tables[0]);
        dv.Sort = "LIST_ORDER ASC";

        ddlType.DataSource = dv;
        ddlType.DataTextField = "LIST_NAME";
        ddlType.DataValueField = "LIST_NAME";
        ddlType.DataBind();

        ListItem ddlTypeDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlType.Items.Insert(0, ddlTypeDefaultItem);

        ListItem ddlActiveDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlActive.Items.Insert(0, ddlActiveDefaultItem);
        ddlActive.Items.Add(new ListItem("Yes", "1"));
        ddlActive.Items.Add(new ListItem("No", "0"));


        isSingleLogin = false;
        if (Session["CTRLTYPE"] != null && Session["CTRLTYPE"] != "")
        {
            if (Convert.ToInt16(Session["CTRLTYPE"]) != 1)
            {
                isSingleLogin = true;
            }
        }
    }

    private void resetSession()
    {
        Session["ADMID"] = null;
        Session["ADMEMAIL"] = null;
        Session["ADMTYPE"] = null;
        Session["ADMROOT"] = null;

        clsProject.resetSession();
    }
    #endregion
}
