﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class adm_admPrintOrder : System.Web.UI.Page
{
    #region "Properties"
    protected int _ordId;
    #endregion

    #region "Property Methods"
    public int ordId
    {
        get
        {
            if (ViewState["ORDID"] == null)
            {
                return _ordId;
            }
            else
            {
                return int.Parse(ViewState["ORDID"].ToString());
            }
        }
        set { ViewState["ORDID"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        Literal litCSS = new Literal();
        litCSS.Text = "<link href='" + ConfigurationManager.AppSettings["cssBase"] + "print.css' rel='Stylesheet' media='print' type='text/css' />";
        this.Page.Header.Controls.Add(litCSS);

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;
                if (int.TryParse(Request["id"], out intTryParse))
                {
                    ordId = intTryParse;
                }
                else
                {
                    ordId = 0;
                }
            }

            setPageProperties();
        }
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        fillForm();
    }

    protected void fillForm()
    {
        clsOrder ord = new clsOrder();
        clsMis mis = new clsMis();
        DataSet ds = new DataSet();
        clsConfig config = new clsConfig();
        
        //Customization for YsHamper Project
        if (config.isYsHamperFlagExist(clsConfig.CONSTNAMEYSHAMPERFLAG, clsConfig.CONSTGROUPWEBSITE))
        {
            if (ord.extractOrderYsHamper(ordId))
            {
                //clsConfig config = new clsConfig();
                string strCurrency = config.currency;

                litOrdNo.Text = ord.ordNo;

                //sales is cancelled
                if (ord.ordCancel == 1)
                {
                    litOrdNo.Text += " (Cancel)";
                }
                else
                {
                    //status is set
                    if (!string.IsNullOrEmpty(ord.ordStatusName)) { litOrdNo.Text += " (" + ord.ordStatusName + ")"; }
                }

                litMember.Text = ord.memEmail;

                if (!string.IsNullOrEmpty(ord.ordTypeName))
                {
                    litOrdType.Text = ord.ordTypeName;
                    trType.Visible = true;

                    if (ord.ordType == 1)
                    {
                        //pnlListingOnline.Visible = true;
                    }
                    else
                    {
                        litMember.Text = "N/A";
                        //pnlListingOnline.Visible = false;
                    }
                }

                litOrdDate.Text = ord.ordCreation.ToString("dd MMM yyyy");
                litOrdAmount.Text = strCurrency + " " + ord.ordTotal.ToString();
                litRemarks.Text = !string.IsNullOrEmpty(ord.ordRemarks) ? ord.ordRemarks.Replace("\n", "<br />") : "-";
                litMsgBuyer.Text = !string.IsNullOrEmpty(ord.ordMsgBuyer) ? ord.ordMsgBuyer.Replace("\n", "<br />") : "-";

                litName.Text = ord.shippingName;
                litAddress.Text = ord.shippingAdd;
                litPoscode.Text = ord.shippingPoscode;
                litContactNo.Text = ord.shippingContactNo;

                litBillName.Text = ord.name;
                litBillAdd.Text = ord.add;
                litBillPoscode.Text = ord.poscode;
                litBillContactNo.Text = ord.contactNo;

                if (!string.IsNullOrEmpty(ord.cityOther))
                {
                    litBillCity.Text = ord.city + " " + ord.cityOther;
                }
                else
                {
                    litBillCity.Text = ord.city;
                }

                if (!string.IsNullOrEmpty(ord.shippingCityOther))
                {
                    litCity.Text = ord.shippingCity + " " + ord.shippingCityOther;
                }
                else
                {
                    litCity.Text = ord.shippingCity;
                }

                if (!string.IsNullOrEmpty(ord.stateOther))
                {
                    litBillState.Text = ord.state + " " + ord.stateOther;
                }
                else
                {
                    litBillState.Text = ord.state;
                }

                if (!string.IsNullOrEmpty(ord.shippingStateOther))
                {
                    litState.Text = ord.shippingState + " " + ord.shippingStateOther;
                }
                else
                {
                    litState.Text = ord.shippingState;
                }

                if (!string.IsNullOrEmpty(ord.countryOther))
                {
                    litBillCountry.Text = ord.countryName + " " + ord.countryOther;
                }
                else
                {
                    litBillCountry.Text = ord.countryName;
                }

                if (!string.IsNullOrEmpty(ord.shippingCountryOther))
                {
                    litCountry.Text = ord.shippingCountryName + " " + ord.shippingCountryOther;
                }
                else
                {
                    litCountry.Text = ord.shippingCountryName;
                }

                trBillEmail.Visible = true;
                trEmail.Visible = true;
                litBillEmail.Text = ord.email;
                litEmail.Text = ord.shippingEmail;

                trBillCompanyName.Visible = true;
                trCompanyName.Visible = true;
                litBillCompanyName.Text = ord.company;
                litCompanyName.Text = ord.shippingCustCompany;

                litPayGateway.Text = ord.ordPayGateway;
                litPayRemark.Text = ord.ordPayRemarks;

                if (ord.ordPayment != DateTime.MaxValue) { litPayDate.Text = ord.ordPayment.ToString("dd MMM yyyy"); }

                //@ Bank Details
                if (ord.ordStatus >= clsAdmin.CONSTORDERSTATUSPAY)
                {
                    litBank.Text = !string.IsNullOrEmpty(ord.ordPayBankName) ? ord.ordPayBankName : "-";
                    litAccount.Text = !string.IsNullOrEmpty(ord.ordPayBankAccount) ? ord.ordPayBankAccount : "-";
                    litTransId.Text = !string.IsNullOrEmpty(ord.ordPayTransId) ? ord.ordPayTransId : "-";

                    // if payment gateway is Bank Transfer
                    if (ord.ordPayGatewayId <= 0)
                    {
                        litPayDate.Text = DateTime.Compare(ord.ordPayBankDate, DateTime.MaxValue) != 0 ? ord.ordPayBankDate.ToString("dd MMM yyyy") : "-";

                        trBank.Visible = true;
                        trAccount.Visible = true;
                        trTransId.Visible = true;
                    }
                }

                pnlOrderDeliveryMessage.Visible = true;
                litMsgTo.Text = ord.ordTo;
                litMsgContent.Text = ord.ordMessage;
                litMsgFrom.Text = ord.ordFrom;
                litSpecialInstruction.Text = ord.ordMsgInstruction;

                ucAdmOrderItems.transId = ordId;
                ucAdmOrderItems.cancel = ord.ordCancel;
                ucAdmOrderItems.status = ord.ordStatus;
                ucAdmOrderItems.type = ord.ordType;
                ucAdmOrderItems.decShipping = ord.ordShipping;
                ucAdmOrderItems.fill();
            }
            else
            {
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            }
        }
        else
        {
            if (ord.extractOrderByIdFromTB(ordId))
            {
                //clsConfig config = new clsConfig();
                string strCurrency = config.currency;

                litOrdNo.Text = ord.ordNo;

                //sales is cancelled
                if (ord.ordCancel == 1)
                {
                    litOrdNo.Text += " (Cancel)";
                }
                else
                {
                    //status is set
                    if (!string.IsNullOrEmpty(ord.ordStatusName)) { litOrdNo.Text += " (" + ord.ordStatusName + ")"; }
                }

                litMember.Text = ord.memEmail;

                if (!string.IsNullOrEmpty(ord.ordTypeName))
                {
                    litOrdType.Text = ord.ordTypeName;
                    trType.Visible = true;

                    if (ord.ordType == 1)
                    {
                        //pnlListingOnline.Visible = true;
                    }
                    else
                    {
                        litMember.Text = "N/A";
                        //pnlListingOnline.Visible = false;
                    }
                }

                litOrdDate.Text = ord.ordCreation.ToString("dd MMM yyyy");
                litOrdAmount.Text = strCurrency + " " + ord.ordTotal.ToString();
                litRemarks.Text = !string.IsNullOrEmpty(ord.ordRemarks) ? ord.ordRemarks.Replace("\n", "<br />") : "-";
                litMsgBuyer.Text = !string.IsNullOrEmpty(ord.ordMsgBuyer) ? ord.ordMsgBuyer.Replace("\n", "<br />") : "-";

                litName.Text = isEmpty(ord.shippingName);
                litContactNo.Text = isEmpty(ord.shippingContactNo);
                litAddress.Text = ord.shippingAdd + (!string.IsNullOrEmpty(litAddress.Text) ? ",<br>" : "");
                litPoscode.Text = ord.shippingPoscode + (!string.IsNullOrEmpty(litAddress.Text) ? "," : "");
                litCity.Text = ord.shippingCity + (!string.IsNullOrEmpty(litAddress.Text) ? ",<br>" : "");

                if(string.IsNullOrEmpty(litAddress.Text + litPoscode.Text + litCity.Text))
                {
                    litAddress.Text = "-";
                }


                litDeliveryDate.Text = isEmpty(ord.deliveryDate.ToString("dd MMM yyyy"));
                litDeliveryTime.Text = isEmpty(ord.deliveryTime);
                litDeliveryRecipientName.Text = isEmpty(ord.deliveryRecipient);
                litDeliverySenderName.Text = isEmpty(ord.deliverySender);
                litDeliveryMsg.Text = isEmpty(ord.deliveryMessage);

                litBillName.Text = ord.name;
                litBillAdd.Text = ord.add;
                litBillPoscode.Text = ord.poscode;
                litBillCity.Text = ord.city;
                litBillContactNo.Text = ord.contactNo;

                litBillState.Text = ord.state;
                litState.Text = ord.shippingState;
                litBillCountry.Text = ord.countryName;
                litCountry.Text = ord.shippingCountryName;

                litPayGateway.Text = ord.ordPayGateway;
                litPayRemark.Text = ord.ordPayRemarks;

                if (ord.ordPayment != DateTime.MaxValue) { litPayDate.Text = ord.ordPayment.ToString("dd MMM yyyy"); }

                //@ Bank Details
                if (ord.ordStatus >= clsAdmin.CONSTORDERSTATUSPAY)
                {
                    litBank.Text = !string.IsNullOrEmpty(ord.ordPayBankName) ? ord.ordPayBankName : "-";
                    litAccount.Text = !string.IsNullOrEmpty(ord.ordPayBankAccount) ? ord.ordPayBankAccount : "-";
                    litTransId.Text = !string.IsNullOrEmpty(ord.ordPayTransId) ? ord.ordPayTransId : "-";

                    // if payment gateway is Bank Transfer
                    if (ord.ordPayGatewayId <= 0)
                    {
                        litPayDate.Text = DateTime.Compare(ord.ordPayBankDate, DateTime.MaxValue) != 0 ? ord.ordPayBankDate.ToString("dd MMM yyyy") : "-";

                        trBank.Visible = true;
                        trAccount.Visible = true;
                        trTransId.Visible = true;
                    }
                }

                ucAdmOrderItems.transId = ordId;
                ucAdmOrderItems.cancel = ord.ordCancel;
                ucAdmOrderItems.status = ord.ordStatus;
                ucAdmOrderItems.type = ord.ordType;
                ucAdmOrderItems.decShipping = ord.ordShipping;
                ucAdmOrderItems.ord = ord;
                ucAdmOrderItems.fill();
            }
            else
            {
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
            }
        }

        fillHeaderFooter();
    }

    private void fillHeaderFooter()
    {
        // bind quick contact value
        clsConfig config = new clsConfig();
        string strGroupName = clsConfig.CONSTGROUPORDERSLIPSETUP;
        DataSet dsOrderSlip = config.getItemsByGroup(strGroupName, 1);
        string strCompanyRegNo = "";
        string strGstRegNo = "";

        //string strShowImgPath = ConfigurationManager.AppSettings["scriptBase"] +
        //                        "cmn/showImage.aspx?img=" +
        //                        Session[clsAdmin.CONSTPROJECTUPLOADPATH];
        foreach (DataRow row in dsOrderSlip.Tables[0].Rows)
        {
            //header
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_COMPANYLOGO, true) == 0)
            {
                string strCompanyLogo = row["CONF_VALUE"].ToString();
                if (!string.IsNullOrEmpty(strCompanyLogo))
                {
                    hypLogo.ToolTip = config.defaultTitle + "- Home";
                    hypLogo.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "usr/page.aspx?pgid=2";
                    divLogo.Visible = true;
                    //imgCompanyLogo.ImageUrl = ConfigurationManager.AppSettings["uplBase"] + "admctrlpnl/" + strCompanyLogo;
                    imgCompanyLogo.ImageUrl = Session[clsAdmin.CONSTPROJECTUSERVIEWURL] +
                                              ConfigurationManager.AppSettings["uplBase2"] + 
                                              clsAdmin.CONSTCTRLPNLADMFOLDER + "/" + 
                                              strCompanyLogo + "";
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_COMPANYNAME, true) == 0)
            {
                string strCompanyName = row["CONF_VALUE"].ToString();
                if (!string.IsNullOrEmpty(strCompanyName))
                {
                    divDomainName.Visible = true;
                    litDomainName.Text = strCompanyName;
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_ADDR1, true) == 0)
            {
                string strAddr1 = row["CONF_VALUE"].ToString();
                if (!string.IsNullOrEmpty(strAddr1))
                {
                    divAddr1.Visible = tdImgTel.Visible = true;
                    litAddr1.Text = strAddr1;
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_ADDR2, true) == 0)
            {
                string strAddr2 = row["CONF_VALUE"].ToString();
                if (!string.IsNullOrEmpty(strAddr2))
                {
                    divAddr2.Visible = tdImgTel.Visible = true;
                    litAddr2.Text = strAddr2;
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_TEL, true) == 0)
            {
                string strQuickContactTel = row["CONF_VALUE"].ToString();
                if (!string.IsNullOrEmpty(strQuickContactTel))
                {
                    tdDivTel.Visible = tdImgTel.Visible = true;
                    litQuickContactTel.Text = strQuickContactTel;
                    imgTel.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "adm/icon-tel.gif";
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_FAX, true) == 0)
            {
                string strQuickContactFax = row["CONF_VALUE"].ToString();
                if (!string.IsNullOrEmpty(strQuickContactFax))
                {
                    tdDivFax.Visible = tdImgFax.Visible = true;
                    litQuickContactFax.Text = strQuickContactFax;
                    imgFax.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "adm/icon-fax.gif";
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_EMAIL, true) == 0)
            {
                string strQuickContactEmail = row["CONF_VALUE"].ToString();
                if (!string.IsNullOrEmpty(strQuickContactEmail))
                {
                    tdDivEmail.Visible = tdImgEmail.Visible = true;
                    litQuickContactEmail.Text = strQuickContactEmail;
                    imgEmail.ImageUrl = ConfigurationManager.AppSettings["imageBase"] + "adm/icon-email.gif";
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_WEBSITE, true) == 0)
            {
                string strPageTitle = row["CONF_VALUE"].ToString();
                if (!string.IsNullOrEmpty(strPageTitle))
                {
                    divPageTitle.Visible = true;
                    litPageTitle.Text = strPageTitle;
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_COMPANYLREGNO, true) == 0)
            {
                strCompanyRegNo = row["CONF_VALUE"].ToString();
                if (!string.IsNullOrEmpty(strCompanyRegNo))
                {
                    divCompanyRegNo.Visible = tdImgEmail.Visible = true;
                    litCompanyRegNo.Text = strCompanyRegNo;
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_GSTREGNO, true) == 0)
            {
                strGstRegNo = row["CONF_VALUE"].ToString();
                if (!string.IsNullOrEmpty(strGstRegNo))
                {
                    divGstRegNo.Visible = tdImgEmail.Visible = true;
                    litGstRegNo.Text = strGstRegNo;
                }
            }

            //footer
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_LINE1, true) == 0)
            {
                string strLine1 = row["CONF_VALUE"].ToString();
                if (!string.IsNullOrEmpty(strLine1))
                {
                    divLine1.Visible = true;
                    litLine1.Text = strLine1;
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_LINE2, true) == 0)
            {
                string strLine2 = row["CONF_VALUE"].ToString();
                if (!string.IsNullOrEmpty(strLine2))
                {
                    divLine2.Visible = true;
                    litLine2.Text = strLine2;
                }
            }
            if (string.Compare(row["CONF_NAME"].ToString(), clsConfig.CONSTNAMEORDERSLIP_LINE3, true) == 0)
            {
                string strLine3 = row["CONF_VALUE"].ToString();
                if (!string.IsNullOrEmpty(strLine3))
                {
                    divLine3.Visible = true;
                    litLine3.Text = strLine3;
                }
            }
        }

        if (!string.IsNullOrEmpty(strCompanyRegNo) && !string.IsNullOrEmpty(strGstRegNo))
        {
            divRegNoSplitter.Visible = true;
        }

        if (!divLogo.Visible)
        {
            divPopUpBar.Visible = false;
        }
    }


    #endregion
    private string isEmpty(string value)
    {
        return isEmpty(value, "-");
    }
    private string isEmpty(string value, string emptyValue)
    {
        if (!string.IsNullOrEmpty(value))
        {
            return value;
        }
        return emptyValue;
    }
}
