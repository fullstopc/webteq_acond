﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="admProduct01.aspx.cs" Inherits="adm_admProduct01" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">

    <div class="main-content__header">
        <div class="title"><asp:Literal ID="litPageTitle" runat="server" meta:resourcekey="litPageTitle"></asp:Literal></div>
        <div class="divListingSplitter"></div>
        <div class="action">
			<asp:Panel ID="pnlListingAction" runat="server" CssClass="divListingAction">
                <asp:HyperLink ID="hypAddProd" runat="server" Text="Add Product" ToolTip="Add Product" NavigateUrl="admProduct0101.aspx" CssClass="hypListingAction add"></asp:HyperLink>
                <div class="divListingSplitter"></div>
                <asp:LinkButton ID="lnkbtnExport" runat="server" Text="Export to Excel" ToolTip="Export to Excel" CssClass="hypListingAction export"></asp:LinkButton>
            </asp:Panel>
        </div>
        <div class="divListingSplitter"></div>
        <div class="filter">
            <div class="divFilterContainer">
                <div class="divFilter">
                    <asp:TextBox ID="txtKeyword" runat="server" placeholder="Keyword Search" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
                    <a href="#" class="btnDropdown" onclick="toggleFilterOption();"></a>
                    <asp:Literal ID="litKeywordRule" runat="server" meta:resourcekey="litKeywordRule" visible="false"></asp:Literal>
                </div>
                <div class="divFilterMore">
                    <div></div>
                    <div>
                        <table class="tblSearch">
                            <tr>
                                <td class="">
                                    <div class="divFilterLabel">New Product:</div>
                                    <asp:DropDownList ID="ddlNew" runat="server" CssClass="ddl_medium"></asp:DropDownList>
                                </td>
                                <td class="">
									<div class="divFilterLabel">Best Seller:</div>
                                    <asp:DropDownList ID="ddlBestSeller" runat="server" CssClass="ddl_medium"></asp:DropDownList>
                                </td>
                            </tr>
							<tr>
                                <td class="">
                                    <div class="divFilterLabel">Show on Top:</div>
                                    <asp:DropDownList ID="ddlShowTop" runat="server" CssClass="ddl_medium"></asp:DropDownList>
                                </td>
                                <td class="">
									<div class="divFilterLabel">Active:</div>
                                    <asp:DropDownList ID="ddlActive" runat="server" CssClass="ddl_medium"></asp:DropDownList>
                                </td>
                            </tr>
							<tr>
                                <td class="">
                                    <div class="divFilterLabel">Hot Sales:</div>
                                    <asp:DropDownList ID="ddlHotSales" runat="server" CssClass="ddl_medium"></asp:DropDownList>
                                </td>
                                <td  id="tdInventory" class="" visible="false" runat="server">
									<div class="divFilterLabel">Show Inventory:</div>
									<asp:DropDownList ID="ddlInventory" runat="server" CssClass="ddl_medium"></asp:DropDownList>
                                </td>
                            </tr>
							
                        </table>

                    </div>
                </div>
            </div>
            <div class="divFilterAction"><%--<asp:Button ID="lnkbtnSearch" runat="server" Text="" ToolTip="Search" CssClass="btn btnSearch" OnClick="lnkbtnSearch_Click"/>--%></div>
        </div>
    </div>
    <div class="divListing">
        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
            <div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
        </asp:Panel>

        <uc:CmnDatatable ID="ucCmnDatatable" runat="server" />
        <asp:HiddenField runat="server" ID="hdnItemID" />
        <asp:LinkButton ID="lnkbtnDelete" runat="server" OnClick="lnkbtnDelete_Click" style="display:none;"></asp:LinkButton>
    </div>
    <script type="text/javascript">

        $(function () {
            var columns = [
                    { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                    { "data": "PROD_CODE", "label": "Code", "order": 1, },
                    { "data": "PROD_NAME", "label": "Name", "order": 2, },
                    { "data": "PROD_DNAME", "label": "Display Name", "order": 3, },
                    { "data": "PROD_NEW", "bSearchable": false, "label": "New", "order": 4, "convert": "active" },
                    { "data": "PROD_SHOWTOP", "bSearchable": false, "label": "Top", "order": 5, "convert": "active" },
                    { "data": "PROD_BEST", "bSearchable": false, "label": "New", "order": 6, "convert": "active" },
                    { "data": "PROD_ACTIVE", "bSearchable": false, "label": "Active", "order": 7, "convert": "active" },
                    { "data": "action", "bSortable": false, "bSearchable": false, "label": "Action", "order": 8 },
            ];
            var columnSort = columns.reduce(function (prev, curr) {
                prev[curr.data] = curr.order;
                return prev;
            }, {});

            var columnDefs = [{
                "render": function (data, type, row) {
                    var lnkbtnEdit = "<a class='lnkbtn lnkbtnEdit' href='admProduct0101.aspx?id=" + row.PROD_ID + "' title='Edit'></a>";
                    var lnkbtnDelete = "<a class='lnkbtn lnkbtnDelete' title='Delete' data-id='"+row.PROD_ID+"'></a>"; 
                    return lnkbtnEdit + lnkbtnDelete;
                    return '';
                },
                "targets": columnSort["action"]
            }];
            var properties = {
                columns: columns,
                columnDefs: columnDefs,
                columnSort: columnSort,
                func: "getProductData",
                name: "product",
            };
            var $datatable = callDataTables(properties);

            // Filter
            $("#<%= ddlActive.ClientID %>").change(function () {
                $datatable
               .columns(columnSort["PROD_ACTIVE"])
               .search(this.value)
               .draw();
            });
            $("#<%= ddlBestSeller.ClientID %>").change(function () {
                $datatable
               .columns(columnSort["PROD_BEST"])
               .search(this.value)
               .draw();
            })
            $("#<%= ddlShowTop.ClientID %>").change(function () {
                $datatable
               .columns(columnSort["PROD_SHOWTOP"])
               .search(this.value)
               .draw();
            });
            $("#<%= ddlNew.ClientID %>").change(function () {
                $datatable
               .columns(columnSort["PROD_NEW"])
               .search(this.value)
               .draw();
            });

             $(document).on("click", ".lnkbtnDelete", function () {
                if (confirm("<%= HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeletePage.Text") %>")) {
                    var itemID = $(this).attr("data-id");
                    document.getElementById('<%= hdnItemID.ClientID %>').value = itemID;
                    document.getElementById('<%= lnkbtnDelete.ClientID %>').click();
                }
             })

            $("#<%= lnkbtnExport.ClientID %>").click(function (e) {
                var $table = $('.dataTable').clone();
                $table.attr("border", "1").find("th:last-child,td:last-child").remove();
                window.open('data:application/vnd.ms-excel,' + encodeURIComponent($table.get(0).outerHTML));

                e.preventDefault();
            });


            
        });
    </script>
</asp:Content>

