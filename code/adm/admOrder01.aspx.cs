﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Configuration;
using System.Data;

public partial class adm_admOrder01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 24;
    protected string listingPage = "";
    protected bool isInvoiceFeature;
    protected bool isCouponFeature;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("pageTitle.Text").ToString(), true);

        if (!ClientScript.IsStartupScriptRegistered("FORMSECT3"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "initSect3();";
            strJS += "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "FORMSECT3", strJS, false);
        }

        if (!IsPostBack)
        {
            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMSALESMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            setPageProperties();

        }
    }

    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        clsMis mis = new clsMis();
        DataSet ds = new DataSet();
        ds = mis.getListByListGrp("ORDER STATUS", 1);
        DataView dv = new DataView(ds.Tables[0]);

        if (ddlType.SelectedValue != "") { dv.RowFilter = "LIST_PARENTVALUE = " + ddlType.SelectedValue; }
        else { dv.RowFilter = "LIST_VALUE > 0"; }

        dv.Sort = "LIST_VALUE ASC";

        ddlStatus.DataSource = dv;
        ddlStatus.DataTextField = "LIST_NAME";
        ddlStatus.DataValueField = "LIST_VALUE";
        ddlStatus.DataBind();

        ListItem ddlStatusDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlStatus.Items.Insert(0, ddlStatusDefaultItem);
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        litPageTitle.Text = GetLocalResourceObject("pageTitle.Text").ToString();
        if (Session[clsAdmin.CONSTPROJECTTYPE] != null && Session[clsAdmin.CONSTPROJECTTYPE] != "")
        {
            int intProjType = int.Parse(Session[clsAdmin.CONSTPROJECTTYPE].ToString());
            isInvoiceFeature = intProjType == clsAdmin.CONSTTYPEESHOPINVOICE || intProjType == clsAdmin.CONSTTYPEESHOPLITEINVOICE;
            isCouponFeature = Session[clsAdmin.CONSTPROJECTCOUPONMANAGER].ToString() == "1";
        }




        hypAddSales.Visible = isInvoiceFeature;
        listingPage = !isInvoiceFeature ? "admOrder0101.aspx" : "admOrder0102.aspx";


        clsMis mis = new clsMis();
        DataSet ds = new DataSet();
        DataView dv;

        ds = mis.getListByListGrp("ORDER TYPE", 1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "LIST_ORDER ASC";

        ddlType.DataSource = dv;
        ddlType.DataTextField = "LIST_NAME";
        ddlType.DataValueField = "LIST_VALUE";
        ddlType.DataBind();
        ddlType.Items.Insert(0, new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), ""));

        ds = mis.getListByListGrp("ORDER STATUS", 1);
        dv = new DataView(ds.Tables[0]);
        dv.RowFilter = "LIST_PARENTVALUE = 1";
        dv.Sort = "LIST_VALUE ASC";

        ddlStatus.DataSource = dv;
        ddlStatus.DataTextField = "LIST_NAME";
        ddlStatus.DataValueField = "LIST_VALUE";
        ddlStatus.DataBind();
        ddlStatus.Items.Insert(0, new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), ""));

        clsProduct prod = new clsProduct();
        ds = prod.getProdList(1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "PROD_DNAME ASC";

        ddlProduct.DataSource = dv;
        ddlProduct.DataTextField = "PROD_DNAME";
        ddlProduct.DataValueField = "PROD_ID";
        ddlProduct.DataBind();
        ddlProduct.Items.Insert(0, new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), ""));

        clsMember mem = new clsMember();
        ds = mem.getMemberCodeEmail(1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "MEM_CODE ASC";

        ddlMember.DataSource = dv;
        ddlMember.DataTextField = "MEM_CODECOMPANYEMAIL";
        ddlMember.DataValueField = "MEM_EMAIL";
        ddlMember.DataBind();
        ddlMember.Items.Insert(0, new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), ""));


    }
    #endregion


    protected void lnkbtnQuickDone_Click(object sender, EventArgs e)
    {
        int intItemID = Convert.ToInt16(hdnItemID.Value);
        clsMis.performSalesQuickDone(intItemID);

        Response.Redirect(Request.Url.ToString());
    }
}