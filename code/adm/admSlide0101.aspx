﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admSlide0101.aspx.cs" Inherits="adm_admSlide0101" Debug="true" ValidateRequest="false" %>
<%@ MasterType VirtualPath="~/mst/private.Master" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmMastheadGallery.ascx" TagName="AdmMastheadGallery" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
        <div class="action">
            <a href="#" class="hypListingAction setting" onclick="toggleSideMenu();">Page Detail</a>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlForm" runat="server" CssClass="divForm">
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <asp:Panel ID="pnlFormSect1" runat="server">
            <uc:AdmMastheadGallery ID="ucAdmMastheadGallery" runat="server" pageListingURL="admSlide01.aspx" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>







