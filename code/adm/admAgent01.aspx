﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admAgent01.aspx.cs" Inherits="adm_adm" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
	<div class="main-content__header">
		<div class="title"><asp:Literal ID="litPageTitle" runat="server"></asp:Literal></div>
		<div class="divListingSplitter"></div>
		<div class="action">
			<asp:Panel ID="pnlListingAction" runat="server" CssClass="divListingAction">
				<asp:LinkButton ID="lnkbtnExport" Visible="false" runat="server" Text="Export" ToolTip="Export" CssClass="hypListingAction export"></asp:LinkButton>
				<a href="admAgent0101.aspx" class="hypListingAction add">Add Agent</a>
                <div class="divListingSplitter"></div>
                <a href="admAgentLeaveCalendar.aspx" class="hypListingAction popup"><i class="fa fa-calendar" aria-hidden="true" style="padding-right:10px;"></i>Agent Leave</a>
			</asp:Panel>
		</div>
		<div class="divListingSplitter"></div>
		<div class="filter">
			<div class="divFilterContainer">
				<div class="divFilter">
					<asp:TextBox ID="txtKeyword" runat="server" placeholder="Keyword Search" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
					<a href="#" class="btnDropdown" onclick="toggleFilterOption();"></a>
					<asp:Literal ID="litKeywordRule" runat="server" meta:resourcekey="litKeywordRule" visible="false"></asp:Literal>
				</div>
				<div class="divFilterMore">
					<div></div>
					<div>
						<table class="tblSearch">
	
						</table>

					</div>
				</div>
			</div>
			<div class="divFilterAction"><%--<asp:Button ID="lnkbtnSearch" runat="server" Text="" ToolTip="Search" CssClass="btn btnSearch" OnClick="lnkbtnSearch_Click"/>--%></div>
		</div>
	</div>
	<div class="divListing">
		<asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
			<div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
		</asp:Panel>
		<uc:CmnDatatable ID="ucCmnDatatable" runat="server" />
		<asp:HiddenField runat="server" ID="hdnItemID" />
		<asp:LinkButton ID="lnkbtnDelete" runat="server" OnClick="lnkbtnDelete_Click" style="display:none;"></asp:LinkButton>
	</div>
	<script type="text/javascript">
	    var $datatable;

		$(function () {
			var columns = [
					{ "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                    { "data": "employee_name", "label": "Name", "order": 1, },
                    { "data": "employee_contact_tel", "label": "Contact No.", "order": 2, render: function (data, type, full, meta) { return full.employee_tel_prefix + "-" + data; }, },
					{ "data": "employee_email", "label": "Email", "order": 3, },
                    { "data": "employee_team", "label": "Team", "order": 4, },
                    { "data": "employee_login_username", "label": "Login ID", "order": 5, },
                    { "data": "employee_leave_flag", "label": "Leave", "order": 6, "convert": "active" },
                    { "data": "employee_active", "label": "Active", "order": 7, "convert": "active" },
                    { "data": "employee_login_timestamp", "label": "Last Login", "order": 8, "convert": "datetime", "format": "d MMM YYYY h:MM A" },
                    { "data": "action", "bSortable": false, "bSearchable": false, "label": "Action", "order": 9 },
			];
			var columnSort = columns.reduce(function (prev, curr) {
				prev[curr.data] = curr.order;
				return prev;
			}, {});

			var columnDefs = [{
				"render": function (data, type, row) {
					var lnkbtnEdit = "<a class='lnkbtn lnkbtnEdit' href='admAgent0101.aspx?id=" + row.employee_id + "' title='Edit'></a>";
					var lnkbtnDelete = "<a class='lnkbtn lnkbtnDelete' title='Delete' data-id='" + row.employee_id + "'></a>";
					return lnkbtnEdit + lnkbtnDelete;
                },
                "targets": columnSort["action"]
            },
            {
                "render": function (data, type, row) {
                    var lnkbtn = "<a href='admAgent0101.aspx?id=" + row.employee_id + "' title='Edit'>" + data + "</a>";
                    return lnkbtn;
                },
                "targets": columnSort["employee_name"]
            }];
            var properties = {
                columns: columns,
				columnDefs: columnDefs,
				columnSort: columnSort,
				func: "getAgentData",
				name: "agent",
			};
			$datatable = callDataTables(properties);

			$(document).on("click", ".lnkbtnDelete", function () {
				if (confirm("Are you sure you want to delete agent")) {
					var itemID = $(this).attr("data-id");
					document.getElementById('<%= hdnItemID.ClientID %>').value = itemID;
					document.getElementById('<%= lnkbtnDelete.ClientID %>').click();
				}
			});
			
		});
    </script>
    <script type="text/javascript">
        $(function () {
            $('.popup').magnificPopup({
                type: 'iframe',
                iframe: {
                    markup: `
                                <div class="mfp-iframe-scaler">
                                    <div class="mfp-close"></div>
                                    <iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>
                                </div>`
                }
            });
        })
    </script>
</asp:Content>