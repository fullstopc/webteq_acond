﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private_nomenu.master" AutoEventWireup="true" CodeFile="admlogin.aspx.cs" Inherits="adm_admlogin" %>
<%@ MasterType VirtualPath="~/mst/private_nomenu.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <script type="text/javascript">
        $(function () {
            $(document).bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    $('#lbtnLogin').click();
                }
            });
        })
    </script>
    <div id="divLoginContainer">
        <asp:Panel ID="pnlAck" runat="server" Visible="false" class="divAck">
            <div id="divAck"><span class="errmsg"><asp:Literal ID="litAck" runat="server"></asp:Literal></span></div>    
        </asp:Panel>  
        <asp:Panel ID="pnlLoginForm" runat="server">  
            <table cellpadding="0" cellspacing="0" id="tblLogin">

                <tr>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" class="txtEmail" autocomplete="off" placeholder="Email Address" MaxLength="250"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="<br />Please enter Email." ControlToValidate="txtEmail" class="errmsg" Display="dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr runat="server" visible="false">
                    <td><asp:TextBox ID="txtUserId" runat="server" class="text" MaxLength="20"></asp:TextBox><asp:RequiredFieldValidator ID="rfvUserId" runat="server" ErrorMessage="<br />Please enter User ID." ControlToValidate="txtUserId" class="errmsg" Display="dynamic"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td><asp:TextBox ID="txtPassword" runat="server" class="txtPassword" placeholder="Enter Your Password" TextMode="Password" MaxLength="20"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="<br />Please enter Password." ControlToValidate="txtPassword" class="errmsg" Display="dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="checkbox-group">
                            <asp:CheckBox runat="server" ID="chkboxRmbrMe" Text="Keep me sign in" />
                        </div>
                    </td>
                </tr> 
                <tr>
                    <td class="tdSpacer"></td>
                </tr> 
                <tr>
                    <td><asp:HyperLink ID="hypForgot" runat="server" Text="Forgot Password" CssClass="hypForgot"></asp:HyperLink></td>
                </tr> 
                <tr>
                    <td class="tdLoginBtn">
                        <button id="lbtnLogin" class="btn right" runat="server" alt="Login" onserverclick="lbLogin_Click" clientidmode="Static" style="border:0">
                            <span>Login</span>
                            <i class="fa fa-sign-in" aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>          
            </table>
        </asp:Panel>  
    </div>
    
</asp:Content>

