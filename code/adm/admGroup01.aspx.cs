﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;
using System.IO;

public partial class adm_admGroup01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 13; //need change sectId according to db
    protected int _grpingId = 0;//clsAdmin.CONSTLISTGROUPINGCAT;
    protected string _grpingName = clsAdmin.CONSTLISRGROUPINGCATNAME;
    protected string grping = clsAdmin.CONSTLISTGROUPINGGROUP;
    protected int _groupId = 0;
    protected int _parentId;
    protected int _groupParentChild;

    protected string _gvSortExpression = "GRP_ID";
    protected string _gvSortDirection = clsAdmin.CONSTSORTDESC;
    protected string _gvSort = "";

    protected string strKeyword;
    protected int intShowTop;
    protected int intActive;
    protected int intGrpCount = 0;
    protected int intGrpTotal = 0;
    protected Boolean _boolSearch = false;
    #endregion

    #region "Property Methods"
    protected string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    protected int grpingId
    {
        get
        {
            if ((Session["GRPINGID"] == null) || (Session["GRPINGID"] == "")) { return _grpingId; }
            else { return Convert.ToInt16(Session["GRPINGID"]); }
        }
        set { Session["GRPINGID"] = value; }
    }

    protected string grpingName
    {
        get { return Session["GRPINGNAME"] as string ?? _grpingName; }
        set { Session["GRPINGNAME"] = value; }
    }

    protected int groupId
    {
        get
        {
            if (ViewState["GROUPID"] == null)
            {
                return _groupId;
            }
            else
            {
                return Convert.ToInt16(ViewState["GROUPID"]);
            }
        }
        set { ViewState["GROUPID"] = value; }
    }
    protected int parentId
    {
        get
        {
            if (Session["PARENTID"] != null && Session["PARENTID"] != "")
            {
                return Convert.ToInt16(Session["PARENTID"]);
            }
            else
            {
                return _parentId;
            }
        }
        set { Session["PARENTID"] = value; }
    }
    protected int groupParentChild
    {
        get { return Convert.ToInt16(Session[clsAdmin.CONSTPROJECTGROUPPARENTCHILD]); }
        set { Session["GROUPPARENTCHILD"] = value; }
    }

    protected string gvSortExpression
    {
        get { return ViewState["GVSORTEXPRESSION"] as string ?? _gvSortExpression; }
        set { ViewState["GVSORTEXPRESSION"] = value; }
    }

    protected string gvSortDirection
    {
        get { return ViewState["GVSORTDIRECTION"] as string ?? _gvSortDirection; }
        set { ViewState["GVSORTDIRECTION"] = value; }
    }

    protected string gvSort
    {
        get { return ViewState["GVSORT"] as string ?? _gvSort; }
        set { ViewState["GVSORT"] = value; }
    }

    public Boolean boolSearch
    {
        get
        {
            if (ViewState["BOOLSEARCH"] == null)
            {
                return _boolSearch;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLSEARCH"]);
            }
        }
        set { ViewState["BOOLSEARCH"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMCATEGORYMANAGERNAME, 1);
            sectId = adm.pageId;

            if (!string.IsNullOrEmpty(Request["gid"]))
            {
                int intTryParse;
                if (int.TryParse(Request["gid"], out intTryParse))
                {
                    grpingId = intTryParse;
                }
            }
            else if (Session["GRPINGID"] != null && Session["GRPINGID"] != "")
            {
                grpingId = Convert.ToInt16(Session["GRPINGID"]);
            }

            //if (grpingId == clsAdmin.CONSTLISTGROUPINGCAT)
            //{
            //    grpingName = clsAdmin.CONSTLISTGROUPINGCATNAME;
            //    sectId = sectIdCat;
            //}
            //else if (grpingId == clsAdmin.CONSTLISTGROUPINGBRAND)
            //{
            //    grpingName = clsAdmin.CONSTLISTGROUPINGBRANDNAME;
            //    sectId = sectIdBrand;
            //}
            //else
            //{
            //    grpingId = clsAdmin.CONSTLISTGROUPINGCAT;
            //    grpingName = clsAdmin.CONSTLISTGROUPINGCATNAME;
            //    sectId = sectIdCat;
            //}

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;
                if (int.TryParse(Request["id"], out intTryParse))
                {
                    groupId = intTryParse;
                }
            }

            Master.sectId = sectId;
            Master.parentId = sectId;
            Master.id = groupId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            if (groupId > 0)
            {
                parentId = groupId;
                //    clsGroup grp = new clsGroup();
                //    grp.extractGrpById2(groupId, 0);

                //    if (grp.isValidGroupingId(grpingId, groupId))
                //    {
                //        if (grpingId == clsAdmin.CONSTLISTGROUPINGCAT)
                //        {
                //            DataSet ds = new DataSet();
                //            ds = grp.getGrpListById2(grpingId, grp.parentId, 0);

                //            Master.showItemsMenu = true;
                //            Master.dsItems = ds;
                //        }
                //        else
                //        {
                //            Response.Redirect(currentPageName + "?gid=" + grpingId);
                //        }
                //    }
                //    else
                //    {
                //        Response.Redirect(currentPageName + "?gid=" + grpingId);
                //    }
            }
            else
            {
                Session["PARENTID"] = null;
            }

            txtKeyword.Attributes.Add("onkeydown", "javascript:if(event.keyCode==13){document.getElementById('" + lnkbtnSearch.ClientID + "').click(); return false;}");

            
            ListItem ddlShowtopDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
            ddlShowTop.Items.Insert(0, ddlShowtopDefaultItem);
            ddlShowTop.Items.Add(new ListItem("Yes", "1"));
            ddlShowTop.Items.Add(new ListItem("No", "0"));

            ListItem ddlActiveDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
            ddlActive.Items.Insert(0, ddlActiveDefaultItem);
            ddlActive.Items.Add(new ListItem("Yes", "1"));
            ddlActive.Items.Add(new ListItem("No", "0"));

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMELISTINGSIZE, clsConfig.CONSTGROUPOTHERSETTINGS, 1);

            gvGroup.PagerSettings.FirstPageText = GetGlobalResourceObject("GlobalResource", "contentAdmFirstPageText.Text").ToString();
            gvGroup.PagerSettings.LastPageText = GetGlobalResourceObject("GlobalResource", "contentAdmLastPageText.Text").ToString();
            gvGroup.PageSize = (!string.IsNullOrEmpty(config.value)) ? Convert.ToInt16(config.value) : clsAdmin.CONSTADMPAGESIZE;
            //gvGroup.PageSize = clsAdmin.CONSTADMPAGESIZE;

            gvSort = gvSortExpression + " " + gvSortDirection;
            setPageProperties();
            bindGV(gvGroup, "GRP_ORDER ASC");
        }

        if (Session["DELETEDGRPNAME"] != null)
        {
            pnlAck.Visible = true;
            litAck.Text = grpingName + " (" + Session["DELETEDGRPNAME"] + ") has been deleted successfully.";

            if (groupParentChild == 0)
            {
                clsGroup grp = new clsGroup();
                grp.grpingId = grpingId;
                grp.extractGrpById(grp.otherGrpId, 0);
                if (Convert.ToInt16(Session["MOVEDPRODNO"]) > 1)
                {
                    litAck.Text = litAck.Text + "<br />" + Session["MOVEDPRODNO"] + " products have been moved to \"" + grp.grpName + "\".";
                }
                else
                {
                    litAck.Text = litAck.Text + "<br />" + Session["MOVEDPRODNO"] + " product has been moved to \"" + grp.grpName + "\".";
                }
            }

            litAck.Text = "<div class=\"noticemsg\">" + litAck.Text + "</div>";

            Session["DELETEDGRPNAME"] = null;
            Session["MOVEDPRODNO"] = 0;

            clsMis.resetListing();
        }
    }

    protected void ddlGrouping_SelectedIndexChanged(object sender, EventArgs e)
    {
        grpingId = Convert.ToInt16(ddlGrouping.SelectedValue);

        clsMis mis = new clsMis();
        DataSet ds = mis.getListByListValue(grping, ddlGrouping.SelectedValue);

        if (ds.Tables[0].Rows.Count > 0)
        {
            grpingName = ds.Tables[0].Rows[0]["LIST_NAME"].ToString();
        }

        Response.Redirect("admGroup01.aspx");

        gvGroup.PageIndex = 0;
        bindGV(gvGroup, "GRP_ORDER ASC");
        setPageProperties();
    }

    protected void gvGroup_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell tc in e.Row.Cells)
            {
                if (tc.HasControls())
                {
                    LinkButton lnkbtn = (LinkButton)tc.Controls[0];
                    //Image icon = new Image();
                    //icon.ImageUrl = "../img/adm/btn-cancel.gif";

                    if (lnkbtn != null && gvSortExpression == lnkbtn.CommandArgument)
                    {
                        //tc.Controls.Add(icon);
                        string strClass = (gvSortDirection == "DESC") ? "desc" : "asc";
                        tc.CssClass = strClass;
                    }


                }
            }
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            intGrpCount++;
            
            //LinkButton lnkbtnEdit = (LinkButton)e.Row.FindControl("lnkbtnEdit");
            LinkButton lnkbtnDelete = (LinkButton)e.Row.FindControl("lnkbtnDelete");
            LinkButton lnkbtnUp = (LinkButton)e.Row.FindControl("lnkbtnUp");
            LinkButton lnkbtnDown = (LinkButton)e.Row.FindControl("lnkbtnDown");
            HyperLink hypEdit = (HyperLink)e.Row.FindControl("hypEdit");
            HyperLink hypbtnSubCategory = (HyperLink)e.Row.FindControl("hypbtnSubCategory");

            int intId = Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "GRP_ID"));
            hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.NavigateUrl = "admGroup0101.aspx?id=" + intId;

            hypbtnSubCategory.NavigateUrl = currentPageName + "?gid=" + grpingId + "&id=" + intId;

            //lnkbtnEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteGroup.Text").ToString().Replace("<!GROUP!>", grpingName.ToLower()) + "');";
            //lnkbtnUp.Text = GetGlobalResourceObject("GlobalResource", "contentAdmUp.Text").ToString();
            //lnkbtnDown.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDown.Text").ToString();

            int grpid = int.Parse(gvGroup.DataKeys[e.Row.RowIndex].Value.ToString());
            clsGroup grp = new clsGroup();
            grp.grpingId = grpingId;

            if (grpid == grp.otherGrpId)
            {
                //@lex lnkbtnEdit.Visible = false;
                lnkbtnDelete.Visible = false;

                HtmlGenericControl spanSplitter = (HtmlGenericControl)e.Row.FindControl("spanSplitter");
                spanSplitter.Visible = false;
            }

            if (groupParentChild == 1)
            {
                //if (grpingId == clsAdmin.CONSTLISTGROUPINGCAT)
                //{
                    int intParent = Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "GRP_PARENT"));
                    int intCatLevel = 2;
                    clsGroup cat = new clsGroup();

                    while (intParent > 0)
                    {
                        if (cat.extractGrpById2(intParent, 1))
                        {
                            intParent = cat.parentId;
                            intCatLevel += 1;
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (intCatLevel > 1)
                    {
                        //LinkButton lnkbtnSubCategory = (LinkButton)e.Row.FindControl("lnkbtnSubCategory");
                        hypbtnSubCategory.Visible = true;

                        hypbtnSubCategory.Text = intCatLevel + "<span class=\"spanSuperscript\">lv</span> " + grpingName;

                        Literal litSplitter = (Literal)e.Row.FindControl("litSplitter");
                        litSplitter.Text = "<span class='spanSplitter'>|</span>";
                    }

                    //if (intCatLevel <= 2)
                    //{
                    //    //LinkButton lnkbtnSubCategory = (LinkButton)e.Row.FindControl("lnkbtnSubCategory");
                    //    lnkbtnSubCategory.Visible = true;

                    //    switch (intCatLevel)
                    //    {
                    //        case 1:
                    //            lnkbtnSubCategory.Text = "2<span class=\"spanSuperscript\">nd</span> " + grpingName;
                    //            break;
                    //        //case 2:
                    //        //    lnkbtnSubCategory.Text = "3<span class=\"spanSuperscript\">rd</span> " + grpingName;
                    //        //break;
                    //        default:
                    //            break;
                    //    }
                    //}
                //}
            }

            if (intGrpCount == 1)
            {
                lnkbtnUp.Enabled = false;
            }
            if (intGrpCount == intGrpTotal)
            {
                lnkbtnDown.Enabled = false;
            }
            //if (intGrpCount == 1 && intGrpCount == intGrpTotal)
            //{
            //    HtmlGenericControl spanSplitter2 = (HtmlGenericControl)e.Row.FindControl("spanSplitter2");
            //    spanSplitter2.Visible= false;
            //    lnkbtnUp.Visible = false;
            //    lnkbtnDown.Visible = false;
            //}
        }
    }

    protected void gvGroup_Sorting(object sender, GridViewSortEventArgs e)
    {
        string strSortExpDir = "";
        if (e.SortExpression != gvSortExpression)
        {
            gvSortExpression = e.SortExpression;
            gvSortDirection = clsAdmin.CONSTSORTASC;
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }
        else
        {
            switch (gvSortDirection)
            {
                case clsAdmin.CONSTSORTASC:
                    gvSortDirection = clsAdmin.CONSTSORTDESC;
                    break;
                case clsAdmin.CONSTSORTDESC:
                    gvSortDirection = clsAdmin.CONSTSORTASC;
                    break;
            }
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }

        gvSort = strSortExpDir;
        bindGV(gvGroup, strSortExpDir);
    }

    protected void gvGroup_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvGroup.PageIndex = e.NewPageIndex;
        bindGV(gvGroup, gvSort);
    }

    protected void gvGroup_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        clsGroup group = new clsGroup();

        if (e.CommandName == "cmdEdit")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int grpid = int.Parse(gvGroup.DataKeys[rowIndex].Value.ToString());
            Response.Redirect("admGroup0101.aspx?id=" + grpid);
        }
        else if(e.CommandName == "cmdDelete")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int grpid = int.Parse(gvGroup.DataKeys[rowIndex].Value.ToString());
            string uploadPath = Server.MapPath(ConfigurationManager.AppSettings["uplBase"].ToString() + "/" + clsAdmin.CONSTADMGRPFOLDER.ToString() + "/");
            int intMovedProdNo = 0;
            string strDeletedGrpName = "";

            clsGroup grp = new clsGroup();

            if (groupParentChild == 0)
            {
                grp.grpingId = grpingId;

                clsMis.performDeleteGroup(grpid, grp.otherGrpId, uploadPath, ref intMovedProdNo, ref strDeletedGrpName);

                Session["MOVEDPRODNO"] = intMovedProdNo;
                Session["DELETEDGRPNAME"] = strDeletedGrpName;

                Response.Redirect(currentPageName);
            }
            else
            {
                //if (grpingId == clsAdmin.CONSTLISTGROUPINGCAT)
                //{
                    grp.extractGrpById2(grpid, 0);

                    if (grp.getGrpNOC(grpingId, grpid, 0) > 0 || grp.getGrpNOP(grpid, 0) > 0)
                    {
                        pnlAck.Visible = true;
                        litAck.Text = "<div class=\"errmsg\">This " + grp.grpName + " cannot be deleted. It has sub categories or product(s).</div>";
                        //Response.Redirect(Request.Url.ToString());
                    }
                    else
                    {
                        int iRecordAffected = grp.deleteGroup(grpid);

                        if (iRecordAffected == 1)
                        {
                            if (grp.grpImage != "") { File.Delete(uploadPath + grp.grpImage); }

                            Session["DELETEDGRPNAME"] = grp.grpName;

                            Response.Redirect(Request.Url.ToString());
                        }
                    }
                //}
            }
            //grp.grpingId = grpingId;
            
            //clsMis.performDeleteGroup(grpid, grp.otherGrpId, uploadPath, ref intMovedProdNo, ref strDeletedGrpName);

            //Session["MOVEDPRODNO"] = intMovedProdNo;
            //Session["DELETEDGRPNAME"] = strDeletedGrpName;

            //Response.Redirect(currentPageName);
        }
        else if (e.CommandName == "cmdUp")
        {
            string strArg = e.CommandArgument.ToString();
            string[] strArgSplit = strArg.Split((char)'|');

            int intId = Convert.ToInt16(strArgSplit[0]);
            int intOrder = Convert.ToInt16(strArgSplit[1]);

            int intPrevOrder = 0;
            int intPrevId = 0;

            if (group.extractPrevGroup(intOrder, groupId, grpingId, 0))
            {
                intPrevOrder = group.grpOrder;
                intPrevId = group.grpId;
                group.updateGrpOrderById(intId, intPrevOrder, groupId);
                group.updateGrpOrderById(intPrevId, intOrder, groupId);

                //clsMis.resetListing();
                bindGV(gvGroup, "GRP_ORDER ASC");
            }
        }
        else if (e.CommandName == "cmdDown")
        {
            string strArg = e.CommandArgument.ToString();
            string[] strArgSplit = strArg.Split((char)'|');

            int intId = Convert.ToInt16(strArgSplit[0]);
            int intOrder = Convert.ToInt16(strArgSplit[1]);

            int intNextOrder = 0;
            int intNextId = 0;

            if (group.extractNextGroup(intOrder, groupId, grpingId, 0))
            {
                intNextOrder = group.grpOrder;
                intNextId = group.grpId;

                group.updateGrpOrderById(intId, intNextOrder, groupId);
                group.updateGrpOrderById(intNextId, intOrder, groupId);

                //clsMis.resetListing();
                bindGV(gvGroup, "GRP_ORDER ASC");
            }
        }
        else if (e.CommandName == "cmdSubCategory")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int grpid = Convert.ToInt16(gvGroup.DataKeys[rowIndex].Value.ToString());
            Response.Redirect(currentPageName + "?gid=" + grpingId + "&id=" + grpid);
        }
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        strKeyword = txtKeyword.Text.Trim();

        if (!string.IsNullOrEmpty(ddlShowTop.SelectedValue)) { intShowTop = Convert.ToInt16(ddlShowTop.SelectedValue); }
        if (!string.IsNullOrEmpty(ddlActive.SelectedValue)) { intActive = Convert.ToInt16(ddlActive.SelectedValue); }
        boolSearch = true;

        bindGV(gvGroup, "GRP_ORDER ASC");
    }

    protected void lnkbtnExport_Click(object sender, EventArgs e)
    {
        ExportGV(gvGroup, grpingName.ToUpper().Replace(" ", "") + "LIST", (DataView)Session["GRPDV"], true);
    }

    public override void VerifyRenderingInServerForm(Control control)
    {

    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        litPageTitle.Text = GetLocalResourceObject("litPageTitle.Text") + "(" + grpingName+")";
        hypAddGroup.Text = "Add " + grpingName;

        clsMis mis = new clsMis();
        DataSet dsGrouping = new DataSet();
        dsGrouping = mis.getListByListGrp("GROUPING", 1);
        DataView dvGrouping = new DataView(dsGrouping.Tables[0]);
        dvGrouping.Sort = "LIST_ORDER ASC";

        ddlGrouping.DataSource = dvGrouping;
        ddlGrouping.DataTextField = "LIST_NAME";
        ddlGrouping.DataValueField = "LIST_VALUE";
        ddlGrouping.DataBind();

        if (grpingId != 0)
        {
            ddlGrouping.SelectedValue = grpingId.ToString();
        }
        else
        {
            grpingId = !string.IsNullOrEmpty(ddlGrouping.SelectedValue) ? Convert.ToInt16(ddlGrouping.SelectedValue) : 0;
        }

        if (groupParentChild != 0)
        {
            //if (grpingId == clsAdmin.CONSTLISTGROUPINGCAT)
            //{
                if (groupId > 0)
                {
                    clsGroup grp = new clsGroup();
                    grp.extractGrpById2(groupId, 0);

                    int intParent;
                    int intCatLevel = 2;

                    while (grp.parentId > 0)
                    {
                        if (grp.extractGrpById2(grp.parentId, 1))
                        {
                            intParent = grp.parentId;
                            intCatLevel += 1;
                        }
                        else
                        {
                            break;
                        }
                    }

                    litPageTitle.Text = (grp.parentId > 0 ? grp.parentName + " > " : "") + grp.grpName;

                    if (intCatLevel > 1)
                    {
                        hypAddGroup.Text = "Add " + intCatLevel + "<span class=\"spanSuperscript\">lv</span> " + grpingName;
                    }
                }
                else
                {
                    litPageTitle.Text = GetLocalResourceObject("litPageTitle.Text") + "(" + grpingName+")";
                    hypAddGroup.Text = "Add 1<span class=\"spanSuperscript\">lv</span> " + grpingName;
                }
            //}
            //else
            //{
            //    litPageTitle.Text = GetLocalResourceObject("litPageTitle.Text") + " - " + grpingName.ToUpper();
            //    hypAddGroup.Text = "Add " + grpingName;
            //}
        }
    }

    protected void bindGV(GridView gvGroupRef, string gvSortExpDir)
    {
        clsGroup grp = new clsGroup();
        DataSet ds = new DataSet();
        DataView dv = new DataView();
        ds = grp.getGrpListByGrpingId2(grpingId, 0);

        dv = new DataView(ds.Tables[0]);
        dv.RowFilter = "1 = 1";
        //dv.RowFilter = "1 = 1 AND GRPING_ID = " + grpingId;
        Boolean boolFilter = false;

        if(grpingId > 0)//if (grpingId == clsAdmin.CONSTLISTGROUPINGCAT)
        {
            dv.RowFilter += " AND GRP_PARENT = " + groupId;
        }

        if (boolSearch)
        {
            if (!string.IsNullOrEmpty(strKeyword))
            {
                dv.RowFilter += " AND (GRP_CODE LIKE '%" + strKeyword + "%' OR GRP_NAME LIKE '%" + strKeyword + "%' OR GRP_NAME_JP LIKE '%" + strKeyword + "%' OR GRP_NAME_MS LIKE '%" + strKeyword + "%' OR GRP_NAME_ZH LIKE '%" + strKeyword + "%' OR GRP_DNAME LIKE '%" + strKeyword + "%' OR GRP_DNAME_JP LIKE '%" + strKeyword + "%' OR GRP_DNAME_MS LIKE '%" + strKeyword + "%' OR GRP_DNAME_ZH LIKE '%" + strKeyword + "%')";
                boolFilter = true;

                litGroupFound.Text = "Search for <span class=\"SectKeyword\">" + strKeyword + "</span> | ";
            }
            else { litGroupFound.Text = ""; }

            if (!string.IsNullOrEmpty(ddlShowTop.SelectedValue))
            {
                dv.RowFilter += " AND GRP_SHOWTOP = " + intShowTop;
                boolFilter = true;
            }

            if (!string.IsNullOrEmpty(ddlActive.SelectedValue))
            {
                dv.RowFilter += " AND GRP_ACTIVE = " + intActive;
                boolFilter = true;
            }
        }

        if (boolFilter)
        {
            //pnlSearchForm.CssClass = "divSearchFormShow";
            //hypHideShow.CssClass = "linkHide";
        }
        else
        {
            //pnlSearchForm.CssClass = "divSearchForm";
            //hypHideShow.CssClass = "linkShow";
        }

        if (gvSortExpDir != "")
        {
            dv.Sort = gvSortExpDir;
        }

        Session["GRPDV"] = dv;

        intGrpTotal = dv.Count;

        checkLanguage(gvGroupRef);
        gvGroupRef.DataSource = dv;
        gvGroupRef.DataBind();

        if (boolSearch && !string.IsNullOrEmpty(strKeyword)) { litGroupFound.Text = litGroupFound.Text + dv.Count + " record(s) listed."; }
        else { litGroupFound.Text = dv.Count + " record(s) listed."; }
        
        if (dv.Count > 0)
        {
            lnkbtnExport.Enabled = true;
            //lnkbtnExport.CssClass = "btn2 btnExport";
        }
        else
        {
            lnkbtnExport.Enabled = false;
            lnkbtnExport.CssClass += " disabled";
        }

        // exclude default group - Other
        if (clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITCATEGORY, (dv.Count - 1)))
        {
            hypAddGroup.Visible = false;
        }
    }

    protected void ExportGV(GridView gvRef, string strFileTitle, DataView dvItems, Boolean boolHideLastColumn)
    {
        DateTime dt = DateTime.Now;
        string strFileName = strFileTitle + "_" + dt.Day + dt.Month + dt.Year + ".xls";
        string strAttachment = "attachment; filename=" + strFileName;

        Response.ClearContent();
        Response.AddHeader("content-disposition", strAttachment);
        Response.ContentType = "application/vnd.xls";
        Response.ContentEncoding = System.Text.Encoding.Unicode;
        Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        checkLanguage(gvRef);
        gvRef.AllowPaging = false;
        gvRef.DataSource = dvItems;
        gvRef.DataBind();

        for (int i = 0; i < gvRef.Columns.Count; i++)
        {
            if (gvRef.HeaderRow.Cells[i].CssClass == "hiddenCol")
            {
                gvRef.HeaderRow.Cells[i].CssClass = "showCol";
            }

            gvRef.HeaderRow.Cells[i].Enabled = false;
        }

        for (int i = 0; i < gvRef.Rows.Count; i++)
        {
            GridViewRow row = gvRef.Rows[i];

            for (int j = 0; j < gvRef.Columns.Count; j++)
            {
                if (row.Cells[j].CssClass == "hiddenCol")
                {
                    row.Cells[j].CssClass = "showCol";
                }
            }
        }

        if (boolHideLastColumn)
        {
            int intColHide = gvRef.Columns.Count - 1;

            gvRef.HeaderRow.Cells[intColHide].Visible = false;
            gvRef.Columns[intColHide].Visible = false;
        }

        gvRef.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    protected void checkLanguage(GridView gvRef)
    {
        if (clsMis.isLangExist(clsAdmin.CONSTLANGJAPANESE))
        {
            gvRef.Columns[3].Visible = true;
            gvRef.Columns[7].Visible = true;
        }
        if (clsMis.isLangExist(clsAdmin.CONSTLANGMALAY))
        {
            gvRef.Columns[4].Visible = true;
            gvRef.Columns[8].Visible = true;
        }
        if (clsMis.isLangExist(clsAdmin.CONSTLANGCHINESE))
        {
            gvRef.Columns[5].Visible = true;
            gvRef.Columns[9].Visible = true;
        }
    }
    #endregion
}
