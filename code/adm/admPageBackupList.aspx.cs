﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class adm_admPageBackupList : System.Web.UI.Page
{
    #region "Properties"
    protected int _pgId = 0;
    protected int _type = 0;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int pgId
    {
        get
        {
            if (ViewState["PGID"] == null)
            {
                return _pgId;
            }
            else
            {
                return int.Parse(ViewState["PGID"].ToString());
            }
        }
        set { ViewState["PGID"] = value; }
    }

    protected int type
    {
        get
        {
            if (ViewState["TYPE"] == null)
            {
                return _type;
            }
            else
            {
                return Convert.ToInt16(ViewState["TYPE"]);
            }
        }
        set { ViewState["TYPE"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    pgId = int.Parse(Request["id"]);
                }
                catch (Exception ex)
                {
                    pgId = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["type"]))
            {
                try
                {
                    type = Convert.ToInt16(Request["type"]);
                }
                catch (Exception ex)
                {
                    type = 0;
                }
            }

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMELISTINGSIZE, clsConfig.CONSTGROUPOTHERSETTINGS, 1);

            gvItems.PageSize = (!string.IsNullOrEmpty(config.value)) ? Convert.ToInt16(config.value) : clsAdmin.CONSTADMPAGESIZE;
            setPageProperties();
        }
    }

    protected void gvItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

    protected void gvItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "cmdRestore")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int itemId = int.Parse(gvItems.DataKeys[rowIndex].Value.ToString());

            clsPage page = new clsPage();

            string strBackupCont = page.getPageBackupContentById(itemId);
            int intRecordAffected = 0;
            
            if (type == 1)
            {
                intRecordAffected = page.updateContentById(pgId, strBackupCont);
                Response.Write("<script>parent.tb_remove(); self.parent.location.href='admPage0101.aspx?id=" + pgId + "'; alert('Successfully Restore.');</script>");
            }
            else if (type == 2)
            {
                intRecordAffected = page.updateMobileContentById(pgId, strBackupCont);
                Response.Write("<script>parent.tb_remove(); self.parent.location.href='admPage0101.aspx?id=" + pgId + "&frm=3'; alert('Successfully Restore.')</script>");
            }

            
        }
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        DataSet ds = new DataSet();
        DataView dv = new DataView();

        clsPage page = new clsPage();
        ds = page.getPageBackupList(pgId, type);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "PAGEBACKUP_DATE DESC";

        gvItems.DataSource = dv;
        gvItems.DataBind();
    }
    #endregion
}
