﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="adm00.aspx.cs" Inherits="adm_adm00" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmDashboard.ascx" TagName="AdmDashboard" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" Runat="Server">

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <asp:Panel ID="pnlAccessDenied" runat="server" Visible="false">
        <div class="main-content__header">
            <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        </div>
        <div>
            <br /><asp:Literal ID="litAccessDenied" runat="server" meta:resourcekey="litAccessDenied"></asp:Literal>
        </div>
    </asp:Panel>
    <asp:Panel CssClass="dashboard__container" runat="server" ID="divDashboardContainer">
        <uc:AdmDashboard ID="ucAdmDashboard" runat="server" />
    </asp:Panel>
</asp:Content>

