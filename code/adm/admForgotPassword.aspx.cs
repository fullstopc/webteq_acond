﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class adm_admForgotPassword : System.Web.UI.Page
{
    #region "Properties"
    protected string _message = "Forgot Password";
    #endregion

    #region "Property Methods"

    public string message
    {
        get { return _message; }
        set { _message = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Master.message = message;

            imgbtnOk.OnClientClick = "javascript:document.location.href='admlogin.aspx'; return false;";
            imgbtnBack.OnClientClick = "javascript:document.location.href='admForgotPassword.aspx'; return false;";
            lnkbtnBack.OnClientClick = "javascript:document.location.href='admlogin.aspx'; return false;";
        }
    }

    protected void lbtnFPwd_Click(object sender, EventArgs e)
    {
        clsUser user = new clsUser();
        user.userEmail = txtEmail.Text.Trim();

        if (user.isEmailExist(1))
        {
            Random rand = new Random();
            int intRandNum = rand.Next(111111, 999999);

            clsMD5 md5 = new clsMD5();
            string strPassword = md5.encrypt(intRandNum.ToString());

            Boolean boolEdited = false;
            Boolean boolSuccess = true;
            int intRecordAffected = 1;

            intRecordAffected = user.updatePasswordByEmail(txtEmail.Text.Trim(), strPassword);
            if (intRecordAffected == 1) { boolEdited = true; }
            else { boolSuccess = false; }

            if (boolEdited)
            {
                if (boolSuccess)
                {
                    sendEmail(txtEmail.Text.Trim(), intRandNum);
                }
                else
                {
                    litAck.Text = "<div class=\"errmsg4\">Failed to assign new password. Please try again.</div>";

                    pnlAck.Visible = true;
                    //imgbtnBack.Visible = true;
                    //imgbtnOk.Visible = false;
                    //pnlLoginForm.Visible = false;
                 }
            }
            else
            {
                litAck.Text = "<div class=\"errmsg4\">Failed to assign new password. Please try again.</div>";

                pnlAck.Visible = true;
                //imgbtnBack.Visible = true;
                //imgbtnOk.Visible = false;
                //pnlLoginForm.Visible = false;
            }
        }
        else
        {
            litInvalidEmail.Text = "<div class=\"errmsg\">Invalid email. Please try again.</div>";

            litInvalidEmail.Visible = true;
            //imgbtnBack.Visible = true;
            //imgbtnOk.Visible = false;
            //pnlLoginForm.Visible = false;
        }
    }

    protected void sendEmail(string strUsrEmail, int intUsrPassword)
    {
        string strUserSubject = "";
        string strUserBody;
        string strUserEmail;
        string strUserName;
        string strAck;
        string strSubjectPrefix;
        string strSignature;
        string strSenderName;
        string strWebsiteName;

        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEFORGOTPWDUSEREMAILCONTENT, clsConfig.CONSTGROUPFORGOTPWDEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;

        strUserEmail = strUsrEmail;
        string[] strUserEmailSplit = strUsrEmail.Split((char)'@');
        strUserName = strUserEmailSplit[0];

        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strUserSubject = strSubjectPrefix + " Forgot Password"; }

        strUserBody = "Dear Admin";
        strUserBody += "<br /><br />As your request, your password for " + strUsrEmail + " has now been reset.<br /><br />";
        strUserBody += "New login details are as follows:<br />";
        strUserBody += "Email: " + strUsrEmail + "<br />";
        strUserBody += "Password: " + intUsrPassword + "<br /><br />";
        strUserBody += "Thank you.<br /><br />";
        strUserBody += strSignature;
        strUserBody = "<font style=\"font-size=12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strUserBody + "</font>";

        if (clsEmail.send(strSenderName, "", "", "", "", "", strUserSubject, strUserBody, 1))
        {
            strAck = "<div class=\"noticemsg3\">New password has been successfully reset.</div>";
        }
        else
        {
            strAck = "<div class=\"errmsg4\">Failed to assign new password. Please try again.<br /></div>";

            imgbtnBack.Visible = true;
            imgbtnOk.Visible = false;
        }

        litAck.Text = strAck;
        pnlAck.Visible = true;
        pnlLoginForm.Visible = false;
    }
    #endregion
}
