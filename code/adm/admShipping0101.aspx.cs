﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_admShipping0101 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 25;

    protected int _shipId = 0;
    protected int _mode = 1;
    protected int _formSect = 1;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int shipId
    {
        get
        {
            if (ViewState["SHIPID"] == null)
            {
                return _shipId;
            }
            else
            {
                return Convert.ToInt16(ViewState["SHIPID"]);
            }
        }
        set { ViewState["SHIPID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set
        {
            ViewState["FORMSECT"] = value;
        }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    shipId = Convert.ToInt16(Request["id"]);
                }
                catch (Exception ex)
                {
                    shipId = 0;
                }
            }
            else if (Session["NEWSHIPID"] != null)
            {
                try
                {
                    shipId = Convert.ToInt16(Session["NEWSHIPID"]);
                }
                catch (Exception ex)
                {
                    shipId = 0;
                }
            }
            else if (Session["EDITEDSHIPID"] != null)
            {
                try
                {
                    shipId = Convert.ToInt16(Session["EDITEDSHIPID"]);
                }
                catch (Exception ex)
                {
                    shipId = 0;
                }
            }

            if (Request["id"] != null || Session["EDITEDSHIPID"] != null)
            {
                mode = 2;
            }

            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMSHIPPINGMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.subSectId = clsMis.getSubSectId(sectId, formSect, 1);
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();
            Master.id = shipId;
            Master.showSideMenu = true;
            Master.boolShowSibling = false;
        }

        setPageProperties();
        Session["NEWSHIPID"] = null;
        Session["EDITEDSHIPID"] = null;
        Session["NOCHANGE"] = null;
        Session["DELETEDSHIPID"] = null;
        Session["ERRMSG"] = null;
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        switch (mode)
        {
            case 1:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleAdd.Text").ToString(), true);
                break;
            case 2:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleEdit.Text").ToString(), true);
                break;
            default:
                break;
        }

        if (!IsPostBack)
        {
            switch (mode)
            {
                case 1://Add
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleAdd.Text").ToString();
                    break;
                case 2://Edit
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleEdit.Text").ToString();
                    break;
                default:
                    break;
            }

            if (Session["NEWSHIPID"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }
            else if (Session["EDITEDSHIPID"] != null)
            {
                if (Session["NOCHANGE"] != null)
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
                }
                else
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";

                    clsMis.resetListing();
                }

                pnlAck.Visible = true;
            }
            else if (Session["DELETEDSHIPID"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }
            else if (Session["ERRMSG"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
            }
        }

        clsConfig config = new clsConfig();
        // default shipping type is 1
        ucAdmShipping.shippingType = !string.IsNullOrEmpty(config.shippingType) ? Convert.ToInt16(config.shippingType) : 1;
        int intType = !string.IsNullOrEmpty(config.shippingType) ? Convert.ToInt16(config.shippingType) : 1;
        
        if (intType == clsAdmin.CONSTSHIPPINGTYPE5)
        {
            pnlShippingDetails2.Visible = true;
            pnlShippingDetails.Visible = false;

            ucAdmShippingCompany.shipId = shipId;
            ucAdmShippingCompany.mode = mode;
            ucAdmShippingCompany.fill();
        }
        else if (intType == clsAdmin.CONSTSHIPPINGTYPE6)
        {
            pnlShippingWithWeightContainer.Visible = true;
            pnlShippingDetails.Visible = false;
            pnlShippingDetails2.Visible = false;

            ucAdmShippingWithWeight.shipId = shipId;
            ucAdmShippingWithWeight.mode = mode;
            ucAdmShippingWithWeight.fill();
        }
        else
        {
            ucAdmShipping.shipId = shipId;
            ucAdmShipping.mode = mode;
            ucAdmShipping.fill();
        }
    }
    #endregion
}