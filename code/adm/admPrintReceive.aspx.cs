﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class adm_admPrintReceive : System.Web.UI.Page
{
    #region "Properties"
    protected int _reId;

    protected int intItemCount = 0;
    protected decimal decDiscount = 0;
    protected decimal decTotal = 0;
    #endregion


    #region "Property Methods"
    public int reId
    {
        get { return _reId; }
        set { _reId = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;
                if (int.TryParse(Request["id"], out intTryParse))
                {
                    reId = intTryParse;
                }
            }

            setPageProperties();
            fillForm();
            bindRptData();
        }
    }

    protected void rptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            intItemCount += 1;

            Literal litNo = (Literal)e.Item.FindControl("litNo");
            litNo.Text = intItemCount.ToString();

            string strItemCode = DataBinder.Eval(e.Item.DataItem, "RI_PRODCODE").ToString();
            string strItemName = DataBinder.Eval(e.Item.DataItem, "RI_PRODNAME").ToString();

            Literal litItem = (Literal)e.Item.FindControl("litItem");
            litItem.Text = strItemCode + " " + strItemName;

            decimal decItemPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "RI_PRODPRICE"));
            decimal decItemDiscount = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "RI_PRODDISCOUNT"));
            decimal decItemTotal = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "RI_PRODTOTAL"));

            Literal litPrice = (Literal)e.Item.FindControl("litPrice");
            litPrice.Text = decItemPrice != 0 ? decItemPrice.ToString() : "";

            Literal litDiscount = (Literal)e.Item.FindControl("litDiscount");
            litDiscount.Text = decItemDiscount != 0 ? decItemDiscount.ToString() : "";

            Literal litAmount = (Literal)e.Item.FindControl("litAmount");
            litAmount.Text = decItemTotal != 0 ? decItemTotal.ToString() : "";
        }
        else if (e.Item.ItemType == ListItemType.Footer)
        {
            Literal litBillDiscount = (Literal)e.Item.FindControl("litBillDiscount");
            litBillDiscount.Text = decDiscount != 0 ? decDiscount.ToString() : "";

            Literal litTotal = (Literal)e.Item.FindControl("litTotal");
            litTotal.Text = decTotal != 0 ? decTotal.ToString() : "";
        }
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
    }

    protected void fillForm()
    {
        clsReceive re = new clsReceive();
        if (re.extractItemById(reId, 0))
        {
            decTotal = re.reTotal;
            decDiscount = re.reDiscount;

            int intVendor = re.vendorId;
            string strVendor = re.vendorName;

            clsVendor vendor = new clsVendor();
            if (vendor.extractMemberById(intVendor, 0))
            {
                strVendor = vendor.vendorName;
                strVendor += "<br />" + vendor.vendorAddress;
                strVendor += "<br /><br />" + vendor.vendorContactNo;
            }

            litVendor.Text = strVendor;
        }
    }

    protected void bindRptData()
    {
        clsReceive re = new clsReceive();
        DataSet ds = new DataSet();
        ds = re.getItemItemListById(reId);

        DataView dv = new DataView(ds.Tables[0]);

        if (dv.Count > 0)
        {
            intItemCount = 0;
            rptItems.DataSource = dv;
            rptItems.DataBind();
        }
    }
    #endregion
}
