﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="admFAQ01.aspx.cs" Inherits="adm_admFAQ01" ValidateRequest="false" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>

<%@ Register Src="~/ctrl/ucAdmCheckAccess.ascx" TagName="AdmCheckAccess" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <uc:AdmCheckAccess ID="AdmCheckAccess1" runat="server" />
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>    
    <div class="divForm">
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <asp:Panel ID="pnlFaqForm" runat="server" DefaultButton="lnkbtnSave">
            <table id="tblFaq" cellpadding="0" cellspacing="0" class="formTbl tblData">
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblQuesName" runat="server"></asp:Label><span class="attention_compulsory">*</span>:</td>
                    <td class="tdMax">
                        <asp:TextBox ID="txtQuesName" runat="server" CssClass="text_fullwidth" MaxLength="250"></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="rfvQuesName" runat="server" ErrorMessage="<br />Please enter question." ControlToValidate="txtQuesName" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgFaq"></asp:RequiredFieldValidator>--%>
                        <asp:CustomValidator ID="cvQuesName" runat="server" ControlToValidate="txtQuesName" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateQuesName_server" OnPreRender="cvQuesName_PreRender" ValidateEmptyText="true"></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel nobr"><asp:Label ID="lblAnsName" runat="server"></asp:Label><span class="attention_compulsory">*</span>:</td>
                    <td class="tdMax">
                        <asp:TextBox ID="txtAnsName" runat="server" CssClass="text_big" TextMode="MultiLine" Rows="5"></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="rfvAnsName" runat="server" ErrorMessage="<br />Please enter answer." ControlToValidate="txtAnsName" Display="Dynamic" CssClass="errmsg" ValidationGroup="vgFaq"></asp:RequiredFieldValidator>--%>
                        <asp:CustomValidator ID="cvAnsName" runat="server" ControlToValidate="txtAnsName" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateAnsName_server" OnPreRender="cvAnsName_PreRender" ValidateEmptyText="true"></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel"><asp:Label ID="lblActive" runat="server"></asp:Label>:</td>
                    <td class="tdMax">
                        <asp:CheckBox ID="chkboxActive" runat="server" Checked="true"/>
                    </td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" ToolTip="Delete" CssClass="btn btnCancel" OnClick="lnkbtnDelete_Click" Visible="false"></asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Cancel" ToolTip="Cancel" CssClass="btn btnBack" Visible="false"></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    
    <div class="divListing">
        <div class="divListingHdr"><asp:Literal ID="litGroupFound" runat="server"></asp:Literal>
            <asp:Panel ID="pnlBtn" runat="server" CssClass="divLinkBtn" Visible="false">
                <asp:LinkButton ID="lnkbtnExport" runat="server" Text="Export" ToolTip="Export" CssClass="btn2 btnExport" OnClick="lnkbtnExport_Click"></asp:LinkButton>
            </asp:Panel>
        </div>
        <asp:Panel ID="pnlAckList" runat="server" Visible="false" CssClass="divAck">
            <asp:Literal ID="litAckList" runat="server"></asp:Literal>
        </asp:Panel>
        <asp:Panel ID="pnlListingData" runat="server" CssClass="divListingData">
            <asp:GridView ID="gvFaq" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom" CssClass="dataTbl" OnRowDataBound="gvFaq_RowDataBound" OnSorting="gvFaq_Sorting" OnPageIndexChanging="gvFaq_PageIndexChanging" OnRowCommand="gvFaq_RowCommand" DataKeyNames="FAQ_ID" EmptyDataText="No record found.">
                <PagerStyle CssClass="pg" HorizontalAlign="Right" />
                <AlternatingRowStyle CssClass="td_alt" />
                <Columns>
                    <asp:TemplateField HeaderText="No.">
                        <ItemStyle CssClass="tdNo" />
                        <ItemTemplate>
                            <asp:Literal ID="litNo" runat="server" Text="<%# Container.DataItemIndex + 1 %>"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Question" DataField="FAQ_QUES" SortExpression="FAQ_QUES" />
                    <asp:BoundField HeaderText="Answer" DataField="FAQ_ANS" SortExpression="FAQ_ANS" HtmlEncode="false"/>
                    <asp:TemplateField HeaderText="Active" SortExpression="FAQ_ACTIVE">
                        <ItemStyle CssClass="td_small" />
                        <ItemTemplate>
                            <asp:Literal ID="litActive" runat="server" Text='<%#clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "FAQ_ACTIVE"))) %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Action">
                        <ItemStyle CssClass="tdAct" />
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnUp" runat="server" CausesValidation="false" CommandName="cmdUp" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FAQ_ID") + "|" + DataBinder.Eval(Container.DataItem, "FAQ_ORDER") %>'></asp:LinkButton>   
                            <asp:LinkButton ID="lnkbtnDown" runat="server" CausesValidation="false" CommandName="cmdDown" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FAQ_ID") + "|" + DataBinder.Eval(Container.DataItem, "FAQ_ORDER") %>'></asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnEdit" runat="server" CausesValidation="false" CommandName="cmdEdit"></asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CausesValidation="false" CommandName="cmdDelete"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </div>
</asp:Content>
