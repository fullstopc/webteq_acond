﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_admUserView : System.Web.UI.Page
{
    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["ADMID"] != null && Session["ADMID"] != "")
            {
                int intUserId = int.Parse(Session["ADMID"].ToString());
                string strEmail = Session["ADMEMAIL"].ToString();
                int intType = Convert.ToInt16(Session["ADMTYPE"]);

                string strSettings = "";
                string strCode = "";

                clsMD5 md5 = new clsMD5();
                strCode = md5.encrypt(strEmail + DateTime.Now);

                if (Session[clsAdmin.CONSTPROJECTCONFIGLIMITPAGE] != null && Session[clsAdmin.CONSTPROJECTCONFIGLIMITPAGE] != "")
                {
                    strSettings += Session[clsAdmin.CONSTPROJECTCONFIGLIMITPAGE].ToString() + clsAdmin.CONSTDEFAULTSEPERATOR.ToString();
                }

                // add by sh.chong 30 Apr 2015 - manage user view to specific page (product,event, page)
                string strParameter = "";
                if (!string.IsNullOrEmpty(Request[clsConfig.CONSTMANAGEUSERVIEW_PARAMS]) && 
                    !string.IsNullOrEmpty(Request[clsConfig.CONSTMANAGEUSERVIEW_TEMPLATE]))
                {
                    strParameter += "&" + clsConfig.CONSTMANAGEUSERVIEW_PARAMS + "=" + Request[clsConfig.CONSTMANAGEUSERVIEW_PARAMS].ToString();
                    strParameter += "&" + clsConfig.CONSTMANAGEUSERVIEW_TEMPLATE + "=" + Request[clsConfig.CONSTMANAGEUSERVIEW_TEMPLATE].ToString();
                    strParameter += "&" + "friendlyUrl" + "=" + Request["friendlyUrl"].ToString();

                    if (Request["title"] != null)
                    {
                        strParameter += "&" + "title" + "=" + Request["title"].ToString();
                    }
                    if (Request["isproduct"] != null)
                    {
                        strParameter += "&" + "isproduct" + "=" + Request["isproduct"].ToString();
                        if (Request["title_group"] != null){strParameter += "&" + "title_group" + "=" + Request["title_group"].ToString();}
                        if (Request["title_product"] != null){strParameter += "&" + "title_product" + "=" + Request["title_product"].ToString();}
                    }
                }
                // end by sh.chong 30 Apr 2015 - manage user view to specific page (product,event, page)


                clsLogin login = new clsLogin();
                int intRecordAffected = 0;

                intRecordAffected = login.add(intUserId, strEmail, intType, strSettings, strCode);

                if (intRecordAffected == 1)
                {
                    Response.Redirect(Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + clsAdmin.CONSTUSERVIEWADMINLOGINPAGE + "?c=" + strCode + strParameter);
                }
            }
        }
    }
    #endregion
}
