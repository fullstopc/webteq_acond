﻿<%@ Page Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admAgentLeaveForm.aspx.cs" Inherits="adm_adm"  ValidateRequest="false" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/mst/blank.Master" %>
<asp:Content ID="Content" ContentPlaceHolderID="cphHead" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <br />
    <div class="container-fluid">
        <div class="row">
            <label class="col-sm-1"><b>Agent:</b></label>
            <div class="col-sm-3">
                <div class="form-group">
                    <asp:DropDownList runat="server" ID="ddlAgent" CssClass="form-control"></asp:DropDownList>
                </div>
                <asp:RequiredFieldValidator ID="rvAgent" 
                 ControlToValidate="ddlAgent"
                 Display="Dynamic"
                 CssClass="errmsg"
                 ErrorMessage="Please select agent."
                 ValidationGroup="vg"
                 runat="server"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-1"><b>Date From:</b></label>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class='input-group date' id='datepickerFrom'>
                        <asp:TextBox ID="txtDateFrom" runat="server" CssClass="form-control datepicker" ></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <asp:RequiredFieldValidator ID="rvDateFrom" 
                 ControlToValidate="txtDateFrom"
                 Display="Dynamic"
                 CssClass="errmsg"
                 ErrorMessage="Please enter date from."
                 ValidationGroup="vg"
                 runat="server"></asp:RequiredFieldValidator>
                
            </div>
        </div>
        <div class="row">
            <label class="col-sm-1"><b>Date To:</b></label>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class='input-group date' id='datepickerTo'>
                        <asp:TextBox ID="txtDateTo" runat="server" CssClass="form-control datepicker" ></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                
                <asp:RequiredFieldValidator ID="rvDateTo" 
                 ControlToValidate="txtDateTo"
                 Display="Dynamic"
                 CssClass="errmsg"
                 ErrorMessage="Please enter date to."
                 ValidationGroup="vg"
                 runat="server"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-1"><b>Reason:</b></label>
            <div class="col-sm-3">
                <div class="form-group">
                    <asp:TextBox runat="server" ID="txtReason" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
               
                
                <asp:RequiredFieldValidator ID="rvReason" 
                 ControlToValidate="txtReason"
                 Display="Dynamic"
                 CssClass="errmsg"
                 ErrorMessage="Please enter reason."
                 ValidationGroup="vg"
                 runat="server"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <asp:LinkButton runat="server" ID="lnkbtnSave" OnClick="lnkbtnSave_Click" ToolTip="Save" CssClass="btn btnSave right" ValidationGroup="vg" style="margin:0px 0px 10px 0px;">Save</asp:LinkButton>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#datepickerFrom, #datepickerTo').datetimepicker({
                format: 'DD MMM YYYY',
            });
        })
    </script>
</asp:Content>