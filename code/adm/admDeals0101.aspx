﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admDeals0101.aspx.cs" Inherits="adm_admDeals0101" ValidateRequest="false" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/mst/private.Master" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmDealDetails.ascx" TagName="AdmDealDetails" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmDownloadedDeal.ascx" TagName="AdmDownloadedDeal" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmDealPrice.ascx" TagName="AdmDealPrice" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmRelatedDeal.ascx" TagName="AdmRelatedDeal" TagPrefix="uc" %>

<%-- Soo Shin -- 2013-11-21 --%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
        <div class="action">
            <a href="#" class="hypListingAction setting" onclick="toggleSideMenu();">Page Detail</a>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlForm" runat="server" CssClass="divForm">
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <asp:Panel ID="pnlFormSect1" runat="server">
            <uc:AdmDealDetails ID="ucAdmDealDetails" runat="server" pageListingURL="admDeals01.aspx" />
        </asp:Panel>
        <asp:Panel ID="pnlFormSect2" runat="server" Visible="false">
            <uc:AdmDownloadedDeal ID="ucAdmDownloadedDeal" runat="server" />
        </asp:Panel>
        <asp:Panel ID="pnlFormSect3" runat="server" Visible="false">
            <uc:AdmDealPrice ID="ucAdmDealPrice" runat="server" pageListingURL="admDeals01.aspx" />
        </asp:Panel>
        <asp:Panel ID="pnlFormSect4" runat="server" Visible="false">
            <uc:AdmRelatedDeal ID="ucAdmRelatedDeal" runat="server" pageListingURL="admDeals01.aspx" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>
<%-- End of Soo Shin -- 2013-11-21 --%>