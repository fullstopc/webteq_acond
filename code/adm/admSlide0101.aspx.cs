﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using System.IO;

public partial class adm_admSlide0101 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 10;
    protected string _currentPageName;
    protected int _mode = 1;
    protected int _formSect = 1;
    protected int _mastId = 0;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    public string strSavedMsg
    {
        get { return String.Format(GetGlobalResourceObject("GlobalResource", "admSavedMessageWithParams.Text").ToString(), "Slide Show"); }
    }
    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int mastId
    {
        get
        {
            if (ViewState["MASTID"] == null)
            {
                return _mastId;
            }
            else
            {
                return int.Parse(ViewState["MASTID"].ToString());
            }
        }
        set
        {
            ViewState["MASTID"] = value;
        }
    }

    public int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set
        {
            ViewState["FORMSECT"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    mastId = Convert.ToInt16(Request["id"]);
                }
                catch (Exception ex)
                {
                    mastId = int.MaxValue;
                }
            }
            else if (Session["NEWMASTID"] != null)
            {
                try
                {
                    mastId = Convert.ToInt16(Session["NEWMASTID"]);
                }
                catch (Exception ex)
                {
                    mastId = int.MaxValue;
                }
            }
            else if (Session["EDITMASTID"] != null)
            {
                try
                {
                    mastId = Convert.ToInt16(Session["EDITMASTID"]);
                }
                catch (Exception ex)
                {
                    mastId = int.MaxValue;
                }
            }
            if (Request["id"] != null || Session["EDITMASTID"] != null)
            {
                mode = 2;
            }

            Session["ORIFILETITLE"] = "";
            Session["ORIFILEUPLOAD"] = "";
            Session["DELFILETITLE"] = "";
            Session["DELFILEUPLOAD"] = "";
            Session["FILETITLE"] = "";
            Session["FILEUPLOAD"] = "";

            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMSLIDESHOWMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.parentId = sectId;

            Master.subSectId = clsMis.getSubSectId(sectId, formSect, 1);
            Master.showSideMenu = true;
            Master.id = mastId;

            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();
        }

        setPageProperties();
        Session["NEWMASTID"] = null;
        Session["EDITMASTID"] = null;
        Session["NOCHANGE"] = null;
        Session["DELETEDMASTNAME"] = null;
        Session["ERRMSG"] = null;
    }

    #region "Methods"

    protected void setPageProperties()
    {
        switch (mode)
        {
            case 1:
                Page.Title += clsMis.formatPageTitle("ADD MASTHEAD IMAGE", true);
                break;
            case 2:
                Page.Title += clsMis.formatPageTitle("EDIT MASTHEAD IMAGE", true);
                break;
            default:
                break;
        }

        if (!IsPostBack)
        {
            switch (mode)
            {
                case 1:
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleAdd.Text").ToString();
                    break;
                case 2:
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleEdit.Text").ToString();
                    break;
                default:
                    break;
            }

            if (Session["NEWMASTID"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + strSavedMsg + "</div>";

                clsMis.resetListing();
            }
            else if (Session["EDITMASTID"] != null)
            {
                if (Session["NOCHANGE"] != null)
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
                }
                else
                {
                    int intTotal = new clsMasthead().getTotalRecord(0, 0);
                    string strUrlFormpage = "<a href='admSlide0101.aspx'>Add New</a>";
                    string strUrlListpage = "<a href='admSlide01.aspx'>Back to Listing</a>";

                    litAck.Text = "<div class=\"noticemsg\">" + strSavedMsg + "  " + strUrlListpage + "";
                    if (!clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITSLIDESHOWIMAGE, intTotal - 1))
                    {
                        litAck.Text += " & " + strUrlFormpage + "";
                    }
                    litAck.Text += "</div>";
                    clsMis.resetListing();
                }

                pnlAck.Visible = true;
            }
            else if (Session["DELETEDMASTNAME"] != null)
            {
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString() + "</div>";

                pnlAck.Visible = true;

                clsMis.resetListing();
            }
            else if (Session["ERRMSG"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
            }
        }
        ucAdmMastheadGallery.fill(mode, mastId);
    }
    #endregion
}
