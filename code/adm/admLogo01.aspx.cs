﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_admLogo01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 84;
    protected int _mode = 1;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            if (Request["id"] != null || Session["EDITEDCMSCP"] != null)
            {
                mode = 2;
            }

            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMIMAGESETTINGS, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.parentId = 1;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            setPageProperties();
        }


        Session["EDITEDLOGO"] = null;
        Session["NOCHANGE"] = null;
        Session["ERRMSG"] = null;
        Session["ADMPAGENAME"] = null;
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        if (Session["EDITEDLOGO"] != null)
        {
            if (Session["NOCHANGE"] != null)
            {
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
            }
            else
            {
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";
                clsMis.resetListing();
            }

            pnlAck.Visible = true;
        }
        else if (Session["ERRMSG"] != null)
        {
            pnlAck.Visible = true;
            litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
        }
    }
    #endregion
}
