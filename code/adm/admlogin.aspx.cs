﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Collections.Specialized;
using System.Linq;

public partial class adm_admlogin : System.Web.UI.Page
{
    #region "Properties"
    protected string _message = "Login for Admin View";
    #endregion

    #region "Property Methods"

    public string message
    {
        get { return _message; }
        set { _message = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        resetSession();
        Session[clsAdmin.CONSTADMINCS] = null;

        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            Master.message = message;
            hypForgot.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admForgotPassword.aspx";
        }
    }

    protected void lbLogin_Click(object sender, EventArgs e)
    {
        resetSession();

        int intRecordAffected;
        clsAdmin adm = new clsAdmin();
        clsMD5 md5 = new clsMD5();
        clsConfig config = new clsConfig();

        string email = txtEmail.Text.Trim();
        string password = txtPassword.Text.Trim();

        adm.email = email;
        adm.password = md5.encrypt(password);


        resetSession();


        if (adm.login(1))
        {
            setAdminSession(adm.admId, adm.type, adm.email, "1");
            if (chkboxRmbrMe.Checked)
            {
                setAdminCookie(adm.admId, 0, 1, adm.email, "1");
            }
            intRecordAffected = adm.setLastLogin();

            if (intRecordAffected == 1)
            {
                initSession();
                string strLandingPage = !string.IsNullOrEmpty(config.admLandingPage) ? config.admLandingPage : "adm00.aspx";
                Response.Redirect(strLandingPage);
            }
            else
            {
                Session["ADMID"] = null;
                Session["ADMEMAIL"] = null;
                Session["ADMTYPE"] = null;
                Session["ADMROOT"] = null;

                pnlAck.Visible = true;
                litAck.Text = "Login failed. Please try again.";
            }
        }
        else
        {
            loginFail();
        }
    }
    #endregion

    #region "Methods"
    private void setAdminCookie(int ID, int projID, int type, string email, string root)
    {
        // for type, 1 = admin, 2 = user, 3 = single login user.
        NameValueCollection cookie = System.Web.HttpUtility.ParseQueryString(string.Empty);

        cookie["ID"] = ID.ToString();
        cookie["projID"] = projID.ToString();
        cookie["Type"] = type.ToString();
        cookie["Email"] = email;
        cookie["Root"] = root;

        clsAES aes = new clsAES();
        Response.Cookies[aes.EncryptToString("CA_Admin")].Value = aes.EncryptToString(cookie.ToString());
        Response.Cookies[aes.EncryptToString("CA_Admin")].Expires = DateTime.Now.AddDays(30);
    }

    private void setAdminSession(int ID, int type, string email, string root)
    {
        Session["ADMID"] = ID;
        Session["ADMEMAIL"] = email;
        Session["ADMTYPE"] = type;
        Session["ADMROOT"] = root;
    }

    private void setCtrlSession(int ID, int type, string email)
    {
        Session["CTRLEMAIL"] = email;
        Session["CTRLTYPE"] = type;
        Session["CTRLID"] = ID;
    }
    private void setCtrlCookie(int ID, int type, string email)
    {
        NameValueCollection cookie = System.Web.HttpUtility.ParseQueryString(string.Empty);

        cookie["ID"] = ID.ToString();
        cookie["Type"] = type.ToString();
        cookie["Email"] = email;

        clsAES aes = new clsAES();
        Response.Cookies[aes.EncryptToString("CA_Controller")].Value = aes.EncryptToString(cookie.ToString());
        Response.Cookies[aes.EncryptToString("CA_Controller")].Expires = DateTime.Now.AddDays(30);
    }
    private void resetSession()
    {
        clsAES aes = new clsAES();
        Response.Cookies[aes.EncryptToString("CA_Admin")].Expires = DateTime.Now.AddDays(-1);

        Session["ADMID"] = null;
        Session["ADMEMAIL"] = null;
        Session["ADMTYPE"] = null;
        Session["ADMROOT"] = null;

        clsProject.resetSession();
    }
    private void resetSingleLoginSession()
    {
        clsAES aes = new clsAES();
        Response.Cookies[aes.EncryptToString("CA_Controller")].Expires = DateTime.Now.AddDays(-1);

        Session["CTRLID"] = null;
        Session["CTRLEMAIL"] = null;
        Session["CTRLTYPE"] = null;
    }
    private void loginFail()
    {
        resetSingleLoginSession();
        resetSession();
        pnlAck.Visible = true;
        litAck.Text = "Login failed. Please try again.";
    }

    private void initSession()
    {
        Session[clsAdmin.CONSTPROJECTNAME] = null;
        Session[clsAdmin.CONSTPROJECTDRIVER] = null;
        Session[clsAdmin.CONSTPROJECTSERVER] = null;
        Session[clsAdmin.CONSTPROJECTUSERID] = null;
        Session[clsAdmin.CONSTPROJECTPASSWORD] = null;
        Session[clsAdmin.CONSTPROJECTDATABASE] = null;
        Session[clsAdmin.CONSTPROJECTOPTION] = null;
        Session[clsAdmin.CONSTPROJECTPORT] = null;

        string strUploadPath = Server.MapPath(ConfigurationManager.AppSettings["uplBase"].ToString());
        strUploadPath = strUploadPath.TrimEnd('\\');
        strUploadPath = strUploadPath.Remove(strUploadPath.LastIndexOf('\\') + 1);

        Session[clsAdmin.CONSTPROJECTUPLOADPATH] = strUploadPath;
        Session[clsAdmin.CONSTPROJECTUSERVIEWURL] = ConfigurationManager.AppSettings["fullbase"] + ConfigurationManager.AppSettings["scriptBase"];
        Session[clsAdmin.CONSTPROJECTTYPE] = "4";
        Session[clsAdmin.CONSTPROJECTLANG] = null;
        Session[clsAdmin.CONSTPROJECTFULLPATH] = null;
        Session[clsAdmin.CONSTPROJECTFULLPATHVALUE] = null;
        Session[clsAdmin.CONSTPROJECTGROUPPARENTCHILD] = null;
        Session[clsAdmin.CONSTPROJECTFAQ] = "0";
        Session[clsAdmin.CONSTPROJECTPROGRAMMANAGER] = "0";
        Session[clsAdmin.CONSTPROJECTEVENTMANAGER] = null;
        Session[clsAdmin.CONSTPROJECTCOUPONMANAGER] = null;
        Session[clsAdmin.CONSTPROJECTMULTIPLECURRENCY] = null;
        Session[clsAdmin.CONSTPROJECTINVENTORY] = "0";
        Session[clsAdmin.CONSTPROJECTMOBILEVIEW] = null;
        Session[clsAdmin.CONSTPROJECTPRODUCTDESCRIPTION] = null;
        Session[clsAdmin.CONSTPROJECTPAYMENTGATEWAY] = "1|3";
        Session[clsAdmin.CONSTPROJECTGST] = null;
        Session[clsAdmin.CONSTPROJECTGSTNOTAVAILABLE] = null;
        Session[clsAdmin.CONSTPROJECTGSTINCLUSIVE] = null;
        Session[clsAdmin.CONSTPROJECTGSTAPPLIED] = null;
        Session[clsAdmin.CONSTPROJECTFBLOGIN] = null;

        Session[clsAdmin.CONSTPROJECTIMGSETTING] = "0";
        Session[clsAdmin.CONSTPROJECTFILESETTING] = "10000";
        Session[clsAdmin.CONSTPROJECTCKEDITORFILE] = null;
        Session[clsAdmin.CONSTPROJECTCKEDITORIMG] = null;
        Session[clsAdmin.CONSTPROJECTDEFAULTSETTING] = "500";
        Session[clsAdmin.CONSTPROJECTEVENTTHUMBSETTING] = null;
        Session[clsAdmin.CONSTPROJECTEVENTGALLSETTING] = null;
        Session[clsAdmin.CONSTPROJECTPRODTHUMBSETTING] = null;
        Session[clsAdmin.CONSTPROJECTPRODGALLSETTING] = null;
        Session[clsAdmin.CONSTPROJECTTRAINING] = "0";
        Session[clsAdmin.CONSTPROJECTEVENTMAXIMAGEUPLOAD] = null;


        // config
        Session[clsAdmin.CONSTPROJECTCONFIGLIMITPAGE] = null;
        Session[clsAdmin.CONSTPROJECTCONFIGLIMITOBJECT] = null;
        Session[clsAdmin.CONSTPROJECTCONFIGLIMITSLIDESHOW] = null;
        Session[clsAdmin.CONSTPROJECTCONFIGLIMITSLIDESHOWIMAGE] = null;
        Session[clsAdmin.CONSTPROJECTCONFIGLIMITEVENT] = null;
        Session[clsAdmin.CONSTPROJECTCONFIGLIMITCATEGORY] = null;
        Session[clsAdmin.CONSTPROJECTCONFIGLIMITPRODUCT] = null;
        Session[clsAdmin.CONSTGOOGLEANALYTICSCLIENTID] = null;
        Session[clsAdmin.CONSTPROJECTWATERMARK] = null;
        Session[clsAdmin.CONSTPROJECTFRIENDLYURL] = null;
        Session[clsAdmin.CONSTPROJECTGROUPMENU] = "0";

        // slider
        Session[clsAdmin.CONSTPROJECT_SLIDER_ACTIVE] = null;
        Session[clsAdmin.CONSTPROJECT_SLIDER_DEFAULT] = null;
        Session[clsAdmin.CONSTPROJECT_SLIDER_FULLSCREEN] = null;


    }

    #endregion
}
