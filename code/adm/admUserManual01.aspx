﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" validateRequest="false" EnableEventValidation="false" CodeFile="admUserManual01.aspx.cs" Inherits="adm_admUserManual01" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmUserManual.ascx" TagName="AdmUserManual" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server" meta:resourcekey="litPageTitle"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <uc:AdmUserManual id="AdmUserManual" runat="server" />
</asp:Content>
