﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_admUploadInventory0101 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 0;
    protected int _invUploadId = 0;
    protected int _mode = 1;
    protected int _formSect = 1;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int mode
    {
        get { return _mode; }
        set { _mode = value; }
    }

    public int invUploadId
    {
        get { return _invUploadId; }
        set { _invUploadId = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMVENDORMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            Master.sectId = sectId;
            //Master.subSectId = clsMis.getSubSectId(sectId, formSect, 1);
            Master.parentId = sectId;
            Master.showSideMenu = false;
        }

        setPageProperties();

        Session["NEWMEMID"] = null;
        Session["NOCHANGE"] = null;
        Session["ERRMSG"] = null;
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
 
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleAdd.Text").ToString(), true);
        litPageTitle.Text = GetLocalResourceObject("litPageTitleAdd.Text").ToString();       

        if (!IsPostBack)
        {
            if (Session["NEWMEMID"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";

                //clsVendor vendor = new clsVendor();
                //vendor.extractMemberById2(memId, 0);
                //clsMis.addAuditTrail(int.Parse(Session["ADMID"].ToString()), GetGlobalResourceObject("GlobalResource", "admAddVendor.Text").ToString() + " (" + vendor.vendorCode + ")");
            }
            else if (Session["ERRMSG"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
            }
        }

        ucAdmUploadInventory.mode = mode;
        ucAdmUploadInventory.invUploadId = invUploadId;
    }
    #endregion
}
