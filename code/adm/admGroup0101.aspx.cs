﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

public partial class adm_admGroup0101 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 13; //need change sectId according to db
    protected int _grpingId = clsAdmin.CONSTLISTGROUPINGCAT;
    protected string _grpingName = clsAdmin.CONSTLISRGROUPINGCATNAME;
    protected string grping = clsAdmin.CONSTLISTGROUPINGGROUP;
    protected int _mode = 1;
    protected int _formSect = 1;
    protected int _groupId = 0;
    protected int _groupParentChild;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    public string strSavedMsg
    {
        get { return String.Format(GetGlobalResourceObject("GlobalResource", "admSavedMessageWithParams.Text").ToString(), grpingName.ToUpper()); }
    }
    protected int grpingId
    {
        get
        {
            if ((Session["GRPINGID"] == null) || (Session["GRPINGID"] == "")) { return _grpingId; }
            else { return Convert.ToInt16(Session["GRPINGID"]); }
        }
        set { Session["GRPINGID"] = value; }
    }

    public string grpingName
    {
        get { return Session["GRPINGNAME"] as string ?? _grpingName; }
        set { Session["GRPINGNAME"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set
        {
            ViewState["FORMSECT"] = value;
        }
    }

    public int groupId
    {
        get
        {
            if (ViewState["groupId"] == null)
            {
                return _groupId;
            }
            else
            {
                return int.Parse(ViewState["groupId"].ToString());
            }
        }
        set { ViewState["groupId"] = value; }
    }

    public int groupParentChild
    {
        get { return Convert.ToInt16(Session[clsAdmin.CONSTPROJECTGROUPPARENTCHILD]); }
        set { Session["GROUPPARENTCHILD"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMCATEGORYMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;
                if (int.TryParse(Request["id"], out intTryParse))
                {
                    groupId = intTryParse;
                }
                else
                {
                    groupId = 0;
                }
            }
            else if (Session["NEWGRPID"] != null)
            {
                try
                {
                    groupId = int.Parse(Session["NEWGRPID"].ToString());
                }
                catch (Exception ex)
                {
                    groupId = 0;
                }
            }
            else if (Session["EDITGRPID"] != null)
            {
                try
                {
                    groupId = int.Parse(Session["EDITGRPID"].ToString());
                }
                catch (Exception ex)
                {
                    groupId = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["frm"]))
            {
                int intTryParse;
                if (int.TryParse(Request["frm"], out intTryParse))
                {
                    formSect = intTryParse;
                }
                else
                {
                    formSect = 1;
                }
            }

            if ((!string.IsNullOrEmpty(Request["id"])) || (Session["EDITGRPID"] != null))
            {
                mode = 2;
            }

            Master.sectId = sectId;
            Master.subSectId = clsMis.getSubSectId(sectId, formSect, 1);
            Master.parentId = sectId;
            Master.showSideMenu = true;
            Master.id = groupId;

            if (mode == 2) { Master.moduleDesc = GetLocalResourceObject("litPageInfoEdit.Text").ToString(); }
            else { Master.moduleDesc = GetLocalResourceObject("litPageInfoAdd.Text").ToString(); }
        }

        setPageProperties();
        Session["NEWGRPID"] = null;
        Session["EDITGRPID"] = null;
        Session["NOCHANGE"] = null;
        Session["DELETEDGRPNAME"] = null;
        Session["MOVEDPRODNO"] = 0;
        Session["ERRMSG"] = null;
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        switch (mode)
        {
            case 1:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleAdd.Text").ToString(), true);
                break;
            case 2:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleEdit.Text").ToString(), true);
                break;
            default:
                break;
        }

        if (!IsPostBack)
        {
            switch (mode)
            {
                case 1:
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleAdd.Text").ToString() + grpingName.ToUpper();

                    clsGroup grp = new clsGroup();
                    int intTotal = grp.getTotalRecord(0, 0);
                    if (clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITCATEGORY, intTotal)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "adm/admGroup01.aspx"); }
                    break;
                case 2:
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleEdit.Text").ToString() + grpingName.ToUpper();
                    break;
                default:
                    break;
            }

            if (Session["NEWGRPID"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + strSavedMsg + "</div>";

                clsMis.resetListing();
            }
            else if (Session["EDITGRPID"] != null)
            {
                if (Session["NOCHANGE"] != null)
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
                }
                else
                {
                    int intTotal = new clsGroup().getTotalRecord(0, 0);
                    string strUrlFormpage = "<a href='admGroup0101.aspx'>Add New</a>";
                    string strUrlListpage = "<a href='admGroup01.aspx'>Back to Listing</a>";

                    litAck.Text = "<div class=\"noticemsg\">" + strSavedMsg + "  " + strUrlListpage + "";
                    if (!clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITCATEGORY, intTotal - 1))
                    {
                        litAck.Text += " & " + strUrlFormpage + "";
                    }
                    litAck.Text += "</div>";

                    clsMis.resetListing();
                }

                pnlAck.Visible = true;
            }
            else if (Session["DELETEDGRPNAME"] != null)
            {
                litAck.Text = grpingName + " (" + Session["DELETEDGRPNAME"] + ") has been deleted successfully.";

                if (groupParentChild == 0)
                {
                    clsGroup grp = new clsGroup();
                    grp.grpingId = grpingId;
                    grp.extractGrpById(grp.otherGrpId, 0);
                    if (Convert.ToInt16(Session["MOVEPRODNO"]) > 1)
                    {
                        litAck.Text = litAck.Text + "<br />" + Session["MOVEDPRODNO"] + " products have been moved to \"" + grp.grpName + "\".";
                    }
                    else
                    {
                        litAck.Text = litAck.Text + "<br />" + Session["MOVEDPRODNO"] + " product has been moved to \"" + grp.grpName + "\".";
                    }
                }

                litAck.Text = "<div class=\"noticemsg\">" + litAck.Text + "</div>";

                pnlAck.Visible = true;

                clsMis.resetListing();
            }
            else if (Session["ERRMSG"] != null)
            {
                pnlAck.Visible = true;
                //litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
                litAck.Text = "<div class=\"errmsg\">" + Session["ERRMSG"] + "</div>";
            }
        }

        switch (formSect)
        {
            case 1:
                ucAdmGroupDetails.mode = mode;
                ucAdmGroupDetails.groupId = groupId;
                ucAdmGroupDetails.grpingId = grpingId;
                ucAdmGroupDetails.grpingName = grpingName;
                ucAdmGroupDetails.fill();

                pnlFormSect1.Visible = true;
                pnlFormSect2.Visible = false;
                break;
            case 2:
                ucAdmGroupDesc.mode = mode;
                ucAdmGroupDesc.grpId = groupId;
                ucAdmGroupDesc.grpingId = grpingId;
                ucAdmGroupDesc.grpingName = grpingName;
                ucAdmGroupDesc.fill();

                pnlFormSect2.Visible = true;
                pnlFormSect1.Visible = false;
                break;
            default:
                break;
        }

        //ucAdmGroupDetails.mode = mode;
        //ucAdmGroupDetails.groupId = groupId;
        //ucAdmGroupDetails.grpingId = grpingId;
        //ucAdmGroupDetails.grpingName = grpingName;
        //ucAdmGroupDetails.fill();
    }
    #endregion
}
