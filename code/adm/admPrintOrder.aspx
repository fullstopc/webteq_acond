﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admPrintOrder.aspx.cs" Inherits="adm_admPrintOrder" %>
<%@ MasterType VirtualPath="~/mst/blank.master" %>
<%@ Register Src="~/ctrl/ucAdmOrderItems.ascx" TagName="AdmOrderItems" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
<script type="text/javascript">

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    
    <div class="divPopUpOuter">
        <div class="divPopUpBar" runat="server" id="divPopUpBar">
            <div class="divPopUpBarInner">
                <div class="divLogo divPopUpBarSubBlock" id="divLogo" runat="server" visible="false">
                    <asp:HyperLink ID="hypLogo" runat="server">
                        <asp:Image ID="imgCompanyLogo" runat="server" CssClass="imgLogo" BorderWidth="0" /> 
                    </asp:HyperLink>     
                </div>
                <div class="divPrint divPopUpBarSubBlock" visible="false" runat="server">
                    <asp:LinkButton id="lnkbtnTopPrint" CssClass="lnkbtn" runat="server" meta:resourcekey="print"></asp:LinkButton>
                </div>  
                <div class="divPageInfo divPopUpBarSubBlock">
                    <div class="divCompanyInfo">
                        <table>
                            <tr>
                                <td>
                                    <div class="divPageTitle" id="divPageTitle" runat="server" visible="false"><asp:Literal ID="litPageTitle" runat="server"></asp:Literal></div>
                                    <div class="divRegNo">
                                        <div class="divCompanyRegNo" id="divCompanyRegNo" runat="server" visible="false">
                                        Co. No: <asp:Literal ID="litCompanyRegNo" runat="server"></asp:Literal>
                                        </div>
                                        <div class="divSplitter" id="divRegNoSplitter" runat="server" visible="false">|</div>
                                        <div class="divGstRegNo" id="divGstRegNo" runat="server" visible="false">
                                        GST Reg. No: <asp:Literal ID="litGstRegNo" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="divCompanyAddr">
                                        <div class="divAddr1" id="divAddr1" runat="server" visible="false"><asp:Literal ID="litAddr1" runat="server"></asp:Literal></div>
                                        <div class="divAddr2" id="divAddr2" runat="server" visible="false"><asp:Literal ID="litAddr2" runat="server"></asp:Literal></div>  
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="divDomainName" id="divDomainName" runat="server" visible="false"><asp:Literal ID="litDomainName" runat="server"></asp:Literal></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="divQuickContact">
                        <table>
                            <tr ID="trQuickContactTel" runat="server">
                                <td runat="server" visible="false" ID="tdImgTel"><asp:Image ID="imgTel" CssClass="imgTel" runat="server"/></td>
                                <td runat="server" visible="false" ID="tdDivTel"><div class="divTel"><asp:Literal ID="litQuickContactTel" meta:resourcekey="litQuickContactTel" runat="server"></asp:Literal></div></td>
                                <td runat="server" visible="false" ID="tdImgFax"><asp:Image ID="imgFax" CssClass="imgFax" runat="server"/></td>
                                <td runat="server" visible="false" ID="tdDivFax"><div class="divFax"><asp:Literal ID="litQuickContactFax" meta:resourcekey="litQuickContactFax" runat="server"></asp:Literal></div></td>
                                <td runat="server" visible="false" ID="tdImgEmail" ><asp:Image ID="imgEmail" CssClass="imgEmail" runat="server"/></td>
                                <td runat="server" visible="false" ID="tdDivEmail" ><div class="divEmail"><asp:Literal ID="litQuickContactEmail" meta:resourcekey="litQuickContactEmail" runat="server"></asp:Literal></div></td>
                            </tr>
                        </table>
                    </div> 
                </div> 
                        
            <%--<span class="hdrTitle"><asp:Literal ID="litPageTitle" runat="server"></asp:Literal></span>--%>
            </div>
        </div>
        <div class="divPopUpContent">
           <asp:Panel ID="pnlListing" runat="server" CssClass="divPopUpContentInner"> 
                <asp:Panel ID="pnlAck" runat="server" CssClass="divAckPopUp">
                    <asp:Literal ID="litAck" runat="server"></asp:Literal>
                </asp:Panel>
                <div class="divColumnLeft">
                    <div class="divListingDetail">
                        <div class="divSubListingDetail">
                            <div class="divDetailHdr"><asp:Label ID="lblBilling" runat="server" meta:resourcekey="lblBilling"></asp:Label></div>
                            <div class="divDetailTbl">
                                <table cellpadding="0" cellspacing="0" border="0" class="frmTblDelivery">
                                    <tr id="trBillCompanyName" runat="server" visible="false">
                                        <td class="tdLabelNor2 nobr">Company Name:</td>
                                        <td class="tdMax"><asp:Literal ID="litBillCompanyName" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td><asp:Label ID="lblNameBilling" runat="server" meta:resourcekey="lblName"></asp:Label></td>
                                        <td><asp:Literal ID="litBillName" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td><asp:Label ID="lblContactNoBilling" runat="server" meta:resourcekey="lblContactNo"></asp:Label></td>
                                        <td><asp:Literal ID="litBillContactNo" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr id="trBillEmail" runat="server" visible="false">
                                        <td class="tdLabelNor2">Email:</td>
                                        <td><asp:Literal ID="litBillEmail" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><asp:Label ID="lblAddBilling" runat="server" meta:resourcekey="lblAdd"></asp:Label></td>
                                        <td>
                                            <asp:Literal ID="litBillAdd" runat="server"></asp:Literal>,<br>
                                            <asp:Literal ID="litBillPoscode" runat="server"></asp:Literal>,
                                            <asp:Literal ID="litBillCity" runat="server"></asp:Literal>,<br>
                                            <asp:Literal ID="litBillState" runat="server"></asp:Literal>,
                                            <asp:Literal ID="litBillCountry" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="divSubListingDetail">
                            <div class="divDetailHdr">
                                <span class=""><asp:Label ID="lblShipping" runat="server" meta:resourcekey="lblShipping"></asp:Label></span>
                                <span class=""><asp:Label ID="lblPickupPoint" runat="server" meta:resourcekey="lblPickupPoint" Visible="false"></asp:Label></span>
                            </div>
                            <div class="divDetailTbl">
                                <table cellpadding="0" cellspacing="0" border="0" class="frmTblDelivery">
                                    <tr id="trCompanyName" runat="server" visible="false">
                                        <td class="tdLabelNor2 nobr">Company Name:</td>
                                        <td class="tdMax"><asp:Literal ID="litCompanyName" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td><asp:Label ID="lblNameShipping" runat="server" meta:resourcekey="lblName"></asp:Label></td>
                                        <td><asp:Literal ID="litName" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td><asp:Label ID="lblContactNoShipping" runat="server" meta:resourcekey="lblContactNo"></asp:Label></td>
                                        <td><asp:Literal ID="litContactNo" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr id="trEmail" runat="server" visible="false">
                                        <td class="tdLabelNor2">Email:</td>
                                        <td><asp:Literal ID="litEmail" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><asp:Label ID="lblAddShipping" runat="server" meta:resourcekey="lblAdd"></asp:Label></td>
                                        <td>
                                            <asp:Literal ID="litAddress" runat="server"></asp:Literal>
                                            <asp:Literal ID="litPoscode" runat="server"></asp:Literal>
                                            <asp:Literal ID="litCity" runat="server"></asp:Literal>
                                            <asp:Literal ID="litState" runat="server"></asp:Literal>
                                            <asp:Literal ID="litCountry" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="divSubListingDetail">
                            <div class="divDetailHdr">
                                <span class=""><asp:Label ID="lblDeliveryInfo" runat="server" meta:resourcekey="lblDeliveryInfo"></asp:Label></span>
                                <span class=""></span>
                            </div>
                            <div class="divDetailTbl">
                                <table cellpadding="0" cellspacing="0" border="0" class="frmTblDelivery">
                                    <tr>
                                        <td><asp:Label ID="lblDeliveryDate" runat="server" meta:resourcekey="lblDeliveryDate"></asp:Label></td>
                                        <td><asp:Literal ID="litDeliveryDate" runat="server"></asp:Literal></td>
                                    </tr>                                    
                                    <tr>
                                        <td><asp:Label ID="lblDeliveryTime" runat="server" meta:resourcekey="lblDeliveryTime"></asp:Label></td>
                                        <td><asp:Literal ID="litDeliveryTime" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td><asp:Label ID="lblDeliveryRecipientName" runat="server" meta:resourcekey="lblDeliveryRecipientName"></asp:Label></td>
                                        <td><asp:Literal ID="litDeliveryRecipientName" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td><asp:Label ID="lblDeliverySenderName" runat="server" meta:resourcekey="lblDeliverySenderName"></asp:Label></td>
                                        <td><asp:Literal ID="litDeliverySenderName" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td><asp:Label ID="lblDeliveryMsg" runat="server" meta:resourcekey="lblDeliveryMsg"></asp:Label></td>
                                        <td><asp:Literal ID="litDeliveryMsg" runat="server"></asp:Literal></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divColumnMiddle"></div>
                <div class="divColumnRight">
                    <asp:Panel ID="pnlSummaryOuter" runat="server" CssClass="divSummaryOuter">
                        <asp:Panel ID="pnlOrdSummary" runat="server" CssClass="divOrdListingSummary">
                            <table cellpadding="0" cellspacing="0" border="0" class="frmTblDelivery">
                                <tr>
                                    <td class="">Sales No.:</td>
                                    <td class="boldmsg"><asp:Literal ID="litOrdNo" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="">Sales Date:</td>
                                    <td><asp:Literal ID="litOrdDate" runat="server"></asp:Literal></td>
                                </tr>
                                <tr id="trType" runat="server" visible="false">
                                    <td class="">Sales Type:</td>
                                    <td><asp:Literal ID="litOrdType" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="">Member:</td>
                                    <td><asp:Literal ID="litMember" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class=" nobr">Sales Amount:</td>
                                    <td><asp:Literal ID="litOrdAmount" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="">Message to seller:</td>
                                    <td><asp:Literal ID="litMsgBuyer" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="">Remarks:</td>
                                    <td><asp:Literal ID="litRemarks" runat="server"></asp:Literal></td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pnlPaymentSummary" runat="server" CssClass="divPaymentSummary">
                            <table cellpadding="0" cellspacing="0" border="0" class="frmTblDelivery">
                                <tr>
                                    <td class="">Payment Gateway:</td>
                                    <td class=""><asp:Literal ID="litPayGateway" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="">Payment Remark:</td>
                                    <td><asp:Literal ID="litPayRemark" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="">Payment Date:</td>
                                    <td><asp:Literal ID="litPayDate" runat="server"></asp:Literal></td>
                                </tr>
                                <tr id="trBank" runat="server" visible="false">
                                    <td class="">Bank:</td>
                                    <td><asp:Literal ID="litBank" runat="server"></asp:Literal></td>
                                </tr>
                                <tr id="trAccount" runat="server" visible="false">
                                    <td class=" nobr">Bank Account No.:</td>
                                    <td><asp:Literal ID="litAccount" runat="server"></asp:Literal></td>
                                </tr>
                                <tr id="trTransId" runat="server" visible="false">
                                    <td class="">Trans. ID:</td>
                                    <td><asp:Literal ID="litTransId" runat="server"></asp:Literal></td>
                                </tr>
                                <tr id="trPayDate" runat="server" visible="false">
                                    <td class="tdLabelNor2">Payment Date:</td>
                                    <td><asp:Literal ID="litPayDate2" runat="server"></asp:Literal></td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pnlOrderDeliveryMessage" runat="server" CssClass="divBillingDetails" Visible="false">
                            <table cellpadding="0" cellspacing="0" border="0" class="formTbl">
                                <tr>
                                    <td class="tdSpacer">&nbsp;</td>
                                    <td class="tdSpacer">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="boldmsg">Delivery Message Details</td>
                                </tr>
                                <tr>
                                    <td id="td1" runat="server" class="tdLabel nobr">Message From:</td>
                                    <td id="td2" runat="server" class="tdLabel tdMax2">
                                        <asp:Literal ID="litMsgFrom" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="td3" runat="server" class="tdLabel nobr">Message To:</td>
                                    <td id="td4" runat="server" class="tdLabel tdMax2">
                                        <asp:Literal ID="litMsgTo" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="td5" runat="server" class="tdLabel nobr">Message Content:</td>
                                    <td id="td6" runat="server" class="tdLabel tdMax2">
                                        <asp:Literal ID="litMsgContent" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="td7" runat="server" class="tdLabel nobr">Special Instruction:</td>
                                    <td id="td8" runat="server" class="tdLabel tdMax2">
                                        <asp:Literal ID="litSpecialInstruction" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </asp:Panel>
                </div>
                <div class="clear"></div>
                <asp:Panel ID="pnlListingItem" runat="server">
                    <div class="divListingHdr">
                        <asp:Label ID="lblItemDetails" runat="server" meta:resourcekey="lblItemDetails"></asp:Label>
                    </div>
                    <div class="divListingItem">
                        <uc:AdmOrderItems ID="ucAdmOrderItems" runat="server" boolShowAction="false" />
                    </div>
                </asp:Panel>
           </asp:Panel>
        </div>
        <div class="divPopUpFooter">
            <div class="divPopUpFooterInner">
                <div class="divRight">
                    <div class="divPrint" runat="server" visible="false">
                        <asp:LinkButton id="lnkbtnBtmPrint" CssClass="lnkbtn" runat="server" meta:resourcekey="print"></asp:LinkButton>
                    </div>
                </div>
                <div class="divLeft">
                    <div class="divLine">
                        <div class="divLine1" id="divLine1" runat="server" visible="false"><asp:Literal ID="litLine1" runat="server"></asp:Literal></div>
                        <div class="divLine2" id="divLine2" runat="server" visible="false"><asp:Literal ID="litLine2" runat="server"></asp:Literal></div>
                        <div class="divLine3" id="divLine3" runat="server" visible="false"><asp:Literal ID="litLine3" runat="server"></asp:Literal></div>
                    </div>
                </div>
                 
            </div>
        </div>
    </div>
</asp:Content>