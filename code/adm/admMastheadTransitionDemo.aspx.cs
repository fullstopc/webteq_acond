﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Linq;
public partial class adm_admMastheadTransitionDemo : System.Web.UI.Page
{
    #region "Properties"
    protected int _slideGrpId = 0;
    protected string _slideOption = "";

    protected string jssorOption = "";
    protected string supersizedOption = "";
    protected string cycle2Option = "";
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int slideGroupId
    {
        get { return _slideGrpId; }
        set { _slideGrpId = value; }
    }
    public string slideOption
    {
        get { return _slideOption; }
        set { _slideOption = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    slideGroupId = int.Parse(Request["id"]);
                }
                catch (Exception ex)
                {
                    slideGroupId = 0;
                }
            }
            if (!string.IsNullOrEmpty(Request["option"]))
            {
                try
                {
                    slideOption = Request["option"].ToString();
                }
                catch (Exception ex)
                {
                    slideOption = "";
                }
            }
        }

        setPageProperties();

        clsMasthead mst = new clsMasthead();
        bool boolMastheadFound = mst.extractGrpById(slideGroupId, 1);
        if (boolMastheadFound)
        {
            string sliderType = mst.grpSliderType;
            switch (sliderType)
            {
                case clsAdmin.CONSTLISTVALUE_DEFAULT:
                    {
                        transDefault.Visible = true;
                        initDefault();

                    }
                    break;

                case clsAdmin.CONSTLISTVALUE_FULLSCREEN:
                    {
                        fullscreen.Visible = true;
                        initFullscreen();
                    }
                    break;

                case clsAdmin.CONSTLISTVALUE_MOBILE:
                    {
                        mobile.Visible = true;
                        initMobile();
                    }
                    break;
            }
        }
    }

    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        Page.Title += clsMis.formatPageTitle("Masthead Transition Demo", true);


    }

    #endregion
    #region "Default"
    protected void rptMasthead_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strMastheadTitle = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_NAME").ToString();
            string strMastheadId = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_ID").ToString();
            string strMastheadImage = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_IMAGE").ToString();
            string strMastTagline1 = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_TAGLINE1").ToString();
            string strMastTagline2 = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_TAGLINE2").ToString();
            int intMastheadClick = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "MASTHEAD_CLICKABLE"));
            string strMastheadUrl = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_URL").ToString();
            string strMastheadTarget = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_TARGET").ToString();
            string strMastheadImageUrl = Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + strMastheadImage + "";


            strMastheadImage = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + strMastheadImage + "";
            System.Drawing.Image img = System.Drawing.Image.FromFile(strMastheadImage);

            int intHeight = img.Height;
            int intWidth = img.Width;
            img.Dispose();

            Image imgMasthead = (Image)e.Item.FindControl("imgMasthead");
            Image imgMastheadImage = (Image)e.Item.FindControl("imgMastheadImage");

            imgMasthead.ImageUrl = ConfigurationManager.AppSettings["imgBase"] + "cmn/trans.gif";
            imgMasthead.ImageUrl = strMastheadImageUrl;
            imgMasthead.AlternateText = strMastheadTitle;
            imgMasthead.ToolTip = strMastheadTitle;
            imgMasthead.Width = intWidth;
            imgMasthead.Height = intHeight;

            imgMastheadImage.ImageUrl = ConfigurationManager.AppSettings["imgBase"] + "cmn/trans.gif";
            imgMastheadImage.ImageUrl = strMastheadImageUrl;
            imgMastheadImage.AlternateText = strMastheadTitle;
            imgMastheadImage.ToolTip = strMastheadTitle;
            imgMastheadImage.Width = intWidth;
            imgMastheadImage.Height = intHeight;
            

            HyperLink hypMasthead = (HyperLink)e.Item.FindControl("hypMasthead");

            if (intMastheadClick > 0)
            {
                imgMasthead.Visible = false;

                hypMasthead.Visible = true;
                hypMasthead.ToolTip = strMastheadTitle;
                hypMasthead.Target = strMastheadTarget;

                if (strMastheadUrl.StartsWith("https://") || strMastheadUrl.StartsWith("http://"))
                {
                    hypMasthead.NavigateUrl = strMastheadUrl;
                }
                else
                {
                    hypMasthead.NavigateUrl = ConfigurationManager.AppSettings["fullBase"] + ConfigurationManager.AppSettings["scriptBase"] + "usr/" + strMastheadUrl;
                }
            }
            Literal litMastTagline1 = (Literal)e.Item.FindControl("litMastTagline1");
            litMastTagline1.Text = strMastTagline1;

            Literal litMastTagline2 = (Literal)e.Item.FindControl("litMastTagline2");
            litMastTagline2.Text = strMastTagline2;
        }
    }
    protected void initDefault()
    {
        clsMasthead masthead = new clsMasthead();

        DataSet dsMasthead = masthead.getMastheadImage2();
        DataView dvMasthead = new DataView(dsMasthead.Tables[0]);

        dvMasthead.RowFilter = "MASTHEAD_ACTIVE = 1 AND GRP_ID = " + slideGroupId;
        dvMasthead.Sort = "MASTHEAD_ORDER ASC, MASTHEAD_NAME ASC";

        rptMasthead.DataSource = dvMasthead;
        rptMasthead.DataBind();

        clsMasthead mst = new clsMasthead();
        if (mst.extractGrpById(slideGroupId, 1))
        {
            if (mst.grpSliderType == clsAdmin.CONSTLISTVALUE_DEFAULT)
            {
                cycle2Option = slideOption;
                string[] strOptions = cycle2Option.Split(',')
                                                         .Where(x => !string.IsNullOrEmpty(x))
                                                         .ToArray();

                foreach (string strOption in strOptions)
                {
                    string[] strKeyAndValue = strOption.Split(':');
                    string strKey = strKeyAndValue[0];
                    string strValue = strKeyAndValue[1];

                    pnlMastheadSlideShow.Attributes[strKey] = strValue;
                }
                pnlMastheadSlideShow.Attributes["data-cycle-slides"] = "> div";
                pnlMastheadSlideShow.CssClass += " cycle-slideshow";
            }
        }
    }
    #endregion
    #region "Fullscreen"
    protected void initFullscreen()
    {
        clsMasthead mst = new clsMasthead();

        Literal litJS = new Literal();
        litJS.Text = "<script src=" + ConfigurationManager.AppSettings["jsBase"] + "supersized.3.2.6.min.js type=text/javascript />";
        litJS.Text += "<script src=" + ConfigurationManager.AppSettings["jsBase"] + "supersized.shutter.min.js type=text/javascript />";
        this.Page.Header.Controls.Add(litJS);

        if (mst.extractGrpById(slideGroupId, 1))
        {
            //string strSliderOption = "";
            //if (mst.extractGrpById(slideGroupId, 1))
            //{
            //    if (mst.grpSliderType == clsAdmin.CONSTLISTVALUE_FULLSCREEN)
            //    {
            //        supersizedOption += mst.grpSliderOption;
            //    }
            //}
            supersizedOption = slideOption;

            DataSet ds = new DataSet();
            ds = mst.getMastheadImageFromGoupID(slideGroupId);

            //ds.Tables[0].DefaultView.Sort = "MASTHEAD_ORDER ASC, MASTHEAD_NAME ASC";

            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = " MASTHEAD_ORDER ASC";

            int intCounter = 0;
            supersizedOption += "slides: [";
            foreach (DataRow row in dv.ToTable().Rows)
            {
                intCounter += 1;
                if (intCounter == ds.Tables[0].Rows.Count)
                {
                    supersizedOption += "{image: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + row["MASTHEAD_IMAGE"].ToString() + "', " +
                    " description  : '" + row["MASTHEAD_TAGLINE1"].ToString().Replace("\n", "").Replace("\r", "") + "'}";
                }
                else
                {
                    supersizedOption += "{image: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + row["MASTHEAD_IMAGE"].ToString() + "', " +
                    " description  : '" + row["MASTHEAD_TAGLINE1"].ToString().Replace("\n", "").Replace("\r", "") + "'},";
                }
            }
            supersizedOption += "],";
        }
    }
    #endregion
    #region "Mobile"
    protected void rptHomeMasthead_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string strMastheadTitle = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_NAME").ToString();
            string strMastheadId = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_ID").ToString();
            string strMastheadImage = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_IMAGE").ToString();
            int intMastheadClick = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "MASTHEAD_CLICKABLE"));
            string strMastheadUrl = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_URL").ToString();
            string strMastheadTarget = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_TARGET").ToString();
            string strtagline1 = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_TAGLINE1").ToString();
            string strtagline2 = DataBinder.Eval(e.Item.DataItem, "MASTHEAD_TAGLINE2").ToString();
            string strMastheadImageUrl = Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + strMastheadImage + "";

            strMastheadImage = Session[clsAdmin.CONSTPROJECTUPLOADPATH] + ConfigurationManager.AppSettings["uplBase2"] + clsAdmin.CONSTMASTHEADFOLDER + "/" + strMastheadImage + "";
            System.Drawing.Image img = System.Drawing.Image.FromFile(strMastheadImage);
            int intHeight = img.Height;
            int intWidth = img.Width;
            img.Dispose();

            Image imgMasthead = (Image)e.Item.FindControl("imgMasthead");
            Image imgMastheadImage = (Image)e.Item.FindControl("imgMastheadImage");

            imgMasthead.ImageUrl = ConfigurationManager.AppSettings["imgBase"] + "cmn/trans.gif";
            imgMasthead.ImageUrl = strMastheadImageUrl;
            imgMasthead.AlternateText = strMastheadTitle;
            imgMasthead.ToolTip = strMastheadTitle;
            //imgMasthead.Width = intWidth;
            //imgMasthead.Height = intHeight;
            pnlSliderContainer.Width = intWidth - 1;
            pnlSliderContainer.Height = intHeight - 1;

            pnlMastheadSlider.Width = intWidth - 1;
            pnlMastheadSlider.Height = intHeight - 1;



            Literal Littag1 = (Literal)e.Item.FindControl("Littag1");
            Littag1.Text = strtagline1;
            Literal Littag2 = (Literal)e.Item.FindControl("Littag2");
            Littag2.Text = strtagline2;

            HyperLink hypMasthead = (HyperLink)e.Item.FindControl("hypMasthead");
            if (intMastheadClick > 0)
            {
                hypMasthead.Enabled = true;
                hypMasthead.ToolTip = strMastheadTitle;
                hypMasthead.Target = strMastheadTarget;
            }
        }
    }

    protected void initMobile()
    {
        clsMasthead masthead = new clsMasthead();
        DataSet dsMasthead = masthead.getMastheadImage();
        DataView dvMasthead = new DataView(dsMasthead.Tables[0]);

        dvMasthead.RowFilter = "MASTHEAD_ACTIVE = 1 AND GRP_ID = " + slideGroupId;
        dvMasthead.Sort = "MASTHEAD_NAME ASC";

        rptHomeMasthead.DataSource = dvMasthead;
        rptHomeMasthead.DataBind();

        // call slider transition
        jssorOption = slideOption;

        //clsMasthead mst = new clsMasthead();
        //if (mst.extractGrpById(slideGroupId, 1))
        //{
        //    if (mst.grpSliderType == clsAdmin.CONSTLISTVALUE_MOBILE)
        //    {
        //        jssorOption = mst.grpSliderOption;
        //    }
        //}
    }

    #endregion
}
