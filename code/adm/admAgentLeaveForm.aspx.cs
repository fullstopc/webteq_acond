﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_adm : System.Web.UI.Page
{
    #region "Properties"
    protected int _mode = 1;
    protected int _leaveID = 0;
    protected int _employeeID = 0;
    #endregion

    #region "Property Methods"
    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    public int leaveID
    {
        get
        {
            if (ViewState["LEAVE_ID"] == null)
            {
                return _leaveID;
            }
            else
            {
                return int.Parse(ViewState["LEAVE_ID"].ToString());
            }
        }
        set
        {
            ViewState["LEAVE_ID"] = value;
        }
    }
    public int employeeID
    {
        get
        {
            if (ViewState["EMPLOYEE_ID"] == null)
            {
                return _employeeID;
            }
            else
            {
                return int.Parse(ViewState["EMPLOYEE_ID"].ToString());
            }
        }
        set
        {
            ViewState["EMPLOYEE_ID"] = value;
        }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            leaveID = Request["id"].ToInteger();
            employeeID = Request["emp_id"].ToInteger();

            ddlAgent.DataSource = new clsEmployee().getDataTable();
            ddlAgent.DataValueField = "employee_id";
            ddlAgent.DataTextField = "employee_name";
            ddlAgent.DataBind();
            ddlAgent.AddDefault();

            if (leaveID > 0)
            {
                mode = 2;
            }

            if(employeeID > 0)
            {
                ddlAgent.SelectedValue = employeeID.ToString();
                ddlAgent.Enabled = false;
            }

            if (mode == 2)
            {
                fillform();
            }

        }

    }
    #endregion

    #region "Methods"
    private void fillform()
    {
        clsEmployeeLeave leave = new clsEmployeeLeave();
        if (leave.extractByID(leaveID))
        {
            txtDateFrom.Text = leave.start.ToReadableDate();
            txtDateTo.Text = leave.end.ToReadableDate();
            txtReason.Text = leave.reason;
        }
    }
    #endregion

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid && employeeID > 0)
        {

            int intAdmID = Session["ADMID"].ToInteger();

            clsEmployeeLeave leave = new clsEmployeeLeave();
            leave.type = 1;
            leave.employeeID = employeeID;
            leave.start = txtDateFrom.Text.ToDateTime().ToEarliestTime();
            leave.end = txtDateTo.Text.ToDateTime().ToLatestTime();
            leave.reason = txtReason.Text.Trim();
            leave.ID = mode == 2 ? leaveID : -1;
            leave.createdby = intAdmID;
            leave.updatedby = intAdmID;

            string msg = "";
            bool Success = true;
            bool Updated = false;

            if (mode == 1)
            {
                if (leave.add())
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();
                }
            }
            else if (mode == 2)
            {
                if (!leave.isSame())
                {
                    if (leave.update())
                    {
                        Updated = true;
                        msg = GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString();
                    }
                    else
                    {
                        Success = false;
                    }
                }

                if (Success && !Updated)
                {
                    msg = GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString();
                }
            }

            if (msg == "")
            {
                msg = GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString();
            }

            Page.ClientScript.RegisterStartupScript(GetType(), "PopupClose", "parent.propertySaveSuccess('" + msg + "')", true);
        }
    }
}