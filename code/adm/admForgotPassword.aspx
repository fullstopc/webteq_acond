﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private_nomenu.master" AutoEventWireup="true" CodeFile="admForgotPassword.aspx.cs" Inherits="adm_admForgotPassword" %>
<%@ MasterType VirtualPath="~/mst/private_nomenu.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div id="divForgotPasswordContainer">
        <asp:Panel ID="pnlAck" runat="server" CssClass="divAck2" Visible="false">
            <asp:Literal ID="litAck" runat="server"></asp:Literal>
            <asp:Panel ID="pnlAckBtn" runat="server" CssClass="divAckBtn2">
                <asp:LinkButton ID="imgbtnOk" runat="server" CssClass="btn right" meta:resourcekey="imgbtnOk" Text="ok"></asp:LinkButton>
                <asp:LinkButton ID="imgbtnBack" runat="server" CssClass="btn right" meta:resourcekey="imgbtnBack" Visible="false" Text="back"></asp:LinkButton>
            </asp:Panel>
        </asp:Panel> 
        <asp:Panel ID="pnlLoginForm" runat="server">  
            <table cellpadding="0" cellspacing="0" id="tblLogin">
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtEmail" runat="server" class="txtEmail" autocomplete="off" placeholder="Please enter your email that is registered." MaxLength="250"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="<br />Please enter your email that is registered." ControlToValidate="txtEmail" class="errmsg" Display="dynamic"></asp:RequiredFieldValidator>
                        <span class="fieldDesc"><asp:RegularExpressionValidator ID="revMail" ErrorMessage="<br />Please enter valid email." runat="server" ControlToValidate="txtEmail" Display="Dynamic" CssClass="errmsg" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></span>
                        <asp:Literal ID="litInvalidEmail" runat="server" Visible="false"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td class="tdLoginBtn2"><asp:LinkButton ID="lbtnFPwd" runat="server" Text="Reset and send me the password" ToolTip="Reset and send me the password" class="btn right" OnClick="lbtnFPwd_Click" /></td>
                    <td class="tdLoginBtn1"><asp:LinkButton ID="lnkbtnBack" runat="server" Text="Back" ToolTip="Back" class="btn btnBack right" CausesValidation="false"></asp:LinkButton></td>
                </tr>          
            </table>
        </asp:Panel>  
    </div>
</asp:Content>