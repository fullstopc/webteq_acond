﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/mst/private.master" CodeFile="admJob0101.aspx.cs" Inherits="adm_admJob0101" %>
<%@ MasterType VirtualPath="~/mst/private.Master" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmJobDetail.ascx" TagName="ucAdmJobDetail" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlForm" runat="server" CssClass="divForm">
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <uc:ucAdmJobDetail ID="ucAdmJobDetail1" runat="server" pageListingURL="admJob01.aspx" />
    </asp:Panel>
</asp:Content>