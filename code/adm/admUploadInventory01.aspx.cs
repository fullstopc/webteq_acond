﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.IO;
using System.Configuration;

public partial class adm_admUploadInventory01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 0;
    protected string _gvSortExpression = "INVUPLOAD_ID";
    protected string _gvSortDirection = clsAdmin.CONSTSORTASC;
    protected string _gvSort = "";

    protected string strKeyword;
    protected Boolean boolSearch = false;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    protected string gvSortExpression
    {
        get { return ViewState["GVSORTEXPRESSION"] as string ?? _gvSortExpression; }
        set { ViewState["GVSORTEXPRESSION"] = value; }
    }

    protected string gvSortDirection
    {
        get { return ViewState["GVSORTDIRECTION"] as string ?? _gvSortDirection; }
        set { ViewState["GVSORTDIRECTION"] = value; }
    }

    protected string gvSort
    {
        get { return ViewState["GVSORT"] as string ?? _gvSort; }
        set { ViewState["GVSORT"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMUPLOADINVENTORY, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            gvInventory.PagerSettings.FirstPageText = GetGlobalResourceObject("GlobalResource", "contentAdmFirstPageText.Text").ToString();
            gvInventory.PagerSettings.LastPageText = GetGlobalResourceObject("GlobalResource", "contentAdmLastPageText.Text").ToString();
            gvInventory.PageSize = clsAdmin.CONSTADMPAGESIZE;

            bindGV(gvInventory, "INVUPLOAD_ID ASC");
            //setPageProperties();
        }
    }

    protected void gvInventory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell tc in e.Row.Cells)
            {
                if (tc.HasControls())
                {
                    LinkButton lnkbtn = (LinkButton)tc.Controls[0];

                    if (lnkbtn != null && gvSortExpression == lnkbtn.CommandArgument)
                    {
                        string strClass = (gvSortDirection == "DESC") ? "desc" : "asc";
                        tc.CssClass = strClass;
                    }
                }
            }
        } 
    }

    protected void gvInventory_Sorting(object sender, GridViewSortEventArgs e)
    {
        string strSortExpDir = "";
        if (e.SortExpression != gvSortExpression)
        {
            gvSortExpression = e.SortExpression;
            gvSortDirection = clsAdmin.CONSTSORTASC;
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }
        else
        {
            switch (gvSortDirection)
            {
                case clsAdmin.CONSTSORTASC:
                    gvSortDirection = clsAdmin.CONSTSORTDESC;
                    break;
                case clsAdmin.CONSTSORTDESC:
                    gvSortDirection = clsAdmin.CONSTSORTASC;
                    break;
            }

            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }

        gvSort = strSortExpDir;
        bindGV(gvInventory, gvSort);
    }

    protected void gvInventory_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvInventory.PageIndex = e.NewPageIndex;
        bindGV(gvInventory, gvSort);
    }

    public override void VerifyRenderingInServerForm(Control control)
    {

    }
    #endregion

    #region "Methods"
    protected void bindGV(GridView gvInventoryRef, string gvSortExpDir)
    {
        clsInventory inv = new clsInventory();
        DataSet ds = new DataSet();
        ds = inv.getInventoryList();

        DataView dv = new DataView(ds.Tables[0]);
        dv.RowFilter = "1 = 1";

        if (gvSortExpDir != "")
        {
            dv.Sort = gvSortExpDir;
        }

        Session["INVDV"] = dv;

        gvInventoryRef.DataSource = dv;
        gvInventoryRef.DataBind();

        litItemFound.Text = dv.Count + " record(s) listed." ;
    }
    #endregion
}