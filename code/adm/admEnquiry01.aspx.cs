﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;

public partial class adm_admEnquiry01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 70;
    protected string _gvSortExpression = "ENQ_CREATION";
    protected string _gvSortDirection = clsAdmin.CONSTSORTDESC;
    protected string _gvSort = "";
    protected Boolean _boolSearch = false;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public string gvSort
    {
        get { return ViewState["GVSORT"] as string ?? _gvSort; }
        set { ViewState["GVSORT"] = value; }
    }

    public string gvSortDirection
    {
        get { return ViewState["GVSORTDIRECTION"] as string ?? _gvSortDirection; }
        set { ViewState["GVSORTDIRECTION"] = value; }
    }

    public string gvSortExpression
    {
        get { return ViewState["GVSORTEXPRESSION"] as string ?? _gvSortExpression; }
        set { ViewState["GVSORTEXPRESSION"] = value; }
    }

    public Boolean boolSearch
    {
        get
        {
            if (ViewState["BOOLSEARCH"] == null)
            {
                return _boolSearch;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLSEARCH"]);
            }
        }
        set { ViewState["BOOLSEARCH"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            Master.sectId = sectId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            //txtKeyword.Attributes.Add("onkeydown", "javascript:if(event.keyCode == 13){document.getElementById('" + lnkbtnGo.ClientID + "').click(); return false;}");

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMELISTINGSIZE, clsConfig.CONSTGROUPOTHERSETTINGS, 1);

            //gvItems.PagerSettings.FirstPageText = GetGlobalResourceObject("GlobalResource", "contentAdmFirstPageText.Text").ToString();
            //gvItems.PagerSettings.LastPageText = GetGlobalResourceObject("GlobalResource", "contentAdmLastPageText.Text").ToString();
            //gvItems.PageSize = (!string.IsNullOrEmpty(config.value)) ? Convert.ToInt16(config.value) : clsAdmin.CONSTADMPAGESIZE;
            //gvSort = gvSortExpression + " " + gvSortDirection;
            //bindGV(gvItems, "ENQ_CREATION DESC");

            setSearchProperties();

            if(Session["DATA_RESEND"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">Email resend successfully.</div>";
            }

        }
        Session["DATA_RESEND"] = null;
        Session["ERRMSG"] = null;
    }

    protected void gvItems_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvItems.PageIndex = e.NewPageIndex;
        bindGV(gvItems, gvSort);
    }

    protected void gvItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell tc in e.Row.Cells)
            {
                if (tc.HasControls())
                {
                    LinkButton lnkbtn = (LinkButton)tc.Controls[0];
                    //Image icon = new Image();
                    //icon.ImageUrl = "../img/adm/btn-cancel.gif";

                    if (lnkbtn != null && gvSortExpression == lnkbtn.CommandArgument)
                    {
                        //tc.Controls.Add(icon);
                        string strClass = (gvSortDirection == "DESC") ? "desc" : "asc";
                        tc.CssClass = strClass;
                    }


                }
            }
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //LinkButton lnkbtnView = (LinkButton)e.Row.FindControl("lnkbtnView");
            int intId = Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "ENQ_ID"));

            HyperLink hypView = (HyperLink)e.Row.FindControl("hypView");

            hypView.Text = GetGlobalResourceObject("GlobalResource", "contentAdmView.Text").ToString();
            hypView.NavigateUrl = "admEnquiry0101.aspx?id=" + intId;

            //lnkbtnView.Text = GetGlobalResourceObject("GlobalResource", "contentAdmView.Text").ToString();
        }
    }

    protected void gvItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "cmdView")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int itemId = int.Parse(gvItems.DataKeys[rowIndex].Value.ToString());

            Response.Redirect("admEnquiry0101.aspx?id=" + itemId);
        }
    }

    protected void lnkbtnGO_Click(object sender, EventArgs e)
    {
        boolSearch = true;

        bindGV(gvItems, "ENQ_CREATION ASC");
    }

    protected void gvItems_Sorting(object sender, GridViewSortEventArgs e)
    {
        string strSortExpDir = "";
        if (e.SortExpression != gvSortExpression)
        {
            gvSortExpression = e.SortExpression;
            gvSortDirection = clsAdmin.CONSTSORTASC;
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }
        else
        {
            switch (gvSortDirection)
            {
                case clsAdmin.CONSTSORTASC:
                    gvSortDirection = clsAdmin.CONSTSORTDESC;
                    break;
                case clsAdmin.CONSTSORTDESC:
                    gvSortDirection = clsAdmin.CONSTSORTASC;
                    break;
            }
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }

        gvSort = strSortExpDir;
        bindGV(gvItems, gvSort);
    }

    protected void lnkbtnExport_Click(object sender, EventArgs e)
    {
        //ExportGV(gvItems, "ENQUIRYLIST", (DataView)Session["ENQUIRYDV"], true);
        ExportExcel();
    }

    protected void lnkbtnResend_Click(object sender, EventArgs e)
    {
        int id = Convert.ToInt16(hdnEnqID.Value);
       
        string strSubjectPrefix;
        string strSubject;
        string strBody;
        string strUserSalutation;
        string strUserName;
        string strUserEmail;
        string strUserSubject;
        string strUserBody;
        string strUserContact;
        string strUserCompany;
        string strUserMessage;
        string strAck;
        string strSenderName;
        string strWebsiteName;
        string strSignature;
        string strGeoLocation = "";
        Boolean boolSuccess = false;

        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILPREFIX, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubjectPrefix = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEEMAILSIGNATURE, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSignature = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEADMINEMAILSUBJECT, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEADMINEMAILCONTENT, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEUSEREMAILSUBJECT, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strUserSubject = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEUSEREMAILCONTENT, clsConfig.CONSTGROUPEMAILCONTENT, 1);
        strUserBody = config.longValue;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEDEFAULTSENDERNAME, clsConfig.CONSTGROUPEMAILSETTING, 1);
        strSenderName = config.value;

        config.extractItemByNameGroup(clsConfig.CONSTNAMEWEBSITENAME, clsConfig.CONSTGROUPWEBSITE, 1);
        strWebsiteName = config.value;

        if (!string.IsNullOrEmpty(strSubjectPrefix)) { strSubject = strSubjectPrefix + " " + strSubject + " - Resend"; }

        clsEnquiry enquiry = new clsEnquiry();
        clsMis mis = new clsMis();
        if (enquiry.extractEnquiryById(id, 0))
        {
            strUserSalutation = (enquiry.enqSalutation > 0) ? mis.getListNameByListValue(enquiry.enqSalutation.ToString(), "SALUTATION", 1, 1) : "-";
            strUserName = enquiry.enqName;
            strUserCompany = (enquiry.enqCompanyName != "") ? enquiry.enqCompanyName : "-";
            strUserContact = (enquiry.enqContactNo != "") ? enquiry.enqContactNo : "-";
            strUserEmail = (enquiry.enqEmailSender != "") ? enquiry.enqEmailSender : "-";
            strUserSubject = (enquiry.enqSubject != "") ? enquiry.enqSubject : "-";
            strUserMessage = (enquiry.enqMessage != "") ? enquiry.enqMessage : "-";

            strBody = strBody.Replace("[SALUTATION]", strUserSalutation);
            strBody = strBody.Replace("[NAME]", strUserName);
            strBody = strBody.Replace("[COMPANY]", strUserCompany);
            strBody = strBody.Replace("[CONTACT]", strUserContact);
            strBody = strBody.Replace("[EMAIL]", strUserEmail);
            strBody = strBody.Replace("[SUBJECT]", strUserSubject);
            strBody = strBody.Replace("[MESSAGE]", strUserMessage);
            strBody = strBody.Replace("[WEBSITENAME]", strWebsiteName);
            strBody = strBody.Replace("[SIGNATURE]", strSignature);

            //@ Send email to admin
            if (clsEmail.send(strSenderName, "", "", "", "", strUserEmail, strSubject, strBody, 1))
            {
                boolSuccess = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(strSubject)) { strSubject = strSubject + " using Webteq"; }
                if (clsEmail.sendByGroup(strSenderName, "", "", "", "", strUserEmail, strSubject, strBody, "", 1, clsConfig.CONSTGROUPWEBTEQEMAILSETTING))
                {
                    boolSuccess = true;
                }
                else
                {
                    boolSuccess = false;
                }
            }

            if (!boolSuccess)
            {
                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to send email. Please try again.</div>";
                Response.Redirect(Request.Url.ToString());
            }
            else
            {
                enquiry.updateEnquiryResend();
                Session["DATA_RESEND"] = 1;
                Response.Redirect(Request.Url.ToString());
            }
        }
    


        Response.Redirect(currentPageName);
    }

    public override void VerifyRenderingInServerForm(Control control)
    {

    }
    #endregion

    #region "Methods"
    protected void setSearchProperties()
    {
        ListItem ddlActiveDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        //ddlActive.Items.Insert(0, ddlActiveDefaultItem);
        //ddlActive.Items.Add(new ListItem("Yes", "1"));
        //ddlActive.Items.Add(new ListItem("No", "0"));
    }

    protected void bindGV(GridView gvRef, string gvSortExpDir)
    {
        clsEnquiry enquiry = new clsEnquiry();
        DataSet ds = new DataSet();
        ds = enquiry.getUserEnquiry(0);

        DataView dv = new DataView(ds.Tables[0]);
        string strKeyword = "";
        //string strKeyword = txtKeyword.Text.Trim();
        dv.RowFilter = "1 = 1";

        Boolean boolFilter = false;

        //if (boolSearch)
        //{
        //    if (!string.IsNullOrEmpty(strKeyword))
        //    {
        //        dv.RowFilter += " AND (ENQ_NAME LIKE '%" + strKeyword + "%' OR ENQ_COMPANYNAME LIKE '%" + strKeyword + "%' OR ENQ_EMAILSENDER LIKE '%" + strKeyword + "%' OR ENQ_SUBJECT LIKE '%" + strKeyword + "%' OR ENQ_EMAILRECIPIENT LIKE '%" + strKeyword + "%' OR ENQ_URL LIKE '%" + strKeyword + "%' )";
        //        boolFilter = true;

        //        litItemFound.Text = "Search for <span class=\"SectKeyword\">" + strKeyword + "</span> | ";
        //    }
        //    else { litItemFound.Text = ""; }

        //    if (!string.IsNullOrEmpty(ddlActive.SelectedValue))
        //    {
        //        dv.RowFilter += " AND ENQ_ACTIVE = " + ddlActive.SelectedValue;
        //        boolFilter = true;
        //    }
        //}

        if (gvSortExpDir != "")
        {
            dv.Sort = gvSortExpDir;
        }
        else
        {
            dv.Sort = "ENQ_CREATION DESC";
        }

        Session["ENQUIRYDV"] = dv;

        gvRef.DataSource = dv;
        gvRef.DataBind();

        if (boolSearch) { litItemFound.Text += dv.Count + " record(s) listed."; }
        else { litItemFound.Text = dv.Count + " record(s) listed."; }

        if (dv.Count == 0)
        {
            lnkbtnExport.Enabled = false;
            lnkbtnExport.CssClass += " disabled";
        }
        else
        {
            lnkbtnExport.Enabled = true;
            //lnkbtnExport.CssClass = "btn2 btnExport";
        }
    }

    protected void ExportGV(GridView gvRef, string strFileTitle, DataView dvItems, Boolean boolHideLastColumn)
    {
        DateTime dt = DateTime.Now;
        string strFileName = strFileTitle + "_" + dt.Day + dt.Month + dt.Year + ".xls";
        string strAttachment = "attachment; filename=" + strFileName;

        Response.ClearContent();
        Response.AddHeader("content-disposition", strAttachment);
        Response.ContentType = "application/vnd.xls";
        Response.ContentEncoding = System.Text.Encoding.Unicode;
        Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        gvRef.AllowPaging = false;
        gvRef.DataSource = dvItems;
        gvRef.DataBind();

        for (int i = 0; i < gvRef.Columns.Count; i++)
        {
            if (gvRef.HeaderRow.Cells[i].CssClass == "hiddenCol")
            {
                gvRef.HeaderRow.Cells[i].CssClass = "showCol";
            }

            gvRef.HeaderRow.Cells[i].Enabled = false;
        }

        for (int i = 0; i < gvRef.Rows.Count; i++)
        {
            GridViewRow row = gvRef.Rows[i];

            for (int j = 0; j < gvRef.Columns.Count; j++)
            {
                if (row.Cells[j].CssClass == "hiddenCol")
                {
                    row.Cells[j].CssClass = "showCol";
                }
            }
        }

        if (boolHideLastColumn)
        {
            int intColHide = gvRef.Columns.Count - 1;

            gvRef.HeaderRow.Cells[intColHide].Visible = false;
            gvRef.Columns[intColHide].Visible = false;
        }

        gvRef.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }
    private void ExportExcel()
    {
        DateTime datetime = DateTime.Now;
        string filename = "ENQUIRYLIST_" + datetime.Day + datetime.Month + datetime.Year + ".xls";
        string attachment = "attachment; filename=" + filename;

        DataTable dt = new clsEnquiry().getUserEnquiry(0).Tables[0];

        GridView GridView1 = new GridView();
        GridView1.AllowPaging = false;
        GridView1.DataSource = dt;
        GridView1.DataBind();

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", attachment);
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel ";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);

        GridView1.RenderControl(hw);
        Response.Output.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }
    #endregion
}