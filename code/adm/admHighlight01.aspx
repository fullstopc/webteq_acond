﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admHighlight01.aspx.cs" Inherits="adm_admHighlight01" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="main-content__header">
        <div class="title"><asp:Literal ID="litPageTitle" runat="server" meta:resourcekey="litPageTitle"></asp:Literal></div>
        <div class="divListingSplitter"></div>
        <div class="action">
			<asp:Panel ID="pnlListingAction" runat="server" CssClass="divListingAction">
                <asp:HyperLink ID="hypAdd" runat="server" Text="Add Event" ToolTip="Add Event" NavigateUrl="admHighlight0101.aspx" CssClass="hypListingAction add"></asp:HyperLink>
				<div class="divListingSplitter"></div>
                <asp:LinkButton ID="lnkbtnExport" runat="server" Text="Export" ToolTip="Export" CssClass="hypListingAction export" OnClick="lnkbtnExport_Click"></asp:LinkButton>
            </asp:Panel>
        </div>
        <div class="divListingSplitter"></div>
        <div class="filter">
            <div class="divFilterContainer">
                <div class="divFilter">
                    <asp:TextBox ID="txtKeyword" runat="server" placeholder="Keyword Search" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
                    <a href="#" class="btnDropdown" onclick="toggleFilterOption();"></a>
                    <asp:Literal ID="litKeywordRule" runat="server" meta:resourcekey="litKeywordRule" visible="false"></asp:Literal>
                </div>
                <div class="divFilterMore">
                    <div></div>
                    <div>
                        <table class="tblSearch">
                            <tr>
                                <td class="">
                                    <div class="divFilterLabel">Category:</div>
                                    <asp:DropDownList ID="ddlCategory" runat="server" CssClass="ddl"></asp:DropDownList>
                                </td>
                                <td class="">
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
            <div class="divFilterAction" runat="server" visible="false"><asp:Button ID="lnkbtnGo" runat="server" Text="" ToolTip="Search" CssClass="btn btnSearch" OnClick="lnkbtnGo_Click"/></div>
        </div>
    </div>
    <div class="divListing">
        <div id="divListingHdr" class="divListingHdr">
            <div class="divListingHdrLeft"><asp:Literal ID="litItemFound" runat="server"></asp:Literal></div>
        </div>
        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
            <asp:Literal ID="litAck" runat="server"></asp:Literal>
        </asp:Panel>

        <uc:CmnDatatable ID="ucCmnDatatable" runat="server" />
        <asp:HiddenField runat="server" ID="hdnItemID" />
        <asp:LinkButton ID="lnkbtnDelete" runat="server" OnClick="lnkbtnDelete_Click" style="display:none;"></asp:LinkButton>

        
        <%-- Visible false at 11 Oct 2016 --%>
        <div class="divListingData" runat="server" visible="false">
            <asp:GridView ID="gvHighlight" runat="server" AutoGenerateColumns="false" AllowSorting="true"
                AllowPaging="true" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom"
                CssClass="dataTbl" OnRowDataBound="gvHighlight_RowDataBound" OnSorting="gvHighlight_Sorting"
                OnPageIndexChanging="gvHighlight_PageIndexChanging" OnRowCommand="gvHighlight_RowCommand"
                DataKeyNames="HIGH_ID" EmptyDataText="No record found">
                <PagerStyle CssClass="pg" HorizontalAlign="Right" />
                <AlternatingRowStyle CssClass="td_alt" />
                <Columns>
                    <asp:TemplateField HeaderText="no.">
                        <ItemStyle CssClass="tdNo" />
                        <ItemTemplate>
                            <asp:Literal ID="litNo" runat="server" Text="<% #Container.DataItemIndex + 1 %>"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="start date" DataField="HIGH_STARTDATE" SortExpression="HIGH_STARTDATE" DataFormatString="{0:d MMM yyyy}" ItemStyle-CssClass="" ItemStyle-Width="100" Visible="false"/>
                    <asp:BoundField HeaderText="end date" DataField="HIGH_ENDDATE" SortExpression="HIGH_ENDDATE" DataFormatString="{0:d MMM yyyy}" ItemStyle-CssClass=""  ItemStyle-Width="100" Visible="false"/>
                    <asp:TemplateField HeaderText="start date" SortExpression="HIGH_STARTDATE">
                        <ItemTemplate>
                            <asp:Literal runat="server" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "HIGH_STARTDATE")).Date == DateTime.MaxValue.Date ? "-" : Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "HIGH_STARTDATE")).ToString("d MMM yyyy") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="end date" SortExpression="HIGH_ENDDATE">
                        <ItemTemplate>
                            <asp:Literal runat="server" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "HIGH_ENDDATE")).Date == DateTime.MaxValue.Date ? "-" : Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "HIGH_ENDDATE")).ToString("d MMM yyyy") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="title" DataField="HIGH_TITLE" SortExpression="HIGH_TITLE" />
                    <asp:BoundField HeaderText="No. of Image" DataField="HIGHGALL_COUNT" SortExpression="HIGHGALL_COUNT" />
                    <asp:BoundField HeaderText="category" DataField="V_GRPNAME" SortExpression="V_GRPNAME" HeaderStyle-CssClass="hiddenCol" ItemStyle-CssClass="hiddenCol"/>
                    <asp:TemplateField HeaderText="comment" SortExpression="V_NOC" HeaderStyle-CssClass="hiddenCol" ItemStyle-CssClass="hiddenCol" Visible="false">
                        <ItemTemplate>
                            <asp:Literal ID="litNoOfComment" runat="server"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="showtop" SortExpression="HIGH_SHOWTOP">
                        <ItemStyle CssClass="td_small" />
                        <ItemTemplate>
                            <asp:Literal ID="litShowTop" runat="server" Text='<%#clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "HIGH_SHOWTOP"))) %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="active" SortExpression="HIGH_VISIBLE">
                        <ItemStyle CssClass="td_small" />
                        <ItemTemplate>
                            <asp:Literal ID="litVisible" runat="server" Text='<%#clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "HIGH_VISIBLE"))) %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="action">
                        <ItemStyle CssClass="tdAct" />
                        <ItemTemplate>
                            <asp:HyperLink ID="hypEdit" CssClass="lnkbtn lnkbtnEdit" runat="server"></asp:HyperLink>
                            <%--<asp:LinkButton ID="lnkbtnEdit" runat="server" CausesValidation="false" CommandName="cmdEdit" CssClass="lnkbtn lnkbtnEdit"></asp:LinkButton>--%>
                            <span id="spanSplitter" runat="server" class="spanSplitter">|</span>
                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CausesValidation="false" CommandName="cmdDelete" CssClass="lnkbtn lnkbtnDelete"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>

    <script type="text/javascript">

        $(function () {
            var columns = [
                    { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                    { "data": "HIGH_STARTDATE", "bSearchable": false, "label": "Start Date", "order": 1, "convert": "datetime" },
                    { "data": "HIGH_ENDDATE", "bSearchable": false, "label": "End Date", "order": 2, "convert": "datetime" },
                    { "data": "HIGH_TITLE", "label": "Title", "order": 3,},
                    { "data": "HIGHGALL_COUNT", "bSearchable": false, "bSearchable": false, "label": "No. of Image", "order": 4, },
                    { "data": "V_GRPNAME", "bSearchable": false, "label": "Category", "order": 5,},
                    { "data": "HIGH_SHOWTOP", "bSearchable": false, "label": "Show On Top", "order": 6, "convert": "active" },
                    { "data": "HIGH_VISIBLE", "bSearchable": false, "label": "Active", "order": 7, "convert": "active" },
                    { "data": "action", "bSortable": false, "bSearchable": false, "label": "Action", "order": 8,"exportable":false },
            ];
            var columnSort = columns.reduce(function (prev, curr) {
                prev[curr.data] = curr.order;
                return prev;
            }, {});

            var columnDefs = [{
                "render": function (data, type, row) {
                    var lnkbtnEdit = "<a class='lnkbtn lnkbtnEdit' href='admHighlight0101.aspx?id=" + row.HIGH_ID + "' title='Edit'></a>";
                    var lnkbtnDelete = "<a class='lnkbtn lnkbtnDelete' title='Delete' data-id='" + row.HIGH_ID + "'></a>";
                    return lnkbtnEdit + lnkbtnDelete;
                },
                "targets": columnSort["action"]
            }];
            var properties = {
                columns: columns,
                columnDefs: columnDefs,
                columnSort: columnSort,
                func: "getHighlightData",
                name: "event",
            };
            var $datatable = callDataTables(properties);

            // Filter
            $("#<%= ddlCategory.ClientID %>").change(function () {
                $datatable
               .columns(columnSort["V_GRPNAME"])
               .search(this.value)
               .draw();
            });

            // Export
            $("#<%= lnkbtnExport.ClientID %>").click(function (e) {
                var $table = $('.dataTable').clone();
                $table.attr("border", "1");
                $table.find("th:last-child,td:last-child").remove();
                window.open('data:application/vnd.ms-excel,' + encodeURIComponent($table.get(0).outerHTML));

                e.preventDefault();
            });

            $(document).on("click", ".lnkbtnDelete", function () {
                if (confirm("<%= HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeletePage.Text") %>")) {
                    var pageID = $(this).attr("data-id");
                    document.getElementById('<%= hdnItemID.ClientID %>').value= pageID;
                    document.getElementById('<%= lnkbtnDelete.ClientID %>').click();
                }
            })
            
        });
    </script>
</asp:Content>
