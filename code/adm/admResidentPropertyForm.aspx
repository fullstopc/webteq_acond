﻿<%@ Page Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admResidentPropertyForm.aspx.cs" Inherits="adm_adm"  ValidateRequest="false" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/mst/blank.Master" %>
<asp:Content ID="Content" ContentPlaceHolderID="cphHead" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="container-fluid timeslot-form">
        <div class="row">
            <label class="col-sm-1"><b>Area:</b></label>
            <div class="col-sm-3">
                <asp:DropDownList runat="server" ID="ddlArea" CssClass="form-control"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="rvArea" 
                 ControlToValidate="ddlArea"
                 Display="Dynamic"
                 CssClass="errmsg"
                 ErrorMessage="Please select area."
                 runat="server"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-1"><b>Unit No.:</b></label>
            <div class="col-sm-3">
                <asp:TextBox ID="txtUnit" runat="server" CssClass="form-control" MaxLength="10" placeholder="Unit No."></asp:TextBox>
                <asp:RequiredFieldValidator ID="rvUnit" 
                 ControlToValidate="txtUnit"
                 Display="Dynamic"
                 CssClass="errmsg"
                 ErrorMessage="Please enter Unit No."
                 runat="server"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-1"><b>Type :</b></label>
            <div class="col-sm-3">
                <asp:DropDownList runat="server" ID="ddlType" CssClass="form-control"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="rvType" 
                 ControlToValidate="ddlType"
                 Display="Dynamic"
                 CssClass="errmsg"
                 ErrorMessage="Please select type."
                 runat="server"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <asp:LinkButton runat="server" ID="lnkbtnSave" OnClick="lnkbtnSave_Click" ToolTip="Save" CssClass="btn btnSave right">Save</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>