﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admDate01.aspx.cs" Inherits="adm_admDate01" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmCheckAccess.ascx" TagName="AdmCheckAccess" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
<script type="text/javascript">
var txtDate;

    function initSect3() {
        txtDate = document.getElementById("<%= txtDate.ClientID %>");
        txtTime = document.getElementById("<%= txtTime.ClientID %>");

        $(function () {

            var date = new Date();
            var currentMonth = date.getMonth();
            var currentDate = date.getDate();
            var currentYear = date.getFullYear();

            $("#<% =txtDate.ClientID %>").datepicker({
                dateFormat: 'dd M yy',
                showOn: 'button',
                //beforeShowDay: $.datepicker.noWeekends,
                //minDate: new Date(currentYear, currentMonth, currentDate)
                buttonImage: '../img/adm/calendar.png',
	            buttonImageOnly: true
            });

            $("#<% =txtTime.ClientID %>").timepicker({
                // minTime: { hour: 9, minute: 30 },
                 //maxTime: { hour: 19, minute: 30 },
                 showPeriod: true,
         //        minutes: { interval: 15 },
                 showLeadingZero: true
             });

             $("#<% =txtDate.ClientID %>").datepicker({ changeMonth: true });
             $("#<% =txtDate.ClientID %>").datepicker({ changeYear: true });

            //getter
             var changeMonth = $("#<% =txtDate.ClientID %>").datepicker('option', 'changeMonth');
             var changeYear = $("#<% =txtDate.ClientID %>").datepicker('option', 'changeYear');

            //setter
             $("#<% =txtDate.ClientID %>").datepicker('option', 'changeMonth', true);
             $("#<% =txtDate.ClientID %>").datepicker('option', 'changeYear', true);
        });
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <uc:AdmCheckAccess ID="AdmCheckAccess1" runat="server" />
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>    
    <div class="divForm">
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <asp:Panel ID="pnlDateForm" runat="server" DefaultButton="lnkbtnSave">
            <table id="tblDate" cellpadding="0" cellspacing="0" class="formTbl tblData">
                <tr>
                    <td colspan="2" class="tdSectionHdr">Date Details</td>
                </tr>
                <tr>
                    <td class="tdSpacer">&nbsp;</td>
                    <td class="tdSpacer">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tdLabel nobr">Date<span class="attention_compulsory">*</span>:</td>
                    <td class="tdMax">
                        <asp:TextBox ID="txtDate" runat="server" CssClass="text_bigdate uniq" MaxLength="10"></asp:TextBox>
                        <span class="fieldDesc"><asp:RequiredFieldValidator ID="rfvDate" runat="server" ErrorMessage="<br />Please select date." ControlToValidate="txtDate" Display="Dynamic" CssClass="errmsgEnquiry" ValidationGroup="vgDate"></asp:RequiredFieldValidator></span>
                        <span class="spanFieldDesc"><asp:CustomValidator ID="cvDate" runat="server" ControlToValidate="txtDate" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateDate_server" OnPreRender="cvDate_PreRender" ValidationGroup="vgDate"></asp:CustomValidator></span>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel nobr">Time<span class="attention_compulsory">*</span>:</td>
                    <td>
                        <asp:TextBox ID="txtTime" runat="server" CssClass="text_big uniq" MaxLength="10"></asp:TextBox>
                        <span class="fieldDesc"><asp:RequiredFieldValidator ID="rfvTime" runat="server" ErrorMessage="<br />Please select time." ControlToValidate="txtTime" Display="Dynamic" CssClass="errmsgEnquiry" ValidationGroup="vgDate"></asp:RequiredFieldValidator></span>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" CssClass="btn btnSave" OnClick="lnkbtnSave_Click" ValidationGroup="vgDate"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <div class="divListing">
        <div class="divListingHdr"><span class="SectHdr"><asp:Literal ID="litDateFound" runat="server"></asp:Literal></div>
        <asp:Panel ID="pnlAckList" runat="server" Visible="false" CssClass="divAck">
            <asp:Literal ID="litAckList" runat="server"></asp:Literal>
        </asp:Panel>
        <asp:Panel ID="pnlListingData" runat="server" CssClass="divListingData">
            <asp:GridView ID="gvDate" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom" CssClass="dataTbl" OnRowDataBound="gvDate_RowDataBound" OnSorting="gvDate_Sorting" OnPageIndexChanging="gvDate_PageIndexChanging" OnRowCommand="gvDate_RowCommand" DataKeyNames="TD_ID" EmptyDataText="No record found.">
                <PagerStyle CssClass="pg" HorizontalAlign="Right" />
                <Columns>
                    <asp:TemplateField HeaderText="NO.">
                        <ItemStyle CssClass="tdNo" />
                        <ItemTemplate>
                            <asp:Literal ID="litNo" runat="server" Text="<%# Container.DataItemIndex + 1 %>"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="DATE" DataField="TD_DATETIME" SortExpression="TD_DATETIME" DataFormatString="{0:dd MMM yyyy}" />
                    <asp:BoundField HeaderText="TIME" DataField="TD_DATETIME" SortExpression="TD_DATETIME" DataFormatString="{0:hh:mm tt}"/>
                    <asp:BoundField HeaderText="STATUS" DataField="LIST_NAME" SortExpression="LIST_NAME"/>
                    <asp:TemplateField HeaderText="ACTION">
                        <ItemStyle CssClass="tdAct" />
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnEdit" CssClass="lnkbtn lnkbtnEdit" runat="server" CausesValidation="false" CommandName="cmdEdit"></asp:LinkButton>
                            <span class="spanSplitter">|</span>
                            <asp:LinkButton ID="lnkbtnDelete" CssClass="lnkbtn lnkbtnDelete" runat="server" CausesValidation="false" CommandName="cmdDelete"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </div>
</asp:Content>