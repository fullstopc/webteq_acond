﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class adm_admRegistrationDetails : System.Web.UI.Page
{
    #region "Properties"
    protected int _parId;
    protected Boolean _boolPaid = false;
    protected decimal _decAmount = Convert.ToDecimal("0.00");
    #endregion

    #region "Property Methods"
    public int parId
    {
        get
        {
            if (ViewState["PARTICIPANTID"] == null)
            {
                return _parId;
            }
            else
            {
                return Convert.ToInt16(ViewState["PARTICIPANTID"]);
            }
        }
        set { ViewState["PARTICIPANTID"] = value; }
    }

    protected Boolean boolPaid
    {
        get
        {
            if (ViewState["BOOLPAID"] == null)
            {
                return _boolPaid;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLPAID"]);
            }
        }
        set { ViewState["BOOLPAID"] = value; }
    }

    protected decimal decAmount
    {
        get
        {
            if (ViewState["DECAMOUNT"] == null)
            {
                return _decAmount;
            }
            else
            {
                return Convert.ToDecimal(ViewState["DECAMOUNT"]);
            }
        }
        set { ViewState["DECAMOUNT"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            litRegDetailsHdr.Text = "REGISTRATION DETAILS";
            litPaymentDetailsHdr.Text = "PAYMENT DETAILS";

            if (!string.IsNullOrEmpty(Request["pid"]))
            {
                int intTryParse;
                if (int.TryParse(Request["pid"], out intTryParse))
                {
                    parId = intTryParse;
                }
                else
                {
                    parId = int.MaxValue;
                }
            }

            setPageProperties();
        }

        hypClose.Attributes["onclick"] = "javascript:window.close();";
        lnkbtnCancel.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmCancelParticipant.Text").ToString() + "'); return false;";

        Session["EDITEDPARID"] = null;
        Session["NOCHANGE"] = null;
        Session["ERRMSG"] = null;
        Session["CANCELLEDNAME"] = null;
    }

    protected void validatePayment_Server(object source, ServerValidateEventArgs args)
    {
        if (!boolPaid)
        {
            string strPayAmount = txtPaymentAmount.Text.Trim();

            if (!string.IsNullOrEmpty(strPayAmount))
            {
                decimal decPayAmount = Convert.ToDecimal("0.00");

                decimal decTryParse;
                if (decimal.TryParse(strPayAmount, out decTryParse))
                {
                    decPayAmount = decTryParse;
                }

                if (decPayAmount >= decAmount)
                {
                    args.IsValid = true;
                }
                else
                {
                    args.IsValid = false;
                }

                cvPaymentAmount2.ErrorMessage = "<br />Payment amount should be same or exceed Program Fee.";
            }
        }
    }

    protected void cvPaymentAmount2_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("PayAmount")))
        {
            string strJS = null;
            strJS = "<script type = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtPaymentAmount.ClientID + "', document.all['" + cvPaymentAmount2.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "PayAmount", strJS, false);
        }
    }

    protected void lnkbtnCancel_Click(object sender, EventArgs e)
    {
        string strDeletedParentName = null;

        clsMis.performCancelRegistration(parId, ref strDeletedParentName);

        Session["CANCELLEDNAME"] = strDeletedParentName;

        Response.Redirect(Request.Url.ToString());
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strFirstName = txtFirstName.Text.Trim();
            string strLastName = txtLastName.Text.Trim();
            string strEmail = txtEmail.Text.Trim();
            string strContactNo = txtContactNo.Text.Trim();
            int intMalaysian = rdoYes.Checked ? clsAdmin.CONSTMALAYSIANYES : (rdoNo.Checked ? clsAdmin.CONSTMALAYSIANNO : 0);
            int intGender = rdoMale.Checked ? clsAdmin.CONSTMALE : (rdoFemale.Checked ? clsAdmin.CONSTFEMALE : 0);
            DateTime dtDOB = DateTime.MaxValue;

            //string strName = txtName.Text.Trim();
            ////string strIcNo = txtIcNo.Text.Trim();
            //string strNationality = txtNationality.Text.Trim();
            //DateTime dtDOB = DateTime.MaxValue;
            //string strAddress = txtAddress.Text.Trim();
            //string strContactNo = txtContactNo.Text.Trim();
            //string strEmail = txtEmail.Text.Trim();
            //int intProgDate = !string.IsNullOrEmpty(ddlProgramDate.SelectedValue) ? Convert.ToInt16(ddlProgramDate.SelectedValue) : 0;
            //string strOtherCourses = txtOtherCourses.Text.Trim();
            //string strPromotionCode = txtPromotionCode.Text.Trim();

            if (!string.IsNullOrEmpty(hdnDOB.Value.Trim()))
            {
                string strDOB = clsMis.formatDateDDMMtoMMDD(hdnDOB.Value.Trim());
                dtDOB = Convert.ToDateTime(strDOB);
            }

            int intHighestEdu = !string.IsNullOrEmpty(ddlHighestEdu.SelectedValue) ? Convert.ToInt16(ddlHighestEdu.SelectedValue) : 0;
            int intEngSpeak = !string.IsNullOrEmpty(ddlEngSpeak.SelectedValue) ? Convert.ToInt16(ddlEngSpeak.SelectedValue) : 0;
            int intEngWrite = !string.IsNullOrEmpty(ddlEngWrite.SelectedValue) ? Convert.ToInt16(ddlEngWrite.SelectedValue) : 0;
            int intProgDate = !string.IsNullOrEmpty(ddlProgramDate.SelectedValue) ? Convert.ToInt16(ddlProgramDate.SelectedValue) : 0;

            clsParticipant par = new clsParticipant();
            int intRecordAffected = 0;
            Boolean boolSuccess = true;
            Boolean boolEdited = false;

            if (!par.isExactSameDataSet(parId, strFirstName, strLastName, strEmail, strContactNo, intMalaysian, intGender, dtDOB, intHighestEdu, intEngSpeak, intEngWrite, intProgDate))
            {
                intRecordAffected = par.updateParticipantById(parId, strFirstName, strLastName, strEmail, strContactNo, intMalaysian, intGender, dtDOB, intHighestEdu, intEngSpeak, intEngWrite, intProgDate);

                if (intRecordAffected == 1) { boolEdited = true; }
                else { boolSuccess = false; }
            }

            //if (!par.isExactSameDataSet(parId, strName, strIcNo, strNationality, dtDOB, strAddress, strContactNo, strEmail, strOtherCourses, intProgDate, strPromotionCode))
            //{
            //    intRecordAffected = par.updateParticipantById(parId, strName, strIcNo, strNationality, dtDOB, strAddress, strContactNo, strEmail, strOtherCourses, intProgDate, strPromotionCode);

                
            //}

            //if (boolSuccess && !boolPaid)
            //{
            //    string strPayGateway = txtPaymentMethod.Text.Trim();
            //    string strTransId = txtTransID.Text.Trim();
            //    string strPayAmount = txtPaymentAmount.Text.Trim();
            //    decimal decPayAmount = Convert.ToDecimal("0.00");

            //    if (!string.IsNullOrEmpty(strPayGateway) && !string.IsNullOrEmpty(strTransId) && !string.IsNullOrEmpty(strPayAmount))
            //    {
            //        decimal decTryParse;
            //        if (decimal.TryParse(strPayAmount, out decTryParse)) { decPayAmount = decTryParse; }

            //        intRecordAffected = par.addParticipantPayment(parId, strPayGateway, decPayAmount, strTransId);

            //        if (intRecordAffected == 1)
            //        {
            //            boolEdited = true;

            //            par.extractParticipantById(parId, 0);

            //            string strRandomCode = par.parRandomCode;
            //            string strMemName = par.parFirstName + " " + par.parLastName;
            //            string strMemEmail = par.parEmail;

            //            if (string.IsNullOrEmpty(strMemName))
            //            {
            //                string[] strMemEmailSplit = strMemName.Split('@');

            //                strMemName = strMemEmailSplit[0];
            //            }

            //            clsConfig config = new clsConfig();
            //            config.extractItemByNameGroup(clsConfig.CONSTNAMECURRENCY, clsConfig.CONSTGROUPDEFAULT, 1);
            //            string strCurrency = config.value;
            //            string strPersonDetails = "Name: " + strMemName + "<br />Email: " + strMemEmail;
            //            //string strPaymentDetails = "Payment Type: " + par.parPaymentGatewayName + "<br />Remarks: " + par.paymentRemarks + "<br />Paid Amount: " + strCurrency + " " + par.programFee + "<br />" + strPersonDetails;

            //            string strRegDate = par.parRegisteredDate.ToString("dd MMM yyyy");
            //            //string strProgDate = par.programStartDate.ToString("dd MMM yyyy") + " - " + par.programEndDate.ToString("dd MMM yyyy");

            //            sendDetails(strMemName, strMemEmail, strRegDate, par.parContact, strProgDate, par.parProgramName, par.programFee, strPaymentDetails);

            //            par.updateParticipantStatus(parId, clsAdmin.CONSTORDERSTATUSPAY);
            //        }
            //        else { boolSuccess = false; }
            //    }
            //}

            if (boolEdited)
            {
                Session["EDITEDPARID"] = parId;
            }
            else
            {
                if (!boolSuccess)
                {
                    Session["ERRMSG"] = "Failed to update participant (" + txtFirstName.Text.Trim() + " " + txtLastName.Text.Trim() + "). Please try again.";
                }
                else
                {
                    Session["EDITEDPARID"] = parId;
                    Session["NOCHANGE"] = 1;
                }
            }

            Response.Redirect(Request.Url.ToString());
        }
    }
    #endregion

    #region "Methods"
    protected void refreshParentWindow()
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("refreshParent"))
        {
            string strJS = "";
            strJS = "<script type=\"text/javascript\">";
            strJS += "window.opener.location.reload(true);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "refreshParent", strJS, false);
        }
    }

    protected void setPageProperties()
    {
        clsMis mis = new clsMis();

        rdoYes.Text = mis.getListNameByListValue(clsAdmin.CONSTMALAYSIANYES.ToString(), "MALAYSIAN TYPE", 1, 1);
        rdoNo.Text = mis.getListNameByListValue(clsAdmin.CONSTMALAYSIANNO.ToString(), "MALAYSIAN TYPE", 1, 1);

        rdoMale.Text = mis.getListNameByListValue(clsAdmin.CONSTMALE.ToString(), "GENDER", 1, 1);
        rdoFemale.Text = mis.getListNameByListValue(clsAdmin.CONSTFEMALE.ToString(), "GENDER", 1, 1);

        registerScript();
        fillForm();

        if (Session["EDITEDPARID"] != null)
        {
            if (Session["NOCHANGE"] != null)
            {
                litAck.Text = "<div class=\"noticemsg2\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
            }
            else
            {
                litAck.Text = "<div class=\"noticemsg2\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";
                refreshParentWindow();

                clsMis.resetListing();
            }

            pnlRegistrationDetailsAck.Visible = true;
        }
        else if (Session["CANCELLEDNAME"] != null)
        {
            litAck.Text = "<div class=\"noticemsg2\">" + GetGlobalResourceObject("GlobalResource", "admCancelMessage.Text").ToString() + "</div>";
            refreshParentWindow();

            clsMis.resetListing();
        }
        else if (Session["ERRMSG"] != null)
        {
            litAck.Text = "<div class=\"errmsg2\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";

            pnlRegistrationDetailsAck.Visible = true;
        }

        lnkbtnPrint.OnClientClick = "javascript:window.open('admPrintRegister.aspx?id=" + parId + "','_blank','toolbar=false,menubar=false,scrollbars,width=900px','false'); return false";
    }

    protected void registerScript()
    {
        if (!Page.ClientScript.IsStartupScriptRegistered("FORMSECT"))
        {
            string strJS = null;
            strJS = "<script type=\"text/javascript\">";
            strJS += "initSect3();";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "FORMSECT", strJS, false);
        }
    }

    protected void fillForm()
    {
        //clsConfig config = new clsConfig();
        //config.extractItemByNameGroup(clsConfig.CONSTNAMECURRENCY, clsConfig.CONSTGROUPDEFAULT, 1);
        //string strCurrency = config.value;
        //litPayAmountCurrency.Text = "(" + strCurrency + ")";

        clsParticipant par = new clsParticipant();

        if (par.extractParticipantById(parId, 0))
        {
            //decAmount = par.programFee;

            if (par.parStatus > 0 && par.proDeleted <= 0)
            {
                txtFirstName.Visible = true;
                txtLastName.Visible = true;
                txtEmail.Visible = true;
                txtContactNo.Visible = true;
                rdoYes.Visible = true;
                rdoNo.Visible = true;

                if (par.parNationalityId == 1) { rdoYes.Checked = true; }
                else if (par.parNationalityId == 2) { rdoNo.Checked = true; }

                rdoMale.Visible = true;
                rdoFemale.Visible = true;

                if (par.parGenderId == 1) { rdoMale.Checked = true; }
                else if (par.parGenderId == 2) { rdoFemale.Checked = true; }

                txtDOB.Visible = true;
                ddlHighestEdu.Visible = true;
                ddlEngSpeak.Visible = true;
                ddlEngWrite.Visible = true;
                ddlProgramDate.Visible = true;

                //txtIcNo.Visible = true;
                //txtNationality.Visible = true;
                //txtDOB.Visible = true;
                //txtAddress.Visible = true;
                //txtContactNo.Visible = true;
                //txtEmail.Visible = true;
                //txtOtherCourses.Visible = true;
                //ddlProgramDate.Visible = true;
                //txtPromotionCode.Visible = true;

                clsProgram prog = new clsProgram();
                clsMis mis = new clsMis();
                DataSet ds = new DataSet();
                DataView dv;

                ds = mis.getListByListGrp("EDUCATION LEVEL", 1);
                dv = new DataView(ds.Tables[0]);
                dv.Sort = "LIST_ORDER ASC";

                ddlHighestEdu.DataSource = dv;
                ddlHighestEdu.DataTextField = "LIST_NAME";
                ddlHighestEdu.DataValueField = "LIST_VALUE";
                ddlHighestEdu.DataBind();

                ListItem ddlHighestEduDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlHighestEdu.Items.Insert(0, ddlHighestEduDefaultItem);

                ds = mis.getListByListGrp("ENGLISH LEVEL", 1);
                dv = new DataView(ds.Tables[0]);
                dv.Sort = "LIST_ORDER ASC";

                ddlEngSpeak.DataSource = dv;
                ddlEngSpeak.DataTextField = "LIST_NAME";
                ddlEngSpeak.DataValueField = "LIST_VALUE";
                ddlEngSpeak.DataBind();

                ListItem ddlEngSpeakDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlEngSpeak.Items.Insert(0, ddlEngSpeakDefaultItem);

                ddlEngWrite.DataSource = dv;
                ddlEngWrite.DataTextField = "LIST_NAME";
                ddlEngWrite.DataValueField = "LIST_VALUE";
                ddlEngWrite.DataBind();

                ListItem ddlEngWriteDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlEngWrite.Items.Insert(0, ddlEngWriteDefaultItem);

                ds = prog.getProgramDateListByProgId(par.parProgramId);
                dv = new DataView(ds.Tables[0]);
                dv.Sort = "PROG_PERIOD ASC";

                ddlProgramDate.DataSource = dv;
                ddlProgramDate.DataTextField = "PROG_PERIOD";
                ddlProgramDate.DataValueField = "PROG_ID";
                ddlProgramDate.DataBind();

                ListItem ddlProgramDateDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlSelect.Text").ToString(), "");
                ddlProgramDate.Items.Insert(0, ddlProgramDateDefaultItem);

                //if (!string.IsNullOrEmpty(par.vGatewayName) || !string.IsNullOrEmpty(par.parPaymentGatewayName))
                //{
                //    boolPaid = true;
                //}
            }
            else
            {
                lnkbtnCancel.Visible = false;
                lnkbtnSave.Visible = false;

                litName.Visible = true;
                litEmail.Visible = true;
                litContactNo.Visible = true;
                litMalaysia.Visible = true;
                litGender.Visible = true;
                litDOB.Visible = true;
                litHighestEdu.Visible = true;
                litEngSpeak.Visible = true;
                litEngWrite.Visible = true;
                litProgDate.Visible = true;

                //litIcNo.Visible = true;
                //litNationality.Visible = true;

                if (par.parDOB.ToShortDateString() != DateTime.MaxValue.ToShortDateString() && par.parDOB.ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    trDOB.Visible = false;
                }

                //litDOB.Visible = true;
                //litAddress.Visible = true;
                //litContactNo.Visible = true;
                //litEmail.Visible = true;
                //litOtherCourses.Visible = true;
                //litProgDate.Visible = true;

                //if (!string.IsNullOrEmpty(par.parPromotionCode))
                //{
                //    litPromotionCode.Visible = true;
                //}
            }

            txtFirstName.Text = par.parFirstName;
            txtLastName.Text = par.parLastName;
            txtEmail.Text = par.parEmail;
            txtContactNo.Text = par.parContactNo;

            if (par.parNationalityId == 1) { rdoYes.Checked = true; }
            else if (par.parNationalityId == 2) { rdoNo.Checked = true; }

            if (par.parGenderId == 1) { rdoMale.Checked = true; }
            else if (par.parGenderId == 2) { rdoFemale.Checked = true; }

            txtDOB.Text = par.parDOB.ToString("dd MMM yyyy");

            ddlHighestEdu.SelectedValue = par.parEduLevelId > 0 ? par.parEduLevelId.ToString() : "";
            ddlEngSpeak.SelectedValue = par.parEngSpeakLevelId > 0 ? par.parEngSpeakLevelId.ToString() : "";
            ddlEngWrite.SelectedValue = par.parEngWriteLevelId > 0 ? par.parEngWriteLevelId.ToString() : "";
            ddlProgramDate.SelectedValue = par.parProgDateId > 0 ? par.parProgDateId.ToString() : "";

            //txtName.Text = par.parName;
            //litPaymentStatus.Text = par.parOrderStatus;
            //txtIcNo.Text = par.parIc;
            //txtNationality.Text = par.parNationality;

            //if (par.parDOB.ToShortDateString() != DateTime.MaxValue.ToShortDateString() && par.parDOB.ToShortDateString() == DateTime.MinValue.ToShortDateString())
            //{
            
            //}

            //txtAddress.Text = par.parAdd;
            //txtContactNo.Text = par.parContact;
            //txtEmail.Text = par.parEmail;
            //txtOtherCourses.Text = par.parOtherCourses;
            //litSelProgram.Text = par.parProgramName + " Program";
            //litProgramFee.Text = strCurrency + " " + par.programFee;
            
            //txtPromotionCode.Text = par.parPromotionCode;

            litName.Text = par.parFirstName + " " + par.parLastName;
            litEmail.Text = par.parEmail;
            litContactNo.Text = par.parContactNo;
            litMalaysia.Text = par.parNationalityId == 1 ? clsAdmin.CONSTADMPARYES : (par.parNationalityId == 2 ? clsAdmin.CONSTADMPARNO : " - ");
            litGender.Text = par.parGenderId == 1 ? clsAdmin.CONSTADMGENDERMALE : (par.parGenderId == 2 ? clsAdmin.CONSTADMGENDERFEMALE : " - ");
            litDOB.Text = par.parDOB.ToString("dd MMM yyyy");
            litHighestEdu.Text = !string.IsNullOrEmpty(par.parEduLevelName) ? par.parEduLevelName : " - ";
            litEngSpeak.Text = !string.IsNullOrEmpty(par.parEngSpeakLevelName) ? par.parEngSpeakLevelName : " - ";
            litEngWrite.Text = !string.IsNullOrEmpty(par.parEngWriteLevelName) ? par.parEngWriteLevelName : " - ";

            //litIcNo.Text = par.parIc;
            //litNationality.Text = par.parNationality;
            //litDOB.Text = par.parDOB.ToString("dd MMM yyyy");
            //litAddress.Text = par.parAdd;
            //litContactNo.Text = par.parContact;
            //litEmail.Text = par.parEmail;
            //litOtherCourses.Text = par.parOtherCourses;

            if (par.parProgStartDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString() && par.parProgEndDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
            {
                litProgDate.Text = par.parProgStartDate.ToString("dd MMM yyyy") + " - " + par.parProgEndDate.ToString("dd MMM yyyy");
            }

            //litPromotionCode.Text = par.parPromotionCode;
            litReason.Text = !string.IsNullOrEmpty(par.parSuitableReason) ? par.parSuitableReason : " - ";
            litResume.Text = !string.IsNullOrEmpty(par.parResumeName) ? par.parResumeName : " - ";
            litLearnAbout.Text = !string.IsNullOrEmpty(par.parLearnAboutName) ? par.parLearnAboutName : " - ";
            litProgCat.Text = !string.IsNullOrEmpty(par.parProgGrpName) ? par.parProgGrpName : " - ";
            litProgName.Text = !string.IsNullOrEmpty(par.parProgramName) ? par.parProgramName : " - ";
            litRecentApplied.Text = !string.IsNullOrEmpty(par.parRecentAppliedName) ? par.parRecentAppliedName : " - ";
            litSubscribe.Text = par.parSubscribe == 1 ? clsAdmin.CONSTADMPARYES : (par.parSubscribe == 0 ? clsAdmin.CONSTADMPARNO : " - ");
            litRegistrationDate.Text = par.parRegisteredDate.ToString("dd MMM yyyy");

            //if (boolPaid || par.parStatus < 0 || par.proDeleted > 0)
            //{
            //    litPaymentMethod.Text = !string.IsNullOrEmpty(par.vGatewayName) ? par.vGatewayName : !string.IsNullOrEmpty(par.parPaymentGatewayName) ? par.parPaymentGatewayName : "-";
            //    litTransID.Text = !string.IsNullOrEmpty(par.paymentRemarks) ? par.paymentRemarks : "-";
            //    litPaymentAmount.Text = par.paymentTotal > 0 ? strCurrency + " " + par.paymentTotal : "-";

            //    if (par.paymentDate.ToShortDateString() != DateTime.MaxValue.ToShortDateString())
            //    {
            //        litPaymentDate.Text = par.paymentDate.ToString("dd MMM yyyy");
            //        trPaymentDate.Visible = true;
            //    }

            //    litPaymentMethod.Visible = true;
            //    litTransID.Visible = true;
            //    litPaymentAmount.Visible = true;
            //}
            //else
            //{
            //    txtPaymentMethod.Visible = true;
            //    txtTransID.Visible = true;
            //    txtPaymentAmount.Visible = true;
            //    litPayAmountCurrency.Visible = true;
            //}
        }
    }

    protected void sendDetails(string strUserName, string strUserEmail, string strRegDate, string strContactNo, string strProgDate, string strOrdNo, decimal decOrdTotal, string strPaymentDetails)
    {
        string strSubject;
        string strBody;

        string strUserSubject;
        string strUserBody;
        string strSenderName = GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString();

        clsConfig config = new clsConfig();
        config.extractItemByNameGroup(clsConfig.CONSTNAMECURRENCY, clsConfig.CONSTGROUPDEFAULT, 1);
        string strCurrency = config.value;

        strSubject = GetGlobalResourceObject("GlobalResource", "contentEmlSubject.Text") + " Registration Details - Payment";
        strBody = "Dear Admin, <br /><br />Payment from " + GetGlobalResourceObject("GlobalResource", "contentWebsiteName.Text").ToString() + " has been made.<br /><br />The details as below:<br />";
        strBody += "Registration Date: " + strRegDate + "<br /><br />";
        strBody += "Name: " + strUserName + "<br />";
        strBody += "Selected Program: " + strOrdNo + "<br />";
        strBody += "Selected Program Date: " + strProgDate + "<br />";
        strBody += "Program Fee: " + strCurrency + " " + decOrdTotal + "<br />";
        strBody += "<br />Here is the payment details:<br />";
        strBody += strPaymentDetails;
        strBody += "<br /><br />Thank you.<br /><br />";
        strBody += GetGlobalResourceObject("GlobalResource", "contentEmlSignature.Text");
        strBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strBody + "</font>";
        strBody = strBody.Replace(Environment.NewLine, "<br />");

        strUserSubject = GetGlobalResourceObject("GlobalResource", "contentEmlSubject.Text") + " Registration Details - Payment";
        strUserBody = "Dear " + strUserName + ",";
        strUserBody += "<br /><br />Thank you for your payment.";
        strUserBody += "<br /><br />Here is the payment details:<br />";
        strUserBody += strPaymentDetails;
        //strUserBody += "<br /><br />You can review and download your order slip from the <a href='" + strOrderStatusUrl + "' title='Check Order Status'>Check Order Status</a> of your account on our Website.";
        strUserBody += "Thank you.<br /><br />";
        strUserBody += GetGlobalResourceObject("GlobalResource", "contentEmlSignature.Text");
        strUserBody = "<font style=\"font-size:12px; font-family:Calibri, Arial, Verdana, Sans-Serif;\">" + strUserBody + "</font>";
        strUserBody = strUserBody.Replace(Environment.NewLine, "<br />");

        //Response.Write(strBody + "<br /><br />" + strUserBody);

        if (clsEmail.send(strSenderName, "", "", "", "", "", strSubject, strBody, 1))
        {
            if (clsEmail.send(strSenderName, "", strUserEmail, "", "", "", strUserSubject, strUserBody, 1))
            {

            }
            else
            {
                clsLog.logErroMsg("Failed to send order details.");
            }
        }
        else
        {
            clsLog.logErroMsg("Failed to send order details.");
        }
    }
    #endregion
}
