﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admPrintReceive.aspx.cs" Inherits="adm_admPrintReceive" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <script type="text/javascript">
        window.print();
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="divPrintContainer">
        <div class="main-content__header">
            <div class="main-content__headerLeft"><asp:Image ID="imgLogo" runat="server" Visible="false" /></div>
            <div class="main-content__headerRight"><span class="spanTitleLarge"><asp:Literal ID="litPageHdr" runat="server" meta:resourcekey="litPageTitle"></asp:Literal></span></div>
        </div>
        <asp:Panel ID="pnlAck" runat="server" CssClass="divAck" Visible="false">
            <asp:Literal ID="litAck" runat="server"></asp:Literal>
        </asp:Panel>
        <div class="divListing">
            <table cellpadding="0" cellspacing="0" class="formTbl">
                <tr>
                    <td>
                        <span class="keyword">Receive From:</span><br /><br />
                        <asp:Literal ID="litVendor" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
        </div>
        <div class="divListing">
            <asp:Repeater ID="rptItems" runat="server" OnItemDataBound="rptItems_ItemDataBound">
                <HeaderTemplate>
                    <table cellpadding="0" cellspacing="0" class="listTbl">
                        <tr>
                            <th>No.</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Unit Price</th>
                            <th>Discount</th>
                            <th>Amount</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                        <tr class="trItem">
                            <td><asp:Literal ID="litNo" runat="server"></asp:Literal></td>
                            <td class="td_norLeft"><asp:Literal ID="litItem" runat="server"></asp:Literal></td>
                            <td><asp:Literal ID="litQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RI_PRODQTY") %>'></asp:Literal></td>
                            <td class="td_right"><asp:Literal ID="litPrice" runat="server"></asp:Literal></td>
                            <td class="td_right"><asp:Literal ID="litDiscount" runat="server"></asp:Literal></td>
                            <td class="td_right"><asp:Literal ID="litAmount" runat="server"></asp:Literal></td>
                        </tr>
                </ItemTemplate>
                <FooterTemplate>
                        <tr class="trFooter">
                            <td colspan="4"></td>
                            <td><br />Bill Discount:</td>
                            <td class="td_right"><asp:Literal ID="litBillDiscount" runat="server"></asp:Literal></td>
                        </tr>
                        <tr class="trFooter">
                            <td colspan="4"></td>
                            <td>Grand Total:<br /><br /></td>
                            <td class="td_right"><asp:Literal ID="litTotal" runat="server"></asp:Literal><br /><br /></td>
                        </tr>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>

