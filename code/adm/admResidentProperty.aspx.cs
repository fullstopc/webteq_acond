﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_adm : System.Web.UI.Page
{
    #region "Properties"
    protected int _mode = 1;
    protected int _memberID = 0;
    #endregion

    #region "Property Methods"
    public int mode 
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    public int memberID 
    {
        get
        {
            if (ViewState["MEMBER_ID"] == null)
            {
                return _memberID;
            }
            else
            {
                return int.Parse(ViewState["MEMBER_ID"].ToString());
            }
        }
        set
        {
            ViewState["MEMBER_ID"] = value;
        }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    memberID = int.Parse(Request["id"]);
                }
                catch (Exception ex)
                {
                    memberID = 0;
                }
            }

            if (memberID > 0)
            {
                mode = 2;
            }

            if (mode == 2)
            {
                fillForm();
            }

        }

    }
    #endregion

    #region "Methods"
    private void fillForm()
    {
        clsMember member = new clsMember();
        if (member.extractByID(memberID))
        {
            litMemberName.Text = string.Format("<b>{0}'s properties:</b>", member.name);
        }
    }
    #endregion

}