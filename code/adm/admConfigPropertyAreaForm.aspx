﻿<%@ Page Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admConfigPropertyAreaForm.aspx.cs" Inherits="adm_adm"  ValidateRequest="false" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/mst/blank.Master" %>
<asp:Content ID="Content" ContentPlaceHolderID="cphHead" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <br />
    <div class="container-fluid">
        <div class="row">
            <label class="col-sm-1"><b>Area:</b></label>
            <div class="col-sm-3">
                <div class="form-group">
                    <asp:TextBox runat="server" ID="txtName" CssClass="form-control"></asp:TextBox>
               
                
                <asp:RequiredFieldValidator ID="rvName" 
                 ControlToValidate="txtName"
                 Display="Dynamic"
                 CssClass="errmsg"
                 ErrorMessage="Please enter Name."
                 ValidationGroup="vg"
                 runat="server"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <asp:LinkButton runat="server" ID="lnkbtnSave" OnClick="lnkbtnSave_Click" ToolTip="Save" CssClass="btn btnSave right" ValidationGroup="vg">Save</asp:LinkButton>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#datepickerFrom, #datepickerTo').datetimepicker({
                format: 'DD MMM YYYY',
            });
        })
    </script>
</asp:Content>