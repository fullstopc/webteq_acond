﻿<%@ Page Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admJob01.aspx.cs" Inherits="adm_adm" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
	<div class="main-content__header">
		<div class="title"><asp:Literal ID="litPageTitle" runat="server"></asp:Literal></div>
		<div class="divListingSplitter"></div>
		<div class="action">
			<asp:Panel ID="pnlListingAction" runat="server" CssClass="divListingAction">
				<asp:LinkButton ID="lnkbtnExport" Visible="false" runat="server" Text="Export" ToolTip="Export" CssClass="hypListingAction export"></asp:LinkButton>
			</asp:Panel>
		</div>
		<div class="divListingSplitter"></div>
		<div class="filter">
			<div class="divFilterContainer">
				<div class="divFilter">
					<asp:TextBox ID="txtKeyword" runat="server" placeholder="Keyword Search" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
					<a href="#" class="btnDropdown" onclick="toggleFilterOption();"></a>
					<asp:Literal ID="litKeywordRule" runat="server" meta:resourcekey="litKeywordRule" visible="false"></asp:Literal>
				</div>
				<div class="divFilterMore">
					<div></div>
					<div>
						<table class="tblSearch">
	
						</table>

					</div>
				</div>
			</div>
			<div class="divFilterAction"><%--<asp:Button ID="lnkbtnSearch" runat="server" Text="" ToolTip="Search" CssClass="btn btnSearch" OnClick="lnkbtnSearch_Click"/>--%></div>
		</div>
	</div>
	<div class="divListing">
		<asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
			<div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
		</asp:Panel>
		<uc:CmnDatatable ID="ucCmnDatatable" runat="server" />
		<asp:HiddenField runat="server" ID="hdnItemID" />
		<asp:LinkButton ID="lnkbtnDelete" runat="server" OnClick="lnkbtnDelete_Click" style="display:none;"></asp:LinkButton>
	</div>
	<script type="text/javascript">
	    var $datatable;

		$(function () {
			var columns = [
					{ "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                    { "data": "res_code", "label": "ID", "order": 1 },
                    { "data": "res_creation", "label": "Order Date", "order": 2, "convert": "datetime", },
                    { "data": "res_service_datetime_from", "label": "Service Date Time", "order": 3, "convert": "datetime",},
					{ "data": "property_area_name", "label": "Area", "order": 4, },
                    { "data": "property_unitno", "label": "Unit No.", "order": 5, },
                    { "data": "mem_name", "label": "Resident", "order": 6, },
                    { "data": "res_service_contact_tel", "label": "Urgent Contact", "order": 7 },
                    { "data": "res_service_team", "label": "Team", "order": 8 },
                    { "data": "order_status", "label": "Status", "order": 9 },
                    { "data": "res_lastupdated", "label": "Last Updated", "order": 10, "convert": "datetime", },
                    { "data": "action", "bSortable": false, "bSearchable": false, "label": "Action", "order": 11 },
			];
			var columnSort = columns.reduce(function (prev, curr) {
				prev[curr.data] = curr.order;
				return prev;
			}, {});

			var columnDefs = [{
				"render": function (data, type, row) {
                    var lnkbtnView = "<a class='lnkbtn lnkbtnView' href='admJob0101.aspx?id=" + row.res_id + "' title='View'></a>";
					return lnkbtnView;
                },
                "targets": columnSort["action"]
            },
            {
                "render": function (data, type, row) {
                    var lnkbtn = "<a href='admJob0101.aspx?id=" + row.res_id + "' title='View'>" + data + "</a>";
                    return lnkbtn;
                },
                "targets": columnSort["res_code"]
            }];
            var properties = {
				columns: columns,
				columnDefs: columnDefs,
				columnSort: columnSort,
				func: "getReservation",
                name: "agent",
                order: [[1, 'desc']]
			};
			$datatable = callDataTables(properties);

			$(document).on("click", ".lnkbtnDelete", function () {
				if (confirm("Are you sure you want to delete agent")) {
					var itemID = $(this).attr("data-id");
					document.getElementById('<%= hdnItemID.ClientID %>').value = itemID;
					document.getElementById('<%= lnkbtnDelete.ClientID %>').click();
				}
			});
			
		});
    </script>
</asp:Content>