﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_admUser0101 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 2;

    protected int _userId = 0;
    protected int _mode = 1;
    protected int _formSect = 1;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int userId
    {
        get
        {
            if (ViewState["USERID"] == null)
            {
                return _userId;
            }
            else
            {
                return int.Parse(ViewState["USERID"].ToString());
            }
        }
        set { ViewState["USERID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set { ViewState["FORMSECT"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    userId = int.Parse(Request["id"]);
                }
                catch (Exception ex)
                {
                    userId = 0;
                }
            }
            else if (Session["NEWUSERID"] != null)
            {
                try
                {
                    userId = int.Parse(Session["NEWUSERID"].ToString());
                }
                catch (Exception ex)
                {
                    userId = 0;
                }
            }
            else if (Session["EDITEDUSERID"] != null)
            {
                try
                {
                    userId = int.Parse(Session["EDITEDUSERID"].ToString());
                }
                catch (Exception ex)
                {
                    userId = 0;
                }
            }

            if (Request["id"] != null || Session["EDITEDUSERID"] != null)
            {
                mode = 2;
            }

            Master.sectId = sectId;
            Master.parentId = sectId;
            Master.subSectId = clsMis.getSubSectId(sectId, formSect, 1);
            Master.id = userId;
            Master.showSideMenu = true;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            ucAdmUserDetails.userId = userId;
            ucAdmUserDetails.mode = mode;
        }

        setPageProperties();
        Session["NEWUSERID"] = null;
        Session["EDITEDUSERID"] = null;
        Session["NOCHANGE"] = null;
        Session["DELETEDUSEREMAIL"] = null;
        Session["ERRMSG"] = null;
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        switch (mode)
        {
            case 1:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleAdd.Text").ToString(), true);
                break;
            case 2:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleEdit.Text").ToString(), true);
                break;
            default:
                break;
        }

        if (!IsPostBack)
        {
            switch (mode)
            {
                case 1://Add User
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleAdd.Text").ToString();
                    break;
                case 2://Edit User
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleEdit.Text").ToString();
                    break;
                default:
                    break;
            }

            if (Session["NEWUSERID"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }
            else if (Session["EDITEDUSERID"] != null)
            {
                if (Session["NOCHANGE"] != null)
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
                }
                else
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";

                    clsMis.resetListing();
                }

                pnlAck.Visible = true;
            }
            else if (Session["DELETEDUSEREMAIL"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }
            else if (Session["ERRMSG"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
            }
        }

        if (pnlForm.Visible)
        {
            ucAdmUserDetails.fill();
        }
    }
    #endregion
}
