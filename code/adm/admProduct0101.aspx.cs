﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using System.IO;

public partial class adm_admProduct0101 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 15; //need change sectId and subsecId according to db

    protected string _currentPageName;
    protected int _mode = 1;
    protected int _prodId = 0;
    protected int _salesId = 0;
    protected int _formSect = 1;
    protected int _specItemId = 0;
    protected int _specGroupId = 0;

    protected bool isAddon = false;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    public string strSavedMsg
    {
        get { return String.Format(GetGlobalResourceObject("GlobalResource", "admSavedMessageWithParams.Text").ToString(), "Product"); }
    }
    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set { ViewState["FORMSECT"] = value; }
    }

    public int prodId
    {
        get
        {
            if (ViewState["PRODID"] == null)
            {
                return _prodId;
            }
            else
            {
                return int.Parse(ViewState["PRODID"].ToString());
            }
        }
        set
        {
            ViewState["PRODID"] = value;
        }
    }

    public int salesId
    {
        get
        {
            if (ViewState["PRODSALESID"] == null)
            {
                return _salesId;
            }
            else
            {
                return Convert.ToInt16(ViewState["PRODSALESID"]);
            }
        }
        set
        {
            ViewState["PRODSALESID"] = value;
        }
    }

    public int specItemId
    {
        get { return _specItemId; }
        set { _specItemId = value; }
    }

    public int specGroupId
    {
        get { return _specGroupId; }
        set { _specGroupId = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["siid"]))
        {
            try
            {
                specItemId = int.Parse(Request["siid"]);
            }
            catch (Exception ex)
            {
                specItemId = 0;
            }
        }

        if (!string.IsNullOrEmpty(Request["sgid"]))
        {
            try
            {
                specGroupId = Convert.ToInt16(Request["sgid"]);
            }
            catch (Exception ex)
            {
                specGroupId = 0;
            }
        }

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    prodId = int.Parse(Request["id"]);
                }
                catch (Exception ex)
                {
                    prodId = 0;
                }
            }
            else if (Session["NEWPRODID"] != null)
            {
                try
                {
                    prodId = int.Parse(Session["NEWPRODID"].ToString());
                }
                catch (Exception ex)
                {
                    prodId = 0;
                }
            }
            else if (Session["EDITPRODID"] != null)
            {
                try
                {
                    prodId = int.Parse(Session["EDITPRODID"].ToString());
                }
                catch (Exception ex)
                {
                    prodId = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["sid"]))
            {
                int intTryParse;
                if (int.TryParse(Request["sid"], out intTryParse))
                {
                    salesId = intTryParse;
                }
                else
                {
                    salesId = int.MaxValue;
                }
            }
            else if (Session["NEWSALESID"] != null)
            {
                try
                {
                    salesId = Convert.ToInt16(Session["NEWSALESID"]);
                }
                catch (Exception ex)
                {
                    salesId = 0;
                }
            }
            else if (Session["EDITSALESID"] != null)
            {
                try
                {
                    salesId = Convert.ToInt16(Session["EDITSALESID"]);
                }
                catch (Exception ex)
                {
                    salesId = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["frm"]))
            {
                int intTryParse;
                if (int.TryParse(Request["frm"], out intTryParse))
                {
                    formSect = intTryParse;
                }
                else
                {
                    formSect = 1;
                }
            }

            switch (formSect)
            {
                case 1: case 2: case 3: case 4: case 6: case 7: case 8: case 9: case 100:
                    if (Request["id"] != null || Session["EDITPRODID"] != null)
                    {
                        mode = 2;
                    }
                    break;
                case 5:
                    if(Request["sid"] != null)
                    {
                        mode = 2;
                    }
                    break;
            }

            //if (Request["id"] != null || Session["EDITPRODID"] != null)
            //{
            //    mode = 2;
            //}

            Session["ORICAT"] = null;
            Session["ORICATNAME"] = null;
            Session["CAT"] = null;
            Session["CATNAME"] = null;
            Session["DELCAT"] = null;

            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMPRODUCTMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.parentId = sectId;
            Master.showSideMenu = true;
            Master.subSectId = clsMis.getSubSectId(sectId, formSect, 1);
            Master.id = prodId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            Session["ORIFILETITLE"] = "";
            Session["ORIFILEUPLOAD"] = "";
            Session["DELFILETITLE"] = "";
            Session["DELFILEUPLOAD"] = "";
            Session["FILETITLE"] = "";
            Session["FILEUPLOAD"] = "";
            Session["TABLEDESC"] = null;
        }

        setPageProperties();
        Session["NEWPRODID"] = null;
        Session["NEWSALESID"] = null;
        Session["EDITPRODID"] = null;
        Session["EDITSALESID"] = null;
        Session["NOCHANGE"] = null;
        Session["DELETEDPRODNAME"] = null;
        Session["DELETEDSALESCODE"] = null;
        Session["ERRMSG"] = null;
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        switch (formSect)
        {
            case 1: case 2: case 3: case 4: case 6: case 7: case 8: case 9:  case 100:
                switch (mode)
                {
                    case 1:
                        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleAdd.Text").ToString(), true);
                        break;
                    case 2:
                        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleEdit.Text").ToString(), true);
                        break;
                    default:
                        break;
                }
                break;
            case 5:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleEdit.Text").ToString(), true);
                break;
            default: break;
        }
        

        if (!IsPostBack)
        {
            clsProduct prod = new clsProduct();

            switch (formSect)
            {
                case 1: case 2: case 3: case 4: case 6: case 7: case 8: case 9:  case 100:
                    switch (mode)
                    {
                        case 1://Add
                            litPageTitle.Text = GetLocalResourceObject("litPageTitleAdd.Text").ToString();

                            int intTotal = prod.getTotalRecord(0);
                            if (clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITPRODUCT, intTotal)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "adm/admProduct01.aspx"); }
                            break;
                        case 2://Edit
                            litPageTitle.Text = GetLocalResourceObject("litPageTitleEdit.Text").ToString();
                            if (prod.extractProdById(prodId, 0))
                            {
                                string desc = "";
                                if (!string.IsNullOrEmpty(prod.prodCode)) { desc += "<div class='item-info'><b>Code</b>: " + prod.prodCode + "</div>"; }
                                if (!string.IsNullOrEmpty(prod.prodName)) { desc += "<div class='item-info'><b>Name</b>: " + prod.prodName + "</div>"; }
                                if (!string.IsNullOrEmpty(prod.prodDName)) { desc += "<div class='item-info'><b>Display Name</b>: " + prod.prodDName + "</div>"; }
                                isAddon = prod.prodAddon == 1;
                                ucAdmPageDesc.desc = desc;
                            }


                            break;
                        default:
                            break;
                    }

                    if (Session["NEWPRODID"] != null)
                    {
                        pnlAck.Visible = true;
                        litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";
                        clsMis.resetListing();
                    }
                    else if (Session["EDITPRODID"] != null)
                    {
                        if (Session["NOCHANGE"] != null)
                        {
                            litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
                        }
                        else
                        {
                            int intTotal = new clsProduct().getTotalRecord(0);
                            string strUrlFormpage = "<a href='admProduct0101.aspx'>Add New</a>";
                            string strUrlListpage = "<a href='admProduct01.aspx'>Back to Listing</a>";

                            litAck.Text = "<div class=\"noticemsg\">" + strSavedMsg + "  " + strUrlListpage + "";
                            if (!clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITPRODUCT, intTotal - 1))
                            {
                                litAck.Text += " & " + strUrlFormpage + "";
                            }
                            litAck.Text += "</div>";
                            clsMis.resetListing();
                        }

                        pnlAck.Visible = true;
                    }
                    else if (Session["DELETEDPRODNAME"] != null)
                    {
                        pnlAck.Visible = true;
                        litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString() + "</div>";

                        clsMis.resetListing();
                    }
                    else if (Session["ERRMSG"] != null)
                    {
                        pnlAck.Visible = true;
                        litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
                    }
                    break;
                case 5:
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleEdit.Text").ToString();

                    prod.extractProdSalesById(salesId);

                    if (Session["NEWSALESID"] != null)
                    {
                        pnlAck.Visible = true;
                        litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";
                        clsMis.resetListing();
                    }
                    else if (Session["EDITSALESID"] != null)
                    {
                        prod.extractProdSalesById(salesId);

                        if (Session["NOCHANGE"] != null)
                        {
                            litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + " for hot sales (" + prod.salesCode + ")" + "</div>";
                        }
                        else
                        {
                            litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + " for hot sales(" + prod.salesCode + ")" + "</div>";

                            clsMis.resetListing();
                        }

                        pnlAck.Visible = true;
                    }
                    else if (Session["DELETEDSALESCODE"] != null)
                    {
                        pnlAck.Visible = true;
                        litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString() + " for hot sales (" + prod.salesCode +")" + "</div>";

                        clsMis.resetListing();
                    }
                    else if (Session["ERRMSG"] != null)
                    {
                        pnlAck.Visible = true;
                        litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
                    }
                    break;
            }
        }

        pnlFormSect1.Visible = false;
        pnlFormSect2.Visible = false;
        pnlFormSect3.Visible = false;
        pnlFormSect4.Visible = false;
        pnlFormSect5.Visible = false;
        pnlFormSect6.Visible = false;
        pnlFormSect7.Visible = false;
        pnlFormSect8.Visible = false;
        pnlFormSect9.Visible = false;
        pnlFormSect100.Visible = false;

        switch (formSect)
        {                
            case 1:
                ucAdmProductDetails.prodId = prodId;
                ucAdmProductDetails.mode = mode;
                ucAdmProductDetails.fill();

                pnlFormSect1.Visible = true;
                break;
            case 2:
                ucAdmProductDesc.mode = mode;
                ucAdmProductDesc.prodId = prodId;
                ucAdmProductDesc.fill();

                pnlFormSect2.Visible = true;
                break;
            case 3:
                ucAdmProductPrice.mode = mode;
                ucAdmProductPrice.prodId = prodId;
                ucAdmProductPrice.fill();

                pnlFormSect3.Visible = true;
                break;
            case 4:
                ucAdmProductRelated.mode = mode;
                ucAdmProductRelated.prodId = prodId;
                ucAdmProductRelated.fill();

                pnlFormSect4.Visible = true;
                break;
            case 5:
                //ucAdmProductHotSales.mode = mode;
                //ucAdmProductHotSales.prodId = prodId;
                //ucAdmProductHotSales.prodSalesId = salesId;
                //ucAdmProductHotSales.fill();

                break;
            case 6:
                pnlFormSect6.Visible = true;
                break;
            case 7:
                ucAdmProductGallery.id = prodId;
                ucAdmProductGallery.mode = mode;
                ucAdmProductGallery.fill();

                pnlFormSect7.Visible = true;
                break;
            case 8:
                ucAdmProductInventory.prodId = prodId;
                ucAdmProductInventory.mode = mode;
                ucAdmProductInventory.fill();

                pnlFormSect8.Visible = true;
                break;

            case 9:
                ucAdmProductAddon.prodId = prodId;
                ucAdmProductAddon.mode = mode;
                ucAdmProductAddon.fill();

                pnlFormSect9.Visible = true;
                break;

            /* Customized - start*/
            case 100:
                ucAdmProductSpec.mode = mode;
                ucAdmProductSpec.formSect = formSect;
                ucAdmProductSpec.prodId = prodId;
                ucAdmProductSpec.siid = specItemId;
                ucAdmProductSpec.sgid = specGroupId;
                ucAdmProductSpec.fill();

                pnlFormSect100.Visible = true;
                break;
            /* Customized - end*/

            default:
                break;
        }
    }
    #endregion
}