﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admProfile.aspx.cs" Inherits="adm_admProfile" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <link  href="<%= System.Configuration.ConfigurationManager.AppSettings["cssBase"] %>cropper.min.css" rel="stylesheet">
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings["jsBase"] %>cropper.min.js"></script>
    <script type="text/javascript">
        function validatePassword_client(source, args) {
            if(args.Value.length < <% =clsAdmin.CONSTADMPASSWORDMINLENGTH %>)
            {
                args.IsValid = false;
            }            
        }
        function validatePasswordRequired_client(source, args) {
            args.IsValid = $("#<%= txtNewPwd.ClientID %>").val().length > 0;    
            source.innerHTML = "This field is required.";
        }
        function validateImage_client(source,args){
            var filename = $("#fileProdImage").val();
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'ico'];
            if ($.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == -1 && filename.length > 0) {
                args.IsValid = false;
                source.innerHTML = "Only formats are allowed : "+fileExtension.join(', ');
            }
        }
    $(document).ready(function(){
        cropInit(<%= aspectRatioX %>,<%= aspectRatioY %>);

        // file upload
        $(".file-group input[type='file']").change(function(){
            var $parent = $(this).parent();
            var $file = $(this);
            var $label = $parent.find(".filename");
            
            var filename = $file.val();
            if(filename.length <= 0){
                filename = "Choose a file";
            }

            $label.html(filename);
        });

    });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>  
    <asp:Panel ID="pnlFormContainer" runat="server" CssClass="divForm"> 
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <asp:Panel ID="pnlForm" runat="server" CssClass="">   
            <table cellpadding="0" cellspacing="0" class="formTbl tblData">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Change Password</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>        
                    <tr>
                        <td class="tdLabel">User ID<span class="attention_unique"></span>:</td>
                        <td class="tdMax"><asp:TextBox ID="txtUserId" runat="server" class="text_fullwidth" MaxLength="20" Enabled="false"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="tdLabel nobr">Current Password<span class="attention_compulsory">*</span>:</td>
                        <td>
                            <asp:TextBox ID="txtCurrentPwd" runat="server" class="text_fullwidth compulsory" MaxLength="20" TextMode="Password"></asp:TextBox>
                            <asp:HiddenField ID="hdnCurrentPwd" runat="server" />
                            <span class="">
                            <%--<asp:RequiredFieldValidator ID="rfvCurrentPwd" runat="server" ErrorMessage="<br />Please enter Current Password." ControlToValidate="txtCurrentPwd" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator>--%>
                            <asp:CustomValidator ID="cvCurrentPwd" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtCurrentPwd" OnServerValidate="validateCurrentPwd_server" OnPreRender="cvCurrentPwd_PreRender" ValidateEmptyText="true"></asp:CustomValidator>                        
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel">New Password<span class="attention_compulsory">*</span>:</td>
                        <td>
                            <asp:TextBox ID="txtNewPwd" runat="server" class="text_fullwidth compulsory" MaxLength="20" TextMode="Password"></asp:TextBox>
                            <span class="">
                            <%--<asp:RequiredFieldValidator ID="rfvNewPwd" runat="server" ErrorMessage="<br />Please enter New Password." ControlToValidate="txtNewPwd" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator>--%>                    
                            <asp:CustomValidator ID="cvNewPwd2" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtNewPwd" ClientValidationFunction="validatePassword_client"></asp:CustomValidator>                          
                            <asp:CustomValidator ID="cvNewPwd3" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtNewPwd" ClientValidationFunction="validatePasswordRequired_client"></asp:CustomValidator>                                  
                            </span>
                        </td>
                    </tr> 
                    <tr>
                        <td class="tdLabel nobr">Confirm Password<span class="attention_compulsory">*</span>:</td>
                        <td>
                            <asp:TextBox ID="txtConfirmPwd" runat="server" class="text_fullwidth compulsory" MaxLength="20" TextMode="Password"></asp:TextBox>
                            <span class="">
                                <%--<asp:RequiredFieldValidator ID="rfvConfirmPwd" runat="server" ErrorMessage="<br />Please enter Confirm Password." ControlToValidate="txtConfirmPwd" Display="Dynamic" CssClass="errmsg"></asp:RequiredFieldValidator>--%>                    
                                <asp:CustomValidator ID="cvConfirmPwd2" runat="server" Display="Dynamic" CssClass="errmsg" ControlToValidate="txtConfirmPwd" ClientValidationFunction="validatePassword_client"></asp:CustomValidator>
                                <asp:CompareValidator ID="cmpConfirmPwd" runat="server" ErrorMessage="<br />Password does not match." ControlToCompare="txtNewPwd" ControlToValidate="txtConfirmPwd" CssClass="errmsg" Display="Dynamic"></asp:CompareValidator>                    
                            </span>
                        </td>
                    </tr>                                                                                                                                                                                           
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                </tbody>

            </table>
            <table cellpadding="0" cellspacing="0" class="formTbl tblData">
                <thead>
                    <tr>
                        <td colspan="2" class="tdSectionHdr">Profile Setting</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>        
                    <tr>
                        <td class="tdLabel">Profile Image<span class="attention_unique"></span>:</td>
                        <td class="tdMax">
                            <asp:Panel ID="pnlFileUpload" runat="server" CssClass="file-group-container">
                                <asp:Panel ID="pnlFileUploadAction" runat="server" CssClass="left">
                                    <div class="file-group">
                                        <input type="file" name="fileProdImage" class="fileImg" id="fileProdImage" accept="image/*">
                                        <label for="fileProdImage">
                                            <i class="material-icons">file_upload</i>
                                            <span class="filename">Choose a file</span>
                                        </label>
                                    </div>
                                </asp:Panel>
                                <i class="material-icons icon-edit">crop</i>
                                <span class="spanTooltip" style="margin-top: 8px;margin-left: 8px;">
                                    <span class="icon"></span>
                                    <span class="msg">
                                        <asp:Literal ID="litUploadRule" runat="server" Text=''></asp:Literal>
                                    </span>
                                </span>
                                <div class="clear"></div>
                            </asp:Panel>
                            <table width="100%" id="tblCropImg" style="display:none;">
                                <thead>
                                    <tr>
                                        <td colspan="2">
                                            <button type="button" class="btnDarkBlue" id="btnConfirmCrop">Crop</button>
                                            <button type="button" class="btnDarkBlue" id="btnCancelCrop">Cancel</button>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="70%">
                                            <div class="divImgContainer" style="width: 100%;">
                                                <img style="max-width:100%;width:100%;height:auto;"/>
                                            </div>
                                        </td>
                                        <td width="30%">
                                            <div class="divImgPreview" style="overflow:hidden;height:200px;width:200px;border:1px solid #ececec;">
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody>
                                    <tr>
                                        <td colspan="2">
                                            <input type="hidden" id="hdnImgX" runat="server" data-id="hdnImgX"/>
                                            <input type="hidden" id="hdnImgY" runat="server" data-id="hdnImgY"/>
                                            <input type="hidden" id="hdnImgWidth" runat="server" data-id="hdnImgWidth"/>
                                            <input type="hidden" id="hdnImgHeight" runat="server" data-id="hdnImgHeight"/>
                                            <input type="hidden" id="hdnImgName" runat="server" data-id="hdnImgName"/>
                                            <input type="hidden" id="hdnImgCropped" runat="server" data-id="hdnImgCropped"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        
                            <asp:HiddenField ID="hdnImage" runat="server" />
                            <asp:HiddenField ID="hdnImageRef" runat="server" />
                            <asp:CustomValidator ID="cvImage" runat="server" Display="Dynamic" CssClass="errmsg" OnServerValidate="validateImage_server" ClientValidationFunction ="validateImage_client" OnPreRender="cvImage_PreRender" ValidationGroup="grpProd"></asp:CustomValidator>
                            <asp:Panel ID="pnlImage" runat="server" Visible="false" CssClass="divSavedImageContainer">
                                <asp:LinkButton ID="lnkbtnDeleteImage" runat="server" Text="Delete" ToolTip="Delete" CausesValidation="false" OnClick="lnkbtnDeleteImage_Click"></asp:LinkButton>
                                <br /><asp:Image ID="imgProfile" runat="server" />
                            </asp:Panel>
                        </td>
                    </tr>                                                                                                                                                                                          
                    <tr>
                        <td class="tdSpacer">&nbsp;</td>
                        <td class="tdSpacer">&nbsp;</td>
                    </tr>
                </tbody>

            </table>
            <table cellpadding="0" cellspacing="0" class="formTbl tblData">
                <tfoot>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlAction" runat="server" CssClass="form__action form__action--floated">
                                <asp:LinkButton ID="lnkbtnSave" runat="server" Text="Save" ToolTip="Save" class="btn btnSave" OnClick="lnkbtnSave_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Back" ToolTip="Back" class="btn btnBack" CausesValidation="false"></asp:LinkButton>
                            </asp:Panel>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </asp:Panel>
        
    </asp:Panel>
</asp:Content>

