﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admCutOffDate01.aspx.cs" Inherits="adm_admCutOffDate01" %>
<%@ MasterType VirtualPath="~/mst/private.Master" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>

<%--Register customized control - start--%>
<%@ Register Src="~/project/yshamper/ctrl/ucAdmCutOffDate.ascx" TagName="AdmCutOffDate" TagPrefix="uc" %>
<%--Register customized control - end--%>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    
    <%--Customized control - start--%>
    <asp:Panel ID="pnlForm" runat="server" CssClass="divForm">
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <uc:AdmCutOffDate ID="ucAdmCutOffDate" runat="server" pageListingURL="admCutOffDate01.aspx" />
    </asp:Panel>
    <%--Customized control - end--%>
</asp:Content>