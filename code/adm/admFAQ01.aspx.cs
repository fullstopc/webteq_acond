﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.IO;


public partial class adm_admFAQ01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 38;
    protected int _faqId = 0;
    protected int _mode = 1;
    protected string _gvSortExpression = "FAQ_ID";
    protected string _gvSortDirection = clsAdmin.CONSTSORTDESC;
    protected string _gvSort = "";
    protected int itemCount = 0;
    protected int itemTotal = 0;
    #endregion


    #region "Property Methods"
    protected string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    protected int faqId
    {
        get
        {
            if (ViewState["FAQID"] == null)
            {
                return _faqId;
            }
            else
            {
                return Convert.ToInt16(ViewState["FAQID"]);
            }
        }
        set { ViewState["FAQID"] = value; }
    }

    protected int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    protected string gvSortExpression
    {
        get { return ViewState["GVSORTEXPRESSION"] as string ?? _gvSortExpression; }
        set { ViewState["GVSORTEXPRESSION"] = value; }
    }

    protected string gvSortDirection
    {
        get { return ViewState["GVSORTDIRECTION"] as string ?? _gvSortDirection; }
        set { ViewState["GVSORTDIRECTION"] = value; }
    }

    protected string gvSort
    {
        get { return ViewState["GVSORT"] as string ?? _gvSort; }
        set { ViewState["GVSORT"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            Master.sectId = sectId;

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;
                if (int.TryParse(Request["id"], out intTryParse))
                {
                    faqId = intTryParse;
                }
                else
                {
                    faqId = int.MaxValue;
                }
            }

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                mode = 2;
            }

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMELISTINGSIZE, clsConfig.CONSTGROUPOTHERSETTINGS, 1);

            gvFaq.PagerSettings.FirstPageText = GetGlobalResourceObject("GlobalResource", "contentAdmFirstPageText.Text").ToString();
            gvFaq.PagerSettings.LastPageText = GetGlobalResourceObject("GlobalResource", "contentAdmLastPageText.Text").ToString();
            gvFaq.PageSize = (!string.IsNullOrEmpty(config.value)) ? Convert.ToInt16(config.value) : clsAdmin.CONSTADMPAGESIZE;
            //gvFaq.PageSize = clsAdmin.CONSTADMPAGESIZE;

            gvSort = gvSortExpression + " " + gvSortDirection;

            bindGV(gvFaq, "FAQ_ORDER ASC");
            setPageProperties();
        }

        Session["NEWFAQID"] = null;
        Session["EDITEDFAQID"] = null;
        Session["NOCHANGE"] = null;
        Session["DELETEDFAQ"] = null;
    }

    protected void validateQuesName_server(object source, ServerValidateEventArgs args)
    {
        if (!string.IsNullOrEmpty(txtQuesName.Text.Trim())) { args.IsValid = true; }
        else
        {
            args.IsValid = false;
            cvQuesName.ErrorMessage = "<br />Please enter question.";
        }

        registerCKEditorScript();
    }

    protected void cvQuesName_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("quesname")))
        {
            string strJS = null;
            strJS = "<script type = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtQuesName.ClientID + "', document.all['" + cvQuesName.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "quesname", strJS, false);
        }
    }

    protected void validateAnsName_server(object source, ServerValidateEventArgs args)
    {
        if (!string.IsNullOrEmpty(txtAnsName.Text.Trim())) { args.IsValid = true; }
        else
        {
            args.IsValid = false;
            cvAnsName.ErrorMessage = "<br />Please enter answer.";
        }

        registerCKEditorScript();
    }

    protected void cvAnsName_PreRender(object sender, EventArgs e)
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("ansname")))
        {
            string strJS = null;
            strJS = "<script type = \"text/javascript\">";
            strJS += "ValidatorHookupControlID('" + txtAnsName.ClientID + "', document.all['" + cvAnsName.ClientID + "']);";
            strJS += "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "ansname", strJS, false);
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strQuesName;
            string strAnsName;
            int intActive = 0;

            strQuesName = txtQuesName.Text.Trim();
            strAnsName = txtAnsName.Text.Trim();
            
            if (chkboxActive.Checked) { intActive = 1; }

            clsFAQ faq = new clsFAQ();
            int intRecordAffected = 0;

            if (mode == 1)
            {
                intRecordAffected = faq.addFaq(strQuesName, strAnsName, intActive, int.Parse(Session["ADMID"].ToString()));

                if (intRecordAffected == 1)
                {
                    Session["NEWFAQID"] = faq.faqId;

                    Response.Redirect(currentPageName);
                }
                else
                {
                    pnlAck.Visible = true;
                    litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new question. Please try again.</div>";
                }
            }
            else if (mode == 2)
            {
                Boolean boolSuccess = true;
                Boolean boolEdited = false;

                if (!faq.isExactSameFaq(faqId, strQuesName, strAnsName, intActive))
                {
                    intRecordAffected = faq.updateFaqById(faqId, strQuesName, strAnsName, intActive,  Convert.ToInt16(Session["ADMID"]));

                    if (intRecordAffected == 1)
                    {
                        boolEdited = true;
                    }
                    else
                    {
                        boolSuccess = false;
                    }
                }

                if (boolEdited)
                {
                    Session["EDITEDFAQID"] = faq.faqId;

                    Response.Redirect(currentPageName);
                }
                else
                {
                    if (!boolSuccess)
                    {
                        faq.extractFaqById(faqId, 0);

                        pnlAck.Visible = true;
                        litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit question). Please try again.</div>";
                    }
                    else
                    {
                        Session["EDITEDFAQID"] = faqId;
                        Session["NOCHANGE"] = 1;

                        Response.Redirect(currentPageName);
                    }
                }
            }
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string strDeletedFaq = "";
        clsMis.performDeleteFaq(faqId, ref strDeletedFaq);

        Session["DELETEDFAQ"] = strDeletedFaq;

        Response.Redirect(currentPageName);
    }

    protected void gvFaq_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell tc in e.Row.Cells)
            {
                if (tc.HasControls())
                {
                    LinkButton lnkbtn = (LinkButton)tc.Controls[0];
                    //Image icon = new Image();
                    //icon.ImageUrl = "../img/adm/btn-cancel.gif";

                    if (lnkbtn != null && gvSortExpression == lnkbtn.CommandArgument)
                    {
                        //tc.Controls.Add(icon);
                        string strClass = (gvSortDirection == "DESC") ? "desc" : "asc";
                        tc.CssClass = strClass;
                    }


                }
            }
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            itemCount += 1;         

            LinkButton lnkbtnEdit = (LinkButton)e.Row.FindControl("lnkbtnEdit");
            LinkButton lnkbtnDelete = (LinkButton)e.Row.FindControl("lnkbtnDelete");
            LinkButton lnkbtnUp = (LinkButton)e.Row.FindControl("lnkbtnUp");
            LinkButton lnkbtnDown = (LinkButton)e.Row.FindControl("lnkbtnDown");

            int faqid = Convert.ToInt16(gvFaq.DataKeys[e.Row.RowIndex].Value.ToString());
          
            lnkbtnEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteDirGroup.Text").ToString().Replace(clsAdmin.CONSTDIRGROUP, "question") + "');";
            lnkbtnUp.Text = GetGlobalResourceObject("GlobalResource", "contentAdmUp.Text").ToString();
            lnkbtnDown.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDown.Text").ToString();

            if (itemCount == 1)
            {
                LinkButton lnkbtnMoveUp = (LinkButton)e.Row.FindControl("lnkbtnMoveUp");
                lnkbtnUp.Enabled = false;
                lnkbtnUp.CssClass = "btnSPDisabled";
            }
            else if (itemCount == itemTotal)
            {
                LinkButton lnkbtnMoveDown = (LinkButton)e.Row.FindControl("lnkbtnMoveDown");
                lnkbtnDown.Enabled = false;
                lnkbtnDown.CssClass = "btnSPDisabled";
            }
        }
    }

    protected void gvFaq_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "cmdEdit")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int faqid = Convert.ToInt16(gvFaq.DataKeys[rowIndex].Value.ToString());
            Response.Redirect(currentPageName + "?id=" + faqid);
        }
        else if (e.CommandName == "cmdDelete")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int faqid = Convert.ToInt16(gvFaq.DataKeys[rowIndex].Value.ToString());

            clsFAQ faq = new clsFAQ();
            string strDeletedFaq = "";

            clsMis.performDeleteFaq(faqid, ref strDeletedFaq);

            Session["DELETEDFAQ"] = strDeletedFaq;
            
            Response.Redirect(currentPageName);
        }
        else if (e.CommandName == "cmdUp")
        {
            string strArg = e.CommandArgument.ToString();

            string[] strArgSplit = strArg.Split((char)'|');

            int intOrder = Convert.ToInt16(strArgSplit[1]);
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int faqid = Convert.ToInt16(gvFaq.DataKeys[rowIndex].Value.ToString());

            int intPrevOrder = 0;
            int intPrevId = 0;

            clsFAQ faq = new clsFAQ();
            if (faq.extractFaqPrevPage(intOrder, 0))
            {
                intPrevOrder = faq.faqOrder;
                intPrevId = faq.faqId;
                faq.updateFaqOrderById2(faqid, intPrevOrder);
                faq.updateFaqOrderById2(intPrevId, intOrder);

                clsMis.resetListing();
                bindGV(gvFaq, "FAQ_ORDER ASC");
            }
        }
        else if (e.CommandName == "cmdDown")
        {
            string strArg = e.CommandArgument.ToString();

            string[] strArgSplit = strArg.Split((char)'|');

            int intOrder = Convert.ToInt16(strArgSplit[1]);
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int faqid = Convert.ToInt16(gvFaq.DataKeys[rowIndex].Value.ToString());

            int intNextOrder = 0;
            int intNextId = 0;

            clsFAQ faq = new clsFAQ();
            if (faq.extractFaqNextPage(intOrder, 0))
            {
                intNextOrder = faq.faqOrder;
                intNextId = faq.faqId;

                faq.updateFaqOrderById2(faqid, intNextOrder);
                faq.updateFaqOrderById2(intNextId, intOrder);

                clsMis.resetListing();
                bindGV(gvFaq, "FAQ_ORDER ASC");
            }
        }
    }

    protected void gvFaq_Sorting(object sender, GridViewSortEventArgs e)
    {
        string strSortExpDir = "";
        if (e.SortExpression != gvSortExpression)
        {
            gvSortExpression = e.SortExpression;
            gvSortDirection = clsAdmin.CONSTSORTASC;
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }
        else
        {
            switch (gvSortDirection)
            {
                case clsAdmin.CONSTSORTASC:
                    gvSortDirection = clsAdmin.CONSTSORTDESC;
                    break;
                case clsAdmin.CONSTSORTDESC:
                    gvSortDirection = clsAdmin.CONSTSORTASC;
                    break;
            }
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }

        gvSort = strSortExpDir;
        bindGV(gvFaq, strSortExpDir);
    }

    protected void gvFaq_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvFaq.PageIndex = e.NewPageIndex;
        bindGV(gvFaq, "FAQ_ORDER ASC");
    }

    protected void lnkbtnExport_Click(object sender, EventArgs e)
    {
        ExportGV(gvFaq);
    }

    public override void VerifyRenderingInServerForm(Control control)
    {

    }
    #endregion


    #region "Methods"
    protected void registerCKEditorScript()
    {
        if ((!Page.ClientScript.IsStartupScriptRegistered("CKEDITOR")))
        {
            int intFullPath = Convert.ToInt16(Session[clsAdmin.CONSTPROJECTFULLPATH]) == 1 ? clsAdmin.CONSTFULLPATHACTIVE : clsAdmin.CONSTFULLPATHDEFAULT;
            string strJS = null;
            strJS = "<script type=\"text/javascript\">" +
                    "CKEDITOR.replace('" + txtAnsName.ClientID + "'," +
                    "{" +
                    "width:'99%'," +
                    "height:'300'," +
                    "toolbar: 'User3'," +
                    "baseHref: '" + Session[clsAdmin.CONSTPROJECTUSERVIEWURL] + "/" + "',";
            strJS += "filebrowserUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSADMCMSVIEW + "&command=QuickUpload&type=Files&path=" + intFullPath + "'," +
                     "filebrowserImageUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSADMCMSVIEW + "&command=QuickUpload&type=Images&path=" + intFullPath + "'," +
                     "filebrowserFlashUploadUrl:'" + System.Configuration.ConfigurationManager.AppSettings["fckfinderBase"] + "core/connector/aspx/connector.aspx?view=" + clsAdmin.CONSTCMSADMCMSVIEW + "&command=QuickUpload&type=Flash&path=" + intFullPath + "'" +
                     "}" +
                     ");";

            strJS += "CKEDITOR.add;";

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMEEDITORSTYLE, clsConfig.CONSTGROUPWEBSITE, 1);
            string strEditorStyle = config.value;
            if (!string.IsNullOrEmpty(strEditorStyle))
            {
                strJS += "CKEDITOR.config.contentsCss = 'body {" + strEditorStyle + "}';";
            }

            strJS += "CKEDITOR.config.stylesCombo_stylesSet= '" + clsAdmin.CONSTADMSTYLESET + ":" + System.Configuration.ConfigurationManager.AppSettings["jsBase"] + clsAdmin.CONSTADMSTYLESETFILE + "';" +
                     "</script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "CKEDITOR", strJS, false);
        }
    }

    protected void setPageProperties()
    {
        registerCKEditorScript();

        litPageTitle.Text = GetLocalResourceObject("litPageTitle.Text").ToString();
        lblQuesName.Text = "Question";
        lblAnsName.Text = "Answer";
        lblActive.Text = "Active";
                
        lnkbtnCancel.OnClientClick = "javascript:document.location.href='" + currentPageName + "'; return false;";
        lnkbtnDelete.OnClientClick = "javascript:return confirm('" + GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteDirGroup.Text").ToString().Replace(clsAdmin.CONSTDIRGROUP, "question") + "');";

        if (mode == 2)
        {
            if (Session["EDITEDFAQID"] == null && Session["DELETEDFAQ"] == null) { fillForm(); }

            lnkbtnCancel.Visible = true;
            lnkbtnDelete.Visible = true;
        }

        if (Session["NEWFAQID"] != null && Session["NEWFAQID"] != "")
        {
            clsFAQ faq = new clsFAQ();
            faq.extractFaqById(Convert.ToInt16(Session["NEWFAQID"]), 0);

            pnlAck.Visible = true;
            litAck.Text = "<div class=\"noticemsg\">Added successfully.</div>";

            clsMis.resetListing();
        }
        else if (Session["EDITEDFAQID"] != null && Session["EDITEDFAQID"] != "")
        {
            clsFAQ faq = new clsFAQ();
            faq.extractFaqById(Convert.ToInt16(Session["EDITEDFAQID"]), 0);

            if (Session["NOCHANGE"] != null)
            {
                litAck.Text = "<div class=\"noticemsg\">No changes detected.</div>";
            }
            else
            {
                litAck.Text = "<div class=\"noticemsg\">Edited successfully.</div>";

                clsMis.resetListing();
            }

            pnlAck.Visible = true;
        }
        else if (Session["DELETEDFAQ"] != null && Session["DELETEDFAQ"] != "")
        {
            string strAck = "(" + Session["DELETEDFAQ"] + ") has been deleted successfully.";
            clsMis.resetListing();
        }
    }

    protected void fillForm()
    {
        registerCKEditorScript();

        clsFAQ faq = new clsFAQ();

        if (faq.extractFaqById(faqId, 0))
        {
            txtQuesName.Text = faq.faqQues;
            txtAnsName.Text = faq.faqAns;

            if (faq.faqActive != 0) { chkboxActive.Checked = true; }
            else { chkboxActive.Checked = false; }
        }
        else
        {
            pnlAck.Visible = true;
            litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "unknownRequest.Text").ToString() + "</div>";
        }
    }

    protected void bindGV(GridView gvRef, string gvSortExpDir)
    {
        clsFAQ faq = new clsFAQ();
        DataSet ds = new DataSet();
        DataView dv = new DataView();
        ds = faq.getFaqListByFaqId();

        dv = new DataView(ds.Tables[0]);

        if (gvSortExpDir != "")
        {
            dv.Sort = gvSortExpDir;
        }

        Session["FAQDV"] = dv;

        itemCount = 0;
        itemTotal = dv.Count;

        gvRef.DataSource = dv;
        gvRef.DataBind();

        litGroupFound.Text = dv.Count + " record(s) listed.";
        
        if (dv.Count > 0)
        {
            
            lnkbtnExport.Enabled = true;
            pnlBtn.Visible = true;
            lnkbtnExport.CssClass = "btn2 btnExport";
        }
        else
        {
            lnkbtnExport.Enabled = false;
            lnkbtnExport.CssClass = "btn2 btnExport btnDisabled";
        }

        registerCKEditorScript();
    }

    protected void ExportGV(GridView gvRef)
    {
        DateTime dt = DateTime.Now;
        string strFileName = clsAdmin.CONSTADMFAQNAME.Replace(" ", "").ToUpper() + "LIST_" + dt.Day + dt.Month + dt.Year + ".xls";
        string strAttachment = "attachment; filename=" + strFileName;

        Response.ClearContent();
        Response.AddHeader("content-disposition", strAttachment);
        Response.ContentType = "application/vnd.xls";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        DataView dv = new DataView();
        dv = (DataView)Session["FAQDV"];

        gvRef.AllowPaging = false;
        gvRef.DataSource = dv;
        gvRef.DataBind();

        int intColHide = gvRef.Columns.Count - 1;

        for (int i = 0; i < gvRef.Columns.Count; i++)
        {
            if (gvRef.HeaderRow.Cells[i].CssClass == "hiddenCol")
            {
                gvRef.HeaderRow.Cells[i].CssClass = "showCol";
            }

            gvRef.HeaderRow.Cells[i].Enabled = false;
        }

        gvRef.HeaderRow.Cells[intColHide].Visible = false;

        for (int i = 0; i < gvRef.Rows.Count; i++)
        {
            GridViewRow row = gvRef.Rows[i];

            for (int j = 0; j < gvRef.Columns.Count; j++)
            {
                if (row.Cells[j].CssClass == "hiddenCol")
                {
                    row.Cells[j].CssClass = "showCol";
                }
            }
        }

        gvRef.Columns[intColHide].Visible = false;

        gvRef.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();

        Session["FAQDV"] = null;
    }
    #endregion
}