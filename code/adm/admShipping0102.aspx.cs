﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class adm_admShipping0102 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 25;

    protected int _waiverId = 0;
    protected int _mode = 1;
    protected int _formSect = 2;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int waiverId
    {
        get
        {
            if (ViewState["WAIVERID"] == null)
            {
                return _waiverId;
            }
            else
            {
                return Convert.ToInt16(ViewState["WAIVERID"]);
            }
        }
        set { ViewState["WAIVERID"] = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set
        {
            ViewState["FORMSECT"] = value;
        }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    waiverId = Convert.ToInt16(Request["id"]);
                }
                catch (Exception ex)
                {
                    waiverId = 0;
                }
            }
            else if (Session["NEWWAIVERID"] != null)
            {
                try
                {
                    waiverId = Convert.ToInt16(Session["NEWWAIVERID"]);
                }
                catch (Exception ex)
                {
                    waiverId = 0;
                }
            }
            else if (Session["EDITEDWAIVERID"] != null)
            {
                try
                {
                    waiverId = Convert.ToInt16(Session["EDITEDWAIVERID"]);
                }
                catch (Exception ex)
                {
                    waiverId = 0;
                }
            }

            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMSHIPPINGMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.subSectId = clsMis.getSubSectId(sectId, formSect, 1);
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();
            Master.id = waiverId;
            Master.showSideMenu = true;
            Master.boolShowSibling = false;

            if (Request["id"] != null || Session["EDITEDWAIVERID"] != null)
            {
                mode = 2;
            }
        }

        setPageProperties();
        Session["NEWWAIVERID"] = null;
        Session["EDITEDWAIVERID"] = null;
        Session["NOCHANGE"] = null;
        Session["DELETEDWAIVERID"] = null;
        Session["ERRMSG"] = null;
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        switch (mode)
        {
            case 1:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleAdd.Text").ToString(), true);
                break;
            case 2:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleEdit.Text").ToString(), true);
                break;
            default:
                break;
        }

        if (!IsPostBack)
        {
            switch (mode)
            {
                case 1://Add
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleAdd.Text").ToString();
                    break;
                case 2://Edit
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleEdit.Text").ToString();
                    break;
                default:
                    break;
            }

            if (Session["NEWWAIVERID"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }
            else if (Session["EDITEDWAIVERID"] != null)
            {
                if (Session["NOCHANGE"] != null)
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
                }
                else
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";

                    clsMis.resetListing();
                }

                pnlAck.Visible = true;
            }
            else if (Session["DELETEDWAIVERID"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }
            else if (Session["ERRMSG"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
            }
        }

        ucAdmShippingWaiver.waiverId = waiverId;
        ucAdmShippingWaiver.mode = mode;
        ucAdmShippingWaiver.fill();

        clsConfig config = new clsConfig();
        int intType = !string.IsNullOrEmpty(config.shippingType) ? Convert.ToInt16(config.shippingType) : 1;
        if (intType != clsAdmin.CONSTSHIPPINGTYPE1)
        {
            Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "adm/admShipping01.aspx");
        }
    }
    #endregion
}
