﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admShipping01.aspx.cs" Inherits="adm_admShipping01" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/mst/private.Master" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title"><asp:Literal ID="litPageTitle" runat="server" meta:resourcekey="litPageTitle"></asp:Literal></div>
        <div class="divListingSplitter"></div>
        <div class="action">
			<asp:Panel ID="pnlListingAction" runat="server" CssClass="divListingAction">
                <asp:HyperLink ID="hypAddShipping" runat="server" Text="Add Shipping" ToolTip="Add Shipping" NavigateUrl="admShipping0101.aspx" CssClass="hypListingAction add"></asp:HyperLink>
				<div class="divListingSplitter"></div>
                <asp:LinkButton ID="lnkbtnExport" runat="server" Text="Export to Excel" ToolTip="Export to Excel" CssClass="hypListingAction export" OnClick="lnkbtnExport_Click"></asp:LinkButton>
            </asp:Panel>
        </div>
        <div class="divListingSplitter"></div>
        <div class="filter">
            <div class="divFilterContainer">
                <div class="divFilter">
                    <asp:TextBox ID="txtKeyword" runat="server" placeholder="Keyword Search" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
                    <a href="#" class="btnDropdown" onclick="toggleFilterOption();"></a>
                    <asp:Literal ID="litKeywordRule" runat="server" meta:resourcekey="litKeywordRule" visible="false"></asp:Literal>
                </div>
                <div class="divFilterMore">
                    <div></div>
                    <div>
                        <table class="tblSearch">
							<tr>
                                <td class="">
									<div class="divFilterLabel">Country:</div>
                                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="ddl_medium"></asp:DropDownList>
                                </td>
								<td></td>
                            </tr>

							
                        </table>

                    </div>
                </div>
            </div>
            <div class="divFilterAction"><asp:Button ID="lnkbtnSearch" runat="server" Text="" ToolTip="Search" CssClass="btn btnSearch" OnClick="lnkbtnSearch_Click"/></div>
        </div>
    </div>
    <div class="divListing">
        <div class="divSectionTitle">SHIPPING COST BY LOCATION</div>
        <div class="divListingHdr">
            <div class="divListingHdrLeft"><asp:Literal ID="litFound" runat="server"></asp:Literal></div>
        </div>
        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
            <div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
        </asp:Panel>
        <div class="divListingData">
            <asp:GridView ID="gvShipping" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom" CssClass="dataTbl" OnRowDataBound="gvShipping_RowDataBound" OnSorting="gvShipping_Sorting" OnPageIndexChanging="gvShipping_PageIndexChanging" OnRowCommand="gvShipping_RowCommand" OnRowCreated="gvItems_RowCreated" DataKeyNames="SHIP_ID" EmptyDataText="No record found.">
                <PagerStyle CssClass="pg" HorizontalAlign="Right" />
                <AlternatingRowStyle CssClass="td_alt" />
                <Columns>
                    <asp:TemplateField HeaderText="no.">
                        <ItemStyle CssClass="tdNo" />
                        <ItemTemplate>
                            <asp:Literal ID="litNo" runat="server" Text="<%#Container.DataItemIndex + 1 %>"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="shipping company" DataField="V_SHIPCOMPNAME" Visible="false" />
                    <asp:BoundField HeaderText="shipping method" DataField="V_SHIPMETHODNAME" Visible="false" />
                    <asp:BoundField HeaderText="country code" DataField="SHIP_COUNTRY" HeaderStyle-CssClass="hiddenCol" ItemStyle-CssClass="hiddenCol" />
                    <asp:BoundField HeaderText="country" DataField="V_SHIPCOUNTRYNAME" SortExpression="V_SHIPCOUNTRYNAME" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="shipCountry" />
                    <asp:BoundField HeaderText="state" DataField="SHIP_STATE" SortExpression="SHIP_STATE" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="city" DataField="SHIP_CITY" SortExpression="SHIP_CITY" ItemStyle-HorizontalAlign="Center" Visible="false" />
                    <asp:BoundField HeaderText="first block (<!shippingunit!>)" DataField="SHIP_KG_FIRST" SortExpression="SHIP_KG_FIRST" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                    <asp:BoundField HeaderText="first block (<!currency!>)" DataField="SHIP_FEE_FIRST" SortExpression="SHIP_FEE_FIRST" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                    <asp:BoundField HeaderText="subsequent block (<!shippingunit!>)" DataField="SHIP_KG" SortExpression="SHIP_KG" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                    <asp:BoundField HeaderText="subsequent block (<!currency!>)" DataField="SHIP_FEE" SortExpression="SHIP_FEE" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                    <asp:BoundField HeaderText="shipping cost per item (<!currency!>)" DataField="SHIP_FEE" SortExpression="SHIP_FEE" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" Visible="false"/>
                    <asp:BoundField HeaderText="free after" DataField="SHIP_FEEFREE" SortExpression="SHIP_FEEFREE" />
                    <asp:TemplateField HeaderText="active" SortExpression="SHIP_ACTIVE" HeaderStyle-CssClass="alignCenter">
                        <ItemStyle CssClass="alignCenter" />
                        <ItemTemplate>
                            <asp:Literal ID="litActive" runat="server" Text='<%#clsMis.formatYesNo(Convert.ToInt16(DataBinder.Eval(Container.DataItem, "SHIP_ACTIVE"))) %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="action" HeaderStyle-CssClass="alignCenter">
                        <ItemStyle CssClass="tdAct" />
                        <ItemTemplate>
                            <asp:HyperLink ID="hypEdit" CssClass="lnkbtn lnkbtnEdit" runat="server"></asp:HyperLink>
                            <%--<asp:LinkButton ID="lnkbtnEdit" runat="server" CausesValidation="false" CommandName="cmdEdit" CssClass="lnkbtn lnkbtnEdit"></asp:LinkButton>--%>
                            <%# (Convert.ToInt16(DataBinder.Eval(Container.DataItem, "SHIP_DEFAULT")) > 0) ? string.Empty : "<span class='spanSplitter'>|</span>"%>
                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CausesValidation="false" CommandName="cmdDelete" CssClass="lnkbtn lnkbtnDelete"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <asp:Panel ID="pnlWaiver" runat="server" CssClass="divListing" Visible="false">
        <div class="divSectionTitle">SHIPPING COST WAIVER</div>
        <div class="divListingHdr">
            <div class="divListingHdrLeft"><asp:Literal ID="litFound2" runat="server"></asp:Literal></div>
            <asp:Panel ID="pnlListingAction2" runat="server" CssClass="divListingAction right">
                <asp:HyperLink ID="hypAddWaiver" runat="server" Text="Add Shipping Waiver" ToolTip="Add Shipping Waiver" NavigateUrl="admShipping0102.aspx" CssClass="btn2 btnAdd"></asp:HyperLink>
                <asp:LinkButton ID="lnkbtnExportWaiver" runat="server" Text="Export" ToolTip="Export" CssClass="btn" OnClick="lnkbtnExportWaiver_Click"></asp:LinkButton>
            </asp:Panel>
        </div>
        <asp:Panel ID="pnlAckWaiver" runat="server" Visible="false" CssClass="divAck">
            <div><asp:Literal ID="litAckWaiver" runat="server"></asp:Literal></div>
        </asp:Panel>
        <asp:UpdatePanel ID="upnlWaiver" runat="server">
            <ContentTemplate>
                <asp:UpdateProgress ID="uprgWaiver" runat="server" AssociatedUpdatePanelID="upnlWaiver" DynamicLayout="true">
                    <ProgressTemplate>
                        <uc:AdmLoading ID="ucAdmLoading" runat="server" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <div class="divListingData">
                    <asp:GridView ID="gvWaiver" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom" CssClass="dataTbl" OnRowDataBound="gvWaiver_RowDataBound" OnSorting="gvWaiver_Sorting" OnPageIndexChanging="gvWaiver_PageIndexChanging" OnRowCommand="gvWaiver_RowCommand" OnRowCreated="gvItems_RowCreated" DataKeyNames="WAIVER_ID" EmptyDataText="No record found.">
                        <PagerStyle CssClass="pg" HorizontalAlign="Right" />
                        <AlternatingRowStyle CssClass="td_alt" />
                        <Columns>
                            <asp:TemplateField HeaderText="NO.">
                                <ItemStyle CssClass="tdNo" />
                                <ItemTemplate>
                                    <asp:Literal ID="litNo" runat="server" Text="<%#Container.DataItemIndex + 1 %>"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="FROM (<!currency!>)" DataField="WAIVER_FROM" SortExpression="WAIVER_FROM" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                            <asp:BoundField HeaderText="TO (<!currency!>)" DataField="WAIVER_TO" SortExpression="WAIVER_TO" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                            <asp:BoundField HeaderText="WAIVE (<!currency!>)" DataField="WAIVER_VALUE" SortExpression="WAIVER_VALUE" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                            <asp:TemplateField HeaderText="ACTION" HeaderStyle-CssClass="alignCenter">
                                <ItemStyle CssClass="tdAct" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypEdit" CssClass="lnkbtn lnkbtnEdit" runat="server"></asp:HyperLink>
                                    <%--<asp:LinkButton ID="lnkbtnEdit" runat="server" CausesValidation="false" CommandName="cmdEdit" CssClass="lnkbtn lnkbtnEdit"></asp:LinkButton>--%>
                                    <span class='spanSplitter'>|</span>
                                    <asp:LinkButton ID="lnkbtnDelete" runat="server" CausesValidation="false" CommandName="cmdDelete" CssClass="lnkbtn lnkbtnDelete"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>