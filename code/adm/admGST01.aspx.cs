﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_admGST01 : System.Web.UI.Page
{

    #region "Properties"
    protected int sectId = 2;
    protected int _mode = 1;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            //if (!string.IsNullOrEmpty(Request["id"]))
            //{
            //    int intTryParse;
            //    if (int.TryParse(Request["id"], out intTryParse))
            //    {
            //        ucAdmControlPanel.areaId = intTryParse;
            //        ucAdmControlPanel.currId = intTryParse;
            //    }
            //}

            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMGSTSETTINGS, 1);
            sectId = adm.pageId;
            
            Master.sectId = sectId;
            Master.parentId = 1;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            setPageProperties();
        }

        //ucAdmControlPanel.fill();

        Session["EDITEDCMSCP"] = null;
        Session["NOCHANGE"] = null;
        Session["ERRMSG"] = null;
        Session["ADMPAGENAME"] = null;
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        if (Session["EDITEDCMSCP"] != null)
        {
            if (Session["NOCHANGE"] != null)
            {
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
            }
            else
            {
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }

            pnlAck.Visible = true;
        }
        else if (Session["ERRMSG"] != null)
        {
            pnlAck.Visible = true;
            litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
        }
    }

   
    #endregion
}
