﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admOrder01.aspx.cs" Inherits="adm_admOrder01" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmCheckAccess.ascx" TagName="AdmCheckAccess" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmLoading.ascx" TagName="AdmLoading" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <script type="text/javascript">
        var txtOrdStartDate;
        var txtOrdEndDate;
        
        function initSect3() {
            txtOrdStartDate = document.getElementById("<%= txtOrdStartDate.ClientID %>");
            txtOrdEndDate = document.getElementById("<%= txtOrdEndDate.ClientID %>");

            $(function () {
                var option = {
                    dateFormat: 'd M yy',
                    changeMonth: true,
                    changeYear: true
                };

                var optionFrom = $.extend({
                    onSelect: function (date) {
                        $('#<% =txtOrdEndDate.ClientID %>').datepicker("option", { minDate: (date) });
                    }
                }, option);

                var optionTo = {
                    minDate:  $("#<% =txtOrdStartDate.ClientID %>").datepicker('getDate'),
                }
                $("#<% =txtOrdStartDate.ClientID %>").datepicker(optionFrom);
                $("#<% =txtOrdEndDate.ClientID %>").datepicker($.extend(option, optionTo));
            });
        }

        function validateOrdPeriod_client(source, args) {
            var boolValid = false;
            source.errormessage = "<br />Please enter valid sales period.";
            source.innerHTML = "<br />Please enter valid sales period.";

            if ((Trim(txtOrdStartDate.value) == '') && (Trim(txtOrdEndDate.value) == '')) {
                args.IsValid = true;
            }
            else {
                if (Trim(txtOrdStartDate.value) != '') {
                    boolValid = true;
                }
                else {
                    boolValid = false;
                }

                if ((boolValid) && (Trim(txtOrdEndDate.value) != '')) {
                    if (validateDate(txtOrdEndDate.value)) {
                        boolValid = true;
                    }
                    else {
                        boolValid = false;
                    }
                }
                else {
                    boolValid = false;
                }

                if ((boolValid) && (validateDateRange(txtOrdStartDate.value, txtOrdEndDate.value))) {
                    boolValid = true;
                }
                else {
                    boolValid = false;
                }

                args.IsValid = boolValid;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">

    <div class="main-content__header">
        <div class="title"><asp:Literal ID="litPageTitle" runat="server"></asp:Literal></div>
        <div class="divListingSplitter"></div>
        <div class="action">
			<asp:Panel ID="pnlListingAction" runat="server" CssClass="divListingAction">
                <asp:HyperLink ID="hypAddSales" runat="server" Text="Add Sales" ToolTip="Add Sales" CssClass="hypListingAction add" NavigateUrl="admOrder0102.aspx"></asp:HyperLink>
				<div class="divListingSplitter"></div>
                <asp:LinkButton ID="lnkbtnExport" runat="server" Text="Export to Excel" ToolTip="Export to Excel" CssClass="hypListingAction export" ></asp:LinkButton>
            </asp:Panel>
        </div>
        <div class="divListingSplitter"></div>
        <div class="filter">
            <div class="divFilterContainer">
                <div class="divFilter">
                    <asp:TextBox ID="txtMember" runat="server" placeholder="Keyword Search" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
                    <a href="#" class="btnDropdown" onclick="toggleFilterOption();"></a>
                    <asp:Literal ID="litMemberRule" runat="server" meta:resourcekey="litMemberRule" visible="false"></asp:Literal>
                </div>
                <div class="divFilterMore">
                    <div></div>
                    <div>
                        <table class="tblSearch">
                            <colgroup>
								<col style="width:50%;"/>
								<col style="width:50%;"/>   
							</colgroup>
							<tr>
								<td id="trMember">
                                    <div class="divFilterLabel">Member:</div>
									<asp:DropDownList ID="ddlMember" runat="server" CssClass="ddl"></asp:DropDownList>
								</td>
                               <td id="trProduct">
                                    <div class="divFilterLabel">Product:</div>
                                    <asp:DropDownList ID="ddlProduct" runat="server" CssClass="ddl"></asp:DropDownList>
                                </td>
                                <td>
                                    <div class="divFilterLabel">Sales Status:</div>
                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="ddl"></asp:DropDownList>
                                </td>
							</tr>
							<tr>
								<td>
                                    <div class="divFilterLabel">Sales Date:</div>
									<div>From</div> <asp:TextBox ID="txtOrdStartDate" runat="server" CssClass="text_medium" MaxLength="10"></asp:TextBox>
                                    <asp:CustomValidator ID="cvPeriod" runat="server" Display="Dynamic" CssClass="errmsg" ClientValidationFunction="validateOrdPeriod_client"></asp:CustomValidator>
								</td>
                                <td>
                                    <div class="divFilterLabel">&nbsp;</div>
                                    <div>To</div>  <asp:TextBox ID="txtOrdEndDate" runat="server" CssClass="text_medium" MaxLength="10"></asp:TextBox>
                                </td>
							</tr>
							<tr>
                                <td>
                                    <div class="divFilterLabel">Report Type:</div>
                                    <div>
                                        <asp:RadioButton ID="rdoDetailed" runat="server" Text="Detailed" GroupName="ReportType" Checked="true" />
									    <asp:RadioButton ID="rdoSummary" runat="server" Text="Summary" GroupName="ReportType" />
                                    </div>
									
								</td>

							</tr>
							<tr visible="false" runat="server">
								<td>
                                    <div class="divFilterLabel">Sales Type:</div>
                                    <asp:DropDownList ID="ddlType" runat="server" CssClass="ddl" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged"></asp:DropDownList>

								</td>

							</tr>

							
                        </table>

                    </div>
                </div>
            </div>
            <div class="divFilterAction"><asp:Button ID="lnkbtnGO" runat="server" Text="" ToolTip="Search" CssClass="btn btnSearch" Visible="false"/></div>
        </div>
    </div>
    <div class="divListing">
        <div class="divListingHdr" runat="server" visible="false">
            <div class="divListingHdrLeft"><asp:Literal ID="litItemFound" runat="server"></asp:Literal></div>
        </div>
        <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAck">
            <asp:Literal ID="litAck" runat="server"></asp:Literal>
        </asp:Panel>
        <div class="divListingData">
            <uc:CmnDatatable ID="ucCmnDatatable_Sales" runat="server" />
            <table id="datatable_sales"></table>
            <table id="datatable_product"></table>
            <asp:HiddenField runat="server" ID="hdnItemID" />
            <asp:LinkButton ID="lnkbtnQuickDone" runat="server" OnClick="lnkbtnQuickDone_Click" style="display:none;"></asp:LinkButton>
        </div>
    </div>

    <script type="text/javascript">
        var columns_sales = [];
        var columnsSort_sales = [];
        var columns_product = [];
        var columnsSort_product = [];

        function getSalesTable() {
            var columns = [
                    { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                    { "data": "ORDER_CREATION", "bSearchable": false, "label": "Sales Date", "order": 1, "convert": "datetime" },
                    { "data": "ORDER_NO", "label": "Sales No.", "order": 2, },
                    { "data": "V_MEMCUSTNAME", "label": "Member", "order": 3, },
                    { "data": "ORDER_TOTAL", "bSearchable": false, "label": "Amount", "order": 4, "convert": "price", "currency": "" },
                    { "data": "V_STATUSNAME", "bSearchable": false, "label": "Status", "order": 5, "searchColumn": "ORDER_STATUS" },
                    <% if (isCouponFeature) { %>
                    { "data": "ORDER_DISCOUPONCODE", "label": "Coupon", "order": 6, },
                    <% } %>


                    { "data": "action", "bSortable": false, "bSearchable": false, "label": "Action", "order": 99 },
            ];
            var columnSort = columns.reduce(function (prev, curr, index) {
                prev[curr.data] = index;
                return prev;
            }, {});
 
            var columnDefs = [{
                "render": function (data, type, row) {
                    var lnkbtn = "";
                    lnkbtn += "<a class='lnkbtn lnkbtnEdit' href='<%= listingPage %>?id=" + row.ORDER_ID + "' title='Edit'></a>";
                    
                    <% if (isInvoiceFeature) {  %>
                    lnkbtn += "<a class='lnkbtn lnkbtnDone' title='Done' data-id='"+row.ORDER_ID+"'></a>";
                    <% } %>

                    return lnkbtn;
                },
                "targets": columnSort["action"]
            }];
            var dataSerializeFunc = function (d) {

                d.searchCustom = [{
                    "type": "daterange",
                    "column_from": "ORDER_CREATION",
                    "column_to": "ORDER_CREATION",
                    "date_from": ($('#ctl00_cphContent_txtOrdStartDate').val() || "1900-01-01") + " 00:00:00",
                    "date_to": ($('#ctl00_cphContent_txtOrdEndDate').val() || "9999-12-31") + " 23:59:59",
                }];
                d.func = "getSalesData"

                columns.filter(function (x) {
                    return x.searchColumn;
                }).forEach(function (x) {
                    var index = columnSort[x.data];
                    d.columns[index].searchColumn = x.searchColumn;
                });

                


                //console.log(d);
                return JSON.stringify(d);
            };
            var properties = {
                columns: columns,
                columnDefs: columnDefs,
                columnSort: columnSort,
                order: [[1, 'desc']],
                func: "getSalesData",
                name: "sales",
                elementID: "datatable_sales",
                dataSerializeFunc: dataSerializeFunc,
            };

            columns_sales = columns;
            columnsSort_sales = columnSort;

            return callDataTables(properties);
        }
        
        function getSalesProductTable() {
            var columns = [
                { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                { "data": "ORDDETAIL_PRODCODE", "label": "Product Code", "order": 1, },
                { "data": "ORDDETAIL_PRODDNAME", "label": "Product Name", "order": 2, },
                { "data": "ORDDETAIL_PRODTOTALAMOUNT", "bSearchable": false, "label": "Amount", "order": 3, "convert": "price" },
                { "data": "ORDER_STATUS", "bSearchable": false, "label": "Status", "order": 4, "visible": false },
                { "data": "ORDER_CREATION", "bSearchable": false, "label": "Status", "order": 5, "visible": false },
            ];
            var columnSort = columns.reduce(function (prev, curr) {
                prev[curr.data] = curr.order;
                return prev;
            }, {});

            var columnDefs = [];
            var dataSerializeFunc = function (d) {
                d.searchCustom = [{
                    "type": "daterange",
                    "column_from": "ORDER_CREATION",
                    "column_to": "ORDER_CREATION",
                    "date_from": ($('#ctl00_cphContent_txtOrdStartDate').val() || "1900-01-01") + " 00:00:00",
                    "date_to": ($('#ctl00_cphContent_txtOrdEndDate').val() || "9999-12-31") + " 23:59:59",
                }];

                //console.log(d);
                return JSON.stringify(d);
            };

            var properties = {
                columns: columns,
                columnDefs: columnDefs,
                columnSort: columnSort,
                dataSerializeFunc: dataSerializeFunc,
                url: "getSalesProductData",
                name: "sales_product",
                elementID: "datatable_product",
            };

            columns_product = columns;
            columnsSort_product = columnSort;

            return callDataTables(properties);
        }

        $(function () {

            var $datatable = getSalesTable();
            var $datatableSalesProduct = getSalesProductTable();

            // Sales Filter
            $("#<%= ddlStatus.ClientID %>").change(function () {
                $datatable
               .columns(columnsSort_sales["V_STATUSNAME"])
               .search(this.value)
               .draw();

                $datatableSalesProduct
               .columns(columnsSort_product["ORDER_STATUS"])
               .search(this.value)
               .draw();
            });
            $("#<%= ddlMember.ClientID %>").change(function () {
                $datatable
               .columns(columnsSort_sales["V_MEMCUSTNAME"])
               .search(this.value)
               .draw();
            });
            $("#<%= ddlProduct.ClientID %>").change(function () {
                $datatableSalesProduct
               .columns(columnsSort_product["ORDDETAIL_PRODDNAME"])
               .search(this.value)
               .draw();
            });

            $('#ctl00_cphContent_txtOrdStartDate').change(function () { $datatable.draw(); $datatableSalesProduct.draw(); });
            $('#ctl00_cphContent_txtOrdEndDate').change(function () { $datatable.draw(); $datatableSalesProduct.draw(); });

            // Sales Product Filter

            $("#trMember").show();
            $("#trProduct").hide();
            $("#datatable_sales_wrapper").show();
            $("#datatable_product_wrapper").hide();
            $('input[type=radio][name=ctl00$cphContent$ReportType]').on('change', function () {
                switch ($(this).val()) {
                    case 'rdoSummary':
                        $("#datatable_sales_wrapper").hide();
                        $("#datatable_product_wrapper").show();
                        $("#trMember").hide();
                        $("#trProduct").show();
                        break;
                    case 'rdoDetailed':
                        $("#datatable_sales_wrapper").show();
                        $("#datatable_product_wrapper").hide();
                        $("#trMember").show();
                        $("#trProduct").hide();
                        break;
                }
            });

            $("#<%= lnkbtnExport.ClientID %>").click(function (e) {

                var $table = null;

                if ($("#ctl00_cphContent_rdoDetailed").is(":checked")) {
                    $table = $('#datatable_sales_wrapper .dataTable').clone();
                    $table.attr("border", "1").find("th:last-child,td:last-child").remove();

                } else if ($("#ctl00_cphContent_rdoSummary").is(":checked")) {
                    $table = $('#datatable_product_wrapper .dataTable').clone();
                }
                window.open('data:application/vnd.ms-excel,' + encodeURIComponent($table.get(0).outerHTML));
                e.preventDefault();
            });

            <% if (isInvoiceFeature) {  %>
            $(document).on("click", ".lnkbtnDone", function () {
                if (confirm("Are you sure want to quick done this order?")) {
                    var itemID = $(this).attr("data-id");
                    document.getElementById('<%= hdnItemID.ClientID %>').value = itemID;
                    document.getElementById('<%= lnkbtnQuickDone.ClientID %>').click();
                }
             })
            <% }%>
            
        });
    </script>
</asp:Content>