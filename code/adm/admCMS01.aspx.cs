﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class adm_admCMS01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 3;
    protected string _gvSortExpression = "CMS_TITLE";
    protected string _gvSortDirection = clsAdmin.CONSTSORTDESC;
    protected string _gvSort = "";

    protected Boolean _boolSearch = false;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public Boolean boolSearch
    {
        get
        {
            if (ViewState["BOOLSEARCH"] == null)
            {
                return _boolSearch;
            }
            else
            {
                return Convert.ToBoolean(ViewState["BOOLSEARCH"]);
            }
        }
        set { ViewState["BOOLSEARCH"] = value; }
    }

    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);
        
        if (!IsPostBack)
        {
            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMOBJECTMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            setSearchProperties();

        
            if (Session["DELETEDCMSTITLE"] != null && Session["DELETEDCMSTITLE"] != "")
            {
                pnlAck.Visible = true;

                litAck.Text = "<div class=\"noticemsg\">CMS (" + Session["DELETEDCMSTITLE"].ToString() + ") has been deleted successfully.</div>";

                Session["DELETEDCMSTITLE"] = null;

                clsMis.resetListing();
            }
        }
    }

    protected void lnkbtnGO_Click(object sender, EventArgs e)
    {
        boolSearch = true;
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        boolSearch = false;

        Response.Redirect(currentPageName);
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        int cmsId = int.Parse(hdnObjectID.Value);

        string strDeletedCMSTitle = "";

        clsCMSObject cms = new clsCMSObject();
        clsPageObject po = new clsPageObject();
        int intRecordAffected = 0;

        cms.extractCMSById(cmsId, 0);

        strDeletedCMSTitle = cms.cmsTitle;

        intRecordAffected = cms.deleteCMSById(cmsId);

        if (intRecordAffected == 1)
        {
            po.deleteItemById(cmsId, clsAdmin.CONSTCMSGROUPCONTENT);
            Session["DELETEDCMSTITLE"] = strDeletedCMSTitle;

            Response.Redirect(currentPageName);
        }
        else
        {
            litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to delete CMS (" + strDeletedCMSTitle + "). Please try again.</div>";

            pnlAck.Visible = true;
        }
    }
    #endregion


    #region "Methods"
    protected void setSearchProperties()
    {
        ListItem ddlActiveDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlActive.Items.Insert(0, ddlActiveDefaultItem);
        ddlActive.Items.Add(new ListItem("Yes", "1"));
        ddlActive.Items.Add(new ListItem("No", "0"));
    }

    #endregion
}
