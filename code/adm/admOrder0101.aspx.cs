﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class adm_admOrder0101 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 24;
    protected int _ordId = 0;
    protected int _formSect = 1;
    protected DataTable _dtOrder = new DataTable();
    protected decimal _decShipping;
    protected string strRemarks;
    protected string _deletedId = null;
    protected int _active = 1;
    protected int _type = 1;
    protected int _status = 0;
    protected DateTime _dtOrdNew;
    protected DateTime _dtOrdPayment;
    protected int intCurStatus;

    //repeater data
    protected Boolean boolRemove = false;
    protected int intProdCount;
    protected decimal decSubtotal = Convert.ToDecimal("0.00");
    protected int intCount;

    //order items data
    protected int intProdId;
    protected decimal decProdPrice;
    protected int intProdQty;
    protected decimal decProdTotal;

    //order delivery data
    protected string strNameBill;
    protected string strAddBill;
    protected string strPoscodeBill;
    protected string strCityBill;
    protected string strContactNoBill;
    protected string strStateBill;
    protected string strCountryBill;

    protected string strName;
    protected string strAddress;
    protected string strPoscode;
    protected string strCity;
    protected string strContactNo;
    protected string strState;
    protected string strCountry;
    #endregion


    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    public string strSavedMsg
    {
        get { return String.Format(GetGlobalResourceObject("GlobalResource", "admSavedMessageWithParams.Text").ToString(), "Sale"); }
    }
    public int ordId
    {
        get
        {
            if (ViewState["ORDID"] == null)
            {
                return _ordId;
            }
            else
            {
                return Convert.ToInt16(ViewState["ORDID"]);
            }
        }
        set { ViewState["ORDID"] = value; }
    }

    public int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set { ViewState["FORMSECT"] = value; }
    }

    public DataTable dtOrder
    {
        get
        {
            if (ViewState["DTORDER"] == null)
            {
                return _dtOrder;
            }
            else
            {
                return (DataTable)ViewState["DTORDER"];
            }
        }
        set { ViewState["DTORDER"] = value; }
    }

    public decimal decShipping
    {
        get
        {
            if (ViewState["ORDSHIPPING"] == null)
            {
                return _decShipping;
            }
            else
            {
                return Convert.ToDecimal(ViewState["ORDSHIPPING"]);
            }
        }
        set { ViewState["ORDSHIPPING"] = value; }
    }

    public string deletedId
    {
        get
        {
            if (ViewState["DELETEDID"] == null)
            {
                return _deletedId;
            }
            else
            {
                return ViewState["DELETEDID"].ToString();
            }
        }
        set { ViewState["DELETEDID"] = value; }
    }

    public int active
    {
        get
        {
            if (ViewState["ACTIVE"] == null)
            {
                return _active;
            }
            else
            {
                return Convert.ToInt16(ViewState["ACTIVE"]);
            }
        }
        set { ViewState["ACTIVE"] = value; }
    }

    public int type
    {
        get
        {
            if (ViewState["TYPE"] == null)
            {
                return _type;
            }
            else
            {
                return Convert.ToInt16(ViewState["TYPE"]);
            }
        }
        set { ViewState["TYPE"] = value; }
    }

    public int status
    {
        get
        {
            if (ViewState["STATUS"] == null)
            {
                return _status;
            }
            else
            {
                return Convert.ToInt16(ViewState["STATUS"]);
            }
        }
        set { ViewState["STATUS"] = value; }
    }

    public DateTime dtOrdNew
    {
        get
        {
            if (ViewState["DTORDNEW"] == null)
            {
                return _dtOrdNew;
            }
            else
            {
                return DateTime.Parse(ViewState["DTORDNEW"].ToString());
            }
        }
        set { ViewState["DTORDNEW"] = value; }
    }

    public DateTime dtOrdPayment
    {
        get
        {
            if (ViewState["DTORDPAYMENT"] == null)
            {
                return _dtOrdPayment;
            }
            else
            {
                return DateTime.Parse(ViewState["DTORDPAYMENT"].ToString());
            }
        }
        set { ViewState["DTORDPAYMENT"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;
                if (int.TryParse(Request["id"], out intTryParse))
                {
                    ordId = intTryParse;
                }
                else
                {
                    ordId = 0;
                }
            }
            else if (Session["EDITEDORDID"] != null)
            {
                try
                {
                    ordId = Convert.ToInt16(Session["EDITEDORDID"]);
                }
                catch (Exception ex)
                {
                    ordId = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["frm"]))
            {
                int intTryParse;
                if (int.TryParse(Request["frm"], out intTryParse))
                {
                    formSect = intTryParse;
                }
                else
                {
                    formSect = 0;
                }
            }

            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMSALESMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();
        }
        
        setPageProperties();
        Session["EDITEDORDID"] = null;
        Session["NOCHANGE"] = null;
        Session["CANCELLEDORDNO"] = null;
        Session["ERRMSG"] = null;
    }

    protected void lnkbtnCancel_Click(object sender, EventArgs e)
    {
        pnlCancelSales.Visible = true;
    }

    protected void lnkbtnConfirm_Click(object sender, EventArgs e)
    {
        clsAdmin adm = new clsAdmin();

        string strUsername = txtUserId.Text.Trim();

        string password = txtPassword.Text.Trim();
        clsMD5 md5 = new clsMD5();
        string strPassword = md5.encrypt(password);

        if (Convert.ToInt16(Session["ADMTYPE"]) == clsAdmin.CONSTADMTYPE)
        {
            string strCancelledOrdNo = "";

            clsMis.performCancelOrder(ordId, ref strCancelledOrdNo);
            Session["CANCELLEDORDNO"] = strCancelledOrdNo;

            Response.Redirect(currentPageName);
        }
        else
        {
            litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmInvalid.Text").ToString();

            pnlAck.Visible = true;
            pnlCancelSales.Visible = false;
        }
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        if (!IsPostBack)
        {
            litPageTitle.Text = GetLocalResourceObject("litPageTitle.Text").ToString();

            lnkbtnCancelDelete.OnClientClick = "javascript:document.location.href='" + currentPageName + "?id=" + ordId + "'; return false;";
            

            if (Session["EDITEDORDID"] != null)
            {
                if (Session["NOCHANGE"] != null)
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
                }
                else
                {
                    litAck.Text = "<div class=\"noticemsg\">" + strSavedMsg + "</div>";

                    clsMis.resetListing();
                }

                pnlAck.Visible = true;
            }
            else if (Session["CANCELLEDORDNO"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }
            else if (Session["ERRMSG"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
            }
        }

        ucAdmOrder.transId = ordId;
        ucAdmOrder.formSect = formSect;
        ucAdmOrder.fill();
    }
    #endregion
}