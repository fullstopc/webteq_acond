﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admHighlight0101.aspx.cs" Inherits="adm_admHighlight0101" ValidateRequest="false" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucAdmPageDesc.ascx" TagName="AdmPageDesc" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmHighlightDetails.ascx" TagName="AdmHighlightDetails" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmDetail.ascx" TagName="AdmDetail" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmHighlightGallery.ascx" TagName="AdmHighlightGallery" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmHighlightDesc.ascx" TagName="AdmHighlightDesc" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmHighlightVideo.ascx" TagName="AdmHighlightVideo" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmHighlightArticle.ascx" TagName="AdmHighlightArticle" TagPrefix="uc" %>
<%@ Register Src="~/ctrl/ucAdmHighlightComment.ascx" TagName="AdmHighlightComment" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
     <div class="main-content__header">
        <div class="title">
            <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
        </div>
        <div class="divListingSplitter"></div>
        <div class="action">
            <a href="#" class="hypListingAction setting" onclick="toggleSideMenu();">Page Detail</a>
        </div>
        <div class="divListingSplitter"></div>
    </div>
    <asp:Panel ID="pnlAck" runat="server" Visible="false" CssClass="divAckGrey">
        <asp:Literal ID="litAck" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Panel ID="pnlForm" runat="server" CssClass="divForm divEventForm">
        <uc:AdmPageDesc ID="ucAdmPageDesc" runat="server" />
        <div class="divListingDetailSplitter"></div>
        <asp:Panel ID="pnlFormDetails" runat="server">
            <uc:AdmHighlightDetails ID="ucAdmHighlightDetails" runat="server" pageListingURL="admHighlight01.aspx" />
        </asp:Panel>
        <asp:Panel ID="pnlFormInner" runat="server" Visible="false">
            <asp:Panel ID="pnlDetail" runat="server">
                <uc:AdmDetail ID="ucAdmDetail" runat="server" />
            </asp:Panel>
            <asp:Panel ID="pnlFormGallery" runat="server" Visible="false">
                <uc:AdmHighlightGallery ID="ucAdmHighlightGallery" runat="server" pageListingURL="admHighlight01.aspx" noOfImageInRow="5" />
            </asp:Panel>
            <asp:Panel ID="pnlFormDesc" runat="server" Visible="false">
                <uc:AdmHighlightDesc ID="ucAdmHighlightDesc" runat="server" pageListingURL="admHighlight01.aspx" />
            </asp:Panel>
            <asp:Panel ID="pnlFormVideo" runat="server" Visible="false">
                <uc:AdmHighlightVideo ID="ucAdmHighlightVideo" runat="server" pageListingURL="admHighlight01.aspx" />
            </asp:Panel>
            <asp:Panel ID="pnlFormArticle" runat="server" Visible="false">
                <uc:AdmHighlightArticle ID="ucAdmHighlightArticle" runat="server" pageListingURL="admHighlight01.aspx" />
            </asp:Panel>
            <asp:Panel ID="pnlFormComment" runat="server" Visible="false">
                <uc:AdmHighlightComment ID="ucAdmHighlightComment" runat="server" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>