﻿<%@ Page Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admResidentProperty.aspx.cs" Inherits="adm_adm" ValidateRequest="false" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/mst/blank.Master" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>

<asp:Content ID="Content" ContentPlaceHolderID="cphHead" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-10">
                <asp:Literal ID="litMemberName" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="row">
            <uc:CmnDatatable ID="ucCmnDatatable_Property" runat="server" />
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            var columns = [
                { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                { "data": "property_area", "label": "Area", "order": 1, "bSearchable": false},
                { "data": "property_unitno", "label": "Unit No.", "order": 2, "bSearchable": false },
                { "data": "property_type", "label": "Type", "order": 3, "bSearchable": false },
                { "data": "member_id", "order": 4, "visible": false },
            ];
            var columnSort = columns.reduce(function (prev, curr) {
                prev[curr.data] = curr.order;
                return prev;
            }, {});
            var properties = {
                columns: columns,
                columnDefs: [],
                columnSort: columnSort,
                func: "getResidentProperty",
                dataSerializeFunc: function (d) {
                    //console.log(d, this.func);
                    d.searchCustom = null;
                    d.func = "getResidentProperty";
                    d.columns[columnSort["member_id"]].search.value = <%= memberID %>;

                return JSON.stringify(d);
                },
                name: "property",
                dom: 'ZBrti',
            };
            var $datatable = callDataTables(properties);

        });
    </script>
</asp:Content>