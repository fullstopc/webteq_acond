﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_admOrder0102 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 37;
    protected int _salesId = 0;
    protected int _mode = 1;
    #endregion

    #region "Property Methods"
    public int salesId
    {
        get { return _salesId; }
        set { _salesId = value; }
    }

    public int mode
    {
        get { return _mode; }
        set { _mode = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["id"]))
        {
            try
            {
                salesId = int.Parse(Request["id"]);
            }
            catch (Exception ex)
            {
                salesId = 0;
            }
        }
        else if (Session["NEWSALESID"] != null)
        {
            try
            {
                salesId = Convert.ToInt16(Session["NEWSALESID"]);
            }
            catch (Exception ex)
            {
                salesId = 0;
            }
        }
        else if (Session["EDITEDSALESID"] != null)
        {
            try
            {
                salesId = Convert.ToInt16(Session["EDITEDSALESID"]);
            }
            catch (Exception ex)
            {
                salesId = 0;
            }
        }

        if (Request["id"] != null || Session["EDITEDSALESID"] != null)
        {
            mode = 2;
        }

        clsAdmin adm = new clsAdmin();
        adm.extractAdminPageByName(clsAdmin.CONSTADMADDSALESNAME, 1);
        sectId = adm.pageId;

        Master.sectId = sectId;
        Master.id = salesId;

        if (!IsPostBack)
        {
            Session["ORISALESREMARKS"] = null;
            Session["SALESREMARKS"] = null;
            Session["DELSALESREMARKS"] = null;

            Session["ORISALES"] = null;
            Session["SALES"] = null;
            Session["DELSALES"] = null;

            Session["SHIPPING"] = null;
            Session["SHIPPINGCHECK"] = null;
        }

        setPageProperties();

        Session["NEWSALESID"] = null;
        Session["EDITEDSALESID"] = null;
        Session["NOCHANGE"] = null;
        Session["DELETEDSALESID"] = null;
        Session["ERRMSG"] = null;
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        switch (mode)
        {
            case 1:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleAdd.Text").ToString(), true);
                break;
            case 2:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleEdit.Text").ToString(), true);
                break;
        }

        if (!IsPostBack)
        {
            switch (mode)
            {
                case 1://Add Sales
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleAdd.Text").ToString();
                    break;
                case 2://Edit Sales
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleEdit.Text").ToString();
                    break;
            }

            if (Session["NEWSALESID"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";
                clsMis.resetListing();
            }
            else if (Session["EDITEDSALESID"] != null)
            {
                if (Session["NOCHANGE"] != null)
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
                }
                else
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";

                    clsMis.resetListing();
                }

                pnlAck.Visible = true;
            }
            else if (Session["DELETEDSALESID"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }
            else if (Session["ERRMSG"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
            }
        }

        ucAdmOrderSales.id = salesId;
        ucAdmOrderSales.mode = mode;
        ucAdmOrderSales.fill();
    }
    #endregion
}
