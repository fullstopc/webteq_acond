﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class adm_admPage0101 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 5; //need change sectId according to db
    protected int _mode = 1;
    protected int _id = 0;
    protected int _formSect = 1;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    public string strSavedMsg
    {
        get { return String.Format(GetGlobalResourceObject("GlobalResource", "admSavedMessageWithParams.Text").ToString(),"Page"); }
    }
    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int id
    {
        get
        {
            if (ViewState["ID"] == null)
            {
                return _id;
            }
            else
            {
                return Convert.ToInt16(ViewState["ID"]);
            }
        }
        set
        {
            ViewState["ID"] = value;
        }
    }

    public int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set
        {
            ViewState["FORMSECT"] = value;
        }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                try
                {
                    id = Convert.ToInt16(Request["id"]);
                }
                catch (Exception ex)
                {
                    id = 0;
                }
            }
            else if (Session["NEWPAGEID"] != null)
            {
                try
                {
                    id = Convert.ToInt16(Session["NEWPAGEID"]);
                }
                catch (Exception ex)
                {
                    id = 0;
                }
            }
            else if (Session["EDITEDPAGEID"] != null)
            {
                try
                {
                    id = Convert.ToInt16(Session["EDITEDPAGEID"]);
                }
                catch (Exception ex)
                {
                    id = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["frm"]))
            {
                int intTryParse;
                if (int.TryParse(Request["frm"], out intTryParse))
                {
                    formSect = intTryParse;
                }
            }

            if (Request["id"] != null || Session["EDITEDPAGEID"] != null)
            {
                mode = 2;
            }

            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMPAGEMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.subSectId = clsMis.getSubSectId(sectId, formSect, 1);
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();
            Master.showSideMenu = true;
            
            clsPage page = new clsPage();
            if (!page.isOther(id)) { Master.id = id; }
        }

        setPageProperties();

        Session["NEWPAGEID"] = null;
        Session["EDITEDPAGEID"] = null;
        Session["NOCHANGE"] = null;
        Session["DELETEDPAGENAME"] = null;
        Session["ERRMSG"] = null;
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        switch (mode)
        {
            case 1:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleAdd.Text").ToString(), true);
                break;
            case 2:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleEdit.Text").ToString(), true);

                // set previous page and next page.
                int intPrevPageId = 0;
                int intNextPageId = 0;
                clsPage page = new clsPage();
                page.getNextPrevPageId(id, out intPrevPageId, out intNextPageId);
                if(intPrevPageId != 0)
                {
                    hypPrevPage.Attributes["href"] = currentPageName + "?id=" + intPrevPageId;
                    hypPrevPage.Visible = true;
                }
                if (intNextPageId != 0)
                {
                    hypNextPage.Attributes["href"] = currentPageName + "?id=" + intNextPageId;
                    hypNextPage.Visible = true;
                }

                break;
            default:
                break;
        }

        if (!IsPostBack)
        {
            switch (mode)
            {
                case 1://Add
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleAdd.Text").ToString();

                    clsPage page = new clsPage();
                    int intTotal = page.getTotalPages(0, 0);
                    if (clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITPAGE, intTotal)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "adm/admPage01.aspx"); }
                    break;
                case 2://Edit
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleEdit.Text").ToString();
                    break;
                default:
                    break;
            }

            

            
            if (Session["NEWPAGEID"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + strSavedMsg + "</div>";

                clsMis.resetListing();
            }
            else if (Session["EDITEDPAGEID"] != null)
            {
                if (Session["NOCHANGE"] != null)
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
                }
                else
                {
                    int intTotal = new clsPage().getTotalPages(0,0);
                    string strUrlFormpage = "<a href='admPage0101.aspx'>Add New</a>";
                    string strUrlListpage = "<a href='admPage01.aspx'>Back to Listing</a>";

                    litAck.Text = "<div class=\"noticemsg\">" + strSavedMsg + "  " + strUrlListpage + "";
                    if (!clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITPAGE, intTotal - 1))
                    {
                        litAck.Text += " & " + strUrlFormpage + "";
                    }
                    litAck.Text +="</div>";

                    clsMis.resetListing();
                }

                pnlAck.Visible = true;
            }
            else if (Session["DELETEDPAGENAME"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }
            else if (Session["ERRMSG"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
            }
        }

        if (formSect == 3)
        {
            ucAdmMobileView.pageId = id;
            ucAdmMobileView.mode = mode;
            ucAdmMobileView.fill();

            litPageTitle.Text += " (Mobile View)";
            pnlForm3.Visible = true;
        }
        else if (formSect == 2)
        {
            ucCmnSiblingPages.pageId = id;
            ucCmnSiblingPages.editPageURL = currentPageName;
            ucCmnSiblingPages.fill();
            pnlForm2.Visible = true;
        }
        else
        {
            ucAdmCMS.pageId = id;
            ucAdmCMS.mode = mode;
            ucAdmCMS.enableLanguage = clsMis.isLangExist();
            ucAdmCMS.fill();

            pnlForm.Visible = true;
        }
    }
    #endregion
}
