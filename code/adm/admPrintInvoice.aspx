﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/blank.master" AutoEventWireup="true" CodeFile="admPrintInvoice.aspx.cs" Inherits="adm_admPrintInvoice" %>
<%@ MasterType VirtualPath="~/mst/blank.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
<script type="text/javascript">
    //$("#test").printElement({ printMode: 'popup' });
    //window.print();
    $(document).ready(function() {
        window.print();
    });
</script>

<%--<script type="text/javascript">
    $(document).ready(function() {
        printElem({});
    });
    
    function printElem(options) {
        $('#divPrintInvoiceContainer').printElement(options);
    }
</script>--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <%--<div id="divPrintInvoiceContainer" class="divPrintContainer">--%>
        <asp:Panel ID="pnlPrintInvoiceContainer" runat="server" CssClass="divPrintContainer">
            <%--<div id="divPrintContainerInner" class="divPrintContainerOuter">--%>
            <div class="divPrintInvoiceTopWrap">
                <%--<div class="divPrintInvoiceTopMain">--%>
                    <asp:Panel ID="pnlPrintInvoiceHeader" runat="server" CssClass="divPrintInvoiceHeader">
                        <div id="divPrintInvoiceHeaderLeft" runat="server"><asp:Image ID="imgInvoiceLogo" runat="server" /></div>
                        <div class="divPrintInvoiceHeaderRight"><asp:Literal ID="litCompanyDetails" runat="server"></asp:Literal></div>
                    </asp:Panel>
                    <asp:Panel ID="pnlInvoiceType" runat="server" CssClass="divInvoiceType">
                        <asp:Literal ID="litInvoiceHdr" runat="server"></asp:Literal>
                    </asp:Panel>
                    <asp:Panel ID="pnlPrintInvoiceBillDelDetails" runat="server" CssClass="divPrintInvoiceBillDelDetails">
                        <table cellpadding="5" cellspacing="0" border="0" class="formTbl">
                            <tr>
                                <td class="tdInvoiceDetail3">
                                    <table cellpadding="0" cellspacing="0" border="0" class="formTbl">
                                        <tr>
                                            <td class="tdInvoiceDetail1">TO:</td>
                                            <td class="tdInvoiceDetail2"><asp:Literal ID="litBillDelName" runat="server"></asp:Literal><asp:Literal ID="litBillDeliveryDetails" runat="server"></asp:Literal></td>
                                        </tr>
                                        <tr>
                                            <td class="tdInvoiceDetail1">TEL 1:</td>
                                            <td class="tdInvoiceDetail2"><asp:Literal ID="litTel1" runat="server"></asp:Literal></td>
                                        </tr>
                                        <tr>
                                            <td class="tdInvoiceDetail1">TEL 2:</td>
                                            <td class="tdInvoiceDetail2"><asp:Literal ID="litTel2" runat="server"></asp:Literal></td>
                                        </tr>
                                        <tr>
                                            <td class="tdInvoiceDetail1">FAX:</td>
                                            <td class="tdInvoiceDetail2"><asp:Literal ID="litFax" runat="server"></asp:Literal></td>
                                        </tr>
                                        <tr>
                                            <td class="tdInvoiceDetail1">ATTN:</td>
                                            <td class="tdInvoiceDetail2"><asp:Literal ID="litAttnName" runat="server"></asp:Literal></td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdInvoiceDetail3">
                                    <table cellpadding="0" cellspacing="0" border="0" class="formTbl">
                                        <tr>
                                            <td class="tdLabelNor3 nobr"><asp:Label ID="lblInvoiceNo" runat="server"></asp:Label></td>
                                            <td class="tdMax"><asp:Literal ID="litSalesNo" runat="server"></asp:Literal></td>
                                        </tr>
                                        <tr>
                                            <td class="tdLabelNor3 nobr"><asp:Label ID="lblDate" runat="server"></asp:Label></td>
                                            <td class="tdMax"><asp:Literal ID="litSalesDate" runat="server"></asp:Literal></td>
                                        </tr>
                                        <tr>
                                            <td class="tdLabelNor3 nobr"><asp:Label ID="lblCustomerNo" runat="server"></asp:Label></td>
                                            <td class="tdMax"><asp:Literal ID="litCustomerNo" runat="server"></asp:Literal></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="pnlPrintInvoiceListingItems" runat="server" CssClass="divPrintInvoiceListingItems">
                        <asp:Repeater ID="rptItems" runat="server" OnItemDataBound="rptItems_ItemDataBound">
                            <HeaderTemplate>
                                <table cellpadding="0" cellspacing="0" border="0" class="listTblInvoice">
                                    <tr>
                                        <th class="thItem">ITEM</th>
                                        <th class="thDesc">DESCRIPTION</th>
                                        <th class="thQty">QTY</th>
                                        <th id="thUnitPrice" runat="server" class="thUnitPrice" visible="false">UNIT PRICE</th>
                                        <th id="thAmount" runat="server" class="thAmount" visible="false">AMOUNT</th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                    <tr class="trItem">
                                        <td id="tdNo" runat="server" class="tdNo"><asp:Literal ID="litNo" runat="server"></asp:Literal></td>
                                        <td id="tdItem" runat="server" class="tdItem"><asp:Literal ID="litItem" runat="server"></asp:Literal></td>
                                        <td id="tdQty" runat="server" class="tdQty"><asp:Literal ID="litQty" runat="server"></asp:Literal></td>
                                        <td id="tdUnitPrice" runat="server" visible="false" class="tdUnitPrice"><asp:Literal ID="litUnitPrice" runat="server"></asp:Literal></td>
                                        <td id="tdTotal" runat="server" visible="false" class="tdTotal"><asp:Literal ID="litItemTotal" runat="server"></asp:Literal></td>
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                    <tr id="trSubtotal" runat="server" class="trFooter" visible="false">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="tdInvoiceTotal">SUBTOTAL<asp:Literal ID="litSubtotalCurrency" runat="server"></asp:Literal></td>
                                        <td><asp:Literal ID="litSubTotal" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr id="trShipping" runat="server" class="trFooter" visible="false">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="tdInvoiceTotal">SHIPPING<asp:Literal ID="litShippingCurrency" runat="server"></asp:Literal></td>
                                        <td><asp:Literal ID="litShipping" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr id="trTotal" runat="server" class="trFooter" visible="false">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="tdInvoiceTotal">TOTAL<asp:Literal ID="litTotalCurrency" runat="server"></asp:Literal></td>
                                        <td><asp:Literal ID="litGrandTotal" runat="server"></asp:Literal></td>
                                    </tr>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                <%--</div>--%>
            </div>
            <div class="divPrintInvoiceBtmContainer">
                <asp:Panel ID="pnlPrintInvoiceSalesRemarks" runat="server" CssClass="divPrintInvoiceSalesRemarks">
                    <asp:Literal ID="litInvoiceSalesRemarks" runat="server"></asp:Literal>
                </asp:Panel>
                <asp:Panel ID="pnlPrintInvoiceAllRemarks" runat="server" Visible="false">
                    <asp:Literal ID="litInvoiceRemarks" runat="server"></asp:Literal>
                    <asp:TextBox ID="txtInvoiceRemarks" runat="server" TextMode="MultiLine" Rows="5" CssClass="text_big2" Visible="false"></asp:TextBox>
                </asp:Panel>
                <asp:Panel ID="pnlPrintInvoiceSignature" runat="server" Visible="false">
                    <asp:Literal ID="litInvoiceSignature" runat="server"></asp:Literal>
                    <asp:TextBox ID="txtInvoiceSignature" runat="server" TextMode="MultiLine" Rows="5" CssClass="text_big2" Visible="false"></asp:TextBox>
                </asp:Panel>
            </div>
            <%-- </div>--%>
       </asp:Panel>
  
   
  <%-- </div>--%>
</asp:Content>

