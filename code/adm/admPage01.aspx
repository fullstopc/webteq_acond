﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mst/private.master" AutoEventWireup="true" CodeFile="admPage01.aspx.cs" Inherits="adm_admPage01" %>
<%@ MasterType VirtualPath="~/mst/private.master" %>
<%@ Register Src="~/ctrl/ucCmnDatatable.ascx" TagName="CmnDatatable" TagPrefix="uc" %>


<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <div class="main-content__header">
        <div class="title"><asp:Literal ID="litPageTitle" runat="server" meta:resourcekey="litPageTitle"></asp:Literal></div>
        <div class="divListingSplitter"></div>
        <div class="action">
            <asp:Panel ID="pnlListingAction" runat="server" CssClass="divListingAction">
                <asp:HyperLink ID="hypAdd" runat="server" Text="Add Page" ToolTip="Add Page" CssClass="hypListingAction add" NavigateUrl="admPage0101.aspx"></asp:HyperLink>
                <div class="divListingSplitter"></div>
                <asp:LinkButton ID="lnkbtnExport" runat="server" Text="Export to Excel" ToolTip="Export to Excel" CssClass="hypListingAction export"></asp:LinkButton>
            </asp:Panel>
        </div>
        <div class="divListingSplitter"></div>
        <div class="filter">
            <div class="divFilterContainer">
                <div class="divFilter">
                    <asp:TextBox ID="txtKeyword" runat="server" placeholder="Keyword Search" CssClass="text txtKeyword" MaxLength="255"></asp:TextBox>
                    <a href="#" class="btnDropdown" onclick="toggleFilterOption();"></a>
                    <asp:Literal ID="litKeywordRule" runat="server" meta:resourcekey="litKeywordRule" visible="false"></asp:Literal>
                </div>
                <div class="divFilterMore">
                    <div></div>
                    <div>
                        <table class="tblSearch">
                            <tr>
                                <td class="">
                                    <div class="divFilterLabel">Show Page Title:</div>
                                    <asp:DropDownList ID="ddlShowPageTitle" runat="server" CssClass="ddl_medium"></asp:DropDownList>

                                </td>
                                <td class="">
                                    <div class="divFilterLabel">Show in Menu:</div>
                                    <asp:DropDownList ID="ddlShowInMenu" runat="server" CssClass="ddl_medium"></asp:DropDownList>

                                </td>
                            </tr>
                            <tr>
                                <td class="">
                                    <div class="divFilterLabel">Parent:</div>
                                    <asp:DropDownList ID="ddlParent" runat="server" CssClass="ddl_medium"></asp:DropDownList>

                                </td>
                                <td class="">
                                    <div class="divFilterLabel">Language:</div>
                                    <asp:DropDownList ID="ddlLanguage" runat="server" CssClass="ddl_medium"></asp:DropDownList>

                                </td>
                            </tr>
                            <tr>
                                <td class="">
                                    <div class="divFilterLabel">Active:</div>
                                    <asp:DropDownList ID="ddlActive" runat="server" CssClass="ddl_medium"></asp:DropDownList>

                                </td>
                                <td class="">
                                    <div class="divFilterLabel">Template:</div>
                                    <asp:DropDownList ID="ddlTemplate" runat="server" CssClass="ddl_medium"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
            <div class="divFilterAction" runat="server" visible="false"><asp:Button ID="lnkbtnGo" runat="server" Text="" ToolTip="Search" CssClass="btn btnSearch"/></div>
        </div>
    </div>
    <div class="divListing">
        <asp:Panel ID="pnlAck" runat="server" CssClass="divAck" Visible="false">
            <div><asp:Literal ID="litAck" runat="server"></asp:Literal></div>
        </asp:Panel>
        <uc:CmnDatatable ID="ucCmnDatatable" runat="server" />

        <asp:HiddenField runat="server" ID="hdnPageID" />
        <asp:LinkButton ID="lnkbtnDelete" runat="server" OnClick="lnkbtnDelete_Click" style="display:none;"></asp:LinkButton>
    </div>
   
    <script type="text/javascript">

        $(function () {
            var columns = [
                    { "data": "index", "bSortable": false, "bSearchable": false, "label": "No.", "order": 0 },
                    { "data": "PAGE_NAME", "label": "Page Name", "order": 1,  },
                    { "data": "PAGE_DISPLAYNAME", "label": "Page Display Name", "order": 2,  },
                    { "data": "V_PARENTNAME", "label": "Parent", "order": 3,  },
                    { "data": "PAGE_CREATION", "bSearchable": false, "label": "Creation Date", "order": 4, "convert" :"datetime"},
                    { "data": "PAGE_LASTUPDATE", "bSearchable": false, "label": "Last Updated Date", "order": 5, "convert": "datetime" },
                    { "data": "PAGE_USEREDITABLE", "bSearchable": false, "label": "User Editable", "order": 6, "convert": "active" },
                    { "data": "PAGE_SHOWINMENU", "bSearchable": false, "label": "Show in Menu", "order": 7, "convert": "active" },
                    { "data": "PAGE_SHOWPAGETITLE", "bSearchable": false, "label": "Show Title", "order": 8, "convert": "active" },
                    { "data": "PAGE_ACTIVE", "bSearchable": false, "label": "Active", "order": 9, "convert": "active" },
                    { "data": "PAGE_TEMPLATE", "bSearchable": false,"visible":false, "label": "Template", "order": 10, },
                    { "data": "action", "bSortable": false, "bSearchable": false, "label": "Action", "order": 11 },
            ];
            var columnSort = columns.reduce(function (prev, curr) {
                prev[curr.data] = curr.order;
                return prev;
            }, {});

            var columnDefs = [{
                "render": function (data, type, row) {
                    var lnkbtnEdit = "<a class='lnkbtn lnkbtnEdit' href='admPage0101.aspx?id=" + row.PAGE_ID + "' title='Edit'></a>";
                    var lnkbtnDelete = "<a class='lnkbtn lnkbtnDelete' title='Delete' data-id='"+row.PAGE_ID+"'></a>"; //href='javascript:return DeletePage(" + row.PAGE_ID + ");' 
                    return lnkbtnEdit + lnkbtnDelete;
                },
                "targets": columnSort["action"]
            }];
            var properties = {
                columns: columns,
                columnDefs: columnDefs,
                columnSort: columnSort,
                func: "getPageData",
                name: "page",
            };
            var $datatable = callDataTables(properties);

            // Filter
            $("#<%= ddlActive.ClientID %>").change(function () {
                $datatable
               .columns(columnSort["PAGE_ACTIVE"])
               .search(this.value)
               .draw();
            });

            $("#<%= ddlShowInMenu.ClientID %>").change(function () {

                $datatable
               .columns(columnSort["PAGE_SHOWINMENU"])
               .search(this.value)
               .draw();
            });

            $("#<%= ddlParent.ClientID %>").change(function () {
                $datatable
               .columns(columnSort["V_PARENTNAME"])
               .search(this.value)
               .draw();
            });

            $("#<%= ddlShowPageTitle.ClientID %>").change(function () {
                $datatable
               .columns(columnSort["PAGE_SHOWPAGETITLE"])
               .search(this.value)
               .draw();
            });

            $("#<%= ddlTemplate.ClientID %>").change(function () {
                $datatable
               .columns(columnSort["PAGE_TEMPLATE"])
               .search(this.value)
               .draw();
            });

            $("#<%= lnkbtnExport.ClientID %>").click(function (e) {
                var $table = $('.dataTable').clone();
                $table.attr("border", "1").find("th:last-child,td:last-child").remove();
                window.open('data:application/vnd.ms-excel,' + encodeURIComponent($table.get(0).outerHTML));

                e.preventDefault();
            });

            $(document).on("click", ".lnkbtnDelete", function () {
                if (confirm("<%= HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeletePage.Text") %>")) {
                    var pageID = $(this).attr("data-id");
                    document.getElementById('<%= hdnPageID.ClientID %>').value= pageID;
                    document.getElementById('<%= lnkbtnDelete.ClientID %>').click();
                }
            })
            
        });
    </script>
</asp:Content>
