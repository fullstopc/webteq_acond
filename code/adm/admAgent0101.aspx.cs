﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adm_admMember0101 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 1002;
    protected int _agentId = 0;
    protected int _mode = 1;
    protected int _formSect = 1;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    public string strSavedMsg
    {
        get { return String.Format(GetGlobalResourceObject("GlobalResource", "admSavedMessageWithParams.Text").ToString(), "Agent"); }
    }
    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int agentId
    {
        get
        {
            if (ViewState["agentId"] == null)
            {
                return _agentId;
            }
            else
            {
                return Convert.ToInt16(ViewState["agentId"]);
            }
        }
        set { ViewState["agentId"] = value; }
    }

    protected int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set
        {
            ViewState["FORMSECT"] = value;
        }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {

            Master.sectId = sectId;
            Master.moduleDesc = "";

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;
                if (int.TryParse(Request["id"], out intTryParse))
                {
                    agentId = intTryParse;
                }
                else { agentId = int.MaxValue; }
            }

            if (Request["id"] != null)
            {
                mode = 2;
            }

            Master.sectId = sectId;
            Master.subSectId = clsMis.getSubSectId(sectId, formSect, 1);
            Master.parentId = sectId;
            Master.showSideMenu = true;
        }

        setPageProperties();
    }
    #endregion

    #region "Methods"
    protected void setPageProperties()
    {
        
        if (mode == 1)
        {
            Page.Title += clsMis.formatPageTitle("Add Agent", true);
            litPageTitle.Text = "Add Service Agent";
        }
        else
        {
            Page.Title += clsMis.formatPageTitle("Edit Agent", true);
            litPageTitle.Text = "Edit Service Agent";
        }

        if (!IsPostBack)
        {

        }

        new Acknowledge(Page)
        {
            container = pnlAck,
            msg = litAck
        }.init();
        ucAdmAgentDetails.mode = mode;
        ucAdmAgentDetails.agentId = agentId;
        ucAdmAgentDetails.fill();
    }
    #endregion
}
