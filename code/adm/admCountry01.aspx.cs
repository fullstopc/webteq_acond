﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class adm_admCountry01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 38;
    protected int _countryId = 0;
    protected int _mode = 1;

    string countryCode = "";
    string countryName = "";
    int active;

    protected string _gvSortExpression = "COUNTRY_ID";
    protected string _gvSortDirection = clsAdmin.CONSTSORTDESC;
    protected string _gvSort = "";
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public int countryId
    {
        get { return _countryId; }
        set { _countryId = value; }
    }

    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public string gvSortDirection
    {
        get { return ViewState["GVSORTDIRECTION"] as string ?? _gvSortDirection; }
        set { ViewState["GVSORTDIRECTION"] = value; }
    }

    public string gvSortExpression
    {
        get { return ViewState["GVSORTEXPRESSION"] as string ?? _gvSortExpression; }
        set { ViewState["GVSORTEXPRESSION"] = value; }
    }

    public string gvSort
    {
        get { return ViewState["GVSORT"] as string ?? _gvSort; }
        set { ViewState["GVSORT"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!string.IsNullOrEmpty(Request["id"]))
        {
            int intTryParse;
            if (int.TryParse(Request["id"], out intTryParse))
            {
                countryId = intTryParse;
            }
            else
            {
                countryId = 0;
            }
        }

        if (!IsPostBack)
        {
            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMCOUNTRYMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMELISTINGSIZE, clsConfig.CONSTGROUPOTHERSETTINGS, 1);

            gvCountry.PagerSettings.FirstPageText = GetGlobalResourceObject("GlobalResource", "contentAdmFirstPageText.Text").ToString();
            gvCountry.PagerSettings.LastPageText = GetGlobalResourceObject("GlobalResource", "contentAdmLastPageText.Text").ToString();
            gvCountry.PageSize = (!string.IsNullOrEmpty(config.value)) ? Convert.ToInt16(config.value) : clsAdmin.CONSTADMPAGESIZE;

            if (Request["id"] != null || Session["EDITEDCOUNTRYID"] != null || Session["DELETEDCOUNTRYNAME"] != null)
            {
                mode = 2;
            }

            bindGV(gvCountry, gvSort);
            setPageProperties();
        }

        if (Session["DELETEDCOUNTRYNAME"] != null)
        {
            pnlAck.Visible = true;
            litAck.Text = Session["DELETEDCOUNTRYNAME"] + " has been deleted successfully.";

            clsMis.resetListing();
            Session["DELETEDCOUNTRYNAME"] = null;
        }
        else if (Session["NEWCOUNTRYID"] != null)
        {
            pnlAck.Visible = true;
            litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";

            clsMis.resetListing();
            Session["NEWCOUNTRYID"] = null;
        }
        else if (Session["EDITEDCOUNTRYID"] != null)
        {
            if (Session["NOCHANGE"] != null)
            {
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
            }
            else
            {
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }

            pnlAck.Visible = true;

            Session["EDITEDCOUNTRYID"] = null;
            Session["NOCHANGE"] = null;
        }
        else if (Session["UPDATEDCOUNTRY"] != null)
        {
            if (Session["NOCHANGE"] != null)
            {
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
            }
            else
            {
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admSavedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }

            pnlAck.Visible = true;

            Session["UPDATEDCOUNTRY"] = null;
            Session["NOCHANGE"] = null;
        }
    }

    protected void lnkbtnSaveCountry_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            countryCode = txtCountryCode.Text.Trim();
            countryName = txtCountry.Text.Trim();
            active = chkboxActive.Checked ? 1 : 0;

            clsCountry country = new clsCountry();
            int intRecordAffected = 0;

            string strErrMsg = string.Empty;
            strErrMsg = checkData();

            if (string.IsNullOrEmpty(strErrMsg))
            {
                switch (mode)
                {
                    case 1:
                        intRecordAffected = country.addCountry(countryCode, countryName, active);

                        if (intRecordAffected == 1)
                        {
                            Session["NEWCOUNTRYID"] = country.countryId;
                            Response.Redirect(currentPageName + "?id=" + country.countryId);
                        }
                        else
                        {
                            Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to add new country. Please try again.</div>";
                            Response.Redirect(currentPageName);
                        }
                        break;
                    case 2:
                        Boolean boolEdited = false;
                        Boolean boolSuccess = true;

                        if (!country.isExactSameCountryDataSet(countryId, countryCode, countryName, active))
                        {
                            intRecordAffected = country.updateCountryById(countryId, countryCode, countryName, active);

                            if (intRecordAffected == 1)
                            {
                                boolEdited = true;
                            }
                            else
                            {
                                boolSuccess = false;
                            }
                        }

                        if (boolEdited)
                        {
                            Session["EDITEDCOUNTRYID"] = country.countryId;
                            Response.Redirect(currentPageName + "?id=" + countryId);
                        }
                        else
                        {
                            if (!boolSuccess)
                            {
                                country.extractCountryById(countryId, 0);
                                Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "contentAdmErrHdr.Text") + "<br />Failed to edit " + country.countryName + ". Please try again.</div>";
                                Response.Redirect(currentPageName + "?id=" + countryId);
                            }
                            else
                            {
                                Session["EDITEDCOUNTRYID"] = countryId;
                                Session["NOCHANGE"] = 1;
                                Response.Redirect(currentPageName + "?id=" + countryId);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            else
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"errmsg\">" + strErrMsg + "</div>";
            }
        }
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            clsCountry country = new clsCountry();
            int intCount = 0;
            Boolean boolSuccess = true;
            Boolean boolEdited = false;
            int intRecordAffected = 0;

            foreach (GridViewRow row in gvCountry.Rows)
            {
                int intCountryId = Convert.ToInt16(gvCountry.DataKeys[intCount].Value.ToString());
                TextBox txtCountryOrder = (TextBox)row.FindControl("txtCountryOrder");
                HiddenField hdnCountryOrder = (HiddenField)row.FindControl("hdnCountryOrder");

                int intOrder = !string.IsNullOrEmpty(txtCountryOrder.Text.Trim()) ? Convert.ToInt16(txtCountryOrder.Text.Trim()) : 0;
                int intOrderOld = !string.IsNullOrEmpty(hdnCountryOrder.Value.Trim()) ? Convert.ToInt16(hdnCountryOrder.Value.Trim()) : 0;

                if (intOrder != intOrderOld)
                {
                    intRecordAffected = country.updateOrderById(intCountryId, intOrder);

                    if (intRecordAffected == 1) { boolEdited = true; }
                    else
                    {
                        boolSuccess = false;
                        break;
                    }
                }

                intCount += 1;
            }

            if (boolEdited)
            {
                Session["UPDATEDCOUNTRY"] = 1;

            }
            else
            {
                if (!boolSuccess)
                {
                    Session["ERRMSG"] = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
                }
                else
                {
                    Session["UPDATEDCOUNTRY"] = 1;
                    Session["NOCHANGE"] = 1;
                }
            }

            Response.Redirect(Request.Url.ToString());
        }
    }

    protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        string strDeletedCountryName = "";

        clsMis.performDeleteCountry(countryId, ref strDeletedCountryName);

        Session["DELETEDCOUNTRYNAME"] = strDeletedCountryName;
        Response.Redirect(currentPageName);
    }

    protected void gvCountry_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkbtnEdit = (LinkButton)e.Row.FindControl("lnkbtnEdit");
            LinkButton lnkbtnDelete = (LinkButton)e.Row.FindControl("lnkbtnDelete");

            lnkbtnEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteCountry.Text") + "');";

            int intOrder = Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "COUNTRY_ORDER"));

            TextBox txtCountryOrder = (TextBox)e.Row.FindControl("txtCountryOrder");
            txtCountryOrder.Text = intOrder.ToString();
        }
    }

    protected void gvCountry_Sorting(object sender, GridViewSortEventArgs e)
    {
        string strSortExpDir = "";
        if (e.SortExpression != gvSortExpression)
        {
            gvSortExpression = e.SortExpression;
            gvSortDirection = clsAdmin.CONSTSORTASC;
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }
        else
        {
            switch (gvSortDirection)
            {
                case clsAdmin.CONSTSORTASC:
                    gvSortDirection = clsAdmin.CONSTSORTDESC;
                    break;
                case clsAdmin.CONSTSORTDESC:
                    gvSortDirection = clsAdmin.CONSTSORTASC;
                    break;
            }
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }

        gvSort = strSortExpDir;
        bindGV(gvCountry, gvSort); 
    }

    protected void gvCountry_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCountry.PageIndex = e.NewPageIndex;
        bindGV(gvCountry, gvSort);
    }

    protected void gvCountry_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "cmdEdit")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int countryId = Convert.ToInt16(gvCountry.DataKeys[rowIndex].Value.ToString());

            Response.Redirect("admCountry01.aspx?id=" + countryId);
        }
        else if (e.CommandName == "cmdDelete")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int countryId = Convert.ToInt16(gvCountry.DataKeys[rowIndex].Value.ToString());
            string strDeletedCountryName = "";

            clsCountry country = new clsCountry();
            country.countryId = countryId;
            country.extractCountryById(countryId, 0);

            int intRecordAffected = country.deleteCountry(country.countryId);

            if (intRecordAffected == 1)
            {
                strDeletedCountryName = country.countryName;
            }

            Response.Redirect(currentPageName);
        }
    }
    #endregion

    #region "Methods"
    protected string checkData()
    {
        clsCountry country = new clsCountry();

        if (country.isCountryExist(countryCode, countryName))
        {
            return "This country is registered. Please try another.";
        }

        return string.Empty;
    }

    protected void bindGV(GridView gvRef, string gvSortExpDir)
    {
        DataSet ds = new DataSet();
        DataView dv = new DataView();

        clsCountry country = new clsCountry();
        ds = country.getCountryList(0);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "COUNTRY_NAME ASC";

        if (!string.IsNullOrEmpty(gvSortExpDir))
        {
            dv.Sort = gvSortExpDir;
        }

        gvCountry.DataSource = dv;
        gvCountry.DataBind();
    }

    protected void setPageProperties()
    {
        switch (mode)
        {
            case 1:
                break;
            case 2:
                lnkbtnDelete.Visible = true;
                fillForm();

                if ((Session["EDITEDCOUNTRYID"] == null) && (Session["DELETEDCOUNTRYNAME"] == null)) 
                {
                    fillForm(); 
                }
                break;
            default:
                break;
        }

        lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteCountry.Text") + "');";
    }

    protected void fillForm()
    {
        clsCountry country = new clsCountry();

        if (country.extractCountryById(countryId, 0))
        {
            txtCountryCode.Text = country.countryCode;
            txtCountry.Text = country.countryName;

            if (country.countryActive == 1) { chkboxActive.Checked = true; }
            else { chkboxActive.Checked = false; }
        }
    }
    #endregion
}
