﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;

public partial class adm_admSlideGroup0101 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 8;
    protected string grping = "GROUPING";
    protected int _mode = 1;
    protected int _groupId = 0;
    protected int _formSect = 1;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }
    public string strSavedMsg
    {
        get { return String.Format(GetGlobalResourceObject("GlobalResource", "admSavedMessageWithParams.Text").ToString(), "Slide Group"); }
    }
    public int mode
    {
        get
        {
            if (ViewState["MODE"] == null)
            {
                return _mode;
            }
            else
            {
                return Convert.ToInt16(ViewState["MODE"]);
            }
        }
        set { ViewState["MODE"] = value; }
    }

    public int groupId
    {
        get
        {
            if (ViewState["groupId"] == null)
            {
                return _groupId;
            }
            else
            {
                return Convert.ToInt16(ViewState["groupId"]);
            }
        }
        set { ViewState["groupId"] = value; }
    }

    public int formSect
    {
        get
        {
            if (ViewState["FORMSECT"] == null)
            {
                return _formSect;
            }
            else
            {
                return Convert.ToInt16(ViewState["FORMSECT"]);
            }
        }
        set { ViewState["FORMSECT"] = value; }
    }
    #endregion


    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                int intTryParse;
                if (int.TryParse(Request["id"], out intTryParse))
                {
                    groupId = intTryParse;
                }
                else
                {
                    groupId = 0;
                }
            }
            else if (Session["NEWGRPID"] != null)
            {
                try
                {
                    groupId = Convert.ToInt16(Session["NEWGRPID"]);
                }
                catch (Exception ex)
                {
                    groupId = 0;
                }
            }
            else if (Session["EDITGRPID"] != null)
            {
                try
                {
                    groupId = Convert.ToInt16(Session["EDITGRPID"]);
                }
                catch (Exception ex)
                {
                    groupId = 0;
                }
            }

            if (!string.IsNullOrEmpty(Request["frm"]))
            {
                int intTryParse;

                if (int.TryParse(Request["frm"], out intTryParse))
                {
                    formSect = intTryParse;
                }
                else
                {
                    formSect = 1;
                }
            }

            if ((!string.IsNullOrEmpty(Request["id"])) || (Session["EDITGRPID"] != null))
            {
                mode = 2;
            }

            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMSLIDESHOWGROUPNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.parentId = sectId;
            Master.subSectId = clsMis.getSubSectId(sectId, formSect, 1);
            Master.id = groupId;
            Master.showSideMenu = true;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();
        }

        setPageProperties();
        Session["NEWGRPID"] = null;
        Session["EDITGRPID"] = null;
        Session["NOCHANGE"] = null;
        Session["DELETEDGRPNAME"] = null;
        Session["MOVEDPRODNO"] = 0;
        Session["ERRMSG"] = null;
    }
    #endregion


    #region "Methods"
    protected void setPageProperties()
    {
        switch (mode)
        {
            case 1:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleAdd.Text").ToString(), true);
                break;
            case 2:
                Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitleEdit.Text").ToString(), true);
                break;
            default:
                break;
        }

        if (!IsPostBack)
        {
            switch (mode)
            {
                case 1:
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleAdd.Text").ToString();

                    // exclude default group - Other
                    clsMasthead mast = new clsMasthead();
                    int intTotal = mast.getTotalRecordGroup(0, 0);
                    if (clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITSLIDESHOW, intTotal)) { Response.Redirect(ConfigurationManager.AppSettings["scriptBase"] + "adm/admSlideGroup01.aspx"); }
                    break;
                case 2:
                    litPageTitle.Text = GetLocalResourceObject("litPageTitleEdit.Text").ToString();
                    break;
                default:
                    break;
            }

            if (Session["NEWGRPID"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + strSavedMsg + "</div>";

                clsMis.resetListing();
            }
            else if (Session["EDITGRPID"] != null)
            {
                if (Session["NOCHANGE"] != null)
                {
                    litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admNoChangesMessage.Text").ToString() + "</div>";
                }
                else
                {
                    int intTotal = new clsMasthead().getTotalRecordGroup(0, 0);
                    string strUrlFormpage = "<a href='admSlideGroup0101.aspx'>Add New</a>";
                    string strUrlListpage = "<a href='admSlideGroup01.aspx'>Back to Listing</a>";

                    litAck.Text = "<div class=\"noticemsg\">" + strSavedMsg + "  " + strUrlListpage + "";
                    if (!clsMis.isLimitReach(clsAdmin.CONSTPROJECTCONFIGLIMITSLIDESHOW, intTotal - 1))
                    {
                        litAck.Text += " & " + strUrlFormpage + "";
                    }
                    litAck.Text += "</div>";

                    clsMis.resetListing();
                }

                pnlAck.Visible = true;
            }
            else if (Session["DELETEDGRPNAME"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"noticemsg\">" + GetGlobalResourceObject("GlobalResource", "admDeletedMessage.Text").ToString() + "</div>";

                clsMis.resetListing();
            }
            else if (Session["ERRMSG"] != null)
            {
                pnlAck.Visible = true;
                litAck.Text = "<div class=\"errmsg\">" + GetGlobalResourceObject("GlobalResource", "admFailedMessage.Text").ToString() + "</div>";
            }
        }

        ucAdmSlideGroup.mode = mode;
        ucAdmSlideGroup.groupId = groupId;
        ucAdmSlideGroup.fill();
    }
    #endregion
}
