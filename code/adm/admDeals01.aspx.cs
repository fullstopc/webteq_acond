﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.IO;

public partial class adm_admDeals01 : System.Web.UI.Page
{
    #region "Properties"
    protected int sectId = 15;
    protected string _gvSortExpression = "DEAL_ID";
    protected string _gvSortDirection = clsAdmin.CONSTSORTDESC;
    protected string _gvSort = "";

    protected string strKeyword;
    protected int intBizType;
    protected int intActive;
    protected int intArea;
    protected int int_Merchant;
    protected Boolean boolSearch;
    #endregion

    #region "Property Methods"
    public string currentPageName
    {
        get { return clsMis.getCurrentPageName(); }
    }

    public string gvSortDirection
    {
        get { return ViewState["GVSORTDIRECTION"] as string ?? _gvSortDirection; }
        set { ViewState["GVSORTDIRECTION"] = value; }
    }

    public string gvSortExpression
    {
        get { return ViewState["GVSORTEXPRESSION"] as string ?? _gvSortExpression; }
        set { ViewState["GVSORTEXPRESSION"] = value; }
    }

    public string gvSort
    {
        get { return ViewState["GVSORT"] as string ?? _gvSort; }
        set { ViewState["GVSORT"] = value; }
    }
    #endregion

    #region "Page Events"
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title += clsMis.formatPageTitle(GetLocalResourceObject("litPageTitle.Text").ToString(), true);

        if (!IsPostBack)
        {
            clsAdmin adm = new clsAdmin();
            adm.extractAdminPageByName(clsAdmin.CONSTADMDEALSMANAGERNAME, 1);
            sectId = adm.pageId;

            Master.sectId = sectId;
            Master.moduleDesc = GetLocalResourceObject("litPageInfo.Text").ToString();

            txtKeyword.Attributes.Add("onkeydown", "javascript:if(event.keyCode==13){document.getElementById('" + lnkbtnSearch.ClientID + "').click(); return false;}");

            clsConfig config = new clsConfig();
            config.extractItemByNameGroup(clsConfig.CONSTNAMELISTINGSIZE, clsConfig.CONSTGROUPOTHERSETTINGS, 1);

            
            gvSort = gvSortExpression + " " + gvSortDirection;
            bindGV(gvDeal, "DEAL_ID ASC");
            setSearchProperties();
        }

        if (Session["DELETEDDEALNAME"] != null)
        {
            pnlAck.Visible = true;
            litAck.Text = "Deal (" + Session["DELETEDDEALNAME"] + ") has been deleted successfully.";
            litAck.Text = "<div class=\"noticemsg\">" + litAck.Text + "</div>";

            Session["DELETEDDEALNAME"] = null;

            clsMis.resetListing();
        }
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        boolSearch = true;
        strKeyword = txtKeyword.Text.Trim();

        bindGV(gvDeal, "DEAL_ID ASC");
    }

    protected void lnkbtnExport_Click(object sender, EventArgs e)
    {

    }

    protected void gvDeal_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell tc in e.Row.Cells)
            {
                if (tc.HasControls())
                {
                    LinkButton lnkbtn = (LinkButton)tc.Controls[0];
                    //Image icon = new Image();
                    //icon.ImageUrl = "../img/adm/btn-cancel.gif";

                    if (lnkbtn != null && gvSortExpression == lnkbtn.CommandArgument)
                    {
                        //tc.Controls.Add(icon);
                        string strClass = (gvSortDirection == "DESC") ? "desc" : "asc";
                        tc.CssClass = strClass;
                    }


                }
            }
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int intDealId = int.Parse(DataBinder.Eval(e.Row.DataItem, "DEAL_ID").ToString());
            string strStartDate = DataBinder.Eval(e.Row.DataItem, "DEAL_STARTDATE").ToString();
            string strEndDate = DataBinder.Eval(e.Row.DataItem, "DEAL_ENDDATE").ToString();

            HyperLink hypEdit = (HyperLink)e.Row.FindControl("hypEdit");
            LinkButton lnkbtnDelete = (LinkButton)e.Row.FindControl("lnkbtnDelete");

            hypEdit.Text = GetGlobalResourceObject("GlobalResource", "contentAdmEdit.Text").ToString();
            hypEdit.NavigateUrl = ConfigurationManager.AppSettings["scriptBase"] + "adm/admDeals0101.aspx?id=" + intDealId;

            lnkbtnDelete.Text = GetGlobalResourceObject("GlobalResource", "contentAdmDelete.Text").ToString();
            lnkbtnDelete.OnClientClick = "javascript:return confirm('" + HttpContext.GetGlobalResourceObject("GlobalResource", "contentAdmConfirmDeleteDeal.Text") + "');";

            Literal litStartDate = (Literal)e.Row.FindControl("litStartDate");
            litStartDate.Text = "";
        }
    }

    protected void gvDeal_Sorting(object sender, GridViewSortEventArgs e)
    {
        string strSortExpDir = "";
        if (e.SortExpression != gvSortExpression)
        {
            gvSortExpression = e.SortExpression;
            gvSortDirection = clsAdmin.CONSTSORTASC;
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }
        else
        {
            switch (gvSortDirection)
            {
                case clsAdmin.CONSTSORTASC:
                    gvSortDirection = clsAdmin.CONSTSORTDESC;
                    break;
                case clsAdmin.CONSTSORTDESC:
                    gvSortDirection = clsAdmin.CONSTSORTASC;
                    break;
            }
            strSortExpDir = e.SortExpression + " " + gvSortDirection;
        }

        gvSort = strSortExpDir;
        bindGV(gvDeal, gvSort);
    }

    protected void gvDeal_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDeal.PageIndex = e.NewPageIndex;
        bindGV(gvDeal, gvSort);
    }

    protected void gvDeal_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "cmdDelete")
        {
            int rowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int intDealId = Convert.ToInt16(gvDeal.DataKeys[rowIndex].Value.ToString());
            string uploadPath = Server.MapPath(ConfigurationManager.AppSettings["uplBase"].ToString() + "/" + clsAdmin.CONSTADMDEALFOLDER + "/");
            string strDeletedDealName = "";

            clsMis.performDeleteDeal(intDealId, uploadPath, ref strDeletedDealName);
            Session["DELETEDDEALNAME"] = strDeletedDealName;

            Response.Redirect(currentPageName);
        }
    }
    #endregion

    #region "Methods"
    protected void setSearchProperties()
    {
        clsDirGroup dirGroup = new clsDirGroup();
        clsDirMerchant dirMerchant = new clsDirMerchant();
        clsArea area = new clsArea();
        DataSet ds = new DataSet();
        DataView dv = new DataView();

        /*Business Type*/
        ds = dirGroup.getGrpListByGrpingId(clsAdmin.CONSTLISTGROUPINGBIZTYPE, 1);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "GRP_ORDER ASC";

        ddlBizType.DataSource = dv;
        ddlBizType.DataTextField = "GRP_DNAME";
        ddlBizType.DataValueField = "GRP_ID";
        ddlBizType.DataBind();

        ListItem ddlBizTypeDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlBizType.Items.Insert(0, ddlBizTypeDefaultItem);
        /*End of Business Type*/

        /*Area*/
        ds = area.getAreaList();
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "AREA_NAME ASC";

        ddlArea.DataSource = dv;
        ddlArea.DataTextField = "AREA_NAME";
        ddlArea.DataValueField = "AREA_ID";
        ddlArea.DataBind();

        ListItem ddlAreaDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlArea.Items.Insert(0, ddlAreaDefaultItem);
        /*End of Area*/

        /*Merchant*/
        ds = dirMerchant.getMemberList(0);
        dv = new DataView(ds.Tables[0]);
        dv.Sort = "MEM_NAME";

        ddlMerchant.DataSource = dv;
        ddlMerchant.DataTextField = "MEM_NAME";
        ddlMerchant.DataValueField = "MEM_ID";
        ddlMerchant.DataBind();

        ListItem ddlMerchantDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlMerchant.Items.Insert(0, ddlMerchantDefaultItem);
        /*End of Merchant*/

        ListItem ddlActiveDefaultItem = new ListItem(GetGlobalResourceObject("GlobalResource", "ddlAll.Text").ToString(), "");
        ddlActive.Items.Insert(0, ddlActiveDefaultItem);
        ddlActive.Items.Add(new ListItem("Yes", "1"));
        ddlActive.Items.Add(new ListItem("No", "0"));
    }

    protected void bindGV(GridView gvDealRef, string gvSortExpDir)
    {
        clsDeal deal = new clsDeal();
        DataSet dsDeal = new DataSet();
        dsDeal = deal.getDealList(0);
        DataView dvDeal = new DataView(dsDeal.Tables[0]);
        dvDeal.RowFilter = "1 = 1";

        Boolean boolFilter = false;

        if (boolSearch)
        {
            if (!string.IsNullOrEmpty(strKeyword))
            {
                dvDeal.RowFilter += " AND (DEAL_CODE LIKE '%" + strKeyword + "%' OR DEAL_NAME LIKE '%" + strKeyword + "%')";

                litItemFound.Text = "Search for <span class=\"SectKeyword\">" + strKeyword + "</span> | ";
                boolFilter = true;
            }
            else { litItemFound.Text = ""; }

            if (!string.IsNullOrEmpty(ddlBizType.SelectedValue))
            {
                dvDeal.RowFilter += " AND DEAL_BIZTYPE = " + Convert.ToInt16(ddlBizType.SelectedValue);
                boolFilter = true;
            }

            if (!string.IsNullOrEmpty(ddlActive.SelectedValue))
            {
                dvDeal.RowFilter += " AND DEAL_ACTIVE = " + Convert.ToInt16(ddlActive.SelectedValue);
                boolFilter = true;
            }

            if (!string.IsNullOrEmpty(ddlArea.SelectedValue))
            {
                dvDeal.RowFilter += " AND DEAL_AREA = " + Convert.ToInt16(ddlArea.SelectedValue);
                boolFilter = true;
            }

            if (!string.IsNullOrEmpty(ddlMerchant.SelectedValue))
            {
                dvDeal.RowFilter += " AND DEAL_MERCHANT = " + Convert.ToInt16(ddlMerchant.SelectedValue);
                boolFilter = true;
            }
        }

        if (boolFilter)
        {
            //pnlSearchForm.CssClass = "divSearchFormShow";
            //hypHideShow.CssClass = "linkHide";
        }
        else
        {
            //pnlSearchForm.CssClass = "divSearchForm";
            //hypHideShow.CssClass = "linkShow";
        }

        if (gvSortExpDir != "")
        {
            dvDeal.Sort = gvSortExpDir;
        }

        Session["PRODDV"] = dvDeal;

        gvDealRef.DataSource = dvDeal;
        gvDealRef.DataBind();

        if (boolSearch) { litItemFound.Text += dvDeal.Count + " record(s) listed."; }
        else { litItemFound.Text = dvDeal.Count + " record(s) listed."; }

        if (dvDeal.Count > 0)
        {
            lnkbtnExport.Enabled = true;
            //lnkbtnExport.CssClass = "btn2 btnExport";
        }
        else
        {
            lnkbtnExport.Enabled = false;
            lnkbtnExport.CssClass += " disabled";
        }
    }

    protected void ExportGV(GridView gvRef, string strFileTitle, DataView dvItems, Boolean boolHideLastColumn)
    {
        DateTime dt = DateTime.Now;
        string strFileName = strFileTitle + "_" + dt.Day + dt.Month + dt.Year + ".xls";
        string strAttachment = "attachment; filename=" + strFileName;

        Response.ClearContent();
        Response.AddHeader("content-disposition", strAttachment);
        Response.ContentType = "application/vnd.xls";
        Response.ContentEncoding = System.Text.Encoding.Unicode;
        Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        gvRef.AllowPaging = false;
        gvRef.DataSource = dvItems;
        gvRef.DataBind();

        for (int i = 0; i < gvRef.Columns.Count; i++)
        {
            if (gvRef.HeaderRow.Cells[i].CssClass == "hiddenCol")
            {
                gvRef.HeaderRow.Cells[i].CssClass = "showCol";
            }

            gvRef.HeaderRow.Cells[i].Enabled = false;
        }

        for (int i = 0; i < gvRef.Rows.Count; i++)
        {
            GridViewRow row = gvRef.Rows[i];

            for (int j = 0; j < gvRef.Columns.Count; j++)
            {
                if (row.Cells[j].CssClass == "hiddenCol")
                {
                    row.Cells[j].CssClass = "showCol";
                }
            }
        }

        if (boolHideLastColumn)
        {
            int intColHide = gvRef.Columns.Count - 1;

            gvRef.HeaderRow.Cells[intColHide].Visible = false;
            gvRef.Columns[intColHide].Visible = false;
        }

        gvRef.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }
    #endregion
}
