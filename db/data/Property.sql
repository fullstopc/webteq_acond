TRUNCATE TABLE tb_property;
INSERT INTO tb_property
(member_id, property_area, property_unitno, property_type, property_creation, property_lastupdated)
VALUES
(4, 1, 'B-01', 2, NOW(), NOW()),
(4, 2, 'B-02', 1, NOW(), NOW()),
(5, 3, 'B-03', 2, NOW(), NOW());