DROP TABLE IF EXISTS tb_reservation_aircond;
CREATE TABLE `tb_reservation_aircond` 
  ( 
     `id`            INT NOT NULL auto_increment, 
     `aircond_id`       INT NOT NULL DEFAULT 0, 
     `aircond_name`     VARCHAR(250) NOT NULL,
     `aircond_charge`   DECIMAL NOT NULL DEFAULT 0,
     `res_id`           INT NOT NULL DEFAULT 0, 
     PRIMARY KEY (`id`) 
  ) 
engine = innodb; 