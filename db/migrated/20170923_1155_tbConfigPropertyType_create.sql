DROP TABLE IF EXISTS `tb_config_property_type`;
CREATE TABLE `tb_config_property_type` 
  ( 
     `type_id`          INT NOT NULL, 
     `type_name`        VARCHAR(250) NOT NULL, 
     `type_aircond_qty` INT NOT NULL DEFAULT '1', 
     `type_creation`    DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
     `type_createdby`   INT NOT NULL DEFAULT '0', 
     `type_lastupdated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
     `type_updatedby`   INT NOT NULL DEFAULT '0' 
  ) 
engine = innodb; 

ALTER TABLE `tb_config_property_type` ADD PRIMARY KEY(type_id);
ALTER TABLE `tb_config_property_type` CHANGE `type_id` `type_id` INT(11) NOT NULL AUTO_INCREMENT;