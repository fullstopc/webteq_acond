UPDATE tb_property SET property_area = 0, property_type = 0;

ALTER TABLE tb_property 
MODIFY COLUMN property_area INT NOT NULL DEFAULT 0;

ALTER TABLE tb_property 
MODIFY COLUMN property_type INT NOT NULL DEFAULT 0;