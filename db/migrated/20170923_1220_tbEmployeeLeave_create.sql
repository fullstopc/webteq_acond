DROP TABLE IF EXISTS `tb_employee_leave`;
CREATE TABLE `tb_employee_leave` 
  ( 
     `leave_id`          INT NOT NULL auto_increment, 
     `leave_type`        INT NOT NULL, 
     `leave_start`       DATETIME NOT NULL, 
     `leave_end`         DATETIME NOT NULL, 
     `leave_reason`      TEXT NOT NULL, 
     `leave_creation`    DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
     `leave_createdby`   INT NOT NULL DEFAULT '0', 
     `leave_lastupdated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
     `leave_updatedby`   INT NOT NULL DEFAULT '0', 
     `employee_id`       INT NOT NULL DEFAULT '0',
     PRIMARY KEY (`leave_id`) 
  ) 
engine = innodb; 