DROP TABLE IF EXISTS tb_config_property_area;
CREATE TABLE `tb_config_property_area` 
  ( 
     `area_id`          INT NOT NULL, 
     `area_name`        VARCHAR(250) NOT NULL, 
     `area_creation`    DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
     `area_createdby`   INT NOT NULL DEFAULT '0', 
     `area_lastupdated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
     `area_updatedby`   INT NOT NULL DEFAULT '0'
  ) 
engine = innodb; 

ALTER TABLE `tb_config_property_area` ADD PRIMARY KEY(area_id);
ALTER TABLE `tb_config_property_area` CHANGE `area_id` `area_id` INT(11) NOT NULL AUTO_INCREMENT;