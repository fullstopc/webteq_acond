DROP TABLE IF EXISTS tb_config_service_type;
CREATE TABLE `tb_config_service_type` 
  ( 
     `type_id`            INT NOT NULL auto_increment, 
     `type_name`          VARCHAR(250) NOT NULL, 
     `type_duration`      DECIMAL NOT NULL, 
     `type_duration_unit` VARCHAR(20) NOT NULL DEFAULT 'MINUTES', 
     `type_charge`        DECIMAL NOT NULL DEFAULT 0, 
     `type_creation`      DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
     `type_createdby`     INT NOT NULL DEFAULT '0', 
     `type_lastupdated`   DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
     `type_updatedby`     INT NOT NULL DEFAULT '0', 
     PRIMARY KEY (`type_id`) 
  ) 
engine = innodb; 