CREATE TABLE IF NOT EXISTS `tb_ad` (
  `AD_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `AD_NAME` varchar(250) DEFAULT NULL,
  `AD_URL` varchar(1000) DEFAULT NULL,
  `AD_BANNER` varchar(250) DEFAULT NULL,
  `AD_STARTDATE` datetime DEFAULT NULL,
  `AD_ENDDATE` datetime DEFAULT NULL,
  `AD_POSITION` int(9) unsigned NOT NULL DEFAULT '0',
  `AD_CREATION` datetime NOT NULL,
  `AD_CREATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `AD_LASTUPDATE` datetime DEFAULT NULL,
  `AD_UPDATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`AD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_adjust`
--

CREATE TABLE IF NOT EXISTS `tb_adjust` (
  `AD_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `AD_CODE` varchar(20) CHARACTER SET utf8 NOT NULL,
  `AD_DATE` datetime NOT NULL,
  `AD_TITLE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `AD_ADJUSTBY` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `AD_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `AD_CREATION` datetime NOT NULL,
  `AD_CREATEDBY` int(9) unsigned NOT NULL DEFAULT '0',
  `AD_LASTUPDATE` datetime DEFAULT NULL,
  `AD_UPDATEDBY` int(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`AD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_adjustitem`
--

CREATE TABLE IF NOT EXISTS `tb_adjustitem` (
  `AI_ID` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `AI_PRODID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `AI_PRODCODE` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `AI_PRODNAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `AI_PRODQTY` int(9) NOT NULL DEFAULT '0',
  `AD_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `AI_CREATION` datetime NOT NULL,
  `AI_CREATEDBY` int(9) unsigned NOT NULL DEFAULT '0',
  `AI_LASTUPDATE` datetime DEFAULT NULL,
  `AI_UPDATEDBY` int(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`AI_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE IF NOT EXISTS `tb_admin` (
  `ADM_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `ADM_EMAIL` varchar(250) CHARACTER SET utf8 NOT NULL,
  `ADM_PWD` varchar(250) NOT NULL,
  `ADM_IMAGE` varchar(250) DEFAULT NULL,
  `ADM_NAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `ADM_TYPE` int(9) NOT NULL DEFAULT '0',
  `ADM_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `ADM_CREATION` datetime NOT NULL,
  `ADM_CREATEDBY` bigint(9) NOT NULL DEFAULT '0',
  `ADM_LASTUPDATE` datetime DEFAULT NULL,
  `ADM_UPDATEDBY` bigint(9) NOT NULL DEFAULT '0',
  `ADM_LASTLOGIN` datetime DEFAULT NULL,
  PRIMARY KEY (`ADM_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`ADM_ID`, `ADM_EMAIL`, `ADM_PWD`, `ADM_IMAGE`, `ADM_NAME`, `ADM_TYPE`, `ADM_ACTIVE`, `ADM_CREATION`, `ADM_CREATEDBY`, `ADM_LASTUPDATE`, `ADM_UPDATEDBY`, `ADM_LASTLOGIN`) VALUES
(1, 'admin', '699da314027c6362884c2f4746c34d19', NULL, NULL, 1, 1, '2012-06-11 17:41:34', 0, NULL, 0, '2017-09-22 11:43:08');

-- --------------------------------------------------------

--
-- Table structure for table `tb_adminpage`
--

CREATE TABLE IF NOT EXISTS `tb_adminpage` (
  `ADMPAGE_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `ADMPAGE_NAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `ADMPAGE_VALUE` int(9) unsigned NOT NULL DEFAULT '0',
  `ADMPAGE_URL` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `ADMPAGE_PARENT` bigint(9) unsigned NOT NULL DEFAULT '0',
  `ADMPAGE_ORDER` int(9) unsigned NOT NULL DEFAULT '0',
  `ADMPAGE_ACCESS` int(1) NOT NULL DEFAULT '0',
  `ADMPAGE_MOBILE` int(1) NOT NULL DEFAULT '0',
  `ADMPAGE_SHOWINMENU` int(1) DEFAULT NULL,
  `ADMPAGE_SHOWINTOPMENU` int(1) NOT NULL DEFAULT '0',
  `ADMPAGE_SHOWINSIDEMENU` int(1) NOT NULL DEFAULT '0',
  `ADMPAGE_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`ADMPAGE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1007 ;

--
-- Dumping data for table `tb_adminpage`
--

INSERT INTO `tb_adminpage` (`ADMPAGE_ID`, `ADMPAGE_NAME`, `ADMPAGE_VALUE`, `ADMPAGE_URL`, `ADMPAGE_PARENT`, `ADMPAGE_ORDER`, `ADMPAGE_ACCESS`, `ADMPAGE_MOBILE`, `ADMPAGE_SHOWINMENU`, `ADMPAGE_SHOWINTOPMENU`, `ADMPAGE_SHOWINSIDEMENU`, `ADMPAGE_ACTIVE`) VALUES
(1, 'website', 1, NULL, 0, 1, 0, 0, 1, 1, 0, 0),
(2, 'control panel', 1, 'adm/admCMSControlPanel01.aspx', 1, 5, 0, 0, 1, 1, 0, 0),
(3, 'object manager', 2, 'adm/admCMS01.aspx', 1, 2, 0, 0, 1, 1, 0, 0),
(4, 'object details', 1, 'adm/admCMS0101.aspx?id=<!ID!>', 3, 1, 0, 0, 1, 0, 1, 0),
(5, 'page content manager', 3, 'adm/admPage01.aspx', 1, 1, 0, 0, 1, 1, 0, 0),
(6, 'page details', 1, 'adm/admPage0101.aspx?id=<!ID!>', 5, 1, 0, 0, 1, 0, 1, 0),
(7, 'sibling pages sorting', 2, 'adm/admPage0101.aspx?id=<!ID!>&frm=2', 5, 2, 0, 0, 1, 0, 1, 0),
(8, 'slide show group', 4, 'adm/admSlideGroup01.aspx', 1, 3, 0, 0, 1, 1, 0, 0),
(9, 'slide show group details', 1, 'adm/admSlideGroup0101.aspx?id=<!ID!>', 8, 1, 0, 0, 1, 0, 1, 0),
(10, 'slide show manager', 5, 'adm/admSlide01.aspx', 1, 4, 0, 0, 1, 1, 0, 0),
(11, 'slide show details', 1, 'adm/admSlide0101.aspx?id=<!ID!>', 10, 1, 0, 0, 1, 0, 1, 0),
(12, 'product', 2, NULL, 0, 2, 0, 0, 1, 1, 0, 0),
(13, 'category manager', 1, 'adm/admGroup01.aspx', 12, 2, 0, 0, 1, 1, 0, 0),
(14, 'category details', 1, 'adm/admGroup0101.aspx?id=<!ID!>', 13, 1, 0, 0, 1, 0, 1, 0),
(15, 'product manager', 2, 'adm/admProduct01.aspx', 12, 3, 0, 0, 1, 1, 0, 0),
(16, 'product detail', 1, 'adm/admProduct0101.aspx?id=<!ID!>&frm=1', 15, 1, 0, 0, 1, 0, 1, 0),
(17, 'product content', 2, 'adm/admProduct0101.aspx?id=<!ID!>&frm=2', 15, 2, 0, 0, 1, 0, 1, 0),
(18, 'product pricing', 3, 'adm/admProduct0101.aspx?id=<!ID!>&frm=3', 15, 3, 0, 0, 1, 0, 1, 0),
(19, 'related product', 4, 'adm/admProduct0101.aspx?id=<!ID!>&frm=4', 15, 4, 0, 0, 1, 0, 1, 0),
(20, 'user', 3, NULL, 0, 3, 0, 0, 1, 1, 0, 0),
(21, 'Resident', 1, 'adm/admMember01.aspx', 0, 3, 0, 0, 1, 1, 0, 1),
(22, 'member details', 1, 'adm/admMember0101.aspx?id=<!ID!>', 21, 1, 0, 0, 1, 0, 1, 0),
(23, 'order', 4, NULL, 0, 4, 0, 0, 1, 1, 0, 0),
(24, 'sales manager', 1, 'adm/admOrder01.aspx', 23, 1, 0, 0, 1, 1, 0, 0),
(25, 'shipping manager', 3, 'adm/admShipping01.aspx', 23, 3, 0, 0, 1, 1, 0, 0),
(26, 'shipping details', 1, 'adm/admShipping0101.aspx?id=<!ID!>', 25, 1, 0, 0, 1, 0, 1, 0),
(27, 'shipping waiver details', 2, 'adm/admShipping0102.aspx?id=<!ID!>', 25, 2, 0, 0, 1, 0, 1, 0),
(28, 'event manager', 6, 'adm/admHighlight01.aspx', 1, 9, 0, 0, 1, 1, 0, 0),
(29, 'event details', 1, 'adm/admHighlight0101.aspx?id=<!ID!>', 28, 1, 0, 0, 1, 0, 1, 0),
(30, 'event gallery', 2, 'adm/admHighlight0101.aspx?id=<!ID!>&frm=2', 28, 2, 0, 0, 1, 0, 1, 0),
(31, 'directory', 0, '', 0, 5, 0, 0, 1, 1, 0, 0),
(32, 'business type manager', 0, 'adm/admDir01.aspx', 31, 1, 0, 0, 1, 1, 0, 0),
(33, 'merchant manager', 0, 'adm/admDir02.aspx', 31, 2, 0, 0, 1, 1, 0, 0),
(34, 'merchant details', 1, 'adm/admDir0201.aspx?id=<!ID!>', 33, 1, 0, 0, 1, 0, 1, 0),
(35, 'ad', 1, 'adm/admAd01.aspx', 0, 6, 0, 0, 1, 1, 0, 0),
(36, 'add sales', 2, 'adm/admOrder0102.aspx', 23, 2, 0, 0, 1, 1, 0, 0),
(45, 'program', 0, NULL, 0, 8, 0, 0, 1, 1, 0, 0),
(46, 'program group manager', 1, 'adm/admProgramGroup01.aspx', 45, 1, 0, 0, 1, 1, 1, 0),
(47, 'program manager', 2, 'adm/admProgram01.aspx', 45, 2, 0, 0, 1, 1, 1, 0),
(48, 'program group details', 1, 'adm/admProgramGroup0101.aspx?id=<!ID!>', 46, 1, 0, 0, 1, 0, 1, 0),
(49, 'program group description', 2, 'adm/admProgramGroup0101.aspx?id=<!ID!>&frm=2', 46, 2, 0, 0, 1, 0, 1, 0),
(50, 'program details', 1, 'adm/admProgram0101.aspx?id=<!ID!>', 47, 1, 0, 0, 1, 0, 1, 0),
(51, 'program participation', 2, 'adm/admProgram0101.aspx?id=<!ID!>&frm=2', 47, 2, 0, 0, 1, 0, 1, 0),
(52, 'event description', 3, 'adm/admHighlight0101.aspx?id=<!ID!>&frm=3', 28, 3, 0, 0, 1, 0, 1, 0),
(53, 'event video', 4, 'adm/admHighlight0101.aspx?id=<!ID!>&frm=4', 28, 4, 0, 0, 1, 0, 1, 0),
(54, 'event article', 5, 'adm/admHighlight0101.aspx?id=<!ID!>&frm=5', 28, 5, 0, 0, 1, 0, 1, 0),
(55, 'event comment', 6, 'adm/admHighlight0101.aspx?id=<!ID!>&frm=6', 28, 6, 0, 0, 1, 0, 1, 0),
(56, 'product gallery', 7, 'adm/admProduct0101.aspx?id=<!ID!>&frm=7', 15, 7, 0, 0, 1, 0, 1, 0),
(57, 'category description', 2, 'adm/admGroup0101.aspx?id=<!ID!>&frm=2', 13, 1, 0, 0, 1, 0, 1, 0),
(58, 'deals', 1, '', 0, 9, 0, 0, 1, 1, 0, 0),
(59, 'deals manager', 1, 'adm/admDeals01.aspx', 58, 1, 0, 0, 1, 1, 0, 0),
(60, 'deals details', 1, 'adm/admDeals0101.aspx', 59, 1, 0, 0, 1, 0, 1, 0),
(61, 'merchant address details', 2, 'adm/admDir0201.aspx?id=<!ID!>&frm=2', 33, 2, 0, 0, 1, 0, 1, 0),
(62, 'merchant content', 3, 'adm/admDir0201.aspx?id=<!ID!>&frm=3', 33, 3, 0, 0, 1, 0, 1, 0),
(63, 'downloaded deals', 2, 'adm/admDeals0101.aspx?id=<!ID!>&frm=2', 59, 2, 0, 0, 1, 0, 1, 0),
(64, 'deals price', 3, 'adm/admDeals0101.aspx?id=<!ID!>&frm=3', 59, 3, 0, 0, 1, 0, 1, 0),
(65, 'related deals', 4, 'adm/admDeals0101.aspx?id=<!ID!>&frm=4', 59, 4, 0, 0, 1, 0, 1, 0),
(66, 'mobile view', 3, 'adm/admPage0101.aspx?id=<!ID!>&frm=3', 5, 3, 0, 0, 1, 0, 1, 0),
(67, 'coupon manager', 3, 'adm/admCoupon01.aspx', 12, 4, 0, 0, 1, 1, 0, 0),
(68, 'coupon details', 1, 'adm/admCoupon0101.aspx?id=<!ID!>', 67, 1, 0, 0, 1, 0, 1, 0),
(69, 'product specification', 100, 'adm/admProduct0101.aspx?id=<!ID!>&frm=100', 15, 100, 0, 0, 1, 0, 1, 0),
(70, 'enquiry', 10, 'adm/admEnquiry01.aspx', 0, 10, 0, 0, 1, 1, 0, 0),
(82, 'gst settings', 1, 'adm/admGST01.aspx', 2, 1, 0, 0, 1, 1, 0, 0),
(83, 'email setup', 7, 'adm/admEmail01.aspx', 1, 6, 0, 0, 1, 1, 0, 0),
(84, 'image setting', 8, 'adm/admLogo01.aspx', 1, 10, 0, 0, 1, 1, 0, 0),
(85, 'add new page', 4, 'adm/admPage0101.aspx', 5, 4, 0, 0, 1, 0, 1, 0),
(86, 'replicate page', 5, 'adm/admPage0101.aspx?rid=<!ID!>', 5, 5, 0, 0, 1, 0, 1, 0),
(87, 'payment gateway', 9, 'adm/admPayment01.aspx', 1, 11, 0, 0, 0, 0, 0, 0),
(88, 'order slip setup', 9, 'adm/admOrderSlip01.aspx', 1, 12, 0, 0, 0, 0, 0, 0),
(89, 'social media', 11, 'adm/admSocial01.aspx', 1, 8, 0, 0, 1, 1, 0, 0),
(90, 'area setting', 3, 'adm/admArea01.aspx', 31, 3, 0, 0, 1, 1, 0, 0),
(91, 'upload photos', 4, 'adm/admDir0201.aspx?id=<!ID!>&frm=4', 33, 4, 0, 0, 1, 0, 1, 0),
(92, 'event', 5, 'adm/admDir0201.aspx?id=<!ID!>&frm=5', 33, 5, 0, 0, 1, 0, 1, 0),
(93, 'event details', 1, 'adm/admDir0202.aspx?id=<!ID!>&eid=<!EID!>&frm=1', 92, 1, 0, 0, 1, 0, 1, 0),
(94, 'event gallery', 2, 'adm/admDir0202.aspx?id=<!ID!>&eid=<!EID!>&frm=2', 92, 2, 0, 0, 1, 0, 1, 0),
(95, 'inventory', 1, '', 0, 13, 0, 0, 1, 1, 0, 0),
(96, 'upload inventory', 1, 'adm/admUploadInventory01.aspx', 95, 1, 0, 0, 1, 1, 0, 0),
(97, 'product inventory', 8, 'adm/admProduct0101.aspx?id=<!ID!>&frm=8', 15, 8, 0, 0, 1, 0, 1, 0),
(98, 'group menu', 4, 'adm/admGrouping01.aspx', 12, 1, 0, 0, 1, 1, 0, 0),
(99, 'training', 13, '', 0, 14, 0, 0, 1, 1, 0, 0),
(100, 'date manager', 1, 'adm/admDate01.aspx', 99, 1, 0, 0, 1, 1, 0, 0),
(101, 'training manager', 2, 'adm/admTraining01.aspx', 99, 2, 0, 0, 1, 1, 0, 0),
(102, 'training details', 1, 'adm/admTraining0101.aspx?id=<!ID!>', 101, 1, 0, 0, 1, 0, 1, 0),
(103, 'product', 3, '', 0, 3, 0, 1, 1, 1, 0, 0),
(104, 'product manager', 2, 'adm/mobile/admProductmb01.aspx', 103, 2, 0, 1, 1, 1, 0, 0),
(105, 'product detail', 1, 'adm/mobile/admProductmb0101.aspx?id=<!ID!>&frm=1', 104, 1, 0, 1, 1, 0, 1, 0),
(106, 'product content', 2, 'adm/mobile/admProductmb0101.aspx?id=<!ID!>&frm=2', 104, 2, 0, 1, 1, 0, 1, 0),
(107, 'product pricing', 3, 'adm/mobile/admProductmb0101.aspx?id=<!ID!>&frm=3', 104, 3, 0, 1, 1, 0, 1, 0),
(108, 'related product', 4, 'adm/mobile/admProductmb0101.aspx?id=<!ID!>&frm=4', 104, 4, 0, 1, 1, 0, 1, 0),
(109, 'website', 2, '', 0, 2, 0, 1, 1, 1, 0, 0),
(110, 'control panel', 1, 'adm/mobile/admCMSControlPanelmb01.aspx', 109, 5, 0, 1, 1, 1, 0, 0),
(111, 'object manager', 2, 'adm/mobile/admCMSmb01.aspx', 109, 2, 0, 1, 1, 1, 0, 0),
(112, 'page content manager', 3, 'adm/mobile/admPagemb01.aspx', 109, 1, 0, 1, 1, 1, 0, 0),
(113, 'slide show group', 4, 'adm/mobile/admSlideGroupmb01.aspx', 109, 3, 0, 1, 1, 1, 0, 0),
(114, 'slide show manager', 5, 'adm/mobile/admSlidemb01.aspx', 109, 4, 0, 1, 1, 1, 0, 0),
(115, 'event manager', 6, 'adm/mobile/admHighlightmb01.aspx', 109, 9, 0, 1, 1, 1, 0, 0),
(116, 'email setup', 7, 'adm/mobile/admEmailmb01.aspx', 109, 6, 0, 1, 1, 1, 0, 0),
(117, 'image setting', 8, 'adm/mobile/admLogomb01.aspx', 109, 9, 0, 1, 1, 1, 0, 0),
(118, 'social media', 11, 'adm/mobile/admSocialmb01.aspx', 109, 7, 0, 1, 1, 1, 0, 0),
(119, 'user', 4, '', 0, 4, 0, 1, 1, 1, 0, 0),
(120, 'member manager', 1, 'adm/mobile/admMembermb01.aspx', 119, 1, 0, 1, 1, 1, 0, 0),
(121, 'member details', 1, 'adm/mobile/admMembermb0101.aspx?id=<!ID!>', 120, 1, 0, 1, 1, 0, 1, 0),
(122, 'order', 5, '', 0, 5, 0, 1, 1, 1, 0, 0),
(123, 'sales manager', 1, 'adm/mobile/admOrdermb01.aspx', 122, 1, 0, 1, 1, 1, 0, 0),
(124, 'shipping manager', 3, 'adm/mobile/admShippingmb01.aspx', 122, 3, 0, 1, 1, 1, 0, 0),
(125, 'shipping details', 1, 'adm/mobile/admShippingmb0101.aspx?id=<!ID!>', 124, 1, 0, 1, 1, 0, 1, 0),
(126, 'shipping waiver details', 2, 'adm/mobile/admShippingmb0102.aspx?id=<!ID!>', 124, 2, 0, 1, 1, 0, 1, 0),
(127, 'enquiry', 6, 'adm/mobile/admEnquirymb01.aspx', 0, 6, 0, 1, 1, 1, 0, 0),
(128, 'category manager', 1, 'adm/mobile/admGroupmb01.aspx', 103, 1, 0, 1, 1, 1, 0, 0),
(129, 'category details', 1, 'adm/mobile/admGroupmb0101.aspx?id=<!ID!>', 128, 1, 0, 1, 1, 0, 1, 0),
(130, 'dashboard', 1, 'adm/mobile/adm00mb.aspx', 0, 1, 0, 1, 1, 1, 0, 0),
(131, 'log out', 7, 'adm/mobile/admloginmb.aspx', 0, 7, 0, 1, 1, 1, 0, 0),
(132, 'google settings', 12, 'adm/admGoogleSettings01.aspx', 1, 13, 0, 0, 1, 1, 0, 0),
(133, 'page authorization', 13, 'adm/admPageAuthorization01.aspx', 1, 14, 0, 0, 1, 1, 0, 0),
(134, 'sms setup', 14, 'adm/admSms01.aspx', 1, 7, 0, 0, 1, 1, 0, 0),
(135, 'addon product', 9, 'adm/admProduct0101.aspx?id=<!ID!>&frm=9', 15, 9, 0, 0, 1, 0, 1, 0),
(1000, 'custom', 1, '', 0, 12, 0, 0, 1, 1, 0, 0),
(1001, 'Date Time', 1, 'adm/admWorking01.aspx', 1006, 1, 0, 0, 1, 1, 0, 1),
(1002, 'Service Agent', 2, 'adm/admAgent01.aspx', 0, 2, 0, 0, 1, 1, 0, 1),
(1003, 'Agent Detail', 1, 'adm/admAgent0101.aspx?id=<!ID!>', 1002, 1, 0, 0, 1, 0, 1, 0),
(1004, 'Dashboard', 1, 'adm/adm00.aspx', 0, 0, 0, 0, 1, 1, 0, 1),
(1005, 'Job', 1, 'adm/admJob01.aspx', 0, 4, 0, 0, 1, 1, 0, 1),
(1006, 'Control Panel', 1, '', 0, 1, 0, 0, 1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_adminpagetype`
--

CREATE TABLE IF NOT EXISTS `tb_adminpagetype` (
  `APTYPE_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `ADMPAGE_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `TYPE_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`APTYPE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=501 ;

--
-- Dumping data for table `tb_adminpagetype`
--

INSERT INTO `tb_adminpagetype` (`APTYPE_ID`, `ADMPAGE_ID`, `TYPE_ID`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1),
(12, 1, 2),
(13, 2, 2),
(14, 3, 2),
(15, 4, 2),
(16, 5, 2),
(17, 6, 2),
(18, 7, 2),
(19, 8, 2),
(20, 9, 2),
(21, 10, 2),
(22, 11, 2),
(23, 28, 2),
(24, 29, 2),
(25, 30, 2),
(26, 1, 3),
(27, 2, 3),
(28, 3, 3),
(29, 4, 3),
(30, 5, 3),
(31, 6, 3),
(32, 7, 3),
(33, 8, 3),
(34, 9, 3),
(35, 10, 3),
(36, 11, 3),
(37, 12, 3),
(38, 13, 3),
(39, 14, 3),
(40, 15, 3),
(41, 16, 3),
(42, 17, 3),
(43, 18, 3),
(44, 19, 3),
(45, 20, 3),
(46, 21, 3),
(47, 22, 3),
(48, 23, 3),
(49, 24, 3),
(50, 25, 3),
(51, 26, 3),
(52, 27, 3),
(53, 1, 4),
(54, 2, 4),
(55, 3, 4),
(56, 4, 4),
(57, 5, 4),
(58, 6, 4),
(59, 7, 4),
(60, 8, 4),
(61, 9, 4),
(62, 10, 4),
(63, 11, 4),
(64, 12, 4),
(65, 13, 4),
(66, 14, 4),
(67, 15, 4),
(68, 16, 4),
(69, 17, 4),
(70, 18, 4),
(71, 19, 4),
(72, 20, 4),
(73, 21, 4),
(74, 22, 4),
(75, 23, 4),
(76, 24, 4),
(77, 25, 4),
(78, 26, 4),
(79, 27, 4),
(80, 1, 5),
(81, 2, 5),
(82, 3, 5),
(83, 4, 5),
(84, 5, 5),
(85, 6, 5),
(86, 7, 5),
(87, 8, 5),
(88, 9, 5),
(89, 10, 5),
(90, 11, 5),
(91, 12, 5),
(92, 13, 5),
(93, 14, 5),
(94, 15, 5),
(95, 16, 5),
(96, 17, 5),
(97, 18, 5),
(98, 19, 5),
(99, 1, 6),
(100, 2, 6),
(101, 3, 6),
(102, 4, 6),
(103, 5, 6),
(104, 6, 6),
(105, 7, 6),
(106, 8, 6),
(107, 9, 6),
(108, 10, 6),
(109, 11, 6),
(110, 12, 6),
(111, 13, 6),
(112, 14, 6),
(113, 15, 6),
(114, 16, 6),
(115, 17, 6),
(116, 18, 6),
(117, 19, 6),
(118, 1, 8),
(119, 2, 8),
(120, 3, 8),
(121, 4, 8),
(122, 5, 8),
(123, 6, 8),
(124, 7, 8),
(125, 8, 8),
(126, 9, 8),
(127, 10, 8),
(128, 11, 8),
(129, 12, 8),
(130, 13, 8),
(131, 14, 8),
(132, 15, 8),
(133, 16, 8),
(134, 17, 8),
(135, 18, 8),
(136, 19, 8),
(137, 28, 8),
(138, 29, 8),
(139, 30, 8),
(140, 1, 9),
(141, 2, 9),
(142, 3, 9),
(143, 4, 9),
(144, 5, 9),
(145, 6, 9),
(146, 7, 9),
(147, 8, 9),
(148, 9, 9),
(149, 10, 9),
(150, 11, 9),
(151, 28, 9),
(152, 29, 9),
(153, 30, 9),
(154, 31, 9),
(155, 32, 9),
(156, 33, 9),
(157, 34, 9),
(158, 35, 9),
(159, 1, 10),
(160, 2, 10),
(161, 3, 10),
(162, 4, 10),
(163, 5, 10),
(164, 6, 10),
(165, 7, 10),
(166, 8, 10),
(167, 9, 10),
(168, 10, 10),
(169, 11, 10),
(170, 12, 10),
(171, 13, 10),
(172, 14, 10),
(173, 15, 10),
(174, 16, 10),
(175, 17, 10),
(176, 18, 10),
(177, 19, 10),
(178, 20, 10),
(179, 21, 10),
(180, 22, 10),
(181, 23, 10),
(182, 24, 10),
(183, 25, 10),
(184, 26, 10),
(185, 27, 10),
(186, 1, 11),
(187, 2, 11),
(188, 3, 11),
(189, 4, 11),
(190, 5, 11),
(191, 6, 11),
(192, 7, 11),
(193, 8, 11),
(194, 9, 11),
(195, 10, 11),
(196, 11, 11),
(197, 12, 11),
(198, 13, 11),
(199, 14, 11),
(200, 15, 11),
(201, 16, 11),
(202, 17, 11),
(203, 18, 11),
(204, 19, 11),
(205, 21, 11),
(206, 22, 11),
(207, 23, 11),
(208, 24, 11),
(209, 26, 11),
(210, 27, 11),
(211, 25, 11),
(234, 28, 1),
(235, 28, 3),
(236, 28, 4),
(237, 28, 5),
(238, 28, 6),
(239, 28, 10),
(240, 28, 11),
(241, 45, 1),
(242, 45, 2),
(243, 45, 3),
(244, 45, 4),
(245, 45, 5),
(246, 45, 6),
(247, 45, 8),
(248, 45, 9),
(249, 45, 10),
(250, 45, 11),
(251, 29, 1),
(252, 29, 3),
(253, 29, 4),
(254, 29, 5),
(255, 29, 6),
(256, 29, 10),
(257, 29, 11),
(258, 30, 1),
(259, 30, 3),
(260, 30, 4),
(261, 30, 5),
(262, 30, 6),
(263, 30, 10),
(264, 30, 11),
(265, 52, 1),
(266, 52, 3),
(267, 52, 4),
(268, 52, 5),
(269, 52, 6),
(270, 52, 10),
(271, 52, 11),
(272, 53, 1),
(273, 53, 3),
(274, 53, 4),
(275, 53, 5),
(276, 53, 6),
(277, 53, 10),
(278, 53, 11),
(279, 54, 1),
(280, 54, 3),
(281, 54, 4),
(282, 54, 5),
(283, 54, 6),
(284, 54, 10),
(285, 54, 11),
(286, 55, 1),
(287, 55, 3),
(288, 55, 4),
(289, 55, 5),
(290, 55, 6),
(291, 55, 10),
(292, 55, 11),
(293, 69, 3),
(294, 69, 4),
(295, 69, 5),
(296, 69, 6),
(297, 69, 8),
(298, 69, 10),
(299, 69, 11),
(300, 82, 3),
(301, 82, 4),
(302, 82, 10),
(303, 82, 11),
(304, 82, 12),
(305, 109, 1),
(306, 110, 1),
(307, 111, 1),
(308, 112, 1),
(309, 113, 1),
(310, 114, 1),
(311, 109, 2),
(312, 110, 2),
(313, 111, 2),
(314, 112, 2),
(315, 113, 2),
(316, 114, 2),
(317, 115, 2),
(318, 109, 3),
(319, 110, 3),
(320, 111, 3),
(321, 112, 3),
(322, 113, 3),
(323, 114, 3),
(324, 103, 3),
(325, 104, 3),
(326, 105, 3),
(327, 106, 3),
(328, 107, 3),
(329, 108, 3),
(330, 128, 3),
(331, 129, 3),
(332, 119, 3),
(333, 120, 3),
(334, 121, 3),
(335, 122, 3),
(336, 123, 3),
(337, 124, 3),
(338, 125, 3),
(339, 126, 3),
(340, 109, 4),
(341, 110, 4),
(342, 111, 4),
(343, 112, 4),
(344, 113, 4),
(345, 114, 4),
(346, 103, 4),
(347, 104, 4),
(348, 105, 4),
(349, 106, 4),
(350, 107, 4),
(351, 108, 4),
(352, 128, 4),
(353, 129, 4),
(354, 119, 4),
(355, 120, 4),
(356, 121, 4),
(357, 122, 4),
(358, 123, 4),
(359, 124, 4),
(360, 125, 4),
(361, 126, 4),
(362, 109, 5),
(363, 110, 5),
(364, 111, 5),
(365, 112, 5),
(366, 113, 5),
(367, 114, 5),
(368, 103, 5),
(369, 104, 5),
(370, 105, 5),
(371, 106, 5),
(372, 107, 5),
(373, 108, 5),
(374, 128, 5),
(375, 129, 5),
(376, 109, 6),
(377, 110, 6),
(378, 111, 6),
(379, 112, 6),
(380, 113, 6),
(381, 114, 6),
(382, 103, 6),
(383, 104, 6),
(384, 105, 6),
(385, 106, 6),
(386, 107, 6),
(387, 108, 6),
(388, 128, 6),
(389, 129, 6),
(390, 109, 8),
(391, 110, 8),
(392, 111, 8),
(393, 112, 8),
(394, 113, 8),
(395, 114, 8),
(396, 103, 8),
(397, 104, 8),
(398, 105, 8),
(399, 106, 8),
(400, 107, 8),
(401, 108, 8),
(402, 128, 8),
(403, 129, 8),
(404, 115, 8),
(405, 109, 9),
(406, 110, 9),
(407, 111, 9),
(408, 112, 9),
(409, 113, 9),
(410, 114, 9),
(411, 115, 9),
(412, 109, 10),
(413, 110, 10),
(414, 111, 10),
(415, 112, 10),
(416, 113, 10),
(417, 114, 10),
(418, 103, 10),
(419, 104, 10),
(420, 105, 10),
(421, 106, 10),
(422, 107, 10),
(423, 108, 10),
(424, 128, 10),
(425, 129, 10),
(426, 119, 10),
(427, 120, 10),
(428, 121, 10),
(429, 122, 10),
(430, 123, 10),
(431, 124, 10),
(432, 125, 10),
(433, 126, 10),
(434, 109, 11),
(435, 110, 11),
(436, 111, 11),
(437, 112, 11),
(438, 113, 11),
(439, 114, 11),
(440, 103, 11),
(441, 104, 11),
(442, 105, 11),
(443, 106, 11),
(444, 107, 11),
(445, 108, 11),
(446, 128, 11),
(447, 129, 11),
(448, 119, 11),
(449, 120, 11),
(450, 121, 11),
(451, 122, 11),
(452, 123, 11),
(453, 124, 11),
(454, 125, 11),
(455, 126, 11),
(456, 115, 1),
(457, 115, 3),
(458, 115, 4),
(459, 115, 5),
(460, 115, 6),
(461, 115, 10),
(462, 115, 11),
(463, 109, 12),
(464, 110, 12),
(465, 111, 12),
(466, 112, 12),
(467, 113, 12),
(468, 114, 12),
(469, 103, 12),
(470, 104, 12),
(471, 105, 12),
(472, 106, 12),
(473, 107, 12),
(474, 108, 12),
(475, 128, 12),
(476, 129, 12),
(477, 119, 12),
(478, 120, 12),
(479, 121, 12),
(480, 122, 12),
(481, 123, 12),
(482, 124, 12),
(483, 125, 12),
(484, 126, 12),
(485, 115, 12),
(486, 109, 13),
(487, 110, 13),
(488, 111, 13),
(489, 112, 13),
(490, 113, 13),
(491, 114, 13),
(492, 103, 13),
(493, 104, 13),
(494, 105, 13),
(495, 106, 13),
(496, 107, 13),
(497, 108, 13),
(498, 128, 13),
(499, 129, 13),
(500, 115, 13);

-- --------------------------------------------------------

--
-- Table structure for table `tb_area`
--

CREATE TABLE IF NOT EXISTS `tb_area` (
  `AREA_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `AREA_NAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `AREA_ORDER` bigint(9) unsigned NOT NULL DEFAULT '0',
  `AREA_CREATION` datetime NOT NULL,
  `AREA_CREATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `AREA_LASTUPDATE` datetime DEFAULT NULL,
  `AREA_UPDATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`AREA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_banktransfer`
--

CREATE TABLE IF NOT EXISTS `tb_banktransfer` (
  `BT_ID` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `BT_NAME` varchar(250) DEFAULT NULL,
  `BT_BANK` varchar(250) NOT NULL DEFAULT '0',
  `BT_BANKACC_NAME` varchar(250) DEFAULT NULL,
  `BT_BANKACC_NUM` varchar(250) DEFAULT NULL,
  `BT_ORDER` varchar(250) NOT NULL DEFAULT '99',
  `BT_ACTIVE` int(1) NOT NULL DEFAULT '1',
  `BT_CREATION` datetime NOT NULL,
  `BT_CREATEDBY` int(9) unsigned NOT NULL DEFAULT '0',
  `BT_LASTUPDATE` datetime DEFAULT NULL,
  `BT_UPDATEDBY` int(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`BT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_cms`
--

CREATE TABLE IF NOT EXISTS `tb_cms` (
  `CMS_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `CMS_TITLE` varchar(250) CHARACTER SET utf8 NOT NULL,
  `CMS_CONTENT` text CHARACTER SET utf8,
  `CMS_TYPE` bigint(9) NOT NULL DEFAULT '0',
  `CMS_CREATION` datetime NOT NULL,
  `CMS_CREATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `CMS_LASTUPDATE` datetime DEFAULT NULL,
  `CMS_UPDATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `CMS_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `CMS_ALLOWDELETE` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`CMS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_cmsbackup`
--

CREATE TABLE IF NOT EXISTS `tb_cmsbackup` (
  `CMSBACKUP_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CMS_ID` int(10) unsigned NOT NULL,
  `CMSBACKUP_CONTENT` longtext NOT NULL,
  `CMSBACKUP_TYPE` int(1) unsigned NOT NULL,
  `CMSBACKUP_DATE` datetime NOT NULL,
  `CMSBACKUP_BY` int(10) unsigned NOT NULL,
  PRIMARY KEY (`CMSBACKUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_config`
--

CREATE TABLE IF NOT EXISTS `tb_config` (
  `CONF_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `CONF_NAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `CONF_VALUE` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `CONF_LONGVALUE` text CHARACTER SET utf8,
  `CONF_GROUP` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `CONF_LANG` varchar(250) DEFAULT NULL,
  `CONF_ORDER` int(9) NOT NULL DEFAULT '0',
  `CONF_OPERATOR` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `CONF_UNIT` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `CONF_TAG` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `CONF_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `CONF_CREATION` datetime NOT NULL,
  `CONF_CREATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `CONF_LASTUPDATE` datetime DEFAULT NULL,
  `CONF_UPDATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`CONF_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=379 ;

--
-- Dumping data for table `tb_config`
--

INSERT INTO `tb_config` (`CONF_ID`, `CONF_NAME`, `CONF_VALUE`, `CONF_LONGVALUE`, `CONF_GROUP`, `CONF_LANG`, `CONF_ORDER`, `CONF_OPERATOR`, `CONF_UNIT`, `CONF_TAG`, `CONF_ACTIVE`, `CONF_CREATION`, `CONF_CREATEDBY`, `CONF_LASTUPDATE`, `CONF_UPDATEDBY`) VALUES
(1, 'Max Total Page', '6', NULL, 'PAGE DETAILS', NULL, 1, NULL, NULL, NULL, 1, '0000-00-00 00:00:00', 0, NULL, 0),
(2, 'Email', 'dev@webteq.com.my', NULL, '', NULL, 1, NULL, NULL, NULL, 1, '0000-00-00 00:00:00', 0, NULL, 0),
(3, 'Page Title Prefix', 'Acond', NULL, 'PAGE DETAILS', NULL, 1, NULL, NULL, NULL, 1, '2011-08-08 15:15:48', 1, '2017-04-28 14:54:39', 290),
(4, 'Default Page Title', 'Acond', NULL, 'PAGE DETAILS', NULL, 2, NULL, NULL, NULL, 1, '2011-08-08 15:15:48', 1, '2017-04-28 14:54:39', 290),
(5, 'Default Keyword', 'Acond', NULL, 'PAGE DETAILS', NULL, 3, NULL, NULL, NULL, 1, '2011-08-08 15:15:48', 1, '2017-04-28 14:54:40', 290),
(6, 'Default Description', 'Acond', NULL, 'PAGE DETAILS', NULL, 4, NULL, NULL, NULL, 1, '2011-08-08 15:15:48', 1, '2017-04-28 14:54:40', 290),
(7, 'Top Menu Separator', '||', NULL, 'SEPARATOR', NULL, 1, NULL, NULL, NULL, 1, '2011-08-25 18:07:00', 0, NULL, 0),
(8, 'CMS REDIRECT TAG', '[!REDIRECT!]', NULL, 'CMSTAG', NULL, 1, NULL, NULL, NULL, 1, '2011-09-07 18:07:00', 0, NULL, 0),
(9, 'CMS GMAP TAG', '[GMAP]', NULL, 'CMSTAG', NULL, 1, NULL, NULL, NULL, 1, '2011-09-07 18:07:00', 0, NULL, 0),
(10, 'GMAP KEY', '', NULL, 'GMAP', NULL, 1, NULL, NULL, NULL, 1, '2011-09-07 18:07:00', 0, NULL, 0),
(11, 'Admin Landing', 'adm00.aspx', NULL, '', NULL, 1, NULL, NULL, NULL, 1, '2012-02-29 18:07:00', 0, NULL, 0),
(12, 'Admin Profile', 'admProfile.aspx', NULL, '', NULL, 1, NULL, NULL, NULL, 1, '2012-02-29 18:07:00', 0, NULL, 0),
(13, 'Group Image', 'data/cms/images/default-img.jpg', NULL, '', NULL, 1, NULL, NULL, NULL, 1, '2012-03-03 18:07:00', 0, '2017-04-28 14:54:40', 290),
(14, 'Admin Email Content', 'Enquiry/ Request Submission', '<div style="font-size:12px; font-family: arial,helvetica,sans-serif;">\r\n<div>Dear Admin,</div>\r\n\r\n<div><br />\r\nHere is the new enquiry/request from Gemilang Coachwork website.The submitted information as below:<br />\r\n&nbsp;</div>\r\n\r\n<table border="0" cellpadding="0" cellspacing="0" style="font-size:12px; font-family: arial,helvetica,sans-serif;">\r\n	<tbody>\r\n		<tr>\r\n			<td>Salutation</td>\r\n			<td style="padding-left: 10px; padding-right: 10px;">:</td>\r\n			<td>[SALUTATION]</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Name</td>\r\n			<td style="padding-left: 10px; padding-right: 10px;">:</td>\r\n			<td>[NAME]</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Company</td>\r\n			<td style="padding-left: 10px; padding-right: 10px;">:</td>\r\n			<td>[COMPANY]</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Contact No.</td>\r\n			<td style="padding-left: 10px; padding-right: 10px;">:</td>\r\n			<td>[CONTACT]</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Email</td>\r\n			<td style="padding-left: 10px; padding-right: 10px;">:</td>\r\n			<td>[EMAIL]</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Subject</td>\r\n			<td style="padding-left: 10px; padding-right: 10px;">:</td>\r\n			<td>[SUBJECT]</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Message</td>\r\n			<td style="padding-left: 10px; padding-right: 10px;">:</td>\r\n			<td>[MESSAGE]</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<div><br />\r\nThank you<br />\r\n<br />\r\n&nbsp;</div>\r\n[SIGNATURE]</div>', 'EMAIL CONTENT', NULL, 1, NULL, NULL, NULL, 1, '2012-06-12 11:55:25', 12, '2017-04-28 14:52:16', 290),
(15, 'User Email Content', NULL, '<div style="font-size:12px; font-family:arial,helvetica,sans-serif;">\r\n<p>Dear [SALUTATION] [NAME],<br />\r\n<br />\r\nYour enquiry/request has been submitted successfully.<br />\r\n<br />\r\nWe will process your enquiry/request and contact you shortly.<br />\r\n<br />\r\nThank You.<br />\r\n&nbsp;</p>\r\n\r\n<div>[SIGNATURE]</div>\r\n</div>', 'EMAIL CONTENT', NULL, 2, NULL, NULL, NULL, 1, '2012-06-12 11:55:25', 12, '2017-04-28 14:52:16', 290),
(16, 'Currency', 'RM', NULL, 'DEFAULT SETTINGS', NULL, 1, NULL, NULL, '<!currency!>', 1, '2012-01-06 00:00:00', 0, NULL, 0),
(17, 'Shipping Unit', 'kg', NULL, 'SHIPPING', NULL, 1, NULL, NULL, '<!shippingunit!>', 1, '2012-01-06 00:00:00', 0, '2017-04-28 14:54:41', 290),
(18, 'Dimension Unit', 'cm', NULL, 'PRODUCT', NULL, 1, NULL, NULL, NULL, 1, '2012-01-06 00:00:00', 0, NULL, 0),
(19, 'Weight Unit', 'gram', NULL, 'PRODUCT', NULL, 1, NULL, NULL, NULL, 1, '2012-01-06 00:00:00', 0, NULL, 0),
(20, 'Shipping Type', '', NULL, 'SHIPPING', NULL, 2, NULL, NULL, NULL, 1, '2012-06-29 16:48:04', 12, '2017-04-28 14:54:41', 290),
(21, 'Smtp Server', 'smtp.gmail.com', NULL, 'EMAIL SETTING', NULL, 1, NULL, NULL, NULL, 1, '2012-07-05 11:50:32', 12, '2017-04-28 14:52:15', 290),
(22, 'Default Sender Name', 'Acond', NULL, 'EMAIL SETTING', NULL, 2, NULL, NULL, NULL, 1, '2012-07-05 11:50:32', 12, '2017-04-28 14:52:15', 290),
(23, 'Default Sender', 'stg@webteq.asia', NULL, 'EMAIL SETTING', NULL, 3, NULL, NULL, NULL, 1, '2012-07-05 11:50:32', 12, '2017-04-28 14:52:15', 290),
(24, 'Default Sender Password', 's123456g', NULL, 'EMAIL SETTING', NULL, 4, NULL, NULL, NULL, 1, '2012-07-05 11:50:32', 12, '2017-04-28 14:52:16', 290),
(25, 'Default Recipient', 'info@acond.com.my', NULL, 'EMAIL SETTING', NULL, 5, NULL, NULL, NULL, 1, '2012-07-05 11:50:32', 12, '2017-04-28 14:52:16', 290),
(26, 'Use Gmail Smtp', 'True', NULL, 'EMAIL SETTING', NULL, 6, NULL, NULL, NULL, 1, '2012-07-05 11:50:32', 12, '2017-04-28 14:52:16', 290),
(27, 'Port', '587', NULL, 'EMAIL SETTING', NULL, 7, NULL, NULL, NULL, 1, '2012-07-05 11:50:32', 12, '2017-04-28 14:52:16', 290),
(28, 'Enable SSL', 'True', NULL, 'EMAIL SETTING', NULL, 8, NULL, NULL, NULL, 1, '2012-07-05 11:50:32', 12, '2017-04-28 14:52:16', 290),
(29, 'Email Subject Prefix', '[Acond Website]', NULL, 'EMAIL CONTENT', NULL, 3, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-04-28 14:52:16', 290),
(30, 'Admin Email Subject', 'Enquiry/ Request Submission', NULL, 'EMAIL CONTENT', NULL, 4, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-04-28 14:52:16', 290),
(31, 'User Email Subject', 'Enquiry/ Request Submission Successful', NULL, 'EMAIL CONTENT', NULL, 5, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-04-28 14:52:16', 290),
(32, 'Quick Contact Tel', '+65 0000 0000', NULL, 'QUICK CONTACT', NULL, 1, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-04-28 14:54:40', 290),
(33, 'Quick Contact Email', 'info@acond.com', NULL, 'QUICK CONTACT', NULL, 2, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-04-28 14:54:40', 290),
(34, 'Facebook Page URL', 'https://www.facebook.com/WebteqSolution/', NULL, 'FACEBOOK', NULL, 1, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-05-02 10:31:04', 10),
(35, 'Facebook Like Title', 'Acond', NULL, 'FACEBOOK', NULL, 2, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-05-02 10:31:04', 10),
(36, 'Facebook Like Type', 'website', NULL, 'FACEBOOK', NULL, 3, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-05-02 10:31:04', 10),
(37, 'Facebook Like URL', 'http://acond.webteq.asia/usr/page.aspx?pgid=2', NULL, 'FACEBOOK', NULL, 4, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-05-02 10:31:04', 10),
(38, 'Facebook Like Image', 'img/usr/cmn/logo-fb.gif', NULL, 'FACEBOOK', NULL, 5, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-05-02 10:31:04', 10),
(39, 'Facebook Like Website Name', 'Acond', NULL, 'FACEBOOK', NULL, 6, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-05-02 10:31:04', 10),
(40, 'Facebook Like Description', 'Like us on Facebook', NULL, 'FACEBOOK', NULL, 7, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-05-02 10:31:04', 10),
(41, 'Editor Style', '', NULL, 'WEBSITE SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-04-28 14:54:40', 290),
(42, 'Copyright', '© Copyright 2016 All rights reserved. Acond', NULL, 'WEBSITE SETTINGS', NULL, 2, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-04-28 14:54:40', 290),
(43, 'Google Analytic', NULL, '', 'GOOGLE SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2016-08-16 12:20:40', 146),
(44, '', '0', NULL, 'SHIPPING', NULL, 3, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-04-28 14:54:41', 290),
(45, 'Invoice Logo', '', NULL, 'INVOICE SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-04-28 14:54:41', 290),
(46, 'Invoice Remarks', NULL, '', 'INVOICE SETTINGS', NULL, 2, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, NULL, 0),
(47, 'Invoice Signature', NULL, '', 'INVOICE SETTINGS', NULL, 3, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, NULL, 0),
(48, 'Send Payment Email', 'True', NULL, 'INVOICE SETTINGS', NULL, 4, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-04-28 14:54:41', 290),
(49, 'Send Processing Email', 'True', NULL, 'INVOICE SETTINGS', NULL, 5, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-04-28 14:54:41', 290),
(50, 'Send Delivery Email', 'True', NULL, 'INVOICE SETTINGS', NULL, 6, NULL, NULL, NULL, 1, '2013-04-22 15:30:56', 70, '2017-04-28 14:54:41', 290),
(51, 'Company Name', '', NULL, 'INVOICE SETTINGS', NULL, 7, NULL, NULL, NULL, 1, '2013-04-23 16:47:54', 70, '2017-04-28 14:54:41', 290),
(52, 'ROC', '', NULL, 'INVOICE SETTINGS', NULL, 8, NULL, NULL, NULL, 1, '2013-04-23 16:47:54', 70, '2017-04-28 14:54:41', 290),
(53, 'Company Address', '', NULL, 'INVOICE SETTINGS', NULL, 9, NULL, NULL, NULL, 1, '2013-04-23 16:47:54', 70, '2017-04-28 14:54:41', 290),
(54, 'Company Telephone', '', NULL, 'INVOICE SETTINGS', NULL, 10, NULL, NULL, NULL, 1, '2013-04-23 16:47:54', 70, '2017-04-28 14:54:41', 290),
(55, 'Company Email', '', NULL, 'INVOICE SETTINGS', NULL, 11, NULL, NULL, NULL, 1, '2013-04-23 16:47:54', 70, '2017-04-28 14:54:41', 290),
(56, 'Company Website', '', NULL, 'INVOICE SETTINGS', NULL, 12, NULL, NULL, NULL, 1, '2013-04-23 16:47:54', 70, '2017-04-28 14:54:41', 290),
(57, 'Dimension Unit', 'cm', NULL, 'PRODUCT SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2013-06-04 15:57:06', 20, '2017-04-28 14:54:40', 290),
(58, 'Invoice Remarks (PDF)', NULL, '', 'INVOICE SETTINGS', NULL, 13, NULL, NULL, NULL, 1, '2013-06-25 11:50:34', 204, NULL, 0),
(59, 'DO Signature', NULL, '', 'INVOICE SETTINGS', NULL, 14, NULL, NULL, NULL, 1, '2013-06-25 11:50:34', 204, NULL, 0),
(60, 'DO Signature (PDF)', NULL, '', 'INVOICE SETTINGS', NULL, 15, NULL, NULL, NULL, 1, '2013-06-25 11:50:34', 204, NULL, 0),
(61, 'Payment Type', '0', NULL, 'INVOICE SETTINGS', NULL, 16, NULL, NULL, NULL, 1, '2013-06-25 11:50:34', 204, '2017-04-28 14:54:41', 290),
(62, 'Shipping Company', '0', NULL, 'INVOICE SETTINGS', NULL, 17, NULL, NULL, NULL, 1, '2013-06-25 11:50:34', 204, '2017-04-28 14:54:41', 290),
(63, 'Listing Size (row)', '100', NULL, 'OTHER SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2013-07-17 16:56:23', 204, '2017-04-28 14:54:41', 290),
(64, 'Session Timeout', '20', NULL, 'OTHER SETTINGS', NULL, 2, NULL, NULL, NULL, 1, '2013-07-17 16:56:23', 204, '2017-04-28 14:54:41', 290),
(65, 'Google+ URL', '', NULL, 'GOOGLEPLUS SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2013-07-29 11:50:24', 204, '2017-05-02 10:31:04', 10),
(66, 'Google+ Like URL', '', NULL, 'GOOGLEPLUS SETTINGS', NULL, 2, NULL, NULL, NULL, 1, '2013-07-29 11:50:24', 204, '2017-05-02 10:31:04', 10),
(67, 'Auto Script Tag', 'True', NULL, 'GOOGLE SETTINGS', NULL, 2, NULL, NULL, NULL, 1, '2013-10-02 22:26:39', 83, '2016-08-16 12:20:26', 146),
(68, 'LinkedIn URL', '', NULL, 'LINKEDIN SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2013-10-02 22:26:39', 83, '2017-05-02 10:31:04', 10),
(69, 'LinkedIn Share URL', '', NULL, 'LINKEDIN SETTINGS', NULL, 2, NULL, NULL, NULL, 1, '2013-10-02 22:26:39', 83, '2017-05-02 10:31:04', 10),
(70, 'FAVICON Image', '1494233413_favicon-acond.png', NULL, 'FAVICON SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2013-10-02 22:26:39', 83, '2017-05-08 16:51:13', 11),
(71, 'Friendly URL', '0', NULL, 'OTHER SETTINGS', NULL, 3, NULL, NULL, NULL, 1, '2014-01-24 17:14:26', 349, '2017-04-28 14:54:42', 290),
(72, 'Product Spec Limit', '3', '', 'WEBSITE SETTING', NULL, 1, '', '', '', 1, '2014-05-12 15:27:17', 1, '2014-05-12 15:27:17', 0),
(73, 'SMS Sender URL', 'http://sms.webteq.com.my/index.php/api/bulk_mt', '', 'SMS SETTINGS', NULL, 1, '', '', '', 1, '2014-09-25 12:15:17', 1, '2014-09-25 12:15:17', 0),
(74, 'SMS Sender UserName', '', '', 'SMS SETTINGS', NULL, 2, '', '', '', 1, '2014-09-25 12:15:17', 1, '2016-09-08 17:18:44', 146),
(75, 'SMS Sender Password', '', '', 'SMS SETTINGS', NULL, 3, '', '', '', 1, '2014-09-25 12:15:17', 1, '2016-09-08 17:18:44', 146),
(76, 'SMS Sender UserId', '', '', 'SMS SETTINGS', NULL, 4, '', '', '', 1, '2014-09-25 12:15:17', 1, '2016-09-08 17:18:44', 146),
(77, 'Thumbnail Width', '', NULL, 'WEBSITE SETTINGS', NULL, 3, NULL, NULL, NULL, 1, '2015-03-09 10:02:27', 427, '2017-04-28 14:54:40', 290),
(78, 'Thumbnail Height', '', NULL, 'WEBSITE SETTINGS', NULL, 4, NULL, NULL, NULL, 1, '2015-03-09 10:02:27', 427, '2017-04-28 14:54:40', 290),
(79, 'Instagram URL', '', NULL, 'INSTAGRAM SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-03-09 10:02:28', 427, '2017-05-02 10:31:04', 10),
(111, 'GST Activation', '1', NULL, 'GST SETTING', NULL, 1, NULL, NULL, NULL, 1, '2015-04-08 16:12:12', 1, NULL, 0),
(112, 'GST Inclusive Icon', '', NULL, 'GST SETTING', NULL, 2, NULL, NULL, NULL, 1, '2015-04-08 16:12:12', 1, NULL, 0),
(113, 'GST %', '0', NULL, 'GST SETTING', NULL, 3, NULL, NULL, NULL, 1, '2015-04-08 16:12:12', 1, NULL, 0),
(114, 'Product with GST', '1', NULL, 'GST SETTING', NULL, 4, NULL, NULL, NULL, 1, '2015-04-08 16:12:12', 1, NULL, 0),
(115, 'Show Itemized', '1', NULL, 'GST SETTING', NULL, 5, NULL, NULL, NULL, 1, '2015-04-08 16:12:12', 1, NULL, 0),
(149, 'Share to Friend Email Subject', '', '', 'SHARE TO FRIEND EMAIL CONTENT', NULL, 1, '', '', '', 1, '2015-03-26 17:09:33', 488, '2017-04-28 14:52:16', 290),
(150, 'Share to Friend User Email Content', '', '', 'SHARE TO FRIEND EMAIL CONTENT', NULL, 2, '', '', '', 1, '2015-03-26 17:09:33', 488, '2015-03-30 14:58:01', 0),
(151, 'Ask Email Subject', '', '', 'ASK EMAIL CONTENT', NULL, 1, '', '', '', 1, '2015-03-26 17:09:33', 488, '2017-04-28 14:52:16', 290),
(152, 'Ask Admin Email Content', '', '', 'ASK EMAIL CONTENT', NULL, 2, '', '', '', 1, '2015-03-26 17:09:33', 488, '2015-03-30 14:58:01', 0),
(153, 'Ask User Email Content', '', '', 'ASK EMAIL CONTENT', NULL, 3, '', '', '', 1, '2015-03-26 17:09:33', 488, '2015-03-30 14:58:01', 0),
(154, 'New Order Email Subject', '', '', 'NEW ORDER EMAIL CONTENT', NULL, 1, '', '', '', 1, '2015-03-26 17:09:33', 488, '2017-04-28 14:52:16', 290),
(155, 'New Order Admin Email Content', '', '', 'NEW ORDER EMAIL CONTENT', NULL, 2, '', '', '', 1, '2015-03-26 17:09:33', 488, '2015-03-30 14:58:41', 0),
(156, 'New Order User Email Content', '', '', 'NEW ORDER EMAIL CONTENT', NULL, 3, '', '', '', 1, '2015-03-26 17:09:33', 488, '2015-03-30 14:58:42', 0),
(157, 'Payment Receive(BT) Email Subject', 'Payment Received', '', 'PAYMENT RECEIVE (BT) EMAIL CONTENT', NULL, 1, '', '', '', 1, '2015-03-26 17:09:33', 488, '2017-04-28 14:52:17', 290),
(158, 'Payment Receive(BT) Admin Email Content', '', '<div style="font-size:12px; font-family: arial,helvetica,sans-serif;">\r\n	<div>\r\n		Dear Admin,<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		Payment from [NAME] has been made.<br />\r\n		<br />\r\n		The order details as below:<br />\r\n		OrderID: [ORDNO]<br />\r\n		Order Total: [CUR] [ORDTOTAL]<br />\r\n		<br />\r\n		Here is the payment details:<br />\r\n		[PAYDETAILS]<br />\r\n		<br />\r\n		Thank you.<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		<div>\r\n			[SIGNATURE]</div>\r\n	</div>\r\n</div>', 'PAYMENT RECEIVE (BT) EMAIL CONTENT', NULL, 2, '', '', '', 1, '2015-03-26 17:09:33', 488, '2015-03-30 14:58:02', 0),
(159, 'Payment Receive(BT) User Email Content', '', '<div style="font-size:12px; font-family: arial,helvetica,sans-serif;">\r\n	<div>\r\n		Dear [NAME],</div>\r\n	<div>\r\n		<br />\r\n		<br />\r\n		<b>Your order [ORDNO] history</b><br />\r\n		<br />\r\n		Thank you for your payment.<br />\r\n		<br />\r\n		Your payment has been verified and we will proceed to pack and deliver your package within the next 2 working days.<br />\r\n		<br />\r\n		Another email will be sent to you once your order is being processed.<br />\r\n		<br />\r\n		Thank you.<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		<div>\r\n			[SIGNATURE]</div>\r\n	</div>\r\n</div>', 'PAYMENT RECEIVE (BT) EMAIL CONTENT', NULL, 3, '', '', '', 1, '2015-03-26 17:09:33', 488, '2015-03-30 14:58:02', 0),
(160, 'Payment Receive(OP) Email Subject', '', '', 'PAYMENT RECEIVE (OP) EMAIL CONTENT', NULL, 1, '', '', '', 1, '2015-03-26 17:09:33', 488, '2017-04-28 14:52:17', 290),
(161, 'Payment Receive(OP) Admin Email Content', '', '', 'PAYMENT RECEIVE (OP) EMAIL CONTENT', NULL, 2, '', '', '', 1, '2015-03-26 17:09:34', 488, '2015-03-30 14:58:02', 0),
(162, 'Payment Receive(OP) User Email Content', '', '', 'PAYMENT RECEIVE (OP) EMAIL CONTENT', NULL, 3, '', '', '', 1, '2015-03-26 17:09:34', 488, '2015-03-30 14:58:02', 0),
(163, 'Payment Error Email Subject', '', '', 'PAYMENT ERROR EMAIL CONTENT', NULL, 1, '', '', '', 1, '2015-03-26 17:09:34', 488, '2017-04-28 14:52:17', 290),
(164, 'Payment Error Admin Email Content', '', '', 'PAYMENT ERROR EMAIL CONTENT', NULL, 2, '', '', '', 1, '2015-03-26 17:09:34', 488, '2015-03-30 14:58:02', 0),
(165, 'Payment Error User Email Content', '', '', 'PAYMENT ERROR EMAIL CONTENT', NULL, 3, '', '', '', 1, '2015-03-26 17:09:34', 488, '2015-03-30 14:58:02', 0),
(166, 'Payment Reject Email Subject', 'Payment Reject', '', 'PAYMENT REJECT EMAIL CONTENT', NULL, 1, '', '', '', 1, '2015-03-26 17:09:34', 488, '2017-04-28 14:52:17', 290),
(167, 'Payment Reject Admin Email Content', '', '<div style="font-size:12px; font-family: arial,helvetica,sans-serif;">\r\n	<div>\r\n		Dear Admin,<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		Please note that there is an error in the transaction details for an order from [NAME] .<br />\r\n		<br />\r\n		The order details as below:<br />\r\n		OrderID: [ORDNO]<br />\r\n		Order Total: [CUR] [ORDTOTAL]<br />\r\n		Reject Remarks: [REJECTMSG]<br />\r\n		<br />\r\n		Thank you.<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		<div>\r\n			[SIGNATURE]</div>\r\n	</div>\r\n</div>', 'PAYMENT REJECT EMAIL CONTENT', NULL, 2, '', '', '', 1, '2015-03-26 17:09:34', 488, '2015-03-30 14:58:02', 0),
(168, 'Payment Reject User Email Content', '', '<div style="font-size:12px; font-family: arial,helvetica,sans-serif;">\r\n	<div>\r\n		Dear [NAME],</div>\r\n	<div>\r\n		<br />\r\n		<br />\r\n		<b>Your order [ORDNO] history</b><br />\r\n		<br />\r\n		Please note that there is an error in the transaction details.<br />\r\n		<br />\r\n		[REJECTMSG]<br />\r\n		<br />\r\n		Please kindly make the amendments and update the form under the order details with the correct information.<br />\r\n		<br />\r\n		Thank you.<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		<div>\r\n			[SIGNATURE]</div>\r\n	</div>\r\n</div>', 'PAYMENT REJECT EMAIL CONTENT', NULL, 3, '', '', '', 1, '2015-03-26 17:09:34', 488, '2015-03-30 14:58:02', 0),
(169, 'Refund Email Subject', '', '', 'REFUND EMAIL CONTENT', NULL, 1, '', '', '', 1, '2015-03-26 17:09:34', 488, '2017-04-28 14:52:17', 290),
(170, 'Refund Admin Email Content', '', '', 'REFUND EMAIL CONTENT', NULL, 2, '', '', '', 1, '2015-03-26 17:09:34', 488, '2015-03-30 14:58:02', 0),
(171, 'Refund User Email Content', '', '', 'REFUND EMAIL CONTENT', NULL, 3, '', '', '', 1, '2015-03-26 17:09:34', 488, '2015-03-30 14:58:02', 0),
(172, 'Order Processing Email Subject', 'Processed', '', 'ORDER PROCESSING EMAIL CONTENT', NULL, 1, '', '', '', 1, '2015-03-26 17:09:34', 488, '2017-04-28 14:52:17', 290),
(173, 'Order Processing Admin Email Content', '', '<div style="font-size:12px; font-family: arial,helvetica,sans-serif;">\r\n	<div>\r\n		Dear Admin,<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		An order from [NAME] is being processed.<br />\r\n		<br />\r\n		The order details as below:<br />\r\n		OrderID: [ORDNO]<br />\r\n		Order Item: [ORDITEM]<br />\r\n		Order Total: [CUR] [ORDTOTAL]<br />\r\n		<br />\r\n		Thank you.<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		<div>\r\n			[SIGNATURE]</div>\r\n	</div>\r\n</div>', 'ORDER PROCESSING EMAIL CONTENT', NULL, 2, '', '', '', 1, '2015-03-26 17:09:34', 488, '2015-03-30 14:58:42', 0),
(174, 'Order Processing User Email Content', '', '<div style="font-size:12px; font-family: arial,helvetica,sans-serif;">\r\n	<div>\r\n		Dear [NAME],</div>\r\n	<div>\r\n		<br />\r\n		<br />\r\n		<b>Your order [ORDNO] history</b><br />\r\n		<br />\r\n		Your order is being processed.<br />\r\n		<br />\r\n		Another email will be sent to you once your package has been delivered.<br />\r\n		<br />\r\n		Thank you.<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		<div>\r\n			[SIGNATURE]</div>\r\n	</div>\r\n</div>', 'ORDER PROCESSING EMAIL CONTENT', NULL, 3, '', '', '', 1, '2015-03-26 17:09:34', 488, '2015-03-30 14:58:42', 0),
(175, 'Order Delivering Email Subject', 'Delivered', '', 'ORDER DELIVERY EMAIL CONTENT', NULL, 1, '', '', '', 1, '2015-03-26 17:09:34', 488, '2017-04-28 14:52:17', 290),
(176, 'Order Delivering Admin Email Content', '', '<div style="font-size:12px; font-family: arial,helvetica,sans-serif;">\r\n	<div>\r\n		Dear Admin,<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		An order from [NAME] has been delivered.<br />\r\n		<br />\r\n		The order details as below:<br />\r\n		OrderID: [ORDNO]<br />\r\n		Order Item: [ORDITEM]<br />\r\n		Order Total: [CUR] [ORDTOTAL]<br />\r\n		Shipping Company: [COURIER]<br />\r\n		Consignment No.: [CONSIGNMENTNO]<br />\r\n		<br />\r\n		Thank you.<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		<div>\r\n			[SIGNATURE]</div>\r\n	</div>\r\n</div>', 'ORDER DELIVERY EMAIL CONTENT', NULL, 2, '', '', '', 1, '2015-03-26 17:09:34', 488, '2015-03-30 14:58:42', 0),
(177, 'Order Delivering User Email Content', '', '<div style="font-size:12px; font-family: arial,helvetica,sans-serif;">\r\n	<div>\r\n		Dear [NAME],</div>\r\n	<div>\r\n		<br />\r\n		<br />\r\n		<b>Your order [ORDNO] history</b><br />\r\n		<br />\r\n		Your order has been processed for delivery.<br />\r\n		<br />\r\n		Here are the shipping details:<br />\r\n		Shipping Company: [COURIER]<br />\r\n		Consignment No.: [CONSIGNMENTNO]<br />\r\n		<br />\r\n		Please allow <b>3 - 4 working days</b> for your package to reach you.<br />\r\n		<br />\r\n		Thank you.<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		<div>\r\n			[SIGNATURE]</div>\r\n	</div>\r\n</div>', 'ORDER DELIVERY EMAIL CONTENT', NULL, 3, '', '', '', 1, '2015-03-26 17:09:34', 488, '2015-03-30 14:58:42', 0),
(178, 'Forgot Password Email Subject', '', '', 'FORGOT PASSWORD EMAIL CONTENT', NULL, 1, '', '', '', 1, '2015-03-26 17:09:34', 488, '2017-04-28 14:52:18', 290),
(179, 'Forgot Password User Email Content', '', '', 'FORGOT PASSWORD EMAIL CONTENT', NULL, 2, '', '', '', 1, '2015-03-26 17:09:34', 488, '2015-03-30 14:58:42', 0),
(197, 'Youtube URL', '', NULL, 'YOUTUBE SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-04-17 20:21:00', 0, '2017-05-02 10:31:04', 10),
(199, 'Email Signature', NULL, '<div>\r\n  <div>Cheers,\r\n  </div>\r\n  <div>Webmaster\r\n  </div>\r\n  <div>Acond\r\n  </div>\r\n  <div><a href="http://www.acond.com/">www.acond.com</a>\r\n  </div>\r\n</div>\r\n', 'EMAIL CONTENT', NULL, 6, NULL, NULL, NULL, 1, '2015-04-22 17:02:51', 594, '2017-04-28 14:52:16', 290),
(200, 'Company Logo', '1494233413_icon-top-panel-acond.gif', '', 'LOGO AND ICON SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-05-12 17:01:13', 0, '2017-05-08 16:51:13', 11),
(201, 'Recommend Icon', '', NULL, 'LOGO AND ICON SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-05-12 17:01:13', 0, '2017-05-08 16:51:13', 11),
(202, 'New Icon', '', NULL, 'LOGO AND ICON SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-05-12 17:01:13', 0, '2017-05-08 16:51:13', 11),
(203, 'Watermark Type', '2', NULL, 'WATERMARK SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-05-12 17:41:51', 0, '2017-05-08 16:51:13', 11),
(204, 'Watermark Opacity', '0.6', NULL, 'WATERMARK SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-05-12 17:41:51', 0, '2017-05-08 16:51:13', 11),
(205, 'Watermark Value', '1493178784_watermrak-sunny.png', NULL, 'WATERMARK SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-05-12 17:41:51', 0, '2017-05-08 16:51:13', 11),
(206, 'Product Page', NULL, NULL, 'DEFAULT PAGE SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-05-12 19:53:26', 0, NULL, 0),
(207, 'Event Page', '0', NULL, 'DEFAULT PAGE SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-05-12 19:53:26', 0, '2017-04-28 14:54:40', 290),
(208, 'molpay', NULL, NULL, 'MOLPAY SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(209, 'merchantID', NULL, NULL, 'MOLPAY SETTINGS', NULL, 2, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(210, 'verifykey', NULL, NULL, 'MOLPAY SETTINGS', NULL, 3, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(211, 'cur', NULL, NULL, 'MOLPAY SETTINGS', NULL, 4, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(212, 'country', NULL, NULL, 'MOLPAY SETTINGS', NULL, 5, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(213, 'langcode', NULL, NULL, 'MOLPAY SETTINGS', NULL, 6, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(214, 'return', NULL, NULL, 'MOLPAY SETTINGS', NULL, 7, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(215, 'returnipn', NULL, NULL, 'MOLPAY SETTINGS', NULL, 8, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(216, 'iPay88EntryPage', NULL, NULL, 'IPAY88 SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(217, 'iPay88EnquiryPage', NULL, NULL, 'IPAY88 SETTINGS', NULL, 2, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(218, 'responseUrl', NULL, NULL, 'IPAY88 SETTINGS', NULL, 3, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(219, 'requestUrl', NULL, NULL, 'IPAY88 SETTINGS', NULL, 4, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(220, 'merchantKey', NULL, NULL, 'IPAY88 SETTINGS', NULL, 5, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(221, 'merchantCode', NULL, NULL, 'IPAY88 SETTINGS', NULL, 6, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(222, 'paymentId', NULL, NULL, 'IPAY88 SETTINGS', NULL, 7, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(223, 'currency', NULL, NULL, 'IPAY88 SETTINGS', NULL, 8, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(224, 'prodDesc', NULL, NULL, 'IPAY88 SETTINGS', NULL, 9, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(225, 'iPay88EntryPage', NULL, NULL, 'IPAY88_ENETS SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(226, 'iPay88EnquiryPage', NULL, NULL, 'IPAY88_ENETS SETTINGS', NULL, 2, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(227, 'responseUrl', NULL, NULL, 'IPAY88_ENETS SETTINGS', NULL, 3, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(228, 'requestUrl', NULL, NULL, 'IPAY88_ENETS SETTINGS', NULL, 4, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(229, 'merchantKey', NULL, NULL, 'IPAY88_ENETS SETTINGS', NULL, 5, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(230, 'merchantCode', NULL, NULL, 'IPAY88_ENETS SETTINGS', NULL, 6, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(231, 'paymentId', NULL, NULL, 'IPAY88_ENETS SETTINGS', NULL, 7, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(232, 'currency', NULL, NULL, 'IPAY88_ENETS SETTINGS', NULL, 8, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(233, 'backendUrl', NULL, NULL, 'IPAY88_ENETS SETTINGS', NULL, 9, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(234, 'useTestUrl', NULL, NULL, 'EGHL SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(235, 'testPaymentUrl', NULL, NULL, 'EGHL SETTINGS', NULL, 2, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(236, 'paymentUrl', NULL, NULL, 'EGHL SETTINGS', NULL, 3, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(237, 'transactionType', NULL, NULL, 'EGHL SETTINGS', NULL, 4, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(238, 'paymentMethod', NULL, NULL, 'EGHL SETTINGS', NULL, 5, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(239, 'merchantID', NULL, NULL, 'EGHL SETTINGS', NULL, 6, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(240, 'merchantPwd', NULL, NULL, 'EGHL SETTINGS', NULL, 7, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(241, 'merchantName', NULL, NULL, 'EGHL SETTINGS', NULL, 8, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(242, 'returnUrl', NULL, NULL, 'EGHL SETTINGS', NULL, 9, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(243, 'callBackUrl', NULL, NULL, 'EGHL SETTINGS', NULL, 10, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(244, 'currencyCode', NULL, NULL, 'EGHL SETTINGS', NULL, 11, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(245, 'pageTimeout', NULL, NULL, 'EGHL SETTINGS', NULL, 12, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(246, 'Order Done Email Subject', 'Completed', '', 'ORDER DONE EMAIL CONTENT', NULL, 1, '', '', '', 1, '2015-05-14 17:34:12', 488, '2017-04-28 14:52:17', 290),
(247, 'Order Done Admin Email Content', '', '<div style="font-size:12px; font-family: arial,helvetica,sans-serif;">\r\n	<div>\r\n		Dear Admin,<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		An order from [NAME] with Order No [ORDNO] is completed.<br />\r\n		<br />\r\n		Thank you.<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		<div>\r\n			[SIGNATURE]</div>\r\n	</div>\r\n</div>', 'ORDER DONE EMAIL CONTENT', NULL, 2, '', '', '', 1, '2015-05-14 17:34:12', 488, '2015-05-14 17:34:12', 0),
(248, 'Order Done User Email Content', '', '<div style="font-size:12px; font-family: arial,helvetica,sans-serif;">\r\n	<div>\r\n		Dear [NAME],</div>\r\n	<div>\r\n		<br />\r\n		Thank you for your purchase, your order with Order No <b>[ORDNO]</b> is completed.<br />\r\n		<br />\r\n		Thank you.<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		<div>\r\n			[SIGNATURE]</div>\r\n	</div>\r\n</div>', 'ORDER DONE EMAIL CONTENT', NULL, 3, '', '', '', 1, '2015-05-14 17:34:12', 488, '2015-05-14 17:34:12', 0),
(249, 'value', '4', NULL, 'MOLPAY SETTINGS', NULL, 9, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(250, 'active', '0', NULL, 'MOLPAY SETTINGS', NULL, 10, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(251, 'name', 'MolPay', NULL, 'MOLPAY SETTINGS', NULL, 11, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(252, 'value', '3', NULL, 'IPAY88 SETTINGS', NULL, 10, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(253, 'active', '0', NULL, 'IPAY88 SETTINGS', NULL, 11, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(254, 'name', 'IPay88', NULL, 'IPAY88 SETTINGS', NULL, 12, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(255, 'value', '5', NULL, 'IPAY88_ENETS SETTINGS', NULL, 10, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(256, 'active', '0', NULL, 'IPAY88_ENETS SETTINGS', NULL, 11, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(257, 'name', 'IPay88 (eNets)', NULL, 'IPAY88_ENETS SETTINGS', NULL, 12, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(258, 'value', '6', NULL, 'EGHL SETTINGS', NULL, 13, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(259, 'active', '0', NULL, 'EGHL SETTINGS', NULL, 14, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(260, 'name', 'eGHL', NULL, 'EGHL SETTINGS', NULL, 15, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(261, 'value', '1', NULL, 'PAYPAL SETTINGS', NULL, 9, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(262, 'active', '0', NULL, 'PAYPAL SETTINGS', NULL, 10, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(263, 'name', 'PayPal', NULL, 'PAYPAL SETTINGS', NULL, 11, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(264, 'value', '2', NULL, 'BANK TRANSFER', NULL, 1, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(265, 'active', '1', NULL, 'BANK TRANSFER', NULL, 2, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(266, 'name', 'Bank Transfer', NULL, 'BANK TRANSFER', NULL, 3, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(267, 'backendUrl', NULL, NULL, 'IPAY88 SETTINGS', NULL, 13, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(268, 'prodDesc', NULL, NULL, 'IPAY88_ENETS SETTINGS', NULL, 13, NULL, NULL, NULL, 1, '2015-05-19 17:46:41', 0, NULL, 0),
(269, 'Company Logo', NULL, NULL, 'ORDER SLIP SETUP', NULL, 1, NULL, NULL, NULL, 1, '2015-05-19 17:06:42', 0, NULL, 0),
(270, 'Company Name', NULL, NULL, 'ORDER SLIP SETUP', NULL, 2, NULL, NULL, NULL, 1, '2015-05-19 17:06:42', 0, NULL, 0),
(271, 'Address 1', NULL, NULL, 'ORDER SLIP SETUP', NULL, 3, NULL, NULL, NULL, 1, '2015-05-19 17:06:42', 0, NULL, 0),
(272, 'Address 2', NULL, NULL, 'ORDER SLIP SETUP', NULL, 4, NULL, NULL, NULL, 1, '2015-05-19 17:06:42', 0, NULL, 0),
(273, 'Tel', NULL, NULL, 'ORDER SLIP SETUP', NULL, 5, NULL, NULL, NULL, 1, '2015-05-19 17:06:42', 0, NULL, 0),
(274, 'Fax', NULL, NULL, 'ORDER SLIP SETUP', NULL, 6, NULL, NULL, NULL, 1, '2015-05-19 17:06:42', 0, NULL, 0),
(275, 'Email', NULL, NULL, 'ORDER SLIP SETUP', NULL, 7, NULL, NULL, NULL, 1, '2015-05-19 17:06:42', 0, NULL, 0),
(276, 'Website', NULL, NULL, 'ORDER SLIP SETUP', NULL, 8, NULL, NULL, NULL, 1, '2015-05-19 17:06:42', 0, NULL, 0),
(277, 'Co Reg No', NULL, NULL, 'ORDER SLIP SETUP', NULL, 9, NULL, NULL, NULL, 1, '2015-05-19 17:06:42', 0, NULL, 0),
(278, 'GST Reg No', NULL, NULL, 'ORDER SLIP SETUP', NULL, 10, NULL, NULL, NULL, 1, '2015-05-19 17:06:42', 0, NULL, 0),
(279, 'Line 1', NULL, NULL, 'ORDER SLIP SETUP', NULL, 11, NULL, NULL, NULL, 1, '2015-05-19 17:06:42', 0, NULL, 0),
(280, 'Line 2', NULL, NULL, 'ORDER SLIP SETUP', NULL, 12, NULL, NULL, NULL, 1, '2015-05-19 17:06:42', 0, NULL, 0),
(281, 'Line 3', NULL, NULL, 'ORDER SLIP SETUP', NULL, 13, NULL, NULL, NULL, 1, '2015-05-19 17:06:42', 0, NULL, 0),
(282, 'API key', '', NULL, 'FACEBOOK LOGIN', NULL, 1, NULL, NULL, NULL, 1, '2015-06-02 19:04:30', 0, '2017-05-02 10:31:04', 10),
(283, 'Secret', '', NULL, 'FACEBOOK LOGIN', NULL, 2, NULL, NULL, NULL, 1, '2015-06-02 19:04:30', 0, '2017-05-02 10:31:04', 10),
(284, 'Active', '0', NULL, 'FACEBOOK LOGIN', NULL, 3, NULL, NULL, NULL, 1, '2015-06-02 19:04:30', 0, '2017-05-02 10:31:04', 10),
(285, 'Twitter URL', '', NULL, 'TWITTER SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-06-02 17:26:59', 0, '2017-05-02 10:31:04', 10),
(286, 'Callback', '', NULL, 'FACEBOOK LOGIN', NULL, 4, NULL, NULL, NULL, 1, '2015-06-02 19:04:30', 0, '2017-05-02 10:31:05', 10),
(287, 'Suffix', '', NULL, 'FACEBOOK LOGIN', NULL, 5, NULL, NULL, NULL, 1, '2015-06-02 19:04:30', 0, '2017-05-02 10:31:05', 10),
(288, 'Google Analytic View ID', '', NULL, 'GOOGLE SETTINGS', NULL, 3, NULL, NULL, NULL, 1, '2015-06-02 19:52:42', 0, '2016-05-06 10:58:53', 822),
(289, 'Mobile View', '0', NULL, 'OTHER SETTINGS', NULL, 4, NULL, NULL, NULL, 1, '2017-05-08 14:54:40', 0, '2017-04-28 14:54:42', 290),
(290, 'Order Cancel Email Subject', '[ORDNO] - Cancel Order', '', 'ORDER CANCEL EMAIL CONTENT', NULL, 1, '', '', '', 1, '2015-07-21 15:49:39', 488, '2017-04-28 14:52:17', 290),
(291, 'Order Cancel Admin Email Content', '', '<div style="font-size:12px; font-family: arial,helvetica,sans-serif;">\r\n	<div>\r\n		Dear Admin,<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		An order from [NAME] has been cancelled.<br />\r\n		<br />\r\n		The order details as below:<br />\r\n		OrderID: [ORDNO]<br />\r\n		Order Item: [ORDITEM]<br />\r\n		Order Total: [CUR] [ORDTOTAL]<br />\r\n		<br />\r\n		Thank you.<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		<div>\r\n			[SIGNATURE]</div>\r\n	</div>\r\n</div>', 'ORDER CANCEL EMAIL CONTENT', NULL, 2, '', '', '', 1, '2015-07-21 15:49:39', 488, '2015-07-22 17:26:45', 0),
(292, 'Order Cancel User Email Content', '', '<div style="font-size:12px; font-family: arial,helvetica,sans-serif;">\r\n	<div>\r\n		Dear [NAME],</div>\r\n	<div>\r\n		<br />\r\n		Your order has been cancelled.<br />\r\n		<br />\r\n		The order details as below:<br />\r\n		OrderID: [ORDNO]<br />\r\n		Order Item: [ORDITEM]<br />\r\n		Order Total: [CUR] [ORDTOTAL]<br />\r\n		<br />\r\n		Thank you.<br />\r\n		&nbsp;</div>\r\n	<div>\r\n		<div>\r\n			[SIGNATURE]</div>\r\n	</div>\r\n</div>', 'ORDER CANCEL EMAIL CONTENT', NULL, 3, '', '', '', 1, '2015-07-21 15:49:39', 488, '2015-07-22 17:26:45', 0),
(293, 'Inventory', '0', NULL, 'PRODUCT SETTINGS', NULL, 2, NULL, NULL, NULL, 1, '2015-08-07 13:08:26', 0, '2017-04-28 14:54:40', 290),
(294, 'Custom Search Engine ID', '', NULL, 'GOOGLE SETTINGS', NULL, 4, NULL, NULL, NULL, 1, '2015-09-22 10:28:16', 0, NULL, 0),
(295, 'Use Volumetric Weight', '', NULL, 'SHIPPING', NULL, 3, NULL, NULL, NULL, 1, '2015-09-22 10:28:16', 0, NULL, 0),
(296, 'Weight Unit', 'g', NULL, 'PRODUCT SETTINGS', NULL, 3, NULL, NULL, NULL, 1, '2015-09-22 10:28:16', 0, '2017-04-28 14:54:40', 290),
(297, 'useSandBox', 'true', NULL, 'PAYPAL SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-11-24 11:25:24', 0, NULL, 0),
(298, 'paypalSandBox', 'https://www.sandbox.paypal.com/cgi-bin/webscr', NULL, 'PAYPAL SETTINGS', NULL, 2, NULL, NULL, NULL, 1, '2015-11-24 11:44:10', 0, NULL, 0),
(299, 'paypal', 'https://www.paypal.com/cgi-bin/webscr', NULL, 'PAYPAL SETTINGS', NULL, 3, NULL, NULL, NULL, 1, '2015-11-24 11:44:48', 0, NULL, 0),
(300, 'currency_code', 'MYR', NULL, 'PAYPAL SETTINGS', NULL, 4, NULL, NULL, NULL, 1, '2015-11-24 11:45:47', 0, NULL, 0),
(301, 'cmd', '_xclick', NULL, 'PAYPAL SETTINGS', NULL, 5, NULL, NULL, NULL, 1, '2015-11-24 11:46:40', 0, NULL, 0),
(302, 'business', 'abc@gmail.com', NULL, 'PAYPAL SETTINGS', NULL, 6, NULL, NULL, NULL, 1, '2015-11-24 11:47:56', 0, NULL, 0),
(303, 'cancel_return', 'usr/cart.aspx?csect=4', NULL, 'PAYPAL SETTINGS', NULL, 7, NULL, NULL, NULL, 1, '2015-11-24 11:48:55', 0, NULL, 0),
(304, 'PDTToken', 'pdttoken', NULL, 'PAYPAL SETTINGS', NULL, 8, NULL, NULL, NULL, 1, '2015-11-24 11:52:04', 0, NULL, 0),
(305, 'Admin Mobile Landing', 'adm00mb.aspx', '', '', NULL, 1, '', '', '', 1, '2015-09-21 12:15:39', 0, '2015-09-21 12:15:39', 0),
(306, 'Admin Mobile Profile', 'admProfilemb.aspx', '', '', NULL, 1, '', '', '', 1, '2015-09-21 12:15:39', 0, '2015-09-21 12:15:39', 0),
(307, 'aspect ratio', '1/1', NULL, 'CROP IMAGE SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2015-10-13 15:23:23', 0, '2017-05-08 16:51:13', 11),
(308, 'Conversion Code (Enquiry)', NULL, '', 'GOOGLE SETTINGS', NULL, 5, NULL, NULL, NULL, 1, '2015-10-09 15:26:05', 0, '2016-05-06 10:58:53', 822),
(309, 'Conversion Code (Order)', NULL, '', 'GOOGLE SETTINGS', NULL, 6, NULL, NULL, NULL, 1, '2015-10-09 15:26:05', 0, '2016-05-06 10:58:53', 822),
(310, 'Conversion Code (Ask)', NULL, '', 'GOOGLE SETTINGS', NULL, 7, NULL, NULL, NULL, 1, '2015-10-09 15:26:05', 0, '2016-05-06 10:58:53', 822),
(311, 'Conversion Code (Share)', NULL, '', 'GOOGLE SETTINGS', NULL, 8, NULL, NULL, NULL, 1, '2015-10-09 15:26:05', 0, '2016-05-06 10:58:53', 822),
(316, 'Conversion Code (Call)', NULL, '', 'GOOGLE SETTINGS', NULL, 8, NULL, NULL, NULL, 1, '2016-02-19 16:16:24', 0, '2016-05-06 10:58:53', 822),
(317, 'Order Manager Delivery Message', '0', NULL, 'OTHER SETTINGS', NULL, 5, NULL, NULL, NULL, 1, '2016-01-08 10:33:43', 0, '2017-04-28 14:54:42', 290),
(318, 'returnMobileUrl', NULL, NULL, 'EGHL SETTINGS', NULL, 16, NULL, NULL, NULL, 1, '2016-04-14 11:27:15', 0, NULL, 0),
(319, 'Display Name', '0', NULL, 'PRODUCT SETTINGS', NULL, 4, NULL, NULL, NULL, 1, '2016-04-28 12:03:16', 0, '2017-04-28 14:54:40', 290),
(320, 'Product Code', '0', NULL, 'PRODUCT SETTINGS', NULL, 5, NULL, NULL, NULL, 1, '2016-04-28 12:03:16', 0, '2017-04-28 14:54:40', 290),
(321, 'Product Snapshot', '0', NULL, 'PRODUCT SETTINGS', NULL, 6, NULL, NULL, NULL, 1, '2016-04-28 12:03:16', 0, '2017-04-28 14:54:40', 290),
(322, 'Product Price', '0', NULL, 'PRODUCT SETTINGS', NULL, 7, NULL, NULL, NULL, 1, '2016-04-28 12:03:16', 0, '2017-04-28 14:54:40', 290),
(323, 'Individual Login Admin Email Subject', '', NULL, 'PAGE AUTHORIZATION EMAIL CONTENT', NULL, 1, NULL, NULL, NULL, 1, '2016-05-18 12:15:06', 0, '2017-04-28 14:52:18', 290),
(324, 'Individual Login Admin Email Content', NULL, '', 'PAGE AUTHORIZATION EMAIL CONTENT', NULL, 2, NULL, NULL, NULL, 1, '2016-05-18 12:15:06', 0, '2016-09-08 17:14:23', 146),
(325, 'Individual Login Company Email Subject', '', NULL, 'PAGE AUTHORIZATION EMAIL CONTENT', NULL, 3, NULL, NULL, NULL, 1, '2016-05-18 12:15:06', 0, '2017-04-28 14:52:18', 290),
(326, 'Individual Login Company Email Content', NULL, '', 'PAGE AUTHORIZATION EMAIL CONTENT', NULL, 4, NULL, NULL, NULL, 1, '2016-05-18 12:15:06', 0, '2016-09-08 17:14:23', 146),
(327, 'Enquiry Title', 'Submit Your Enquiry', NULL, 'ENQUIRY SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2016-08-03 15:42:28', 0, '2017-04-28 14:54:42', 290),
(328, 'Enquiry Description', NULL, '<p>\r\n  \r\n  We welcome any inquries or favorable feedback from you. You may submit in your inquries via our web form below, or alternatively, you may either email us at <a class="hypEmail" href="mailto: [EMAIL]" title="[EMAIL]">[EMAIL]</a>. We will try our very best to reply you as soon as possible.\r\n</p>', 'ENQUIRY SETTINGS', NULL, 2, NULL, NULL, NULL, 1, '2016-08-03 15:42:28', 0, '2017-04-28 14:54:42', 290),
(329, 'CA Logo', '1494233473_fb-logo.gif', NULL, 'LOGO AND ICON SETTINGS', NULL, 5, NULL, NULL, NULL, 1, '2016-08-03 15:42:28', 0, '2017-05-08 16:51:13', 11),
(330, 'Webteq Logo', 'logo_webteq_lightblue.gif', NULL, 'LOGO AND ICON SETTINGS', NULL, 5, NULL, NULL, NULL, 0, '2016-08-03 15:42:28', 0, '2017-05-08 16:50:35', 11),
(331, 'Stop Right Click', '1', NULL, 'WEBSITE SETTINGS', NULL, 5, NULL, NULL, NULL, 1, '2016-08-18 15:36:16', 0, '2017-04-28 14:54:42', 290),
(332, 'Max. Quantity for Single Product', '0', NULL, 'PRODUCT SETTINGS', NULL, 8, NULL, NULL, NULL, 1, '2016-08-18 15:38:11', 0, '2017-04-28 14:54:41', 290),
(333, 'SMS Sender API Key', '', NULL, 'SMS SETTINGS', NULL, 5, NULL, NULL, NULL, 1, '2016-09-08 13:11:08', 0, '2016-09-08 17:18:44', 146),
(334, 'Enquiry SMS', '', 'New enquiry from website. [EnquiryLink]', 'SMS CONTENT', NULL, 1, NULL, NULL, NULL, 1, '2016-09-08 13:45:41', 0, NULL, 0),
(335, 'SMS Default Recipient', '', NULL, 'SMS SETTINGS', NULL, 6, NULL, NULL, NULL, 1, '2016-09-08 13:11:08', 0, '2016-09-08 17:18:44', 146),
(336, 'Send Enquiry SMS', '0', NULL, 'SMS SETTINGS', NULL, 7, NULL, NULL, NULL, 1, '2016-09-08 13:11:08', 0, '2016-09-08 17:18:44', 146),
(337, 'Enquiry Recaptcha', '1', NULL, 'ENQUIRY SETTINGS', NULL, 3, NULL, NULL, NULL, 1, '2016-09-08 13:11:08', 0, '2017-04-28 14:54:42', 290),
(338, 'Recaptcha Site Key', '6LcfHx8UAAAAADj9y8gKXNm0Ev4VeUvpiYi_3UJL', NULL, 'GOOGLE RECAPTCHA SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2016-09-08 13:11:08', 0, '2017-04-28 14:54:42', 290),
(339, 'Recaptcha Secret Key', '6LcfHx8UAAAAAMJ0Pr9NSZcCCUXSJrhg_Mhk7JB-', NULL, 'GOOGLE RECAPTCHA SETTINGS', NULL, 2, NULL, NULL, NULL, 1, '2016-09-08 13:11:08', 0, '2017-04-28 14:54:42', 290),
(340, 'Smtp Server', 'smtp.gmail.com', NULL, 'WEBTEQ EMAIL SETTING', NULL, 1, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(341, 'Default Sender Name', 'Webteq Solution', NULL, 'WEBTEQ EMAIL SETTING', NULL, 2, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(342, 'Default Sender', 'stg@webteq.asia', NULL, 'WEBTEQ EMAIL SETTING', NULL, 3, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(343, 'Default Sender Password', 's123456g', NULL, 'WEBTEQ EMAIL SETTING', NULL, 4, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(344, 'Default Recipient', 'stg@webteq.asia', NULL, 'WEBTEQ EMAIL SETTING', NULL, 5, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(345, 'Use Gmail Smtp', 'True', NULL, 'WEBTEQ EMAIL SETTING', NULL, 6, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(346, 'Port', '587', NULL, 'WEBTEQ EMAIL SETTING', NULL, 7, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(347, 'Enable SSL', 'True', NULL, 'WEBTEQ EMAIL SETTING', NULL, 8, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(348, 'Default Recipient BCC', 'support@webteq.com.my', NULL, 'WEBTEQ EMAIL SETTING', NULL, 9, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(349, 'Page Title Prefix', '', NULL, 'PAGE DETAILS LANGUAGE', 'zh', 1, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(350, 'Default Page Title', '', NULL, 'PAGE DETAILS LANGUAGE', 'zh', 2, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(351, 'Default Keyword', '', NULL, 'PAGE DETAILS LANGUAGE', 'zh', 3, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(352, 'Default Description', '', NULL, 'PAGE DETAILS LANGUAGE', 'zh', 4, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(353, 'Page Title Prefix', '', NULL, 'PAGE DETAILS LANGUAGE', 'ms', 1, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(354, 'Default Page Title', '', NULL, 'PAGE DETAILS LANGUAGE', 'ms', 2, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(355, 'Default Keyword', '', NULL, 'PAGE DETAILS LANGUAGE', 'ms', 3, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(356, 'Default Description', '', NULL, 'PAGE DETAILS LANGUAGE', 'ms', 4, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(357, 'Page Title Prefix', '', NULL, 'PAGE DETAILS LANGUAGE', 'ja', 1, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(358, 'Default Page Title', '', NULL, 'PAGE DETAILS LANGUAGE', 'ja', 2, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(359, 'Default Keyword', '', NULL, 'PAGE DETAILS LANGUAGE', 'ja', 3, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(360, 'Default Description', '', NULL, 'PAGE DETAILS LANGUAGE', 'ja', 4, NULL, NULL, NULL, 1, '2016-10-24 19:20:53', 0, NULL, 0),
(361, 'Random Related Product Enabled', '0', NULL, 'PRODUCT SETTINGS', NULL, 9, NULL, NULL, NULL, 1, '2016-10-25 10:28:10', 28, NULL, 0),
(365, 'Random Related Product Enabled', '0', NULL, 'PRODUCT SETTINGS', NULL, 9, NULL, NULL, NULL, 1, '2016-11-08 15:42:43', 0, NULL, 0),
(366, 'Delivery Week', '1', '0|0|0|0|0|0|0', 'SHIPPING', NULL, 4, NULL, NULL, NULL, 1, '2016-11-08 15:46:27', 0, NULL, 0),
(367, 'Delivery Time', '1', 'Morning(8:00AM - 10:00AM)|Afternoon(12:00PM - 2:00PM)', 'SHIPPING', NULL, 5, NULL, NULL, NULL, 1, '2016-11-08 15:46:27', 0, NULL, 0),
(368, 'Enable Enquiry Recipient', '0', NULL, 'ENQUIRY SETTINGS', NULL, 4, NULL, NULL, NULL, 1, '2016-11-08 15:47:27', 0, '2017-04-28 14:54:42', 290),
(369, 'Image Aspect Ratio', '4/3', NULL, 'PRODUCT SETTINGS', NULL, 10, NULL, NULL, NULL, 1, '2016-11-08 15:48:19', 0, '2017-05-08 16:51:13', 11),
(370, 'Max. Quantity for Single Promotion Product', '10', NULL, 'PRODUCT SETTINGS', NULL, 11, NULL, NULL, NULL, 1, '2016-11-08 15:49:04', 0, '2017-04-28 14:54:41', 290),
(371, 'Random Related Product Enabled', '0', NULL, 'PRODUCT SETTINGS', NULL, 12, NULL, NULL, NULL, 1, '2016-11-08 18:35:57', 215, NULL, 0),
(372, 'Random Related Product Enabled', '0', NULL, 'PRODUCT SETTINGS', NULL, 13, NULL, NULL, NULL, 1, '2016-11-15 14:37:07', 215, NULL, 0),
(373, 'Random Related Product Enabled', '0', NULL, 'PRODUCT SETTINGS', NULL, 14, NULL, NULL, NULL, 1, '2016-11-15 14:37:19', 215, NULL, 0),
(374, 'Random Related Product Enabled', '0', NULL, 'PRODUCT SETTINGS', NULL, 15, NULL, NULL, NULL, 1, '2016-11-15 17:35:27', 215, NULL, 0),
(375, 'Random Related Product Enabled', '0', NULL, 'PRODUCT SETTINGS', NULL, 16, NULL, NULL, NULL, 1, '2016-12-05 10:46:05', 215, NULL, 0),
(376, 'Product Detail Display Type', '1', '', 'PRODUCT SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2017-01-12 16:47:00', 0, NULL, 0),
(378, 'Product Price Range Filter', '0', '', 'PRODUCT SETTINGS', NULL, 1, NULL, NULL, NULL, 1, '2017-01-12 17:57:59', 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_country`
--

CREATE TABLE IF NOT EXISTS `tb_country` (
  `COUNTRY_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `COUNTRY_CODE` varchar(20) CHARACTER SET utf8 NOT NULL,
  `COUNTRY_NAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `COUNTRY_ORDER` int(9) unsigned NOT NULL DEFAULT '0',
  `COUNTRY_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `COUNTRY_DEFAULT` int(9) NOT NULL DEFAULT '0',
  `SHOW_STATE` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`COUNTRY_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=241 ;

--
-- Dumping data for table `tb_country`
--

INSERT INTO `tb_country` (`COUNTRY_ID`, `COUNTRY_CODE`, `COUNTRY_NAME`, `COUNTRY_ORDER`, `COUNTRY_ACTIVE`, `COUNTRY_DEFAULT`, `SHOW_STATE`) VALUES
(1, 'AD', 'Andorra', 0, 1, 0, 1),
(2, 'AE', 'United Arab Emirates', 0, 1, 0, 1),
(3, 'AF', 'Afghanistan', 0, 1, 0, 1),
(4, 'AG', 'Antigua and Barbuda', 0, 1, 0, 1),
(5, 'AI', 'Anguilla', 0, 1, 0, 1),
(6, 'AL', 'Albania', 0, 1, 0, 1),
(7, 'AM', 'Armenia', 0, 1, 0, 1),
(8, 'AN', 'Netherlands Antilles', 0, 1, 0, 1),
(9, 'AO', 'Angola', 0, 1, 0, 1),
(10, 'AQ', 'Antarctica', 0, 1, 0, 1),
(11, 'AR', 'Argentina', 0, 1, 0, 1),
(12, 'AS', 'American Samoa', 0, 1, 0, 1),
(13, 'AT', 'Austria', 0, 1, 0, 1),
(14, 'AU', 'Australia', 0, 1, 0, 1),
(15, 'AW', 'Aruba', 0, 1, 0, 1),
(16, 'AZ', 'Azerbaijan', 0, 1, 0, 1),
(17, 'BA', 'Bosnia and Herzegovina', 0, 1, 0, 1),
(18, 'BB', 'Barbados', 0, 1, 0, 1),
(19, 'BD', 'Bangladesh', 0, 1, 0, 1),
(20, 'BE', 'Belgium', 0, 1, 0, 1),
(21, 'BF', 'Burkina Faso', 0, 1, 0, 1),
(22, 'BG', 'Bulgaria', 0, 1, 0, 1),
(23, 'BH', 'Bahrain', 0, 1, 0, 1),
(24, 'BI', 'Burundi', 0, 1, 0, 1),
(25, 'BJ', 'Benin', 0, 1, 0, 1),
(26, 'BM', 'Bermuda', 0, 1, 0, 1),
(27, 'BN', 'Brunei Darussalam', 0, 1, 0, 1),
(28, 'BO', 'Bolivia', 0, 1, 0, 1),
(29, 'BR', 'Brazil', 0, 1, 0, 1),
(30, 'BS', 'Bahamas', 0, 1, 0, 1),
(31, 'BT', 'Bhutan', 0, 1, 0, 1),
(32, 'BV', 'Bouvet Island', 0, 1, 0, 1),
(33, 'BW', 'Botswana', 0, 1, 0, 1),
(34, 'BY', 'Belarus', 0, 1, 0, 1),
(35, 'BZ', 'Belize', 0, 1, 0, 1),
(36, 'CA', 'Canada', 0, 1, 0, 1),
(37, 'CC', 'Cocos (keeling) Islands', 0, 1, 0, 1),
(38, 'CD', 'Congo, The Democratic Republic Of The', 0, 1, 0, 1),
(39, 'CF', 'Central African Republic', 0, 1, 0, 1),
(40, 'CG', 'Congo', 0, 1, 0, 1),
(41, 'CH', 'Switzerland', 0, 1, 0, 1),
(42, 'CI', 'Cote D''ivoire', 0, 1, 0, 1),
(43, 'CK', 'Cook Islands', 0, 1, 0, 1),
(44, 'CL', 'Chile', 0, 1, 0, 1),
(45, 'CM', 'Cameroon', 0, 1, 0, 1),
(46, 'CN', 'China', 0, 1, 0, 1),
(47, 'CO', 'Colombia', 0, 1, 0, 1),
(48, 'CR', 'Costa Rica', 0, 1, 0, 1),
(49, 'CS', 'Serbia and Montenegro', 0, 1, 0, 1),
(50, 'CU', 'Cuba', 0, 1, 0, 1),
(51, 'CV', 'Cape Verde', 0, 1, 0, 1),
(52, 'CX', 'Christmas Island', 0, 1, 0, 1),
(53, 'CY', 'Cyprus', 0, 1, 0, 1),
(54, 'CZ', 'Czech Republic', 0, 1, 0, 1),
(55, 'DE', 'Germany', 0, 1, 0, 1),
(56, 'DJ', 'Djibouti', 0, 1, 0, 1),
(57, 'DK', 'Denmark', 0, 1, 0, 1),
(58, 'DM', 'Dominica', 0, 1, 0, 1),
(59, 'DO', 'Dominican Republic', 0, 1, 0, 1),
(60, 'DZ', 'Algeria', 0, 1, 0, 1),
(61, 'EC', 'Ecuador', 0, 1, 0, 1),
(62, 'EE', 'Estonia', 0, 1, 0, 1),
(63, 'EG', 'Egypt', 0, 1, 0, 1),
(64, 'EH', 'Western Sahara', 0, 1, 0, 1),
(65, 'ER', 'Eritrea', 0, 1, 0, 1),
(66, 'ES', 'Spain', 0, 1, 0, 1),
(67, 'ET', 'Ethiopia', 0, 1, 0, 1),
(68, 'FI', 'Finland', 0, 1, 0, 1),
(69, 'FJ', 'Fiji', 0, 1, 0, 1),
(70, 'FK', 'Falkland Islands (malvinas)', 0, 1, 0, 1),
(71, 'FM', 'Micronesia, Federated States Of', 0, 1, 0, 1),
(72, 'FO', 'Faroe Islands', 0, 1, 0, 1),
(73, 'FR', 'France', 0, 1, 0, 1),
(74, 'GA', 'Gabon', 0, 1, 0, 1),
(75, 'GB', 'United Kingdom', 0, 1, 0, 1),
(76, 'GD', 'Grenada', 0, 1, 0, 1),
(77, 'GE', 'Georgia', 0, 1, 0, 1),
(78, 'GF', 'French Guiana', 0, 1, 0, 1),
(79, 'GH', 'Ghana', 0, 1, 0, 1),
(80, 'GI', 'Gibraltar', 0, 1, 0, 1),
(81, 'GL', 'Greenland', 0, 1, 0, 1),
(82, 'GM', 'Gambia', 0, 1, 0, 1),
(83, 'GN', 'Guinea', 0, 1, 0, 1),
(84, 'GP', 'Guadeloupe', 0, 1, 0, 1),
(85, 'GQ', 'Equatorial Guinea', 0, 1, 0, 1),
(86, 'GR', 'Greece', 0, 1, 0, 1),
(87, 'GS', 'South Georgia and The South Sandwich Islands', 0, 1, 0, 1),
(88, 'GT', 'Guatemala', 0, 1, 0, 1),
(89, 'GU', 'Guam', 0, 1, 0, 1),
(90, 'GW', 'Guinea-bissau', 0, 1, 0, 1),
(91, 'GY', 'Guyana', 0, 1, 0, 1),
(92, 'HK', 'Hong Kong', 0, 1, 0, 1),
(93, 'HM', 'Heard Island and Mcdonald Islands', 0, 1, 0, 1),
(94, 'HN', 'Honduras', 0, 1, 0, 1),
(95, 'HR', 'Croatia', 0, 1, 0, 1),
(96, 'HT', 'Haiti', 0, 1, 0, 1),
(97, 'HU', 'Hungary', 0, 1, 0, 1),
(98, 'ID', 'Indonesia', 0, 1, 0, 1),
(99, 'IE', 'Ireland', 0, 1, 0, 1),
(100, 'IL', 'Israel', 0, 1, 0, 1),
(101, 'IN', 'India', 0, 1, 0, 1),
(102, 'IO', 'British Indian Ocean Territory', 0, 1, 0, 1),
(103, 'IQ', 'Iraq', 0, 1, 0, 1),
(104, 'IR', 'Iran, Islamic Republic Of', 0, 1, 0, 1),
(105, 'IS', 'Iceland', 0, 1, 0, 1),
(106, 'IT', 'Italy', 0, 1, 0, 1),
(107, 'JM', 'Jamaica', 0, 1, 0, 1),
(108, 'JO', 'Jordan', 0, 1, 0, 1),
(109, 'JP', 'Japan', 0, 1, 0, 1),
(110, 'KE', 'Kenya', 0, 1, 0, 1),
(111, 'KG', 'Kyrgyzstan', 0, 1, 0, 1),
(112, 'KH', 'Cambodia', 0, 1, 0, 1),
(113, 'KI', 'Kiribati', 0, 1, 0, 1),
(114, 'KM', 'Comoros', 0, 1, 0, 1),
(115, 'KN', 'Saint Kitts and Nevis', 0, 1, 0, 1),
(116, 'KP', 'Korea, Democratic People''s Republic Of', 0, 1, 0, 1),
(117, 'KR', 'Korea, Republic Of', 0, 1, 0, 1),
(118, 'KW', 'Kuwait', 0, 1, 0, 1),
(119, 'KY', 'Cayman Islands', 0, 1, 0, 1),
(120, 'KZ', 'Kazakhstan', 0, 1, 0, 1),
(121, 'LA', 'Lao People''s Democratic Republic', 0, 1, 0, 1),
(122, 'LB', 'Lebanon', 0, 1, 0, 1),
(123, 'LC', 'Saint Lucia', 0, 1, 0, 1),
(124, 'LI', 'Liechtenstein', 0, 1, 0, 1),
(125, 'LK', 'Sri Lanka', 0, 1, 0, 1),
(126, 'LR', 'Liberia', 0, 1, 0, 1),
(127, 'LS', 'Lesotho', 0, 1, 0, 1),
(128, 'LT', 'Lithuania', 0, 1, 0, 1),
(129, 'LU', 'Luxembourg', 0, 1, 0, 1),
(130, 'LV', 'Latvia', 0, 1, 0, 1),
(131, 'LY', 'Libyan Arab Jamahiriya', 0, 1, 0, 1),
(132, 'MA', 'Morocco', 0, 1, 0, 1),
(133, 'MC', 'Monaco', 0, 1, 0, 1),
(134, 'MD', 'Moldova, Republic Of', 0, 1, 0, 1),
(135, 'MG', 'Madagascar', 0, 1, 0, 1),
(136, 'MH', 'Marshall Islands', 0, 1, 0, 1),
(137, 'MK', 'Macedonia, The Former Yugoslav Republic Of', 0, 1, 0, 1),
(138, 'ML', 'Mali', 0, 1, 0, 1),
(139, 'MM', 'Myanmar', 0, 1, 0, 1),
(140, 'MN', 'Mongolia', 0, 1, 0, 1),
(141, 'MO', 'Macao', 0, 1, 0, 1),
(142, 'MP', 'Northern Mariana Islands', 0, 1, 0, 1),
(143, 'MQ', 'Martinique', 0, 1, 0, 1),
(144, 'MR', 'Mauritania', 0, 1, 0, 1),
(145, 'MS', 'Montserrat', 0, 1, 0, 1),
(146, 'MT', 'Malta', 0, 1, 0, 1),
(147, 'MU', 'Mauritius', 0, 1, 0, 1),
(148, 'MV', 'Maldives', 0, 1, 0, 1),
(149, 'MW', 'Malawi', 0, 1, 0, 1),
(150, 'MX', 'Mexico', 0, 1, 0, 1),
(151, 'MY', 'Malaysia', 0, 1, 0, 1),
(152, 'MZ', 'Mozambique', 0, 1, 0, 1),
(153, 'NA', 'Namibia', 0, 1, 0, 1),
(154, 'NC', 'New Caledonia', 0, 1, 0, 1),
(155, 'NE', 'Niger', 0, 1, 0, 1),
(156, 'NF', 'Norfolk Island', 0, 1, 0, 1),
(157, 'NG', 'Nigeria', 0, 1, 0, 1),
(158, 'NI', 'Nicaragua', 0, 1, 0, 1),
(159, 'NL', 'Netherlands', 0, 1, 0, 1),
(160, 'NO', 'Norway', 0, 1, 0, 1),
(161, 'NP', 'Nepal', 0, 1, 0, 1),
(162, 'NR', 'Nauru', 0, 1, 0, 1),
(163, 'NU', 'Niue', 0, 1, 0, 1),
(164, 'NZ', 'New Zealand', 0, 1, 0, 1),
(165, 'OM', 'Oman', 0, 1, 0, 1),
(166, 'PA', 'Panama', 0, 1, 0, 1),
(167, 'PE', 'Peru', 0, 1, 0, 1),
(168, 'PF', 'French Polynesia', 0, 1, 0, 1),
(169, 'PG', 'Papua New Guinea', 0, 1, 0, 1),
(170, 'PH', 'Philippines', 0, 1, 0, 1),
(171, 'PK', 'Pakistan', 0, 1, 0, 1),
(172, 'PL', 'Poland', 0, 1, 0, 1),
(173, 'PM', 'Saint Pierre and Miquelon', 0, 1, 0, 1),
(174, 'PN', 'Pitcairn', 0, 1, 0, 1),
(175, 'PR', 'Puerto Rico', 0, 1, 0, 1),
(176, 'PS', 'Palestinian Territory, Occupied', 0, 1, 0, 1),
(177, 'PT', 'Portugal', 0, 1, 0, 1),
(178, 'PW', 'Palau', 0, 1, 0, 1),
(179, 'PY', 'Paraguay', 0, 1, 0, 1),
(180, 'QA', 'Qatar', 0, 1, 0, 1),
(181, 'RE', 'Reunion', 0, 1, 0, 1),
(182, 'RO', 'Romania', 0, 1, 0, 1),
(183, 'RU', 'Russian Federation', 0, 1, 0, 1),
(184, 'RW', 'Rwanda', 0, 1, 0, 1),
(185, 'SA', 'Saudi Arabia', 0, 1, 0, 1),
(186, 'SB', 'Solomon Islands', 0, 1, 0, 1),
(187, 'SC', 'Seychelles', 0, 1, 0, 1),
(188, 'SD', 'Sudan', 0, 1, 0, 1),
(189, 'SE', 'Sweden', 0, 1, 0, 1),
(190, 'SG', 'Singapore', 0, 1, 0, 0),
(191, 'SH', 'Saint Helena', 0, 1, 0, 1),
(192, 'SI', 'Slovenia', 0, 1, 0, 1),
(193, 'SJ', 'Svalbard and Jan Mayen', 0, 1, 0, 1),
(194, 'SK', 'Slovakia', 0, 1, 0, 1),
(195, 'SL', 'Sierra Leone', 0, 1, 0, 1),
(196, 'SM', 'San Marino', 0, 1, 0, 1),
(197, 'SN', 'Senegal', 0, 1, 0, 1),
(198, 'SO', 'Somalia', 0, 1, 0, 1),
(199, 'SR', 'Suriname', 0, 1, 0, 1),
(200, 'ST', 'Sao Tome and Principe', 0, 1, 0, 1),
(201, 'SV', 'El Salvador', 0, 1, 0, 1),
(202, 'SY', 'Syrian Arab Republic', 0, 1, 0, 1),
(203, 'SZ', 'Swaziland', 0, 1, 0, 1),
(204, 'TC', 'Turks and Caicos Islands', 0, 1, 0, 1),
(205, 'TD', 'Chad', 0, 1, 0, 1),
(206, 'TF', 'French Southern Territories', 0, 1, 0, 1),
(207, 'TG', 'Togo', 0, 1, 0, 1),
(208, 'TH', 'Thailand', 0, 1, 0, 1),
(209, 'TJ', 'Tajikistan', 0, 1, 0, 1),
(210, 'TK', 'Tokelau', 0, 1, 0, 1),
(211, 'TL', 'Timor-leste', 0, 1, 0, 1),
(212, 'TM', 'Turkmenistan', 0, 1, 0, 1),
(213, 'TN', 'Tunisia', 0, 1, 0, 1),
(214, 'TO', 'Tonga', 0, 1, 0, 1),
(215, 'TR', 'Turkey', 0, 1, 0, 1),
(216, 'TT', 'Trinidad and Tobago', 0, 1, 0, 1),
(217, 'TV', 'Tuvalu', 0, 1, 0, 1),
(218, 'TW', 'Taiwan, Province Of China', 0, 1, 0, 1),
(219, 'TZ', 'Tanzania, United Republic Of', 0, 1, 0, 1),
(220, 'UA', 'Ukraine', 0, 1, 0, 1),
(221, 'UG', 'Uganda', 0, 1, 0, 1),
(222, 'UM', 'United States Minor Outlying Islands', 0, 1, 0, 1),
(223, 'US', 'United States', 0, 1, 0, 1),
(224, 'UY', 'Uruguay', 0, 1, 0, 1),
(225, 'UZ', 'Uzbekistan', 0, 1, 0, 1),
(226, 'VA', 'Holy See (vatican City State)', 0, 1, 0, 1),
(227, 'VC', 'Saint Vincent and The Grenadines', 0, 1, 0, 1),
(228, 'VE', 'Venezuela', 0, 1, 0, 1),
(229, 'VG', 'Virgin Islands, British', 0, 1, 0, 1),
(230, 'VI', 'Virgin Islands, U.s.', 0, 1, 0, 1),
(231, 'VN', 'Viet Nam', 0, 1, 0, 1),
(232, 'VU', 'Vanuatu', 0, 1, 0, 1),
(233, 'WF', 'Wallis and Futuna', 0, 1, 0, 1),
(234, 'WS', 'Samoa', 0, 1, 0, 1),
(235, 'YE', 'Yemen', 0, 1, 0, 1),
(236, 'YT', 'Mayotte', 0, 1, 0, 1),
(237, 'ZA', 'South Africa', 0, 1, 0, 1),
(238, 'ZM', 'Zambia', 0, 1, 0, 1),
(239, 'ZW', 'Zimbabwe', 0, 1, 0, 1),
(240, 'ALL', 'All', 0, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_coupon`
--

CREATE TABLE IF NOT EXISTS `tb_coupon` (
  `COUPON_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `COUPON_NAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `COUPON_CODE` varchar(20) CHARACTER SET utf8 NOT NULL,
  `COUPON_DISOPERATOR` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `COUPON_DISVALUE` decimal(5,2) NOT NULL DEFAULT '0.00',
  `COUPON_STARTDATE` datetime DEFAULT NULL,
  `COUPON_ENDDATE` datetime DEFAULT NULL,
  `COUPON_ACTIVE` int(1) NOT NULL DEFAULT '1',
  `COUPON_CREATION` datetime DEFAULT NULL,
  `COUPON_CREATEDBY` bigint(9) NOT NULL DEFAULT '0',
  `COUPON_LASTUPDATE` datetime DEFAULT NULL,
  `COUPON_UPDATEDBY` bigint(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`COUPON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_currency`
--

CREATE TABLE IF NOT EXISTS `tb_currency` (
  `CUR_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `CUR_NAME` varchar(150) CHARACTER SET utf8 DEFAULT '',
  `CUR_VALUE` varchar(150) CHARACTER SET utf8 DEFAULT '',
  `CUR_FLAG` varchar(250) CHARACTER SET utf8 DEFAULT '',
  `CUR_ORDER` bigint(9) unsigned zerofill NOT NULL,
  `CUR_ACTIVE` int(1) unsigned zerofill NOT NULL,
  `CUR_CONVRATE` decimal(7,2) NOT NULL DEFAULT '0.00',
  `CUR_CREATION` datetime DEFAULT NULL,
  `CUR_CREATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `CUR_LASTUPDATE` datetime DEFAULT NULL,
  `CUR_UPDATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `CUR_VALUE2` varchar(150) CHARACTER SET utf8 DEFAULT '',
  PRIMARY KEY (`CUR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_dealprice`
--

CREATE TABLE IF NOT EXISTS `tb_dealprice` (
  `PRC_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `DEAL_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `PRC_NORMALPRICE` decimal(7,2) NOT NULL DEFAULT '0.00',
  `PRC_PRICE` decimal(7,2) NOT NULL DEFAULT '0.00',
  `PRC_DISCOUNTRATE` decimal(5,2) NOT NULL DEFAULT '0.00',
  `PRC_DISCOUNTTOTAL` decimal(7,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`PRC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee`
--

CREATE TABLE IF NOT EXISTS `tb_employee` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_name` varchar(250) NOT NULL,
  `employee_email` varchar(50) NOT NULL,
  `employee_contact_tel` varchar(20) DEFAULT NULL,
  `employee_login_username` varchar(250) DEFAULT NULL,
  `employee_login_password` varchar(250) DEFAULT NULL,
  `employee_active` int(1) NOT NULL DEFAULT '1',
  `user_id` int(9) NOT NULL DEFAULT '0',
  `employee_createdby` int(9) NOT NULL DEFAULT '0',
  `employee_updatedby` int(9) NOT NULL DEFAULT '0',
  `employee_login_timestamp` datetime DEFAULT NULL,
  `employee_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `employee_lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `employee_team` varchar(50) NOT NULL DEFAULT '',
  `employee_tel_prefix` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tb_employee`
--

INSERT INTO `tb_employee` (`employee_id`, `employee_name`, `employee_email`, `employee_contact_tel`, `employee_login_username`, `employee_login_password`, `employee_active`, `user_id`, `employee_createdby`, `employee_updatedby`, `employee_login_timestamp`, `employee_creation`, `employee_lastupdated`, `employee_team`, `employee_tel_prefix`) VALUES
(6, 'Alex', 'alex@mail.com', '7781195', '+60137781195', 'xKdFz3KR9Mw=', 1, 0, 0, 0, NULL, '2017-06-23 06:22:52', '2017-06-23 08:56:29', 'Group1', '+6016'),
(7, 'Ben', 'ben@mail.com', '6658947', '+60166658947', '+BjhZ9UFIjs=', 1, 0, 0, 0, NULL, '2017-06-23 06:24:54', '2017-06-23 08:56:40', 'Group2', '+6012'),
(8, 'Charlie', 'charlie@mail.com', '5243995', 'charlie', 'WSmuwtcRAgI=', 1, 0, 0, 0, NULL, '2017-06-23 08:58:14', '2017-06-23 08:58:14', 'Group2', '+6011'),
(9, 'David', 'david@mail.com', '7148995', 'david', 'TsoLENMHA7w=', 1, 0, 0, 0, NULL, '2017-06-23 08:59:03', '2017-06-23 08:59:03', 'Group2', '+6012'),
(10, 'Eric', 'eric@mail.com', '7788995', 'eric', 'Y9dacZPu1rE=', 1, 0, 0, 0, NULL, '2017-06-23 08:59:45', '2017-06-23 08:59:45', 'Group1', '+6016');

-- --------------------------------------------------------

--
-- Table structure for table `tb_enquiry`
--

CREATE TABLE IF NOT EXISTS `tb_enquiry` (
  `ENQ_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `ENQ_EMAILRECIPIENT` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `ENQ_URL` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `ENQ_SALUTATION` bigint(9) unsigned DEFAULT '0',
  `ENQ_NAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `ENQ_COMPANYNAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `ENQ_CONTACTNO` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `ENQ_EMAILSENDER` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `ENQ_SUBJECT` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `ENQ_MESSAGE` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `ENQ_GEOLOCATION` varchar(250) DEFAULT NULL,
  `ENQ_REMARK1` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `ENQ_REMARK2` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `ENQ_REMARK3` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `ENQ_REMARK4` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `ENQ_REMARK5` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `ENQ_RANDOMID` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `ENQ_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `ENQ_CREATION` datetime NOT NULL,
  `ENQ_LASTUPDATE` datetime DEFAULT NULL,
  `ENQ_LASTRESEND` datetime DEFAULT NULL,
  `ENQ_RESENDCOUNT` int(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ENQ_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_enquiry_field`
--

CREATE TABLE IF NOT EXISTS `tb_enquiry_field` (
  `enqfield_id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `enqfield_name` varchar(250) DEFAULT NULL,
  `enqfield_type` varchar(20) NOT NULL,
  `enqfield_parent` int(9) unsigned NOT NULL DEFAULT '0',
  `enqfield_required` int(1) unsigned NOT NULL DEFAULT '1',
  `enqfield_validate` varchar(250) DEFAULT NULL,
  `enqfield_position_x` varchar(10) NOT NULL DEFAULT '0',
  `enqfield_position_y` varchar(10) NOT NULL DEFAULT '0',
  `enqfield_size_x` varchar(10) DEFAULT NULL,
  `enqfield_size_y` varchar(10) DEFAULT NULL,
  `enqfield_order` int(2) unsigned NOT NULL DEFAULT '99',
  `enqfield_active` int(1) unsigned NOT NULL DEFAULT '1',
  `enqfield_deletable` int(1) unsigned NOT NULL DEFAULT '1',
  `enqfield_creation` datetime NOT NULL,
  `enqfield_createdby` int(9) unsigned NOT NULL,
  `enqfield_lastupdate` datetime DEFAULT NULL,
  `enqfield_updatedby` int(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`enqfield_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tb_enquiry_field`
--

INSERT INTO `tb_enquiry_field` (`enqfield_id`, `enqfield_name`, `enqfield_type`, `enqfield_parent`, `enqfield_required`, `enqfield_validate`, `enqfield_position_x`, `enqfield_position_y`, `enqfield_size_x`, `enqfield_size_y`, `enqfield_order`, `enqfield_active`, `enqfield_deletable`, `enqfield_creation`, `enqfield_createdby`, `enqfield_lastupdate`, `enqfield_updatedby`) VALUES
(1, 'salutation', 'select', 0, 1, NULL, '1', '1', '12', '1', 1, 1, 0, '2017-01-12 18:10:30', 0, NULL, 0),
(2, 'name', 'input', 0, 1, NULL, '2', '1', '12', '1', 2, 1, 0, '2017-01-12 18:10:30', 0, NULL, 0),
(3, 'company', 'input', 0, 0, NULL, '3', '1', '12', '1', 3, 1, 0, '2017-01-12 18:10:30', 0, NULL, 0),
(4, 'contact', 'input', 0, 1, '^\\+?\\d{2,5}\\s?-?\\s?\\d{3,7}\\s?\\d{3,10}$', '4', '1', '12', '1', 4, 1, 0, '2017-01-12 18:10:30', 0, NULL, 0),
(5, 'email', 'input', 0, 1, '\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*', '5', '1', '12', '1', 5, 1, 0, '2017-01-12 18:10:30', 0, NULL, 0),
(6, 'subject', 'input', 0, 1, NULL, '6', '1', '12', '1', 6, 1, 0, '2017-01-12 18:10:30', 0, NULL, 0),
(7, 'message', 'textarea', 0, 1, NULL, '7', '1', '12', '1', 7, 1, 0, '2017-01-12 18:10:30', 0, NULL, 0),
(8, NULL, 'option', 1, 0, NULL, '0', '1', '12', '1', 99, 1, 1, '2017-01-12 18:12:06', 0, NULL, 0),
(9, NULL, 'option', 1, 0, NULL, '0', '1', '12', '1', 99, 1, 1, '2017-01-12 18:12:06', 0, NULL, 0),
(10, NULL, 'option', 1, 0, NULL, '0', '1', '12', '1', 99, 1, 1, '2017-01-12 18:12:06', 0, NULL, 0),
(11, 'recipient', 'select', 0, 1, NULL, '8', '1', '12', '1', 9, 0, 0, '2017-02-13 15:19:49', 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_enquiry_field_attribute`
--

CREATE TABLE IF NOT EXISTS `tb_enquiry_field_attribute` (
  `enqattr_id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `enqfield_id` int(1) unsigned NOT NULL DEFAULT '1',
  `enqattr_name` varchar(20) NOT NULL,
  `enqattr_value` varchar(250) NOT NULL,
  PRIMARY KEY (`enqattr_id`),
  KEY `enqfield_id` (`enqfield_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tb_enquiry_field_attribute`
--

INSERT INTO `tb_enquiry_field_attribute` (`enqattr_id`, `enqfield_id`, `enqattr_name`, `enqattr_value`) VALUES
(1, 8, 'value', '1'),
(2, 9, 'value', '2'),
(3, 10, 'value', '3');

-- --------------------------------------------------------

--
-- Table structure for table `tb_enquiry_field_label`
--

CREATE TABLE IF NOT EXISTS `tb_enquiry_field_label` (
  `enqlabel_id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `enqfield_id` int(9) unsigned NOT NULL,
  `enqlabel_value` varchar(250) NOT NULL,
  `enqlabel_lang` varchar(10) NOT NULL DEFAULT 'en',
  PRIMARY KEY (`enqlabel_id`),
  KEY `enqfield_id` (`enqfield_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tb_enquiry_field_label`
--

INSERT INTO `tb_enquiry_field_label` (`enqlabel_id`, `enqfield_id`, `enqlabel_value`, `enqlabel_lang`) VALUES
(1, 1, 'Salutation', 'en'),
(2, 2, 'Name', 'en'),
(3, 3, 'Company Name', 'en'),
(4, 4, 'Contact No.', 'en'),
(5, 5, 'Email', 'en'),
(6, 6, 'Subject', 'en'),
(7, 7, 'Message', 'en'),
(8, 8, 'Ms.', 'en'),
(9, 9, 'Mr.', 'en'),
(10, 10, 'Mrs.', 'en'),
(11, 11, 'Recipient', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `tb_enquiry_recipient`
--

CREATE TABLE IF NOT EXISTS `tb_enquiry_recipient` (
  `enqrcpt_id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `enqrcpt_name` varchar(250) NOT NULL,
  `enqrcpt_email` varchar(250) NOT NULL,
  `enqrcpt_order` int(2) unsigned NOT NULL DEFAULT '99',
  `enqrcpt_active` int(1) unsigned NOT NULL DEFAULT '1',
  `enqrcpt_createdby` int(9) unsigned NOT NULL,
  `enqrcpt_creation` datetime NOT NULL,
  `enqrcpt_updatedby` int(9) unsigned NOT NULL DEFAULT '0',
  `enqrcpt_lastupdate` datetime DEFAULT NULL,
  PRIMARY KEY (`enqrcpt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_group`
--

CREATE TABLE IF NOT EXISTS `tb_group` (
  `GRP_ID` bigint(9) NOT NULL AUTO_INCREMENT,
  `GRP_CODE` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `GRP_NAME` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `GRP_NAME_JP` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `GRP_NAME_MS` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `GRP_NAME_ZH` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `GRP_DNAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `GRP_DNAME_JP` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `GRP_DNAME_MS` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `GRP_DNAME_ZH` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `GRP_PAGETITLEFURL` varchar(250) CHARACTER SET utf8 NOT NULL,
  `GRP_DESC` text CHARACTER SET utf8,
  `GRP_SNAPSHOT` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `GRPING_ID` bigint(9) NOT NULL DEFAULT '0',
  `GRP_IMAGE` varchar(250) DEFAULT NULL,
  `GRP_SHOWTOP` int(1) NOT NULL DEFAULT '0',
  `GRP_ACTIVE` int(1) NOT NULL DEFAULT '1',
  `GRP_ORDER` bigint(9) NOT NULL DEFAULT '0',
  `GRP_CREATION` datetime NOT NULL,
  `GRP_CREATEDBY` bigint(9) NOT NULL DEFAULT '0',
  `GRP_LASTUPDATE` datetime DEFAULT NULL,
  `GRP_UPDATEDBY` bigint(9) NOT NULL DEFAULT '0',
  `GRP_OTHER` int(1) unsigned NOT NULL DEFAULT '0',
  `GRP_SHOWDESC` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`GRP_ID`,`GRPING_ID`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tb_group`
--

INSERT INTO `tb_group` (`GRP_ID`, `GRP_CODE`, `GRP_NAME`, `GRP_NAME_JP`, `GRP_NAME_MS`, `GRP_NAME_ZH`, `GRP_DNAME`, `GRP_DNAME_JP`, `GRP_DNAME_MS`, `GRP_DNAME_ZH`, `GRP_PAGETITLEFURL`, `GRP_DESC`, `GRP_SNAPSHOT`, `GRPING_ID`, `GRP_IMAGE`, `GRP_SHOWTOP`, `GRP_ACTIVE`, `GRP_ORDER`, `GRP_CREATION`, `GRP_CREATEDBY`, `GRP_LASTUPDATE`, `GRP_UPDATEDBY`, `GRP_OTHER`, `GRP_SHOWDESC`) VALUES
(1, 'OTHER1', 'OTHER', NULL, NULL, NULL, 'OTHER', NULL, NULL, NULL, '', NULL, NULL, 1, '', 0, 0, 1, '2017-05-08 14:54:40', 1, '2017-05-08 14:54:40', 12, 1, 0),
(2, 'OTHER2', 'OTHER', NULL, NULL, NULL, 'OTHER', NULL, NULL, NULL, '', NULL, NULL, 2, '', 0, 0, 2, '2017-05-08 14:54:40', 1, '2017-05-08 14:54:40', 0, 2, 0),
(3, 'OTHER3', 'OTHER', NULL, NULL, NULL, 'OTHER', NULL, NULL, NULL, '', NULL, NULL, 3, '', 0, 0, 3, '2017-05-08 14:54:40', 1, '2017-05-08 14:54:40', 12, 1, 0),
(4, 'OTHER4', 'OTHER', NULL, NULL, NULL, 'OTHER', NULL, NULL, NULL, '', NULL, NULL, 4, '', 0, 0, 4, '2017-05-08 14:54:40', 1, '2017-05-08 14:54:40', 12, 1, 0),
(5, 'OTHER5', 'OTHER', NULL, NULL, NULL, 'OTHER', NULL, NULL, NULL, '', NULL, NULL, 5, '', 0, 0, 5, '2017-05-08 14:54:40', 1, '2017-05-08 14:54:40', 12, 1, 0),
(6, 'OTHER6', 'OTHER', NULL, NULL, NULL, 'OTHER', NULL, NULL, NULL, '', NULL, NULL, 6, '', 0, 0, 6, '2017-05-08 14:54:40', 1, '2017-05-08 14:54:40', 12, 1, 0),
(7, 'OTHER7', 'OTHER', NULL, NULL, NULL, 'OTHER', NULL, NULL, NULL, '', NULL, NULL, 7, '', 0, 0, 7, '2017-05-08 14:54:40', 1, '2017-05-08 14:54:40', 12, 1, 0),
(8, 'OTHER8', 'OTHER', NULL, NULL, NULL, 'OTHER', NULL, NULL, NULL, '', NULL, NULL, 8, '', 0, 0, 8, '2017-05-08 14:54:40', 1, '2017-05-08 14:54:40', 12, 1, 0),
(9, 'OTHER9', 'OTHER', NULL, NULL, NULL, 'OTHER', NULL, NULL, NULL, '', NULL, NULL, 9, '', 0, 0, 9, '2017-05-08 14:54:40', 1, '2017-05-08 14:54:40', 12, 1, 0),
(10, 'OTHER10', 'OTHER', NULL, NULL, NULL, 'OTHER', NULL, NULL, NULL, '', NULL, NULL, 10, '', 0, 0, 10, '2017-05-08 14:54:40', 1, '2017-05-08 14:54:40', 12, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_higharticle`
--

CREATE TABLE IF NOT EXISTS `tb_higharticle` (
  `HIGHART_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `HIGH_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `HIGHART_TITLE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `HIGHART_TITLE_ZH` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `HIGHART_FILE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`HIGHART_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_highcomment`
--

CREATE TABLE IF NOT EXISTS `tb_highcomment` (
  `HIGHCOMM_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `HIGH_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `HIGHCOMM_COMMENT` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `HIGHCOMM_POSTEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `HIGHCOMM_POSTEDBYNAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `HIGHCOMM_EMAIL` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `HIGHCOMM_REPLY` int(1) unsigned NOT NULL DEFAULT '0',
  `HIGHCOMM_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `HIGHCOMM_POSTEDDATE` datetime DEFAULT NULL,
  PRIMARY KEY (`HIGHCOMM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_highgallery`
--

CREATE TABLE IF NOT EXISTS `tb_highgallery` (
  `HIGHGALL_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `HIGH_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `HIGHGALL_TITLE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `HIGHGALL_TITLE_ZH` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `HIGHGALL_IMAGE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `HIGHGALL_ORDER` int(9) DEFAULT NULL,
  `HIGHGALL_TITLE_JP` varchar(250) DEFAULT NULL,
  `HIGHGALL_TITLE_MS` varchar(250) DEFAULT NULL,
  `HIGHGALL_THUMBNAIL` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`HIGHGALL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_highgroup`
--

CREATE TABLE IF NOT EXISTS `tb_highgroup` (
  `HIGHGRP_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `HIGH_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `GRP_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`HIGHGRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_highlight`
--

CREATE TABLE IF NOT EXISTS `tb_highlight` (
  `HIGH_ID` bigint(9) NOT NULL AUTO_INCREMENT,
  `HIGH_TITLE` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `HIGH_TITLEFURL` varchar(250) DEFAULT NULL,
  `HIGH_TITLE_ZH` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `HIGH_SNAPSHOT` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `HIGH_SNAPSHOT_JP` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `HIGH_SNAPSHOT_MS` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `HIGH_SNAPSHOT_ZH` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `HIGH_SHORTDESC` text CHARACTER SET utf8 NOT NULL,
  `HIGH_SHORTDESC_ZH` text CHARACTER SET utf8,
  `HIGH_STARTDATE` datetime DEFAULT NULL,
  `HIGH_ENDDATE` datetime DEFAULT NULL,
  `HIGH_IMAGE` varchar(250) DEFAULT NULL,
  `HIGH_ORDER` int(9) NOT NULL DEFAULT '0',
  `HIGH_SHOWTOP` int(1) NOT NULL DEFAULT '1',
  `HIGH_VISIBLE` int(1) NOT NULL DEFAULT '1',
  `HIGH_ALLOWCOMMENT` int(1) NOT NULL DEFAULT '1',
  `HIGH_SOURCE_URL` varchar(500) DEFAULT NULL,
  `HIGH_SOURCE_TITLE` varchar(500) DEFAULT NULL,
  `HIGH_DESC` text CHARACTER SET utf8,
  `HIGH_DESC_ZH` text CHARACTER SET utf8,
  `HIGH_ACTIVE` int(1) NOT NULL DEFAULT '1',
  `HIGH_CREATEDBY` bigint(9) NOT NULL DEFAULT '0',
  `HIGH_CREATION` datetime NOT NULL,
  `HIGH_UPDATEDBY` bigint(9) DEFAULT '0',
  `HIGH_LASTUPDATE` datetime DEFAULT NULL,
  `HIGH_TITLE_JP` varchar(250) DEFAULT NULL,
  `HIGH_TITLE_MS` varchar(250) DEFAULT NULL,
  `HIGH_SHORTDESC_JP` text CHARACTER SET utf8,
  `HIGH_SHORTDESC_MS` text CHARACTER SET utf8,
  `HIGH_DESC_JP` text,
  `HIGH_DESC_MS` text,
  PRIMARY KEY (`HIGH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_highreply`
--

CREATE TABLE IF NOT EXISTS `tb_highreply` (
  `HIGHREPLY_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `HIGHCOMM_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `HIGH_ID` bigint(9) NOT NULL DEFAULT '0',
  `HIGHREPLY_COMMENT` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `HIGHREPLY_REPLIEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `HIGHREPLY_REPLIEDBYNAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `HIGHREPLY_EMAIL` varchar(250) DEFAULT NULL,
  `HIGHREPLY_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `HIGHREPLY_REPLIEDDATE` datetime DEFAULT NULL,
  PRIMARY KEY (`HIGHREPLY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_highvideo`
--

CREATE TABLE IF NOT EXISTS `tb_highvideo` (
  `HIGHVIDEO_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `HIGH_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `HIGHVIDEO_VIDEO` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `HIGHVIDEO_THUMBNAIL` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`HIGHVIDEO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_inventory`
--

CREATE TABLE IF NOT EXISTS `tb_inventory` (
  `INV_ID` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `PROD_ID` int(9) unsigned NOT NULL DEFAULT '0',
  `INV_QTY` varchar(20) DEFAULT NULL,
  `INV_CREATION` datetime NOT NULL,
  `INV_LASTUPDATE` datetime DEFAULT NULL,
  PRIMARY KEY (`INV_ID`),
  KEY `PROD_ID` (`PROD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_invtranslog`
--

CREATE TABLE IF NOT EXISTS `tb_invtranslog` (
  `INVTRANS_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `INVTRANS_PRODID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `INVTRANS_ORDERID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `INVTRANS_SPECID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `INVTRANS_CREATION` datetime NOT NULL,
  `INVTRANS_STOCKBEFORE` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `INVTRANS_QTY` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `INVTRANS_STOCKAFTER` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`INVTRANS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_list`
--

CREATE TABLE IF NOT EXISTS `tb_list` (
  `LIST_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `LIST_NAME` varchar(50) CHARACTER SET utf8 NOT NULL,
  `LIST_VALUE` varchar(250) CHARACTER SET utf8 NOT NULL,
  `LIST_GROUP` varchar(50) CHARACTER SET utf8 NOT NULL,
  `LIST_ORDER` int(9) unsigned NOT NULL,
  `LIST_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `LIST_PARENTVALUE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `LIST_PARENTGROUP` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `LIST_LANG` varchar(10) NOT NULL DEFAULT 'en',
  `LIST_DEFAULT` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`LIST_ID`),
  KEY `LIST_NAME` (`LIST_NAME`),
  KEY `LIST_VALUE` (`LIST_VALUE`),
  KEY `LIST_GROUP` (`LIST_GROUP`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1019 ;

--
-- Dumping data for table `tb_list`
--

INSERT INTO `tb_list` (`LIST_ID`, `LIST_NAME`, `LIST_VALUE`, `LIST_GROUP`, `LIST_ORDER`, `LIST_ACTIVE`, `LIST_PARENTVALUE`, `LIST_PARENTGROUP`, `LIST_LANG`, `LIST_DEFAULT`) VALUES
(1, 'New', '1', 'ORDER STATUS', 1, 1, '1', 'ORDER TYPE', 'en', 1),
(2, 'Paid', '2', 'ORDER STATUS', 2, 1, '1', 'ORDER TYPE', 'en', 1),
(3, 'Processing', '3', 'ORDER STATUS', 3, 1, '1', 'ORDER TYPE', 'en', 1),
(4, 'Shipping', '4', 'ORDER STATUS', 4, 1, '1', 'ORDER TYPE', 'en', 1),
(5, 'Done', '5', 'ORDER STATUS', 5, 1, '1', 'ORDER TYPE', 'en', 1),
(6, 'Uploaded', '6', 'ORDER STATUS', 1, 1, '2', 'ORDER TYPE', 'en', 1),
(7, 'Approve', '7', 'ORDER STATUS', 2, 1, '2', 'ORDER TYPE', 'en', 1),
(8, 'Reject', '8', 'ORDER STATUS', 2, 1, '2', 'ORDER TYPE', 'en', 1),
(9, 'Online', '1', 'ORDER TYPE', 1, 1, '0', NULL, 'en', 1),
(10, 'Upload', '2', 'ORDER TYPE', 2, 1, '0', NULL, 'en', 1),
(11, 'Cancelled', '-1', 'ORDER STATUS', 6, 1, '0', '', 'en', 1),
(12, 'Items', '1', 'ORDER DETAIL', 1, 1, '0', NULL, 'en', 1),
(13, 'Delivery', '2', 'ORDER DETAIL', 2, 1, '1', 'ORDER TYPE', 'en', 1),
(14, 'Payment', '3', 'ORDER DETAIL', 3, 1, '1', 'ORDER TYPE', 'en', 1),
(15, 'Online Payment (PayPal)', '1', 'PAYMENT TYPE', 1, 1, '0', NULL, 'en', 1),
(16, 'Bank Transfer', '2', 'PAYMENT TYPE', 3, 1, '0', NULL, 'en', 1),
(17, 'Online Payment (iPay88)', '3', 'PAYMENT TYPE', 2, 0, NULL, NULL, 'en', 1),
(18, 'Ms.', '1', 'SALUTATION', 1, 1, '0', NULL, 'en', 1),
(19, 'Mr.', '2', 'SALUTATION', 2, 1, '0', NULL, 'en', 1),
(20, 'Mrs.', '3', 'SALUTATION', 3, 1, '0', NULL, 'en', 1),
(21, 'Male', '1', 'GENDER', 1, 1, '0', NULL, 'en', 1),
(22, 'Female', '2', 'GENDER', 2, 1, '0', NULL, 'en', 1),
(23, 'Chinese', '1', 'RACE', 1, 1, '0', NULL, 'en', 1),
(24, 'India', '2', 'RACE', 2, 1, '0', NULL, 'en', 1),
(25, 'Malay', '3', 'RACE', 3, 1, '0', NULL, 'en', 1),
(26, 'Single', '1', 'MARITAL STATUS', 1, 1, '0', NULL, 'en', 1),
(27, 'Married', '2', 'MARITAL STATUS', 2, 1, '0', NULL, 'en', 1),
(28, 'Checkout', '1', 'CARTMENUITEM', 1, 1, NULL, NULL, 'en', 1),
(29, 'Login', '2', 'CARTMENUITEM', 2, 1, NULL, NULL, 'en', 1),
(30, 'Delivery', '3', 'CARTMENUITEM', 3, 1, NULL, NULL, 'en', 1),
(31, 'Payment', '4', 'CARTMENUITEM', 4, 1, NULL, NULL, 'en', 1),
(32, 'Done', '5', 'CARTMENUITEM', 5, 1, NULL, NULL, 'en', 1),
(33, 'Grouping 1', '1', 'GROUPING', 1, 1, NULL, NULL, 'en', 1),
(34, 'Grouping 2', '2', 'GROUPING', 2, 1, NULL, NULL, 'en', 1),
(35, 'MASTHEAD', '1', 'CMS TYPE', 1, 0, NULL, NULL, 'en', 1),
(36, 'BOTTOM BANNER', '2', 'CMS TYPE', 2, 1, NULL, NULL, 'en', 1),
(37, 'page.aspx', 'page.aspx', 'PAGE TEMPLATE', 1, 1, NULL, NULL, 'en', 1),
(38, 'pagesub.aspx', 'pagesub.aspx', 'PAGE TEMPLATE', 2, 1, NULL, NULL, 'en', 1),
(39, 'contactus.aspx', 'contactus.aspx', 'PAGE TEMPLATE', 3, 1, NULL, NULL, 'en', 1),
(40, 'product.aspx', 'product.aspx', 'PAGE TEMPLATE', 4, 0, NULL, NULL, 'en', 1),
(41, 'Content', '1', 'CMS GROUP', 1, 1, NULL, NULL, 'en', 1),
(42, 'Slide Show', '2', 'CMS GROUP', 2, 1, NULL, NULL, 'en', 1),
(43, 'Shipping with Waiver (Weight)', '1', 'SHIPPING TYPE', 1, 1, NULL, NULL, 'en', 1),
(44, 'Shipping with Free After (Weight)', '2', 'SHIPPING TYPE', 2, 1, NULL, NULL, 'en', 1),
(45, 'Shipping with Free After (Amount)', '3', 'SHIPPING TYPE', 3, 1, NULL, NULL, 'en', 1),
(46, 'grouping3', '3', 'GROUPING', 3, 0, '0', NULL, 'en', 1),
(47, 'grouping4', '4', 'GROUPING', 4, 0, '0', NULL, 'en', 1),
(48, 'grouping5', '5', 'GROUPING', 5, 0, '0', NULL, 'en', 1),
(49, 'grouping6', '6', 'GROUPING', 6, 0, '0', NULL, 'en', 1),
(50, 'grouping7', '7', 'GROUPING', 7, 0, '0', NULL, 'en', 1),
(51, 'grouping8', '8', 'GROUPING', 8, 0, '0', NULL, 'en', 1),
(52, 'grouping9', '9', 'GROUPING', 9, 0, '0', NULL, 'en', 1),
(53, 'grouping10', '10', 'GROUPING', 10, 0, '0', NULL, 'en', 1),
(54, 'Same Window', '', 'TARGET', 1, 1, NULL, NULL, 'en', 1),
(55, 'New Window', '_blank', 'TARGET', 2, 1, NULL, NULL, 'en', 1),
(56, 'Top', '1', 'POSITION', 1, 0, NULL, NULL, 'en', 1),
(57, 'Bottom', '2', 'POSITION', 2, 1, NULL, NULL, 'en', 1),
(58, 'Same Window', '', 'SLIDE SHOW TARGET', 1, 1, NULL, NULL, 'en', 1),
(59, 'New Window', '_blank', 'SLIDE SHOW TARGET', 2, 1, NULL, NULL, 'en', 1),
(60, 'topMenuItem', 'topMenuItem', 'TOP MENU CSS', 1, 1, NULL, NULL, 'en', 1),
(62, 'Flat Shipping Rate', '4', 'SHIPPING TYPE', 4, 1, NULL, NULL, 'en', 1),
(63, 'Shipping with Waiver with Company', '5', 'SHIPPING TYPE', 5, 1, NULL, NULL, 'en', 1),
(64, 'topMisMenuItem', 'topMisMenuItem', 'TOP MENU CSS', 2, 1, NULL, NULL, 'en', 1),
(66, 'BANNER', '3', 'CMS TYPE', 3, 1, NULL, NULL, 'en', 1),
(78, 'gallery.aspx', 'gallery.aspx', 'PAGE TEMPLATE', 5, 1, NULL, NULL, 'en', 1),
(79, 'Bunker Enquires', '1', 'ENQUIRE SUBJECT', 1, 1, NULL, NULL, 'en', 1),
(80, 'Lubricants Enquires', '2', 'ENQUIRE SUBJECT', 2, 1, NULL, NULL, 'en', 1),
(81, 'Transportation Provider Enquires', '3', 'ENQUIRE SUBJECT', 3, 1, NULL, NULL, 'en', 1),
(82, 'Others', '4', 'ENQUIRE SUBJECT', 4, 1, NULL, NULL, 'en', 1),
(83, '+', '+', 'OPERATOR', 1, 1, '', '', 'en', 1),
(84, '-', '-', 'OPERATOR', 2, 1, '', '', 'en', 1),
(85, 'pagesub2.aspx', 'pagesub2.aspx', 'PAGE TEMPLATE', 2, 1, NULL, NULL, 'en', 1),
(86, 'Not Available', '1', 'GST TYPE', 1, 1, '1', 'GST SETTING', 'en', 1),
(87, 'GST Inclusive', '2', 'GST TYPE', 2, 1, '1', 'GST SETTING', 'en', 1),
(88, 'GST Applied', '3', 'GST TYPE', 3, 1, '1', 'GST SETTING', 'en', 1),
(89, 'All Applied', '1', 'GST PRODUCT TYPE', 1, 1, '1', 'GST SETTING', 'en', 1),
(90, 'Partially Applied', '2', 'GST PRODUCT TYPE', 2, 1, '1', 'GST SETTING', 'en', 1),
(91, 'pageblank.aspx', 'pageblank.aspx', 'PAGE TEMPLATE', 6, 1, NULL, NULL, 'en', 1),
(150, 'Text', '1', 'WATERMARK SETTINGS', 1, 1, NULL, NULL, 'en', 1),
(151, 'Image', '2', 'WATERMARK SETTINGS', 1, 1, NULL, NULL, 'en', 1),
(152, 'MOBILE BANNER', '10', 'CMS TYPE', 10, 1, NULL, NULL, 'en', 1),
(153, 'pagemb.aspx', 'pagemb.aspx', 'MOBILE PAGE TEMPLATE', 1, 1, NULL, NULL, 'en', 1),
(154, 'pagesubmb.aspx', 'pagesubmb.aspx', 'MOBILE PAGE TEMPLATE', 2, 1, NULL, NULL, 'en', 1),
(155, 'contactusmb.aspx', 'contactusmb.aspx', 'MOBILE PAGE TEMPLATE', 3, 1, NULL, NULL, 'en', 1),
(156, 'gallerymb.aspx', 'gallerymb.aspx', 'MOBILE PAGE TEMPLATE', 4, 1, NULL, NULL, 'en', 1),
(157, 'productmb.aspx', 'productmb.aspx', 'MOBILE PAGE TEMPLATE', 5, 0, NULL, NULL, 'en', 1),
(158, 'Alliance Bank Malaysia Berhad', 'bank_allianceImg.png', 'BANK SELECTION', 1, 1, NULL, NULL, 'en', 1),
(159, 'AmBank(M) Berhad', 'bank_amImg.png', 'BANK SELECTION', 2, 1, NULL, NULL, 'en', 1),
(160, 'CIMB Bank Berhad', 'bank_cimbImg.png', 'BANK SELECTION', 3, 1, NULL, NULL, 'en', 1),
(161, 'Hong Leong Bank Berhad', 'bank_hongleongImg.gif', 'BANK SELECTION', 4, 1, NULL, NULL, 'en', 1),
(162, 'Malayan Banking Berhad', 'bank_mayImg.png', 'BANK SELECTION', 5, 1, NULL, NULL, 'en', 1),
(163, 'OCBC Bank', 'bank_ocbcImg.jpg', 'BANK SELECTION', 6, 1, NULL, NULL, 'en', 1),
(164, 'POSB Bank', 'bank_posbImg.png', 'BANK SELECTION', 7, 1, NULL, NULL, 'en', 1),
(165, 'Public Bank Berhad', 'bank_publicImg.png', 'BANK SELECTION', 8, 1, NULL, NULL, 'en', 1),
(166, 'RHB Banking Group', 'bank_rhbImg.gif', 'BANK SELECTION', 9, 1, NULL, NULL, 'en', 1),
(167, 'DBS Bank', 'bank-dbs.gif', 'BANK SELECTION', 10, 1, NULL, NULL, 'en', 1),
(168, 'United Overseas Bank', 'bnn-uob.gif', 'BANK SELECTION', 11, 1, NULL, NULL, 'en', 1),
(169, 'topMenuItemMobile', 'topMenuItemMobile', 'MOBILE TOP MENU CSS', 1, 1, NULL, NULL, 'en', 1),
(170, 'merchantsmb.aspx', 'merchantsmb.aspx', 'MOBILE PAGE TEMPLATE', 6, 0, NULL, NULL, 'en', 1),
(171, 'Overwrite Existing Inventory', '1', 'INVENTORY UPDATE METHOD', 1, 1, '', '', 'en', 1),
(172, 'Add/Minus From Existing Inventory', '2', 'INVENTORY UPDATE METHOD', 2, 1, '', '', 'en', 1),
(173, 'faqmb.aspx', 'faqmb.aspx', 'MOBILE PAGE TEMPLATE', 7, 0, NULL, NULL, 'en', 1),
(174, 'Normal Sliding Effect', '1', 'SLIDER TYPE', 1, 1, NULL, NULL, 'en', 1),
(175, 'Fullscreen Sliding Effect', '2', 'SLIDER TYPE', 2, 1, NULL, NULL, 'en', 1),
(176, 'none', '0', 'TRANSITION TYPE FULLSCREEN', 1, 1, '2', 'SLIDER TYPE', 'en', 1),
(177, 'fade', '1', 'TRANSITION TYPE FULLSCREEN', 2, 1, '2', 'SLIDER TYPE', 'en', 1),
(178, 'slideTop', '2', 'TRANSITION TYPE FULLSCREEN', 3, 1, '2', 'SLIDER TYPE', 'en', 1),
(179, 'slideRight', '3', 'TRANSITION TYPE FULLSCREEN', 4, 1, '2', 'SLIDER TYPE', 'en', 1),
(180, 'slideBottom', '4', 'TRANSITION TYPE FULLSCREEN', 5, 1, '2', 'SLIDER TYPE', 'en', 1),
(181, 'slideLeft', '5', 'TRANSITION TYPE FULLSCREEN', 6, 1, '2', 'SLIDER TYPE', 'en', 1),
(182, 'carouselRight', '6', 'TRANSITION TYPE FULLSCREEN', 7, 1, '2', 'SLIDER TYPE', 'en', 1),
(183, 'carouselLeft', '7', 'TRANSITION TYPE FULLSCREEN', 8, 1, '2', 'SLIDER TYPE', 'en', 1),
(184, 'fade', 'fade', 'TRANSITION TYPE DEFAULT', 1, 1, '1', 'SLIDER TYPE', 'en', 1),
(185, 'Flip Horizontal', 'flipHorz', 'TRANSITION TYPE DEFAULT', 2, 1, '1', 'SLIDER TYPE', 'en', 1),
(186, 'Flip Vertical', 'flipVert', 'TRANSITION TYPE DEFAULT', 3, 1, '1', 'SLIDER TYPE', 'en', 1),
(187, 'Shuffle', 'shuffle', 'TRANSITION TYPE DEFAULT', 4, 1, '1', 'SLIDER TYPE', 'en', 1),
(188, 'Tile Slide', 'tileSlide', 'TRANSITION TYPE DEFAULT', 5, 1, '1', 'SLIDER TYPE', 'en', 1),
(189, 'Tile Blind', 'tileBlind', 'TRANSITION TYPE DEFAULT', 6, 1, '1', 'SLIDER TYPE', 'en', 1),
(190, 'Scroll Horizontal', 'scrollHorz', 'TRANSITION TYPE DEFAULT', 7, 1, '1', 'SLIDER TYPE', 'en', 1),
(191, 'Scroll Vertical', 'scrollVert', 'TRANSITION TYPE DEFAULT', 8, 1, '1', 'SLIDER TYPE', 'en', 1),
(192, 'Available', '1', 'TRAINING DATE STATUS', 1, 1, '', '', 'en', 1),
(193, 'Occupied', '2', 'TRAINING DATE STATUS', 2, 1, '', '', 'en', 1),
(194, 'trainingform.aspx', 'trainingform.aspx', 'PAGE TEMPLATE', 7, 0, '', '', 'en', 1),
(196, 'Mobile Sliding Effect', '3', 'SLIDER TYPE', 3, 1, NULL, NULL, 'en', 1),
(197, 'Swing Inside in Stairs', '1', 'TRANSITION TYPE MOBILE', 1, 1, '1', 'SLIDER TYPE', 'en', 1),
(198, 'Dodge Dance in Stairs', '2', 'TRANSITION TYPE MOBILE', 2, 1, '1', 'SLIDER TYPE', 'en', 1),
(199, 'Dodge Pet Inside in Stairs', '3', 'TRANSITION TYPE MOBILE', 3, 1, '1', 'SLIDER TYPE', 'en', 1),
(200, 'Dodge Inside in Stairs', '4', 'TRANSITION TYPE MOBILE', 4, 1, '1', 'SLIDER TYPE', 'en', 1),
(201, 'Flutter Inside in', '5', 'TRANSITION TYPE MOBILE', 5, 1, '1', 'SLIDER TYPE', 'en', 1),
(202, 'Rotate VDouble+ in', '6', 'TRANSITION TYPE MOBILE', 6, 1, '1', 'SLIDER TYPE', 'en', 1),
(203, 'Zoom VDouble+ in', '7', 'TRANSITION TYPE MOBILE', 7, 1, '1', 'SLIDER TYPE', 'en', 1),
(204, 'Collapse Stairs', '8', 'TRANSITION TYPE MOBILE', 8, 1, '1', 'SLIDER TYPE', 'en', 1),
(205, 'Clip & Chess in', '9', 'TRANSITION TYPE MOBILE', 9, 1, '1', 'SLIDER TYPE', 'en', 1),
(206, 'Expand Stairs', '10', 'TRANSITION TYPE MOBILE', 10, 1, '1', 'SLIDER TYPE', 'en', 1),
(207, 'Dominoes Stripe', '11', 'TRANSITION TYPE MOBILE', 11, 1, '1', 'SLIDER TYPE', 'en', 1),
(208, 'Wave out', '12', 'TRANSITION TYPE MOBILE', 12, 1, '1', 'SLIDER TYPE', 'en', 1),
(209, 'Wave in', '13', 'TRANSITION TYPE MOBILE', 13, 1, '1', 'SLIDER TYPE', 'en', 1),
(210, 'memberlogin.aspx', 'memberlogin.aspx', 'PAGE TEMPLATE', 8, 0, '', '', 'en', 1),
(211, 'Default', 'HIGH_ORDER ASC', 'EVENT SORT OPTION', 1, 1, '', '', 'en', 1),
(212, 'Latest on Top', 'HIGH_STARTDATE DESC, HIGH_SHOWTOP DESC, HIGH_CREATION DESC, HIGH_ORDER ASC', 'EVENT SORT OPTION', 2, 1, '', '', 'en', 1),
(213, 'Oldest on Top', 'HIGH_STARTDATE ASC, HIGH_SHOWTOP DESC, HIGH_CREATION DESC, HIGH_ORDER ASC', 'EVENT SORT OPTION', 3, 1, '', '', 'en', 1),
(214, 'Title (A - Z)', 'HIGH_TITLE ASC,HIGH_SHOWTOP DESC, HIGH_CREATION DESC, HIGH_ORDER ASC', 'EVENT SORT OPTION', 4, 1, '', '', 'en', 1),
(215, 'Title (Z - A)', 'HIGH_TITLE DESC, HIGH_SHOWTOP DESC, HIGH_CREATION DESC, HIGH_ORDER ASC', 'EVENT SORT OPTION', 5, 1, '', '', 'en', 1),
(216, 'Root', '0', 'TOP MENU ROOT', 1, 1, '', '', 'en', 1),
(217, 'Root2', '-1', 'TOP MENU ROOT', 2, 1, '', '', 'en', 1),
(218, 'Root3', '-2', 'TOP MENU ROOT', 3, 1, '', '', 'en', 1),
(219, 'Root4', '-3', 'TOP MENU ROOT', 4, 1, '', '', 'en', 1),
(220, 'Root5', '-4', 'TOP MENU ROOT', 5, 1, '', '', 'en', 1),
(221, 'Root6', '-5', 'TOP MENU ROOT', 6, 1, '', '', 'en', 1),
(222, 'Root7', '-6', 'TOP MENU ROOT', 7, 1, '', '', 'en', 1),
(223, 'Root8', '-7', 'TOP MENU ROOT', 8, 1, '', '', 'en', 1),
(224, 'Root9', '-8', 'TOP MENU ROOT', 9, 1, '', '', 'en', 1),
(225, 'Root10', '-9', 'TOP MENU ROOT', 10, 1, '', '', 'en', 1),
(226, 'Newest on Top', 'PROD_NEW DESC, PROD_SHOWTOP DESC, PROD_CREATION DESC', 'PRODUCT SORT OPTION', 1, 1, '', '', 'en', 1),
(227, 'Price (Low - High)', 'PROD_PRICE ASC, PROD_SHOWTOP DESC, PROD_CREATION DESC', 'PRODUCT SORT OPTION', 2, 1, '', '', 'en', 1),
(228, 'Price (High - Low)', 'PROD_PRICE DESC, PROD_SHOWTOP DESC, PROD_CREATION DESC', 'PRODUCT SORT OPTION', 3, 1, '', '', 'en', 1),
(229, 'Name (A - Z)', 'PROD_DNAME ASC, PROD_SHOWTOP DESC, PROD_CREATION DESC', 'PRODUCT SORT OPTION', 4, 1, '', '', 'en', 1),
(230, 'Name (Z - A)', 'PROD_DNAME DESC, PROD_SHOWTOP DESC, PROD_CREATION DESC', 'PRODUCT SORT OPTION', 5, 1, '', '', 'en', 1),
(231, 'Code (A - Z)', 'PROD_CODE ASC, PROD_SHOWTOP DESC, PROD_CREATION DESC', 'PRODUCT SORT OPTION', 6, 1, '', '', 'en', 1),
(232, 'Code (Z - A)', 'PROD_CODE DESC, PROD_SHOWTOP DESC, PROD_CREATION DESC', 'PRODUCT SORT OPTION', 7, 1, '', '', 'en', 1),
(233, 'Amount', 'Amount', 'ORDER UNIT', 1, 1, '', '', 'en', 1),
(234, 'Percentage(%)', 'Percentage(%)', 'ORDER UNIT', 2, 1, '', '', 'en', 1),
(235, 'Show on Individual Page', '1', 'Product Detail Display Type', 1, 1, '', '', 'en', 1),
(236, 'Show as Popup', '2', 'Product Detail Display Type', 2, 1, '', '', 'en', 1),
(237, 'Standard Chartered', 'Standard_Chartered.png', 'BANK SELECTION', 12, 1, '', '', 'en', 1),
(1001, 'pagesub2.aspx', 'pagesub2.aspx', 'PAGE TEMPLATE', 9, 0, NULL, NULL, 'en', 1),
(1002, 'loginmb.aspx', 'loginmb.aspx', 'MOBILE PAGE TEMPLATE', 8, 1, NULL, NULL, 'en', 1),
(1003, 'bookingmb.aspx', 'bookingmb.aspx', 'MOBILE PAGE TEMPLATE', 9, 1, NULL, NULL, 'en', 1),
(1004, 'historymb.aspx', 'historymb.aspx', 'MOBILE PAGE TEMPLATE', 10, 1, NULL, NULL, 'en', 1),
(1005, 'login', 'login', 'MOBILE TOP MENU CSS', 2, 1, NULL, NULL, 'en', 1),
(1006, 'booking', 'booking', 'MOBILE TOP MENU CSS', 3, 1, NULL, NULL, 'en', 1),
(1007, 'marked', 'marked', 'MOBILE TOP MENU CSS', 4, 1, NULL, NULL, 'en', 1),
(1008, 'chat', 'chat', 'MOBILE TOP MENU CSS', 5, 1, NULL, NULL, 'en', 1),
(1009, 'history', 'history', 'MOBILE TOP MENU CSS', 6, 1, NULL, NULL, 'en', 1),
(1010, 'memberprofilemb.aspx', 'memberprofilemb.aspx', 'MOBILE PAGE TEMPLATE', 11, 1, NULL, NULL, 'en', 1),
(1011, 'Paid', '0', 'RESERVATION STATUS', 1, 1, NULL, NULL, 'en', 1),
(1012, 'Assigned', '1', 'RESERVATION STATUS', 2, 1, NULL, NULL, 'en', 1),
(1013, 'Completed', '2', 'RESERVATION STATUS', 3, 1, NULL, NULL, 'en', 1),
(1014, 'Cancelled', '3', 'RESERVATION STATUS', 4, 1, NULL, NULL, 'en', 1),
(1015, 'Payment', '1', 'PAYMENT STATUS', 1, 1, NULL, NULL, 'en', 1),
(1016, 'Refund', '2', 'PAYMENT STATUS', 2, 1, NULL, NULL, 'en', 1),
(1017, 'Pending', '0', 'SERVICE STATUS', 1, 1, NULL, NULL, 'en', 1),
(1018, 'Completed', '1', 'SERVICE STATUS', 2, 1, NULL, NULL, 'en', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_login`
--

CREATE TABLE IF NOT EXISTS `tb_login` (
  `LOGIN_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `LOGIN_USERID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `LOGIN_USEREMAIL` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `LOGIN_USERTYPE` int(9) DEFAULT '0',
  `LOGIN_SETTINGS` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `LOGIN_CODE` varchar(50) CHARACTER SET utf8 NOT NULL,
  `LOGIN_CREATION` datetime NOT NULL,
  PRIMARY KEY (`LOGIN_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_login`
--

INSERT INTO `tb_login` (`LOGIN_ID`, `LOGIN_USERID`, `LOGIN_USEREMAIL`, `LOGIN_USERTYPE`, `LOGIN_SETTINGS`, `LOGIN_CODE`, `LOGIN_CREATION`) VALUES
(1, 11, 'acond@webteq.com.my', 1, '', '0ed52884141c801a953c468856ecf8bb', '2017-05-08 16:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `tb_masthead`
--

CREATE TABLE IF NOT EXISTS `tb_masthead` (
  `MASTHEAD_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `MASTHEAD_NAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `MASTHEAD_IMAGE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `MASTHEAD_ACTIVE` int(1) NOT NULL DEFAULT '1',
  `MASTHEAD_THUMB` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `MASTHEAD_SHORTDESC` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `MASTHEAD_TAGLINE1` text CHARACTER SET utf8,
  `MASTHEAD_TAGLINE2` text CHARACTER SET utf8,
  `GRP_ID` int(9) NOT NULL DEFAULT '0',
  `MASTHEAD_ORDER` int(9) NOT NULL DEFAULT '0',
  `MASTHEAD_CLICKABLE` int(1) NOT NULL DEFAULT '0',
  `MASTHEAD_TARGET` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `MASTHEAD_URL` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `MASTHEAD_CREATION` datetime DEFAULT NULL,
  `MASTHEAD_LASTUPDATED` datetime DEFAULT NULL,
  PRIMARY KEY (`MASTHEAD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_member`
--

CREATE TABLE IF NOT EXISTS `tb_member` (
  `mem_id` int(9) NOT NULL AUTO_INCREMENT,
  `mem_name` varchar(250) NOT NULL,
  `mem_email` varchar(50) NOT NULL,
  `mem_addr_unit` varchar(10) DEFAULT NULL,
  `mem_contact_tel` varchar(20) DEFAULT NULL,
  `mem_login_username` varchar(250) DEFAULT NULL,
  `mem_login_password` varchar(250) DEFAULT NULL,
  `mem_active` int(1) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `mem_createdby` int(9) NOT NULL DEFAULT '0',
  `mem_updatedby` int(9) NOT NULL DEFAULT '0',
  `mem_login_timestamp` datetime DEFAULT NULL,
  `mem_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mem_lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mem_tel_prefix` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`mem_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tb_member`
--

INSERT INTO `tb_member` (`mem_id`, `mem_name`, `mem_email`, `mem_addr_unit`, `mem_contact_tel`, `mem_login_username`, `mem_login_password`, `mem_active`, `user_id`, `mem_createdby`, `mem_updatedby`, `mem_login_timestamp`, `mem_creation`, `mem_lastupdated`, `mem_tel_prefix`) VALUES
(4, 'Cindy', 'cindy@mail.com', NULL, '7788995', 'cindy', '6UeCOdZiPwjE1NiO6bCmQQ==', 1, 0, 0, 0, NULL, '2017-06-23 09:00:54', '2017-07-20 09:47:30', '+6016'),
(5, 'Sharon', 'sharon@mail.com', NULL, '7148995', 'sharon', '4KJIT4K0s1I=', 1, 0, 0, 0, NULL, '2017-06-23 09:07:37', '2017-06-23 09:07:37', '+6012'),
(6, 'Vivian', 'vivian@mail.com', NULL, '7781195', 'vivian@mail.com', 'fidkqRe4JPE=', 1, 0, 0, 0, NULL, '2017-06-23 09:43:30', '2017-06-23 09:43:30', '+6013');

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE IF NOT EXISTS `tb_order` (
  `ORDER_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `MEM_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `MEM_CODE` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `MEM_NAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `MEM_COMPANYNAME` varchar(250) DEFAULT NULL,
  `MEM_EMAIL` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `MEM_TYPE` int(1) NOT NULL DEFAULT '1',
  `ORDER_NO` varchar(20) CHARACTER SET utf8 NOT NULL,
  `ORDER_CREATION` datetime NOT NULL,
  `ORDER_DUEDATE` datetime DEFAULT NULL,
  `ORDER_REMARKS` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `ORDER_READ` int(1) unsigned NOT NULL DEFAULT '0',
  `ORDER_TYPE` bigint(9) unsigned NOT NULL DEFAULT '1',
  `ORDER_STATUSID` bigint(9) NOT NULL DEFAULT '0',
  `ORDER_STATUS` varchar(20) NOT NULL DEFAULT '1',
  `ORDER_COMPANY` varchar(250) DEFAULT NULL,
  `ORDER_NAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `ORDER_ADD` varchar(500) CHARACTER SET utf8 NOT NULL,
  `ORDER_POSCODE` varchar(20) CHARACTER SET utf8 NOT NULL,
  `ORDER_CITY` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `ORDER_STATE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `ORDER_COUNTRY` varchar(10) DEFAULT NULL,
  `ORDER_CONTACTNO` varchar(20) CHARACTER SET utf8 NOT NULL,
  `ORDER_CONTACTNO2` varchar(20) DEFAULT NULL,
  `ORDER_FAX` varchar(20) DEFAULT NULL,
  `ORDER_EMAIL` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SHIPPING_CUSTCOMPANY` varchar(250) DEFAULT NULL,
  `SHIPPING_NAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SHIPPING_ADD` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SHIPPING_POSCODE` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `SHIPPING_CITY` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `SHIPPING_STATE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SHIPPING_COUNTRY` varchar(10) DEFAULT NULL,
  `SHIPPING_CONTACTNO` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `SHIPPING_CONTACTNO2` varchar(20) DEFAULT NULL,
  `SHIPPING_FAX` varchar(20) DEFAULT NULL,
  `SHIPPING_EMAIL` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SHIPPING_REMARKS` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `SHIPPING_DATE` datetime DEFAULT NULL,
  `SHIPPING_TRACKCODE` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `SHIPPING_COMPANY` int(9) NOT NULL DEFAULT '0',
  `SHIPPING_COMPANYNAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SHIPPING_COLLECTIONDATE` datetime DEFAULT NULL,
  `ORDER_BILLSHIPSAME` int(1) NOT NULL DEFAULT '0',
  `ORDER_SHIPPINGCHECK` int(1) NOT NULL DEFAULT '0',
  `ORDER_SHIPPING` decimal(15,2) NOT NULL DEFAULT '0.00',
  `ORDER_SHIPPINGCONVERTED` decimal(7,2) DEFAULT NULL,
  `ORDER_GST` int(9) DEFAULT '0',
  `ORDER_TOTALGST` decimal(7,2) DEFAULT '0.00',
  `ORDER_PAYMENT` bigint(9) NOT NULL DEFAULT '0',
  `ORDER_TOTALCONVERTED` decimal(7,2) DEFAULT NULL,
  `ORDER_CONVERTIONRATE` decimal(7,2) DEFAULT NULL,
  `ORDER_CONVERTIONUNIT` varchar(10) DEFAULT NULL,
  `ORDER_DONE` int(1) NOT NULL DEFAULT '0',
  `ORDER_CANCEL` int(1) NOT NULL DEFAULT '0',
  `ORDER_DLVFLAG` int(1) NOT NULL DEFAULT '0',
  `ORDER_DLVSERVICE` int(1) NOT NULL DEFAULT '0',
  `ORDER_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `ORDER_PICKUPPOINT` int(9) unsigned NOT NULL DEFAULT '0',
  `ORDER_PICKUPPOINTNAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `ORDER_INVENTORYFLAG` int(1) NOT NULL DEFAULT '0',
  `ORDER_UPDATEDBY` bigint(9) NOT NULL DEFAULT '0',
  `ORDER_LASTUPDATE` datetime DEFAULT NULL,
  `ORDER_DISCOUPONCODE` varchar(20) DEFAULT NULL,
  `ORDER_DISOPERATOR` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORDER_DISVALUE` decimal(7,2) DEFAULT '0.00',
  `ORDER_MSG_BUYER` text,
  `DELIVERY_DATE` datetime DEFAULT NULL,
  `DELIVERY_TIME` varchar(250) DEFAULT NULL,
  `DELIVERY_RECIPIENT` varchar(250) DEFAULT NULL,
  `DELIVERY_SENDER` varchar(250) DEFAULT NULL,
  `DELIVERY_MESSAGE` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ORDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_orderdetail`
--

CREATE TABLE IF NOT EXISTS `tb_orderdetail` (
  `ORDDETAIL_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `ORDER_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `ORDDETAIL_PRODID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `ORDDETAIL_PRODCODE` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `ORDDETAIL_PRODNAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `ORDDETAIL_PRODDNAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `ORDDETAIL_PRODSNAPSHOT` varchar(1000) DEFAULT NULL,
  `ORDDETAIL_PRODUOM` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `ORDDETAIL_PRODWEIGHT` int(9) unsigned DEFAULT NULL,
  `ORDDETAIL_PRODLENGTH` int(9) unsigned DEFAULT NULL,
  `ORDDETAIL_PRODWIDTH` int(9) unsigned DEFAULT NULL,
  `ORDDETAIL_PRODHEIGHT` int(9) unsigned DEFAULT NULL,
  `ORDDETAIL_PRODIMAGE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `ORDDETAIL_PRODPRICE` decimal(15,2) NOT NULL DEFAULT '0.00',
  `ORDDETAIL_PRODPRICING` decimal(15,2) NOT NULL DEFAULT '0.00',
  `ORDDETAIL_PRICINGID` int(9) DEFAULT '0',
  `ORDDETAIL_PRICINGNAME1` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `ORDDETAIL_PRICINGNAME2` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `ORDDETAIL_PRICINGEQUIVALENT` int(9) DEFAULT '1',
  `ORDDETAIL_PRODQTY` int(9) unsigned NOT NULL DEFAULT '0',
  `ORDDETAIL_TOTALUNITSELL` int(9) DEFAULT '0',
  `ORDDETAIL_PRODTOTAL` decimal(15,2) DEFAULT '0.00',
  `ORDDETAIL_PRODCAT1` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `ORDDETAIL_PRODCAT2` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ORDDETAIL_ID`),
  KEY `ORDDETAIL_PRODTOTAL` (`ORDDETAIL_PRODTOTAL`),
  KEY `ORDDETAIL_PRODQTY` (`ORDDETAIL_PRODQTY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_orderprodspec`
--

CREATE TABLE IF NOT EXISTS `tb_orderprodspec` (
  `ORDPRODSPEC_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `ORDDETAIL_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `SPEC_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `PROD_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `SPEC_CODE` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_CODE2` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_TYPE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_TYPEOPTION` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_GROUPID` int(9) NOT NULL DEFAULT '0',
  `SPEC_GROUP` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_VALUE` decimal(7,2) DEFAULT '0.00',
  `SPEC_VALUE2` decimal(7,2) NOT NULL DEFAULT '0.00',
  `SPEC_OPERATOR` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_ORDER` bigint(9) NOT NULL DEFAULT '0',
  `SPEC_UNIT` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_PERPAGE` int(1) NOT NULL DEFAULT '0',
  `SPEC_ACTIVE` int(1) NOT NULL DEFAULT '1',
  `SPEC_FILETITLE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_THUMBNAIL` varchar(250) DEFAULT NULL,
  `SPEC_FILE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_DESC` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`ORDPRODSPEC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_orderprodspecitem`
--

CREATE TABLE IF NOT EXISTS `tb_orderprodspecitem` (
  `ORDPRODSPECITEM_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `ORDDETAIL_ID` bigint(9) NOT NULL DEFAULT '0',
  `ORDPRODSPEC_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `SI_ID` bigint(9) NOT NULL DEFAULT '0',
  `SPEC_ID` bigint(9) NOT NULL DEFAULT '0',
  `PROD_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `SPEC_GROUPID` int(9) NOT NULL DEFAULT '0',
  `SI_NAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SI_OPERATOR` varchar(45) NOT NULL DEFAULT '+',
  `SI_PRICE` decimal(7,2) DEFAULT '0.00',
  `SI_ATTACHMENT` varchar(250) DEFAULT NULL,
  `SI_DATETITLE` varchar(250) DEFAULT NULL,
  `SI_DATE` datetime DEFAULT NULL,
  `SI_DATEACTIVE` int(1) unsigned NOT NULL DEFAULT '0',
  `SI_ORDER` bigint(9) NOT NULL DEFAULT '0',
  `SI_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`ORDPRODSPECITEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_orderremarks`
--

CREATE TABLE IF NOT EXISTS `tb_orderremarks` (
  `ORDREMARKS_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `ORDER_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `ORDREMARKS_REMARK` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ORDREMARKS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_orderstatus`
--

CREATE TABLE IF NOT EXISTS `tb_orderstatus` (
  `ORDSTATUS_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `ORDER_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `STATUS_ID` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `STATUS_DATE` datetime DEFAULT NULL,
  `STATUS_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`ORDSTATUS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_page`
--

CREATE TABLE IF NOT EXISTS `tb_page` (
  `PAGE_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PAGE_NAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `PAGE_DISPLAYNAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `PAGE_TITLE` varchar(250) CHARACTER SET utf8 NOT NULL,
  `PAGE_TITLEFURL` varchar(250) CHARACTER SET utf8 NOT NULL,
  `PAGE_CONTENT` text CHARACTER SET utf8,
  `PAGE_CONTENTPARENT` int(1) unsigned NOT NULL DEFAULT '0',
  `PAGE_SHOWPAGETITLE` int(1) unsigned NOT NULL DEFAULT '1',
  `PAGE_SHOWINMENU` int(1) unsigned NOT NULL DEFAULT '0',
  `PAGE_SHOWTOP` int(1) NOT NULL DEFAULT '0',
  `PAGE_SHOWBOTTOM` int(1) NOT NULL DEFAULT '0',
  `PAGE_SHOWINBREADCRUMB` int(1) DEFAULT NULL,
  `PAGE_USEREDITABLE` int(1) NOT NULL DEFAULT '1',
  `PAGE_ENABLELINK` int(1) NOT NULL DEFAULT '1',
  `PAGE_SHOWINSITEMAP` int(1) NOT NULL DEFAULT '1',
  `PAGE_CSSCLASS` varchar(250) DEFAULT NULL,
  `PAGE_ORDER` int(9) unsigned NOT NULL DEFAULT '0',
  `PAGE_LANG` varchar(10) NOT NULL DEFAULT 'en',
  `PAGE_TEMPLATE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PAGE_TEMPLATEPARENT` int(1) NOT NULL DEFAULT '0',
  `PAGE_PARENT` bigint(9) NOT NULL DEFAULT '0',
  `PAGE_KEYWORD` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `PAGE_DEFAULTKEYWORD` int(1) NOT NULL DEFAULT '1',
  `PAGE_DESC` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `PAGE_DEFAULTDESC` int(1) NOT NULL DEFAULT '1',
  `PAGE_REDIRECT` bigint(9) NOT NULL DEFAULT '0',
  `PAGE_REDIRECTLINK` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `PAGE_REDIRECTTARGET` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PAGE_SHOWMOBILEVIEW` int(1) DEFAULT '0',
  `PAGE_MOBILESHOWINMENU` int(1) unsigned NOT NULL DEFAULT '0',
  `PAGE_MOBILESHOWTOP` int(1) unsigned NOT NULL DEFAULT '0',
  `PAGE_MOBILESHOWBOTTOM` int(1) unsigned NOT NULL DEFAULT '0',
  `PAGE_MOBILECSSCLASS` varchar(250) DEFAULT NULL,
  `PAGE_MOBILETEMPLATE` varchar(250) DEFAULT NULL,
  `PAGE_MOBILESLIDESHOWGROUP` int(9) unsigned NOT NULL DEFAULT '0',
  `PAGE_MOBILECONTENT` text CHARACTER SET utf8,
  `PAGE_MOBILEREDIRECT` bigint(9) NOT NULL DEFAULT '0',
  `PAGE_MOBILEREDIRECTLINK` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `PAGE_MOBILEREDIRECTTARGET` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PAGE_AUTHORIZE` int(1) NOT NULL DEFAULT '0',
  `PAGE_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `PAGE_OTHER` int(1) NOT NULL DEFAULT '0',
  `PAGE_CREATEDBY` bigint(9) unsigned NOT NULL,
  `PAGE_CREATION` datetime NOT NULL,
  `PAGE_UPDATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `PAGE_LASTUPDATE` datetime DEFAULT NULL,
  `PAGE_TOPMENUCONTENT` text CHARACTER SET utf8,
  `GRP_ID` int(9) DEFAULT '0',
  PRIMARY KEY (`PAGE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tb_page`
--

INSERT INTO `tb_page` (`PAGE_ID`, `PAGE_NAME`, `PAGE_DISPLAYNAME`, `PAGE_TITLE`, `PAGE_TITLEFURL`, `PAGE_CONTENT`, `PAGE_CONTENTPARENT`, `PAGE_SHOWPAGETITLE`, `PAGE_SHOWINMENU`, `PAGE_SHOWTOP`, `PAGE_SHOWBOTTOM`, `PAGE_SHOWINBREADCRUMB`, `PAGE_USEREDITABLE`, `PAGE_ENABLELINK`, `PAGE_SHOWINSITEMAP`, `PAGE_CSSCLASS`, `PAGE_ORDER`, `PAGE_LANG`, `PAGE_TEMPLATE`, `PAGE_TEMPLATEPARENT`, `PAGE_PARENT`, `PAGE_KEYWORD`, `PAGE_DEFAULTKEYWORD`, `PAGE_DESC`, `PAGE_DEFAULTDESC`, `PAGE_REDIRECT`, `PAGE_REDIRECTLINK`, `PAGE_REDIRECTTARGET`, `PAGE_SHOWMOBILEVIEW`, `PAGE_MOBILESHOWINMENU`, `PAGE_MOBILESHOWTOP`, `PAGE_MOBILESHOWBOTTOM`, `PAGE_MOBILECSSCLASS`, `PAGE_MOBILETEMPLATE`, `PAGE_MOBILESLIDESHOWGROUP`, `PAGE_MOBILECONTENT`, `PAGE_MOBILEREDIRECT`, `PAGE_MOBILEREDIRECTLINK`, `PAGE_MOBILEREDIRECTTARGET`, `PAGE_AUTHORIZE`, `PAGE_ACTIVE`, `PAGE_OTHER`, `PAGE_CREATEDBY`, `PAGE_CREATION`, `PAGE_UPDATEDBY`, `PAGE_LASTUPDATE`, `PAGE_TOPMENUCONTENT`, `GRP_ID`) VALUES
(1, 'Other', 'Other', 'Other', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 1, '', 1, 'en', 'page.aspx', 1, 0, '', 1, '', 1, 0, '', '', 0, 0, 0, 0, NULL, NULL, 0, NULL, 0, NULL, NULL, 0, 0, 1, 1, '2017-05-08 14:54:40', 0, '2017-05-08 14:54:40', NULL, 0),
(2, 'Login', 'Login', 'Login', '', '', 0, 0, 1, 1, 1, 1, 1, 1, 1, '', 5, 'en', 'page.aspx', 1, 0, '', 1, '', 1, 0, '', '', 1, 1, 0, 1, 'login', 'loginmb.aspx', 0, '', 0, '', '', 0, 1, 0, 0, '2017-05-25 16:48:13', 0, '2017-05-25 16:48:13', NULL, 0),
(3, 'Book A Job', 'Book A Job', 'Book A Job', '', '', 0, 0, 1, 1, 1, 1, 1, 1, 1, '', 1, 'en', 'pagesub.aspx', 1, 0, '', 1, '', 1, 0, '', '', 1, 1, 0, 1, 'booking', 'bookingmb.aspx', 0, '', 0, '', '', 0, 1, 0, 0, '2017-05-25 16:48:13', 0, '2017-05-25 16:48:13', NULL, 0),
(4, 'Jobs', 'Jobs', 'Jobs', '', '', 0, 0, 1, 1, 1, 1, 1, 1, 1, '', 2, 'en', 'pagesub.aspx', 1, 0, '', 1, '', 1, 0, '', '', 1, 1, 0, 1, 'history', 'historymb.aspx', 0, '', 0, '', '', 0, 1, 0, 0, '2017-05-25 16:48:13', 0, '2017-05-25 16:48:13', NULL, 0),
(5, 'Marked', 'Marked', 'Marked', '', '', 0, 0, 1, 1, 1, 1, 1, 1, 1, '', 3, 'en', 'pagesub.aspx', 1, 0, '', 1, '', 1, 0, '', '', 1, 1, 0, 1, 'marked', 'pagesubmb.aspx', 0, '', 0, '', '', 0, 1, 0, 0, '2017-05-25 16:48:13', 0, '2017-05-25 16:48:13', NULL, 0),
(6, 'Chat', 'Chat', 'Chat', '', '', 0, 0, 1, 1, 1, 1, 1, 1, 1, '', 4, 'en', 'pagesub.aspx', 1, 0, '', 1, '', 1, 0, '', '', 1, 1, 0, 1, 'chat', 'pagesubmb.aspx', 0, '', 0, '', '', 0, 1, 0, 0, '2017-05-25 16:48:13', 0, '2017-05-25 16:48:13', NULL, 0),
(7, 'Profile', 'Profile', 'Profile', '', '', 0, 0, 1, 1, 1, 1, 1, 1, 1, '', 5, 'en', 'memberprofilemb.aspx', 1, 0, '', 1, '', 1, 0, '', '', 1, 1, 0, 1, 'login', 'memberprofilemb.aspx', 0, '', 0, '', '', 0, 1, 0, 0, '2017-05-25 16:48:13', 0, '2017-05-25 16:48:13', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pageauthorization`
--

CREATE TABLE IF NOT EXISTS `tb_pageauthorization` (
  `PGAUTH_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PGAUTH_GROUP` varchar(50) CHARACTER SET utf8 NOT NULL,
  `PGAUTH_UNILOGIN` varchar(250) DEFAULT NULL,
  `PGAUTH_UNIPWD` varchar(250) DEFAULT NULL,
  `PGAUTH_INDCOMPANYNAME` varchar(250) DEFAULT NULL,
  `PGAUTH_INDCONTACTPERSON` varchar(250) DEFAULT NULL,
  `PGAUTH_INDEMAIL` varchar(250) DEFAULT NULL,
  `PGAUTH_INDCONTACTNO` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `PGAUTH_INDREMARKS` varchar(1000) DEFAULT NULL,
  `PGAUTH_INDLOGIN` varchar(250) DEFAULT NULL,
  `PGAUTH_INDPWD` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PGAUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pagebackup`
--

CREATE TABLE IF NOT EXISTS `tb_pagebackup` (
  `PAGEBACKUP_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PAGE_ID` int(9) NOT NULL DEFAULT '0',
  `PAGEBACKUP_CONTENT` text,
  `PAGEBACKUP_TYPE` int(1) NOT NULL DEFAULT '0',
  `PAGEBACKUP_DATE` datetime NOT NULL,
  `PAGEBACKUP_BY` bigint(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PAGEBACKUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pagecms`
--

CREATE TABLE IF NOT EXISTS `tb_pagecms` (
  `PAGECMS_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PAGE_ID` int(9) NOT NULL DEFAULT '0',
  `CUSTOM_ID` bigint(9) NOT NULL DEFAULT '0',
  `CUSTOM_ID_2` bigint(9) NOT NULL DEFAULT '0',
  `BLOCK_ID` int(9) NOT NULL DEFAULT '0',
  `CMS_ID` bigint(9) unsigned NOT NULL,
  `PAGECMS_TYPE` bigint(9) unsigned NOT NULL DEFAULT '0',
  `PAGECMS_GROUP` int(9) unsigned NOT NULL DEFAULT '1',
  `PAGECMS_LANG` varchar(10) NOT NULL DEFAULT 'en',
  `PAGECMS_ADMINVIEW` int(1) NOT NULL DEFAULT '0',
  `PAGECMS_CREATION` datetime NOT NULL,
  `PAGECMS_CREATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `PAGECMS_LASTUPDATE` datetime DEFAULT NULL,
  `PAGECMS_UPDATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`PAGECMS_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pagecorrespond`
--

CREATE TABLE IF NOT EXISTS `tb_pagecorrespond` (
  `PC_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PAGE_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `PC_PAGEID` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`PC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_participant`
--

CREATE TABLE IF NOT EXISTS `tb_participant` (
  `PAR_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PAR_FIRSTNAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PAR_LASTNAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PAR_EMAIL` varchar(250) CHARACTER SET utf8 NOT NULL,
  `PAR_CONTACTNO` varchar(50) CHARACTER SET utf8 NOT NULL,
  `PAR_NATIONALITY` int(1) unsigned NOT NULL DEFAULT '0',
  `PAR_GENDER` int(1) NOT NULL DEFAULT '0',
  `PAR_DOB` datetime DEFAULT NULL,
  `PAR_EDULEVEL` int(9) unsigned NOT NULL DEFAULT '0',
  `PAR_ENGSPEAKLEVEL` int(9) unsigned NOT NULL DEFAULT '0',
  `PAR_ENGWRITELEVEL` int(9) NOT NULL DEFAULT '0',
  `PAR_SUITABLEREASON` text,
  `PAR_RESUMENAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PAR_RESUME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PAR_LEARNABOUT` int(9) unsigned DEFAULT NULL,
  `PAR_PROGGRP` int(9) unsigned DEFAULT NULL,
  `PAR_PROGRAM` int(9) unsigned NOT NULL DEFAULT '0',
  `PAR_PROGRAMDATE` int(9) NOT NULL DEFAULT '0',
  `PAR_RECENTAPPLIED` int(9) unsigned DEFAULT NULL,
  `PAR_SUBSCRIBE` int(1) NOT NULL DEFAULT '0',
  `PAR_REGISTEREDDATE` datetime DEFAULT NULL,
  `PAR_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `PARTICIPANT_STATUS` varchar(20) NOT NULL DEFAULT '1',
  PRIMARY KEY (`PAR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_payment`
--

CREATE TABLE IF NOT EXISTS `tb_payment` (
  `PAYMENT_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `ORDER_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `PAYMENT_GATEWAY` bigint(9) NOT NULL DEFAULT '0',
  `PAYMENT_REMARKS` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `PAYMENT_DATE` datetime DEFAULT NULL,
  `PAYMENT_TOTAL` decimal(15,2) DEFAULT '0.00',
  `PAYMENT_BANK` int(9) NOT NULL DEFAULT '0',
  `PAYMENT_BANKNAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PAYMENT_BANKACCOUNT` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PAYMENT_TRANSID` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `PAYMENT_BANKDATE` datetime DEFAULT NULL,
  `PAYMENT_EMAIL` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PAYMENT_REJECT` int(1) NOT NULL DEFAULT '0',
  `PAYMENT_REJECTREMARKS` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PAYMENT_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `PAYMENT_STATUS` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`PAYMENT_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_payment`
--

INSERT INTO `tb_payment` (`PAYMENT_ID`, `ORDER_ID`, `PAYMENT_GATEWAY`, `PAYMENT_REMARKS`, `PAYMENT_DATE`, `PAYMENT_TOTAL`, `PAYMENT_BANK`, `PAYMENT_BANKNAME`, `PAYMENT_BANKACCOUNT`, `PAYMENT_TRANSID`, `PAYMENT_BANKDATE`, `PAYMENT_EMAIL`, `PAYMENT_REJECT`, `PAYMENT_REJECTREMARKS`, `PAYMENT_ACTIVE`, `PAYMENT_STATUS`) VALUES
(1, 2, 1, NULL, '2016-06-22 00:00:00', '255.00', 0, NULL, NULL, '0021YGHKJS', NULL, NULL, 0, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pricing`
--

CREATE TABLE IF NOT EXISTS `tb_pricing` (
  `PRICING_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PROD_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `PRICING_NAME1` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PRICING_NAME2` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PRICING_PRICE` decimal(7,2) NOT NULL DEFAULT '0.00',
  `PRICING_EQUIVALENT` int(9) NOT NULL DEFAULT '1',
  `PRICING_ORDER` int(9) NOT NULL DEFAULT '0',
  `PRICING_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `PRICING_CREATION` datetime NOT NULL,
  `PRICING_CREATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `PRICING_LASTUPDATE` datetime DEFAULT NULL,
  `PRICING_UPDATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`PRICING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_prodaddon`
--

CREATE TABLE IF NOT EXISTS `tb_prodaddon` (
  `addon_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prod_id` int(10) unsigned NOT NULL,
  `prod_addon_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`addon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_prodarticle`
--

CREATE TABLE IF NOT EXISTS `tb_prodarticle` (
  `PRODART_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PROD_ID` bigint(9) unsigned NOT NULL,
  `PRODART_TITLE` varchar(250) CHARACTER SET utf8 NOT NULL,
  `PRODART_FILE` varchar(250) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`PRODART_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_proddescription`
--

CREATE TABLE IF NOT EXISTS `tb_proddescription` (
  `PRODDESC_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PROD_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `PRODDESC_TITLE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PRODDESC_TITLE_ZH` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PRODDESC_TITLE_JP` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PRODDESC_TITLE_MS` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PRODDESC_DESC` text CHARACTER SET utf8,
  `PRODDESC_DESC_ZH` text CHARACTER SET utf8,
  `PRODDESC_DESC_JP` text CHARACTER SET utf8,
  `PRODDESC_DESC_MS` text CHARACTER SET utf8,
  PRIMARY KEY (`PRODDESC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_prodgallery`
--

CREATE TABLE IF NOT EXISTS `tb_prodgallery` (
  `PRODGALL_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PROD_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `PRODGALL_TITLE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PRODGALL_IMAGE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PRODGALL_DESC` varchar(250) DEFAULT NULL,
  `PRODGALL_DEFAULT` int(1) unsigned NOT NULL DEFAULT '0',
  `PRODGALL_ORDER` int(9) NOT NULL DEFAULT '0',
  `PRODGALL_CREATION` datetime NOT NULL,
  `PRODGALL_CREATEDBY` int(9) unsigned NOT NULL DEFAULT '0',
  `PRODGALL_LASTUPDATE` datetime DEFAULT NULL,
  `PRODGALL_UPDATEDBY` int(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`PRODGALL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_prodgroup`
--

CREATE TABLE IF NOT EXISTS `tb_prodgroup` (
  `PRODGRP_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PROD_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `GRP_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`PRODGRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_prodprice`
--

CREATE TABLE IF NOT EXISTS `tb_prodprice` (
  `PRODPRC_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PROD_ID` bigint(9) unsigned NOT NULL,
  `PRODPRC_COST` decimal(15,2) DEFAULT NULL,
  `PRODPRC_PRICE` decimal(15,2) DEFAULT NULL,
  `PRODPRC_PROMPRICE` decimal(15,2) DEFAULT NULL,
  `PRODPRC_PROMSTARTDATE` datetime DEFAULT NULL,
  `PRODPRC_PROMENDDATE` datetime DEFAULT NULL,
  PRIMARY KEY (`PRODPRC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_prodpromo`
--

CREATE TABLE IF NOT EXISTS `tb_prodpromo` (
  `promo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prod_id` int(10) unsigned NOT NULL,
  `promo_price` decimal(10,0) NOT NULL,
  `promo_name` varchar(250) DEFAULT NULL,
  `promo_from` datetime NOT NULL,
  `promo_to` datetime NOT NULL,
  `promo_active` int(1) unsigned NOT NULL DEFAULT '1',
  `promo_order` int(9) unsigned NOT NULL DEFAULT '99',
  `promo_createdat` datetime NOT NULL,
  `promo_createdby` int(10) unsigned NOT NULL,
  `promo_updatedat` datetime DEFAULT NULL,
  `promo_updatedby` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`promo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_prodspec`
--

CREATE TABLE IF NOT EXISTS `tb_prodspec` (
  `SPEC_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PROD_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `SPEC_CODE` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_CODE2` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_TYPE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_TYPEOPTION` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_GROUPID` int(9) NOT NULL DEFAULT '0',
  `SPEC_GROUP` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_VALUE` decimal(7,2) DEFAULT '0.00',
  `SPEC_VALUE2` decimal(7,2) NOT NULL DEFAULT '0.00',
  `SPEC_OPERATOR` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_ORDER` bigint(9) NOT NULL DEFAULT '0',
  `SPEC_UNIT` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_PERPAGE` int(1) NOT NULL DEFAULT '0',
  `SPEC_ACTIVE` int(1) NOT NULL DEFAULT '1',
  `SPEC_FILETITLE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_THUMBNAIL` varchar(250) DEFAULT NULL,
  `SPEC_FILE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SPEC_DESC` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`SPEC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_prodspecitem`
--

CREATE TABLE IF NOT EXISTS `tb_prodspecitem` (
  `SI_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `SPEC_ID` bigint(9) NOT NULL DEFAULT '0',
  `PROD_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `SPEC_GROUPID` int(9) NOT NULL DEFAULT '0',
  `SI_NAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SI_OPERATOR` varchar(45) NOT NULL DEFAULT '+',
  `SI_PRICE` decimal(7,2) DEFAULT '0.00',
  `SI_ATTACHMENT` varchar(250) DEFAULT NULL,
  `SI_THUMBNAIL` varchar(250) DEFAULT NULL,
  `SI_DATETITLE` varchar(250) DEFAULT NULL,
  `SI_DATEACTIVE` int(1) unsigned NOT NULL DEFAULT '0',
  `SI_ORDER` bigint(9) NOT NULL DEFAULT '0',
  `SI_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`SI_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_prodsupplier`
--

CREATE TABLE IF NOT EXISTS `tb_prodsupplier` (
  `PRODSUPPLIER_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PROD_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `SUPPLIER_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`PRODSUPPLIER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_product`
--

CREATE TABLE IF NOT EXISTS `tb_product` (
  `PROD_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PROD_CODE` varchar(30) CHARACTER SET utf8 NOT NULL,
  `PROD_NAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `PROD_NAME_JP` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PROD_NAME_MS` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PROD_NAME_ZH` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PROD_DNAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `PROD_DNAME_JP` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PROD_DNAME_MS` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PROD_DNAME_ZH` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PROD_PAGETITLEFURL` varchar(250) CHARACTER SET utf8 NOT NULL,
  `PROD_DESC` text CHARACTER SET utf8,
  `PROD_DESC_JP` text CHARACTER SET utf8,
  `PROD_DESC_MS` text CHARACTER SET utf8,
  `PROD_DESC_ZH` text CHARACTER SET utf8,
  `PROD_SNAPSHOT` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `PROD_SNAPSHOT_JP` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `PROD_SNAPSHOT_MS` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `PROD_SNAPSHOT_ZH` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `PROD_REMARKS` text CHARACTER SET utf8,
  `PROD_REMARKS_JP` text CHARACTER SET utf8,
  `PROD_REMARKS_MS` text CHARACTER SET utf8,
  `PROD_REMARKS_ZH` text CHARACTER SET utf8,
  `PROD_UOM` varchar(10) DEFAULT NULL,
  `PROD_WEIGHT` int(9) unsigned DEFAULT '0',
  `PROD_LENGTH` int(9) unsigned DEFAULT '0',
  `PROD_WIDTH` int(9) unsigned DEFAULT '0',
  `PROD_HEIGHT` int(9) unsigned DEFAULT '0',
  `PROD_NEW` int(1) unsigned NOT NULL DEFAULT '0',
  `PROD_SHOWTOP` int(1) unsigned NOT NULL DEFAULT '0',
  `PROD_SOLD` int(1) NOT NULL DEFAULT '0',
  `PROD_RENT` int(1) NOT NULL DEFAULT '0',
  `PROD_OUTOFSTOCK` int(1) NOT NULL DEFAULT '0',
  `PROD_PREORDER` int(1) NOT NULL DEFAULT '0',
  `PROD_BEST` int(1) unsigned NOT NULL DEFAULT '0',
  `PROD_WITH_GST` int(1) unsigned NOT NULL DEFAULT '0',
  `PROD_ACTIVE` int(1) unsigned NOT NULL DEFAULT '0',
  `PROD_IMAGE` varchar(250) DEFAULT NULL,
  `PROD_NOV` bigint(9) unsigned NOT NULL DEFAULT '0',
  `PROD_NOP` bigint(9) unsigned NOT NULL DEFAULT '0',
  `PROD_SUPPLIER` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `PROD_INVALLOWNEGATIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `PROD_CREATION` datetime NOT NULL,
  `PROD_CREATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `PROD_LASTUPDATE` datetime DEFAULT NULL,
  `PROD_UPDATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `PROD_STOCKSTATUS` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `PROD_CONTAINLIQUID` int(1) unsigned NOT NULL DEFAULT '0',
  `PROD_SALES` int(1) unsigned NOT NULL DEFAULT '1',
  `PROD_ADDON` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`PROD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_proggroup`
--

CREATE TABLE IF NOT EXISTS `tb_proggroup` (
  `PROGGRP_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PROG_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `GRP_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`PROGGRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_property`
--

CREATE TABLE IF NOT EXISTS `tb_property` (
  `property_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `property_area` varchar(45) NOT NULL DEFAULT '',
  `property_unitno` varchar(20) NOT NULL DEFAULT '',
  `property_type` varchar(45) NOT NULL DEFAULT '',
  `property_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `property_lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`property_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tb_property`
--

INSERT INTO `tb_property` (`property_id`, `member_id`, `property_area`, `property_unitno`, `property_type`, `property_creation`, `property_lastupdated`) VALUES
(3, 4, 'Desa Tebrau', 'B-01', 'Studio', '2017-06-23 09:08:04', '2017-06-23 09:08:04'),
(4, 4, 'Austin', '23', 'Studio', '2017-06-23 09:08:15', '2017-06-23 09:08:15'),
(5, 5, 'Tmn Daya', '501', 'Apartment', '2017-06-23 09:11:00', '2017-06-23 09:11:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_receive`
--

CREATE TABLE IF NOT EXISTS `tb_receive` (
  `RE_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `RE_CODE` varchar(20) CHARACTER SET utf8 NOT NULL,
  `RE_DATE` datetime DEFAULT NULL,
  `VENDOR_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `VENDOR_NAME` varchar(250) DEFAULT NULL,
  `RE_DO` varchar(250) DEFAULT NULL,
  `RE_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `RE_DISCOUNT` decimal(7,2) NOT NULL DEFAULT '0.00',
  `RE_TOTAL` decimal(7,2) NOT NULL DEFAULT '0.00',
  `RE_CREATION` datetime NOT NULL,
  `RE_CREATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `RE_LASTUPDATE` datetime DEFAULT NULL,
  `RE_UPDATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`RE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_receiveitem`
--

CREATE TABLE IF NOT EXISTS `tb_receiveitem` (
  `RI_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RI_PRODID` int(9) NOT NULL DEFAULT '0',
  `RI_PRODCODE` varchar(20) CHARACTER SET utf8 NOT NULL,
  `RI_PRODNAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `RI_PRODQTY` int(9) unsigned NOT NULL DEFAULT '0',
  `RI_PRODPRICE` decimal(7,2) NOT NULL DEFAULT '0.00',
  `RI_PRODDISCOUNT` decimal(7,2) NOT NULL DEFAULT '0.00',
  `RI_PRODTOTAL` decimal(7,2) NOT NULL DEFAULT '0.00',
  `RE_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `RI_CREATION` datetime NOT NULL,
  `RI_CREATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `RI_LASTUPDATE` datetime DEFAULT NULL,
  `RI_UPDATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`RI_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_relateddeal`
--

CREATE TABLE IF NOT EXISTS `tb_relateddeal` (
  `RELDEAL_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `DEAL_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `RELDEAL_RELDEALID` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`RELDEAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_relatedprodlist`
--

CREATE TABLE IF NOT EXISTS `tb_relatedprodlist` (
  `RELPRODLIST_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PROD_ID` bigint(9) unsigned NOT NULL DEFAULT '0',
  `RELPRODLIST_RELPRODID` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`RELPRODLIST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_reservation`
--

CREATE TABLE IF NOT EXISTS `tb_reservation` (
  `res_id` int(9) NOT NULL AUTO_INCREMENT,
  `res_code` varchar(20) DEFAULT NULL,
  `res_status` int(1) NOT NULL DEFAULT '1',
  `res_service_datetime_from` datetime NOT NULL,
  `res_service_datetime_to` datetime NOT NULL,
  `res_service_duration_hour` int(11) NOT NULL DEFAULT '1',
  `res_service_item_qty` int(3) NOT NULL DEFAULT '1',
  `res_service_addr_unit` varchar(20) NOT NULL,
  `res_service_contact_tel` varchar(20) NOT NULL,
  `res_service_charge_amount` decimal(10,0) NOT NULL,
  `res_service_remark` varchar(250) DEFAULT NULL,
  `mem_id` int(9) NOT NULL,
  `mem_name` varchar(250) NOT NULL DEFAULT '',
  `mem_email` varchar(50) NOT NULL DEFAULT '',
  `mem_contact_tel` varchar(20) NOT NULL DEFAULT '',
  `employee_id` int(9) NOT NULL,
  `res_createdby` int(9) NOT NULL DEFAULT '0',
  `res_updatedby` int(9) NOT NULL DEFAULT '0',
  `res_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `res_lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `res_service_area` varchar(45) NOT NULL DEFAULT '',
  `res_service_team` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`res_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tb_reservation`
--

INSERT INTO `tb_reservation` (`res_id`, `res_code`, `res_status`, `res_service_datetime_from`, `res_service_datetime_to`, `res_service_duration_hour`, `res_service_item_qty`, `res_service_addr_unit`, `res_service_contact_tel`, `res_service_charge_amount`, `res_service_remark`, `mem_id`, `mem_name`, `mem_email`, `mem_contact_tel`, `employee_id`, `res_createdby`, `res_updatedby`, `res_creation`, `res_lastupdated`, `res_service_area`, `res_service_team`) VALUES
(1, NULL, 1, '2017-05-31 14:00:00', '2017-05-31 18:00:00', 4, 2, 'B-01', '016 7788995', '160', '', 1, '', '', '', 2, 0, 0, '2017-05-26 03:20:23', '2017-05-26 03:20:23', '', ''),
(2, 'ORD201705260002', 1, '2017-05-31 08:00:00', '2017-05-31 12:00:00', 4, 2, 'B-01', '016 7788995', '0', 'Please be careful', 1, '', '', '', 3, 0, 0, '2017-05-26 07:53:36', '2017-06-23 09:11:34', '', 'Group1'),
(3, 'ORD201705260003', 1, '2017-06-09 08:00:00', '2017-06-09 16:00:00', 6, 3, 'B-01', '016 7788995', '0', '', 1, '', '', '', 2, 0, 0, '2017-05-26 08:58:56', '2017-06-23 09:11:44', '', 'Group1'),
(4, 'ORD201707200004', 1, '2017-07-27 08:00:00', '2017-07-27 00:00:00', 4, 2, '123', '7788995', '0', '', 4, '', '', '', 8, 0, 0, '2017-07-20 10:53:55', '2017-07-20 10:53:55', '', ''),
(5, 'ORD201707220005', 1, '2017-09-14 08:00:00', '2017-09-14 00:00:00', 8, 4, '4', '7788995', '0', '', 4, '', '', '', 9, 0, 0, '2017-07-22 05:36:55', '2017-07-22 05:36:55', '', ''),
(6, 'ORD201707220006', 1, '2017-07-27 08:00:00', '2017-07-27 00:00:00', 4, 2, 'A-06-01', '7788995', '0', '', 4, '', '', '', 7, 0, 0, '2017-07-22 05:49:07', '2017-07-22 05:49:08', '', ''),
(7, 'ORD201707220007', 1, '2017-07-27 08:00:00', '2017-07-27 00:00:00', 2, 1, 'A-06-01', '7788995', '0', '', 4, '', '', '', 7, 0, 0, '2017-07-22 05:51:21', '2017-07-22 05:51:21', '', ''),
(8, 'ORD201707240008', 1, '2017-07-27 08:00:00', '2017-07-27 00:00:00', 4, 2, 'A-24-11', '7788995', '0', '', 4, '', '', '', 6, 0, 0, '2017-07-23 16:24:48', '2017-07-23 16:24:48', '', ''),
(9, 'ORD201707270009', 1, '2017-07-27 08:00:00', '2017-07-27 00:00:00', 8, 4, 'A-24-11', '7788995', '0', '', 4, '', '', '', 9, 0, 0, '2017-07-27 06:50:52', '2017-07-27 06:50:53', '', ''),
(10, 'ORD201708080010', 1, '2017-08-09 08:00:00', '2017-08-09 00:00:00', 8, 4, 'A-06-01', '7788995', '0', '', 4, '', '', '', 8, 0, 0, '2017-08-08 12:25:55', '2017-08-08 12:25:56', '', ''),
(11, 'ORD201709180011', 1, '2017-09-19 10:00:00', '2017-09-19 00:00:00', 8, 4, 'A-24-11', '7788995', '0', '', 4, '', '', '', 8, 0, 0, '2017-09-18 07:36:17', '2017-09-18 07:36:17', '', ''),
(12, 'ORD201709210012', 1, '2017-09-21 10:00:00', '2017-09-21 00:00:00', 2, 1, 'Unit 01', '7788995', '0', '', 4, '', '', '', 8, 0, 0, '2017-09-20 16:49:22', '2017-09-20 16:49:22', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_reservation_status`
--

CREATE TABLE IF NOT EXISTS `tb_reservation_status` (
  `sta_id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `res_id` int(9) unsigned NOT NULL DEFAULT '0',
  `sta_status` int(1) unsigned NOT NULL DEFAULT '0',
  `sta_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tb_reservation_status`
--

INSERT INTO `tb_reservation_status` (`sta_id`, `res_id`, `sta_status`, `sta_datetime`) VALUES
(1, 2, 1, '2017-06-22 10:31:23'),
(2, 2, 1, '2017-06-22 10:31:23'),
(3, 2, 2, '2017-06-22 10:31:23'),
(4, 2, 1, '2017-06-23 09:11:34'),
(5, 3, 1, '2017-06-23 09:11:44'),
(6, 4, 0, '2017-07-20 10:53:55'),
(7, 5, 0, '2017-07-22 05:36:55'),
(8, 6, 0, '2017-07-22 05:49:08'),
(9, 7, 0, '2017-07-22 05:51:21'),
(10, 8, 0, '2017-07-23 16:24:48'),
(11, 9, 0, '2017-07-27 06:50:53'),
(12, 10, 0, '2017-08-08 12:25:55'),
(13, 11, 0, '2017-09-18 07:36:17'),
(14, 12, 0, '2017-09-20 16:49:22');

-- --------------------------------------------------------

--
-- Table structure for table `tb_reservation_time`
--

CREATE TABLE IF NOT EXISTS `tb_reservation_time` (
  `time_id` int(9) NOT NULL AUTO_INCREMENT,
  `res_id` int(9) NOT NULL,
  `entity_id` int(9) NOT NULL,
  `employee_id` int(9) NOT NULL,
  `time_from` datetime NOT NULL,
  `time_to` datetime NOT NULL,
  `time_createdby` int(9) NOT NULL DEFAULT '0',
  `time_updatedby` int(9) NOT NULL DEFAULT '0',
  `time_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `time_lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`time_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_seokeyword`
--

CREATE TABLE IF NOT EXISTS `tb_seokeyword` (
  `seo_id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_group` varchar(250) NOT NULL,
  `seo_keyword` varchar(250) NOT NULL,
  `seo_important` int(11) NOT NULL DEFAULT '0',
  `seo_order` int(11) NOT NULL DEFAULT '999',
  `seo_creation` datetime NOT NULL,
  `seo_createdby` int(11) NOT NULL,
  `seo_lastupdate` datetime DEFAULT NULL,
  `seo_updatedby` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`seo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tb_seokeyword`
--

INSERT INTO `tb_seokeyword` (`seo_id`, `seo_group`, `seo_keyword`, `seo_important`, `seo_order`, `seo_creation`, `seo_createdby`, `seo_lastupdate`, `seo_updatedby`) VALUES
(1, 'Company Name', '', 0, 1, '2017-01-12 18:42:13', 0, NULL, 0),
(2, 'Company Short Name', '', 0, 1, '2017-01-12 18:42:13', 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_sequence`
--

CREATE TABLE IF NOT EXISTS `tb_sequence` (
  `SEQ_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `SEQ_DATEGET` datetime NOT NULL,
  PRIMARY KEY (`SEQ_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_service_status`
--

CREATE TABLE IF NOT EXISTS `tb_service_status` (
  `sta_id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `res_id` int(9) unsigned NOT NULL DEFAULT '0',
  `sta_status` int(1) unsigned NOT NULL DEFAULT '0',
  `sta_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_service_status`
--

INSERT INTO `tb_service_status` (`sta_id`, `res_id`, `sta_status`, `sta_datetime`) VALUES
(1, 2, 1, '2017-06-22 10:33:39');

-- --------------------------------------------------------

--
-- Table structure for table `tb_shipping2`
--

CREATE TABLE IF NOT EXISTS `tb_shipping2` (
  `SHIP_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `SHIP_FEE` decimal(15,2) NOT NULL DEFAULT '0.00',
  `SHIP_COMPANY` int(1) DEFAULT NULL,
  `SHIP_METHOD` int(1) DEFAULT NULL,
  `SHIP_COUNTRY` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `SHIP_STATE` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SHIP_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `SHIP_CREATION` datetime DEFAULT NULL,
  `SHIP_CREATEDBY` bigint(9) NOT NULL DEFAULT '0',
  `SHIP_LASTUPDATE` datetime DEFAULT NULL,
  `SHIP_UPDATEDBY` bigint(9) NOT NULL DEFAULT '0',
  `SHIP_DEFAULT` int(9) NOT NULL DEFAULT '0',
  `SHIP_FEE_FIRST` decimal(15,2) NOT NULL DEFAULT '0.00',
  `SHIP_KG_FIRST` decimal(15,2) NOT NULL DEFAULT '1.00',
  `SHIP_KG` decimal(15,2) NOT NULL DEFAULT '1.00',
  `SHIP_FEEFREE` decimal(15,2) NOT NULL DEFAULT '0.00',
  `SHIP_CITY` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SHIP_LEADTIME` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `SHIP_LIQUID` int(1) unsigned NOT NULL DEFAULT '1',
  `SHIP_COUNTRYTEXT` int(1) unsigned NOT NULL DEFAULT '0',
  `SHIP_STATETEXT` int(1) unsigned NOT NULL DEFAULT '0',
  `SHIP_CITYTEXT` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`SHIP_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_shipping2`
--

INSERT INTO `tb_shipping2` (`SHIP_ID`, `SHIP_FEE`, `SHIP_COMPANY`, `SHIP_METHOD`, `SHIP_COUNTRY`, `SHIP_STATE`, `SHIP_ACTIVE`, `SHIP_CREATION`, `SHIP_CREATEDBY`, `SHIP_LASTUPDATE`, `SHIP_UPDATEDBY`, `SHIP_DEFAULT`, `SHIP_FEE_FIRST`, `SHIP_KG_FIRST`, `SHIP_KG`, `SHIP_FEEFREE`, `SHIP_CITY`, `SHIP_LEADTIME`, `SHIP_LIQUID`, `SHIP_COUNTRYTEXT`, `SHIP_STATETEXT`, `SHIP_CITYTEXT`) VALUES
(1, '0.00', NULL, NULL, 'ALL', '', 1, '2017-05-08 14:54:40', 1, '2017-05-08 14:54:40', 12, 1, '5.00', '1.00', '1.00', '0.00', NULL, NULL, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_slidegroup`
--

CREATE TABLE IF NOT EXISTS `tb_slidegroup` (
  `GRP_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `GRP_NAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `GRP_DNAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `GRP_SLIDERTYPE` varchar(20) NOT NULL DEFAULT '1',
  `GRP_SLIDERTRANS` varchar(20) NOT NULL DEFAULT '0',
  `GRP_SLIDEROPTION` varchar(250) DEFAULT NULL,
  `GRP_SHOWTOP` int(1) unsigned NOT NULL DEFAULT '0',
  `GRP_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `GRP_CREATION` datetime NOT NULL,
  `GRP_CREATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `GRP_LASTUPDATE` datetime DEFAULT NULL,
  `GRP_UPDATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `GRP_OTHER` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`GRP_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_slidegroup`
--

INSERT INTO `tb_slidegroup` (`GRP_ID`, `GRP_NAME`, `GRP_DNAME`, `GRP_SLIDERTYPE`, `GRP_SLIDERTRANS`, `GRP_SLIDEROPTION`, `GRP_SHOWTOP`, `GRP_ACTIVE`, `GRP_CREATION`, `GRP_CREATEDBY`, `GRP_LASTUPDATE`, `GRP_UPDATEDBY`, `GRP_OTHER`) VALUES
(1, 'Other', 'Other', '1', '0', NULL, 0, 0, '2017-05-08 14:54:40', 1, '2017-05-08 14:54:40', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_sms`
--

CREATE TABLE IF NOT EXISTS `tb_sms` (
  `SMS_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `SMS_MODULEPREFIX` varchar(150) DEFAULT NULL,
  `SMS_STATUSID` varchar(150) DEFAULT NULL,
  `SMS_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `SMS_CREATEDBY` bigint(9) unsigned NOT NULL,
  `SMS_CREATION` datetime NOT NULL,
  `SMS_UPDATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `SMS_LASTUPDATE` datetime DEFAULT NULL,
  PRIMARY KEY (`SMS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_state`
--

CREATE TABLE IF NOT EXISTS `tb_state` (
  `STATE_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `STATE_CODE` varchar(250) CHARACTER SET utf8 NOT NULL,
  `STATE_NAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `STATE_ORDER` int(9) unsigned NOT NULL DEFAULT '0',
  `STATE_COUNTRY` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `STATE_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `STATE_DEFAULT` int(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`STATE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tb_state`
--

INSERT INTO `tb_state` (`STATE_ID`, `STATE_CODE`, `STATE_NAME`, `STATE_ORDER`, `STATE_COUNTRY`, `STATE_ACTIVE`, `STATE_DEFAULT`) VALUES
(1, 'Johor', 'Johor', 1, 'MY', 1, 0),
(2, 'Kuala Lumpur', 'Kuala Lumpur', 4, 'MY', 1, 0),
(3, 'Penang', 'Penang', 8, 'MY', 1, 0),
(4, 'Pahang', 'Pahang', 7, 'MY', 1, 0),
(5, 'Melaka', 'Melaka', 5, 'MY', 1, 0),
(6, 'Perak', 'Perak', 9, 'MY', 1, 0),
(7, 'Sabah', 'Sabah', 13, 'MY', 1, 0),
(8, 'Sarawak', 'Sarawak', 14, 'MY', 1, 0),
(9, 'Perlis', 'Perlis', 10, 'MY', 1, 0),
(10, 'Terengganu', 'Terengganu', 12, 'MY', 1, 0),
(11, 'Kelantan', 'Kelantan', 3, 'MY', 1, 0),
(12, 'Selangor', 'Selangor', 11, 'MY', 1, 0),
(13, 'Negeri Sembilan', 'Negeri Sembilan', 6, 'MY', 1, 0),
(14, 'Kedah', 'Kedah', 2, 'MY', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_supplier`
--

CREATE TABLE IF NOT EXISTS `tb_supplier` (
  `SUPPLIER_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `SUPPLIER_COMPANYNAME` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SUPPLIER_ADD` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SUPPLIER_CONTACTPERSON` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `SUPPLIER_TEL` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `SUPPLIER_FAX` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `SUPPLIER_EMAIL` varchar(250) CHARACTER SET utf8 NOT NULL,
  `SUPPLIER_SHOPNO` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`SUPPLIER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_training`
--

CREATE TABLE IF NOT EXISTS `tb_training` (
  `TRAINING_ID` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `TD_ID` int(9) unsigned NOT NULL DEFAULT '0',
  `TRAINING_COMPNAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `TRAINING_TYPE` int(1) NOT NULL DEFAULT '0',
  `TRAINING_TYPEOTHER` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `TRAINING_REMARK` varchar(250) CHARACTER SET utf8 NOT NULL,
  `TRAINING_CONTACTPERSON` varchar(250) DEFAULT NULL,
  `TRAINING_CONTACTNO` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `TRAINING_EMAIL` varchar(250) DEFAULT NULL,
  `TRAINING_STATUS` int(1) NOT NULL DEFAULT '0',
  `TRAINING_TRAINER` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `TRAINING_REASON` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `TRAINING_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `TRAINING_CREATION` datetime NOT NULL,
  `TRAINING_CREATEDBY` int(9) unsigned NOT NULL DEFAULT '0',
  `TRAINING_LASTUPDATE` datetime DEFAULT NULL,
  `TRAINING_UPDATEDBY` int(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`TRAINING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_uploadinventory`
--

CREATE TABLE IF NOT EXISTS `tb_uploadinventory` (
  `INVUPLOAD_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `INVUPLOAD_DATE` datetime NOT NULL,
  `INVUPLOAD_FILENAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `INVUPLOAD_RESULT` varchar(250) CHARACTER SET utf8 NOT NULL,
  `INVUPLOAD_FAILEDRECORDS` longtext CHARACTER SET utf8,
  `INVUPLOAD_UPDATEMETHOD` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`INVUPLOAD_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_vendor`
--

CREATE TABLE IF NOT EXISTS `tb_vendor` (
  `VENDOR_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `VENDOR_CODE` varchar(20) CHARACTER SET utf8 NOT NULL,
  `VENDOR_NAME` varchar(250) CHARACTER SET utf8 NOT NULL,
  `VENDOR_EMAIL` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `VENDOR_CONTACTNO` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `VENDOR_FAX` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `VENDOR_ADDRESS` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `VENDOR_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `VENDOR_CREATION` datetime NOT NULL,
  `VENDOR_CREATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `VENDOR_LASTUPDATE` datetime DEFAULT NULL,
  `VENDOR_UPDATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`VENDOR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_vendorsequence`
--

CREATE TABLE IF NOT EXISTS `tb_vendorsequence` (
  `VSEQ_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `VSEQ_DATEGET` datetime NOT NULL,
  PRIMARY KEY (`VSEQ_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_waiver`
--

CREATE TABLE IF NOT EXISTS `tb_waiver` (
  `WAIVER_ID` bigint(9) unsigned NOT NULL AUTO_INCREMENT,
  `WAIVER_FROM` decimal(15,2) NOT NULL DEFAULT '0.00',
  `WAIVER_TO` decimal(15,2) NOT NULL DEFAULT '0.00',
  `WAIVER_VALUE` decimal(15,2) NOT NULL DEFAULT '0.00',
  `WAIVER_ACTIVE` int(1) unsigned NOT NULL DEFAULT '1',
  `WAIVER_CREATION` datetime NOT NULL,
  `WAIVER_CREATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  `WAIVER_LASTUPDATE` datetime DEFAULT NULL,
  `WAIVER_UPDATEDBY` bigint(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`WAIVER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_working_config_holiday`
--

CREATE TABLE IF NOT EXISTS `tb_working_config_holiday` (
  `holiday_id` int(9) NOT NULL AUTO_INCREMENT,
  `holiday_date` datetime NOT NULL,
  `holiday_remark` varchar(250) DEFAULT NULL,
  `holiday_active` int(1) NOT NULL DEFAULT '1',
  `holiday_createdby` int(9) NOT NULL DEFAULT '0',
  `holiday_updatedby` int(9) NOT NULL DEFAULT '0',
  `holiday_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `holiday_lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `holiday_date_to` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`holiday_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tb_working_config_holiday`
--

INSERT INTO `tb_working_config_holiday` (`holiday_id`, `holiday_date`, `holiday_remark`, `holiday_active`, `holiday_createdby`, `holiday_updatedby`, `holiday_creation`, `holiday_lastupdated`, `holiday_date_to`) VALUES
(1, '2017-01-01 00:00:00', 'New Years Day', 1, 0, 0, '2017-05-25 08:47:10', '2017-06-22 10:26:29', '2017-01-01 00:00:00'),
(2, '2017-05-01 00:00:00', 'Labour Day', 1, 0, 0, '2017-05-25 08:47:10', '2017-06-22 10:26:29', '2017-05-01 00:00:00'),
(3, '2017-08-31 00:00:00', 'National Day', 1, 0, 0, '2017-05-25 08:47:10', '2017-06-22 10:26:29', '2017-08-31 00:00:00'),
(4, '2017-12-25 00:00:00', 'Christmas Day', 1, 0, 0, '2017-05-25 08:47:10', '2017-06-22 10:26:29', '2017-12-25 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_working_config_time`
--

CREATE TABLE IF NOT EXISTS `tb_working_config_time` (
  `time_id` int(9) NOT NULL AUTO_INCREMENT,
  `time_from` datetime NOT NULL,
  `time_to` datetime NOT NULL,
  `time_active` int(1) NOT NULL DEFAULT '1',
  `time_createdby` int(9) NOT NULL DEFAULT '0',
  `time_updatedby` int(9) NOT NULL DEFAULT '0',
  `time_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `time_lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `time_group` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`time_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tb_working_config_time`
--

INSERT INTO `tb_working_config_time` (`time_id`, `time_from`, `time_to`, `time_active`, `time_createdby`, `time_updatedby`, `time_creation`, `time_lastupdated`, `time_group`) VALUES
(1, '2017-07-20 08:00:00', '2017-07-20 10:00:00', 1, 0, 0, '2017-05-25 08:46:31', '2017-07-20 10:51:08', 'Timezone1'),
(2, '2017-07-22 02:00:00', '2017-07-22 04:00:00', 1, 0, 0, '2017-05-25 08:46:31', '2017-07-22 05:46:14', 'Timezone1'),
(3, '2017-07-22 04:00:00', '2017-07-22 06:00:00', 1, 0, 0, '2017-05-25 08:46:31', '2017-07-22 05:46:27', 'Timezone1'),
(4, '2017-07-20 10:00:00', '2017-07-20 12:00:00', 1, 0, 0, '2017-05-25 08:46:31', '2017-07-20 10:51:24', 'Timezone1'),
(5, '1900-01-01 08:00:00', '1900-01-01 10:00:00', 1, 0, 0, '2017-07-22 05:48:09', '2017-07-22 05:48:09', 'Group 2');

-- --------------------------------------------------------

--
-- Table structure for table `tb_working_config_week`
--

CREATE TABLE IF NOT EXISTS `tb_working_config_week` (
  `week_id` int(9) NOT NULL DEFAULT '0',
  `week_day` varchar(20) NOT NULL,
  `week_checked` int(1) NOT NULL DEFAULT '1',
  `week_createdby` int(9) NOT NULL DEFAULT '0',
  `week_updatedby` int(9) NOT NULL DEFAULT '0',
  `week_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `week_lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `week_group` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`week_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_working_config_week`
--

INSERT INTO `tb_working_config_week` (`week_id`, `week_day`, `week_checked`, `week_createdby`, `week_updatedby`, `week_creation`, `week_lastupdated`, `week_group`) VALUES
(0, 'Sun', 0, 0, 0, '2017-05-25 08:46:52', '2017-07-22 08:27:11', 'Timezone1'),
(1, 'Mon', 1, 0, 0, '2017-05-25 08:46:52', '2017-07-22 05:46:50', 'Timezone1'),
(2, 'Tue', 1, 0, 0, '2017-05-25 08:46:52', '2017-07-22 05:46:50', 'Timezone1'),
(3, 'Wed', 1, 0, 0, '2017-05-25 08:46:52', '2017-07-20 10:50:28', 'Timezone1'),
(4, 'Thu', 1, 0, 0, '2017-05-25 08:46:52', '2017-06-22 10:26:40', 'Timezone1'),
(5, 'Fri', 1, 0, 0, '2017-05-25 08:46:52', '2017-07-22 08:27:11', 'Timezone1'),
(6, 'Sat', 0, 0, 0, '2017-05-25 08:46:52', '2017-07-22 05:46:50', 'Timezone1');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_ad`
--
CREATE TABLE IF NOT EXISTS `vw_ad` (
`AD_ID` bigint(9) unsigned
,`AD_NAME` varchar(250)
,`AD_URL` varchar(1000)
,`AD_BANNER` varchar(250)
,`AD_STARTDATE` datetime
,`AD_ENDDATE` datetime
,`AD_POSITION` int(9) unsigned
,`V_POSITIONNAME` varchar(50)
,`AD_CREATION` datetime
,`AD_CREATEDBY` bigint(9) unsigned
,`AD_LASTUPDATE` datetime
,`AD_UPDATEDBY` bigint(9) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_adminpage`
--
CREATE TABLE IF NOT EXISTS `vw_adminpage` (
`ADMPAGE_ID` bigint(9) unsigned
,`ADMPAGE_NAME` varchar(250)
,`ADMPAGE_VALUE` int(9) unsigned
,`ADMPAGE_URL` varchar(250)
,`ADMPAGE_PARENT` bigint(9) unsigned
,`ADMPAGE_ORDER` int(9) unsigned
,`ADMPAGE_ACCESS` int(1)
,`ADMPAGE_SHOWINMENU` int(1)
,`ADMPAGE_SHOWINTOPMENU` int(1)
,`ADMPAGE_SHOWINSIDEMENU` int(1)
,`ADMPAGE_ACTIVE` int(1) unsigned
,`V_TYPE` bigint(9) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_cms`
--
CREATE TABLE IF NOT EXISTS `vw_cms` (
`CMS_ID` bigint(9) unsigned
,`CMS_TITLE` varchar(250)
,`CMS_CONTENT` text
,`CMS_TYPE` bigint(9)
,`V_CMSTYPENAME` varchar(50)
,`CMS_CREATION` datetime
,`CMS_CREATEDBY` bigint(9) unsigned
,`CMS_LASTUPDATE` datetime
,`CMS_UPDATEDBY` bigint(9) unsigned
,`CMS_ACTIVE` int(1) unsigned
,`CMS_ALLOWDELETE` int(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_cmsgroup`
--
CREATE TABLE IF NOT EXISTS `vw_cmsgroup` (
`CMSGROUP_NAME` varchar(50)
,`CMSGROUP_ID` varchar(250)
,`CMSGROUP_GROUP` varchar(50)
,`CMSGROUP_ORDER` int(9) unsigned
,`CMSGROUP_ACTIVE` int(1) unsigned
,`CMSGROUP_PARENTVALUE` varchar(250)
,`CMSGROUP_PARENTGROUP` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_cmstype`
--
CREATE TABLE IF NOT EXISTS `vw_cmstype` (
`CMSTYPE_NAME` varchar(50)
,`CMSTYPE_ID` varchar(250)
,`CMSTYPE_GROUP` varchar(50)
,`CMSTYPE_ORDER` int(9) unsigned
,`CMSTYPE_ACTIVE` int(1) unsigned
,`CMSTYPE_PARENTVALUE` varchar(250)
,`CMSTYPE_PARENTGROUP` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_group`
--
CREATE TABLE IF NOT EXISTS `vw_group` (
`GRP_ID` bigint(9)
,`GRP_CODE` varchar(20)
,`GRP_NAME` varchar(250)
,`GRP_DNAME` varchar(250)
,`GRP_DESC` text
,`GRPING_ID` bigint(9)
,`GRP_IMAGE` varchar(250)
,`GRP_SHOWTOP` int(1)
,`GRP_ACTIVE` int(1)
,`GRP_ORDER` bigint(9)
,`GRP_CREATEDBY` bigint(9)
,`GRP_CREATION` datetime
,`GRP_UPDATEDBY` bigint(9)
,`GRP_LASTUPDATE` datetime
,`GRP_OTHER` int(1) unsigned
,`V_NOP` bigint(21)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_highgroup`
--
CREATE TABLE IF NOT EXISTS `vw_highgroup` (
`HIGH_ID` bigint(9) unsigned
,`GRP_ID` bigint(9) unsigned
,`GRP_NAME` varchar(50)
,`GRP_NAME_ZH` varchar(50)
,`GRP_NAME_JP` varchar(50)
,`GRP_NAME_MS` varchar(50)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_highlight`
--
CREATE TABLE IF NOT EXISTS `vw_highlight` (
`HIGH_ID` bigint(9)
,`HIGH_TITLE` varchar(250)
,`HIGH_TITLE_ZH` varchar(250)
,`HIGH_TITLE_JP` varchar(250)
,`HIGH_TITLE_MS` varchar(250)
,`HIGH_SHORTDESC` text
,`HIGH_SHORTDESC_ZH` text
,`HIGH_SHORTDESC_JP` text
,`HIGH_SHORTDESC_MS` text
,`HIGH_SNAPSHOT` varchar(1000)
,`HIGH_STARTDATE` datetime
,`HIGH_ENDDATE` datetime
,`HIGH_IMAGE` varchar(250)
,`HIGH_ORDER` int(9)
,`HIGH_SHOWTOP` int(1)
,`HIGH_VISIBLE` int(1)
,`HIGH_ALLOWCOMMENT` int(1)
,`HIGH_DESC` text
,`HIGH_DESC_ZH` text
,`HIGH_DESC_JP` text
,`HIGH_DESC_MS` text
,`HIGH_ACTIVE` int(1)
,`HIGH_CREATEDBY` bigint(9)
,`HIGH_CREATION` datetime
,`HIGH_UPDATEDBY` bigint(9)
,`HIGH_LASTUPDATE` datetime
,`V_GRP` bigint(9) unsigned
,`V_GRPNAME` varchar(50)
,`V_GRPNAME_ZH` varchar(50)
,`V_GRPNAME_JP` varchar(50)
,`V_GRPNAME_MS` varchar(50)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_language`
--
CREATE TABLE IF NOT EXISTS `vw_language` (
`LANG_NAME` varchar(50)
,`LANG_ID` varchar(250)
,`LANG_GROUP` varchar(50)
,`LANG_ORDER` int(9) unsigned
,`LANG_ACTIVE` int(1) unsigned
,`LANG_PARENTVALUE` varchar(250)
,`LANG_PARENTGROUP` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_masthead`
--
CREATE TABLE IF NOT EXISTS `vw_masthead` (
`MASTHEAD_ID` bigint(9) unsigned
,`MASTHEAD_NAME` varchar(250)
,`MASTHEAD_IMAGE` varchar(250)
,`MASTHEAD_ACTIVE` int(1)
,`MASTHEAD_THUMB` varchar(250)
,`MASTHEAD_CLICKABLE` int(1)
,`MASTHEAD_URL` varchar(250)
,`MASTHEAD_TAGLINE1` text
,`MASTHEAD_TAGLINE2` text
,`MASTHEAD_TARGET` varchar(20)
,`GRP_ID` int(9)
,`V_GRPNAME` varchar(250)
,`V_GRPDNAME` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_memgender`
--
CREATE TABLE IF NOT EXISTS `vw_memgender` (
`GENDER_NAME` varchar(50)
,`GENDER_ID` varchar(250)
,`GENDER_ORDER` int(9) unsigned
,`GENDER_ACTIVE` int(1) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_memmaritalstatus`
--
CREATE TABLE IF NOT EXISTS `vw_memmaritalstatus` (
`MARITALSTATUS_NAME` varchar(50)
,`MARITALSTATUS_ID` varchar(250)
,`MARITALSTATUS_ORDER` int(9) unsigned
,`MARITALSTATUS_ACTIVE` int(1) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_memrace`
--
CREATE TABLE IF NOT EXISTS `vw_memrace` (
`RACE_NAME` varchar(50)
,`RACE_ID` varchar(250)
,`RACE_ORDER` int(9) unsigned
,`RACE_ACTIVE` int(1) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_memsalutation`
--
CREATE TABLE IF NOT EXISTS `vw_memsalutation` (
`SALUTATION_NAME` varchar(50)
,`SALUTATION_ID` varchar(250)
,`SALUTATION_ORDER` int(9) unsigned
,`SALUTATION_ACTIVE` int(1) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_order`
--
CREATE TABLE IF NOT EXISTS `vw_order` (
`ORDER_ID` bigint(9) unsigned
,`V_MEM` bigint(9) unsigned
,`V_MEMCODE` varchar(20)
,`V_MEMNAME` varchar(250)
,`V_MEMEMAIL` varchar(250)
,`ORDER_NO` varchar(20)
,`ORDER_CREATION` datetime
,`ORDER_DUEDATE` datetime
,`ORDER_REMARKS` varchar(1000)
,`ORDER_READ` int(1) unsigned
,`V_ORDERTYPE` bigint(9) unsigned
,`V_ORDERTYPENAME` varchar(50)
,`ORDER_STATUSID` bigint(9)
,`ORDER_STATUS` varchar(20)
,`V_ORDERSTATUS` varchar(10)
,`V_ORDERSTATUSNAME` varchar(50)
,`V_STATUSDATE` datetime
,`ORDER_NAME` varchar(250)
,`ORDER_ADD` varchar(500)
,`ORDER_POSCODE` varchar(20)
,`ORDER_CITY` varchar(50)
,`ORDER_STATE` varchar(250)
,`ORDER_COUNTRY` varchar(10)
,`V_COUNTRYNAME` varchar(250)
,`ORDER_CONTACTNO` varchar(20)
,`ORDER_EMAIL` varchar(250)
,`SHIPPING_NAME` varchar(250)
,`SHIPPING_ADD` varchar(250)
,`SHIPPING_POSCODE` varchar(20)
,`SHIPPING_CITY` varchar(50)
,`SHIPPING_STATE` varchar(250)
,`SHIPPING_COUNTRY` varchar(10)
,`V_SHIPPINGCOUNTRYNAME` varchar(250)
,`SHIPPING_CONTACTNO` varchar(20)
,`SHIPPING_EMAIL` varchar(250)
,`SHIPPING_REMARKS` varchar(1000)
,`SHIPPING_TRACKCODE` varchar(20)
,`SHIPPING_COMPANY` int(9)
,`V_COMPANYNAME` varchar(50)
,`SHIPPING_COMPANYNAME` varchar(250)
,`ORDER_SHIPPING` decimal(15,2)
,`ORDER_PAYMENT` bigint(9)
,`PAYMENT_GATEWAY` bigint(9)
,`PAYMENT_GATEWAYNAME` varchar(50)
,`PAYMENT_REMARKS` varchar(1000)
,`PAYMENT_DATE` datetime
,`PAYMENT_TOTAL` decimal(15,2)
,`PAYMENT_BANK` int(9)
,`PAYMENT_BANKNAME` varchar(250)
,`PAYMENT_BANKACCOUNT` varchar(50)
,`PAYMENT_TRANSID` varchar(20)
,`PAYMENT_BANKDATE` datetime
,`PAYMENT_REJECT` int(1)
,`PAYMENT_REJECTREMARKS` varchar(250)
,`ORDER_DONE` int(1)
,`ORDER_CANCEL` int(1)
,`ORDER_DLVFLAG` int(1)
,`ORDER_ACTIVE` int(1) unsigned
,`ORDER_UPDATEDBY` bigint(9)
,`ORDER_LASTUPDATE` datetime
,`ORDER_TOTALITEM` decimal(32,0)
,`ORDER_TOTALWEIGHT` decimal(42,0)
,`ORDER_SUBTOTAL` decimal(37,2)
,`ORDER_TOTAL` decimal(38,2)
,`ORDER_PICKUPPOINT` int(9) unsigned
,`ORDER_PICKUPPOINTNAME` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_orderstatus`
--
CREATE TABLE IF NOT EXISTS `vw_orderstatus` (
`ORDSTATUS_ID` bigint(9) unsigned
,`ORDER_ID` bigint(9) unsigned
,`STATUS_ID` varchar(10)
,`V_STATUSNAME` varchar(50)
,`STATUS_DATE` datetime
,`STATUS_ACTIVE` int(1) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_orderstatuslist`
--
CREATE TABLE IF NOT EXISTS `vw_orderstatuslist` (
`ORDERSTATUS_NAME` varchar(50)
,`ORDERSTATUS_ID` varchar(250)
,`ORDERSTATUS_ORDER` int(9) unsigned
,`ORDERSTATUS_GROUP` varchar(50)
,`ORDERSTATUS_PARENTVALUE` varchar(250)
,`ORDERSTATUS_PARENTGROUP` varchar(250)
,`ORDERSTATUS_ACTIVE` int(1) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_ordertype`
--
CREATE TABLE IF NOT EXISTS `vw_ordertype` (
`ORDERTYPE_NAME` varchar(50)
,`ORDERTYPE_ID` varchar(250)
,`ORDERTYPE_ORDER` int(9) unsigned
,`ORDERTYPE_ACTIVE` int(1) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_page`
--
CREATE TABLE IF NOT EXISTS `vw_page` (
`PAGE_ID` bigint(9) unsigned
,`PAGE_NAME` varchar(250)
,`PAGE_DISPLAYNAME` varchar(250)
,`PAGE_TITLE` varchar(250)
,`PAGE_CONTENT` text
,`PAGE_SHOWPAGETITLE` int(1) unsigned
,`PAGE_SHOWINMENU` int(1) unsigned
,`PAGE_SHOWTOP` int(1)
,`PAGE_SHOWBOTTOM` int(1)
,`PAGE_SHOWINBREADCRUMB` int(1)
,`PAGE_USEREDITABLE` int(1)
,`PAGE_ENABLELINK` int(1)
,`PAGE_CSSCLASS` varchar(250)
,`PAGE_ORDER` int(9) unsigned
,`PAGE_LANG` varchar(10)
,`V_LANGNAME` varchar(50)
,`PAGE_TEMPLATE` varchar(250)
,`PAGE_TEMPLATEPARENT` int(1)
,`V_TEMPLATEPARENT` varchar(250)
,`PAGE_PARENT` bigint(9)
,`V_PARENTNAME` varchar(250)
,`V_PARENTCSSCLASS` varchar(250)
,`PAGE_KEYWORD` varchar(1000)
,`PAGE_DEFAULTKEYWORD` int(1)
,`PAGE_DESC` varchar(1000)
,`PAGE_DEFAULTDESC` int(1)
,`PAGE_REDIRECT` bigint(9)
,`V_REDIRECTNAME` varchar(250)
,`V_REDIRECTPARENT` bigint(9)
,`V_REDIRECTTEMPLATE` varchar(250)
,`V_REDIRECTTEMPLATEPARENT` int(1)
,`V_REDIRECTPARENTTEMPLATE` varchar(250)
,`PAGE_ACTIVE` int(1) unsigned
,`PAGE_OTHER` int(1)
,`PAGE_CREATEDBY` bigint(9) unsigned
,`PAGE_CREATION` datetime
,`PAGE_UPDATEDBY` bigint(9) unsigned
,`PAGE_LASTUPDATE` datetime
,`PAGE_REDIRECTLINK` varchar(1000)
,`PAGE_REDIRECTTARGET` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_pagecms`
--
CREATE TABLE IF NOT EXISTS `vw_pagecms` (
`PAGECMS_ID` bigint(9) unsigned
,`PAGE_ID` int(9)
,`CUSTOM_ID` bigint(9)
,`CUSTOM_ID_2` bigint(9)
,`BLOCK_ID` int(9)
,`CMS_ID` bigint(9) unsigned
,`V_CMSTITLE` varchar(250)
,`V_CMSCONTENT` text
,`V_CMSCREATION` datetime
,`V_CMSCREATEDBY` bigint(9) unsigned
,`V_CMSLASTUPDATE` datetime
,`V_CMSUPDATEDBY` bigint(9) unsigned
,`V_GRP_ACTIVE` int(1) unsigned
,`V_CMSACTIVE` int(1) unsigned
,`PAGECMS_TYPE` bigint(9) unsigned
,`V_TYPENAME` varchar(50)
,`PAGECMS_GROUP` int(9) unsigned
,`V_GROUPNAME` varchar(50)
,`PAGECMS_LANG` varchar(10)
,`PAGECMS_ADMINVIEW` int(1)
,`PAGECMS_CREATION` datetime
,`PAGECMS_CREATEDBY` bigint(9) unsigned
,`PAGECMS_LASTUPDATE` datetime
,`PAGECMS_UPDATEDBY` bigint(9) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_pagecorrespond`
--
CREATE TABLE IF NOT EXISTS `vw_pagecorrespond` (
`PC_ID` bigint(9) unsigned
,`PAGE_ID` bigint(9) unsigned
,`PC_PAGEID` bigint(9) unsigned
,`V_PAGETITLE` varchar(250)
,`V_PAGEORDER` int(9) unsigned
,`V_PAGELANG` varchar(10)
,`V_PAGETEMPLATE` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_payment`
--
CREATE TABLE IF NOT EXISTS `vw_payment` (
`PAYMENT_ID` bigint(9) unsigned
,`ORDER_ID` bigint(9) unsigned
,`PAYMENT_GATEWAY` bigint(9)
,`PAYMENT_GATEWAYNAME` varchar(50)
,`PAYMENT_REMARKS` varchar(1000)
,`PAYMENT_DATE` datetime
,`PAYMENT_TOTAL` decimal(15,2)
,`PAYMENT_BANK` int(9)
,`PAYMENT_BANKNAME` varchar(250)
,`PAYMENT_BANKACCOUNT` varchar(50)
,`PAYMENT_TRANSID` varchar(20)
,`PAYMENT_BANKDATE` datetime
,`PAYMENT_REJECT` int(1)
,`PAYMENT_REJECTREMARKS` varchar(250)
,`PAYMENT_ACTIVE` int(1) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_paymenttype`
--
CREATE TABLE IF NOT EXISTS `vw_paymenttype` (
`PAYMENTTYPE_NAME` varchar(50)
,`PAYMENTTYPE_ID` varchar(250)
,`PAYMENTTYPE_ORDER` int(9) unsigned
,`PAYMENTTYPE_ACTIVE` int(1) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_position`
--
CREATE TABLE IF NOT EXISTS `vw_position` (
`POSITION_NAME` varchar(50)
,`POSITION_ID` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_prodcat1`
--
CREATE TABLE IF NOT EXISTS `vw_prodcat1` (
`PROD_ID` bigint(9) unsigned
,`GRP_ID` bigint(9) unsigned
,`GRP_NAME` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_prodcat2`
--
CREATE TABLE IF NOT EXISTS `vw_prodcat2` (
`PROD_ID` bigint(9) unsigned
,`GRP_ID` bigint(9) unsigned
,`GRP_NAME` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_prodcat3`
--
CREATE TABLE IF NOT EXISTS `vw_prodcat3` (
`PROD_ID` bigint(9) unsigned
,`GRP_ID` bigint(9) unsigned
,`GRP_NAME` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_prodcat4`
--
CREATE TABLE IF NOT EXISTS `vw_prodcat4` (
`PROD_ID` bigint(9) unsigned
,`GRP_ID` bigint(9) unsigned
,`GRP_NAME` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_prodcat5`
--
CREATE TABLE IF NOT EXISTS `vw_prodcat5` (
`PROD_ID` bigint(9) unsigned
,`GRP_ID` bigint(9) unsigned
,`GRP_NAME` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_prodcat6`
--
CREATE TABLE IF NOT EXISTS `vw_prodcat6` (
`PROD_ID` bigint(9) unsigned
,`GRP_ID` bigint(9) unsigned
,`GRP_NAME` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_prodcat7`
--
CREATE TABLE IF NOT EXISTS `vw_prodcat7` (
`PROD_ID` bigint(9) unsigned
,`GRP_ID` bigint(9) unsigned
,`GRP_NAME` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_prodcat8`
--
CREATE TABLE IF NOT EXISTS `vw_prodcat8` (
`PROD_ID` bigint(9) unsigned
,`GRP_ID` bigint(9) unsigned
,`GRP_NAME` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_prodcat9`
--
CREATE TABLE IF NOT EXISTS `vw_prodcat9` (
`PROD_ID` bigint(9) unsigned
,`GRP_ID` bigint(9) unsigned
,`GRP_NAME` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_prodcat10`
--
CREATE TABLE IF NOT EXISTS `vw_prodcat10` (
`PROD_ID` bigint(9) unsigned
,`GRP_ID` bigint(9) unsigned
,`GRP_NAME` varchar(250)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_product`
--
CREATE TABLE IF NOT EXISTS `vw_product` (
`PROD_ID` bigint(9) unsigned
,`PROD_CODE` varchar(30)
,`PROD_NAME` varchar(250)
,`PROD_DNAME` varchar(250)
,`PROD_DESC` text
,`PROD_SNAPSHOT` varchar(1000)
,`PROD_UOM` varchar(10)
,`PROD_WEIGHT` int(9) unsigned
,`PROD_LENGTH` int(9) unsigned
,`PROD_WIDTH` int(9) unsigned
,`PROD_HEIGHT` int(9) unsigned
,`PROD_NEW` int(1) unsigned
,`PROD_SHOWTOP` int(1) unsigned
,`PROD_ACTIVE` int(1) unsigned
,`PROD_BEST` int(1) unsigned
,`PROD_IMAGE` varchar(250)
,`PROD_NOV` bigint(9) unsigned
,`PROD_NOP` bigint(9) unsigned
,`PROD_CREATEDBY` bigint(9) unsigned
,`PROD_CREATION` datetime
,`PROD_UPDATEDBY` bigint(9) unsigned
,`PROD_LASTUPDATE` datetime
,`V_CAT1` bigint(9) unsigned
,`V_CAT1NAME` varchar(250)
,`V_CAT2` bigint(9) unsigned
,`V_CAT2NAME` varchar(250)
,`V_CAT3` bigint(9) unsigned
,`V_CAT3NAME` varchar(250)
,`V_CAT4` bigint(9) unsigned
,`V_CAT4NAME` varchar(250)
,`V_CAT5` bigint(9) unsigned
,`V_CAT5NAME` varchar(250)
,`V_CAT6` bigint(9) unsigned
,`V_CAT6NAME` varchar(250)
,`V_CAT7` bigint(9) unsigned
,`V_CAT7NAME` varchar(250)
,`V_CAT8` bigint(9) unsigned
,`V_CAT8NAME` varchar(250)
,`V_CAT9` bigint(9) unsigned
,`V_CAT9NAME` varchar(250)
,`V_CAT10` bigint(9) unsigned
,`V_CAT10NAME` varchar(250)
,`PROD_COST` decimal(15,2)
,`PROD_PRICE` decimal(15,2)
,`PROD_PROMPRICE` decimal(15,2)
,`PROD_PROMSTARTDATE` datetime
,`PROD_PROMENDDATE` datetime
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_relproduct`
--
CREATE TABLE IF NOT EXISTS `vw_relproduct` (
`PROD_ID` bigint(9) unsigned
,`RELPROD_ID` bigint(9) unsigned
,`RELPROD_CODE` varchar(30)
,`RELPROD_NAME` varchar(250)
,`RELPROD_DNAME` varchar(250)
,`RELPROD_DESC` text
,`RELPROD_SNAPSHOT` varchar(1000)
,`RELPROD_UOM` varchar(10)
,`RELPROD_WEIGHT` int(9) unsigned
,`RELPROD_LENGTH` int(9) unsigned
,`RELPROD_WIDTH` int(9) unsigned
,`RELPROD_HEIGHT` int(9) unsigned
,`RELPROD_NEW` int(1) unsigned
,`RELPROD_SHOWTOP` int(1) unsigned
,`RELPROD_ACTIVE` int(1) unsigned
,`RELPROD_IMAGE` varchar(250)
,`RELPROD_NOV` bigint(9) unsigned
,`RELPROD_NOP` bigint(9) unsigned
,`RELPROD_CREATEDBY` bigint(9) unsigned
,`RELPROD_CREATION` datetime
,`RELPROD_UPDATEDBY` bigint(9) unsigned
,`RELPROD_LASTUPDATE` datetime
,`RELPROD_CAT1` bigint(9) unsigned
,`RELPROD_CAT1NAME` varchar(250)
,`RELPROD_CAT2` bigint(9) unsigned
,`RELPROD_CAT2NAME` varchar(250)
,`RELPROD_COST` decimal(15,2)
,`RELPROD_PRICE` decimal(15,2)
,`RELPROD_PROMPRICE` decimal(15,2)
,`RELPROD_PROMSTARTDATE` datetime
,`RELPROD_PROMENDDATE` datetime
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_shipping2`
--
CREATE TABLE IF NOT EXISTS `vw_shipping2` (
`SHIP_ID` bigint(9) unsigned
,`SHIP_KG_FIRST` decimal(15,2)
,`SHIP_FEE_FIRST` decimal(15,2)
,`SHIP_KG` decimal(15,2)
,`SHIP_FEE` decimal(15,2)
,`SHIP_COUNTRY` varchar(10)
,`V_SHIPCOUNTRYNAME` varchar(250)
,`SHIP_STATE` varchar(250)
,`SHIP_ACTIVE` int(1) unsigned
,`SHIP_CREATION` datetime
,`SHIP_CREATEDBY` bigint(9)
,`SHIP_LASTUPDATE` datetime
,`SHIP_UPDATEDBY` bigint(9)
,`SHIP_DEFAULT` int(9)
,`SHIP_FEEFREE` decimal(15,2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_shippingcompany`
--
CREATE TABLE IF NOT EXISTS `vw_shippingcompany` (
`COMPANY_NAME` varchar(50)
,`COMPANY_ID` varchar(250)
,`COMPANY_ORDER` int(9) unsigned
,`COMPANY_ACTIVE` int(1) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_state`
--
CREATE TABLE IF NOT EXISTS `vw_state` (
`STATE_NAME` varchar(50)
,`STATE_ID` varchar(250)
,`STATE_ORDER` int(9) unsigned
,`STATE_ACTIVE` int(1) unsigned
,`COUNTRY_VALUE` varchar(250)
);
-- --------------------------------------------------------

--
-- Structure for view `vw_ad`
--
DROP TABLE IF EXISTS `vw_ad`;

CREATE VIEW `vw_ad` AS select `a`.`AD_ID` AS `AD_ID`,`a`.`AD_NAME` AS `AD_NAME`,`a`.`AD_URL` AS `AD_URL`,`a`.`AD_BANNER` AS `AD_BANNER`,`a`.`AD_STARTDATE` AS `AD_STARTDATE`,`a`.`AD_ENDDATE` AS `AD_ENDDATE`,`a`.`AD_POSITION` AS `AD_POSITION`,`b`.`POSITION_NAME` AS `V_POSITIONNAME`,`a`.`AD_CREATION` AS `AD_CREATION`,`a`.`AD_CREATEDBY` AS `AD_CREATEDBY`,`a`.`AD_LASTUPDATE` AS `AD_LASTUPDATE`,`a`.`AD_UPDATEDBY` AS `AD_UPDATEDBY` from (`tb_ad` `a` left join `vw_position` `b` on((`a`.`AD_POSITION` = `b`.`POSITION_ID`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_adminpage`
--
DROP TABLE IF EXISTS `vw_adminpage`;

CREATE VIEW `vw_adminpage` AS select `a`.`ADMPAGE_ID` AS `ADMPAGE_ID`,`a`.`ADMPAGE_NAME` AS `ADMPAGE_NAME`,`a`.`ADMPAGE_VALUE` AS `ADMPAGE_VALUE`,`a`.`ADMPAGE_URL` AS `ADMPAGE_URL`,`a`.`ADMPAGE_PARENT` AS `ADMPAGE_PARENT`,`a`.`ADMPAGE_ORDER` AS `ADMPAGE_ORDER`,`a`.`ADMPAGE_ACCESS` AS `ADMPAGE_ACCESS`,`a`.`ADMPAGE_SHOWINMENU` AS `ADMPAGE_SHOWINMENU`,`a`.`ADMPAGE_SHOWINTOPMENU` AS `ADMPAGE_SHOWINTOPMENU`,`a`.`ADMPAGE_SHOWINSIDEMENU` AS `ADMPAGE_SHOWINSIDEMENU`,`a`.`ADMPAGE_ACTIVE` AS `ADMPAGE_ACTIVE`,`b`.`TYPE_ID` AS `V_TYPE` from (`tb_adminpage` `a` left join `tb_adminpagetype` `b` on((`a`.`ADMPAGE_ID` = `b`.`ADMPAGE_ID`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_cms`
--
DROP TABLE IF EXISTS `vw_cms`;

CREATE VIEW `vw_cms` AS select `a`.`CMS_ID` AS `CMS_ID`,`a`.`CMS_TITLE` AS `CMS_TITLE`,`a`.`CMS_CONTENT` AS `CMS_CONTENT`,`a`.`CMS_TYPE` AS `CMS_TYPE`,`b`.`CMSTYPE_NAME` AS `V_CMSTYPENAME`,`a`.`CMS_CREATION` AS `CMS_CREATION`,`a`.`CMS_CREATEDBY` AS `CMS_CREATEDBY`,`a`.`CMS_LASTUPDATE` AS `CMS_LASTUPDATE`,`a`.`CMS_UPDATEDBY` AS `CMS_UPDATEDBY`,`a`.`CMS_ACTIVE` AS `CMS_ACTIVE`,`a`.`CMS_ALLOWDELETE` AS `CMS_ALLOWDELETE` from (`tb_cms` `a` left join `vw_cmstype` `b` on((`a`.`CMS_TYPE` = `b`.`CMSTYPE_ID`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_cmsgroup`
--
DROP TABLE IF EXISTS `vw_cmsgroup`;

CREATE VIEW `vw_cmsgroup` AS select `a`.`LIST_NAME` AS `CMSGROUP_NAME`,`a`.`LIST_VALUE` AS `CMSGROUP_ID`,`a`.`LIST_GROUP` AS `CMSGROUP_GROUP`,`a`.`LIST_ORDER` AS `CMSGROUP_ORDER`,`a`.`LIST_ACTIVE` AS `CMSGROUP_ACTIVE`,`a`.`LIST_PARENTVALUE` AS `CMSGROUP_PARENTVALUE`,`a`.`LIST_PARENTGROUP` AS `CMSGROUP_PARENTGROUP` from `tb_list` `a` where (`a`.`LIST_GROUP` = 'CMS GROUP');

-- --------------------------------------------------------

--
-- Structure for view `vw_cmstype`
--
DROP TABLE IF EXISTS `vw_cmstype`;

CREATE VIEW `vw_cmstype` AS select `a`.`LIST_NAME` AS `CMSTYPE_NAME`,`a`.`LIST_VALUE` AS `CMSTYPE_ID`,`a`.`LIST_GROUP` AS `CMSTYPE_GROUP`,`a`.`LIST_ORDER` AS `CMSTYPE_ORDER`,`a`.`LIST_ACTIVE` AS `CMSTYPE_ACTIVE`,`a`.`LIST_PARENTVALUE` AS `CMSTYPE_PARENTVALUE`,`a`.`LIST_PARENTGROUP` AS `CMSTYPE_PARENTGROUP` from `tb_list` `a` where (`a`.`LIST_GROUP` = 'CMS TYPE');

-- --------------------------------------------------------

--
-- Structure for view `vw_group`
--
DROP TABLE IF EXISTS `vw_group`;

CREATE VIEW `vw_group` AS select `a`.`GRP_ID` AS `GRP_ID`,`a`.`GRP_CODE` AS `GRP_CODE`,`a`.`GRP_NAME` AS `GRP_NAME`,`a`.`GRP_DNAME` AS `GRP_DNAME`,`a`.`GRP_DESC` AS `GRP_DESC`,`a`.`GRPING_ID` AS `GRPING_ID`,`a`.`GRP_IMAGE` AS `GRP_IMAGE`,`a`.`GRP_SHOWTOP` AS `GRP_SHOWTOP`,`a`.`GRP_ACTIVE` AS `GRP_ACTIVE`,`a`.`GRP_ORDER` AS `GRP_ORDER`,`a`.`GRP_CREATEDBY` AS `GRP_CREATEDBY`,`a`.`GRP_CREATION` AS `GRP_CREATION`,`a`.`GRP_UPDATEDBY` AS `GRP_UPDATEDBY`,`a`.`GRP_LASTUPDATE` AS `GRP_LASTUPDATE`,`a`.`GRP_OTHER` AS `GRP_OTHER`,count(`c`.`PROD_ID`) AS `V_NOP` from (`tb_group` `a` left join `tb_prodgroup` `c` on((`a`.`GRP_ID` = `c`.`GRP_ID`))) group by `a`.`GRP_ID`;

-- --------------------------------------------------------

--
-- Structure for view `vw_highgroup`
--
DROP TABLE IF EXISTS `vw_highgroup`;

CREATE VIEW `vw_highgroup` AS select `a`.`HIGH_ID` AS `HIGH_ID`,`a`.`GRP_ID` AS `GRP_ID`,(select `b`.`LIST_NAME` AS `LIST_NAME` from `tb_list` `b` where ((`b`.`LIST_GROUP` = 'HIGHLIGHT GROUP') and (`a`.`GRP_ID` = `b`.`LIST_VALUE`) and (`b`.`LIST_DEFAULT` = 1))) AS `GRP_NAME`,(select `c`.`LIST_NAME` AS `LIST_NAME` from `tb_list` `c` where ((`c`.`LIST_GROUP` = 'HIGHLIGHT GROUP') and (`c`.`LIST_VALUE` = `a`.`GRP_ID`) and (`c`.`LIST_LANG` = 'zh'))) AS `GRP_NAME_ZH`,(select `c`.`LIST_NAME` AS `LIST_NAME` from `tb_list` `c` where ((`c`.`LIST_GROUP` = 'HIGHLIGHT GROUP') and (`c`.`LIST_VALUE` = `a`.`GRP_ID`) and (`c`.`LIST_LANG` = 'ja'))) AS `GRP_NAME_JP`,(select `c`.`LIST_NAME` AS `LIST_NAME` from `tb_list` `c` where ((`c`.`LIST_GROUP` = 'HIGHLIGHT GROUP') and (`c`.`LIST_VALUE` = `a`.`GRP_ID`) and (`c`.`LIST_LANG` = 'ms'))) AS `GRP_NAME_MS` from `tb_highgroup` `a`;

-- --------------------------------------------------------

--
-- Structure for view `vw_highlight`
--
DROP TABLE IF EXISTS `vw_highlight`;

CREATE VIEW `vw_highlight` AS select `a`.`HIGH_ID` AS `HIGH_ID`,`a`.`HIGH_TITLE` AS `HIGH_TITLE`,`a`.`HIGH_TITLE_ZH` AS `HIGH_TITLE_ZH`,`a`.`HIGH_TITLE_JP` AS `HIGH_TITLE_JP`,`a`.`HIGH_TITLE_MS` AS `HIGH_TITLE_MS`,`a`.`HIGH_SHORTDESC` AS `HIGH_SHORTDESC`,`a`.`HIGH_SHORTDESC_ZH` AS `HIGH_SHORTDESC_ZH`,`a`.`HIGH_SHORTDESC_JP` AS `HIGH_SHORTDESC_JP`,`a`.`HIGH_SHORTDESC_MS` AS `HIGH_SHORTDESC_MS`,`a`.`HIGH_SNAPSHOT` AS `HIGH_SNAPSHOT`,`a`.`HIGH_STARTDATE` AS `HIGH_STARTDATE`,`a`.`HIGH_ENDDATE` AS `HIGH_ENDDATE`,`a`.`HIGH_IMAGE` AS `HIGH_IMAGE`,`a`.`HIGH_ORDER` AS `HIGH_ORDER`,`a`.`HIGH_SHOWTOP` AS `HIGH_SHOWTOP`,`a`.`HIGH_VISIBLE` AS `HIGH_VISIBLE`,`a`.`HIGH_ALLOWCOMMENT` AS `HIGH_ALLOWCOMMENT`,`a`.`HIGH_DESC` AS `HIGH_DESC`,`a`.`HIGH_DESC_ZH` AS `HIGH_DESC_ZH`,`a`.`HIGH_DESC_JP` AS `HIGH_DESC_JP`,`a`.`HIGH_DESC_MS` AS `HIGH_DESC_MS`,`a`.`HIGH_ACTIVE` AS `HIGH_ACTIVE`,`a`.`HIGH_CREATEDBY` AS `HIGH_CREATEDBY`,`a`.`HIGH_CREATION` AS `HIGH_CREATION`,`a`.`HIGH_UPDATEDBY` AS `HIGH_UPDATEDBY`,`a`.`HIGH_LASTUPDATE` AS `HIGH_LASTUPDATE`,`c`.`GRP_ID` AS `V_GRP`,`c`.`GRP_NAME` AS `V_GRPNAME`,`c`.`GRP_NAME_ZH` AS `V_GRPNAME_ZH`,`c`.`GRP_NAME_JP` AS `V_GRPNAME_JP`,`c`.`GRP_NAME_MS` AS `V_GRPNAME_MS` from ((`tb_highlight` `a` left join `tb_highgallery` `b` on((`a`.`HIGH_ID` = `b`.`HIGH_ID`))) left join `vw_highgroup` `c` on((`a`.`HIGH_ID` = `c`.`HIGH_ID`))) group by `a`.`HIGH_ID`;

-- --------------------------------------------------------

--
-- Structure for view `vw_language`
--
DROP TABLE IF EXISTS `vw_language`;

CREATE VIEW `vw_language` AS select `a`.`LIST_NAME` AS `LANG_NAME`,`a`.`LIST_VALUE` AS `LANG_ID`,`a`.`LIST_GROUP` AS `LANG_GROUP`,`a`.`LIST_ORDER` AS `LANG_ORDER`,`a`.`LIST_ACTIVE` AS `LANG_ACTIVE`,`a`.`LIST_PARENTVALUE` AS `LANG_PARENTVALUE`,`a`.`LIST_PARENTGROUP` AS `LANG_PARENTGROUP` from `tb_list` `a` where (`a`.`LIST_GROUP` = 'LANGUAGE');

-- --------------------------------------------------------

--
-- Structure for view `vw_masthead`
--
DROP TABLE IF EXISTS `vw_masthead`;

CREATE VIEW `vw_masthead` AS select `a`.`MASTHEAD_ID` AS `MASTHEAD_ID`,`a`.`MASTHEAD_NAME` AS `MASTHEAD_NAME`,`a`.`MASTHEAD_IMAGE` AS `MASTHEAD_IMAGE`,`a`.`MASTHEAD_ACTIVE` AS `MASTHEAD_ACTIVE`,`a`.`MASTHEAD_THUMB` AS `MASTHEAD_THUMB`,`a`.`MASTHEAD_CLICKABLE` AS `MASTHEAD_CLICKABLE`,`a`.`MASTHEAD_URL` AS `MASTHEAD_URL`,`a`.`MASTHEAD_TAGLINE1` AS `MASTHEAD_TAGLINE1`,`a`.`MASTHEAD_TAGLINE2` AS `MASTHEAD_TAGLINE2`,`a`.`MASTHEAD_TARGET` AS `MASTHEAD_TARGET`,`a`.`GRP_ID` AS `GRP_ID`,`b`.`GRP_NAME` AS `V_GRPNAME`,`b`.`GRP_DNAME` AS `V_GRPDNAME` from (`tb_masthead` `a` left join `tb_slidegroup` `b` on((`a`.`GRP_ID` = `b`.`GRP_ID`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_memgender`
--
DROP TABLE IF EXISTS `vw_memgender`;

CREATE VIEW `vw_memgender` AS select `tb_list`.`LIST_NAME` AS `GENDER_NAME`,`tb_list`.`LIST_VALUE` AS `GENDER_ID`,`tb_list`.`LIST_ORDER` AS `GENDER_ORDER`,`tb_list`.`LIST_ACTIVE` AS `GENDER_ACTIVE` from `tb_list` where (`tb_list`.`LIST_GROUP` = 'GENDER');

-- --------------------------------------------------------

--
-- Structure for view `vw_memmaritalstatus`
--
DROP TABLE IF EXISTS `vw_memmaritalstatus`;

CREATE VIEW `vw_memmaritalstatus` AS select `tb_list`.`LIST_NAME` AS `MARITALSTATUS_NAME`,`tb_list`.`LIST_VALUE` AS `MARITALSTATUS_ID`,`tb_list`.`LIST_ORDER` AS `MARITALSTATUS_ORDER`,`tb_list`.`LIST_ACTIVE` AS `MARITALSTATUS_ACTIVE` from `tb_list` where (`tb_list`.`LIST_GROUP` = 'MARITAL STATUS');

-- --------------------------------------------------------

--
-- Structure for view `vw_memrace`
--
DROP TABLE IF EXISTS `vw_memrace`;

CREATE VIEW `vw_memrace` AS select `tb_list`.`LIST_NAME` AS `RACE_NAME`,`tb_list`.`LIST_VALUE` AS `RACE_ID`,`tb_list`.`LIST_ORDER` AS `RACE_ORDER`,`tb_list`.`LIST_ACTIVE` AS `RACE_ACTIVE` from `tb_list` where (`tb_list`.`LIST_GROUP` = 'RACE');

-- --------------------------------------------------------

--
-- Structure for view `vw_memsalutation`
--
DROP TABLE IF EXISTS `vw_memsalutation`;

CREATE VIEW `vw_memsalutation` AS select `tb_list`.`LIST_NAME` AS `SALUTATION_NAME`,`tb_list`.`LIST_VALUE` AS `SALUTATION_ID`,`tb_list`.`LIST_ORDER` AS `SALUTATION_ORDER`,`tb_list`.`LIST_ACTIVE` AS `SALUTATION_ACTIVE` from `tb_list` where (`tb_list`.`LIST_GROUP` = 'SALUTATION');

-- --------------------------------------------------------

--
-- Structure for view `vw_order`
--
DROP TABLE IF EXISTS `vw_order`;

CREATE VIEW `vw_order` AS select `a`.`ORDER_ID` AS `ORDER_ID`,`a`.`MEM_ID` AS `V_MEM`,`a`.`MEM_CODE` AS `V_MEMCODE`,`a`.`MEM_NAME` AS `V_MEMNAME`,`a`.`MEM_EMAIL` AS `V_MEMEMAIL`,`a`.`ORDER_NO` AS `ORDER_NO`,`a`.`ORDER_CREATION` AS `ORDER_CREATION`,`a`.`ORDER_DUEDATE` AS `ORDER_DUEDATE`,`a`.`ORDER_REMARKS` AS `ORDER_REMARKS`,`a`.`ORDER_READ` AS `ORDER_READ`,`a`.`ORDER_TYPE` AS `V_ORDERTYPE`,`d`.`ORDERTYPE_NAME` AS `V_ORDERTYPENAME`,`a`.`ORDER_STATUSID` AS `ORDER_STATUSID`,`a`.`ORDER_STATUS` AS `ORDER_STATUS`,`f`.`STATUS_ID` AS `V_ORDERSTATUS`,`f`.`V_STATUSNAME` AS `V_ORDERSTATUSNAME`,`f`.`STATUS_DATE` AS `V_STATUSDATE`,`a`.`ORDER_NAME` AS `ORDER_NAME`,`a`.`ORDER_ADD` AS `ORDER_ADD`,`a`.`ORDER_POSCODE` AS `ORDER_POSCODE`,`a`.`ORDER_CITY` AS `ORDER_CITY`,`a`.`ORDER_STATE` AS `ORDER_STATE`,`a`.`ORDER_COUNTRY` AS `ORDER_COUNTRY`,`g`.`COUNTRY_NAME` AS `V_COUNTRYNAME`,`a`.`ORDER_CONTACTNO` AS `ORDER_CONTACTNO`,`a`.`ORDER_EMAIL` AS `ORDER_EMAIL`,`a`.`SHIPPING_NAME` AS `SHIPPING_NAME`,`a`.`SHIPPING_ADD` AS `SHIPPING_ADD`,`a`.`SHIPPING_POSCODE` AS `SHIPPING_POSCODE`,`a`.`SHIPPING_CITY` AS `SHIPPING_CITY`,`a`.`SHIPPING_STATE` AS `SHIPPING_STATE`,`a`.`SHIPPING_COUNTRY` AS `SHIPPING_COUNTRY`,`h`.`COUNTRY_NAME` AS `V_SHIPPINGCOUNTRYNAME`,`a`.`SHIPPING_CONTACTNO` AS `SHIPPING_CONTACTNO`,`a`.`SHIPPING_EMAIL` AS `SHIPPING_EMAIL`,`a`.`SHIPPING_REMARKS` AS `SHIPPING_REMARKS`,`a`.`SHIPPING_TRACKCODE` AS `SHIPPING_TRACKCODE`,`a`.`SHIPPING_COMPANY` AS `SHIPPING_COMPANY`,`i`.`COMPANY_NAME` AS `V_COMPANYNAME`,`a`.`SHIPPING_COMPANYNAME` AS `SHIPPING_COMPANYNAME`,`a`.`ORDER_SHIPPING` AS `ORDER_SHIPPING`,`a`.`ORDER_PAYMENT` AS `ORDER_PAYMENT`,`e`.`PAYMENT_GATEWAY` AS `PAYMENT_GATEWAY`,`e`.`PAYMENT_GATEWAYNAME` AS `PAYMENT_GATEWAYNAME`,`e`.`PAYMENT_REMARKS` AS `PAYMENT_REMARKS`,`e`.`PAYMENT_DATE` AS `PAYMENT_DATE`,`e`.`PAYMENT_TOTAL` AS `PAYMENT_TOTAL`,`e`.`PAYMENT_BANK` AS `PAYMENT_BANK`,`e`.`PAYMENT_BANKNAME` AS `PAYMENT_BANKNAME`,`e`.`PAYMENT_BANKACCOUNT` AS `PAYMENT_BANKACCOUNT`,`e`.`PAYMENT_TRANSID` AS `PAYMENT_TRANSID`,`e`.`PAYMENT_BANKDATE` AS `PAYMENT_BANKDATE`,`e`.`PAYMENT_REJECT` AS `PAYMENT_REJECT`,`e`.`PAYMENT_REJECTREMARKS` AS `PAYMENT_REJECTREMARKS`,`a`.`ORDER_DONE` AS `ORDER_DONE`,`a`.`ORDER_CANCEL` AS `ORDER_CANCEL`,`a`.`ORDER_DLVFLAG` AS `ORDER_DLVFLAG`,`a`.`ORDER_ACTIVE` AS `ORDER_ACTIVE`,`a`.`ORDER_UPDATEDBY` AS `ORDER_UPDATEDBY`,`a`.`ORDER_LASTUPDATE` AS `ORDER_LASTUPDATE`,sum(`c`.`ORDDETAIL_PRODQTY`) AS `ORDER_TOTALITEM`,sum((`c`.`ORDDETAIL_PRODWEIGHT` * `c`.`ORDDETAIL_PRODQTY`)) AS `ORDER_TOTALWEIGHT`,sum(`c`.`ORDDETAIL_PRODTOTAL`) AS `ORDER_SUBTOTAL`,(sum(`c`.`ORDDETAIL_PRODTOTAL`) + `a`.`ORDER_SHIPPING`) AS `ORDER_TOTAL`,`a`.`ORDER_PICKUPPOINT` AS `ORDER_PICKUPPOINT`,`a`.`ORDER_PICKUPPOINTNAME` AS `ORDER_PICKUPPOINTNAME` from (((((((`tb_order` `a` left join `tb_orderdetail` `c` on((`a`.`ORDER_ID` = `c`.`ORDER_ID`))) left join `vw_ordertype` `d` on((`a`.`ORDER_TYPE` = `d`.`ORDERTYPE_ID`))) left join `vw_payment` `e` on(((`a`.`ORDER_PAYMENT` = `e`.`PAYMENT_ID`) or ((`a`.`ORDER_ID` = `e`.`ORDER_ID`) and (`e`.`PAYMENT_ACTIVE` = 1))))) left join `vw_orderstatus` `f` on(((`a`.`ORDER_STATUSID` = `f`.`ORDSTATUS_ID`) or ((convert(`a`.`ORDER_STATUS` using utf8) = convert(`f`.`STATUS_ID` using utf8)) and (`a`.`ORDER_ID` = `f`.`ORDER_ID`) and (`f`.`STATUS_ACTIVE` = 1))))) left join `tb_country` `g` on((convert(`a`.`ORDER_COUNTRY` using utf8) = `g`.`COUNTRY_CODE`))) left join `tb_country` `h` on((convert(`a`.`SHIPPING_COUNTRY` using utf8) = `h`.`COUNTRY_CODE`))) left join `vw_shippingcompany` `i` on((`a`.`SHIPPING_COMPANY` = `i`.`COMPANY_ID`))) group by `a`.`ORDER_ID`;

-- --------------------------------------------------------

--
-- Structure for view `vw_orderstatus`
--
DROP TABLE IF EXISTS `vw_orderstatus`;

CREATE VIEW `vw_orderstatus` AS select `a`.`ORDSTATUS_ID` AS `ORDSTATUS_ID`,`a`.`ORDER_ID` AS `ORDER_ID`,`a`.`STATUS_ID` AS `STATUS_ID`,`b`.`ORDERSTATUS_NAME` AS `V_STATUSNAME`,`a`.`STATUS_DATE` AS `STATUS_DATE`,`a`.`STATUS_ACTIVE` AS `STATUS_ACTIVE` from (`tb_orderstatus` `a` left join `vw_orderstatuslist` `b` on((`a`.`STATUS_ID` = convert(`b`.`ORDERSTATUS_ID` using utf8))));

-- --------------------------------------------------------

--
-- Structure for view `vw_orderstatuslist`
--
DROP TABLE IF EXISTS `vw_orderstatuslist`;

CREATE VIEW `vw_orderstatuslist` AS select `a`.`LIST_NAME` AS `ORDERSTATUS_NAME`,`a`.`LIST_VALUE` AS `ORDERSTATUS_ID`,`a`.`LIST_ORDER` AS `ORDERSTATUS_ORDER`,`a`.`LIST_GROUP` AS `ORDERSTATUS_GROUP`,`a`.`LIST_PARENTVALUE` AS `ORDERSTATUS_PARENTVALUE`,`a`.`LIST_PARENTGROUP` AS `ORDERSTATUS_PARENTGROUP`,`a`.`LIST_ACTIVE` AS `ORDERSTATUS_ACTIVE` from `tb_list` `a` where ((`a`.`LIST_GROUP` = 'ORDER STATUS') and (`a`.`LIST_DEFAULT` = 1));

-- --------------------------------------------------------

--
-- Structure for view `vw_ordertype`
--
DROP TABLE IF EXISTS `vw_ordertype`;

CREATE VIEW `vw_ordertype` AS select `tb_list`.`LIST_NAME` AS `ORDERTYPE_NAME`,`tb_list`.`LIST_VALUE` AS `ORDERTYPE_ID`,`tb_list`.`LIST_ORDER` AS `ORDERTYPE_ORDER`,`tb_list`.`LIST_ACTIVE` AS `ORDERTYPE_ACTIVE` from `tb_list` where (`tb_list`.`LIST_GROUP` = 'ORDER TYPE');

-- --------------------------------------------------------

--
-- Structure for view `vw_page`
--
DROP TABLE IF EXISTS `vw_page`;

CREATE VIEW `vw_page` AS select `a`.`PAGE_ID` AS `PAGE_ID`,`a`.`PAGE_NAME` AS `PAGE_NAME`,`a`.`PAGE_DISPLAYNAME` AS `PAGE_DISPLAYNAME`,`a`.`PAGE_TITLE` AS `PAGE_TITLE`,`a`.`PAGE_CONTENT` AS `PAGE_CONTENT`,`a`.`PAGE_SHOWPAGETITLE` AS `PAGE_SHOWPAGETITLE`,`a`.`PAGE_SHOWINMENU` AS `PAGE_SHOWINMENU`,`a`.`PAGE_SHOWTOP` AS `PAGE_SHOWTOP`,`a`.`PAGE_SHOWBOTTOM` AS `PAGE_SHOWBOTTOM`,`a`.`PAGE_SHOWINBREADCRUMB` AS `PAGE_SHOWINBREADCRUMB`,`a`.`PAGE_USEREDITABLE` AS `PAGE_USEREDITABLE`,`a`.`PAGE_ENABLELINK` AS `PAGE_ENABLELINK`,`a`.`PAGE_CSSCLASS` AS `PAGE_CSSCLASS`,`a`.`PAGE_ORDER` AS `PAGE_ORDER`,`a`.`PAGE_LANG` AS `PAGE_LANG`,`b`.`LANG_NAME` AS `V_LANGNAME`,`a`.`PAGE_TEMPLATE` AS `PAGE_TEMPLATE`,`a`.`PAGE_TEMPLATEPARENT` AS `PAGE_TEMPLATEPARENT`,`c`.`PAGE_TEMPLATE` AS `V_TEMPLATEPARENT`,`a`.`PAGE_PARENT` AS `PAGE_PARENT`,`c`.`PAGE_TITLE` AS `V_PARENTNAME`,`c`.`PAGE_CSSCLASS` AS `V_PARENTCSSCLASS`,`a`.`PAGE_KEYWORD` AS `PAGE_KEYWORD`,`a`.`PAGE_DEFAULTKEYWORD` AS `PAGE_DEFAULTKEYWORD`,`a`.`PAGE_DESC` AS `PAGE_DESC`,`a`.`PAGE_DEFAULTDESC` AS `PAGE_DEFAULTDESC`,`a`.`PAGE_REDIRECT` AS `PAGE_REDIRECT`,`d`.`PAGE_TITLE` AS `V_REDIRECTNAME`,`d`.`PAGE_PARENT` AS `V_REDIRECTPARENT`,`d`.`PAGE_TEMPLATE` AS `V_REDIRECTTEMPLATE`,`d`.`PAGE_TEMPLATEPARENT` AS `V_REDIRECTTEMPLATEPARENT`,`e`.`PAGE_TEMPLATE` AS `V_REDIRECTPARENTTEMPLATE`,`a`.`PAGE_ACTIVE` AS `PAGE_ACTIVE`,`a`.`PAGE_OTHER` AS `PAGE_OTHER`,`a`.`PAGE_CREATEDBY` AS `PAGE_CREATEDBY`,`a`.`PAGE_CREATION` AS `PAGE_CREATION`,`a`.`PAGE_UPDATEDBY` AS `PAGE_UPDATEDBY`,`a`.`PAGE_LASTUPDATE` AS `PAGE_LASTUPDATE`,`a`.`PAGE_REDIRECTLINK` AS `PAGE_REDIRECTLINK`,`a`.`PAGE_REDIRECTTARGET` AS `PAGE_REDIRECTTARGET` from ((((`tb_page` `a` left join `vw_language` `b` on((convert(`a`.`PAGE_LANG` using utf8) = `b`.`LANG_ID`))) left join `tb_page` `c` on((`a`.`PAGE_PARENT` = `c`.`PAGE_ID`))) left join `tb_page` `d` on((`a`.`PAGE_REDIRECT` = `d`.`PAGE_ID`))) left join `tb_page` `e` on((`d`.`PAGE_PARENT` = `e`.`PAGE_ID`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_pagecms`
--
DROP TABLE IF EXISTS `vw_pagecms`;

CREATE VIEW `vw_pagecms` AS select `a`.`PAGECMS_ID` AS `PAGECMS_ID`,`a`.`PAGE_ID` AS `PAGE_ID`,`a`.`CUSTOM_ID` AS `CUSTOM_ID`,`a`.`CUSTOM_ID_2` AS `CUSTOM_ID_2`,`a`.`BLOCK_ID` AS `BLOCK_ID`,`a`.`CMS_ID` AS `CMS_ID`,`b`.`CMS_TITLE` AS `V_CMSTITLE`,`b`.`CMS_CONTENT` AS `V_CMSCONTENT`,`b`.`CMS_CREATION` AS `V_CMSCREATION`,`b`.`CMS_CREATEDBY` AS `V_CMSCREATEDBY`,`b`.`CMS_LASTUPDATE` AS `V_CMSLASTUPDATE`,`b`.`CMS_UPDATEDBY` AS `V_CMSUPDATEDBY`,`e`.`GRP_ACTIVE` AS `V_GRP_ACTIVE`,`b`.`CMS_ACTIVE` AS `V_CMSACTIVE`,`a`.`PAGECMS_TYPE` AS `PAGECMS_TYPE`,`c`.`CMSTYPE_NAME` AS `V_TYPENAME`,`a`.`PAGECMS_GROUP` AS `PAGECMS_GROUP`,`d`.`CMSGROUP_NAME` AS `V_GROUPNAME`,`a`.`PAGECMS_LANG` AS `PAGECMS_LANG`,`a`.`PAGECMS_ADMINVIEW` AS `PAGECMS_ADMINVIEW`,`a`.`PAGECMS_CREATION` AS `PAGECMS_CREATION`,`a`.`PAGECMS_CREATEDBY` AS `PAGECMS_CREATEDBY`,`a`.`PAGECMS_LASTUPDATE` AS `PAGECMS_LASTUPDATE`,`a`.`PAGECMS_UPDATEDBY` AS `PAGECMS_UPDATEDBY` from ((((`tb_pagecms` `a` left join `tb_cms` `b` on(((`a`.`CMS_ID` = `b`.`CMS_ID`) and (`a`.`PAGECMS_GROUP` = 1)))) left join `vw_cmstype` `c` on((`a`.`PAGECMS_TYPE` = `c`.`CMSTYPE_ID`))) left join `vw_cmsgroup` `d` on((`a`.`PAGECMS_GROUP` = `d`.`CMSGROUP_ID`))) left join `tb_slidegroup` `e` on((`a`.`CMS_ID` = `e`.`GRP_ID`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_pagecorrespond`
--
DROP TABLE IF EXISTS `vw_pagecorrespond`;

CREATE VIEW `vw_pagecorrespond` AS select `a`.`PC_ID` AS `PC_ID`,`a`.`PAGE_ID` AS `PAGE_ID`,`a`.`PC_PAGEID` AS `PC_PAGEID`,`b`.`PAGE_TITLE` AS `V_PAGETITLE`,`b`.`PAGE_ORDER` AS `V_PAGEORDER`,`b`.`PAGE_LANG` AS `V_PAGELANG`,`b`.`PAGE_TEMPLATE` AS `V_PAGETEMPLATE` from (`tb_pagecorrespond` `a` left join `tb_page` `b` on((`a`.`PC_PAGEID` = `b`.`PAGE_ID`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_payment`
--
DROP TABLE IF EXISTS `vw_payment`;

CREATE VIEW `vw_payment` AS select `a`.`PAYMENT_ID` AS `PAYMENT_ID`,`a`.`ORDER_ID` AS `ORDER_ID`,`a`.`PAYMENT_GATEWAY` AS `PAYMENT_GATEWAY`,`b`.`PAYMENTTYPE_NAME` AS `PAYMENT_GATEWAYNAME`,`a`.`PAYMENT_REMARKS` AS `PAYMENT_REMARKS`,`a`.`PAYMENT_DATE` AS `PAYMENT_DATE`,`a`.`PAYMENT_TOTAL` AS `PAYMENT_TOTAL`,`a`.`PAYMENT_BANK` AS `PAYMENT_BANK`,`a`.`PAYMENT_BANKNAME` AS `PAYMENT_BANKNAME`,`a`.`PAYMENT_BANKACCOUNT` AS `PAYMENT_BANKACCOUNT`,`a`.`PAYMENT_TRANSID` AS `PAYMENT_TRANSID`,`a`.`PAYMENT_BANKDATE` AS `PAYMENT_BANKDATE`,`a`.`PAYMENT_REJECT` AS `PAYMENT_REJECT`,`a`.`PAYMENT_REJECTREMARKS` AS `PAYMENT_REJECTREMARKS`,`a`.`PAYMENT_ACTIVE` AS `PAYMENT_ACTIVE` from (`tb_payment` `a` left join `vw_paymenttype` `b` on((`a`.`PAYMENT_GATEWAY` = `b`.`PAYMENTTYPE_ID`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_paymenttype`
--
DROP TABLE IF EXISTS `vw_paymenttype`;

CREATE VIEW `vw_paymenttype` AS select `a`.`LIST_NAME` AS `PAYMENTTYPE_NAME`,`a`.`LIST_VALUE` AS `PAYMENTTYPE_ID`,`a`.`LIST_ORDER` AS `PAYMENTTYPE_ORDER`,`a`.`LIST_ACTIVE` AS `PAYMENTTYPE_ACTIVE` from `tb_list` `a` where (`a`.`LIST_GROUP` = 'PAYMENT TYPE');

-- --------------------------------------------------------

--
-- Structure for view `vw_position`
--
DROP TABLE IF EXISTS `vw_position`;

CREATE VIEW `vw_position` AS select `a`.`LIST_NAME` AS `POSITION_NAME`,`a`.`LIST_VALUE` AS `POSITION_ID` from `tb_list` `a` where (`a`.`LIST_GROUP` = 'POSITION');

-- --------------------------------------------------------

--
-- Structure for view `vw_prodcat1`
--
DROP TABLE IF EXISTS `vw_prodcat1`;

CREATE VIEW `vw_prodcat1` AS select `a`.`PROD_ID` AS `PROD_ID`,`a`.`GRP_ID` AS `GRP_ID`,`b`.`GRP_NAME` AS `GRP_NAME` from (`tb_prodgroup` `a` join `tb_group` `b`) where ((`a`.`GRP_ID` = `b`.`GRP_ID`) and (`b`.`GRPING_ID` = 1));

-- --------------------------------------------------------

--
-- Structure for view `vw_prodcat2`
--
DROP TABLE IF EXISTS `vw_prodcat2`;

CREATE VIEW `vw_prodcat2` AS select `a`.`PROD_ID` AS `PROD_ID`,`a`.`GRP_ID` AS `GRP_ID`,`b`.`GRP_NAME` AS `GRP_NAME` from (`tb_prodgroup` `a` join `tb_group` `b`) where ((`a`.`GRP_ID` = `b`.`GRP_ID`) and (`b`.`GRPING_ID` = 2));

-- --------------------------------------------------------

--
-- Structure for view `vw_prodcat3`
--
DROP TABLE IF EXISTS `vw_prodcat3`;

CREATE VIEW `vw_prodcat3` AS select `a`.`PROD_ID` AS `PROD_ID`,`a`.`GRP_ID` AS `GRP_ID`,`b`.`GRP_NAME` AS `GRP_NAME` from (`tb_prodgroup` `a` join `tb_group` `b`) where ((`a`.`GRP_ID` = `b`.`GRP_ID`) and (`b`.`GRPING_ID` = 3));

-- --------------------------------------------------------

--
-- Structure for view `vw_prodcat4`
--
DROP TABLE IF EXISTS `vw_prodcat4`;

CREATE VIEW `vw_prodcat4` AS select `a`.`PROD_ID` AS `PROD_ID`,`a`.`GRP_ID` AS `GRP_ID`,`b`.`GRP_NAME` AS `GRP_NAME` from (`tb_prodgroup` `a` join `tb_group` `b`) where ((`a`.`GRP_ID` = `b`.`GRP_ID`) and (`b`.`GRPING_ID` = 4));

-- --------------------------------------------------------

--
-- Structure for view `vw_prodcat5`
--
DROP TABLE IF EXISTS `vw_prodcat5`;

CREATE VIEW `vw_prodcat5` AS select `a`.`PROD_ID` AS `PROD_ID`,`a`.`GRP_ID` AS `GRP_ID`,`b`.`GRP_NAME` AS `GRP_NAME` from (`tb_prodgroup` `a` join `tb_group` `b`) where ((`a`.`GRP_ID` = `b`.`GRP_ID`) and (`b`.`GRPING_ID` = 5));

-- --------------------------------------------------------

--
-- Structure for view `vw_prodcat6`
--
DROP TABLE IF EXISTS `vw_prodcat6`;

CREATE VIEW `vw_prodcat6` AS select `a`.`PROD_ID` AS `PROD_ID`,`a`.`GRP_ID` AS `GRP_ID`,`b`.`GRP_NAME` AS `GRP_NAME` from (`tb_prodgroup` `a` join `tb_group` `b`) where ((`a`.`GRP_ID` = `b`.`GRP_ID`) and (`b`.`GRPING_ID` = 6));

-- --------------------------------------------------------

--
-- Structure for view `vw_prodcat7`
--
DROP TABLE IF EXISTS `vw_prodcat7`;

CREATE VIEW `vw_prodcat7` AS select `a`.`PROD_ID` AS `PROD_ID`,`a`.`GRP_ID` AS `GRP_ID`,`b`.`GRP_NAME` AS `GRP_NAME` from (`tb_prodgroup` `a` join `tb_group` `b`) where ((`a`.`GRP_ID` = `b`.`GRP_ID`) and (`b`.`GRPING_ID` = 7));

-- --------------------------------------------------------

--
-- Structure for view `vw_prodcat8`
--
DROP TABLE IF EXISTS `vw_prodcat8`;

CREATE VIEW `vw_prodcat8` AS select `a`.`PROD_ID` AS `PROD_ID`,`a`.`GRP_ID` AS `GRP_ID`,`b`.`GRP_NAME` AS `GRP_NAME` from (`tb_prodgroup` `a` join `tb_group` `b`) where ((`a`.`GRP_ID` = `b`.`GRP_ID`) and (`b`.`GRPING_ID` = 8));

-- --------------------------------------------------------

--
-- Structure for view `vw_prodcat9`
--
DROP TABLE IF EXISTS `vw_prodcat9`;

CREATE VIEW `vw_prodcat9` AS select `a`.`PROD_ID` AS `PROD_ID`,`a`.`GRP_ID` AS `GRP_ID`,`b`.`GRP_NAME` AS `GRP_NAME` from (`tb_prodgroup` `a` join `tb_group` `b`) where ((`a`.`GRP_ID` = `b`.`GRP_ID`) and (`b`.`GRPING_ID` = 9));

-- --------------------------------------------------------

--
-- Structure for view `vw_prodcat10`
--
DROP TABLE IF EXISTS `vw_prodcat10`;

CREATE VIEW `vw_prodcat10` AS select `a`.`PROD_ID` AS `PROD_ID`,`a`.`GRP_ID` AS `GRP_ID`,`b`.`GRP_NAME` AS `GRP_NAME` from (`tb_prodgroup` `a` join `tb_group` `b`) where ((`a`.`GRP_ID` = `b`.`GRP_ID`) and (`b`.`GRPING_ID` = 10));

-- --------------------------------------------------------

--
-- Structure for view `vw_product`
--
DROP TABLE IF EXISTS `vw_product`;

CREATE VIEW `vw_product` AS select `a`.`PROD_ID` AS `PROD_ID`,`a`.`PROD_CODE` AS `PROD_CODE`,`a`.`PROD_NAME` AS `PROD_NAME`,`a`.`PROD_DNAME` AS `PROD_DNAME`,`a`.`PROD_DESC` AS `PROD_DESC`,`a`.`PROD_SNAPSHOT` AS `PROD_SNAPSHOT`,`a`.`PROD_UOM` AS `PROD_UOM`,`a`.`PROD_WEIGHT` AS `PROD_WEIGHT`,`a`.`PROD_LENGTH` AS `PROD_LENGTH`,`a`.`PROD_WIDTH` AS `PROD_WIDTH`,`a`.`PROD_HEIGHT` AS `PROD_HEIGHT`,`a`.`PROD_NEW` AS `PROD_NEW`,`a`.`PROD_SHOWTOP` AS `PROD_SHOWTOP`,`a`.`PROD_ACTIVE` AS `PROD_ACTIVE`,`a`.`PROD_BEST` AS `PROD_BEST`,`a`.`PROD_IMAGE` AS `PROD_IMAGE`,`a`.`PROD_NOV` AS `PROD_NOV`,`a`.`PROD_NOP` AS `PROD_NOP`,`a`.`PROD_CREATEDBY` AS `PROD_CREATEDBY`,`a`.`PROD_CREATION` AS `PROD_CREATION`,`a`.`PROD_UPDATEDBY` AS `PROD_UPDATEDBY`,`a`.`PROD_LASTUPDATE` AS `PROD_LASTUPDATE`,`b`.`GRP_ID` AS `V_CAT1`,`b`.`GRP_NAME` AS `V_CAT1NAME`,`c`.`GRP_ID` AS `V_CAT2`,`c`.`GRP_NAME` AS `V_CAT2NAME`,`e`.`GRP_ID` AS `V_CAT3`,`e`.`GRP_NAME` AS `V_CAT3NAME`,`f`.`GRP_ID` AS `V_CAT4`,`f`.`GRP_NAME` AS `V_CAT4NAME`,`g`.`GRP_ID` AS `V_CAT5`,`g`.`GRP_NAME` AS `V_CAT5NAME`,`h`.`GRP_ID` AS `V_CAT6`,`h`.`GRP_NAME` AS `V_CAT6NAME`,`i`.`GRP_ID` AS `V_CAT7`,`i`.`GRP_NAME` AS `V_CAT7NAME`,`j`.`GRP_ID` AS `V_CAT8`,`j`.`GRP_NAME` AS `V_CAT8NAME`,`k`.`GRP_ID` AS `V_CAT9`,`k`.`GRP_NAME` AS `V_CAT9NAME`,`l`.`GRP_ID` AS `V_CAT10`,`l`.`GRP_NAME` AS `V_CAT10NAME`,`d`.`PRODPRC_COST` AS `PROD_COST`,`d`.`PRODPRC_PRICE` AS `PROD_PRICE`,`d`.`PRODPRC_PROMPRICE` AS `PROD_PROMPRICE`,`d`.`PRODPRC_PROMSTARTDATE` AS `PROD_PROMSTARTDATE`,`d`.`PRODPRC_PROMENDDATE` AS `PROD_PROMENDDATE` from (((((((((((`tb_product` `a` left join `vw_prodcat1` `b` on((`a`.`PROD_ID` = `b`.`PROD_ID`))) left join `vw_prodcat2` `c` on((`a`.`PROD_ID` = `c`.`PROD_ID`))) left join `tb_prodprice` `d` on((`a`.`PROD_ID` = `d`.`PROD_ID`))) left join `vw_prodcat3` `e` on((`a`.`PROD_ID` = `e`.`PROD_ID`))) left join `vw_prodcat4` `f` on((`a`.`PROD_ID` = `f`.`PROD_ID`))) left join `vw_prodcat5` `g` on((`a`.`PROD_ID` = `g`.`PROD_ID`))) left join `vw_prodcat6` `h` on((`a`.`PROD_ID` = `h`.`PROD_ID`))) left join `vw_prodcat7` `i` on((`a`.`PROD_ID` = `i`.`PROD_ID`))) left join `vw_prodcat8` `j` on((`a`.`PROD_ID` = `j`.`PROD_ID`))) left join `vw_prodcat9` `k` on((`a`.`PROD_ID` = `k`.`PROD_ID`))) left join `vw_prodcat10` `l` on((`a`.`PROD_ID` = `l`.`PROD_ID`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_relproduct`
--
DROP TABLE IF EXISTS `vw_relproduct`;

CREATE VIEW `vw_relproduct` AS select `a`.`PROD_ID` AS `PROD_ID`,`b`.`PROD_ID` AS `RELPROD_ID`,`b`.`PROD_CODE` AS `RELPROD_CODE`,`b`.`PROD_NAME` AS `RELPROD_NAME`,`b`.`PROD_DNAME` AS `RELPROD_DNAME`,`b`.`PROD_DESC` AS `RELPROD_DESC`,`b`.`PROD_SNAPSHOT` AS `RELPROD_SNAPSHOT`,`b`.`PROD_UOM` AS `RELPROD_UOM`,`b`.`PROD_WEIGHT` AS `RELPROD_WEIGHT`,`b`.`PROD_LENGTH` AS `RELPROD_LENGTH`,`b`.`PROD_WIDTH` AS `RELPROD_WIDTH`,`b`.`PROD_HEIGHT` AS `RELPROD_HEIGHT`,`b`.`PROD_NEW` AS `RELPROD_NEW`,`b`.`PROD_SHOWTOP` AS `RELPROD_SHOWTOP`,`b`.`PROD_ACTIVE` AS `RELPROD_ACTIVE`,`b`.`PROD_IMAGE` AS `RELPROD_IMAGE`,`b`.`PROD_NOV` AS `RELPROD_NOV`,`b`.`PROD_NOP` AS `RELPROD_NOP`,`b`.`PROD_CREATEDBY` AS `RELPROD_CREATEDBY`,`b`.`PROD_CREATION` AS `RELPROD_CREATION`,`b`.`PROD_UPDATEDBY` AS `RELPROD_UPDATEDBY`,`b`.`PROD_LASTUPDATE` AS `RELPROD_LASTUPDATE`,`b`.`V_CAT1` AS `RELPROD_CAT1`,`b`.`V_CAT1NAME` AS `RELPROD_CAT1NAME`,`b`.`V_CAT2` AS `RELPROD_CAT2`,`b`.`V_CAT2NAME` AS `RELPROD_CAT2NAME`,`b`.`PROD_COST` AS `RELPROD_COST`,`b`.`PROD_PRICE` AS `RELPROD_PRICE`,`b`.`PROD_PROMPRICE` AS `RELPROD_PROMPRICE`,`b`.`PROD_PROMSTARTDATE` AS `RELPROD_PROMSTARTDATE`,`b`.`PROD_PROMENDDATE` AS `RELPROD_PROMENDDATE` from (`tb_relatedprodlist` `a` left join `vw_product` `b` on((`a`.`RELPRODLIST_RELPRODID` = `b`.`PROD_ID`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_shipping2`
--
DROP TABLE IF EXISTS `vw_shipping2`;

CREATE VIEW `vw_shipping2` AS select `a`.`SHIP_ID` AS `SHIP_ID`,`a`.`SHIP_KG_FIRST` AS `SHIP_KG_FIRST`,`a`.`SHIP_FEE_FIRST` AS `SHIP_FEE_FIRST`,`a`.`SHIP_KG` AS `SHIP_KG`,`a`.`SHIP_FEE` AS `SHIP_FEE`,`a`.`SHIP_COUNTRY` AS `SHIP_COUNTRY`,`b`.`COUNTRY_NAME` AS `V_SHIPCOUNTRYNAME`,`a`.`SHIP_STATE` AS `SHIP_STATE`,`a`.`SHIP_ACTIVE` AS `SHIP_ACTIVE`,`a`.`SHIP_CREATION` AS `SHIP_CREATION`,`a`.`SHIP_CREATEDBY` AS `SHIP_CREATEDBY`,`a`.`SHIP_LASTUPDATE` AS `SHIP_LASTUPDATE`,`a`.`SHIP_UPDATEDBY` AS `SHIP_UPDATEDBY`,`a`.`SHIP_DEFAULT` AS `SHIP_DEFAULT`,`a`.`SHIP_FEEFREE` AS `SHIP_FEEFREE` from (`tb_shipping2` `a` left join `tb_country` `b` on((`a`.`SHIP_COUNTRY` = `b`.`COUNTRY_CODE`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_shippingcompany`
--
DROP TABLE IF EXISTS `vw_shippingcompany`;

CREATE VIEW `vw_shippingcompany` AS select `tb_list`.`LIST_NAME` AS `COMPANY_NAME`,`tb_list`.`LIST_VALUE` AS `COMPANY_ID`,`tb_list`.`LIST_ORDER` AS `COMPANY_ORDER`,`tb_list`.`LIST_ACTIVE` AS `COMPANY_ACTIVE` from `tb_list` where (`tb_list`.`LIST_GROUP` = 'SHIPPING COMPANY');

-- --------------------------------------------------------

--
-- Structure for view `vw_state`
--
DROP TABLE IF EXISTS `vw_state`;

CREATE VIEW `vw_state` AS select `tb_list`.`LIST_NAME` AS `STATE_NAME`,`tb_list`.`LIST_VALUE` AS `STATE_ID`,`tb_list`.`LIST_ORDER` AS `STATE_ORDER`,`tb_list`.`LIST_ACTIVE` AS `STATE_ACTIVE`,`tb_list`.`LIST_PARENTVALUE` AS `COUNTRY_VALUE` from `tb_list` where ((`tb_list`.`LIST_GROUP` like '%STATE%') and (`tb_list`.`LIST_PARENTGROUP` = 'COUNTRY'));

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_enquiry_field_attribute`
--
ALTER TABLE `tb_enquiry_field_attribute`
  ADD CONSTRAINT `tb_enquiry_field_attribute_ibfk_1` FOREIGN KEY (`enqfield_id`) REFERENCES `tb_enquiry_field` (`enqfield_id`);

--
-- Constraints for table `tb_enquiry_field_label`
--
ALTER TABLE `tb_enquiry_field_label`
  ADD CONSTRAINT `tb_enquiry_field_label_ibfk_1` FOREIGN KEY (`enqfield_id`) REFERENCES `tb_enquiry_field` (`enqfield_id`);