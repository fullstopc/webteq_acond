ALTER TABLE tb_reservation 
DROP COLUMN res_service_addr_unit;

ALTER TABLE tb_reservation 
DROP COLUMN res_service_area;
	
ALTER TABLE tb_reservation
ADD COLUMN property_unitno VARCHAR(250) NOT NULL;

ALTER TABLE tb_reservation
ADD COLUMN property_area INT DEFAULT 0;

ALTER TABLE tb_reservation
ADD COLUMN property_type INT DEFAULT 0;

ALTER TABLE tb_reservation
ADD COLUMN property_type_name VARCHAR(250) NOT NULL;

ALTER TABLE tb_reservation
ADD COLUMN property_area_name VARCHAR(250) NOT NULL;