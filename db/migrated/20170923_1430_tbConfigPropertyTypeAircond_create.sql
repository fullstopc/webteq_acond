DROP TABLE IF EXISTS tb_config_property_type_aircond;
CREATE TABLE `tb_config_property_type_aircond` 
  ( 
     `aircond_id`            INT NOT NULL auto_increment, 
     `aircond_name`          VARCHAR(250) NOT NULL,  
     `aircond_charge`        DECIMAL NOT NULL DEFAULT 0,
     `aircond_creation`      DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
     `aircond_createdby`     INT NOT NULL DEFAULT '0', 
     `aircond_lastupdated`   DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
     `aircond_updatedby`     INT NOT NULL DEFAULT '0', 
     `type_id`           INT NOT NULL DEFAULT 0, 
     PRIMARY KEY (`aircond_id`) 
  ) 
engine = innodb; 